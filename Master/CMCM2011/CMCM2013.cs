﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;
using System.Security.Cryptography;
using jp.co.fsi.common.mynumber;

namespace jp.co.fsi.cm.cmcm2011
{
    /// <summary>
    /// 船主マスタ登録(CMCM2012)
    /// </summary>
    public partial class CMCM2013 : BasePgForm
    {
        #region 定数
        //パスワードに使用する文字
        private static readonly string passwordChars = "0123456789abcdefghijklmnopqrstuvwxyz";
        #endregion

        #region プロパティ
        /// <summary>
        /// 公開フラグ用変数
        /// </summary>
        private bool _numberFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._numberFlg;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public CMCM2013()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region メソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // タイトルは非表示
            this.lblTitle.Visible = false;

            //ボタン表示
            this.ShowFButton = true;


            // ボタンの配置を調整
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF1.Location = this.btnF2.Location;
            this.btnF6.Location = this.btnF3.Location;

            // ボタンを調整
            this.btnF1.Visible = true;
            this.btnF2.Visible = false;
            this.btnF3.Visible = false;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF6.Visible = true;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;

            // 設定値の表示
            InitDisp();
            // フォーカス設定
            this.txtTankaShutokuHoho.Focus();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 支所,郵便番号1・2,住所1・2,担当者CD,消費税入力方法,日付(年),船名CD,漁法CD,地区CDにフォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtShohizeiNyuryokuHoho":
                    this.btnF1.Enabled = true;
                    break;
                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;
            String[] result;

            switch (this.ActiveCtlNm)
            {
                case "txtShohizeiNyuryokuHoho":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom("CMCM1041.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1041.CMCM1041");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "TB_ZM_F_SHOHIZEI_NYURYOKU_HOHO";
                            frm.InData = this.txtShohizeiNyuryokuHoho.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.txtShohizeiNyuryokuHoho.Text = result[0];
                                this.lblShzNrkHohoNm.Text = result[1];
                            }
                        }
                    }
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF6();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF6()
        {
            // 確認メッセージを表示
            string msg = "登録しますか？";
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            // 入力値の保存
            SaveData();

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 単価取得方法の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTankaShutokuHoho_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidTankaShutokuHoho())
            {
                e.Cancel = true;
                this.txtTankaShutokuHoho.SelectAll();
            }
        }

        /// <summary>
        /// 金額端数処理の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKingakuHasuShori_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidKingakuHasuShori())
            {
                e.Cancel = true;
                this.txtKingakuHasuShori.SelectAll();
            }
        }

        /// <summary>
        /// 消費税入力方法の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShohizeiNyuryokuHoho_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShohizeiNyuryokuHoho())
            {
                e.Cancel = true;
                this.txtShohizeiNyuryokuHoho.SelectAll();
            }
        }

        /// <summary>
        /// 消費税端数処理の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShohizeiHasuShori_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShohizeiHasuShori())
            {
                e.Cancel = true;
                this.txtShohizeiHasuShori.SelectAll();
            }
        }

        /// <summary>
        /// 消費税転嫁方法の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShohizeiTenkaHoho_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShohizeiTenkaHoho())
            {
                e.Cancel = true;
                this.txtShohizeiTenkaHoho.SelectAll();
            }
        }

        private void txtShohizeiTenkaHoho_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.PressF6();
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 単価取得方法の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidTankaShutokuHoho()
        {
            // 未入力は0とする
            if (ValChk.IsEmpty(this.txtTankaShutokuHoho.Text))
            {
                this.txtTankaShutokuHoho.Text = "0";
            }

            // 0,1,2のみ入力を許可
            if (!ValChk.IsNumber(this.txtTankaShutokuHoho.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            int intval = Util.ToInt(this.txtTankaShutokuHoho.Text);
            if (intval < 0 || intval > 2)
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 金額端数処理の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidKingakuHasuShori()
        {
            // 未入力は1とする
            if (ValChk.IsEmpty(this.txtKingakuHasuShori.Text))
            {
                this.txtKingakuHasuShori.Text = "1";
            }

            // 1,2,3のみ入力を許可
            if (!ValChk.IsNumber(this.txtKingakuHasuShori.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            int intval = Util.ToInt(this.txtKingakuHasuShori.Text);
            if (intval < 1 || intval > 3)
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 消費税入力方法の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShohizeiNyuryokuHoho()
        {
            // 未入力は0とする
            if (ValChk.IsEmpty(this.txtShohizeiNyuryokuHoho.Text))
            {
                this.txtShohizeiNyuryokuHoho.Text = "0";
            }

            // 0,1,2,3のみ入力を許可
            if (!ValChk.IsNumber(this.txtShohizeiNyuryokuHoho.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 登録済みの値のみ許可
            string name = this.Dba.GetName(this.UInfo, "TB_ZM_F_SHOHIZEI_NYURYOKU_HOHO", "", this.txtShohizeiNyuryokuHoho.Text);
            if (ValChk.IsEmpty(name))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            this.lblShzNrkHohoNm.Text = name;
            return true;
        }

        /// <summary>
        /// 消費税端数処理の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShohizeiHasuShori()
        {
            // 未入力は1とする
            if (ValChk.IsEmpty(this.txtShohizeiHasuShori.Text))
            {
                this.txtShohizeiHasuShori.Text = "1";
            }

            // 1,2,3のみ入力許可
            if (!ValChk.IsNumber(this.txtShohizeiHasuShori.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            int intval = Util.ToInt(this.txtShohizeiHasuShori.Text);
            if (intval < 1 || intval > 3)
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 消費税転嫁方法の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShohizeiTenkaHoho()
        {
            // 未入力は1とする
            if (ValChk.IsEmpty(this.txtShohizeiTenkaHoho.Text))
            {
                this.txtShohizeiTenkaHoho.Text = "1";
            }

            // 1,2,3のみ入力許可
            if (!ValChk.IsNumber(this.txtShohizeiTenkaHoho.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            int intval = Util.ToInt(this.txtShohizeiTenkaHoho.Text);
            if (intval < 1 || intval > 3)
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }
        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 単価取得方法のチェック
            if (!IsValidTankaShutokuHoho())
            {
                this.txtTankaShutokuHoho.Focus();
                return false;
            }

            // 金額端数処理のチェック
            if (!IsValidKingakuHasuShori())
            {
                this.txtKingakuHasuShori.Focus();
                return false;
            }

            // 消費税入力方法のチェック
            if (!IsValidShohizeiNyuryokuHoho())
            {
                this.txtShohizeiNyuryokuHoho.Focus();
                return false;
            }

            // 消費税端数処理のチェック
            if (!IsValidShohizeiHasuShori())
            {
                this.txtShohizeiHasuShori.Focus();
                return false;
            }

            // 消費税転嫁方法のチェック
            if (!IsValidShohizeiTenkaHoho())
            {
                this.txtShohizeiTenkaHoho.Focus();
                return false;
            }

            return true;
        }

        /// <summary>
        /// 初期表示
        /// </summary>
        private void InitDisp()
        {
            // 初期値、入力制御を実装
            string ret = "";
            // 単価取得方法0
            ret = Util.ToString(this.Config.LoadPgConfig(common.constants.Constants.SubSys.Han, this.ProductName, "Setting", "TankaShutokuHoho"));
            this.txtTankaShutokuHoho.Text = ret;
            // 金額端数処理2
            ret = Util.ToString(this.Config.LoadPgConfig(common.constants.Constants.SubSys.Han, this.ProductName, "Setting", "KingakuHasuShori"));
            this.txtKingakuHasuShori.Text = ret;
            // 消費税入力方法2
            ret = Util.ToString(this.Config.LoadPgConfig(common.constants.Constants.SubSys.Han, this.ProductName, "Setting", "ShohizeiNyuryokuHoho"));
            this.txtShohizeiNyuryokuHoho.Text = ret;
            this.lblShzNrkHohoNm.Text = this.Dba.GetName(this.UInfo, "TB_ZM_F_SHOHIZEI_NYURYOKU_HOHO", "", this.txtShohizeiNyuryokuHoho.Text);
            // 消費税端数処理2
            ret = Util.ToString(this.Config.LoadPgConfig(common.constants.Constants.SubSys.Han, this.ProductName, "Setting", "ShohizeiHasuShori"));
            this.txtShohizeiHasuShori.Text = ret;
            // 消費税転嫁方法2
            ret = Util.ToString(this.Config.LoadPgConfig(common.constants.Constants.SubSys.Han, this.ProductName, "Setting", "ShohizeiTenkaHoho"));
            this.txtShohizeiTenkaHoho.Text = ret;
            // 正式船主名に初期フォーカス
            this.ActiveControl = this.txtTankaShutokuHoho;
            this.txtTankaShutokuHoho.Focus();
            // 削除ボタン非表示
            this.btnF3.Enabled = false;
        }

        /// <summary>
        /// 設定の保存
        /// </summary>
        private void SaveData()
        {
            // 初期値、入力制御を実装
            string ret = "";
            // 単価取得方法0
            ret = this.txtTankaShutokuHoho.Text;
            this.Config.SetPgConfig(common.constants.Constants.SubSys.Han, this.ProductName, "Setting", "TankaShutokuHoho", ret);
            // 金額端数処理2
            ret = this.txtKingakuHasuShori.Text;
            this.Config.SetPgConfig(common.constants.Constants.SubSys.Han, this.ProductName, "Setting", "KingakuHasuShori", ret);
            // 消費税入力方法2
            ret = this.txtShohizeiNyuryokuHoho.Text;
            this.Config.SetPgConfig(common.constants.Constants.SubSys.Han, this.ProductName, "Setting", "ShohizeiNyuryokuHoho", ret);
            // 消費税端数処理2
            ret = this.txtShohizeiHasuShori.Text;
            this.Config.SetPgConfig(common.constants.Constants.SubSys.Han, this.ProductName, "Setting", "ShohizeiHasuShori", ret);
            // 消費税転嫁方法2
            ret = this.txtShohizeiTenkaHoho.Text;
            this.Config.SetPgConfig(common.constants.Constants.SubSys.Han, this.ProductName, "Setting", "ShohizeiTenkaHoho", ret);

            // 設定した値のセーブ
            this.Config.SaveConfig();
        }
        #endregion
    }
}
