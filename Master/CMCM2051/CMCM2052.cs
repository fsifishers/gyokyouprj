﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Reflection;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.cm.cmcm2051
{
    /// <summary>
    /// 売上自動仕訳の設定[発生](CMCM2052)
    /// </summary>
    public partial class CMCM2052 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// モード(新規)
        /// </summary>
        private const string MODE_NEW = "1";

        /// <summary>
        /// モード(編集)
        /// </summary>
        private const string MODE_EDIT = "2";

        /// <summary>
        /// 設定区分
        /// </summary>
        private const string SETTEI_KUBUN = "1";

        /// <summary>
        /// 部門有無
        /// </summary>
        private const string BUMON_UMU = "0";

        /// <summary>
        /// 補助有無
        /// </summary>
        private const string HOJO_UMU = "0";
        #endregion

        string _SHISHO_CD;
        public string SHISHO_CD
        {
            get
            {
                return this._SHISHO_CD;
            }
            set
            {
                this._SHISHO_CD = value;
            }
        }

        #region プロパティ
        /// <summary>
        /// 画面上最後となるフォーカスのEnterボタン押下時処理用変数
        /// </summary>
        private bool _dtFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._dtFlg;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public CMCM2052()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="par1">引数1</param>
        /// <param name="par2">引数2</param>
        /// <param name="par3">引数3</param>
        public CMCM2052(string par1,string par2,string par3) : base(par1,par2,par3)
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            //タイトルを非表示
            this.lblTitle.Visible = false;

            // 引数：Par1／モード(1:新規、2:変更)、InData：担当者コード
            if (MODE_NEW.Equals(this.Par1))
            {
                // 新規モードの初期表示
                InitDispOnNew();
                IsValidJigyoKbn();
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // 編集モードの初期表示
                InitDispOnEdit();
            }
            else
            {
                // 不正な起動として閉じる
                Msg.Error("不正な起動です。終了します。");
                this.Close();
            }

            // 水揚支所
            this.txtShishoCd.Text = SHISHO_CD;
            this.txtShishoCd.Enabled = (SHISHO_CD == "1") ? true : false;
            this.lblShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", SHISHO_CD, SHISHO_CD);

            // ボタンの表示非表示を設定
            this.btnF6.Location = this.btnF4.Location;
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF1.Location = this.btnF2.Location;

            this.btnF1.Visible = true;
            this.btnF2.Visible = false;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;

            // メンテナンス画面の場合
            if (this.Text == "")
            {
                this.btnF1.Text = "F1" + "\n\r" + "\n\r" + "検索";
                this.btnF3.Text = "F3" + "\n\r" + "\n\r" + "削除";
                if (MODE_NEW.Equals(this.Par1))
                {
                    this.btnF6.Text = "F6" + "\n\r" + "\n\r" + "登録";
                }
                else if (MODE_EDIT.Equals(this.Par1))
                {
                    this.btnF6.Text = "F6" + "\n\r" + "\n\r" + "更新";
                }
                this.btnEsc.Text = "Esc" + "\n\r" + "\n\r" + "戻る";

                string ttl = "";
                string ttlsub = (this.Par3 == "1" ? "(相手)" : "(発生)");
                switch (this.Par2)
                {
                    case "1":
                        ttl = "売上自動仕訳" + ttlsub + "設定";
                        break;
                    case "2":
                        ttl = "仕入自動仕訳" + ttlsub + "設定";
                        break;
                    case "3":
                        ttl = "セリ自動仕訳" + ttlsub + "設定";
                        break;
                    case "4":
                        ttl = "控除自動仕訳" + ttlsub + "設定";
                        break;
                    case "5":
                        ttl = "支払自動仕訳" + ttlsub + "設定";
                        break;
                }
                this.Text = ttl;
            }

            // Enter処理を無効化
            this._dtFlg = false;
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 水揚支所,売上仕訳コード,仕入仕訳コード,商品区分1～5にフォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd":
                case "txtKanjoKamokuCd":
                case "txtHojoKamokuCd":
                case "txtBumonCd":
                case "txtZeiKbn":
                case "txtJigyoKbn":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;
            String[] result;

            switch (this.ActiveCtlNm)
            {
                case "txtShishoCd": // 水揚支所
                    // アセンブリのロード
                    asm = Assembly.LoadFrom("CMCM2031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2031.CMCM2031");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtShishoCd.Text = outData[0];
                                this.lblShishoNm.Text = outData[1];
                            }
                        }
                    }
                    break;

                case "txtShiwakeCd":
                    // TODO:コード体系をいじることもないだろうし、一旦保留
                    //Msg.Notice("TODO:仕訳コード検索画面を起動しないといけない");
                    break;

                case "txtKanjoKamokuCd":
                    // 選択会計年度チェック
                    DateTime today = DateTime.Today;
                    if (Util.GetKaikeiNendo(today, this.Dba) != this.UInfo.KaikeiNendo)
                    {
                        if (Msg.ConfNmYesNo("勘定科目", this.UInfo.KaikeiNendo + "年度の勘定科目で検索しますが、宜しいですか？") == DialogResult.No)
                        {
                            return;
                        }
                    }

                    //アセンブリのロード
                    asm = Assembly.LoadFrom("ZMCM1031.exe");
                    // フォーム作成
                    //t = asm.GetType("jp.co.fsi.zm.zmcm1031.ZMCM1031"); ;
                    t = asm.GetType("jp.co.fsi.zm.zmcm1031.ZMCM1033"); ;
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            //frm.Par1 = "1";
                            frm.Par1 = "2";
                            frm.InData = this.txtKanjoKamokuCd.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.txtKanjoKamokuCd.Text = result[0];
                                this.lblKanjoKamokuNm.Text = result[1];

                                IsValidKanjoKamokuCd(false);
                            }
                        }
                    }
                    break;

                case "txtHojoKamokuCd":
                    //アセンブリのロード
                    asm = Assembly.LoadFrom("ZMCM1041.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.zm.zmcm1041.ZMCM1044");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.Par2 = this.txtShishoCd.Text;
                            frm.InData = this.txtKanjoKamokuCd.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.txtHojoKamokuCd.Text = result[0];
                                this.lblHojoKamokuNm.Text = result[1];
                            }
                        }
                    }
                    break;

                case "txtBumonCd":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom("CMCM2041.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2041.CMCM2041");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            //frm.Par1 = "TB_CM_BUMON";
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.txtBumonCd.Text = result[0];
                                this.lblBumonNm.Text = result[1];
                            }
                        }
                    }
                    break;

                case "txtZeiKbn":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom("ZMCM1061.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.zm.zmcm1061.ZMCM1061");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            //frm.Par1 = "TB_ZM_F_ZEI_KUBUN";
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.txtZeiKbn.Text = result[0];
                                this.lblZeiKbnNm.Text = result[1];
                            }
                        }
                    }
                    break;

                case "txtJigyoKbn":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom("CMCM1041.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1041.CMCM1041");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "TB_ZM_F_JIGYO_KUBUN";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.txtJigyoKbn.Text = result[0];
                                this.lblJigyoKbnNm.Text = result[1];
                            }
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// F3キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF3();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF3()
        {
            if (MODE_NEW.Equals(Par1))
            {
                return;
            }

            if (Msg.ConfYesNo("削除しますか？") == DialogResult.No)
            {
                return;
            }

            // 使用するテーブルを設定
            string table;
            if (SETTEI_KUBUN.Equals(this.Par3))
            {
                table = " TB_HN_ZIDO_SHIWAKE_SETTEI_A ";
            }
            else
            {
                table = " TB_HN_ZIDO_SHIWAKE_SETTEI_B ";
            }

            try
            {
                this.Dba.BeginTransaction();

                StringBuilder Sql = new StringBuilder();
                DbParamCollection dpc = new DbParamCollection();
                Sql.Append("DELETE ");
                Sql.Append("FROM");
                Sql.Append( table );
                Sql.Append("WHERE");
                Sql.Append(" KAISHA_CD = @KAISHA_CD AND");
                Sql.Append(" DENPYO_KUBUN = @DENPYO_KUBUN AND");
                Sql.Append(" SHIWAKE_CD = @SHIWAKE_CD ");
                Sql.Append(" AND SHISHO_CD = @SHISHO_CD ");

                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                dpc.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 6, this.Par2);
                dpc.SetParam("@SHIWAKE_CD", SqlDbType.Decimal, 6, Util.ToDecimal(txtShiwakeCd.Text));
                dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToDecimal(SHISHO_CD));

                this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                // トランザクションをコミット
                this.Dba.Commit();

                Msg.Info("削除しました。");
            }
            catch
            {
                Msg.Info("削除に失敗しました。");
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF6();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF6()
        {
            // 確認メッセージを表示
            string msg = (MODE_NEW.Equals(this.Par1) ? "登録" : "更新") + "しますか？";
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            // 使用するテーブルを設定
            string table;
            if (SETTEI_KUBUN.Equals(this.Par3))
            {
                table = "TB_HN_ZIDO_SHIWAKE_SETTEI_A";
            }
            else
            {
                table = "TB_HN_ZIDO_SHIWAKE_SETTEI_B";
            }

            // 入力値をバインドパラメータとしてセットする
            ArrayList alParamsCmShiwake = SetCmShiwakeParams();

            try
            {
                this.Dba.BeginTransaction();

                if (MODE_NEW.Equals(this.Par1))
                {
                    // データ登録
                    // 販売.商品
                    this.Dba.Insert(table, (DbParamCollection)alParamsCmShiwake[0]);
                }
                else if (MODE_EDIT.Equals(this.Par1))
                {
                    // データ更新
                    // 販売.商品
                    this.Dba.Update(table,
                        (DbParamCollection)alParamsCmShiwake[1],
                        "KAISHA_CD = @KAISHA_CD AND SHIWAKE_CD = @SHIWAKE_CD AND DENPYO_KUBUN = @DENPYO_KUBUN AND SHISHO_CD = @SHISHO_CD",
                        (DbParamCollection)alParamsCmShiwake[0]);
                }

                // トランザクションをコミット
                this.Dba.Commit();

                Msg.Info("登録しました。");
            }
            catch
            {
                Msg.Info("登録に失敗しました。");
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 水揚支所入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMizuageShishoCd_Validating(object sender, CancelEventArgs e)
        {
            if (!this.IsValidMizuageShishoCd())
            {
                e.Cancel = true;

                this.lblShishoNm.Text = "";
                this.txtShishoCd.SelectAll();
                this.txtShishoCd.Focus();
            }
        }

        /// <summary>
        /// 仕訳コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShiwakeCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShiwakeCd())
            {
                e.Cancel = true;
                this.txtShiwakeCd.SelectAll();
            }
        }

        /// <summary>
        /// 仕訳名称の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShiwakeNm_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShiwakeNm())
            {
                e.Cancel = true;
                this.txtShiwakeNm.SelectAll();
            }
        }

        /// <summary>
        /// 勘定科目コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKanjoKamokuCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidKanjoKamokuCd(false))
            {
                e.Cancel = true;
                this.txtKanjoKamokuCd.SelectAll();
            }
        }

        /// <summary>
        /// 補助科目コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtHojoKamokuCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidHojoKamokuCd())
            {
                e.Cancel = true;
                this.txtHojoKamokuCd.SelectAll();
            }
        }

        /// <summary>
        /// 部門コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtBumonCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidBumonCd())
            {
                e.Cancel = true;
                this.txtBumonCd.SelectAll();
            }
        }

        /// <summary>
        /// 税区分の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtZeiKubun_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidZeiKbn())
            {
                e.Cancel = true;
                this.txtZeiKbn.SelectAll();
            }
        }

        /// <summary>
        /// 事業区分の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtJigyoKbn_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidJigyoKbn())
            {
                e.Cancel = true;
                this.txtJigyoKbn.SelectAll();

                // Enter処理を無効化
                this._dtFlg = false;
            }
            else
            {
                if (!this.txtTaishakuKbn.Enabled)
                {
                    // Enter処理を有効化
                    this._dtFlg = true;
                }
            }
        }

        /// <summary>
        /// 事業区分のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtJigyoKbn_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.Flg)
            {
                // Enter処理を無効化
                this._dtFlg = false;

                PressF6();
            }
        }

        /// <summary>
        /// 貸借区分の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTaishakuKbn_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidTaishakuKbn())
            {
                e.Cancel = true;
                this.txtTaishakuKbn.SelectAll();

                // Enter処理を無効化
                this._dtFlg = false;
            }
            else
            {
                // Enter処理を有効化
                this._dtFlg = true;
            }
        }

        /// <summary>
        /// 貸借区分のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTaishakuKbn_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.Flg)
            {
                // Enter処理を無効化
                this._dtFlg = false;

                PressF6();
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 水揚支所の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool IsValidMizuageShishoCd()
        {
            // 空入力の場合
            if (ValChk.IsEmpty(this.txtShishoCd.Text))
            {
                // 水揚支所名称を表示する
                this.lblShishoNm.Text = "全て";
                return true;
            }

            // 最大桁数チェック
            if (!ValChk.IsWithinLength(this.txtShishoCd.Text, this.txtShishoCd.MaxLength))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            // 数値チェック
            if (!ValChk.IsNumber(this.txtShishoCd.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                return false;
            }

            // 0入力の場合
            if (Equals(this.txtShishoCd.Text, "0"))
            {
                // 水揚支所名称を表示する
                this.lblShishoNm.Text = "全て";
                return true;
            }

            // 水揚支所名称を表示する
            string shishoCd = this.txtShishoCd.Text;
            this.lblShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", shishoCd, shishoCd);

            if (ValChk.IsEmpty(this.lblShishoNm.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 仕訳コードの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShiwakeCd()
        {
            // 未入力はエラーとする
            if (ValChk.IsEmpty(this.txtShiwakeCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtShiwakeCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 使用するテーブルを設定
            string table;
            if (SETTEI_KUBUN.Equals(this.Par3))
            {
                table = "TB_HN_ZIDO_SHIWAKE_SETTEI_A";
            }
            else
            {
                table = "TB_HN_ZIDO_SHIWAKE_SETTEI_B";
            }

            // 既に存在するコードを入力した場合はエラーとする
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 2, this.Par2);
            dpc.SetParam("@SHIWAKE_CD", SqlDbType.Decimal, 13, this.txtShiwakeCd.Text);
            StringBuilder where = new StringBuilder("KAISHA_CD = @KAISHA_CD");
            where.Append(" AND DENPYO_KUBUN = @DENPYO_KUBUN");
            where.Append(" AND SHIWAKE_CD = @SHIWAKE_CD");
            DataTable dtTanto =
                this.Dba.GetDataTableByConditionWithParams("SHIWAKE_CD",
                    table, Util.ToString(where), dpc);
            if (dtTanto.Rows.Count > 0)
            {
                Msg.Error("既に存在する仕訳コードと重複しています。");
                return false;
            }
            return true;
        }

        /// <summary>
        /// 仕訳名称の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShiwakeNm()
        {
            // 30バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtShiwakeNm.Text, this.txtShiwakeNm.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 勘定科目コードの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidKanjoKamokuCd(bool all)
        {
            // 未入力は0とする
            if (ValChk.IsEmpty(this.txtKanjoKamokuCd.Text))
            {
                this.txtKanjoKamokuCd.Text = "0";
            }

            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtKanjoKamokuCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 勘定科目名の取得
            StringBuilder cols = new StringBuilder();
            cols.Append("*  ");

            StringBuilder from = new StringBuilder();
            from.Append(" VI_ZM_KANJO_KAMOKU");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            dpc.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 6, this.txtKanjoKamokuCd.Text);

            DataTable dtDispData =
                this.Dba.GetDataTableByConditionWithParams(
                    Util.ToString(cols), Util.ToString(from),
                    "KAISHA_CD = @KAISHA_CD AND KAIKEI_NENDO = @KAIKEI_NENDO AND KANJO_KAMOKU_CD = @KANJO_KAMOKU_CD",
                    dpc);

            if (dtDispData.Rows.Count == 0)
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            //else  ValidateAllの時には実行しないように修正
            else if (all == false)
            {
                //入力不可の解除
                this.txtHojoKamokuCd.Enabled = true;
                this.txtBumonCd.Enabled = true;
                this.txtTaishakuKbn.Enabled = true;

                // 取得した内容を表示
                DataRow drDispData = dtDispData.Rows[0];
                this.lblKanjoKamokuNm.Text = Util.ToString(drDispData["KANJO_KAMOKU_NM"]);
                string hojoCode = this.txtHojoKamokuCd.Text;
                this.txtHojoKamokuCd.Text = "0";
                this.lblHojoKamokuNm.Text = "";
                this.txtBumonCd.Text = "0";
                this.lblBumonNm.Text = "";
                if ("1".Equals(Util.ToString(drDispData["TAISHAKU_KUBUN"])))
                {
                    this.txtZeiKbn.Text = Util.ToString(drDispData["KARIKATA_ZEI_KUBUN"]);
                    this.lblZeiKbnNm.Text = Util.ToString(drDispData["KARIKATA_ZEI_KUBUN_NM"]);
                }
                else
                {
                    this.txtZeiKbn.Text = Util.ToString(drDispData["KASHIKATA_ZEI_KUBUN"]);
                    this.lblZeiKbnNm.Text = Util.ToString(drDispData["KASHIKATA_ZEI_KUBUN_NM"]);
                }
                // 設定区分が1の場合、貸借区分の入力不可
                if (SETTEI_KUBUN.Equals(this.Par3))
                {
                    this.txtTaishakuKbn.Enabled = false;
                }
                // 部門有無が0の場合、部門コードの入力不可
                if (BUMON_UMU.Equals(Util.ToString(drDispData["BUMON_UMU"])))
                {
                    this.txtBumonCd.Enabled = false;
                }
                // 補助有無が0の場合、補助科目コードの入力不可
                if (HOJO_UMU.Equals(Util.ToString(drDispData["HOJO_KAMOKU_UMU"])))
                {
                    this.txtHojoKamokuCd.Enabled = false;
                }

                // 新規モードで使用区分が取引先なら
                if (MODE_NEW.Equals(this.Par1) && Util.ToInt(Util.ToString(drDispData["HOJO_SHIYO_KUBUN"])) >= 2)
                {
                    this.txtHojoKamokuCd.Text = "-1";
                }
                // 変更モードで元が-1なら戻す
                if (MODE_EDIT.Equals(this.Par1) && hojoCode == "-1")
                {
                    this.txtHojoKamokuCd.Text = hojoCode;
                }
            }

            return true;
        }

        /// <summary>
        /// 補助科目コードの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidHojoKamokuCd()
        {
            // 未入力は0とする
            if (ValChk.IsEmpty(this.txtHojoKamokuCd.Text))
            {
                this.txtHojoKamokuCd.Text = "0";
            }

            // 数字か-1のみの入力を許可
            if (!ValChk.IsNumber(this.txtHojoKamokuCd.Text) && !"-1".Equals(this.txtHojoKamokuCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 補助科目名の取得
            StringBuilder cols = new StringBuilder();
            cols.Append("     HOJO_KAMOKU_NM       AS HOJO_KAMOKU_NM");

            StringBuilder from = new StringBuilder();
            from.Append(" VI_ZM_HOJO_KAMOKU");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 6, this.txtKanjoKamokuCd.Text);
            dpc.SetParam("@HOJO_KAMOKU_CD", SqlDbType.Decimal, 10, this.txtHojoKamokuCd.Text);

            DataTable dtDispData =
                this.Dba.GetDataTableByConditionWithParams(
                    Util.ToString(cols), Util.ToString(from),
                    "KAISHA_CD = @KAISHA_CD AND KANJO_KAMOKU_CD = @KANJO_KAMOKU_CD AND HOJO_KAMOKU_CD = @HOJO_KAMOKU_CD",
                    dpc);

            if (dtDispData.Rows.Count == 0)
            {
                // 存在しないコードを入力されたらエラー
                // 但し-1と0は許可
                if ("-1".Equals(this.txtHojoKamokuCd.Text) || "0".Equals(this.txtHojoKamokuCd.Text))
                {
                    this.lblHojoKamokuNm.Text = string.Empty;
                }
                else
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }
            }
            else
            {
            // 取得した内容を表示
            DataRow drDispData = dtDispData.Rows[0];
            this.lblHojoKamokuNm.Text = Util.ToString(drDispData["HOJO_KAMOKU_NM"]);
            }
            return true;
        }

        /// <summary>
        /// 部門コードの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidBumonCd()
        {
            // 未入力は0とする
            if (ValChk.IsEmpty(this.txtBumonCd.Text))
            {
                this.txtBumonCd.Text = "0";
            }

            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtBumonCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 存在しないコードを入力されたらエラー
            // 但し0は許可
            if ("0".Equals(this.txtBumonCd.Text))
            {
                this.lblBumonNm.Text = string.Empty;
            }
            else
            {
                string name = this.Dba.GetName(this.UInfo, "TB_CM_BUMON", "", this.txtBumonCd.Text);
                if (ValChk.IsEmpty(name))
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }

                this.lblBumonNm.Text = name;
            }

            return true;
        }

        /// <summary>
        /// 税区分の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidZeiKbn()
        {
            // 未入力は0とする
            if (ValChk.IsEmpty(this.txtZeiKbn.Text))
            {
                this.txtZeiKbn.Text = "0";
            }

            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtZeiKbn.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 存在しないコードを入力されたらエラー
            string name = this.Dba.GetName(this.UInfo, "TB_ZM_F_ZEI_KUBUN", "", this.txtZeiKbn.Text);
            if (ValChk.IsEmpty(name))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

                this.lblZeiKbnNm.Text = name;

            return true;
        }

        /// <summary>
        /// 事業区分の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidJigyoKbn()
        {
            // 未入力は0とする
            if (ValChk.IsEmpty(this.txtJigyoKbn.Text))
            {
                this.txtJigyoKbn.Text = "0";
            }

            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtJigyoKbn.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 存在しないコードを入力されたらエラー
            // 但し0は許可
            if ("0".Equals(this.txtJigyoKbn.Text))
            {
                this.lblJigyoKbnNm.Text = string.Empty;
            }
            else
            {
                string name = this.Dba.GetName(this.UInfo, "TB_ZM_F_JIGYO_KUBUN", "", this.txtJigyoKbn.Text);
                if (ValChk.IsEmpty(name))
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }

                this.lblJigyoKbnNm.Text = name;
            }

            return true;
        }

        /// <summary>
        /// 貸借区分の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidTaishakuKbn()
        {
            // 未入力は1とする
            if (ValChk.IsEmpty(this.txtTaishakuKbn.Text))
            {
                this.txtTaishakuKbn.Text = "1";
            }

            // 1,2のみ入力を許可
            if (!ValChk.IsNumber(this.txtTaishakuKbn.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            int intval = Util.ToInt(this.txtTaishakuKbn.Text);
            if (intval == 0 || intval > 2)
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 水揚支所の入力チェック
            if (!IsValidMizuageShishoCd())
            {
                this.txtShishoCd.Focus();
                this.txtShishoCd.SelectAll();
                return false;
            }

            if (MODE_NEW.Equals(this.Par1))
            {
                // 仕訳コードのチェック
                if (!IsValidShiwakeCd())
                {
                    this.txtShiwakeCd.Focus();
                    return false;
                }
            }

            // 仕訳名称のチェック
            if (!IsValidShiwakeNm())
            {
                this.txtShiwakeNm.Focus();
                return false;
            }

            // 勘定科目コードのチェック
            if (!IsValidKanjoKamokuCd(true))
            {
                this.txtKanjoKamokuCd.Focus();
                return false;
            }

            // 補助科目コードのチェック
            if (!IsValidHojoKamokuCd())
            {
                this.txtHojoKamokuCd.Focus();
                return false;
            }

            // 部門コードのチェック
            if (!IsValidBumonCd())
            {
                this.txtBumonCd.Focus();
                return false;
            }

            // 税区分のチェック
            if (!IsValidZeiKbn())
            {
                this.txtZeiKbn.Focus();
                return false;
            }

            // 事業区分のチェック
            if (!IsValidJigyoKbn())
            {
                this.txtJigyoKbn.Focus();
                return false;
            }

            // 貸借区分のチェック
            if (!IsValidTaishakuKbn())
            {
                this.txtTaishakuKbn.Focus();
                return false;
            }
            return true;
        }

        /// <summary>
        /// 検索サブウィンドウオープン
        /// </summary>
        /// <param name="moduleName">例："COMC8111"</param>
        /// <param name="para1">例："TB_CM_SHISHO"</param>
        /// <param name="textBoxOfCode">例：this.txtGyogyoushuCd.Text</param>
        /// <param name="indata">例： this.txtGyogyoushuCd.Text</param>
        /// <returns>String[0]：検索結果から選択したコード , String[1]：検索結果から選択した名称</returns>
        private String[] openSearchWindow(String moduleName, String para1, String indata)
        {
            string[] result = { "", "" };

            // ネームスペースに使うモジュール名の小文字
            string lowerModuleName = moduleName.ToLower();

            // ネームスペースの末尾
            string nameSpace = lowerModuleName.Substring(0, 3);

            // アセンブリのロード
            Assembly asm = Assembly.LoadFrom(moduleName + ".exe");

            // フォーム作成
            string moduleNameSpace = "jp.co.fsi." + nameSpace + "." + lowerModuleName + "." + moduleName;
            Type t = asm.GetType(moduleNameSpace);

            if (t != null)
            {
                Object obj = Activator.CreateInstance(t);
                if (obj != null)
                {
                    BasePgForm frm = (BasePgForm)obj;
                    frm.Par1 = para1;
                    frm.InData = indata;
                    frm.ShowDialog(this);

                    if (frm.DialogResult == DialogResult.OK)
                    {
                        string[] ret = (string[])frm.OutData;
                        result[0] = ret[0];
                        result[1] = ret[1];
                        return result;
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// 新規モードの初期表示
        /// </summary>
        private void InitDispOnNew()
        {
            // 初期値、入力制御を実装

            // 使用するテーブルを設定
            string table;
            if (SETTEI_KUBUN.Equals(this.Par3))
            {
                table = "TB_HN_ZIDO_SHIWAKE_SETTEI_A";
            }
            else
            {
                table = "TB_HN_ZIDO_SHIWAKE_SETTEI_B";
            }

            string shishoCD = SHISHO_CD;
            // 仕訳コードの初期値を取得
            // 仕訳の中でのMAX+1を初期表示する
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 2, this.Par2);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCD);
            StringBuilder where = new StringBuilder("KAISHA_CD = @KAISHA_CD");
            where.Append(" AND DENPYO_KUBUN = @DENPYO_KUBUN");
            where.Append(" AND SHISHO_CD = @SHISHO_CD");

            DataTable dtMaxShiwake =
                this.Dba.GetDataTableByConditionWithParams("MAX(SHIWAKE_CD) AS MAX_CD",
                    table, Util.ToString(where), dpc);
            if (dtMaxShiwake.Rows.Count > 0 && !ValChk.IsEmpty(dtMaxShiwake.Rows[0]["MAX_CD"]))
            {
                this.txtShiwakeCd.Text = Util.ToString(Util.ToInt(dtMaxShiwake.Rows[0]["MAX_CD"]) + 1);
            }
            else
            {
                this.txtShiwakeCd.Text = "1";
            }

            // 各項目の初期値を設定
            // 勘定科目コード
            this.txtKanjoKamokuCd.Text = "0";
            // 補助科目コード
            this.txtHojoKamokuCd.Text = "0";
            // 部門コード
            this.txtBumonCd.Text = "0";
            // 税区分
            this.txtZeiKbn.Text = "0";
            // 事業区分
            this.txtJigyoKbn.Text = "3";
            // 貸借区分
            this.txtTaishakuKbn.Text = "1";

            // F3ボタンを非活性にする
            this.btnF3.Enabled = false;

            // 担当者名に初期フォーカス
            this.ActiveControl = this.txtShiwakeNm;
            this.txtShiwakeNm.Focus();
        }

        /// <summary>
        /// 編集モードの初期表示
        /// </summary>
        private void InitDispOnEdit()
        {
            // 使用するテーブルを設定
            string table;
            if (SETTEI_KUBUN.Equals(this.Par3))
            {
                table = "VI_HN_ZIDO_SHIWAKE_SETTEI_A";
            }
            else
            {
                table = "VI_HN_ZIDO_SHIWAKE_SETTEI_B";
            }

            string shishoCD = SHISHO_CD;

            // 現在DBに登録されている値、入力制御を実装
            StringBuilder cols = new StringBuilder();
            cols.Append("     A.KAISHA_CD     AS KAISHA_CD,");
            cols.Append("     A.SHISHO_CD     AS SHISHO_CD,");
            cols.Append("     A.DENPYO_KUBUN       AS DENPYO_KUBUN,");
            cols.Append("     A.SHIWAKE_CD     AS SHIWAKE_CD,");
            cols.Append("     A.SHIWAKE_NM       AS SHIWAKE_NM,");
            cols.Append("     A.KANJO_KAMOKU_CD AS KANJO_KAMOKU_CD,");
            cols.Append("     B.KANJO_KAMOKU_NM     AS KANJO_KAMOKU_NM,");
            cols.Append("     B.BUMON_UMU       AS BUMON_UMU,");
            cols.Append("     B.HOJO_KAMOKU_UMU   AS HOJO_KAMOKU_UMU,");
            cols.Append("     B.HOJO_SHIYO_KUBUN   AS HOJO_SHIYO_KUBUN,");
            cols.Append("     A.HOJO_KAMOKU_CD AS HOJO_KAMOKU_CD,");
            cols.Append("     C.HOJO_KAMOKU_NM     AS HOJO_KAMOKU_NM,");
            cols.Append("     A.BUMON_CD     AS BUMON_CD,");
            cols.Append("     A.BUMON_NM         AS BUMON_NM,");
            cols.Append("     A.ZEI_KUBUN         AS ZEI_KUBUN,");
            cols.Append("     A.ZEI_KUBUN_NM     AS ZEI_KUBUN_NM,");
            cols.Append("     A.JIGYO_KUBUN       AS JIGYO_KUBUN,");
            cols.Append("     A.JIGYO_KUBUN_NM   AS JIGYO_KUBUN_NM,");
            cols.Append("     A.TAISHAKU_KUBUN       AS TAISHAKU_KUBUN,");
            cols.Append("     A.TAISHAKU_KUBUN_NM   AS TAISHAKU_KUBUN_NM");

            StringBuilder from = new StringBuilder();
            from.Append(table + " AS A ");
            from.Append(" LEFT OUTER JOIN  TB_ZM_KANJO_KAMOKU  AS B     ");
            from.Append("     ON  A.KANJO_KAMOKU_CD = B.KANJO_KAMOKU_CD    ");
            from.Append("     AND A.KAISHA_CD       = B.KAISHA_CD ");
            from.Append("     AND B.KAIKEI_NENDO    = @KAIKEI_NENDO ");
            from.Append(" LEFT OUTER JOIN  VI_ZM_HOJO_KAMOKU  AS C ");
            from.Append("     ON  A.KANJO_KAMOKU_CD = C.KANJO_KAMOKU_CD");
            from.Append("     AND A.HOJO_KAMOKU_CD  = C.HOJO_KAMOKU_CD");
            from.Append("     AND A.KAISHA_CD       = C.KAISHA_CD");
            from.Append("     AND A.SHISHO_CD       = C.SHISHO_CD");
            from.Append("     AND C.KAIKEI_NENDO    = @KAIKEI_NENDO ");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 2, this.Par2);
            dpc.SetParam("@SHIWAKE_CD", SqlDbType.Decimal, 13, Util.ToString(this.InData));
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToDecimal(shishoCD));
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);

            DataTable dtDispData =
                this.Dba.GetDataTableByConditionWithParams(
                    Util.ToString(cols), Util.ToString(from),
                    "A.KAISHA_CD = @KAISHA_CD AND A.SHIWAKE_CD = @SHIWAKE_CD AND A.DENPYO_KUBUN = @DENPYO_KUBUN AND A.SHISHO_CD = @SHISHO_CD",
                    dpc);

            if (dtDispData.Rows.Count == 0)
            {
                Msg.Error("不正な起動です。終了します。");
                this.Close();
            }

            // 取得した内容を表示
            DataRow drDispData = dtDispData.Rows[0];
            this.txtShiwakeCd.Text = Util.ToString(drDispData["SHIWAKE_CD"]);
            this.txtShiwakeNm.Text = Util.ToString(drDispData["SHIWAKE_NM"]);
            this.txtKanjoKamokuCd.Text = Util.ToString(drDispData["KANJO_KAMOKU_CD"]);
            this.lblKanjoKamokuNm.Text = Util.ToString(drDispData["KANJO_KAMOKU_NM"]);
            this.txtHojoKamokuCd.Text = Util.ToString(drDispData["HOJO_KAMOKU_CD"]);
            this.lblHojoKamokuNm.Text = Util.ToString(drDispData["HOJO_KAMOKU_NM"]);
            this.txtBumonCd.Text = Util.ToString(drDispData["BUMON_CD"]);
            this.lblBumonNm.Text = Util.ToString(drDispData["BUMON_NM"]);
            this.txtZeiKbn.Text = Util.ToString(drDispData["ZEI_KUBUN"]);
            this.lblZeiKbnNm.Text = Util.ToString(drDispData["ZEI_KUBUN_NM"]);
            this.txtJigyoKbn.Text = Util.ToString(drDispData["JIGYO_KUBUN"]);
            this.lblJigyoKbnNm.Text = Util.ToString(drDispData["JIGYO_KUBUN_NM"]);
            this.txtTaishakuKbn.Text = Util.ToString(drDispData["TAISHAKU_KUBUN"]);
            this.lblTaishakuKbnNm.Text = Util.ToString(drDispData["TAISHAKU_KUBUN_NM"]);

            // 商品コードは入力不可
            this.txtShiwakeCd.Enabled = false;
            // 設定区分が1の場合、貸借区分の入力不可
            if (SETTEI_KUBUN.Equals(this.Par3))
            {
                this.txtTaishakuKbn.Enabled = false;
            }
            // 部門有無が0の場合、部門コードの入力不可
            if (BUMON_UMU.Equals(Util.ToString(drDispData["BUMON_UMU"])))
            {
                this.txtBumonCd.Enabled = false;
            }
            // 補助有無が0の場合、補助科目コードの入力不可
            if (HOJO_UMU.Equals(Util.ToString(drDispData["HOJO_KAMOKU_UMU"])))
            {
                this.txtHojoKamokuCd.Enabled = false;
            }
        }

        /// <summary>
        /// TB_HN_ZIDO_SHIWAKE_SETTEI_Aに更新するためのパラメータ設定をします。
        /// </summary>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// 更新処理：DbParamCollection*2(Where句,Set句)
        /// </returns>
        private ArrayList SetCmShiwakeParams()
        {
            string shishoCd = SHISHO_CD;

            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            if (MODE_NEW.Equals(this.Par1))
            {
                // 会社コードと伝票コードと仕訳コードを更新パラメータに設定
                updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                updParam.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 2, this.Par2);
                updParam.SetParam("@SHIWAKE_CD", SqlDbType.Decimal, 13, this.txtShiwakeCd.Text);
                updParam.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToDecimal( shishoCd));

                // 登録日
                updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWDATE");
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // 会社コードと伝票コードと仕訳コードをWhere句のパラメータに設定
                DbParamCollection whereParam = new DbParamCollection();
                whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                whereParam.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 2, this.Par2);
                whereParam.SetParam("@SHIWAKE_CD", SqlDbType.Decimal, 13, this.txtShiwakeCd.Text);
                whereParam.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToDecimal(shishoCd));

                alParams.Add(whereParam);
            }

            // 仕訳名称
            updParam.SetParam("@SHIWAKE_NM", SqlDbType.VarChar, 30, this.txtShiwakeNm.Text);
            // 勘定科目コード
            updParam.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 6, this.txtKanjoKamokuCd.Text);
            // 補助科目コード
            updParam.SetParam("@HOJO_KAMOKU_CD", SqlDbType.Decimal, 10, this.txtHojoKamokuCd.Text);
            // 部門コード
            updParam.SetParam("@BUMON_CD", SqlDbType.Decimal, 4, this.txtBumonCd.Text);
            // 税区分
            updParam.SetParam("@ZEI_KUBUN", SqlDbType.Decimal, 2, this.txtZeiKbn.Text);
            // 事業区分
            updParam.SetParam("@JIGYO_KUBUN", SqlDbType.Decimal, 1, this.txtJigyoKbn.Text);
            // 貸借区分
            updParam.SetParam("@TAISHAKU_KUBUN", SqlDbType.Decimal, 1, this.txtTaishakuKbn.Text);
            // 更新日
            updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWDATE");

            alParams.Add(updParam);

            return alParams;
        }
        #endregion

        private void gbxJoken_Enter(object sender, EventArgs e)
        {

        }
    }
}
