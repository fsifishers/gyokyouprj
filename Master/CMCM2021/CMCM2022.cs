﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.cm.cmcm2021
{
    /// <summary>
    /// 担当者の登録(変更・追加)(CMCM2022)
    /// </summary>
    public partial class CMCM2022 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// モード(新規)
        /// </summary>
        private const string MODE_NEW = "1";

        /// <summary>
        /// モード(編集)
        /// </summary>
        private const string MODE_EDIT = "2";
        #endregion

        #region プロパティ
        /// <summary>
        /// 画面上最後となるフォーカスのEnterボタン押下時処理用変数
        /// </summary>
        private bool _dtFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._dtFlg;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public CMCM2022()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="par1">引数1</param>
        public CMCM2022(string par1) : base(par1)
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // タイトルは非表示
            this.lblTitle.Visible = false;

            // 引数：Par1／モード(1:新規、2:変更)、InData：担当者コード
            if (MODE_NEW.Equals(this.Par1))
            {
                // 新規モードの初期表示
                InitDispOnNew();
                this.btnF3.Enabled = false;
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // 編集モードの初期表示
                InitDispOnEdit();
            }
            else
            {
                // 不正な起動として閉じる
                Msg.Error("不正な起動です。終了します。");
                this.Close();
            }

            // ボタンの表示非表示を設定
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF6.Location = this.btnF3.Location;
            this.btnF3.Location = this.btnF2.Location;
            this.btnF2.Visible = false;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;

            // 支所
            this.txtShishoCd.Text = this.Par2;
            this.txtShishoCd.Enabled = (this.txtShishoCd.Text == "1") ? true : false;
            this.lblShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtShishoCd.Text, this.txtShishoCd.Text);

            // Enter処理を無効化
            this._dtFlg = false;

            // 給与は固定で０にする
            txtKyuyo.Text = "0";

        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 支所,部門にフォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtShishoCd":
                case "txtBumon":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;
            String[] result;

            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd": // 支所
                    // アセンブリのロード
                    asm = Assembly.LoadFrom("CMCM2031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2031.CMCM2031");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.InData = this.txtShishoCd.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.txtShishoCd.Text = result[0];
                                this.lblShishoNm.Text = result[1];
                            }
                        }
                    }
                    break;
                case "txtBumon":
                    // アセンブリのロード
                    //asm = Assembly.LoadFrom("CMCM2041.exe");
                    asm = Assembly.LoadFrom("CMCM1041.exe");
                    // フォーム作成
                    //t = asm.GetType("jp.co.fsi.cm.cmcm2041.CMCM2041");
                    t = asm.GetType("jp.co.fsi.cm.cmcm1041.CMCM1041");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            //frm.Par1 = "1";
                            frm.Par1 = "TB_CM_BUMON";
                            frm.InData = this.txtBumon.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.txtBumon.Text = result[0];
                                this.lblBumonNm.Text = result[1];
                            }
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// F3キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF3();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF3()
        {
            if (MODE_NEW.Equals(Par1))
            {
                return;
            }

            if (Msg.ConfYesNo("削除しますか？") == DialogResult.No)
            {
                return;
            }

            try
            {
                // トランザクションの開始
                this.Dba.BeginTransaction();

                StringBuilder Sql;
                DbParamCollection dpc;
                dpc = new DbParamCollection();
                Sql = new StringBuilder();
                Sql.Append(" DELETE ");
                Sql.Append(" FROM ");
                Sql.Append("     TB_CM_TANTOSHA ");
                Sql.Append(" WHERE ");
                Sql.Append("     KAISHA_CD = @KAISHA_CD AND ");
                Sql.Append("     TANTOSHA_CD = @TANTOSHA_CD");
                Sql.Append("     AND SHISHO_CD = @SHISHO_CD ");

                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                dpc.SetParam("@TANTOSHA_CD", SqlDbType.Decimal, 6, Util.ToDecimal(txtTantoshaCd.Text));
                dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.txtShishoCd.Text);

                this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                // トランザクションをコミット
                this.Dba.Commit();

                Msg.Info("削除しました。");
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF6();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF6()
        {
            // 確認メッセージを表示
            string msg = (MODE_NEW.Equals(this.Par1) ? "登録" : "更新") + "しますか？";
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }


            // 既に存在するユーザID・パスワードを入力した場合はエラーとする
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@TANTOSHA_CD", SqlDbType.Decimal, 4, Util.ToDecimal(this.txtTantoshaCd.Text));
            dpc.SetParam("@USER_ID", SqlDbType.VarChar, 30, this.txtUserId.Text);
            dpc.SetParam("@PASSWORD", SqlDbType.VarChar, 30, this.txtPassWord.Text);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            StringBuilder where = new StringBuilder("KAISHA_CD = @KAISHA_CD");
            where.Append(" AND TANTOSHA_CD <> @TANTOSHA_CD AND USER_ID = @USER_ID AND PASSWORD = @PASSWORD");
            where.Append(" AND SHISHO_CD = @SHISHO_CD ");
            DataTable dtTanto =
                this.Dba.GetDataTableByConditionWithParams("TANTOSHA_CD",
                    "TB_CM_TANTOSHA", Util.ToString(where), dpc);
            if (dtTanto.Rows.Count > 0)
            {
                Msg.Info("既に使用されているパスワードのため登録できません。");
                this.txtUserId.Focus();
                return ;
            }

            // 入力値をバインドパラメータとしてセットする
            ArrayList alParamsCmTanto = SetCmTantoParams();

            try
            {
                // トランザクションの開始
                this.Dba.BeginTransaction();

                if (MODE_NEW.Equals(this.Par1))
                {
                    // データ登録
                    // 共通.担当者マスタ
                    this.Dba.Insert("TB_CM_TANTOSHA", (DbParamCollection)alParamsCmTanto[0]);
                }
                else if (MODE_EDIT.Equals(this.Par1))
                {
                    // データ更新
                    // 共通.担当者マスタ
                    this.Dba.Update("TB_CM_TANTOSHA",
                        (DbParamCollection)alParamsCmTanto[1],
                        "KAISHA_CD = @KAISHA_CD AND TANTOSHA_CD = @TANTOSHA_CD AND SHISHO_CD = @SHISHO_CD",
                        (DbParamCollection)alParamsCmTanto[0]);

                    if (this.txtUserKubun.Text.Equals("0"))
                    {
                        // 管理者権限もちのユーザがが0人になるの場合変更できないようにする。
                        dpc = new DbParamCollection();
                        dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                        dpc.SetParam("@USER_KUBUN", SqlDbType.Decimal, 4, 1);
                        where = new StringBuilder("KAISHA_CD = @KAISHA_CD");
                        where.Append(" AND USER_KUBUN = @USER_KUBUN");
                        DataTable dtTanto1 =
                            this.Dba.GetDataTableByConditionWithParams("TANTOSHA_CD",
                                "TB_CM_TANTOSHA", Util.ToString(where), dpc);

                        if (dtTanto1.Rows.Count == 0)
                        {
                            Msg.Error("管理者権限のユーザが消えてしまいますので変更できません。");
                            this.txtUserKubun.Text = "1";
                            return;
                        }
                    }

                }

                // トランザクションをコミット
                this.Dba.Commit();

                Msg.Info("登録しました。");
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 支所入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMizuageShishoCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsValidShishoCd(this.txtShishoCd.Text, this.lblShishoNm.Text, this.txtShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                e.Cancel = true;
                this.txtShishoCd.SelectAll();
                this.txtShishoCd.Focus();
            }
        }

        /// <summary>
        /// 担当者コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTantoshaCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidTantoshaCd())
            {
                e.Cancel = true;
                this.txtTantoshaCd.SelectAll();
            }
        }

        /// <summary>
        /// 担当者名の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTantoshaNm_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidTantoshaNm())
            {
                e.Cancel = true;
                this.txtTantoshaNm.SelectAll();
            }
        }

        /// <summary>
        /// 担当者カナ名の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTantoshaKanaNm_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidTantoshaKanaNm())
            {
                e.Cancel = true;
                this.txtTantoshaKanaNm.SelectAll();
            }
        }

        /// <summary>
        /// 部門の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtBumon_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidBumon())
            {
                e.Cancel = true;
                this.txtBumon.SelectAll();

            }
        }
        /// <summary>
        /// ユーザIDの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtUserId_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidUserId())
            {
                e.Cancel = true;
                this.txtUserId.SelectAll();
            }
        }
        /// <summary>
        /// パスワードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtPassword_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidPassword())
            {
                e.Cancel = true;
                this.txtPassWord.SelectAll();
            }
        }
        /// <summary>
        /// 権限の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtUserKubun_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidUserKubun())
            {
                e.Cancel = true;
                this.txtUserKubun.SelectAll();
            }
        }
        /// <summary>
        /// セリ販売の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSeri_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidSeri())
            {
                e.Cancel = true;
                this.txtSeri.SelectAll();
            }
        }
        /// <summary>
        /// 購買の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKobai_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidKobai())
            {
                e.Cancel = true;
                this.txtKobai.SelectAll();
            }
        }
        /// <summary>
        /// 食堂の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShokudo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShokudo())
            {
                e.Cancel = true;
                this.txtShokudo.SelectAll();
            }
        }
        /// <summary>
        /// 財務の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtZaimu_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidZaimu())
            {
                e.Cancel = true;
                this.txtZaimu.SelectAll();
            }
        }
        /// <summary>
        /// 給与の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKyuyo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidKyuyo())
            {
                e.Cancel = true;
                this.txtKyuyo.SelectAll();

                // Enter処理を無効化
                this._dtFlg = false;
            }
            else
            {
                // Enter処理を有効化
                this._dtFlg = true;
            }
        }

        /// <summary>
        /// 給与のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKyuyo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.Flg)
            {
                // Enter処理を無効化
                this._dtFlg = false;

                PressF6();
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 支所の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool IsValidMizuageShishoCd()
        {
            // 空 又は 0入力の場合
            if (ValChk.IsEmpty(this.txtShishoCd.Text) || Equals(this.txtShishoCd.Text, "0"))
            {
                // 水揚支所名称を表示する
                this.txtShishoCd.Text = "0";
                this.lblShishoNm.Text = "全て";
                return true;
            }

            // 水揚支所名称を表示する
            this.lblShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtShishoCd.Text, this.txtShishoCd.Text);

            if (ValChk.IsEmpty(this.lblShishoNm.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            return true;
        }
        
        /// <summary>
        /// 担当者コードの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidTantoshaCd()
        {
            // 未入力はエラーとする
            if (ValChk.IsEmpty(this.txtTantoshaCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtTantoshaCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 既に存在するコードを入力した場合はエラーとする
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@TANTOSHA_CD", SqlDbType.Decimal, 4, this.txtTantoshaCd.Text);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.txtShishoCd.Text);
            StringBuilder where = new StringBuilder("KAISHA_CD = @KAISHA_CD");
            where.Append(" AND TANTOSHA_CD = @TANTOSHA_CD");
            where.Append(" AND SHISHO_CD = @SHISHO_CD");
            DataTable dtTanto =
                this.Dba.GetDataTableByConditionWithParams("TANTOSHA_CD",
                    "TB_CM_TANTOSHA", Util.ToString(where), dpc);
            if (dtTanto.Rows.Count > 0)
            {
                Msg.Error("既に存在する担当者コードと重複しています。");
                return false;
            }
            return true;
        }

        /// <summary>
        /// 担当者コードの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidTantoshaCd(string Code)
        {
            // 伝票などで使用されているかチェック
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToDecimal(this.txtShishoCd.Text));
            dpc.SetParam("@TANTOSHA_CD", SqlDbType.Decimal, 4, Util.ToDecimal( Code));

            StringBuilder where = new StringBuilder("KAISHA_CD = @KAISHA_CD");
            where.Append(" AND TANTOSHA_CD = @TANTOSHA_CD");
            where.Append(" AND SHISHO_CD = @SHISHO_CD");
            DataTable dtTanto =
                this.Dba.GetDataTableByConditionWithParams("TANTOSHA_CD",
                    "TB_HN_TORIHIKI_DENPYO", Util.ToString(where), dpc);
            if (dtTanto.Rows.Count > 0)
            {
                //Msg.Error("既に存在する担当者コードと重複しています。");
                return false;
            }

            where = new StringBuilder("KAISHA_CD = @KAISHA_CD");
            where.Append(" AND TANTOSHA_CD = @TANTOSHA_CD");
            where.Append(" AND SHISHO_CD = @SHISHO_CD");
            dtTanto =
                this.Dba.GetDataTableByConditionWithParams("TANTOSHA_CD",
                    "TB_ZM_SHIWAKE_DENPYO", Util.ToString(where), dpc);
            if (dtTanto.Rows.Count > 0)
            {
                //Msg.Error("既に存在する担当者コードと重複しています。");
                return false;
            }
            return true;
        }

        /// <summary>
        /// 担当者名の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidTantoshaNm()
        {
            // 40バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtTantoshaNm.Text, this.txtTantoshaNm.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 担当者カナ名の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidTantoshaKanaNm()
        {
            // 30バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtTantoshaKanaNm.Text, this.txtTantoshaKanaNm.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 部門の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidBumon()
        {
            // 未入力は0とする
            if (ValChk.IsEmpty(this.txtBumon.Text))
            {
                this.txtBumon.Text = "0";
            }

            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtBumon.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 存在しないコードを入力されたらエラー
            // 但し0は許可
            if ("0".Equals(this.txtBumon.Text))
            {
                this.lblBumonNm.Text = string.Empty;
            }
            else
            {
                string bumon = this.Dba.GetName(this.UInfo, "TB_CM_BUMON", "", this.txtBumon.Text);
                if (ValChk.IsEmpty(bumon))
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }

                this.lblBumonNm.Text = bumon;
            }

            return true;
        }

        /// <summary>
        /// ユーザIDの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidUserId()
        {

            // 未入力はエラーとする
            if (ValChk.IsEmpty(this.txtUserId.Text))
            {
                Msg.Error("ユーザIDを入力してください");
                return false;
            }

            // 30バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtUserId.Text, this.txtUserId.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            return true;
        }

        /// <summary>
        /// パスワードの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidPassword()
        {
            // 未入力はエラーとする
            if (ValChk.IsEmpty(this.txtPassWord.Text))
            {
                Msg.Error("パスワードを入力してください");
                return false;
            }

            // 30バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtPassWord.Text, this.txtPassWord.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            return true;
        }

        /// <summary>
        /// 権限の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidUserKubun()
        {
            // 未入力は0とする
            if (ValChk.IsEmpty(this.txtUserKubun.Text))
            {
                this.txtUserKubun.Text = "0";
            }

            // 0,1のみ入力許可
            if (!ValChk.IsNumber(this.txtUserKubun.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            int intval = Util.ToInt(this.txtUserKubun.Text);
            if (intval < 0 || intval > 1)
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// セリ販売の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidSeri()
        {
            // 未入力は0とする
            if (ValChk.IsEmpty(this.txtSeri.Text))
            {
                this.txtUserKubun.Text = "0";
            }

            // 0,1のみ入力許可
            if (!ValChk.IsNumber(this.txtSeri.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            int intval = Util.ToInt(this.txtSeri.Text);
            if (intval < 0 || intval > 1)
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 購買の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidKobai()
        {
            // 未入力は0とする
            if (ValChk.IsEmpty(this.txtKobai.Text))
            {
                this.txtUserKubun.Text = "0";
            }

            // 0,1のみ入力許可
            if (!ValChk.IsNumber(this.txtKobai.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            int intval = Util.ToInt(this.txtKobai.Text);
            if (intval < 0 || intval > 1)
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 食堂の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShokudo()
        {
            // 未入力は0とする
            if (ValChk.IsEmpty(this.txtShokudo.Text))
            {
                this.txtUserKubun.Text = "0";
            }

            // 0,1のみ入力許可
            if (!ValChk.IsNumber(this.txtShokudo.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            int intval = Util.ToInt(this.txtShokudo.Text);
            if (intval < 0 || intval > 1)
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 財務の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidZaimu()
        {
            // 未入力は0とする
            if (ValChk.IsEmpty(this.txtZaimu.Text))
            {
                this.txtUserKubun.Text = "0";
            }

            // 0,1のみ入力許可
            if (!ValChk.IsNumber(this.txtZaimu.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            int intval = Util.ToInt(this.txtZaimu.Text);
            if (intval < 0 || intval > 1)
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 給与の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidKyuyo()
        {
            // 未入力は0とする
            if (ValChk.IsEmpty(this.txtKyuyo.Text))
            {
                this.txtUserKubun.Text = "0";
            }

            // 0,1のみ入力許可
            if (!ValChk.IsNumber(this.txtKyuyo.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            int intval = Util.ToInt(this.txtKyuyo.Text);
            if (intval < 0 || intval > 1)
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 支所の入力チェック
            if (!IsValidMizuageShishoCd())
            {
                this.txtShishoCd.Focus();
                this.txtShishoCd.SelectAll();
                return false;
            }

            if (MODE_NEW.Equals(this.Par1))
            {
                // 担当者コードのチェック
                if (!IsValidTantoshaCd())
                {
                    this.txtTantoshaCd.Focus();
                    return false;
                }
            }

            // 担当者名のチェック
            if (!IsValidTantoshaNm())
            {
                this.txtTantoshaNm.Focus();
                return false;
            }

            // 担当者カナ名のチェック
            if (!IsValidTantoshaKanaNm())
            {
                this.txtTantoshaKanaNm.Focus();
                return false;
            }

            // 部門コードのチェック
            if (!IsValidBumon())
            {
                this.txtBumon.Focus();
                return false;
            }
            
            // ユーザIDのチェック
            if (!IsValidUserId())
            {
                this.txtUserId.Focus();
                return false;
            }

            // パスワードのチェック
            if (!IsValidPassword())
            {
                this.txtPassWord.Focus();
                return false;
            }

            // 権限のチェック
            if (!IsValidUserKubun())
            {
                this.txtUserKubun.Focus();
                return false;
            }

            // セリ販売のチェック
            if (!IsValidSeri())
            {
                this.txtSeri.Focus();
                return false;
            }

            // 購買のチェック
            if (!IsValidKobai())
            {
                this.txtKobai.Focus();
                return false;
            }

            // 食堂のチェック
            if (!IsValidShokudo())
            {
                this.txtShokudo.Focus();
                return false;
            }

            // 財務のチェック
            if (!IsValidZaimu())
            {
                this.txtZaimu.Focus();
                return false;
            }

            // 給与のチェック
            if (!IsValidKyuyo())
            {
                this.txtKyuyo.Focus();
                return false;
            }
            return true;
        }

        /// <summary>
        /// 新規モードの初期表示
        /// </summary>
        private void InitDispOnNew()
        {
            // 初期値、入力制御を実装

            // 担当者コードの初期値を取得
            // 担当者の中でのMAX+1を初期表示する
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.Par2);
            StringBuilder where = new StringBuilder("KAISHA_CD = @KAISHA_CD");
            where.Append(" AND SHISHO_CD = @SHISHO_CD");
            DataTable dtMaxTanto =
                this.Dba.GetDataTableByConditionWithParams("MAX(TANTOSHA_CD) AS MAX_CD",
                    "TB_CM_TANTOSHA", Util.ToString(where), dpc);
            if (dtMaxTanto.Rows.Count > 0 && !ValChk.IsEmpty(dtMaxTanto.Rows[0]["MAX_CD"]))
            {
                this.txtTantoshaCd.Text = Util.ToString(Util.ToInt(dtMaxTanto.Rows[0]["MAX_CD"]) + 1);
            }
            else
            {
                this.txtTantoshaCd.Text = "1";
            }

            // 部門の初期値を設定
            this.txtBumon.Text = "0";
            this.txtUserKubun.Text = "0";
            this.txtSeri.Text = "1";
            this.txtKobai.Text = "1";
            this.txtShokudo.Text = "1";
            this.txtZaimu.Text = "1";
            this.txtKyuyo.Text = "1";

            DataRow roginUser = GetroginUserdetail();//ログインしている人のユーザ情報取得

            if (Util.ToInt(roginUser["USER_KUBUN"]).Equals(1))// 管理者権限のみ詳細を表示
            {
                this.shosai.Visible = true;
            }
            else
            {
                this.shosai.Visible = false;
            }

            // 担当者名に初期フォーカス
            this.ActiveControl = this.txtTantoshaNm;
            this.txtTantoshaNm.Focus();
        }

        /// <summary>
        /// 編集モードの初期表示
        /// </summary>
        private void InitDispOnEdit()
        {
            // 現在DBに登録されている値、入力制御を実装
            StringBuilder cols = new StringBuilder();
            cols.Append(" KAISHA_CD");
            cols.Append(" ,TANTOSHA_CD");
            cols.Append(" ,TANTOSHA_NM");
            cols.Append(" ,TANTOSHA_KANA_NM");
            cols.Append(" ,BUMON_CD");
            cols.Append(" ,USER_ID");
            cols.Append(" ,USER_KUBUN");
            cols.Append(" ,PASSWORD");
            cols.Append(" ,SERI_FLG");
            cols.Append(" ,KOBAI_FLG");
            cols.Append(" ,SHOKUDO_FLG");
            cols.Append(" ,ZAIMU_FLG");
            cols.Append(" ,KYUYO_FLG");

            StringBuilder from = new StringBuilder();
            from.Append("TB_CM_TANTOSHA");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@TANTOSHA_CD", SqlDbType.Decimal, 4, Util.ToString(this.InData));

            DataTable dtDispData =
                this.Dba.GetDataTableByConditionWithParams(
                    Util.ToString(cols), Util.ToString(from),
                    "KAISHA_CD = @KAISHA_CD AND TANTOSHA_CD = @TANTOSHA_CD",
                    dpc);

            if (dtDispData.Rows.Count == 0)
            {
                Msg.Error("不正な起動です。終了します。");
                this.Close();
            }

            // 取得した内容を表示
            DataRow drDispData = dtDispData.Rows[0];
            this.txtTantoshaCd.Text = Util.ToString(drDispData["TANTOSHA_CD"]);
            this.txtTantoshaNm.Text = Util.ToString(drDispData["TANTOSHA_NM"]);
            this.txtTantoshaKanaNm.Text = Util.ToString(drDispData["TANTOSHA_KANA_NM"]);
            this.txtBumon.Text = Util.ToString(drDispData["BUMON_CD"]);
            this.lblBumonNm.Text =
                this.Dba.GetName(this.UInfo, "TB_CM_BUMON", "", this.txtBumon.Text);
            this.txtUserId.Text = Util.ToString(drDispData["USER_ID"]);
            this.txtPassWord.Text = Util.ToString(drDispData["PASSWORD"]);
            this.txtUserKubun.Text = Util.ToString(drDispData["USER_KUBUN"]);
            this.txtSeri.Text = Util.ToString(drDispData["SERI_FLG"]);
            this.txtKobai.Text = Util.ToString(drDispData["KOBAI_FLG"]);
            this.txtShokudo.Text = Util.ToString(drDispData["SHOKUDO_FLG"]);
            this.txtZaimu.Text = Util.ToString(drDispData["ZAIMU_FLG"]);
            this.txtKyuyo.Text = Util.ToString(drDispData["KYUYO_FLG"]);

            // 今のログインユーザのみログインユーザとパスワードを表示
            if (this.UInfo.UserId == Util.ToString(drDispData["USER_ID"]))
            {
                this.txtUserId.Visible = true;
                this.txtPassWord.Visible = true;
                this.lblpassword.Visible = true;
                this.lblUserId.Visible = true;
            }
            else
            {
                this.txtUserId.Visible = false;
                this.txtPassWord.Visible = false;
                this.lblpassword.Visible = false;
                this.lblUserId.Visible = false;
            }

            DataRow roginUser = GetroginUserdetail();//ログインしている人のユーザ情報取得

            if (Util.ToInt(roginUser["USER_KUBUN"]).Equals(1))// 管理者権限のみ詳細を表示
            {
                this.shosai.Visible = true;
                this.txtUserId.Visible = true;
                this.txtPassWord.Visible = true;
                this.lblpassword.Visible = true;
                this.lblUserId.Visible = true;
            }
            else
            {
                this.shosai.Visible = false;
            }

            // 担当者コードは入力不可
            this.txtTantoshaCd.Enabled = false;

            //
            this.btnF3.Enabled = IsValidTantoshaCd(this.txtTantoshaCd.Text);
        }

        /// <summary>
        /// 今ログインしている人のデータを取得
        /// </summary>
        /// <returns>roginUser</returns>
        private DataRow GetroginUserdetail()
        {
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@TANTOSHA_CD", SqlDbType.Decimal, 4, this.UInfo.UserCd);
            //dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.Par2);
            StringBuilder sql = new StringBuilder();
            sql.Append(" SELECT ");
            sql.Append(" * ");
            sql.Append(" FROM ");
            sql.Append(" TB_CM_TANTOSHA ");
            sql.Append(" WHERE ");
            sql.Append(" KAISHA_CD = @KAISHA_CD AND ");
            sql.Append(" TANTOSHA_CD = @TANTOSHA_CD ");
            //sql.Append(" AND SHISHO_CD = @SHISHO_CD ");

            DataTable dt = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);

            if (dt.Rows.Count == 0)
            {
                Msg.Info("ユーザ情報取得に失敗しました。");
            }
            DataRow dr = dt.Rows[0];
            return dr;
        }

        /// <summary>
        /// TB_CM_TANTOSHAに更新するためのパラメータ設定をします。
        /// </summary>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// 更新処理：DbParamCollection*2(Where句,Set句)
        /// </returns>
        private ArrayList SetCmTantoParams()
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            if (MODE_NEW.Equals(this.Par1))
            {
                // 会社コードと担当者コードを更新パラメータに設定
                updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                updParam.SetParam("@TANTOSHA_CD", SqlDbType.Decimal, 4, this.txtTantoshaCd.Text);
                updParam.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.txtShishoCd.Text);
                // 登録日
                updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWDATE");
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // 会社コードと担当者コードをWhere句のパラメータに設定
                DbParamCollection whereParam = new DbParamCollection();
                whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                whereParam.SetParam("@TANTOSHA_CD", SqlDbType.Decimal, 4, this.txtTantoshaCd.Text);
                whereParam.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.txtShishoCd.Text);
                alParams.Add(whereParam);
            }

            // 担当者名
            updParam.SetParam("@TANTOSHA_NM", SqlDbType.VarChar, 40, this.txtTantoshaNm.Text);
            // 担当者カナ名
            updParam.SetParam("@TANTOSHA_KANA_NM", SqlDbType.VarChar, 30, this.txtTantoshaKanaNm.Text);
            // 部門コード
            updParam.SetParam("@BUMON_CD", SqlDbType.Decimal, 4, this.txtBumon.Text);
            // 更新日
            updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWDATE");
            // ユーザID
            updParam.SetParam("@USER_ID", SqlDbType.VarChar, 30, this.txtUserId.Text);
            // パスワード
            updParam.SetParam("@PASSWORD", SqlDbType.VarChar, 30, this.txtPassWord.Text);
            // ユーザ区分
            updParam.SetParam("@USER_KUBUN", SqlDbType.VarChar, 30, this.txtUserKubun.Text);
            // セリ販売表示フラグ
            updParam.SetParam("@SERI_FLG", SqlDbType.VarChar, 30, this.txtSeri.Text);
            // 購買表示フラグ
            updParam.SetParam("@KOBAI_FLG", SqlDbType.VarChar, 30, this.txtKobai.Text);
            // 食堂表示フラグ
            updParam.SetParam("@SHOKUDO_FLG", SqlDbType.VarChar, 30, this.txtShokudo.Text);
            // 財務表示フラグ
            updParam.SetParam("@ZAIMU_FLG", SqlDbType.VarChar, 30, this.txtZaimu.Text);
            // 給与表示フラグ
            updParam.SetParam("@KYUYO_FLG", SqlDbType.VarChar, 30, this.txtKyuyo.Text);

            alParams.Add(updParam);

            return alParams;
        }
        #endregion
    }
}
