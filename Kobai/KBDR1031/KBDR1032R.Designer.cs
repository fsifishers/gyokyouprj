﻿namespace jp.co.fsi.kb.kbdr1031
{
    /// <summary>
    /// KOBR2032R の概要の説明です。
    /// </summary>
    partial class KBDR1032R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(KBDR1032R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.直線68 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.テキスト188 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGrpPages = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル71 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.テキスト194 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル180 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル181 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル182 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル183 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル184 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル185 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル186 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル187 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.テキスト189 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト190 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト255 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト256 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト195 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.reportInfo1 = new GrapeCity.ActiveReports.SectionReportModel.ReportInfo();
            this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.judgeNo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.テキスト32 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト33 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト35 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト36 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト37 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト38 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト39 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト49 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.detailLine = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線18 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線21 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線22 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線23 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線24 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線25 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線26 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線27 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線106 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.テキスト193 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lastLine = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.groupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.groupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト188)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGrpPages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル71)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト194)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル180)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル181)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル182)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル183)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル184)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル185)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル186)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル187)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト189)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト190)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト255)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト256)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト195)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportInfo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.judgeNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト193)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.直線68,
            this.テキスト188,
            this.txtGrpPages,
            this.ラベル71,
            this.テキスト194,
            this.ラベル180,
            this.ラベル181,
            this.ラベル182,
            this.ラベル183,
            this.ラベル184,
            this.ラベル185,
            this.ラベル186,
            this.ラベル187,
            this.テキスト189,
            this.テキスト190,
            this.テキスト255,
            this.テキスト256,
            this.テキスト195,
            this.reportInfo1,
            this.label1,
            this.label2});
            this.pageHeader.Height = 1.158547F;
            this.pageHeader.Name = "pageHeader";
            // 
            // 直線68
            // 
            this.直線68.Height = 0F;
            this.直線68.Left = 2.643311F;
            this.直線68.LineWeight = 1F;
            this.直線68.Name = "直線68";
            this.直線68.Tag = "";
            this.直線68.Top = 0.3126641F;
            this.直線68.Width = 2.473616F;
            this.直線68.X1 = 2.643311F;
            this.直線68.X2 = 5.116926F;
            this.直線68.Y1 = 0.3126641F;
            this.直線68.Y2 = 0.3126641F;
            // 
            // テキスト188
            // 
            this.テキスト188.DataField = "ITEM07";
            this.テキスト188.Height = 0.1770833F;
            this.テキスト188.Left = 2.331103F;
            this.テキスト188.Name = "テキスト188";
            this.テキスト188.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-weight: normal; text-alig" +
    "n: center; ddo-char-set: 1";
            this.テキスト188.Tag = "";
            this.テキスト188.Text = "ITEM07";
            this.テキスト188.Top = 0.3543307F;
            this.テキスト188.Width = 3.098032F;
            // 
            // txtGrpPages
            // 
            this.txtGrpPages.Height = 0.1926126F;
            this.txtGrpPages.Left = 7.151836F;
            this.txtGrpPages.Name = "txtGrpPages";
            this.txtGrpPages.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 1";
            this.txtGrpPages.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtGrpPages.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.txtGrpPages.Tag = "";
            this.txtGrpPages.Text = null;
            this.txtGrpPages.Top = 0.005025204F;
            this.txtGrpPages.Width = 0.375F;
            // 
            // ラベル71
            // 
            this.ラベル71.Height = 0.1974737F;
            this.ラベル71.HyperLink = null;
            this.ラベル71.Left = 7.532392F;
            this.ラベル71.Name = "ラベル71";
            this.ラベル71.Style = "color: #4C535C; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 1";
            this.ラベル71.Tag = "";
            this.ラベル71.Text = "頁";
            this.ラベル71.Top = 0.0001640916F;
            this.ラベル71.Width = 0.2027778F;
            // 
            // テキスト194
            // 
            this.テキスト194.DataField = "ITEM08";
            this.テキスト194.Height = 0.28125F;
            this.テキスト194.Left = 2.697827F;
            this.テキスト194.Name = "テキスト194";
            this.テキスト194.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 18pt; font-weight: bold; text-align:" +
    " center; ddo-char-set: 128";
            this.テキスト194.Tag = "";
            this.テキスト194.Text = "ITEM08";
            this.テキスト194.Top = 0.0001640916F;
            this.テキスト194.Width = 2.364583F;
            // 
            // ラベル180
            // 
            this.ラベル180.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル180.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル180.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル180.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル180.Height = 0.1972441F;
            this.ラベル180.HyperLink = null;
            this.ラベル180.Left = 0F;
            this.ラベル180.Name = "ラベル180";
            this.ラベル180.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; f" +
    "ont-weight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.ラベル180.Tag = "";
            this.ラベル180.Text = "伝票日付";
            this.ラベル180.Top = 0.96063F;
            this.ラベル180.Width = 0.7917323F;
            // 
            // ラベル181
            // 
            this.ラベル181.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル181.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル181.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル181.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル181.Height = 0.1972441F;
            this.ラベル181.HyperLink = null;
            this.ラベル181.Left = 0.7916667F;
            this.ラベル181.Name = "ラベル181";
            this.ラベル181.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; f" +
    "ont-weight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.ラベル181.Tag = "";
            this.ラベル181.Text = "伝票No.";
            this.ラベル181.Top = 0.96063F;
            this.ラベル181.Width = 0.5417323F;
            // 
            // ラベル182
            // 
            this.ラベル182.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル182.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル182.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル182.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル182.Height = 0.1972441F;
            this.ラベル182.HyperLink = null;
            this.ラベル182.Left = 1.309028F;
            this.ラベル182.Name = "ラベル182";
            this.ラベル182.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; f" +
    "ont-weight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.ラベル182.Tag = "";
            this.ラベル182.Text = "取引区分";
            this.ラベル182.Top = 0.96063F;
            this.ラベル182.Width = 0.6807359F;
            // 
            // ラベル183
            // 
            this.ラベル183.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル183.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル183.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル183.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル183.Height = 0.1971298F;
            this.ラベル183.HyperLink = null;
            this.ラベル183.Left = 1.989764F;
            this.ラベル183.Name = "ラベル183";
            this.ラベル183.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; f" +
    "ont-weight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.ラベル183.Tag = "";
            this.ラベル183.Text = "摘　　　要";
            this.ラベル183.Top = 0.96063F;
            this.ラベル183.Width = 2.208333F;
            // 
            // ラベル184
            // 
            this.ラベル184.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル184.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル184.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル184.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル184.Height = 0.1972441F;
            this.ラベル184.HyperLink = null;
            this.ラベル184.Left = 5.099306F;
            this.ラベル184.Name = "ラベル184";
            this.ラベル184.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; f" +
    "ont-weight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.ラベル184.Tag = "";
            this.ラベル184.Text = "消費税額";
            this.ラベル184.Top = 0.96063F;
            this.ラベル184.Width = 0.8228346F;
            // 
            // ラベル185
            // 
            this.ラベル185.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル185.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル185.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル185.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル185.Height = 0.1972441F;
            this.ラベル185.HyperLink = null;
            this.ラベル185.Left = 5.916667F;
            this.ラベル185.Name = "ラベル185";
            this.ラベル185.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; f" +
    "ont-weight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.ラベル185.Tag = "";
            this.ラベル185.Text = "入　金　額";
            this.ラベル185.Top = 0.96063F;
            this.ラベル185.Width = 0.8232284F;
            // 
            // ラベル186
            // 
            this.ラベル186.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル186.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル186.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル186.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル186.Height = 0.1972441F;
            this.ラベル186.HyperLink = null;
            this.ラベル186.Left = 6.739764F;
            this.ラベル186.Name = "ラベル186";
            this.ラベル186.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; f" +
    "ont-weight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.ラベル186.Tag = "";
            this.ラベル186.Text = "残　高";
            this.ラベル186.Top = 0.96063F;
            this.ラベル186.Width = 0.9893701F;
            // 
            // ラベル187
            // 
            this.ラベル187.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル187.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル187.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル187.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル187.Height = 0.1972441F;
            this.ラベル187.HyperLink = null;
            this.ラベル187.Left = 4.197917F;
            this.ラベル187.Name = "ラベル187";
            this.ラベル187.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; f" +
    "ont-weight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.ラベル187.Tag = "";
            this.ラベル187.Text = "売上金額";
            this.ラベル187.Top = 0.96063F;
            this.ラベル187.Width = 0.9165355F;
            // 
            // テキスト189
            // 
            this.テキスト189.DataField = "ITEM01";
            this.テキスト189.Height = 0.2006944F;
            this.テキスト189.Left = 0F;
            this.テキスト189.Name = "テキスト189";
            this.テキスト189.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.テキスト189.Tag = "";
            this.テキスト189.Text = "ITEM01";
            this.テキスト189.Top = 0.4889002F;
            this.テキスト189.Width = 0.6583334F;
            // 
            // テキスト190
            // 
            this.テキスト190.DataField = "ITEM02";
            this.テキスト190.Height = 0.2006944F;
            this.テキスト190.Left = 0.7083333F;
            this.テキスト190.Name = "テキスト190";
            this.テキスト190.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-weight: normal; text-alig" +
    "n: left; ddo-char-set: 1";
            this.テキスト190.Tag = "";
            this.テキスト190.Text = "ITEM02";
            this.テキスト190.Top = 0.4889002F;
            this.テキスト190.Width = 1.73125F;
            // 
            // テキスト255
            // 
            this.テキスト255.DataField = "ITEM03";
            this.テキスト255.Height = 0.2006944F;
            this.テキスト255.Left = 0.3930556F;
            this.テキスト255.Name = "テキスト255";
            this.テキスト255.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 1";
            this.テキスト255.Tag = "";
            this.テキスト255.Text = "ITEM03";
            this.テキスト255.Top = 0.7250113F;
            this.テキスト255.Width = 1.23125F;
            // 
            // テキスト256
            // 
            this.テキスト256.DataField = "ITEM04";
            this.テキスト256.Height = 0.2006944F;
            this.テキスト256.Left = 1.989764F;
            this.テキスト256.Name = "テキスト256";
            this.テキスト256.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 1";
            this.テキスト256.Tag = "";
            this.テキスト256.Text = "ITEM04";
            this.テキスト256.Top = 0.7251973F;
            this.テキスト256.Width = 1.23125F;
            // 
            // テキスト195
            // 
            this.テキスト195.DataField = "ITEM18";
            this.テキスト195.Height = 0.2006944F;
            this.テキスト195.Left = 4.423229F;
            this.テキスト195.Name = "テキスト195";
            this.テキスト195.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.テキスト195.Tag = "";
            this.テキスト195.Text = "ITEM18";
            this.テキスト195.Top = 0.685428F;
            this.テキスト195.Width = 3.288578F;
            // 
            // reportInfo1
            // 
            this.reportInfo1.FormatString = "{RunDateTime:yyyy/MM/dd}";
            this.reportInfo1.Height = 0.1925197F;
            this.reportInfo1.Left = 6.220079F;
            this.reportInfo1.Name = "reportInfo1";
            this.reportInfo1.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; ddo-char-set: 1";
            this.reportInfo1.Top = 0.005118111F;
            this.reportInfo1.Width = 0.9319234F;
            // 
            // label1
            // 
            this.label1.Height = 0.2011483F;
            this.label1.HyperLink = null;
            this.label1.Left = 0.08346462F;
            this.label1.Name = "label1";
            this.label1.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; ddo-char-set: 1";
            this.label1.Text = "TEL:";
            this.label1.Top = 0.7251973F;
            this.label1.Width = 0.3094488F;
            // 
            // label2
            // 
            this.label2.Height = 0.2011483F;
            this.label2.HyperLink = null;
            this.label2.Left = 1.661417F;
            this.label2.Name = "label2";
            this.label2.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; ddo-char-set: 1";
            this.label2.Text = "FAX:";
            this.label2.Top = 0.7244099F;
            this.label2.Width = 0.3094487F;
            // 
            // judgeNo
            // 
            this.judgeNo.CountNullValues = true;
            this.judgeNo.DataField = "ITEM23";
            this.judgeNo.Height = 0.1456693F;
            this.judgeNo.Left = 0F;
            this.judgeNo.Name = "judgeNo";
            this.judgeNo.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: center; ddo-char-set: 1";
            this.judgeNo.Tag = "";
            this.judgeNo.Text = "2222";
            this.judgeNo.Top = 0F;
            this.judgeNo.Visible = false;
            this.judgeNo.Width = 0.3826776F;
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.テキスト32,
            this.テキスト33,
            this.テキスト35,
            this.テキスト36,
            this.テキスト37,
            this.テキスト38,
            this.テキスト39,
            this.テキスト49,
            this.detailLine,
            this.直線18,
            this.直線21,
            this.直線22,
            this.直線23,
            this.直線24,
            this.直線25,
            this.直線26,
            this.直線27,
            this.直線106,
            this.テキスト193,
            this.lastLine,
            this.judgeNo});
            this.detail.Height = 0.1968504F;
            this.detail.Name = "detail";
            this.detail.Format += new System.EventHandler(this.detail_Format);
            // 
            // テキスト32
            // 
            this.テキスト32.DataField = "ITEM09";
            this.テキスト32.Height = 0.1814961F;
            this.テキスト32.Left = 0.03125F;
            this.テキスト32.Name = "テキスト32";
            this.テキスト32.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.テキスト32.Tag = "";
            this.テキスト32.Text = "ITEM09";
            this.テキスト32.Top = 0.005118111F;
            this.テキスト32.Width = 0.75F;
            // 
            // テキスト33
            // 
            this.テキスト33.DataField = "ITEM10";
            this.テキスト33.Height = 0.1814961F;
            this.テキスト33.Left = 0.7868056F;
            this.テキスト33.Name = "テキスト33";
            this.テキスト33.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 1";
            this.テキスト33.Tag = "";
            this.テキスト33.Text = "ITEM10";
            this.テキスト33.Top = 0.005118111F;
            this.テキスト33.Width = 0.5229167F;
            // 
            // テキスト35
            // 
            this.テキスト35.DataField = "ITEM11";
            this.テキスト35.Height = 0.1814961F;
            this.テキスト35.Left = 1.3125F;
            this.テキスト35.MultiLine = false;
            this.テキスト35.Name = "テキスト35";
            this.テキスト35.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.テキスト35.Tag = "";
            this.テキスト35.Text = "ITEM11";
            this.テキスト35.Top = 0.005118111F;
            this.テキスト35.Width = 0.6583334F;
            // 
            // テキスト36
            // 
            this.テキスト36.DataField = "ITEM13";
            this.テキスト36.Height = 0.1814961F;
            this.テキスト36.Left = 2.051389F;
            this.テキスト36.MultiLine = false;
            this.テキスト36.Name = "テキスト36";
            this.テキスト36.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 1";
            this.テキスト36.Tag = "";
            this.テキスト36.Text = "ITEM13";
            this.テキスト36.Top = 0.005118111F;
            this.テキスト36.Width = 2.158333F;
            // 
            // テキスト37
            // 
            this.テキスト37.DataField = "ITEM14";
            this.テキスト37.Height = 0.1814961F;
            this.テキスト37.Left = 4.209843F;
            this.テキスト37.Name = "テキスト37";
            this.テキスト37.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 1";
            this.テキスト37.Tag = "";
            this.テキスト37.Text = "ITEM14";
            this.テキスト37.Top = 0.005118111F;
            this.テキスト37.Width = 0.8380742F;
            // 
            // テキスト38
            // 
            this.テキスト38.DataField = "ITEM16";
            this.テキスト38.Height = 0.1814961F;
            this.テキスト38.Left = 5.904861F;
            this.テキスト38.Name = "テキスト38";
            this.テキスト38.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 1";
            this.テキスト38.Tag = "";
            this.テキスト38.Text = "ITEM16";
            this.テキスト38.Top = 0.005118111F;
            this.テキスト38.Width = 0.79375F;
            // 
            // テキスト39
            // 
            this.テキスト39.DataField = "ITEM17";
            this.テキスト39.Height = 0.1814961F;
            this.テキスト39.Left = 6.735417F;
            this.テキスト39.Name = "テキスト39";
            this.テキスト39.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 1";
            this.テキスト39.Tag = "";
            this.テキスト39.Text = "ITEM17";
            this.テキスト39.Top = 0.005118111F;
            this.テキスト39.Width = 0.9291667F;
            // 
            // テキスト49
            // 
            this.テキスト49.DataField = "ITEM15";
            this.テキスト49.Height = 0.1814961F;
            this.テキスト49.Left = 5.118055F;
            this.テキスト49.Name = "テキスト49";
            this.テキスト49.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 1";
            this.テキスト49.Tag = "";
            this.テキスト49.Text = "ITEM15";
            this.テキスト49.Top = 0.005118111F;
            this.テキスト49.Width = 0.7520834F;
            // 
            // detailLine
            // 
            this.detailLine.Height = 0F;
            this.detailLine.Left = 0F;
            this.detailLine.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.detailLine.LineWeight = 1F;
            this.detailLine.Name = "detailLine";
            this.detailLine.Tag = "";
            this.detailLine.Top = 0.1866142F;
            this.detailLine.Width = 7.736111F;
            this.detailLine.X1 = 0F;
            this.detailLine.X2 = 7.736111F;
            this.detailLine.Y1 = 0.1866142F;
            this.detailLine.Y2 = 0.1866142F;
            // 
            // 直線18
            // 
            this.直線18.Height = 0.1826553F;
            this.直線18.Left = 0F;
            this.直線18.LineWeight = 1F;
            this.直線18.Name = "直線18";
            this.直線18.Tag = "";
            this.直線18.Top = 0F;
            this.直線18.Width = 0F;
            this.直線18.X1 = 0F;
            this.直線18.X2 = 0F;
            this.直線18.Y1 = 0F;
            this.直線18.Y2 = 0.1826553F;
            // 
            // 直線21
            // 
            this.直線21.Height = 0.1826553F;
            this.直線21.Left = 0.7916667F;
            this.直線21.LineWeight = 1F;
            this.直線21.Name = "直線21";
            this.直線21.Tag = "";
            this.直線21.Top = 0F;
            this.直線21.Width = 6.562471E-05F;
            this.直線21.X1 = 0.7917323F;
            this.直線21.X2 = 0.7916667F;
            this.直線21.Y1 = 0F;
            this.直線21.Y2 = 0.1826553F;
            // 
            // 直線22
            // 
            this.直線22.Height = 0.1826553F;
            this.直線22.Left = 1.3125F;
            this.直線22.LineWeight = 1F;
            this.直線22.Name = "直線22";
            this.直線22.Tag = "";
            this.直線22.Top = 0F;
            this.直線22.Width = 6.604195E-05F;
            this.直線22.X1 = 1.312566F;
            this.直線22.X2 = 1.3125F;
            this.直線22.Y1 = 0F;
            this.直線22.Y2 = 0.1826553F;
            // 
            // 直線23
            // 
            this.直線23.Height = 0.1826553F;
            this.直線23.Left = 1.989583F;
            this.直線23.LineWeight = 1F;
            this.直線23.Name = "直線23";
            this.直線23.Tag = "";
            this.直線23.Top = 0F;
            this.直線23.Width = 6.604195E-05F;
            this.直線23.X1 = 1.989649F;
            this.直線23.X2 = 1.989583F;
            this.直線23.Y1 = 0F;
            this.直線23.Y2 = 0.1826553F;
            // 
            // 直線24
            // 
            this.直線24.Height = 0.1775372F;
            this.直線24.Left = 4.197917F;
            this.直線24.LineWeight = 0F;
            this.直線24.Name = "直線24";
            this.直線24.Tag = "";
            this.直線24.Top = 0F;
            this.直線24.Width = 0.0001149178F;
            this.直線24.X1 = 4.198032F;
            this.直線24.X2 = 4.197917F;
            this.直線24.Y1 = 0F;
            this.直線24.Y2 = 0.1775372F;
            // 
            // 直線25
            // 
            this.直線25.Height = 0.1826553F;
            this.直線25.Left = 5.114583F;
            this.直線25.LineWeight = 1F;
            this.直線25.Name = "直線25";
            this.直線25.Tag = "";
            this.直線25.Top = 0F;
            this.直線25.Width = 6.580353E-05F;
            this.直線25.X1 = 5.114649F;
            this.直線25.X2 = 5.114583F;
            this.直線25.Y1 = 0F;
            this.直線25.Y2 = 0.1826553F;
            // 
            // 直線26
            // 
            this.直線26.Height = 0.1826553F;
            this.直線26.Left = 5.916667F;
            this.直線26.LineWeight = 1F;
            this.直線26.Name = "直線26";
            this.直線26.Tag = "";
            this.直線26.Top = 0F;
            this.直線26.Width = 6.580353E-05F;
            this.直線26.X1 = 5.916733F;
            this.直線26.X2 = 5.916667F;
            this.直線26.Y1 = 0F;
            this.直線26.Y2 = 0.1826553F;
            // 
            // 直線27
            // 
            this.直線27.Height = 0.1826553F;
            this.直線27.Left = 6.739583F;
            this.直線27.LineWeight = 1F;
            this.直線27.Name = "直線27";
            this.直線27.Tag = "";
            this.直線27.Top = 0F;
            this.直線27.Width = 6.580353E-05F;
            this.直線27.X1 = 6.739649F;
            this.直線27.X2 = 6.739583F;
            this.直線27.Y1 = 0F;
            this.直線27.Y2 = 0.1826553F;
            // 
            // 直線106
            // 
            this.直線106.Height = 0.1826553F;
            this.直線106.Left = 7.729167F;
            this.直線106.LineWeight = 1F;
            this.直線106.Name = "直線106";
            this.直線106.Tag = "";
            this.直線106.Top = 0F;
            this.直線106.Width = 6.580353E-05F;
            this.直線106.X1 = 7.729233F;
            this.直線106.X2 = 7.729167F;
            this.直線106.Y1 = 0F;
            this.直線106.Y2 = 0.1826553F;
            // 
            // テキスト193
            // 
            this.テキスト193.DataField = "ITEM12";
            this.テキスト193.Height = 0.1814961F;
            this.テキスト193.Left = 2.051575F;
            this.テキスト193.MultiLine = false;
            this.テキスト193.Name = "テキスト193";
            this.テキスト193.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.テキスト193.Tag = "";
            this.テキスト193.Text = "ITEM12";
            this.テキスト193.Top = 0F;
            this.テキスト193.Width = 2.146457F;
            // 
            // lastLine
            // 
            this.lastLine.Height = 0F;
            this.lastLine.Left = 0F;
            this.lastLine.LineWeight = 1F;
            this.lastLine.Name = "lastLine";
            this.lastLine.Tag = "";
            this.lastLine.Top = 0.1866142F;
            this.lastLine.Width = 7.729135F;
            this.lastLine.X1 = 0F;
            this.lastLine.X2 = 7.729135F;
            this.lastLine.Y1 = 0.1866142F;
            this.lastLine.Y2 = 0.1866142F;
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            // 
            // groupHeader1
            // 
            this.groupHeader1.DataField = "ITEM01";
            this.groupHeader1.Height = 0F;
            this.groupHeader1.Name = "groupHeader1";
            this.groupHeader1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
            // 
            // groupFooter1
            // 
            this.groupFooter1.Height = 0F;
            this.groupFooter1.Name = "groupFooter1";
            // 
            // KBDR1032R
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0F;
            this.PageSettings.Margins.Left = 0.2362205F;
            this.PageSettings.Margins.Right = 0.2362205F;
            this.PageSettings.Margins.Top = 0.3937007F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Portrait;
            this.PageSettings.PaperHeight = 11.69291F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.267716F;
            this.PrintWidth = 7.760237F;
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.groupHeader1);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.groupFooter1);
            this.Sections.Add(this.pageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.テキスト188)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGrpPages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル71)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト194)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル180)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル181)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル182)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル183)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル184)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル185)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル186)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル187)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト189)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト190)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト255)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト256)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト195)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportInfo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.judgeNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト193)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.Line 直線68;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト188;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGrpPages;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル71;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト194;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル180;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル181;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル182;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル183;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル184;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル185;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル186;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル187;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト189;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト190;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト255;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト256;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト195;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト32;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト33;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト35;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト36;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト37;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト38;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト39;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト49;
        private GrapeCity.ActiveReports.SectionReportModel.Line detailLine;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線18;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線21;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線22;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線23;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線24;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線25;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線26;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線27;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線106;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト193;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader groupHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter groupFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.Line lastLine;
        private GrapeCity.ActiveReports.SectionReportModel.ReportInfo reportInfo1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox judgeNo;
    }
}
