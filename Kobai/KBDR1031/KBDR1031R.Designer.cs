﻿namespace jp.co.fsi.kb.kbdr1031
{
    /// <summary>
    /// KBDR1031R の概要の説明です。
    /// </summary>
    partial class KBDR1031R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(KBDR1031R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.txtGrpPages = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.直線68 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.テキスト188 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル71 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.テキスト307 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト316 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト317 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト318 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト319 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト332 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル321 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル322 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル323 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル324 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル325 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル326 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル327 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル328 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル329 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル330 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.reportInfo1 = new GrapeCity.ActiveReports.SectionReportModel.ReportInfo();
            this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.judgeNo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.textBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト32 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト33 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト35 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト36 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト37 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト38 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト39 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト40 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト42 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト43 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト72 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.直線91 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線92 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線93 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線94 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線95 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線96 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線97 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線98 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線99 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線100 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線102 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.テキスト253 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト308 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.detailLine = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lastLine = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.groupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.groupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            ((System.ComponentModel.ISupportInitialize)(this.txtGrpPages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト188)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル71)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト307)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト316)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト317)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト318)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト319)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト332)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル321)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル322)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル323)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル324)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル325)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル326)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル327)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル328)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル329)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル330)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportInfo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.judgeNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト72)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト253)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト308)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtGrpPages,
            this.直線68,
            this.テキスト188,
            this.ラベル71,
            this.テキスト307,
            this.テキスト316,
            this.テキスト317,
            this.テキスト318,
            this.テキスト319,
            this.テキスト332,
            this.ラベル321,
            this.ラベル322,
            this.ラベル323,
            this.ラベル324,
            this.ラベル326,
            this.ラベル327,
            this.ラベル328,
            this.ラベル329,
            this.ラベル330,
            this.reportInfo1,
            this.label1,
            this.label2,
            this.label3,
            this.ラベル325});
            this.pageHeader.Height = 0.8164644F;
            this.pageHeader.Name = "pageHeader";
            // 
            // txtGrpPages
            // 
            this.txtGrpPages.Height = 0.1875F;
            this.txtGrpPages.Left = 10.59348F;
            this.txtGrpPages.Name = "txtGrpPages";
            this.txtGrpPages.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 1";
            this.txtGrpPages.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtGrpPages.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.txtGrpPages.Tag = "";
            this.txtGrpPages.Text = null;
            this.txtGrpPages.Top = 0.08333334F;
            this.txtGrpPages.Width = 0.375F;
            // 
            // 直線68
            // 
            this.直線68.Height = 0F;
            this.直線68.Left = 4.393405F;
            this.直線68.LineWeight = 1F;
            this.直線68.Name = "直線68";
            this.直線68.Tag = "";
            this.直線68.Top = 0.3125F;
            this.直線68.Width = 2.473611F;
            this.直線68.X1 = 4.393405F;
            this.直線68.X2 = 6.867016F;
            this.直線68.Y1 = 0.3125F;
            this.直線68.Y2 = 0.3125F;
            // 
            // テキスト188
            // 
            this.テキスト188.DataField = "ITEM07";
            this.テキスト188.Height = 0.1770833F;
            this.テキスト188.Left = 3.907294F;
            this.テキスト188.Name = "テキスト188";
            this.テキスト188.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-weight: normal; text-alig" +
    "n: center; ddo-char-set: 1";
            this.テキスト188.Tag = "";
            this.テキスト188.Text = "ITEM07";
            this.テキスト188.Top = 0.3541667F;
            this.テキスト188.Width = 3.445833F;
            // 
            // ラベル71
            // 
            this.ラベル71.Height = 0.1972222F;
            this.ラベル71.HyperLink = null;
            this.ラベル71.Left = 10.97403F;
            this.ラベル71.Name = "ラベル71";
            this.ラベル71.Style = "color: #4C535C; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 1";
            this.ラベル71.Tag = "";
            this.ラベル71.Text = "頁";
            this.ラベル71.Top = 0.07847222F;
            this.ラベル71.Width = 0.2027778F;
            // 
            // テキスト307
            // 
            this.テキスト307.DataField = "ITEM08";
            this.テキスト307.Height = 0.28125F;
            this.テキスト307.Left = 4.494794F;
            this.テキスト307.Name = "テキスト307";
            this.テキスト307.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 18pt; font-weight: bold; text-align:" +
    " center; ddo-char-set: 128";
            this.テキスト307.Tag = "";
            this.テキスト307.Text = "ITEM08";
            this.テキスト307.Top = 0F;
            this.テキスト307.Width = 2.270833F;
            // 
            // テキスト316
            // 
            this.テキスト316.DataField = "ITEM01";
            this.テキスト316.Height = 0.2006944F;
            this.テキスト316.Left = 0.232367F;
            this.テキスト316.Name = "テキスト316";
            this.テキスト316.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: normal; text-a" +
    "lign: right; ddo-char-set: 1";
            this.テキスト316.Tag = "";
            this.テキスト316.Text = "ITEM01";
            this.テキスト316.Top = 0.1180556F;
            this.テキスト316.Width = 0.6583334F;
            // 
            // テキスト317
            // 
            this.テキスト317.DataField = "ITEM02";
            this.テキスト317.Height = 0.2006944F;
            this.テキスト317.Left = 0.9407004F;
            this.テキスト317.Name = "テキスト317";
            this.テキスト317.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: normal; text-a" +
    "lign: left; ddo-char-set: 1";
            this.テキスト317.Tag = "";
            this.テキスト317.Text = "ITEM02";
            this.テキスト317.Top = 0.1180556F;
            this.テキスト317.Width = 2.168749F;
            // 
            // テキスト318
            // 
            this.テキスト318.DataField = "ITEM03";
            this.テキスト318.Height = 0.2006944F;
            this.テキスト318.Left = 0.901117F;
            this.テキスト318.Name = "テキスト318";
            this.テキスト318.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 1";
            this.テキスト318.Tag = "";
            this.テキスト318.Text = "ITEM03";
            this.テキスト318.Top = 0.3541667F;
            this.テキスト318.Width = 0.9862846F;
            // 
            // テキスト319
            // 
            this.テキスト319.DataField = "ITEM04";
            this.テキスト319.Height = 0.2006944F;
            this.テキスト319.Left = 2.251181F;
            this.テキスト319.Name = "テキスト319";
            this.テキスト319.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 1";
            this.テキスト319.Tag = "";
            this.テキスト319.Text = "ITEM04";
            this.テキスト319.Top = 0.3543307F;
            this.テキスト319.Width = 1.179134F;
            // 
            // テキスト332
            // 
            this.テキスト332.DataField = "ITEM23";
            this.テキスト332.Height = 0.2006944F;
            this.テキスト332.Left = 8.296851F;
            this.テキスト332.Name = "テキスト332";
            this.テキスト332.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 1";
            this.テキスト332.Tag = "";
            this.テキスト332.Text = "ITEM23";
            this.テキスト332.Top = 0.3541667F;
            this.テキスト332.Width = 2.877183F;
            // 
            // ラベル321
            // 
            this.ラベル321.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル321.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル321.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル321.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル321.Height = 0.198797F;
            this.ラベル321.HyperLink = null;
            this.ラベル321.Left = 0F;
            this.ラベル321.Name = "ラベル321";
            this.ラベル321.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; f" +
    "ont-weight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.ラベル321.Tag = "";
            this.ラベル321.Text = "伝票日付";
            this.ラベル321.Top = 0.6169292F;
            this.ラベル321.Width = 0.725F;
            // 
            // ラベル322
            // 
            this.ラベル322.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル322.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル322.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル322.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル322.Height = 0.1972222F;
            this.ラベル322.HyperLink = null;
            this.ラベル322.Left = 0.7291667F;
            this.ラベル322.Name = "ラベル322";
            this.ラベル322.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; f" +
    "ont-weight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.ラベル322.Tag = "";
            this.ラベル322.Text = "伝票No.";
            this.ラベル322.Top = 0.6171151F;
            this.ラベル322.Width = 0.53125F;
            // 
            // ラベル323
            // 
            this.ラベル323.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル323.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル323.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル323.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル323.Height = 0.1972222F;
            this.ラベル323.HyperLink = null;
            this.ラベル323.Left = 1.260417F;
            this.ラベル323.Name = "ラベル323";
            this.ラベル323.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; f" +
    "ont-weight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.ラベル323.Tag = "";
            this.ラベル323.Text = "取引区分";
            this.ラベル323.Top = 0.6171151F;
            this.ラベル323.Width = 0.6770833F;
            // 
            // ラベル324
            // 
            this.ラベル324.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル324.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル324.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル324.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル324.Height = 0.1972222F;
            this.ラベル324.HyperLink = null;
            this.ラベル324.Left = 1.9375F;
            this.ラベル324.Name = "ラベル324";
            this.ラベル324.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; f" +
    "ont-weight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.ラベル324.Tag = "";
            this.ラベル324.Text = "商　品　名　・　規　格";
            this.ラベル324.Top = 0.6171151F;
            this.ラベル324.Width = 3.570374F;
            // 
            // ラベル325
            // 
            this.ラベル325.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル325.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル325.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル325.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル325.Height = 0.1972441F;
            this.ラベル325.HyperLink = null;
            this.ラベル325.Left = 5.901575F;
            this.ラベル325.Name = "ラベル325";
            this.ラベル325.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; f" +
    "ont-weight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.ラベル325.Tag = "";
            this.ラベル325.Text = "数　量";
            this.ラベル325.Top = 0.6169292F;
            this.ラベル325.Width = 0.6850394F;
            // 
            // ラベル326
            // 
            this.ラベル326.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル326.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル326.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル326.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル326.Height = 0.1972222F;
            this.ラベル326.HyperLink = null;
            this.ラベル326.Left = 9.279529F;
            this.ラベル326.Name = "ラベル326";
            this.ラベル326.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; f" +
    "ont-weight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.ラベル326.Tag = "";
            this.ラベル326.Text = "入 金 額";
            this.ラベル326.Top = 0.6169292F;
            this.ラベル326.Width = 0.9448819F;
            // 
            // ラベル327
            // 
            this.ラベル327.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル327.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル327.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル327.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル327.Height = 0.1972222F;
            this.ラベル327.HyperLink = null;
            this.ラベル327.Left = 10.22874F;
            this.ラベル327.Name = "ラベル327";
            this.ラベル327.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; f" +
    "ont-weight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.ラベル327.Tag = "";
            this.ラベル327.Text = "残　高";
            this.ラベル327.Top = 0.6169292F;
            this.ラベル327.Width = 0.9447708F;
            // 
            // ラベル328
            // 
            this.ラベル328.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル328.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル328.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル328.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル328.Height = 0.1972222F;
            this.ラベル328.HyperLink = null;
            this.ラベル328.Left = 5.507874F;
            this.ラベル328.Name = "ラベル328";
            this.ラベル328.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; f" +
    "ont-weight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.ラベル328.Tag = "";
            this.ラベル328.Text = "入数";
            this.ラベル328.Top = 0.6169292F;
            this.ラベル328.Width = 0.3937008F;
            // 
            // ラベル329
            // 
            this.ラベル329.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル329.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル329.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル329.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル329.Height = 0.1972222F;
            this.ラベル329.HyperLink = null;
            this.ラベル329.Left = 7.530757F;
            this.ラベル329.Name = "ラベル329";
            this.ラベル329.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; f" +
    "ont-weight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.ラベル329.Tag = "";
            this.ラベル329.Text = "売上金額";
            this.ラベル329.Top = 0.6171151F;
            this.ラベル329.Width = 0.9448819F;
            // 
            // ラベル330
            // 
            this.ラベル330.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル330.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル330.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル330.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル330.Height = 0.1972222F;
            this.ラベル330.HyperLink = null;
            this.ラベル330.Left = 6.552362F;
            this.ラベル330.Name = "ラベル330";
            this.ラベル330.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; f" +
    "ont-weight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.ラベル330.Tag = "";
            this.ラベル330.Text = "単　価";
            this.ラベル330.Top = 0.6169292F;
            this.ラベル330.Width = 0.9783469F;
            // 
            // reportInfo1
            // 
            this.reportInfo1.FormatString = "{RunDateTime:yyyy/MM/dd}";
            this.reportInfo1.Height = 0.1924869F;
            this.reportInfo1.Left = 9.207874F;
            this.reportInfo1.Name = "reportInfo1";
            this.reportInfo1.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; ddo-char-set: 1";
            this.reportInfo1.Top = 0.07834646F;
            this.reportInfo1.Width = 1.385466F;
            // 
            // label1
            // 
            this.label1.Height = 0.1977526F;
            this.label1.HyperLink = null;
            this.label1.Left = 0.5267717F;
            this.label1.Name = "label1";
            this.label1.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; ddo-char-set: 1";
            this.label1.Text = "TEL:";
            this.label1.Top = 0.3543307F;
            this.label1.Width = 0.3637796F;
            // 
            // label2
            // 
            this.label2.Height = 0.1977526F;
            this.label2.HyperLink = null;
            this.label2.Left = 1.887402F;
            this.label2.Name = "label2";
            this.label2.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; ddo-char-set: 1";
            this.label2.Text = "FAX:";
            this.label2.Top = 0.3543307F;
            this.label2.Width = 0.3637797F;
            // 
            // label3
            // 
            this.label3.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label3.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label3.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label3.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label3.Height = 0.1972222F;
            this.label3.HyperLink = null;
            this.label3.Left = 8.475591F;
            this.label3.Name = "label3";
            this.label3.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; f" +
    "ont-weight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.label3.Tag = "";
            this.label3.Text = "消費税";
            this.label3.Top = 0.6169292F;
            this.label3.Width = 0.8039375F;
            // 
            // judgeNo
            // 
            this.judgeNo.DataField = "ITEM24";
            this.judgeNo.Height = 0.1874016F;
            this.judgeNo.Left = 0F;
            this.judgeNo.Name = "judgeNo";
            this.judgeNo.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: center; ddo-char-set: 1";
            this.judgeNo.Tag = "";
            this.judgeNo.Text = "ITEM24";
            this.judgeNo.Top = 0F;
            this.judgeNo.Visible = false;
            this.judgeNo.Width = 0.3826776F;
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.textBox1,
            this.テキスト32,
            this.テキスト33,
            this.テキスト35,
            this.テキスト36,
            this.テキスト37,
            this.テキスト38,
            this.テキスト39,
            this.テキスト40,
            this.テキスト42,
            this.テキスト43,
            this.テキスト72,
            this.直線91,
            this.直線92,
            this.直線93,
            this.直線94,
            this.直線95,
            this.直線96,
            this.直線97,
            this.直線98,
            this.直線99,
            this.直線100,
            this.直線102,
            this.テキスト253,
            this.テキスト308,
            this.detailLine,
            this.lastLine,
            this.judgeNo,
            this.line1});
            this.detail.Height = 0.2006944F;
            this.detail.Name = "detail";
            this.detail.Format += new System.EventHandler(this.detail_Format);
            // 
            // textBox1
            // 
            this.textBox1.DataField = "ITEM20";
            this.textBox1.Height = 0.1784722F;
            this.textBox1.Left = 8.475591F;
            this.textBox1.Name = "textBox1";
            this.textBox1.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 1";
            this.textBox1.Tag = "";
            this.textBox1.Text = "ITEM20";
            this.textBox1.Top = 0.0003937008F;
            this.textBox1.Width = 0.803937F;
            // 
            // テキスト32
            // 
            this.テキスト32.DataField = "ITEM09";
            this.テキスト32.Height = 0.1784722F;
            this.テキスト32.Left = 0.02083334F;
            this.テキスト32.Name = "テキスト32";
            this.テキスト32.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.テキスト32.Tag = "";
            this.テキスト32.Text = "ITEM09";
            this.テキスト32.Top = 0.0003937008F;
            this.テキスト32.Width = 0.6840278F;
            // 
            // テキスト33
            // 
            this.テキスト33.DataField = "ITEM10";
            this.テキスト33.Height = 0.1784722F;
            this.テキスト33.Left = 0.7515749F;
            this.テキスト33.Name = "テキスト33";
            this.テキスト33.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 1";
            this.テキスト33.Tag = "";
            this.テキスト33.Text = "ITEM10";
            this.テキスト33.Top = 0.0003937008F;
            this.テキスト33.Width = 0.4901575F;
            // 
            // テキスト35
            // 
            this.テキスト35.DataField = "ITEM11";
            this.テキスト35.Height = 0.1784722F;
            this.テキスト35.Left = 1.281102F;
            this.テキスト35.MultiLine = false;
            this.テキスト35.Name = "テキスト35";
            this.テキスト35.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.テキスト35.Tag = "";
            this.テキスト35.Text = "ITEM11";
            this.テキスト35.Top = 0.0003937008F;
            this.テキスト35.Width = 0.6271654F;
            // 
            // テキスト36
            // 
            this.テキスト36.DataField = "ITEM13";
            this.テキスト36.Height = 0.1784722F;
            this.テキスト36.Left = 1.96875F;
            this.テキスト36.Name = "テキスト36";
            this.テキスト36.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 1";
            this.テキスト36.Tag = "";
            this.テキスト36.Text = "ITEM13";
            this.テキスト36.Top = 0.0003937008F;
            this.テキスト36.Width = 0.4395833F;
            // 
            // テキスト37
            // 
            this.テキスト37.DataField = "ITEM16";
            this.テキスト37.Height = 0.1784722F;
            this.テキスト37.Left = 5.524804F;
            this.テキスト37.Name = "テキスト37";
            this.テキスト37.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 1";
            this.テキスト37.Tag = "";
            this.テキスト37.Text = "ITEM16";
            this.テキスト37.Top = 0.0003937008F;
            this.テキスト37.Width = 0.3769736F;
            // 
            // テキスト38
            // 
            this.テキスト38.DataField = "ITEM17";
            this.テキスト38.Height = 0.1784722F;
            this.テキスト38.Left = 5.919888F;
            this.テキスト38.Name = "テキスト38";
            this.テキスト38.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 1";
            this.テキスト38.Tag = "";
            this.テキスト38.Text = "ITEM17";
            this.テキスト38.Top = 0.0003937008F;
            this.テキスト38.Width = 0.6509789F;
            // 
            // テキスト39
            // 
            this.テキスト39.DataField = "ITEM18";
            this.テキスト39.Height = 0.1784722F;
            this.テキスト39.Left = 6.587402F;
            this.テキスト39.Name = "テキスト39";
            this.テキスト39.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 1";
            this.テキスト39.Tag = "";
            this.テキスト39.Text = "ITEM18";
            this.テキスト39.Top = 0.0003937008F;
            this.テキスト39.Width = 0.9433073F;
            // 
            // テキスト40
            // 
            this.テキスト40.DataField = "ITEM19";
            this.テキスト40.Height = 0.1784722F;
            this.テキスト40.Left = 7.55315F;
            this.テキスト40.Name = "テキスト40";
            this.テキスト40.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 1";
            this.テキスト40.Tag = "";
            this.テキスト40.Text = "ITEM19";
            this.テキスト40.Top = 0.0003937008F;
            this.テキスト40.Width = 0.922441F;
            // 
            // テキスト42
            // 
            this.テキスト42.DataField = "ITEM21";
            this.テキスト42.Height = 0.1787402F;
            this.テキスト42.Left = 9.279627F;
            this.テキスト42.Name = "テキスト42";
            this.テキスト42.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 1";
            this.テキスト42.Tag = "";
            this.テキスト42.Text = "ITEM21";
            this.テキスト42.Top = 0F;
            this.テキスト42.Width = 0.9448819F;
            // 
            // テキスト43
            // 
            this.テキスト43.DataField = "ITEM22";
            this.テキスト43.Height = 0.1784722F;
            this.テキスト43.Left = 10.22874F;
            this.テキスト43.Name = "テキスト43";
            this.テキスト43.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 1";
            this.テキスト43.Tag = "";
            this.テキスト43.Text = "ITEM22";
            this.テキスト43.Top = 0.0003937008F;
            this.テキスト43.Width = 0.9448869F;
            // 
            // テキスト72
            // 
            this.テキスト72.DataField = "ITEM14";
            this.テキスト72.Height = 0.1784722F;
            this.テキスト72.Left = 2.479167F;
            this.テキスト72.MultiLine = false;
            this.テキスト72.Name = "テキスト72";
            this.テキスト72.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 1";
            this.テキスト72.Tag = "";
            this.テキスト72.Text = "ITEM14";
            this.テキスト72.Top = 0.0003937008F;
            this.テキスト72.Width = 2.265715F;
            // 
            // 直線91
            // 
            this.直線91.Height = 0.1877953F;
            this.直線91.Left = 0.7298611F;
            this.直線91.LineWeight = 1F;
            this.直線91.Name = "直線91";
            this.直線91.Tag = "";
            this.直線91.Top = 0F;
            this.直線91.Width = 0F;
            this.直線91.X1 = 0.7298611F;
            this.直線91.X2 = 0.7298611F;
            this.直線91.Y1 = 0F;
            this.直線91.Y2 = 0.1877953F;
            // 
            // 直線92
            // 
            this.直線92.Height = 0.1877953F;
            this.直線92.Left = 1.260417F;
            this.直線92.LineWeight = 1F;
            this.直線92.Name = "直線92";
            this.直線92.Tag = "";
            this.直線92.Top = 0F;
            this.直線92.Width = 0F;
            this.直線92.X1 = 1.260417F;
            this.直線92.X2 = 1.260417F;
            this.直線92.Y1 = 0F;
            this.直線92.Y2 = 0.1877953F;
            // 
            // 直線93
            // 
            this.直線93.Height = 0.1877953F;
            this.直線93.Left = 1.9375F;
            this.直線93.LineWeight = 1F;
            this.直線93.Name = "直線93";
            this.直線93.Tag = "";
            this.直線93.Top = 0F;
            this.直線93.Width = 0F;
            this.直線93.X1 = 1.9375F;
            this.直線93.X2 = 1.9375F;
            this.直線93.Y1 = 0F;
            this.直線93.Y2 = 0.1877953F;
            // 
            // 直線94
            // 
            this.直線94.Height = 0.1877953F;
            this.直線94.Left = 5.508832F;
            this.直線94.LineWeight = 1F;
            this.直線94.Name = "直線94";
            this.直線94.Tag = "";
            this.直線94.Top = 2.910383E-11F;
            this.直線94.Width = 0F;
            this.直線94.X1 = 5.508832F;
            this.直線94.X2 = 5.508832F;
            this.直線94.Y1 = 2.910383E-11F;
            this.直線94.Y2 = 0.1877953F;
            // 
            // 直線95
            // 
            this.直線95.Height = 0.1877953F;
            this.直線95.Left = 5.902385F;
            this.直線95.LineWeight = 1F;
            this.直線95.Name = "直線95";
            this.直線95.Tag = "";
            this.直線95.Top = 5.820766E-11F;
            this.直線95.Width = 0F;
            this.直線95.X1 = 5.902385F;
            this.直線95.X2 = 5.902385F;
            this.直線95.Y1 = 5.820766E-11F;
            this.直線95.Y2 = 0.1877953F;
            // 
            // 直線96
            // 
            this.直線96.Height = 0.1877953F;
            this.直線96.Left = 6.587417F;
            this.直線96.LineWeight = 1F;
            this.直線96.Name = "直線96";
            this.直線96.Tag = "";
            this.直線96.Top = 0.0001859069F;
            this.直線96.Width = 0F;
            this.直線96.X1 = 6.587417F;
            this.直線96.X2 = 6.587417F;
            this.直線96.Y1 = 0.0001859069F;
            this.直線96.Y2 = 0.1879812F;
            // 
            // 直線97
            // 
            this.直線97.Height = 0.1877953F;
            this.直線97.Left = 11.17717F;
            this.直線97.LineWeight = 1F;
            this.直線97.Name = "直線97";
            this.直線97.Tag = "";
            this.直線97.Top = 0F;
            this.直線97.Width = 0F;
            this.直線97.X1 = 11.17717F;
            this.直線97.X2 = 11.17717F;
            this.直線97.Y1 = 0F;
            this.直線97.Y2 = 0.1877953F;
            // 
            // 直線98
            // 
            this.直線98.Height = 0.1877953F;
            this.直線98.Left = 10.22874F;
            this.直線98.LineWeight = 1F;
            this.直線98.Name = "直線98";
            this.直線98.Tag = "";
            this.直線98.Top = 0F;
            this.直線98.Width = 0F;
            this.直線98.X1 = 10.22874F;
            this.直線98.X2 = 10.22874F;
            this.直線98.Y1 = 0F;
            this.直線98.Y2 = 0.1877953F;
            // 
            // 直線99
            // 
            this.直線99.Height = 0.1877953F;
            this.直線99.Left = 9.279529F;
            this.直線99.LineWeight = 1F;
            this.直線99.Name = "直線99";
            this.直線99.Tag = "";
            this.直線99.Top = 0F;
            this.直線99.Width = 0F;
            this.直線99.X1 = 9.279529F;
            this.直線99.X2 = 9.279529F;
            this.直線99.Y1 = 0F;
            this.直線99.Y2 = 0.1877953F;
            // 
            // 直線100
            // 
            this.直線100.Height = 0.1877953F;
            this.直線100.Left = 7.530756F;
            this.直線100.LineWeight = 1F;
            this.直線100.Name = "直線100";
            this.直線100.Tag = "";
            this.直線100.Top = 5.820766E-11F;
            this.直線100.Width = 0F;
            this.直線100.X1 = 7.530756F;
            this.直線100.X2 = 7.530756F;
            this.直線100.Y1 = 5.820766E-11F;
            this.直線100.Y2 = 0.1877953F;
            // 
            // 直線102
            // 
            this.直線102.Height = 0.1877953F;
            this.直線102.Left = 0F;
            this.直線102.LineWeight = 1F;
            this.直線102.Name = "直線102";
            this.直線102.Tag = "";
            this.直線102.Top = 0F;
            this.直線102.Width = 0F;
            this.直線102.X1 = 0F;
            this.直線102.X2 = 0F;
            this.直線102.Y1 = 0F;
            this.直線102.Y2 = 0.1877953F;
            // 
            // テキスト253
            // 
            this.テキスト253.DataField = "ITEM12";
            this.テキスト253.Height = 0.1784722F;
            this.テキスト253.Left = 2.408268F;
            this.テキスト253.MultiLine = false;
            this.テキスト253.Name = "テキスト253";
            this.テキスト253.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.テキスト253.Tag = "";
            this.テキスト253.Text = "ITEM12";
            this.テキスト253.Top = 0.0003937008F;
            this.テキスト253.Width = 3.099606F;
            // 
            // テキスト308
            // 
            this.テキスト308.DataField = "ITEM15";
            this.テキスト308.Height = 0.1784722F;
            this.テキスト308.Left = 4.515749F;
            this.テキスト308.MultiLine = false;
            this.テキスト308.Name = "テキスト308";
            this.テキスト308.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 1";
            this.テキスト308.Tag = "";
            this.テキスト308.Text = "ITEM15";
            this.テキスト308.Top = 0.0003937008F;
            this.テキスト308.Width = 0.992126F;
            // 
            // detailLine
            // 
            this.detailLine.Height = 0F;
            this.detailLine.Left = 0F;
            this.detailLine.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.detailLine.LineWeight = 1F;
            this.detailLine.Name = "detailLine";
            this.detailLine.Tag = "";
            this.detailLine.Top = 0.1878937F;
            this.detailLine.Width = 11.17361F;
            this.detailLine.X1 = 0F;
            this.detailLine.X2 = 11.17361F;
            this.detailLine.Y1 = 0.1878937F;
            this.detailLine.Y2 = 0.1878937F;
            // 
            // lastLine
            // 
            this.lastLine.Height = 0F;
            this.lastLine.Left = 0.003543307F;
            this.lastLine.LineWeight = 1F;
            this.lastLine.Name = "lastLine";
            this.lastLine.Tag = "";
            this.lastLine.Top = 0.1877953F;
            this.lastLine.Width = 11.17361F;
            this.lastLine.X1 = 0.003543307F;
            this.lastLine.X2 = 11.17715F;
            this.lastLine.Y1 = 0.1877953F;
            this.lastLine.Y2 = 0.1877953F;
            // 
            // line1
            // 
            this.line1.Height = 0.1877953F;
            this.line1.Left = 8.475591F;
            this.line1.LineWeight = 1F;
            this.line1.Name = "line1";
            this.line1.Tag = "";
            this.line1.Top = 0.006299213F;
            this.line1.Width = 0F;
            this.line1.X1 = 8.475591F;
            this.line1.X2 = 8.475591F;
            this.line1.Y1 = 0.006299213F;
            this.line1.Y2 = 0.1940945F;
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            // 
            // groupHeader1
            // 
            this.groupHeader1.DataField = "ITEM01";
            this.groupHeader1.Height = 0F;
            this.groupHeader1.Name = "groupHeader1";
            this.groupHeader1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
            // 
            // groupFooter1
            // 
            this.groupFooter1.Height = 0F;
            this.groupFooter1.Name = "groupFooter1";
            // 
            // KBDR1031R
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0F;
            this.PageSettings.Margins.Left = 0.1968504F;
            this.PageSettings.Margins.Right = 0.1968504F;
            this.PageSettings.Margins.Top = 0.1968504F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
            this.PageSettings.PaperHeight = 11.69291F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.267716F;
            this.PrintWidth = 11.26042F;
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.groupHeader1);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.groupFooter1);
            this.Sections.Add(this.pageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.txtGrpPages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト188)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル71)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト307)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト316)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト317)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト318)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト319)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト332)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル321)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル322)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル323)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル324)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル325)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル326)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル327)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル328)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル329)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル330)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportInfo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.judgeNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト72)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト253)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト308)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGrpPages;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線68;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト188;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル71;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト307;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト316;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト317;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト318;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト319;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト332;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader groupHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter groupFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル321;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル322;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル323;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル324;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル325;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル326;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル327;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル328;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル329;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル330;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト32;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト33;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト35;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト36;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト37;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト38;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト39;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト40;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト42;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト43;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト72;
        private GrapeCity.ActiveReports.SectionReportModel.Line detailLine;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線91;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線92;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線93;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線94;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線95;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線96;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線97;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線98;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線99;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線100;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線102;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト253;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト308;
        private GrapeCity.ActiveReports.SectionReportModel.Line lastLine;
        private GrapeCity.ActiveReports.SectionReportModel.ReportInfo reportInfo1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox judgeNo;
        private GrapeCity.ActiveReports.SectionReportModel.Label label3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox1;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
    }
}
