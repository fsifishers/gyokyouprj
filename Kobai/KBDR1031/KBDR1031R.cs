﻿using System.Data;
using jp.co.fsi.common.report;
using jp.co.fsi.common.util;

namespace jp.co.fsi.kb.kbdr1031
{
    /// <summary>
    /// KBDR1031R の帳票
    /// </summary>
    public partial class KBDR1031R : BaseReport
    {

        public KBDR1031R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }

        private void detail_Format(object sender, System.EventArgs e)
        {

            if (Util.ToInt(this.judgeNo.Text) == 0)
            {
                this.detailLine.Visible = true;
                this.lastLine.Visible = false;
            }
            else
            {
                this.detailLine.Visible = false;
                this.lastLine.Visible = true;
            }
        }
    }
}
