﻿namespace jp.co.fsi.kb.kbdr1031
{
    partial class KBDR1031
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtFunanushiCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblFunanushiCdFr = new System.Windows.Forms.Label();
            this.lblCodeBet = new System.Windows.Forms.Label();
            this.lblFunanushiCdTo = new System.Windows.Forms.Label();
            this.txtFunanushiCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblJpFr = new System.Windows.Forms.Label();
            this.lblJpTo = new System.Windows.Forms.Label();
            this.txtJpYearFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtJpYearTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.gbxSearchRangeDate = new System.Windows.Forms.GroupBox();
            this.lblJpDayFrTo = new System.Windows.Forms.Label();
            this.lblCodeBetDate = new System.Windows.Forms.Label();
            this.lblJpDayFrFr = new System.Windows.Forms.Label();
            this.lblJpMonthTo = new System.Windows.Forms.Label();
            this.lblJpMonthFr = new System.Windows.Forms.Label();
            this.lblJpearTo = new System.Windows.Forms.Label();
            this.txtJpDayTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtJpDayFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblJpearFr = new System.Windows.Forms.Label();
            this.txtJpMonthTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtJpMonthFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblEraBackTo = new System.Windows.Forms.Label();
            this.lblEraBackFr = new System.Windows.Forms.Label();
            this.gbxDisplayMethod = new System.Windows.Forms.GroupBox();
            this.rdo2 = new System.Windows.Forms.RadioButton();
            this.rdo1 = new System.Windows.Forms.RadioButton();
            this.gbxFunanushiCd = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMizuageShishoNm = new System.Windows.Forms.Label();
            this.lblMizuageShisho = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.gbxSearchRangeDate.SuspendLayout();
            this.gbxDisplayMethod.SuspendLayout();
            this.gbxFunanushiCd.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Text = "売上元帳問合せ";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.pnlDebug.Size = new System.Drawing.Size(847, 100);
            // 
            // txtFunanushiCdFr
            // 
            this.txtFunanushiCdFr.AutoSizeFromLength = false;
            this.txtFunanushiCdFr.DisplayLength = null;
            this.txtFunanushiCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFunanushiCdFr.Location = new System.Drawing.Point(19, 32);
            this.txtFunanushiCdFr.MaxLength = 4;
            this.txtFunanushiCdFr.Name = "txtFunanushiCdFr";
            this.txtFunanushiCdFr.Size = new System.Drawing.Size(40, 20);
            this.txtFunanushiCdFr.TabIndex = 9;
            this.txtFunanushiCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtCodeFr_Validating);
            // 
            // lblFunanushiCdFr
            // 
            this.lblFunanushiCdFr.BackColor = System.Drawing.Color.Silver;
            this.lblFunanushiCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFunanushiCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFunanushiCdFr.Location = new System.Drawing.Point(67, 32);
            this.lblFunanushiCdFr.Name = "lblFunanushiCdFr";
            this.lblFunanushiCdFr.Size = new System.Drawing.Size(300, 20);
            this.lblFunanushiCdFr.TabIndex = 1;
            this.lblFunanushiCdFr.Text = "先　頭";
            this.lblFunanushiCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCodeBet
            // 
            this.lblCodeBet.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblCodeBet.Location = new System.Drawing.Point(377, 31);
            this.lblCodeBet.Name = "lblCodeBet";
            this.lblCodeBet.Size = new System.Drawing.Size(18, 20);
            this.lblCodeBet.TabIndex = 2;
            this.lblCodeBet.Text = "～";
            this.lblCodeBet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblFunanushiCdTo
            // 
            this.lblFunanushiCdTo.BackColor = System.Drawing.Color.Silver;
            this.lblFunanushiCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFunanushiCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFunanushiCdTo.Location = new System.Drawing.Point(460, 32);
            this.lblFunanushiCdTo.Name = "lblFunanushiCdTo";
            this.lblFunanushiCdTo.Size = new System.Drawing.Size(300, 20);
            this.lblFunanushiCdTo.TabIndex = 4;
            this.lblFunanushiCdTo.Text = "最　後";
            this.lblFunanushiCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFunanushiCdTo
            // 
            this.txtFunanushiCdTo.AutoSizeFromLength = false;
            this.txtFunanushiCdTo.DisplayLength = null;
            this.txtFunanushiCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFunanushiCdTo.Location = new System.Drawing.Point(412, 32);
            this.txtFunanushiCdTo.MaxLength = 4;
            this.txtFunanushiCdTo.Name = "txtFunanushiCdTo";
            this.txtFunanushiCdTo.Size = new System.Drawing.Size(40, 20);
            this.txtFunanushiCdTo.TabIndex = 10;
            this.txtFunanushiCdTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtFunanushiCdTo_KeyDown);
            this.txtFunanushiCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtCodeTo_Validating);
            // 
            // lblJpFr
            // 
            this.lblJpFr.BackColor = System.Drawing.Color.Silver;
            this.lblJpFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblJpFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblJpFr.Location = new System.Drawing.Point(27, 30);
            this.lblJpFr.Name = "lblJpFr";
            this.lblJpFr.Size = new System.Drawing.Size(40, 20);
            this.lblJpFr.TabIndex = 0;
            this.lblJpFr.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblJpTo
            // 
            this.lblJpTo.BackColor = System.Drawing.Color.Silver;
            this.lblJpTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblJpTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblJpTo.Location = new System.Drawing.Point(316, 30);
            this.lblJpTo.Name = "lblJpTo";
            this.lblJpTo.Size = new System.Drawing.Size(40, 20);
            this.lblJpTo.TabIndex = 8;
            this.lblJpTo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtJpYearFr
            // 
            this.txtJpYearFr.AutoSizeFromLength = false;
            this.txtJpYearFr.DisplayLength = null;
            this.txtJpYearFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtJpYearFr.Location = new System.Drawing.Point(72, 30);
            this.txtJpYearFr.MaxLength = 2;
            this.txtJpYearFr.Name = "txtJpYearFr";
            this.txtJpYearFr.Size = new System.Drawing.Size(40, 20);
            this.txtJpYearFr.TabIndex = 1;
            this.txtJpYearFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtJpYearFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtYearFr_Validating);
            // 
            // txtJpYearTo
            // 
            this.txtJpYearTo.AutoSizeFromLength = false;
            this.txtJpYearTo.DisplayLength = null;
            this.txtJpYearTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtJpYearTo.Location = new System.Drawing.Point(361, 30);
            this.txtJpYearTo.MaxLength = 2;
            this.txtJpYearTo.Name = "txtJpYearTo";
            this.txtJpYearTo.Size = new System.Drawing.Size(40, 20);
            this.txtJpYearTo.TabIndex = 4;
            this.txtJpYearTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtJpYearTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtYearTo_Validating);
            // 
            // gbxSearchRangeDate
            // 
            this.gbxSearchRangeDate.Controls.Add(this.lblJpDayFrTo);
            this.gbxSearchRangeDate.Controls.Add(this.lblCodeBetDate);
            this.gbxSearchRangeDate.Controls.Add(this.lblJpDayFrFr);
            this.gbxSearchRangeDate.Controls.Add(this.lblJpMonthTo);
            this.gbxSearchRangeDate.Controls.Add(this.lblJpMonthFr);
            this.gbxSearchRangeDate.Controls.Add(this.lblJpearTo);
            this.gbxSearchRangeDate.Controls.Add(this.txtJpDayTo);
            this.gbxSearchRangeDate.Controls.Add(this.txtJpDayFr);
            this.gbxSearchRangeDate.Controls.Add(this.lblJpearFr);
            this.gbxSearchRangeDate.Controls.Add(this.txtJpMonthTo);
            this.gbxSearchRangeDate.Controls.Add(this.txtJpMonthFr);
            this.gbxSearchRangeDate.Controls.Add(this.txtJpYearTo);
            this.gbxSearchRangeDate.Controls.Add(this.txtJpYearFr);
            this.gbxSearchRangeDate.Controls.Add(this.lblJpTo);
            this.gbxSearchRangeDate.Controls.Add(this.lblJpFr);
            this.gbxSearchRangeDate.Controls.Add(this.lblEraBackTo);
            this.gbxSearchRangeDate.Controls.Add(this.lblEraBackFr);
            this.gbxSearchRangeDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxSearchRangeDate.ForeColor = System.Drawing.Color.Black;
            this.gbxSearchRangeDate.Location = new System.Drawing.Point(12, 147);
            this.gbxSearchRangeDate.Name = "gbxSearchRangeDate";
            this.gbxSearchRangeDate.Size = new System.Drawing.Size(596, 75);
            this.gbxSearchRangeDate.TabIndex = 2;
            this.gbxSearchRangeDate.TabStop = false;
            this.gbxSearchRangeDate.Text = "日付範囲";
            // 
            // lblJpDayFrTo
            // 
            this.lblJpDayFrTo.AutoSize = true;
            this.lblJpDayFrTo.BackColor = System.Drawing.Color.Silver;
            this.lblJpDayFrTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblJpDayFrTo.Location = new System.Drawing.Point(543, 34);
            this.lblJpDayFrTo.Name = "lblJpDayFrTo";
            this.lblJpDayFrTo.Size = new System.Drawing.Size(21, 13);
            this.lblJpDayFrTo.TabIndex = 14;
            this.lblJpDayFrTo.Text = "日";
            // 
            // lblCodeBetDate
            // 
            this.lblCodeBetDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblCodeBetDate.Location = new System.Drawing.Point(288, 29);
            this.lblCodeBetDate.Name = "lblCodeBetDate";
            this.lblCodeBetDate.Size = new System.Drawing.Size(18, 20);
            this.lblCodeBetDate.TabIndex = 7;
            this.lblCodeBetDate.Text = "～";
            this.lblCodeBetDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblJpDayFrFr
            // 
            this.lblJpDayFrFr.AutoSize = true;
            this.lblJpDayFrFr.BackColor = System.Drawing.Color.Silver;
            this.lblJpDayFrFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblJpDayFrFr.Location = new System.Drawing.Point(252, 34);
            this.lblJpDayFrFr.Name = "lblJpDayFrFr";
            this.lblJpDayFrFr.Size = new System.Drawing.Size(21, 13);
            this.lblJpDayFrFr.TabIndex = 6;
            this.lblJpDayFrFr.Text = "日";
            // 
            // lblJpMonthTo
            // 
            this.lblJpMonthTo.AutoSize = true;
            this.lblJpMonthTo.BackColor = System.Drawing.Color.Silver;
            this.lblJpMonthTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblJpMonthTo.Location = new System.Drawing.Point(475, 34);
            this.lblJpMonthTo.Name = "lblJpMonthTo";
            this.lblJpMonthTo.Size = new System.Drawing.Size(21, 13);
            this.lblJpMonthTo.TabIndex = 12;
            this.lblJpMonthTo.Text = "月";
            // 
            // lblJpMonthFr
            // 
            this.lblJpMonthFr.AutoSize = true;
            this.lblJpMonthFr.BackColor = System.Drawing.Color.Silver;
            this.lblJpMonthFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblJpMonthFr.Location = new System.Drawing.Point(184, 34);
            this.lblJpMonthFr.Name = "lblJpMonthFr";
            this.lblJpMonthFr.Size = new System.Drawing.Size(21, 13);
            this.lblJpMonthFr.TabIndex = 4;
            this.lblJpMonthFr.Text = "月";
            // 
            // lblJpearTo
            // 
            this.lblJpearTo.AutoSize = true;
            this.lblJpearTo.BackColor = System.Drawing.Color.Silver;
            this.lblJpearTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblJpearTo.Location = new System.Drawing.Point(407, 34);
            this.lblJpearTo.Name = "lblJpearTo";
            this.lblJpearTo.Size = new System.Drawing.Size(21, 13);
            this.lblJpearTo.TabIndex = 10;
            this.lblJpearTo.Text = "年";
            // 
            // txtJpDayTo
            // 
            this.txtJpDayTo.AutoSizeFromLength = false;
            this.txtJpDayTo.DisplayLength = null;
            this.txtJpDayTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtJpDayTo.Location = new System.Drawing.Point(497, 30);
            this.txtJpDayTo.MaxLength = 2;
            this.txtJpDayTo.Name = "txtJpDayTo";
            this.txtJpDayTo.Size = new System.Drawing.Size(40, 20);
            this.txtJpDayTo.TabIndex = 6;
            this.txtJpDayTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtJpDayTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDayTo_Validating);
            // 
            // txtJpDayFr
            // 
            this.txtJpDayFr.AutoSizeFromLength = false;
            this.txtJpDayFr.DisplayLength = null;
            this.txtJpDayFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtJpDayFr.Location = new System.Drawing.Point(206, 30);
            this.txtJpDayFr.MaxLength = 2;
            this.txtJpDayFr.Name = "txtJpDayFr";
            this.txtJpDayFr.Size = new System.Drawing.Size(40, 20);
            this.txtJpDayFr.TabIndex = 3;
            this.txtJpDayFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtJpDayFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDayFr_Validating);
            // 
            // lblJpearFr
            // 
            this.lblJpearFr.AutoSize = true;
            this.lblJpearFr.BackColor = System.Drawing.Color.Silver;
            this.lblJpearFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblJpearFr.Location = new System.Drawing.Point(117, 34);
            this.lblJpearFr.Name = "lblJpearFr";
            this.lblJpearFr.Size = new System.Drawing.Size(21, 13);
            this.lblJpearFr.TabIndex = 2;
            this.lblJpearFr.Text = "年";
            // 
            // txtJpMonthTo
            // 
            this.txtJpMonthTo.AutoSizeFromLength = false;
            this.txtJpMonthTo.DisplayLength = null;
            this.txtJpMonthTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtJpMonthTo.Location = new System.Drawing.Point(429, 30);
            this.txtJpMonthTo.MaxLength = 2;
            this.txtJpMonthTo.Name = "txtJpMonthTo";
            this.txtJpMonthTo.Size = new System.Drawing.Size(40, 20);
            this.txtJpMonthTo.TabIndex = 5;
            this.txtJpMonthTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtJpMonthTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthTo_Validating);
            // 
            // txtJpMonthFr
            // 
            this.txtJpMonthFr.AutoSizeFromLength = false;
            this.txtJpMonthFr.DisplayLength = null;
            this.txtJpMonthFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtJpMonthFr.Location = new System.Drawing.Point(139, 30);
            this.txtJpMonthFr.MaxLength = 2;
            this.txtJpMonthFr.Name = "txtJpMonthFr";
            this.txtJpMonthFr.Size = new System.Drawing.Size(40, 20);
            this.txtJpMonthFr.TabIndex = 2;
            this.txtJpMonthFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtJpMonthFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthFr_Validating);
            // 
            // lblEraBackTo
            // 
            this.lblEraBackTo.BackColor = System.Drawing.Color.Silver;
            this.lblEraBackTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblEraBackTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblEraBackTo.Location = new System.Drawing.Point(313, 28);
            this.lblEraBackTo.Name = "lblEraBackTo";
            this.lblEraBackTo.Size = new System.Drawing.Size(258, 25);
            this.lblEraBackTo.TabIndex = 905;
            this.lblEraBackTo.Text = " ";
            // 
            // lblEraBackFr
            // 
            this.lblEraBackFr.BackColor = System.Drawing.Color.Silver;
            this.lblEraBackFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblEraBackFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblEraBackFr.Location = new System.Drawing.Point(24, 28);
            this.lblEraBackFr.Name = "lblEraBackFr";
            this.lblEraBackFr.Size = new System.Drawing.Size(258, 25);
            this.lblEraBackFr.TabIndex = 906;
            this.lblEraBackFr.Text = " ";
            // 
            // gbxDisplayMethod
            // 
            this.gbxDisplayMethod.Controls.Add(this.rdo2);
            this.gbxDisplayMethod.Controls.Add(this.rdo1);
            this.gbxDisplayMethod.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxDisplayMethod.ForeColor = System.Drawing.Color.Black;
            this.gbxDisplayMethod.Location = new System.Drawing.Point(622, 147);
            this.gbxDisplayMethod.Name = "gbxDisplayMethod";
            this.gbxDisplayMethod.Size = new System.Drawing.Size(175, 75);
            this.gbxDisplayMethod.TabIndex = 4;
            this.gbxDisplayMethod.TabStop = false;
            this.gbxDisplayMethod.Text = "表示方法";
            // 
            // rdo2
            // 
            this.rdo2.AutoSize = true;
            this.rdo2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdo2.Location = new System.Drawing.Point(12, 47);
            this.rdo2.Name = "rdo2";
            this.rdo2.Size = new System.Drawing.Size(81, 17);
            this.rdo2.TabIndex = 8;
            this.rdo2.Text = "伝票合計";
            this.rdo2.UseVisualStyleBackColor = true;
            // 
            // rdo1
            // 
            this.rdo1.AutoSize = true;
            this.rdo1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdo1.Location = new System.Drawing.Point(12, 20);
            this.rdo1.Name = "rdo1";
            this.rdo1.Size = new System.Drawing.Size(81, 17);
            this.rdo1.TabIndex = 7;
            this.rdo1.Text = "伝票明細";
            this.rdo1.UseVisualStyleBackColor = true;
            // 
            // gbxFunanushiCd
            // 
            this.gbxFunanushiCd.Controls.Add(this.lblFunanushiCdTo);
            this.gbxFunanushiCd.Controls.Add(this.lblCodeBet);
            this.gbxFunanushiCd.Controls.Add(this.txtFunanushiCdFr);
            this.gbxFunanushiCd.Controls.Add(this.lblFunanushiCdFr);
            this.gbxFunanushiCd.Controls.Add(this.txtFunanushiCdTo);
            this.gbxFunanushiCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxFunanushiCd.ForeColor = System.Drawing.Color.Black;
            this.gbxFunanushiCd.Location = new System.Drawing.Point(12, 238);
            this.gbxFunanushiCd.Name = "gbxFunanushiCd";
            this.gbxFunanushiCd.Size = new System.Drawing.Size(785, 75);
            this.gbxFunanushiCd.TabIndex = 3;
            this.gbxFunanushiCd.TabStop = false;
            this.gbxFunanushiCd.Text = "船主コード範囲";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtMizuageShishoCd);
            this.groupBox1.Controls.Add(this.lblMizuageShishoNm);
            this.groupBox1.Controls.Add(this.lblMizuageShisho);
            this.groupBox1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBox1.ForeColor = System.Drawing.Color.Black;
            this.groupBox1.Location = new System.Drawing.Point(12, 49);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(372, 83);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "支所";
            // 
            // txtMizuageShishoCd
            // 
            this.txtMizuageShishoCd.AutoSizeFromLength = true;
            this.txtMizuageShishoCd.DisplayLength = null;
            this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtMizuageShishoCd.Location = new System.Drawing.Point(68, 31);
            this.txtMizuageShishoCd.MaxLength = 4;
            this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
            this.txtMizuageShishoCd.Size = new System.Drawing.Size(34, 20);
            this.txtMizuageShishoCd.TabIndex = 1;
            this.txtMizuageShishoCd.TabStop = false;
            this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
            // 
            // lblMizuageShishoNm
            // 
            this.lblMizuageShishoNm.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMizuageShishoNm.Location = new System.Drawing.Point(102, 32);
            this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
            this.lblMizuageShishoNm.Size = new System.Drawing.Size(212, 20);
            this.lblMizuageShishoNm.TabIndex = 2;
            this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMizuageShisho
            // 
            this.lblMizuageShisho.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShisho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShisho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblMizuageShisho.Location = new System.Drawing.Point(26, 29);
            this.lblMizuageShisho.Name = "lblMizuageShisho";
            this.lblMizuageShisho.Size = new System.Drawing.Size(294, 25);
            this.lblMizuageShisho.TabIndex = 3;
            this.lblMizuageShisho.Text = "支所";
            this.lblMizuageShisho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // KBDR1031
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 638);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.gbxSearchRangeDate);
            this.Controls.Add(this.gbxDisplayMethod);
            this.Controls.Add(this.gbxFunanushiCd);
            this.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "KBDR1031";
            this.Par1 = "1";
            this.Text = "売上元帳問合せ";
            this.Controls.SetChildIndex(this.gbxFunanushiCd, 0);
            this.Controls.SetChildIndex(this.gbxDisplayMethod, 0);
            this.Controls.SetChildIndex(this.gbxSearchRangeDate, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.pnlDebug.ResumeLayout(false);
            this.gbxSearchRangeDate.ResumeLayout(false);
            this.gbxSearchRangeDate.PerformLayout();
            this.gbxDisplayMethod.ResumeLayout(false);
            this.gbxDisplayMethod.PerformLayout();
            this.gbxFunanushiCd.ResumeLayout(false);
            this.gbxFunanushiCd.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private jp.co.fsi.common.controls.FsiTextBox txtFunanushiCdFr;
        private System.Windows.Forms.Label lblFunanushiCdFr;
        private System.Windows.Forms.Label lblCodeBet;
        private System.Windows.Forms.Label lblFunanushiCdTo;
        private jp.co.fsi.common.controls.FsiTextBox txtFunanushiCdTo;
        private System.Windows.Forms.Label lblJpFr;
        private System.Windows.Forms.Label lblJpTo;
        private jp.co.fsi.common.controls.FsiTextBox txtJpYearFr;
        private jp.co.fsi.common.controls.FsiTextBox txtJpYearTo;
        private System.Windows.Forms.GroupBox gbxSearchRangeDate;
        private jp.co.fsi.common.controls.FsiTextBox txtJpDayTo;
        private jp.co.fsi.common.controls.FsiTextBox txtJpDayFr;
        private jp.co.fsi.common.controls.FsiTextBox txtJpMonthTo;
        private jp.co.fsi.common.controls.FsiTextBox txtJpMonthFr;
        private System.Windows.Forms.Label lblJpMonthTo;
        private System.Windows.Forms.Label lblJpMonthFr;
        private System.Windows.Forms.Label lblJpearTo;
        private System.Windows.Forms.Label lblJpearFr;
        private System.Windows.Forms.Label lblJpDayFrFr;
        private System.Windows.Forms.Label lblJpDayFrTo;
        private System.Windows.Forms.Label lblCodeBetDate;
        private System.Windows.Forms.GroupBox gbxDisplayMethod;
        private System.Windows.Forms.RadioButton rdo2;
        private System.Windows.Forms.RadioButton rdo1;
        private System.Windows.Forms.GroupBox gbxFunanushiCd;
        private System.Windows.Forms.Label lblEraBackTo;
        private System.Windows.Forms.Label lblEraBackFr;
        private System.Windows.Forms.GroupBox groupBox1;
        private common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private System.Windows.Forms.Label lblMizuageShisho;
    }
}