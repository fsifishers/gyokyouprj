﻿using System;
using System.ComponentModel;
using System.Data;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using System.Collections.Generic;

using GrapeCity.ActiveReports;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;
using jp.co.fsi.common.constants;

namespace jp.co.fsi.kb.kbdr1031
{
    #region 構造体
    /// <summary>
    /// 印刷ワークテーブルのデータ構造体
    /// </summary>
    struct NumVals
    {
        public decimal URIAGEGAKU;
        public decimal SHOHIZEIGAKU;
        public decimal NYUKINGAKU;
        public decimal ZANDAKA;
        public decimal KOBAI;
        public decimal KORI_ESA;
        public decimal GENKIN;

        public void Clear()
        {
            URIAGEGAKU = 0;
            SHOHIZEIGAKU = 0;
            NYUKINGAKU = 0;
            ZANDAKA = 0;
            KOBAI = 0;
            KORI_ESA = 0;
            GENKIN = 0;
        }
    }

    /// <summary>
    /// 印刷ヘッダ用データ構造体
    /// </summary>
    struct PageHead
    {
        public string ToriCd;           //取引先コード
        public string ToriNm;           //取引先名
        public string Tel;              //電話番号
        public string Fax;              //FAX番号
        public string DateFr;           //開始年月日
        public string DateTo;           //終了年月日
        public string PrintDate;        //印刷日付
        public string ReportTitle;      //帳票タイトル
        public string KaishaNm;         //会社名

        public void Clear()
        {
            ToriCd = "";
            ToriNm = "";
            Tel = "";
            Fax = "";
        }

        public void AllClear()
        {
            ToriCd = "";
            ToriNm = "";
            Tel = "";
            Fax = "";
            DateFr = "";
            DateTo = "";
            PrintDate = "";
            ReportTitle = "";
            KaishaNm = "";
        }
    }

    struct DenHead
    {
        public string DenpyoDate;
        public string DenpyoMonth;
        public string DenpyoBango;
        public int TorihikiKbn;
        public string TorihikiKbnNm;

        public void Clear()
        {
            DenpyoDate = "";
            DenpyoMonth = "";
            DenpyoBango = "";
            TorihikiKbn = 0;
            TorihikiKbnNm = "";
        }
    }

    #endregion

    /// <summary>
    /// 売上元帳(KBDR1031)
    /// </summary>
    public partial class KBDR1031 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// 印刷テーブル使用列数
        /// </summary>
        public const int prtCols = 24;

        /// <summary>
        /// 財務データ取引区分
        /// </summary>
        public const int ZamToriKbn = 9;

        /// <summary>
        /// レポート最大行数
        /// </summary>
        public const int MeiMultiple = 36;
        #endregion

        #region プロパティ
        /// <summary>
        /// 画面上最後となるフォーカスのEnterボタン押下時処理用変数
        /// </summary>
        private bool _dtFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._dtFlg;
            }
        }

        private decimal Motocho_kbn = 1;
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KBDR1031()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region メソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.InitForm();は呼び出さなくて構いません。
        /// また、このメソッド内の処理を外出しでこのクラス内にメソッド化するのは構いませんが、
        /// 原則、独自で起動時のイベント処理を実装することは禁じます。
        /// </remarks>
        protected override void InitForm()
        {
            // 水揚支所
#if DEBUG
            this.txtMizuageShishoCd.Text = "1";
#else
            this.txtMizuageShishoCd.Text = Uinfo.shishoCd;
#endif
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
            this.txtMizuageShishoCd.Enabled = (this.txtMizuageShishoCd.Text == "1") ? true : false;

            // 現在の年号を取得する
            string[] jpDate = Util.ConvJpDate(DateTime.Now, this.Dba);
            //今月の最初の日を取得する
            DateTime dtMonthFr = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            string[] jpMontFr = Util.ConvJpDate(dtMonthFr, this.Dba);
            // 日付範囲(自)
            this.lblJpFr.Text = Util.ToString(jpMontFr[0]);
            this.txtJpYearFr.Text = jpMontFr[2];
            this.txtJpMonthFr.Text = jpMontFr[3];
            this.txtJpDayFr.Text = jpMontFr[4];
            // 日付範囲(至)
            this.lblJpTo.Text = Util.ToString(jpDate[0]);
            this.txtJpYearTo.Text = jpDate[2];
            this.txtJpMonthTo.Text = jpDate[3];
            this.txtJpDayTo.Text = jpDate[4];

            //元帳区分を引数から設定する
            Motocho_kbn = Util.ToDecimal(this.Par1);

            // 初期フォーカス設定
            this.txtJpYearFr.Focus();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 年度1・2、仕入先コード1・2に
            // フォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd":
                case "txtJpYearFr":
                case "txtJpYearTo":
                case "txtFunanushiCdFr":
                case "txtFunanushiCdTo":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;
            String[] result;
            string stCurrentDir = System.IO.Directory.GetCurrentDirectory();

            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd": // 水揚支所
                    #region 水揚支所
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM2031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2031.CMCM2031");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.InData = this.txtMizuageShishoCd.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.txtMizuageShishoCd.Text = result[0];
                                this.lblMizuageShishoNm.Text = result[1];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtFunanushiCdFr":
                    #region 船主CD
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM2011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2011.CMCM2011");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtFunanushiCdFr.Text = outData[0];
                                this.lblFunanushiCdFr.Text = outData[1];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtFunanushiCdTo":
                    #region 船主CD
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM2011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2011.CMCM2011");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtFunanushiCdTo.Text = outData[0];
                                this.lblFunanushiCdTo.Text = outData[1];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtJpYearFr":
                    #region 元号
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblJpFr.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (string[])frm.OutData;
                                this.lblJpFr.Text = result[1];

                                // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
                                DateTime tmpDate = Util.ConvAdDate(this.lblJpFr.Text, this.txtJpYearFr.Text,
                                    this.txtJpMonthFr.Text, "1", this.Dba);
                                int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

                                if (Util.ToInt(this.txtJpDayFr.Text) > lastDayInMonth)
                                {
                                    this.txtJpDayFr.Text = Util.ToString(lastDayInMonth);
                                }

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblJpFr.Text,
                                        this.txtJpYearFr.Text,
                                        this.txtJpMonthFr.Text,
                                        this.txtJpDayFr.Text,
                                        this.Dba);
                                this.lblJpFr.Text = arrJpDate[0];
                                this.txtJpYearFr.Text = arrJpDate[2];
                                this.txtJpMonthFr.Text = arrJpDate[3];
                                this.txtJpDayFr.Text = arrJpDate[4];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtJpYearTo":
                    #region 元号
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblJpTo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (string[])frm.OutData;
                                this.lblJpTo.Text = result[1];

                                // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
                                DateTime tmpDate = Util.ConvAdDate(this.lblJpTo.Text, this.txtJpYearTo.Text,
                                    this.txtJpMonthTo.Text, "1", this.Dba);
                                int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

                                if (Util.ToInt(this.txtJpDayTo.Text) > lastDayInMonth)
                                {
                                    this.txtJpDayTo.Text = Util.ToString(lastDayInMonth);
                                }

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblJpTo.Text,
                                        this.txtJpYearTo.Text,
                                        this.txtJpMonthTo.Text,
                                        this.txtJpDayTo.Text,
                                        this.Dba);
                                this.lblJpTo.Text = arrJpDate[0];
                                this.txtJpYearTo.Text = arrJpDate[2];
                                this.txtJpMonthTo.Text = arrJpDate[3];
                                this.txtJpDayTo.Text = arrJpDate[4];
                            }
                        }
                    }
                    #endregion
                    break;
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
            {
                // プレビュー処理
                DoPrint(true);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("印刷", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }

        /// <summary>
        /// F6キー押下時処理
        /// PDF出力
        /// </summary>
        public override void PressF6()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("PDF出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, true);
            }
        }

        /// <summary>
        /// F7キー押下時処理
        /// EXCEL出力
        /// </summary>
        public override void PressF7()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("EXCEL出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, false, true);
            }
        }

        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            // 設定画面の起動
            // MEMO:原則としてここで渡す帳票IDの設定はReport.csvに保持していることが前提ですが、
            // 保持していない場合は、設定画面での保存(F6)時に新規に設定が保持されます。
            PrintSettingForm psForm = new PrintSettingForm(new string[2] { "KBDR1031R", "KOBR2032R" });
            psForm.ShowDialog();
        }

        #endregion

        #region イベント
        /// <summary>
        /// 水揚支所入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMizuageShishoCd_Validating(object sender, CancelEventArgs e)
        {
            if (!this.IsValidMizuageShishoCd())
            {
                e.Cancel = true;

                this.lblMizuageShishoNm.Text = "";
                this.txtMizuageShishoCd.SelectAll();
                this.txtMizuageShishoCd.Focus();
            }
        }

        /// <summary>
        /// 年(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtYearFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidYearFr())
            {
                e.Cancel = true;
                this.txtJpYearFr.SelectAll();
            }
            else
            {
                CheckJpDateFr();
                SetJpDateFr();
            }
        }
        /// <summary>
        /// 月(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMonthFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidMonthFr())
            {
                e.Cancel = true;
                this.txtJpMonthFr.SelectAll();
            }
            else
            {
                CheckJpDateFr();
                SetJpDateFr();
            }
        }
        /// <summary>
        /// 日(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDayFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidDayFr())
            {
                e.Cancel = true;
                this.txtJpDayFr.SelectAll();
            }
            else
            {
                CheckJpDateFr();
                SetJpDateFr();
            }
        }
        /// <summary>
        /// 年(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtYearTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidYearTo())
            {
                e.Cancel = true;
                this.txtJpYearTo.SelectAll();
            }
            else
            {
                CheckJpDateTo();
                SetJpDateTo();
            }
        }
        /// <summary>
        /// 月(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMonthTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidMonthTo())
            {
                e.Cancel = true;
                this.txtJpMonthTo.SelectAll();
            }
            else
            {
                CheckJpDateTo();
                SetJpDateTo();
            }
        }
        /// <summary>
        /// 日(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDayTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidDayTo())
            {
                e.Cancel = true;
                this.txtJpDayTo.SelectAll();
            }
            else
            {
                CheckJpDateTo();
                SetJpDateTo();
            }
        }
        /// <summary>
        /// 船主コード(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCodeFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidCodeFr())
            {
                e.Cancel = true;
                this.txtFunanushiCdFr.SelectAll();
            }
        }

        /// <summary>
        /// 船主コード(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCodeTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidCodeTo())
            {
                e.Cancel = true;
                this.txtFunanushiCdTo.SelectAll();

                // Enter処理を無効化
                this._dtFlg = false;
            }
            else
            {
                // Enter処理を有効化
                this._dtFlg = true;
            }
        }

        /// <summary>
        /// 船主CD(至)のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFunanushiCdTo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.Flg)
            {
                // Enter処理を無効化
                this._dtFlg = false;

                // 全項目を再度入力値チェック
                if (!ValidateAll())
                {
                    // エラーありの場合ここで処理終了
                    return;
                }

                if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
                {
                    // ﾌﾟﾚﾋﾞｭｰ処理
                    DoPrint(true);
                }
                else
                {
                    this.txtFunanushiCdTo.Focus();
                }
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 水揚支所の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool IsValidMizuageShishoCd()
        {
            // 空入力の場合
            if (ValChk.IsEmpty(this.txtMizuageShishoCd.Text))
            {
                // 水揚支所名称を表示する
                this.txtMizuageShishoCd.Text = "0";
                this.lblMizuageShishoNm.Text = "全て";
                return true;
            }

            // 最大桁数チェック
            if (!ValChk.IsWithinLength(this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.MaxLength))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            // 数値チェック
            if (!ValChk.IsNumber(this.txtMizuageShishoCd.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                return false;
            }

            // 0入力の場合
            if (Equals(this.txtMizuageShishoCd.Text, "0"))
            {
                // 水揚支所名称を表示する
                this.lblMizuageShishoNm.Text = "全て";
                return true;
            }

            // 水揚支所名称を表示する
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);

            if (ValChk.IsEmpty(this.lblMizuageShishoNm.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 年(自)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidYearFr()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtJpYearFr.Text))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }
            // 空の場合、0年として処理
            if (ValChk.IsEmpty(this.txtJpYearFr.Text))
            {
                this.txtJpYearFr.Text = "0";
            }

            return true;
        }

        /// <summary>
        /// 月(自)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidMonthFr()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtJpMonthFr.Text))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }
            // 空の場合、1月として処理
            if (ValChk.IsEmpty(this.txtJpMonthFr.Text))
            {
                this.txtJpMonthFr.Text = "1";
            }
            else
            {
                // 12を超える月が入力された場合、12月として処理
                if (Util.ToInt(this.txtJpMonthFr.Text) > 12)
                {
                    this.txtJpMonthFr.Text = "12";
                }
                // 1より小さい月が入力された場合、1月として処理
                else if (Util.ToInt(this.txtJpMonthFr.Text) < 1)
                {
                    this.txtJpMonthFr.Text = "1";
                }
            }

            return true;
        }

        /// <summary>
        /// 日(自)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidDayFr()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtJpDayFr.Text))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }

            if (ValChk.IsEmpty(this.txtJpDayFr.Text))
            {
                // 空の場合、1日として処理
                this.txtJpDayFr.Text = "1";
            }
            else
            {
                // 1より小さい日が入力された場合、1日として処理
                if (Util.ToInt(this.txtJpDayFr.Text) < 1)
                {
                    this.txtJpDayFr.Text = "1";
                }
            }

            return true;
        }

        /// <summary>
        /// 年月日(自)の月末入力チェック
        /// </summary>
        /// 
        private void CheckJpDateFr()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblJpFr.Text, this.txtJpYearFr.Text,
                this.txtJpMonthFr.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtJpDayFr.Text) > lastDayInMonth)
            {
                this.txtJpDayFr.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(自)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetJpDateFr()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDateFr(Util.FixJpDate(this.lblJpFr.Text, this.txtJpYearFr.Text,
                this.txtJpMonthFr.Text, this.txtJpDayFr.Text, this.Dba));
        }

        /// <summary>
        /// 年(至)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidYearTo()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtJpYearTo.Text))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }
            // 空の場合、0年として処理
            if (ValChk.IsEmpty(this.txtJpYearTo.Text))
            {
                this.txtJpYearTo.Text = "0";
            }

            return true;
        }

        /// <summary>
        /// 月(至)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidMonthTo()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtJpMonthTo.Text))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }
            // 空の場合、1月として処理
            if (ValChk.IsEmpty(this.txtJpMonthTo.Text))
            {
                this.txtJpMonthTo.Text = "1";
            }
            else
            {
                // 12を超える月が入力された場合、12月として処理
                if (Util.ToInt(this.txtJpMonthTo.Text) > 12)
                {
                    this.txtJpMonthTo.Text = "12";
                }
                // 1より小さい月が入力された場合、1月として処理
                else if (Util.ToInt(this.txtJpMonthTo.Text) < 1)
                {
                    this.txtJpMonthTo.Text = "1";
                }
            }

            return true;
        }

        /// <summary>
        /// 日(至)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidDayTo()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtJpDayTo.Text))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }

            if (ValChk.IsEmpty(this.txtJpDayTo.Text))
            {
                // 空の場合、1日として処理
                this.txtJpDayTo.Text = "1";
            }
            else
            {
                // 1より小さい日が入力された場合、1日として処理
                if (Util.ToInt(this.txtJpDayTo.Text) < 1)
                {
                    this.txtJpDayTo.Text = "1";
                }
            }

            return true;
        }

        /// <summary>
        /// 年月日(至)の月末入力チェック
        /// </summary>
        /// 
        private void CheckJpDateTo()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblJpTo.Text, this.txtJpYearTo.Text,
                this.txtJpMonthTo.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtJpDayTo.Text) > lastDayInMonth)
            {
                this.txtJpDayTo.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(至)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetJpDateTo()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDateTo(Util.FixJpDate(this.lblJpTo.Text, this.txtJpYearTo.Text,
                this.txtJpMonthTo.Text, this.txtJpDayTo.Text, this.Dba));
        }

        /// <summary>
        /// 船主コード(自)の入力チェック
        /// </summary>
        private bool IsValidCodeFr()
        {
            if (ValChk.IsEmpty(this.txtFunanushiCdFr.Text))
            {
                this.lblFunanushiCdFr.Text = "先　頭";
            }
            else
                // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
                if (!ValChk.IsNumber(this.txtFunanushiCdFr.Text))
                {
                    Msg.Notice("コード(自)は数値のみで入力してください。");
                    return false;
                }
                else
                {
                    // コードを元に名称を取得する
                    this.lblFunanushiCdFr.Text = this.Dba.GetName(this.UInfo, "VI_HN_FUNANUSHI", this.txtMizuageShishoCd.Text, this.txtFunanushiCdFr.Text);
                }

            return true;
        }

        /// <summary>
        /// 船主コード(至)の入力チェック
        /// </summary>
        private bool IsValidCodeTo()
        {
            if (ValChk.IsEmpty(this.txtFunanushiCdTo.Text))
            {
                this.lblFunanushiCdTo.Text = "最　後";
            }
            else
                // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
                if (!ValChk.IsNumber(this.txtFunanushiCdTo.Text))
                {
                    Msg.Notice("コード(至)は数値のみで入力してください。");
                    return false;
                }
                else
                {
                    // コードを元に名称を取得する
                    this.lblFunanushiCdTo.Text = this.Dba.GetName(this.UInfo, "VI_HN_FUNANUSHI", this.txtMizuageShishoCd.Text, this.txtFunanushiCdTo.Text);
                }

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 年(自)のチェック
            if (!IsValidYearFr())
            {
                this.txtJpYearFr.Focus();
                this.txtJpYearFr.SelectAll();
                return false;
            }
            // 月(自)のチェック
            if (!IsValidMonthFr())
            {
                this.txtJpMonthFr.Focus();
                this.txtJpMonthFr.SelectAll();
                return false;
            }
            // 日(自)のチェック
            if (!IsValidDayFr())
            {
                this.txtJpDayFr.Focus();
                this.txtJpDayFr.SelectAll();
                return false;
            }
            // 年月日(自)の月末入力チェック処理
            CheckJpDateFr();
            // 年月日(自)の正しい和暦への変換処理
            SetJpDateFr();

            // 年(至)のチェック
            if (!IsValidYearTo())
            {
                this.txtJpYearTo.Focus();
                this.txtJpYearTo.SelectAll();
                return false;
            }
            // 月(至)のチェック
            if (!IsValidMonthTo())
            {
                this.txtJpMonthTo.Focus();
                this.txtJpMonthTo.SelectAll();
                return false;
            }
            // 日(至)のチェック
            if (!IsValidDayTo())
            {
                this.txtJpDayTo.Focus();
                this.txtJpDayTo.SelectAll();
                return false;
            }
            // 年月日(至)の月末入力チェック処理
            CheckJpDateTo();
            // 年月日(至)の正しい和暦への変換処理
            SetJpDateTo();

            // 船主コード(自)の入力チェック
            if (!IsValidCodeFr())
            {
                this.txtFunanushiCdFr.Focus();
                this.txtFunanushiCdFr.SelectAll();
                return false;
            }
            // 船主コード(至)の入力チェック
            if (!IsValidCodeTo())
            {
                this.txtFunanushiCdTo.Focus();
                this.txtFunanushiCdTo.SelectAll();
                return false;
            }

            return true;
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpDateFr(string[] arrJpDate)
        {
            this.lblJpFr.Text = arrJpDate[0];
            this.txtJpYearFr.Text = arrJpDate[2];
            this.txtJpMonthFr.Text = arrJpDate[3];
            this.txtJpDayFr.Text = arrJpDate[4];
        }
        private void SetJpDateTo(string[] arrJpDate)
        {
            this.lblJpTo.Text = arrJpDate[0];
            this.txtJpYearTo.Text = arrJpDate[2];
            this.txtJpMonthTo.Text = arrJpDate[3];
            this.txtJpDayTo.Text = arrJpDate[4];
        }

        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview, bool isPdf = false, bool isExcel = false, bool isCsv = false)
        {
            try
            {
#if DEBUG
                //印刷用ワークテーブルの削除
                this.Dba.DeleteWork("PR_HN_TBL", this.UnqId);
#endif

                bool dataFlag;

                this.Dba.BeginTransaction();

                // 帳票出力用にワークテーブルにデータを作成
                if (rdo1.Checked) //明細選択時
                {
                    dataFlag = MakeWkData01();
                }
                else //合計選択時
                {
                    dataFlag = MakeWkData02();
                }

                // 帳票出力
                if (dataFlag)
                {
                    // 取得列の定義
                    StringBuilder cols = Util.ColsArray(prtCols, "");

                    // バインドパラメータの設定
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);

                    // データの取得
                    DataTable dtOutput = this.Dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_HN_TBL", "GUID = @GUID", "SORT ASC", dpc);

                    // 帳票オブジェクトをインスタンス化
                    jp.co.fsi.common.report.BaseReport rpt;
                    string subTitle;
                    if (rdo1.Checked) //明細選択時
                    {
                        rpt = new KBDR1031R(dtOutput);
                        subTitle = " (伝票明細)";
                    }
                    else
                    {
                        rpt = new KBDR1032R(dtOutput);
                        subTitle = " (伝票合計)";
                    }
                    rpt.Document.Printer.DocumentName = this.lblTitle.Text + subTitle;
                    rpt.Document.Name = this.lblTitle.Text + subTitle;

                    if (isExcel)
                    {
                        GrapeCity.ActiveReports.Export.Excel.Section.XlsExport xlsExport1 = new GrapeCity.ActiveReports.Export.Excel.Section.XlsExport();
                        //SetExcelSetting(xlsExport1);
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Kob, rpt.Document.Name, 2);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            xlsExport1.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("EXCEL出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPdf)
                    {
                        GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport p = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Kob, rpt.Document.Name, 1);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            p.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("PDF出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPreview)
                    {
                        // プレビュー画面表示
                        PreviewForm pFrm = new PreviewForm(rpt, this.UnqId);
                        pFrm.WindowState = FormWindowState.Maximized;
                        pFrm.Show();
                    }
                    else
                    {
                        // 直接印刷
                        rpt.Run(false);
                        rpt.Document.Print(true, true, false);
                    }


                }
            }
            finally
            {
#if DEBUG
                this.Dba.Commit();
#else
                this.Dba.Rollback();
#endif
            }
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData01()
        {
            #region データ取得準備
            // 入力された情報を元にワークテーブルに更新をする
            DbParamCollection dpc = new DbParamCollection();
            // 日付範囲を西暦にして取得
            DateTime tmpDateFr = Util.ConvAdDate(this.lblJpFr.Text, this.txtJpYearFr.Text,
                    this.txtJpMonthFr.Text, this.txtJpDayFr.Text, this.Dba);
            DateTime tmpDateTo = Util.ConvAdDate(this.lblJpTo.Text, this.txtJpYearTo.Text,
                    this.txtJpMonthTo.Text, this.txtJpDayTo.Text, this.Dba);
            //今月の最後の日
            DateTime tmpLastDay = Util.ConvAdDate(this.lblJpTo.Text, this.txtJpYearTo.Text,
                    this.txtJpMonthTo.Text, Util.ToString(DateTime.DaysInMonth(Util.ToInt(this.txtJpYearTo.Text), Util.ToInt(this.txtJpMonthTo.Text))), this.Dba);

            // 日付範囲を和暦で保持
            string[] tmpjpDateFr = Util.ConvJpDate(tmpDateFr, this.Dba);
            string[] tmpjpDateTo = Util.ConvJpDate(tmpDateTo, this.Dba);

            // 日付表示形式を判断
            int dataHyojiFlag = 0; // 表示日付用変数
            // 入力開始日付が1日でない場合
            if (tmpDateFr.Day != 1)
            {
                dataHyojiFlag = 1;
            }
            // 入力終了日付が、その月の最終日でない場合
            else if (tmpDateTo != tmpLastDay)
            {
                dataHyojiFlag = 1;
            }
            // 入力開始日付と入力終了日付の年又は月が一致でない場合
            if (tmpDateFr.Year != tmpDateTo.Year || tmpDateFr.Month != tmpDateTo.Month)
            {
                dataHyojiFlag = 1;
            }

            string hyojiDate; // 表示用日付            
            if (dataHyojiFlag == 0)
            {
                hyojiDate = string.Format("{0}{1}年{2}月", tmpjpDateFr[0], tmpjpDateFr[2], tmpjpDateFr[3]) + "度";
            }
            else
            {
                hyojiDate = string.Format("{0}{1}年{2}月{3}日", tmpjpDateFr[0], tmpjpDateFr[2], tmpjpDateFr[3], tmpjpDateFr[4])
                          + "～"
                          + string.Format("{0}{1}年{2}月{3}日", tmpjpDateTo[0], tmpjpDateTo[2], tmpjpDateTo[3], tmpjpDateTo[4]);
            }

            // 船主コード設定
            String FUNANUSHI_CD_FR;
            String FUNANUSHI_CD_TO;
            if (Util.ToDecimal(txtFunanushiCdFr.Text) > 0)
            {
                FUNANUSHI_CD_FR = txtFunanushiCdFr.Text;
            }
            else
            {
                FUNANUSHI_CD_FR = "0";
            }
            if (Util.ToDecimal(txtFunanushiCdTo.Text) > 0)
            {
                FUNANUSHI_CD_TO = txtFunanushiCdTo.Text;
            }
            else
            {
                FUNANUSHI_CD_TO = "9999";
            }
            // 会計年度設定
            int kaikeiNendo;
            int kaikeiNendoTo = tmpDateTo.Year;
            if (tmpDateFr.Month <= 3)
            {
                kaikeiNendo = tmpDateFr.Year - 1;
            }
            else
            {
                kaikeiNendo = tmpDateFr.Year;
            }
            // 元帳名設定
            string tmpMotochoNm = "売 上 元 帳";
            //支所コードの退避
            string shishoCd = this.txtMizuageShishoCd.Text;
            int LineType = 0;
            //現金取引区分を取得
            decimal GENKIN = Util.ToDecimal(this.Config.LoadPgConfig(Constants.SubSys.Kob, this.ProductName, "Setting", "GENKIN"));
            GENKIN = (GENKIN == 0) ? 2 : GENKIN;

            StringBuilder Sql = new StringBuilder();
            #endregion

            #region 各業者の期首残を取得
            dpc = new DbParamCollection();
            Sql = new StringBuilder();
            Sql.Append(" SELECT ");
            Sql.Append(" A.TORIHIKISAKI_CD,");
            Sql.Append(" A.TORIHIKISAKI_NM,");
            Sql.Append(" A.DENWA_BANGO,");
            Sql.Append(" A.FAX_BANGO,");
            Sql.Append(" ISNULL(Z.ZANDAKA, 0) AS ZANDAKA,");
            Sql.Append(" ISNULL(Z.KISHUZAN, 0) AS KISHUZAN");
            Sql.Append(" FROM TB_CM_TORIHIKISAKI AS A ");
            Sql.Append(" INNER JOIN TB_HN_TORIHIKISAKI_JOHO AS A1");
            Sql.Append(" 	ON	A.KAISHA_CD = A1.KAISHA_CD");
            Sql.Append(" 	AND A.TORIHIKISAKI_CD = A1.TORIHIKISAKI_CD");
            Sql.Append(" 	AND A1.TORIHIKISAKI_KUBUN1 = 1  ");
            Sql.Append(" LEFT JOIN ");
            Sql.Append(" (");
            Sql.Append(" 	SELECT");
            Sql.Append(" 		C.KAISHA_CD,");
            Sql.Append(" 		C.HOJO_KAMOKU_CD AS TORIHIKISAKI_CD,");
            Sql.Append(" 		SUM(CASE WHEN C.TAISHAKU_KUBUN = D.TAISHAKU_KUBUN THEN C.ZEIKOMI_KINGAKU ELSE (C.ZEIKOMI_KINGAKU * -1) END) AS ZANDAKA,");
            Sql.Append(" 		SUM(CASE WHEN C.DENPYO_DATE < @DATE_FR THEN (CASE WHEN C.TAISHAKU_KUBUN = D.TAISHAKU_KUBUN THEN C.ZEIKOMI_KINGAKU ELSE (C.ZEIKOMI_KINGAKU * -1) END) ELSE 0 END) AS KISHUZAN ");
            Sql.Append(" 	FROM TB_ZM_SHIWAKE_MEISAI AS C ");
            Sql.Append(" 	INNER JOIN TB_HN_KISHU_ZAN_KAMOKU AS B ");
            Sql.Append(" 		ON  B.KAISHA_CD = C.KAISHA_CD  ");
            Sql.Append(" 		AND B.SHISHO_CD = C.SHISHO_CD  ");
            Sql.Append(" 		AND B.KANJO_KAMOKU_CD = C.KANJO_KAMOKU_CD  ");
            //Sql.Append(" 		AND 11 = B.MOTOCHO_KUBUN ");
            Sql.Append(" 		AND @MOTOCHO_KUBUN = B.MOTOCHO_KUBUN ");
            Sql.Append(" 		AND C.DENPYO_DATE < @DATE_TO ");
            Sql.Append(" 		AND C.KAIKEI_NENDO = @KAIKEI_NENDO ");
            Sql.Append(" 	LEFT OUTER JOIN TB_ZM_KANJO_KAMOKU AS D ");
            Sql.Append(" 		ON C.KAISHA_CD = D.KAISHA_CD  ");
            Sql.Append(" 		AND C.KAIKEI_NENDO = D.KAIKEI_NENDO");
            Sql.Append(" 		AND C.KANJO_KAMOKU_CD = D.KANJO_KAMOKU_CD  ");
            Sql.Append(" 	WHERE   ");
            Sql.Append(" 		C.KAISHA_CD = @KAISHA_CD AND  ");
            if (shishoCd != "0")
                Sql.Append(" 		C.SHISHO_CD = @SHISHO_CD AND  ");
            Sql.Append(" 		D.KAIKEI_NENDO = @KAIKEI_NENDO AND  ");
            Sql.Append(" 		C.HOJO_KAMOKU_CD BETWEEN @FUNANUSHI_CD_FR AND @FUNANUSHI_CD_TO ");
            Sql.Append(" 	GROUP BY");
            Sql.Append(" 	C.KAISHA_CD,");
            Sql.Append(" 	C.SHISHO_CD,");
            Sql.Append(" 	C.HOJO_KAMOKU_CD");
            Sql.Append(" ) Z");
            Sql.Append(" ON	A.KAISHA_CD = Z.KAISHA_CD");
            Sql.Append(" AND A.TORIHIKISAKI_CD = Z.TORIHIKISAKI_CD");
            Sql.Append(" ORDER BY  ");
            Sql.Append(" A.TORIHIKISAKI_CD ASC");

            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.VarChar, 4, kaikeiNendo);
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@DATE_FR", SqlDbType.VarChar, 10, tmpDateFr.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@DATE_TO", SqlDbType.VarChar, 10, tmpDateTo.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@FUNANUSHI_CD_FR", SqlDbType.VarChar, 6, FUNANUSHI_CD_FR);
            dpc.SetParam("@FUNANUSHI_CD_TO", SqlDbType.VarChar, 6, FUNANUSHI_CD_TO);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            dpc.SetParam("@MOTOCHO_KUBUN", SqlDbType.Decimal, 4, Motocho_kbn);

            DataTable dtKishu = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
            #endregion

            #region メインデータを取得
            // 日付範囲から発生している取引先データを取得
            dpc = new DbParamCollection();
            Sql = new StringBuilder();
            Sql.Append(" SELECT ");
            Sql.Append(" A.TOKUISAKI_CD AS TORIHIKISAKI_CD,");
            Sql.Append(" A.DENPYO_DATE AS DENPYO_DATE,");
            Sql.Append(" A.DENPYO_BANGO AS DENPYO_BANGO,");
            Sql.Append(" A.GYO_BANGO AS GYO_BANGO,");
            Sql.Append(" '' AS TEKIYO,");
            Sql.Append(" 0 AS ZEIKOMI_KINGAKU,");
            Sql.Append(" A.TORIHIKI_KUBUN1 AS TORIHIKI_KUBUN,");
            Sql.Append(" ((A.TORIHIKI_KUBUN1 * 10) + A.TORIHIKI_KUBUN2) AS TORIHIKI_KUBUN1_AND_2,");
            Sql.Append(" A.TORIHIKI_KUBUN_NM AS TORIHIKI_KUBUN_NM,");
            Sql.Append(" A.SHOHIN_CD AS SHOHIN_CD,");
            Sql.Append(" A.SHOHIN_NM AS SHOHIN_NM,");
            Sql.Append(" B.KIKAKU AS KIKAKU,");
            Sql.Append(" A.IRISU AS IRISU,");
            Sql.Append(" (A.SURYO * A.ENZAN_HOHO) AS SURYO,");
            Sql.Append(" A.TANKA AS TANKA,");
            Sql.Append(" (A.URIAGE_KINGAKU * A.ENZAN_HOHO) AS URIAGE_KINGAKU,");
            Sql.Append(" (A.SHOHIZEIGAKU * A.ENZAN_HOHO) AS SHOHIZEI,");
            Sql.Append(" A.ZEI_RITSU AS ZEI_RITSU,");
            Sql.Append(" A.SHOHIZEI_NYURYOKU_HOHO AS SHOHIZEI_NYURYOKU_HOHO,");

            Sql.Append(" C.SHOHIZEI_TENKA_HOHO AS SHOHIZEI_TENKA_HOHO,");

            Sql.Append(" 3 AS RANK ");
            Sql.Append(" FROM VI_HN_TORIHIKI_MOTOCHO AS A ");
            Sql.Append(" LEFT OUTER JOIN TB_HN_SHOHIN AS B ");
            Sql.Append(" 	ON  A.KAISHA_CD = B.KAISHA_CD  ");
            Sql.Append(" 	AND A.SHISHO_CD = B.SHISHO_CD  ");
            Sql.Append(" 	AND A.SHOHIN_CD = B.SHOHIN_CD  ");

            Sql.Append(" LEFT OUTER JOIN VI_HN_TORIHIKISAKI_JOHO AS C ");
            Sql.Append(" 	ON	A.KAISHA_CD = C.KAISHA_CD     ");
            Sql.Append(" 	AND A.TOKUISAKI_CD = C.TORIHIKISAKI_CD ");

            Sql.Append(" WHERE ");
            Sql.Append(" 	A.KAISHA_CD = @KAISHA_CD AND ");
            if (shishoCd != "0")
                Sql.Append(" 	A.SHISHO_CD = @SHISHO_CD AND ");
            Sql.Append(" 	A.DENPYO_KUBUN = 1 AND ");
            Sql.Append(" 	A.DENPYO_DATE BETWEEN @DATE_FR AND @DATE_TO AND ");
            Sql.Append(" 	A.TOKUISAKI_CD BETWEEN @FUNANUSHI_CD_FR AND @FUNANUSHI_CD_TO ");
            Sql.Append(" UNION ALL ");
            Sql.Append(" SELECT    ");
            Sql.Append(" 	B.HOJO_KAMOKU_CD AS TORIHIKISAKI_CD,");
            Sql.Append(" 	B.DENPYO_DATE AS DENPYO_DATE,");
            Sql.Append(" 	B.DENPYO_BANGO AS DENPYO_BANGO,");
            Sql.Append(" 	B.GYO_BANGO AS GYO_BANGO,");
            Sql.Append(" 	B.TEKIYO AS TEKIYO,");
            Sql.Append(" 	CASE WHEN C.TAISHAKU_KUBUN = B.TAISHAKU_KUBUN THEN -1 * B.ZEIKOMI_KINGAKU ELSE B.ZEIKOMI_KINGAKU END AS ZEIKOMI_KINGAKU,");
            Sql.Append(" 	9 AS TORIHIKI_KUBUN,");
            Sql.Append(" 	NULL AS TORIHIKI_KUBUN1_AND_2,");
            Sql.Append(" 	NULL AS TORIHIKI_KUBUN_NM,");
            Sql.Append(" 	NULL AS SHOHIN_CD,");
            Sql.Append(" 	NULL AS SHOHIN_NM,");
            Sql.Append(" 	NULL AS KIKAKU,");
            Sql.Append(" 	NULL AS IRISU,");
            Sql.Append(" 	NULL AS SURYO,");
            Sql.Append(" 	NULL AS TANKA,");
            Sql.Append(" 	NULL AS URIAGE_KINGAKU,");
            Sql.Append(" 	NULL AS SHOHIZEI,");
            Sql.Append(" 	NULL AS ZEI_RITSU,");
            Sql.Append(" 	NULL AS SHOHIZEI_NYURYOKU_HOHO,");

            Sql.Append(" 	NULL AS SHOHIZEI_TENKA_HOHO,");

            Sql.Append(" 	CASE WHEN C.TAISHAKU_KUBUN = B.TAISHAKU_KUBUN THEN 2 ELSE 1 END RANK");
            Sql.Append(" FROM TB_HN_NYUKIN_KAMOKU AS A  ");
            Sql.Append(" LEFT OUTER JOIN TB_ZM_SHIWAKE_MEISAI AS B ");
            Sql.Append(" 	ON 	A.KAISHA_CD = B.KAISHA_CD ");
            Sql.Append(" 	AND A.SHISHO_CD = B.SHISHO_CD ");
            Sql.Append(" 	AND 1 = B.DENPYO_KUBUN ");
            Sql.Append(" 	AND A.KANJO_KAMOKU_CD = B.KANJO_KAMOKU_CD ");
            Sql.Append(" LEFT OUTER JOIN TB_ZM_KANJO_KAMOKU AS C ");
            Sql.Append(" 	ON 	B.KAISHA_CD = C.KAISHA_CD ");
            Sql.Append(" 	AND B.KANJO_KAMOKU_CD = C.KANJO_KAMOKU_CD ");
            Sql.Append(" 	AND B.KAIKEI_NENDO = C.KAIKEI_NENDO  ");
            Sql.Append(" LEFT OUTER JOIN TB_HN_TORIHIKISAKI_JOHO AS D ");
            Sql.Append(" 	ON	B.KAISHA_CD = D.KAISHA_CD ");
            Sql.Append(" 	AND B.HOJO_KAMOKU_CD = D.TORIHIKISAKI_CD ");
            Sql.Append(" WHERE     ");
            Sql.Append(" 	A.KAISHA_CD = @KAISHA_CD AND     ");
            if (shishoCd != "0")
                Sql.Append(" 	B.SHISHO_CD = @SHISHO_CD AND");
            Sql.Append(" 	B.KAIKEI_NENDO = @KAIKEI_NENDO AND");
            //Sql.Append(" 	A.MOTOCHO_KUBUN = 11 AND    ");
            Sql.Append(" 	A.MOTOCHO_KUBUN = @MOTOCHO_KUBUN AND    ");
            Sql.Append(" 	B.HOJO_KAMOKU_CD BETWEEN @FUNANUSHI_CD_FR AND @FUNANUSHI_CD_TO AND     ");
            Sql.Append(" 	B.DENPYO_DATE Between @DATE_FR AND @DATE_TO AND    ");
            Sql.Append(" 	ISNULL(B.SHIWAKE_SAKUSEI_KUBUN,0) = 0 AND     ");
            Sql.Append(" 	D.TORIHIKISAKI_KUBUN1 = 1 ");
            Sql.Append(" ORDER BY ");
            Sql.Append(" 	TORIHIKISAKI_CD ASC,");
            Sql.Append(" 	DENPYO_DATE ASC,");
            Sql.Append(" 	DENPYO_BANGO ASC,");
            Sql.Append(" 	TORIHIKI_KUBUN1_AND_2 ASC,");
            Sql.Append(" 	GYO_BANGO ASC,");
            Sql.Append(" 	RANK ASC");

            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.VarChar, 4, kaikeiNendo);
            dpc.SetParam("@KAIKEI_NENDO_TO", SqlDbType.VarChar, 4, kaikeiNendoTo);
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@DATE_FR", SqlDbType.VarChar, 10, tmpDateFr.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@DATE_TO", SqlDbType.VarChar, 10, tmpDateTo.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@FUNANUSHI_CD_FR", SqlDbType.VarChar, 6, FUNANUSHI_CD_FR);
            dpc.SetParam("@FUNANUSHI_CD_TO", SqlDbType.VarChar, 6, FUNANUSHI_CD_TO);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            dpc.SetParam("@MOTOCHO_KUBUN", SqlDbType.Decimal, 4, Motocho_kbn);

            DataTable dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
            #endregion

            if (dtMainLoop.Rows.Count == 0)
            {
                int i = 0; // ループ用カウント変数
                int flag = 0; // 表示フラグ
                List<int> tirihikisakiCd = new List<int>(); // 取引先番号格納用変数
                while (dtKishu.Rows.Count > i)
                {
                    if (Util.ToInt(dtKishu.Rows[i]["KISHUZAN"]) != 0)
                    {
                        tirihikisakiCd.Add(i);
                        flag = 1;
                    }
                    i++;
                }
                if (flag != 0)
                {
                    i = 0;
                    int dbSORT = 1;
                    while (tirihikisakiCd.Count > i)
                    {
                        #region 前月繰越テーブルの登録
                        #region インサートテーブル
                        Sql = new StringBuilder();
                        dpc = new DbParamCollection();
                        Sql.Append("INSERT INTO PR_HN_TBL(");
                        Sql.Append("  GUID");
                        Sql.Append(" ,SORT");
                        Sql.Append(" ,ITEM01");
                        Sql.Append(" ,ITEM02");
                        Sql.Append(" ,ITEM03");
                        Sql.Append(" ,ITEM04");
                        Sql.Append(" ,ITEM05");
                        Sql.Append(" ,ITEM06");
                        Sql.Append(" ,ITEM07");
                        Sql.Append(" ,ITEM08");
                        Sql.Append(" ,ITEM12");
                        Sql.Append(" ,ITEM22");
                        Sql.Append(" ,ITEM23");
                        Sql.Append(") ");
                        Sql.Append("VALUES(");
                        Sql.Append("  @GUID");
                        Sql.Append(" ,@SORT");
                        Sql.Append(" ,@ITEM01");
                        Sql.Append(" ,@ITEM02");
                        Sql.Append(" ,@ITEM03");
                        Sql.Append(" ,@ITEM04");
                        Sql.Append(" ,@ITEM05");
                        Sql.Append(" ,@ITEM06");
                        Sql.Append(" ,@ITEM07");
                        Sql.Append(" ,@ITEM08");
                        Sql.Append(" ,@ITEM12");
                        Sql.Append(" ,@ITEM22");
                        Sql.Append(" ,@ITEM23");
                        Sql.Append(") ");
                        #endregion

                        #region ページヘッダーデータを設定
                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                        dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                        dbSORT++;
                        dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dtKishu.Rows[tirihikisakiCd[i]]["TORIHIKISAKI_CD"]); // 取引先コード
                        dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtKishu.Rows[tirihikisakiCd[i]]["TORIHIKISAKI_NM"]); // 取引先名
                        dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dtKishu.Rows[tirihikisakiCd[i]]["DENWA_BANGO"]); // 電話番号
                        dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtKishu.Rows[tirihikisakiCd[i]]["FAX_BANGO"]); // Fax番号
                        dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                        dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                        dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                        dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                        dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                        #endregion

                        dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "◇◇◇ 前月より繰越し ◇◇◇"); // 繰越時データ
                        dpc.SetParam("@ITEM22", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(dtKishu.Rows[tirihikisakiCd[i]]["KISHUZAN"]))); // 期首残高

                        this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                        #endregion

                        #region 空データの登録
                        #region インサートテーブル
                        Sql = new StringBuilder();
                        dpc = new DbParamCollection();
                        Sql.Append("INSERT INTO PR_HN_TBL(");
                        Sql.Append("  GUID");
                        Sql.Append(" ,SORT");
                        Sql.Append(" ,ITEM01");
                        Sql.Append(" ,ITEM02");
                        Sql.Append(" ,ITEM03");
                        Sql.Append(" ,ITEM04");
                        Sql.Append(" ,ITEM05");
                        Sql.Append(" ,ITEM06");
                        Sql.Append(" ,ITEM07");
                        Sql.Append(" ,ITEM08");
                        Sql.Append(" ,ITEM23");
                        Sql.Append(") ");
                        Sql.Append("VALUES(");
                        Sql.Append("  @GUID");
                        Sql.Append(" ,@SORT");
                        Sql.Append(" ,@ITEM01");
                        Sql.Append(" ,@ITEM02");
                        Sql.Append(" ,@ITEM03");
                        Sql.Append(" ,@ITEM04");
                        Sql.Append(" ,@ITEM05");
                        Sql.Append(" ,@ITEM06");
                        Sql.Append(" ,@ITEM07");
                        Sql.Append(" ,@ITEM08");
                        Sql.Append(" ,@ITEM23");
                        Sql.Append(") ");
                        #endregion

                        #region ページヘッダーデータを設定
                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                        dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                        dbSORT++;
                        dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dtKishu.Rows[tirihikisakiCd[i]]["TORIHIKISAKI_CD"]); // 取引先コード
                        dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtKishu.Rows[tirihikisakiCd[i]]["TORIHIKISAKI_NM"]); // 取引先名
                        dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dtKishu.Rows[tirihikisakiCd[i]]["DENWA_BANGO"]); // 電話番号
                        dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtKishu.Rows[tirihikisakiCd[i]]["FAX_BANGO"]); // Fax番号
                        dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                        dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                        dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                        dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                        dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                        #endregion

                        this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                        #endregion

                        #region 次月繰越テーブルの登録
                        #region インサートテーブル
                        Sql = new StringBuilder();
                        dpc = new DbParamCollection();
                        Sql.Append("INSERT INTO PR_HN_TBL(");
                        Sql.Append("  GUID");
                        Sql.Append(" ,SORT");
                        Sql.Append(" ,ITEM01");
                        Sql.Append(" ,ITEM02");
                        Sql.Append(" ,ITEM03");
                        Sql.Append(" ,ITEM04");
                        Sql.Append(" ,ITEM05");
                        Sql.Append(" ,ITEM06");
                        Sql.Append(" ,ITEM07");
                        Sql.Append(" ,ITEM08");
                        Sql.Append(" ,ITEM12");
                        Sql.Append(" ,ITEM22");
                        Sql.Append(" ,ITEM23");
                        Sql.Append(" ,ITEM24");
                        Sql.Append(") ");
                        Sql.Append("VALUES(");
                        Sql.Append("  @GUID");
                        Sql.Append(" ,@SORT");
                        Sql.Append(" ,@ITEM01");
                        Sql.Append(" ,@ITEM02");
                        Sql.Append(" ,@ITEM03");
                        Sql.Append(" ,@ITEM04");
                        Sql.Append(" ,@ITEM05");
                        Sql.Append(" ,@ITEM06");
                        Sql.Append(" ,@ITEM07");
                        Sql.Append(" ,@ITEM08");
                        Sql.Append(" ,@ITEM12");
                        Sql.Append(" ,@ITEM22");
                        Sql.Append(" ,@ITEM23");
                        Sql.Append(" ,@ITEM24");
                        Sql.Append(") ");
                        #endregion

                        #region ページヘッダーデータを設定
                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                        dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                        dbSORT++;
                        dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dtKishu.Rows[tirihikisakiCd[i]]["TORIHIKISAKI_CD"]); // 取引先コード
                        dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtKishu.Rows[tirihikisakiCd[i]]["TORIHIKISAKI_NM"]); // 取引先名
                        dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dtKishu.Rows[tirihikisakiCd[i]]["DENWA_BANGO"]); // 電話番号
                        dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtKishu.Rows[tirihikisakiCd[i]]["FAX_BANGO"]); // Fax番号
                        dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                        dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                        dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                        dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                        dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                        dpc.SetParam("@ITEM24", SqlDbType.VarChar, 200, 1); // 会社名
                        #endregion

                        dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "◇◇◇ 次月へ繰越し ◇◇◇"); // 繰越時データ
                        dpc.SetParam("@ITEM22", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(dtKishu.Rows[tirihikisakiCd[i]]["KISHUZAN"]))); // 期首残高

                        this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                        #endregion

                        i++;
                    }
                }
                else
                {
                    Msg.Info("該当データがありません。");
                    return false;
                }
            }
            else
            {
                NumVals DENPYO = new NumVals(); // 伝票グループ
                NumVals MONTH = new NumVals(); // 月グループ
                NumVals TORIHIKISAKI = new NumVals(); // 取引先グループ

                #region メインループ準備処理
                String tmpGetsu = null;// 月
                String tmpNextGetsu = null;// 比較用月
                String tmpBeforeGetsu = null;// 比較用月
                int tmpDenpyo = 0;// 伝票
                int tmpNextDenpyo = 0;// 比較用伝票
                int tmpBeforeDenpyo = 0;// 比較用伝票
                String tmpTorihikisakiCdAsc = null;// 昇順用取引先コード
                int tmpTorihikisakiCd = 0;// 取引先コード
                int tmpNextTorihikisakiCd = 0;// 比較用取引先コード
                int tmpBeforeTorihikisakiCd = 0;// 比較用取引先コード
                int flag = 0;// 空データ表示フラグ
                int flagShizai = 0;// 資材空データ表示フラグ
                int flagU01 = 0;// 売上計表示フラグ
                int flagU02 = 0;// 売上計表示フラグ
                int flagU03 = 1;// 売上計表示フラグ
                //double tmpRoundUriage;
                decimal tmpRoundUriage;

                String DenpyoDate = "";
                String DenpyoNo = "";
                String TorihikiKubunn = "";
                String Zandaka = "";

                String tmpDenwaBango = "";//電話番号変数
                String tmpFaxBango = "";//FAX番号変数
                String tmpTorihikisakiNm = null;// 取引先名

                int i = 0; // ループ用カウント変数
                int j = 1; // 1件後のデータとの比較用カウンタ変数
                int k = -1; // 1件前のデータとの比較用カウンタ変数
                int Multiple = 36; // 指定行での改ページ用変数
                int Count = 1; // 指定行での改ページ用カウンタ変数
                int z = 0; // 期首残・電話番号・FAX番号・取引先名の取得用カウンタ変数
                int dbSORT = 1;
                #endregion

                while (dtMainLoop.Rows.Count > i)
                {
                    #region データの準備
                    // 伝票日付を和暦に変換
                    DateTime d = Util.ToDate(dtMainLoop.Rows[i]["DENPYO_DATE"]);
                    string[] denpyo_date = Util.ConvJpDate(d, this.Dba);
                    if (denpyo_date[3].Length == 1)
                    {
                        denpyo_date[3] = " " + denpyo_date[3];
                    }
                    if (denpyo_date[4].Length == 1)
                    {
                        denpyo_date[4] = " " + denpyo_date[4];
                    }
                    String wareki_denpyo_date = denpyo_date[2] + "/" + denpyo_date[3] + "/" + denpyo_date[4];

                    // 比較用データがnullの場合(取引先コードが変わった時)、期首残・電話番号・FAX番号・取引先名を取得
                    if (tmpNextGetsu == null)
                    {
                        while (dtKishu.Rows.Count > z)
                        {
                            if (Util.ToInt(dtKishu.Rows[z]["TORIHIKISAKI_CD"]) == Util.ToInt(dtMainLoop.Rows[i]["TORIHIKISAKI_CD"]))
                            {
                                tmpTorihikisakiNm = Util.ToString(dtKishu.Rows[z]["TORIHIKISAKI_NM"]);
                                TORIHIKISAKI.ZANDAKA += Util.ToInt(dtKishu.Rows[z]["KISHUZAN"]);
                                tmpDenwaBango = Util.ToString(dtKishu.Rows[z]["DENWA_BANGO"]);
                                tmpFaxBango = Util.ToString(dtKishu.Rows[z]["FAX_BANGO"]);
                                break;
                            }
                            z++;
                        }
                    }

                    // 現在の伝票日付の月・伝票番号・取引先コードを取得
                    tmpGetsu = denpyo_date[3];
                    tmpDenpyo = Util.ToInt(dtMainLoop.Rows[i]["DENPYO_BANGO"]);
                    tmpTorihikisakiCd = Util.ToInt(dtMainLoop.Rows[i]["TORIHIKISAKI_CD"]);

                    // 比較用の伝票日付の月・伝票番号を取得
                    if (j < dtMainLoop.Rows.Count)
                    {
                        // 伝票日付を和暦に変換
                        DateTime tmpD = Util.ToDate(dtMainLoop.Rows[j]["DENPYO_DATE"]);
                        string[] tmpDenpyoDate = Util.ConvJpDate(tmpD, this.Dba);
                        if (tmpDenpyoDate[3].Length == 1)
                        {
                            tmpDenpyoDate[3] = " " + tmpDenpyoDate[3];
                        }
                        tmpNextGetsu = tmpDenpyoDate[3];
                        tmpNextDenpyo = Util.ToInt(dtMainLoop.Rows[j]["DENPYO_BANGO"]);
                        tmpNextTorihikisakiCd = Util.ToInt(dtMainLoop.Rows[j]["TORIHIKISAKI_CD"]);
                    }
                    if (k != -1)
                    {
                        // 伝票日付を和暦に変換
                        DateTime tmpD = Util.ToDate(dtMainLoop.Rows[k]["DENPYO_DATE"]);
                        string[] tmpDenpyoDate = Util.ConvJpDate(tmpD, this.Dba);
                        if (tmpDenpyoDate[3].Length == 1)
                        {
                            tmpDenpyoDate[3] = " " + tmpDenpyoDate[3];
                        }
                        tmpBeforeGetsu = tmpDenpyoDate[3];
                        tmpBeforeDenpyo = Util.ToInt(dtMainLoop.Rows[k]["DENPYO_BANGO"]);
                        tmpBeforeTorihikisakiCd = Util.ToInt(dtMainLoop.Rows[k]["TORIHIKISAKI_CD"]);
                    }
                    #endregion

                    #region 印刷ワークテーブルに登録
                    // 登録するデータを判別
                    if (Util.ToInt(dtMainLoop.Rows[i]["DENPYO_BANGO"]) != 0)
                    {
                        if (Util.ToInt(dtMainLoop.Rows[i]["TORIHIKISAKI_CD"]) == 99)
                        {
                            tmpTorihikisakiCdAsc = "00" + Util.ToInt(dtMainLoop.Rows[i]["TORIHIKISAKI_CD"]);
                        }
                        else
                        {
                            tmpTorihikisakiCdAsc = Util.ToString(dtMainLoop.Rows[i]["TORIHIKISAKI_CD"]);
                        }
                        // 現在の取引先コードと前回の取引先コードが違っていれば実行 // 前回の月と現在の月を比較し、違っていれば実行
                        if (tmpTorihikisakiCd != tmpBeforeTorihikisakiCd || tmpGetsu != tmpBeforeGetsu)
                        {
                            #region 前月繰越テーブルの登録
                            #region インサートテーブル
                            Sql = new StringBuilder();
                            dpc = new DbParamCollection();
                            Sql.Append("INSERT INTO PR_HN_TBL(");
                            Sql.Append("  GUID");
                            Sql.Append(" ,SORT");
                            Sql.Append(" ,ITEM01");
                            Sql.Append(" ,ITEM02");
                            Sql.Append(" ,ITEM03");
                            Sql.Append(" ,ITEM04");
                            Sql.Append(" ,ITEM05");
                            Sql.Append(" ,ITEM06");
                            Sql.Append(" ,ITEM07");
                            Sql.Append(" ,ITEM08");
                            Sql.Append(" ,ITEM12");
                            Sql.Append(" ,ITEM22");
                            Sql.Append(" ,ITEM23");
                            Sql.Append(") ");
                            Sql.Append("VALUES(");
                            Sql.Append("  @GUID");
                            Sql.Append(" ,@SORT");
                            Sql.Append(" ,@ITEM01");
                            Sql.Append(" ,@ITEM02");
                            Sql.Append(" ,@ITEM03");
                            Sql.Append(" ,@ITEM04");
                            Sql.Append(" ,@ITEM05");
                            Sql.Append(" ,@ITEM06");
                            Sql.Append(" ,@ITEM07");
                            Sql.Append(" ,@ITEM08");
                            Sql.Append(" ,@ITEM12");
                            Sql.Append(" ,@ITEM22");
                            Sql.Append(" ,@ITEM23");
                            Sql.Append(") ");
                            #endregion

                            #region ページヘッダーデータを設定
                            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                            dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                            dbSORT++;
                            dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, tmpTorihikisakiCdAsc); // 取引先コード
                            dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpTorihikisakiNm); // 取引先名
                            dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpDenwaBango); // 電話番号
                            dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpFaxBango); // Fax番号
                            dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                            dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                            dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                            dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                            dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                            #endregion

                            dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "◇◇◇ 前月より繰越し ◇◇◇"); // 繰越時データ
                            dpc.SetParam("@ITEM22", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 期首残高

                            this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                            Count++; // 改ページ用カウンタ
                            #endregion
                        }

                        // 改ページ用カウンタが34の倍数の時に実行
                        if (Count % Multiple == 0)
                        {
                            #region 改ページ時処理
                            #region インサートテーブル
                            Sql = new StringBuilder();
                            dpc = new DbParamCollection();
                            Sql.Append("INSERT INTO PR_HN_TBL(");
                            Sql.Append("  GUID");
                            Sql.Append(" ,SORT");
                            Sql.Append(" ,ITEM01");
                            Sql.Append(" ,ITEM02");
                            Sql.Append(" ,ITEM03");
                            Sql.Append(" ,ITEM04");
                            Sql.Append(" ,ITEM05");
                            Sql.Append(" ,ITEM06");
                            Sql.Append(" ,ITEM07");
                            Sql.Append(" ,ITEM08");
                            Sql.Append(" ,ITEM12");
                            Sql.Append(" ,ITEM19");
                            Sql.Append(" ,ITEM20");
                            Sql.Append(" ,ITEM21");
                            Sql.Append(" ,ITEM22");
                            Sql.Append(" ,ITEM23");
                            Sql.Append(" ,ITEM24");
                            Sql.Append(") ");
                            Sql.Append("VALUES(");
                            Sql.Append("  @GUID");
                            Sql.Append(" ,@SORT");
                            Sql.Append(" ,@ITEM01");
                            Sql.Append(" ,@ITEM02");
                            Sql.Append(" ,@ITEM03");
                            Sql.Append(" ,@ITEM04");
                            Sql.Append(" ,@ITEM05");
                            Sql.Append(" ,@ITEM06");
                            Sql.Append(" ,@ITEM07");
                            Sql.Append(" ,@ITEM08");
                            Sql.Append(" ,@ITEM12");
                            Sql.Append(" ,@ITEM19");
                            Sql.Append(" ,@ITEM20");
                            Sql.Append(" ,@ITEM21");
                            Sql.Append(" ,@ITEM22");
                            Sql.Append(" ,@ITEM23");
                            Sql.Append(" ,@ITEM24");
                            Sql.Append(") ");
                            #endregion

                            #region ページヘッダーデータを設定
                            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                            dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                            dbSORT++;
                            dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, tmpTorihikisakiCdAsc); // 取引先コード
                            dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpTorihikisakiNm); // 取引先名
                            dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpDenwaBango); // 電話番号
                            dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpFaxBango); // Fax番号
                            dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                            dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                            dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                            dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                            dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                            dpc.SetParam("@ITEM24", SqlDbType.VarChar, 200, 1); // 会社名
                            #endregion

                            dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "☆☆ 次頁へ繰越し ☆☆"); // 改ページ時文字列
                            dpc.SetParam("@ITEM19", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.URIAGEGAKU))); // 売上金額合計
                            dpc.SetParam("@ITEM20", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHOHIZEIGAKU))); // 売上金額合計
                            dpc.SetParam("@ITEM21", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.NYUKINGAKU))); // 入金額合計
                            dpc.SetParam("@ITEM22", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高

                            this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                            Count++; // 改ページ用カウンタ
                            #endregion

                            #region 改ページ時処理
                            #region インサートテーブル
                            Sql = new StringBuilder();
                            dpc = new DbParamCollection();
                            Sql.Append("INSERT INTO PR_HN_TBL(");
                            Sql.Append("  GUID");
                            Sql.Append(" ,SORT");
                            Sql.Append(" ,ITEM01");
                            Sql.Append(" ,ITEM02");
                            Sql.Append(" ,ITEM03");
                            Sql.Append(" ,ITEM04");
                            Sql.Append(" ,ITEM05");
                            Sql.Append(" ,ITEM06");
                            Sql.Append(" ,ITEM07");
                            Sql.Append(" ,ITEM08");
                            Sql.Append(" ,ITEM12");
                            Sql.Append(" ,ITEM19");
                            Sql.Append(" ,ITEM20");
                            Sql.Append(" ,ITEM21");
                            Sql.Append(" ,ITEM22");
                            Sql.Append(" ,ITEM23");
                            Sql.Append(") ");
                            Sql.Append("VALUES(");
                            Sql.Append("  @GUID");
                            Sql.Append(" ,@SORT");
                            Sql.Append(" ,@ITEM01");
                            Sql.Append(" ,@ITEM02");
                            Sql.Append(" ,@ITEM03");
                            Sql.Append(" ,@ITEM04");
                            Sql.Append(" ,@ITEM05");
                            Sql.Append(" ,@ITEM06");
                            Sql.Append(" ,@ITEM07");
                            Sql.Append(" ,@ITEM08");
                            Sql.Append(" ,@ITEM12");
                            Sql.Append(" ,@ITEM19");
                            Sql.Append(" ,@ITEM20");
                            Sql.Append(" ,@ITEM21");
                            Sql.Append(" ,@ITEM22");
                            Sql.Append(" ,@ITEM23");
                            Sql.Append(") ");
                            #endregion

                            #region ページヘッダーデータを設定
                            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                            dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                            dbSORT++;
                            dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, tmpTorihikisakiCdAsc); // 取引先コード
                            dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpTorihikisakiNm); // 取引先名
                            dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpDenwaBango); // 電話番号
                            dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpFaxBango); // Fax番号
                            dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                            dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                            dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                            dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                            dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                            #endregion

                            dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "☆☆ 前頁より繰越し ☆☆"); // 改ページ時文字列
                            dpc.SetParam("@ITEM19", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.URIAGEGAKU))); // 売上金額合計
                            dpc.SetParam("@ITEM20", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHOHIZEIGAKU))); // 売上金額合計
                            dpc.SetParam("@ITEM21", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.NYUKINGAKU))); // 入金額合計
                            dpc.SetParam("@ITEM22", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高

                            this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                            Count++; // 改ページ用カウンタ
                            #endregion
                        }

                        // 常に実行
                        #region 実データ登録
                        #region インサートテーブル
                        Sql = new StringBuilder();
                        dpc = new DbParamCollection();
                        Sql.Append("INSERT INTO PR_HN_TBL(");
                        Sql.Append("  GUID");
                        Sql.Append(" ,SORT");
                        Sql.Append(" ,ITEM01");
                        Sql.Append(" ,ITEM02");
                        Sql.Append(" ,ITEM03");
                        Sql.Append(" ,ITEM04");
                        Sql.Append(" ,ITEM05");
                        Sql.Append(" ,ITEM06");
                        Sql.Append(" ,ITEM07");
                        Sql.Append(" ,ITEM08");
                        Sql.Append(" ,ITEM09");
                        Sql.Append(" ,ITEM10");
                        Sql.Append(" ,ITEM11");
                        Sql.Append(" ,ITEM12");
                        Sql.Append(" ,ITEM13");
                        Sql.Append(" ,ITEM14");
                        Sql.Append(" ,ITEM15");
                        Sql.Append(" ,ITEM16");
                        Sql.Append(" ,ITEM17");
                        Sql.Append(" ,ITEM18");
                        Sql.Append(" ,ITEM19");
                        Sql.Append(" ,ITEM20");
                        Sql.Append(" ,ITEM21");
                        Sql.Append(" ,ITEM22");
                        Sql.Append(" ,ITEM23");
                        Sql.Append(") ");
                        Sql.Append("VALUES(");
                        Sql.Append("  @GUID");
                        Sql.Append(" ,@SORT");
                        Sql.Append(" ,@ITEM01");
                        Sql.Append(" ,@ITEM02");
                        Sql.Append(" ,@ITEM03");
                        Sql.Append(" ,@ITEM04");
                        Sql.Append(" ,@ITEM05");
                        Sql.Append(" ,@ITEM06");
                        Sql.Append(" ,@ITEM07");
                        Sql.Append(" ,@ITEM08");
                        Sql.Append(" ,@ITEM09");
                        Sql.Append(" ,@ITEM10");
                        Sql.Append(" ,@ITEM11");
                        Sql.Append(" ,@ITEM12");
                        Sql.Append(" ,@ITEM13");
                        Sql.Append(" ,@ITEM14");
                        Sql.Append(" ,@ITEM15");
                        Sql.Append(" ,@ITEM16");
                        Sql.Append(" ,@ITEM17");
                        Sql.Append(" ,@ITEM18");
                        Sql.Append(" ,@ITEM19");
                        Sql.Append(" ,@ITEM20");
                        Sql.Append(" ,@ITEM21");
                        Sql.Append(" ,@ITEM22");
                        Sql.Append(" ,@ITEM23");
                        Sql.Append(") ");
                        #endregion

                        #region ページヘッダーデータを設定
                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                        dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                        dbSORT++;
                        dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, tmpTorihikisakiCdAsc); // 取引先コード
                        dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpTorihikisakiNm); // 取引先名
                        dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpDenwaBango); // 電話番号
                        dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpFaxBango); // Fax番号
                        dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                        dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                        dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                        dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                        dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                        #endregion

                        if (tmpDenpyo == tmpBeforeDenpyo)
                        {
                            DenpyoDate = "";
                            DenpyoNo = "";
                            TorihikiKubunn = "";
                            Zandaka = "";
                        }
                        else
                        {
                            DenpyoDate = wareki_denpyo_date;
                            DenpyoNo = Util.ToString(dtMainLoop.Rows[i]["DENPYO_BANGO"]);
                            TorihikiKubunn = Util.ToString(dtMainLoop.Rows[i]["TORIHIKI_KUBUN_NM"]);
                            Zandaka = Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA));
                        }

                        // 現金売上時
                        if (Util.ToInt(dtMainLoop.Rows[i]["TORIHIKI_KUBUN"]) == GENKIN)
                        {

                            dpc.SetParam("@ITEM09", SqlDbType.VarChar, 20, DenpyoDate); // 伝票日付
                            dpc.SetParam("@ITEM10", SqlDbType.VarChar, 20, DenpyoNo); // 伝票No
                            dpc.SetParam("@ITEM11", SqlDbType.VarChar, 20, TorihikiKubunn); // 取引区分
                            dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, ""); // 文字列データ
                            dpc.SetParam("@ITEM13", SqlDbType.VarChar, 20, dtMainLoop.Rows[i]["SHOHIN_CD"]); // 商品番号
                            dpc.SetParam("@ITEM14", SqlDbType.VarChar, 20, dtMainLoop.Rows[i]["SHOHIN_NM"]); // 商品名
                            dpc.SetParam("@ITEM15", SqlDbType.VarChar, 20, dtMainLoop.Rows[i]["KIKAKU"]); // 規格
                            dpc.SetParam("@ITEM16", SqlDbType.VarChar, 20, dtMainLoop.Rows[i]["IRISU"]); // 入数
                            dpc.SetParam("@ITEM17", SqlDbType.VarChar, 20, Util.FormatNum(dtMainLoop.Rows[i]["SURYO"], 1)); // 数量
                            dpc.SetParam("@ITEM18", SqlDbType.VarChar, 20, Util.FormatNum(dtMainLoop.Rows[i]["TANKA"], 1)); // 単価
                            //tmpRoundUriage = Util.ToInt(dtMainLoop.Rows[i]["URIAGE_KINGAKU"]) + Util.ToInt(dtMainLoop.Rows[i]["SHOHIZEI"]);
                            //dpc.SetParam("@ITEM19", SqlDbType.VarChar, 20, Util.FormatNum(tmpRoundUriage)); // 売上金額
                            dpc.SetParam("@ITEM19", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(dtMainLoop.Rows[i]["URIAGE_KINGAKU"]))); // 売上金額
                            if (Util.ToString(dtMainLoop.Rows[i]["SHOHIZEI_TENKA_HOHO"]) == "1")
                            {
                                dpc.SetParam("@ITEM20", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(dtMainLoop.Rows[i]["SHOHIZEI"]))); // 消費税
                            }
                            else
                            {
                                dpc.SetParam("@ITEM20", SqlDbType.VarChar, 20, ""); // 消費税
                            }
                            dpc.SetParam("@ITEM21", SqlDbType.VarChar, 20, ""); // 入金額
                            //dpc.SetParam("@ITEM22", SqlDbType.VarChar, 20, ""); // 残高
                            dpc.SetParam("@ITEM22", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高


                            this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                            tmpRoundUriage = Util.ToDecimal(dtMainLoop.Rows[i]["URIAGE_KINGAKU"]) + Util.ToDecimal(dtMainLoop.Rows[i]["SHOHIZEI"]);

                            // 伝票グループ
                            //DENPYO.URIAGEGAKU += Util.ToDecimal(tmpRoundUriage); // 伝票売上金額合計用
                            DENPYO.URIAGEGAKU += Util.ToDecimal(dtMainLoop.Rows[i]["URIAGE_KINGAKU"]);
                            DENPYO.SHOHIZEIGAKU += Util.ToDecimal(dtMainLoop.Rows[i]["SHOHIZEI"]);
                            // 月グループ
                            //MONTH.URIAGEGAKU += Util.ToDecimal(tmpRoundUriage); // 月売上金額合計用
                            MONTH.URIAGEGAKU += Util.ToDecimal(dtMainLoop.Rows[i]["URIAGE_KINGAKU"]);
                            MONTH.SHOHIZEIGAKU += Util.ToDecimal(dtMainLoop.Rows[i]["SHOHIZEI"]);
                            MONTH.NYUKINGAKU += Util.ToDecimal(tmpRoundUriage); // 月入金額合計用
                            // 取引先グループ
                            //TORIHIKISAKI.URIAGEGAKU += Util.ToDecimal(tmpRoundUriage); // 取引先売上金額合計用
                            TORIHIKISAKI.URIAGEGAKU += Util.ToDecimal(dtMainLoop.Rows[i]["URIAGE_KINGAKU"]);
                            TORIHIKISAKI.SHOHIZEIGAKU += Util.ToDecimal(dtMainLoop.Rows[i]["SHOHIZEI"]);
                            TORIHIKISAKI.NYUKINGAKU += 0; // 取引先入金額合計用

                            if (tmpDenpyo != tmpBeforeDenpyo)
                            {
                                flagU01 = 1; // 売上計表示フラグ
                                flagU03 = 1; // 売上計表示フラグ
                            }
                            flagShizai = 1; // 資材空データ表示フラグ
                        }
                        // 売上時
                        else if (Util.ToInt(dtMainLoop.Rows[i]["TORIHIKI_KUBUN"]) != 9)
                        {
                            tmpRoundUriage = Util.ToDecimal(dtMainLoop.Rows[i]["URIAGE_KINGAKU"]) + Util.ToDecimal(dtMainLoop.Rows[i]["SHOHIZEI"]);
                            TORIHIKISAKI.ZANDAKA += Util.ToDecimal(tmpRoundUriage); // 取引先残高合計用

                            dpc.SetParam("@ITEM09", SqlDbType.VarChar, 20, DenpyoDate); // 伝票日付
                            dpc.SetParam("@ITEM10", SqlDbType.VarChar, 20, DenpyoNo); // 伝票No
                            dpc.SetParam("@ITEM11", SqlDbType.VarChar, 20, TorihikiKubunn); // 取引区分
                            dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, ""); // 文字列データ
                            dpc.SetParam("@ITEM13", SqlDbType.VarChar, 20, dtMainLoop.Rows[i]["SHOHIN_CD"]); // 商品番号
                            dpc.SetParam("@ITEM14", SqlDbType.VarChar, 20, dtMainLoop.Rows[i]["SHOHIN_NM"]); // 商品名
                            dpc.SetParam("@ITEM15", SqlDbType.VarChar, 20, dtMainLoop.Rows[i]["KIKAKU"]); // 規格
                            dpc.SetParam("@ITEM16", SqlDbType.VarChar, 20, dtMainLoop.Rows[i]["IRISU"]); // 入数
                            dpc.SetParam("@ITEM17", SqlDbType.VarChar, 20, Util.FormatNum(dtMainLoop.Rows[i]["SURYO"], 1)); // 数量
                            dpc.SetParam("@ITEM18", SqlDbType.VarChar, 20, Util.FormatNum(dtMainLoop.Rows[i]["TANKA"], 1)); // 単価
                            dpc.SetParam("@ITEM19", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(dtMainLoop.Rows[i]["URIAGE_KINGAKU"]))); // 売上金額
                            //dpc.SetParam("@ITEM20", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(dtMainLoop.Rows[i]["SHOHIZEI"]))); // 売上金額
                            if (Util.ToString(dtMainLoop.Rows[i]["SHOHIZEI_TENKA_HOHO"]) == "1")
                            {
                                dpc.SetParam("@ITEM20", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(dtMainLoop.Rows[i]["SHOHIZEI"]))); // 消費税
                            }
                            else
                            {
                                dpc.SetParam("@ITEM20", SqlDbType.VarChar, 20, ""); // 消費税
                            }
                            dpc.SetParam("@ITEM21", SqlDbType.VarChar, 20, ""); // 入金額
                            //dpc.SetParam("@ITEM22", SqlDbType.VarChar, 20, ""); // 残高
                            dpc.SetParam("@ITEM22", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高

                            this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                            // 伝票グループ
                            //DENPYO.URIAGEGAKU += Util.ToDecimal(tmpRoundUriage); // 伝票売上金額合計用
                            DENPYO.URIAGEGAKU += Util.ToDecimal(dtMainLoop.Rows[i]["URIAGE_KINGAKU"]);
                            DENPYO.SHOHIZEIGAKU += Util.ToDecimal(dtMainLoop.Rows[i]["SHOHIZEI"]);
                            DENPYO.ZANDAKA += Util.ToDecimal(tmpRoundUriage); // 伝票残高合計用
                                                                              // 月グループ
                                                                              //MONTH.URIAGEGAKU += Util.ToDecimal(tmpRoundUriage); // 月売上金額合計用
                            MONTH.URIAGEGAKU += Util.ToDecimal(dtMainLoop.Rows[i]["URIAGE_KINGAKU"]);
                            MONTH.SHOHIZEIGAKU += Util.ToDecimal(dtMainLoop.Rows[i]["SHOHIZEI"]);
                            MONTH.NYUKINGAKU += 0; // 月入金額合計用
                                MONTH.ZANDAKA += Util.ToDecimal(tmpRoundUriage); // 月残高合計用
                                                                                 // 取引先グループ
                                                                                 //TORIHIKISAKI.URIAGEGAKU += Util.ToDecimal(tmpRoundUriage); // 取引先売上金額合計用
                            TORIHIKISAKI.URIAGEGAKU += Util.ToDecimal(dtMainLoop.Rows[i]["URIAGE_KINGAKU"]);
                            TORIHIKISAKI.SHOHIZEIGAKU += Util.ToDecimal(dtMainLoop.Rows[i]["SHOHIZEI"]);
                            TORIHIKISAKI.NYUKINGAKU += 0; // 取引先入金額合計用
                                //TORIHIKISAKI.ZANDAKA += Util.ToDecimal(tmpRoundUriage); // 取引先残高合計用

                            if (tmpDenpyo != tmpBeforeDenpyo)
                            {
                                flagU01 = 1; // 売上計表示フラグ
                            }
                            if (Util.ToInt(dtMainLoop.Rows[i]["TORIHIKI_KUBUN"]) == 3 || Util.ToDecimal(dtMainLoop.Rows[i]["TORIHIKI_KUBUN"]) == 4)
                            {
                                flagShizai = 1; // 資材空データ表示フラグ
                                flagU03 = 0; // 売上計表示フラグ
                            }
                        }
                        // 入金時
                        else
                        {
                            decimal Nyukin_Gaku;
                            //// 貸借区分を比較して逆
                            //if (Util.ToDecimal(dtMainLoop.Rows[i]["SETTEI_TAISHAKU_KUBUN"]) == Util.ToDecimal(dtMainLoop.Rows[i]["TAISHAKU_KUBUN"]))
                            //{
                            //    Nyukin_Gaku = Util.ToDecimal(dtMainLoop.Rows[i]["NYUKINGAKU"]) * -1;
                            //}
                            //else
                            //{
                            //    Nyukin_Gaku = Util.ToDecimal(dtMainLoop.Rows[i]["NYUKINGAKU"]);
                            //}
                            Nyukin_Gaku = Util.ToDecimal(dtMainLoop.Rows[i]["ZEIKOMI_KINGAKU"]);

                            dpc.SetParam("@ITEM09", SqlDbType.VarChar, 20, wareki_denpyo_date); // 伝票日付
                            dpc.SetParam("@ITEM10", SqlDbType.VarChar, 20, Util.ToString(dtMainLoop.Rows[i]["DENPYO_BANGO"])); // 伝票No
                            dpc.SetParam("@ITEM11", SqlDbType.VarChar, 20, ""); // 取引区分
                            dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, ""); // 文字列データ
                            dpc.SetParam("@ITEM13", SqlDbType.VarChar, 20, ""); // 商品番号
                            dpc.SetParam("@ITEM14", SqlDbType.VarChar, 20, dtMainLoop.Rows[i]["TEKIYO"]); // 商品名
                            dpc.SetParam("@ITEM15", SqlDbType.VarChar, 20, ""); // 規格
                            dpc.SetParam("@ITEM16", SqlDbType.VarChar, 20, ""); // 入数
                            dpc.SetParam("@ITEM17", SqlDbType.VarChar, 20, ""); // 数量
                            dpc.SetParam("@ITEM18", SqlDbType.VarChar, 20, ""); // 単価
                            dpc.SetParam("@ITEM19", SqlDbType.VarChar, 20, ""); // 売上金額
                            dpc.SetParam("@ITEM20", SqlDbType.VarChar, 20, ""); // 売上金額
                            dpc.SetParam("@ITEM21", SqlDbType.VarChar, 20, Util.FormatNum(Nyukin_Gaku)); // 入金額
                            TORIHIKISAKI.ZANDAKA -= Nyukin_Gaku; // 伝票合計用残高
                            dpc.SetParam("@ITEM22", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高

                            this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                            // 月グループ
                            MONTH.URIAGEGAKU += 0; // 月売上金額合計用

                            MONTH.NYUKINGAKU += Nyukin_Gaku; // 月入金額合計用
                            // 取引先グループ
                            TORIHIKISAKI.URIAGEGAKU += 0; // 取引先売上金額合計用
                            TORIHIKISAKI.NYUKINGAKU += Nyukin_Gaku; // 取引先入金額合計用
                        }
                        Count++; // 改ページ用カウンタ
                        #endregion

                        // 改ページ用カウンタが34の倍数の時に実行
                        if (Count % Multiple == 0)
                        {
                            #region 改ページ時処理
                            #region インサートテーブル
                            Sql = new StringBuilder();
                            dpc = new DbParamCollection();
                            Sql.Append("INSERT INTO PR_HN_TBL(");
                            Sql.Append("  GUID");
                            Sql.Append(" ,SORT");
                            Sql.Append(" ,ITEM01");
                            Sql.Append(" ,ITEM02");
                            Sql.Append(" ,ITEM03");
                            Sql.Append(" ,ITEM04");
                            Sql.Append(" ,ITEM05");
                            Sql.Append(" ,ITEM06");
                            Sql.Append(" ,ITEM07");
                            Sql.Append(" ,ITEM08");
                            Sql.Append(" ,ITEM12");
                            Sql.Append(" ,ITEM19");
                            Sql.Append(" ,ITEM20");
                            Sql.Append(" ,ITEM21");
                            Sql.Append(" ,ITEM22");
                            Sql.Append(" ,ITEM23");
                            Sql.Append(" ,ITEM24");
                            Sql.Append(") ");
                            Sql.Append("VALUES(");
                            Sql.Append("  @GUID");
                            Sql.Append(" ,@SORT");
                            Sql.Append(" ,@ITEM01");
                            Sql.Append(" ,@ITEM02");
                            Sql.Append(" ,@ITEM03");
                            Sql.Append(" ,@ITEM04");
                            Sql.Append(" ,@ITEM05");
                            Sql.Append(" ,@ITEM06");
                            Sql.Append(" ,@ITEM07");
                            Sql.Append(" ,@ITEM08");
                            Sql.Append(" ,@ITEM12");
                            Sql.Append(" ,@ITEM19");
                            Sql.Append(" ,@ITEM20");
                            Sql.Append(" ,@ITEM21");
                            Sql.Append(" ,@ITEM22");
                            Sql.Append(" ,@ITEM23");
                            Sql.Append(" ,@ITEM24");
                            Sql.Append(") ");
                            #endregion

                            #region ページヘッダーデータを設定
                            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                            dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                            dbSORT++;
                            dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, tmpTorihikisakiCdAsc); // 取引先コード
                            dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpTorihikisakiNm); // 取引先名
                            dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpDenwaBango); // 電話番号
                            dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpFaxBango); // Fax番号
                            dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                            dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                            dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                            dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                            dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                            dpc.SetParam("@ITEM24", SqlDbType.VarChar, 200, 1); // 会社名
                            #endregion

                            dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "☆☆ 次頁へ繰越し ☆☆"); // 改ページ時文字列
                            dpc.SetParam("@ITEM19", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.URIAGEGAKU))); // 売上金額合計
                            dpc.SetParam("@ITEM20", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHOHIZEIGAKU))); // 売上金額合計
                            dpc.SetParam("@ITEM21", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.NYUKINGAKU))); // 入金額合計
                            dpc.SetParam("@ITEM22", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高

                            this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                            Count++; // 改ページ用カウンタ
                            #endregion

                            #region 改ページ時処理
                            #region インサートテーブル
                            Sql = new StringBuilder();
                            dpc = new DbParamCollection();
                            Sql.Append("INSERT INTO PR_HN_TBL(");
                            Sql.Append("  GUID");
                            Sql.Append(" ,SORT");
                            Sql.Append(" ,ITEM01");
                            Sql.Append(" ,ITEM02");
                            Sql.Append(" ,ITEM03");
                            Sql.Append(" ,ITEM04");
                            Sql.Append(" ,ITEM05");
                            Sql.Append(" ,ITEM06");
                            Sql.Append(" ,ITEM07");
                            Sql.Append(" ,ITEM08");
                            Sql.Append(" ,ITEM12");
                            Sql.Append(" ,ITEM19");
                            Sql.Append(" ,ITEM20");
                            Sql.Append(" ,ITEM21");
                            Sql.Append(" ,ITEM22");
                            Sql.Append(" ,ITEM23");
                            Sql.Append(") ");
                            Sql.Append("VALUES(");
                            Sql.Append("  @GUID");
                            Sql.Append(" ,@SORT");
                            Sql.Append(" ,@ITEM01");
                            Sql.Append(" ,@ITEM02");
                            Sql.Append(" ,@ITEM03");
                            Sql.Append(" ,@ITEM04");
                            Sql.Append(" ,@ITEM05");
                            Sql.Append(" ,@ITEM06");
                            Sql.Append(" ,@ITEM07");
                            Sql.Append(" ,@ITEM08");
                            Sql.Append(" ,@ITEM12");
                            Sql.Append(" ,@ITEM19");
                            Sql.Append(" ,@ITEM20");
                            Sql.Append(" ,@ITEM21");
                            Sql.Append(" ,@ITEM22");
                            Sql.Append(" ,@ITEM23");
                            Sql.Append(") ");
                            #endregion

                            #region ページヘッダーデータを設定
                            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                            dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                            dbSORT++;
                            dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, tmpTorihikisakiCdAsc); // 取引先コード
                            dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpTorihikisakiNm); // 取引先名
                            dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpDenwaBango); // 電話番号
                            dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpFaxBango); // Fax番号
                            dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                            dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                            dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                            dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                            dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                            #endregion

                            dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "☆☆ 前頁より繰越し ☆☆"); // 改ページ時文字列
                            dpc.SetParam("@ITEM19", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.URIAGEGAKU))); // 売上金額合計
                            dpc.SetParam("@ITEM20", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHOHIZEIGAKU))); // 入金額合計
                            dpc.SetParam("@ITEM21", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.NYUKINGAKU))); // 入金額合計
                            dpc.SetParam("@ITEM22", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高

                            this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                            Count++; // 改ページ用カウンタ
                            #endregion
                        }

                        // 取引区分が9でない時に実行  // 最後のデータ(次回カウンタと総データ数が一致)の時に実行 // 現在の伝票と次回の伝票が違っていれば実行
                        if (Util.ToInt(dtMainLoop.Rows[i]["TORIHIKI_KUBUN"]) != 9)
                        {
                            if (dtMainLoop.Rows.Count == j || tmpDenpyo != tmpNextDenpyo)
                            {
                                #region 伝票合計テーブルの登録
                                #region インサートテーブル
                                Sql = new StringBuilder();
                                dpc = new DbParamCollection();
                                Sql.Append("INSERT INTO PR_HN_TBL(");
                                Sql.Append("  GUID");
                                Sql.Append(" ,SORT");
                                Sql.Append(" ,ITEM01");
                                Sql.Append(" ,ITEM02");
                                Sql.Append(" ,ITEM03");
                                Sql.Append(" ,ITEM04");
                                Sql.Append(" ,ITEM05");
                                Sql.Append(" ,ITEM06");
                                Sql.Append(" ,ITEM07");
                                Sql.Append(" ,ITEM08");
                                Sql.Append(" ,ITEM18");
                                Sql.Append(" ,ITEM19");
                                Sql.Append(" ,ITEM20");
                                Sql.Append(" ,ITEM22");
                                Sql.Append(" ,ITEM23");
                                Sql.Append(") ");
                                Sql.Append("VALUES(");
                                Sql.Append("  @GUID");
                                Sql.Append(" ,@SORT");
                                Sql.Append(" ,@ITEM01");
                                Sql.Append(" ,@ITEM02");
                                Sql.Append(" ,@ITEM03");
                                Sql.Append(" ,@ITEM04");
                                Sql.Append(" ,@ITEM05");
                                Sql.Append(" ,@ITEM06");
                                Sql.Append(" ,@ITEM07");
                                Sql.Append(" ,@ITEM08");
                                Sql.Append(" ,@ITEM18");
                                Sql.Append(" ,@ITEM19");
                                Sql.Append(" ,@ITEM20");
                                Sql.Append(" ,@ITEM22");
                                Sql.Append(" ,@ITEM23");
                                Sql.Append(") ");
                                #endregion

                                #region ページヘッダーデータを設定
                                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                dbSORT++;
                                dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, tmpTorihikisakiCdAsc); // 取引先コード
                                dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpTorihikisakiNm); // 取引先名
                                dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpDenwaBango); // 電話番号
                                dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpFaxBango); // Fax番号
                                dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                                dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                                dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                                #endregion

                                dpc.SetParam("@ITEM18", SqlDbType.VarChar, 20, "【伝票合計】"); // 伝票合計
                                dpc.SetParam("@ITEM19", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(DENPYO.URIAGEGAKU))); // 売上金額合計
                                dpc.SetParam("@ITEM20", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(DENPYO.SHOHIZEIGAKU))); // 売上金額合計
                                dpc.SetParam("@ITEM22", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高

                                this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                                flag = 1;
                                flagShizai = 0;
                                // 伝票合計をクリア
                                DENPYO.Clear();
                                Count++; // 改ページ用カウンタ
                                #endregion
                            }
                        }

                        // 改ページ用カウンタが34の倍数の時に実行
                        if (Count % Multiple == 0)
                        {
                            #region 改ページ時処理
                            #region インサートテーブル
                            Sql = new StringBuilder();
                            dpc = new DbParamCollection();
                            Sql.Append("INSERT INTO PR_HN_TBL(");
                            Sql.Append("  GUID");
                            Sql.Append(" ,SORT");
                            Sql.Append(" ,ITEM01");
                            Sql.Append(" ,ITEM02");
                            Sql.Append(" ,ITEM03");
                            Sql.Append(" ,ITEM04");
                            Sql.Append(" ,ITEM05");
                            Sql.Append(" ,ITEM06");
                            Sql.Append(" ,ITEM07");
                            Sql.Append(" ,ITEM08");
                            Sql.Append(" ,ITEM12");
                            Sql.Append(" ,ITEM19");
                            Sql.Append(" ,ITEM20");
                            Sql.Append(" ,ITEM21");
                            Sql.Append(" ,ITEM22");
                            Sql.Append(" ,ITEM23");
                            Sql.Append(" ,ITEM24");
                            Sql.Append(") ");
                            Sql.Append("VALUES(");
                            Sql.Append("  @GUID");
                            Sql.Append(" ,@SORT");
                            Sql.Append(" ,@ITEM01");
                            Sql.Append(" ,@ITEM02");
                            Sql.Append(" ,@ITEM03");
                            Sql.Append(" ,@ITEM04");
                            Sql.Append(" ,@ITEM05");
                            Sql.Append(" ,@ITEM06");
                            Sql.Append(" ,@ITEM07");
                            Sql.Append(" ,@ITEM08");
                            Sql.Append(" ,@ITEM12");
                            Sql.Append(" ,@ITEM19");
                            Sql.Append(" ,@ITEM20");
                            Sql.Append(" ,@ITEM21");
                            Sql.Append(" ,@ITEM22");
                            Sql.Append(" ,@ITEM23");
                            Sql.Append(" ,@ITEM24");
                            Sql.Append(") ");
                            #endregion

                            #region ページヘッダーデータを設定
                            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                            dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                            dbSORT++;
                            dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, tmpTorihikisakiCdAsc); // 取引先コード
                            dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpTorihikisakiNm); // 取引先名
                            dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpDenwaBango); // 電話番号
                            dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpFaxBango); // Fax番号
                            dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                            dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                            dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                            dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                            dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                            dpc.SetParam("@ITEM24", SqlDbType.VarChar, 200, 1); // 会社名
                            #endregion

                            dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "☆☆ 次頁へ繰越し ☆☆"); // 改ページ時文字列
                            dpc.SetParam("@ITEM19", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.URIAGEGAKU))); // 売上金額合計
                            dpc.SetParam("@ITEM20", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHOHIZEIGAKU))); // 売上金額合計
                            dpc.SetParam("@ITEM21", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.NYUKINGAKU))); // 入金額合計
                            dpc.SetParam("@ITEM22", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高

                            this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                            Count++; // 改ページ用カウンタ
                            #endregion

                            #region 改ページ時処理
                            #region インサートテーブル
                            Sql = new StringBuilder();
                            dpc = new DbParamCollection();
                            Sql.Append("INSERT INTO PR_HN_TBL(");
                            Sql.Append("  GUID");
                            Sql.Append(" ,SORT");
                            Sql.Append(" ,ITEM01");
                            Sql.Append(" ,ITEM02");
                            Sql.Append(" ,ITEM03");
                            Sql.Append(" ,ITEM04");
                            Sql.Append(" ,ITEM05");
                            Sql.Append(" ,ITEM06");
                            Sql.Append(" ,ITEM07");
                            Sql.Append(" ,ITEM08");
                            Sql.Append(" ,ITEM12");
                            Sql.Append(" ,ITEM19");
                            Sql.Append(" ,ITEM20");
                            Sql.Append(" ,ITEM21");
                            Sql.Append(" ,ITEM22");
                            Sql.Append(" ,ITEM23");
                            Sql.Append(") ");
                            Sql.Append("VALUES(");
                            Sql.Append("  @GUID");
                            Sql.Append(" ,@SORT");
                            Sql.Append(" ,@ITEM01");
                            Sql.Append(" ,@ITEM02");
                            Sql.Append(" ,@ITEM03");
                            Sql.Append(" ,@ITEM04");
                            Sql.Append(" ,@ITEM05");
                            Sql.Append(" ,@ITEM06");
                            Sql.Append(" ,@ITEM07");
                            Sql.Append(" ,@ITEM08");
                            Sql.Append(" ,@ITEM12");
                            Sql.Append(" ,@ITEM19");
                            Sql.Append(" ,@ITEM20");
                            Sql.Append(" ,@ITEM21");
                            Sql.Append(" ,@ITEM22");
                            Sql.Append(" ,@ITEM23");
                            Sql.Append(") ");
                            #endregion

                            #region ページヘッダーデータを設定
                            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                            dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                            dbSORT++;
                            dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, tmpTorihikisakiCdAsc); // 取引先コード
                            dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpTorihikisakiNm); // 取引先名
                            dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpDenwaBango); // 電話番号
                            dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpFaxBango); // Fax番号
                            dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                            dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                            dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                            dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                            dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                            #endregion

                            dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "☆☆ 前頁より繰越し ☆☆"); // 改ページ時文字列
                            dpc.SetParam("@ITEM19", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.URIAGEGAKU))); // 売上金額合計
                            dpc.SetParam("@ITEM20", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHOHIZEIGAKU))); // 売上金額合計
                            dpc.SetParam("@ITEM21", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.NYUKINGAKU))); // 入金額合計
                            dpc.SetParam("@ITEM22", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高

                            this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                            Count++; // 改ページ用カウンタ
                            #endregion
                        }

                        // flagShizaiが0であれば実効 // 現在の伝票と次回の伝票が違っていれば実行 // 取引区分が9でなければ実行 // 最後のデータ(次回カウンタと総データ数が一致)の時に実行 // flagが1の時に実行
                        if (flagShizai == 0)
                        {
                            if (tmpDenpyo != tmpNextDenpyo || Util.ToInt(dtMainLoop.Rows[i]["TORIHIKI_KUBUN"]) != 9 || dtMainLoop.Rows.Count == j || flag == 1)
                            {
                                #region 空データの登録
                                #region インサートテーブル
                                Sql = new StringBuilder();
                                dpc = new DbParamCollection();
                                Sql.Append("INSERT INTO PR_HN_TBL(");
                                Sql.Append("  GUID");
                                Sql.Append(" ,SORT");
                                Sql.Append(" ,ITEM01");
                                Sql.Append(" ,ITEM02");
                                Sql.Append(" ,ITEM03");
                                Sql.Append(" ,ITEM04");
                                Sql.Append(" ,ITEM05");
                                Sql.Append(" ,ITEM06");
                                Sql.Append(" ,ITEM07");
                                Sql.Append(" ,ITEM08");
                                Sql.Append(" ,ITEM23");
                                Sql.Append(") ");
                                Sql.Append("VALUES(");
                                Sql.Append("  @GUID");
                                Sql.Append(" ,@SORT");
                                Sql.Append(" ,@ITEM01");
                                Sql.Append(" ,@ITEM02");
                                Sql.Append(" ,@ITEM03");
                                Sql.Append(" ,@ITEM04");
                                Sql.Append(" ,@ITEM05");
                                Sql.Append(" ,@ITEM06");
                                Sql.Append(" ,@ITEM07");
                                Sql.Append(" ,@ITEM08");
                                Sql.Append(" ,@ITEM23");
                                Sql.Append(") ");
                                #endregion

                                #region ページヘッダーデータを設定
                                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                dbSORT++;
                                dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, tmpTorihikisakiCdAsc); // 取引先コード
                                dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpTorihikisakiNm); // 取引先名
                                dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpDenwaBango); // 電話番号
                                dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpFaxBango); // Fax番号
                                dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                                dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                                dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                                #endregion

                                this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                                flag = 0;
                                Count++; // 改ページ用カウンタ
                                #endregion
                            }
                        }

                        // 改ページ用カウンタが34の倍数の時に実行
                        if (Count % Multiple == 0)
                        {
                            #region 改ページ時処理
                            #region インサートテーブル
                            Sql = new StringBuilder();
                            dpc = new DbParamCollection();
                            Sql.Append("INSERT INTO PR_HN_TBL(");
                            Sql.Append("  GUID");
                            Sql.Append(" ,SORT");
                            Sql.Append(" ,ITEM01");
                            Sql.Append(" ,ITEM02");
                            Sql.Append(" ,ITEM03");
                            Sql.Append(" ,ITEM04");
                            Sql.Append(" ,ITEM05");
                            Sql.Append(" ,ITEM06");
                            Sql.Append(" ,ITEM07");
                            Sql.Append(" ,ITEM08");
                            Sql.Append(" ,ITEM12");
                            Sql.Append(" ,ITEM19");
                            Sql.Append(" ,ITEM20");
                            Sql.Append(" ,ITEM21");
                            Sql.Append(" ,ITEM22");
                            Sql.Append(" ,ITEM23");
                            Sql.Append(" ,ITEM24");
                            Sql.Append(") ");
                            Sql.Append("VALUES(");
                            Sql.Append("  @GUID");
                            Sql.Append(" ,@SORT");
                            Sql.Append(" ,@ITEM01");
                            Sql.Append(" ,@ITEM02");
                            Sql.Append(" ,@ITEM03");
                            Sql.Append(" ,@ITEM04");
                            Sql.Append(" ,@ITEM05");
                            Sql.Append(" ,@ITEM06");
                            Sql.Append(" ,@ITEM07");
                            Sql.Append(" ,@ITEM08");
                            Sql.Append(" ,@ITEM12");
                            Sql.Append(" ,@ITEM19");
                            Sql.Append(" ,@ITEM20");
                            Sql.Append(" ,@ITEM21");
                            Sql.Append(" ,@ITEM22");
                            Sql.Append(" ,@ITEM23");
                            Sql.Append(" ,@ITEM24");
                            Sql.Append(") ");
                            #endregion

                            #region ページヘッダーデータを設定
                            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                            dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                            dbSORT++;
                            dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, tmpTorihikisakiCdAsc); // 取引先コード
                            dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpTorihikisakiNm); // 取引先名
                            dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpDenwaBango); // 電話番号
                            dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpFaxBango); // Fax番号
                            dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                            dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                            dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                            dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                            dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                            dpc.SetParam("@ITEM24", SqlDbType.VarChar, 200, 1); // 会社名
                            #endregion

                            dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "☆☆ 次頁へ繰越し ☆☆"); // 改ページ時文字列
                            dpc.SetParam("@ITEM19", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.URIAGEGAKU))); // 売上金額合計
                            dpc.SetParam("@ITEM20", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHOHIZEIGAKU))); // 売上金額合計
                            dpc.SetParam("@ITEM21", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.NYUKINGAKU))); // 入金額合計
                            dpc.SetParam("@ITEM22", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高

                            this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                            Count++; // 改ページ用カウンタ
                            #endregion

                            #region 改ページ時処理
                            #region インサートテーブル
                            Sql = new StringBuilder();
                            dpc = new DbParamCollection();
                            Sql.Append("INSERT INTO PR_HN_TBL(");
                            Sql.Append("  GUID");
                            Sql.Append(" ,SORT");
                            Sql.Append(" ,ITEM01");
                            Sql.Append(" ,ITEM02");
                            Sql.Append(" ,ITEM03");
                            Sql.Append(" ,ITEM04");
                            Sql.Append(" ,ITEM05");
                            Sql.Append(" ,ITEM06");
                            Sql.Append(" ,ITEM07");
                            Sql.Append(" ,ITEM08");
                            Sql.Append(" ,ITEM12");
                            Sql.Append(" ,ITEM19");
                            Sql.Append(" ,ITEM20");
                            Sql.Append(" ,ITEM21");
                            Sql.Append(" ,ITEM22");
                            Sql.Append(" ,ITEM23");
                            Sql.Append(") ");
                            Sql.Append("VALUES(");
                            Sql.Append("  @GUID");
                            Sql.Append(" ,@SORT");
                            Sql.Append(" ,@ITEM01");
                            Sql.Append(" ,@ITEM02");
                            Sql.Append(" ,@ITEM03");
                            Sql.Append(" ,@ITEM04");
                            Sql.Append(" ,@ITEM05");
                            Sql.Append(" ,@ITEM06");
                            Sql.Append(" ,@ITEM07");
                            Sql.Append(" ,@ITEM08");
                            Sql.Append(" ,@ITEM12");
                            Sql.Append(" ,@ITEM19");
                            Sql.Append(" ,@ITEM20");
                            Sql.Append(" ,@ITEM21");
                            Sql.Append(" ,@ITEM22");
                            Sql.Append(" ,@ITEM23");
                            Sql.Append(") ");
                            #endregion

                            #region ページヘッダーデータを設定
                            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                            dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                            dbSORT++;
                            dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, tmpTorihikisakiCdAsc); // 取引先コード
                            dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpTorihikisakiNm); // 取引先名
                            dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpDenwaBango); // 電話番号
                            dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpFaxBango); // Fax番号
                            dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                            dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                            dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                            dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                            dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                            #endregion

                            dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "☆☆ 前頁より繰越し ☆☆"); // 改ページ時文字列
                            dpc.SetParam("@ITEM19", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.URIAGEGAKU))); // 売上金額合計
                            dpc.SetParam("@ITEM20", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHOHIZEIGAKU))); // 売上金額合計
                            dpc.SetParam("@ITEM21", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.NYUKINGAKU))); // 入金額合計
                            dpc.SetParam("@ITEM22", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高

                            this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                            Count++; // 改ページ用カウンタ
                            #endregion
                        }

                        // 最後のデータ(次回カウンタと総データ数が一致)の時に実行 // 現在の月と次回の月が違っていれば実行 // 現在の取引先コードと次回の取引先コードが違っていれば実行
                        if (dtMainLoop.Rows.Count == j || tmpGetsu != tmpNextGetsu || tmpTorihikisakiCd != tmpNextTorihikisakiCd)
                        {
                            #region 月合計テーブルの登録
                            #region インサートテーブル
                            Sql = new StringBuilder();
                            dpc = new DbParamCollection();
                            Sql.Append("INSERT INTO PR_HN_TBL(");
                            Sql.Append("  GUID");
                            Sql.Append(" ,SORT");
                            Sql.Append(" ,ITEM01");
                            Sql.Append(" ,ITEM02");
                            Sql.Append(" ,ITEM03");
                            Sql.Append(" ,ITEM04");
                            Sql.Append(" ,ITEM05");
                            Sql.Append(" ,ITEM06");
                            Sql.Append(" ,ITEM07");
                            Sql.Append(" ,ITEM08");
                            Sql.Append(" ,ITEM12");
                            Sql.Append(" ,ITEM19");
                            Sql.Append(" ,ITEM20");
                            Sql.Append(" ,ITEM21");
                            Sql.Append(" ,ITEM22");
                            Sql.Append(" ,ITEM23");
                            Sql.Append(") ");
                            Sql.Append("VALUES(");
                            Sql.Append("  @GUID");
                            Sql.Append(" ,@SORT");
                            Sql.Append(" ,@ITEM01");
                            Sql.Append(" ,@ITEM02");
                            Sql.Append(" ,@ITEM03");
                            Sql.Append(" ,@ITEM04");
                            Sql.Append(" ,@ITEM05");
                            Sql.Append(" ,@ITEM06");
                            Sql.Append(" ,@ITEM07");
                            Sql.Append(" ,@ITEM08");
                            Sql.Append(" ,@ITEM12");
                            Sql.Append(" ,@ITEM19");
                            Sql.Append(" ,@ITEM20");
                            Sql.Append(" ,@ITEM21");
                            Sql.Append(" ,@ITEM22");
                            Sql.Append(" ,@ITEM23");
                            Sql.Append(") ");
                            #endregion

                            #region ページヘッダーデータを設定
                            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                            dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                            dbSORT++;
                            dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, tmpTorihikisakiCdAsc); // 取引先コード
                            dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpTorihikisakiNm); // 取引先名
                            dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpDenwaBango); // 電話番号
                            dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpFaxBango); // Fax番号
                            dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                            dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                            dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                            dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                            dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                            #endregion

                            dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "【　　" + tmpGetsu + "月度 合計】"); // 月度合計
                            dpc.SetParam("@ITEM19", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.URIAGEGAKU))); // 売上金額合計
                            dpc.SetParam("@ITEM20", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHOHIZEIGAKU))); // 売上金額合計
                            dpc.SetParam("@ITEM21", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.NYUKINGAKU))); // 入金額合計
                            dpc.SetParam("@ITEM22", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高

                            this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                            flag = 1; // 空データ表示フラグ
                            flagU02 = 1; // 売上計表示フラグ
                            Count++; // 改ページ用カウンタ
                            #endregion
                        }

                        // 改ページ用カウンタが34の倍数の時に実行
                        if (Count % Multiple == 0)
                        {
                            #region 改ページ時処理
                            #region インサートテーブル
                            Sql = new StringBuilder();
                            dpc = new DbParamCollection();
                            Sql.Append("INSERT INTO PR_HN_TBL(");
                            Sql.Append("  GUID");
                            Sql.Append(" ,SORT");
                            Sql.Append(" ,ITEM01");
                            Sql.Append(" ,ITEM02");
                            Sql.Append(" ,ITEM03");
                            Sql.Append(" ,ITEM04");
                            Sql.Append(" ,ITEM05");
                            Sql.Append(" ,ITEM06");
                            Sql.Append(" ,ITEM07");
                            Sql.Append(" ,ITEM08");
                            Sql.Append(" ,ITEM12");
                            Sql.Append(" ,ITEM19");
                            Sql.Append(" ,ITEM20");
                            Sql.Append(" ,ITEM21");
                            Sql.Append(" ,ITEM22");
                            Sql.Append(" ,ITEM23");
                            Sql.Append(" ,ITEM24");
                            Sql.Append(") ");
                            Sql.Append("VALUES(");
                            Sql.Append("  @GUID");
                            Sql.Append(" ,@SORT");
                            Sql.Append(" ,@ITEM01");
                            Sql.Append(" ,@ITEM02");
                            Sql.Append(" ,@ITEM03");
                            Sql.Append(" ,@ITEM04");
                            Sql.Append(" ,@ITEM05");
                            Sql.Append(" ,@ITEM06");
                            Sql.Append(" ,@ITEM07");
                            Sql.Append(" ,@ITEM08");
                            Sql.Append(" ,@ITEM12");
                            Sql.Append(" ,@ITEM19");
                            Sql.Append(" ,@ITEM20");
                            Sql.Append(" ,@ITEM21");
                            Sql.Append(" ,@ITEM22");
                            Sql.Append(" ,@ITEM23");
                            Sql.Append(" ,@ITEM24");
                            Sql.Append(") ");
                            #endregion

                            #region ページヘッダーデータを設定
                            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                            dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                            dbSORT++;
                            dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, tmpTorihikisakiCdAsc); // 取引先コード
                            dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpTorihikisakiNm); // 取引先名
                            dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpDenwaBango); // 電話番号
                            dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpFaxBango); // Fax番号
                            dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                            dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                            dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                            dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                            dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                            dpc.SetParam("@ITEM24", SqlDbType.VarChar, 200, 1); // 会社名
                            #endregion

                            dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "☆☆ 次頁へ繰越し ☆☆"); // 改ページ時文字列
                            dpc.SetParam("@ITEM19", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.URIAGEGAKU))); // 売上金額合計
                            dpc.SetParam("@ITEM20", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHOHIZEIGAKU))); // 売上金額合計
                            dpc.SetParam("@ITEM21", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.NYUKINGAKU))); // 入金額合計
                            dpc.SetParam("@ITEM22", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高

                            this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                            Count++; // 改ページ用カウンタ
                            #endregion

                            #region 改ページ時処理
                            #region インサートテーブル
                            Sql = new StringBuilder();
                            dpc = new DbParamCollection();
                            Sql.Append("INSERT INTO PR_HN_TBL(");
                            Sql.Append("  GUID");
                            Sql.Append(" ,SORT");
                            Sql.Append(" ,ITEM01");
                            Sql.Append(" ,ITEM02");
                            Sql.Append(" ,ITEM03");
                            Sql.Append(" ,ITEM04");
                            Sql.Append(" ,ITEM05");
                            Sql.Append(" ,ITEM06");
                            Sql.Append(" ,ITEM07");
                            Sql.Append(" ,ITEM08");
                            Sql.Append(" ,ITEM12");
                            Sql.Append(" ,ITEM19");
                            Sql.Append(" ,ITEM20");
                            Sql.Append(" ,ITEM21");
                            Sql.Append(" ,ITEM22");
                            Sql.Append(" ,ITEM23");
                            Sql.Append(") ");
                            Sql.Append("VALUES(");
                            Sql.Append("  @GUID");
                            Sql.Append(" ,@SORT");
                            Sql.Append(" ,@ITEM01");
                            Sql.Append(" ,@ITEM02");
                            Sql.Append(" ,@ITEM03");
                            Sql.Append(" ,@ITEM04");
                            Sql.Append(" ,@ITEM05");
                            Sql.Append(" ,@ITEM06");
                            Sql.Append(" ,@ITEM07");
                            Sql.Append(" ,@ITEM08");
                            Sql.Append(" ,@ITEM12");
                            Sql.Append(" ,@ITEM19");
                            Sql.Append(" ,@ITEM20");
                            Sql.Append(" ,@ITEM21");
                            Sql.Append(" ,@ITEM22");
                            Sql.Append(" ,@ITEM23");
                            Sql.Append(") ");
                            #endregion

                            #region ページヘッダーデータを設定
                            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                            dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                            dbSORT++;
                            dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, tmpTorihikisakiCdAsc); // 取引先コード
                            dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpTorihikisakiNm); // 取引先名
                            dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpDenwaBango); // 電話番号
                            dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpFaxBango); // Fax番号
                            dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                            dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                            dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                            dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                            dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                            #endregion

                            dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "☆☆ 前頁より繰越し ☆☆"); // 改ページ時文字列
                            dpc.SetParam("@ITEM19", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.URIAGEGAKU))); // 売上金額合計
                            dpc.SetParam("@ITEM20", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHOHIZEIGAKU))); // 売上金額合計
                            dpc.SetParam("@ITEM21", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.NYUKINGAKU))); // 入金額合計
                            dpc.SetParam("@ITEM22", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高

                            this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                            Count++; // 改ページ用カウンタ
                            #endregion
                        }

                        // flagU01とflagU02とflagU03が1の時に実行
                        //if (flagU01 == 1 && flagU02 == 1 && flagU03 == 1)
                        if (flagU01 == 9 && flagU02 == 9 && flagU03 == 9)
                        {
                            #region 売上計テーブルの登録
                            #region インサートテーブル
                            Sql = new StringBuilder();
                            dpc = new DbParamCollection();
                            Sql.Append("INSERT INTO PR_HN_TBL(");
                            Sql.Append("  GUID");
                            Sql.Append(" ,SORT");
                            Sql.Append(" ,ITEM01");
                            Sql.Append(" ,ITEM02");
                            Sql.Append(" ,ITEM03");
                            Sql.Append(" ,ITEM04");
                            Sql.Append(" ,ITEM05");
                            Sql.Append(" ,ITEM06");
                            Sql.Append(" ,ITEM07");
                            Sql.Append(" ,ITEM08");
                            Sql.Append(" ,ITEM12");
                            Sql.Append(" ,ITEM19");
                            Sql.Append(" ,ITEM23");
                            Sql.Append(") ");
                            Sql.Append("VALUES(");
                            Sql.Append("  @GUID");
                            Sql.Append(" ,@SORT");
                            Sql.Append(" ,@ITEM01");
                            Sql.Append(" ,@ITEM02");
                            Sql.Append(" ,@ITEM03");
                            Sql.Append(" ,@ITEM04");
                            Sql.Append(" ,@ITEM05");
                            Sql.Append(" ,@ITEM06");
                            Sql.Append(" ,@ITEM07");
                            Sql.Append(" ,@ITEM08");
                            Sql.Append(" ,@ITEM12");
                            Sql.Append(" ,@ITEM19");
                            Sql.Append(" ,@ITEM23");
                            Sql.Append(") ");
                            #endregion

                            #region ページヘッダーデータを設定
                            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                            dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                            dbSORT++;
                            dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, tmpTorihikisakiCdAsc); // 取引先コード
                            dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpTorihikisakiNm); // 取引先名
                            dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpDenwaBango); // 電話番号
                            dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpFaxBango); // Fax番号
                            dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                            dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                            dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                            dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                            dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                            #endregion
                            if (Util.ToInt(dtMainLoop.Rows[i]["TORIHIKISAKI_CD"]) == 99)
                            {
                                dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "( 現 金 売 上 計 )"); // 繰越時データ
                            }
                            else
                            {
                                dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "( 燃 料 売 上 計 )"); // 繰越時データ
                            }
                            dpc.SetParam("@ITEM19", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.URIAGEGAKU))); // 月売上計

                            this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                            flag = 1; // 空データ表示フラグ
                            Count++; // 改ページ用カウンタ
                            #endregion
                        }

                        // 改ページ用カウンタが34の倍数の時に実行
                        if (Count % Multiple == 0)
                        {
                            #region 改ページ時処理
                            #region インサートテーブル
                            Sql = new StringBuilder();
                            dpc = new DbParamCollection();
                            Sql.Append("INSERT INTO PR_HN_TBL(");
                            Sql.Append("  GUID");
                            Sql.Append(" ,SORT");
                            Sql.Append(" ,ITEM01");
                            Sql.Append(" ,ITEM02");
                            Sql.Append(" ,ITEM03");
                            Sql.Append(" ,ITEM04");
                            Sql.Append(" ,ITEM05");
                            Sql.Append(" ,ITEM06");
                            Sql.Append(" ,ITEM07");
                            Sql.Append(" ,ITEM08");
                            Sql.Append(" ,ITEM12");
                            Sql.Append(" ,ITEM19");
                            Sql.Append(" ,ITEM20");
                            Sql.Append(" ,ITEM21");
                            Sql.Append(" ,ITEM22");
                            Sql.Append(" ,ITEM23");
                            Sql.Append(" ,ITEM24");
                            Sql.Append(") ");
                            Sql.Append("VALUES(");
                            Sql.Append("  @GUID");
                            Sql.Append(" ,@SORT");
                            Sql.Append(" ,@ITEM01");
                            Sql.Append(" ,@ITEM02");
                            Sql.Append(" ,@ITEM03");
                            Sql.Append(" ,@ITEM04");
                            Sql.Append(" ,@ITEM05");
                            Sql.Append(" ,@ITEM06");
                            Sql.Append(" ,@ITEM07");
                            Sql.Append(" ,@ITEM08");
                            Sql.Append(" ,@ITEM12");
                            Sql.Append(" ,@ITEM19");
                            Sql.Append(" ,@ITEM20");
                            Sql.Append(" ,@ITEM21");
                            Sql.Append(" ,@ITEM22");
                            Sql.Append(" ,@ITEM23");
                            Sql.Append(" ,@ITEM24");
                            Sql.Append(") ");
                            #endregion

                            #region ページヘッダーデータを設定
                            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                            dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                            dbSORT++;
                            dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, tmpTorihikisakiCdAsc); // 取引先コード
                            dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpTorihikisakiNm); // 取引先名
                            dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpDenwaBango); // 電話番号
                            dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpFaxBango); // Fax番号
                            dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                            dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                            dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                            dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                            dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                            dpc.SetParam("@ITEM24", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                            #endregion

                            dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "☆☆ 次頁へ繰越し ☆☆"); // 改ページ時文字列
                            dpc.SetParam("@ITEM19", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.URIAGEGAKU))); // 売上金額合計
                            dpc.SetParam("@ITEM20", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHOHIZEIGAKU))); // 売上金額合計
                            dpc.SetParam("@ITEM21", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.NYUKINGAKU))); // 入金額合計
                            dpc.SetParam("@ITEM22", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高

                            this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                            Count++; // 改ページ用カウンタ
                            #endregion

                            #region 改ページ時処理
                            #region インサートテーブル
                            Sql = new StringBuilder();
                            dpc = new DbParamCollection();
                            Sql.Append("INSERT INTO PR_HN_TBL(");
                            Sql.Append("  GUID");
                            Sql.Append(" ,SORT");
                            Sql.Append(" ,ITEM01");
                            Sql.Append(" ,ITEM02");
                            Sql.Append(" ,ITEM03");
                            Sql.Append(" ,ITEM04");
                            Sql.Append(" ,ITEM05");
                            Sql.Append(" ,ITEM06");
                            Sql.Append(" ,ITEM07");
                            Sql.Append(" ,ITEM08");
                            Sql.Append(" ,ITEM12");
                            Sql.Append(" ,ITEM19");
                            Sql.Append(" ,ITEM20");
                            Sql.Append(" ,ITEM21");
                            Sql.Append(" ,ITEM22");
                            Sql.Append(" ,ITEM23");
                            Sql.Append(") ");
                            Sql.Append("VALUES(");
                            Sql.Append("  @GUID");
                            Sql.Append(" ,@SORT");
                            Sql.Append(" ,@ITEM01");
                            Sql.Append(" ,@ITEM02");
                            Sql.Append(" ,@ITEM03");
                            Sql.Append(" ,@ITEM04");
                            Sql.Append(" ,@ITEM05");
                            Sql.Append(" ,@ITEM06");
                            Sql.Append(" ,@ITEM07");
                            Sql.Append(" ,@ITEM08");
                            Sql.Append(" ,@ITEM12");
                            Sql.Append(" ,@ITEM19");
                            Sql.Append(" ,@ITEM20");
                            Sql.Append(" ,@ITEM21");
                            Sql.Append(" ,@ITEM22");
                            Sql.Append(" ,@ITEM23");
                            Sql.Append(") ");
                            #endregion

                            #region ページヘッダーデータを設定
                            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                            dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                            dbSORT++;
                            dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, tmpTorihikisakiCdAsc); // 取引先コード
                            dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpTorihikisakiNm); // 取引先名
                            dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpDenwaBango); // 電話番号
                            dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpFaxBango); // Fax番号
                            dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                            dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                            dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                            dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                            dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                            #endregion

                            dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "☆☆ 前頁より繰越し ☆☆"); // 改ページ時文字列
                            dpc.SetParam("@ITEM19", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.URIAGEGAKU))); // 売上金額合計
                            dpc.SetParam("@ITEM20", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHOHIZEIGAKU))); // 売上金額合計
                            dpc.SetParam("@ITEM21", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.NYUKINGAKU))); // 入金額合計
                            dpc.SetParam("@ITEM22", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高

                            this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                            Count++; // 改ページ用カウンタ
                            #endregion
                        }

                        // flagが1の時に実行
                        if (flag == 1)
                        {
                            #region 空データの登録
                            #region インサートテーブル
                            Sql = new StringBuilder();
                            dpc = new DbParamCollection();
                            Sql.Append("INSERT INTO PR_HN_TBL(");
                            Sql.Append("  GUID");
                            Sql.Append(" ,SORT");
                            Sql.Append(" ,ITEM01");
                            Sql.Append(" ,ITEM02");
                            Sql.Append(" ,ITEM03");
                            Sql.Append(" ,ITEM04");
                            Sql.Append(" ,ITEM05");
                            Sql.Append(" ,ITEM06");
                            Sql.Append(" ,ITEM07");
                            Sql.Append(" ,ITEM08");
                            Sql.Append(" ,ITEM23");
                            Sql.Append(") ");
                            Sql.Append("VALUES(");
                            Sql.Append("  @GUID");
                            Sql.Append(" ,@SORT");
                            Sql.Append(" ,@ITEM01");
                            Sql.Append(" ,@ITEM02");
                            Sql.Append(" ,@ITEM03");
                            Sql.Append(" ,@ITEM04");
                            Sql.Append(" ,@ITEM05");
                            Sql.Append(" ,@ITEM06");
                            Sql.Append(" ,@ITEM07");
                            Sql.Append(" ,@ITEM08");
                            Sql.Append(" ,@ITEM23");
                            Sql.Append(") ");
                            #endregion

                            #region ページヘッダーデータを設定
                            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                            dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                            dbSORT++;
                            dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, tmpTorihikisakiCdAsc); // 取引先コード
                            dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpTorihikisakiNm); // 取引先名
                            dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpDenwaBango); // 電話番号
                            dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpFaxBango); // Fax番号
                            dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                            dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                            dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                            dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                            dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                            #endregion

                            this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                            flag = 0;
                            Count++; // 改ページ用カウンタ
                            #endregion
                        }

                        // 改ページ用カウンタが34の倍数の時に実行
                        if (Count % Multiple == 0)
                        {
                            #region 改ページ時処理
                            #region インサートテーブル
                            Sql = new StringBuilder();
                            dpc = new DbParamCollection();
                            Sql.Append("INSERT INTO PR_HN_TBL(");
                            Sql.Append("  GUID");
                            Sql.Append(" ,SORT");
                            Sql.Append(" ,ITEM01");
                            Sql.Append(" ,ITEM02");
                            Sql.Append(" ,ITEM03");
                            Sql.Append(" ,ITEM04");
                            Sql.Append(" ,ITEM05");
                            Sql.Append(" ,ITEM06");
                            Sql.Append(" ,ITEM07");
                            Sql.Append(" ,ITEM08");
                            Sql.Append(" ,ITEM12");
                            Sql.Append(" ,ITEM19");
                            Sql.Append(" ,ITEM20");
                            Sql.Append(" ,ITEM21");
                            Sql.Append(" ,ITEM22");
                            Sql.Append(" ,ITEM23");
                            Sql.Append(" ,ITEM24");
                            Sql.Append(") ");
                            Sql.Append("VALUES(");
                            Sql.Append("  @GUID");
                            Sql.Append(" ,@SORT");
                            Sql.Append(" ,@ITEM01");
                            Sql.Append(" ,@ITEM02");
                            Sql.Append(" ,@ITEM03");
                            Sql.Append(" ,@ITEM04");
                            Sql.Append(" ,@ITEM05");
                            Sql.Append(" ,@ITEM06");
                            Sql.Append(" ,@ITEM07");
                            Sql.Append(" ,@ITEM08");
                            Sql.Append(" ,@ITEM12");
                            Sql.Append(" ,@ITEM19");
                            Sql.Append(" ,@ITEM20");
                            Sql.Append(" ,@ITEM21");
                            Sql.Append(" ,@ITEM22");
                            Sql.Append(" ,@ITEM23");
                            Sql.Append(" ,@ITEM24");
                            Sql.Append(") ");
                            #endregion

                            #region ページヘッダーデータを設定
                            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                            dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                            dbSORT++;
                            dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, tmpTorihikisakiCdAsc); // 取引先コード
                            dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpTorihikisakiNm); // 取引先名
                            dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpDenwaBango); // 電話番号
                            dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpFaxBango); // Fax番号
                            dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                            dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                            dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                            dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                            dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                            dpc.SetParam("@ITEM24", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                            #endregion

                            dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "☆☆ 次頁へ繰越し ☆☆"); // 改ページ時文字列
                            dpc.SetParam("@ITEM19", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.URIAGEGAKU))); // 売上金額合計
                            dpc.SetParam("@ITEM20", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHOHIZEIGAKU))); // 売上金額合計
                            dpc.SetParam("@ITEM21", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.NYUKINGAKU))); // 入金額合計
                            dpc.SetParam("@ITEM22", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高

                            this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                            Count++; // 改ページ用カウンタ
                            #endregion

                            #region 改ページ時処理
                            #region インサートテーブル
                            Sql = new StringBuilder();
                            dpc = new DbParamCollection();
                            Sql.Append("INSERT INTO PR_HN_TBL(");
                            Sql.Append("  GUID");
                            Sql.Append(" ,SORT");
                            Sql.Append(" ,ITEM01");
                            Sql.Append(" ,ITEM02");
                            Sql.Append(" ,ITEM03");
                            Sql.Append(" ,ITEM04");
                            Sql.Append(" ,ITEM05");
                            Sql.Append(" ,ITEM06");
                            Sql.Append(" ,ITEM07");
                            Sql.Append(" ,ITEM08");
                            Sql.Append(" ,ITEM12");
                            Sql.Append(" ,ITEM19");
                            Sql.Append(" ,ITEM20");
                            Sql.Append(" ,ITEM21");
                            Sql.Append(" ,ITEM22");
                            Sql.Append(" ,ITEM23");
                            Sql.Append(") ");
                            Sql.Append("VALUES(");
                            Sql.Append("  @GUID");
                            Sql.Append(" ,@SORT");
                            Sql.Append(" ,@ITEM01");
                            Sql.Append(" ,@ITEM02");
                            Sql.Append(" ,@ITEM03");
                            Sql.Append(" ,@ITEM04");
                            Sql.Append(" ,@ITEM05");
                            Sql.Append(" ,@ITEM06");
                            Sql.Append(" ,@ITEM07");
                            Sql.Append(" ,@ITEM08");
                            Sql.Append(" ,@ITEM12");
                            Sql.Append(" ,@ITEM19");
                            Sql.Append(" ,@ITEM20");
                            Sql.Append(" ,@ITEM21");
                            Sql.Append(" ,@ITEM22");
                            Sql.Append(" ,@ITEM23");
                            Sql.Append(") ");
                            #endregion

                            #region ページヘッダーデータを設定
                            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                            dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                            dbSORT++;
                            dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, tmpTorihikisakiCdAsc); // 取引先コード
                            dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpTorihikisakiNm); // 取引先名
                            dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpDenwaBango); // 電話番号
                            dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpFaxBango); // Fax番号
                            dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                            dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                            dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                            dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                            dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                            #endregion

                            dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "☆☆ 前頁より繰越し ☆☆"); // 改ページ時文字列
                            dpc.SetParam("@ITEM19", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.URIAGEGAKU))); // 売上金額合計
                            dpc.SetParam("@ITEM20", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHOHIZEIGAKU))); // 売上金額合計
                            dpc.SetParam("@ITEM21", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.NYUKINGAKU))); // 入金額合計
                            dpc.SetParam("@ITEM22", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高

                            this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                            Count++; // 改ページ用カウンタ
                            #endregion
                        }

                        // 最後のデータ(次回カウンタと総データ数が一致)の時に実行 // 現在の月と次回の月が違っていれば実行 // 現在の取引先コードと次回の取引先コードが違っていれば実行
                        if (dtMainLoop.Rows.Count == j || tmpGetsu != tmpNextGetsu || tmpTorihikisakiCd != tmpNextTorihikisakiCd)
                        {
                            #region 次月繰越テーブルの登録
                            #region インサートテーブル
                            Sql = new StringBuilder();
                            dpc = new DbParamCollection();
                            Sql.Append("INSERT INTO PR_HN_TBL(");
                            Sql.Append("  GUID");
                            Sql.Append(" ,SORT");
                            Sql.Append(" ,ITEM01");
                            Sql.Append(" ,ITEM02");
                            Sql.Append(" ,ITEM03");
                            Sql.Append(" ,ITEM04");
                            Sql.Append(" ,ITEM05");
                            Sql.Append(" ,ITEM06");
                            Sql.Append(" ,ITEM07");
                            Sql.Append(" ,ITEM08");
                            Sql.Append(" ,ITEM12");
                            Sql.Append(" ,ITEM22");
                            Sql.Append(" ,ITEM23");
                            Sql.Append(" ,ITEM24");
                            Sql.Append(") ");
                            Sql.Append("VALUES(");
                            Sql.Append("  @GUID");
                            Sql.Append(" ,@SORT");
                            Sql.Append(" ,@ITEM01");
                            Sql.Append(" ,@ITEM02");
                            Sql.Append(" ,@ITEM03");
                            Sql.Append(" ,@ITEM04");
                            Sql.Append(" ,@ITEM05");
                            Sql.Append(" ,@ITEM06");
                            Sql.Append(" ,@ITEM07");
                            Sql.Append(" ,@ITEM08");
                            Sql.Append(" ,@ITEM12");
                            Sql.Append(" ,@ITEM22");
                            Sql.Append(" ,@ITEM23");
                            Sql.Append(" ,@ITEM24");
                            Sql.Append(") ");
                            #endregion

                            #region ページヘッダーデータを設定
                            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                            dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                            dbSORT++;
                            dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, tmpTorihikisakiCdAsc); // 取引先コード
                            dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpTorihikisakiNm); // 取引先名
                            dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpDenwaBango); // 電話番号
                            dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpFaxBango); // Fax番号
                            dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                            dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                            dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                            dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                            dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                            #endregion

                            dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "◇◇◇ 次月へ繰越し ◇◇◇"); // 繰越時データ
                            dpc.SetParam("@ITEM22", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 期首残高
                            // 取引先が変わらないなら、罫線は点線
                            if ( tmpGetsu != tmpNextGetsu && tmpTorihikisakiCd == tmpNextTorihikisakiCd )
                            {
                                LineType = 0;
                            }
                            else
                            {
                                LineType = 1;
                            }
                            dpc.SetParam("@ITEM24", SqlDbType.VarChar, 200, LineType); // 会社名

                            this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                            // 月合計をクリア
                            MONTH.Clear();
                            flag = 1;// 空データ表示フラグ
                            flagShizai = 0;// 資材空データフラグ
                            flagU01 = 0; // 売上計表示フラグ
                            flagU02 = 0; // 売上計表示フラグ
                            Count++; // 改ページ用カウンタ
                            #endregion
                        }

                        // 改ページ用カウンタが34の倍数の時に実行 // 現在の取引先コードと次回の取引先コードが一致なら実行
                        if (Count % Multiple == 0 && tmpTorihikisakiCd == tmpNextTorihikisakiCd)
                        {
                            #region 改ページ時処理
                            #region インサートテーブル
                            Sql = new StringBuilder();
                            dpc = new DbParamCollection();
                            Sql.Append("INSERT INTO PR_HN_TBL(");
                            Sql.Append("  GUID");
                            Sql.Append(" ,SORT");
                            Sql.Append(" ,ITEM01");
                            Sql.Append(" ,ITEM02");
                            Sql.Append(" ,ITEM03");
                            Sql.Append(" ,ITEM04");
                            Sql.Append(" ,ITEM05");
                            Sql.Append(" ,ITEM06");
                            Sql.Append(" ,ITEM07");
                            Sql.Append(" ,ITEM08");
                            Sql.Append(" ,ITEM12");
                            Sql.Append(" ,ITEM19");
                            Sql.Append(" ,ITEM20");
                            Sql.Append(" ,ITEM21");
                            Sql.Append(" ,ITEM22");
                            Sql.Append(" ,ITEM23");
                            Sql.Append(" ,ITEM24");
                            Sql.Append(") ");
                            Sql.Append("VALUES(");
                            Sql.Append("  @GUID");
                            Sql.Append(" ,@SORT");
                            Sql.Append(" ,@ITEM01");
                            Sql.Append(" ,@ITEM02");
                            Sql.Append(" ,@ITEM03");
                            Sql.Append(" ,@ITEM04");
                            Sql.Append(" ,@ITEM05");
                            Sql.Append(" ,@ITEM06");
                            Sql.Append(" ,@ITEM07");
                            Sql.Append(" ,@ITEM08");
                            Sql.Append(" ,@ITEM12");
                            Sql.Append(" ,@ITEM19");
                            Sql.Append(" ,@ITEM20");
                            Sql.Append(" ,@ITEM21");
                            Sql.Append(" ,@ITEM22");
                            Sql.Append(" ,@ITEM23");
                            Sql.Append(" ,@ITEM24");
                            Sql.Append(") ");
                            #endregion

                            #region ページヘッダーデータを設定
                            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                            dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                            dbSORT++;
                            dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, tmpTorihikisakiCdAsc); // 取引先コード
                            dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpTorihikisakiNm); // 取引先名
                            dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpDenwaBango); // 電話番号
                            dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpFaxBango); // Fax番号
                            dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                            dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                            dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                            dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                            dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                            dpc.SetParam("@ITEM24", SqlDbType.VarChar, 200, 1); // 会社名
                            #endregion

                            dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "☆☆ 次頁へ繰越し ☆☆"); // 改ページ時文字列
                            dpc.SetParam("@ITEM19", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.URIAGEGAKU))); // 売上金額合計
                            dpc.SetParam("@ITEM20", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHOHIZEIGAKU))); // 売上金額合計
                            dpc.SetParam("@ITEM21", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.NYUKINGAKU))); // 入金額合計
                            dpc.SetParam("@ITEM22", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高

                            this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                            Count++; // 改ページ用カウンタ
                            #endregion

                            #region 改ページ時処理
                            #region インサートテーブル
                            Sql = new StringBuilder();
                            dpc = new DbParamCollection();
                            Sql.Append("INSERT INTO PR_HN_TBL(");
                            Sql.Append("  GUID");
                            Sql.Append(" ,SORT");
                            Sql.Append(" ,ITEM01");
                            Sql.Append(" ,ITEM02");
                            Sql.Append(" ,ITEM03");
                            Sql.Append(" ,ITEM04");
                            Sql.Append(" ,ITEM05");
                            Sql.Append(" ,ITEM06");
                            Sql.Append(" ,ITEM07");
                            Sql.Append(" ,ITEM08");
                            Sql.Append(" ,ITEM12");
                            Sql.Append(" ,ITEM19");
                            Sql.Append(" ,ITEM20");
                            Sql.Append(" ,ITEM21");
                            Sql.Append(" ,ITEM22");
                            Sql.Append(" ,ITEM23");
                            Sql.Append(") ");
                            Sql.Append("VALUES(");
                            Sql.Append("  @GUID");
                            Sql.Append(" ,@SORT");
                            Sql.Append(" ,@ITEM01");
                            Sql.Append(" ,@ITEM02");
                            Sql.Append(" ,@ITEM03");
                            Sql.Append(" ,@ITEM04");
                            Sql.Append(" ,@ITEM05");
                            Sql.Append(" ,@ITEM06");
                            Sql.Append(" ,@ITEM07");
                            Sql.Append(" ,@ITEM08");
                            Sql.Append(" ,@ITEM12");
                            Sql.Append(" ,@ITEM19");
                            Sql.Append(" ,@ITEM20");
                            Sql.Append(" ,@ITEM21");
                            Sql.Append(" ,@ITEM22");
                            Sql.Append(" ,@ITEM23");
                            Sql.Append(") ");
                            #endregion

                            #region ページヘッダーデータを設定
                            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                            dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                            dbSORT++;
                            dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, tmpTorihikisakiCdAsc); // 取引先コード
                            dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpTorihikisakiNm); // 取引先名
                            dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpDenwaBango); // 電話番号
                            dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpFaxBango); // Fax番号
                            dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                            dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                            dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                            dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                            dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                            #endregion

                            dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "☆☆ 前頁より繰越し ☆☆"); // 改ページ時文字列
                            dpc.SetParam("@ITEM19", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.URIAGEGAKU))); // 売上金額合計
                            dpc.SetParam("@ITEM20", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHOHIZEIGAKU))); // 売上金額合計
                            dpc.SetParam("@ITEM21", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.NYUKINGAKU))); // 入金額合計
                            dpc.SetParam("@ITEM22", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高

                            this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                            Count++; // 改ページ用カウンタ
                            #endregion
                        }

                        // flagが1の時に実行 // 現在の取引先コードと次回の取引先コードが一致なら実行
                        // if (flag == 1 && tmpTorihikisakiCd == tmpNextTorihikisakiCd)
                        if (flag == 1 && tmpTorihikisakiCd == tmpNextTorihikisakiCd && j < dtMainLoop.Rows.Count)
                        {
                            #region 空データの登録
                            #region インサートテーブル
                            Sql = new StringBuilder();
                            dpc = new DbParamCollection();
                            Sql.Append("INSERT INTO PR_HN_TBL(");
                            Sql.Append("  GUID");
                            Sql.Append(" ,SORT");
                            Sql.Append(" ,ITEM01");
                            Sql.Append(" ,ITEM02");
                            Sql.Append(" ,ITEM03");
                            Sql.Append(" ,ITEM04");
                            Sql.Append(" ,ITEM05");
                            Sql.Append(" ,ITEM06");
                            Sql.Append(" ,ITEM07");
                            Sql.Append(" ,ITEM08");
                            Sql.Append(" ,ITEM23");
                            Sql.Append(") ");
                            Sql.Append("VALUES(");
                            Sql.Append("  @GUID");
                            Sql.Append(" ,@SORT");
                            Sql.Append(" ,@ITEM01");
                            Sql.Append(" ,@ITEM02");
                            Sql.Append(" ,@ITEM03");
                            Sql.Append(" ,@ITEM04");
                            Sql.Append(" ,@ITEM05");
                            Sql.Append(" ,@ITEM06");
                            Sql.Append(" ,@ITEM07");
                            Sql.Append(" ,@ITEM08");
                            Sql.Append(" ,@ITEM23");
                            Sql.Append(") ");
                            #endregion

                            #region ページヘッダーデータを設定
                            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                            dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                            dbSORT++;
                            dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, tmpTorihikisakiCdAsc); // 取引先コード
                            dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpTorihikisakiNm); // 取引先名
                            dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpDenwaBango); // 電話番号
                            dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpFaxBango); // Fax番号
                            dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                            dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                            dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                            dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                            dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                            #endregion

                            this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                            flag = 0;
                            Count++; // 改ページ用カウンタ
                            #endregion
                        }

                        // 改ページ用カウンタが34の倍数の時に実行 // 現在の取引先コードと次回の取引先コードが一致なら実行
                        if (Count % Multiple == 0 && tmpTorihikisakiCd == tmpNextTorihikisakiCd)
                        {
                            #region 改ページ時処理
                            #region インサートテーブル
                            Sql = new StringBuilder();
                            dpc = new DbParamCollection();
                            Sql.Append("INSERT INTO PR_HN_TBL(");
                            Sql.Append("  GUID");
                            Sql.Append(" ,SORT");
                            Sql.Append(" ,ITEM01");
                            Sql.Append(" ,ITEM02");
                            Sql.Append(" ,ITEM03");
                            Sql.Append(" ,ITEM04");
                            Sql.Append(" ,ITEM05");
                            Sql.Append(" ,ITEM06");
                            Sql.Append(" ,ITEM07");
                            Sql.Append(" ,ITEM08");
                            Sql.Append(" ,ITEM12");
                            Sql.Append(" ,ITEM19");
                            Sql.Append(" ,ITEM20");
                            Sql.Append(" ,ITEM21");
                            Sql.Append(" ,ITEM22");
                            Sql.Append(" ,ITEM23");
                            Sql.Append(" ,ITEM24");
                            Sql.Append(") ");
                            Sql.Append("VALUES(");
                            Sql.Append("  @GUID");
                            Sql.Append(" ,@SORT");
                            Sql.Append(" ,@ITEM01");
                            Sql.Append(" ,@ITEM02");
                            Sql.Append(" ,@ITEM03");
                            Sql.Append(" ,@ITEM04");
                            Sql.Append(" ,@ITEM05");
                            Sql.Append(" ,@ITEM06");
                            Sql.Append(" ,@ITEM07");
                            Sql.Append(" ,@ITEM08");
                            Sql.Append(" ,@ITEM12");
                            Sql.Append(" ,@ITEM19");
                            Sql.Append(" ,@ITEM20");
                            Sql.Append(" ,@ITEM21");
                            Sql.Append(" ,@ITEM22");
                            Sql.Append(" ,@ITEM23");
                            Sql.Append(" ,@ITEM24");
                            Sql.Append(") ");
                            #endregion

                            #region ページヘッダーデータを設定
                            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                            dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                            dbSORT++;
                            dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, tmpTorihikisakiCdAsc); // 取引先コード
                            dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpTorihikisakiNm); // 取引先名
                            dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpDenwaBango); // 電話番号
                            dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpFaxBango); // Fax番号
                            dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                            dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                            dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                            dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                            dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                            dpc.SetParam("@ITEM24", SqlDbType.VarChar, 200, 1); // 会社名
                            #endregion

                            dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "☆☆ 次頁へ繰越し ☆☆"); // 改ページ時文字列
                            dpc.SetParam("@ITEM19", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.URIAGEGAKU))); // 売上金額合計
                            dpc.SetParam("@ITEM20", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHOHIZEIGAKU))); // 売上金額合計
                            dpc.SetParam("@ITEM21", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.NYUKINGAKU))); // 入金額合計
                            dpc.SetParam("@ITEM22", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高

                            this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                            Count++; // 改ページ用カウンタ
                            #endregion

                            #region 改ページ時処理
                            #region インサートテーブル
                            Sql = new StringBuilder();
                            dpc = new DbParamCollection();
                            Sql.Append("INSERT INTO PR_HN_TBL(");
                            Sql.Append("  GUID");
                            Sql.Append(" ,SORT");
                            Sql.Append(" ,ITEM01");
                            Sql.Append(" ,ITEM02");
                            Sql.Append(" ,ITEM03");
                            Sql.Append(" ,ITEM04");
                            Sql.Append(" ,ITEM05");
                            Sql.Append(" ,ITEM06");
                            Sql.Append(" ,ITEM07");
                            Sql.Append(" ,ITEM08");
                            Sql.Append(" ,ITEM12");
                            Sql.Append(" ,ITEM19");
                            Sql.Append(" ,ITEM20");
                            Sql.Append(" ,ITEM21");
                            Sql.Append(" ,ITEM22");
                            Sql.Append(" ,ITEM23");
                            Sql.Append(") ");
                            Sql.Append("VALUES(");
                            Sql.Append("  @GUID");
                            Sql.Append(" ,@SORT");
                            Sql.Append(" ,@ITEM01");
                            Sql.Append(" ,@ITEM02");
                            Sql.Append(" ,@ITEM03");
                            Sql.Append(" ,@ITEM04");
                            Sql.Append(" ,@ITEM05");
                            Sql.Append(" ,@ITEM06");
                            Sql.Append(" ,@ITEM07");
                            Sql.Append(" ,@ITEM08");
                            Sql.Append(" ,@ITEM12");
                            Sql.Append(" ,@ITEM19");
                            Sql.Append(" ,@ITEM20");
                            Sql.Append(" ,@ITEM21");
                            Sql.Append(" ,@ITEM22");
                            Sql.Append(" ,@ITEM23");
                            Sql.Append(") ");
                            #endregion

                            #region ページヘッダーデータを設定
                            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                            dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                            dbSORT++;
                            dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, tmpTorihikisakiCdAsc); // 取引先コード
                            dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpTorihikisakiNm); // 取引先名
                            dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpDenwaBango); // 電話番号
                            dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpFaxBango); // Fax番号
                            dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                            dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                            dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                            dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                            dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                            #endregion

                            dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "☆☆ 前頁より繰越し ☆☆"); // 改ページ時文字列
                            dpc.SetParam("@ITEM19", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.URIAGEGAKU))); // 売上金額合計
                            dpc.SetParam("@ITEM20", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHOHIZEIGAKU))); // 売上金額合計
                            dpc.SetParam("@ITEM21", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.NYUKINGAKU))); // 入金額合計
                            dpc.SetParam("@ITEM22", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高

                            this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                            Count++; // 改ページ用カウンタ
                            #endregion
                        }

                    }
                    #region データの準備
                    // 比較用の取引先コードを取得
                    if (j < dtMainLoop.Rows.Count)
                    {
                        tmpNextTorihikisakiCd = Util.ToInt(dtMainLoop.Rows[j]["TORIHIKISAKI_CD"]);
                    }

                    // 現在と次回の取引先コードが不一致の場合 // 比較用月データを初期化 // 改ページ用カウンタを初期化 // 空データ用フラグを初期化 // 残高をクリア
                    if (tmpTorihikisakiCd != tmpNextTorihikisakiCd)
                    {
                        tmpNextGetsu = null;
                        Count = 1;
                        flag = 0;
                        TORIHIKISAKI.Clear();
                    }
                    i++;
                    j++;
                    k++;
                    #endregion
                    #endregion
                }
            }

            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
                "SORT",
                "PR_HN_TBL",
                "GUID = @GUID",
                dpc);

            bool dataFlag;
            if (tmpdtPR_HN_TBL.Rows.Count > 0)
            {
                dataFlag = true;
            }
            else
            {
                dataFlag = false;
            }

            return dataFlag;
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData01_nago()//明細選択時
        {
            #region データ取得準備
            // 入力された情報を元にワークテーブルに更新をする
            DbParamCollection dpc = new DbParamCollection();
            // 日付範囲を西暦にして取得
            DateTime tmpDateFr = Util.ConvAdDate(this.lblJpFr.Text, this.txtJpYearFr.Text,
                    this.txtJpMonthFr.Text, this.txtJpDayFr.Text, this.Dba);
            DateTime tmpDateTo = Util.ConvAdDate(this.lblJpTo.Text, this.txtJpYearTo.Text,
                    this.txtJpMonthTo.Text, this.txtJpDayTo.Text, this.Dba);
            //今月の最後の日
            DateTime tmpLastDay = Util.ConvAdDate(this.lblJpTo.Text, this.txtJpYearTo.Text,
                    this.txtJpMonthTo.Text, Util.ToString(DateTime.DaysInMonth(Util.ToInt(this.txtJpYearTo.Text), Util.ToInt(this.txtJpMonthTo.Text))), this.Dba);

            // 日付範囲を和暦で保持
            string[] tmpjpDateFr = Util.ConvJpDate(tmpDateFr, this.Dba);
            string[] tmpjpDateTo = Util.ConvJpDate(tmpDateTo, this.Dba);

            // 日付表示形式を判断
            int dataHyojiFlag = 0; // 表示日付用変数
            // 入力開始日付が1日でない場合
            if (tmpDateFr.Day != 1)
            {
                dataHyojiFlag = 1;
            }
            // 入力終了日付が、その月の最終日でない場合
            else if (tmpDateTo != tmpLastDay)
            {
                dataHyojiFlag = 1;
            }
            // 入力開始日付と入力終了日付の年又は月が一致でない場合
            if (tmpDateFr.Year != tmpDateTo.Year || tmpDateFr.Month != tmpDateTo.Month)
            {
                dataHyojiFlag = 1;
            }

            string hyojiDate; // 表示用日付            
            if (dataHyojiFlag == 0)
            {
                hyojiDate = string.Format("{0}{1}年{2}月", tmpjpDateFr[0], tmpjpDateFr[2], tmpjpDateFr[3]) + "度";
            }
            else
            {
                hyojiDate = string.Format("{0}{1}年{2}月{3}日", tmpjpDateFr[0], tmpjpDateFr[2], tmpjpDateFr[3], tmpjpDateFr[4])
                          + "～"
                          + string.Format("{0}{1}年{2}月{3}日", tmpjpDateTo[0], tmpjpDateTo[2], tmpjpDateTo[3], tmpjpDateTo[4]);
            }

            // 船主コード設定
            String FUNANUSHI_CD_FR;
            String FUNANUSHI_CD_TO;
            if (Util.ToDecimal(txtFunanushiCdFr.Text) > 0)
            {
                FUNANUSHI_CD_FR = txtFunanushiCdFr.Text;
            }
            else
            {
                FUNANUSHI_CD_FR = "0";
            }
            if (Util.ToDecimal(txtFunanushiCdTo.Text) > 0)
            {
                FUNANUSHI_CD_TO = txtFunanushiCdTo.Text;
            }
            else
            {
                FUNANUSHI_CD_TO = "9999";
            }
            // 会計年度設定
            int kaikeiNendo;
            int kaikeiNendoTo = tmpDateTo.Year;
            if (tmpDateFr.Month <= 3)
            {
                kaikeiNendo = tmpDateFr.Year - 1;
            }
            else
            {
                kaikeiNendo = tmpDateFr.Year;
            }
            //支所コードの退避
            string shishoCd = this.txtMizuageShishoCd.Text;
            // 元帳名設定
            String tmpMotochoNm = "売 上 元 帳";

            StringBuilder Sql = new StringBuilder();
            #endregion

            #region 各業者の期首残を取得
            dpc = new DbParamCollection();
            Sql = new StringBuilder();
            //Sql.Append(" SELECT");
            //Sql.Append(" A.TORIHIKISAKI_CD AS TORIHIKISAKI_CD,");
            //Sql.Append(" MIN(A.TORIHIKISAKI_NM) AS TORIHIKISAKI_NM,");
            //Sql.Append(" MIN(A.DENWA_BANGO) AS DENWA_BANGO,");
            //Sql.Append(" MIN(A.FAX_BANGO) AS FAX_BANGO,");
            //Sql.Append(" SUM(CASE WHEN C.TAISHAKU_KUBUN = D.TAISHAKU_KUBUN THEN C.ZEIKOMI_KINGAKU ELSE (C.ZEIKOMI_KINGAKU * -1) END) AS ZANDAKA,");
            //Sql.Append(" SUM(CASE WHEN C.DENPYO_DATE < @DATE_FR");
            //Sql.Append(" THEN (CASE WHEN C.TAISHAKU_KUBUN = D.TAISHAKU_KUBUN");
            //Sql.Append(" THEN C.ZEIKOMI_KINGAKU ELSE (C.ZEIKOMI_KINGAKU * -1) END) ELSE 0 END) AS KISHUZAN");
            //Sql.Append(" FROM VI_HN_TORIHIKISAKI_JOHO AS A");
            //Sql.Append(" LEFT OUTER JOIN TB_HN_KISHU_ZAN_KAMOKU AS B");
            //Sql.Append(" ON A.KAISHA_CD = B.KAISHA_CD AND 11 = B.MOTOCHO_KUBUN");
            //Sql.Append(" LEFT OUTER JOIN TB_ZM_SHIWAKE_MEISAI AS C");
            //Sql.Append(" ON A.KAISHA_CD = C.KAISHA_CD AND A.TORIHIKISAKI_CD = C.HOJO_KAMOKU_CD AND KAIKEI_NENDO = @KAIKEI_NENDO AND");
            //Sql.Append(" B.KANJO_KAMOKU_CD = C.KANJO_KAMOKU_CD AND C.DENPYO_DATE < @DATE_TO");
            //Sql.Append(" LEFT OUTER JOIN TB_ZM_KANJO_KAMOKU AS D");
            //Sql.Append(" ON B.KAISHA_CD = D.KAISHA_CD AND B.KANJO_KAMOKU_CD = D.KANJO_KAMOKU_CD");
            //Sql.Append(" WHERE A.KAISHA_CD = @KAISHA_CD AND A.TORIHIKISAKI_CD BETWEEN @FUNANUSHI_CD_FR AND @FUNANUSHI_CD_TO AND");
            //Sql.Append(" A.TORIHIKISAKI_KUBUN1 = 1 AND D.KAIKEI_NENDO = @KAIKEI_NENDO");
            //Sql.Append(" GROUP BY A.TORIHIKISAKI_CD");
            //Sql.Append(" ORDER BY A.TORIHIKISAKI_CD ASC");
            //Sql.Append(" SELECT");
            //Sql.Append(" A.TORIHIKISAKI_CD AS TORIHIKISAKI_CD,");
            //Sql.Append(" MIN(A.TORIHIKISAKI_NM) AS TORIHIKISAKI_NM,");
            //Sql.Append(" MIN(A.DENWA_BANGO) AS DENWA_BANGO,");
            //Sql.Append(" MIN(A.FAX_BANGO) AS FAX_BANGO,");
            //Sql.Append(" SUM(CASE WHEN C.TAISHAKU_KUBUN = D.TAISHAKU_KUBUN");
            //Sql.Append(" THEN C.ZEIKOMI_KINGAKU ELSE (C.ZEIKOMI_KINGAKU * -1) END) AS ZANDAKA,");
            //Sql.Append(" SUM(CASE WHEN C.DENPYO_DATE < @DATE_FR");
            //Sql.Append(" THEN (CASE WHEN C.TAISHAKU_KUBUN = D.TAISHAKU_KUBUN THEN C.ZEIKOMI_KINGAKU ELSE (C.ZEIKOMI_KINGAKU * -1) END) ELSE 0 END) AS KISHUZAN");
            //Sql.Append(" FROM TB_CM_TORIHIKISAKI AS A");
            //Sql.Append(" INNER JOIN TB_HN_TORIHIKISAKI_JOHO AS A1");
            //Sql.Append("    ON  A.KAISHA_CD = A1.KAISHA_CD ");
            //Sql.Append("    AND A.TORIHIKISAKI_CD = A1.TORIHIKISAKI_CD ");
            //Sql.Append(" LEFT OUTER JOIN TB_ZM_SHIWAKE_MEISAI AS C");
            //Sql.Append("    ON A.KAISHA_CD = C.KAISHA_CD ");
            //Sql.Append("    AND A.TORIHIKISAKI_CD = C.HOJO_KAMOKU_CD");
            //Sql.Append(" INNER JOIN TB_HN_KISHU_ZAN_KAMOKU AS B");
            //Sql.Append("    ON  B.KAISHA_CD = C.KAISHA_CD ");
            //Sql.Append("    AND B.SHISHO_CD = C.SHISHO_CD ");
            //Sql.Append("    AND B.KANJO_KAMOKU_CD = C.KANJO_KAMOKU_CD ");
            //Sql.Append("    AND 11 = B.MOTOCHO_KUBUN");
            //Sql.Append("    AND C.DENPYO_DATE < @DATE_TO");
            //Sql.Append(" LEFT OUTER JOIN TB_ZM_KANJO_KAMOKU AS D");
            //Sql.Append("    ON  C.KAISHA_CD = D.KAISHA_CD ");
            //Sql.Append("    AND C.KAIKEI_NENDO = D.KAIKEI_NENDO");
            //Sql.Append("    AND C.KANJO_KAMOKU_CD = D.KANJO_KAMOKU_CD ");
            //Sql.Append(" WHERE  ");
            //Sql.Append(" A.KAISHA_CD = @KAISHA_CD AND ");
            //Sql.Append(" C.SHISHO_CD = @SHISHO_CD AND ");
            //Sql.Append(" D.KAIKEI_NENDO = @KAIKEI_NENDO AND ");
            //Sql.Append(" A.TORIHIKISAKI_CD BETWEEN @FUNANUSHI_CD_FR AND @FUNANUSHI_CD_TO AND ");
            //Sql.Append(" A1.TORIHIKISAKI_KUBUN1 = 1 ");
            //Sql.Append(" GROUP BY ");
            //Sql.Append(" A.TORIHIKISAKI_CD ");
            //Sql.Append(" ORDER BY ");
            //Sql.Append(" A.TORIHIKISAKI_CD ASC");
            Sql.Append(" SELECT ");
            Sql.Append(" A.TORIHIKISAKI_CD,");
            Sql.Append(" A.TORIHIKISAKI_NM,");
            Sql.Append(" A.DENWA_BANGO,");
            Sql.Append(" A.FAX_BANGO,");
            Sql.Append(" ISNULL(Z.ZANDAKA, 0) AS ZANDAKA,");
            Sql.Append(" ISNULL(Z.KISHUZAN, 0) AS KISHUZAN");
            Sql.Append(" FROM TB_CM_TORIHIKISAKI AS A ");
            Sql.Append(" INNER JOIN TB_HN_TORIHIKISAKI_JOHO AS A1");
            Sql.Append(" 	ON	A.KAISHA_CD = A1.KAISHA_CD");
            Sql.Append(" 	AND A.TORIHIKISAKI_CD = A1.TORIHIKISAKI_CD");
            Sql.Append(" 	AND A1.TORIHIKISAKI_KUBUN1 = 1  ");
            Sql.Append(" LEFT JOIN ");
            Sql.Append(" (");
            Sql.Append(" 	SELECT");
            Sql.Append(" 		C.KAISHA_CD,");
            Sql.Append(" 		C.HOJO_KAMOKU_CD AS TORIHIKISAKI_CD,");
            Sql.Append(" 		SUM(CASE WHEN C.TAISHAKU_KUBUN = D.TAISHAKU_KUBUN THEN C.ZEIKOMI_KINGAKU ELSE (C.ZEIKOMI_KINGAKU * -1) END) AS ZANDAKA,");
            Sql.Append(" 		SUM(CASE WHEN C.DENPYO_DATE < @DATE_FR THEN (CASE WHEN C.TAISHAKU_KUBUN = D.TAISHAKU_KUBUN THEN C.ZEIKOMI_KINGAKU ELSE (C.ZEIKOMI_KINGAKU * -1) END) ELSE 0 END) AS KISHUZAN ");
            Sql.Append(" 	FROM TB_ZM_SHIWAKE_MEISAI AS C ");
            Sql.Append(" 	INNER JOIN TB_HN_KISHU_ZAN_KAMOKU AS B ");
            Sql.Append(" 		ON  B.KAISHA_CD = C.KAISHA_CD  ");
            Sql.Append(" 		AND B.SHISHO_CD = C.SHISHO_CD  ");
            Sql.Append(" 		AND B.KANJO_KAMOKU_CD = C.KANJO_KAMOKU_CD  ");
            Sql.Append(" 		AND 11 = B.MOTOCHO_KUBUN ");
            Sql.Append(" 		AND C.DENPYO_DATE < @DATE_TO ");
            Sql.Append(" 		AND C.KAIKEI_NENDO = @KAIKEI_NENDO ");
            Sql.Append(" 	LEFT OUTER JOIN TB_ZM_KANJO_KAMOKU AS D ");
            Sql.Append(" 		ON C.KAISHA_CD = D.KAISHA_CD  ");
            Sql.Append(" 		AND C.KAIKEI_NENDO = D.KAIKEI_NENDO");
            Sql.Append(" 		AND C.KANJO_KAMOKU_CD = D.KANJO_KAMOKU_CD  ");
            Sql.Append(" 	WHERE   ");
            Sql.Append(" 		C.KAISHA_CD = @KAISHA_CD AND  ");
            Sql.Append(" 		C.SHISHO_CD = @SHISHO_CD AND  ");
            Sql.Append(" 		D.KAIKEI_NENDO = @KAIKEI_NENDO AND  ");
            Sql.Append(" 		C.HOJO_KAMOKU_CD BETWEEN @FUNANUSHI_CD_FR AND @FUNANUSHI_CD_TO ");
            Sql.Append(" 	GROUP BY");
            Sql.Append(" 	C.KAISHA_CD,");
            Sql.Append(" 	C.SHISHO_CD,");
            Sql.Append(" 	C.HOJO_KAMOKU_CD");
            Sql.Append(" ) Z");
            Sql.Append(" ON	A.KAISHA_CD = Z.KAISHA_CD");
            Sql.Append(" AND A.TORIHIKISAKI_CD = Z.TORIHIKISAKI_CD");
            Sql.Append(" ORDER BY  ");
            Sql.Append(" A.TORIHIKISAKI_CD ASC");

            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.VarChar, 4, kaikeiNendo);
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@DATE_FR", SqlDbType.VarChar, 10, tmpDateFr.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@DATE_TO", SqlDbType.VarChar, 10, tmpDateTo.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@FUNANUSHI_CD_FR", SqlDbType.VarChar, 6, FUNANUSHI_CD_FR);
            dpc.SetParam("@FUNANUSHI_CD_TO", SqlDbType.VarChar, 6, FUNANUSHI_CD_TO);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);

            DataTable dtKishu = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
            #endregion

            #region メインデータを取得
            // 日付範囲から発生している取引先データを取得
            dpc = new DbParamCollection();
            Sql = new StringBuilder();
            //Sql.Append(" SELECT");
            ////Sql.Append("    A.KAIIN_BANGO AS TORIHIKISAKI_CD,");
            //Sql.Append("    A.TOKUISAKI_CD AS TORIHIKISAKI_CD,");
            //Sql.Append("    A.DENPYO_DATE AS DENPYO_DATE,");
            //Sql.Append("    A.DENPYO_BANGO AS DENPYO_BANGO,");
            //Sql.Append("    A.GYO_BANGO AS GYO_BANGO,");
            //Sql.Append("    NULL AS TEKIYO,");
            //Sql.Append("    NULL AS ZEIKOMI_KINGAKU,");
            //Sql.Append("    A.TORIHIKI_KUBUN1 AS TORIHIKI_KUBUN,");
            //Sql.Append("    ((A.TORIHIKI_KUBUN1 * 10) + A.TORIHIKI_KUBUN2) AS TORIHIKI_KUBUN1_AND_2,");
            //Sql.Append("    A.TORIHIKI_KUBUN_NM AS TORIHIKI_KUBUN_NM,");
            //Sql.Append("    A.SHOHIN_CD AS SHOHIN_CD,");
            //Sql.Append("    A.SHOHIN_NM AS SHOHIN_NM,");
            //Sql.Append("    B.KIKAKU AS KIKAKU,");
            //Sql.Append("    A.IRISU AS IRISU,");    
            //Sql.Append("    (A.SURYO * A.ENZAN_HOHO) AS SURYO,");
            //Sql.Append("    A.TANKA AS TANKA,");
            //Sql.Append("    (A.URIAGE_KINGAKU * A.ENZAN_HOHO) AS URIAGE_KINGAKU,");
            //Sql.Append("    (A.SHOHIZEIGAKU * A.ENZAN_HOHO) AS SHOHIZEI,");
            //Sql.Append("    A.ZEI_RITSU AS ZEI_RITSU,");
            //Sql.Append("    A.SHOHIZEI_NYURYOKU_HOHO AS SHOHIZEI_NYURYOKU_HOHO,");
            //Sql.Append("    3 AS RANK");
            ////Sql.Append(" FROM VI_HN_TORIHIKI_MOTOCHO AS A");
            ////Sql.Append("    LEFT OUTER JOIN TB_HN_SHOHIN AS B");
            ////Sql.Append("    ON A.KAISHA_CD = B.KAISHA_CD AND A.SHOHIN_CD = B.SHOHIN_CD AND B.BARCODE1 != '999'");
            ////Sql.Append("    WHERE A.KAISHA_CD = @KAISHA_CD AND A.DENPYO_KUBUN = 1 AND A.DENPYO_DATE BETWEEN @DATE_FR AND @DATE_TO AND");
            ////Sql.Append("    A.KAIIN_BANGO BETWEEN @FUNANUSHI_CD_FR AND @FUNANUSHI_CD_TO");
            //Sql.Append(" FROM VI_HN_TORIHIKI_MOTOCHO AS A");
            //Sql.Append(" LEFT OUTER JOIN TB_HN_SHOHIN AS B");
            //Sql.Append(" ON  A.KAISHA_CD = B.KAISHA_CD ");
            //Sql.Append(" AND A.SHISHO_CD = B.SHISHO_CD ");
            //Sql.Append(" AND A.SHOHIN_CD = B.SHOHIN_CD ");
            //Sql.Append(" WHERE");
            //Sql.Append(" A.KAISHA_CD = @KAISHA_CD AND");
            //Sql.Append(" A.SHISHO_CD = @SHISHO_CD AND");
            //Sql.Append(" A.DENPYO_KUBUN = 1 AND");
            //Sql.Append(" A.DENPYO_DATE BETWEEN @DATE_FR AND @DATE_TO AND");
            //Sql.Append(" A.TOKUISAKI_CD BETWEEN @FUNANUSHI_CD_FR AND @FUNANUSHI_CD_TO ");

            //Sql.Append(" UNION ALL");
            //Sql.Append(" SELECT");
            //Sql.Append("    B.HOJO_KAMOKU_CD AS TORIHIKISAKI_CD,");
            //Sql.Append("    B.DENPYO_DATE AS DENPYO_DATE,");
            //Sql.Append("    B.DENPYO_BANGO AS DENPYO_BANGO,");
            //Sql.Append("    B.GYO_BANGO AS GYO_BANGO,");
            //Sql.Append("    B.TEKIYO AS TEKIYO,");
            //Sql.Append("    B.ZEIKOMI_KINGAKU AS ZEIKOMI_KINGAKU,");
            //Sql.Append("    9 AS TORIHIKI_KUBUN,");
            //Sql.Append("    NULL AS TORIHIKI_KUBUN1_AND_2,");
            //Sql.Append("    NULL AS TORIHIKI_KUBUN_NM,");
            //Sql.Append("    NULL AS SHOHIN_CD,");
            //Sql.Append("    NULL AS SHOHIN_NM,");
            //Sql.Append("    NULL AS KIKAKU,");
            //Sql.Append("    NULL AS IRISU,");
            //Sql.Append("    NULL AS SURYO,");
            //Sql.Append("    NULL AS TANKA,");
            //Sql.Append("    NULL AS URIAGE_KINGAKU,");
            //Sql.Append("    NULL AS SHOHIZEI,");
            //Sql.Append("    NULL AS ZEI_RITSU,");
            //Sql.Append("    NULL AS SHOHIZEI_NYURYOKU_HOHO,");
            //Sql.Append("    1 AS RANK");
            //Sql.Append(" FROM TB_HN_NYUKIN_KAMOKU AS A");
            //Sql.Append("  LEFT OUTER JOIN TB_ZM_SHIWAKE_MEISAI AS B");
            //Sql.Append("    ON A.KAISHA_CD = B.KAISHA_CD ");
            //Sql.Append("    AND 1 = B.DENPYO_KUBUN ");
            //Sql.Append("    AND A.KANJO_KAMOKU_CD = B.KANJO_KAMOKU_CD ");
            //Sql.Append("    AND (B.KAIKEI_NENDO = @KAIKEI_NENDO OR B.KAIKEI_NENDO = @KAIKEI_NENDO_TO)");
            //Sql.Append("  LEFT OUTER JOIN TB_ZM_KANJO_KAMOKU AS C");
            //Sql.Append("    ON A.KAISHA_CD = C.KAISHA_CD ");
            //Sql.Append("    AND A.KANJO_KAMOKU_CD = C.KANJO_KAMOKU_CD ");
            //Sql.Append("    AND C.KAIKEI_NENDO = @KAIKEI_NENDO");
            //Sql.Append("  LEFT OUTER JOIN VI_HN_TORIHIKISAKI_JOHO AS D");
            //Sql.Append("    ON B.KAISHA_CD = D.KAISHA_CD ");
            //Sql.Append("    AND B.HOJO_KAMOKU_CD = D.TORIHIKISAKI_CD");
            //Sql.Append(" WHERE ");
            //Sql.Append("    A.KAISHA_CD = @KAISHA_CD AND ");
            //Sql.Append("    A.MOTOCHO_KUBUN = 11 AND");
            //Sql.Append("    B.HOJO_KAMOKU_CD BETWEEN @FUNANUSHI_CD_FR AND @FUNANUSHI_CD_TO AND ");
            //Sql.Append("    B.DENPYO_DATE Between @DATE_FR AND @DATE_TO AND");
            //Sql.Append("    B.TAISHAKU_KUBUN <> C.TAISHAKU_KUBUN AND ");
            //Sql.Append("    (ISNULL(B.SHIWAKE_SAKUSEI_KUBUN,0) = 0 AND ");
            //Sql.Append("    B.ZEI_KUBUN <> 10) AND");
            //Sql.Append("    D.TORIHIKISAKI_KUBUN1 = 1");
            //Sql.Append(" UNION ALL");
            //Sql.Append(" SELECT");
            //Sql.Append("    B.HOJO_KAMOKU_CD AS TORIHIKISAKI_CD,");
            //Sql.Append("    B.DENPYO_DATE AS DENPYO_DATE,");
            //Sql.Append("    B.DENPYO_BANGO AS DENPYO_BANGO,");
            //Sql.Append("    B.GYO_BANGO AS GYO_BANGO,");
            //Sql.Append("    B.TEKIYO AS TEKIYO,");
            //Sql.Append("    B.ZEIKOMI_KINGAKU * -1 AS ZEIKOMI_KINGAKU,");
            //Sql.Append("    9 AS TORIHIKI_KUBUN,");
            //Sql.Append("    NULL AS TORIHIKI_KUBUN1_AND_2,");
            //Sql.Append("    NULL AS TORIHIKI_KUBUN_NM,");
            //Sql.Append("    NULL AS SHOHIN_CD,");
            //Sql.Append("    NULL AS SHOHIN_NM,");
            //Sql.Append("    NULL AS KIKAKU,");
            //Sql.Append("    NULL AS IRISU,");
            //Sql.Append("    NULL AS SURYO,");
            //Sql.Append("    NULL AS TANKA,");
            //Sql.Append("    NULL AS URIAGE_KINGAKU,");
            //Sql.Append("    NULL AS SHOHIZEI,");
            //Sql.Append("    NULL AS ZEI_RITSU,");
            //Sql.Append("    NULL AS SHOHIZEI_NYURYOKU_HOHO,");
            //Sql.Append("    2 AS RANK");
            //Sql.Append(" FROM TB_HN_NYUKIN_KAMOKU AS A");
            //Sql.Append("  LEFT OUTER JOIN TB_ZM_SHIWAKE_MEISAI AS B");
            //Sql.Append("    ON  A.KAISHA_CD = B.KAISHA_CD ");
            //Sql.Append("    AND 1 = B.DENPYO_KUBUN ");
            //Sql.Append("    AND A.KANJO_KAMOKU_CD = B.KANJO_KAMOKU_CD ");
            //Sql.Append("    AND B.KAIKEI_NENDO = @KAIKEI_NENDO");
            //Sql.Append("  LEFT OUTER JOIN TB_ZM_KANJO_KAMOKU AS C");
            //Sql.Append("    ON  A.KAISHA_CD = C.KAISHA_CD ");
            //Sql.Append("    AND A.KANJO_KAMOKU_CD = C.KANJO_KAMOKU_CD ");
            //Sql.Append("    AND C.KAIKEI_NENDO = @KAIKEI_NENDO");
            //Sql.Append("  LEFT OUTER JOIN VI_HN_TORIHIKISAKI_JOHO AS D");
            //Sql.Append("    ON  B.KAISHA_CD = D.KAISHA_CD ");
            //Sql.Append("    AND B.HOJO_KAMOKU_CD = D.TORIHIKISAKI_CD");
            //Sql.Append(" WHERE ");
            //Sql.Append("    A.KAISHA_CD = @KAISHA_CD AND ");
            //Sql.Append("    A.MOTOCHO_KUBUN = 11 AND");
            //Sql.Append("    B.HOJO_KAMOKU_CD BETWEEN @FUNANUSHI_CD_FR AND @FUNANUSHI_CD_TO AND ");
            //Sql.Append("    B.DENPYO_DATE Between @DATE_FR AND @DATE_TO AND");
            //Sql.Append("    B.TAISHAKU_KUBUN = C.TAISHAKU_KUBUN AND ");
            //Sql.Append("    B.DENPYO_BANGO NOT IN (");
            //Sql.Append(" SELECT ");
            //Sql.Append("    DENPYO_BANGO ");
            //Sql.Append(" FROM TB_ZM_SHIWAKE_MEISAI AS B1 ");
            //Sql.Append(" WHERE B1.TAISHAKU_KUBUN = 2 AND B1.ZEI_KUBUN = 10 AND B1.DENPYO_DATE BETWEEN @DATE_FR AND @DATE_TO AND");
            //Sql.Append("    KAIKEI_NENDO = @KAIKEI_NENDO) AND D.TORIHIKISAKI_KUBUN1 = 1");
            //Sql.Append(" ORDER BY TORIHIKISAKI_CD ASC, RANK ASC, TORIHIKI_KUBUN1_AND_2 ASC, A.DENPYO_DATE ASC, DENPYO_BANGO ASC, GYO_BANGO ASC");
            Sql.Append(" SELECT ");
            Sql.Append(" A.TOKUISAKI_CD AS TORIHIKISAKI_CD,");
            Sql.Append(" A.DENPYO_DATE AS DENPYO_DATE,");
            Sql.Append(" A.DENPYO_BANGO AS DENPYO_BANGO,");
            Sql.Append(" A.GYO_BANGO AS GYO_BANGO,");
            Sql.Append(" NULL AS TEKIYO,");
            Sql.Append(" NULL AS ZEIKOMI_KINGAKU,");
            Sql.Append(" A.TORIHIKI_KUBUN1 AS TORIHIKI_KUBUN,");
            Sql.Append(" ((A.TORIHIKI_KUBUN1 * 10) + A.TORIHIKI_KUBUN2) AS TORIHIKI_KUBUN1_AND_2,");
            Sql.Append(" A.TORIHIKI_KUBUN_NM AS TORIHIKI_KUBUN_NM,");
            Sql.Append(" A.SHOHIN_CD AS SHOHIN_CD,");
            Sql.Append(" A.SHOHIN_NM AS SHOHIN_NM,");
            Sql.Append(" B.KIKAKU AS KIKAKU,");
            Sql.Append(" A.IRISU AS IRISU,");
            Sql.Append(" (A.SURYO * A.ENZAN_HOHO) AS SURYO,");
            Sql.Append(" A.TANKA AS TANKA,");
            Sql.Append(" (A.URIAGE_KINGAKU * A.ENZAN_HOHO) AS URIAGE_KINGAKU,");
            Sql.Append(" (A.SHOHIZEIGAKU * A.ENZAN_HOHO) AS SHOHIZEI,");
            Sql.Append(" A.ZEI_RITSU AS ZEI_RITSU,");
            Sql.Append(" A.SHOHIZEI_NYURYOKU_HOHO AS SHOHIZEI_NYURYOKU_HOHO,");
            Sql.Append(" 3 AS RANK ");
            Sql.Append(" FROM VI_HN_TORIHIKI_MOTOCHO AS A ");
            Sql.Append(" LEFT OUTER JOIN TB_HN_SHOHIN AS B ");
            Sql.Append(" 	ON  A.KAISHA_CD = B.KAISHA_CD  ");
            Sql.Append(" 	AND A.SHISHO_CD = B.SHISHO_CD  ");
            Sql.Append(" 	AND A.SHOHIN_CD = B.SHOHIN_CD  ");
            Sql.Append(" WHERE ");
            Sql.Append(" 	A.KAISHA_CD = @KAISHA_CD AND ");
            Sql.Append(" 	A.SHISHO_CD = @SHISHO_CD AND ");
            Sql.Append(" 	A.DENPYO_KUBUN = 1 AND ");
            Sql.Append(" 	A.DENPYO_DATE BETWEEN @DATE_FR AND @DATE_TO AND ");
            Sql.Append(" 	A.TOKUISAKI_CD BETWEEN @FUNANUSHI_CD_FR AND @FUNANUSHI_CD_TO ");
            Sql.Append(" UNION ALL ");
            Sql.Append(" SELECT    ");
            Sql.Append(" 	B.HOJO_KAMOKU_CD AS TORIHIKISAKI_CD,");
            Sql.Append(" 	B.DENPYO_DATE AS DENPYO_DATE,");
            Sql.Append(" 	B.DENPYO_BANGO AS DENPYO_BANGO,");
            Sql.Append(" 	B.GYO_BANGO AS GYO_BANGO,");
            Sql.Append(" 	B.TEKIYO AS TEKIYO,");
            Sql.Append(" 	CASE WHEN C.TAISHAKU_KUBUN = B.TAISHAKU_KUBUN THEN -1 * B.ZEIKOMI_KINGAKU ELSE B.ZEIKOMI_KINGAKU END AS ZEIKOMI_KINGAKU,");
            Sql.Append(" 	9 AS TORIHIKI_KUBUN,");
            Sql.Append(" 	NULL AS TORIHIKI_KUBUN1_AND_2,");
            Sql.Append(" 	NULL AS TORIHIKI_KUBUN_NM,");
            Sql.Append(" 	NULL AS SHOHIN_CD,");
            Sql.Append(" 	NULL AS SHOHIN_NM,");
            Sql.Append(" 	NULL AS KIKAKU,");
            Sql.Append(" 	NULL AS IRISU,");
            Sql.Append(" 	NULL AS SURYO,");
            Sql.Append(" 	NULL AS TANKA,");
            Sql.Append(" 	NULL AS URIAGE_KINGAKU,");
            Sql.Append(" 	NULL AS SHOHIZEI,");
            Sql.Append(" 	NULL AS ZEI_RITSU,");
            Sql.Append(" 	NULL AS SHOHIZEI_NYURYOKU_HOHO,");
            Sql.Append(" 	CASE WHEN C.TAISHAKU_KUBUN = B.TAISHAKU_KUBUN THEN 2 ELSE 1 END RANK");
            Sql.Append(" FROM TB_HN_NYUKIN_KAMOKU AS A  ");
            Sql.Append(" LEFT OUTER JOIN TB_ZM_SHIWAKE_MEISAI AS B    ");
            Sql.Append(" 	ON 	A.KAISHA_CD = B.KAISHA_CD     ");
            Sql.Append(" 	AND A.SHISHO_CD = B.SHISHO_CD");
            Sql.Append(" 	AND 1 = B.DENPYO_KUBUN     ");
            Sql.Append(" 	AND A.KANJO_KAMOKU_CD = B.KANJO_KAMOKU_CD     ");
            Sql.Append(" LEFT OUTER JOIN TB_ZM_KANJO_KAMOKU AS C    ");
            Sql.Append(" 	ON 	B.KAISHA_CD = C.KAISHA_CD     ");
            Sql.Append(" 	AND B.KANJO_KAMOKU_CD = C.KANJO_KAMOKU_CD     ");
            Sql.Append(" 	AND B.KAIKEI_NENDO = C.KAIKEI_NENDO  ");
            //Sql.Append(" LEFT OUTER JOIN VI_HN_TORIHIKISAKI_JOHO AS D ");
            Sql.Append(" LEFT OUTER JOIN TB_HN_TORIHIKISAKI_JOHO AS D ");
            Sql.Append(" 	ON	B.KAISHA_CD = D.KAISHA_CD     ");
            Sql.Append(" 	AND B.HOJO_KAMOKU_CD = D.TORIHIKISAKI_CD ");
            Sql.Append(" WHERE     ");
            Sql.Append(" 	A.KAISHA_CD = @KAISHA_CD AND     ");
            Sql.Append(" 	B.KAIKEI_NENDO = @KAIKEI_NENDO AND");
            Sql.Append(" 	A.MOTOCHO_KUBUN = 11 AND    ");
            Sql.Append(" 	B.HOJO_KAMOKU_CD BETWEEN @FUNANUSHI_CD_FR AND @FUNANUSHI_CD_TO AND     ");
            Sql.Append(" 	B.DENPYO_DATE Between @DATE_FR AND @DATE_TO AND    ");
            Sql.Append(" 	ISNULL(B.SHIWAKE_SAKUSEI_KUBUN,0) = 0 AND     ");
            Sql.Append(" 	D.TORIHIKISAKI_KUBUN1 = 1 ");
            Sql.Append(" ORDER BY ");
            Sql.Append(" 	TORIHIKISAKI_CD ASC,");
            Sql.Append(" 	DENPYO_DATE ASC,");
            Sql.Append(" 	DENPYO_BANGO ASC,");
            Sql.Append(" 	TORIHIKI_KUBUN1_AND_2 ASC,");
            Sql.Append(" 	GYO_BANGO ASC,");
            Sql.Append(" 	RANK ASC");

            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.VarChar, 4, kaikeiNendo);
            dpc.SetParam("@KAIKEI_NENDO_TO", SqlDbType.VarChar, 4, kaikeiNendoTo);
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@DATE_FR", SqlDbType.VarChar, 10, tmpDateFr.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@DATE_TO", SqlDbType.VarChar, 10, tmpDateTo.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@FUNANUSHI_CD_FR", SqlDbType.VarChar, 6, FUNANUSHI_CD_FR);
            dpc.SetParam("@FUNANUSHI_CD_TO", SqlDbType.VarChar, 6, FUNANUSHI_CD_TO);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);

            DataTable dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
            #endregion

            #region メインデータが0の場合
            if (dtMainLoop.Rows.Count == 0)
            {
                int i = 0; // ループ用カウント変数
                int flag = 0; // 表示フラグ
                List<int> tirihikisakiCd = new List<int>(); // 取引先番号格納用変数
                while (dtKishu.Rows.Count > i)
                {
                    // if (Util.ToInt(dtKishu.Rows[i]["KISHUZAN"]) > 0)
                    if (Util.ToInt(dtKishu.Rows[i]["KISHUZAN"]) != 0)
                    {
                        tirihikisakiCd.Add(i);
                        flag = 1;
                    }
                    i++;
                }
                if (flag != 0)
                {
                    i = 0;
                    int dbSORT = 1;
                    while (tirihikisakiCd.Count > i)
                    {
                        #region 前月繰越テーブルの登録
                        #region インサートテーブル
                        Sql = new StringBuilder();
                        dpc = new DbParamCollection();
                        Sql.Append("INSERT INTO PR_HN_TBL(");
                        Sql.Append("  GUID");
                        Sql.Append(" ,SORT");
                        Sql.Append(" ,ITEM01");
                        Sql.Append(" ,ITEM02");
                        Sql.Append(" ,ITEM03");
                        Sql.Append(" ,ITEM04");
                        Sql.Append(" ,ITEM05");
                        Sql.Append(" ,ITEM06");
                        Sql.Append(" ,ITEM07");
                        Sql.Append(" ,ITEM08");
                        Sql.Append(" ,ITEM12");
                        Sql.Append(" ,ITEM21");
                        Sql.Append(" ,ITEM22");
                        Sql.Append(" ,ITEM23");
                        Sql.Append(") ");
                        Sql.Append("VALUES(");
                        Sql.Append("  @GUID");
                        Sql.Append(" ,@SORT");
                        Sql.Append(" ,@ITEM01");
                        Sql.Append(" ,@ITEM02");
                        Sql.Append(" ,@ITEM03");
                        Sql.Append(" ,@ITEM04");
                        Sql.Append(" ,@ITEM05");
                        Sql.Append(" ,@ITEM06");
                        Sql.Append(" ,@ITEM07");
                        Sql.Append(" ,@ITEM08");
                        Sql.Append(" ,@ITEM12");
                        Sql.Append(" ,@ITEM21");
                        Sql.Append(" ,@ITEM22");
                        Sql.Append(" ,@ITEM23");
                        Sql.Append(") ");
                        #endregion

                        #region ページヘッダーデータを設定
                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                        dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                        dbSORT++;
                        dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dtKishu.Rows[tirihikisakiCd[i]]["TORIHIKISAKI_CD"]); // 取引先コード
                        dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtKishu.Rows[tirihikisakiCd[i]]["TORIHIKISAKI_NM"]); // 取引先名
                        dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dtKishu.Rows[tirihikisakiCd[i]]["DENWA_BANGO"]); // 電話番号
                        dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtKishu.Rows[tirihikisakiCd[i]]["FAX_BANGO"]); // Fax番号
                        dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                        dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                        dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                        dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                        dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                        #endregion

                        dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "◇◇◇ 前月より繰越し ◇◇◇"); // 繰越時データ
                        dpc.SetParam("@ITEM21", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(dtKishu.Rows[tirihikisakiCd[i]]["KISHUZAN"]))); // 期首残高
                        dpc.SetParam("@ITEM23", SqlDbType.VarChar, 20, 0); // 0か1を判断 0は点線

                        this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                        #endregion

                        #region 空データの登録
                        #region インサートテーブル
                        Sql = new StringBuilder();
                        dpc = new DbParamCollection();
                        Sql.Append("INSERT INTO PR_HN_TBL(");
                        Sql.Append("  GUID");
                        Sql.Append(" ,SORT");
                        Sql.Append(" ,ITEM01");
                        Sql.Append(" ,ITEM02");
                        Sql.Append(" ,ITEM03");
                        Sql.Append(" ,ITEM04");
                        Sql.Append(" ,ITEM05");
                        Sql.Append(" ,ITEM06");
                        Sql.Append(" ,ITEM07");
                        Sql.Append(" ,ITEM08");
                        Sql.Append(" ,ITEM22");
                        Sql.Append(" ,ITEM23");
                        Sql.Append(") ");
                        Sql.Append("VALUES(");
                        Sql.Append("  @GUID");
                        Sql.Append(" ,@SORT");
                        Sql.Append(" ,@ITEM01");
                        Sql.Append(" ,@ITEM02");
                        Sql.Append(" ,@ITEM03");
                        Sql.Append(" ,@ITEM04");
                        Sql.Append(" ,@ITEM05");
                        Sql.Append(" ,@ITEM06");
                        Sql.Append(" ,@ITEM07");
                        Sql.Append(" ,@ITEM08");
                        Sql.Append(" ,@ITEM22");
                        Sql.Append(" ,@ITEM23");
                        Sql.Append(") ");
                        #endregion

                        #region ページヘッダーデータを設定
                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                        dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                        dbSORT++;
                        dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dtKishu.Rows[tirihikisakiCd[i]]["TORIHIKISAKI_CD"]); // 取引先コード
                        dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtKishu.Rows[tirihikisakiCd[i]]["TORIHIKISAKI_NM"]); // 取引先名
                        dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dtKishu.Rows[tirihikisakiCd[i]]["DENWA_BANGO"]); // 電話番号
                        dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtKishu.Rows[tirihikisakiCd[i]]["FAX_BANGO"]); // Fax番号
                        dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                        dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                        dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                        dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                        dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                        dpc.SetParam("@ITEM23", SqlDbType.VarChar, 20, 0); // 0か1を判断 0は点線
                        #endregion

                        this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                        #endregion

                        #region 次月繰越テーブルの登録
                        #region インサートテーブル
                        Sql = new StringBuilder();
                        dpc = new DbParamCollection();
                        Sql.Append("INSERT INTO PR_HN_TBL(");
                        Sql.Append("  GUID");
                        Sql.Append(" ,SORT");
                        Sql.Append(" ,ITEM01");
                        Sql.Append(" ,ITEM02");
                        Sql.Append(" ,ITEM03");
                        Sql.Append(" ,ITEM04");
                        Sql.Append(" ,ITEM05");
                        Sql.Append(" ,ITEM06");
                        Sql.Append(" ,ITEM07");
                        Sql.Append(" ,ITEM08");
                        Sql.Append(" ,ITEM12");
                        Sql.Append(" ,ITEM21");
                        Sql.Append(" ,ITEM22");
                        Sql.Append(" ,ITEM23");
                        Sql.Append(") ");
                        Sql.Append("VALUES(");
                        Sql.Append("  @GUID");
                        Sql.Append(" ,@SORT");
                        Sql.Append(" ,@ITEM01");
                        Sql.Append(" ,@ITEM02");
                        Sql.Append(" ,@ITEM03");
                        Sql.Append(" ,@ITEM04");
                        Sql.Append(" ,@ITEM05");
                        Sql.Append(" ,@ITEM06");
                        Sql.Append(" ,@ITEM07");
                        Sql.Append(" ,@ITEM08");
                        Sql.Append(" ,@ITEM12");
                        Sql.Append(" ,@ITEM21");
                        Sql.Append(" ,@ITEM22");
                        Sql.Append(" ,@ITEM23");
                        Sql.Append(") ");
                        #endregion

                        #region ページヘッダーデータを設定
                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                        dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                        dbSORT++;
                        dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dtKishu.Rows[tirihikisakiCd[i]]["TORIHIKISAKI_CD"]); // 取引先コード
                        dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtKishu.Rows[tirihikisakiCd[i]]["TORIHIKISAKI_NM"]); // 取引先名
                        dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dtKishu.Rows[tirihikisakiCd[i]]["DENWA_BANGO"]); // 電話番号
                        dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtKishu.Rows[tirihikisakiCd[i]]["FAX_BANGO"]); // Fax番号
                        dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                        dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                        dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                        dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                        dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                        #endregion

                        dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "◇◇◇ 次月へ繰越し ◇◇◇"); // 繰越時データ
                        dpc.SetParam("@ITEM21", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(dtKishu.Rows[tirihikisakiCd[i]]["KISHUZAN"]))); // 期首残高
                        dpc.SetParam("@ITEM23", SqlDbType.VarChar, 20, 0); // 0か1を判断 0は点線

                        this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                        #endregion

                        i++;
                    }
                }
                else
                {
                    Msg.Info("該当データがありません。");
                    return false;
                }
            }
            #endregion
            #region メインデータが0じゃない場合
            else
            {
                NumVals DENPYO = new NumVals(); // 伝票グループ
                NumVals TORIHIKISAKI = new NumVals(); // 取引先グループ

                #region メインループ準備処理
                String tmpGetsu = null;// 月
                String tmpNextGetsu = null;// 比較用月
                String tmpBeforeGetsu = null;// 比較用月
                int tmpDenpyo = 0;// 伝票
                int tmpNextDenpyo = 0;// 比較用伝票
                int tmpBeforeDenpyo = 0;// 比較用伝票
                int tmpTorihikisakiCd = 0;// 取引先コード
                int tmpNextTorihikisakiCd = 0;// 比較用取引先コード
                int tmpBeforeTorihikisakiCd = 0;// 比較用取引先コード
                int flag = 0;// 空データ表示フラグ
                int flagU01 = 0;// 売上計表示フラグ
                int flagU02 = 0;// 売上計表示フラグ
                int flagU03 = 0;// 売上計表示フラグ
                decimal tmpRoundUriage;
                // double tmpRoundUriage;

                String DenpyoDate = "";
                String DenpyoNo = "";
                String TorihikiKubunn = "";
                String Zandaka = "";

                String tmpDenwaBango = "";//電話番号変数
                String tmpFaxBango = "";//FAX番号変数
                String tmpTorihikisakiNm = null;// 取引先名

                int i = 0; // ループ用カウント変数
                int j = 1; // 1件後のデータとの比較用カウンタ変数
                int k = -1; // 1件前のデータとの比較用カウンタ変数
                int l = 0; // ループ用カウント変数
                int Multiple = 36; // 指定行での改ページ用変数
                int Count = 1; // 指定行での改ページ用カウンタ変数
                int z = 0; // 期首残・電話番号・FAX番号・取引先名の取得用カウンタ変数
                int dbSORT = 1;

                DataRow[] dr; // メインループ用データ変数
                #endregion

                while (dtKishu.Rows.Count > l)
                {
                    dr = dtMainLoop.Select("TORIHIKISAKI_CD = " + Util.ToString(dtKishu.Rows[l]["TORIHIKISAKI_CD"]));

                    // if ((Util.ToDecimal(dtKishu.Rows[l]["KISHUZAN"]) > 0 || Util.ToDecimal(dtKishu.Rows[l]["ZANDAKA"]) > 0) && dr.Length == 0)
                    if ((Util.ToDecimal(dtKishu.Rows[l]["KISHUZAN"]) != 0 || Util.ToDecimal(dtKishu.Rows[l]["ZANDAKA"]) > 0) && dr.Length == 0)
                    {
                        #region 前月繰越テーブルの登録
                        #region インサートテーブル
                        Sql = new StringBuilder();
                        dpc = new DbParamCollection();
                        Sql.Append("INSERT INTO PR_HN_TBL(");
                        Sql.Append("  GUID");
                        Sql.Append(" ,SORT");
                        Sql.Append(" ,ITEM01");
                        Sql.Append(" ,ITEM02");
                        Sql.Append(" ,ITEM03");
                        Sql.Append(" ,ITEM04");
                        Sql.Append(" ,ITEM05");
                        Sql.Append(" ,ITEM06");
                        Sql.Append(" ,ITEM07");
                        Sql.Append(" ,ITEM08");
                        Sql.Append(" ,ITEM12");
                        Sql.Append(" ,ITEM21");
                        Sql.Append(" ,ITEM22");
                        Sql.Append(" ,ITEM23");
                        Sql.Append(") ");
                        Sql.Append("VALUES(");
                        Sql.Append("  @GUID");
                        Sql.Append(" ,@SORT");
                        Sql.Append(" ,@ITEM01");
                        Sql.Append(" ,@ITEM02");
                        Sql.Append(" ,@ITEM03");
                        Sql.Append(" ,@ITEM04");
                        Sql.Append(" ,@ITEM05");
                        Sql.Append(" ,@ITEM06");
                        Sql.Append(" ,@ITEM07");
                        Sql.Append(" ,@ITEM08");
                        Sql.Append(" ,@ITEM12");
                        Sql.Append(" ,@ITEM21");
                        Sql.Append(" ,@ITEM22");
                        Sql.Append(" ,@ITEM23");
                        Sql.Append(") ");
                        #endregion

                        #region ページヘッダーデータを設定
                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                        dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                        dbSORT++;
                        dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_CD"]); // 取引先コード
                        dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_NM"]); // 取引先名
                        dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dtKishu.Rows[l]["DENWA_BANGO"]); // 電話番号
                        dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtKishu.Rows[l]["FAX_BANGO"]); // Fax番号
                        dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                        dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                        dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                        dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                        dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                        #endregion

                        dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "◇◇◇ 前月より繰越し ◇◇◇"); // 繰越時データ
                        dpc.SetParam("@ITEM21", SqlDbType.VarChar, 20, Util.FormatNum(dtKishu.Rows[l]["KISHUZAN"])); // 期首残高
                        dpc.SetParam("@ITEM23", SqlDbType.VarChar, 20, 0); // 0か1を判断 0は点線

                        this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                        Count++; // 改ページ用カウンタ
                        #endregion
                    }
                    else if (dr.Length > 0)
                    {
                        while (dr.Length > i)
                        {
                            #region データの準備
                            // 伝票日付を和暦に変換
                            DateTime d = Util.ToDate(dr[i]["DENPYO_DATE"]);
                            string[] denpyo_date = Util.ConvJpDate(d, this.Dba);
                            if (denpyo_date[3].Length == 1)
                            {
                                denpyo_date[3] = " " + denpyo_date[3];
                            }
                            if (denpyo_date[4].Length == 1)
                            {
                                denpyo_date[4] = " " + denpyo_date[4];
                            }
                            String wareki_denpyo_date = denpyo_date[2] + "/" + denpyo_date[3] + "/" + denpyo_date[4];

                            // 比較用データがnullの場合(取引先コードが変わった時)、期首残・電話番号・FAX番号・取引先名を取得
                            if (tmpNextGetsu == null)
                            {
                                while (dtKishu.Rows.Count > z)
                                {
                                    if (Util.ToInt(dtKishu.Rows[z]["TORIHIKISAKI_CD"]) == Util.ToInt(dr[i]["TORIHIKISAKI_CD"]))
                                    {
                                        tmpTorihikisakiNm = Util.ToString(dtKishu.Rows[z]["TORIHIKISAKI_NM"]);
                                        TORIHIKISAKI.ZANDAKA += Util.ToDecimal(dtKishu.Rows[z]["KISHUZAN"]);
                                        tmpDenwaBango = Util.ToString(dtKishu.Rows[z]["DENWA_BANGO"]);
                                        tmpFaxBango = Util.ToString(dtKishu.Rows[z]["FAX_BANGO"]);
                                        break;
                                    }
                                    z++;
                                }
                            }

                            // 現在の伝票日付の月・伝票番号・取引先コードを取得
                            tmpGetsu = denpyo_date[3];
                            tmpDenpyo = Util.ToInt(dr[i]["DENPYO_BANGO"]);
                            tmpTorihikisakiCd = Util.ToInt(dr[i]["TORIHIKISAKI_CD"]);

                            // 比較用の伝票日付の月・伝票番号を取得
                            if (j < dr.Length)
                            {
                                // 伝票日付を和暦に変換
                                DateTime tmpD = Util.ToDate(dr[j]["DENPYO_DATE"]);
                                string[] tmpDenpyoDate = Util.ConvJpDate(tmpD, this.Dba);
                                if (tmpDenpyoDate[3].Length == 1)
                                {
                                    tmpDenpyoDate[3] = " " + tmpDenpyoDate[3];
                                }
                                tmpNextGetsu = tmpDenpyoDate[3];
                                tmpNextDenpyo = Util.ToInt(dr[j]["DENPYO_BANGO"]);
                                tmpNextTorihikisakiCd = Util.ToInt(dr[j]["TORIHIKISAKI_CD"]);
                            }
                            if (k != -1)
                            {
                                // 伝票日付を和暦に変換
                                DateTime tmpD = Util.ToDate(dr[k]["DENPYO_DATE"]);
                                string[] tmpDenpyoDate = Util.ConvJpDate(tmpD, this.Dba);
                                if (tmpDenpyoDate[3].Length == 1)
                                {
                                    tmpDenpyoDate[3] = " " + tmpDenpyoDate[3];
                                }
                                tmpBeforeGetsu = tmpDenpyoDate[3];
                                tmpBeforeDenpyo = Util.ToInt(dr[k]["DENPYO_BANGO"]);
                                tmpBeforeTorihikisakiCd = Util.ToInt(dr[k]["TORIHIKISAKI_CD"]);
                            }
                            #endregion

                            #region 印刷ワークテーブルに登録
                            // 登録するデータを判別
                            if (Util.ToInt(dr[i]["DENPYO_BANGO"]) != 0)
                            {
                                // 現在の取引先コードと前回の取引先コードが違っていれば実行 // 前回の月と現在の月を比較し、違っていれば実行
                                if (tmpTorihikisakiCd != tmpBeforeTorihikisakiCd)
                                {
                                    #region 前月繰越テーブルの登録
                                    #region インサートテーブル
                                    Sql = new StringBuilder();
                                    dpc = new DbParamCollection();
                                    Sql.Append("INSERT INTO PR_HN_TBL(");
                                    Sql.Append("  GUID");
                                    Sql.Append(" ,SORT");
                                    Sql.Append(" ,ITEM01");
                                    Sql.Append(" ,ITEM02");
                                    Sql.Append(" ,ITEM03");
                                    Sql.Append(" ,ITEM04");
                                    Sql.Append(" ,ITEM05");
                                    Sql.Append(" ,ITEM06");
                                    Sql.Append(" ,ITEM07");
                                    Sql.Append(" ,ITEM08");
                                    Sql.Append(" ,ITEM12");
                                    Sql.Append(" ,ITEM21");
                                    Sql.Append(" ,ITEM22");
                                    Sql.Append(" ,ITEM23");
                                    Sql.Append(") ");
                                    Sql.Append("VALUES(");
                                    Sql.Append("  @GUID");
                                    Sql.Append(" ,@SORT");
                                    Sql.Append(" ,@ITEM01");
                                    Sql.Append(" ,@ITEM02");
                                    Sql.Append(" ,@ITEM03");
                                    Sql.Append(" ,@ITEM04");
                                    Sql.Append(" ,@ITEM05");
                                    Sql.Append(" ,@ITEM06");
                                    Sql.Append(" ,@ITEM07");
                                    Sql.Append(" ,@ITEM08");
                                    Sql.Append(" ,@ITEM12");
                                    Sql.Append(" ,@ITEM21");
                                    Sql.Append(" ,@ITEM22");
                                    Sql.Append(" ,@ITEM23");
                                    Sql.Append(") ");
                                    #endregion

                                    #region ページヘッダーデータを設定
                                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                    dbSORT++;
                                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_CD"]); // 取引先コード
                                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_NM"]); // 取引先名
                                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dtKishu.Rows[l]["DENWA_BANGO"]); // 電話番号
                                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtKishu.Rows[l]["FAX_BANGO"]); // Fax番号
                                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                                    dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                                    dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                                    #endregion

                                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "◇◇◇ 前月より繰越し ◇◇◇"); // 繰越時データ
                                    dpc.SetParam("@ITEM21", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 期首残高
                                    dpc.SetParam("@ITEM23", SqlDbType.VarChar, 20, 0); // 0か1を判断 0はfalse

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                                    Count++; // 改ページ用カウンタ
                                    #endregion
                                }

                                // 改ページ用カウンタが36の倍数の時に実行
                                if (Count % Multiple == 0)
                                {
                                    #region 改ページ時処理
                                    #region インサートテーブル
                                    Sql = new StringBuilder();
                                    dpc = new DbParamCollection();
                                    Sql.Append("INSERT INTO PR_HN_TBL(");
                                    Sql.Append("  GUID");
                                    Sql.Append(" ,SORT");
                                    Sql.Append(" ,ITEM01");
                                    Sql.Append(" ,ITEM02");
                                    Sql.Append(" ,ITEM03");
                                    Sql.Append(" ,ITEM04");
                                    Sql.Append(" ,ITEM05");
                                    Sql.Append(" ,ITEM06");
                                    Sql.Append(" ,ITEM07");
                                    Sql.Append(" ,ITEM08");
                                    Sql.Append(" ,ITEM12");
                                    Sql.Append(" ,ITEM19");
                                    Sql.Append(" ,ITEM20");
                                    Sql.Append(" ,ITEM21");
                                    Sql.Append(" ,ITEM22");
                                    Sql.Append(" ,ITEM23");
                                    Sql.Append(") ");
                                    Sql.Append("VALUES(");
                                    Sql.Append("  @GUID");
                                    Sql.Append(" ,@SORT");
                                    Sql.Append(" ,@ITEM01");
                                    Sql.Append(" ,@ITEM02");
                                    Sql.Append(" ,@ITEM03");
                                    Sql.Append(" ,@ITEM04");
                                    Sql.Append(" ,@ITEM05");
                                    Sql.Append(" ,@ITEM06");
                                    Sql.Append(" ,@ITEM07");
                                    Sql.Append(" ,@ITEM08");
                                    Sql.Append(" ,@ITEM12");
                                    Sql.Append(" ,@ITEM19");
                                    Sql.Append(" ,@ITEM20");
                                    Sql.Append(" ,@ITEM21");
                                    Sql.Append(" ,@ITEM22");
                                    Sql.Append(" ,@ITEM23");
                                    Sql.Append(") ");
                                    #endregion

                                    #region ページヘッダーデータを設定
                                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                    dbSORT++;
                                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_CD"]); // 取引先コード
                                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_NM"]); // 取引先名
                                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dtKishu.Rows[l]["DENWA_BANGO"]); // 電話番号
                                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtKishu.Rows[l]["FAX_BANGO"]); // Fax番号
                                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                                    dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                                    dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                                    #endregion

                                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "☆☆ 次頁へ繰越し ☆☆"); // 改ページ時文字列
                                    dpc.SetParam("@ITEM19", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.URIAGEGAKU))); // 売上金額合計
                                    dpc.SetParam("@ITEM20", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.NYUKINGAKU))); // 入金額合計
                                    dpc.SetParam("@ITEM21", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高
                                    dpc.SetParam("@ITEM23", SqlDbType.VarChar, 20, 1); // 0か1を判断

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                                    Count++; // 改ページ用カウンタ
                                    #endregion

                                    #region 改ページ時処理
                                    #region インサートテーブル
                                    Sql = new StringBuilder();
                                    dpc = new DbParamCollection();
                                    Sql.Append("INSERT INTO PR_HN_TBL(");
                                    Sql.Append("  GUID");
                                    Sql.Append(" ,SORT");
                                    Sql.Append(" ,ITEM01");
                                    Sql.Append(" ,ITEM02");
                                    Sql.Append(" ,ITEM03");
                                    Sql.Append(" ,ITEM04");
                                    Sql.Append(" ,ITEM05");
                                    Sql.Append(" ,ITEM06");
                                    Sql.Append(" ,ITEM07");
                                    Sql.Append(" ,ITEM08");
                                    Sql.Append(" ,ITEM12");
                                    Sql.Append(" ,ITEM19");
                                    Sql.Append(" ,ITEM20");
                                    Sql.Append(" ,ITEM21");
                                    Sql.Append(" ,ITEM22");
                                    Sql.Append(" ,ITEM23");
                                    Sql.Append(") ");
                                    Sql.Append("VALUES(");
                                    Sql.Append("  @GUID");
                                    Sql.Append(" ,@SORT");
                                    Sql.Append(" ,@ITEM01");
                                    Sql.Append(" ,@ITEM02");
                                    Sql.Append(" ,@ITEM03");
                                    Sql.Append(" ,@ITEM04");
                                    Sql.Append(" ,@ITEM05");
                                    Sql.Append(" ,@ITEM06");
                                    Sql.Append(" ,@ITEM07");
                                    Sql.Append(" ,@ITEM08");
                                    Sql.Append(" ,@ITEM12");
                                    Sql.Append(" ,@ITEM19");
                                    Sql.Append(" ,@ITEM20");
                                    Sql.Append(" ,@ITEM21");
                                    Sql.Append(" ,@ITEM22");
                                    Sql.Append(" ,@ITEM23");
                                    Sql.Append(") ");
                                    #endregion

                                    #region ページヘッダーデータを設定
                                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                    dbSORT++;
                                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_CD"]); // 取引先コード
                                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_NM"]); // 取引先名
                                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dtKishu.Rows[l]["DENWA_BANGO"]); // 電話番号
                                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtKishu.Rows[l]["FAX_BANGO"]); // Fax番号
                                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                                    dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                                    dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                                    #endregion

                                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "☆☆ 前頁より繰越し ☆☆"); // 改ページ時文字列
                                    dpc.SetParam("@ITEM19", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.URIAGEGAKU))); // 売上金額合計
                                    dpc.SetParam("@ITEM20", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.NYUKINGAKU))); // 入金額合計
                                    dpc.SetParam("@ITEM21", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高
                                    dpc.SetParam("@ITEM23", SqlDbType.VarChar, 20, 0); // 0か1を判断 

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                                    Count++; // 改ページ用カウンタ
                                    #endregion
                                }

                                // 常に実行
                                #region 実データ登録
                                #region インサートテーブル
                                Sql = new StringBuilder();
                                dpc = new DbParamCollection();
                                Sql.Append("INSERT INTO PR_HN_TBL(");
                                Sql.Append("  GUID");
                                Sql.Append(" ,SORT");
                                Sql.Append(" ,ITEM01");
                                Sql.Append(" ,ITEM02");
                                Sql.Append(" ,ITEM03");
                                Sql.Append(" ,ITEM04");
                                Sql.Append(" ,ITEM05");
                                Sql.Append(" ,ITEM06");
                                Sql.Append(" ,ITEM07");
                                Sql.Append(" ,ITEM08");
                                Sql.Append(" ,ITEM09");
                                Sql.Append(" ,ITEM10");
                                Sql.Append(" ,ITEM11");
                                Sql.Append(" ,ITEM12");
                                Sql.Append(" ,ITEM13");
                                Sql.Append(" ,ITEM14");
                                Sql.Append(" ,ITEM15");
                                Sql.Append(" ,ITEM16");
                                Sql.Append(" ,ITEM17");
                                Sql.Append(" ,ITEM18");
                                Sql.Append(" ,ITEM19");
                                Sql.Append(" ,ITEM20");
                                Sql.Append(" ,ITEM21");
                                Sql.Append(" ,ITEM22");
                                Sql.Append(" ,ITEM23");
                                Sql.Append(") ");
                                Sql.Append("VALUES(");
                                Sql.Append("  @GUID");
                                Sql.Append(" ,@SORT");
                                Sql.Append(" ,@ITEM01");
                                Sql.Append(" ,@ITEM02");
                                Sql.Append(" ,@ITEM03");
                                Sql.Append(" ,@ITEM04");
                                Sql.Append(" ,@ITEM05");
                                Sql.Append(" ,@ITEM06");
                                Sql.Append(" ,@ITEM07");
                                Sql.Append(" ,@ITEM08");
                                Sql.Append(" ,@ITEM09");
                                Sql.Append(" ,@ITEM10");
                                Sql.Append(" ,@ITEM11");
                                Sql.Append(" ,@ITEM12");
                                Sql.Append(" ,@ITEM13");
                                Sql.Append(" ,@ITEM14");
                                Sql.Append(" ,@ITEM15");
                                Sql.Append(" ,@ITEM16");
                                Sql.Append(" ,@ITEM17");
                                Sql.Append(" ,@ITEM18");
                                Sql.Append(" ,@ITEM19");
                                Sql.Append(" ,@ITEM20");
                                Sql.Append(" ,@ITEM21");
                                Sql.Append(" ,@ITEM22");
                                Sql.Append(" ,@ITEM23");
                                Sql.Append(") ");
                                #endregion

                                #region ページヘッダーデータを設定
                                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                dbSORT++;
                                dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_CD"]); // 取引先コード
                                dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_NM"]); // 取引先名
                                dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dtKishu.Rows[l]["DENWA_BANGO"]); // 電話番号
                                dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtKishu.Rows[l]["FAX_BANGO"]); // Fax番号
                                dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                                dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                                dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                                if (Count % Multiple == 0)
                                {
                                    dpc.SetParam("@ITEM23", SqlDbType.VarChar, 20, 1); // 0か1を判断 1は実線
                                }
                                else
                                {
                                    dpc.SetParam("@ITEM23", SqlDbType.VarChar, 20, 0); // 0か1を判断 0は点線
                                }
                                #endregion

                                if (tmpDenpyo == tmpBeforeDenpyo)
                                {
                                    DenpyoDate = "";
                                    DenpyoNo = "";
                                    TorihikiKubunn = "";
                                    Zandaka = "";
                                }
                                else
                                {
                                    DenpyoDate = wareki_denpyo_date;
                                    DenpyoNo = Util.ToString(dr[i]["DENPYO_BANGO"]);
                                    TorihikiKubunn = Util.ToString(dr[i]["TORIHIKI_KUBUN_NM"]);
                                    Zandaka = Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA));
                                }

                                #region 現金売上時
                                if (Util.ToInt(dr[i]["TORIHIKI_KUBUN"]) == 2)
                                {
                                    dpc.SetParam("@ITEM09", SqlDbType.VarChar, 20, DenpyoDate); // 伝票日付
                                    dpc.SetParam("@ITEM10", SqlDbType.VarChar, 20, DenpyoNo); // 伝票No
                                    dpc.SetParam("@ITEM11", SqlDbType.VarChar, 20, TorihikiKubunn); // 取引区分
                                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, ""); // 文字列データ
                                    dpc.SetParam("@ITEM13", SqlDbType.VarChar, 20, dr[i]["SHOHIN_CD"]); // 商品番号
                                    dpc.SetParam("@ITEM14", SqlDbType.VarChar, 20, dr[i]["SHOHIN_NM"]); // 商品名
                                    dpc.SetParam("@ITEM15", SqlDbType.VarChar, 20, dr[i]["KIKAKU"]); // 規格
                                    dpc.SetParam("@ITEM16", SqlDbType.VarChar, 20, dr[i]["IRISU"]); // 入数
                                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 20, Util.FormatNum(dr[i]["SURYO"], 1)); // 数量
                                    dpc.SetParam("@ITEM18", SqlDbType.VarChar, 20, Util.FormatNum(dr[i]["TANKA"], 1)); // 単価
                                    tmpRoundUriage = Util.ToDecimal(dr[i]["URIAGE_KINGAKU"]) + Util.ToDecimal(dr[i]["SHOHIZEI"]);
                                    dpc.SetParam("@ITEM19", SqlDbType.VarChar, 20, Util.FormatNum(tmpRoundUriage)); // 売上金額
                                    dpc.SetParam("@ITEM20", SqlDbType.VarChar, 20, ""); // 入金額
                                    dpc.SetParam("@ITEM21", SqlDbType.VarChar, 20, ""); // 残高

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                                    // 伝票グループ
                                    DENPYO.URIAGEGAKU += tmpRoundUriage; // 伝票売上金額合計用
                                    // 取引先グループ
                                    TORIHIKISAKI.URIAGEGAKU += tmpRoundUriage; // 取引先売上金額合計用
                                    TORIHIKISAKI.NYUKINGAKU += tmpRoundUriage; // 取引先入金額合計用
                                }
                                #endregion
                                #region 売上時
                                else if (Util.ToInt(dr[i]["TORIHIKI_KUBUN"]) != 9)
                                {
                                    dpc.SetParam("@ITEM09", SqlDbType.VarChar, 20, DenpyoDate); // 伝票日付
                                    dpc.SetParam("@ITEM10", SqlDbType.VarChar, 20, DenpyoNo); // 伝票No
                                    dpc.SetParam("@ITEM11", SqlDbType.VarChar, 20, TorihikiKubunn); // 取引区分
                                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, ""); // 文字列データ
                                    dpc.SetParam("@ITEM13", SqlDbType.VarChar, 20, dr[i]["SHOHIN_CD"]); // 商品番号
                                    dpc.SetParam("@ITEM14", SqlDbType.VarChar, 20, dr[i]["SHOHIN_NM"]); // 商品名
                                    dpc.SetParam("@ITEM15", SqlDbType.VarChar, 20, dr[i]["KIKAKU"]); // 規格
                                    dpc.SetParam("@ITEM16", SqlDbType.VarChar, 20, dr[i]["IRISU"]); // 入数
                                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 20, Util.FormatNum(dr[i]["SURYO"], 1)); // 数量
                                    dpc.SetParam("@ITEM18", SqlDbType.VarChar, 20, Util.FormatNum(dr[i]["TANKA"], 1)); // 単価
                                    dpc.SetParam("@ITEM19", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(dr[i]["URIAGE_KINGAKU"]))); // 売上金額
                                    dpc.SetParam("@ITEM20", SqlDbType.VarChar, 20, ""); // 入金額
                                    dpc.SetParam("@ITEM21", SqlDbType.VarChar, 20, ""); // 残高

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                                    tmpRoundUriage = Util.ToDecimal(dr[i]["URIAGE_KINGAKU"]) + Util.ToDecimal(dr[i]["SHOHIZEI"]);
                                    // 伝票グループ
                                    DENPYO.URIAGEGAKU += tmpRoundUriage; // 伝票売上金額合計用
                                    DENPYO.ZANDAKA += tmpRoundUriage; // 伝票残高合計用
                                    // 取引先グループ
                                    TORIHIKISAKI.URIAGEGAKU += tmpRoundUriage; // 取引先売上金額合計用
                                    TORIHIKISAKI.NYUKINGAKU += 0; // 取引先入金額合計用
                                    TORIHIKISAKI.ZANDAKA += tmpRoundUriage; // 取引先残高合計用
                                }
                                #endregion
                                #region 入金時
                                else
                                {
                                    dpc.SetParam("@ITEM09", SqlDbType.VarChar, 20, DenpyoDate); // 伝票日付
                                    dpc.SetParam("@ITEM10", SqlDbType.VarChar, 20, DenpyoNo); // 伝票No
                                    dpc.SetParam("@ITEM11", SqlDbType.VarChar, 20, ""); // 取引区分
                                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, ""); // 文字列データ
                                    dpc.SetParam("@ITEM13", SqlDbType.VarChar, 20, ""); // 商品番号
                                    dpc.SetParam("@ITEM14", SqlDbType.VarChar, 20, dr[i]["TEKIYO"]); // 商品名
                                    dpc.SetParam("@ITEM15", SqlDbType.VarChar, 20, ""); // 規格
                                    dpc.SetParam("@ITEM16", SqlDbType.VarChar, 20, ""); // 入数
                                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 20, ""); // 数量
                                    dpc.SetParam("@ITEM18", SqlDbType.VarChar, 20, ""); // 単価
                                    dpc.SetParam("@ITEM19", SqlDbType.VarChar, 20, ""); // 売上金額
                                    dpc.SetParam("@ITEM20", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(dr[i]["ZEIKOMI_KINGAKU"]))); // 入金額
                                    TORIHIKISAKI.ZANDAKA -= Util.ToDecimal(dr[i]["ZEIKOMI_KINGAKU"]); // 伝票合計用残高
                                    dpc.SetParam("@ITEM21", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                                    // 取引先グループ
                                    TORIHIKISAKI.URIAGEGAKU += 0; // 取引先売上金額合計用
                                    //TORIHIKISAKI.NYUKINGAKU += 0; // 取引先入金額合計用
                                    TORIHIKISAKI.NYUKINGAKU += Util.ToDecimal(dr[i]["ZEIKOMI_KINGAKU"]); // 取引先入金額合計用
                                    
                                }
                                #endregion

                                if (Util.ToInt(dr[i]["TORIHIKI_KUBUN1_AND_2"]) == 11)
                                {
                                    TORIHIKISAKI.KOBAI += Util.ToDecimal(dr[i]["URIAGE_KINGAKU"]) + Util.ToDecimal(dr[i]["SHOHIZEI"]); // 購買金額合計用
                                    flagU01 = 1; // 売上計表示フラグ
                                }
                                else if (Util.ToInt(dr[i]["TORIHIKI_KUBUN1_AND_2"]) == 13)
                                {
                                    TORIHIKISAKI.KORI_ESA += Util.ToDecimal(dr[i]["URIAGE_KINGAKU"]) + Util.ToDecimal(dr[i]["SHOHIZEI"]); // 氷・ｴｻ金額合計用
                                    flagU02 = 1; // 売上計表示フラグ
                                }
                                else if (Util.ToInt(dr[i]["TORIHIKI_KUBUN1_AND_2"]) == 21)
                                {
                                    TORIHIKISAKI.GENKIN += Util.ToDecimal(dr[i]["URIAGE_KINGAKU"]) + Util.ToDecimal(dr[i]["SHOHIZEI"]); // 現金金額合計用
                                    flagU03 = 1; // 売上計表示フラグ
                                }


                                Count++; // 改ページ用カウンタ
                                #endregion

                                // 改ページ用カウンタが36の倍数の時に実行
                                if (Count % Multiple == 0)
                                {
                                    #region 改ページ時処理
                                    #region インサートテーブル
                                    Sql = new StringBuilder();
                                    dpc = new DbParamCollection();
                                    Sql.Append("INSERT INTO PR_HN_TBL(");
                                    Sql.Append("  GUID");
                                    Sql.Append(" ,SORT");
                                    Sql.Append(" ,ITEM01");
                                    Sql.Append(" ,ITEM02");
                                    Sql.Append(" ,ITEM03");
                                    Sql.Append(" ,ITEM04");
                                    Sql.Append(" ,ITEM05");
                                    Sql.Append(" ,ITEM06");
                                    Sql.Append(" ,ITEM07");
                                    Sql.Append(" ,ITEM08");
                                    Sql.Append(" ,ITEM12");
                                    Sql.Append(" ,ITEM19");
                                    Sql.Append(" ,ITEM20");
                                    Sql.Append(" ,ITEM21");
                                    Sql.Append(" ,ITEM22");
                                    Sql.Append(" ,ITEM23");
                                    Sql.Append(") ");
                                    Sql.Append("VALUES(");
                                    Sql.Append("  @GUID");
                                    Sql.Append(" ,@SORT");
                                    Sql.Append(" ,@ITEM01");
                                    Sql.Append(" ,@ITEM02");
                                    Sql.Append(" ,@ITEM03");
                                    Sql.Append(" ,@ITEM04");
                                    Sql.Append(" ,@ITEM05");
                                    Sql.Append(" ,@ITEM06");
                                    Sql.Append(" ,@ITEM07");
                                    Sql.Append(" ,@ITEM08");
                                    Sql.Append(" ,@ITEM12");
                                    Sql.Append(" ,@ITEM19");
                                    Sql.Append(" ,@ITEM20");
                                    Sql.Append(" ,@ITEM21");
                                    Sql.Append(" ,@ITEM22");
                                    Sql.Append(" ,@ITEM23");
                                    Sql.Append(") ");
                                    #endregion

                                    #region ページヘッダーデータを設定
                                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                    dbSORT++;
                                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_CD"]); // 取引先コード
                                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_NM"]); // 取引先名
                                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dtKishu.Rows[l]["DENWA_BANGO"]); // 電話番号
                                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtKishu.Rows[l]["FAX_BANGO"]); // Fax番号
                                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                                    dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                                    dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                                    #endregion

                                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "☆☆ 次頁へ繰越し ☆☆"); // 改ページ時文字列
                                    dpc.SetParam("@ITEM19", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.URIAGEGAKU))); // 売上金額合計
                                    dpc.SetParam("@ITEM20", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.NYUKINGAKU))); // 入金額合計
                                    dpc.SetParam("@ITEM21", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高
                                    dpc.SetParam("@ITEM23", SqlDbType.VarChar, 20, 1); // 0か1を判断 1はtrue

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                                    Count++; // 改ページ用カウンタ
                                    #endregion

                                    #region 改ページ時処理
                                    #region インサートテーブル
                                    Sql = new StringBuilder();
                                    dpc = new DbParamCollection();
                                    Sql.Append("INSERT INTO PR_HN_TBL(");
                                    Sql.Append("  GUID");
                                    Sql.Append(" ,SORT");
                                    Sql.Append(" ,ITEM01");
                                    Sql.Append(" ,ITEM02");
                                    Sql.Append(" ,ITEM03");
                                    Sql.Append(" ,ITEM04");
                                    Sql.Append(" ,ITEM05");
                                    Sql.Append(" ,ITEM06");
                                    Sql.Append(" ,ITEM07");
                                    Sql.Append(" ,ITEM08");
                                    Sql.Append(" ,ITEM12");
                                    Sql.Append(" ,ITEM19");
                                    Sql.Append(" ,ITEM20");
                                    Sql.Append(" ,ITEM21");
                                    Sql.Append(" ,ITEM22");
                                    Sql.Append(" ,ITEM23");
                                    Sql.Append(") ");
                                    Sql.Append("VALUES(");
                                    Sql.Append("  @GUID");
                                    Sql.Append(" ,@SORT");
                                    Sql.Append(" ,@ITEM01");
                                    Sql.Append(" ,@ITEM02");
                                    Sql.Append(" ,@ITEM03");
                                    Sql.Append(" ,@ITEM04");
                                    Sql.Append(" ,@ITEM05");
                                    Sql.Append(" ,@ITEM06");
                                    Sql.Append(" ,@ITEM07");
                                    Sql.Append(" ,@ITEM08");
                                    Sql.Append(" ,@ITEM12");
                                    Sql.Append(" ,@ITEM19");
                                    Sql.Append(" ,@ITEM20");
                                    Sql.Append(" ,@ITEM21");
                                    Sql.Append(" ,@ITEM22");
                                    Sql.Append(" ,@ITEM23");
                                    Sql.Append(") ");
                                    #endregion

                                    #region ページヘッダーデータを設定
                                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                    dbSORT++;
                                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_CD"]); // 取引先コード
                                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_NM"]); // 取引先名
                                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dtKishu.Rows[l]["DENWA_BANGO"]); // 電話番号
                                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtKishu.Rows[l]["FAX_BANGO"]); // Fax番号
                                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                                    dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                                    dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                                    #endregion

                                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "☆☆ 前頁より繰越し ☆☆"); // 改ページ時文字列
                                    dpc.SetParam("@ITEM19", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.URIAGEGAKU))); // 売上金額合計
                                    dpc.SetParam("@ITEM20", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.NYUKINGAKU))); // 入金額合計
                                    dpc.SetParam("@ITEM21", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高
                                    dpc.SetParam("@ITEM23", SqlDbType.VarChar, 20, 0); // 0か1を判断 0はfalse

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                                    Count++; // 改ページ用カウンタ
                                    #endregion
                                }

                                // 取引区分が9でない時に実行  // 最後のデータ(次回カウンタと総データ数が一致)の時に実行 // 現在の伝票と次回の伝票が違っていれば実行
                                if (Util.ToInt(dr[i]["TORIHIKI_KUBUN"]) != 9)
                                {
                                    if (dr.Length == j || tmpDenpyo != tmpNextDenpyo)
                                    {
                                        #region 伝票合計テーブルの登録
                                        #region インサートテーブル
                                        Sql = new StringBuilder();
                                        dpc = new DbParamCollection();
                                        Sql.Append("INSERT INTO PR_HN_TBL(");
                                        Sql.Append("  GUID");
                                        Sql.Append(" ,SORT");
                                        Sql.Append(" ,ITEM01");
                                        Sql.Append(" ,ITEM02");
                                        Sql.Append(" ,ITEM03");
                                        Sql.Append(" ,ITEM04");
                                        Sql.Append(" ,ITEM05");
                                        Sql.Append(" ,ITEM06");
                                        Sql.Append(" ,ITEM07");
                                        Sql.Append(" ,ITEM08");
                                        Sql.Append(" ,ITEM18");
                                        Sql.Append(" ,ITEM19");
                                        Sql.Append(" ,ITEM21");
                                        Sql.Append(" ,ITEM22");
                                        Sql.Append(" ,ITEM23");
                                        Sql.Append(") ");
                                        Sql.Append("VALUES(");
                                        Sql.Append("  @GUID");
                                        Sql.Append(" ,@SORT");
                                        Sql.Append(" ,@ITEM01");
                                        Sql.Append(" ,@ITEM02");
                                        Sql.Append(" ,@ITEM03");
                                        Sql.Append(" ,@ITEM04");
                                        Sql.Append(" ,@ITEM05");
                                        Sql.Append(" ,@ITEM06");
                                        Sql.Append(" ,@ITEM07");
                                        Sql.Append(" ,@ITEM08");
                                        Sql.Append(" ,@ITEM18");
                                        Sql.Append(" ,@ITEM19");
                                        Sql.Append(" ,@ITEM21");
                                        Sql.Append(" ,@ITEM22");
                                        Sql.Append(" ,@ITEM23");
                                        Sql.Append(") ");
                                        #endregion

                                        #region ページヘッダーデータを設定
                                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                        dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                        dbSORT++;
                                        dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_CD"]); // 取引先コード
                                        dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_NM"]); // 取引先名
                                        dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dtKishu.Rows[l]["DENWA_BANGO"]); // 電話番号
                                        dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtKishu.Rows[l]["FAX_BANGO"]); // Fax番号
                                        dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); // 開始年月日
                                        dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                        dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                                        dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                                        dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                                        #endregion

                                        dpc.SetParam("@ITEM18", SqlDbType.VarChar, 20, "【伝票合計】"); // 伝票合計
                                        dpc.SetParam("@ITEM19", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(DENPYO.URIAGEGAKU))); // 売上金額合計
                                        dpc.SetParam("@ITEM21", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高
                                        if (Count % Multiple == 0)
                                        {
                                            dpc.SetParam("@ITEM23", SqlDbType.VarChar, 20, 1); // 0か1を判断 1は実線
                                        }
                                        else
                                        {
                                            dpc.SetParam("@ITEM23", SqlDbType.VarChar, 20, 0); // 0か1を判断 0は点線
                                        }

                                        this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                                        flag = 1;
                                        // 伝票合計をクリア
                                        DENPYO.Clear();
                                        Count++; // 改ページ用カウンタ
                                        #endregion
                                    }
                                }

                                // 改ページ用カウンタが36の倍数の時に実行
                                if (Count % Multiple == 0)
                                {
                                    #region 改ページ時処理
                                    #region インサートテーブル
                                    Sql = new StringBuilder();
                                    dpc = new DbParamCollection();
                                    Sql.Append("INSERT INTO PR_HN_TBL(");
                                    Sql.Append("  GUID");
                                    Sql.Append(" ,SORT");
                                    Sql.Append(" ,ITEM01");
                                    Sql.Append(" ,ITEM02");
                                    Sql.Append(" ,ITEM03");
                                    Sql.Append(" ,ITEM04");
                                    Sql.Append(" ,ITEM05");
                                    Sql.Append(" ,ITEM06");
                                    Sql.Append(" ,ITEM07");
                                    Sql.Append(" ,ITEM08");
                                    Sql.Append(" ,ITEM12");
                                    Sql.Append(" ,ITEM19");
                                    Sql.Append(" ,ITEM20");
                                    Sql.Append(" ,ITEM21");
                                    Sql.Append(" ,ITEM22");
                                    Sql.Append(" ,ITEM23");
                                    Sql.Append(") ");
                                    Sql.Append("VALUES(");
                                    Sql.Append("  @GUID");
                                    Sql.Append(" ,@SORT");
                                    Sql.Append(" ,@ITEM01");
                                    Sql.Append(" ,@ITEM02");
                                    Sql.Append(" ,@ITEM03");
                                    Sql.Append(" ,@ITEM04");
                                    Sql.Append(" ,@ITEM05");
                                    Sql.Append(" ,@ITEM06");
                                    Sql.Append(" ,@ITEM07");
                                    Sql.Append(" ,@ITEM08");
                                    Sql.Append(" ,@ITEM12");
                                    Sql.Append(" ,@ITEM19");
                                    Sql.Append(" ,@ITEM20");
                                    Sql.Append(" ,@ITEM21");
                                    Sql.Append(" ,@ITEM22");
                                    Sql.Append(" ,@ITEM23");
                                    Sql.Append(") ");
                                    #endregion

                                    #region ページヘッダーデータを設定
                                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                    dbSORT++;
                                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_CD"]); // 取引先コード
                                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_NM"]); // 取引先名
                                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dtKishu.Rows[l]["DENWA_BANGO"]); // 電話番号
                                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtKishu.Rows[l]["FAX_BANGO"]); // Fax番号
                                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                                    dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                                    dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                                    #endregion

                                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "☆☆ 次頁へ繰越し ☆☆"); // 改ページ時文字列
                                    dpc.SetParam("@ITEM19", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.URIAGEGAKU))); // 売上金額合計
                                    dpc.SetParam("@ITEM20", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.NYUKINGAKU))); // 入金額合計
                                    dpc.SetParam("@ITEM21", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高
                                    dpc.SetParam("@ITEM23", SqlDbType.VarChar, 20, 1); // 0か1を判断 1はture

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                                    Count++; // 改ページ用カウンタ
                                    #endregion

                                    #region 改ページ時処理
                                    #region インサートテーブル
                                    Sql = new StringBuilder();
                                    dpc = new DbParamCollection();
                                    Sql.Append("INSERT INTO PR_HN_TBL(");
                                    Sql.Append("  GUID");
                                    Sql.Append(" ,SORT");
                                    Sql.Append(" ,ITEM01");
                                    Sql.Append(" ,ITEM02");
                                    Sql.Append(" ,ITEM03");
                                    Sql.Append(" ,ITEM04");
                                    Sql.Append(" ,ITEM05");
                                    Sql.Append(" ,ITEM06");
                                    Sql.Append(" ,ITEM07");
                                    Sql.Append(" ,ITEM08");
                                    Sql.Append(" ,ITEM12");
                                    Sql.Append(" ,ITEM19");
                                    Sql.Append(" ,ITEM20");
                                    Sql.Append(" ,ITEM21");
                                    Sql.Append(" ,ITEM22");
                                    Sql.Append(" ,ITEM23");
                                    Sql.Append(") ");
                                    Sql.Append("VALUES(");
                                    Sql.Append("  @GUID");
                                    Sql.Append(" ,@SORT");
                                    Sql.Append(" ,@ITEM01");
                                    Sql.Append(" ,@ITEM02");
                                    Sql.Append(" ,@ITEM03");
                                    Sql.Append(" ,@ITEM04");
                                    Sql.Append(" ,@ITEM05");
                                    Sql.Append(" ,@ITEM06");
                                    Sql.Append(" ,@ITEM07");
                                    Sql.Append(" ,@ITEM08");
                                    Sql.Append(" ,@ITEM12");
                                    Sql.Append(" ,@ITEM19");
                                    Sql.Append(" ,@ITEM20");
                                    Sql.Append(" ,@ITEM21");
                                    Sql.Append(" ,@ITEM22");
                                    Sql.Append(" ,@ITEM23");
                                    Sql.Append(") ");
                                    #endregion

                                    #region ページヘッダーデータを設定
                                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                    dbSORT++;
                                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_CD"]); // 取引先コード
                                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_NM"]); // 取引先名
                                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dtKishu.Rows[l]["DENWA_BANGO"]); // 電話番号
                                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtKishu.Rows[l]["FAX_BANGO"]); // Fax番号
                                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                                    dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                                    dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                                    #endregion

                                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "☆☆ 前頁より繰越し ☆☆"); // 改ページ時文字列
                                    dpc.SetParam("@ITEM19", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.URIAGEGAKU))); // 売上金額合計
                                    dpc.SetParam("@ITEM20", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.NYUKINGAKU))); // 入金額合計
                                    dpc.SetParam("@ITEM21", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高
                                    dpc.SetParam("@ITEM23", SqlDbType.VarChar, 20, 0); // 0か1を判断 0はfalse

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                                    Count++; // 改ページ用カウンタ
                                    #endregion
                                }

                                // 現在の伝票と次回の伝票が違っていれば実行 // 最後のデータ(次回カウンタと総データ数が一致)の時に実行 // flagが1の時に実行
                                if (tmpDenpyo != tmpNextDenpyo || dr.Length == j || flag == 1)
                                {
                                    #region 空データの登録 
                                    #region インサートテーブル
                                    Sql = new StringBuilder();
                                    dpc = new DbParamCollection();
                                    Sql.Append("INSERT INTO PR_HN_TBL(");
                                    Sql.Append("  GUID");
                                    Sql.Append(" ,SORT");
                                    Sql.Append(" ,ITEM01");
                                    Sql.Append(" ,ITEM02");
                                    Sql.Append(" ,ITEM03");
                                    Sql.Append(" ,ITEM04");
                                    Sql.Append(" ,ITEM05");
                                    Sql.Append(" ,ITEM06");
                                    Sql.Append(" ,ITEM07");
                                    Sql.Append(" ,ITEM08");
                                    Sql.Append(" ,ITEM22");
                                    Sql.Append(" ,ITEM23");
                                    Sql.Append(") ");
                                    Sql.Append("VALUES(");
                                    Sql.Append("  @GUID");
                                    Sql.Append(" ,@SORT");
                                    Sql.Append(" ,@ITEM01");
                                    Sql.Append(" ,@ITEM02");
                                    Sql.Append(" ,@ITEM03");
                                    Sql.Append(" ,@ITEM04");
                                    Sql.Append(" ,@ITEM05");
                                    Sql.Append(" ,@ITEM06");
                                    Sql.Append(" ,@ITEM07");
                                    Sql.Append(" ,@ITEM08");
                                    Sql.Append(" ,@ITEM22");
                                    Sql.Append(" ,@ITEM23");
                                    Sql.Append(") ");
                                    #endregion

                                    #region ページヘッダーデータを設定
                                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                    dbSORT++;
                                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_CD"]); // 取引先コード
                                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_NM"]); // 取引先名
                                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dtKishu.Rows[l]["DENWA_BANGO"]); // 電話番号
                                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtKishu.Rows[l]["FAX_BANGO"]); // Fax番号
                                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                                    dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                                    dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                                    if (Count % Multiple == 0)
                                    {
                                        dpc.SetParam("@ITEM23", SqlDbType.VarChar, 20, 1); // 0か1を判断 1は実線
                                    }
                                    else
                                    {
                                        dpc.SetParam("@ITEM23", SqlDbType.VarChar, 20, 0); // 0か1を判断 0は点線
                                    }
                                    #endregion

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                                    flag = 0;
                                    Count++; // 改ページ用カウンタ
                                    #endregion
                                }

                                // 改ページ用カウンタが36の倍数の時に実行
                                if (Count % Multiple == 0)
                                {
                                    #region 改ページ時処理
                                    #region インサートテーブル
                                    Sql = new StringBuilder();
                                    dpc = new DbParamCollection();
                                    Sql.Append("INSERT INTO PR_HN_TBL(");
                                    Sql.Append("  GUID");
                                    Sql.Append(" ,SORT");
                                    Sql.Append(" ,ITEM01");
                                    Sql.Append(" ,ITEM02");
                                    Sql.Append(" ,ITEM03");
                                    Sql.Append(" ,ITEM04");
                                    Sql.Append(" ,ITEM05");
                                    Sql.Append(" ,ITEM06");
                                    Sql.Append(" ,ITEM07");
                                    Sql.Append(" ,ITEM08");
                                    Sql.Append(" ,ITEM12");
                                    Sql.Append(" ,ITEM19");
                                    Sql.Append(" ,ITEM20");
                                    Sql.Append(" ,ITEM21");
                                    Sql.Append(" ,ITEM22");
                                    Sql.Append(" ,ITEM23");
                                    Sql.Append(") ");
                                    Sql.Append("VALUES(");
                                    Sql.Append("  @GUID");
                                    Sql.Append(" ,@SORT");
                                    Sql.Append(" ,@ITEM01");
                                    Sql.Append(" ,@ITEM02");
                                    Sql.Append(" ,@ITEM03");
                                    Sql.Append(" ,@ITEM04");
                                    Sql.Append(" ,@ITEM05");
                                    Sql.Append(" ,@ITEM06");
                                    Sql.Append(" ,@ITEM07");
                                    Sql.Append(" ,@ITEM08");
                                    Sql.Append(" ,@ITEM12");
                                    Sql.Append(" ,@ITEM19");
                                    Sql.Append(" ,@ITEM20");
                                    Sql.Append(" ,@ITEM21");
                                    Sql.Append(" ,@ITEM22");
                                    Sql.Append(" ,@ITEM23");
                                    Sql.Append(") ");
                                    #endregion

                                    #region ページヘッダーデータを設定
                                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                    dbSORT++;
                                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_CD"]); // 取引先コード
                                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_NM"]); // 取引先名
                                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dtKishu.Rows[l]["DENWA_BANGO"]); // 電話番号
                                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtKishu.Rows[l]["FAX_BANGO"]); // Fax番号
                                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                                    dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                                    dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                                    #endregion

                                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "☆☆ 次頁へ繰越し ☆☆"); // 改ページ時文字列
                                    dpc.SetParam("@ITEM19", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.URIAGEGAKU))); // 売上金額合計
                                    dpc.SetParam("@ITEM20", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.NYUKINGAKU))); // 入金額合計
                                    dpc.SetParam("@ITEM21", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高
                                    dpc.SetParam("@ITEM23", SqlDbType.VarChar, 20, 1); // 0か1を判断 
                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                                    Count++; // 改ページ用カウンタ
                                    #endregion

                                    #region 改ページ時処理
                                    #region インサートテーブル
                                    Sql = new StringBuilder();
                                    dpc = new DbParamCollection();
                                    Sql.Append("INSERT INTO PR_HN_TBL(");
                                    Sql.Append("  GUID");
                                    Sql.Append(" ,SORT");
                                    Sql.Append(" ,ITEM01");
                                    Sql.Append(" ,ITEM02");
                                    Sql.Append(" ,ITEM03");
                                    Sql.Append(" ,ITEM04");
                                    Sql.Append(" ,ITEM05");
                                    Sql.Append(" ,ITEM06");
                                    Sql.Append(" ,ITEM07");
                                    Sql.Append(" ,ITEM08");
                                    Sql.Append(" ,ITEM12");
                                    Sql.Append(" ,ITEM19");
                                    Sql.Append(" ,ITEM20");
                                    Sql.Append(" ,ITEM21");
                                    Sql.Append(" ,ITEM22");
                                    Sql.Append(" ,ITEM23");
                                    Sql.Append(") ");
                                    Sql.Append("VALUES(");
                                    Sql.Append("  @GUID");
                                    Sql.Append(" ,@SORT");
                                    Sql.Append(" ,@ITEM01");
                                    Sql.Append(" ,@ITEM02");
                                    Sql.Append(" ,@ITEM03");
                                    Sql.Append(" ,@ITEM04");
                                    Sql.Append(" ,@ITEM05");
                                    Sql.Append(" ,@ITEM06");
                                    Sql.Append(" ,@ITEM07");
                                    Sql.Append(" ,@ITEM08");
                                    Sql.Append(" ,@ITEM12");
                                    Sql.Append(" ,@ITEM19");
                                    Sql.Append(" ,@ITEM20");
                                    Sql.Append(" ,@ITEM21");
                                    Sql.Append(" ,@ITEM22");
                                    Sql.Append(" ,@ITEM23");
                                    Sql.Append(") ");
                                    #endregion

                                    #region ページヘッダーデータを設定
                                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                    dbSORT++;
                                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_CD"]); // 取引先コード
                                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_NM"]); // 取引先名
                                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dtKishu.Rows[l]["DENWA_BANGO"]); // 電話番号
                                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtKishu.Rows[l]["FAX_BANGO"]); // Fax番号
                                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                                    dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                                    dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                                    #endregion

                                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "☆☆ 前頁より繰越し ☆☆"); // 改ページ時文字列
                                    dpc.SetParam("@ITEM19", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.URIAGEGAKU))); // 売上金額合計
                                    dpc.SetParam("@ITEM20", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.NYUKINGAKU))); // 入金額合計
                                    dpc.SetParam("@ITEM21", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高
                                    dpc.SetParam("@ITEM23", SqlDbType.VarChar, 20, 0); // 0か1を判断 0はfalse

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                                    Count++; // 改ページ用カウンタ
                                    #endregion
                                }

                                // flagが1の時に実行
                                if (flag == 1)
                                {
                                    #region 空データの登録
                                    #region インサートテーブル
                                    Sql = new StringBuilder();
                                    dpc = new DbParamCollection();
                                    Sql.Append("INSERT INTO PR_HN_TBL(");
                                    Sql.Append("  GUID");
                                    Sql.Append(" ,SORT");
                                    Sql.Append(" ,ITEM01");
                                    Sql.Append(" ,ITEM02");
                                    Sql.Append(" ,ITEM03");
                                    Sql.Append(" ,ITEM04");
                                    Sql.Append(" ,ITEM05");
                                    Sql.Append(" ,ITEM06");
                                    Sql.Append(" ,ITEM07");
                                    Sql.Append(" ,ITEM08");
                                    Sql.Append(" ,ITEM22");
                                    Sql.Append(" ,ITEM23");
                                    Sql.Append(") ");
                                    Sql.Append("VALUES(");
                                    Sql.Append("  @GUID");
                                    Sql.Append(" ,@SORT");
                                    Sql.Append(" ,@ITEM01");
                                    Sql.Append(" ,@ITEM02");
                                    Sql.Append(" ,@ITEM03");
                                    Sql.Append(" ,@ITEM04");
                                    Sql.Append(" ,@ITEM05");
                                    Sql.Append(" ,@ITEM06");
                                    Sql.Append(" ,@ITEM07");
                                    Sql.Append(" ,@ITEM08");
                                    Sql.Append(" ,@ITEM22");
                                    Sql.Append(" ,@ITEM23");
                                    Sql.Append(") ");
                                    #endregion

                                    #region ページヘッダーデータを設定
                                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                    dbSORT++;
                                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_CD"]); // 取引先コード
                                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_NM"]); // 取引先名
                                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dtKishu.Rows[l]["DENWA_BANGO"]); // 電話番号
                                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtKishu.Rows[l]["FAX_BANGO"]); // Fax番号
                                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                                    dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                                    dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                                    if (Count % Multiple == 0)
                                    {
                                        dpc.SetParam("@ITEM23", SqlDbType.VarChar, 20, 1); // 0か1を判断 1は実線
                                    }
                                    else
                                    {
                                        dpc.SetParam("@ITEM23", SqlDbType.VarChar, 20, 0); // 0か1を判断 0は点線
                                    }
                                    #endregion

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                                    flag = 0;
                                    Count++; // 改ページ用カウンタ
                                    #endregion
                                }

                                // 改ページ用カウンタが36の倍数の時に実行
                                if (Count % Multiple == 0)
                                {
                                    #region 改ページ時処理
                                    #region インサートテーブル
                                    Sql = new StringBuilder();
                                    dpc = new DbParamCollection();
                                    Sql.Append("INSERT INTO PR_HN_TBL(");
                                    Sql.Append("  GUID");
                                    Sql.Append(" ,SORT");
                                    Sql.Append(" ,ITEM01");
                                    Sql.Append(" ,ITEM02");
                                    Sql.Append(" ,ITEM03");
                                    Sql.Append(" ,ITEM04");
                                    Sql.Append(" ,ITEM05");
                                    Sql.Append(" ,ITEM06");
                                    Sql.Append(" ,ITEM07");
                                    Sql.Append(" ,ITEM08");
                                    Sql.Append(" ,ITEM12");
                                    Sql.Append(" ,ITEM19");
                                    Sql.Append(" ,ITEM20");
                                    Sql.Append(" ,ITEM21");
                                    Sql.Append(" ,ITEM22");
                                    Sql.Append(" ,ITEM23");
                                    Sql.Append(") ");
                                    Sql.Append("VALUES(");
                                    Sql.Append("  @GUID");
                                    Sql.Append(" ,@SORT");
                                    Sql.Append(" ,@ITEM01");
                                    Sql.Append(" ,@ITEM02");
                                    Sql.Append(" ,@ITEM03");
                                    Sql.Append(" ,@ITEM04");
                                    Sql.Append(" ,@ITEM05");
                                    Sql.Append(" ,@ITEM06");
                                    Sql.Append(" ,@ITEM07");
                                    Sql.Append(" ,@ITEM08");
                                    Sql.Append(" ,@ITEM12");
                                    Sql.Append(" ,@ITEM19");
                                    Sql.Append(" ,@ITEM20");
                                    Sql.Append(" ,@ITEM21");
                                    Sql.Append(" ,@ITEM22");
                                    Sql.Append(" ,@ITEM23");
                                    Sql.Append(") ");
                                    #endregion

                                    #region ページヘッダーデータを設定
                                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                    dbSORT++;
                                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_CD"]); // 取引先コード
                                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_NM"]); // 取引先名
                                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dtKishu.Rows[l]["DENWA_BANGO"]); // 電話番号
                                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtKishu.Rows[l]["FAX_BANGO"]); // Fax番号
                                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                                    dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                                    dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                                    #endregion

                                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "☆☆ 次頁へ繰越し ☆☆"); // 改ページ時文字列
                                    dpc.SetParam("@ITEM19", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.URIAGEGAKU))); // 売上金額合計
                                    dpc.SetParam("@ITEM20", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.NYUKINGAKU))); // 入金額合計
                                    dpc.SetParam("@ITEM21", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高
                                    dpc.SetParam("@ITEM23", SqlDbType.VarChar, 20, 1); // 0か1を判断 1は実線

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                                    Count++; // 改ページ用カウンタ
                                    #endregion

                                    #region 改ページ時処理
                                    #region インサートテーブル
                                    Sql = new StringBuilder();
                                    dpc = new DbParamCollection();
                                    Sql.Append("INSERT INTO PR_HN_TBL(");
                                    Sql.Append("  GUID");
                                    Sql.Append(" ,SORT");
                                    Sql.Append(" ,ITEM01");
                                    Sql.Append(" ,ITEM02");
                                    Sql.Append(" ,ITEM03");
                                    Sql.Append(" ,ITEM04");
                                    Sql.Append(" ,ITEM05");
                                    Sql.Append(" ,ITEM06");
                                    Sql.Append(" ,ITEM07");
                                    Sql.Append(" ,ITEM08");
                                    Sql.Append(" ,ITEM12");
                                    Sql.Append(" ,ITEM19");
                                    Sql.Append(" ,ITEM20");
                                    Sql.Append(" ,ITEM21");
                                    Sql.Append(" ,ITEM22");
                                    Sql.Append(" ,ITEM23");
                                    Sql.Append(") ");
                                    Sql.Append("VALUES(");
                                    Sql.Append("  @GUID");
                                    Sql.Append(" ,@SORT");
                                    Sql.Append(" ,@ITEM01");
                                    Sql.Append(" ,@ITEM02");
                                    Sql.Append(" ,@ITEM03");
                                    Sql.Append(" ,@ITEM04");
                                    Sql.Append(" ,@ITEM05");
                                    Sql.Append(" ,@ITEM06");
                                    Sql.Append(" ,@ITEM07");
                                    Sql.Append(" ,@ITEM08");
                                    Sql.Append(" ,@ITEM12");
                                    Sql.Append(" ,@ITEM19");
                                    Sql.Append(" ,@ITEM20");
                                    Sql.Append(" ,@ITEM21");
                                    Sql.Append(" ,@ITEM22");
                                    Sql.Append(" ,@ITEM23");
                                    Sql.Append(") ");
                                    #endregion

                                    #region ページヘッダーデータを設定
                                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                    dbSORT++;
                                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_CD"]); // 取引先コード
                                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_NM"]); // 取引先名
                                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dtKishu.Rows[l]["DENWA_BANGO"]); // 電話番号
                                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtKishu.Rows[l]["FAX_BANGO"]); // Fax番号
                                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                                    dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                                    dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                                    #endregion

                                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "☆☆ 前頁より繰越し ☆☆"); // 改ページ時文字列
                                    dpc.SetParam("@ITEM19", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.URIAGEGAKU))); // 売上金額合計
                                    dpc.SetParam("@ITEM20", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.NYUKINGAKU))); // 入金額合計
                                    dpc.SetParam("@ITEM21", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高
                                    dpc.SetParam("@ITEM23", SqlDbType.VarChar, 20, 0); // 0か1を判断 0は点線

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                                    Count++; // 改ページ用カウンタ
                                    #endregion
                                }

                                // flagが1の時に実行 // 現在の取引先コードと次回の取引先コードが一致なら実行
                                if (flag == 1 && tmpTorihikisakiCd == tmpNextTorihikisakiCd)
                                {
                                    #region 空データの登録
                                    #region インサートテーブル
                                    Sql = new StringBuilder();
                                    dpc = new DbParamCollection();
                                    Sql.Append("INSERT INTO PR_HN_TBL(");
                                    Sql.Append("  GUID");
                                    Sql.Append(" ,SORT");
                                    Sql.Append(" ,ITEM01");
                                    Sql.Append(" ,ITEM02");
                                    Sql.Append(" ,ITEM03");
                                    Sql.Append(" ,ITEM04");
                                    Sql.Append(" ,ITEM05");
                                    Sql.Append(" ,ITEM06");
                                    Sql.Append(" ,ITEM07");
                                    Sql.Append(" ,ITEM08");
                                    Sql.Append(" ,ITEM22");
                                    Sql.Append(" ,ITEM23");
                                    Sql.Append(") ");
                                    Sql.Append("VALUES(");
                                    Sql.Append("  @GUID");
                                    Sql.Append(" ,@SORT");
                                    Sql.Append(" ,@ITEM01");
                                    Sql.Append(" ,@ITEM02");
                                    Sql.Append(" ,@ITEM03");
                                    Sql.Append(" ,@ITEM04");
                                    Sql.Append(" ,@ITEM05");
                                    Sql.Append(" ,@ITEM06");
                                    Sql.Append(" ,@ITEM07");
                                    Sql.Append(" ,@ITEM08");
                                    Sql.Append(" ,@ITEM22");
                                    Sql.Append(" ,@ITEM23");
                                    Sql.Append(") ");
                                    #endregion

                                    #region ページヘッダーデータを設定
                                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                    dbSORT++;
                                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_CD"]); // 取引先コード
                                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_NM"]); // 取引先名
                                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dtKishu.Rows[l]["DENWA_BANGO"]); // 電話番号
                                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtKishu.Rows[l]["FAX_BANGO"]); // Fax番号
                                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                                    dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                                    dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                                    if (Count % Multiple == 0)
                                    {
                                        dpc.SetParam("@ITEM23", SqlDbType.VarChar, 20, 1); // 0か1を判断 1は実線
                                    }
                                    else
                                    {
                                        dpc.SetParam("@ITEM23", SqlDbType.VarChar, 20, 0); // 0か1を判断 0は点線
                                    }
                                    #endregion

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                                    flag = 0;
                                    Count++; // 改ページ用カウンタ
                                    #endregion
                                }

                                // 改ページ用カウンタが36の倍数の時に実行 // 現在の取引先コードと次回の取引先コードが一致なら実行
                                if (Count % Multiple == 0 && tmpTorihikisakiCd == tmpNextTorihikisakiCd)
                                {
                                    #region 改ページ時処理
                                    #region インサートテーブル
                                    Sql = new StringBuilder();
                                    dpc = new DbParamCollection();
                                    Sql.Append("INSERT INTO PR_HN_TBL(");
                                    Sql.Append("  GUID");
                                    Sql.Append(" ,SORT");
                                    Sql.Append(" ,ITEM01");
                                    Sql.Append(" ,ITEM02");
                                    Sql.Append(" ,ITEM03");
                                    Sql.Append(" ,ITEM04");
                                    Sql.Append(" ,ITEM05");
                                    Sql.Append(" ,ITEM06");
                                    Sql.Append(" ,ITEM07");
                                    Sql.Append(" ,ITEM08");
                                    Sql.Append(" ,ITEM12");
                                    Sql.Append(" ,ITEM19");
                                    Sql.Append(" ,ITEM20");
                                    Sql.Append(" ,ITEM21");
                                    Sql.Append(" ,ITEM22");
                                    Sql.Append(" ,ITEM23");
                                    Sql.Append(") ");
                                    Sql.Append("VALUES(");
                                    Sql.Append("  @GUID");
                                    Sql.Append(" ,@SORT");
                                    Sql.Append(" ,@ITEM01");
                                    Sql.Append(" ,@ITEM02");
                                    Sql.Append(" ,@ITEM03");
                                    Sql.Append(" ,@ITEM04");
                                    Sql.Append(" ,@ITEM05");
                                    Sql.Append(" ,@ITEM06");
                                    Sql.Append(" ,@ITEM07");
                                    Sql.Append(" ,@ITEM08");
                                    Sql.Append(" ,@ITEM12");
                                    Sql.Append(" ,@ITEM19");
                                    Sql.Append(" ,@ITEM20");
                                    Sql.Append(" ,@ITEM21");
                                    Sql.Append(" ,@ITEM22");
                                    Sql.Append(" ,@ITEM23");
                                    Sql.Append(") ");
                                    #endregion

                                    #region ページヘッダーデータを設定
                                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                    dbSORT++;
                                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_CD"]); // 取引先コード
                                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_NM"]); // 取引先名
                                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dtKishu.Rows[l]["DENWA_BANGO"]); // 電話番号
                                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtKishu.Rows[l]["FAX_BANGO"]); // Fax番号
                                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                                    dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                                    dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                                    #endregion

                                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "☆☆ 次頁へ繰越し ☆☆"); // 改ページ時文字列
                                    dpc.SetParam("@ITEM19", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.URIAGEGAKU))); // 売上金額合計
                                    dpc.SetParam("@ITEM20", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.NYUKINGAKU))); // 入金額合計
                                    dpc.SetParam("@ITEM21", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高
                                    dpc.SetParam("@ITEM23", SqlDbType.VarChar, 20, 1); // 0か1を判断 

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                                    Count++; // 改ページ用カウンタ
                                    #endregion

                                    #region 改ページ時処理
                                    #region インサートテーブル
                                    Sql = new StringBuilder();
                                    dpc = new DbParamCollection();
                                    Sql.Append("INSERT INTO PR_HN_TBL(");
                                    Sql.Append("  GUID");
                                    Sql.Append(" ,SORT");
                                    Sql.Append(" ,ITEM01");
                                    Sql.Append(" ,ITEM02");
                                    Sql.Append(" ,ITEM03");
                                    Sql.Append(" ,ITEM04");
                                    Sql.Append(" ,ITEM05");
                                    Sql.Append(" ,ITEM06");
                                    Sql.Append(" ,ITEM07");
                                    Sql.Append(" ,ITEM08");
                                    Sql.Append(" ,ITEM12");
                                    Sql.Append(" ,ITEM19");
                                    Sql.Append(" ,ITEM20");
                                    Sql.Append(" ,ITEM21");
                                    Sql.Append(" ,ITEM22");
                                    Sql.Append(" ,ITEM23");
                                    Sql.Append(") ");
                                    Sql.Append("VALUES(");
                                    Sql.Append("  @GUID");
                                    Sql.Append(" ,@SORT");
                                    Sql.Append(" ,@ITEM01");
                                    Sql.Append(" ,@ITEM02");
                                    Sql.Append(" ,@ITEM03");
                                    Sql.Append(" ,@ITEM04");
                                    Sql.Append(" ,@ITEM05");
                                    Sql.Append(" ,@ITEM06");
                                    Sql.Append(" ,@ITEM07");
                                    Sql.Append(" ,@ITEM08");
                                    Sql.Append(" ,@ITEM12");
                                    Sql.Append(" ,@ITEM19");
                                    Sql.Append(" ,@ITEM20");
                                    Sql.Append(" ,@ITEM21");
                                    Sql.Append(" ,@ITEM22");
                                    Sql.Append(" ,@ITEM23");
                                    Sql.Append(") ");
                                    #endregion

                                    #region ページヘッダーデータを設定
                                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                    dbSORT++;
                                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_CD"]); // 取引先コード
                                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_NM"]); // 取引先名
                                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dtKishu.Rows[l]["DENWA_BANGO"]); // 電話番号
                                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtKishu.Rows[l]["FAX_BANGO"]); // Fax番号
                                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                                    dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                                    dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                                    #endregion

                                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "☆☆ 前頁より繰越し ☆☆"); // 改ページ時文字列
                                    dpc.SetParam("@ITEM19", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.URIAGEGAKU))); // 売上金額合計
                                    dpc.SetParam("@ITEM20", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.NYUKINGAKU))); // 入金額合計
                                    dpc.SetParam("@ITEM21", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高
                                    dpc.SetParam("@ITEM23", SqlDbType.VarChar, 20, 0); // 0か1を判断 0はfalse

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                                    Count++; // 改ページ用カウンタ
                                    #endregion
                                }

                                // 最後のデータ(次回カウンタと総データ数が一致)の時に実行
                                if (dr.Length == j)
                                {
                                    // flagU01が1の時に実行
                                    if (flagU01 == 1)
                                    {
                                        #region 購買掛売計テーブルの登録
                                        #region インサートテーブル
                                        Sql = new StringBuilder();
                                        dpc = new DbParamCollection();
                                        Sql.Append("INSERT INTO PR_HN_TBL(");
                                        Sql.Append("  GUID");
                                        Sql.Append(" ,SORT");
                                        Sql.Append(" ,ITEM01");
                                        Sql.Append(" ,ITEM02");
                                        Sql.Append(" ,ITEM03");
                                        Sql.Append(" ,ITEM04");
                                        Sql.Append(" ,ITEM05");
                                        Sql.Append(" ,ITEM06");
                                        Sql.Append(" ,ITEM07");
                                        Sql.Append(" ,ITEM08");
                                        Sql.Append(" ,ITEM12");
                                        Sql.Append(" ,ITEM19");
                                        Sql.Append(" ,ITEM22");
                                        Sql.Append(" ,ITEM23");
                                        Sql.Append(") ");
                                        Sql.Append("VALUES(");
                                        Sql.Append("  @GUID");
                                        Sql.Append(" ,@SORT");
                                        Sql.Append(" ,@ITEM01");
                                        Sql.Append(" ,@ITEM02");
                                        Sql.Append(" ,@ITEM03");
                                        Sql.Append(" ,@ITEM04");
                                        Sql.Append(" ,@ITEM05");
                                        Sql.Append(" ,@ITEM06");
                                        Sql.Append(" ,@ITEM07");
                                        Sql.Append(" ,@ITEM08");
                                        Sql.Append(" ,@ITEM12");
                                        Sql.Append(" ,@ITEM19");
                                        Sql.Append(" ,@ITEM22");
                                        Sql.Append(" ,@ITEM23");
                                        Sql.Append(") ");
                                        #endregion

                                        #region ページヘッダーデータを設定
                                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                        dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                        dbSORT++;
                                        dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_CD"]); // 取引先コード
                                        dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_NM"]); // 取引先名
                                        dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dtKishu.Rows[l]["DENWA_BANGO"]); // 電話番号
                                        dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtKishu.Rows[l]["FAX_BANGO"]); // Fax番号
                                        dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                        dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                        dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                                        dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                                        dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                                        #endregion

                                        dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "( 購 買 掛 売 計 )"); // 繰越時データ
                                        dpc.SetParam("@ITEM19", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.KOBAI))); // 購買掛売計
                                        if (Count % Multiple == 0 || flagU02 != 1 && flagU03 != 1)
                                        {
                                            dpc.SetParam("@ITEM23", SqlDbType.VarChar, 20, 1); // 0か1を判断 1は実線
                                        }
                                        else
                                        {
                                            dpc.SetParam("@ITEM23", SqlDbType.VarChar, 20, 0); // 0か1を判断 0は点線
                                        }

                                        this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                                        Count++; // 改ページ用カウンタ
                                        #endregion
                                    }

                                    // flagU02が1の時に実行
                                    if (flagU02 == 1)
                                    {
                                        #region 氷・ｴｻ掛売上計テーブルの登録
                                        #region インサートテーブル
                                        Sql = new StringBuilder();
                                        dpc = new DbParamCollection();
                                        Sql.Append("INSERT INTO PR_HN_TBL(");
                                        Sql.Append("  GUID");
                                        Sql.Append(" ,SORT");
                                        Sql.Append(" ,ITEM01");
                                        Sql.Append(" ,ITEM02");
                                        Sql.Append(" ,ITEM03");
                                        Sql.Append(" ,ITEM04");
                                        Sql.Append(" ,ITEM05");
                                        Sql.Append(" ,ITEM06");
                                        Sql.Append(" ,ITEM07");
                                        Sql.Append(" ,ITEM08");
                                        Sql.Append(" ,ITEM12");
                                        Sql.Append(" ,ITEM19");
                                        Sql.Append(" ,ITEM22");
                                        Sql.Append(" ,ITEM23");
                                        Sql.Append(") ");
                                        Sql.Append("VALUES(");
                                        Sql.Append("  @GUID");
                                        Sql.Append(" ,@SORT");
                                        Sql.Append(" ,@ITEM01");
                                        Sql.Append(" ,@ITEM02");
                                        Sql.Append(" ,@ITEM03");
                                        Sql.Append(" ,@ITEM04");
                                        Sql.Append(" ,@ITEM05");
                                        Sql.Append(" ,@ITEM06");
                                        Sql.Append(" ,@ITEM07");
                                        Sql.Append(" ,@ITEM08");
                                        Sql.Append(" ,@ITEM12");
                                        Sql.Append(" ,@ITEM19");
                                        Sql.Append(" ,@ITEM22");
                                        Sql.Append(" ,@ITEM23");
                                        Sql.Append(") ");
                                        #endregion

                                        #region ページヘッダーデータを設定
                                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                        dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                        dbSORT++;
                                        dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_CD"]); // 取引先コード
                                        dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_NM"]); // 取引先名
                                        dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dtKishu.Rows[l]["DENWA_BANGO"]); // 電話番号
                                        dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtKishu.Rows[l]["FAX_BANGO"]); // Fax番号
                                        dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                        dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                        dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                                        dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                                        dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                                        #endregion

                                        dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "( 氷・ｴ ｻ 掛 売 上 計 )"); // 繰越時データ
                                        dpc.SetParam("@ITEM19", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.KORI_ESA))); // 氷・ｴｻ掛売上計
                                        if (Count % Multiple == 0 || flagU03 != 1)
                                        {
                                            dpc.SetParam("@ITEM23", SqlDbType.VarChar, 20, 1); // 0か1を判断 1は実線
                                        }
                                        else
                                        {
                                            dpc.SetParam("@ITEM23", SqlDbType.VarChar, 20, 0); // 0か1を判断 0は点線
                                        }

                                        this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                                        Count++; // 改ページ用カウンタ
                                        #endregion
                                    }

                                    // flagU03が1の時に実行
                                    if (flagU03 == 1)
                                    {
                                        #region 現金売上計テーブルの登録
                                        #region インサートテーブル
                                        Sql = new StringBuilder();
                                        dpc = new DbParamCollection();
                                        Sql.Append("INSERT INTO PR_HN_TBL(");
                                        Sql.Append("  GUID");
                                        Sql.Append(" ,SORT");
                                        Sql.Append(" ,ITEM01");
                                        Sql.Append(" ,ITEM02");
                                        Sql.Append(" ,ITEM03");
                                        Sql.Append(" ,ITEM04");
                                        Sql.Append(" ,ITEM05");
                                        Sql.Append(" ,ITEM06");
                                        Sql.Append(" ,ITEM07");
                                        Sql.Append(" ,ITEM08");
                                        Sql.Append(" ,ITEM12");
                                        Sql.Append(" ,ITEM19");
                                        Sql.Append(" ,ITEM22");
                                        Sql.Append(" ,ITEM23");
                                        Sql.Append(") ");
                                        Sql.Append("VALUES(");
                                        Sql.Append("  @GUID");
                                        Sql.Append(" ,@SORT");
                                        Sql.Append(" ,@ITEM01");
                                        Sql.Append(" ,@ITEM02");
                                        Sql.Append(" ,@ITEM03");
                                        Sql.Append(" ,@ITEM04");
                                        Sql.Append(" ,@ITEM05");
                                        Sql.Append(" ,@ITEM06");
                                        Sql.Append(" ,@ITEM07");
                                        Sql.Append(" ,@ITEM08");
                                        Sql.Append(" ,@ITEM12");
                                        Sql.Append(" ,@ITEM19");
                                        Sql.Append(" ,@ITEM22");
                                        Sql.Append(" ,@ITEM23");
                                        Sql.Append(") ");
                                        #endregion

                                        #region ページヘッダーデータを設定
                                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                        dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                        dbSORT++;
                                        dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_CD"]); // 取引先コード
                                        dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_NM"]); // 取引先名
                                        dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dtKishu.Rows[l]["DENWA_BANGO"]); // 電話番号
                                        dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtKishu.Rows[l]["FAX_BANGO"]); // Fax番号
                                        dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                        dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                        dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                                        dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                                        dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                                        #endregion

                                        dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "( 現 金 売 上 計 )"); // 繰越時データ
                                        dpc.SetParam("@ITEM19", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.GENKIN))); // 現金売上計
                                        dpc.SetParam("@ITEM23", SqlDbType.VarChar, 20, 1); // 0か1を判断 1は実線

                                        this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                                        Count++; // 改ページ用カウンタ
                                        #endregion
                                    }
                                }
                            }

                            #region データの準備
                            // 比較用の取引先コードを取得
                            if (j == dr.Length)
                            {
                                if (l + 1 < dtKishu.Rows.Count)
                                {
                                    tmpNextTorihikisakiCd = Util.ToInt(dtKishu.Rows[l + 1]["TORIHIKISAKI_CD"]);
                                }
                            }

                            // 現在と次回の取引先コードが不一致の場合 // 比較用月データを初期化 // 改ページ用カウンタを初期化 // 空データ用フラグを初期化 // 残高をクリア
                            if (tmpTorihikisakiCd != tmpNextTorihikisakiCd)
                            {
                                tmpNextGetsu = null;
                                Count = 1;
                                flag = 0;
                                TORIHIKISAKI.Clear();
                            }
                            i++;
                            j++;
                            k++;
                            #endregion

                            #endregion
                        }
                    }
                    i = 0;
                    j = 1;
                    k = -1;
                    l++;
                    flagU01 = 0;
                    flagU02 = 0;
                    flagU03 = 0;
                }
            }
            #endregion

            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
                "SORT",
                "PR_HN_TBL",
                "GUID = @GUID",
                dpc);

            bool dataFlag;
            if (tmpdtPR_HN_TBL.Rows.Count > 0)
            {
                dataFlag = true;
            }
            else
            {
                dataFlag = false;
            }

            return dataFlag;
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData02()//合計選択時
        {
            #region データ取得準備
            // 入力された情報を元にワークテーブルに更新をする
            DbParamCollection dpc = new DbParamCollection();
            // 日付範囲を西暦にして取得
            DateTime tmpDateFr = Util.ConvAdDate(this.lblJpFr.Text, this.txtJpYearFr.Text,
                    this.txtJpMonthFr.Text, this.txtJpDayFr.Text, this.Dba);
            DateTime tmpDateTo = Util.ConvAdDate(this.lblJpTo.Text, this.txtJpYearTo.Text,
                    this.txtJpMonthTo.Text, this.txtJpDayTo.Text, this.Dba);
            //今月の最後の日
            DateTime tmpLastDay = Util.ConvAdDate(this.lblJpTo.Text, this.txtJpYearTo.Text,
                    this.txtJpMonthTo.Text, Util.ToString(DateTime.DaysInMonth(Util.ToInt(this.txtJpYearTo.Text), Util.ToInt(this.txtJpMonthTo.Text))), this.Dba);

            // 日付範囲を和暦で保持
            string[] tmpjpDateFr = Util.ConvJpDate(tmpDateFr, this.Dba);
            string[] tmpjpDateTo = Util.ConvJpDate(tmpDateTo, this.Dba);

            // 日付表示形式を判断
            int dataHyojiFlag = 0; // 表示日付用変数
            // 入力開始日付が1日でない場合
            if (tmpDateFr.Day != 1)
            {
                dataHyojiFlag = 1;
            }
            // 入力終了日付が、その月の最終日でない場合
            else if (tmpDateTo != tmpLastDay)
            {
                dataHyojiFlag = 1;
            }
            // 入力開始日付と入力終了日付の年又は月が一致でない場合
            if (tmpDateFr.Year != tmpDateTo.Year || tmpDateFr.Month != tmpDateTo.Month)
            {
                dataHyojiFlag = 1;
            }

            string hyojiDate; // 表示用日付            
            if (dataHyojiFlag == 0)
            {
                hyojiDate = string.Format("{0}{1}年{2}月", tmpjpDateFr[0], tmpjpDateFr[2], tmpjpDateFr[3]) + "度";
            }
            else
            {
                hyojiDate = string.Format("{0}{1}年{2}月{3}日", tmpjpDateFr[0], tmpjpDateFr[2], tmpjpDateFr[3], tmpjpDateFr[4])
                          + "～"
                          + string.Format("{0}{1}年{2}月{3}日", tmpjpDateTo[0], tmpjpDateTo[2], tmpjpDateTo[3], tmpjpDateTo[4]);
            }

            // 船主コード設定
            String FUNANUSHI_CD_FR;
            String FUNANUSHI_CD_TO;
            if (Util.ToDecimal(txtFunanushiCdFr.Text) > 0)
            {
                FUNANUSHI_CD_FR = txtFunanushiCdFr.Text;
            }
            else
            {
                FUNANUSHI_CD_FR = "0";
            }
            if (Util.ToDecimal(txtFunanushiCdTo.Text) > 0)
            {
                FUNANUSHI_CD_TO = txtFunanushiCdTo.Text;
            }
            else
            {
                FUNANUSHI_CD_TO = "9999";
            }
            // 会計年度設定
            int kaikeiNendo;
            if (tmpDateFr.Month <= 3)
            {
                kaikeiNendo = tmpDateFr.Year - 1;
            }
            else
            {
                kaikeiNendo = tmpDateFr.Year;
            }
            //支所コードの退避
            string shishoCd = this.txtMizuageShishoCd.Text;
            // 元帳名設定
            String tmpMotochoNm = "売 上 元 帳";

            StringBuilder Sql = new StringBuilder();
            #endregion

            #region 各業者の期首算を取得
            dpc = new DbParamCollection();
            Sql = new StringBuilder();
            Sql.Append(" SELECT ");
            Sql.Append(" A.TORIHIKISAKI_CD,");
            Sql.Append(" A.TORIHIKISAKI_NM,");
            Sql.Append(" A.DENWA_BANGO,");
            Sql.Append(" A.FAX_BANGO,");
            Sql.Append(" ISNULL(Z.ZANDAKA, 0) AS ZANDAKA,");
            Sql.Append(" ISNULL(Z.KISHUZAN, 0) AS KISHUZAN");
            Sql.Append(" FROM TB_CM_TORIHIKISAKI AS A ");
            Sql.Append(" INNER JOIN TB_HN_TORIHIKISAKI_JOHO AS A1");
            Sql.Append(" 	ON	A.KAISHA_CD = A1.KAISHA_CD");
            Sql.Append(" 	AND A.TORIHIKISAKI_CD = A1.TORIHIKISAKI_CD");
            Sql.Append(" 	AND A1.TORIHIKISAKI_KUBUN1 = 1  ");
            Sql.Append(" LEFT JOIN ");
            Sql.Append(" (");
            Sql.Append(" 	SELECT");
            Sql.Append(" 		C.KAISHA_CD,");
            Sql.Append(" 		C.HOJO_KAMOKU_CD AS TORIHIKISAKI_CD,");
            Sql.Append(" 		SUM(CASE WHEN C.TAISHAKU_KUBUN = D.TAISHAKU_KUBUN THEN C.ZEIKOMI_KINGAKU ELSE (C.ZEIKOMI_KINGAKU * -1) END) AS ZANDAKA,");
            Sql.Append(" 		SUM(CASE WHEN C.DENPYO_DATE < @DATE_FR THEN (CASE WHEN C.TAISHAKU_KUBUN = D.TAISHAKU_KUBUN THEN C.ZEIKOMI_KINGAKU ELSE (C.ZEIKOMI_KINGAKU * -1) END) ELSE 0 END) AS KISHUZAN ");
            Sql.Append(" 	FROM TB_ZM_SHIWAKE_MEISAI AS C ");
            Sql.Append(" 	INNER JOIN TB_HN_KISHU_ZAN_KAMOKU AS B ");
            Sql.Append(" 		ON  B.KAISHA_CD = C.KAISHA_CD  ");
            Sql.Append(" 		AND B.SHISHO_CD = C.SHISHO_CD  ");
            Sql.Append(" 		AND B.KANJO_KAMOKU_CD = C.KANJO_KAMOKU_CD  ");
            Sql.Append(" 		AND 11 = B.MOTOCHO_KUBUN ");
            Sql.Append(" 		AND C.DENPYO_DATE < @DATE_TO ");
            Sql.Append(" 		AND C.KAIKEI_NENDO = @KAIKEI_NENDO ");
            Sql.Append(" 	LEFT OUTER JOIN TB_ZM_KANJO_KAMOKU AS D ");
            Sql.Append(" 		ON C.KAISHA_CD = D.KAISHA_CD  ");
            Sql.Append(" 		AND C.KAIKEI_NENDO = D.KAIKEI_NENDO");
            Sql.Append(" 		AND C.KANJO_KAMOKU_CD = D.KANJO_KAMOKU_CD  ");
            Sql.Append(" 	WHERE   ");
            Sql.Append(" 		C.KAISHA_CD = @KAISHA_CD AND  ");
            if (shishoCd != "0")
                Sql.Append(" 		C.SHISHO_CD = @SHISHO_CD AND  ");
            Sql.Append(" 		D.KAIKEI_NENDO = @KAIKEI_NENDO AND  ");
            Sql.Append(" 		C.HOJO_KAMOKU_CD BETWEEN @FUNANUSHI_CD_FR AND @FUNANUSHI_CD_TO ");
            Sql.Append(" 	GROUP BY");
            Sql.Append(" 	C.KAISHA_CD,");
            Sql.Append(" 	C.SHISHO_CD,");
            Sql.Append(" 	C.HOJO_KAMOKU_CD");
            Sql.Append(" ) Z");
            Sql.Append(" ON	A.KAISHA_CD = Z.KAISHA_CD");
            Sql.Append(" AND A.TORIHIKISAKI_CD = Z.TORIHIKISAKI_CD");
            Sql.Append(" ORDER BY  ");
            Sql.Append(" A.TORIHIKISAKI_CD ASC");

            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.VarChar, 4, kaikeiNendo);
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@DATE_FR", SqlDbType.VarChar, 10, tmpDateFr.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@DATE_TO", SqlDbType.VarChar, 10, tmpDateTo.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@FUNANUSHI_CD_FR", SqlDbType.VarChar, 6, FUNANUSHI_CD_FR);
            dpc.SetParam("@FUNANUSHI_CD_TO", SqlDbType.VarChar, 6, FUNANUSHI_CD_TO);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);

            DataTable dtKishu = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
            #endregion

            #region メインデータを取得
            // 日付範囲から発生している取引先データを取得
            dpc = new DbParamCollection();
            Sql = new StringBuilder();
            Sql.Append(" SELECT ");
            Sql.Append(" A.TOKUISAKI_CD AS TORIHIKISAKI_CD,");
            Sql.Append(" A.DENPYO_DATE AS DENPYO_DATE,");
            Sql.Append(" A.DENPYO_BANGO AS DENPYO_BANGO,");
            Sql.Append(" 0 AS GYO_BANGO,");
            Sql.Append(" MIN(A.TORIHIKI_KUBUN1) AS TORIHIKI_KUBUN,");
            Sql.Append(" MIN(A.TORIHIKI_KUBUN_NM) AS TORIHIKI_KUBUN_NM,");
            Sql.Append(" SUM(A.URIAGE_KINGAKU * A.ENZAN_HOHO) AS URIAGE_KINGAKU,");
            Sql.Append(" SUM(A.SHOHIZEI * A.ENZAN_HOHO) AS SHOHIZEIGAKU,");
            Sql.Append(" A.ZEI_RITSU AS ZEI_RITSU,");
            Sql.Append(" '' AS TEKIYO,");
            Sql.Append(" A.SHOHIZEI_NYURYOKU_HOHO AS SHOHIZEI_NYURYOKU_HOHO,");
            Sql.Append(" 0 AS ZEIKOMI_KINGAKU ");
            Sql.Append(" FROM VI_HN_TORIHIKI_MOTOCHO AS A ");
            Sql.Append(" WHERE ");
            Sql.Append(" 	A.KAISHA_CD = @KAISHA_CD AND ");
            if (shishoCd != "0")
                Sql.Append(" 	A.SHISHO_CD = @SHISHO_CD AND");
            Sql.Append(" 	A.DENPYO_KUBUN = 1 AND ");
            Sql.Append(" 	A.DENPYO_DATE Between @DATE_FR AND @DATE_TO AND ");
            Sql.Append(" 	A.TOKUISAKI_CD BETWEEN @FUNANUSHI_CD_FR AND @FUNANUSHI_CD_TO ");
            Sql.Append(" GROUP BY ");
            Sql.Append(" A.TOKUISAKI_CD,");
            Sql.Append(" A.DENPYO_DATE,");
            Sql.Append(" A.DENPYO_BANGO,");
            Sql.Append(" A.ZEI_RITSU,");
            Sql.Append(" A.SHOHIZEI_NYURYOKU_HOHO ");
            Sql.Append(" UNION ALL ");
            Sql.Append(" SELECT ");
            Sql.Append(" 	B.HOJO_KAMOKU_CD AS TORIHIKISAKI_CD,");
            Sql.Append(" 	B.DENPYO_DATE AS DENPYO_DATE,");
            Sql.Append(" 	B.DENPYO_BANGO AS DENPYO_BANGO,");
            Sql.Append(" 	B.GYO_BANGO AS GYO_BANGO,");
            Sql.Append(" 	'9' AS TORIHIKI_KUBUN,");
            Sql.Append(" 	'' AS TORIHIKI_KUBUN_NM,");
            Sql.Append(" 	0 AS URIAGE_KINGAKU,");
            Sql.Append(" 	0 AS SHOHIZEIGAKU,");
            Sql.Append(" 	0 AS ZEI_RITSU,");
            Sql.Append(" 	B.TEKIYO AS TEKIYO,");
            Sql.Append(" 	0 AS SHOHIZEI_NYURYOKU_HOHO,");
            Sql.Append(" 	CASE WHEN B.TAISHAKU_KUBUN = C.TAISHAKU_KUBUN THEN -1 * B.ZEIKOMI_KINGAKU ELSE B.ZEIKOMI_KINGAKU END AS ZEIKOMI_KINGAKU ");
            Sql.Append(" FROM TB_HN_NYUKIN_KAMOKU AS A ");
            Sql.Append(" LEFT OUTER JOIN TB_ZM_SHIWAKE_MEISAI AS B ");
            Sql.Append(" 	ON	A.KAISHA_CD = B.KAISHA_CD ");
            Sql.Append(" 	AND A.SHISHO_CD = B.SHISHO_CD");
            Sql.Append(" 	AND A.KANJO_KAMOKU_CD = B.KANJO_KAMOKU_CD ");
            Sql.Append(" 	AND 1 = B.DENPYO_KUBUN ");
            Sql.Append(" 	AND B.KAIKEI_NENDO = @KAIKEI_NENDO ");
            Sql.Append("  LEFT OUTER JOIN TB_ZM_KANJO_KAMOKU AS C ");
            Sql.Append(" 	ON 	B.KAISHA_CD = C.KAISHA_CD ");
            Sql.Append(" 	AND B.KANJO_KAMOKU_CD = C.KANJO_KAMOKU_CD ");
            Sql.Append(" 	AND B.KAIKEI_NENDO = C.KAIKEI_NENDO ");
            Sql.Append("  LEFT OUTER JOIN TB_HN_TORIHIKISAKI_JOHO AS D ");
            Sql.Append(" 	ON	B.KAISHA_CD = D.KAISHA_CD ");
            Sql.Append(" 	AND B.HOJO_KAMOKU_CD = D.TORIHIKISAKI_CD ");
            Sql.Append("  WHERE ");
            Sql.Append(" 	A.KAISHA_CD = @KAISHA_CD AND ");
            if (shishoCd != "0")
                Sql.Append(" 	A.SHISHO_CD = @SHISHO_CD AND");
            Sql.Append(" 	A.MOTOCHO_KUBUN = 11 AND ");
            Sql.Append(" 	B.HOJO_KAMOKU_CD BETWEEN @FUNANUSHI_CD_FR AND @FUNANUSHI_CD_TO AND ");
            Sql.Append(" 	B.DENPYO_DATE Between @DATE_FR AND @DATE_TO AND ");
            Sql.Append(" (ISNULL(B.SHIWAKE_SAKUSEI_KUBUN,0) = 0 AND B.ZEI_KUBUN <> 10) AND ");
            Sql.Append(" 	D.TORIHIKISAKI_KUBUN1 = 1 ");
            Sql.Append("  ORDER BY ");
            Sql.Append("  TORIHIKISAKI_CD ASC, ");
            Sql.Append("  DENPYO_DATE ASC,");
            Sql.Append("  TORIHIKI_KUBUN ASC,");
            Sql.Append("  DENPYO_BANGO ASC,");
            Sql.Append("  GYO_BANGO ASC");

            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.VarChar, 4, kaikeiNendo);
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@DATE_FR", SqlDbType.VarChar, 10, tmpDateFr.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@DATE_TO", SqlDbType.VarChar, 10, tmpDateTo.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@FUNANUSHI_CD_FR", SqlDbType.VarChar, 6, FUNANUSHI_CD_FR);
            dpc.SetParam("@FUNANUSHI_CD_TO", SqlDbType.VarChar, 6, FUNANUSHI_CD_TO);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);

            DataTable dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
            #endregion

            if (dtMainLoop.Rows.Count == 0)
            {
                #region 該当日付範囲のデータが無い場合
                int i = 0; // ループ用カウント変数
                int flag = 0; // 表示フラグ
                List<int> tirihikisakiCd = new List<int>(); // 取引先番号格納用変数
                while (dtKishu.Rows.Count > i)
                {
                    // if (Util.ToInt(dtKishu.Rows[i]["KISHUZAN"]) > 0)
                    if (Util.ToInt(dtKishu.Rows[i]["KISHUZAN"]) != 0)
                    {
                        tirihikisakiCd.Add(i);
                        flag = 1;
                    }
                    i++;
                }
                if (flag != 0)
                {
                    i = 0;
                    int dbSORT = 1;
                    while (tirihikisakiCd.Count > i)
                    {
                        #region 前月繰越テーブルの登録
                        #region インサートテーブル
                        Sql = new StringBuilder();
                        dpc = new DbParamCollection();
                        Sql.Append("INSERT INTO PR_HN_TBL(");
                        Sql.Append("  GUID");
                        Sql.Append(" ,SORT");
                        Sql.Append(" ,ITEM01");
                        Sql.Append(" ,ITEM02");
                        Sql.Append(" ,ITEM03");
                        Sql.Append(" ,ITEM04");
                        Sql.Append(" ,ITEM05");
                        Sql.Append(" ,ITEM06");
                        Sql.Append(" ,ITEM07");
                        Sql.Append(" ,ITEM08");
                        Sql.Append(" ,ITEM12");
                        Sql.Append(" ,ITEM17");
                        Sql.Append(" ,ITEM18");
                        Sql.Append(" ,ITEM23");
                        Sql.Append(") ");
                        Sql.Append("VALUES(");
                        Sql.Append("  @GUID");
                        Sql.Append(" ,@SORT");
                        Sql.Append(" ,@ITEM01");
                        Sql.Append(" ,@ITEM02");
                        Sql.Append(" ,@ITEM03");
                        Sql.Append(" ,@ITEM04");
                        Sql.Append(" ,@ITEM05");
                        Sql.Append(" ,@ITEM06");
                        Sql.Append(" ,@ITEM07");
                        Sql.Append(" ,@ITEM08");
                        Sql.Append(" ,@ITEM12");
                        Sql.Append(" ,@ITEM17");
                        Sql.Append(" ,@ITEM23");
                        Sql.Append(") ");
                        #endregion

                        #region ページヘッダーデータを設定
                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                        dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                        dbSORT++;
                        dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dtKishu.Rows[tirihikisakiCd[i]]["TORIHIKISAKI_CD"]); // 取引先コード
                        dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtKishu.Rows[tirihikisakiCd[i]]["TORIHIKISAKI_NM"]); // 取引先名
                        dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dtKishu.Rows[tirihikisakiCd[i]]["DENWA_BANGO"]); // 電話番号
                        dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtKishu.Rows[tirihikisakiCd[i]]["FAX_BANGO"]); // Fax番号
                        dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                        dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                        dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                        dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                        dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                        #endregion

                        dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "◇◇◇ 前月より繰越し ◇◇◇"); // 繰越時データ
                        dpc.SetParam("@ITEM17", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(dtKishu.Rows[tirihikisakiCd[i]]["KISHUZAN"]))); // 期首残高
                        dpc.SetParam("@ITEM23", SqlDbType.VarChar, 20, 0); // 0か1を判断 0は点線

                        this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                        #endregion

                        #region 空データの登録
                        #region インサートテーブル
                        Sql = new StringBuilder();
                        dpc = new DbParamCollection();
                        Sql.Append("INSERT INTO PR_HN_TBL(");
                        Sql.Append("  GUID");
                        Sql.Append(" ,SORT");
                        Sql.Append(" ,ITEM01");
                        Sql.Append(" ,ITEM02");
                        Sql.Append(" ,ITEM03");
                        Sql.Append(" ,ITEM04");
                        Sql.Append(" ,ITEM05");
                        Sql.Append(" ,ITEM06");
                        Sql.Append(" ,ITEM07");
                        Sql.Append(" ,ITEM08");
                        Sql.Append(" ,ITEM18");
                        Sql.Append(" ,ITEM23");
                        Sql.Append(") ");
                        Sql.Append("VALUES(");
                        Sql.Append("  @GUID");
                        Sql.Append(" ,@SORT");
                        Sql.Append(" ,@ITEM01");
                        Sql.Append(" ,@ITEM02");
                        Sql.Append(" ,@ITEM03");
                        Sql.Append(" ,@ITEM04");
                        Sql.Append(" ,@ITEM05");
                        Sql.Append(" ,@ITEM06");
                        Sql.Append(" ,@ITEM07");
                        Sql.Append(" ,@ITEM08");
                        Sql.Append(" ,@ITEM18");
                        Sql.Append(" ,@ITEM23");
                        Sql.Append(") ");
                        #endregion

                        #region ページヘッダーデータを設定
                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                        dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                        dbSORT++;
                        dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dtKishu.Rows[tirihikisakiCd[i]]["TORIHIKISAKI_CD"]); // 取引先コード
                        dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtKishu.Rows[tirihikisakiCd[i]]["TORIHIKISAKI_NM"]); // 取引先名
                        dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dtKishu.Rows[tirihikisakiCd[i]]["DENWA_BANGO"]); // 電話番号
                        dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtKishu.Rows[tirihikisakiCd[i]]["FAX_BANGO"]); // Fax番号
                        dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                        dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                        dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                        dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                        dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                        dpc.SetParam("@ITEM23", SqlDbType.VarChar, 20, 0); // 0か1を判断 0は点線
                        #endregion

                        this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                        #endregion

                        #region 次月繰越テーブルの登録
                        #region インサートテーブル
                        Sql = new StringBuilder();
                        dpc = new DbParamCollection();
                        Sql.Append("INSERT INTO PR_HN_TBL(");
                        Sql.Append("  GUID");
                        Sql.Append(" ,SORT");
                        Sql.Append(" ,ITEM01");
                        Sql.Append(" ,ITEM02");
                        Sql.Append(" ,ITEM03");
                        Sql.Append(" ,ITEM04");
                        Sql.Append(" ,ITEM05");
                        Sql.Append(" ,ITEM06");
                        Sql.Append(" ,ITEM07");
                        Sql.Append(" ,ITEM08");
                        Sql.Append(" ,ITEM12");
                        Sql.Append(" ,ITEM17");
                        Sql.Append(" ,ITEM18");
                        Sql.Append(" ,ITEM23");
                        Sql.Append(") ");
                        Sql.Append("VALUES(");
                        Sql.Append("  @GUID");
                        Sql.Append(" ,@SORT");
                        Sql.Append(" ,@ITEM01");
                        Sql.Append(" ,@ITEM02");
                        Sql.Append(" ,@ITEM03");
                        Sql.Append(" ,@ITEM04");
                        Sql.Append(" ,@ITEM05");
                        Sql.Append(" ,@ITEM06");
                        Sql.Append(" ,@ITEM07");
                        Sql.Append(" ,@ITEM08");
                        Sql.Append(" ,@ITEM12");
                        Sql.Append(" ,@ITEM17");
                        Sql.Append(" ,@ITEM18");
                        Sql.Append(" ,@ITEM23");
                        Sql.Append(") ");
                        #endregion

                        #region ページヘッダーデータを設定
                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                        dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                        dbSORT++;
                        dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dtKishu.Rows[tirihikisakiCd[i]]["TORIHIKISAKI_CD"]); // 取引先コード
                        dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtKishu.Rows[tirihikisakiCd[i]]["TORIHIKISAKI_NM"]); // 取引先名
                        dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dtKishu.Rows[tirihikisakiCd[i]]["DENWA_BANGO"]); // 電話番号
                        dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtKishu.Rows[tirihikisakiCd[i]]["FAX_BANGO"]); // Fax番号
                        dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                        dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                        dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                        dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                        dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                        #endregion

                        dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "◇◇◇ 次月へ繰越し ◇◇◇"); // 繰越時データ
                        dpc.SetParam("@ITEM17", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(dtKishu.Rows[tirihikisakiCd[i]]["KISHUZAN"]))); // 期首残高
                        dpc.SetParam("@ITEM23", SqlDbType.VarChar, 20, 0); // 0か1を判断 0は点線

                        this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                        #endregion

                        i++;
                    }
                }
                else
                {
                    Msg.Info("該当データがありません。");
                    return false;
                }
                #endregion
            }
            else
            {
                NumVals MONTH = new NumVals(); // 月グループ
                NumVals TORIHIKISAKI = new NumVals(); // 取引先グループ

                #region メインループ準備処理
                String tmpGetsu = null;// 月
                String tmpNextGetsu = null;// 比較用月
                String tmpBeforeGetsu = null;// 比較用月
                int tmpDenpyo = 0;// 伝票
                int tmpNextDenpyo = 0;// 比較用伝票
                int tmpBeforeDenpyo = 0;// 比較用伝票
                int tmpTorihikisakiCd = 0;// 取引先コード
                int tmpNextTorihikisakiCd = 0;// 比較用取引先コード
                int tmpBeforeTorihikisakiCd = 0;// 比較用取引先コード
                int flag = 0;// 空データ表示フラグ

                String tmpDenwaBango = "";//電話番号変数
                String tmpFaxBango = "";//FAX番号変数
                String tmpTorihikisakiNm = null;// 取引先名

                int i = 0; // ループ用カウント変数
                int j = 1; // 1件後のデータとの比較用カウンタ変数
                int k = -1; // 1件前のデータとの比較用カウンタ変数
                int l = 0; // ループ用カウント変数
                int Multiple = 51; // 指定行での改ページ用変数
                int Count = 1; // 指定行での改ページ用カウンタ変数
                int z = 0; // 期首残・電話番号・FAX番号・取引先名の取得用カウンタ変数
                int dbSORT = 1;

                DataRow[] dr; // メインループ用データ変数
                #endregion

                while (dtKishu.Rows.Count > l)
                {
                    dr = dtMainLoop.Select("TORIHIKISAKI_CD = " + Util.ToString(dtKishu.Rows[l]["TORIHIKISAKI_CD"]));

                    // if ((Util.ToDecimal(dtKishu.Rows[l]["KISHUZAN"]) > 0 || Util.ToDecimal(dtKishu.Rows[l]["ZANDAKA"]) > 0) && dr.Length == 0)
                    if ((Util.ToDecimal(dtKishu.Rows[l]["KISHUZAN"]) != 0 || Util.ToDecimal(dtKishu.Rows[l]["ZANDAKA"]) > 0) && dr.Length == 0)
                    {
                        #region 期首残又は前月残高があり、該当日付範囲のデータが無い船主の場合
                        #region 前月繰越テーブルの登録
                        #region インサートテーブル
                        Sql = new StringBuilder();
                        dpc = new DbParamCollection();
                        Sql.Append("INSERT INTO PR_HN_TBL(");
                        Sql.Append("  GUID");
                        Sql.Append(" ,SORT");
                        Sql.Append(" ,ITEM01");
                        Sql.Append(" ,ITEM02");
                        Sql.Append(" ,ITEM03");
                        Sql.Append(" ,ITEM04");
                        Sql.Append(" ,ITEM05");
                        Sql.Append(" ,ITEM06");
                        Sql.Append(" ,ITEM07");
                        Sql.Append(" ,ITEM08");
                        Sql.Append(" ,ITEM12");
                        Sql.Append(" ,ITEM17");
                        Sql.Append(" ,ITEM22");
                        Sql.Append(" ,ITEM23");
                        Sql.Append(") ");
                        Sql.Append("VALUES(");
                        Sql.Append("  @GUID");
                        Sql.Append(" ,@SORT");
                        Sql.Append(" ,@ITEM01");
                        Sql.Append(" ,@ITEM02");
                        Sql.Append(" ,@ITEM03");
                        Sql.Append(" ,@ITEM04");
                        Sql.Append(" ,@ITEM05");
                        Sql.Append(" ,@ITEM06");
                        Sql.Append(" ,@ITEM07");
                        Sql.Append(" ,@ITEM08");
                        Sql.Append(" ,@ITEM12");
                        Sql.Append(" ,@ITEM17");
                        Sql.Append(" ,@ITEM22");
                        Sql.Append(" ,@ITEM23");
                        Sql.Append(") ");
                        #endregion

                        #region ページヘッダーデータを設定
                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                        dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                        dbSORT++;
                        dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_CD"]); // 取引先コード
                        dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_NM"]); // 取引先名
                        dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dtKishu.Rows[l]["DENWA_BANGO"]); // 電話番号
                        dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtKishu.Rows[l]["FAX_BANGO"]); // Fax番号
                        dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                        dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                        dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                        dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                        dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                        #endregion

                        dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "◇◇◇ 前月より繰越し ◇◇◇"); // 繰越時データ
                        dpc.SetParam("@ITEM17", SqlDbType.VarChar, 20, Util.FormatNum(dtKishu.Rows[l]["KISHUZAN"])); // 期首残高
                        dpc.SetParam("@ITEM23", SqlDbType.VarChar, 20, 0); // 0か1を判断 0は点線

                        this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                        Count++; // 改ページ用カウンタ
                        #endregion

                        #region 次月繰越テーブルの登録
                        #region インサートテーブル
                        Sql = new StringBuilder();
                        dpc = new DbParamCollection();
                        Sql.Append("INSERT INTO PR_HN_TBL(");
                        Sql.Append("  GUID");
                        Sql.Append(" ,SORT");
                        Sql.Append(" ,ITEM01");
                        Sql.Append(" ,ITEM02");
                        Sql.Append(" ,ITEM03");
                        Sql.Append(" ,ITEM04");
                        Sql.Append(" ,ITEM05");
                        Sql.Append(" ,ITEM06");
                        Sql.Append(" ,ITEM07");
                        Sql.Append(" ,ITEM08");
                        Sql.Append(" ,ITEM12");
                        Sql.Append(" ,ITEM17");
                        Sql.Append(" ,ITEM22");
                        Sql.Append(" ,ITEM23");
                        Sql.Append(") ");
                        Sql.Append("VALUES(");
                        Sql.Append("  @GUID");
                        Sql.Append(" ,@SORT");
                        Sql.Append(" ,@ITEM01");
                        Sql.Append(" ,@ITEM02");
                        Sql.Append(" ,@ITEM03");
                        Sql.Append(" ,@ITEM04");
                        Sql.Append(" ,@ITEM05");
                        Sql.Append(" ,@ITEM06");
                        Sql.Append(" ,@ITEM07");
                        Sql.Append(" ,@ITEM08");
                        Sql.Append(" ,@ITEM12");
                        Sql.Append(" ,@ITEM17");
                        Sql.Append(" ,@ITEM22");
                        Sql.Append(" ,@ITEM23");
                        Sql.Append(") ");
                        #endregion

                        #region ページヘッダーデータを設定
                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                        dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                        dbSORT++;
                        dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_CD"]); // 取引先コード
                        dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_NM"]); // 取引先名
                        dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dtKishu.Rows[l]["DENWA_BANGO"]); // 電話番号
                        dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtKishu.Rows[l]["FAX_BANGO"]); // Fax番号
                        dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                        dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                        dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                        dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                        dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                        #endregion

                        dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "◇◇◇ 次月へ繰越し ◇◇◇"); // 繰越時データ
                        dpc.SetParam("@ITEM17", SqlDbType.VarChar, 20, Util.FormatNum(dtKishu.Rows[l]["KISHUZAN"])); // 期首残高
                        if (Count % Multiple == 0)
                        {
                            dpc.SetParam("@ITEM23", SqlDbType.VarChar, 20, 1); // 0か1を判断 1は実線
                        }
                        else
                        {
                            dpc.SetParam("@ITEM23", SqlDbType.VarChar, 20, 0); // 0か1を判断 0は点線
                        }

                        this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                        Count++; // 改ページ用カウンタ
                        #endregion
                        #endregion
                    }
                    else if (dr.Length > 0)
                    {
                        while (dr.Length > i)
                        {
                            #region データの準備
                            // 伝票日付を和暦に変換
                            DateTime d = Util.ToDate(dr[i]["DENPYO_DATE"]);
                            string[] denpyo_date = Util.ConvJpDate(d, this.Dba);
                            if (denpyo_date[3].Length == 1)
                            {
                                denpyo_date[3] = " " + denpyo_date[3];
                            }
                            if (denpyo_date[4].Length == 1)
                            {
                                denpyo_date[4] = " " + denpyo_date[4];
                            }
                            String wareki_denpyo_date = denpyo_date[2] + "/" + denpyo_date[3] + "/" + denpyo_date[4];

                            // 比較用データがnullの場合(取引先コードが変わった時)、期首残・電話番号・FAX番号・取引先名を取得
                            if (tmpNextGetsu == null)
                            {
                                while (dtKishu.Rows.Count > z)
                                {
                                    if (Util.ToInt(dtKishu.Rows[z]["TORIHIKISAKI_CD"]) == Util.ToInt(dr[i]["TORIHIKISAKI_CD"]))
                                    {
                                        tmpTorihikisakiNm = Util.ToString(dtKishu.Rows[z]["TORIHIKISAKI_NM"]);
                                        TORIHIKISAKI.ZANDAKA += Util.ToDecimal(dtKishu.Rows[z]["KISHUZAN"]);
                                        tmpDenwaBango = Util.ToString(dtKishu.Rows[z]["DENWA_BANGO"]);
                                        tmpFaxBango = Util.ToString(dtKishu.Rows[z]["FAX_BANGO"]);
                                        break;
                                    }
                                    z++;
                                }
                            }

                            // 現在の伝票日付の月・伝票番号・取引先コードを取得
                            tmpGetsu = denpyo_date[3];
                            tmpDenpyo = Util.ToInt(dr[i]["DENPYO_BANGO"]);
                            tmpTorihikisakiCd = Util.ToInt(dr[i]["TORIHIKISAKI_CD"]);

                            // 比較用の伝票日付の月・伝票番号を取得
                            if (j < dr.Length)
                            {
                                // 伝票日付を和暦に変換
                                DateTime tmpD = Util.ToDate(dr[j]["DENPYO_DATE"]);
                                string[] tmpDenpyoDate = Util.ConvJpDate(tmpD, this.Dba);
                                if (tmpDenpyoDate[3].Length == 1)
                                {
                                    tmpDenpyoDate[3] = " " + tmpDenpyoDate[3];
                                }
                                tmpNextGetsu = tmpDenpyoDate[3];
                                tmpNextDenpyo = Util.ToInt(dr[j]["DENPYO_BANGO"]);
                                tmpNextTorihikisakiCd = Util.ToInt(dr[j]["TORIHIKISAKI_CD"]);
                            }
                            if (k != -1)
                            {
                                // 伝票日付を和暦に変換
                                DateTime tmpD = Util.ToDate(dr[k]["DENPYO_DATE"]);
                                string[] tmpDenpyoDate = Util.ConvJpDate(tmpD, this.Dba);
                                if (tmpDenpyoDate[3].Length == 1)
                                {
                                    tmpDenpyoDate[3] = " " + tmpDenpyoDate[3];
                                }
                                tmpBeforeGetsu = tmpDenpyoDate[3];
                                tmpBeforeDenpyo = Util.ToInt(dr[k]["DENPYO_BANGO"]);
                                tmpBeforeTorihikisakiCd = Util.ToInt(dr[k]["TORIHIKISAKI_CD"]);
                            }
                            #endregion

                            #region 印刷ワークテーブルに登録
                            // 登録するデータを判別
                            if (Util.ToInt(dr[i]["DENPYO_BANGO"]) != 0)
                            {
                                // 現在の取引先コードと前回の取引先コードが違っていれば実行 // 前回の月と現在の月を比較し、違っていれば実行
                                if (tmpTorihikisakiCd != tmpBeforeTorihikisakiCd || tmpGetsu != tmpBeforeGetsu)
                                {
                                    #region 前月繰越テーブルの登録
                                    #region インサートテーブル
                                    Sql = new StringBuilder();
                                    dpc = new DbParamCollection();
                                    Sql.Append("INSERT INTO PR_HN_TBL(");
                                    Sql.Append("  GUID");
                                    Sql.Append(" ,SORT");
                                    Sql.Append(" ,ITEM01");
                                    Sql.Append(" ,ITEM02");
                                    Sql.Append(" ,ITEM03");
                                    Sql.Append(" ,ITEM04");
                                    Sql.Append(" ,ITEM05");
                                    Sql.Append(" ,ITEM06");
                                    Sql.Append(" ,ITEM07");
                                    Sql.Append(" ,ITEM08");
                                    Sql.Append(" ,ITEM12");
                                    Sql.Append(" ,ITEM17");
                                    Sql.Append(" ,ITEM18");
                                    Sql.Append(" ,ITEM23");
                                    Sql.Append(") ");
                                    Sql.Append("VALUES(");
                                    Sql.Append("  @GUID");
                                    Sql.Append(" ,@SORT");
                                    Sql.Append(" ,@ITEM01");
                                    Sql.Append(" ,@ITEM02");
                                    Sql.Append(" ,@ITEM03");
                                    Sql.Append(" ,@ITEM04");
                                    Sql.Append(" ,@ITEM05");
                                    Sql.Append(" ,@ITEM06");
                                    Sql.Append(" ,@ITEM07");
                                    Sql.Append(" ,@ITEM08");
                                    Sql.Append(" ,@ITEM12");
                                    Sql.Append(" ,@ITEM17");
                                    Sql.Append(" ,@ITEM18");
                                    Sql.Append(" ,@ITEM23");
                                    Sql.Append(") ");
                                    #endregion

                                    #region ページヘッダーデータを設定
                                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                    dbSORT++;
                                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_CD"]); // 取引先コード
                                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_NM"]); // 取引先名
                                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dtKishu.Rows[l]["DENWA_BANGO"]); // 電話番号
                                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtKishu.Rows[l]["FAX_BANGO"]); // Fax番号
                                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                                    dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                                    dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                                    #endregion

                                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "◇◇◇ 前月より繰越し ◇◇◇"); // 繰越時データ
                                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 期首残高
                                    if (Count % Multiple == 0)
                                    {
                                        dpc.SetParam("@ITEM23", SqlDbType.VarChar, 20, 1); // 0か1を判断 1は実線
                                    }
                                    else
                                    {
                                        dpc.SetParam("@ITEM23", SqlDbType.VarChar, 20, 0); // 0か1を判断 0は点線
                                    }

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                                    Count++; // 改ページ用カウンタ
                                    #endregion
                                }

                                // 改ページ用カウンタが51の倍数の時に実行
                                if (Count % Multiple == 0)
                                {
                                    #region 改ページ時処理
                                    #region インサートテーブル
                                    Sql = new StringBuilder();
                                    dpc = new DbParamCollection();
                                    Sql.Append("INSERT INTO PR_HN_TBL(");
                                    Sql.Append("  GUID");
                                    Sql.Append(" ,SORT");
                                    Sql.Append(" ,ITEM01");
                                    Sql.Append(" ,ITEM02");
                                    Sql.Append(" ,ITEM03");
                                    Sql.Append(" ,ITEM04");
                                    Sql.Append(" ,ITEM05");
                                    Sql.Append(" ,ITEM06");
                                    Sql.Append(" ,ITEM07");
                                    Sql.Append(" ,ITEM08");
                                    Sql.Append(" ,ITEM12");
                                    Sql.Append(" ,ITEM14");
                                    Sql.Append(" ,ITEM15");
                                    Sql.Append(" ,ITEM16");
                                    Sql.Append(" ,ITEM17");
                                    Sql.Append(" ,ITEM23");
                                    Sql.Append(") ");
                                    Sql.Append("VALUES(");
                                    Sql.Append("  @GUID");
                                    Sql.Append(" ,@SORT");
                                    Sql.Append(" ,@ITEM01");
                                    Sql.Append(" ,@ITEM02");
                                    Sql.Append(" ,@ITEM03");
                                    Sql.Append(" ,@ITEM04");
                                    Sql.Append(" ,@ITEM05");
                                    Sql.Append(" ,@ITEM06");
                                    Sql.Append(" ,@ITEM07");
                                    Sql.Append(" ,@ITEM08");
                                    Sql.Append(" ,@ITEM12");
                                    Sql.Append(" ,@ITEM14");
                                    Sql.Append(" ,@ITEM15");
                                    Sql.Append(" ,@ITEM16");
                                    Sql.Append(" ,@ITEM17");
                                    Sql.Append(" ,@ITEM18");
                                    Sql.Append(" ,@ITEM23");
                                    Sql.Append(") ");
                                    #endregion

                                    #region ページヘッダーデータを設定
                                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                    dbSORT++;
                                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_CD"]); // 取引先コード
                                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_NM"]); // 取引先名
                                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dtKishu.Rows[l]["DENWA_BANGO"]); // 電話番号
                                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtKishu.Rows[l]["FAX_BANGO"]); // Fax番号
                                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                                    dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                                    dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                                    #endregion

                                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "☆☆ 次頁へ繰越し ☆☆"); // 改ページ時文字列
                                    dpc.SetParam("@ITEM14", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.URIAGEGAKU))); // 売上金額合計
                                    dpc.SetParam("@ITEM15", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHOHIZEIGAKU))); // 消費税合計
                                    dpc.SetParam("@ITEM16", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.NYUKINGAKU))); // 入金額合計
                                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高
                                    dpc.SetParam("@ITEM23", SqlDbType.VarChar, 20, 1); // 0か1を判断 1は実線

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                                    Count++; // 改ページ用カウンタ
                                    #endregion
                                    
                                    #region 改ページ時処理
                                    #region インサートテーブル
                                    Sql = new StringBuilder();
                                    dpc = new DbParamCollection();
                                    Sql.Append("INSERT INTO PR_HN_TBL(");
                                    Sql.Append("  GUID");
                                    Sql.Append(" ,SORT");
                                    Sql.Append(" ,ITEM01");
                                    Sql.Append(" ,ITEM02");
                                    Sql.Append(" ,ITEM03");
                                    Sql.Append(" ,ITEM04");
                                    Sql.Append(" ,ITEM05");
                                    Sql.Append(" ,ITEM06");
                                    Sql.Append(" ,ITEM07");
                                    Sql.Append(" ,ITEM08");
                                    Sql.Append(" ,ITEM12");
                                    Sql.Append(" ,ITEM14");
                                    Sql.Append(" ,ITEM15");
                                    Sql.Append(" ,ITEM16");
                                    Sql.Append(" ,ITEM17");
                                    Sql.Append(" ,ITEM18");
                                    Sql.Append(" ,ITEM23");
                                    Sql.Append(") ");
                                    Sql.Append("VALUES(");
                                    Sql.Append("  @GUID");
                                    Sql.Append(" ,@SORT");
                                    Sql.Append(" ,@ITEM01");
                                    Sql.Append(" ,@ITEM02");
                                    Sql.Append(" ,@ITEM03");
                                    Sql.Append(" ,@ITEM04");
                                    Sql.Append(" ,@ITEM05");
                                    Sql.Append(" ,@ITEM06");
                                    Sql.Append(" ,@ITEM07");
                                    Sql.Append(" ,@ITEM08");
                                    Sql.Append(" ,@ITEM12");
                                    Sql.Append(" ,@ITEM14");
                                    Sql.Append(" ,@ITEM15");
                                    Sql.Append(" ,@ITEM16");
                                    Sql.Append(" ,@ITEM17");
                                    Sql.Append(" ,@ITEM18");
                                    Sql.Append(" ,@ITEM23");
                                    Sql.Append(") ");
                                    #endregion

                                    #region ページヘッダーデータを設定
                                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                    dbSORT++;
                                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_CD"]); // 取引先コード
                                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_NM"]); // 取引先名
                                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dtKishu.Rows[l]["DENWA_BANGO"]); // 電話番号
                                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtKishu.Rows[l]["FAX_BANGO"]); // Fax番号
                                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                                    dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                                    dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                                    #endregion

                                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "☆☆ 前頁より繰越し ☆☆"); // 改ページ時文字列
                                    dpc.SetParam("@ITEM14", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.URIAGEGAKU))); // 売上金額合計
                                    dpc.SetParam("@ITEM15", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHOHIZEIGAKU))); // 消費税合計
                                    dpc.SetParam("@ITEM16", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.NYUKINGAKU))); // 入金額合計
                                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高
                                    dpc.SetParam("@ITEM23", SqlDbType.VarChar, 20, 0); // 0か1を判断 0は点線

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                                    Count++; // 改ページ用カウンタ
                                    #endregion
                                }

                                // 常に実行
                                #region 実データ登録
                                #region インサートテーブル
                                Sql = new StringBuilder();
                                dpc = new DbParamCollection();
                                Sql.Append("INSERT INTO PR_HN_TBL(");
                                Sql.Append("  GUID");
                                Sql.Append(" ,SORT");
                                Sql.Append(" ,ITEM01");
                                Sql.Append(" ,ITEM02");
                                Sql.Append(" ,ITEM03");
                                Sql.Append(" ,ITEM04");
                                Sql.Append(" ,ITEM05");
                                Sql.Append(" ,ITEM06");
                                Sql.Append(" ,ITEM07");
                                Sql.Append(" ,ITEM08");
                                Sql.Append(" ,ITEM09");
                                Sql.Append(" ,ITEM10");
                                Sql.Append(" ,ITEM11");
                                Sql.Append(" ,ITEM12");
                                Sql.Append(" ,ITEM13");
                                Sql.Append(" ,ITEM14");
                                Sql.Append(" ,ITEM15");
                                Sql.Append(" ,ITEM16");
                                Sql.Append(" ,ITEM17");
                                Sql.Append(" ,ITEM18");
                                Sql.Append(" ,ITEM23");
                                Sql.Append(") ");
                                Sql.Append("VALUES(");
                                Sql.Append("  @GUID");
                                Sql.Append(" ,@SORT");
                                Sql.Append(" ,@ITEM01");
                                Sql.Append(" ,@ITEM02");
                                Sql.Append(" ,@ITEM03");
                                Sql.Append(" ,@ITEM04");
                                Sql.Append(" ,@ITEM05");
                                Sql.Append(" ,@ITEM06");
                                Sql.Append(" ,@ITEM07");
                                Sql.Append(" ,@ITEM08");
                                Sql.Append(" ,@ITEM09");
                                Sql.Append(" ,@ITEM10");
                                Sql.Append(" ,@ITEM11");
                                Sql.Append(" ,@ITEM12");
                                Sql.Append(" ,@ITEM13");
                                Sql.Append(" ,@ITEM14");
                                Sql.Append(" ,@ITEM15");
                                Sql.Append(" ,@ITEM16");
                                Sql.Append(" ,@ITEM17");
                                Sql.Append(" ,@ITEM18");
                                Sql.Append(" ,@ITEM23");
                                Sql.Append(") ");
                                #endregion

                                #region ページヘッダーデータを設定
                                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                dbSORT++;
                                dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_CD"]); // 取引先コード
                                dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_NM"]); // 取引先名
                                dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dtKishu.Rows[l]["DENWA_BANGO"]); // 電話番号
                                dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtKishu.Rows[l]["FAX_BANGO"]); // Fax番号
                                dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                                dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                                dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                                //dpc.SetParam("@ITEM23", SqlDbType.VarChar, 20, 0); // 0か1を判断 0
                                if (Count % Multiple == 0)
                                {
                                    dpc.SetParam("@ITEM23", SqlDbType.VarChar, 20, 1); // 0か1を判断 1は実線
                                }
                                else
                                {
                                    dpc.SetParam("@ITEM23", SqlDbType.VarChar, 20, 0); // 0か1を判断 0は点線
                                }
                                #endregion

                                // 現金売上時
                                if (Util.ToInt(dr[i]["TORIHIKI_KUBUN"]) == 2)
                                {
                                    dpc.SetParam("@ITEM09", SqlDbType.VarChar, 20, wareki_denpyo_date); // 伝票日付
                                    dpc.SetParam("@ITEM10", SqlDbType.VarChar, 20, Util.ToString(dr[i]["DENPYO_BANGO"])); // 伝票No
                                    dpc.SetParam("@ITEM11", SqlDbType.VarChar, 20, Util.ToString(dr[i]["TORIHIKI_KUBUN_NM"])); // 取引区分
                                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, ""); // 文字列データ
                                    dpc.SetParam("@ITEM13", SqlDbType.VarChar, 20, ""); // 摘要
                                    dpc.SetParam("@ITEM14", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(dr[i]["URIAGE_KINGAKU"]))); // 売上金額
                                    dpc.SetParam("@ITEM15", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(dr[i]["SHOHIZEIGAKU"]))); // 消費税額
                                    dpc.SetParam("@ITEM16", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(dr[i]["URIAGE_KINGAKU"]) + Util.ToDecimal(dr[i]["SHOHIZEIGAKU"]))); // 入金額
                                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                                    // 月グループ
                                    MONTH.URIAGEGAKU += Util.ToDecimal(dr[i]["URIAGE_KINGAKU"]); // 月売上金額合計用
                                    MONTH.SHOHIZEIGAKU += Util.ToDecimal(dr[i]["SHOHIZEIGAKU"]); // 月消費税額合計用
                                    MONTH.NYUKINGAKU += Util.ToDecimal(dr[i]["URIAGE_KINGAKU"]) + Util.ToDecimal(dr[i]["SHOHIZEIGAKU"]); // 月入金額合計用
                                    // 取引先グループ
                                    TORIHIKISAKI.URIAGEGAKU += Util.ToDecimal(dr[i]["URIAGE_KINGAKU"]); // 取引先売上金額合計用
                                    TORIHIKISAKI.SHOHIZEIGAKU += Util.ToDecimal(dr[i]["SHOHIZEIGAKU"]); // 取引先消費税額合計用
                                    TORIHIKISAKI.NYUKINGAKU += Util.ToDecimal(dr[i]["URIAGE_KINGAKU"]) + Util.ToDecimal(dr[i]["SHOHIZEIGAKU"]); // 取引先入金額合計用

                                }
                                // 売上時
                                else if (Util.ToInt(dr[i]["TORIHIKI_KUBUN"]) != 9)
                                {
                                    dpc.SetParam("@ITEM09", SqlDbType.VarChar, 20, wareki_denpyo_date); // 伝票日付
                                    dpc.SetParam("@ITEM10", SqlDbType.VarChar, 20, Util.ToString(dr[i]["DENPYO_BANGO"])); // 伝票No
                                    dpc.SetParam("@ITEM11", SqlDbType.VarChar, 20, Util.ToString(dr[i]["TORIHIKI_KUBUN_NM"])); // 取引区分
                                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, ""); // 文字列データ
                                    dpc.SetParam("@ITEM13", SqlDbType.VarChar, 20, dr[i]["TEKIYO"]); // 摘要
                                    dpc.SetParam("@ITEM14", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(dr[i]["URIAGE_KINGAKU"]))); // 売上金額
                                    dpc.SetParam("@ITEM15", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(dr[i]["SHOHIZEIGAKU"]))); // 消費税額
                                    dpc.SetParam("@ITEM16", SqlDbType.VarChar, 20, ""); // 入金額
                                    TORIHIKISAKI.ZANDAKA += Util.ToDecimal(dr[i]["URIAGE_KINGAKU"]) + Util.ToDecimal(dr[i]["SHOHIZEIGAKU"]); // 合計用残高
                                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高
                                    
                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                                    // 月グループ
                                    MONTH.URIAGEGAKU += Util.ToDecimal(dr[i]["URIAGE_KINGAKU"]); // 月売上金額合計用
                                    MONTH.SHOHIZEIGAKU += Util.ToDecimal(dr[i]["SHOHIZEIGAKU"]); // 月消費税額合計用
                                    MONTH.NYUKINGAKU += 0; // 月入金額合計用
                                    // 取引先グループ
                                    TORIHIKISAKI.URIAGEGAKU += Util.ToDecimal(dr[i]["URIAGE_KINGAKU"]); // 取引先売上金額合計用
                                    TORIHIKISAKI.SHOHIZEIGAKU += Util.ToDecimal(dr[i]["SHOHIZEIGAKU"]); // 取引先消費税額合計用
                                    TORIHIKISAKI.NYUKINGAKU += 0; // 取引先入金額合計用

                                }
                                // 入金時
                                else
                                {
                                    dpc.SetParam("@ITEM09", SqlDbType.VarChar, 20, wareki_denpyo_date); // 伝票日付
                                    dpc.SetParam("@ITEM10", SqlDbType.VarChar, 20, Util.ToString(dr[i]["DENPYO_BANGO"])); // 伝票No
                                    dpc.SetParam("@ITEM11", SqlDbType.VarChar, 20, ""); // 取引区分
                                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, ""); // 文字列データ
                                    dpc.SetParam("@ITEM13", SqlDbType.VarChar, 20, dr[i]["TEKIYO"]); // 摘要
                                    dpc.SetParam("@ITEM14", SqlDbType.VarChar, 20, ""); // 売上金額
                                    dpc.SetParam("@ITEM15", SqlDbType.VarChar, 20, ""); // 消費税額
                                    dpc.SetParam("@ITEM16", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(dr[i]["ZEIKOMI_KINGAKU"]))); // 入金額
                                    TORIHIKISAKI.ZANDAKA -= Util.ToDecimal(dr[i]["ZEIKOMI_KINGAKU"]); // 合計用残高
                                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高
                                    
                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                                    // 月グループ
                                    MONTH.URIAGEGAKU += 0; // 月売上金額合計用
                                    MONTH.SHOHIZEIGAKU += 0; // 月消費税額合計用
                                    MONTH.NYUKINGAKU += Util.ToDecimal(dr[i]["ZEIKOMI_KINGAKU"]); // 月入金額合計用
                                    // 取引先グループ
                                    TORIHIKISAKI.URIAGEGAKU += 0; // 取引先売上金額合計用
                                    TORIHIKISAKI.SHOHIZEIGAKU += 0; // 取引先消費税額合計用
                                    TORIHIKISAKI.NYUKINGAKU += Util.ToDecimal(dr[i]["ZEIKOMI_KINGAKU"]); // 取引先入金額合計用
                                }
                                Count++; // 改ページ用カウンタ
                                #endregion

                                // 改ページ用カウンタが51の倍数の時に実行
                                if (Count % Multiple == 0)
                                {
                                    #region 改ページ時処理
                                    #region インサートテーブル
                                    Sql = new StringBuilder();
                                    dpc = new DbParamCollection();
                                    Sql.Append("INSERT INTO PR_HN_TBL(");
                                    Sql.Append("  GUID");
                                    Sql.Append(" ,SORT");
                                    Sql.Append(" ,ITEM01");
                                    Sql.Append(" ,ITEM02");
                                    Sql.Append(" ,ITEM03");
                                    Sql.Append(" ,ITEM04");
                                    Sql.Append(" ,ITEM05");
                                    Sql.Append(" ,ITEM06");
                                    Sql.Append(" ,ITEM07");
                                    Sql.Append(" ,ITEM08");
                                    Sql.Append(" ,ITEM12");
                                    Sql.Append(" ,ITEM14");
                                    Sql.Append(" ,ITEM15");
                                    Sql.Append(" ,ITEM16");
                                    Sql.Append(" ,ITEM17");
                                    Sql.Append(" ,ITEM18");
                                    Sql.Append(" ,ITEM23");
                                    Sql.Append(") ");
                                    Sql.Append("VALUES(");
                                    Sql.Append("  @GUID");
                                    Sql.Append(" ,@SORT");
                                    Sql.Append(" ,@ITEM01");
                                    Sql.Append(" ,@ITEM02");
                                    Sql.Append(" ,@ITEM03");
                                    Sql.Append(" ,@ITEM04");
                                    Sql.Append(" ,@ITEM05");
                                    Sql.Append(" ,@ITEM06");
                                    Sql.Append(" ,@ITEM07");
                                    Sql.Append(" ,@ITEM08");
                                    Sql.Append(" ,@ITEM12");
                                    Sql.Append(" ,@ITEM14");
                                    Sql.Append(" ,@ITEM15");
                                    Sql.Append(" ,@ITEM16");
                                    Sql.Append(" ,@ITEM17");
                                    Sql.Append(" ,@ITEM18");
                                    Sql.Append(" ,@ITEM23");
                                    Sql.Append(") ");
                                    #endregion

                                    #region ページヘッダーデータを設定
                                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                    dbSORT++;
                                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_CD"]); // 取引先コード
                                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_NM"]); // 取引先名
                                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dtKishu.Rows[l]["DENWA_BANGO"]); // 電話番号
                                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtKishu.Rows[l]["FAX_BANGO"]); // Fax番号
                                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                                    dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                                    dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                                    #endregion

                                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "☆☆ 次頁へ繰越し ☆☆"); // 改ページ時文字列
                                    dpc.SetParam("@ITEM14", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.URIAGEGAKU))); // 売上金額合計
                                    dpc.SetParam("@ITEM15", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHOHIZEIGAKU))); // 消費税合計
                                    dpc.SetParam("@ITEM16", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.NYUKINGAKU))); // 入金額合計
                                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高
                                    dpc.SetParam("@ITEM23", SqlDbType.VarChar, 20, 1); // 0か1を判断 1は実線

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                                    Count++; // 改ページ用カウンタ
                                    #endregion

                                    #region 改ページ時処理
                                    #region インサートテーブル
                                    Sql = new StringBuilder();
                                    dpc = new DbParamCollection();
                                    Sql.Append("INSERT INTO PR_HN_TBL(");
                                    Sql.Append("  GUID");
                                    Sql.Append(" ,SORT");
                                    Sql.Append(" ,ITEM01");
                                    Sql.Append(" ,ITEM02");
                                    Sql.Append(" ,ITEM03");
                                    Sql.Append(" ,ITEM04");
                                    Sql.Append(" ,ITEM05");
                                    Sql.Append(" ,ITEM06");
                                    Sql.Append(" ,ITEM07");
                                    Sql.Append(" ,ITEM08");
                                    Sql.Append(" ,ITEM12");
                                    Sql.Append(" ,ITEM14");
                                    Sql.Append(" ,ITEM15");
                                    Sql.Append(" ,ITEM16");
                                    Sql.Append(" ,ITEM17");
                                    Sql.Append(" ,ITEM18");
                                    Sql.Append(" ,ITEM23");
                                    Sql.Append(") ");
                                    Sql.Append("VALUES(");
                                    Sql.Append("  @GUID");
                                    Sql.Append(" ,@SORT");
                                    Sql.Append(" ,@ITEM01");
                                    Sql.Append(" ,@ITEM02");
                                    Sql.Append(" ,@ITEM03");
                                    Sql.Append(" ,@ITEM04");
                                    Sql.Append(" ,@ITEM05");
                                    Sql.Append(" ,@ITEM06");
                                    Sql.Append(" ,@ITEM07");
                                    Sql.Append(" ,@ITEM08");
                                    Sql.Append(" ,@ITEM12");
                                    Sql.Append(" ,@ITEM14");
                                    Sql.Append(" ,@ITEM15");
                                    Sql.Append(" ,@ITEM16");
                                    Sql.Append(" ,@ITEM17");
                                    Sql.Append(" ,@ITEM18");
                                    Sql.Append(" ,@ITEM23");
                                    Sql.Append(") ");
                                    #endregion

                                    #region ページヘッダーデータを設定
                                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                    dbSORT++;
                                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_CD"]); // 取引先コード
                                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_NM"]); // 取引先名
                                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dtKishu.Rows[l]["DENWA_BANGO"]); // 電話番号
                                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtKishu.Rows[l]["FAX_BANGO"]); // Fax番号
                                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                                    dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                                    dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                                    #endregion

                                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "☆☆ 前頁より繰越し ☆☆"); // 改ページ時文字列
                                    dpc.SetParam("@ITEM14", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.URIAGEGAKU))); // 売上金額合計
                                    dpc.SetParam("@ITEM15", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHOHIZEIGAKU))); // 消費税合計
                                    dpc.SetParam("@ITEM16", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.NYUKINGAKU))); // 入金額合計
                                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高
                                    dpc.SetParam("@ITEM23", SqlDbType.VarChar, 20, 0); // 0か1を判断 0は点線

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                                    Count++; // 改ページ用カウンタ
                                    #endregion
                                }

                                // 最後のデータ(次回カウンタと総データ数が一致)の時に実行 // 現在の月と次回の月が違っていれば実行 // 現在の取引先コードと次回の取引先コードが違っていれば実行
                                if (dr.Length == j || tmpGetsu != tmpNextGetsu || tmpTorihikisakiCd != tmpNextTorihikisakiCd)
                                {
                                    #region 月合計テーブルの登録
                                    #region インサートテーブル
                                    Sql = new StringBuilder();
                                    dpc = new DbParamCollection();
                                    Sql.Append("INSERT INTO PR_HN_TBL(");
                                    Sql.Append("  GUID");
                                    Sql.Append(" ,SORT");
                                    Sql.Append(" ,ITEM01");
                                    Sql.Append(" ,ITEM02");
                                    Sql.Append(" ,ITEM03");
                                    Sql.Append(" ,ITEM04");
                                    Sql.Append(" ,ITEM05");
                                    Sql.Append(" ,ITEM06");
                                    Sql.Append(" ,ITEM07");
                                    Sql.Append(" ,ITEM08");
                                    Sql.Append(" ,ITEM12");
                                    Sql.Append(" ,ITEM14");
                                    Sql.Append(" ,ITEM15");
                                    Sql.Append(" ,ITEM16");
                                    Sql.Append(" ,ITEM17");
                                    Sql.Append(" ,ITEM18");
                                    Sql.Append(" ,ITEM23");
                                    Sql.Append(") ");
                                    Sql.Append("VALUES(");
                                    Sql.Append("  @GUID");
                                    Sql.Append(" ,@SORT");
                                    Sql.Append(" ,@ITEM01");
                                    Sql.Append(" ,@ITEM02");
                                    Sql.Append(" ,@ITEM03");
                                    Sql.Append(" ,@ITEM04");
                                    Sql.Append(" ,@ITEM05");
                                    Sql.Append(" ,@ITEM06");
                                    Sql.Append(" ,@ITEM07");
                                    Sql.Append(" ,@ITEM08");
                                    Sql.Append(" ,@ITEM12");
                                    Sql.Append(" ,@ITEM14");
                                    Sql.Append(" ,@ITEM15");
                                    Sql.Append(" ,@ITEM16");
                                    Sql.Append(" ,@ITEM17");
                                    Sql.Append(" ,@ITEM18");
                                    Sql.Append(" ,@ITEM23");
                                    Sql.Append(") ");
                                    #endregion

                                    #region ページヘッダーデータを設定
                                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                    dbSORT++;
                                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_CD"]); // 取引先コード
                                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_NM"]); // 取引先名
                                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dtKishu.Rows[l]["DENWA_BANGO"]); // 電話番号
                                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtKishu.Rows[l]["FAX_BANGO"]); // Fax番号
                                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                                    dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                                    dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                                    #endregion

                                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "【　　" + tmpGetsu + "月度 合計】"); // 月度合計
                                    dpc.SetParam("@ITEM14", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.URIAGEGAKU))); // 売上金額合計
                                    if (MONTH.SHOHIZEIGAKU == 0)
                                    {
                                        dpc.SetParam("@ITEM15", SqlDbType.VarChar, 20, ""); // 消費税合計
                                    }
                                    else
                                    {
                                        dpc.SetParam("@ITEM15", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHOHIZEIGAKU))); // 消費税合計
                                    }
                                    if (MONTH.NYUKINGAKU == 0)
                                    {
                                        dpc.SetParam("@ITEM16", SqlDbType.VarChar, 20, ""); // 入金額合計
                                    }
                                    else
                                    {
                                        dpc.SetParam("@ITEM16", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.NYUKINGAKU))); // 入金額合計
                                    }
                                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高
                                    if (Count % Multiple == 0)
                                    {
                                        dpc.SetParam("@ITEM23", SqlDbType.VarChar, 20, 1); // 0か1を判断 1は実線
                                    }
                                    else
                                    {
                                        dpc.SetParam("@ITEM23", SqlDbType.VarChar, 20, 0); // 0か1を判断 0は点線
                                    }

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                                    // 月合計をクリア
                                    MONTH.Clear();
                                    Count++; // 改ページ用カウンタ
                                    #endregion
                                }

                                // 改ページ用カウンタが51の時に実行
                                if (Count % Multiple == 0)
                                {
                                    #region 改ページ時処理
                                    #region インサートテーブル
                                    Sql = new StringBuilder();
                                    dpc = new DbParamCollection();
                                    Sql.Append("INSERT INTO PR_HN_TBL(");
                                    Sql.Append("  GUID");
                                    Sql.Append(" ,SORT");
                                    Sql.Append(" ,ITEM01");
                                    Sql.Append(" ,ITEM02");
                                    Sql.Append(" ,ITEM03");
                                    Sql.Append(" ,ITEM04");
                                    Sql.Append(" ,ITEM05");
                                    Sql.Append(" ,ITEM06");
                                    Sql.Append(" ,ITEM07");
                                    Sql.Append(" ,ITEM08");
                                    Sql.Append(" ,ITEM12");
                                    Sql.Append(" ,ITEM14");
                                    Sql.Append(" ,ITEM15");
                                    Sql.Append(" ,ITEM16");
                                    Sql.Append(" ,ITEM17");
                                    Sql.Append(" ,ITEM18");
                                    Sql.Append(" ,ITEM23");
                                    Sql.Append(") ");
                                    Sql.Append("VALUES(");
                                    Sql.Append("  @GUID");
                                    Sql.Append(" ,@SORT");
                                    Sql.Append(" ,@ITEM01");
                                    Sql.Append(" ,@ITEM02");
                                    Sql.Append(" ,@ITEM03");
                                    Sql.Append(" ,@ITEM04");
                                    Sql.Append(" ,@ITEM05");
                                    Sql.Append(" ,@ITEM06");
                                    Sql.Append(" ,@ITEM07");
                                    Sql.Append(" ,@ITEM08");
                                    Sql.Append(" ,@ITEM12");
                                    Sql.Append(" ,@ITEM14");
                                    Sql.Append(" ,@ITEM15");
                                    Sql.Append(" ,@ITEM16");
                                    Sql.Append(" ,@ITEM17");
                                    Sql.Append(" ,@ITEM18");
                                    Sql.Append(" ,@ITEM23");
                                    Sql.Append(") ");
                                    #endregion

                                    #region ページヘッダーデータを設定
                                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                    dbSORT++;
                                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_CD"]); // 取引先コード
                                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_NM"]); // 取引先名
                                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dtKishu.Rows[l]["DENWA_BANGO"]); // 電話番号
                                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtKishu.Rows[l]["FAX_BANGO"]); // Fax番号
                                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                                    dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                                    dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                                    #endregion

                                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "☆☆ 次頁へ繰越し ☆☆"); // 改ページ時文字列
                                    dpc.SetParam("@ITEM14", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.URIAGEGAKU))); // 売上金額合計
                                    dpc.SetParam("@ITEM15", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHOHIZEIGAKU))); // 消費税合計
                                    dpc.SetParam("@ITEM16", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.NYUKINGAKU))); // 入金額合計
                                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高
                                    dpc.SetParam("@ITEM23", SqlDbType.VarChar, 20, 1); // 0か1を判断 1は実線

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                                    Count++; // 改ページ用カウンタ
                                    #endregion

                                    #region 改ページ時処理
                                    #region インサートテーブル
                                    Sql = new StringBuilder();
                                    dpc = new DbParamCollection();
                                    Sql.Append("INSERT INTO PR_HN_TBL(");
                                    Sql.Append("  GUID");
                                    Sql.Append(" ,SORT");
                                    Sql.Append(" ,ITEM01");
                                    Sql.Append(" ,ITEM02");
                                    Sql.Append(" ,ITEM03");
                                    Sql.Append(" ,ITEM04");
                                    Sql.Append(" ,ITEM05");
                                    Sql.Append(" ,ITEM06");
                                    Sql.Append(" ,ITEM07");
                                    Sql.Append(" ,ITEM08");
                                    Sql.Append(" ,ITEM12");
                                    Sql.Append(" ,ITEM14");
                                    Sql.Append(" ,ITEM15");
                                    Sql.Append(" ,ITEM16");
                                    Sql.Append(" ,ITEM17");
                                    Sql.Append(" ,ITEM18");
                                    Sql.Append(" ,ITEM23");
                                    Sql.Append(") ");
                                    Sql.Append("VALUES(");
                                    Sql.Append("  @GUID");
                                    Sql.Append(" ,@SORT");
                                    Sql.Append(" ,@ITEM01");
                                    Sql.Append(" ,@ITEM02");
                                    Sql.Append(" ,@ITEM03");
                                    Sql.Append(" ,@ITEM04");
                                    Sql.Append(" ,@ITEM05");
                                    Sql.Append(" ,@ITEM06");
                                    Sql.Append(" ,@ITEM07");
                                    Sql.Append(" ,@ITEM08");
                                    Sql.Append(" ,@ITEM12");
                                    Sql.Append(" ,@ITEM14");
                                    Sql.Append(" ,@ITEM15");
                                    Sql.Append(" ,@ITEM16");
                                    Sql.Append(" ,@ITEM17");
                                    Sql.Append(" ,@ITEM18");
                                    Sql.Append(" ,@ITEM23");
                                    Sql.Append(") ");
                                    #endregion

                                    #region ページヘッダーデータを設定
                                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                    dbSORT++;
                                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_CD"]); // 取引先コード
                                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_NM"]); // 取引先名
                                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dtKishu.Rows[l]["DENWA_BANGO"]); // 電話番号
                                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtKishu.Rows[l]["FAX_BANGO"]); // Fax番号
                                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                                    dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                                    dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                                    #endregion

                                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "☆☆ 前頁より繰越し ☆☆"); // 改ページ時文字列
                                    dpc.SetParam("@ITEM14", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.URIAGEGAKU))); // 売上金額合計
                                    dpc.SetParam("@ITEM15", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHOHIZEIGAKU))); // 消費税合計
                                    dpc.SetParam("@ITEM16", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.NYUKINGAKU))); // 入金額合計
                                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高
                                    dpc.SetParam("@ITEM23", SqlDbType.VarChar, 20, 0); // 0か1を判断 0は実線

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                                    Count++; // 改ページ用カウンタ
                                    #endregion
                                }

                                // 最後のデータ(次回カウンタと総データ数が一致)の時に実行 // 現在の月と次回の月が違っていれば実行 // 現在の取引先コードと次回の取引先コードが違っていれば実行
                                if (dr.Length == j || tmpGetsu != tmpNextGetsu || tmpTorihikisakiCd != tmpNextTorihikisakiCd)
                                {
                                    #region 次月繰越テーブルの登録
                                    #region インサートテーブル
                                    Sql = new StringBuilder();
                                    dpc = new DbParamCollection();
                                    Sql.Append("INSERT INTO PR_HN_TBL(");
                                    Sql.Append("  GUID");
                                    Sql.Append(" ,SORT");
                                    Sql.Append(" ,ITEM01");
                                    Sql.Append(" ,ITEM02");
                                    Sql.Append(" ,ITEM03");
                                    Sql.Append(" ,ITEM04");
                                    Sql.Append(" ,ITEM05");
                                    Sql.Append(" ,ITEM06");
                                    Sql.Append(" ,ITEM07");
                                    Sql.Append(" ,ITEM08");
                                    Sql.Append(" ,ITEM12");
                                    Sql.Append(" ,ITEM17");
                                    Sql.Append(" ,ITEM18");
                                    Sql.Append(" ,ITEM23");
                                    Sql.Append(") ");
                                    Sql.Append("VALUES(");
                                    Sql.Append("  @GUID");
                                    Sql.Append(" ,@SORT");
                                    Sql.Append(" ,@ITEM01");
                                    Sql.Append(" ,@ITEM02");
                                    Sql.Append(" ,@ITEM03");
                                    Sql.Append(" ,@ITEM04");
                                    Sql.Append(" ,@ITEM05");
                                    Sql.Append(" ,@ITEM06");
                                    Sql.Append(" ,@ITEM07");
                                    Sql.Append(" ,@ITEM08");
                                    Sql.Append(" ,@ITEM12");
                                    Sql.Append(" ,@ITEM17");
                                    Sql.Append(" ,@ITEM18");
                                    Sql.Append(" ,@ITEM23");
                                    Sql.Append(") ");
                                    #endregion

                                    #region ページヘッダーデータを設定
                                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                    dbSORT++;
                                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_CD"]); // 取引先コード
                                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_NM"]); // 取引先名
                                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dtKishu.Rows[l]["DENWA_BANGO"]); // 電話番号
                                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtKishu.Rows[l]["FAX_BANGO"]); // Fax番号
                                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                                    dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                                    dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                                    #endregion

                                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "◇◇◇ 次月へ繰越し ◇◇◇"); // 繰越時データ
                                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 期首残高
                                    if (Count % Multiple == 0)
                                    {
                                        dpc.SetParam("@ITEM23", SqlDbType.VarChar, 20, 1); // 0か1を判断 1は実線
                                    }
                                    else
                                    {
                                        dpc.SetParam("@ITEM23", SqlDbType.VarChar, 20, 0); // 0か1を判断 0は点線
                                    }

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                                    flag = 1;// 空データ表示フラグ
                                    Count++; // 改ページ用カウンタ
                                    #endregion
                                }

                                // 改ページ用カウンタが51の倍数の時に実行
                                if (Count % Multiple == 0)
                                {
                                    #region 改ページ時処理
                                    #region インサートテーブル
                                    Sql = new StringBuilder();
                                    dpc = new DbParamCollection();
                                    Sql.Append("INSERT INTO PR_HN_TBL(");
                                    Sql.Append("  GUID");
                                    Sql.Append(" ,SORT");
                                    Sql.Append(" ,ITEM01");
                                    Sql.Append(" ,ITEM02");
                                    Sql.Append(" ,ITEM03");
                                    Sql.Append(" ,ITEM04");
                                    Sql.Append(" ,ITEM05");
                                    Sql.Append(" ,ITEM06");
                                    Sql.Append(" ,ITEM07");
                                    Sql.Append(" ,ITEM08");
                                    Sql.Append(" ,ITEM12");
                                    Sql.Append(" ,ITEM14");
                                    Sql.Append(" ,ITEM15");
                                    Sql.Append(" ,ITEM16");
                                    Sql.Append(" ,ITEM17");
                                    Sql.Append(" ,ITEM18");
                                    Sql.Append(" ,ITEM23");
                                    Sql.Append(") ");
                                    Sql.Append("VALUES(");
                                    Sql.Append("  @GUID");
                                    Sql.Append(" ,@SORT");
                                    Sql.Append(" ,@ITEM01");
                                    Sql.Append(" ,@ITEM02");
                                    Sql.Append(" ,@ITEM03");
                                    Sql.Append(" ,@ITEM04");
                                    Sql.Append(" ,@ITEM05");
                                    Sql.Append(" ,@ITEM06");
                                    Sql.Append(" ,@ITEM07");
                                    Sql.Append(" ,@ITEM08");
                                    Sql.Append(" ,@ITEM12");
                                    Sql.Append(" ,@ITEM14");
                                    Sql.Append(" ,@ITEM15");
                                    Sql.Append(" ,@ITEM16");
                                    Sql.Append(" ,@ITEM17");
                                    Sql.Append(" ,@ITEM18");
                                    Sql.Append(" ,@ITEM23");
                                    Sql.Append(") ");
                                    #endregion

                                    #region ページヘッダーデータを設定
                                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                    dbSORT++;
                                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_CD"]); // 取引先コード
                                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_NM"]); // 取引先名
                                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dtKishu.Rows[l]["DENWA_BANGO"]); // 電話番号
                                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtKishu.Rows[l]["FAX_BANGO"]); // Fax番号
                                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                                    dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                                    dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                                    #endregion

                                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "☆☆ 次頁へ繰越し ☆☆"); // 改ページ時文字列
                                    dpc.SetParam("@ITEM14", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.URIAGEGAKU))); // 売上金額合計
                                    dpc.SetParam("@ITEM15", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHOHIZEIGAKU))); // 消費税合計
                                    dpc.SetParam("@ITEM16", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.NYUKINGAKU))); // 入金額合計
                                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高
                                    dpc.SetParam("@ITEM23", SqlDbType.VarChar, 20, 1); // 0か1を判断 1は実線

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                                    Count++; // 改ページ用カウンタ
                                    #endregion

                                    #region 改ページ時処理
                                    #region インサートテーブル
                                    Sql = new StringBuilder();
                                    dpc = new DbParamCollection();
                                    Sql.Append("INSERT INTO PR_HN_TBL(");
                                    Sql.Append("  GUID");
                                    Sql.Append(" ,SORT");
                                    Sql.Append(" ,ITEM01");
                                    Sql.Append(" ,ITEM02");
                                    Sql.Append(" ,ITEM03");
                                    Sql.Append(" ,ITEM04");
                                    Sql.Append(" ,ITEM05");
                                    Sql.Append(" ,ITEM06");
                                    Sql.Append(" ,ITEM07");
                                    Sql.Append(" ,ITEM08");
                                    Sql.Append(" ,ITEM12");
                                    Sql.Append(" ,ITEM14");
                                    Sql.Append(" ,ITEM15");
                                    Sql.Append(" ,ITEM16");
                                    Sql.Append(" ,ITEM17");
                                    Sql.Append(" ,ITEM18");
                                    Sql.Append(" ,ITEM23");
                                    Sql.Append(") ");
                                    Sql.Append("VALUES(");
                                    Sql.Append("  @GUID");
                                    Sql.Append(" ,@SORT");
                                    Sql.Append(" ,@ITEM01");
                                    Sql.Append(" ,@ITEM02");
                                    Sql.Append(" ,@ITEM03");
                                    Sql.Append(" ,@ITEM04");
                                    Sql.Append(" ,@ITEM05");
                                    Sql.Append(" ,@ITEM06");
                                    Sql.Append(" ,@ITEM07");
                                    Sql.Append(" ,@ITEM08");
                                    Sql.Append(" ,@ITEM12");
                                    Sql.Append(" ,@ITEM14");
                                    Sql.Append(" ,@ITEM15");
                                    Sql.Append(" ,@ITEM16");
                                    Sql.Append(" ,@ITEM17");
                                    Sql.Append(" ,@ITEM18");
                                    Sql.Append(" ,@ITEM23");
                                    Sql.Append(") ");
                                    #endregion

                                    #region ページヘッダーデータを設定
                                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                    dbSORT++;
                                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_CD"]); // 取引先コード
                                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_NM"]); // 取引先名
                                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dtKishu.Rows[l]["DENWA_BANGO"]); // 電話番号
                                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtKishu.Rows[l]["FAX_BANGO"]); // Fax番号
                                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                                    dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                                    dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                                    #endregion

                                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "☆☆ 前頁より繰越し ☆☆"); // 改ページ時文字列
                                    dpc.SetParam("@ITEM14", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.URIAGEGAKU))); // 売上金額合計
                                    dpc.SetParam("@ITEM15", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHOHIZEIGAKU))); // 消費税合計
                                    dpc.SetParam("@ITEM16", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.NYUKINGAKU))); // 入金額合計
                                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高
                                    dpc.SetParam("@ITEM23", SqlDbType.VarChar, 20, 0); // 0か1を判断 0は実線

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                                    Count++; // 改ページ用カウンタ
                                    #endregion
                                }

                                // flagが1の時に実行
                                if (flag == 1)
                                {
                                    #region 空データの登録
                                    #region インサートテーブル
                                    Sql = new StringBuilder();
                                    dpc = new DbParamCollection();
                                    Sql.Append("INSERT INTO PR_HN_TBL(");
                                    Sql.Append("  GUID");
                                    Sql.Append(" ,SORT");
                                    Sql.Append(" ,ITEM01");
                                    Sql.Append(" ,ITEM02");
                                    Sql.Append(" ,ITEM03");
                                    Sql.Append(" ,ITEM04");
                                    Sql.Append(" ,ITEM05");
                                    Sql.Append(" ,ITEM06");
                                    Sql.Append(" ,ITEM07");
                                    Sql.Append(" ,ITEM08");
                                    Sql.Append(" ,ITEM18");
                                    Sql.Append(" ,ITEM23");
                                    Sql.Append(") ");
                                    Sql.Append("VALUES(");
                                    Sql.Append("  @GUID");
                                    Sql.Append(" ,@SORT");
                                    Sql.Append(" ,@ITEM01");
                                    Sql.Append(" ,@ITEM02");
                                    Sql.Append(" ,@ITEM03");
                                    Sql.Append(" ,@ITEM04");
                                    Sql.Append(" ,@ITEM05");
                                    Sql.Append(" ,@ITEM06");
                                    Sql.Append(" ,@ITEM07");
                                    Sql.Append(" ,@ITEM08");
                                    Sql.Append(" ,@ITEM18");
                                    Sql.Append(" ,@ITEM23");
                                    Sql.Append(") ");
                                    #endregion

                                    #region ページヘッダーデータを設定
                                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                    dbSORT++;
                                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_CD"]); // 取引先コード
                                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_NM"]); // 取引先名
                                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dtKishu.Rows[l]["DENWA_BANGO"]); // 電話番号
                                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtKishu.Rows[l]["FAX_BANGO"]); // Fax番号
                                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                                    dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                                    dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                                    if (Count % Multiple == 0)
                                    {
                                        dpc.SetParam("@ITEM23", SqlDbType.VarChar, 20, 1); // 0か1を判断 1は実線
                                    }
                                    else
                                    {
                                        dpc.SetParam("@ITEM23", SqlDbType.VarChar, 20, 0); // 0か1を判断 0は点線
                                    }
                                    #endregion

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                                    flag = 0;
                                    Count++; // 改ページ用カウンタ
                                    #endregion
                                }

                                // 改ページ用カウンタが51の倍数の時に実行
                                if (Count % Multiple == 0)
                                {
                                    #region 改ページ時処理
                                    #region インサートテーブル
                                    Sql = new StringBuilder();
                                    dpc = new DbParamCollection();
                                    Sql.Append("INSERT INTO PR_HN_TBL(");
                                    Sql.Append("  GUID");
                                    Sql.Append(" ,SORT");
                                    Sql.Append(" ,ITEM01");
                                    Sql.Append(" ,ITEM02");
                                    Sql.Append(" ,ITEM03");
                                    Sql.Append(" ,ITEM04");
                                    Sql.Append(" ,ITEM05");
                                    Sql.Append(" ,ITEM06");
                                    Sql.Append(" ,ITEM07");
                                    Sql.Append(" ,ITEM08");
                                    Sql.Append(" ,ITEM12");
                                    Sql.Append(" ,ITEM14");
                                    Sql.Append(" ,ITEM15");
                                    Sql.Append(" ,ITEM16");
                                    Sql.Append(" ,ITEM17");
                                    Sql.Append(" ,ITEM18");
                                    Sql.Append(" ,ITEM23");
                                    Sql.Append(") ");
                                    Sql.Append("VALUES(");
                                    Sql.Append("  @GUID");
                                    Sql.Append(" ,@SORT");
                                    Sql.Append(" ,@ITEM01");
                                    Sql.Append(" ,@ITEM02");
                                    Sql.Append(" ,@ITEM03");
                                    Sql.Append(" ,@ITEM04");
                                    Sql.Append(" ,@ITEM05");
                                    Sql.Append(" ,@ITEM06");
                                    Sql.Append(" ,@ITEM07");
                                    Sql.Append(" ,@ITEM08");
                                    Sql.Append(" ,@ITEM12");
                                    Sql.Append(" ,@ITEM14");
                                    Sql.Append(" ,@ITEM15");
                                    Sql.Append(" ,@ITEM16");
                                    Sql.Append(" ,@ITEM17");
                                    Sql.Append(" ,@ITEM18");
                                    Sql.Append(" ,@ITEM23");
                                    Sql.Append(") ");
                                    #endregion

                                    #region ページヘッダーデータを設定
                                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                    dbSORT++;
                                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_CD"]); // 取引先コード
                                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_NM"]); // 取引先名
                                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dtKishu.Rows[l]["DENWA_BANGO"]); // 電話番号
                                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtKishu.Rows[l]["FAX_BANGO"]); // Fax番号
                                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                                    dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                                    dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                                    #endregion

                                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "☆☆ 次頁へ繰越し ☆☆"); // 改ページ時文字列
                                    dpc.SetParam("@ITEM14", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.URIAGEGAKU))); // 売上金額合計
                                    dpc.SetParam("@ITEM15", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHOHIZEIGAKU))); // 消費税合計
                                    dpc.SetParam("@ITEM16", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.NYUKINGAKU))); // 入金額合計
                                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高
                                    dpc.SetParam("@ITEM23", SqlDbType.VarChar, 20, 1); // 0か1を判断 1は実線

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                                    Count++; // 改ページ用カウンタ
                                    #endregion

                                    #region 改ページ時処理
                                    #region インサートテーブル
                                    Sql = new StringBuilder();
                                    dpc = new DbParamCollection();
                                    Sql.Append("INSERT INTO PR_HN_TBL(");
                                    Sql.Append("  GUID");
                                    Sql.Append(" ,SORT");
                                    Sql.Append(" ,ITEM01");
                                    Sql.Append(" ,ITEM02");
                                    Sql.Append(" ,ITEM03");
                                    Sql.Append(" ,ITEM04");
                                    Sql.Append(" ,ITEM05");
                                    Sql.Append(" ,ITEM06");
                                    Sql.Append(" ,ITEM07");
                                    Sql.Append(" ,ITEM08");
                                    Sql.Append(" ,ITEM12");
                                    Sql.Append(" ,ITEM14");
                                    Sql.Append(" ,ITEM15");
                                    Sql.Append(" ,ITEM16");
                                    Sql.Append(" ,ITEM17");
                                    Sql.Append(" ,ITEM18");
                                    Sql.Append(" ,ITEM23");
                                    Sql.Append(") ");
                                    Sql.Append("VALUES(");
                                    Sql.Append("  @GUID");
                                    Sql.Append(" ,@SORT");
                                    Sql.Append(" ,@ITEM01");
                                    Sql.Append(" ,@ITEM02");
                                    Sql.Append(" ,@ITEM03");
                                    Sql.Append(" ,@ITEM04");
                                    Sql.Append(" ,@ITEM05");
                                    Sql.Append(" ,@ITEM06");
                                    Sql.Append(" ,@ITEM07");
                                    Sql.Append(" ,@ITEM08");
                                    Sql.Append(" ,@ITEM12");
                                    Sql.Append(" ,@ITEM14");
                                    Sql.Append(" ,@ITEM15");
                                    Sql.Append(" ,@ITEM16");
                                    Sql.Append(" ,@ITEM17");
                                    Sql.Append(" ,@ITEM18");
                                    Sql.Append(" ,@ITEM23");
                                    Sql.Append(") ");
                                    #endregion

                                    #region ページヘッダーデータを設定
                                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                    dbSORT++;
                                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_CD"]); // 取引先コード
                                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_NM"]); // 取引先名
                                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dtKishu.Rows[l]["DENWA_BANGO"]); // 電話番号
                                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtKishu.Rows[l]["FAX_BANGO"]); // Fax番号
                                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                                    dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                                    dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                                    #endregion

                                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "☆☆ 前頁より繰越し ☆☆"); // 改ページ時文字列
                                    dpc.SetParam("@ITEM14", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.URIAGEGAKU))); // 売上金額合計
                                    dpc.SetParam("@ITEM15", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHOHIZEIGAKU))); // 消費税合計
                                    dpc.SetParam("@ITEM16", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.NYUKINGAKU))); // 入金額合計
                                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高
                                    dpc.SetParam("@ITEM23", SqlDbType.VarChar, 20, 0); // 0か1を判断 0は点線

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                                    Count++; // 改ページ用カウンタ
                                    #endregion
                                }

                                // 最後のデータ(次回カウンタと総データ数が一致)の時に実行 // 現在の取引先コードと次回の取引先コードが違っていれば実行
                                if (dr.Length == j || tmpTorihikisakiCd != tmpNextTorihikisakiCd)
                                {
                                    #region 取引先総合計テーブルの登録
                                    #region インサートテーブル
                                    Sql = new StringBuilder();
                                    dpc = new DbParamCollection();
                                    Sql.Append("INSERT INTO PR_HN_TBL(");
                                    Sql.Append("  GUID");
                                    Sql.Append(" ,SORT");
                                    Sql.Append(" ,ITEM01");
                                    Sql.Append(" ,ITEM02");
                                    Sql.Append(" ,ITEM03");
                                    Sql.Append(" ,ITEM04");
                                    Sql.Append(" ,ITEM05");
                                    Sql.Append(" ,ITEM06");
                                    Sql.Append(" ,ITEM07");
                                    Sql.Append(" ,ITEM08");
                                    Sql.Append(" ,ITEM12");
                                    Sql.Append(" ,ITEM14");
                                    Sql.Append(" ,ITEM15");
                                    Sql.Append(" ,ITEM16");
                                    Sql.Append(" ,ITEM18");
                                    Sql.Append(" ,ITEM23");
                                    Sql.Append(") ");
                                    Sql.Append("VALUES(");
                                    Sql.Append("  @GUID");
                                    Sql.Append(" ,@SORT");
                                    Sql.Append(" ,@ITEM01");
                                    Sql.Append(" ,@ITEM02");
                                    Sql.Append(" ,@ITEM03");
                                    Sql.Append(" ,@ITEM04");
                                    Sql.Append(" ,@ITEM05");
                                    Sql.Append(" ,@ITEM06");
                                    Sql.Append(" ,@ITEM07");
                                    Sql.Append(" ,@ITEM08");
                                    Sql.Append(" ,@ITEM12");
                                    Sql.Append(" ,@ITEM14");
                                    Sql.Append(" ,@ITEM15");
                                    Sql.Append(" ,@ITEM16");
                                    Sql.Append(" ,@ITEM18");
                                    Sql.Append(" ,@ITEM23");
                                    Sql.Append(") ");
                                    #endregion

                                    #region ページヘッダーデータを設定
                                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                    dbSORT++;
                                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_CD"]); // 取引先コード
                                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtKishu.Rows[l]["TORIHIKISAKI_NM"]); // 取引先名
                                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dtKishu.Rows[l]["DENWA_BANGO"]); // 電話番号
                                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtKishu.Rows[l]["FAX_BANGO"]); // Fax番号
                                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpjpDateFr[5]); //開始年月日
                                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpjpDateTo[5]); // 終了年月日
                                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, hyojiDate);// 年月日
                                    dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, tmpMotochoNm); // 元帳名
                                    dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                                    dpc.SetParam("@ITEM23", SqlDbType.VarChar, 20, 1); // 0か1を判断 1は実線
                                    #endregion

                                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, "【　総 合 計　】"); // 取引先別総合計
                                    dpc.SetParam("@ITEM14", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.URIAGEGAKU))); // 売上金額合計
                                    if (TORIHIKISAKI.SHOHIZEIGAKU == 0)
                                    {
                                        dpc.SetParam("@ITEM15", SqlDbType.VarChar, 20, ""); // 消費税合計
                                    }
                                    else
                                    {
                                        dpc.SetParam("@ITEM15", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.SHOHIZEIGAKU))); // 消費税合計
                                    }
                                    if (TORIHIKISAKI.NYUKINGAKU == 0)
                                    {
                                        dpc.SetParam("@ITEM16", SqlDbType.VarChar, 20, ""); // 入金額合計
                                    }
                                    else
                                    {
                                        dpc.SetParam("@ITEM16", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.NYUKINGAKU))); // 入金額合計
                                    }
                                    
                                    

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                                    // 取引先総合計をクリア
                                    TORIHIKISAKI.Clear();
                                    Count++; // 改ページ用カウンタ
                                    #endregion
                                }

                            }

                            #region データの準備
                            // 比較用の取引先コードを取得
                            if (j == dr.Length)
                            {
                                if (l + 1 < dtKishu.Rows.Count)
                                {
                                    tmpNextTorihikisakiCd = Util.ToInt(dtKishu.Rows[l + 1]["TORIHIKISAKI_CD"]);
                                }
                            }

                            // 現在と次回の取引先コードが不一致の場合 // 比較用月データを初期化 // 改ページ用カウンタを初期化 // 残高をクリア
                            if (tmpTorihikisakiCd != tmpNextTorihikisakiCd)
                            {
                                tmpNextGetsu = null;
                                Count = 1;
                                TORIHIKISAKI.Clear();
                            }

                            i++;
                            j++;
                            k++;
                            #endregion

                            #endregion
                        }
                    }
                    i = 0;
                    j = 1;
                    k = -1;
                    l++;
                }
            }

            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
                "SORT",
                "PR_HN_TBL",
                "GUID = @GUID",
                dpc);

            bool dataFlag;
            if (tmpdtPR_HN_TBL.Rows.Count > 0)
            {

                dataFlag = true;
            }
            else
            {
                dataFlag = false;
            }

            return dataFlag;
        }
        #endregion

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// 標準仕様の伝票明細メソッド
        /// </summary>
        private bool MakeWkData03()//明細選択時
        {
            #region データ取得準備
            // 入力された情報を元にワークテーブルに更新をする
            DbParamCollection dpc = new DbParamCollection();
            // 日付範囲を西暦にして取得
            DateTime tmpDateFr = Util.ConvAdDate(this.lblJpFr.Text, this.txtJpYearFr.Text,
                    this.txtJpMonthFr.Text, this.txtJpDayFr.Text, this.Dba);
            DateTime tmpDateTo = Util.ConvAdDate(this.lblJpTo.Text, this.txtJpYearTo.Text,
                    this.txtJpMonthTo.Text, this.txtJpDayTo.Text, this.Dba);
            //今月の最後の日
            DateTime tmpLastDay = Util.ConvAdDate(this.lblJpTo.Text, this.txtJpYearTo.Text,
                    this.txtJpMonthTo.Text, Util.ToString(DateTime.DaysInMonth(Util.ToInt(this.txtJpYearTo.Text), Util.ToInt(this.txtJpMonthTo.Text))), this.Dba);

            // 日付範囲を和暦で保持
            string[] tmpjpDateFr = Util.ConvJpDate(tmpDateFr, this.Dba);
            string[] tmpjpDateTo = Util.ConvJpDate(tmpDateTo, this.Dba);

            // 日付表示形式を判断
            int dataHyojiFlag = 0; // 表示日付用変数
            // 入力開始日付が1日でない場合
            if (tmpDateFr.Day != 1)
            {
                dataHyojiFlag = 1;
            }
            // 入力終了日付が、その月の最終日でない場合
            else if (tmpDateTo != tmpLastDay)
            {
                dataHyojiFlag = 1;
            }
            // 入力開始日付と入力終了日付の年又は月が一致でない場合
            if (tmpDateFr.Year != tmpDateTo.Year || tmpDateFr.Month != tmpDateTo.Month)
            {
                dataHyojiFlag = 1;
            }

            string hyojiDate; // 表示用日付            
            if (dataHyojiFlag == 0)
            {
                hyojiDate = string.Format("{0}{1}年{2}月", tmpjpDateFr[0], tmpjpDateFr[2], tmpjpDateFr[3]) + "度";
            }
            else
            {
                hyojiDate = string.Format("{0}{1}年{2}月{3}日", tmpjpDateFr[0], tmpjpDateFr[2], tmpjpDateFr[3], tmpjpDateFr[4])
                          + "～"
                          + string.Format("{0}{1}年{2}月{3}日", tmpjpDateTo[0], tmpjpDateTo[2], tmpjpDateTo[3], tmpjpDateTo[4]);
            }

            // 船主コード設定
            String FUNANUSHI_CD_FR;
            String FUNANUSHI_CD_TO;
            if (Util.ToDecimal(txtFunanushiCdFr.Text) > 0)
            {
                FUNANUSHI_CD_FR = txtFunanushiCdFr.Text;
            }
            else
            {
                FUNANUSHI_CD_FR = "0";
            }
            if (Util.ToDecimal(txtFunanushiCdTo.Text) > 0)
            {
                FUNANUSHI_CD_TO = txtFunanushiCdTo.Text;
            }
            else
            {
                FUNANUSHI_CD_TO = "9999";
            }
            // 会計年度設定
            int kaikeiNendo;
            int kaikeiNendoTo = tmpDateTo.Year;
            if (tmpDateFr.Month <= 3)
            {
                kaikeiNendo = tmpDateFr.Year - 1;
            }
            else
            {
                kaikeiNendo = tmpDateFr.Year;
            }
            //支所コードの退避
            string shishoCd = this.txtMizuageShishoCd.Text;
            // 元帳名設定
            String tmpMotochoNm = "売 上 元 帳";
            // ページヘッダ用構造体
            PageHead pHeadData = new PageHead();
            pHeadData.DateFr = tmpjpDateFr[5];
            pHeadData.DateTo = tmpjpDateTo[5];
            pHeadData.PrintDate = hyojiDate;
            pHeadData.ReportTitle = tmpMotochoNm;
            pHeadData.KaishaNm = this.UInfo.KaishaNm;
            // 期首残退避用
            string kingaku;
            // 罫線タイプ
            int lineType;

            StringBuilder Sql = new StringBuilder();
            #endregion

            #region 各業者の期首残を取得
            dpc = new DbParamCollection();
            Sql = new StringBuilder();
            Sql.Append(" SELECT ");
            Sql.Append(" A.TORIHIKISAKI_CD,");
            Sql.Append(" A.TORIHIKISAKI_NM,");
            Sql.Append(" A.DENWA_BANGO,");
            Sql.Append(" A.FAX_BANGO,");
            Sql.Append(" ISNULL(Z.ZANDAKA, 0) AS ZANDAKA,");
            Sql.Append(" ISNULL(Z.KISHUZAN, 0) AS KISHUZAN");
            Sql.Append(" FROM TB_CM_TORIHIKISAKI AS A ");               
            Sql.Append(" INNER JOIN TB_HN_TORIHIKISAKI_JOHO AS A1");
            Sql.Append(" 	ON	A.KAISHA_CD = A1.KAISHA_CD");
            Sql.Append(" 	AND A.TORIHIKISAKI_CD = A1.TORIHIKISAKI_CD");
            Sql.Append(" 	AND A1.TORIHIKISAKI_KUBUN1 = 1  ");
            Sql.Append(" LEFT JOIN ");
            Sql.Append(" (");
            Sql.Append(" 	SELECT");
            Sql.Append(" 		C.KAISHA_CD,");
            Sql.Append(" 		C.HOJO_KAMOKU_CD AS TORIHIKISAKI_CD,");
            Sql.Append(" 		SUM(CASE WHEN C.TAISHAKU_KUBUN = D.TAISHAKU_KUBUN THEN C.ZEIKOMI_KINGAKU ELSE (C.ZEIKOMI_KINGAKU * -1) END) AS ZANDAKA,");
            Sql.Append(" 		SUM(CASE WHEN C.DENPYO_DATE < @DATE_FR THEN (CASE WHEN C.TAISHAKU_KUBUN = D.TAISHAKU_KUBUN THEN C.ZEIKOMI_KINGAKU ELSE (C.ZEIKOMI_KINGAKU * -1) END) ELSE 0 END) AS KISHUZAN ");
            Sql.Append(" 	FROM TB_ZM_SHIWAKE_MEISAI AS C ");
            Sql.Append(" 	INNER JOIN TB_HN_KISHU_ZAN_KAMOKU AS B ");
            Sql.Append(" 		ON  B.KAISHA_CD = C.KAISHA_CD  ");
            Sql.Append(" 		AND B.SHISHO_CD = C.SHISHO_CD  ");
            Sql.Append(" 		AND B.KANJO_KAMOKU_CD = C.KANJO_KAMOKU_CD  ");
            Sql.Append(" 		AND 11 = B.MOTOCHO_KUBUN ");
            Sql.Append(" 		AND C.DENPYO_DATE < @DATE_TO ");
            Sql.Append(" 		AND C.KAIKEI_NENDO = @KAIKEI_NENDO ");
            Sql.Append(" 	LEFT OUTER JOIN TB_ZM_KANJO_KAMOKU AS D ");
            Sql.Append(" 		ON C.KAISHA_CD = D.KAISHA_CD  ");
            Sql.Append(" 		AND C.KAIKEI_NENDO = D.KAIKEI_NENDO");
            Sql.Append(" 		AND C.KANJO_KAMOKU_CD = D.KANJO_KAMOKU_CD  ");
            Sql.Append(" 	WHERE   ");
            Sql.Append(" 		C.KAISHA_CD = @KAISHA_CD AND  ");
            Sql.Append(" 		C.SHISHO_CD = @SHISHO_CD AND  ");
            Sql.Append(" 		D.KAIKEI_NENDO = @KAIKEI_NENDO AND  ");
            Sql.Append(" 		C.HOJO_KAMOKU_CD BETWEEN @FUNANUSHI_CD_FR AND @FUNANUSHI_CD_TO ");
            Sql.Append(" 	GROUP BY");
            Sql.Append(" 	C.KAISHA_CD,");
            Sql.Append(" 	C.SHISHO_CD,");
            Sql.Append(" 	C.HOJO_KAMOKU_CD");
            Sql.Append(" ) Z");
            Sql.Append(" ON	A.KAISHA_CD = Z.KAISHA_CD");
            Sql.Append(" AND A.TORIHIKISAKI_CD = Z.TORIHIKISAKI_CD");
            Sql.Append(" ORDER BY  ");
            Sql.Append(" A.TORIHIKISAKI_CD ASC");

            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.VarChar, 4, kaikeiNendo);
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@DATE_FR", SqlDbType.VarChar, 10, tmpDateFr.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@DATE_TO", SqlDbType.VarChar, 10, tmpDateTo.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@FUNANUSHI_CD_FR", SqlDbType.VarChar, 6, FUNANUSHI_CD_FR);
            dpc.SetParam("@FUNANUSHI_CD_TO", SqlDbType.VarChar, 6, FUNANUSHI_CD_TO);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);

            DataTable dtKishu = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
            #endregion

            #region メインデータを取得
            // 日付範囲から発生している取引先データを取得
            dpc = new DbParamCollection();
            Sql = new StringBuilder();
            Sql.Append(" SELECT ");
            Sql.Append(" A.TOKUISAKI_CD AS TORIHIKISAKI_CD,");
            Sql.Append(" A.DENPYO_DATE AS DENPYO_DATE,");
            Sql.Append(" A.DENPYO_BANGO AS DENPYO_BANGO,");
            Sql.Append(" A.GYO_BANGO AS GYO_BANGO,");
            Sql.Append(" NULL AS TEKIYO,");
            //Sql.Append(" NULL AS ZEIKOMI_KINGAKU,");
            Sql.Append(" CASE WHEN A.TORIHIKI_KUBUN1 = 2 THEN (A.URIAGE_KINGAKU + A.SHOHIZEIGAKU) * A.ENZAN_HOHO ELSE 0 END AS ZEIKOMI_KINGAKU,");
            Sql.Append(" A.TORIHIKI_KUBUN1 AS TORIHIKI_KUBUN,");
            Sql.Append(" ((A.TORIHIKI_KUBUN1 * 10) + A.TORIHIKI_KUBUN2) AS TORIHIKI_KUBUN1_AND_2,");
            Sql.Append(" A.TORIHIKI_KUBUN_NM AS TORIHIKI_KUBUN_NM,");
            Sql.Append(" A.SHOHIN_CD AS SHOHIN_CD,");
            Sql.Append(" A.SHOHIN_NM AS SHOHIN_NM,");
            Sql.Append(" B.KIKAKU AS KIKAKU,");
            Sql.Append(" A.IRISU AS IRISU,");
            Sql.Append(" (A.SURYO * A.ENZAN_HOHO) AS SURYO,");
            Sql.Append(" A.TANKA AS TANKA,");
            Sql.Append(" (A.URIAGE_KINGAKU * A.ENZAN_HOHO) AS URIAGE_KINGAKU,");
            Sql.Append(" (A.SHOHIZEIGAKU * A.ENZAN_HOHO) AS SHOHIZEI,");
            Sql.Append(" A.ZEI_RITSU AS ZEI_RITSU,");
            Sql.Append(" A.SHOHIZEI_NYURYOKU_HOHO AS SHOHIZEI_NYURYOKU_HOHO,");
            Sql.Append(" 3 AS RANK ");
            Sql.Append(" FROM VI_HN_TORIHIKI_MOTOCHO AS A ");
            Sql.Append(" LEFT OUTER JOIN TB_HN_SHOHIN AS B ");
            Sql.Append(" 	ON  A.KAISHA_CD = B.KAISHA_CD  ");
            Sql.Append(" 	AND A.SHISHO_CD = B.SHISHO_CD  ");
            Sql.Append(" 	AND A.SHOHIN_CD = B.SHOHIN_CD  ");
            Sql.Append(" WHERE ");
            Sql.Append(" 	A.KAISHA_CD = @KAISHA_CD AND ");
            Sql.Append(" 	A.SHISHO_CD = @SHISHO_CD AND ");
            Sql.Append(" 	A.DENPYO_KUBUN = 1 AND ");
            Sql.Append(" 	A.DENPYO_DATE BETWEEN @DATE_FR AND @DATE_TO AND ");
            Sql.Append(" 	A.TOKUISAKI_CD BETWEEN @FUNANUSHI_CD_FR AND @FUNANUSHI_CD_TO ");
            Sql.Append(" UNION ALL ");
            Sql.Append(" SELECT    ");
            Sql.Append(" 	B.HOJO_KAMOKU_CD AS TORIHIKISAKI_CD,");
            Sql.Append(" 	B.DENPYO_DATE AS DENPYO_DATE,");
            Sql.Append(" 	B.DENPYO_BANGO AS DENPYO_BANGO,");
            Sql.Append(" 	B.GYO_BANGO AS GYO_BANGO,");
            Sql.Append(" 	B.TEKIYO AS TEKIYO,");
            Sql.Append(" 	CASE WHEN C.TAISHAKU_KUBUN = B.TAISHAKU_KUBUN THEN -1 * B.ZEIKOMI_KINGAKU ELSE B.ZEIKOMI_KINGAKU END AS ZEIKOMI_KINGAKU,");
            Sql.Append(" 	9 AS TORIHIKI_KUBUN,");
            Sql.Append(" 	NULL AS TORIHIKI_KUBUN1_AND_2,");
            Sql.Append(" 	NULL AS TORIHIKI_KUBUN_NM,");
            Sql.Append(" 	NULL AS SHOHIN_CD,");
            Sql.Append(" 	NULL AS SHOHIN_NM,");
            Sql.Append(" 	NULL AS KIKAKU,");
            Sql.Append(" 	NULL AS IRISU,");
            Sql.Append(" 	NULL AS SURYO,");
            Sql.Append(" 	NULL AS TANKA,");
            Sql.Append(" 	NULL AS URIAGE_KINGAKU,");
            Sql.Append(" 	NULL AS SHOHIZEI,");
            Sql.Append(" 	NULL AS ZEI_RITSU,");
            Sql.Append(" 	NULL AS SHOHIZEI_NYURYOKU_HOHO,");
            Sql.Append(" 	CASE WHEN C.TAISHAKU_KUBUN = B.TAISHAKU_KUBUN THEN 2 ELSE 1 END RANK");
            Sql.Append(" FROM TB_HN_NYUKIN_KAMOKU AS A  ");
            Sql.Append(" LEFT OUTER JOIN TB_ZM_SHIWAKE_MEISAI AS B    ");
            Sql.Append(" 	ON 	A.KAISHA_CD = B.KAISHA_CD     ");
            Sql.Append(" 	AND A.SHISHO_CD = B.SHISHO_CD");
            Sql.Append(" 	AND 1 = B.DENPYO_KUBUN     ");
            Sql.Append(" 	AND A.KANJO_KAMOKU_CD = B.KANJO_KAMOKU_CD     ");
            Sql.Append(" LEFT OUTER JOIN TB_ZM_KANJO_KAMOKU AS C    ");
            Sql.Append(" 	ON 	B.KAISHA_CD = C.KAISHA_CD     ");
            Sql.Append(" 	AND B.KANJO_KAMOKU_CD = C.KANJO_KAMOKU_CD     ");
            Sql.Append(" 	AND B.KAIKEI_NENDO = C.KAIKEI_NENDO  ");
            Sql.Append(" LEFT OUTER JOIN TB_HN_TORIHIKISAKI_JOHO AS D ");
            Sql.Append(" 	ON	B.KAISHA_CD = D.KAISHA_CD     ");
            Sql.Append(" 	AND B.HOJO_KAMOKU_CD = D.TORIHIKISAKI_CD ");
            Sql.Append(" WHERE     ");
            Sql.Append(" 	A.KAISHA_CD = @KAISHA_CD AND     ");
            Sql.Append(" 	B.KAIKEI_NENDO = @KAIKEI_NENDO AND");
            Sql.Append(" 	A.MOTOCHO_KUBUN = 11 AND    ");
            Sql.Append(" 	B.HOJO_KAMOKU_CD BETWEEN @FUNANUSHI_CD_FR AND @FUNANUSHI_CD_TO AND     ");
            Sql.Append(" 	B.DENPYO_DATE Between @DATE_FR AND @DATE_TO AND    ");
            Sql.Append(" 	ISNULL(B.SHIWAKE_SAKUSEI_KUBUN,0) = 0 AND     ");
            Sql.Append(" 	D.TORIHIKISAKI_KUBUN1 = 1 ");
            Sql.Append(" ORDER BY ");
            Sql.Append(" 	TORIHIKISAKI_CD ASC,");
            Sql.Append(" 	DENPYO_DATE ASC,");
            Sql.Append(" 	DENPYO_BANGO ASC,");
            Sql.Append(" 	TORIHIKI_KUBUN1_AND_2 ASC,");
            Sql.Append(" 	GYO_BANGO ASC,");
            Sql.Append(" 	RANK ASC");

            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.VarChar, 4, kaikeiNendo);
            dpc.SetParam("@KAIKEI_NENDO_TO", SqlDbType.VarChar, 4, kaikeiNendoTo);
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@DATE_FR", SqlDbType.VarChar, 10, tmpDateFr.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@DATE_TO", SqlDbType.VarChar, 10, tmpDateTo.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@FUNANUSHI_CD_FR", SqlDbType.VarChar, 6, FUNANUSHI_CD_FR);
            dpc.SetParam("@FUNANUSHI_CD_TO", SqlDbType.VarChar, 6, FUNANUSHI_CD_TO);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);

            DataTable dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
            #endregion

            #region メインデータが0の場合
            if (dtMainLoop.Rows.Count == 0)
            {
                #region 該当日付範囲のデータが無い場合
                int i = 0; // ループ用カウント変数
                int flag = 0; // 表示フラグ
                List<int> tirihikisakiCd = new List<int>(); // 取引先番号格納用変数
                while (dtKishu.Rows.Count > i)
                {
                    if (Util.ToInt(dtKishu.Rows[i]["KISHUZAN"]) != 0)
                    {
                        tirihikisakiCd.Add(i);
                        flag = 1;
                    }
                    i++;
                }
                if (flag != 0)
                {
                    i = 0;
                    int dbSORT = 1;
                    while (tirihikisakiCd.Count > i)
                    {
                        //取引先データの退避
                        pHeadData.ToriCd = Util.ToString( dtKishu.Rows[tirihikisakiCd[i]]["TORIHIKISAKI_CD"]);
                        pHeadData.ToriNm = Util.ToString(dtKishu.Rows[tirihikisakiCd[i]]["TORIHIKISAKI_NM"]);
                        pHeadData.Tel = Util.ToString(dtKishu.Rows[tirihikisakiCd[i]]["DENWA_BANGO"]); // 電話番号
                        pHeadData.Fax= Util.ToString(dtKishu.Rows[tirihikisakiCd[i]]["FAX_BANGO"]); // Fax番号

                        #region 前月繰越テーブルの登録
                        kingaku = Util.ToString(dtKishu.Rows[tirihikisakiCd[i]]["KISHUZAN"]);
                        InsertHeadRow(pHeadData, dbSORT, 1, kingaku, 0);
                        dbSORT++;
                        #endregion

                        #region 空データの登録
                        InsertBlank(pHeadData, dbSORT, 0);
                        dbSORT++;
                        #endregion

                        #region 次月繰越テーブルの登録
                        kingaku = Util.ToString(dtKishu.Rows[tirihikisakiCd[i]]["KISHUZAN"]);
                        InsertFooterRow(pHeadData, dbSORT, 1, kingaku, 0);
                        dbSORT++;
                        #endregion

                        //取引先データのクリア
                        pHeadData.Clear();
                        i++;
                    }
                }
                else
                {
                    Msg.Info("該当データがありません。");
                    return false;
                }
#endregion
            }
            #endregion
            #region メインデータが0じゃない場合
            else
            {
                NumVals DENPYO = new NumVals(); // 伝票グループ
                NumVals MONTH = new NumVals(); // 月グループ
                NumVals TORIHIKISAKI = new NumVals(); // 取引先グループ
                DenHead dHead = new DenHead();

                #region メインループ準備処理
                String tmpGetsu = null;// 月
                String tmpNextGetsu = null;// 比較用月
                String tmpBeforeGetsu = null;// 比較用月
                int tmpDenpyo = 0;// 伝票
                int tmpNextDenpyo = 0;// 比較用伝票
                int tmpBeforeDenpyo = 0;// 比較用伝票
                int tmpTorihikisakiCd = 0;// 取引先コード
                int tmpNextTorihikisakiCd = 0;// 比較用取引先コード
                int tmpBeforeTorihikisakiCd = 0;// 比較用取引先コード
                int flag = 0;// 空データ表示フラグ
                int flagU01 = 0;// 売上計表示フラグ
                int flagU02 = 0;// 売上計表示フラグ
                int flagU03 = 0;// 売上計表示フラグ
                decimal tmpRoundUriage;
                // double tmpRoundUriage;

                String DenpyoDate = "";
                String DenpyoNo = "";
                String TorihikiKubunn = "";
                String Zandaka = "";

                String tmpDenwaBango = "";//電話番号変数
                String tmpFaxBango = "";//FAX番号変数
                String tmpTorihikisakiNm = null;// 取引先名

                int i = 0; // ループ用カウント変数
                //int j = 1; // 1件後のデータとの比較用カウンタ変数
                //int k = -1; // 1件前のデータとの比較用カウンタ変数
                int l = 0; // ループ用カウント変数
                //int Multiple = 36; // 指定行での改ページ用変数
                int Count = 1; // 指定行での改ページ用カウンタ変数
                //int z = 0; // 期首残・電話番号・FAX番号・取引先名の取得用カウンタ変数
                int dbSORT = 1;

                DataRow[] dr; // メインループ用データ変数
                #endregion

                while (dtKishu.Rows.Count > l)
                {
                    dr = dtMainLoop.Select("TORIHIKISAKI_CD = " + Util.ToString(dtKishu.Rows[l]["TORIHIKISAKI_CD"]));

                    //取引先データの退避
                    pHeadData.ToriCd = Util.ToString(dtKishu.Rows[i]["TORIHIKISAKI_CD"]);
                    pHeadData.ToriNm = Util.ToString(dtKishu.Rows[i]["TORIHIKISAKI_NM"]);
                    pHeadData.Tel = Util.ToString(dtKishu.Rows[i]["DENWA_BANGO"]); // 電話番号
                    pHeadData.Fax = Util.ToString(dtKishu.Rows[i]["FAX_BANGO"]); // Fax番号
                    kingaku = Util.ToString(dtKishu.Rows[i]["KISHUZAN"]);

                    // 期首残高又は先月残高がゼロでなく、該当日付範囲のデータが無い場合
                    if ((Util.ToDecimal(dtKishu.Rows[l]["KISHUZAN"]) != 0 || Util.ToDecimal(dtKishu.Rows[l]["ZANDAKA"]) > 0) && dr.Length == 0)
                    {
                        #region 前月繰越テーブルの登録
                        InsertHeadRow(pHeadData, dbSORT, 1, kingaku, 0);
                        dbSORT++;

                        //Count++; // 改ページ用カウンタ
                        #endregion

                        //空白行のインサート
                        InsertBlank(pHeadData, dbSORT, 0);
                        dbSORT++;

                        //次月繰越データのインサート
                        InsertFooterRow(pHeadData, dbSORT, 2, kingaku, 1);
                        dbSORT++;

                        Count++; // 改ページ用カウンタ
                    }
                    else if (dr.Length > 0)
                    {
                        //該当日付範囲のデータが存在する場合
                        while (dr.Length > i)
                        {
                            #region データの準備
                            // 伝票日付を和暦に変換
                            DateTime d = Util.ToDate(dr[i]["DENPYO_DATE"]);
                            string[] denpyo_date = Util.ConvJpDate(d, this.Dba);
                            string wareki_denpyo_date = denpyoDate(denpyo_date);

                            //改頁チェック
                            if (Count == 1)
                            {
                                //ヘッダーの挿入
                                InsertHeadRow(pHeadData, dbSORT, 1, kingaku, 0);
                                dbSORT++;
                                Count++;
                            }
                            else if (Count % MeiMultiple == 0)
                            {
                                // 次頁繰越の挿入
                                insertChangePage(pHeadData, dbSORT, 2, MONTH, TORIHIKISAKI);
                                dbSORT++;
                                // 次頁繰越の挿入
                                insertChangePage(pHeadData, dbSORT, 1, MONTH, TORIHIKISAKI);
                                dbSORT++;

                            }

                            //伝票ブレイクチェック
                            if (dHead.DenpyoBango != Util.ToString(dr[i]["DENPYO_BANGO"]))
                            {
                                //伝票合計行インサート
                                lineType = (Count % MeiMultiple == 0) ? 1 : 0;
                                InsertDenpyoGokei(pHeadData, dbSORT, DENPYO, TORIHIKISAKI, lineType);
                                dbSORT++;
                                Count++;

                                //空白行のインサート
                                InsertBlank(pHeadData, dbSORT, 0);
                                dHead.Clear();
                                dbSORT++;
                                Count++;
                            }
                            // 年月ブレイクチェック
                            if (dHead.DenpyoMonth != denpyo_date[3])
                            {
                                // 次月繰越データインサート

                            }
                            // 明細データインサート
                            dHead.DenpyoDate = wareki_denpyo_date;
                            dHead.DenpyoMonth = denpyo_date[3];
                            dHead.DenpyoBango = Util.ToString(dr[i]["DENPYO_BANGO"]);
                            dHead.TorihikiKbn = Util.ToInt(dr[i]["TORIHIKI_KUBUN"]);
                            dHead.TorihikiKbnNm = Util.ToString(dr[i]["TORIHIKI_KUBUN_NM"]);
                            insertDetail(pHeadData, dHead, dbSORT, dr[i], DENPYO, MONTH, TORIHIKISAKI, 0);

                            // 伝票集計
                            DENPYO.URIAGEGAKU += Util.ToDecimal( dr[i]["URIAGE_KINGAKU"]);
                            DENPYO.SHOHIZEIGAKU += Util.ToDecimal(dr[i]["SHOHIZEI"]);
                            // 月合計
                            MONTH.URIAGEGAKU += Util.ToDecimal(dr[i]["URIAGE_KINGAKU"]);
                            MONTH.SHOHIZEIGAKU += Util.ToDecimal(dr[i]["SHOHIZEI"]); 
                            MONTH.NYUKINGAKU += Util.ToDecimal(dr[i]["URIAGE_KINGAKU"]) + Util.ToDecimal(dr[i]["SHOHIZEI"]);
                            //取引先の合計
                            TORIHIKISAKI.URIAGEGAKU += Util.ToDecimal(dr[i]["URIAGE_KINGAKU"]);
                            TORIHIKISAKI.SHOHIZEIGAKU += Util.ToDecimal(dr[i]["SHOHIZEI"]);
                            TORIHIKISAKI.NYUKINGAKU += Util.ToDecimal(dr[i]["URIAGE_KINGAKU"]) + Util.ToDecimal(dr[i]["SHOHIZEI"]);
                        }

                        //取引先が変わるので次月繰越データの挿入


                        // 伝票合計のクリア
                        DENPYO.Clear();
                        // 月合計のクリア
                        MONTH.Clear();
                        // 取引先合計のクリア
                        TORIHIKISAKI.Clear();
                        Count = 1;
                        i = 0;
                        l++;
                    }
                }
            }
            #endregion

            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
                "SORT",
                "PR_HN_TBL",
                "GUID = @GUID",
                dpc);

            bool dataFlag;
            if (tmpdtPR_HN_TBL.Rows.Count > 0)
            {
                dataFlag = true;
            }
            else
            {
                dataFlag = false;
            }

            return dataFlag;
        }

        /// <summary>
        /// 伝票日付の退避
        /// </summary>
        /// <param name="denpyo_date">和暦日付の配列</param>
        /// <returns>和暦日付を返す</returns>
        private string denpyoDate(string[] denpyo_date)
        {
            if (denpyo_date[3].Length == 1)
            {
                denpyo_date[3] = " " + denpyo_date[3];
            }
            if (denpyo_date[4].Length == 1)
            {
                denpyo_date[4] = " " + denpyo_date[4];
            }
            return denpyo_date[2] + "/" + denpyo_date[3] + "/" + denpyo_date[4];
        }

        /// <summary>
        /// 空白行の挿入
        /// </summary>
        /// <param name="dbSORT"></param>
        private void InsertBlank(PageHead pHead, int dbSORT, int lineType)
        {
            StringBuilder Sql = new StringBuilder();
            DbParamCollection dpc = new DbParamCollection();
            Sql.Append("INSERT INTO PR_HN_TBL(");
            Sql.Append("  GUID");
            Sql.Append(" ,SORT");
            Sql.Append(" ,ITEM01");
            Sql.Append(" ,ITEM02");
            Sql.Append(" ,ITEM03");
            Sql.Append(" ,ITEM04");
            Sql.Append(" ,ITEM05");
            Sql.Append(" ,ITEM06");
            Sql.Append(" ,ITEM07");
            Sql.Append(" ,ITEM08");
            Sql.Append(" ,ITEM22");
            Sql.Append(" ,ITEM23");
            Sql.Append(") ");
            Sql.Append("VALUES(");
            Sql.Append("  @GUID");
            Sql.Append(" ,@SORT");
            Sql.Append(" ,@ITEM01");
            Sql.Append(" ,@ITEM02");
            Sql.Append(" ,@ITEM03");
            Sql.Append(" ,@ITEM04");
            Sql.Append(" ,@ITEM05");
            Sql.Append(" ,@ITEM06");
            Sql.Append(" ,@ITEM07");
            Sql.Append(" ,@ITEM08");
            Sql.Append(" ,@ITEM22");
            Sql.Append(" ,@ITEM23");
            Sql.Append(") ");

            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
            SetHeadParam(dpc, pHead);
            dpc.SetParam("@ITEM23", SqlDbType.VarChar, 20, lineType); // 0か1を判断 0は点線
            this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
        }

        /// <summary>
        /// ページヘッダレコードの挿入
        /// </summary>
        /// <param name="pHead">ページヘッダ構造体</param>
        /// <param name="dbSORT">シーケンス番号</param>
        /// <param name="typeNo">繰越タイプ 1:前月繰越 2:前頁繰越</param>
        /// <param name="Kingaku">表示金額</param>
        /// <param name="lineType">罫線タイプ 1:実践 0:点線</param>
        private void InsertHeadRow(PageHead pHead, int dbSORT, int typeNo, string Kingaku, int lineType)
        {
            StringBuilder Sql = new StringBuilder();
            DbParamCollection dpc = new DbParamCollection();
            Sql.Append("INSERT INTO PR_HN_TBL(");
            Sql.Append("  GUID");
            Sql.Append(" ,SORT");
            Sql.Append(" ,ITEM01");
            Sql.Append(" ,ITEM02");
            Sql.Append(" ,ITEM03");
            Sql.Append(" ,ITEM04");
            Sql.Append(" ,ITEM05");
            Sql.Append(" ,ITEM06");
            Sql.Append(" ,ITEM07");
            Sql.Append(" ,ITEM08");
            Sql.Append(" ,ITEM12");
            Sql.Append(" ,ITEM21");
            Sql.Append(" ,ITEM22");
            Sql.Append(" ,ITEM23");
            Sql.Append(") ");
            Sql.Append("VALUES(");
            Sql.Append("  @GUID");
            Sql.Append(" ,@SORT");
            Sql.Append(" ,@ITEM01");
            Sql.Append(" ,@ITEM02");
            Sql.Append(" ,@ITEM03");
            Sql.Append(" ,@ITEM04");
            Sql.Append(" ,@ITEM05");
            Sql.Append(" ,@ITEM06");
            Sql.Append(" ,@ITEM07");
            Sql.Append(" ,@ITEM08");
            Sql.Append(" ,@ITEM12");
            Sql.Append(" ,@ITEM21");
            Sql.Append(" ,@ITEM22");
            Sql.Append(" ,@ITEM23");
            Sql.Append(") ");

            #region ページヘッダーデータを設定
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
            SetHeadParam(dpc, pHead);
            #endregion

            string comment = "";
            switch (typeNo)
            {
                case 1:
                    //前月繰越
                    comment = "◇◇◇ 前月より繰越し ◇◇◇";
                    break;
                case 2:
                    //ページ繰越
                    comment = "◇◇◇ 前頁より繰越し ◇◇◇";
                    break;
                default:
                    break;

            }
            dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, comment); // 繰越時データ
            dpc.SetParam("@ITEM21", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(Kingaku))); // 期首残高
            dpc.SetParam("@ITEM23", SqlDbType.VarChar, 20, lineType); // 0か1を判断 0は点線

            this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
        }

        /// <summary>
        /// ページフッタレコードの挿入
        /// </summary>
        /// <param name="pHead">ページヘッダ構造体</param>
        /// <param name="dbSORT">シーケンス番号</param>
        /// <param name="typeNo">繰越タイプ 1:前月繰越 2:前頁繰越</param>
        /// <param name="Kingaku">表示金額</param>
        /// <param name="lineType">罫線タイプ 1:実践 0:点線</param>
        private void InsertFooterRow(PageHead pHead, int dbSORT, int typeNo, string Kingaku, int lineType)
        {
            StringBuilder Sql = new StringBuilder();
            DbParamCollection dpc = new DbParamCollection();
            Sql.Append("INSERT INTO PR_HN_TBL(");
            Sql.Append("  GUID");
            Sql.Append(" ,SORT");
            Sql.Append(" ,ITEM01");
            Sql.Append(" ,ITEM02");
            Sql.Append(" ,ITEM03");
            Sql.Append(" ,ITEM04");
            Sql.Append(" ,ITEM05");
            Sql.Append(" ,ITEM06");
            Sql.Append(" ,ITEM07");
            Sql.Append(" ,ITEM08");
            Sql.Append(" ,ITEM12");
            Sql.Append(" ,ITEM21");
            Sql.Append(" ,ITEM22");
            Sql.Append(" ,ITEM23");
            Sql.Append(") ");
            Sql.Append("VALUES(");
            Sql.Append("  @GUID");
            Sql.Append(" ,@SORT");
            Sql.Append(" ,@ITEM01");
            Sql.Append(" ,@ITEM02");
            Sql.Append(" ,@ITEM03");
            Sql.Append(" ,@ITEM04");
            Sql.Append(" ,@ITEM05");
            Sql.Append(" ,@ITEM06");
            Sql.Append(" ,@ITEM07");
            Sql.Append(" ,@ITEM08");
            Sql.Append(" ,@ITEM12");
            Sql.Append(" ,@ITEM21");
            Sql.Append(" ,@ITEM22");
            Sql.Append(" ,@ITEM23");
            Sql.Append(") ");

            #region ページヘッダーデータを設定
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
            SetHeadParam(dpc, pHead);
            #endregion

            string comment = "";
            switch (typeNo)
            {
                case 1:
                    //前月繰越
                    comment = "◇◇◇ 次月へ繰越し ◇◇◇";
                    break;
                case 2:
                    //ページ繰越
                    comment = "◇◇◇ 次頁へ繰越し ◇◇◇";
                    break;
                default:
                    break;

            }
            dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, comment); // 繰越時データ
            dpc.SetParam("@ITEM21", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(Kingaku))); // 期首残高
            dpc.SetParam("@ITEM23", SqlDbType.VarChar, 20, lineType); // 0か1を判断 0は点線

            this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
        }

        /// <summary>
        /// 改頁データのインサート
        /// </summary>
        /// <param name="pHead"></param>
        /// <param name="dbSORT"></param>
        /// <param name="typeNo"></param>
        /// <param name="MONTH"></param>
        /// <param name="TORIHIKISAKI"></param>
        private void insertChangePage(PageHead pHead, int dbSORT, int typeNo, NumVals MONTH, NumVals TORIHIKISAKI)
        {
            StringBuilder Sql = new StringBuilder();
            DbParamCollection dpc = new DbParamCollection();
            Sql.Append("INSERT INTO PR_HN_TBL(");
            Sql.Append("  GUID");
            Sql.Append(" ,SORT");
            Sql.Append(" ,ITEM01");
            Sql.Append(" ,ITEM02");
            Sql.Append(" ,ITEM03");
            Sql.Append(" ,ITEM04");
            Sql.Append(" ,ITEM05");
            Sql.Append(" ,ITEM06");
            Sql.Append(" ,ITEM07");
            Sql.Append(" ,ITEM08");
            Sql.Append(" ,ITEM12");
            Sql.Append(" ,ITEM19");
            Sql.Append(" ,ITEM20");
            Sql.Append(" ,ITEM21");
            Sql.Append(" ,ITEM22");
            Sql.Append(" ,ITEM23");
            Sql.Append(" ,ITEM24");
            Sql.Append(") ");
            Sql.Append("VALUES(");
            Sql.Append("  @GUID");
            Sql.Append(" ,@SORT");
            Sql.Append(" ,@ITEM01");
            Sql.Append(" ,@ITEM02");
            Sql.Append(" ,@ITEM03");
            Sql.Append(" ,@ITEM04");
            Sql.Append(" ,@ITEM05");
            Sql.Append(" ,@ITEM06");
            Sql.Append(" ,@ITEM07");
            Sql.Append(" ,@ITEM08");
            Sql.Append(" ,@ITEM12");
            Sql.Append(" ,@ITEM19");
            Sql.Append(" ,@ITEM20");
            Sql.Append(" ,@ITEM21");
            Sql.Append(" ,@ITEM22");
            Sql.Append(" ,@ITEM23");
            Sql.Append(" ,@ITEM24");
            Sql.Append(") ");
            #region ページヘッダーデータを設定
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
            //固定パラメータの設定
            SetHeadParam(dpc, pHead);
            #endregion

            string comment;
            switch (typeNo)
            {
                case 1:
                    comment = "☆☆ 次頁へ繰越し ☆☆";
                    dpc.SetParam("@ITEM24", SqlDbType.VarChar, 20, 1); // 0か1を判断 
                    break;
                case 2:
                    comment = "☆☆ 前頁より繰越し ☆☆";
                    dpc.SetParam("@ITEM24", SqlDbType.VarChar, 20, 0); // 0か1を判断 
                    break;
                default:
                    comment = "";
                    break;
            }
            dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, comment); // 改ページ時文字列
            dpc.SetParam("@ITEM19", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.URIAGEGAKU))); // 売上金額合計
            dpc.SetParam("@ITEM20", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.SHOHIZEIGAKU))); // 売上消費税
            dpc.SetParam("@ITEM21", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(MONTH.NYUKINGAKU))); // 入金額合計
            dpc.SetParam("@ITEM22", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高

            this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

        }

        /// <summary>
        /// 明細データのインサート
        /// </summary>
        /// <param name="pHead"></param>
        /// <param name="dHead"></param>
        /// <param name="dbSORT"></param>
        /// <param name="dr"></param>
        /// <param name="DENPYO"></param>
        /// <param name="MONTH"></param>
        /// <param name="TORIHIKISAKI"></param>
        /// <param name="lineType"></param>
        private void insertDetail(PageHead pHead, DenHead dHead, int dbSORT, DataRow dr, NumVals DENPYO, NumVals MONTH, NumVals TORIHIKISAKI, int lineType)
        {
            #region インサートテーブル
            StringBuilder Sql = new StringBuilder();
            DbParamCollection dpc = new DbParamCollection();
            Sql.Append("INSERT INTO PR_HN_TBL(");
            Sql.Append("  GUID");
            Sql.Append(" ,SORT");
            Sql.Append(" ," + Util.ColsArray(prtCols, ""));
            Sql.Append(") ");
            Sql.Append("VALUES(");
            Sql.Append("  @GUID");
            Sql.Append(" ,@SORT");
            Sql.Append(" ," + Util.ColsArray(prtCols, "@"));
            Sql.Append(") ");
            #endregion

            #region ページヘッダーデータを設定
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
            SetHeadParam(dpc, pHead);
            dpc.SetParam("@ITEM24", SqlDbType.VarChar, 20, lineType); // 0か1を判断 1は実線
            #endregion

            decimal tmpRoundUriage = Util.ToDecimal(dr["URIAGE_KINGAKU"]) + Util.ToDecimal(dr["SHOHIZEI"]);

            dpc.SetParam("@ITEM09", SqlDbType.VarChar, 20, dHead.DenpyoDate); // 伝票日付
            dpc.SetParam("@ITEM10", SqlDbType.VarChar, 20, dHead.DenpyoBango); // 伝票No

            // 取引区分でレコードを分ける
            if (dHead.TorihikiKbn != ZamToriKbn)
            {
                dpc.SetParam("@ITEM11", SqlDbType.VarChar, 20, dHead.TorihikiKbnNm); // 取引区分
                dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, ""); // 文字列データ
                dpc.SetParam("@ITEM13", SqlDbType.VarChar, 20, Util.ToString(dr["SHOHIN_CD"])); // 商品番号
                dpc.SetParam("@ITEM14", SqlDbType.VarChar, 20, Util.ToString(dr["SHOHIN_NM"])); // 商品名
                dpc.SetParam("@ITEM15", SqlDbType.VarChar, 20, Util.ToString(dr["KIKAKU"])); // 規格
                dpc.SetParam("@ITEM16", SqlDbType.VarChar, 20, Util.ToString(dr["IRISU"])); // 入数
                dpc.SetParam("@ITEM17", SqlDbType.VarChar, 20, Util.FormatNum(dr["SURYO"], 1)); // 数量
                dpc.SetParam("@ITEM18", SqlDbType.VarChar, 20, Util.FormatNum(dr["TANKA"], 1)); // 単価
                dpc.SetParam("@ITEM19", SqlDbType.VarChar, 20, Util.FormatNum(dr["URIAGE_KINGAKU"])); // 売上金額
                dpc.SetParam("@ITEM20", SqlDbType.VarChar, 20, Util.FormatNum(dr["SHOHIZEI"])); // 売上金額
                dpc.SetParam("@ITEM21", SqlDbType.VarChar, 20, ""); // 入金額
                dpc.SetParam("@ITEM22", SqlDbType.VarChar, 20, ""); // 残高
            }
            else
            {
                dpc.SetParam("@ITEM11", SqlDbType.VarChar, 20, ""); // 取引区分
                dpc.SetParam("@ITEM12", SqlDbType.VarChar, 20, ""); // 文字列データ
                dpc.SetParam("@ITEM13", SqlDbType.VarChar, 20, ""); // 商品番号
                dpc.SetParam("@ITEM14", SqlDbType.VarChar, 20, Util.ToString(dr["TEKIYO"])); // 商品名
                dpc.SetParam("@ITEM15", SqlDbType.VarChar, 20, ""); // 規格
                dpc.SetParam("@ITEM16", SqlDbType.VarChar, 20, ""); // 入数
                dpc.SetParam("@ITEM17", SqlDbType.VarChar, 20, ""); // 数量
                dpc.SetParam("@ITEM18", SqlDbType.VarChar, 20, ""); // 単価
                dpc.SetParam("@ITEM19", SqlDbType.VarChar, 20, ""); // 売上金額
                dpc.SetParam("@ITEM20", SqlDbType.VarChar, 20, ""); // 消費税
                dpc.SetParam("@ITEM21", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(dr["ZEIKOMI_KINGAKU"]))); // 入金額
                dpc.SetParam("@ITEM22", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高

                TORIHIKISAKI.ZANDAKA -= Util.ToDecimal(dr["ZEIKOMI_KINGAKU"]); // 伝票合計用残高

            }

            this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

        }

        private void InsertDenpyoGokei(PageHead pHead, int dbSORT, NumVals DENPYO, NumVals TORIHIKISAKI, int lineType)
        {
            StringBuilder Sql = new StringBuilder();
            DbParamCollection dpc = new DbParamCollection();
            Sql.Append("INSERT INTO PR_HN_TBL(");
            Sql.Append("  GUID");
            Sql.Append(" ,SORT");
            Sql.Append(" ,ITEM01");
            Sql.Append(" ,ITEM02");
            Sql.Append(" ,ITEM03");
            Sql.Append(" ,ITEM04");
            Sql.Append(" ,ITEM05");
            Sql.Append(" ,ITEM06");
            Sql.Append(" ,ITEM07");
            Sql.Append(" ,ITEM08");
            Sql.Append(" ,ITEM18");
            Sql.Append(" ,ITEM19");
            Sql.Append(" ,ITEM20");
            Sql.Append(" ,ITEM23");
            Sql.Append(" ,ITEM24");
            Sql.Append(") ");
            Sql.Append("VALUES(");
            Sql.Append("  @GUID");
            Sql.Append(" ,@SORT");
            Sql.Append(" ,@ITEM01");
            Sql.Append(" ,@ITEM02");
            Sql.Append(" ,@ITEM03");
            Sql.Append(" ,@ITEM04");
            Sql.Append(" ,@ITEM05");
            Sql.Append(" ,@ITEM06");
            Sql.Append(" ,@ITEM07");
            Sql.Append(" ,@ITEM08");
            Sql.Append(" ,@ITEM18");
            Sql.Append(" ,@ITEM19");
            Sql.Append(" ,@ITEM20");
            Sql.Append(" ,@ITEM23");
            Sql.Append(") ");
            #endregion

            #region ページヘッダーデータを設定
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
            SetHeadParam(dpc, pHead);
            #endregion

            dpc.SetParam("@ITEM18", SqlDbType.VarChar, 20, "【伝票合計】"); // 伝票合計
            dpc.SetParam("@ITEM19", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(DENPYO.URIAGEGAKU))); // 売上金額合計
            dpc.SetParam("@ITEM20", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(DENPYO))); // 売上金額合計
            dpc.SetParam("@ITEM23", SqlDbType.VarChar, 20, Util.FormatNum(Util.ToDecimal(TORIHIKISAKI.ZANDAKA))); // 残高
            dpc.SetParam("@ITEM24", SqlDbType.VarChar, 20, lineType); // 0か1を判断 1は実線

            this.Dba.ModifyBySql(Util.ToString(Sql), dpc);


        }

        private void SetHeadParam(DbParamCollection dpc, PageHead pHead)
        {
            dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, pHead.ToriCd);  // 取引先コード
            dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, pHead.ToriNm); // 取引先名
            dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, pHead.Tel); // 電話番号
            dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, pHead.Fax); // Fax番号
            dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, pHead.DateFr); //開始年月日
            dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, pHead.DateTo); // 終了年月日
            dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, pHead.PrintDate);// 年月日
            dpc.SetParam("@ITEM08", SqlDbType.VarChar, 20, pHead.ReportTitle); // 元帳名
            dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, pHead.KaishaNm); // 会社名
        }



    }
}
