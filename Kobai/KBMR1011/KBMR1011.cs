﻿using System;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Reflection;
using System.Windows.Forms;

using GrapeCity.ActiveReports;

using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.kb.kbmr1011
{
    #region 棚卸設定構造体
    // 棚卸設定構造体
    public struct Settei
    {
        public int kbn;
        public decimal tanka;
        public decimal gokei;
        public int sort;
        public void Clear()
        {
            kbn = 0;
            tanka = 0;
            gokei = 0;
            sort = 0;
        }
    }
    #endregion

    /// <summary>
    /// 購買商品受払月報(KBMR1011)
    /// </summary>
    public partial class KBMR1011 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// 印刷ワーク更新用列数
        /// </summary>
        private const int prtCols = 28;
        #endregion

        /// <summary>
        /// 棚卸設定退避用変数
        /// </summary>
        public Settei gSettei;

        #region プロパティ
        /// <summary>
        /// 画面上最後となるフォーカスのEnterボタン押下時処理用変数
        /// </summary>
        private bool _dtFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._dtFlg;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KBMR1011()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 水揚支所
#if DEBUG
            this.txtMizuageShishoCd.Text = "1";
#else
            this.txtMizuageShishoCd.Text = Uinfo.shishoCd;
#endif
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
            this.txtMizuageShishoCd.Enabled = (this.txtMizuageShishoCd.Text == "1") ? true : false;

            string[] jpDate = Util.ConvJpDate(DateTime.Now, this.Dba);
            lblDateGengo.Text = jpDate[0];
            txtDateYear.Text = jpDate[2];
            txtDateMonth.Text = jpDate[3];
            rdoZeinuki.Checked = true;

            this.comboBox1.SelectedIndex = 1;
            // 初期フォーカス
            txtDateYear.Focus();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 日付、商品区分に
            // フォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd":
                case "txtDateYear":
                case "txtShohinKubunFr":
                case "txtShohinKubunTo":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;
            String[] result;

            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd": // 水揚支所
                    #region 水揚支所
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM2031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2031.CMCM2031");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.InData = this.txtMizuageShishoCd.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.txtMizuageShishoCd.Text = result[0];
                                this.lblMizuageShishoNm.Text = result[1];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtShohinKubunFr":
                    #region 商品区分１
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1041.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1041.CMCM1041");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "VI_HN_SHOHIN_KBN1";
                            frm.InData = this.txtShohinKubunFr.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (string[])frm.OutData;
                                this.txtShohinKubunFr.Text = result[0];
                                this.lblShohinKubunFr.Text = result[1];

                                // 次の項目(商品区分２)にフォーカス
                                this.txtShohinKubunTo.Focus();
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtShohinKubunTo":
                    #region 商品区分１
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1041.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1041.CMCM1041");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "VI_HN_SHOHIN_KBN1";
                            frm.InData = this.txtShohinKubunTo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (string[])frm.OutData;
                                this.txtShohinKubunTo.Text = result[0];
                                this.lblShohinKubunTo.Text = result[1];

                            }
                        }
                    }
                    #endregion
                    break;

                case "txtDateYear":
                    #region 元号
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblDateGengo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (string[])frm.OutData;
                                this.lblDateGengo.Text = result[1];

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblDateGengo.Text,
                                        this.txtDateYear.Text,
                                        this.txtDateMonth.Text,
                                        "1",
                                        this.Dba);
                                this.lblDateGengo.Text = arrJpDate[0];
                                this.txtDateYear.Text = arrJpDate[2];
                                this.txtDateMonth.Text = arrJpDate[3];
                            }
                        }
                    }
                    #endregion
                    break;

            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
            {
                // プレビュー処理
                DoPrint(true);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("印刷", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }

        /// <summary>
        /// F6キー押下時処理
        /// PDF出力
        /// </summary>
        public override void PressF6()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("PDF出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, true);
            }
        }

        /// <summary>
        /// F7キー押下時処理
        /// EXCEL出力
        /// </summary>
        public override void PressF7()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("EXCEL出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, false, true);
            }
        }

        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            // 設定画面の起動
            // MEMO:原則としてここで渡す帳票IDの設定はReport.csvに保持していることが前提ですが、
            // 保持していない場合は、設定画面での保存(F6)時に新規に設定が保持されます。
            PrintSettingForm psForm = new PrintSettingForm(new string[1] { "KBMR1011R" });
            psForm.ShowDialog();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 水揚支所入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMizuageShishoCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                e.Cancel = true;
                this.txtMizuageShishoCd.SelectAll();
                this.txtMizuageShishoCd.Focus();
            }
        }

        /// <summary>
        /// 商品区分１(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShohinKubunFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShohinKubunFr())
            {
                e.Cancel = true;
                this.txtShohinKubunFr.SelectAll();
            }
        }

        /// <summary>
        /// 商品区分１(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShohinKubunTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShohinKubunTo())
            {
                e.Cancel = true;
                this.txtShohinKubunTo.SelectAll();

                // Enter処理を無効化
                this._dtFlg = false;
            }
            else
            {
                // Enter処理を有効化
                this._dtFlg = true;
            }
        }

        /// <summary>
        /// 和暦(年)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateYear_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtDateYear.Text, this.txtDateYear.MaxLength))
            {
                e.Cancel = true;
                this.txtDateYear.SelectAll();
            }
            else
            {
                this.txtDateYear.Text = Util.ToString(IsValid.SetYear(this.txtDateYear.Text));
                CheckJp();
                SetJp();
            }
        }

        /// <summary>
        /// 和暦(月)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateMonth_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtDateMonth.Text, this.txtDateMonth.MaxLength))
            {
                e.Cancel = true;

                this.txtDateMonth.SelectAll();
            }
            else
            {
                this.txtDateMonth.Text = Util.ToString(IsValid.SetMonth(this.txtDateMonth.Text));
                CheckJp();
                SetJp();
            }
        }

        /// <summary>
        /// 商品区分(至)のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShohinKubunTo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.Flg)
            {
                // Enter処理を無効化
                this._dtFlg = false;

                // 全項目を再度入力値チェック
                if (!ValidateAll())
                {
                    // エラーありの場合ここで処理終了
                    return;
                }

                if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
                {
                    // ﾌﾟﾚﾋﾞｭｰ処理
                    DoPrint(true);
                }
                else
                {
                    this.txtShohinKubunTo.Focus();
                }
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 水揚支所の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool IsValidMizuageShishoCd()
        {
            // 空 又は 0入力の場合
            if (ValChk.IsEmpty(this.txtMizuageShishoCd.Text) || Equals(this.txtMizuageShishoCd.Text, "0"))
            {
                // 水揚支所名称を表示する
                this.txtMizuageShishoCd.Text = "0";
                this.lblMizuageShishoNm.Text = "全て";
                return true;
            }
            // 水揚支所名称を表示する
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);

            if (ValChk.IsEmpty(this.lblMizuageShishoNm.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 商品区分１(自)の入力チェック
        /// </summary>
        private bool IsValidShohinKubunFr()
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtShohinKubunFr.Text))
            {
                Msg.Error("商品区分１は数値のみで入力してください。");
                return false;
            }

            // コードを元に名称を取得する
            // 取得された場合、名称をラベルに反映する
            if (this.txtShohinKubunFr.Text.Length > 0)
            {
                this.lblShohinKubunFr.Text = this.Dba.GetName(this.UInfo, "VI_HN_SHOHIN_KBN1", this.txtMizuageShishoCd.Text, this.txtShohinKubunFr.Text);
            }
            else
            {
                this.lblShohinKubunFr.Text = "先　頭";
            }

            return true;
        }

        /// <summary>
        /// 商品区分１(至)の入力チェック
        /// </summary>
        private bool IsValidShohinKubunTo()
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtShohinKubunTo.Text))
            {
                Msg.Error("商品区分１は数値のみで入力してください。");
                return false;
            }

            // コードを元に名称を取得する
            // 取得された場合、名称をラベルに反映する
            if (this.txtShohinKubunTo.Text.Length > 0)
            {
                this.lblShohinKubunTo.Text = this.Dba.GetName(this.UInfo, "VI_HN_SHOHIN_KBN1", this.txtMizuageShishoCd.Text, this.txtShohinKubunTo.Text);
            }
            else
            {
                this.lblShohinKubunTo.Text = "最　後";
            }

            return true;
        }

        /// <summary>
        /// 年月日(自)の月末入力チェック
        /// </summary>
        /// 
        private void CheckJp()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblDateGengo.Text, this.txtDateYear.Text,
                this.txtDateMonth.Text, "1", this.Dba);
        }

        /// <summary>
        /// 年月日(自)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetJp()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJp(Util.FixJpDate(this.lblDateGengo.Text, this.txtDateYear.Text,
                this.txtDateMonth.Text, "1", this.Dba));
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 水揚支所の入力チェック
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                this.txtMizuageShishoCd.Focus();
                this.txtMizuageShishoCd.SelectAll();
                return false;
            }
            // 年(自)のチェック
            if (!IsValid.IsYear(this.txtDateYear.Text, this.txtDateYear.MaxLength))
            {
                this.txtDateYear.Focus();
                this.txtDateYear.SelectAll();
                return false;
            }
            // 月(自)のチェック
            if (!IsValid.IsMonth(this.txtDateMonth.Text, this.txtDateMonth.MaxLength))
            {
                this.txtDateMonth.Focus();
                this.txtDateMonth.SelectAll();
                return false;
            }
            CheckJp();
            SetJp();

            // 商品区分１(自)の入力チェック
            if (!IsValidShohinKubunFr())
            {
                this.txtShohinKubunFr.Focus();
                this.txtShohinKubunFr.SelectAll();
                return false;
            }
            // 商品区分１(至)の入力チェック
            if (!IsValidShohinKubunTo())
            {
                this.txtShohinKubunTo.Focus();
                this.txtShohinKubunTo.SelectAll();
                return false;
            }

            return true;
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJp(string[] arrJpDate)
        {
            this.lblDateGengo.Text = arrJpDate[0];
            this.txtDateYear.Text = arrJpDate[2];
            this.txtDateMonth.Text = arrJpDate[3];
        }
        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview, bool isPdf = false, bool isExcel = false, bool isCsv = false)
        {
            try
            {
#if DEBUG
                //印刷用ワークテーブルの削除
                this.Dba.DeleteWork("PR_HN_TBL", this.UnqId);
#endif

                bool dataFlag;
                
                this.Dba.BeginTransaction();

                // 日付を西暦にして取得
                DateTime tmpDateFr = Util.ConvAdDate(this.lblDateGengo.Text, this.txtDateYear.Text,
                        this.txtDateMonth.Text, "1", this.Dba);

                // 前回棚卸日を取得
                DateTime zenkaiTnorsDt = new DateTime();
                DbParamCollection dpc = new DbParamCollection();
                StringBuilder sql = new StringBuilder();
                sql.Append(" SELECT");
                sql.Append("     MAX(TANAOROSHI_DATE) AS 最終棚卸日付");
                sql.Append(" FROM");
                sql.Append("     TB_HN_TANAOROSHI_BACKUP2 AS A ");
                sql.Append("     LEFT OUTER JOIN TB_HN_SHOHIN AS B ");
                sql.Append("     ON A.KAISHA_CD = B.KAISHA_CD ");
                sql.Append("     AND A.SHISHO_CD = B.SHISHO_CD ");
                sql.Append("     AND A.SHOHIN_CD = B.SHOHIN_CD ");
                sql.Append(" WHERE");
                sql.Append("     A.KAISHA_CD = @KAISHA_CD AND ");
                if (this.txtMizuageShishoCd.Text != "0")
                    sql.Append("     A.SHISHO_CD = @SHISHO_CD AND ");
                sql.Append("     A.TANAOROSHI_DATE < @TANAOROSHI_DATE AND ");
                sql.Append("     B.SHOHIN_KUBUN5 <> 1");

                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.txtMizuageShishoCd.Text);
                dpc.SetParam("@TANAOROSHI_DATE", SqlDbType.DateTime, 10, tmpDateFr);

                DataTable dtTnorsBkup = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);

                if (!ValChk.IsEmpty(dtTnorsBkup.Rows[0]["最終棚卸日付"]))
                {
                    zenkaiTnorsDt = Util.ToDate(dtTnorsBkup.Rows[0]["最終棚卸日付"]);
                }
                else
                {
                    // 棚卸が未実行の場合はシステムが稼働した期の期首からを累計とする
                    Msg.Error("棚卸ﾃﾞｰﾀがありません！");

                    return;
                }

                // 帳票出力用にワークテーブルにデータを作成
                dataFlag = MakeWkData(zenkaiTnorsDt);

                // 帳票出力
                if (dataFlag)
                {
                    // 取得列の定義
                    StringBuilder cols = Util.ColsArray(prtCols, "");

                    // バインドパラメータの設定
                    dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);

                    // データの取得
                    DataTable dtOutput = this.Dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_HN_TBL", "GUID = @GUID", "ITEM23 ASC , ITEM26 ASC", dpc);

                    // 帳票オブジェクトをインスタンス化
                    KBMR1011R rpt = new KBMR1011R(dtOutput);
                    if (isExcel)
                    {
                        GrapeCity.ActiveReports.Export.Excel.Section.XlsExport xlsExport1 = new GrapeCity.ActiveReports.Export.Excel.Section.XlsExport();
                        //SetExcelSetting(xlsExport1);
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Kob, rpt.Document.Name, 2);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            xlsExport1.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("EXCEL出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPdf)
                    {
                        GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport p = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Kob, rpt.Document.Name, 1);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            p.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("PDF出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPreview)
                    {
                        // プレビュー画面表示
                        PreviewForm pFrm = new PreviewForm(rpt, this.UnqId);
                        pFrm.WindowState = FormWindowState.Maximized;
                        pFrm.Show();
                    }
                    else
                    {
                        // 直接印刷
                        rpt.Run(false);
                        rpt.Document.Print(true, true, false);
                    }
                }
                else
                {
                    Msg.Error("データがありません。");
                }
            }
            finally
            {
#if DEBUG
                this.Dba.Commit();
#else
                this.Dba.Rollback();
#endif
            }
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        /// <param name="zenkaiTnorsDt">前回棚卸日</param>
        private bool MakeWkData(DateTime zenkaiTnorsDt)
        {
            DbParamCollection dpc = new DbParamCollection();
            DataRow drShohin;
            // 日付を西暦にして取得
            DateTime tmpDateFr = Util.ConvAdDate(this.lblDateGengo.Text, this.txtDateYear.Text,
                    this.txtDateMonth.Text, "1", this.Dba);

            // 月の最終日を取得
            int year = int.Parse(tmpDateFr.Date.ToString("yyyy"));
            int month = int.Parse(tmpDateFr.Date.ToString("MM"));
            string mlast = DateTime.DaysInMonth(year, month).ToString();
            DateTime tmpDateTo = Util.ConvAdDate(this.lblDateGengo.Text, this.txtDateYear.Text,
                    this.txtDateMonth.Text, mlast, this.Dba);

            // 日付を和暦で保持
            string[] tmpjpDate = Util.ConvJpDate(tmpDateFr, this.Dba);
            int i; // ループ用カウント変数

            // 出力日付設定
            string[] nowDate = Util.ConvJpDate(DateTime.Now, this.Dba);
            string outputDate = nowDate[5];

            // 商品区分設定
            string shohinKbnFr;
            string shohinKbnTo;
            if (Util.ToDecimal(txtShohinKubunFr.Text) > 0)
            {
                shohinKbnFr = txtShohinKubunFr.Text;
            }
            else
            {
                shohinKbnFr = "0";
            }
            if (Util.ToDecimal(txtShohinKubunTo.Text) > 0)
            {
                shohinKbnTo = txtShohinKubunTo.Text;
            }
            else
            {
                shohinKbnTo = "9999";
            }
            // 税抜,税込設定
            string zeiKbn;
            if (rdoZeinuki.Checked)
            {
                //zeiKbn = "税抜き";
                zeiKbn = this.rdoZeinuki.Text;
            }
            else
            {
                //zeiKbn = "税込み";
                zeiKbn = this.rdoZeikomi.Text;
            }
            string shishoCd = this.txtMizuageShishoCd.Text;
            StringBuilder sql;
           

            // Zam.TB_会社情報(TB_ZM_KAISHA_JOHO)
            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.VarChar, 6, this.UInfo.KaikeiNendo);
            DataTable dtZM_KAISHA_JOHO = this.Dba.GetDataTableByConditionWithParams(
                "*",
                "TB_ZM_KAISHA_JOHO",
                "KAISHA_CD = @KAISHA_CD AND KAIKEI_NENDO = @KAIKEI_NENDO",
                "KAISHA_CD,KESSANKI",
                dpc);
            if (dtZM_KAISHA_JOHO.Rows.Count == 0)
            {
                return false;
            }

            i = 1;
            //中止区分
            int Chushikubun = this.comboBox1.SelectedIndex;
            #region データインサート,アップデート処理
            // 商品情報取得
            dpc = new DbParamCollection();
            sql = new StringBuilder();
            sql.Append(" SELECT");
            sql.Append("     A.KAISHA_CD,");
            sql.Append("     A.SHOHIN_CD,");
            sql.Append("     A.SHOHIN_NM,");
            sql.Append("     A.SHIIRE_TANKA");
            sql.Append(" FROM");
            sql.Append("     TB_HN_SHOHIN AS A");
            sql.Append(" WHERE");
            sql.Append("     A.KAISHA_CD = @KAISHA_CD AND");
            if (shishoCd != "0")
                sql.Append("     A.SHISHO_CD = @SHISHO_CD AND");
            sql.Append("     A.SHOHIN_KUBUN5 <> 1 AND");
            switch (Chushikubun)
            {
                case 0:
                    // 全ての商品が対象
                    break;
                case 1:
                    // 中止区分1：取引有りの商品が対象
                    sql.Append("     ISNULL(A.CHUSHI_KUBUN, 0) = 1 AND");
                    break;
                case 2:
                    // 中止区分２：取引中止の商品が対象
                    sql.Append("     ISNULL(A.CHUSHI_KUBUN, 0) = 2 AND");
                    break;
            }
            sql.Append("     A.ZAIKO_KANRI_KUBUN = 1");
            sql.Append(" ORDER BY");
            sql.Append("     A.SHOHIN_CD");

            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);

            DataTable dt_SHOHIN = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);

            foreach (DataRow dr in dt_SHOHIN.Rows)
            {
                #region インサートテーブル
                sql = new StringBuilder();
                dpc = new DbParamCollection();
                sql.Append("INSERT INTO PR_HN_TBL(");
                sql.Append("  GUID");
                sql.Append(" ,SORT");
                sql.Append(" ,ITEM01");
                sql.Append(" ,ITEM02");
                sql.Append(" ,ITEM03");
                sql.Append(" ,ITEM04");
                sql.Append(" ,ITEM05");
                sql.Append(" ,ITEM06");
                sql.Append(" ,ITEM07");
                sql.Append(" ,ITEM08");
                sql.Append(" ,ITEM09");
                sql.Append(" ,ITEM10");
                sql.Append(" ,ITEM11");
                sql.Append(" ,ITEM12");
                sql.Append(" ,ITEM13");
                sql.Append(" ,ITEM14");
                sql.Append(" ,ITEM15");
                sql.Append(" ,ITEM16");
                sql.Append(" ,ITEM17");
                sql.Append(" ,ITEM18");
                sql.Append(" ,ITEM19");
                sql.Append(" ,ITEM20");
                sql.Append(" ,ITEM21");
                sql.Append(" ,ITEM22");
                sql.Append(" ,ITEM23");
                sql.Append(" ,ITEM24");
                sql.Append(" ,ITEM25");
                sql.Append(" ,ITEM26");
                sql.Append(" ,ITEM27");
                sql.Append(" ,ITEM28");
                sql.Append(") ");
                sql.Append("VALUES(");
                sql.Append("  @GUID");
                sql.Append(" ,@SORT");
                sql.Append(" ,@ITEM01");
                sql.Append(" ,@ITEM02");
                sql.Append(" ,@ITEM03");
                sql.Append(" ,@ITEM04");
                sql.Append(" ,@ITEM05");
                sql.Append(" ,@ITEM06");
                sql.Append(" ,@ITEM07");
                sql.Append(" ,@ITEM08");
                sql.Append(" ,@ITEM09");
                sql.Append(" ,@ITEM10");
                sql.Append(" ,@ITEM11");
                sql.Append(" ,@ITEM12");
                sql.Append(" ,@ITEM13");
                sql.Append(" ,@ITEM14");
                sql.Append(" ,@ITEM15");
                sql.Append(" ,@ITEM16");
                sql.Append(" ,@ITEM17");
                sql.Append(" ,@ITEM18");
                sql.Append(" ,@ITEM19");
                sql.Append(" ,@ITEM20");
                sql.Append(" ,@ITEM21");
                sql.Append(" ,@ITEM22");
                sql.Append(" ,@ITEM23");
                sql.Append(" ,@ITEM24");
                sql.Append(" ,@ITEM25");
                sql.Append(" ,@ITEM26");
                sql.Append(" ,@ITEM27");
                sql.Append(" ,@ITEM28");
                sql.Append(") ");
                #endregion

                #region データ登録
                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                dpc.SetParam("@SORT", SqlDbType.Int, i);
                // ページヘッダーデータを設定
                dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, string.Format("{0}{1}年{2}月分", tmpjpDate[0], tmpjpDate[2], tmpjpDate[3]));
                dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, zeiKbn);

                //データを設定
                dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dr["SHOHIN_CD"]);
                dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dr["SHOHIN_NM"]);
                dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, "0.00");
                dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, "0");
                dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, "0.00");
                dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, "0");
                dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, "0.00");
                dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, "0");
                dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, "0.00");
                dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, "0");
                dpc.SetParam("@ITEM13", SqlDbType.VarChar, 200, "0.00");
                dpc.SetParam("@ITEM14", SqlDbType.VarChar, 200, "0");
                dpc.SetParam("@ITEM15", SqlDbType.VarChar, 200, "0.00");
                dpc.SetParam("@ITEM16", SqlDbType.VarChar, 200, "0");
                dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, "0.00");
                dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, "0");
                dpc.SetParam("@ITEM19", SqlDbType.VarChar, 200, "0.00");
                dpc.SetParam("@ITEM20", SqlDbType.VarChar, 200, "0");
                dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, "0");
                dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, "0");
                dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, "0");
                dpc.SetParam("@ITEM24", SqlDbType.VarChar, 200, "0");
                dpc.SetParam("@ITEM25", SqlDbType.VarChar, 200, dr["KAISHA_CD"]);
                dpc.SetParam("@ITEM26", SqlDbType.VarChar, 200, "0");
                dpc.SetParam("@ITEM27", SqlDbType.VarChar, 200, "0");
                dpc.SetParam("@ITEM28", SqlDbType.VarChar, 200, outputDate);

                this.Dba.ModifyBySql(Util.ToString(sql), dpc);
                i++;
                #endregion
            }

            #region 累計数量、累計金額の更新(数量×単価)
            dpc = new DbParamCollection();
            sql = new StringBuilder();
            sql.Append(" SELECT");
            sql.Append("     A.KAISHA_CD AS KAISHA_CD,");
            sql.Append("     A.TANAOROSHI_DATE AS TANAOROSHI_DATE,");
            sql.Append("     A.SHOHIN_CD AS SHOHIN_CD,");
            sql.Append("     CASE WHEN B.IRISU <> 0 THEN ((B.IRISU * A.SURYO1) + SURYO2) ELSE SURYO2 END AS SURYO2,");
            sql.Append("     A.SHIIRE_TANKA AS SHIIRE_TANKA,");
            sql.Append("     B.IRISU     AS IRISU,");
            sql.Append("     C.JITSU_SURYO1 AS SURYO1,");
            sql.Append("     C.JITSU_SURYO2 AS SURYO");
            sql.Append(" FROM");
            sql.Append("     TB_HN_TANAOROSHI_BACKUP2 AS A");
            sql.Append(" LEFT OUTER JOIN TB_HN_SHOHIN AS B");
            sql.Append(" ON  A.KAISHA_CD = B.KAISHA_CD");
            sql.Append(" AND A.SHISHO_CD = B.SHISHO_CD");
            sql.Append(" AND A.SHOHIN_CD = B.SHOHIN_CD");
            sql.Append(" AND B.ZAIKO_KANRI_KUBUN = 1");
            sql.Append(" LEFT OUTER JOIN TB_HN_TANAOROSHI_BACKUP1 AS C");
            sql.Append(" ON  A.KAISHA_CD = C.KAISHA_CD");
            sql.Append(" AND A.SHISHO_CD = C.SHISHO_CD");
            sql.Append(" AND A.SHOHIN_CD = C.SHOHIN_CD");
            sql.Append(" AND A.TANAOROSHI_DATE = C.TANAOROSHI_DATE");
            sql.Append(" WHERE");
            sql.Append("     A.KAISHA_CD = @KAISHA_CD");
            if (shishoCd != "0")
                sql.Append(" AND A.SHISHO_CD = @SHISHO_CD");
            sql.Append(" AND A.TANAOROSHI_DATE = @TANAOROSHI_DATE");
            sql.Append(" AND B.SHOHIN_KUBUN5 <> 1");
            sql.Append(" ORDER BY");
            sql.Append("     A.SHOHIN_CD");

            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            dpc.SetParam("@TANAOROSHI_DATE", SqlDbType.DateTime, 10, zenkaiTnorsDt);

            DataTable dtR_KURIKOSHI = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);
            #endregion

            decimal suryo = 0;
            decimal tanka = 0;
            decimal kin = 0;
            foreach (DataRow dr in dtR_KURIKOSHI.Rows)
            {
                sql = new StringBuilder();
                dpc = new DbParamCollection();
                sql.Append("UPDATE PR_HN_TBL SET");
                sql.Append("  ITEM13 = @ITEM13");
                sql.Append(" ,ITEM14 = @ITEM14");
                sql.Append(" WHERE");
                sql.Append("  GUID = @GUID");
                sql.Append(" AND");
                sql.Append("  ITEM03 = @ITEM03");

                suryo = Util.Round(Util.ToDecimal(dr["SURYO"]), 2);
                kin = Util.Round(Util.ToDecimal(dr["SURYO"]) * Util.ToDecimal(dr["SHIIRE_TANKA"]), 0);

                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dr["SHOHIN_CD"]);

                dpc.SetParam("@ITEM13", SqlDbType.VarChar, 200, Util.FormatNum(suryo, 2));
                dpc.SetParam("@ITEM14", SqlDbType.VarChar, 200, Util.FormatNum(kin));

                this.Dba.ModifyBySql(Util.ToString(sql), dpc);
            }

            // 累計払出数量、累計払出金額、累計受入数量、累計受入金額の更新(前回棚卸日の翌日から条件月の末)
            // 取得した結果をそのまま更新
            dpc = new DbParamCollection();
            sql = new StringBuilder();
            sql.Append(" SELECT");
            sql.Append("     B.SHOHIN_CD AS SHOHIN_CD,");
            sql.Append("     SUM(CASE WHEN A.DENPYO_KUBUN = 1 THEN (CASE WHEN A.TORIHIKI_KUBUN2 = 2 THEN (((B.SURYO1 * B.IRISU) + B.SURYO2) * -1) ELSE ((B.SURYO1 * B.IRISU) + B.SURYO2) END) ELSE 0 END) AS URIAGE_SURYO,");
            sql.Append("     SUM(CASE WHEN A.DENPYO_KUBUN = 1 ");
            sql.Append(" 			 THEN (");
            sql.Append(" 				CASE WHEN A.TORIHIKI_KUBUN2 = 2 ");
            sql.Append(" 					 THEN (");
            sql.Append(" 						CASE WHEN B.SHOHIZEI_NYURYOKU_HOHO = 3 ");
            sql.Append(" 							 THEN B.BAIKA_KINGAKU - B.SHOHIZEI");
            sql.Append(" 							 ELSE B.BAIKA_KINGAKU ");
            sql.Append(" 						END) * -1 ");
            sql.Append(" 					 ELSE (");
            sql.Append(" 						CASE WHEN B.SHOHIZEI_NYURYOKU_HOHO = 3 ");
            sql.Append(" 							 THEN B.BAIKA_KINGAKU - B.SHOHIZEI");
            sql.Append(" 							 ELSE B.BAIKA_KINGAKU ");
            sql.Append(" 						END)");
            sql.Append(" 				END) ");
            sql.Append(" 			 ELSE 0 ");
            sql.Append(" 		END) AS URIAGE_KINGAKU,");
            sql.Append("     SUM(CASE WHEN A.DENPYO_KUBUN = 1 THEN (CASE WHEN A.TORIHIKI_KUBUN2 = 2 THEN B.SHOHIZEI * -1 ELSE B.SHOHIZEI END) ELSE 0 END) AS URIAGE_SHOHIZEI,");
            sql.Append("     SUM(CASE WHEN A.DENPYO_KUBUN = 2 THEN (CASE WHEN A.TORIHIKI_KUBUN2 = 2 THEN (((B.SURYO1 * B.IRISU) + B.SURYO2) * -1) ELSE ((B.SURYO1 * B.IRISU) + B.SURYO2) END) ELSE 0 END) AS SHIIRE_SURYO,");
            sql.Append("     SUM(CASE WHEN A.DENPYO_KUBUN = 2 ");
            sql.Append(" 			 THEN (");
            sql.Append(" 				CASE WHEN A.TORIHIKI_KUBUN2 = 2 ");
            sql.Append(" 					 THEN (");
            sql.Append(" 						CASE WHEN B.SHOHIZEI_NYURYOKU_HOHO = 3 ");
            sql.Append(" 							 THEN B.BAIKA_KINGAKU - B.SHOHIZEI");
            sql.Append(" 							 ELSE B.BAIKA_KINGAKU ");
            sql.Append(" 						END) * -1 ");
            sql.Append(" 					 ELSE (");
            sql.Append(" 						CASE WHEN B.SHOHIZEI_NYURYOKU_HOHO = 3 ");
            sql.Append(" 							 THEN B.BAIKA_KINGAKU - B.SHOHIZEI");
            sql.Append(" 							 ELSE B.BAIKA_KINGAKU ");
            sql.Append(" 						END)");
            sql.Append(" 				END) ");
            sql.Append(" 			 ELSE 0 ");
            sql.Append(" 		END) AS  SHIIRE_KINGAKU,");
            sql.Append("     SUM(CASE WHEN A.DENPYO_KUBUN = 2 THEN (CASE WHEN A.TORIHIKI_KUBUN2 = 2 THEN B.SHOHIZEI * -1 ELSE B.SHOHIZEI END) ELSE 0 END) AS SHIIRE_SHOHIZEI");
            sql.Append(" FROM TB_HN_TORIHIKI_DENPYO AS A");
            sql.Append(" LEFT OUTER JOIN TB_HN_TORIHIKI_MEISAI AS B");
            sql.Append(" 	ON  A.KAISHA_CD = B.KAISHA_CD");
            sql.Append(" 	AND A.SHISHO_CD = B.SHISHO_CD");
            sql.Append(" 	AND A.KAIKEI_NENDO = B.KAIKEI_NENDO");
            sql.Append(" 	AND A.DENPYO_KUBUN = B.DENPYO_KUBUN");
            sql.Append(" 	AND A.DENPYO_BANGO = B.DENPYO_BANGO");
            sql.Append(" LEFT OUTER JOIN  TB_HN_SHOHIN AS C");
            sql.Append(" 	ON  B.KAISHA_CD = C.KAISHA_CD");
            sql.Append(" 	AND B.SHISHO_CD = C.SHISHO_CD");
            sql.Append(" 	AND B.SHOHIN_CD = C.SHOHIN_CD");
            sql.Append(" WHERE");
            sql.Append("     A.KAISHA_CD = @KAISHA_CD");
            if (shishoCd != "0")
                sql.Append(" AND A.SHISHO_CD = @SHISHO_CD");
            sql.Append(" AND A.DENPYO_DATE BETWEEN @DATE_FR AND @DATE_TO");
            sql.Append(" AND C.SHOHIN_KUBUN5 <> 1");
            sql.Append(" AND C.ZAIKO_KANRI_KUBUN = 1");
            sql.Append(" GROUP BY");
            sql.Append("     B.SHOHIN_CD");
            sql.Append(" ORDER BY");
            sql.Append("     B.SHOHIN_CD");

            // 前回棚卸日の翌日から当月末までの累計を取る
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            dpc.SetParam("@DATE_FR", SqlDbType.DateTime, zenkaiTnorsDt.AddDays(1));
            dpc.SetParam("@DATE_TO", SqlDbType.DateTime, tmpDateTo);

            DataTable dtR_UKE_HARAI = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);

            decimal sirKin = 0;
            decimal uragKin = 0;
            decimal sirSuryo = 0;
            decimal uragSuryo = 0;
            foreach (DataRow dr in dtR_UKE_HARAI.Rows)
            {
                //if (zeiKbn == "税抜き")
                if (zeiKbn == this.rdoZeinuki.Text)
                {
                    sirKin = Util.ToDecimal(dr["SHIIRE_KINGAKU"]);
                    uragKin = Util.ToDecimal(dr["URIAGE_KINGAKU"]);
                }
                else
                {
                    sirKin = Util.ToDecimal(dr["SHIIRE_KINGAKU"]) + Util.ToDecimal(dr["SHIIRE_SHOHIZEI"]);
                    uragKin = Util.ToDecimal(dr["URIAGE_KINGAKU"]) + Util.ToDecimal(dr["URIAGE_SHOHIZEI"]);
                }
                // 四捨五入処理
                uragSuryo = Util.Round(Util.ToDecimal(dr["URIAGE_SURYO"]), 2);
                sirSuryo = Util.Round(Util.ToDecimal(dr["SHIIRE_SURYO"]), 2);

                sql = new StringBuilder();
                dpc = new DbParamCollection();
                sql.Append("UPDATE PR_HN_TBL SET");
                sql.Append("  ITEM15 = @ITEM15");
                sql.Append(" ,ITEM16 = @ITEM16");
                sql.Append(" ,ITEM17 = @ITEM17");
                sql.Append(" ,ITEM18 = @ITEM18");
                sql.Append(" WHERE");
                sql.Append("  GUID = @GUID");
                sql.Append(" AND");
                sql.Append("  ITEM03 = @ITEM03");

                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dr["SHOHIN_CD"]);

                dpc.SetParam("@ITEM15", SqlDbType.VarChar, 200, Util.FormatNum(sirSuryo, 2));
                dpc.SetParam("@ITEM16", SqlDbType.VarChar, 200, Util.FormatNum(sirKin));
                dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, Util.FormatNum(uragSuryo, 2));
                dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, Util.FormatNum(uragKin));

                this.Dba.ModifyBySql(Util.ToString(sql), dpc);
            }

            // 月計数量、月計金額の更新
            // 現在の在庫数
            dpc = new DbParamCollection();
            sql = new StringBuilder();
            sql.Append(" SELECT");
            sql.Append("     A.SHOHIN_CD    AS SHOHIN_CD,");
            sql.Append("     A.SURYO        AS SURYO,");
            sql.Append("     A.SHIIRE_TANKA AS SHIIRE_TANKA,");
            sql.Append("     B.SURYO1       AS GENZAI_ZAIKO_SURYO1,");
            sql.Append("     B.SURYO2       AS GENZAI_ZAIKO_SURYO");
            sql.Append(" FROM");
            sql.Append("     VI_HN_TANAOROSHI_BACKUP AS A");
            sql.Append(" LEFT OUTER JOIN TB_HN_ZAIKO AS B");
            sql.Append(" ON A.KAISHA_CD = B.KAISHA_CD");
            sql.Append(" AND A.SHISHO_CD = B.SHISHO_CD");
            sql.Append(" AND A.SHOHIN_CD = B.SHOHIN_CD");
            sql.Append(" WHERE");
            sql.Append("     A.KAISHA_CD = @KAISHA_CD");
            if (shishoCd != "0")
                sql.Append(" AND A.SHISHO_CD = @SHISHO_CD");
            sql.Append(" ORDER BY");
            sql.Append("     A.SHOHIN_CD");

            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);

            DataTable dtZAIKO = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);

            // 月の移動数量
            dpc = new DbParamCollection();
            sql = new StringBuilder();
            sql.Append(" SELECT");
            sql.Append("     A.SHOHIN_CD      AS SHOHIN_CD,");
            sql.Append("     SUM(CASE WHEN B.DENPYO_KUBUN = 1 THEN ");
            sql.Append("                   (CASE WHEN B.TORIHIKI_KUBUN2 = 2 THEN ");
            sql.Append("                              (((A.SURYO1 * A.IRISU) + A.SURYO2) * -1) ");
            sql.Append("                         ELSE ((A.SURYO1 * A.IRISU) + A.SURYO2) ");
            sql.Append("                    END) ");
            sql.Append("              ELSE (CASE WHEN B.TORIHIKI_KUBUN2 = 2 THEN ");
            sql.Append("                              ((A.SURYO1 * A.IRISU) + A.SURYO2) ");
            sql.Append("                         ELSE (((A.SURYO1 * A.IRISU) + A.SURYO2) * -1) ");
            sql.Append("                    END) ");
            sql.Append("         END) AS IDO_SURYO");
            sql.Append(" FROM");
            sql.Append("     TB_HN_TORIHIKI_MEISAI AS A ");
            sql.Append(" LEFT OUTER JOIN TB_HN_TORIHIKI_DENPYO AS B ");
            sql.Append(" ON A.KAISHA_CD = B.KAISHA_CD ");
            sql.Append(" AND A.SHISHO_CD = B.SHISHO_CD ");
            sql.Append(" AND A.KAIKEI_NENDO = B.KAIKEI_NENDO ");
            sql.Append(" AND A.DENPYO_KUBUN = B.DENPYO_KUBUN ");
            sql.Append(" AND A.DENPYO_BANGO = B.DENPYO_BANGO ");
            sql.Append(" LEFT OUTER JOIN  TB_HN_SHOHIN AS C ");
            sql.Append(" ON  A.KAISHA_CD = C.KAISHA_CD ");
            sql.Append(" AND A.SHISHO_CD = C.SHISHO_CD ");
            sql.Append(" AND A.SHOHIN_CD = C.SHOHIN_CD ");
            sql.Append(" WHERE");
            sql.Append("     A.KAISHA_CD = @KAISHA_CD AND");
            if (shishoCd != "0")
                sql.Append("     A.SHISHO_CD = @SHISHO_CD AND");
            sql.Append("     B.DENPYO_DATE > @DATE_TO AND");
            sql.Append("     C.SHOHIN_KUBUN5 <> 1 AND");
            sql.Append("     C.ZAIKO_KANRI_KUBUN = 1");
            sql.Append(" GROUP BY");
            sql.Append("     A.SHOHIN_CD");
            sql.Append(" ORDER BY");
            sql.Append("     A.SHOHIN_CD");

            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            dpc.SetParam("@DATE_TO", SqlDbType.DateTime, tmpDateFr.AddDays(-1));

            DataTable dtM_IDO = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);

            DataRow[] aryDrZaiko;
            DataRow[] aryDrMIdo;
            foreach (DataRow dr in dt_SHOHIN.Rows)
            {
                // 現在在庫数量から当月以降の取引分を差し引き。
                // 評価金額は前回棚卸時点の単価で計算
                // 商品に該当する在庫データを取得
                aryDrZaiko = dtZAIKO.Select("SHOHIN_CD = " + Util.ToString(dr["SHOHIN_CD"]));
                // 商品に該当する移動データを取得
                aryDrMIdo = dtM_IDO.Select("SHOHIN_CD = " + Util.ToString(dr["SHOHIN_CD"]));
                // 数量の計算
                suryo = 0;
                tanka = 0;

                if (aryDrZaiko.Length > 0)
                {
                    suryo = Util.ToDecimal(aryDrZaiko[0]["GENZAI_ZAIKO_SURYO"]);
                    if (Util.ToDecimal(aryDrZaiko[0]["SHIIRE_TANKA"]) != 0)
                    {
                        tanka = Util.ToDecimal(aryDrZaiko[0]["SHIIRE_TANKA"]);
                    }
                    else
                    {
                        tanka = Util.ToDecimal(dr["SHIIRE_TANKA"]);
                    }
                }
                else
                {
                    tanka = Util.ToDecimal(dr["SHIIRE_TANKA"]);
                }

                if (aryDrMIdo.Length > 0)
                {
                    // (繰越)現在在庫数量と移動数量が無い場合は0
                    if (Util.ToDecimal(aryDrMIdo[0]["IDO_SURYO"]) != 0 || suryo != 0)
                    {                     
                        suryo += Util.ToDecimal(aryDrMIdo[0]["IDO_SURYO"]);
                    }
                }

                suryo = Util.Round(suryo, 2);
                kin = Util.Round(suryo * tanka, 0);

                sql = new StringBuilder();
                dpc = new DbParamCollection();
                sql.Append("UPDATE PR_HN_TBL SET");
                sql.Append("  ITEM05 = @ITEM05");
                sql.Append(" ,ITEM06 = @ITEM06");
                sql.Append(" WHERE");
                sql.Append("  GUID = @GUID");
                sql.Append(" AND");
                sql.Append("  ITEM03 = @ITEM03");

                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dr["SHOHIN_CD"]);

                dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, Util.FormatNum(suryo, 2));
                dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, Util.FormatNum(kin));

                this.Dba.ModifyBySql(Util.ToString(sql), dpc);
            }

            // 月計払出数量、月計払出金額、月計受入数量、月計受入金額の更新(前回棚卸日の翌日から条件月の末)
            dpc = new DbParamCollection();
            sql = new StringBuilder();
            sql.Append(" SELECT");
            sql.Append("     B.SHOHIN_CD AS SHOHIN_CD,");
            sql.Append("     SUM(CASE WHEN A.DENPYO_KUBUN = 1 THEN (CASE WHEN A.TORIHIKI_KUBUN2 = 2 THEN (((B.SURYO1 * B.IRISU) + B.SURYO2) * -1) ELSE ((B.SURYO1 * B.IRISU) + B.SURYO2) END) ELSE 0 END) AS URIAGE_SURYO,");
            sql.Append("     SUM(CASE WHEN A.DENPYO_KUBUN = 1 ");
            sql.Append(" 			 THEN (");
            sql.Append(" 				CASE WHEN A.TORIHIKI_KUBUN2 = 2 ");
            sql.Append(" 					 THEN (");
            sql.Append(" 						CASE WHEN B.SHOHIZEI_NYURYOKU_HOHO = 3 ");
            sql.Append(" 							 THEN B.BAIKA_KINGAKU - B.SHOHIZEI");
            sql.Append(" 							 ELSE B.BAIKA_KINGAKU ");
            sql.Append(" 						END) * -1 ");
            sql.Append(" 					 ELSE (");
            sql.Append(" 						CASE WHEN B.SHOHIZEI_NYURYOKU_HOHO = 3 ");
            sql.Append(" 							 THEN B.BAIKA_KINGAKU - B.SHOHIZEI");
            sql.Append(" 							 ELSE B.BAIKA_KINGAKU ");
            sql.Append(" 						END)");
            sql.Append(" 				END) ");
            sql.Append(" 			 ELSE 0 ");
            sql.Append(" 		END) AS URIAGE_KINGAKU,");
            sql.Append("     SUM(CASE WHEN A.DENPYO_KUBUN = 1 THEN (CASE WHEN A.TORIHIKI_KUBUN2 = 2 THEN B.SHOHIZEI * -1 ELSE B.SHOHIZEI END) ELSE 0 END) AS URIAGE_SHOHIZEI,");
            sql.Append("     SUM(CASE WHEN A.DENPYO_KUBUN = 2 THEN (CASE WHEN A.TORIHIKI_KUBUN2 = 2 THEN (((B.SURYO1 * B.IRISU) + B.SURYO2) * -1) ELSE ((B.SURYO1 * B.IRISU) + B.SURYO2) END) ELSE 0 END) AS SHIIRE_SURYO,");
            sql.Append("     SUM(CASE WHEN A.DENPYO_KUBUN = 2 ");
            sql.Append(" 			 THEN (");
            sql.Append(" 				CASE WHEN A.TORIHIKI_KUBUN2 = 2 ");
            sql.Append(" 					 THEN (");
            sql.Append(" 						CASE WHEN B.SHOHIZEI_NYURYOKU_HOHO = 3 ");
            sql.Append(" 							 THEN B.BAIKA_KINGAKU - B.SHOHIZEI");
            sql.Append(" 							 ELSE B.BAIKA_KINGAKU ");
            sql.Append(" 						END) * -1 ");
            sql.Append(" 					 ELSE (");
            sql.Append(" 						CASE WHEN B.SHOHIZEI_NYURYOKU_HOHO = 3 ");
            sql.Append(" 							 THEN B.BAIKA_KINGAKU - B.SHOHIZEI");
            sql.Append(" 							 ELSE B.BAIKA_KINGAKU ");
            sql.Append(" 						END)");
            sql.Append(" 				END) ");
            sql.Append(" 			 ELSE 0 ");
            sql.Append(" 		END) AS  SHIIRE_KINGAKU,");
            sql.Append("     SUM(CASE WHEN A.DENPYO_KUBUN = 2 THEN (CASE WHEN A.TORIHIKI_KUBUN2 = 2 THEN B.SHOHIZEI * -1 ELSE B.SHOHIZEI END) ELSE 0 END) AS SHIIRE_SHOHIZEI");
            sql.Append(" FROM TB_HN_TORIHIKI_DENPYO AS A");
            sql.Append(" LEFT OUTER JOIN TB_HN_TORIHIKI_MEISAI AS B");
            sql.Append(" 	ON  A.KAISHA_CD = B.KAISHA_CD");
            sql.Append(" 	AND A.SHISHO_CD = B.SHISHO_CD");
            sql.Append(" 	AND A.KAIKEI_NENDO = B.KAIKEI_NENDO");
            sql.Append(" 	AND A.DENPYO_KUBUN = B.DENPYO_KUBUN");
            sql.Append(" 	AND A.DENPYO_BANGO = B.DENPYO_BANGO");
            sql.Append(" LEFT OUTER JOIN  TB_HN_SHOHIN AS C");
            sql.Append(" 	ON  B.KAISHA_CD = C.KAISHA_CD");
            sql.Append(" 	AND B.SHISHO_CD = C.SHISHO_CD");
            sql.Append(" 	AND B.SHOHIN_CD = C.SHOHIN_CD");
            sql.Append(" WHERE");
            sql.Append("     A.KAISHA_CD = @KAISHA_CD");
            if (shishoCd != "0")
                sql.Append(" AND A.SHISHO_CD = @SHISHO_CD");
            sql.Append(" AND A.DENPYO_DATE BETWEEN @DATE_FR AND @DATE_TO");
            sql.Append(" AND C.SHOHIN_KUBUN5 <> 1");
            sql.Append(" AND C.ZAIKO_KANRI_KUBUN = 1");
            sql.Append(" GROUP BY");
            sql.Append("     B.SHOHIN_CD");
            sql.Append(" ORDER BY");
            sql.Append("     B.SHOHIN_CD");

            dpc.SetParam("@DATE_FR", SqlDbType.DateTime, tmpDateFr);
            dpc.SetParam("@DATE_TO", SqlDbType.DateTime, tmpDateTo);
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            //dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);

            DataTable dtM_TORIHIKI = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);

            foreach (DataRow dr in dtM_TORIHIKI.Rows)
            {
                // 四捨五入処理
                sirSuryo = Util.Round(Util.ToDecimal(dr["SHIIRE_SURYO"]), 2);
                uragSuryo = Util.Round(Util.ToDecimal(dr["URIAGE_SURYO"]), 2);
                //if (zeiKbn == "税抜き")
                if (zeiKbn == this.rdoZeinuki.Text)
                {
                    sirKin = Util.ToDecimal(dr["SHIIRE_KINGAKU"]);
                    uragKin = Util.ToDecimal(dr["URIAGE_KINGAKU"]);
                }
                else
                {
                    sirKin = Util.ToDecimal(dr["SHIIRE_KINGAKU"]) + Util.ToDecimal(dr["SHIIRE_SHOHIZEI"]);
                    uragKin = Util.ToDecimal(dr["URIAGE_KINGAKU"]) + Util.ToDecimal(dr["URIAGE_SHOHIZEI"]);
                }

                sql = new StringBuilder();
                dpc = new DbParamCollection();
                sql.Append("UPDATE PR_HN_TBL SET");
                sql.Append("  ITEM07 = @ITEM07");
                sql.Append(" ,ITEM08 = @ITEM08");
                sql.Append(" ,ITEM09 = @ITEM09");
                sql.Append(" ,ITEM10 = @ITEM10");
                sql.Append(" ,ITEM21 = @ITEM21");
                sql.Append(" ,ITEM22 = @ITEM22");
                sql.Append(" WHERE");
                sql.Append("  GUID = @GUID");
                sql.Append(" AND");
                sql.Append("  ITEM03 = @ITEM03");

                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dr["SHOHIN_CD"]);

                dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, Util.FormatNum(sirSuryo, 2));
                dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, Util.FormatNum(sirKin));
                dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, Util.FormatNum(uragSuryo, 2));
                dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, Util.FormatNum(uragKin));
                dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, dr["SHIIRE_SHOHIZEI"]);
                dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, dr["URIAGE_SHOHIZEI"]);

                this.Dba.ModifyBySql(Util.ToString(sql), dpc);
            }

            // 累計在庫数量、累計在庫金額、月計在庫数量、月計在庫金額の更新
            // 指定月より後の移動数量
            dpc = new DbParamCollection();
            sql = new StringBuilder();
            sql.Append(" SELECT");
            sql.Append("     A.SHOHIN_CD    AS SHOHIN_CD,");
            sql.Append("     A.SURYO        AS SURYO,");
            sql.Append("     A.SHIIRE_TANKA AS SHIIRE_TANKA,");
            sql.Append("     B.SURYO1       AS GENZAI_ZAIKO_SURYO1,");
            sql.Append("     B.SURYO2       AS GENZAI_ZAIKO_SURYO");
            sql.Append(" FROM");
            sql.Append("     VI_HN_TANAOROSHI_BACKUP AS A");
            sql.Append(" LEFT OUTER JOIN TB_HN_ZAIKO AS B");
            sql.Append(" ON  A.KAISHA_CD = B.KAISHA_CD");
            sql.Append(" AND A.SHISHO_CD = B.SHISHO_CD");
            sql.Append(" AND A.SHOHIN_CD = B.SHOHIN_CD");
            sql.Append(" WHERE");
            sql.Append("     A.KAISHA_CD = @KAISHA_CD");
            if (shishoCd != "0")
                sql.Append(" AND A.SHISHO_CD = @SHISHO_CD");
            sql.Append(" ORDER BY");
            sql.Append("     A.SHOHIN_CD");
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);

            DataTable dtMR_ZAIKO = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);

            dpc = new DbParamCollection();
            sql = new StringBuilder();
            sql.Append(" SELECT");
            sql.Append("     A.SHOHIN_CD      AS SHOHIN_CD,");
            sql.Append("     SUM(CASE WHEN B.DENPYO_KUBUN = 1 THEN ");
            sql.Append("                   (CASE WHEN B.TORIHIKI_KUBUN2 = 2 THEN ");
            sql.Append("                              (((A.SURYO1 * A.IRISU) + A.SURYO2) * -1) ");
            sql.Append("                         ELSE ((A.SURYO1 * A.IRISU) + A.SURYO2) ");
            sql.Append("                    END) ");
            sql.Append("              ELSE (CASE WHEN B.TORIHIKI_KUBUN2 = 2 THEN ");
            sql.Append("                              ((A.SURYO1 * A.IRISU) + A.SURYO2) ");
            sql.Append("                         ELSE (((A.SURYO1 * A.IRISU) + A.SURYO2) * -1) ");
            sql.Append("                    END) ");
            sql.Append("         END) AS IDO_SURYO");
            sql.Append(" FROM");
            sql.Append("     TB_HN_TORIHIKI_MEISAI AS A ");
            sql.Append(" LEFT OUTER JOIN TB_HN_TORIHIKI_DENPYO AS B ");
            sql.Append(" ON A.KAISHA_CD = B.KAISHA_CD ");
            sql.Append(" AND A.SHISHO_CD = B.SHISHO_CD ");
            sql.Append(" AND A.KAIKEI_NENDO = B.KAIKEI_NENDO ");
            sql.Append(" AND A.DENPYO_KUBUN = B.DENPYO_KUBUN ");
            sql.Append(" AND A.DENPYO_BANGO = B.DENPYO_BANGO ");
            sql.Append(" LEFT OUTER JOIN  TB_HN_SHOHIN AS C ");
            sql.Append(" ON A.KAISHA_CD = C.KAISHA_CD ");
            sql.Append(" AND A.SHISHO_CD = C.SHISHO_CD ");
            sql.Append(" AND A.SHOHIN_CD = C.SHOHIN_CD ");
            sql.Append(" WHERE");
            sql.Append("     A.KAISHA_CD = @KAISHA_CD AND");
            if (shishoCd != "0")
                sql.Append("     A.SHISHO_CD = @SHISHO_CD AND");
            sql.Append("     B.DENPYO_DATE > @DATE_TO AND");
            sql.Append("     C.SHOHIN_KUBUN5 <> 1 AND");
            sql.Append("     C.ZAIKO_KANRI_KUBUN = 1");
            sql.Append(" GROUP BY");
            sql.Append("     A.SHOHIN_CD");
            sql.Append(" ORDER BY");
            sql.Append("     A.SHOHIN_CD");

            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            dpc.SetParam("@DATE_TO", SqlDbType.DateTime, tmpDateTo);

            DataTable dtM_IDO_AFT = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);

            foreach (DataRow dr in dt_SHOHIN.Rows)
            {
                // 在庫データの1行分を変数に格納
                drShohin = dr;

                // 最終仕入単価を取得
                DataRow drSaishushiireTanka = getSaishuSiireTanka(tmpDateTo, drShohin);

                // 現在在庫数量から翌月以降の取引分を差し引き

                // 商品に該当する在庫データを取得
                aryDrZaiko = dtZAIKO.Select("SHOHIN_CD = " + Util.ToString(dr["SHOHIN_CD"]));
                // 商品に該当する移動データを取得
                aryDrMIdo = dtM_IDO_AFT.Select("SHOHIN_CD = " + Util.ToString(dr["SHOHIN_CD"]));

                // 数量の計算
                suryo = 0;
                tanka = 0;
                if (aryDrZaiko.Length > 0)
                {
                    suryo = Util.ToDecimal(aryDrZaiko[0]["GENZAI_ZAIKO_SURYO"]);
                    // 棚卸表の設定画面で最終仕入単価選択時処理
                    if (gSettei.tanka == 1)
                    {
                        // 最終仕入単価の場合
                        tanka = Util.ToDecimal(drSaishushiireTanka["SAISHU_SHIIRE_TANKA"]);
                        if (tanka == 0)
                        {
                            tanka = Util.ToDecimal(drShohin["SHIIRE_TANKA"]);
                        }
                    }
                    else
                    {
                        // 棚卸表の設定画面で商品マスタ仕入単価選択時処理
                        tanka = Util.ToDecimal(drShohin["SHIIRE_TANKA"]);
                    }

                }
                else
                {
                    tanka = Util.ToDecimal(dr["SHIIRE_TANKA"]);
                }

                if (aryDrMIdo.Length > 0)
                {
                    // (在庫)現在在庫数量と移動数量が無い場合は0
                    if (Util.ToDecimal(aryDrMIdo[0]["IDO_SURYO"]) != 0 || suryo != 0)
                    {
                        suryo += Util.ToDecimal(aryDrMIdo[0]["IDO_SURYO"]);
                    }
                }

                suryo = Util.Round(suryo, 2);
                kin = Util.Round(suryo * tanka, 0);

                sql = new StringBuilder();
                dpc = new DbParamCollection();
                sql.Append("UPDATE PR_HN_TBL SET");
                sql.Append("  ITEM11 = @ITEM11");
                sql.Append(" ,ITEM12 = @ITEM12");
                sql.Append(" ,ITEM19 = @ITEM19");
                sql.Append(" ,ITEM20 = @ITEM20");
                sql.Append(" WHERE");
                sql.Append("  GUID = @GUID");
                sql.Append(" AND");
                sql.Append("  ITEM03 = @ITEM03");

                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dr["SHOHIN_CD"]);

                dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, Util.FormatNum(suryo, 2));
                dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, Util.FormatNum(kin));
                dpc.SetParam("@ITEM19", SqlDbType.VarChar, 200, Util.FormatNum(suryo, 2));
                dpc.SetParam("@ITEM20", SqlDbType.VarChar, 200, Util.FormatNum(kin));

                this.Dba.ModifyBySql(Util.ToString(sql), dpc);
            }
            #endregion

            // 商品区分番号,商品区分名
            dpc = new DbParamCollection();
            sql = new StringBuilder();
            sql.Append(" SELECT");
            sql.Append("     A.ITEM03,");
            //sql.Append("     B.BARCODE1,");
            sql.Append("     B.SHOHIN_KUBUN1,");
            sql.Append("     B.SHOHIN_KUBUN_NM1,");
            sql.Append("     B.SHOHIN_KUBUN2,");
            sql.Append("     B.SHOHIN_KUBUN_NM2");
            sql.Append(" FROM");
            sql.Append("     PR_HN_TBL AS A,");
            sql.Append("     VI_HN_SHOHIN AS B");
            sql.Append(" WHERE");
            sql.Append("     A.GUID = @GUID AND");
            sql.Append("     A.ITEM25 = B.KAISHA_CD AND");
            sql.Append("     A.ITEM03 = B.SHOHIN_CD AND");
            sql.Append("     A.ITEM04 = B.SHOHIN_NM ");
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);

            if (dtMainLoop.Rows.Count == 0)
            {
                Msg.Info("該当データがありません。");
                return false;
            }
            else
            {
                dpc = new DbParamCollection();

                #region 印刷ワークテーブルに登録
                foreach (DataRow dr in dtMainLoop.Rows)
                {
                    sql = new StringBuilder();
                    dpc = new DbParamCollection();
                    sql.Append("UPDATE PR_HN_TBL SET");
                    sql.Append("  ITEM23 = @ITEM23");
                    sql.Append(" ,ITEM24 = @ITEM24");
                    sql.Append(" ,ITEM26 = @ITEM26");
                    sql.Append(" ,ITEM27 = @ITEM27");
                    sql.Append(" WHERE");
                    sql.Append("  GUID = @GUID");
                    sql.Append(" AND");
                    sql.Append("  ITEM03 = @ITEM03");

                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dr["ITEM03"]);
                    dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, dr["SHOHIN_KUBUN1"]);
                    dpc.SetParam("@ITEM24", SqlDbType.VarChar, 200, dr["SHOHIN_KUBUN_NM1"]);
                    dpc.SetParam("@ITEM26", SqlDbType.VarChar, 200, Util.ToString(dr["SHOHIN_KUBUN2"]).PadLeft(2, Convert.ToChar("0")));
                    dpc.SetParam("@ITEM27", SqlDbType.VarChar, 200, dr["SHOHIN_KUBUN_NM2"]);

                    this.Dba.ModifyBySql(Util.ToString(sql), dpc);
                }

                #endregion
            }

            // 指定された商品区分以外を削除
            dpc = new DbParamCollection();
            sql = new StringBuilder();
            sql.Append(" DELETE");
            sql.Append(" FROM");
            sql.Append("     PR_HN_TBL");
            sql.Append(" WHERE");
            sql.Append("     GUID = @GUID AND");
            sql.Append("     (ITEM23 < @SHOHIN_KUBUN_FR OR");
            sql.Append("     ITEM23 > @SHOHIN_KUBUN_TO)");
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            dpc.SetParam("@SHOHIN_KUBUN_FR", SqlDbType.VarChar, 10, shohinKbnFr);
            dpc.SetParam("@SHOHIN_KUBUN_TO", SqlDbType.VarChar, 10, shohinKbnTo);
            this.Dba.ModifyBySql(Util.ToString(sql), dpc);

            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
                "SORT",
                "PR_HN_TBL",
                "GUID = @GUID",
                dpc);

            bool dataFlag;
            if (tmpdtPR_HN_TBL.Rows.Count > 0)
            {
                dataFlag = true;
            }
            else
            {
                dataFlag = false;
            }

            return dataFlag;
        }

        private void GetSettei()
        {
            // 使用する商品区分取得
            gSettei.kbn = Util.ToInt(this.Config.LoadPgConfig(Constants.SubSys.Kob, "TANAOROSHI", "Setting", "ShohinKbn"));
            // 在庫金額計算用単価
            gSettei.tanka = Util.ToInt(this.Config.LoadPgConfig(Constants.SubSys.Kob, "TANAOROSHI", "Setting", "UseTanka"));
            // 棚卸表・記入票の合計印字設定
            gSettei.gokei = Util.ToInt(this.Config.LoadPgConfig(Constants.SubSys.Kob, "TANAOROSHI", "Setting", "PrintGokei"));
            // 棚卸入力ソート順
            gSettei.sort = Util.ToInt(this.Config.LoadPgConfig(Constants.SubSys.Kob, "TANAOROSHI", "Setting", "Sort"));
        }
        private DataRow getSaishuSiireTanka(DateTime tanaoroshiDate, DataRow drShohin)
        {
            #region 最終仕入単価選択時 取得用SQL
            // の検索日付に発生しているデータを取得
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder Sql = new StringBuilder();
            Sql.Append(" EXEC SP_SAISHU_SHIIRE_TANKA @KAISHA_CD, @SHISHO_CD, @SHOHIN_CD, @TANAOROSHI_DATE");

            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.txtMizuageShishoCd.Text);
            // 検索する棚卸日付をセット
            dpc.SetParam("@TANAOROSHI_DATE", SqlDbType.VarChar, 10, tanaoroshiDate);
            // 検索する商品コードをセット
            dpc.SetParam("@SHOHIN_CD", SqlDbType.VarChar, 6, drShohin["SHOHIN_CD"]);
            DataTable dtSaishushiireTanka = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            // 最終仕入単価を取得
            DataRow drSaishushiireTanka;
            if (dtSaishushiireTanka.Rows.Count != 0)
            {
                drSaishushiireTanka = dtSaishushiireTanka.Rows[0];
            }
            else
            {
                drSaishushiireTanka = drShohin;
            }
            #endregion
            return drSaishushiireTanka;
        }

        #endregion

    }
}
