﻿namespace jp.co.fsi.kb.kbmr1011
{
    partial class KBMR1011
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbxDate = new System.Windows.Forms.GroupBox();
            this.lblDateMonth = new System.Windows.Forms.Label();
            this.lblDateYear = new System.Windows.Forms.Label();
            this.txtDateYear = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtDateMonth = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblDateGengo = new System.Windows.Forms.Label();
            this.lblDate = new System.Windows.Forms.Label();
            this.gbxShohinKubun = new System.Windows.Forms.GroupBox();
            this.lblShohinKubunBet = new System.Windows.Forms.Label();
            this.lblShohinKubunFr = new System.Windows.Forms.Label();
            this.lblShohinKubunTo = new System.Windows.Forms.Label();
            this.txtShohinKubunTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtShohinKubunFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.gbxZei = new System.Windows.Forms.GroupBox();
            this.rdoZeikomi = new System.Windows.Forms.RadioButton();
            this.rdoZeinuki = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMizuageShishoNm = new System.Windows.Forms.Label();
            this.lblMizuageShisho = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.pnlDebug.SuspendLayout();
            this.gbxDate.SuspendLayout();
            this.gbxShohinKubun.SuspendLayout();
            this.gbxZei.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Text = "";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(5, 531);
            this.pnlDebug.Size = new System.Drawing.Size(843, 100);
            // 
            // gbxDate
            // 
            this.gbxDate.Controls.Add(this.lblDateMonth);
            this.gbxDate.Controls.Add(this.lblDateYear);
            this.gbxDate.Controls.Add(this.txtDateYear);
            this.gbxDate.Controls.Add(this.txtDateMonth);
            this.gbxDate.Controls.Add(this.lblDateGengo);
            this.gbxDate.Controls.Add(this.lblDate);
            this.gbxDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxDate.ForeColor = System.Drawing.SystemColors.ControlText;
            this.gbxDate.Location = new System.Drawing.Point(18, 125);
            this.gbxDate.Name = "gbxDate";
            this.gbxDate.Size = new System.Drawing.Size(213, 67);
            this.gbxDate.TabIndex = 1;
            this.gbxDate.TabStop = false;
            this.gbxDate.Text = "出力月";
            // 
            // lblDateMonth
            // 
            this.lblDateMonth.BackColor = System.Drawing.Color.Silver;
            this.lblDateMonth.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateMonth.Location = new System.Drawing.Point(164, 26);
            this.lblDateMonth.Name = "lblDateMonth";
            this.lblDateMonth.Size = new System.Drawing.Size(15, 19);
            this.lblDateMonth.TabIndex = 5;
            this.lblDateMonth.Text = "月";
            this.lblDateMonth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateYear
            // 
            this.lblDateYear.BackColor = System.Drawing.Color.Silver;
            this.lblDateYear.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateYear.Location = new System.Drawing.Point(111, 24);
            this.lblDateYear.Name = "lblDateYear";
            this.lblDateYear.Size = new System.Drawing.Size(17, 21);
            this.lblDateYear.TabIndex = 3;
            this.lblDateYear.Text = "年";
            this.lblDateYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDateYear
            // 
            this.txtDateYear.AutoSizeFromLength = false;
            this.txtDateYear.DisplayLength = null;
            this.txtDateYear.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateYear.Location = new System.Drawing.Point(79, 25);
            this.txtDateYear.MaxLength = 2;
            this.txtDateYear.Name = "txtDateYear";
            this.txtDateYear.Size = new System.Drawing.Size(30, 20);
            this.txtDateYear.TabIndex = 2;
            this.txtDateYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateYear.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateYear_Validating);
            // 
            // txtDateMonth
            // 
            this.txtDateMonth.AutoSizeFromLength = false;
            this.txtDateMonth.DisplayLength = null;
            this.txtDateMonth.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateMonth.Location = new System.Drawing.Point(131, 25);
            this.txtDateMonth.MaxLength = 2;
            this.txtDateMonth.Name = "txtDateMonth";
            this.txtDateMonth.Size = new System.Drawing.Size(30, 20);
            this.txtDateMonth.TabIndex = 4;
            this.txtDateMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateMonth.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateMonth_Validating);
            // 
            // lblDateGengo
            // 
            this.lblDateGengo.BackColor = System.Drawing.Color.Silver;
            this.lblDateGengo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDateGengo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateGengo.Location = new System.Drawing.Point(36, 25);
            this.lblDateGengo.Name = "lblDateGengo";
            this.lblDateGengo.Size = new System.Drawing.Size(38, 21);
            this.lblDateGengo.TabIndex = 1;
            this.lblDateGengo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDate
            // 
            this.lblDate.BackColor = System.Drawing.Color.Silver;
            this.lblDate.Location = new System.Drawing.Point(32, 21);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(153, 29);
            this.lblDate.TabIndex = 1;
            // 
            // gbxShohinKubun
            // 
            this.gbxShohinKubun.Controls.Add(this.lblShohinKubunBet);
            this.gbxShohinKubun.Controls.Add(this.lblShohinKubunFr);
            this.gbxShohinKubun.Controls.Add(this.lblShohinKubunTo);
            this.gbxShohinKubun.Controls.Add(this.txtShohinKubunTo);
            this.gbxShohinKubun.Controls.Add(this.txtShohinKubunFr);
            this.gbxShohinKubun.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxShohinKubun.ForeColor = System.Drawing.SystemColors.ControlText;
            this.gbxShohinKubun.Location = new System.Drawing.Point(18, 198);
            this.gbxShohinKubun.Name = "gbxShohinKubun";
            this.gbxShohinKubun.Size = new System.Drawing.Size(490, 72);
            this.gbxShohinKubun.TabIndex = 3;
            this.gbxShohinKubun.TabStop = false;
            this.gbxShohinKubun.Text = "商品区分１の検索";
            // 
            // lblShohinKubunBet
            // 
            this.lblShohinKubunBet.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShohinKubunBet.Location = new System.Drawing.Point(240, 29);
            this.lblShohinKubunBet.Name = "lblShohinKubunBet";
            this.lblShohinKubunBet.Size = new System.Drawing.Size(17, 20);
            this.lblShohinKubunBet.TabIndex = 2;
            this.lblShohinKubunBet.Text = "～";
            this.lblShohinKubunBet.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblShohinKubunFr
            // 
            this.lblShohinKubunFr.BackColor = System.Drawing.Color.Silver;
            this.lblShohinKubunFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShohinKubunFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShohinKubunFr.Location = new System.Drawing.Point(81, 29);
            this.lblShohinKubunFr.Name = "lblShohinKubunFr";
            this.lblShohinKubunFr.Size = new System.Drawing.Size(153, 20);
            this.lblShohinKubunFr.TabIndex = 1;
            this.lblShohinKubunFr.Text = "先　頭";
            this.lblShohinKubunFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShohinKubunTo
            // 
            this.lblShohinKubunTo.BackColor = System.Drawing.Color.Silver;
            this.lblShohinKubunTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShohinKubunTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShohinKubunTo.Location = new System.Drawing.Point(319, 29);
            this.lblShohinKubunTo.Name = "lblShohinKubunTo";
            this.lblShohinKubunTo.Size = new System.Drawing.Size(153, 20);
            this.lblShohinKubunTo.TabIndex = 4;
            this.lblShohinKubunTo.Text = "最　後";
            this.lblShohinKubunTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShohinKubunTo
            // 
            this.txtShohinKubunTo.AutoSizeFromLength = false;
            this.txtShohinKubunTo.DisplayLength = null;
            this.txtShohinKubunTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShohinKubunTo.Location = new System.Drawing.Point(263, 29);
            this.txtShohinKubunTo.MaxLength = 4;
            this.txtShohinKubunTo.Name = "txtShohinKubunTo";
            this.txtShohinKubunTo.Size = new System.Drawing.Size(50, 20);
            this.txtShohinKubunTo.TabIndex = 3;
            this.txtShohinKubunTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShohinKubunTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtShohinKubunTo_KeyDown);
            this.txtShohinKubunTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohinKubunTo_Validating);
            // 
            // txtShohinKubunFr
            // 
            this.txtShohinKubunFr.AutoSizeFromLength = false;
            this.txtShohinKubunFr.DisplayLength = null;
            this.txtShohinKubunFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShohinKubunFr.Location = new System.Drawing.Point(25, 29);
            this.txtShohinKubunFr.MaxLength = 4;
            this.txtShohinKubunFr.Name = "txtShohinKubunFr";
            this.txtShohinKubunFr.Size = new System.Drawing.Size(50, 20);
            this.txtShohinKubunFr.TabIndex = 0;
            this.txtShohinKubunFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShohinKubunFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohinKubunFr_Validating);
            // 
            // gbxZei
            // 
            this.gbxZei.Controls.Add(this.rdoZeikomi);
            this.gbxZei.Controls.Add(this.rdoZeinuki);
            this.gbxZei.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxZei.ForeColor = System.Drawing.SystemColors.ControlText;
            this.gbxZei.Location = new System.Drawing.Point(237, 125);
            this.gbxZei.Name = "gbxZei";
            this.gbxZei.Size = new System.Drawing.Size(186, 67);
            this.gbxZei.TabIndex = 2;
            this.gbxZei.TabStop = false;
            // 
            // rdoZeikomi
            // 
            this.rdoZeikomi.AutoSize = true;
            this.rdoZeikomi.BackColor = System.Drawing.Color.Transparent;
            this.rdoZeikomi.Location = new System.Drawing.Point(101, 27);
            this.rdoZeikomi.Name = "rdoZeikomi";
            this.rdoZeikomi.Size = new System.Drawing.Size(53, 17);
            this.rdoZeikomi.TabIndex = 1;
            this.rdoZeikomi.TabStop = true;
            this.rdoZeikomi.Text = "税込";
            this.rdoZeikomi.UseVisualStyleBackColor = false;
            // 
            // rdoZeinuki
            // 
            this.rdoZeinuki.AutoSize = true;
            this.rdoZeinuki.BackColor = System.Drawing.Color.Transparent;
            this.rdoZeinuki.Location = new System.Drawing.Point(18, 27);
            this.rdoZeinuki.Name = "rdoZeinuki";
            this.rdoZeinuki.Size = new System.Drawing.Size(53, 17);
            this.rdoZeinuki.TabIndex = 0;
            this.rdoZeinuki.TabStop = true;
            this.rdoZeinuki.Text = "税抜";
            this.rdoZeinuki.UseVisualStyleBackColor = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtMizuageShishoCd);
            this.groupBox1.Controls.Add(this.lblMizuageShishoNm);
            this.groupBox1.Controls.Add(this.lblMizuageShisho);
            this.groupBox1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBox1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox1.Location = new System.Drawing.Point(18, 53);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(348, 66);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "支所";
            // 
            // txtMizuageShishoCd
            // 
            this.txtMizuageShishoCd.AutoSizeFromLength = true;
            this.txtMizuageShishoCd.DisplayLength = null;
            this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtMizuageShishoCd.Location = new System.Drawing.Point(71, 25);
            this.txtMizuageShishoCd.MaxLength = 4;
            this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
            this.txtMizuageShishoCd.Size = new System.Drawing.Size(34, 20);
            this.txtMizuageShishoCd.TabIndex = 908;
            this.txtMizuageShishoCd.TabStop = false;
            this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
            // 
            // lblMizuageShishoNm
            // 
            this.lblMizuageShishoNm.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMizuageShishoNm.Location = new System.Drawing.Point(107, 25);
            this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
            this.lblMizuageShishoNm.Size = new System.Drawing.Size(212, 20);
            this.lblMizuageShishoNm.TabIndex = 910;
            this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMizuageShisho
            // 
            this.lblMizuageShisho.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShisho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShisho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblMizuageShisho.Location = new System.Drawing.Point(29, 23);
            this.lblMizuageShisho.Name = "lblMizuageShisho";
            this.lblMizuageShisho.Size = new System.Drawing.Size(295, 25);
            this.lblMizuageShisho.TabIndex = 909;
            this.lblMizuageShisho.Text = "支所";
            this.lblMizuageShisho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.comboBox1);
            this.groupBox2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBox2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox2.Location = new System.Drawing.Point(17, 285);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(270, 62);
            this.groupBox2.TabIndex = 902;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "中止区分";
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "0: 全ての商品",
            "1: 取引有りの商品のみ",
            "2: 取引中止の商品のみ"});
            this.comboBox1.Location = new System.Drawing.Point(26, 25);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(222, 21);
            this.comboBox1.TabIndex = 0;
            // 
            // KBMR1011
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(835, 634);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.gbxDate);
            this.Controls.Add(this.gbxShohinKubun);
            this.Controls.Add(this.gbxZei);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "KBMR1011";
            this.Text = "";
            this.Controls.SetChildIndex(this.gbxZei, 0);
            this.Controls.SetChildIndex(this.gbxShohinKubun, 0);
            this.Controls.SetChildIndex(this.gbxDate, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.Controls.SetChildIndex(this.groupBox2, 0);
            this.pnlDebug.ResumeLayout(false);
            this.gbxDate.ResumeLayout(false);
            this.gbxDate.PerformLayout();
            this.gbxShohinKubun.ResumeLayout(false);
            this.gbxShohinKubun.PerformLayout();
            this.gbxZei.ResumeLayout(false);
            this.gbxZei.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbxDate;
        private jp.co.fsi.common.controls.FsiTextBox txtDateYear;
        private System.Windows.Forms.Label lblDateGengo;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.Label lblDateMonth;
        private System.Windows.Forms.Label lblDateYear;
        private jp.co.fsi.common.controls.FsiTextBox txtDateMonth;
        private System.Windows.Forms.GroupBox gbxShohinKubun;
        private System.Windows.Forms.Label lblShohinKubunFr;
        private jp.co.fsi.common.controls.FsiTextBox txtShohinKubunFr;
        private System.Windows.Forms.Label lblShohinKubunBet;
        private System.Windows.Forms.Label lblShohinKubunTo;
        private jp.co.fsi.common.controls.FsiTextBox txtShohinKubunTo;
        private System.Windows.Forms.GroupBox gbxZei;
        private System.Windows.Forms.RadioButton rdoZeikomi;
        private System.Windows.Forms.RadioButton rdoZeinuki;
        private System.Windows.Forms.GroupBox groupBox1;
        private common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private System.Windows.Forms.Label lblMizuageShisho;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox comboBox1;
    }
}