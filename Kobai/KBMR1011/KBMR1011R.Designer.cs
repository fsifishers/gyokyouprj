﻿namespace jp.co.fsi.kb.kbmr1011
{
    /// <summary>
    /// KBMR1011R の概要の説明です。
    /// </summary>
    partial class KBMR1011R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(KBMR1011R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtToday = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDateTo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblJojun = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTyujun = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblGejun = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblSekisu01 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSuryo01 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblKingaku01 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSekisu02 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSuryo02 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblKingaku02 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSekisu03 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSuryo03 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblKingaku03 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSekisu04 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSuryo04 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.textBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblTitle01 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtGyohoCd = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGyohoNm = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtGyoshuCd = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGyoshuNm = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox21 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox58 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.reportHeader1 = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.reportFooter1 = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtTotal01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotal07 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotal04 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotal09 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotal02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotal08 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotal05 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotal10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotal03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotal06 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.groupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.groupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox22 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox23 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox24 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox25 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox26 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox27 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox28 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox29 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox30 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox31 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox32 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox43 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox44 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.groupHeader2 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.textBox33 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox34 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.groupFooter2 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox36 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox37 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox40 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox42 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox46 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox48 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox50 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox51 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox53 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox54 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.textBox55 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox56 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox60 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox61 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox62 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox63 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox64 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox65 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox66 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox67 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox68 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtToday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblJojun)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTyujun)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGejun)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSekisu01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSuryo01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKingaku01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSekisu02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSuryo02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKingaku02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSekisu03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSuryo03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKingaku03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSekisu04)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSuryo04)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyohoCd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyohoNm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyoshuCd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyoshuNm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox58)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal07)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal04)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal09)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal08)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal05)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal06)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox54)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox55)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox56)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox60)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox61)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox62)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox63)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox64)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox65)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox66)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox67)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox68)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblDate,
            this.txtToday,
            this.txtPage,
            this.lblPage,
            this.txtDateTo,
            this.lblJojun,
            this.lblTyujun,
            this.lblGejun,
            this.line1,
            this.line2,
            this.line3,
            this.line4,
            this.line5,
            this.line6,
            this.lblSekisu01,
            this.lblSuryo01,
            this.lblKingaku01,
            this.lblSekisu02,
            this.lblSuryo02,
            this.lblKingaku02,
            this.lblSekisu03,
            this.lblSuryo03,
            this.lblKingaku03,
            this.lblSekisu04,
            this.lblSuryo04,
            this.label2,
            this.line7,
            this.line8,
            this.line10,
            this.textBox1,
            this.lblTitle01});
            this.pageHeader.Height = 0.9714404F;
            this.pageHeader.Name = "pageHeader";
            this.pageHeader.Format += new System.EventHandler(this.pageHeader_Format);
            // 
            // lblDate
            // 
            this.lblDate.Height = 0.2F;
            this.lblDate.HyperLink = null;
            this.lblDate.Left = 7.927953F;
            this.lblDate.Name = "lblDate";
            this.lblDate.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; ddo-char-set: 1";
            this.lblDate.Text = "作 成 日";
            this.lblDate.Top = 0.1212599F;
            this.lblDate.Width = 0.6299214F;
            // 
            // txtToday
            // 
            this.txtToday.DataField = "ITEM28";
            this.txtToday.Height = 0.2F;
            this.txtToday.Left = 8.636615F;
            this.txtToday.MultiLine = false;
            this.txtToday.Name = "txtToday";
            this.txtToday.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; ddo-char-set: 1";
            this.txtToday.Text = "gy年MM月dd日";
            this.txtToday.Top = 0.1212599F;
            this.txtToday.Width = 1.181103F;
            // 
            // txtPage
            // 
            this.txtPage.Height = 0.2F;
            this.txtPage.Left = 9.842914F;
            this.txtPage.Name = "txtPage";
            this.txtPage.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtPage.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtPage.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.txtPage.Text = "Page";
            this.txtPage.Top = 0.1212599F;
            this.txtPage.Width = 0.3748035F;
            // 
            // lblPage
            // 
            this.lblPage.Height = 0.2F;
            this.lblPage.HyperLink = null;
            this.lblPage.Left = 10.21772F;
            this.lblPage.Name = "lblPage";
            this.lblPage.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; ddo-char-set: 1";
            this.lblPage.Text = "頁";
            this.lblPage.Top = 0.1212599F;
            this.lblPage.Width = 0.1666665F;
            // 
            // txtDateTo
            // 
            this.txtDateTo.DataField = "ITEM01";
            this.txtDateTo.Height = 0.2F;
            this.txtDateTo.Left = 9.064568F;
            this.txtDateTo.MultiLine = false;
            this.txtDateTo.Name = "txtDateTo";
            this.txtDateTo.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-style: normal; font-weight: normal; t" +
    "ext-align: right; ddo-char-set: 128";
            this.txtDateTo.Text = null;
            this.txtDateTo.Top = 0.3574803F;
            this.txtDateTo.Width = 1.337008F;
            // 
            // lblJojun
            // 
            this.lblJojun.Height = 0.1452756F;
            this.lblJojun.HyperLink = null;
            this.lblJojun.Left = 4.038977F;
            this.lblJojun.Name = "lblJojun";
            this.lblJojun.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; d" +
    "do-char-set: 1";
            this.lblJojun.Text = "繰  越";
            this.lblJojun.Top = 0.5877953F;
            this.lblJojun.Width = 0.4897637F;
            // 
            // lblTyujun
            // 
            this.lblTyujun.Height = 0.1452756F;
            this.lblTyujun.HyperLink = null;
            this.lblTyujun.Left = 5.785433F;
            this.lblTyujun.Name = "lblTyujun";
            this.lblTyujun.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; d" +
    "do-char-set: 1";
            this.lblTyujun.Text = "受  入";
            this.lblTyujun.Top = 0.5877953F;
            this.lblTyujun.Width = 0.5023623F;
            // 
            // lblGejun
            // 
            this.lblGejun.Height = 0.1452756F;
            this.lblGejun.HyperLink = null;
            this.lblGejun.Left = 7.584252F;
            this.lblGejun.Name = "lblGejun";
            this.lblGejun.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; d" +
    "do-char-set: 1";
            this.lblGejun.Text = "払  出";
            this.lblGejun.Top = 0.5877953F;
            this.lblGejun.Width = 0.4996066F;
            // 
            // line1
            // 
            this.line1.Height = 0F;
            this.line1.Left = 3.562598F;
            this.line1.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.line1.LineWeight = 3F;
            this.line1.Name = "line1";
            this.line1.Top = 0.6736221F;
            this.line1.Width = 0.4763801F;
            this.line1.X1 = 3.562598F;
            this.line1.X2 = 4.038978F;
            this.line1.Y1 = 0.6736221F;
            this.line1.Y2 = 0.6736221F;
            // 
            // line2
            // 
            this.line2.Height = 0F;
            this.line2.Left = 4.543306F;
            this.line2.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.line2.LineWeight = 3F;
            this.line2.Name = "line2";
            this.line2.Top = 0.6736221F;
            this.line2.Width = 0.3763781F;
            this.line2.X1 = 4.543306F;
            this.line2.X2 = 4.919684F;
            this.line2.Y1 = 0.6736221F;
            this.line2.Y2 = 0.6736221F;
            // 
            // line3
            // 
            this.line3.Height = 0F;
            this.line3.Left = 5.383071F;
            this.line3.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.line3.LineWeight = 3F;
            this.line3.Name = "line3";
            this.line3.Top = 0.6736221F;
            this.line3.Width = 0.4023619F;
            this.line3.X1 = 5.383071F;
            this.line3.X2 = 5.785433F;
            this.line3.Y1 = 0.6736221F;
            this.line3.Y2 = 0.6736221F;
            // 
            // line4
            // 
            this.line4.Height = 0F;
            this.line4.Left = 6.287796F;
            this.line4.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.line4.LineWeight = 3F;
            this.line4.Name = "line4";
            this.line4.Top = 0.6736221F;
            this.line4.Width = 0.4212599F;
            this.line4.X1 = 6.287796F;
            this.line4.X2 = 6.709056F;
            this.line4.Y1 = 0.6736221F;
            this.line4.Y2 = 0.6736221F;
            // 
            // line5
            // 
            this.line5.Height = 0F;
            this.line5.Left = 7.187008F;
            this.line5.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.line5.LineWeight = 3F;
            this.line5.Name = "line5";
            this.line5.Top = 0.6736221F;
            this.line5.Width = 0.397244F;
            this.line5.X1 = 7.187008F;
            this.line5.X2 = 7.584252F;
            this.line5.Y1 = 0.6736221F;
            this.line5.Y2 = 0.6736221F;
            // 
            // line6
            // 
            this.line6.Height = 0F;
            this.line6.Left = 8.107483F;
            this.line6.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.line6.LineWeight = 3F;
            this.line6.Name = "line6";
            this.line6.Top = 0.6736221F;
            this.line6.Width = 0.4129896F;
            this.line6.X1 = 8.107483F;
            this.line6.X2 = 8.520473F;
            this.line6.Y1 = 0.6736221F;
            this.line6.Y2 = 0.6736221F;
            // 
            // lblSekisu01
            // 
            this.lblSekisu01.Height = 0.1574803F;
            this.lblSekisu01.HyperLink = null;
            this.lblSekisu01.Left = 0.07677166F;
            this.lblSekisu01.Name = "lblSekisu01";
            this.lblSekisu01.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; dd" +
    "o-char-set: 1";
            this.lblSekisu01.Text = "分類名";
            this.lblSekisu01.Top = 0.7787402F;
            this.lblSekisu01.Width = 0.4417323F;
            // 
            // lblSuryo01
            // 
            this.lblSuryo01.Height = 0.1574803F;
            this.lblSuryo01.HyperLink = null;
            this.lblSuryo01.Left = 0.6393701F;
            this.lblSuryo01.Name = "lblSuryo01";
            this.lblSuryo01.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ddo" +
    "-char-set: 1";
            this.lblSuryo01.Text = "商品CD";
            this.lblSuryo01.Top = 0.7787402F;
            this.lblSuryo01.Width = 0.4496063F;
            // 
            // lblKingaku01
            // 
            this.lblKingaku01.Height = 0.1574803F;
            this.lblKingaku01.HyperLink = null;
            this.lblKingaku01.Left = 1.11378F;
            this.lblKingaku01.Name = "lblKingaku01";
            this.lblKingaku01.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; dd" +
    "o-char-set: 1";
            this.lblKingaku01.Text = "商品名";
            this.lblKingaku01.Top = 0.7787402F;
            this.lblKingaku01.Width = 0.4374021F;
            // 
            // lblSekisu02
            // 
            this.lblSekisu02.Height = 0.1574803F;
            this.lblSekisu02.HyperLink = null;
            this.lblSekisu02.Left = 4.446851F;
            this.lblSekisu02.Name = "lblSekisu02";
            this.lblSekisu02.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; dd" +
    "o-char-set: 1";
            this.lblSekisu02.Text = "金  額";
            this.lblSekisu02.Top = 0.7787402F;
            this.lblSekisu02.Width = 0.4559054F;
            // 
            // lblSuryo02
            // 
            this.lblSuryo02.Height = 0.1574803F;
            this.lblSuryo02.HyperLink = null;
            this.lblSuryo02.Left = 3.589764F;
            this.lblSuryo02.Name = "lblSuryo02";
            this.lblSuryo02.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; dd" +
    "o-char-set: 1";
            this.lblSuryo02.Text = "数  量";
            this.lblSuryo02.Top = 0.7787402F;
            this.lblSuryo02.Width = 0.4929135F;
            // 
            // lblKingaku02
            // 
            this.lblKingaku02.Height = 0.1574803F;
            this.lblKingaku02.HyperLink = null;
            this.lblKingaku02.Left = 6.217323F;
            this.lblKingaku02.Name = "lblKingaku02";
            this.lblKingaku02.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; dd" +
    "o-char-set: 1";
            this.lblKingaku02.Text = "金  額";
            this.lblKingaku02.Top = 0.7787402F;
            this.lblKingaku02.Width = 0.4917331F;
            // 
            // lblSekisu03
            // 
            this.lblSekisu03.Height = 0.1574803F;
            this.lblSekisu03.HyperLink = null;
            this.lblSekisu03.Left = 5.383071F;
            this.lblSekisu03.Name = "lblSekisu03";
            this.lblSekisu03.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; dd" +
    "o-char-set: 1";
            this.lblSekisu03.Text = "数  量";
            this.lblSekisu03.Top = 0.7787402F;
            this.lblSekisu03.Width = 0.4751968F;
            // 
            // lblSuryo03
            // 
            this.lblSuryo03.Height = 0.1574803F;
            this.lblSuryo03.HyperLink = null;
            this.lblSuryo03.Left = 7.187008F;
            this.lblSuryo03.Name = "lblSuryo03";
            this.lblSuryo03.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; dd" +
    "o-char-set: 1";
            this.lblSuryo03.Text = "数  量";
            this.lblSuryo03.Top = 0.7787402F;
            this.lblSuryo03.Width = 0.4582677F;
            // 
            // lblKingaku03
            // 
            this.lblKingaku03.Height = 0.1574803F;
            this.lblKingaku03.HyperLink = null;
            this.lblKingaku03.Left = 8.066931F;
            this.lblKingaku03.Name = "lblKingaku03";
            this.lblKingaku03.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; dd" +
    "o-char-set: 1";
            this.lblKingaku03.Text = "金  額";
            this.lblKingaku03.Top = 0.7874016F;
            this.lblKingaku03.Width = 0.4535427F;
            // 
            // lblSekisu04
            // 
            this.lblSekisu04.Height = 0.1574804F;
            this.lblSekisu04.HyperLink = null;
            this.lblSekisu04.Left = 9.874804F;
            this.lblSekisu04.Name = "lblSekisu04";
            this.lblSekisu04.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; dd" +
    "o-char-set: 1";
            this.lblSekisu04.Text = "金  額";
            this.lblSekisu04.Top = 0.7874016F;
            this.lblSekisu04.Width = 0.4559049F;
            // 
            // lblSuryo04
            // 
            this.lblSuryo04.Height = 0.1574804F;
            this.lblSuryo04.HyperLink = null;
            this.lblSuryo04.Left = 8.972049F;
            this.lblSuryo04.Name = "lblSuryo04";
            this.lblSuryo04.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; dd" +
    "o-char-set: 1";
            this.lblSuryo04.Text = "数  量";
            this.lblSuryo04.Top = 0.7787402F;
            this.lblSuryo04.Width = 0.4846444F;
            // 
            // label2
            // 
            this.label2.Height = 0.1452756F;
            this.label2.HyperLink = null;
            this.label2.Left = 9.37756F;
            this.label2.Name = "label2";
            this.label2.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; d" +
    "do-char-set: 1";
            this.label2.Text = "在  庫";
            this.label2.Top = 0.5877953F;
            this.label2.Width = 0.4814968F;
            // 
            // line7
            // 
            this.line7.Height = 0F;
            this.line7.Left = 8.955513F;
            this.line7.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.line7.LineWeight = 3F;
            this.line7.Name = "line7";
            this.line7.Top = 0.6736221F;
            this.line7.Width = 0.4220467F;
            this.line7.X1 = 8.955513F;
            this.line7.X2 = 9.37756F;
            this.line7.Y1 = 0.6736221F;
            this.line7.Y2 = 0.6736221F;
            // 
            // line8
            // 
            this.line8.Height = 0F;
            this.line8.Left = 9.874804F;
            this.line8.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.line8.LineWeight = 3F;
            this.line8.Name = "line8";
            this.line8.Top = 0.6736221F;
            this.line8.Width = 0.4393663F;
            this.line8.X1 = 9.874804F;
            this.line8.X2 = 10.31417F;
            this.line8.Y1 = 0.6736221F;
            this.line8.Y2 = 0.6736221F;
            // 
            // line10
            // 
            this.line10.Height = 0F;
            this.line10.Left = 0F;
            this.line10.LineWeight = 3F;
            this.line10.Name = "line10";
            this.line10.Top = 0.944882F;
            this.line10.Width = 10.62992F;
            this.line10.X1 = 0F;
            this.line10.X2 = 10.62992F;
            this.line10.Y1 = 0.944882F;
            this.line10.Y2 = 0.944882F;
            // 
            // textBox1
            // 
            this.textBox1.DataField = "ITEM02";
            this.textBox1.Height = 0.2F;
            this.textBox1.Left = 0.1354331F;
            this.textBox1.MultiLine = false;
            this.textBox1.Name = "textBox1";
            this.textBox1.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-style: normal; font-weight: normal; d" +
    "do-char-set: 128";
            this.textBox1.Text = null;
            this.textBox1.Top = 0.2362205F;
            this.textBox1.Width = 1.124409F;
            // 
            // lblTitle01
            // 
            this.lblTitle01.Height = 0.2625984F;
            this.lblTitle01.HyperLink = null;
            this.lblTitle01.Left = 3.710629F;
            this.lblTitle01.Name = "lblTitle01";
            this.lblTitle01.Style = "font-family: ＭＳ 明朝; font-size: 15pt; font-weight: bold; ddo-char-set: 1";
            this.lblTitle01.Text = "＊＊＊商品分類受払月報＊＊＊";
            this.lblTitle01.Top = 0F;
            this.lblTitle01.Width = 3.208661F;
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtGyohoCd,
            this.txtGyohoNm,
            this.label4,
            this.label5,
            this.txtGyoshuCd,
            this.txtGyoshuNm,
            this.textBox11,
            this.textBox12,
            this.textBox21,
            this.textBox58,
            this.textBox13,
            this.textBox14,
            this.textBox15,
            this.textBox16,
            this.textBox17,
            this.textBox20,
            this.textBox19,
            this.textBox18,
            this.textBox2,
            this.textBox3});
            this.detail.Height = 0.3710302F;
            this.detail.KeepTogether = true;
            this.detail.Name = "detail";
            // 
            // txtGyohoCd
            // 
            this.txtGyohoCd.DataField = "ITEM03";
            this.txtGyohoCd.Height = 0.1688977F;
            this.txtGyohoCd.Left = 0.6393701F;
            this.txtGyohoCd.Name = "txtGyohoCd";
            this.txtGyohoCd.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; ddo-char-set: 1";
            this.txtGyohoCd.Text = "Cd";
            this.txtGyohoCd.Top = 0F;
            this.txtGyohoCd.Width = 0.4326772F;
            // 
            // txtGyohoNm
            // 
            this.txtGyohoNm.DataField = "ITEM04";
            this.txtGyohoNm.Height = 0.1688977F;
            this.txtGyohoNm.Left = 1.072047F;
            this.txtGyohoNm.MultiLine = false;
            this.txtGyohoNm.Name = "txtGyohoNm";
            this.txtGyohoNm.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; ddo-char-set: 1";
            this.txtGyohoNm.Text = "漁法NM";
            this.txtGyohoNm.Top = 0F;
            this.txtGyohoNm.Width = 1.806692F;
            // 
            // label4
            // 
            this.label4.Height = 0.1688976F;
            this.label4.HyperLink = null;
            this.label4.Left = 3.003937F;
            this.label4.Name = "label4";
            this.label4.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; dd" +
    "o-char-set: 1";
            this.label4.Text = "月計";
            this.label4.Top = 0F;
            this.label4.Width = 0.3228345F;
            // 
            // label5
            // 
            this.label5.Height = 0.1688976F;
            this.label5.HyperLink = null;
            this.label5.Left = 3.003937F;
            this.label5.Name = "label5";
            this.label5.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; dd" +
    "o-char-set: 1";
            this.label5.Text = "累計";
            this.label5.Top = 0.2F;
            this.label5.Width = 0.3228347F;
            // 
            // txtGyoshuCd
            // 
            this.txtGyoshuCd.DataField = "ITEM05";
            this.txtGyoshuCd.Height = 0.1688976F;
            this.txtGyoshuCd.Left = 3.37126F;
            this.txtGyoshuCd.Name = "txtGyoshuCd";
            this.txtGyoshuCd.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtGyoshuCd.Text = "05";
            this.txtGyoshuCd.Top = 0F;
            this.txtGyoshuCd.Width = 0.7114177F;
            // 
            // txtGyoshuNm
            // 
            this.txtGyoshuNm.DataField = "ITEM06";
            this.txtGyoshuNm.Height = 0.1688976F;
            this.txtGyoshuNm.Left = 4.114173F;
            this.txtGyoshuNm.Name = "txtGyoshuNm";
            this.txtGyoshuNm.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtGyoshuNm.Text = "06";
            this.txtGyoshuNm.Top = 0F;
            this.txtGyoshuNm.Width = 0.805512F;
            // 
            // textBox11
            // 
            this.textBox11.DataField = "ITEM10";
            this.textBox11.Height = 0.1688976F;
            this.textBox11.Left = 7.676772F;
            this.textBox11.Name = "textBox11";
            this.textBox11.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox11.Text = "10";
            this.textBox11.Top = 0F;
            this.textBox11.Width = 0.8437014F;
            // 
            // textBox12
            // 
            this.textBox12.DataField = "ITEM11";
            this.textBox12.Height = 0.1688976F;
            this.textBox12.Left = 8.612992F;
            this.textBox12.Name = "textBox12";
            this.textBox12.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox12.Text = "11";
            this.textBox12.Top = 0F;
            this.textBox12.Width = 0.8437014F;
            // 
            // textBox21
            // 
            this.textBox21.DataField = "ITEM07";
            this.textBox21.Height = 0.1688976F;
            this.textBox21.Left = 5.027166F;
            this.textBox21.Name = "textBox21";
            this.textBox21.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox21.Text = "07";
            this.textBox21.Top = 0F;
            this.textBox21.Width = 0.8311024F;
            // 
            // textBox58
            // 
            this.textBox58.DataField = "ITEM08";
            this.textBox58.Height = 0.1688976F;
            this.textBox58.Left = 5.909449F;
            this.textBox58.Name = "textBox58";
            this.textBox58.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox58.Text = "08";
            this.textBox58.Top = 0F;
            this.textBox58.Width = 0.7996069F;
            // 
            // textBox13
            // 
            this.textBox13.DataField = "ITEM09";
            this.textBox13.Height = 0.1688976F;
            this.textBox13.Left = 6.799213F;
            this.textBox13.Name = "textBox13";
            this.textBox13.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox13.Text = "09";
            this.textBox13.Top = 0F;
            this.textBox13.Width = 0.8460631F;
            // 
            // textBox14
            // 
            this.textBox14.DataField = "ITEM12";
            this.textBox14.Height = 0.1688976F;
            this.textBox14.Left = 9.502756F;
            this.textBox14.Name = "textBox14";
            this.textBox14.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox14.Text = "12";
            this.textBox14.Top = 0F;
            this.textBox14.Width = 0.8279533F;
            // 
            // textBox15
            // 
            this.textBox15.DataField = "ITEM13";
            this.textBox15.Height = 0.1688976F;
            this.textBox15.Left = 3.37126F;
            this.textBox15.Name = "textBox15";
            this.textBox15.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox15.Text = "13";
            this.textBox15.Top = 0.2F;
            this.textBox15.Width = 0.7114167F;
            // 
            // textBox16
            // 
            this.textBox16.DataField = "ITEM14";
            this.textBox16.Height = 0.1688976F;
            this.textBox16.Left = 4.114173F;
            this.textBox16.Name = "textBox16";
            this.textBox16.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox16.Text = "14";
            this.textBox16.Top = 0.2F;
            this.textBox16.Width = 0.805511F;
            // 
            // textBox17
            // 
            this.textBox17.DataField = "ITEM15";
            this.textBox17.Height = 0.1688976F;
            this.textBox17.Left = 5.027166F;
            this.textBox17.Name = "textBox17";
            this.textBox17.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox17.Text = "15";
            this.textBox17.Top = 0.2F;
            this.textBox17.Width = 0.8311014F;
            // 
            // textBox20
            // 
            this.textBox20.DataField = "ITEM18";
            this.textBox20.Height = 0.1688976F;
            this.textBox20.Left = 7.676772F;
            this.textBox20.Name = "textBox20";
            this.textBox20.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox20.Text = "18";
            this.textBox20.Top = 0.2F;
            this.textBox20.Width = 0.8437008F;
            // 
            // textBox19
            // 
            this.textBox19.DataField = "ITEM17";
            this.textBox19.Height = 0.1688976F;
            this.textBox19.Left = 6.799213F;
            this.textBox19.Name = "textBox19";
            this.textBox19.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox19.Text = "17";
            this.textBox19.Top = 0.2F;
            this.textBox19.Width = 0.8460627F;
            // 
            // textBox18
            // 
            this.textBox18.DataField = "ITEM16";
            this.textBox18.Height = 0.1688976F;
            this.textBox18.Left = 5.920473F;
            this.textBox18.Name = "textBox18";
            this.textBox18.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox18.Text = "16";
            this.textBox18.Top = 0.2F;
            this.textBox18.Width = 0.7885826F;
            // 
            // textBox2
            // 
            this.textBox2.DataField = "ITEM20";
            this.textBox2.Height = 0.1688976F;
            this.textBox2.Left = 9.502756F;
            this.textBox2.Name = "textBox2";
            this.textBox2.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox2.Text = "20";
            this.textBox2.Top = 0.2F;
            this.textBox2.Width = 0.8279533F;
            // 
            // textBox3
            // 
            this.textBox3.DataField = "ITEM19";
            this.textBox3.Height = 0.1688976F;
            this.textBox3.Left = 8.612992F;
            this.textBox3.Name = "textBox3";
            this.textBox3.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox3.Text = "19";
            this.textBox3.Top = 0.2F;
            this.textBox3.Width = 0.8437014F;
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            // 
            // reportHeader1
            // 
            this.reportHeader1.Height = 0F;
            this.reportHeader1.Name = "reportHeader1";
            // 
            // reportFooter1
            // 
            this.reportFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.label13,
            this.label14,
            this.label15,
            this.txtTotal01,
            this.txtTotal07,
            this.txtTotal04,
            this.txtTotal09,
            this.txtTotal02,
            this.txtTotal08,
            this.txtTotal05,
            this.txtTotal10,
            this.txtTotal03,
            this.txtTotal06,
            this.label9,
            this.label12,
            this.line9});
            this.reportFooter1.Height = 0.5788714F;
            this.reportFooter1.Name = "reportFooter1";
            this.reportFooter1.Format += new System.EventHandler(this.reportFooter1_Format);
            // 
            // label13
            // 
            this.label13.Height = 0.1688977F;
            this.label13.HyperLink = null;
            this.label13.Left = 3.003937F;
            this.label13.Name = "label13";
            this.label13.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; dd" +
    "o-char-set: 1";
            this.label13.Text = "月計";
            this.label13.Top = 0.2153544F;
            this.label13.Width = 0.3228343F;
            // 
            // label14
            // 
            this.label14.Height = 0.1688976F;
            this.label14.HyperLink = null;
            this.label14.Left = 3.003937F;
            this.label14.Name = "label14";
            this.label14.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; dd" +
    "o-char-set: 1";
            this.label14.Text = "累計";
            this.label14.Top = 0.4102362F;
            this.label14.Width = 0.3228347F;
            // 
            // label15
            // 
            this.label15.Height = 0.1688976F;
            this.label15.HyperLink = null;
            this.label15.Left = 0.7283465F;
            this.label15.Name = "label15";
            this.label15.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ddo" +
    "-char-set: 1";
            this.label15.Text = "消費税額";
            this.label15.Top = 0.03110236F;
            this.label15.Width = 0.7051181F;
            // 
            // txtTotal01
            // 
            this.txtTotal01.DataField = "ITEM06";
            this.txtTotal01.Height = 0.1688976F;
            this.txtTotal01.Left = 3.740945F;
            this.txtTotal01.Name = "txtTotal01";
            this.txtTotal01.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtTotal01.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtTotal01.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtTotal01.Text = "06";
            this.txtTotal01.Top = 0.2153544F;
            this.txtTotal01.Width = 1.161811F;
            // 
            // txtTotal07
            // 
            this.txtTotal07.DataField = "ITEM10";
            this.txtTotal07.Height = 0.1688976F;
            this.txtTotal07.Left = 7.481497F;
            this.txtTotal07.Name = "txtTotal07";
            this.txtTotal07.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtTotal07.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtTotal07.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtTotal07.Text = "10";
            this.txtTotal07.Top = 0.2153544F;
            this.txtTotal07.Width = 1.038976F;
            // 
            // txtTotal04
            // 
            this.txtTotal04.DataField = "ITEM08";
            this.txtTotal04.Height = 0.1688976F;
            this.txtTotal04.Left = 5.548819F;
            this.txtTotal04.Name = "txtTotal04";
            this.txtTotal04.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtTotal04.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtTotal04.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtTotal04.Text = "08";
            this.txtTotal04.Top = 0.2153544F;
            this.txtTotal04.Width = 1.148819F;
            // 
            // txtTotal09
            // 
            this.txtTotal09.DataField = "ITEM12";
            this.txtTotal09.Height = 0.1688976F;
            this.txtTotal09.Left = 9.305906F;
            this.txtTotal09.Name = "txtTotal09";
            this.txtTotal09.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtTotal09.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtTotal09.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtTotal09.Text = "12";
            this.txtTotal09.Top = 0.2153544F;
            this.txtTotal09.Width = 1.024804F;
            // 
            // txtTotal02
            // 
            this.txtTotal02.DataField = "ITEM14";
            this.txtTotal02.Height = 0.1688976F;
            this.txtTotal02.Left = 3.740945F;
            this.txtTotal02.Name = "txtTotal02";
            this.txtTotal02.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtTotal02.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtTotal02.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtTotal02.Text = "14";
            this.txtTotal02.Top = 0.4102362F;
            this.txtTotal02.Width = 1.16181F;
            // 
            // txtTotal08
            // 
            this.txtTotal08.DataField = "ITEM18";
            this.txtTotal08.Height = 0.1688976F;
            this.txtTotal08.Left = 7.481497F;
            this.txtTotal08.Name = "txtTotal08";
            this.txtTotal08.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtTotal08.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtTotal08.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtTotal08.Text = "18";
            this.txtTotal08.Top = 0.4102362F;
            this.txtTotal08.Width = 1.038977F;
            // 
            // txtTotal05
            // 
            this.txtTotal05.DataField = "ITEM16";
            this.txtTotal05.Height = 0.1688976F;
            this.txtTotal05.Left = 5.548819F;
            this.txtTotal05.Name = "txtTotal05";
            this.txtTotal05.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtTotal05.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtTotal05.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtTotal05.Text = "16";
            this.txtTotal05.Top = 0.4102362F;
            this.txtTotal05.Width = 1.148819F;
            // 
            // txtTotal10
            // 
            this.txtTotal10.DataField = "ITEM20";
            this.txtTotal10.Height = 0.1688976F;
            this.txtTotal10.Left = 9.305906F;
            this.txtTotal10.Name = "txtTotal10";
            this.txtTotal10.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtTotal10.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtTotal10.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtTotal10.Text = "20";
            this.txtTotal10.Top = 0.4102362F;
            this.txtTotal10.Width = 1.024804F;
            // 
            // txtTotal03
            // 
            this.txtTotal03.DataField = "ITEM21";
            this.txtTotal03.Height = 0.1688976F;
            this.txtTotal03.Left = 5.548819F;
            this.txtTotal03.Name = "txtTotal03";
            this.txtTotal03.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtTotal03.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtTotal03.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtTotal03.Text = "21";
            this.txtTotal03.Top = 0.03110236F;
            this.txtTotal03.Width = 1.148425F;
            // 
            // txtTotal06
            // 
            this.txtTotal06.DataField = "ITEM22";
            this.txtTotal06.Height = 0.1688976F;
            this.txtTotal06.Left = 7.481497F;
            this.txtTotal06.Name = "txtTotal06";
            this.txtTotal06.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtTotal06.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtTotal06.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtTotal06.Text = "22";
            this.txtTotal06.Top = 0.03110236F;
            this.txtTotal06.Width = 1.038976F;
            // 
            // label9
            // 
            this.label9.Height = 0.2F;
            this.label9.HyperLink = null;
            this.label9.Left = 1.092914F;
            this.label9.Name = "label9";
            this.label9.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ddo" +
    "-char-set: 1";
            this.label9.Text = "＊ 総 合 計 ＊";
            this.label9.Top = 0.2311024F;
            this.label9.Width = 1.220472F;
            // 
            // label12
            // 
            this.label12.Height = 0.1688976F;
            this.label12.HyperLink = null;
            this.label12.Left = 3.003937F;
            this.label12.Name = "label12";
            this.label12.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; dd" +
    "o-char-set: 1";
            this.label12.Text = "月計";
            this.label12.Top = 0.03110236F;
            this.label12.Width = 0.322835F;
            // 
            // line9
            // 
            this.line9.Height = 0F;
            this.line9.Left = 0F;
            this.line9.LineWeight = 3F;
            this.line9.Name = "line9";
            this.line9.Top = 0F;
            this.line9.Width = 10.62992F;
            this.line9.X1 = 0F;
            this.line9.X2 = 10.62992F;
            this.line9.Y1 = 0F;
            this.line9.Y2 = 0F;
            // 
            // groupHeader1
            // 
            this.groupHeader1.DataField = "ITEM23";
            this.groupHeader1.Height = 0F;
            this.groupHeader1.Name = "groupHeader1";
            // 
            // groupFooter1
            // 
            this.groupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.label7,
            this.label8,
            this.label10,
            this.label11,
            this.textBox4,
            this.textBox5,
            this.textBox6,
            this.textBox7,
            this.textBox8,
            this.textBox9,
            this.textBox10,
            this.textBox22,
            this.textBox23,
            this.textBox24,
            this.textBox25,
            this.textBox26,
            this.textBox27,
            this.textBox28,
            this.textBox29,
            this.textBox30,
            this.textBox31,
            this.textBox32,
            this.textBox43,
            this.label16,
            this.textBox44,
            this.label1,
            this.label6,
            this.line11});
            this.groupFooter1.Height = 0.9669454F;
            this.groupFooter1.KeepTogether = true;
            this.groupFooter1.Name = "groupFooter1";
            // 
            // label7
            // 
            this.label7.Height = 0.1688976F;
            this.label7.HyperLink = null;
            this.label7.Left = 3.003937F;
            this.label7.Name = "label7";
            this.label7.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; dd" +
    "o-char-set: 1";
            this.label7.Text = "月計";
            this.label7.Top = 0.4F;
            this.label7.Width = 0.3228346F;
            // 
            // label8
            // 
            this.label8.Height = 0.2F;
            this.label8.HyperLink = null;
            this.label8.Left = 0.7283465F;
            this.label8.Name = "label8";
            this.label8.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ddo" +
    "-char-set: 1";
            this.label8.Text = "消費税額";
            this.label8.Top = 0.4F;
            this.label8.Width = 0.7051181F;
            // 
            // label10
            // 
            this.label10.Height = 0.1688976F;
            this.label10.HyperLink = null;
            this.label10.Left = 3.003937F;
            this.label10.Name = "label10";
            this.label10.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; dd" +
    "o-char-set: 1";
            this.label10.Text = "月計";
            this.label10.Top = 0.584252F;
            this.label10.Width = 0.3228346F;
            // 
            // label11
            // 
            this.label11.Height = 0.1688976F;
            this.label11.HyperLink = null;
            this.label11.Left = 3.003937F;
            this.label11.Name = "label11";
            this.label11.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; dd" +
    "o-char-set: 1";
            this.label11.Text = "累計";
            this.label11.Top = 0.768504F;
            this.label11.Width = 0.3228346F;
            // 
            // textBox4
            // 
            this.textBox4.DataField = "ITEM05";
            this.textBox4.Height = 0.1688976F;
            this.textBox4.Left = 3.37126F;
            this.textBox4.Name = "textBox4";
            this.textBox4.OutputFormat = resources.GetString("textBox4.OutputFormat");
            this.textBox4.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox4.SummaryGroup = "groupHeader1";
            this.textBox4.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox4.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox4.Text = "05";
            this.textBox4.Top = 0F;
            this.textBox4.Width = 0.7114167F;
            // 
            // textBox5
            // 
            this.textBox5.DataField = "ITEM06";
            this.textBox5.Height = 0.1688976F;
            this.textBox5.Left = 3.911024F;
            this.textBox5.Name = "textBox5";
            this.textBox5.OutputFormat = resources.GetString("textBox5.OutputFormat");
            this.textBox5.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox5.SummaryGroup = "groupHeader1";
            this.textBox5.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox5.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox5.Text = "06";
            this.textBox5.Top = 0.584252F;
            this.textBox5.Width = 1.009055F;
            // 
            // textBox6
            // 
            this.textBox6.DataField = "ITEM10";
            this.textBox6.Height = 0.1688976F;
            this.textBox6.Left = 7.473229F;
            this.textBox6.Name = "textBox6";
            this.textBox6.OutputFormat = resources.GetString("textBox6.OutputFormat");
            this.textBox6.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox6.SummaryGroup = "groupHeader1";
            this.textBox6.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox6.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox6.Text = "10";
            this.textBox6.Top = 0.584252F;
            this.textBox6.Width = 1.047243F;
            // 
            // textBox7
            // 
            this.textBox7.DataField = "ITEM11";
            this.textBox7.Height = 0.1688976F;
            this.textBox7.Left = 8.637795F;
            this.textBox7.Name = "textBox7";
            this.textBox7.OutputFormat = resources.GetString("textBox7.OutputFormat");
            this.textBox7.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox7.SummaryGroup = "groupHeader1";
            this.textBox7.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox7.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox7.Text = "11";
            this.textBox7.Top = 0F;
            this.textBox7.Width = 0.8188972F;
            // 
            // textBox8
            // 
            this.textBox8.DataField = "ITEM07";
            this.textBox8.Height = 0.1688976F;
            this.textBox8.Left = 5.027166F;
            this.textBox8.Name = "textBox8";
            this.textBox8.OutputFormat = resources.GetString("textBox8.OutputFormat");
            this.textBox8.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox8.SummaryGroup = "groupHeader1";
            this.textBox8.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox8.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox8.Text = "07";
            this.textBox8.Top = 0F;
            this.textBox8.Width = 0.8311024F;
            // 
            // textBox9
            // 
            this.textBox9.DataField = "ITEM08";
            this.textBox9.Height = 0.1688976F;
            this.textBox9.Left = 5.665749F;
            this.textBox9.Name = "textBox9";
            this.textBox9.OutputFormat = resources.GetString("textBox9.OutputFormat");
            this.textBox9.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox9.SummaryGroup = "groupHeader1";
            this.textBox9.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox9.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox9.Text = "08";
            this.textBox9.Top = 0.584252F;
            this.textBox9.Width = 1.031889F;
            // 
            // textBox10
            // 
            this.textBox10.DataField = "ITEM09";
            this.textBox10.Height = 0.1688976F;
            this.textBox10.Left = 6.799213F;
            this.textBox10.Name = "textBox10";
            this.textBox10.OutputFormat = resources.GetString("textBox10.OutputFormat");
            this.textBox10.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox10.SummaryGroup = "groupHeader1";
            this.textBox10.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox10.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox10.Text = "09";
            this.textBox10.Top = 0F;
            this.textBox10.Width = 0.8460631F;
            // 
            // textBox22
            // 
            this.textBox22.DataField = "ITEM12";
            this.textBox22.Height = 0.1688976F;
            this.textBox22.Left = 9.318504F;
            this.textBox22.Name = "textBox22";
            this.textBox22.OutputFormat = resources.GetString("textBox22.OutputFormat");
            this.textBox22.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox22.SummaryGroup = "groupHeader1";
            this.textBox22.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox22.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox22.Text = "12";
            this.textBox22.Top = 0.584252F;
            this.textBox22.Width = 1.012205F;
            // 
            // textBox23
            // 
            this.textBox23.DataField = "ITEM13";
            this.textBox23.Height = 0.1688976F;
            this.textBox23.Left = 3.37126F;
            this.textBox23.Name = "textBox23";
            this.textBox23.OutputFormat = resources.GetString("textBox23.OutputFormat");
            this.textBox23.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox23.SummaryGroup = "groupHeader1";
            this.textBox23.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox23.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox23.Text = "13";
            this.textBox23.Top = 0.1850394F;
            this.textBox23.Width = 0.7114162F;
            // 
            // textBox24
            // 
            this.textBox24.DataField = "ITEM14";
            this.textBox24.Height = 0.1688976F;
            this.textBox24.Left = 3.91063F;
            this.textBox24.Name = "textBox24";
            this.textBox24.OutputFormat = resources.GetString("textBox24.OutputFormat");
            this.textBox24.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox24.SummaryGroup = "groupHeader1";
            this.textBox24.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox24.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox24.Text = "14";
            this.textBox24.Top = 0.768504F;
            this.textBox24.Width = 1.009055F;
            // 
            // textBox25
            // 
            this.textBox25.DataField = "ITEM15";
            this.textBox25.Height = 0.1688976F;
            this.textBox25.Left = 5.027166F;
            this.textBox25.Name = "textBox25";
            this.textBox25.OutputFormat = resources.GetString("textBox25.OutputFormat");
            this.textBox25.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox25.SummaryGroup = "groupHeader1";
            this.textBox25.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox25.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox25.Text = "15";
            this.textBox25.Top = 0.1850394F;
            this.textBox25.Width = 0.8311009F;
            // 
            // textBox26
            // 
            this.textBox26.DataField = "ITEM18";
            this.textBox26.Height = 0.1688976F;
            this.textBox26.Left = 7.473229F;
            this.textBox26.Name = "textBox26";
            this.textBox26.OutputFormat = resources.GetString("textBox26.OutputFormat");
            this.textBox26.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox26.SummaryGroup = "groupHeader1";
            this.textBox26.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox26.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox26.Text = "18";
            this.textBox26.Top = 0.768504F;
            this.textBox26.Width = 1.047243F;
            // 
            // textBox27
            // 
            this.textBox27.DataField = "ITEM17";
            this.textBox27.Height = 0.1688976F;
            this.textBox27.Left = 6.799213F;
            this.textBox27.Name = "textBox27";
            this.textBox27.OutputFormat = resources.GetString("textBox27.OutputFormat");
            this.textBox27.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox27.SummaryGroup = "groupHeader1";
            this.textBox27.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox27.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox27.Text = "17";
            this.textBox27.Top = 0.1850394F;
            this.textBox27.Width = 0.8460631F;
            // 
            // textBox28
            // 
            this.textBox28.DataField = "ITEM16";
            this.textBox28.Height = 0.1688976F;
            this.textBox28.Left = 5.666536F;
            this.textBox28.Name = "textBox28";
            this.textBox28.OutputFormat = resources.GetString("textBox28.OutputFormat");
            this.textBox28.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox28.SummaryGroup = "groupHeader1";
            this.textBox28.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox28.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox28.Text = "16";
            this.textBox28.Top = 0.768504F;
            this.textBox28.Width = 1.031889F;
            // 
            // textBox29
            // 
            this.textBox29.DataField = "ITEM20";
            this.textBox29.Height = 0.1688976F;
            this.textBox29.Left = 9.314961F;
            this.textBox29.Name = "textBox29";
            this.textBox29.OutputFormat = resources.GetString("textBox29.OutputFormat");
            this.textBox29.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox29.SummaryGroup = "groupHeader1";
            this.textBox29.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox29.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox29.Text = "20";
            this.textBox29.Top = 0.768504F;
            this.textBox29.Width = 1.015748F;
            // 
            // textBox30
            // 
            this.textBox30.DataField = "ITEM19";
            this.textBox30.Height = 0.1688976F;
            this.textBox30.Left = 8.636615F;
            this.textBox30.Name = "textBox30";
            this.textBox30.OutputFormat = resources.GetString("textBox30.OutputFormat");
            this.textBox30.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox30.SummaryGroup = "groupHeader1";
            this.textBox30.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox30.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox30.Text = "19";
            this.textBox30.Top = 0.1850394F;
            this.textBox30.Width = 0.8200784F;
            // 
            // textBox31
            // 
            this.textBox31.DataField = "ITEM21";
            this.textBox31.Height = 0.1688976F;
            this.textBox31.Left = 5.66693F;
            this.textBox31.Name = "textBox31";
            this.textBox31.OutputFormat = resources.GetString("textBox31.OutputFormat");
            this.textBox31.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox31.SummaryGroup = "groupHeader1";
            this.textBox31.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox31.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox31.Text = "21";
            this.textBox31.Top = 0.4F;
            this.textBox31.Width = 1.031495F;
            // 
            // textBox32
            // 
            this.textBox32.DataField = "ITEM22";
            this.textBox32.Height = 0.1688976F;
            this.textBox32.Left = 7.473229F;
            this.textBox32.Name = "textBox32";
            this.textBox32.OutputFormat = resources.GetString("textBox32.OutputFormat");
            this.textBox32.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox32.SummaryGroup = "groupHeader1";
            this.textBox32.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox32.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox32.Text = "22";
            this.textBox32.Top = 0.4F;
            this.textBox32.Width = 1.047243F;
            // 
            // textBox43
            // 
            this.textBox43.DataField = "ITEM23";
            this.textBox43.Height = 0.1688976F;
            this.textBox43.Left = 0.7649607F;
            this.textBox43.MultiLine = false;
            this.textBox43.Name = "textBox43";
            this.textBox43.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: normal; text-align: right; d" +
    "do-char-set: 1";
            this.textBox43.Text = "23";
            this.textBox43.Top = 0F;
            this.textBox43.Width = 0.4775591F;
            // 
            // label16
            // 
            this.label16.Height = 0.1688976F;
            this.label16.HyperLink = null;
            this.label16.Left = 0.9F;
            this.label16.Name = "label16";
            this.label16.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ddo" +
    "-char-set: 1";
            this.label16.Text = "＊ 区 分 別 合 計 ＊";
            this.label16.Top = 0.622441F;
            this.label16.Width = 1.413386F;
            // 
            // textBox44
            // 
            this.textBox44.DataField = "ITEM24";
            this.textBox44.Height = 0.1688976F;
            this.textBox44.Left = 1.259843F;
            this.textBox44.MultiLine = false;
            this.textBox44.Name = "textBox44";
            this.textBox44.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: normal; ddo-char-set: 1";
            this.textBox44.Text = "24";
            this.textBox44.Top = 0F;
            this.textBox44.Width = 0.947638F;
            // 
            // label1
            // 
            this.label1.Height = 0.1688976F;
            this.label1.HyperLink = null;
            this.label1.Left = 0.2248032F;
            this.label1.Name = "label1";
            this.label1.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; " +
    "ddo-char-set: 1";
            this.label1.Text = "＜＜";
            this.label1.Top = 0F;
            this.label1.Width = 0.4145668F;
            // 
            // label6
            // 
            this.label6.Height = 0.1688976F;
            this.label6.HyperLink = null;
            this.label6.Left = 2.313386F;
            this.label6.Name = "label6";
            this.label6.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left";
            this.label6.Text = "＞＞";
            this.label6.Top = 0F;
            this.label6.Width = 0.3893702F;
            // 
            // line11
            // 
            this.line11.Height = 0F;
            this.line11.Left = 0F;
            this.line11.LineWeight = 2.5F;
            this.line11.Name = "line11";
            this.line11.Top = 0.9669292F;
            this.line11.Width = 10.62992F;
            this.line11.X1 = 0F;
            this.line11.X2 = 10.62992F;
            this.line11.Y1 = 0.9669292F;
            this.line11.Y2 = 0.9669292F;
            // 
            // groupHeader2
            // 
            this.groupHeader2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.textBox33,
            this.textBox34});
            this.groupHeader2.DataField = "ITEM26";
            this.groupHeader2.Height = 0.1666667F;
            this.groupHeader2.Name = "groupHeader2";
            // 
            // textBox33
            // 
            this.textBox33.DataField = "ITEM26";
            this.textBox33.Height = 0.1574803F;
            this.textBox33.Left = 0.07677166F;
            this.textBox33.MultiLine = false;
            this.textBox33.Name = "textBox33";
            this.textBox33.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: normal; text-align: right; d" +
    "do-char-set: 1";
            this.textBox33.Text = "26";
            this.textBox33.Top = 0F;
            this.textBox33.Width = 0.4775591F;
            // 
            // textBox34
            // 
            this.textBox34.DataField = "ITEM27";
            this.textBox34.Height = 0.1574803F;
            this.textBox34.Left = 0.6393701F;
            this.textBox34.MultiLine = false;
            this.textBox34.Name = "textBox34";
            this.textBox34.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: normal; ddo-char-set: 1";
            this.textBox34.Text = "27";
            this.textBox34.Top = 0F;
            this.textBox34.Width = 1.407874F;
            // 
            // groupFooter2
            // 
            this.groupFooter2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.label3,
            this.label17,
            this.label18,
            this.label19,
            this.textBox36,
            this.textBox37,
            this.textBox40,
            this.textBox42,
            this.textBox46,
            this.textBox48,
            this.textBox50,
            this.textBox51,
            this.textBox53,
            this.textBox54,
            this.label20,
            this.line12,
            this.textBox55,
            this.textBox56,
            this.textBox60,
            this.textBox61,
            this.textBox62,
            this.textBox63,
            this.textBox64,
            this.textBox65,
            this.textBox66,
            this.textBox67,
            this.textBox68});
            this.groupFooter2.Height = 0.9740158F;
            this.groupFooter2.KeepTogether = true;
            this.groupFooter2.Name = "groupFooter2";
            // 
            // label3
            // 
            this.label3.Height = 0.1688976F;
            this.label3.HyperLink = null;
            this.label3.Left = 3.003939F;
            this.label3.Name = "label3";
            this.label3.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; dd" +
    "o-char-set: 1";
            this.label3.Text = "月計";
            this.label3.Top = 0.3937008F;
            this.label3.Width = 0.3228346F;
            // 
            // label17
            // 
            this.label17.Height = 0.1511811F;
            this.label17.HyperLink = null;
            this.label17.Left = 0.7283496F;
            this.label17.Name = "label17";
            this.label17.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ddo" +
    "-char-set: 1";
            this.label17.Text = "消費税額";
            this.label17.Top = 0.3937008F;
            this.label17.Width = 0.7051181F;
            // 
            // label18
            // 
            this.label18.Height = 0.1688976F;
            this.label18.HyperLink = null;
            this.label18.Left = 3.003939F;
            this.label18.Name = "label18";
            this.label18.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; dd" +
    "o-char-set: 1";
            this.label18.Text = "月計";
            this.label18.Top = 0.576378F;
            this.label18.Width = 0.3228346F;
            // 
            // label19
            // 
            this.label19.Height = 0.1688976F;
            this.label19.HyperLink = null;
            this.label19.Left = 3.003939F;
            this.label19.Name = "label19";
            this.label19.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; dd" +
    "o-char-set: 1";
            this.label19.Text = "累計";
            this.label19.Top = 0.7661419F;
            this.label19.Width = 0.3228346F;
            // 
            // textBox36
            // 
            this.textBox36.DataField = "ITEM06";
            this.textBox36.Height = 0.1688976F;
            this.textBox36.Left = 3.911026F;
            this.textBox36.Name = "textBox36";
            this.textBox36.OutputFormat = resources.GetString("textBox36.OutputFormat");
            this.textBox36.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox36.SummaryGroup = "groupHeader2";
            this.textBox36.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox36.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox36.Text = "06";
            this.textBox36.Top = 0.576378F;
            this.textBox36.Width = 1.008662F;
            // 
            // textBox37
            // 
            this.textBox37.DataField = "ITEM10";
            this.textBox37.Height = 0.1688976F;
            this.textBox37.Left = 7.473625F;
            this.textBox37.Name = "textBox37";
            this.textBox37.OutputFormat = resources.GetString("textBox37.OutputFormat");
            this.textBox37.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox37.SummaryGroup = "groupHeader2";
            this.textBox37.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox37.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox37.Text = "10";
            this.textBox37.Top = 0.576378F;
            this.textBox37.Width = 1.04685F;
            // 
            // textBox40
            // 
            this.textBox40.DataField = "ITEM08";
            this.textBox40.Height = 0.1688976F;
            this.textBox40.Left = 5.666538F;
            this.textBox40.Name = "textBox40";
            this.textBox40.OutputFormat = resources.GetString("textBox40.OutputFormat");
            this.textBox40.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox40.SummaryGroup = "groupHeader2";
            this.textBox40.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox40.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox40.Text = "08";
            this.textBox40.Top = 0.576378F;
            this.textBox40.Width = 1.031496F;
            // 
            // textBox42
            // 
            this.textBox42.DataField = "ITEM12";
            this.textBox42.Height = 0.1688976F;
            this.textBox42.Left = 9.3189F;
            this.textBox42.Name = "textBox42";
            this.textBox42.OutputFormat = resources.GetString("textBox42.OutputFormat");
            this.textBox42.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox42.SummaryGroup = "groupHeader2";
            this.textBox42.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox42.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox42.Text = "12";
            this.textBox42.Top = 0.576378F;
            this.textBox42.Width = 1.011812F;
            // 
            // textBox46
            // 
            this.textBox46.DataField = "ITEM14";
            this.textBox46.Height = 0.1688976F;
            this.textBox46.Left = 3.911026F;
            this.textBox46.Name = "textBox46";
            this.textBox46.OutputFormat = resources.GetString("textBox46.OutputFormat");
            this.textBox46.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox46.SummaryGroup = "groupHeader2";
            this.textBox46.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox46.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox46.Text = "14";
            this.textBox46.Top = 0.7661419F;
            this.textBox46.Width = 1.008662F;
            // 
            // textBox48
            // 
            this.textBox48.DataField = "ITEM18";
            this.textBox48.Height = 0.1688976F;
            this.textBox48.Left = 7.473625F;
            this.textBox48.Name = "textBox48";
            this.textBox48.OutputFormat = resources.GetString("textBox48.OutputFormat");
            this.textBox48.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox48.SummaryGroup = "groupHeader2";
            this.textBox48.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox48.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox48.Text = "18";
            this.textBox48.Top = 0.7661419F;
            this.textBox48.Width = 1.04685F;
            // 
            // textBox50
            // 
            this.textBox50.DataField = "ITEM16";
            this.textBox50.Height = 0.1688976F;
            this.textBox50.Left = 5.666538F;
            this.textBox50.Name = "textBox50";
            this.textBox50.OutputFormat = resources.GetString("textBox50.OutputFormat");
            this.textBox50.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox50.SummaryGroup = "groupHeader2";
            this.textBox50.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox50.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox50.Text = "16";
            this.textBox50.Top = 0.7661419F;
            this.textBox50.Width = 1.031496F;
            // 
            // textBox51
            // 
            this.textBox51.DataField = "ITEM20";
            this.textBox51.Height = 0.1688976F;
            this.textBox51.Left = 9.3189F;
            this.textBox51.Name = "textBox51";
            this.textBox51.OutputFormat = resources.GetString("textBox51.OutputFormat");
            this.textBox51.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox51.SummaryGroup = "groupHeader2";
            this.textBox51.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox51.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox51.Text = "20";
            this.textBox51.Top = 0.7661419F;
            this.textBox51.Width = 1.015355F;
            // 
            // textBox53
            // 
            this.textBox53.DataField = "ITEM21";
            this.textBox53.Height = 0.1688976F;
            this.textBox53.Left = 5.666932F;
            this.textBox53.Name = "textBox53";
            this.textBox53.OutputFormat = resources.GetString("textBox53.OutputFormat");
            this.textBox53.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox53.SummaryGroup = "groupHeader2";
            this.textBox53.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox53.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox53.Text = "21";
            this.textBox53.Top = 0.3937008F;
            this.textBox53.Width = 1.031102F;
            // 
            // textBox54
            // 
            this.textBox54.DataField = "ITEM22";
            this.textBox54.Height = 0.1688976F;
            this.textBox54.Left = 7.473623F;
            this.textBox54.Name = "textBox54";
            this.textBox54.OutputFormat = resources.GetString("textBox54.OutputFormat");
            this.textBox54.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox54.SummaryGroup = "groupHeader2";
            this.textBox54.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox54.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox54.Text = "22";
            this.textBox54.Top = 0.3937008F;
            this.textBox54.Width = 1.04685F;
            // 
            // label20
            // 
            this.label20.Height = 0.1688976F;
            this.label20.HyperLink = null;
            this.label20.Left = 0.7874032F;
            this.label20.Name = "label20";
            this.label20.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ddo" +
    "-char-set: 1";
            this.label20.Text = "＊ 　　　　　　　 合 計 ＊";
            this.label20.Top = 0.576378F;
            this.label20.Width = 1.811024F;
            // 
            // line12
            // 
            this.line12.Height = 0F;
            this.line12.Left = 1.549721E-06F;
            this.line12.LineWeight = 1F;
            this.line12.Name = "line12";
            this.line12.Top = 0.9503937F;
            this.line12.Width = 10.62992F;
            this.line12.X1 = 1.549721E-06F;
            this.line12.X2 = 10.62992F;
            this.line12.Y1 = 0.9503937F;
            this.line12.Y2 = 0.9503937F;
            // 
            // textBox55
            // 
            this.textBox55.DataField = "ITEM27";
            this.textBox55.Height = 0.1688976F;
            this.textBox55.Left = 1.023624F;
            this.textBox55.MultiLine = false;
            this.textBox55.Name = "textBox55";
            this.textBox55.Style = "font-family: ＭＳ 明朝; font-size: 10.5pt; ddo-char-set: 1";
            this.textBox55.Text = "27";
            this.textBox55.Top = 0.576378F;
            this.textBox55.Width = 0.9448819F;
            // 
            // textBox56
            // 
            this.textBox56.DataField = "ITEM05";
            this.textBox56.Height = 0.1688976F;
            this.textBox56.Left = 3.371262F;
            this.textBox56.Name = "textBox56";
            this.textBox56.OutputFormat = resources.GetString("textBox56.OutputFormat");
            this.textBox56.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox56.SummaryGroup = "groupHeader2";
            this.textBox56.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox56.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox56.Text = "05";
            this.textBox56.Top = 0F;
            this.textBox56.Width = 0.7114166F;
            // 
            // textBox60
            // 
            this.textBox60.DataField = "ITEM11";
            this.textBox60.Height = 0.1688976F;
            this.textBox60.Left = 8.6378F;
            this.textBox60.Name = "textBox60";
            this.textBox60.OutputFormat = resources.GetString("textBox60.OutputFormat");
            this.textBox60.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox60.SummaryGroup = "groupHeader2";
            this.textBox60.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox60.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox60.Text = "11";
            this.textBox60.Top = 0F;
            this.textBox60.Width = 0.8188972F;
            // 
            // textBox61
            // 
            this.textBox61.DataField = "ITEM07";
            this.textBox61.Height = 0.1688976F;
            this.textBox61.Left = 5.027165F;
            this.textBox61.Name = "textBox61";
            this.textBox61.OutputFormat = resources.GetString("textBox61.OutputFormat");
            this.textBox61.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox61.SummaryGroup = "groupHeader2";
            this.textBox61.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox61.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox61.Text = "07";
            this.textBox61.Top = 0F;
            this.textBox61.Width = 0.8311021F;
            // 
            // textBox62
            // 
            this.textBox62.DataField = "ITEM09";
            this.textBox62.Height = 0.1688976F;
            this.textBox62.Left = 6.799212F;
            this.textBox62.Name = "textBox62";
            this.textBox62.OutputFormat = resources.GetString("textBox62.OutputFormat");
            this.textBox62.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox62.SummaryGroup = "groupHeader2";
            this.textBox62.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox62.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox62.Text = "09";
            this.textBox62.Top = 0F;
            this.textBox62.Width = 0.8460633F;
            // 
            // textBox63
            // 
            this.textBox63.DataField = "ITEM13";
            this.textBox63.Height = 0.1688976F;
            this.textBox63.Left = 3.37126F;
            this.textBox63.Name = "textBox63";
            this.textBox63.OutputFormat = resources.GetString("textBox63.OutputFormat");
            this.textBox63.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox63.SummaryGroup = "groupHeader2";
            this.textBox63.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox63.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox63.Text = "13";
            this.textBox63.Top = 0.1850394F;
            this.textBox63.Width = 0.7114159F;
            // 
            // textBox64
            // 
            this.textBox64.DataField = "ITEM15";
            this.textBox64.Height = 0.1688976F;
            this.textBox64.Left = 5.027165F;
            this.textBox64.Name = "textBox64";
            this.textBox64.OutputFormat = resources.GetString("textBox64.OutputFormat");
            this.textBox64.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox64.SummaryGroup = "groupHeader2";
            this.textBox64.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox64.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox64.Text = "15";
            this.textBox64.Top = 0.1850394F;
            this.textBox64.Width = 0.8311007F;
            // 
            // textBox65
            // 
            this.textBox65.DataField = "ITEM17";
            this.textBox65.Height = 0.1688976F;
            this.textBox65.Left = 6.799212F;
            this.textBox65.Name = "textBox65";
            this.textBox65.OutputFormat = resources.GetString("textBox65.OutputFormat");
            this.textBox65.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox65.SummaryGroup = "groupHeader2";
            this.textBox65.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox65.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox65.Text = "17";
            this.textBox65.Top = 0.1850394F;
            this.textBox65.Width = 0.8460633F;
            // 
            // textBox66
            // 
            this.textBox66.DataField = "ITEM19";
            this.textBox66.Height = 0.1688976F;
            this.textBox66.Left = 8.63662F;
            this.textBox66.Name = "textBox66";
            this.textBox66.OutputFormat = resources.GetString("textBox66.OutputFormat");
            this.textBox66.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox66.SummaryGroup = "groupHeader2";
            this.textBox66.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox66.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox66.Text = "19";
            this.textBox66.Top = 0.1850394F;
            this.textBox66.Width = 0.8200784F;
            // 
            // textBox67
            // 
            this.textBox67.DataField = "ITEM26";
            this.textBox67.Height = 0.1688976F;
            this.textBox67.Left = 0.7649607F;
            this.textBox67.MultiLine = false;
            this.textBox67.Name = "textBox67";
            this.textBox67.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: normal; text-align: right; d" +
    "do-char-set: 1";
            this.textBox67.Text = "26";
            this.textBox67.Top = 0F;
            this.textBox67.Width = 0.4775591F;
            // 
            // textBox68
            // 
            this.textBox68.DataField = "ITEM27";
            this.textBox68.Height = 0.1688976F;
            this.textBox68.Left = 1.259844F;
            this.textBox68.MultiLine = false;
            this.textBox68.Name = "textBox68";
            this.textBox68.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: normal; ddo-char-set: 1";
            this.textBox68.Text = "27";
            this.textBox68.Top = 0F;
            this.textBox68.Width = 1.259843F;
            // 
            // KBMR1011R
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.2755905F;
            this.PageSettings.Margins.Left = 0.5275591F;
            this.PageSettings.Margins.Right = 0.5275591F;
            this.PageSettings.Margins.Top = 0.2755905F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
            this.PageSettings.PaperHeight = 11.69291F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.267716F;
            this.PrintWidth = 10.62992F;
            this.Sections.Add(this.reportHeader1);
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.groupHeader1);
            this.Sections.Add(this.groupHeader2);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.groupFooter2);
            this.Sections.Add(this.groupFooter1);
            this.Sections.Add(this.pageFooter);
            this.Sections.Add(this.reportFooter1);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtToday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblJojun)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTyujun)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGejun)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSekisu01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSuryo01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKingaku01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSekisu02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSuryo02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKingaku02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSekisu03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSuryo03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKingaku03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSekisu04)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSuryo04)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyohoCd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyohoNm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyoshuCd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyoshuNm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox58)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal07)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal04)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal09)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal08)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal05)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal06)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox54)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox55)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox56)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox60)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox61)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox62)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox63)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox64)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox65)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox66)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox67)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox68)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.ReportHeader reportHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.ReportFooter reportFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader groupHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter groupFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtToday;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDateTo;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle01;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblJojun;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTyujun;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblGejun;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
        private GrapeCity.ActiveReports.SectionReportModel.Line line2;
        private GrapeCity.ActiveReports.SectionReportModel.Line line3;
        private GrapeCity.ActiveReports.SectionReportModel.Line line4;
        private GrapeCity.ActiveReports.SectionReportModel.Line line5;
        private GrapeCity.ActiveReports.SectionReportModel.Line line6;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSekisu01;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSuryo01;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblKingaku01;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSekisu02;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSuryo02;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblKingaku02;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSekisu03;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSuryo03;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblKingaku03;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSekisu04;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSuryo04;
        private GrapeCity.ActiveReports.SectionReportModel.Label label2;
        private GrapeCity.ActiveReports.SectionReportModel.Line line7;
        private GrapeCity.ActiveReports.SectionReportModel.Line line8;
        private GrapeCity.ActiveReports.SectionReportModel.Line line10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGyohoCd;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGyohoNm;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGyoshuCd;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGyoshuNm;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox21;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox58;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox13;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox14;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox15;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox16;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox17;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox20;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox19;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox18;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox3;
        private GrapeCity.ActiveReports.SectionReportModel.Label label12;
        private GrapeCity.ActiveReports.SectionReportModel.Label label13;
        private GrapeCity.ActiveReports.SectionReportModel.Label label14;
        private GrapeCity.ActiveReports.SectionReportModel.Label label15;
        private GrapeCity.ActiveReports.SectionReportModel.Label label16;
        private GrapeCity.ActiveReports.SectionReportModel.Label label7;
        private GrapeCity.ActiveReports.SectionReportModel.Label label8;
        private GrapeCity.ActiveReports.SectionReportModel.Label label10;
        private GrapeCity.ActiveReports.SectionReportModel.Label label11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal07;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal04;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal09;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal02;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal08;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal05;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal03;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal06;
        private GrapeCity.ActiveReports.SectionReportModel.Label label9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox22;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox23;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox24;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox25;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox26;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox27;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox28;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox29;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox30;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox31;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox32;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox43;
        private GrapeCity.ActiveReports.SectionReportModel.Line line9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox44;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label6;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader groupHeader2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox33;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox34;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter groupFooter2;
        private GrapeCity.ActiveReports.SectionReportModel.Line line11;
        private GrapeCity.ActiveReports.SectionReportModel.Label label3;
        private GrapeCity.ActiveReports.SectionReportModel.Label label17;
        private GrapeCity.ActiveReports.SectionReportModel.Label label18;
        private GrapeCity.ActiveReports.SectionReportModel.Label label19;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox36;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox37;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox40;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox42;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox46;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox48;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox50;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox51;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox53;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox54;
        private GrapeCity.ActiveReports.SectionReportModel.Label label20;
        private GrapeCity.ActiveReports.SectionReportModel.Line line12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox55;
        private GrapeCity.ActiveReports.SectionReportModel.Label label4;
        private GrapeCity.ActiveReports.SectionReportModel.Label label5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox56;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox60;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox61;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox62;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox63;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox64;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox65;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox66;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox67;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox68;
    }
}
