﻿using System.Data;
using jp.co.fsi.common.report;
using jp.co.fsi.common.util;

namespace jp.co.fsi.kb.kbdr1011
{
    /// <summary>
    /// KBDR1011R の帳票
    /// </summary>
    public partial class KBDR1011R : BaseReport
    {

        public KBDR1011R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }

        private void detail_Format(object sender, System.EventArgs e)
        {
            if (Util.ToInt(this.nowRouNo.Text) % 48 == 0 || this.nowRouNo.Text == this.totalRowNo.Text)
            {
                this.detailLine.Visible = false;
                this.lastLine.Visible = true;
            }
            else
            {
                this.detailLine.Visible = true;
                this.lastLine.Visible = false;
            }
        }

        private void Preview_(object sender, System.EventArgs e)
        {

        }
    }
}
