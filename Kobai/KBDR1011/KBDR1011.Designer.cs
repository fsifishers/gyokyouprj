﻿namespace jp.co.fsi.kb.kbdr1011
{
    partial class KBDR1011
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbxDate = new System.Windows.Forms.GroupBox();
            this.lblDateDay = new System.Windows.Forms.Label();
            this.lblDateMonth = new System.Windows.Forms.Label();
            this.labelDateYear = new System.Windows.Forms.Label();
            this.txtDateDay = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtDateYear = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtDateMonth = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblDateGengo = new System.Windows.Forms.Label();
            this.lblDate = new System.Windows.Forms.Label();
            this.gbxTantoCd = new System.Windows.Forms.GroupBox();
            this.lblTantoCdBet = new System.Windows.Forms.Label();
            this.lblTantoCdFr = new System.Windows.Forms.Label();
            this.lblTantoCdTo = new System.Windows.Forms.Label();
            this.txtTantoCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtTantoCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.gbxFunanushiCd = new System.Windows.Forms.GroupBox();
            this.txtFunanushiCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblFunanushiCdFr = new System.Windows.Forms.Label();
            this.lblSenshuCdBet = new System.Windows.Forms.Label();
            this.txtFunanushiCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblFunanushiCdTo = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMizuageShishoNm = new System.Windows.Forms.Label();
            this.lblMizuageShisho = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.gbxDate.SuspendLayout();
            this.gbxTantoCd.SuspendLayout();
            this.gbxFunanushiCd.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Text = "";
            // 
            // btnF6
            // 
            this.btnF6.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.PreviewClose_KeyDown);
            // 
            // pnlDebug
            // 
            this.pnlDebug.Size = new System.Drawing.Size(847, 100);
            // 
            // gbxDate
            // 
            this.gbxDate.Controls.Add(this.lblDateDay);
            this.gbxDate.Controls.Add(this.lblDateMonth);
            this.gbxDate.Controls.Add(this.labelDateYear);
            this.gbxDate.Controls.Add(this.txtDateDay);
            this.gbxDate.Controls.Add(this.txtDateYear);
            this.gbxDate.Controls.Add(this.txtDateMonth);
            this.gbxDate.Controls.Add(this.lblDateGengo);
            this.gbxDate.Controls.Add(this.lblDate);
            this.gbxDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxDate.ForeColor = System.Drawing.SystemColors.ControlText;
            this.gbxDate.Location = new System.Drawing.Point(18, 52);
            this.gbxDate.Name = "gbxDate";
            this.gbxDate.Size = new System.Drawing.Size(247, 71);
            this.gbxDate.TabIndex = 1;
            this.gbxDate.TabStop = false;
            this.gbxDate.Text = "伝票日付";
            // 
            // lblDateDay
            // 
            this.lblDateDay.BackColor = System.Drawing.Color.Silver;
            this.lblDateDay.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateDay.Location = new System.Drawing.Point(201, 29);
            this.lblDateDay.Name = "lblDateDay";
            this.lblDateDay.Size = new System.Drawing.Size(20, 18);
            this.lblDateDay.TabIndex = 7;
            this.lblDateDay.Text = "日";
            this.lblDateDay.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateMonth
            // 
            this.lblDateMonth.BackColor = System.Drawing.Color.Silver;
            this.lblDateMonth.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateMonth.Location = new System.Drawing.Point(151, 29);
            this.lblDateMonth.Name = "lblDateMonth";
            this.lblDateMonth.Size = new System.Drawing.Size(15, 19);
            this.lblDateMonth.TabIndex = 5;
            this.lblDateMonth.Text = "月";
            this.lblDateMonth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelDateYear
            // 
            this.labelDateYear.BackColor = System.Drawing.Color.Silver;
            this.labelDateYear.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.labelDateYear.Location = new System.Drawing.Point(100, 29);
            this.labelDateYear.Name = "labelDateYear";
            this.labelDateYear.Size = new System.Drawing.Size(14, 17);
            this.labelDateYear.TabIndex = 3;
            this.labelDateYear.Text = "年";
            this.labelDateYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDateDay
            // 
            this.txtDateDay.AutoSizeFromLength = false;
            this.txtDateDay.DisplayLength = null;
            this.txtDateDay.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateDay.Location = new System.Drawing.Point(169, 28);
            this.txtDateDay.MaxLength = 2;
            this.txtDateDay.Name = "txtDateDay";
            this.txtDateDay.Size = new System.Drawing.Size(30, 20);
            this.txtDateDay.TabIndex = 6;
            this.txtDateDay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateDay.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateDay_Validating);
            // 
            // txtDateYear
            // 
            this.txtDateYear.AutoSizeFromLength = false;
            this.txtDateYear.DisplayLength = null;
            this.txtDateYear.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateYear.Location = new System.Drawing.Point(69, 28);
            this.txtDateYear.MaxLength = 2;
            this.txtDateYear.Name = "txtDateYear";
            this.txtDateYear.Size = new System.Drawing.Size(30, 20);
            this.txtDateYear.TabIndex = 2;
            this.txtDateYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateYear.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateYear_Validating);
            // 
            // txtDateMonth
            // 
            this.txtDateMonth.AutoSizeFromLength = false;
            this.txtDateMonth.DisplayLength = null;
            this.txtDateMonth.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateMonth.Location = new System.Drawing.Point(119, 28);
            this.txtDateMonth.MaxLength = 2;
            this.txtDateMonth.Name = "txtDateMonth";
            this.txtDateMonth.Size = new System.Drawing.Size(30, 20);
            this.txtDateMonth.TabIndex = 4;
            this.txtDateMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateMonth.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateMonth_Validating);
            // 
            // lblDateGengo
            // 
            this.lblDateGengo.BackColor = System.Drawing.Color.Silver;
            this.lblDateGengo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDateGengo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateGengo.Location = new System.Drawing.Point(26, 28);
            this.lblDateGengo.Name = "lblDateGengo";
            this.lblDateGengo.Size = new System.Drawing.Size(41, 21);
            this.lblDateGengo.TabIndex = 1;
            this.lblDateGengo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDate
            // 
            this.lblDate.BackColor = System.Drawing.Color.Silver;
            this.lblDate.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDate.Location = new System.Drawing.Point(24, 25);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(199, 28);
            this.lblDate.TabIndex = 1;
            // 
            // gbxTantoCd
            // 
            this.gbxTantoCd.Controls.Add(this.lblTantoCdBet);
            this.gbxTantoCd.Controls.Add(this.lblTantoCdFr);
            this.gbxTantoCd.Controls.Add(this.lblTantoCdTo);
            this.gbxTantoCd.Controls.Add(this.txtTantoCdTo);
            this.gbxTantoCd.Controls.Add(this.txtTantoCdFr);
            this.gbxTantoCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxTantoCd.ForeColor = System.Drawing.SystemColors.ControlText;
            this.gbxTantoCd.Location = new System.Drawing.Point(18, 129);
            this.gbxTantoCd.Name = "gbxTantoCd";
            this.gbxTantoCd.Size = new System.Drawing.Size(606, 71);
            this.gbxTantoCd.TabIndex = 2;
            this.gbxTantoCd.TabStop = false;
            this.gbxTantoCd.Text = "担当者CD範囲";
            // 
            // lblTantoCdBet
            // 
            this.lblTantoCdBet.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTantoCdBet.Location = new System.Drawing.Point(293, 28);
            this.lblTantoCdBet.Name = "lblTantoCdBet";
            this.lblTantoCdBet.Size = new System.Drawing.Size(17, 20);
            this.lblTantoCdBet.TabIndex = 2;
            this.lblTantoCdBet.Text = "～";
            this.lblTantoCdBet.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblTantoCdFr
            // 
            this.lblTantoCdFr.BackColor = System.Drawing.Color.Silver;
            this.lblTantoCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTantoCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTantoCdFr.Location = new System.Drawing.Point(83, 28);
            this.lblTantoCdFr.Name = "lblTantoCdFr";
            this.lblTantoCdFr.Size = new System.Drawing.Size(204, 20);
            this.lblTantoCdFr.TabIndex = 1;
            this.lblTantoCdFr.Text = "先　頭";
            this.lblTantoCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTantoCdTo
            // 
            this.lblTantoCdTo.BackColor = System.Drawing.Color.Silver;
            this.lblTantoCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTantoCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTantoCdTo.Location = new System.Drawing.Point(372, 28);
            this.lblTantoCdTo.Name = "lblTantoCdTo";
            this.lblTantoCdTo.Size = new System.Drawing.Size(204, 20);
            this.lblTantoCdTo.TabIndex = 4;
            this.lblTantoCdTo.Text = "最　後";
            this.lblTantoCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTantoCdTo
            // 
            this.txtTantoCdTo.AutoSizeFromLength = false;
            this.txtTantoCdTo.DisplayLength = null;
            this.txtTantoCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTantoCdTo.Location = new System.Drawing.Point(316, 28);
            this.txtTantoCdTo.MaxLength = 4;
            this.txtTantoCdTo.Name = "txtTantoCdTo";
            this.txtTantoCdTo.Size = new System.Drawing.Size(50, 20);
            this.txtTantoCdTo.TabIndex = 3;
            this.txtTantoCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTantoCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtTantoCdTo_Validating);
            // 
            // txtTantoCdFr
            // 
            this.txtTantoCdFr.AutoSizeFromLength = false;
            this.txtTantoCdFr.DisplayLength = null;
            this.txtTantoCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTantoCdFr.Location = new System.Drawing.Point(27, 28);
            this.txtTantoCdFr.MaxLength = 4;
            this.txtTantoCdFr.Name = "txtTantoCdFr";
            this.txtTantoCdFr.Size = new System.Drawing.Size(50, 20);
            this.txtTantoCdFr.TabIndex = 0;
            this.txtTantoCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTantoCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtTantoCdFr_Validating);
            // 
            // gbxFunanushiCd
            // 
            this.gbxFunanushiCd.Controls.Add(this.txtFunanushiCdFr);
            this.gbxFunanushiCd.Controls.Add(this.lblFunanushiCdFr);
            this.gbxFunanushiCd.Controls.Add(this.lblSenshuCdBet);
            this.gbxFunanushiCd.Controls.Add(this.txtFunanushiCdTo);
            this.gbxFunanushiCd.Controls.Add(this.lblFunanushiCdTo);
            this.gbxFunanushiCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxFunanushiCd.ForeColor = System.Drawing.SystemColors.ControlText;
            this.gbxFunanushiCd.Location = new System.Drawing.Point(18, 206);
            this.gbxFunanushiCd.Name = "gbxFunanushiCd";
            this.gbxFunanushiCd.Size = new System.Drawing.Size(606, 73);
            this.gbxFunanushiCd.TabIndex = 3;
            this.gbxFunanushiCd.TabStop = false;
            this.gbxFunanushiCd.Text = "船主CD範囲";
            // 
            // txtFunanushiCdFr
            // 
            this.txtFunanushiCdFr.AutoSizeFromLength = false;
            this.txtFunanushiCdFr.DisplayLength = null;
            this.txtFunanushiCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFunanushiCdFr.Location = new System.Drawing.Point(27, 29);
            this.txtFunanushiCdFr.MaxLength = 4;
            this.txtFunanushiCdFr.Name = "txtFunanushiCdFr";
            this.txtFunanushiCdFr.Size = new System.Drawing.Size(50, 20);
            this.txtFunanushiCdFr.TabIndex = 0;
            this.txtFunanushiCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFunanushiCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtFunanushiCdFr_Validating);
            // 
            // lblFunanushiCdFr
            // 
            this.lblFunanushiCdFr.BackColor = System.Drawing.Color.Silver;
            this.lblFunanushiCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFunanushiCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFunanushiCdFr.Location = new System.Drawing.Point(83, 29);
            this.lblFunanushiCdFr.Name = "lblFunanushiCdFr";
            this.lblFunanushiCdFr.Size = new System.Drawing.Size(204, 20);
            this.lblFunanushiCdFr.TabIndex = 1;
            this.lblFunanushiCdFr.Text = "先　頭";
            this.lblFunanushiCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSenshuCdBet
            // 
            this.lblSenshuCdBet.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblSenshuCdBet.Location = new System.Drawing.Point(294, 29);
            this.lblSenshuCdBet.Name = "lblSenshuCdBet";
            this.lblSenshuCdBet.Size = new System.Drawing.Size(17, 20);
            this.lblSenshuCdBet.TabIndex = 2;
            this.lblSenshuCdBet.Text = "～";
            this.lblSenshuCdBet.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtFunanushiCdTo
            // 
            this.txtFunanushiCdTo.AutoSizeFromLength = false;
            this.txtFunanushiCdTo.DisplayLength = null;
            this.txtFunanushiCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFunanushiCdTo.Location = new System.Drawing.Point(317, 29);
            this.txtFunanushiCdTo.MaxLength = 4;
            this.txtFunanushiCdTo.Name = "txtFunanushiCdTo";
            this.txtFunanushiCdTo.Size = new System.Drawing.Size(50, 20);
            this.txtFunanushiCdTo.TabIndex = 3;
            this.txtFunanushiCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFunanushiCdTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtFunanushiCdTo_KeyDown);
            this.txtFunanushiCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtFunanushiCdTo_Validating);
            // 
            // lblFunanushiCdTo
            // 
            this.lblFunanushiCdTo.BackColor = System.Drawing.Color.Silver;
            this.lblFunanushiCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFunanushiCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFunanushiCdTo.Location = new System.Drawing.Point(373, 29);
            this.lblFunanushiCdTo.Name = "lblFunanushiCdTo";
            this.lblFunanushiCdTo.Size = new System.Drawing.Size(204, 20);
            this.lblFunanushiCdTo.TabIndex = 4;
            this.lblFunanushiCdTo.Text = "最　後";
            this.lblFunanushiCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtMizuageShishoCd);
            this.groupBox1.Controls.Add(this.lblMizuageShishoNm);
            this.groupBox1.Controls.Add(this.lblMizuageShisho);
            this.groupBox1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBox1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox1.Location = new System.Drawing.Point(282, 52);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(342, 71);
            this.groupBox1.TabIndex = 905;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "支所";
            // 
            // txtMizuageShishoCd
            // 
            this.txtMizuageShishoCd.AutoSizeFromLength = true;
            this.txtMizuageShishoCd.DisplayLength = null;
            this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtMizuageShishoCd.Location = new System.Drawing.Point(68, 28);
            this.txtMizuageShishoCd.MaxLength = 4;
            this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
            this.txtMizuageShishoCd.Size = new System.Drawing.Size(34, 20);
            this.txtMizuageShishoCd.TabIndex = 1;
            this.txtMizuageShishoCd.TabStop = false;
            this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
            // 
            // lblMizuageShishoNm
            // 
            this.lblMizuageShishoNm.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMizuageShishoNm.Location = new System.Drawing.Point(103, 29);
            this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
            this.lblMizuageShishoNm.Size = new System.Drawing.Size(212, 20);
            this.lblMizuageShishoNm.TabIndex = 907;
            this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMizuageShisho
            // 
            this.lblMizuageShisho.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShisho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShisho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblMizuageShisho.Location = new System.Drawing.Point(26, 26);
            this.lblMizuageShisho.Name = "lblMizuageShisho";
            this.lblMizuageShisho.Size = new System.Drawing.Size(293, 25);
            this.lblMizuageShisho.TabIndex = 906;
            this.lblMizuageShisho.Text = "支所";
            this.lblMizuageShisho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // KBDR1011
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 638);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.gbxDate);
            this.Controls.Add(this.gbxTantoCd);
            this.Controls.Add(this.gbxFunanushiCd);
            this.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "KBDR1011";
            this.Text = "";
            this.Controls.SetChildIndex(this.gbxFunanushiCd, 0);
            this.Controls.SetChildIndex(this.gbxTantoCd, 0);
            this.Controls.SetChildIndex(this.gbxDate, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.pnlDebug.ResumeLayout(false);
            this.gbxDate.ResumeLayout(false);
            this.gbxDate.PerformLayout();
            this.gbxTantoCd.ResumeLayout(false);
            this.gbxTantoCd.PerformLayout();
            this.gbxFunanushiCd.ResumeLayout(false);
            this.gbxFunanushiCd.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbxDate;
        private jp.co.fsi.common.controls.FsiTextBox txtDateYear;
        private System.Windows.Forms.Label lblDateGengo;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.Label lblDateDay;
        private System.Windows.Forms.Label lblDateMonth;
        private System.Windows.Forms.Label labelDateYear;
        private jp.co.fsi.common.controls.FsiTextBox txtDateDay;
        private jp.co.fsi.common.controls.FsiTextBox txtDateMonth;
        private System.Windows.Forms.GroupBox gbxTantoCd;
        private System.Windows.Forms.Label lblTantoCdFr;
        private jp.co.fsi.common.controls.FsiTextBox txtTantoCdFr;
        private System.Windows.Forms.Label lblTantoCdBet;
        private System.Windows.Forms.Label lblTantoCdTo;
        private jp.co.fsi.common.controls.FsiTextBox txtTantoCdTo;
        private System.Windows.Forms.GroupBox gbxFunanushiCd;
        private jp.co.fsi.common.controls.FsiTextBox txtFunanushiCdFr;
        private System.Windows.Forms.Label lblFunanushiCdFr;
        private System.Windows.Forms.Label lblSenshuCdBet;
        private jp.co.fsi.common.controls.FsiTextBox txtFunanushiCdTo;
        private System.Windows.Forms.Label lblFunanushiCdTo;
        private System.Windows.Forms.GroupBox groupBox1;
        private common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private System.Windows.Forms.Label lblMizuageShisho;
    }
}