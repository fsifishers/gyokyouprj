﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Reflection;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.kob.kobc9071
{
    /// <summary>
    /// 仕入先の登録(KOBC9012)
    /// </summary>
    public partial class KOBC9072 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// モード(新規)
        /// </summary>
        private const string MODE_NEW = "1";

        /// <summary>
        /// モード(編集)
        /// </summary>
        private const string MODE_EDIT = "2";
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KOBC9072()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="par1">引数1</param>
        public KOBC9072(string par1) : base(par1)
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 引数：Par1／モード(1:新規、2:変更)、InData：仕入先コード
            if (MODE_NEW.Equals(this.Par1))
            {
                // 新規モードの初期表示
                InitDispOnNew();
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // 編集モードの初期表示
                InitDispOnEdit();
            }
            else
            {
                // 不正な起動として閉じる
                Msg.Error("不正な起動です。終了します。");
                this.Close();
            }
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // ファンクションキー押下の可／不可を制御しない
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF6();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF6()
        {
            // 確認メッセージを表示
            string msg = (MODE_NEW.Equals(this.Par1) ? "登録" : "更新") + "しますか？";
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            // 入力値をバインドパラメータとしてセットする
            ArrayList alParamsCmTori = SetCmToriParams();

            try
            {
                this.Dba.BeginTransaction();

                if (MODE_NEW.Equals(this.Par1))
                {
                    if (IsCodeExists())
                    {
                        Msg.Error("締日区分が登録済みです。");
                        return;
                    }

                    // データ登録
                    // TB_S締日区分テーブル
                    this.Dba.Insert("TB_HN_S_SHIMEBI_KUBUN", (DbParamCollection)alParamsCmTori[0]);
                }
                else if (MODE_EDIT.Equals(this.Par1))
                {
                    if (!IsCodeExists())
                    {
                        Msg.Error("締日区分が登録されていません。");
                        return;
                    }

                    // データ更新
                    // TB_S締日区分テーブル
                    this.Dba.Update("TB_HN_S_SHIMEBI_KUBUN",
                        (DbParamCollection)alParamsCmTori[1],
                        "SHIMEBI_KUBUN = @SHIMEBI_KBN",
                        (DbParamCollection)alParamsCmTori[0]);
                }

                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 仕入先コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShiireCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShimebiKbn())
            {
                e.Cancel = true;
                this.txtShimebiKbn.SelectAll();
            }
        }

        /// <summary>
        /// 仕入先名の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShiireNm_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShimebiKbnNm())
            {
                e.Cancel = true;
                this.txtShimebiKbnNm.SelectAll();
            }
        }


        #endregion

        #region privateメソッド
        /// <summary>
        /// 新規モードの初期表示
        /// </summary>
        private void InitDispOnNew()
        {
            // 初期値、入力制御を実装
            
            // 仕入先コードの初期値を取得
            // 仕入先の中でのMAX+1を初期表示する
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            DataTable dtMaxShrSk =
                this.Dba.GetDataTableByConditionWithParams("MAX(SHIMEBI_KUBUN) AS MAX_CD",
                    "TB_HN_S_SHIMEBI_KUBUN", "", dpc);
            if (dtMaxShrSk.Rows.Count > 0 && !ValChk.IsEmpty(dtMaxShrSk.Rows[0]["MAX_CD"]))
            {
                this.txtShimebiKbn.Text = Util.ToString(Util.ToInt(dtMaxShrSk.Rows[0]["MAX_CD"]) + 1);
            }
            else
            {
                this.txtShimebiKbn.Text = "1";
            }

            // 編集時の締日区分は入力不可
            // this.lblShimebiKbn.Enabled = false;
            // this.txtShimebiKbn.Enabled = false;

            // 締日区分名に初期フォーカス
            this.ActiveControl = this.txtShimebiKbnNm;
            this.txtShimebiKbnNm.Focus();
        }

        /// <summary>
        /// 編集モードの初期表示
        /// </summary>
        private void InitDispOnEdit()
        {
            // 現在DBに登録されている値、入力制御を実装
            StringBuilder cols = new StringBuilder();
            cols.Append("A.SHIMEBI_KUBUN");
            cols.Append(" ,A.SHIMEBI_KUBUN_NM");

            StringBuilder from = new StringBuilder();
            from.Append("TB_HN_S_SHIMEBI_KUBUN AS A");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@SHIMEBI_KBN", SqlDbType.Decimal, 2, Util.ToString(this.InData));

            DataTable dtDispData =
                this.Dba.GetDataTableByConditionWithParams(
                    Util.ToString(cols), Util.ToString(from),
                    "A.SHIMEBI_KUBUN = @SHIMEBI_KBN",
                    dpc);

            if (dtDispData.Rows.Count == 0)
            {
                Msg.Error("不正な起動です。終了します。");
                this.Close();
            }

            // 取得した内容を表示
            DataRow drDispData = dtDispData.Rows[0];
            this.txtShimebiKbn.Text = Util.ToString(drDispData["SHIMEBI_KUBUN"]);
            this.txtShimebiKbnNm.Text = Util.ToString(drDispData["SHIMEBI_KUBUN_NM"]);

            // 編集時の締日区分は入力不可
            this.lblShimebiKbn.Enabled = false;
            this.txtShimebiKbn.Enabled = false;

            if (IsCodeExists())
            {
                // 使用されているので削除不可
                this.btnF3.Enabled = false;
            }
            else
            {
                // 使用されていないので削除OK
                this.btnF3.Enabled = true;
            }
        }

        /// <summary>
        /// 締日区分の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShimebiKbn()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtShimebiKbn.Text))
            {
                Msg.Error("締日区分に数字以外の文字があります。");
                return false;
            }

            // 1未満はエラー
            if (int.Parse(this.txtShimebiKbn.Text) < 1)
            {
                Msg.Error("1未満の値が入力されました。");
                return false;
            }

            // 既に存在するコードを入力した場合はエラーとする
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@SHIMEBI_KBN", SqlDbType.Decimal, 2, this.txtShimebiKbn.Text);
            StringBuilder where = new StringBuilder("SHIMEBI_KUBUN = @SHIMEBI_KBN");
            DataTable dtToriSk =
                this.Dba.GetDataTableByConditionWithParams("SHIMEBI_KUBUN",
                    "TB_HN_S_SHIMEBI_KUBUN", Util.ToString(where), dpc);
            if (dtToriSk.Rows.Count > 0)
            {
                Msg.Error("既に存在する仕入区分と重複しています。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 締日区分名入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShimebiKbnNm()
        {
            // 30文字を超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtShimebiKbnNm.Text, this.txtShimebiKbnNm.MaxLength))
            {
                Msg.Error("締日区分名が長過ぎます。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            if (MODE_NEW.Equals(this.Par1))
            {
                // 仕入先コードのチェック
                if (!IsValidShimebiKbn())
                {
                    this.txtShimebiKbn.Focus();
                    return false;
                }
            }

            // 仕入先名のチェック
            if (!IsValidShimebiKbnNm())
            {
                this.txtShimebiKbnNm.Focus();
                return false;
            }

            return true;
        }

        /// <summary>
        /// TB_CM_TORIHIKISAKIに更新するためのパラメータ設定をします。
        /// </summary>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// 更新処理：DbParamCollection*2(Where句,Set句)
        /// </returns>
        private ArrayList SetCmToriParams()
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            if (MODE_NEW.Equals(this.Par1))
            {
                // 締日区分とを更新パラメータに設定
                updParam.SetParam("@SHIMEBI_KUBUN", SqlDbType.Decimal, 2, this.txtShimebiKbn.Text);
                // 登録日
                updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWDATE");
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // 締日区分をWhere句のパラメータに設定
                DbParamCollection whereParam = new DbParamCollection();
                whereParam.SetParam("@SHIMEBI_KBN", SqlDbType.Decimal, 2, this.txtShimebiKbn.Text);
                alParams.Add(whereParam);
            }

            // 締日区分名
            updParam.SetParam("@SHIMEBI_KUBUN_NM", SqlDbType.VarChar, 30, this.txtShimebiKbnNm.Text);

            // 更新日
            updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWDATE");

            alParams.Add(updParam);

            return alParams;
        }

        /// <summary>
        /// 対象の締日区分がマスタ登録されているかどうかをチェックします。
        /// </summary>
        /// <returns>true:使用されている/false:使用されていない</returns>
        private bool IsCodeExists()
        {
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@SHIMEBI_KBN", SqlDbType.Decimal, 2, this.txtShimebiKbn.Text);
            DataTable dt_S_ShimebiKbun = this.Dba.GetDataTableByConditionWithParams("SHIMEBI_KUBUN", "TB_HN_S_SHIMEBI_KUBUN", "SHIMEBI_KUBUN = @SHIMEBI_KBN", dpc);

            if (dt_S_ShimebiKbun.Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion

    }
}
