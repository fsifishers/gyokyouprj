﻿using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.kob.kobc9071
{
    /// <summary>
    /// 締日気分の登録(KOBC9071)
    /// </summary>
    public partial class KOBC9071 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// モード(コード検索)
        /// </summary>
        private const string MODE_CD_SRC = "1";
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KOBC9071()
        {
            InitializeComponent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.InitForm();は呼び出さなくて構いません。
        /// また、このメソッド内の処理を外出しでこのクラス内にメソッド化するのは構いませんが、
        /// 原則、独自で起動時のイベント処理を実装することは禁じます。
        /// </remarks>
        protected override void InitForm()
        {
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                // Par1が"1"の場合、コード検索画面としての挙動をする
                // サイズを縮める
                this.Size = new Size(600, 500);
                // EscapeとF1のみ表示
                this.ShowFButton = true;
                this.btnEsc.Location = this.btnF1.Location;
                this.btnF1.Location = this.btnF2.Location;
                this.btnF2.Visible = false;
                this.btnF3.Visible = false;
                this.btnF4.Visible = false;
                this.btnF5.Visible = false;
                this.btnF6.Visible = false;
                this.btnF7.Visible = false;
                this.btnF8.Visible = false;
                this.btnF9.Visible = false;
                this.btnF10.Visible = false;
                this.btnF11.Visible = false;
                this.btnF12.Visible = false;
            }

            // まずデータが存在し得ない検索条件で検索をし、結果をバインドすることで、
            // 初期状態を作り出す
            SearchData(true);

        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                // Par1が"1"の場合、ダイアログとしての処理結果を返却する
                this.DialogResult = DialogResult.Cancel;
            }
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
        }

        /// <summary>
        /// F2キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF2();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF2()
        {
            // メンテ機能で立ち上げている場合のみ削除可能
            if (!ValChk.IsEmpty(this.Par1))
            {
                return;
            }

            // 締日区分未選択時は処理終了する
            string shimebiKbn = Util.ToString(this.dgvList.SelectedRows[0].Cells["区分"].Value);
            if (ValChk.IsEmpty(shimebiKbn))
            {
                return;
            }

            string shimebiKbnNM = Util.ToString(this.dgvList.SelectedRows[0].Cells["区分名"].Value);
            if (Msg.ConfYesNo(shimebiKbnNM + "を削除しますか？") == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 削除処理
            try
            {
                // データ削除
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@SHIMEBI_KBN", SqlDbType.Decimal, 2, shimebiKbn);
                this.Dba.Delete("TB_HN_S_SHIMEBI_KUBUN", "SHIMEBI_KUBUN = @SHIMEBI_KBN", dpc);

                // トランザクションをコミット
                this.Dba.Commit();

                // 再表示
                SearchData(true);

                Msg.Info(shimebiKbnNM + "を削除しました。");
            }
            catch (System.Exception e)
            {
                // ロールバック
                this.Dba.Rollback();

                Msg.Notice(shimebiKbnNM + "の削除が失敗しました。");

            }

        }

        /// <summary>
        /// F3キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF4();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF3()
        {
            // メンテ機能で立ち上げている場合のみ仕入先登録画面を立ち上げる
            if (ValChk.IsEmpty(this.Par1))
            {
                // 締日区分登録画面の起動
                EditShimebiKbn(string.Empty);
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF4();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF4()
        {
            // メンテ機能で立ち上げている場合のみ仕入先登録画面を立ち上げる
            if (ValChk.IsEmpty(this.Par1))
            {
                // 締日区分更新画面の起動
                EditShimebiKbn(Util.ToString(this.dgvList.SelectedRows[0].Cells["区分"].Value));
            }
        }

        #endregion

        #region イベント
        /// <summary>
        /// グリッドでのキーダウン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (MODE_CD_SRC.Equals(this.Par1))
                {
                    ReturnVal();
                }
                else
                {
                    EditShimebiKbn(Util.ToString(this.dgvList.SelectedRows[0].Cells["区分"].Value));
                    e.Handled = true;
                }
            }
        }

        /// <summary>
        /// グリッドのセルダブルクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                ReturnVal();
            }
            else
            {
                EditShimebiKbn(Util.ToString(this.dgvList.SelectedRows[0].Cells["区分"].Value));
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// データを検索する
        /// </summary>
        /// <param name="isInitial">初期処理であるかどうか</param>
        private void SearchData(bool isInitial)
        {
            // 仕入先マスタからデータを取得して表示
            DbParamCollection dpc = new DbParamCollection();
            string where = "";
            string cols = "HN.SHIMEBI_KUBUN AS 区分";
            cols += ", HN.SHIMEBI_KUBUN_NM AS 区分名";
            string from = "TB_HN_S_SHIMEBI_KUBUN AS HN";

            DataTable dtShiire =
                this.Dba.GetDataTableByConditionWithParams(cols, from,
                    where, "HN.SHIMEBI_KUBUN", dpc);

            // 初期処理以外の場合、該当データがなければエラーメッセージを表示
            if (dtShiire.Rows.Count == 0)
            {
                if (!isInitial)
                {
                    Msg.Info("該当データがありません。");
                }

                dtShiire.Rows.Add(dtShiire.NewRow());
            }

            this.dgvList.DataSource = dtShiire;

            // ユーザーによるソートを禁止させる
            foreach (DataGridViewColumn c in this.dgvList.Columns)
                c.SortMode = DataGridViewColumnSortMode.NotSortable;

            // フォントを設定する
            this.dgvList.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F, FontStyle.Bold);
            this.dgvList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.dgvList.DefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F);

            // 列幅を設定する
            this.dgvList.Columns[0].Width = 110;
            this.dgvList.Columns[1].Width = 400;
        }

        /// <summary>
        /// 区分を追加編集する
        /// </summary>
        /// <param name="code">区分(空：新規登録、以外：編集)</param>
        private void EditShimebiKbn(string code)
        {
            KOBC9072 frmKOBC9012;

            if (ValChk.IsEmpty(code))
            {
                // 新規登録モードで登録画面を起動
                frmKOBC9012 = new KOBC9072("1");
            }
            else
            {
                // 編集モードで登録画面を起動
                frmKOBC9012 = new KOBC9072("2");
                frmKOBC9012.InData = code;
            }

            DialogResult result = frmKOBC9012.ShowDialog(this);

            if (result == DialogResult.OK)
            {
                // データを再検索する
                SearchData(false);
                // 元々選択していたデータを選択
                for (int i = 0; i < this.dgvList.Rows.Count; i++)
                {
                    if (code.Equals(Util.ToString(this.dgvList.Rows[i].Cells["区分"].Value)))
                    {
                        this.dgvList.Rows[i].Selected = true;
                        break;
                    }
                }
                // Gridに再度フォーカスをセット
                this.ActiveControl = this.dgvList;
                this.dgvList.Focus();
            }
        }

        /// <summary>
        /// 呼び出し元に戻り値を返す
        /// </summary>
        private void ReturnVal()
        {
            this.OutData = new string[2] { 
                Util.ToString(this.dgvList.SelectedRows[0].Cells["区分"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["区分名"].Value)
            };
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        #endregion
    }
}
