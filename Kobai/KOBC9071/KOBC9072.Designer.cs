﻿namespace jp.co.fsi.kob.kobc9071
{
    partial class KOBC9072
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtShimebiKbn = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShimebiKbn = new System.Windows.Forms.Label();
            this.pnlMain = new jp.co.fsi.common.FsiPanel();
            this.txtShimebiKbnNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShimebiNm = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.pnlMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Text = "締日区分の登録";
            // 
            // btnF1
            // 
            this.btnF1.Enabled = false;
            this.btnF1.Text = "F1\r\n\r\n";
            // 
            // btnF2
            // 
            this.btnF2.Enabled = false;
            this.btnF2.Text = "F2\r\n\r\n";
            // 
            // btnF3
            // 
            this.btnF3.Enabled = false;
            this.btnF3.Text = "F3\r\n\r\n";
            // 
            // btnF4
            // 
            this.btnF4.Enabled = false;
            this.btnF4.Text = "F4\r\n\r\n";
            // 
            // btnF5
            // 
            this.btnF5.Enabled = false;
            this.btnF5.Text = "F5\r\n\r\n";
            // 
            // btnF7
            // 
            this.btnF7.Enabled = false;
            // 
            // btnF6
            // 
            this.btnF6.Text = "F6\r\n\r\n保存";
            // 
            // btnF8
            // 
            this.btnF8.Enabled = false;
            // 
            // btnF9
            // 
            this.btnF9.Enabled = false;
            // 
            // btnF12
            // 
            this.btnF12.Enabled = false;
            this.btnF12.Text = "F12\r\n\r\n";
            // 
            // btnF11
            // 
            this.btnF11.Enabled = false;
            // 
            // btnF10
            // 
            this.btnF10.Enabled = false;
            // 
            // pnlDebug
            // 
            this.pnlDebug.Size = new System.Drawing.Size(847, 100);
            // 
            // txtShimebiKbn
            // 
            this.txtShimebiKbn.AutoSizeFromLength = true;
            this.txtShimebiKbn.DisplayLength = null;
            this.txtShimebiKbn.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShimebiKbn.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtShimebiKbn.Location = new System.Drawing.Point(137, 48);
            this.txtShimebiKbn.MaxLength = 2;
            this.txtShimebiKbn.Name = "txtShimebiKbn";
            this.txtShimebiKbn.Size = new System.Drawing.Size(20, 20);
            this.txtShimebiKbn.TabIndex = 2;
            this.txtShimebiKbn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShimebiKbn.Validating += new System.ComponentModel.CancelEventHandler(this.txtShiireCd_Validating);
            // 
            // lblShimebiKbn
            // 
            this.lblShimebiKbn.BackColor = System.Drawing.Color.Silver;
            this.lblShimebiKbn.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShimebiKbn.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShimebiKbn.Location = new System.Drawing.Point(22, 48);
            this.lblShimebiKbn.Name = "lblShimebiKbn";
            this.lblShimebiKbn.Size = new System.Drawing.Size(115, 20);
            this.lblShimebiKbn.TabIndex = 1;
            this.lblShimebiKbn.Text = "締日区分";
            this.lblShimebiKbn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnlMain
            // 
            this.pnlMain.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlMain.Controls.Add(this.txtShimebiKbnNm);
            this.pnlMain.Controls.Add(this.lblShimebiNm);
            this.pnlMain.Location = new System.Drawing.Point(17, 70);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(794, 260);
            this.pnlMain.TabIndex = 3;
            // 
            // txtShimebiKbnNm
            // 
            this.txtShimebiKbnNm.AutoSizeFromLength = false;
            this.txtShimebiKbnNm.DisplayLength = null;
            this.txtShimebiKbnNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShimebiKbnNm.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtShimebiKbnNm.Location = new System.Drawing.Point(118, 3);
            this.txtShimebiKbnNm.MaxLength = 30;
            this.txtShimebiKbnNm.Name = "txtShimebiKbnNm";
            this.txtShimebiKbnNm.Size = new System.Drawing.Size(269, 20);
            this.txtShimebiKbnNm.TabIndex = 1;
            this.txtShimebiKbnNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtShiireNm_Validating);
            // 
            // lblShimebiNm
            // 
            this.lblShimebiNm.BackColor = System.Drawing.Color.Silver;
            this.lblShimebiNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShimebiNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShimebiNm.Location = new System.Drawing.Point(3, 3);
            this.lblShimebiNm.Name = "lblShimebiNm";
            this.lblShimebiNm.Size = new System.Drawing.Size(115, 20);
            this.lblShimebiNm.TabIndex = 0;
            this.lblShimebiNm.Text = "締日区分名";
            this.lblShimebiNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // KOBC9072
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 638);
            this.Controls.Add(this.pnlMain);
            this.Controls.Add(this.txtShimebiKbn);
            this.Controls.Add(this.lblShimebiKbn);
            this.Name = "KOBC9072";
            this.ShowFButton = true;
            this.Text = "締日区分の登録";
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblShimebiKbn, 0);
            this.Controls.SetChildIndex(this.txtShimebiKbn, 0);
            this.Controls.SetChildIndex(this.pnlMain, 0);
            this.pnlDebug.ResumeLayout(false);
            this.pnlMain.ResumeLayout(false);
            this.pnlMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private jp.co.fsi.common.controls.FsiTextBox txtShimebiKbn;
        private System.Windows.Forms.Label lblShimebiKbn;
        private jp.co.fsi.common.FsiPanel pnlMain;
        private jp.co.fsi.common.controls.FsiTextBox txtShimebiKbnNm;
        private System.Windows.Forms.Label lblShimebiNm;
    }
}