﻿namespace jp.co.fsi.kb.kbmr1021
{
    partial class KBMR1021
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbxOutput = new System.Windows.Forms.GroupBox();
            this.rdoOutput2 = new System.Windows.Forms.RadioButton();
            this.rdoOutput1 = new System.Windows.Forms.RadioButton();
            this.txtTanabanTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtTanabanFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblCodeBet3 = new System.Windows.Forms.Label();
            this.gbxPaper = new System.Windows.Forms.GroupBox();
            this.rdoPaper2 = new System.Windows.Forms.RadioButton();
            this.rdoPaper1 = new System.Windows.Forms.RadioButton();
            this.gbxZeroOutput = new System.Windows.Forms.GroupBox();
            this.rdoZeroOutput2 = new System.Windows.Forms.RadioButton();
            this.rdoZeroOutput1 = new System.Windows.Forms.RadioButton();
            this.gbxOutputRank = new System.Windows.Forms.GroupBox();
            this.rdoOutputRank3 = new System.Windows.Forms.RadioButton();
            this.rdoOutputRank2 = new System.Windows.Forms.RadioButton();
            this.rdoOutputRank1 = new System.Windows.Forms.RadioButton();
            this.gbxCondition = new System.Windows.Forms.GroupBox();
            this.lblTanabanNmTo = new System.Windows.Forms.Label();
            this.lblTanabanNmFr = new System.Windows.Forms.Label();
            this.lblTanabanCap = new System.Windows.Forms.Label();
            this.lblShohinNmTo = new System.Windows.Forms.Label();
            this.txtShohinCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblCodeBet2 = new System.Windows.Forms.Label();
            this.lblShohinNmFr = new System.Windows.Forms.Label();
            this.txtShohinCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShohinCdCap = new System.Windows.Forms.Label();
            this.lblShohinKbnTo = new System.Windows.Forms.Label();
            this.txtShohinKbnTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblCodeBet1 = new System.Windows.Forms.Label();
            this.lblShohinKbnFr = new System.Windows.Forms.Label();
            this.txtShohinKbnFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShohinKbnCap = new System.Windows.Forms.Label();
            this.lblDate = new System.Windows.Forms.Label();
            this.lblMonth = new System.Windows.Forms.Label();
            this.lblYear = new System.Windows.Forms.Label();
            this.txtDateYear = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblDateGengo = new System.Windows.Forms.Label();
            this.txtDateMonth = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtDateDay = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblDay = new System.Windows.Forms.Label();
            this.gbxTanaoroshiDate = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMizuageShishoNm = new System.Windows.Forms.Label();
            this.lblMizuageShisho = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.pnlDebug.SuspendLayout();
            this.gbxOutput.SuspendLayout();
            this.gbxPaper.SuspendLayout();
            this.gbxZeroOutput.SuspendLayout();
            this.gbxOutputRank.SuspendLayout();
            this.gbxCondition.SuspendLayout();
            this.gbxTanaoroshiDate.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Text = "棚卸表";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.pnlDebug.Location = new System.Drawing.Point(5, 531);
            this.pnlDebug.Size = new System.Drawing.Size(843, 100);
            // 
            // gbxOutput
            // 
            this.gbxOutput.Controls.Add(this.rdoOutput2);
            this.gbxOutput.Controls.Add(this.rdoOutput1);
            this.gbxOutput.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxOutput.Location = new System.Drawing.Point(12, 113);
            this.gbxOutput.Name = "gbxOutput";
            this.gbxOutput.Size = new System.Drawing.Size(200, 60);
            this.gbxOutput.TabIndex = 1;
            this.gbxOutput.TabStop = false;
            this.gbxOutput.Text = "出力帳票";
            // 
            // rdoOutput2
            // 
            this.rdoOutput2.AutoSize = true;
            this.rdoOutput2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoOutput2.Location = new System.Drawing.Point(104, 25);
            this.rdoOutput2.Name = "rdoOutput2";
            this.rdoOutput2.Size = new System.Drawing.Size(67, 17);
            this.rdoOutput2.TabIndex = 1;
            this.rdoOutput2.Text = "記入表";
            this.rdoOutput2.UseVisualStyleBackColor = true;
            // 
            // rdoOutput1
            // 
            this.rdoOutput1.AutoSize = true;
            this.rdoOutput1.Checked = true;
            this.rdoOutput1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoOutput1.Location = new System.Drawing.Point(24, 25);
            this.rdoOutput1.Name = "rdoOutput1";
            this.rdoOutput1.Size = new System.Drawing.Size(67, 17);
            this.rdoOutput1.TabIndex = 0;
            this.rdoOutput1.TabStop = true;
            this.rdoOutput1.Text = "棚卸表";
            this.rdoOutput1.UseVisualStyleBackColor = true;
            this.rdoOutput1.CheckedChanged += new System.EventHandler(this.rdoOutput_CheckedChanged);
            // 
            // txtTanabanTo
            // 
            this.txtTanabanTo.AutoSizeFromLength = true;
            this.txtTanabanTo.DisplayLength = null;
            this.txtTanabanTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTanabanTo.Location = new System.Drawing.Point(330, 112);
            this.txtTanabanTo.MaxLength = 6;
            this.txtTanabanTo.Name = "txtTanabanTo";
            this.txtTanabanTo.Size = new System.Drawing.Size(48, 20);
            this.txtTanabanTo.TabIndex = 16;
            this.txtTanabanTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTanabanTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtTanabanTo_KeyDown);
            this.txtTanabanTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtTanabanTo_Validating);
            // 
            // txtTanabanFr
            // 
            this.txtTanabanFr.AutoSizeFromLength = true;
            this.txtTanabanFr.DisplayLength = null;
            this.txtTanabanFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTanabanFr.Location = new System.Drawing.Point(107, 113);
            this.txtTanabanFr.MaxLength = 6;
            this.txtTanabanFr.Name = "txtTanabanFr";
            this.txtTanabanFr.Size = new System.Drawing.Size(48, 20);
            this.txtTanabanFr.TabIndex = 13;
            this.txtTanabanFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTanabanFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtTanabanFr_Validating);
            // 
            // lblCodeBet3
            // 
            this.lblCodeBet3.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblCodeBet3.Location = new System.Drawing.Point(305, 112);
            this.lblCodeBet3.Name = "lblCodeBet3";
            this.lblCodeBet3.Size = new System.Drawing.Size(18, 20);
            this.lblCodeBet3.TabIndex = 15;
            this.lblCodeBet3.Text = "～";
            this.lblCodeBet3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // gbxPaper
            // 
            this.gbxPaper.Controls.Add(this.rdoPaper2);
            this.gbxPaper.Controls.Add(this.rdoPaper1);
            this.gbxPaper.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxPaper.Location = new System.Drawing.Point(219, 113);
            this.gbxPaper.Name = "gbxPaper";
            this.gbxPaper.Size = new System.Drawing.Size(175, 60);
            this.gbxPaper.TabIndex = 2;
            this.gbxPaper.TabStop = false;
            this.gbxPaper.Text = "用紙";
            // 
            // rdoPaper2
            // 
            this.rdoPaper2.AutoSize = true;
            this.rdoPaper2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoPaper2.Location = new System.Drawing.Point(98, 25);
            this.rdoPaper2.Name = "rdoPaper2";
            this.rdoPaper2.Size = new System.Drawing.Size(53, 17);
            this.rdoPaper2.TabIndex = 1;
            this.rdoPaper2.Text = "よこ";
            this.rdoPaper2.UseVisualStyleBackColor = true;
            // 
            // rdoPaper1
            // 
            this.rdoPaper1.AutoSize = true;
            this.rdoPaper1.Checked = true;
            this.rdoPaper1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoPaper1.Location = new System.Drawing.Point(24, 25);
            this.rdoPaper1.Name = "rdoPaper1";
            this.rdoPaper1.Size = new System.Drawing.Size(53, 17);
            this.rdoPaper1.TabIndex = 0;
            this.rdoPaper1.TabStop = true;
            this.rdoPaper1.Text = "たて";
            this.rdoPaper1.UseVisualStyleBackColor = true;
            // 
            // gbxZeroOutput
            // 
            this.gbxZeroOutput.Controls.Add(this.rdoZeroOutput2);
            this.gbxZeroOutput.Controls.Add(this.rdoZeroOutput1);
            this.gbxZeroOutput.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxZeroOutput.Location = new System.Drawing.Point(401, 113);
            this.gbxZeroOutput.Name = "gbxZeroOutput";
            this.gbxZeroOutput.Size = new System.Drawing.Size(190, 60);
            this.gbxZeroOutput.TabIndex = 3;
            this.gbxZeroOutput.TabStop = false;
            this.gbxZeroOutput.Text = "差ｾﾞﾛ出力";
            // 
            // rdoZeroOutput2
            // 
            this.rdoZeroOutput2.AutoSize = true;
            this.rdoZeroOutput2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoZeroOutput2.Location = new System.Drawing.Point(98, 25);
            this.rdoZeroOutput2.Name = "rdoZeroOutput2";
            this.rdoZeroOutput2.Size = new System.Drawing.Size(67, 17);
            this.rdoZeroOutput2.TabIndex = 1;
            this.rdoZeroOutput2.Text = "しない";
            this.rdoZeroOutput2.UseVisualStyleBackColor = true;
            // 
            // rdoZeroOutput1
            // 
            this.rdoZeroOutput1.AutoSize = true;
            this.rdoZeroOutput1.Checked = true;
            this.rdoZeroOutput1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoZeroOutput1.Location = new System.Drawing.Point(24, 25);
            this.rdoZeroOutput1.Name = "rdoZeroOutput1";
            this.rdoZeroOutput1.Size = new System.Drawing.Size(53, 17);
            this.rdoZeroOutput1.TabIndex = 0;
            this.rdoZeroOutput1.TabStop = true;
            this.rdoZeroOutput1.Text = "する";
            this.rdoZeroOutput1.UseVisualStyleBackColor = true;
            // 
            // gbxOutputRank
            // 
            this.gbxOutputRank.Controls.Add(this.rdoOutputRank3);
            this.gbxOutputRank.Controls.Add(this.rdoOutputRank2);
            this.gbxOutputRank.Controls.Add(this.rdoOutputRank1);
            this.gbxOutputRank.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxOutputRank.Location = new System.Drawing.Point(259, 182);
            this.gbxOutputRank.Name = "gbxOutputRank";
            this.gbxOutputRank.Size = new System.Drawing.Size(331, 60);
            this.gbxOutputRank.TabIndex = 5;
            this.gbxOutputRank.TabStop = false;
            this.gbxOutputRank.Text = "出力順位";
            // 
            // rdoOutputRank3
            // 
            this.rdoOutputRank3.AutoSize = true;
            this.rdoOutputRank3.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoOutputRank3.Location = new System.Drawing.Point(226, 25);
            this.rdoOutputRank3.Name = "rdoOutputRank3";
            this.rdoOutputRank3.Size = new System.Drawing.Size(81, 17);
            this.rdoOutputRank3.TabIndex = 2;
            this.rdoOutputRank3.Text = "商品合計";
            this.rdoOutputRank3.UseVisualStyleBackColor = true;
            // 
            // rdoOutputRank2
            // 
            this.rdoOutputRank2.AutoSize = true;
            this.rdoOutputRank2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoOutputRank2.Location = new System.Drawing.Point(125, 25);
            this.rdoOutputRank2.Name = "rdoOutputRank2";
            this.rdoOutputRank2.Size = new System.Drawing.Size(95, 17);
            this.rdoOutputRank2.TabIndex = 1;
            this.rdoOutputRank2.Text = "倉庫商品順";
            this.rdoOutputRank2.UseVisualStyleBackColor = true;
            // 
            // rdoOutputRank1
            // 
            this.rdoOutputRank1.AutoSize = true;
            this.rdoOutputRank1.Checked = true;
            this.rdoOutputRank1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoOutputRank1.Location = new System.Drawing.Point(24, 25);
            this.rdoOutputRank1.Name = "rdoOutputRank1";
            this.rdoOutputRank1.Size = new System.Drawing.Size(95, 17);
            this.rdoOutputRank1.TabIndex = 0;
            this.rdoOutputRank1.TabStop = true;
            this.rdoOutputRank1.Text = "商品倉庫順";
            this.rdoOutputRank1.UseVisualStyleBackColor = true;
            // 
            // gbxCondition
            // 
            this.gbxCondition.Controls.Add(this.lblTanabanNmTo);
            this.gbxCondition.Controls.Add(this.lblTanabanNmFr);
            this.gbxCondition.Controls.Add(this.lblTanabanCap);
            this.gbxCondition.Controls.Add(this.lblShohinNmTo);
            this.gbxCondition.Controls.Add(this.txtShohinCdTo);
            this.gbxCondition.Controls.Add(this.lblCodeBet2);
            this.gbxCondition.Controls.Add(this.lblShohinNmFr);
            this.gbxCondition.Controls.Add(this.txtShohinCdFr);
            this.gbxCondition.Controls.Add(this.lblShohinCdCap);
            this.gbxCondition.Controls.Add(this.lblShohinKbnTo);
            this.gbxCondition.Controls.Add(this.txtShohinKbnTo);
            this.gbxCondition.Controls.Add(this.lblCodeBet1);
            this.gbxCondition.Controls.Add(this.lblShohinKbnFr);
            this.gbxCondition.Controls.Add(this.txtShohinKbnFr);
            this.gbxCondition.Controls.Add(this.lblShohinKbnCap);
            this.gbxCondition.Controls.Add(this.txtTanabanTo);
            this.gbxCondition.Controls.Add(this.txtTanabanFr);
            this.gbxCondition.Controls.Add(this.lblCodeBet3);
            this.gbxCondition.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxCondition.Location = new System.Drawing.Point(12, 251);
            this.gbxCondition.Name = "gbxCondition";
            this.gbxCondition.Size = new System.Drawing.Size(579, 149);
            this.gbxCondition.TabIndex = 6;
            this.gbxCondition.TabStop = false;
            this.gbxCondition.Text = "抽出範囲";
            // 
            // lblTanabanNmTo
            // 
            this.lblTanabanNmTo.BackColor = System.Drawing.Color.Silver;
            this.lblTanabanNmTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTanabanNmTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTanabanNmTo.Location = new System.Drawing.Point(379, 112);
            this.lblTanabanNmTo.Name = "lblTanabanNmTo";
            this.lblTanabanNmTo.Size = new System.Drawing.Size(158, 20);
            this.lblTanabanNmTo.TabIndex = 17;
            this.lblTanabanNmTo.Text = "最　後";
            this.lblTanabanNmTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTanabanNmFr
            // 
            this.lblTanabanNmFr.BackColor = System.Drawing.Color.Silver;
            this.lblTanabanNmFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTanabanNmFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTanabanNmFr.Location = new System.Drawing.Point(156, 113);
            this.lblTanabanNmFr.Name = "lblTanabanNmFr";
            this.lblTanabanNmFr.Size = new System.Drawing.Size(146, 20);
            this.lblTanabanNmFr.TabIndex = 14;
            this.lblTanabanNmFr.Text = "先　頭";
            this.lblTanabanNmFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTanabanCap
            // 
            this.lblTanabanCap.BackColor = System.Drawing.Color.Silver;
            this.lblTanabanCap.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTanabanCap.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTanabanCap.Location = new System.Drawing.Point(24, 113);
            this.lblTanabanCap.Name = "lblTanabanCap";
            this.lblTanabanCap.Size = new System.Drawing.Size(81, 20);
            this.lblTanabanCap.TabIndex = 12;
            this.lblTanabanCap.Text = "棚　　　番";
            this.lblTanabanCap.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShohinNmTo
            // 
            this.lblShohinNmTo.BackColor = System.Drawing.Color.Silver;
            this.lblShohinNmTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShohinNmTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShohinNmTo.Location = new System.Drawing.Point(205, 84);
            this.lblShohinNmTo.Name = "lblShohinNmTo";
            this.lblShohinNmTo.Size = new System.Drawing.Size(229, 20);
            this.lblShohinNmTo.TabIndex = 11;
            this.lblShohinNmTo.Text = "最　後";
            this.lblShohinNmTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShohinCdTo
            // 
            this.txtShohinCdTo.AutoSizeFromLength = true;
            this.txtShohinCdTo.DisplayLength = null;
            this.txtShohinCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShohinCdTo.Location = new System.Drawing.Point(107, 84);
            this.txtShohinCdTo.MaxLength = 13;
            this.txtShohinCdTo.Name = "txtShohinCdTo";
            this.txtShohinCdTo.Size = new System.Drawing.Size(97, 20);
            this.txtShohinCdTo.TabIndex = 10;
            this.txtShohinCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShohinCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohinCdTo_Validating);
            // 
            // lblCodeBet2
            // 
            this.lblCodeBet2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblCodeBet2.Location = new System.Drawing.Point(438, 55);
            this.lblCodeBet2.Name = "lblCodeBet2";
            this.lblCodeBet2.Size = new System.Drawing.Size(18, 20);
            this.lblCodeBet2.TabIndex = 9;
            this.lblCodeBet2.Text = "～";
            this.lblCodeBet2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShohinNmFr
            // 
            this.lblShohinNmFr.BackColor = System.Drawing.Color.Silver;
            this.lblShohinNmFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShohinNmFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShohinNmFr.Location = new System.Drawing.Point(205, 55);
            this.lblShohinNmFr.Name = "lblShohinNmFr";
            this.lblShohinNmFr.Size = new System.Drawing.Size(229, 20);
            this.lblShohinNmFr.TabIndex = 8;
            this.lblShohinNmFr.Text = "先　頭";
            this.lblShohinNmFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShohinCdFr
            // 
            this.txtShohinCdFr.AutoSizeFromLength = true;
            this.txtShohinCdFr.DisplayLength = null;
            this.txtShohinCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShohinCdFr.Location = new System.Drawing.Point(107, 55);
            this.txtShohinCdFr.MaxLength = 13;
            this.txtShohinCdFr.Name = "txtShohinCdFr";
            this.txtShohinCdFr.Size = new System.Drawing.Size(97, 20);
            this.txtShohinCdFr.TabIndex = 7;
            this.txtShohinCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShohinCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohinCdFr_Validating);
            // 
            // lblShohinCdCap
            // 
            this.lblShohinCdCap.BackColor = System.Drawing.Color.Silver;
            this.lblShohinCdCap.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShohinCdCap.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShohinCdCap.Location = new System.Drawing.Point(24, 55);
            this.lblShohinCdCap.Name = "lblShohinCdCap";
            this.lblShohinCdCap.Size = new System.Drawing.Size(81, 20);
            this.lblShohinCdCap.TabIndex = 6;
            this.lblShohinCdCap.Text = "商品コード";
            this.lblShohinCdCap.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShohinKbnTo
            // 
            this.lblShohinKbnTo.BackColor = System.Drawing.Color.Silver;
            this.lblShohinKbnTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShohinKbnTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShohinKbnTo.Location = new System.Drawing.Point(364, 26);
            this.lblShohinKbnTo.Name = "lblShohinKbnTo";
            this.lblShohinKbnTo.Size = new System.Drawing.Size(174, 20);
            this.lblShohinKbnTo.TabIndex = 5;
            this.lblShohinKbnTo.Text = "最　後";
            this.lblShohinKbnTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShohinKbnTo
            // 
            this.txtShohinKbnTo.AutoSizeFromLength = true;
            this.txtShohinKbnTo.DisplayLength = null;
            this.txtShohinKbnTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShohinKbnTo.Location = new System.Drawing.Point(329, 26);
            this.txtShohinKbnTo.MaxLength = 4;
            this.txtShohinKbnTo.Name = "txtShohinKbnTo";
            this.txtShohinKbnTo.Size = new System.Drawing.Size(34, 20);
            this.txtShohinKbnTo.TabIndex = 4;
            this.txtShohinKbnTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShohinKbnTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohinKbnTo_Validating);
            // 
            // lblCodeBet1
            // 
            this.lblCodeBet1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblCodeBet1.Location = new System.Drawing.Point(305, 26);
            this.lblCodeBet1.Name = "lblCodeBet1";
            this.lblCodeBet1.Size = new System.Drawing.Size(18, 20);
            this.lblCodeBet1.TabIndex = 3;
            this.lblCodeBet1.Text = "～";
            this.lblCodeBet1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShohinKbnFr
            // 
            this.lblShohinKbnFr.BackColor = System.Drawing.Color.Silver;
            this.lblShohinKbnFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShohinKbnFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShohinKbnFr.Location = new System.Drawing.Point(142, 26);
            this.lblShohinKbnFr.Name = "lblShohinKbnFr";
            this.lblShohinKbnFr.Size = new System.Drawing.Size(160, 20);
            this.lblShohinKbnFr.TabIndex = 2;
            this.lblShohinKbnFr.Text = "先　頭";
            this.lblShohinKbnFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShohinKbnFr
            // 
            this.txtShohinKbnFr.AutoSizeFromLength = true;
            this.txtShohinKbnFr.DisplayLength = null;
            this.txtShohinKbnFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShohinKbnFr.Location = new System.Drawing.Point(107, 26);
            this.txtShohinKbnFr.MaxLength = 4;
            this.txtShohinKbnFr.Name = "txtShohinKbnFr";
            this.txtShohinKbnFr.Size = new System.Drawing.Size(34, 20);
            this.txtShohinKbnFr.TabIndex = 1;
            this.txtShohinKbnFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShohinKbnFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohinKbnFr_Validating);
            // 
            // lblShohinKbnCap
            // 
            this.lblShohinKbnCap.BackColor = System.Drawing.Color.Silver;
            this.lblShohinKbnCap.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShohinKbnCap.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShohinKbnCap.Location = new System.Drawing.Point(24, 26);
            this.lblShohinKbnCap.Name = "lblShohinKbnCap";
            this.lblShohinKbnCap.Size = new System.Drawing.Size(81, 20);
            this.lblShohinKbnCap.TabIndex = 0;
            this.lblShohinKbnCap.Text = "商品区分";
            this.lblShohinKbnCap.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDate
            // 
            this.lblDate.BackColor = System.Drawing.Color.Silver;
            this.lblDate.Location = new System.Drawing.Point(23, 18);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(166, 29);
            this.lblDate.TabIndex = 7;
            // 
            // lblMonth
            // 
            this.lblMonth.BackColor = System.Drawing.Color.Silver;
            this.lblMonth.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMonth.Location = new System.Drawing.Point(129, 23);
            this.lblMonth.Name = "lblMonth";
            this.lblMonth.Size = new System.Drawing.Size(20, 20);
            this.lblMonth.TabIndex = 4;
            this.lblMonth.Text = "月";
            this.lblMonth.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblYear
            // 
            this.lblYear.BackColor = System.Drawing.Color.Silver;
            this.lblYear.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblYear.Location = new System.Drawing.Point(87, 23);
            this.lblYear.Name = "lblYear";
            this.lblYear.Size = new System.Drawing.Size(20, 20);
            this.lblYear.TabIndex = 2;
            this.lblYear.Text = "年";
            this.lblYear.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtDateYear
            // 
            this.txtDateYear.AutoSizeFromLength = true;
            this.txtDateYear.DisplayLength = null;
            this.txtDateYear.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateYear.Location = new System.Drawing.Point(66, 23);
            this.txtDateYear.MaxLength = 2;
            this.txtDateYear.Name = "txtDateYear";
            this.txtDateYear.Size = new System.Drawing.Size(20, 20);
            this.txtDateYear.TabIndex = 1;
            this.txtDateYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateYear.TextChanged += new System.EventHandler(this.txtDateYear_TextChanged);
            this.txtDateYear.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateYear_Validating);
            // 
            // lblDateGengo
            // 
            this.lblDateGengo.BackColor = System.Drawing.Color.Silver;
            this.lblDateGengo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDateGengo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateGengo.Location = new System.Drawing.Point(27, 23);
            this.lblDateGengo.Name = "lblDateGengo";
            this.lblDateGengo.Size = new System.Drawing.Size(38, 20);
            this.lblDateGengo.TabIndex = 0;
            this.lblDateGengo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDateMonth
            // 
            this.txtDateMonth.AutoSizeFromLength = true;
            this.txtDateMonth.DisplayLength = null;
            this.txtDateMonth.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateMonth.Location = new System.Drawing.Point(108, 23);
            this.txtDateMonth.MaxLength = 2;
            this.txtDateMonth.Name = "txtDateMonth";
            this.txtDateMonth.Size = new System.Drawing.Size(20, 20);
            this.txtDateMonth.TabIndex = 3;
            this.txtDateMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateMonth.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonth_Validating);
            // 
            // txtDateDay
            // 
            this.txtDateDay.AutoSizeFromLength = true;
            this.txtDateDay.DisplayLength = null;
            this.txtDateDay.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateDay.Location = new System.Drawing.Point(150, 23);
            this.txtDateDay.MaxLength = 2;
            this.txtDateDay.Name = "txtDateDay";
            this.txtDateDay.Size = new System.Drawing.Size(20, 20);
            this.txtDateDay.TabIndex = 5;
            this.txtDateDay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateDay.Validating += new System.ComponentModel.CancelEventHandler(this.txtDay_Validating);
            // 
            // lblDay
            // 
            this.lblDay.BackColor = System.Drawing.Color.Silver;
            this.lblDay.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDay.Location = new System.Drawing.Point(171, 23);
            this.lblDay.Name = "lblDay";
            this.lblDay.Size = new System.Drawing.Size(18, 20);
            this.lblDay.TabIndex = 6;
            this.lblDay.Text = "日";
            this.lblDay.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // gbxTanaoroshiDate
            // 
            this.gbxTanaoroshiDate.Controls.Add(this.lblDay);
            this.gbxTanaoroshiDate.Controls.Add(this.txtDateDay);
            this.gbxTanaoroshiDate.Controls.Add(this.txtDateMonth);
            this.gbxTanaoroshiDate.Controls.Add(this.lblDateGengo);
            this.gbxTanaoroshiDate.Controls.Add(this.txtDateYear);
            this.gbxTanaoroshiDate.Controls.Add(this.lblYear);
            this.gbxTanaoroshiDate.Controls.Add(this.lblMonth);
            this.gbxTanaoroshiDate.Controls.Add(this.lblDate);
            this.gbxTanaoroshiDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxTanaoroshiDate.Location = new System.Drawing.Point(12, 182);
            this.gbxTanaoroshiDate.Name = "gbxTanaoroshiDate";
            this.gbxTanaoroshiDate.Size = new System.Drawing.Size(240, 60);
            this.gbxTanaoroshiDate.TabIndex = 4;
            this.gbxTanaoroshiDate.TabStop = false;
            this.gbxTanaoroshiDate.Text = "棚卸日付";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtMizuageShishoCd);
            this.groupBox1.Controls.Add(this.lblMizuageShishoNm);
            this.groupBox1.Controls.Add(this.lblMizuageShisho);
            this.groupBox1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBox1.ForeColor = System.Drawing.Color.Black;
            this.groupBox1.Location = new System.Drawing.Point(12, 39);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(372, 68);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "支所";
            // 
            // txtMizuageShishoCd
            // 
            this.txtMizuageShishoCd.AutoSizeFromLength = true;
            this.txtMizuageShishoCd.DisplayLength = null;
            this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtMizuageShishoCd.Location = new System.Drawing.Point(68, 26);
            this.txtMizuageShishoCd.MaxLength = 4;
            this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
            this.txtMizuageShishoCd.Size = new System.Drawing.Size(34, 20);
            this.txtMizuageShishoCd.TabIndex = 1;
            this.txtMizuageShishoCd.TabStop = false;
            this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
            // 
            // lblMizuageShishoNm
            // 
            this.lblMizuageShishoNm.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMizuageShishoNm.Location = new System.Drawing.Point(103, 27);
            this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
            this.lblMizuageShishoNm.Size = new System.Drawing.Size(212, 20);
            this.lblMizuageShishoNm.TabIndex = 2;
            this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMizuageShisho
            // 
            this.lblMizuageShisho.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShisho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShisho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblMizuageShisho.Location = new System.Drawing.Point(26, 24);
            this.lblMizuageShisho.Name = "lblMizuageShisho";
            this.lblMizuageShisho.Size = new System.Drawing.Size(295, 25);
            this.lblMizuageShisho.TabIndex = 3;
            this.lblMizuageShisho.Text = "支所";
            this.lblMizuageShisho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.comboBox1);
            this.groupBox2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBox2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox2.Location = new System.Drawing.Point(12, 406);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(270, 62);
            this.groupBox2.TabIndex = 903;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "中止区分";
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "0: 全ての商品",
            "1: 取引有りの商品のみ",
            "2: 取引中止の商品のみ"});
            this.comboBox1.Location = new System.Drawing.Point(26, 25);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(222, 21);
            this.comboBox1.TabIndex = 0;
            // 
            // KBMR1021
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(835, 634);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.gbxCondition);
            this.Controls.Add(this.gbxTanaoroshiDate);
            this.Controls.Add(this.gbxOutputRank);
            this.Controls.Add(this.gbxZeroOutput);
            this.Controls.Add(this.gbxPaper);
            this.Controls.Add(this.gbxOutput);
            this.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "KBMR1021";
            this.Text = "棚卸表";
            this.Controls.SetChildIndex(this.gbxOutput, 0);
            this.Controls.SetChildIndex(this.gbxPaper, 0);
            this.Controls.SetChildIndex(this.gbxZeroOutput, 0);
            this.Controls.SetChildIndex(this.gbxOutputRank, 0);
            this.Controls.SetChildIndex(this.gbxTanaoroshiDate, 0);
            this.Controls.SetChildIndex(this.gbxCondition, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.Controls.SetChildIndex(this.groupBox2, 0);
            this.pnlDebug.ResumeLayout(false);
            this.gbxOutput.ResumeLayout(false);
            this.gbxOutput.PerformLayout();
            this.gbxPaper.ResumeLayout(false);
            this.gbxPaper.PerformLayout();
            this.gbxZeroOutput.ResumeLayout(false);
            this.gbxZeroOutput.PerformLayout();
            this.gbxOutputRank.ResumeLayout(false);
            this.gbxOutputRank.PerformLayout();
            this.gbxCondition.ResumeLayout(false);
            this.gbxCondition.PerformLayout();
            this.gbxTanaoroshiDate.ResumeLayout(false);
            this.gbxTanaoroshiDate.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbxOutput;
        private System.Windows.Forms.RadioButton rdoOutput2;
        private System.Windows.Forms.RadioButton rdoOutput1;
        private jp.co.fsi.common.controls.FsiTextBox txtTanabanTo;
        private jp.co.fsi.common.controls.FsiTextBox txtTanabanFr;
        private System.Windows.Forms.Label lblCodeBet3;
        private System.Windows.Forms.GroupBox gbxPaper;
        private System.Windows.Forms.RadioButton rdoPaper2;
        private System.Windows.Forms.RadioButton rdoPaper1;
        private System.Windows.Forms.GroupBox gbxZeroOutput;
        private System.Windows.Forms.RadioButton rdoZeroOutput2;
        private System.Windows.Forms.RadioButton rdoZeroOutput1;
        private System.Windows.Forms.GroupBox gbxOutputRank;
        private System.Windows.Forms.RadioButton rdoOutputRank3;
        private System.Windows.Forms.RadioButton rdoOutputRank2;
        private System.Windows.Forms.RadioButton rdoOutputRank1;
        private System.Windows.Forms.GroupBox gbxCondition;
        private jp.co.fsi.common.controls.FsiTextBox txtShohinKbnFr;
        private System.Windows.Forms.Label lblShohinKbnCap;
        private System.Windows.Forms.Label lblCodeBet1;
        private System.Windows.Forms.Label lblShohinKbnFr;
        private System.Windows.Forms.Label lblShohinKbnTo;
        private jp.co.fsi.common.controls.FsiTextBox txtShohinKbnTo;
        private System.Windows.Forms.Label lblCodeBet2;
        private System.Windows.Forms.Label lblShohinNmFr;
        private jp.co.fsi.common.controls.FsiTextBox txtShohinCdFr;
        private System.Windows.Forms.Label lblShohinCdCap;
        private System.Windows.Forms.Label lblTanabanNmTo;
        private System.Windows.Forms.Label lblTanabanNmFr;
        private System.Windows.Forms.Label lblTanabanCap;
        private System.Windows.Forms.Label lblShohinNmTo;
        private jp.co.fsi.common.controls.FsiTextBox txtShohinCdTo;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.Label lblMonth;
        private System.Windows.Forms.Label lblYear;
        private common.controls.FsiTextBox txtDateYear;
        private System.Windows.Forms.Label lblDateGengo;
        private common.controls.FsiTextBox txtDateMonth;
        private common.controls.FsiTextBox txtDateDay;
        private System.Windows.Forms.Label lblDay;
        private System.Windows.Forms.GroupBox gbxTanaoroshiDate;
        private System.Windows.Forms.GroupBox groupBox1;
        private common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private System.Windows.Forms.Label lblMizuageShisho;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox comboBox1;
    }
}