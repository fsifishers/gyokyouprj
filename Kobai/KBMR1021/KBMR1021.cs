﻿using System;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

using GrapeCity.ActiveReports;

using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.kb.kbmr1021
{
    #region 棚卸設定構造体
    // 棚卸設定構造体
    public struct Settei
    {
        public int kbn;
        public decimal tanka;
        public decimal gokei;
        public void Clear()
        {
            kbn = 0;
            tanka = 0;
            gokei = 0;
        }
    }
    #endregion

    /// <summary>
    /// 棚卸表(KBMR1021)
    /// </summary>
    public partial class KBMR1021 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// 印刷テーブル使用列数
        /// </summary>
        public const int prtCols = 30;
        #endregion

        /// <summary>
        /// 棚卸設定退避用変数
        /// </summary>
        public Settei gSettei;

        #region プロパティ
        /// <summary>
        /// 画面上最後となるフォーカスのEnterボタン押下時処理用変数
        /// </summary>
        private bool _dtFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._dtFlg;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KBMR1021()
        {
            InitializeComponent();
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 水揚支所
#if DEBUG
            this.txtMizuageShishoCd.Text = "1";
#else
            this.txtMizuageShishoCd.Text = Uinfo.shishoCd;
#endif
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
            this.txtMizuageShishoCd.Enabled = (this.txtMizuageShishoCd.Text == "1") ? true : false;

            // 最新の棚卸日を取得する
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.txtMizuageShishoCd.Text);

            DataTable dtTnors = this.Dba.GetDataTableByConditionWithParams(
                "MAX(TANAOROSHI_DATE) AS 最終棚卸日付",
                "TB_HN_TANAOROSHI AS A LEFT OUTER JOIN TB_HN_SHOHIN AS B ON A.KAISHA_CD = B.KAISHA_CD AND A.SHISHO_CD = B.SHISHO_CD AND A.SHOHIN_CD = B.SHOHIN_CD",
                "A.KAISHA_CD = @KAISHA_CD AND A.SHISHO_CD = @SHISHO_CD AND B.SHOHIN_KUBUN5 <> 1",
                dpc);

            DateTime initialDate = DateTime.Now;

            if (dtTnors.Rows.Count > 0 && !ValChk.IsEmpty(dtTnors.Rows[0]["最終棚卸日付"]))
            {
                // 取得してきた日付を設定
                initialDate = Util.ToDate(dtTnors.Rows[0]["最終棚卸日付"]);
            }
            //設定取得
            GetSettei();
            // 最初に設定されている商品区分名を表示する
            ControlItemsBySettings();
            // 和暦変換して画面に表示
            string[] arrJpDate = Util.ConvJpDate(initialDate, this.Dba);
            // 取得された場合、年号をラベルに反映する
            this.lblDateGengo.Text = arrJpDate[0];
            this.txtDateYear.Text = arrJpDate[2];
            this.txtDateMonth.Text = arrJpDate[3];
            this.txtDateDay.Text = arrJpDate[4];

            // 棚卸表にフォーカス
            this.rdoOutput1.Focus();
            this.rdoOutput1.PerformClick();
            // ボタン表示
            this.btnF9.Visible = true;
            this.btnF9.Enabled = true;
            // 中止区分の初期表示
            this.comboBox1.SelectedIndex = 1;

        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd":
                case "txtDateYear":
                case "txtShohinKbnFr":
                case "txtShohinKbnTo":
                case "txtShohinCdFr":
                case "txtShohinCdTo":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        public override void PressF1()
        {
            Assembly asm;
            Type t;
            String[] result;

            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd": // 水揚支所
                    #region 水揚支所
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM2031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2031.CMCM2031");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.InData = this.txtMizuageShishoCd.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.txtMizuageShishoCd.Text = result[0];
                                this.lblMizuageShishoNm.Text = result[1];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtDateYear":
                //case "txtMonth":
                //case "txtDay":
                    #region 元号
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblDateGengo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (string[])frm.OutData;
                                this.lblDateGengo.Text = result[1];

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblDateGengo.Text,
                                        this.txtDateYear.Text,
                                        this.txtDateMonth.Text,
                                        this.txtDateDay.Text,
                                        this.Dba);
                                this.lblDateGengo.Text = arrJpDate[0];
                                this.txtDateYear.Text = arrJpDate[2];
                                this.txtDateMonth.Text = arrJpDate[3];
                                this.txtDateDay.Text = arrJpDate[4];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtShohinKbnFr":
                    #region 商品区分
                    // 商品区分1検索を起動
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1041.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1041.CMCM1041");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;

                            if(this.lblShohinKbnCap.Text == "商品区分1")
                            {
                                frm.Par1 = "VI_HN_SHOHIN_KBN1";
                            }
                            else if (this.lblShohinKbnCap.Text == "商品区分2")
                            {
                                frm.Par1 = "VI_HN_SHOHIN_KBN2";
                            }
                            else
                            {
                                frm.Par1 = "VI_HN_SHOHIN_KBN3";
                            }

                            frm.InData = this.txtShohinKbnFr.Text;
                            frm.ShowDialog(this);
                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (string[])frm.OutData;
                                this.txtShohinKbnFr.Text = result[0];
                                this.lblShohinKbnFr.Text = result[1];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtShohinKbnTo":
                    #region 商品区分
                    // 商品区分1検索を起動
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1041.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1041.CMCM1041");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;

                            if (this.lblShohinKbnCap.Text == "商品区分1")
                            {
                                frm.Par1 = "VI_HN_SHOHIN_KBN1";
                            }
                            else if (this.lblShohinKbnCap.Text == "商品区分2")
                            {
                                frm.Par1 = "VI_HN_SHOHIN_KBN2";
                            }
                            else
                            {
                                frm.Par1 = "VI_HN_SHOHIN_KBN3";
                            }
                            frm.InData = this.txtShohinKbnTo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (string[])frm.OutData;
                                this.txtShohinKbnTo.Text = result[0];
                                this.lblShohinKbnTo.Text = result[1];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtShohinCdFr":
                    #region 商品CD
                    // 商品コード検索を起動
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "KBCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.kb.kbcm1021.KBCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtShohinCdFr.Text = outData[0];
                                this.lblShohinNmFr.Text = outData[1];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtShohinCdTo":
                    #region 商品CD
                    // 商品コード検索を起動
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "KBCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.kb.kbcm1021.KBCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtShohinCdTo.Text = outData[0];
                                this.lblShohinNmTo.Text = outData[1];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtTanabanFr":
                case "txtTanabanTo":
                    #region 棚番
                    // 棚番検索を起動
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "KBCM2011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.kb.kbcm2011.KBCM2011");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            if (this.ActiveCtlNm == "txtTanabanFr")
                            {
                                frm.InData = this.txtTanabanFr.Text;
                            }
                            else if (this.ActiveCtlNm == "txtTanabanTo")
                            {
                                frm.InData = this.txtTanabanTo.Text;
                            }
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (string[])frm.OutData;
                                if (this.ActiveCtlNm == "txtTanabanFr")
                                {
                                    this.txtTanabanFr.Text = result[0];
                                    this.lblTanabanNmFr.Text = result[0];
                                }
                                else if (this.ActiveCtlNm == "txtTanabanTo")
                                {
                                    this.txtTanabanTo.Text = result[0];
                                    this.lblTanabanNmTo.Text = result[0];
                                }
                            }
                        }
                    }
                    #endregion
                    break;
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
            {
                // プレビュー処理
                DoPrint(true);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("印刷", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }

        /// <summary>
        /// F6キー押下時処理
        /// PDF出力
        /// </summary>
        public override void PressF6()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("PDF出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, true);
            }
        }

        /// <summary>
        /// F7キー押下時処理
        /// EXCEL出力
        /// </summary>
        public override void PressF7()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("EXCEL出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, false, true);
            }
        }

        /// <summary>
        /// F9キー押下時処理
        /// </summary>
        public override void PressF9()
        {
            // 参照画面の起動
            //KBMR1022 frmKBMR1022 = new KBMR1022(this);
            KBMR1022 frmKBMR1022 = new KBMR1022();
            if (frmKBMR1022.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
            {
                this.Config.ReloadConfig();
                // 設定内容の取得
                GetSettei();
                // 商品区分を設定
                ControlItemsBySettings();
            }
            frmKBMR1022.Dispose();
        }

        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            // 設定画面の起動
            // MEMO:原則としてここで渡す帳票IDの設定はReport.csvに保持していることが前提ですが、
            // 保持していない場合は、設定画面での保存(F6)時に新規に設定が保持されます。
            PrintSettingForm psForm = new PrintSettingForm(new string[2] { "KBMR10211R", "KBMR10212R" });
            psForm.ShowDialog();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 水揚支所入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMizuageShishoCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                e.Cancel = true;
                this.txtMizuageShishoCd.SelectAll();
                this.txtMizuageShishoCd.Focus();
            }
        }

        /// <summary>
        /// 年の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateYear_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtDateYear.Text, this.txtDateYear.MaxLength))
            {
                e.Cancel = true;
                this.txtDateYear.SelectAll();
            }
            else
            {
                this.txtDateYear.Text = Util.ToString(IsValid.SetYear(this.txtDateYear.Text));
                CheckJpFr();
                SetJpFr();
            }
        }

        /// <summary>
        /// 月の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMonth_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtDateMonth.Text, this.txtDateMonth.MaxLength))
            {
                e.Cancel = true;

                this.txtDateMonth.SelectAll();
            }
            else
            {
                this.txtDateMonth.Text = Util.ToString(IsValid.SetMonth(this.txtDateMonth.Text));
                CheckJpFr();
                SetJpFr();
            }
        }

        /// <summary>
        /// 日の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDay_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsDay(this.txtDateDay.Text, this.txtDateDay.MaxLength))
            {
                e.Cancel = true;
                this.txtDateDay.SelectAll();
            }
            else
            {
                this.txtDateDay.Text = Util.ToString(IsValid.SetDay(this.txtDateDay.Text));
                CheckJpFr();
                SetJpFr();
            }
        }

        /// <summary>
        /// 商品区分(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShohinKbnFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShohinKbnFr())
            {
                e.Cancel = true;
                this.txtShohinKbnFr.SelectAll();
            }
        }

        /// <summary>
        /// 商品区分(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShohinKbnTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShohinKbnTo())
            {
                e.Cancel = true;
                this.txtShohinKbnTo.SelectAll();
            }
        }

        /// <summary>
        /// 商品コード(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShohinCdFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShohinCdFr())
            {
                e.Cancel = true;
                this.txtShohinCdFr.SelectAll();
            }
        }

        /// <summary>
        /// 商品コード(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShohinCdTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShohinCdTo())
            {
                e.Cancel = true;
                this.txtShohinCdTo.SelectAll();
            }
        }

        /// <summary>
        /// 棚番(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTanabanFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidTanabanFr())
            {
                e.Cancel = true;
                this.txtTanabanFr.SelectAll();
            }
        }

        /// <summary>
        /// 棚番(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTanabanTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidTanabanTo())
            {
                e.Cancel = true;
                this.txtTanabanTo.SelectAll();

                // Enter処理を無効化
                this._dtFlg = false;
            }
            else
            {
                // Enter処理を有効化
                this._dtFlg = true;
            }
        }

        /// <summary>
        /// 出力帳票切り替え時のイベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rdoOutput_CheckedChanged(object sender, EventArgs e)
        {
            if (this.rdoOutput1.Checked)
            {
                // 差ｾﾞﾛ出力の選択を可能にする
                this.rdoZeroOutput1.Enabled = true;
                this.rdoZeroOutput2.Enabled = true;
            }
            else
            {
                // 差ｾﾞﾛ出力の選択を不可にする
                this.rdoZeroOutput1.Checked = true;
                this.rdoZeroOutput1.Enabled = false;
                this.rdoZeroOutput2.Enabled = false;
            }
        }

        /// <summary>
        /// 棚番(至)のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTanabanTo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.Flg)
            {
                // Enter処理を無効化
                this._dtFlg = false;

                // 全項目を再度入力値チェック
                if (!ValidateAll())
                {
                    // エラーありの場合ここで処理終了
                    return;
                }

                if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
                {
                    // ﾌﾟﾚﾋﾞｭｰ処理
                    DoPrint(true);
                }
                else
                {
                    this.txtTanabanTo.Focus();
                }
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 水揚支所の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool IsValidMizuageShishoCd()
        {
            // 空 又は 0入力の場合
            if (ValChk.IsEmpty(this.txtMizuageShishoCd.Text) || Equals(this.txtMizuageShishoCd.Text, "0"))
            {
                // 水揚支所名称を表示する
                this.txtMizuageShishoCd.Text = "0";
                this.lblMizuageShishoNm.Text = "全て";
                return true;
            }
            // 水揚支所名称を表示する
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);

            if (ValChk.IsEmpty(this.lblMizuageShishoNm.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 商品区分(自)の入力チェック
        /// </summary>
        /// <returns>true:OK/false:NG</returns>
        private bool IsValidShohinKbnFr()
        {
            // 未入力の場合、「先頭」を表示
            if (ValChk.IsEmpty(this.txtShohinKbnFr.Text))
            {
                this.lblShohinKbnFr.Text = "先　頭";
                return true;
            }
            // 数値のみの入力を許可
            else if (!ValChk.IsNumber(this.txtShohinKbnFr.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtShohinKbnFr.SelectAll();
                return false;
            }
            // 名称を表示
            else
            {
                switch (gSettei.kbn)
                {
                    case 1:
                        this.lblShohinKbnFr.Text = this.Dba.GetName(this.UInfo, "VI_HN_SHOHIN_KBN1", this.txtMizuageShishoCd.Text, this.txtShohinKbnFr.Text);
                        break;
                    case 2:
                        this.lblShohinKbnFr.Text = this.Dba.GetName(this.UInfo, "VI_HN_SHOHIN_KBN2", this.txtMizuageShishoCd.Text, this.txtShohinKbnFr.Text);
                        break;
                    case 3:
                        this.lblShohinKbnFr.Text = this.Dba.GetName(this.UInfo, "VI_HN_SHOHIN_KBN3", this.txtMizuageShishoCd.Text, this.txtShohinKbnFr.Text);
                        break;
                    default:
                        break;
                }
            }
            return true;
        }

        /// <summary>
        /// 商品区分(至)の入力チェック
        /// </summary>
        /// <returns>true:OK/false:NG</returns>
        private bool IsValidShohinKbnTo()
        {
            // 未入力の場合、「最後」を表示
            if (ValChk.IsEmpty(this.txtShohinKbnTo.Text))
            {
                this.lblShohinKbnTo.Text = "最　後";
                return true;
            }
            // 数値のみの入力を許可
            else if (!ValChk.IsNumber(this.txtShohinKbnTo.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtShohinKbnTo.SelectAll();
                return false;
            }
            // 名称を表示
            else
            {
                switch (gSettei.kbn)
                {
                    case 1:
                        this.lblShohinKbnTo.Text = this.Dba.GetName(this.UInfo, "VI_HN_SHOHIN_KBN1", this.txtMizuageShishoCd.Text, this.txtShohinKbnTo.Text);
                        break;
                    case 2:
                        this.lblShohinKbnTo.Text = this.Dba.GetName(this.UInfo, "VI_HN_SHOHIN_KBN2", this.txtMizuageShishoCd.Text, this.txtShohinKbnTo.Text);
                        break;
                    case 3:
                        this.lblShohinKbnTo.Text = this.Dba.GetName(this.UInfo, "VI_HN_SHOHIN_KBN3", this.txtMizuageShishoCd.Text, this.txtShohinKbnTo.Text);
                        break;
                    default:
                        break;
                }
            }
            return true;
        }

        /// <summary>
        /// 商品コード(自)の入力チェック
        /// </summary>
        /// <returns>true:OK/false:NG</returns>
        private bool IsValidShohinCdFr()
        {
            // 未入力の場合、「先頭」を表示
            if (ValChk.IsEmpty(this.txtShohinCdFr.Text))
            {
                this.lblShohinNmFr.Text = "先　頭";
                return true;
            }
            // 数値のみの入力を許可
            else if (!ValChk.IsNumber(this.txtShohinCdFr.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtShohinCdFr.SelectAll();
                return false;
            }
            // 名称を表示
            else
            {
                this.lblShohinNmFr.Text = this.Dba.GetName(this.UInfo, "TB_HN_SHOHIN", this.txtMizuageShishoCd.Text, this.txtShohinCdFr.Text);
            }

            return true;
        }

        /// <summary>
        /// 商品コード(至)の入力チェック
        /// </summary>
        /// <returns>true:OK/false:NG</returns>
        private bool IsValidShohinCdTo()
        {
            // 未入力の場合、「最後」を表示
            if (ValChk.IsEmpty(this.txtShohinCdTo.Text))
            {
                this.lblShohinNmTo.Text = "最　後";
                return true;
            }
            // 数値のみの入力を許可
            else if (!ValChk.IsNumber(this.txtShohinCdTo.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtShohinCdTo.SelectAll();
                return false;
            }
            // 名称を表示
            else
            {
                this.lblShohinNmTo.Text = this.Dba.GetName(this.UInfo, "TB_HN_SHOHIN", this.txtMizuageShishoCd.Text, this.txtShohinCdTo.Text);
            }

            return true;
        }

        /// <summary>
        /// 棚番(自)の入力チェック
        /// </summary>
        /// <returns>true:OK/false:NG</returns>
        private bool IsValidTanabanFr()
        {
            // 未入力の場合、「先頭」を表示
            if (ValChk.IsEmpty(this.txtTanabanFr.Text))
            {
                this.lblTanabanNmFr.Text = "先　頭";
                return true;
            }
            //// 数値のみの入力を許可
            //else if (!ValChk.IsNumber(this.txtTanabanFr.Text))
            else if (!ValChk.IsHalfChar(this.txtTanabanFr.Text))
            {
                //Msg.Notice("数値のみで入力してください。");
                Msg.Notice("半角英数字のみで入力してください。");
                this.txtTanabanFr.SelectAll();
                return false;
            }

            // 入力されている場合は、名称欄を空白表示
            this.lblTanabanNmFr.Text = string.Empty;

            return true;
        }

        /// <summary>
        /// 棚番(至)の入力チェック
        /// </summary>
        /// <returns>true:OK/false:NG</returns>
        private bool IsValidTanabanTo()
        {
            // 未入力の場合、「最後」を表示
            if (ValChk.IsEmpty(this.txtTanabanTo.Text))
            {
                this.lblTanabanNmTo.Text = "最　後";
                return true;
            }
            //// 数値のみの入力を許可
            else if(!ValChk.IsHalfChar(this.txtTanabanTo.Text))
            {
                Msg.Notice("半角英数字のみで入力してください。");
                this.txtTanabanTo.SelectAll();
                return false;
            }

            // 入力されている場合は、名称欄を空白表示
            this.lblTanabanNmTo.Text = string.Empty;

            return true;
        }

        /// <summary>
        /// 年月日(自)の月末入力チェック
        /// </summary>
        /// 
        private void CheckJpFr()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblDateGengo.Text, this.txtDateYear.Text,
                this.txtDateMonth.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDateDay.Text) > lastDayInMonth)
            {
                this.txtDateDay.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(自)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetJpFr()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJp(Util.FixJpDate(this.lblDateGengo.Text, this.txtDateYear.Text,
                this.txtDateMonth.Text, this.txtDateDay.Text, this.Dba));
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 水揚支所の入力チェック
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                this.txtMizuageShishoCd.Focus();
                this.txtMizuageShishoCd.SelectAll();
                return false;
            }
            // 単項目チェック
            // 年(自)のチェック
            if (!IsValid.IsYear(this.txtDateYear.Text, this.txtDateYear.MaxLength))
            {
                this.txtDateYear.Focus();
                this.txtDateYear.SelectAll();
                return false;
            }
            // 月(自)のチェック
            if (!IsValid.IsMonth(this.txtDateMonth.Text, this.txtDateMonth.MaxLength))
            {
                this.txtDateMonth.Focus();
                this.txtDateMonth.SelectAll();
                return false;
            }
            // 日(自)のチェック
            if (!IsValid.IsDay(this.txtDateDay.Text, this.txtDateDay.MaxLength))
            {
                this.txtDateDay.Focus();
                this.txtDateDay.SelectAll();
                return false;
            }
            // 年月日(自)の月末入力チェック処理
            CheckJpFr();
            // 年月日(自)の正しい和暦への変換処理
            SetJpFr();

            if (rdoOutput1.Checked)
            {
                DateTime tanaoroshiDate = Util.ConvAdDate(this.lblDateGengo.Text, this.txtDateYear.Text,
                    this.txtDateMonth.Text, this.txtDateDay.Text, this.Dba);

                // 入力した日付が棚卸テーブルに無ければエラー
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
                dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 2, this.txtMizuageShishoCd.Text);
                dpc.SetParam("@TANAOROSHI_DATE", SqlDbType.DateTime, tanaoroshiDate);
                StringBuilder where = new StringBuilder();
                where.Append("KAISHA_CD = @KAISHA_CD AND ");
                if (this.txtMizuageShishoCd.Text != "0")
                    where.Append("SHISHO_CD = @SHISHO_CD AND ");
                where.Append("TANAOROSHI_DATE = @TANAOROSHI_DATE");
                DataTable dtDateChk = this.Dba.GetDataTableByConditionWithParams("COUNT(*) AS 件数", "TB_HN_TANAOROSHI", Util.ToString(where), dpc);
                if (dtDateChk.Rows.Count == 0 || Util.ToInt(dtDateChk.Rows[0]["件数"]) == 0)
                {
                    // Msg.Notice("入力に誤りがあります。");
                    Msg.Notice("その日付での棚卸はありません。");
                    this.txtDateDay.Focus();
                    this.txtDateDay.SelectAll();
                    return false;
                }
            }
            // 商品区分(自)
            if (!IsValidShohinKbnFr())
            {
                this.txtShohinKbnFr.Focus();
                this.txtShohinKbnFr.SelectAll();
                return false;
            }

            // 商品区分(至)
            if (!IsValidShohinKbnTo())
            {
                this.txtShohinKbnTo.Focus();
                this.txtShohinKbnTo.SelectAll();
                return false;
            }

            // 商品コード(自)
            if (!IsValidShohinCdFr())
            {
                this.txtShohinCdFr.Focus();
                this.txtShohinCdFr.SelectAll();
                return false;
            }

            // 商品コード(至)
            if (!IsValidShohinCdTo())
            {
                this.txtShohinCdTo.Focus();
                this.txtShohinCdTo.SelectAll();
                return false;
            }

            // 棚番(自)
            if (!IsValidTanabanFr())
            {
                this.txtTanabanFr.Focus();
                this.txtTanabanFr.SelectAll();
                return false;
            }

            // 棚番(至)
            if (!IsValidTanabanTo())
            {
                this.txtTanabanFr.Focus();
                this.txtTanabanFr.SelectAll();
                return false;
            }

            return true;
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJp(string[] arrJpDate)
        {
            this.lblDateGengo.Text = arrJpDate[0];
            this.txtDateYear.Text = arrJpDate[2];
            this.txtDateMonth.Text = arrJpDate[3];
            this.txtDateDay.Text = arrJpDate[4];
        }

        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview, bool isPdf = false, bool isExcel = false, bool isCsv = false)
        {
            try
            {
#if DEBUG
                //印刷用ワークテーブルの削除
                this.Dba.DeleteWork("PR_HN_TBL", this.UnqId);
#endif

                bool dataFlag;

                this.Dba.BeginTransaction();

                //// 帳票出力用にワークテーブルにデータを作成
                DataTable dtOutput = MakeOutputData();
                dataFlag = InsertWkData(dtOutput);

                // 帳票出力
                if (dataFlag)
                {
                    // 取得列の定義
                    StringBuilder cols = Util.ColsArray(prtCols, "");

                    // バインドパラメータの設定
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);

                    // データの取得
                    dtOutput = this.Dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_HN_TBL", "GUID = @GUID", "SORT ASC", dpc);

                    jp.co.fsi.common.report.BaseReport rpt;
                    string title = setTitle();
                    if (rdoOutput1.Checked)
                    {

                    }
                    if (rdoPaper1.Checked)
                    {
                        // 帳票オブジェクトをインスタンス化
                        rpt = new KBMR10211R(dtOutput);
                    }
                    else
                    {
                        rpt = new KBMR10212R(dtOutput);
                    }
                    rpt.Document.Name = title; // this.lblTitle.Text;
                    rpt.Document.Printer.DocumentName = title; //this.lblTitle.Text;

                    if (isExcel)
                    {
                        GrapeCity.ActiveReports.Export.Excel.Section.XlsExport xlsExport1 = new GrapeCity.ActiveReports.Export.Excel.Section.XlsExport();
                        //SetExcelSetting(xlsExport1);
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Kob, rpt.Document.Name, 2);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            xlsExport1.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("EXCEL出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPdf)
                    {
                        GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport p = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Kob, rpt.Document.Name, 1);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            p.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("PDF出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPreview)
                    {
                        // プレビュー画面表示
                        PreviewForm pFrm = new PreviewForm(rpt, this.UnqId);
                        pFrm.WindowState = FormWindowState.Maximized;
                        pFrm.Show();
                    }
                    else
                    {
                        // 直接印刷
                        rpt.Run(false);
                        rpt.Document.Print(true, true, false);
                    }
                }
            }
            finally
            {
#if DEBUG
                this.Dba.Commit();
#else
                this.Dba.Rollback();
#endif
            }
        }

        /// <summary>
        /// 出力データを作成します。
        /// </summary>
        /// <returns>DBから取得した情報を加工した出力データ</returns>
        private DataTable MakeOutputData()
        {
            DateTime tanaoroshiDate = Util.ConvAdDate(this.lblDateGengo.Text, this.txtDateYear.Text,
                this.txtDateMonth.Text, this.txtDateDay.Text, this.Dba);

            #region 設定ファイルから設定値を取得
            //string tanaoroshiTanka = "";
            //if (Util.ToInt(this.Config.LoadPgConfig(Constants.SubSys.Kob, "KBMR1021", "Setting", "TanaoroshiKingakuTanka")) == 0)
            //{
            //    tanaoroshiTanka = "0";
            //}
            //else if (Util.ToInt(this.Config.LoadPgConfig(Constants.SubSys.Kob, "KBMR1021", "Setting", "TanaoroshiKingakuTanka")) == 1)
            //{
            //    tanaoroshiTanka = "1";
            //}
            //// 商品区分絞り込み条件設定を取得
            //int kbn = Util.ToInt(this.Config.LoadPgConfig(Constants.SubSys.Kob, "KBMR1021", "Setting", "ShohinKubun"));
            #endregion

            string shishoCd = this.txtMizuageShishoCd.Text;

            // 商品在庫データを取得
            DataTable dtShohinZaiko = GetDataShohinZaiko(gSettei.kbn);
            DataRow drShohinZaiko;

            // 仕入数量データを取得
            DataTable dtShiireSuryo = GetDataShiireSuryo(tanaoroshiDate, gSettei.kbn);
            DataRow[] aryShiireSuryo;
            DataRow drShiireSuryo;

            // 入出庫数量データを取得
            DataTable dtNyusyukkoSuryo = GetDataNyushukkoSuryo(tanaoroshiDate, gSettei.kbn);
            DataRow[] aryNyusyukkoSuryo;
            DataRow drNyusyukkoSuryo;

            // 棚卸数量データを取得
            DataTable dtTanaoroshi = GetDataTanaoroshi(tanaoroshiDate, gSettei.kbn);
            DataRow[] aryTanaoroshi;
            DataRow drTanaoroshi;

            // 格納するDataTableの定義を作成
            DataTable dtOutput = new DataTable();
            dtOutput.Columns.Add("TANABAN");
            dtOutput.Columns.Add("SHOHIN_CD");
            dtOutput.Columns.Add("SHOHIN_NM");
            dtOutput.Columns.Add("KIKAKU");
            dtOutput.Columns.Add("IRISU");
            dtOutput.Columns.Add("SOKO");
            dtOutput.Columns.Add("CHOBO_CASE_SU");
            dtOutput.Columns.Add("CHOBO_BARA_SU");
            dtOutput.Columns.Add("CHOBO_KINGAKU");
            dtOutput.Columns.Add("TANAOROSHI_CASE_SU");
            dtOutput.Columns.Add("TANAOROSHI_BARA_SU");
            dtOutput.Columns.Add("SAI_CASE_SU");
            dtOutput.Columns.Add("SAI_BARA_SU");
            dtOutput.Columns.Add("SAI_KINGAKU");
            dtOutput.Columns.Add("ZAIKO_KINGAKU");

            dtOutput.Columns.Add("IRISU_GOKEI");
            dtOutput.Columns.Add("SOKO_GOKEI");
            dtOutput.Columns.Add("CHOBO_CASE_SU_GOKEI");
            dtOutput.Columns.Add("CHOBO_BARA_SU_GOKEI");
            dtOutput.Columns.Add("CHOBO_KINGAKU_GOKEI");
            dtOutput.Columns.Add("TANAOROSHI_CASE_SU_GOKEI");
            dtOutput.Columns.Add("TANAOROSHI_BARA_SU_GOKEI");
            dtOutput.Columns.Add("SAI_CASE_SU_GOKEI");
            dtOutput.Columns.Add("SAI_BARA_SU_GOKEI");
            dtOutput.Columns.Add("SAI_KINGAKU_GOKEI");
            dtOutput.Columns.Add("ZAIKO_KINGAKU_GOKEI");

            DataRow drOutput = null;    // 格納するDataTableの行Obj

            int irisu;
            decimal choboCaseSu = 0;
            decimal choboBaraSu = 0;
            decimal tanaoroshiCaseSu = 0;
            decimal tanaoroshiBaraSu = 0;
            decimal saiCaseSu = 0;
            decimal saiBaraSu = 0;
            decimal choboKingaku = 0;
            decimal choboKingakuKei = 0;
            decimal saiKingaku = 0;
            decimal saiKingakuKei = 0;
            decimal zaikoKingaku = 0;
            decimal zaikoKingakuKei = 0;
            decimal kinyuSuryo;

            decimal irisuGokei = 0;
            decimal sokoGokei = 0;
            decimal choboCaseSuGokei = 0;
            decimal choboBaraSuGokei = 0;
            decimal choboKingakuGokei = 0;
            decimal tanaoroshiCaseSuGokei = 0;
            decimal tanaoroshiBaraSuGokei = 0;
            decimal saiCaseSuGokei = 0;
            decimal saiBaraSuGokei = 0;
            decimal saiKingakuGokei = 0;
            decimal zaikoKingakuGokei = 0;

            decimal work_saiCaseSu = 0;
            decimal work_saiBaraSu = 0;

            for (int i = 0; i < dtShohinZaiko.Rows.Count; i++)
            {
                // 在庫データの1行分を変数に格納
                drShohinZaiko = dtShohinZaiko.Rows[i];

                // 新しい行の器を作成
                drOutput = dtOutput.NewRow();

                #region 最終仕入単価選択時 取得用SQL
                // 最終仕入単価を取得
                DataRow drSaishushiireTanka = getSaishuSiireTanka(tanaoroshiDate, drShohinZaiko);
                #endregion

                // 在庫データから取得した情報を帳票出力情報のDataTableに格納
                drOutput["TANABAN"] = drShohinZaiko["TANABAN"];
                drOutput["SHOHIN_CD"] = drShohinZaiko["SHOHIN_CD"];
                drOutput["SHOHIN_NM"] = drShohinZaiko["SHOHIN_NM"];
                drOutput["KIKAKU"] = drShohinZaiko["KIKAKU"];
                irisu = Util.ToInt(drShohinZaiko["IRISU"]);
                drOutput["IRISU"] = irisu;
                drOutput["SOKO"] = "0";

                // 数量をセット--
                #region 棚卸の場合
                if (this.rdoOutput1.Checked)
                {
                    string sql = "商品コード = " + Util.ToString(drShohinZaiko["SHOHIN_CD"]);
                    aryTanaoroshi = dtTanaoroshi.Select(sql);

                    if (Util.ToString(drShohinZaiko["SHOHIN_CD"]) == Util.ToString(10101))
                    {
                        int aaa = Util.ToInt(drSaishushiireTanka["SAISHU_SHIIRE_TANKA"]);
                    }
                    if (aryTanaoroshi.Length > 0)
                    {
                        drTanaoroshi = aryTanaoroshi[0];

                        // 差ゼロの出力しない場合は行を作成しない（合計が合わない為）
                        work_saiCaseSu = Util.ToDecimal(drTanaoroshi["差数量１"]);
                        work_saiBaraSu = Util.ToDecimal(drTanaoroshi["差数量２"]);
                        SetSuryo(Util.ToDecimal(drShohinZaiko["IRISU"]), ref work_saiCaseSu, ref work_saiBaraSu);
                        if (!(rdoZeroOutput2.Checked &&
                                work_saiCaseSu == 0 &&
                                    work_saiBaraSu == 0))
                        {
                        }
                        else
                        {
                            continue;
                        }

                        choboCaseSu = Util.ToDecimal(drTanaoroshi["現数量１"]);
                        choboBaraSu = Util.ToDecimal(drTanaoroshi["現数量２"]);
                        SetSuryo(Util.ToDecimal(drShohinZaiko["IRISU"]), ref choboCaseSu, ref choboBaraSu);
                        tanaoroshiCaseSu = Util.ToDecimal(drTanaoroshi["実数量１"]);
                        tanaoroshiBaraSu = Util.ToDecimal(drTanaoroshi["実数量２"]);
                        SetSuryo(Util.ToDecimal(drShohinZaiko["IRISU"]), ref tanaoroshiCaseSu, ref tanaoroshiBaraSu);
                        saiCaseSu = Util.ToDecimal(drTanaoroshi["差数量１"]);
                        saiBaraSu = Util.ToDecimal(drTanaoroshi["差数量２"]);
                        SetSuryo(Util.ToDecimal(drShohinZaiko["IRISU"]), ref saiCaseSu, ref saiBaraSu);

                        choboKingakuKei = GetBaraSu(Util.ToDecimal(drTanaoroshi["現数量１"]), Util.ToDecimal(drTanaoroshi["現数量２"]), Util.ToDecimal(drShohinZaiko["IRISU"]));
                        saiKingakuKei = GetBaraSu(Util.ToDecimal(drTanaoroshi["差数量１"]), Util.ToDecimal(drTanaoroshi["差数量２"]), Util.ToDecimal(drShohinZaiko["IRISU"]));
                        zaikoKingakuKei = GetBaraSu(Util.ToDecimal(drTanaoroshi["実数量１"]), Util.ToDecimal(drTanaoroshi["実数量２"]), Util.ToDecimal(drShohinZaiko["IRISU"]));

                    }
                    else
                    {
                        // 値をクリア
                        choboCaseSu = 0;
                        choboBaraSu = 0;
                        tanaoroshiCaseSu = 0;
                        tanaoroshiBaraSu = 0;
                        saiCaseSu = 0;
                        saiBaraSu = 0;
                        choboKingakuKei = 0;
                        saiKingakuKei = 0;
                        zaikoKingakuKei = 0;
                    }
                }
                #endregion
                #region 記入表の場合
                else
                {
                    string sql = "商品コード = " + Util.ToString(drShohinZaiko["SHOHIN_CD"]);
                    aryShiireSuryo = dtShiireSuryo.Select(sql);
                    aryNyusyukkoSuryo = dtNyusyukkoSuryo.Select(sql);

                    kinyuSuryo = Util.ToDecimal(drShohinZaiko["SURYO2"]);

                    if (aryShiireSuryo.Length > 0)
                    {
                        drShiireSuryo = aryShiireSuryo[0];
                        kinyuSuryo += Util.ToDecimal(drShiireSuryo["移動数量"]);
                    }
                    if (aryNyusyukkoSuryo.Length > 0)
                    {
                        drNyusyukkoSuryo = aryNyusyukkoSuryo[0];
                        kinyuSuryo += Util.ToDecimal(drNyusyukkoSuryo["移動数量"]);
                    }

                    choboCaseSu = Util.ToDecimal(drShohinZaiko["SURYO1"]);
                    choboBaraSu = kinyuSuryo;
                    SetSuryo(Util.ToDecimal(drShohinZaiko["IRISU"]), ref choboCaseSu, ref choboBaraSu);
                    tanaoroshiCaseSu = 0;
                    tanaoroshiBaraSu = 0;
                    saiCaseSu = 0;
                    saiBaraSu = 0;

                    choboKingakuKei = GetBaraSu(choboCaseSu, choboBaraSu, Util.ToDecimal(drShohinZaiko["IRISU"]));
                    saiKingakuKei = saiBaraSu;
                    zaikoKingakuKei = tanaoroshiBaraSu;
                }
                #endregion

                #region 縦表示の場合
                if (rdoPaper1.Checked)
                {
                    drOutput["CHOBO_CASE_SU"] = Util.FormatNum(choboCaseSu, 2);
                    drOutput["CHOBO_BARA_SU"] = Util.FormatNum(choboBaraSu, 2);
                    drOutput["TANAOROSHI_CASE_SU"] = Util.FormatNum(tanaoroshiCaseSu, 2);
                    drOutput["TANAOROSHI_BARA_SU"] = Util.FormatNum(tanaoroshiBaraSu, 2);
                    drOutput["SAI_CASE_SU"] = Util.FormatNum(saiCaseSu, 2);
                    drOutput["SAI_BARA_SU"] = Util.FormatNum(saiBaraSu, 2);
                }
                #endregion
                #region 横表示の場合
                else
                {
                    drOutput["CHOBO_CASE_SU"] = Util.FormatNum(choboCaseSu, 2);
                    drOutput["CHOBO_BARA_SU"] = Util.FormatNum(choboBaraSu, 2);
                    drOutput["TANAOROSHI_CASE_SU"] = Util.FormatNum(tanaoroshiCaseSu, 2);
                    drOutput["TANAOROSHI_BARA_SU"] = Util.FormatNum(tanaoroshiBaraSu, 2);
                    drOutput["SAI_CASE_SU"] = Util.FormatNum(saiCaseSu, 2);
                    drOutput["SAI_BARA_SU"] = Util.FormatNum(saiBaraSu, 2);
                }
                #endregion

                // 単価選択時
                decimal tanka = 0;
                if (gSettei.tanka == 1)
                {
                    // 最終仕入単価の場合
                    tanka = Util.ToDecimal(drSaishushiireTanka["SAISHU_SHIIRE_TANKA"]);
                    if (tanka == 0)
                    {
                        tanka = Util.ToDecimal(drShohinZaiko["SHIIRE_TANKA"]);
                    }
                }
                else
                {
                    // 商品ﾏｽﾀｰ仕入単価の場合(帳簿金額)
                    tanka = Util.ToDecimal(drShohinZaiko["SHIIRE_TANKA"]);
                }
                //帳簿金額
                choboKingaku = choboKingakuKei * tanka;

                // 単価選択時、商品ﾏｽﾀｰ仕入単価の場合(差異金額)
                //差異金額
                saiKingaku = Util.ToDecimal(saiKingakuKei) * Util.ToDecimal(drShohinZaiko["SHIIRE_TANKA"]);
                //// 単価選択時、商品ﾏｽﾀｰ仕入単価の場合(在庫金額)
                //在庫金額
                zaikoKingaku = Util.ToDecimal(zaikoKingakuKei) * tanka;

                drOutput["CHOBO_KINGAKU"] = Util.FormatNum(choboKingaku);
                drOutput["SAI_KINGAKU"] = Util.FormatNum(saiKingaku);
                drOutput["ZAIKO_KINGAKU"] = Util.FormatNum(zaikoKingaku);

                // 合計値を設定
                irisuGokei += Util.ToDecimal(drShohinZaiko["IRISU"]);
                sokoGokei += 0;
                choboCaseSuGokei += choboCaseSu;
                choboBaraSuGokei += choboBaraSu;
                choboKingakuGokei += choboKingaku;
                tanaoroshiCaseSuGokei += tanaoroshiCaseSu;
                tanaoroshiBaraSuGokei += tanaoroshiBaraSu;
                saiCaseSuGokei += saiCaseSu;
                saiBaraSuGokei += saiBaraSu;
                saiKingakuGokei += Util.ToDecimal(Util.FormatNum(saiKingaku));
                zaikoKingakuGokei += zaikoKingaku;

                drOutput["IRISU_GOKEI"] = Util.FormatNum(irisuGokei);
                drOutput["SOKO_GOKEI"] = Util.FormatNum(sokoGokei);
                drOutput["CHOBO_CASE_SU_GOKEI"] = Util.FormatNum(choboCaseSuGokei, 2);
                drOutput["CHOBO_BARA_SU_GOKEI"] = Util.FormatNum(choboBaraSuGokei, 2);
                drOutput["CHOBO_KINGAKU_GOKEI"] = Util.FormatNum(choboKingakuGokei, 2);
                drOutput["TANAOROSHI_CASE_SU_GOKEI"] = Util.FormatNum(tanaoroshiCaseSuGokei, 2);
                drOutput["TANAOROSHI_BARA_SU_GOKEI"] = Util.FormatNum(tanaoroshiBaraSuGokei, 2);
                drOutput["SAI_CASE_SU_GOKEI"] = Util.FormatNum(saiCaseSuGokei, 2);
                drOutput["SAI_BARA_SU_GOKEI"] = Util.FormatNum(saiBaraSuGokei, 2);
                drOutput["SAI_KINGAKU_GOKEI"] = Util.FormatNum(saiKingakuGokei);
                drOutput["ZAIKO_KINGAKU_GOKEI"] = Util.FormatNum(zaikoKingakuGokei);

                // 作成した行データをDataTableに反映
                dtOutput.Rows.Add(drOutput);
            }
            return dtOutput;
        }

        /// <summary>
        /// 商品在庫データを取得します。
        /// </summary>
        /// <returns>取得結果</returns>
        private DataTable GetDataShohinZaiko(int kbn)
        {
            StringBuilder sql = new StringBuilder();
            DbParamCollection dpc = new DbParamCollection();

            sql.Append("SELECT");
            sql.Append(" * ");
            sql.Append("FROM");
            sql.Append(" VI_HN_SHOHIN_ZAIKO ");
            sql.Append("WHERE ");
            sql.Append(" KAISHA_CD = @KAISHA_CD AND");
            if (this.txtMizuageShishoCd.Text != "0")
                sql.Append(" SHISHO_CD = @SHISHO_CD AND");
            sql.Append(" SHOHIN_CD BETWEEN @SHOHIN_CD_FR AND @SHOHIN_CD_TO AND");
            if (kbn == 1)
            {
                sql.Append(" SHOHIN_KUBUN1 BETWEEN @SHOHIN_KUBUN1_FR AND @SHOHIN_KUBUN1_TO AND");
            }
            else if (kbn == 2)
            {
                sql.Append(" SHOHIN_KUBUN2 BETWEEN @SHOHIN_KUBUN1_FR AND @SHOHIN_KUBUN1_TO AND");
            }
            else
            {
                sql.Append(" SHOHIN_KUBUN3 BETWEEN @SHOHIN_KUBUN1_FR AND @SHOHIN_KUBUN1_TO AND");
            }
            sql.Append(" SHOHIN_KUBUN5 <> 1 AND");
            if (!ValChk.IsEmpty(this.txtTanabanFr.Text))
            {
                sql.Append(" TANABAN >= @TANABAN_FR AND");
                dpc.SetParam("@TANABAN_FR", SqlDbType.VarChar, 6, this.txtTanabanFr.Text);
            }
            if (!ValChk.IsEmpty(this.txtTanabanTo.Text))
            {
                sql.Append(" TANABAN <= @TANABAN_TO AND");
                dpc.SetParam("@TANABAN_TO", SqlDbType.VarChar, 6, this.txtTanabanTo.Text);
            }
            //中止区分
            int Chushikubun = this.comboBox1.SelectedIndex;
            switch (Chushikubun)
            {
                case 0:
                    // 全ての商品が対象
                    break;
                case 1:
                    // 中止区分1：取引有りの商品が対象
                    sql.Append("     ISNULL(CHUSHI_KUBUN, 0) = 1 AND");
                    break;
                case 2:
                    // 中止区分２：取引中止の商品が対象
                    sql.Append("     ISNULL(CHUSHI_KUBUN, 0) = 2 AND");
                    break;
            }
            sql.Append(" ZAIKO_KANRI_KUBUN = 1 ");
            sql.Append("ORDER BY");
            sql.Append(" TANABAN,");
            sql.Append(" SHOHIN_CD");

            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.txtMizuageShishoCd.Text);
            dpc.SetParam("@SHOHIN_CD_FR", SqlDbType.Decimal, 13, ValChk.IsEmpty(this.txtShohinCdFr.Text) ? 0 : Util.ToDecimal(this.txtShohinCdFr.Text));
            dpc.SetParam("@SHOHIN_CD_TO", SqlDbType.Decimal, 13, ValChk.IsEmpty(this.txtShohinCdTo.Text) ? 9999999999999 : Util.ToDecimal(this.txtShohinCdTo.Text));
            dpc.SetParam("@SHOHIN_KUBUN1_FR", SqlDbType.Decimal, 4, ValChk.IsEmpty(this.txtShohinKbnFr.Text) ? 0 : Util.ToDecimal(this.txtShohinKbnFr.Text));
            dpc.SetParam("@SHOHIN_KUBUN1_TO", SqlDbType.Decimal, 4, ValChk.IsEmpty(this.txtShohinKbnTo.Text) ? 9999 : Util.ToDecimal(this.txtShohinKbnTo.Text));

            DataTable dtResult = this.Dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            return dtResult;
        }

        /// <summary>
        /// 棚卸日以降の仕入数量を取得します。
        /// </summary>
        /// <param name="tanaoroshiDate">棚卸日</param>
        /// <returns>取得結果</returns>
        private DataTable GetDataShiireSuryo(DateTime tanaoroshiDate, int kbn)
        {
            StringBuilder sql = new StringBuilder();
            DbParamCollection dpc = new DbParamCollection();

            sql.Append("SELECT");
            sql.Append(" C.TANABAN   AS 棚番,");
            sql.Append(" A.SHOHIN_CD AS 商品コード,");
            sql.Append(" SUM(CASE WHEN B.DENPYO_KUBUN = 1 THEN");
            sql.Append("  (CASE WHEN B.TORIHIKI_KUBUN2 = 2 THEN (((A.SURYO1 * A.IRISU) + A.SURYO2) * -1)");
            sql.Append("    ELSE ((A.SURYO1 * A.IRISU) + A.SURYO2)");
            sql.Append("    END)");
            sql.Append("   ELSE (CASE WHEN B.TORIHIKI_KUBUN2 = 2 THEN ((A.SURYO1 * A.IRISU) + A.SURYO2)");
            sql.Append("   ELSE (((A.SURYO1 * A.IRISU) + A.SURYO2) * -1)");
            sql.Append("   END)");
            sql.Append("  END) AS 移動数量 ");
            sql.Append("FROM");
            sql.Append(" TB_HN_TORIHIKI_MEISAI AS A ");
            sql.Append("LEFT OUTER JOIN");
            sql.Append(" TB_HN_TORIHIKI_DENPYO AS B ");
            sql.Append("ON");
            sql.Append(" A.KAISHA_CD = B.KAISHA_CD AND");
            sql.Append(" A.SHISHO_CD = B.SHISHO_CD AND");
            sql.Append(" A.KAIKEI_NENDO = B.KAIKEI_NENDO AND");
            sql.Append(" A.DENPYO_KUBUN = B.DENPYO_KUBUN AND");
            sql.Append(" A.DENPYO_BANGO = B.DENPYO_BANGO ");
            sql.Append("LEFT OUTER JOIN");
            sql.Append(" TB_HN_SHOHIN AS C ");
            sql.Append("ON");
            sql.Append(" A.KAISHA_CD = C.KAISHA_CD AND");
            sql.Append(" A.SHISHO_CD = C.SHISHO_CD AND");
            sql.Append(" A.SHOHIN_CD = C.SHOHIN_CD ");
            sql.Append("WHERE");
            sql.Append(" A.KAISHA_CD = @KAISHA_CD AND");
            if (this.txtMizuageShishoCd.Text != "0")
                sql.Append(" A.SHISHO_CD = @SHISHO_CD AND");
            sql.Append(" B.DENPYO_DATE > @TANAOROSHI_DATE AND");
            sql.Append(" A.SHOHIN_CD BETWEEN @SHOHIN_CD_FR AND @SHOHIN_CD_TO AND");
            if (kbn == 1)
            {
                sql.Append(" C.SHOHIN_KUBUN1 BETWEEN @SHOHIN_KUBUN1_FR AND @SHOHIN_KUBUN1_TO AND");
            }
            else if (kbn == 2)
            {
                sql.Append(" C.SHOHIN_KUBUN2 BETWEEN @SHOHIN_KUBUN1_FR AND @SHOHIN_KUBUN1_TO AND");
            }
            else
            {
                sql.Append(" C.SHOHIN_KUBUN3 BETWEEN @SHOHIN_KUBUN1_FR AND @SHOHIN_KUBUN1_TO AND");
            }
            sql.Append(" C.SHOHIN_KUBUN5 <> 1 AND");
            if (!ValChk.IsEmpty(this.txtTanabanFr.Text))
            {
                sql.Append(" C.TANABAN >= @TANABAN_FR AND");
                dpc.SetParam("@TANABAN_FR", SqlDbType.VarChar, 6, this.txtTanabanFr.Text);
            }
            if (!ValChk.IsEmpty(this.txtTanabanTo.Text))
            {
                sql.Append(" C.TANABAN <= @TANABAN_TO AND");
                dpc.SetParam("@TANABAN_TO", SqlDbType.VarChar, 6, this.txtTanabanTo.Text);
            }
            sql.Append(" C.ZAIKO_KANRI_KUBUN = 1 ");
            sql.Append("GROUP BY");
            sql.Append(" C.TANABAN,");
            sql.Append(" A.SHOHIN_CD ");
            sql.Append("ORDER BY");
            sql.Append(" C.TANABAN,");
            sql.Append(" A.SHOHIN_CD");

            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.txtMizuageShishoCd.Text);
            dpc.SetParam("@TANAOROSHI_DATE", SqlDbType.DateTime, tanaoroshiDate);
            dpc.SetParam("@SHOHIN_CD_FR", SqlDbType.Decimal, 13, ValChk.IsEmpty(this.txtShohinCdFr.Text) ? 0 : Util.ToDecimal(this.txtShohinCdFr.Text));
            dpc.SetParam("@SHOHIN_CD_TO", SqlDbType.Decimal, 13, ValChk.IsEmpty(this.txtShohinCdTo.Text) ? 9999999999999 : Util.ToDecimal(this.txtShohinCdTo.Text));
            dpc.SetParam("@SHOHIN_KUBUN1_FR", SqlDbType.Decimal, 4, ValChk.IsEmpty(this.txtShohinKbnFr.Text) ? 0 : Util.ToDecimal(this.txtShohinKbnFr.Text));
            dpc.SetParam("@SHOHIN_KUBUN1_TO", SqlDbType.Decimal, 4, ValChk.IsEmpty(this.txtShohinKbnTo.Text) ? 9999 : Util.ToDecimal(this.txtShohinKbnTo.Text));

            DataTable dtResult = this.Dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            return dtResult;
        }

        /// <summary>
        /// 入出庫数量を取得します。
        /// </summary>
        /// <param name="tanaoroshiDate">棚卸日</param>
        /// <returns>取得結果</returns>
        private DataTable GetDataNyushukkoSuryo(DateTime tanaoroshiDate, int kbn)
        {
            StringBuilder sql = new StringBuilder();
            DbParamCollection dpc = new DbParamCollection();

            sql.Append("SELECT");
            sql.Append(" C.TANABAN   AS 棚番,");
            sql.Append(" A.SHOHIN_CD AS 商品コード,");
            sql.Append(" SUM(CASE WHEN A.DENPYO_KUBUN = 3 THEN ((A.SURYO1 * A.IRISU) + A.SURYO2) * -1");
            sql.Append("  ELSE ((A.SURYO1 * A.IRISU) + A.SURYO2)");
            sql.Append("  END) AS 移動数量 ");
            sql.Append("FROM");
            sql.Append(" TB_HN_NYUSHUKKO_MEISAI AS A ");
            sql.Append("LEFT OUTER JOIN");
            sql.Append(" TB_HN_NYUSHUKKO_DENPYO AS B ");
            sql.Append("ON");
            sql.Append(" A.KAISHA_CD = B.KAISHA_CD AND");
            sql.Append(" A.SHISHO_CD = B.SHISHO_CD AND");
            sql.Append(" A.KAIKEI_NENDO = B.KAIKEI_NENDO AND");
            sql.Append(" A.DENPYO_KUBUN = B.DENPYO_KUBUN AND");
            sql.Append(" A.DENPYO_BANGO = B.DENPYO_BANGO ");
            sql.Append("LEFT OUTER JOIN");
            sql.Append(" TB_HN_SHOHIN AS C ");
            sql.Append("ON");
            sql.Append(" A.KAISHA_CD = C.KAISHA_CD AND");
            sql.Append(" A.SHISHO_CD = C.SHISHO_CD AND");
            sql.Append(" A.SHOHIN_CD = C.SHOHIN_CD ");
            sql.Append("WHERE");
            sql.Append(" A.KAISHA_CD = @KAISHA_CD AND");
            if (this.txtMizuageShishoCd.Text != "0")
                sql.Append(" A.SHISHO_CD = @SHISHO_CD AND");
            sql.Append(" B.DENPYO_DATE > @TANAOROSHI_DATE AND");
            sql.Append(" A.SHOHIN_CD BETWEEN @SHOHIN_CD_FR AND @SHOHIN_CD_TO AND");
            if (kbn == 1)
            {
                sql.Append(" C.SHOHIN_KUBUN1 BETWEEN @SHOHIN_KUBUN1_FR AND @SHOHIN_KUBUN1_TO AND");
            }
            else if (kbn == 2)
            {
                sql.Append(" C.SHOHIN_KUBUN2 BETWEEN @SHOHIN_KUBUN1_FR AND @SHOHIN_KUBUN1_TO AND");
            }
            else
            {
                sql.Append(" C.SHOHIN_KUBUN3 BETWEEN @SHOHIN_KUBUN1_FR AND @SHOHIN_KUBUN1_TO AND");
            }
            sql.Append(" C.SHOHIN_KUBUN5 <> 1 AND");
            if (!ValChk.IsEmpty(this.txtTanabanFr.Text))
            {
                sql.Append(" C.TANABAN >= @TANABAN_FR AND");
                dpc.SetParam("@TANABAN_FR", SqlDbType.VarChar, 6, this.txtTanabanFr.Text);
            }
            if (!ValChk.IsEmpty(this.txtTanabanTo.Text))
            {
                sql.Append(" C.TANABAN <= @TANABAN_TO AND");
                dpc.SetParam("@TANABAN_TO", SqlDbType.VarChar, 6, this.txtTanabanTo.Text);
            }
            sql.Append(" C.ZAIKO_KANRI_KUBUN = 1 AND");
            sql.Append(" (A.DENPYO_KUBUN = 3 OR A.DENPYO_KUBUN = 4) ");
            sql.Append("GROUP BY");
            sql.Append(" C.TANABAN,");
            sql.Append(" A.SHOHIN_CD ");
            sql.Append("ORDER BY");
            sql.Append(" C.TANABAN,");
            sql.Append(" A.SHOHIN_CD");

            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.txtMizuageShishoCd.Text);
            dpc.SetParam("@TANAOROSHI_DATE", SqlDbType.DateTime, tanaoroshiDate);
            dpc.SetParam("@SHOHIN_CD_FR", SqlDbType.Decimal, 13, ValChk.IsEmpty(this.txtShohinCdFr.Text) ? 0 : Util.ToDecimal(this.txtShohinCdFr.Text));
            dpc.SetParam("@SHOHIN_CD_TO", SqlDbType.Decimal, 13, ValChk.IsEmpty(this.txtShohinCdTo.Text) ? 9999999999999 : Util.ToDecimal(this.txtShohinCdTo.Text));
            dpc.SetParam("@SHOHIN_KUBUN1_FR", SqlDbType.Decimal, 4, ValChk.IsEmpty(this.txtShohinKbnFr.Text) ? 0 : Util.ToDecimal(this.txtShohinKbnFr.Text));
            dpc.SetParam("@SHOHIN_KUBUN1_TO", SqlDbType.Decimal, 4, ValChk.IsEmpty(this.txtShohinKbnTo.Text) ? 9999 : Util.ToDecimal(this.txtShohinKbnTo.Text));

            DataTable dtResult = this.Dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            return dtResult;
        }

        /// <summary>
        /// 移動出庫数量を取得します。
        /// </summary>
        /// <param name="tanaoroshiDate">棚卸日</param>
        /// <returns>取得結果</returns>
        private DataTable GetDataIdoShukkoSuryo(DateTime tanaoroshiDate)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT ");
            sql.Append("  C.TANABAN   AS 棚番 ");
            sql.Append(" ,A.SHOHIN_CD AS 商品コード ");
            sql.Append(" ,SUM(((A.SURYO1 * A.IRISU) + A.SURYO2) * -1) AS 移動数量 ");
            sql.Append("FROM ");
            sql.Append("  TB_HN_NYUSHUKKO_MEISAI AS A ");
            sql.Append("LEFT OUTER JOIN TB_HN_NYUSHUKKO_DENPYO AS B ");
            sql.Append("ON A.KAISHA_CD = B.KAISHA_CD ");
            sql.Append("AND A.KAIKEI_NENDO = B.KAIKEI_NENDO ");
            sql.Append("AND A.DENPYO_KUBUN = B.DENPYO_KUBUN ");
            sql.Append("AND A.DENPYO_BANGO = B.DENPYO_BANGO ");
            sql.Append("LEFT OUTER JOIN TB_HN_SHOHIN AS C ");
            sql.Append("ON A.KAISHA_CD = C.KAISHA_CD ");
            sql.Append("AND A.SHOHIN_CD = C.SHOHIN_CD ");
            sql.Append("WHERE ");
            sql.Append("    A.KAISHA_CD = @KAISHA_CD ");
            sql.Append("AND B.DENPYO_DATE > @TANAOROSHI_DATE ");
            sql.Append("AND A.SHOHIN_CD BETWEEN @SHOHIN_CD_FR AND @SHOHIN_CD_TO ");
            sql.Append("AND C.SHOHIN_KUBUN1 BETWEEN @SHOHIN_KUBUN1_FR AND @SHOHIN_KUBUN1_TO ");
            sql.Append("AND C.SHOHIN_KUBUN5 <> 1 ");
            if (!ValChk.IsEmpty(this.txtTanabanFr.Text))
            {
                sql.Append("AND C.TANABAN >= @TANABAN_FR ");
            }
            if (!ValChk.IsEmpty(this.txtTanabanTo.Text))
            {
                sql.Append("AND C.TANABAN <= @TANABAN_TO ");
            }
            sql.Append("AND C.ZAIKO_KANRI_KUBUN = 1 ");
            sql.Append("AND B.DENPYO_KUBUN = 7 ");
            sql.Append("GROUP BY ");
            sql.Append("  C.TANABAN ");
            sql.Append(" ,A.SHOHIN_CD ");
            sql.Append("ORDER BY ");
            sql.Append("  C.TANABAN ");
            sql.Append(" ,A.SHOHIN_CD ");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@TANAOROSHI_DATE", SqlDbType.DateTime, tanaoroshiDate);
            dpc.SetParam("@SHOHIN_CD_FR", SqlDbType.Decimal, 13, ValChk.IsEmpty(this.txtShohinCdFr.Text) ? 0 : Util.ToDecimal(this.txtShohinCdFr.Text));
            dpc.SetParam("@SHOHIN_CD_TO", SqlDbType.Decimal, 13, ValChk.IsEmpty(this.txtShohinCdTo.Text) ? 9999999999999 : Util.ToDecimal(this.txtShohinCdTo.Text));
            dpc.SetParam("@SHOHIN_KUBUN1_FR", SqlDbType.Decimal, 4, ValChk.IsEmpty(this.txtShohinKbnFr.Text) ? 0 : Util.ToDecimal(this.txtShohinKbnFr.Text));
            dpc.SetParam("@SHOHIN_KUBUN1_TO", SqlDbType.Decimal, 4, ValChk.IsEmpty(this.txtShohinKbnTo.Text) ? 9999 : Util.ToDecimal(this.txtShohinKbnTo.Text));
            if (!ValChk.IsEmpty(this.txtTanabanFr.Text))
            {
                dpc.SetParam("@TANABAN_FR", SqlDbType.VarChar, 6, this.txtTanabanFr.Text);
            }
            if (!ValChk.IsEmpty(this.txtTanabanTo.Text))
            {
                dpc.SetParam("@TANABAN_TO", SqlDbType.VarChar, 6, this.txtTanabanTo.Text);
            }

            DataTable dtResult = this.Dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            return dtResult;
        }

        /// <summary>
        /// 移動入庫数量を取得します。
        /// </summary>
        /// <param name="tanaoroshiDate">棚卸日</param>
        /// <returns>取得結果</returns>
        private DataTable GetDataIdoNyukoSuryo(DateTime tanaoroshiDate)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT ");
            sql.Append("  C.TANABAN   AS 棚番 ");
            sql.Append(" ,A.SHOHIN_CD AS 商品コード ");
            sql.Append(" ,SUM(((A.SURYO1 * A.IRISU) + A.SURYO2) * 1) AS 移動数量 ");
            sql.Append("FROM ");
            sql.Append("  TB_HN_NYUSHUKKO_MEISAI AS A ");
            sql.Append("LEFT OUTER JOIN TB_HN_NYUSHUKKO_DENPYO AS B ");
            sql.Append("ON A.KAISHA_CD = B.KAISHA_CD ");
            sql.Append("AND A.KAIKEI_NENDO = B.KAIKEI_NENDO ");
            sql.Append("AND A.DENPYO_KUBUN = B.DENPYO_KUBUN ");
            sql.Append("AND A.DENPYO_BANGO = B.DENPYO_BANGO ");
            sql.Append("LEFT OUTER JOIN TB_HN_SHOHIN AS C ");
            sql.Append("ON A.KAISHA_CD = C.KAISHA_CD ");
            sql.Append("AND A.SHOHIN_CD = C.SHOHIN_CD ");
            sql.Append("WHERE ");
            sql.Append("    A.KAISHA_CD = @KAISHA_CD ");
            sql.Append("AND B.DENPYO_DATE > @TANAOROSHI_DATE ");
            sql.Append("AND A.SHOHIN_CD BETWEEN @SHOHIN_CD_FR AND @SHOHIN_CD_TO ");
            sql.Append("AND C.SHOHIN_KUBUN1 BETWEEN @SHOHIN_KUBUN1_FR AND @SHOHIN_KUBUN1_TO ");
            sql.Append("AND C.SHOHIN_KUBUN5 <> 1 ");
            if (!ValChk.IsEmpty(this.txtTanabanFr.Text))
            {
                sql.Append("AND C.TANABAN >= @TANABAN_FR ");
            }
            if (!ValChk.IsEmpty(this.txtTanabanTo.Text))
            {
                sql.Append("AND C.TANABAN <= @TANABAN_TO ");
            }
            sql.Append("AND C.ZAIKO_KANRI_KUBUN = 1 ");
            sql.Append("AND B.DENPYO_KUBUN = 7 ");
            sql.Append("GROUP BY ");
            sql.Append("  C.TANABAN ");
            sql.Append(" ,A.SHOHIN_CD ");
            sql.Append("ORDER BY ");
            sql.Append("  C.TANABAN ");
            sql.Append(" ,A.SHOHIN_CD ");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@TANAOROSHI_DATE", SqlDbType.DateTime, tanaoroshiDate);
            dpc.SetParam("@SHOHIN_CD_FR", SqlDbType.Decimal, 13, ValChk.IsEmpty(this.txtShohinCdFr.Text) ? 0 : Util.ToDecimal(this.txtShohinCdFr.Text));
            dpc.SetParam("@SHOHIN_CD_TO", SqlDbType.Decimal, 13, ValChk.IsEmpty(this.txtShohinCdTo.Text) ? 9999999999999 : Util.ToDecimal(this.txtShohinCdTo.Text));
            dpc.SetParam("@SHOHIN_KUBUN1_FR", SqlDbType.Decimal, 4, ValChk.IsEmpty(this.txtShohinKbnFr.Text) ? 0 : Util.ToDecimal(this.txtShohinKbnFr.Text));
            dpc.SetParam("@SHOHIN_KUBUN1_TO", SqlDbType.Decimal, 4, ValChk.IsEmpty(this.txtShohinKbnTo.Text) ? 9999 : Util.ToDecimal(this.txtShohinKbnTo.Text));
            if (!ValChk.IsEmpty(this.txtTanabanFr.Text))
            {
                dpc.SetParam("@TANABAN_FR", SqlDbType.VarChar, 6, this.txtTanabanFr.Text);
            }
            if (!ValChk.IsEmpty(this.txtTanabanTo.Text))
            {
                dpc.SetParam("@TANABAN_TO", SqlDbType.VarChar, 6, this.txtTanabanTo.Text);
            }

            DataTable dtResult = this.Dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            return dtResult;
        }

        /// <summary>
        /// 棚卸数量を取得します。
        /// </summary>
        /// <returns>取得結果</returns>
        private DataTable GetDataTanaoroshi(DateTime tanaoroshiDate, int kbn)
        {
            StringBuilder sql = new StringBuilder();
            DbParamCollection dpc = new DbParamCollection();

            sql.Append("SELECT");
            sql.Append(" A.KAISHA_CD AS 会社コード,");
            sql.Append(" A.TANAOROSHI_DATE AS 棚卸日付,");
            sql.Append(" A.SHOHIN_CD AS 商品コード,");
            sql.Append(" A.GEN_SURYO1 AS 現数量１,");
            sql.Append(" A.GEN_SURYO2 AS 現数量２,");
            sql.Append(" A.JITSU_SURYO1 AS 実数量１,");
            sql.Append(" A.JITSU_SURYO2 AS 実数量２,");
            sql.Append(" A.SA_SURYO1 AS 差数量１,");
            sql.Append(" A.SA_SURYO2 AS 差数量２,");
            sql.Append(" B.TANABAN AS 棚番 ");
            sql.Append("FROM");
            sql.Append(" TB_HN_TANAOROSHI AS A ");
            sql.Append("LEFT OUTER JOIN");
            sql.Append(" TB_HN_SHOHIN AS B ");
            sql.Append("ON");
            sql.Append(" A.KAISHA_CD = B.KAISHA_CD AND");
            sql.Append(" A.SHISHO_CD = B.SHISHO_CD AND");
            sql.Append(" A.SHOHIN_CD = B.SHOHIN_CD AND");
            sql.Append(" B.ZAIKO_KANRI_KUBUN = 1 ");
            sql.Append("WHERE");
            sql.Append(" A.KAISHA_CD = @KAISHA_CD AND");
            if (this.txtMizuageShishoCd.Text != "0")
                sql.Append(" A.SHISHO_CD = @SHISHO_CD AND");
            sql.Append(" A.SHOHIN_CD BETWEEN @SHOHIN_CD_FR AND @SHOHIN_CD_TO AND");

            if (kbn == 1)
            {
                sql.Append(" B.SHOHIN_KUBUN1 BETWEEN @SHOHIN_KUBUN1_FR AND @SHOHIN_KUBUN1_TO AND");
            }
            else if (kbn == 2)
            {
                sql.Append(" B.SHOHIN_KUBUN2 BETWEEN @SHOHIN_KUBUN1_FR AND @SHOHIN_KUBUN1_TO AND");
            }
            else
            {
                sql.Append(" B.SHOHIN_KUBUN3 BETWEEN @SHOHIN_KUBUN1_FR AND @SHOHIN_KUBUN1_TO AND");
            }

            sql.Append(" B.SHOHIN_KUBUN5 <> 1 AND");
            if (!ValChk.IsEmpty(this.txtTanabanFr.Text))
            {
                sql.Append(" B.TANABAN >= @TANABAN_FR AND");
                dpc.SetParam("@TANABAN_FR", SqlDbType.VarChar, 6, this.txtTanabanFr.Text);
            }
            if (!ValChk.IsEmpty(this.txtTanabanTo.Text))
            {
                sql.Append(" B.TANABAN <= @TANABAN_TO AND");
                dpc.SetParam("@TANABAN_TO", SqlDbType.VarChar, 6, this.txtTanabanTo.Text);
            }
            sql.Append(" A.TANAOROSHI_DATE = @TANAOROSHI_DATE ");
            sql.Append("ORDER BY");
            sql.Append(" B.TANABAN,");
            sql.Append(" A.SHOHIN_CD");

            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.txtMizuageShishoCd.Text);
            dpc.SetParam("@TANAOROSHI_DATE", SqlDbType.DateTime, tanaoroshiDate);
            dpc.SetParam("@SHOHIN_CD_FR", SqlDbType.Decimal, 13, ValChk.IsEmpty(this.txtShohinCdFr.Text) ? 0 : Util.ToDecimal(this.txtShohinCdFr.Text));
            dpc.SetParam("@SHOHIN_CD_TO", SqlDbType.Decimal, 13, ValChk.IsEmpty(this.txtShohinCdTo.Text) ? 9999999999999 : Util.ToDecimal(this.txtShohinCdTo.Text));
            dpc.SetParam("@SHOHIN_KUBUN1_FR", SqlDbType.Decimal, 4, ValChk.IsEmpty(this.txtShohinKbnFr.Text) ? 0 : Util.ToDecimal(this.txtShohinKbnFr.Text));
            dpc.SetParam("@SHOHIN_KUBUN1_TO", SqlDbType.Decimal, 4, ValChk.IsEmpty(this.txtShohinKbnTo.Text) ? 9999 : Util.ToDecimal(this.txtShohinKbnTo.Text));

            DataTable dtResult = this.Dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            return dtResult;
        }
        
        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool InsertWkData(DataTable dtOutput)
        {
            // 入力された情報を元にワークテーブルに更新をする
            // 表示日付設定
            string hyojiDate = this.lblDateGengo.Text + this.txtDateYear.Text + "年" + this.txtDateMonth.Text + "月" + this.txtDateDay.Text +"日";

            DbParamCollection dpc = new DbParamCollection();
            StringBuilder Sql = new StringBuilder();

            if (dtOutput.Rows.Count == 0)
            {
                Msg.Info("該当データがありません。");
                return false;
            }
            else
            {
                #region データ準備
                // 帳票タイトル設定
                string title;
                if (this.rdoOutput1.Checked)
                {
                    title = "棚卸表";
                }
                else
                {
                    title = "記入表";
                }
                // 出力順位設定
                string juni;
                if (this.rdoOutputRank1.Checked)
                {
                    juni = "【商品倉庫】";
                }
                else if (this.rdoOutputRank2.Checked)
                {
                    juni = "【倉庫商品】";
                }
                else
                {
                    juni = "【商品合計】";
                }
                
                #region 設定ファイルから設定値を取得
                string tanaoroshiInji = "";
                if (Util.ToInt(this.Config.LoadPgConfig(Constants.SubSys.Kob, "TANAOROSHI", "Setting", "PrintGokei")) == 0)
                {
                    tanaoroshiInji = "0";
                }
                else
                {
                    tanaoroshiInji = "1";
                }
                #endregion

                int i = 0; // ループ用カウント変数
                int j = dtOutput.Rows.Count - 1; // 合計値表示用変数
                #endregion

                while (dtOutput.Rows.Count > i)
                {

                    Sql = new StringBuilder();
                    dpc = new DbParamCollection();

                    if (!(rdoZeroOutput2.Checked &&
                            Util.ToDecimal(dtOutput.Rows[i]["SAI_CASE_SU"]) == 0 &&
                                Util.ToDecimal(dtOutput.Rows[i]["SAI_BARA_SU"]) == 0))
                    {
                        #region インサートテーブル
                        Sql.Append("INSERT INTO PR_HN_TBL(");
                        Sql.Append("  GUID");
                        Sql.Append(" ,SORT");
                        Sql.Append(" ," + Util.ColsArray(prtCols, ""));
                        Sql.Append(") ");
                        Sql.Append("VALUES(");
                        Sql.Append("  @GUID");
                        Sql.Append(" ,@SORT");
                        Sql.Append(" ," + Util.ColsArray(prtCols, "@"));
                        Sql.Append(") ");
                        #endregion

                        #region データ登録
                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                        dpc.SetParam("@SORT", SqlDbType.VarChar, 4, i);
                        dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, title);//棚卸表か記入表
                        dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);//会社名
                        dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, hyojiDate);//日付
                        dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, juni);//出力順位
                        dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, dtOutput.Rows[i]["TANABAN"]);//棚番
                        dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, dtOutput.Rows[i]["SHOHIN_CD"]);//商品コード
                        dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, dtOutput.Rows[i]["SHOHIN_NM"]);//商品名
                        dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, dtOutput.Rows[i]["KIKAKU"]);//規格
                        dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, dtOutput.Rows[i]["IRISU"]);//入数
                        dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, dtOutput.Rows[i]["SOKO"]);//倉庫
                        dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, dtOutput.Rows[i]["CHOBO_CASE_SU"]);//帳簿ケース数
                        dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, dtOutput.Rows[i]["CHOBO_BARA_SU"]);//帳簿バラ数
                        dpc.SetParam("@ITEM13", SqlDbType.VarChar, 200, dtOutput.Rows[i]["CHOBO_KINGAKU"]);//帳簿金額
                        dpc.SetParam("@ITEM14", SqlDbType.VarChar, 200, dtOutput.Rows[i]["TANAOROSHI_CASE_SU"]);//棚卸ケース数
                        dpc.SetParam("@ITEM15", SqlDbType.VarChar, 200, dtOutput.Rows[i]["TANAOROSHI_BARA_SU"]);//棚卸バラ数
                        dpc.SetParam("@ITEM16", SqlDbType.VarChar, 200, dtOutput.Rows[i]["SAI_CASE_SU"]);//差異ケース数
                        dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, dtOutput.Rows[i]["SAI_BARA_SU"]);//差異バラ数
                        dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, dtOutput.Rows[i]["SAI_KINGAKU"]);//差異金額
                        dpc.SetParam("@ITEM19", SqlDbType.VarChar, 200, dtOutput.Rows[i]["ZAIKO_KINGAKU"]);//在庫金額


                        //dpc.SetParam("@ITEM20", SqlDbType.VarChar, 200, Util.FormatNum(dtOutput.Rows[j]["IRISU_GOKEI"]));//入数合計
                        //dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, Util.FormatNum(dtOutput.Rows[j]["SOKO_GOKEI"]));//倉庫合計
                        dpc.SetParam("@ITEM20", SqlDbType.VarChar, 200, 0);//入数合計
                        dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, 0);//倉庫合計
                        if (Util.ToInt(tanaoroshiInji) == 0)
                        { 
                            dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, Util.FormatNum(dtOutput.Rows[j]["CHOBO_CASE_SU_GOKEI"]));//帳簿ケース数合計
                            if (rdoPaper1.Checked)
                            {
                                //dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, Util.FormatNum(dtOutput.Rows[j]["CHOBO_BARA_SU_GOKEI"], 1));//帳簿バラ数合計
                                dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, Util.FormatNum(dtOutput.Rows[j]["CHOBO_BARA_SU_GOKEI"], 2));//帳簿バラ数合計
                            }
                            else
                            {
                                //dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, Util.FormatNum(dtOutput.Rows[j]["CHOBO_BARA_SU_GOKEI"], 1));//帳簿バラ数合計
                                dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, Util.FormatNum(dtOutput.Rows[j]["CHOBO_BARA_SU_GOKEI"], 2));//帳簿バラ数合計
                            }
                            dpc.SetParam("@ITEM25", SqlDbType.VarChar, 200, Util.FormatNum(dtOutput.Rows[j]["TANAOROSHI_CASE_SU_GOKEI"]));//棚卸ケース数合計
                            //dpc.SetParam("@ITEM26", SqlDbType.VarChar, 200, Util.FormatNum(dtOutput.Rows[j]["TANAOROSHI_BARA_SU_GOKEI"], 1));//棚卸バラ数計
                            dpc.SetParam("@ITEM26", SqlDbType.VarChar, 200, Util.FormatNum(dtOutput.Rows[j]["TANAOROSHI_BARA_SU_GOKEI"], 2));//棚卸バラ数計
                            dpc.SetParam("@ITEM27", SqlDbType.VarChar, 200, Util.FormatNum(dtOutput.Rows[j]["SAI_CASE_SU_GOKEI"]));//差異ケース数計
                            //dpc.SetParam("@ITEM28", SqlDbType.VarChar, 200, Util.FormatNum(dtOutput.Rows[j]["SAI_BARA_SU_GOKEI"], 1));//差異バラ数合計
                            dpc.SetParam("@ITEM28", SqlDbType.VarChar, 200, Util.FormatNum(dtOutput.Rows[j]["SAI_BARA_SU_GOKEI"], 2));//差異バラ数合計
                        }
                        else
                        {
                            dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, 0);//帳簿ケース数合計
                            if (rdoPaper1.Checked)
                            {
                                dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, 0);//帳簿バラ数合計
                            }
                            else
                            {
                                dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, 0);//帳簿バラ数合計
                            }
                            dpc.SetParam("@ITEM25", SqlDbType.VarChar, 200, 0);//棚卸ケース数合計
                            dpc.SetParam("@ITEM26", SqlDbType.VarChar, 200, 0);//棚卸バラ数計
                            dpc.SetParam("@ITEM27", SqlDbType.VarChar, 200, 0);//差異ケース数計
                            dpc.SetParam("@ITEM28", SqlDbType.VarChar, 200, 0);//差異バラ数合計
                        }
                        dpc.SetParam("@ITEM24", SqlDbType.VarChar, 200, Util.FormatNum(dtOutput.Rows[j]["CHOBO_KINGAKU_GOKEI"]));//帳簿金額合計
                        dpc.SetParam("@ITEM29", SqlDbType.VarChar, 200, Util.FormatNum(dtOutput.Rows[j]["SAI_KINGAKU_GOKEI"]));//差異金額合計
                        dpc.SetParam("@ITEM30", SqlDbType.VarChar, 200, Util.FormatNum(dtOutput.Rows[j]["ZAIKO_KINGAKU_GOKEI"]));//在庫金額合計

                        this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                        #endregion
                    }

                    i++;
                }
            }

            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
                "SORT",
                "PR_HN_TBL",
                "GUID = @GUID",
                dpc);

            bool dataFlag;
            if (tmpdtPR_HN_TBL.Rows.Count > 0)
            {
                dataFlag = true;
            }
            else
            {
                dataFlag = false;
            }

            return dataFlag;
        }

        /// <summary>
        /// 設定内容によって項目の使用可否を再度制御します。
        /// </summary>
        private void ControlItemsBySettings()
        {
            if (gSettei.kbn == 1)
            {
                this.lblShohinKbnCap.Text = "商品区分1";
            }
            else if (gSettei.kbn == 2)
            {
                this.lblShohinKbnCap.Text = "商品区分2";
            }
            else
            {
                this.lblShohinKbnCap.Text = "商品分類";
            }
        }

        private void GetSettei()
        {
            // 使用する商品区分取得
            gSettei.kbn = Util.ToInt(this.Config.LoadPgConfig(Constants.SubSys.Kob, "TANAOROSHI", "Setting", "ShohinKbn"));
            // 在庫金額計算用単価
            gSettei.tanka = Util.ToInt(this.Config.LoadPgConfig(Constants.SubSys.Kob, "TANAOROSHI", "Setting", "UseTanka"));
            // 棚卸表・記入票の合計印字設定
            gSettei.gokei = Util.ToInt(this.Config.LoadPgConfig(Constants.SubSys.Kob, "TANAOROSHI", "Setting", "PrintGokei"));
        }

        private DataRow getSaishuSiireTanka(DateTime tanaoroshiDate, DataRow drShohinZaiko)
        {
            #region 最終仕入単価選択時 取得用SQL
            // の検索日付に発生しているデータを取得
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder Sql = new StringBuilder();
            Sql.Append(" EXEC SP_SAISHU_SHIIRE_TANKA @KAISHA_CD, @SHISHO_CD, @SHOHIN_CD, @TANAOROSHI_DATE");

            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.txtMizuageShishoCd.Text);
            // 検索する棚卸日付をセット
            dpc.SetParam("@TANAOROSHI_DATE", SqlDbType.VarChar, 10, tanaoroshiDate);
            // 検索する商品コードをセット
            dpc.SetParam("@SHOHIN_CD", SqlDbType.VarChar, 6, drShohinZaiko["SHOHIN_CD"]);
            DataTable dtSaishushiireTanka = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            // 最終仕入単価を取得
            DataRow drSaishushiireTanka;
            if (dtSaishushiireTanka.Rows.Count != 0)
            {
                drSaishushiireTanka = dtSaishushiireTanka.Rows[0];
            }
            else
            {
                drSaishushiireTanka = drShohinZaiko;
            }
            #endregion
            return drSaishushiireTanka;
        }

        private void SetSuryo(decimal pIriSu, ref decimal pCsSu, ref decimal pBrSu)
        {
            decimal wkCsSu;
            decimal wkSu;
            if (pIriSu > 1)
            {
                wkSu = pCsSu * pIriSu + pBrSu;
                wkCsSu = (wkSu / pIriSu);
                pCsSu = TaxUtil.CalcFraction(wkCsSu, 0, TaxUtil.ROUND_CATEGORY.DOWN);
                pBrSu = wkSu - (pCsSu * pIriSu);
            }
            else
            {
                wkSu = pCsSu + pBrSu;
                pCsSu = 0;
                pBrSu = wkSu;
            }
        }

        private decimal GetBaraSu(decimal CsSu, decimal BrSu, decimal Irisu)
        {
            decimal wSu;

            wSu = CsSu * Irisu + BrSu;
            return wSu;
        }

        private string setTitle()
        {
            string title;
            if (rdoOutput1.Checked)
            {
                title = "棚卸表";
            }
            else
            {
                title = "記入表";
            }
            if (rdoOutputRank1.Checked)
            {
                title += " (商品倉庫順";
            }
            else if(rdoOutputRank2.Checked)
            {
                title += " (倉庫商品順";
            }
            else
            {
                title += " (商品合計";
            }
            if (rdoOutput1.Checked && rdoZeroOutput1.Checked)
            {
                title += "、差ゼロ有り)";
            }
            else if(rdoOutput1.Checked && rdoZeroOutput2.Checked)
            {
                title += "、差ゼロ無し)";
            }
            else
            {
                title += ")";
            }
            return title;
        }
        #endregion

        private void txtDateYear_TextChanged(object sender, EventArgs e)
        {

        }
    }
}