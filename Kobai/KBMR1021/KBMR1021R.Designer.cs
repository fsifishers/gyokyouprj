﻿namespace jp.co.fsi.kb.kbmr1021
{
    /// <summary>
    /// KBMR1021R の概要の説明です。
    /// </summary>
    partial class KBMR1021R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(KBMR1021R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtPageCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCompanyName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblGenzai = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtJuni = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtHyojiDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblTanaban = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblShohinmei = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblIrisu = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSoko = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTyoboSuryo = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTanaorosiSuryo = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSai = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblZaikoKingaku = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblKesusu01 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblBarasu01 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblKesusu02 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblBarasu02 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblKesusu03 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblBarasu03 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.textBox15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox44 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.reportInfo1 = new GrapeCity.ActiveReports.SectionReportModel.ReportInfo();
            this.crossSectionBox1 = new GrapeCity.ActiveReports.SectionReportModel.CrossSectionBox();
            this.crossSectionLine1 = new GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine();
            this.crossSectionLine2 = new GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine();
            this.crossSectionLine3 = new GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine();
            this.crossSectionLine7 = new GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine();
            this.crossSectionLine8 = new GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine();
            this.crossSectionLine9 = new GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine();
            this.crossSectionLine10 = new GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine();
            this.crossSectionLine11 = new GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine();
            this.crossSectionLine12 = new GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine();
            this.crossSectionLine4 = new GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtTanaban = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtShohinCd = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtIrisu = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSouko = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtKesusu01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBarasu01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtKesusu02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBarasu02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtShohinNm = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtKikaku = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBarasu03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtKesusu03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtZaikoKingaku = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line22 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line16 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.txtTotalIrisu = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotalSouko = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotalKesusu01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotalBarasu01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotalKesusu02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotalBarasu02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotalKesusu03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotalBarasu03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotalZaikoKingaku = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.reportHeader1 = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.reportFooter1 = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line18 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line15 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGenzai)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtJuni)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHyojiDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTanaban)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblShohinmei)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblIrisu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSoko)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTyoboSuryo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTanaorosiSuryo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSai)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblZaikoKingaku)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKesusu01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBarasu01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKesusu02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBarasu02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKesusu03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBarasu03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportInfo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTanaban)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShohinCd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIrisu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSouko)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKesusu01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBarasu01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKesusu02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBarasu02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShohinNm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKikaku)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBarasu03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKesusu03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZaikoKingaku)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalIrisu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalSouko)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalKesusu01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalBarasu01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalKesusu02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalBarasu02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalKesusu03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalBarasu03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalZaikoKingaku)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblPage,
            this.txtPageCount,
            this.txtCompanyName,
            this.lblTitle,
            this.lblGenzai,
            this.txtJuni,
            this.txtHyojiDate,
            this.txtTitle,
            this.lblTanaban,
            this.lblShohinmei,
            this.lblIrisu,
            this.lblSoko,
            this.lblTyoboSuryo,
            this.lblTanaorosiSuryo,
            this.lblSai,
            this.lblZaikoKingaku,
            this.lblKesusu01,
            this.lblBarasu01,
            this.lblKesusu02,
            this.lblBarasu02,
            this.lblKesusu03,
            this.lblBarasu03,
            this.line1,
            this.line3,
            this.line13,
            this.textBox15,
            this.textBox20,
            this.textBox44,
            this.textBox9,
            this.reportInfo1,
            this.crossSectionBox1,
            this.crossSectionLine1,
            this.crossSectionLine2,
            this.crossSectionLine3,
            this.crossSectionLine7,
            this.crossSectionLine8,
            this.crossSectionLine9,
            this.crossSectionLine10,
            this.crossSectionLine11,
            this.crossSectionLine12,
            this.crossSectionLine4,
            this.label1,
            this.label2,
            this.label3});
            this.pageHeader.Height = 1.270472F;
            this.pageHeader.Name = "pageHeader";
            // 
            // lblPage
            // 
            this.lblPage.Height = 0.2070866F;
            this.lblPage.HyperLink = null;
            this.lblPage.Left = 7.265748F;
            this.lblPage.Name = "lblPage";
            this.lblPage.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle";
            this.lblPage.Text = "頁";
            this.lblPage.Top = 0.2023622F;
            this.lblPage.Width = 0.1590552F;
            // 
            // txtPageCount
            // 
            this.txtPageCount.Height = 0.2070866F;
            this.txtPageCount.Left = 7.022441F;
            this.txtPageCount.MultiLine = false;
            this.txtPageCount.Name = "txtPageCount";
            this.txtPageCount.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle";
            this.txtPageCount.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtPageCount.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.txtPageCount.Text = "999";
            this.txtPageCount.Top = 0.2023622F;
            this.txtPageCount.Width = 0.2433071F;
            // 
            // txtCompanyName
            // 
            this.txtCompanyName.DataField = "ITEM02";
            this.txtCompanyName.Height = 0.1968504F;
            this.txtCompanyName.Left = 0.05590552F;
            this.txtCompanyName.MultiLine = false;
            this.txtCompanyName.Name = "txtCompanyName";
            this.txtCompanyName.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; vertical-align: middle";
            this.txtCompanyName.Text = "2";
            this.txtCompanyName.Top = 0.1637795F;
            this.txtCompanyName.Width = 2.764961F;
            // 
            // lblTitle
            // 
            this.lblTitle.Height = 0.3968504F;
            this.lblTitle.HyperLink = null;
            this.lblTitle.Left = 0.01968504F;
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Style = "background-color: Cyan; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: nor" +
    "mal; text-align: center";
            this.lblTitle.Text = "";
            this.lblTitle.Top = 0.8625984F;
            this.lblTitle.Width = 7.700788F;
            // 
            // lblGenzai
            // 
            this.lblGenzai.Height = 0.2F;
            this.lblGenzai.HyperLink = null;
            this.lblGenzai.Left = 4.255512F;
            this.lblGenzai.Name = "lblGenzai";
            this.lblGenzai.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle";
            this.lblGenzai.Text = "現在";
            this.lblGenzai.Top = 0.3472441F;
            this.lblGenzai.Width = 0.3255906F;
            // 
            // txtJuni
            // 
            this.txtJuni.DataField = "ITEM04";
            this.txtJuni.Height = 0.2F;
            this.txtJuni.Left = 6.309449F;
            this.txtJuni.MultiLine = false;
            this.txtJuni.Name = "txtJuni";
            this.txtJuni.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt";
            this.txtJuni.Text = "04";
            this.txtJuni.Top = 0.5472441F;
            this.txtJuni.Width = 1.115355F;
            // 
            // txtHyojiDate
            // 
            this.txtHyojiDate.DataField = "ITEM03";
            this.txtHyojiDate.Height = 0.2F;
            this.txtHyojiDate.Left = 2.905906F;
            this.txtHyojiDate.Name = "txtHyojiDate";
            this.txtHyojiDate.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle";
            this.txtHyojiDate.Text = "平成27年15月14日";
            this.txtHyojiDate.Top = 0.3472441F;
            this.txtHyojiDate.Width = 1.349607F;
            // 
            // txtTitle
            // 
            this.txtTitle.DataField = "ITEM01";
            this.txtTitle.Height = 0.2464567F;
            this.txtTitle.Left = 2.833465F;
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Style = "font-family: ＭＳ 明朝; font-size: 15.75pt; font-weight: bold; text-align: center";
            this.txtTitle.Text = "01";
            this.txtTitle.Top = 0F;
            this.txtTitle.Width = 2.018898F;
            // 
            // lblTanaban
            // 
            this.lblTanaban.Height = 0.2000001F;
            this.lblTanaban.HyperLink = null;
            this.lblTanaban.Left = 0.01181102F;
            this.lblTanaban.LineSpacing = 1F;
            this.lblTanaban.MultiLine = false;
            this.lblTanaban.Name = "lblTanaban";
            this.lblTanaban.Style = "background-color: Cyan; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font" +
    "-weight: normal; text-align: center; vertical-align: middle";
            this.lblTanaban.Text = "棚番";
            this.lblTanaban.Top = 0.9740158F;
            this.lblTanaban.Width = 0.3834646F;
            // 
            // lblShohinmei
            // 
            this.lblShohinmei.Height = 0.2000001F;
            this.lblShohinmei.HyperLink = null;
            this.lblShohinmei.Left = 0.4043307F;
            this.lblShohinmei.LineSpacing = 1F;
            this.lblShohinmei.MultiLine = false;
            this.lblShohinmei.Name = "lblShohinmei";
            this.lblShohinmei.Style = "background-color: Cyan; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fon" +
    "t-weight: bold; text-align: center; vertical-align: middle";
            this.lblShohinmei.Text = "商  品  名";
            this.lblShohinmei.Top = 0.9740158F;
            this.lblShohinmei.Width = 2.105118F;
            // 
            // lblIrisu
            // 
            this.lblIrisu.Height = 0.2F;
            this.lblIrisu.HyperLink = null;
            this.lblIrisu.Left = 2.509449F;
            this.lblIrisu.LineSpacing = 1F;
            this.lblIrisu.MultiLine = false;
            this.lblIrisu.Name = "lblIrisu";
            this.lblIrisu.Style = "background-color: Cyan; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font" +
    "-weight: normal; text-align: center; vertical-align: middle";
            this.lblIrisu.Text = "入数";
            this.lblIrisu.Top = 0.9740158F;
            this.lblIrisu.Width = 0.3114176F;
            // 
            // lblSoko
            // 
            this.lblSoko.Height = 0.2000001F;
            this.lblSoko.HyperLink = null;
            this.lblSoko.Left = 2.820866F;
            this.lblSoko.LineSpacing = 1F;
            this.lblSoko.MultiLine = false;
            this.lblSoko.Name = "lblSoko";
            this.lblSoko.Style = "background-color: Cyan; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font" +
    "-weight: normal; text-align: center; vertical-align: middle";
            this.lblSoko.Text = "倉庫";
            this.lblSoko.Top = 0.9740158F;
            this.lblSoko.Width = 0.3350394F;
            // 
            // lblTyoboSuryo
            // 
            this.lblTyoboSuryo.Height = 0.1968504F;
            this.lblTyoboSuryo.HyperLink = null;
            this.lblTyoboSuryo.Left = 3.207874F;
            this.lblTyoboSuryo.LineSpacing = 1F;
            this.lblTyoboSuryo.MultiLine = false;
            this.lblTyoboSuryo.Name = "lblTyoboSuryo";
            this.lblTyoboSuryo.Style = "background-color: Cyan; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fon" +
    "t-weight: bold; text-align: center; text-justify: auto; vertical-align: bottom";
            this.lblTyoboSuryo.Text = "帳簿数量";
            this.lblTyoboSuryo.Top = 0.8625985F;
            this.lblTyoboSuryo.Width = 1.193701F;
            // 
            // lblTanaorosiSuryo
            // 
            this.lblTanaorosiSuryo.Height = 0.1968504F;
            this.lblTanaorosiSuryo.HyperLink = null;
            this.lblTanaorosiSuryo.Left = 4.401575F;
            this.lblTanaorosiSuryo.LineSpacing = 1F;
            this.lblTanaorosiSuryo.MultiLine = false;
            this.lblTanaorosiSuryo.Name = "lblTanaorosiSuryo";
            this.lblTanaorosiSuryo.Style = "background-color: Cyan; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fon" +
    "t-weight: bold; text-align: center; vertical-align: bottom";
            this.lblTanaorosiSuryo.Text = "棚卸数量";
            this.lblTanaorosiSuryo.Top = 0.8625985F;
            this.lblTanaorosiSuryo.Width = 1.25945F;
            // 
            // lblSai
            // 
            this.lblSai.Height = 0.1968504F;
            this.lblSai.HyperLink = null;
            this.lblSai.Left = 5.661024F;
            this.lblSai.LineSpacing = 1F;
            this.lblSai.MultiLine = false;
            this.lblSai.Name = "lblSai";
            this.lblSai.Style = "background-color: Cyan; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fon" +
    "t-weight: bold; text-align: center; vertical-align: bottom";
            this.lblSai.Text = "差    異";
            this.lblSai.Top = 0.8625985F;
            this.lblSai.Width = 1.244488F;
            // 
            // lblZaikoKingaku
            // 
            this.lblZaikoKingaku.Height = 0.2F;
            this.lblZaikoKingaku.HyperLink = null;
            this.lblZaikoKingaku.Left = 6.927166F;
            this.lblZaikoKingaku.LineSpacing = 1F;
            this.lblZaikoKingaku.MultiLine = false;
            this.lblZaikoKingaku.Name = "lblZaikoKingaku";
            this.lblZaikoKingaku.Style = "background-color: Cyan; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fon" +
    "t-weight: bold; text-align: center; vertical-align: middle";
            this.lblZaikoKingaku.Text = "在庫金額";
            this.lblZaikoKingaku.Top = 0.9740158F;
            this.lblZaikoKingaku.Width = 0.7933068F;
            // 
            // lblKesusu01
            // 
            this.lblKesusu01.Height = 0.2F;
            this.lblKesusu01.HyperLink = null;
            this.lblKesusu01.Left = 3.168504F;
            this.lblKesusu01.LineSpacing = 1F;
            this.lblKesusu01.MultiLine = false;
            this.lblKesusu01.Name = "lblKesusu01";
            this.lblKesusu01.Style = "background-color: Cyan; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font" +
    "-weight: bold; text-align: center; vertical-align: bottom";
            this.lblKesusu01.Text = "ケース数";
            this.lblKesusu01.Top = 1.059449F;
            this.lblKesusu01.Width = 0.5759842F;
            // 
            // lblBarasu01
            // 
            this.lblBarasu01.Height = 0.2F;
            this.lblBarasu01.HyperLink = null;
            this.lblBarasu01.Left = 3.744488F;
            this.lblBarasu01.LineSpacing = 1F;
            this.lblBarasu01.MultiLine = false;
            this.lblBarasu01.Name = "lblBarasu01";
            this.lblBarasu01.Style = "background-color: Cyan; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font" +
    "-weight: bold; text-align: center; vertical-align: bottom";
            this.lblBarasu01.Text = "バラ数";
            this.lblBarasu01.Top = 1.059449F;
            this.lblBarasu01.Width = 0.6570871F;
            // 
            // lblKesusu02
            // 
            this.lblKesusu02.Height = 0.2F;
            this.lblKesusu02.HyperLink = null;
            this.lblKesusu02.Left = 4.401575F;
            this.lblKesusu02.LineSpacing = 1F;
            this.lblKesusu02.MultiLine = false;
            this.lblKesusu02.Name = "lblKesusu02";
            this.lblKesusu02.Style = "background-color: Cyan; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font" +
    "-weight: bold; text-align: center; vertical-align: bottom";
            this.lblKesusu02.Text = "ケース数";
            this.lblKesusu02.Top = 1.059449F;
            this.lblKesusu02.Width = 0.6047249F;
            // 
            // lblBarasu02
            // 
            this.lblBarasu02.Height = 0.2F;
            this.lblBarasu02.HyperLink = null;
            this.lblBarasu02.Left = 5.006299F;
            this.lblBarasu02.LineSpacing = 1F;
            this.lblBarasu02.MultiLine = false;
            this.lblBarasu02.Name = "lblBarasu02";
            this.lblBarasu02.Style = "background-color: Cyan; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font" +
    "-weight: bold; text-align: center; vertical-align: bottom";
            this.lblBarasu02.Text = "バラ数";
            this.lblBarasu02.Top = 1.059449F;
            this.lblBarasu02.Width = 0.6547244F;
            // 
            // lblKesusu03
            // 
            this.lblKesusu03.Height = 0.2F;
            this.lblKesusu03.HyperLink = null;
            this.lblKesusu03.Left = 5.661024F;
            this.lblKesusu03.LineSpacing = 1F;
            this.lblKesusu03.MultiLine = false;
            this.lblKesusu03.Name = "lblKesusu03";
            this.lblKesusu03.Style = "background-color: Cyan; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font" +
    "-weight: bold; text-align: center; vertical-align: bottom";
            this.lblKesusu03.Text = "ケース数";
            this.lblKesusu03.Top = 1.059449F;
            this.lblKesusu03.Width = 0.6047254F;
            // 
            // lblBarasu03
            // 
            this.lblBarasu03.Height = 0.2F;
            this.lblBarasu03.HyperLink = null;
            this.lblBarasu03.Left = 6.265749F;
            this.lblBarasu03.LineSpacing = 1F;
            this.lblBarasu03.MultiLine = false;
            this.lblBarasu03.Name = "lblBarasu03";
            this.lblBarasu03.Style = "background-color: Cyan; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font" +
    "-weight: bold; text-align: center; vertical-align: bottom";
            this.lblBarasu03.Text = "バラ数";
            this.lblBarasu03.Top = 1.059449F;
            this.lblBarasu03.Width = 0.6397638F;
            // 
            // line1
            // 
            this.line1.Height = 0F;
            this.line1.Left = 2.952756F;
            this.line1.LineWeight = 1F;
            this.line1.Name = "line1";
            this.line1.Top = 0.2464567F;
            this.line1.Width = 1.764173F;
            this.line1.X1 = 2.952756F;
            this.line1.X2 = 4.716929F;
            this.line1.Y1 = 0.2464567F;
            this.line1.Y2 = 0.2464567F;
            // 
            // line3
            // 
            this.line3.Height = 0F;
            this.line3.Left = 0.01181102F;
            this.line3.LineWeight = 1F;
            this.line3.Name = "line3";
            this.line3.Top = 1.259449F;
            this.line3.Width = 7.708662F;
            this.line3.X1 = 0.01181102F;
            this.line3.X2 = 7.720473F;
            this.line3.Y1 = 1.259449F;
            this.line3.Y2 = 1.259449F;
            // 
            // line13
            // 
            this.line13.Height = 0F;
            this.line13.Left = 3.155906F;
            this.line13.LineWeight = 1F;
            this.line13.Name = "line13";
            this.line13.Top = 1.059449F;
            this.line13.Width = 3.749606F;
            this.line13.X1 = 3.155906F;
            this.line13.X2 = 6.905512F;
            this.line13.Y1 = 1.059449F;
            this.line13.Y2 = 1.059449F;
            // 
            // textBox15
            // 
            this.textBox15.DataField = "ITEM13";
            this.textBox15.Height = 0.1688976F;
            this.textBox15.Left = 0.05590558F;
            this.textBox15.Name = "textBox15";
            this.textBox15.Style = "font-family: ＭＳ 明朝; font-size: 9pt; text-align: center";
            this.textBox15.Text = "13";
            this.textBox15.Top = 0.4094489F;
            this.textBox15.Visible = false;
            this.textBox15.Width = 0.5948821F;
            // 
            // textBox20
            // 
            this.textBox20.DataField = "ITEM18";
            this.textBox20.Height = 0.1688976F;
            this.textBox20.Left = 0.6507874F;
            this.textBox20.Name = "textBox20";
            this.textBox20.Style = "font-family: ＭＳ 明朝; font-size: 9pt; text-align: center";
            this.textBox20.Text = "18";
            this.textBox20.Top = 0.4094489F;
            this.textBox20.Visible = false;
            this.textBox20.Width = 0.7574803F;
            // 
            // textBox44
            // 
            this.textBox44.DataField = "ITEM24";
            this.textBox44.Height = 0.1688976F;
            this.textBox44.Left = 0.05590558F;
            this.textBox44.MultiLine = false;
            this.textBox44.Name = "textBox44";
            this.textBox44.Style = "font-family: ＭＳ 明朝; font-size: 9pt; text-align: center";
            this.textBox44.Text = "24";
            this.textBox44.Top = 0.5783466F;
            this.textBox44.Visible = false;
            this.textBox44.Width = 0.5948818F;
            // 
            // textBox9
            // 
            this.textBox9.DataField = "ITEM29";
            this.textBox9.Height = 0.1688976F;
            this.textBox9.Left = 0.6507874F;
            this.textBox9.MultiLine = false;
            this.textBox9.Name = "textBox9";
            this.textBox9.Style = "font-family: ＭＳ 明朝; font-size: 9pt; text-align: center";
            this.textBox9.Text = "29";
            this.textBox9.Top = 0.5783466F;
            this.textBox9.Visible = false;
            this.textBox9.Width = 0.75748F;
            // 
            // reportInfo1
            // 
            this.reportInfo1.FormatString = "{RunDateTime:yyyy/MM/dd}";
            this.reportInfo1.Height = 0.2070866F;
            this.reportInfo1.Left = 5.857087F;
            this.reportInfo1.Name = "reportInfo1";
            this.reportInfo1.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
            this.reportInfo1.Top = 0.2023622F;
            this.reportInfo1.Width = 1.165354F;
            // 
            // crossSectionBox1
            // 
            this.crossSectionBox1.Bottom = 0.3070866F;
            this.crossSectionBox1.Left = 0.01181173F;
            this.crossSectionBox1.LineWeight = 1F;
            this.crossSectionBox1.Name = "crossSectionBox1";
            this.crossSectionBox1.Right = 7.720473F;
            this.crossSectionBox1.Top = 0.8625985F;
            // 
            // crossSectionLine1
            // 
            this.crossSectionLine1.Bottom = 0.3070866F;
            this.crossSectionLine1.Left = 0.3952756F;
            this.crossSectionLine1.LineWeight = 1F;
            this.crossSectionLine1.Name = "crossSectionLine1";
            this.crossSectionLine1.Top = 0.8720473F;
            // 
            // crossSectionLine2
            // 
            this.crossSectionLine2.Bottom = 0.3070866F;
            this.crossSectionLine2.Left = 5.006299F;
            this.crossSectionLine2.LineWeight = 1F;
            this.crossSectionLine2.Name = "crossSectionLine2";
            this.crossSectionLine2.Top = 1.059449F;
            // 
            // crossSectionLine3
            // 
            this.crossSectionLine3.Bottom = 0.3070866F;
            this.crossSectionLine3.Left = 3.155905F;
            this.crossSectionLine3.LineWeight = 1F;
            this.crossSectionLine3.Name = "crossSectionLine3";
            this.crossSectionLine3.Top = 0.8720473F;
            // 
            // crossSectionLine7
            // 
            this.crossSectionLine7.Bottom = 0.3070866F;
            this.crossSectionLine7.Left = 5.661024F;
            this.crossSectionLine7.LineWeight = 1F;
            this.crossSectionLine7.Name = "crossSectionLine7";
            this.crossSectionLine7.Top = 0.8625985F;
            // 
            // crossSectionLine8
            // 
            this.crossSectionLine8.Bottom = 0.3070866F;
            this.crossSectionLine8.Left = 2.820866F;
            this.crossSectionLine8.LineWeight = 1F;
            this.crossSectionLine8.Name = "crossSectionLine8";
            this.crossSectionLine8.Top = 0.8625985F;
            // 
            // crossSectionLine9
            // 
            this.crossSectionLine9.Bottom = 0.3070866F;
            this.crossSectionLine9.Left = 6.265748F;
            this.crossSectionLine9.LineWeight = 1F;
            this.crossSectionLine9.Name = "crossSectionLine9";
            this.crossSectionLine9.Top = 1.059449F;
            // 
            // crossSectionLine10
            // 
            this.crossSectionLine10.Bottom = 0.3070866F;
            this.crossSectionLine10.Left = 3.744488F;
            this.crossSectionLine10.LineWeight = 1F;
            this.crossSectionLine10.Name = "crossSectionLine10";
            this.crossSectionLine10.Top = 1.059449F;
            // 
            // crossSectionLine11
            // 
            this.crossSectionLine11.Bottom = 0.3070866F;
            this.crossSectionLine11.Left = 6.90748F;
            this.crossSectionLine11.LineWeight = 1F;
            this.crossSectionLine11.Name = "crossSectionLine11";
            this.crossSectionLine11.Top = 0.8625985F;
            // 
            // crossSectionLine12
            // 
            this.crossSectionLine12.Bottom = 0.3070866F;
            this.crossSectionLine12.Left = 2.509449F;
            this.crossSectionLine12.LineWeight = 1F;
            this.crossSectionLine12.Name = "crossSectionLine12";
            this.crossSectionLine12.Top = 0.8625985F;
            // 
            // crossSectionLine4
            // 
            this.crossSectionLine4.Bottom = 0.3070866F;
            this.crossSectionLine4.Left = 4.401575F;
            this.crossSectionLine4.LineWeight = 1F;
            this.crossSectionLine4.Name = "crossSectionLine4";
            this.crossSectionLine4.Top = 0.8720473F;
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtTanaban,
            this.txtShohinCd,
            this.txtIrisu,
            this.txtSouko,
            this.txtKesusu01,
            this.txtBarasu01,
            this.txtKesusu02,
            this.txtBarasu02,
            this.txtShohinNm,
            this.txtKikaku,
            this.txtBarasu03,
            this.txtKesusu03,
            this.txtZaikoKingaku,
            this.line22,
            this.line16});
            this.detail.Height = 0.3074803F;
            this.detail.Name = "detail";
            this.detail.Format += new System.EventHandler(this.detail_Format);
            // 
            // txtTanaban
            // 
            this.txtTanaban.DataField = "ITEM05";
            this.txtTanaban.Height = 0.1688976F;
            this.txtTanaban.Left = 0.05590552F;
            this.txtTanaban.MultiLine = false;
            this.txtTanaban.Name = "txtTanaban";
            this.txtTanaban.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left";
            this.txtTanaban.Text = "5";
            this.txtTanaban.Top = 0.08346457F;
            this.txtTanaban.Width = 0.3393701F;
            // 
            // txtShohinCd
            // 
            this.txtShohinCd.DataField = "ITEM06";
            this.txtShohinCd.Height = 0.1688976F;
            this.txtShohinCd.Left = 0.4043307F;
            this.txtShohinCd.MultiLine = false;
            this.txtShohinCd.Name = "txtShohinCd";
            this.txtShohinCd.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right";
            this.txtShohinCd.Text = "6";
            this.txtShohinCd.Top = 0F;
            this.txtShohinCd.Width = 0.3696851F;
            // 
            // txtIrisu
            // 
            this.txtIrisu.DataField = "ITEM09";
            this.txtIrisu.Height = 0.1688976F;
            this.txtIrisu.Left = 2.509449F;
            this.txtIrisu.Name = "txtIrisu";
            this.txtIrisu.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle";
            this.txtIrisu.Text = "9";
            this.txtIrisu.Top = 0.08346457F;
            this.txtIrisu.Width = 0.2755907F;
            // 
            // txtSouko
            // 
            this.txtSouko.DataField = "ITEM10";
            this.txtSouko.Height = 0.1688976F;
            this.txtSouko.Left = 2.820866F;
            this.txtSouko.Name = "txtSouko";
            this.txtSouko.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle";
            this.txtSouko.Text = "10";
            this.txtSouko.Top = 0.08346457F;
            this.txtSouko.Width = 0.2783465F;
            // 
            // txtKesusu01
            // 
            this.txtKesusu01.DataField = "ITEM11";
            this.txtKesusu01.Height = 0.1688976F;
            this.txtKesusu01.Left = 3.168504F;
            this.txtKesusu01.Name = "txtKesusu01";
            this.txtKesusu01.Style = "font-family: ＭＳ 明朝; font-size: 9pt; text-align: right; vertical-align: middle";
            this.txtKesusu01.Text = "11";
            this.txtKesusu01.Top = 0.08346457F;
            this.txtKesusu01.Width = 0.5342521F;
            // 
            // txtBarasu01
            // 
            this.txtBarasu01.DataField = "ITEM12";
            this.txtBarasu01.Height = 0.1688976F;
            this.txtBarasu01.Left = 3.744488F;
            this.txtBarasu01.Name = "txtBarasu01";
            this.txtBarasu01.Style = "font-family: ＭＳ 明朝; font-size: 9pt; text-align: right; vertical-align: middle";
            this.txtBarasu01.Text = "123,132.00";
            this.txtBarasu01.Top = 0.08346457F;
            this.txtBarasu01.Width = 0.6362212F;
            // 
            // txtKesusu02
            // 
            this.txtKesusu02.DataField = "ITEM14";
            this.txtKesusu02.Height = 0.1688976F;
            this.txtKesusu02.Left = 4.445276F;
            this.txtKesusu02.Name = "txtKesusu02";
            this.txtKesusu02.Style = "font-family: ＭＳ 明朝; font-size: 9pt; text-align: right; vertical-align: middle";
            this.txtKesusu02.Text = "14";
            this.txtKesusu02.Top = 0.08346457F;
            this.txtKesusu02.Width = 0.5047245F;
            // 
            // txtBarasu02
            // 
            this.txtBarasu02.DataField = "ITEM15";
            this.txtBarasu02.Height = 0.1688976F;
            this.txtBarasu02.Left = 5.006299F;
            this.txtBarasu02.Name = "txtBarasu02";
            this.txtBarasu02.Style = "font-family: ＭＳ 明朝; font-size: 9pt; text-align: right; vertical-align: middle";
            this.txtBarasu02.Text = "123,12.00";
            this.txtBarasu02.Top = 0.08346457F;
            this.txtBarasu02.Width = 0.6236224F;
            // 
            // txtShohinNm
            // 
            this.txtShohinNm.DataField = "ITEM07";
            this.txtShohinNm.Height = 0.1688976F;
            this.txtShohinNm.Left = 0.8468505F;
            this.txtShohinNm.Name = "txtShohinNm";
            this.txtShohinNm.Style = "font-family: ＭＳ 明朝; font-size: 9pt; text-align: left";
            this.txtShohinNm.Text = "7";
            this.txtShohinNm.Top = 0F;
            this.txtShohinNm.Width = 1.662599F;
            // 
            // txtKikaku
            // 
            this.txtKikaku.DataField = "ITEM08";
            this.txtKikaku.Height = 0.1688976F;
            this.txtKikaku.Left = 0.8468505F;
            this.txtKikaku.Name = "txtKikaku";
            this.txtKikaku.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left";
            this.txtKikaku.Text = "8";
            this.txtKikaku.Top = 0.1688976F;
            this.txtKikaku.Width = 1.662599F;
            // 
            // txtBarasu03
            // 
            this.txtBarasu03.DataField = "ITEM17";
            this.txtBarasu03.Height = 0.1688976F;
            this.txtBarasu03.Left = 6.265749F;
            this.txtBarasu03.Name = "txtBarasu03";
            this.txtBarasu03.Style = "font-family: ＭＳ 明朝; font-size: 9pt; text-align: right; vertical-align: middle";
            this.txtBarasu03.Text = "123,12.00";
            this.txtBarasu03.Top = 0.08346457F;
            this.txtBarasu03.Width = 0.6188969F;
            // 
            // txtKesusu03
            // 
            this.txtKesusu03.DataField = "ITEM16";
            this.txtKesusu03.Height = 0.1688976F;
            this.txtKesusu03.Left = 5.730709F;
            this.txtKesusu03.Name = "txtKesusu03";
            this.txtKesusu03.Style = "font-family: ＭＳ 明朝; font-size: 9pt; text-align: right; vertical-align: middle";
            this.txtKesusu03.Text = "16";
            this.txtKesusu03.Top = 0.08346457F;
            this.txtKesusu03.Width = 0.5350389F;
            // 
            // txtZaikoKingaku
            // 
            this.txtZaikoKingaku.DataField = "ITEM19";
            this.txtZaikoKingaku.Height = 0.1688976F;
            this.txtZaikoKingaku.Left = 6.927166F;
            this.txtZaikoKingaku.Name = "txtZaikoKingaku";
            this.txtZaikoKingaku.Style = "font-family: ＭＳ 明朝; font-size: 9pt; text-align: right; vertical-align: middle";
            this.txtZaikoKingaku.Text = "19";
            this.txtZaikoKingaku.Top = 0.08346457F;
            this.txtZaikoKingaku.Width = 0.732677F;
            // 
            // line22
            // 
            this.line22.Height = 0F;
            this.line22.Left = 0.01181102F;
            this.line22.LineWeight = 2F;
            this.line22.Name = "line22";
            this.line22.Top = 0.3149606F;
            this.line22.Width = 7.685039F;
            this.line22.X1 = 0.01181102F;
            this.line22.X2 = 7.69685F;
            this.line22.Y1 = 0.3149606F;
            this.line22.Y2 = 0.3149606F;
            // 
            // line16
            // 
            this.line16.Height = 0F;
            this.line16.Left = 0.0196867F;
            this.line16.LineWeight = 2F;
            this.line16.Name = "line16";
            this.line16.Top = 0.3102362F;
            this.line16.Visible = false;
            this.line16.Width = 7.685035F;
            this.line16.X1 = 0.0196867F;
            this.line16.X2 = 7.704722F;
            this.line16.Y1 = 0.3102362F;
            this.line16.Y2 = 0.3102362F;
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            this.pageFooter.Visible = false;
            // 
            // txtTotalIrisu
            // 
            this.txtTotalIrisu.DataField = "ITEM20";
            this.txtTotalIrisu.Height = 0.1688976F;
            this.txtTotalIrisu.Left = 2.509449F;
            this.txtTotalIrisu.Name = "txtTotalIrisu";
            this.txtTotalIrisu.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
            this.txtTotalIrisu.Text = "20";
            this.txtTotalIrisu.Top = 0.06259843F;
            this.txtTotalIrisu.Width = 0.2755906F;
            // 
            // txtTotalSouko
            // 
            this.txtTotalSouko.DataField = "ITEM21";
            this.txtTotalSouko.Height = 0.1688976F;
            this.txtTotalSouko.Left = 2.820866F;
            this.txtTotalSouko.Name = "txtTotalSouko";
            this.txtTotalSouko.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
            this.txtTotalSouko.Text = "21";
            this.txtTotalSouko.Top = 0.06259843F;
            this.txtTotalSouko.Width = 0.2783465F;
            // 
            // txtTotalKesusu01
            // 
            this.txtTotalKesusu01.DataField = "ITEM22";
            this.txtTotalKesusu01.Height = 0.1688976F;
            this.txtTotalKesusu01.Left = 3.168504F;
            this.txtTotalKesusu01.Name = "txtTotalKesusu01";
            this.txtTotalKesusu01.Style = "font-family: ＭＳ 明朝; font-size: 9pt; text-align: right; vertical-align: middle; dd" +
    "o-char-set: 1";
            this.txtTotalKesusu01.Text = "22";
            this.txtTotalKesusu01.Top = 0.06259843F;
            this.txtTotalKesusu01.Width = 0.5342521F;
            // 
            // txtTotalBarasu01
            // 
            this.txtTotalBarasu01.DataField = "ITEM23";
            this.txtTotalBarasu01.Height = 0.1688976F;
            this.txtTotalBarasu01.Left = 3.744488F;
            this.txtTotalBarasu01.Name = "txtTotalBarasu01";
            this.txtTotalBarasu01.Style = "font-family: ＭＳ 明朝; font-size: 9pt; text-align: right; vertical-align: middle; dd" +
    "o-char-set: 1";
            this.txtTotalBarasu01.Text = "123,123.00";
            this.txtTotalBarasu01.Top = 0.06259843F;
            this.txtTotalBarasu01.Width = 0.6362212F;
            // 
            // txtTotalKesusu02
            // 
            this.txtTotalKesusu02.DataField = "ITEM25";
            this.txtTotalKesusu02.Height = 0.1688976F;
            this.txtTotalKesusu02.Left = 4.445276F;
            this.txtTotalKesusu02.Name = "txtTotalKesusu02";
            this.txtTotalKesusu02.Style = "font-family: ＭＳ 明朝; font-size: 9pt; text-align: right; vertical-align: middle; dd" +
    "o-char-set: 1";
            this.txtTotalKesusu02.Text = "25";
            this.txtTotalKesusu02.Top = 0.06259843F;
            this.txtTotalKesusu02.Width = 0.5047245F;
            // 
            // txtTotalBarasu02
            // 
            this.txtTotalBarasu02.DataField = "ITEM26";
            this.txtTotalBarasu02.Height = 0.1688976F;
            this.txtTotalBarasu02.Left = 5.006299F;
            this.txtTotalBarasu02.Name = "txtTotalBarasu02";
            this.txtTotalBarasu02.Style = "font-family: ＭＳ 明朝; font-size: 9pt; text-align: right; vertical-align: middle; dd" +
    "o-char-set: 1";
            this.txtTotalBarasu02.Text = "26";
            this.txtTotalBarasu02.Top = 0.06259843F;
            this.txtTotalBarasu02.Width = 0.6236224F;
            // 
            // txtTotalKesusu03
            // 
            this.txtTotalKesusu03.DataField = "ITEM27";
            this.txtTotalKesusu03.Height = 0.1688976F;
            this.txtTotalKesusu03.Left = 5.730709F;
            this.txtTotalKesusu03.Name = "txtTotalKesusu03";
            this.txtTotalKesusu03.Style = "font-family: ＭＳ 明朝; font-size: 9pt; text-align: right; vertical-align: middle; dd" +
    "o-char-set: 1";
            this.txtTotalKesusu03.Text = "27";
            this.txtTotalKesusu03.Top = 0.06259843F;
            this.txtTotalKesusu03.Width = 0.5059056F;
            // 
            // txtTotalBarasu03
            // 
            this.txtTotalBarasu03.DataField = "ITEM28";
            this.txtTotalBarasu03.Height = 0.1688976F;
            this.txtTotalBarasu03.Left = 6.265749F;
            this.txtTotalBarasu03.Name = "txtTotalBarasu03";
            this.txtTotalBarasu03.Style = "font-family: ＭＳ 明朝; font-size: 9pt; text-align: right; vertical-align: middle; dd" +
    "o-char-set: 1";
            this.txtTotalBarasu03.Text = "28";
            this.txtTotalBarasu03.Top = 0.06259843F;
            this.txtTotalBarasu03.Width = 0.6188969F;
            // 
            // txtTotalZaikoKingaku
            // 
            this.txtTotalZaikoKingaku.DataField = "ITEM30";
            this.txtTotalZaikoKingaku.Height = 0.1688976F;
            this.txtTotalZaikoKingaku.Left = 6.927166F;
            this.txtTotalZaikoKingaku.Name = "txtTotalZaikoKingaku";
            this.txtTotalZaikoKingaku.Style = "font-family: ＭＳ 明朝; font-size: 9pt; text-align: right; vertical-align: middle; dd" +
    "o-char-set: 1";
            this.txtTotalZaikoKingaku.Text = "12,123,000";
            this.txtTotalZaikoKingaku.Top = 0.06259843F;
            this.txtTotalZaikoKingaku.Width = 0.732677F;
            // 
            // lblTotal
            // 
            this.lblTotal.Height = 0.1688977F;
            this.lblTotal.HyperLink = null;
            this.lblTotal.Left = 1.351575F;
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
            this.lblTotal.Text = "【合    計】";
            this.lblTotal.Top = 0.06259843F;
            this.lblTotal.Width = 1.101181F;
            // 
            // reportHeader1
            // 
            this.reportHeader1.Height = 0F;
            this.reportHeader1.Name = "reportHeader1";
            // 
            // reportFooter1
            // 
            this.reportFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtTotalIrisu,
            this.txtTotalSouko,
            this.txtTotalKesusu01,
            this.txtTotalBarasu01,
            this.txtTotalKesusu02,
            this.txtTotalBarasu02,
            this.txtTotalKesusu03,
            this.txtTotalBarasu03,
            this.txtTotalZaikoKingaku,
            this.lblTotal,
            this.line2,
            this.line18,
            this.line4,
            this.line5,
            this.line6,
            this.line7,
            this.line8,
            this.line9,
            this.line10,
            this.line11,
            this.line12,
            this.line14,
            this.line15});
            this.reportFooter1.Height = 0.3102362F;
            this.reportFooter1.Name = "reportFooter1";
            this.reportFooter1.Format += new System.EventHandler(this.reportFooter1_Format);
            // 
            // line2
            // 
            this.line2.Height = 0F;
            this.line2.Left = 0.01968504F;
            this.line2.LineWeight = 1F;
            this.line2.Name = "line2";
            this.line2.Top = 0.3102362F;
            this.line2.Width = 7.700788F;
            this.line2.X1 = 0.01968504F;
            this.line2.X2 = 7.720473F;
            this.line2.Y1 = 0.3102362F;
            this.line2.Y2 = 0.3102362F;
            // 
            // line18
            // 
            this.line18.Height = 0.3102362F;
            this.line18.Left = 0.02244095F;
            this.line18.LineWeight = 1F;
            this.line18.Name = "line18";
            this.line18.Top = 0F;
            this.line18.Width = 0F;
            this.line18.X1 = 0.02244095F;
            this.line18.X2 = 0.02244095F;
            this.line18.Y1 = 0.3102362F;
            this.line18.Y2 = 0F;
            // 
            // line4
            // 
            this.line4.Height = 0.2944881F;
            this.line4.Left = 7.711024F;
            this.line4.LineWeight = 1F;
            this.line4.Name = "line4";
            this.line4.Top = 5.960464E-08F;
            this.line4.Width = 0.0003933907F;
            this.line4.X1 = 7.711024F;
            this.line4.X2 = 7.711417F;
            this.line4.Y1 = 0.2944882F;
            this.line4.Y2 = 5.960464E-08F;
            // 
            // line5
            // 
            this.line5.Height = 0.2944882F;
            this.line5.Left = 0.3952756F;
            this.line5.LineWeight = 1F;
            this.line5.Name = "line5";
            this.line5.Top = 0F;
            this.line5.Width = 0F;
            this.line5.X1 = 0.3952756F;
            this.line5.X2 = 0.3952756F;
            this.line5.Y1 = 0.2944882F;
            this.line5.Y2 = 0F;
            // 
            // line6
            // 
            this.line6.Height = 0.2944882F;
            this.line6.Left = 2.509449F;
            this.line6.LineWeight = 1F;
            this.line6.Name = "line6";
            this.line6.Top = 0F;
            this.line6.Width = 0F;
            this.line6.X1 = 2.509449F;
            this.line6.X2 = 2.509449F;
            this.line6.Y1 = 0.2944882F;
            this.line6.Y2 = 0F;
            // 
            // line7
            // 
            this.line7.Height = 0.2944882F;
            this.line7.Left = 2.820866F;
            this.line7.LineWeight = 1F;
            this.line7.Name = "line7";
            this.line7.Top = 0F;
            this.line7.Width = 0F;
            this.line7.X1 = 2.820866F;
            this.line7.X2 = 2.820866F;
            this.line7.Y1 = 0.2944882F;
            this.line7.Y2 = 0F;
            // 
            // line8
            // 
            this.line8.Height = 0.2944882F;
            this.line8.Left = 3.155906F;
            this.line8.LineWeight = 1F;
            this.line8.Name = "line8";
            this.line8.Top = 0F;
            this.line8.Width = 0F;
            this.line8.X1 = 3.155906F;
            this.line8.X2 = 3.155906F;
            this.line8.Y1 = 0.2944882F;
            this.line8.Y2 = 0F;
            // 
            // line9
            // 
            this.line9.Height = 0.2944882F;
            this.line9.Left = 3.744488F;
            this.line9.LineWeight = 1F;
            this.line9.Name = "line9";
            this.line9.Top = 0F;
            this.line9.Width = 0F;
            this.line9.X1 = 3.744488F;
            this.line9.X2 = 3.744488F;
            this.line9.Y1 = 0.2944882F;
            this.line9.Y2 = 0F;
            // 
            // line10
            // 
            this.line10.Height = 0.2944882F;
            this.line10.Left = 4.401575F;
            this.line10.LineWeight = 1F;
            this.line10.Name = "line10";
            this.line10.Top = 0F;
            this.line10.Width = 0F;
            this.line10.X1 = 4.401575F;
            this.line10.X2 = 4.401575F;
            this.line10.Y1 = 0.2944882F;
            this.line10.Y2 = 0F;
            // 
            // line11
            // 
            this.line11.Height = 0.2944882F;
            this.line11.Left = 5.006299F;
            this.line11.LineWeight = 1F;
            this.line11.Name = "line11";
            this.line11.Top = 0F;
            this.line11.Width = 0F;
            this.line11.X1 = 5.006299F;
            this.line11.X2 = 5.006299F;
            this.line11.Y1 = 0.2944882F;
            this.line11.Y2 = 0F;
            // 
            // line12
            // 
            this.line12.Height = 0.2944882F;
            this.line12.Left = 5.661024F;
            this.line12.LineWeight = 1F;
            this.line12.Name = "line12";
            this.line12.Top = 0F;
            this.line12.Width = 0F;
            this.line12.X1 = 5.661024F;
            this.line12.X2 = 5.661024F;
            this.line12.Y1 = 0.2944882F;
            this.line12.Y2 = 0F;
            // 
            // line14
            // 
            this.line14.Height = 0.2944882F;
            this.line14.Left = 6.265749F;
            this.line14.LineWeight = 1F;
            this.line14.Name = "line14";
            this.line14.Top = 0F;
            this.line14.Width = 0F;
            this.line14.X1 = 6.265749F;
            this.line14.X2 = 6.265749F;
            this.line14.Y1 = 0.2944882F;
            this.line14.Y2 = 0F;
            // 
            // line15
            // 
            this.line15.Height = 0.2944882F;
            this.line15.Left = 6.907481F;
            this.line15.LineWeight = 1F;
            this.line15.Name = "line15";
            this.line15.Top = 0F;
            this.line15.Width = 0F;
            this.line15.X1 = 6.907481F;
            this.line15.X2 = 6.907481F;
            this.line15.Y1 = 0.2944882F;
            this.line15.Y2 = 0F;
            // 
            // label1
            // 
            this.label1.Height = 0.2000002F;
            this.label1.HyperLink = null;
            this.label1.Left = 0.02440977F;
            this.label1.LineSpacing = 1F;
            this.label1.MultiLine = false;
            this.label1.Name = "label1";
            this.label1.Style = "background-color: Cyan; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font" +
    "-weight: bold; text-align: center; vertical-align: middle";
            this.label1.Text = "棚番";
            this.label1.Top = 0.974016F;
            this.label1.Width = 0.3834646F;
            // 
            // label2
            // 
            this.label2.Height = 0.2F;
            this.label2.HyperLink = null;
            this.label2.Left = 2.522047F;
            this.label2.LineSpacing = 1F;
            this.label2.MultiLine = false;
            this.label2.Name = "label2";
            this.label2.Style = "background-color: Cyan; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font" +
    "-weight: bold; text-align: center; vertical-align: middle";
            this.label2.Text = "入数";
            this.label2.Top = 0.974016F;
            this.label2.Width = 0.3114176F;
            // 
            // label3
            // 
            this.label3.Height = 0.2000002F;
            this.label3.HyperLink = null;
            this.label3.Left = 2.833465F;
            this.label3.LineSpacing = 1F;
            this.label3.MultiLine = false;
            this.label3.Name = "label3";
            this.label3.Style = "background-color: Cyan; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font" +
    "-weight: bold; text-align: center; vertical-align: middle";
            this.label3.Text = "倉庫";
            this.label3.Top = 0.974016F;
            this.label3.Width = 0.3350394F;
            // 
            // KBMR1021R
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.6889764F;
            this.PageSettings.Margins.Left = 0.2755905F;
            this.PageSettings.Margins.Right = 0.2755905F;
            this.PageSettings.Margins.Top = 0.5905512F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Portrait;
            this.PageSettings.PaperHeight = 11.69291F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.267716F;
            this.PrintWidth = 7.720473F;
            this.Sections.Add(this.reportHeader1);
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.pageFooter);
            this.Sections.Add(this.reportFooter1);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGenzai)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtJuni)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHyojiDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTanaban)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblShohinmei)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblIrisu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSoko)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTyoboSuryo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTanaorosiSuryo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSai)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblZaikoKingaku)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKesusu01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBarasu01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKesusu02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBarasu02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKesusu03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBarasu03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportInfo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTanaban)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShohinCd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIrisu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSouko)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKesusu01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBarasu01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKesusu02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBarasu02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShohinNm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKikaku)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBarasu03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKesusu03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZaikoKingaku)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalIrisu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalSouko)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalKesusu01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalBarasu01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalKesusu02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalBarasu02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalKesusu03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalBarasu03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalZaikoKingaku)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPageCount;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCompanyName;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblGenzai;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtJuni;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHyojiDate;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTanaban;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblShohinmei;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblIrisu;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSoko;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTyoboSuryo;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTanaorosiSuryo;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSai;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblZaikoKingaku;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblKesusu01;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblBarasu01;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblKesusu02;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblBarasu02;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblKesusu03;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblBarasu03;
        private GrapeCity.ActiveReports.SectionReportModel.Line line3;
        private GrapeCity.ActiveReports.SectionReportModel.Line line13;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTanaban;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtShohinCd;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtIrisu;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSouko;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKesusu01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBarasu01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox15;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKesusu02;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBarasu02;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtShohinNm;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKikaku;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox20;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox44;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBarasu03;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKesusu03;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtZaikoKingaku;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalIrisu;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalSouko;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalKesusu01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalBarasu01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalKesusu02;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalBarasu02;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalKesusu03;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalBarasu03;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalZaikoKingaku;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTotal;
        private GrapeCity.ActiveReports.SectionReportModel.Line line22;
        private GrapeCity.ActiveReports.SectionReportModel.ReportInfo reportInfo1;
        private GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine crossSectionLine1;
        private GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine crossSectionLine2;
        private GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine crossSectionLine3;
        private GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine crossSectionLine7;
        private GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine crossSectionLine8;
        private GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine crossSectionLine9;
        private GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine crossSectionLine10;
        private GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine crossSectionLine11;
        private GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine crossSectionLine4;
        private GrapeCity.ActiveReports.SectionReportModel.CrossSectionBox crossSectionBox1;
        private GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine crossSectionLine12;
        private GrapeCity.ActiveReports.SectionReportModel.ReportHeader reportHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.ReportFooter reportFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.Line line2;
        private GrapeCity.ActiveReports.SectionReportModel.Line line18;
        private GrapeCity.ActiveReports.SectionReportModel.Line line4;
        private GrapeCity.ActiveReports.SectionReportModel.Line line5;
        private GrapeCity.ActiveReports.SectionReportModel.Line line6;
        private GrapeCity.ActiveReports.SectionReportModel.Line line7;
        private GrapeCity.ActiveReports.SectionReportModel.Line line8;
        private GrapeCity.ActiveReports.SectionReportModel.Line line9;
        private GrapeCity.ActiveReports.SectionReportModel.Line line10;
        private GrapeCity.ActiveReports.SectionReportModel.Line line11;
        private GrapeCity.ActiveReports.SectionReportModel.Line line12;
        private GrapeCity.ActiveReports.SectionReportModel.Line line14;
        private GrapeCity.ActiveReports.SectionReportModel.Line line15;
        private GrapeCity.ActiveReports.SectionReportModel.Line line16;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label2;
        private GrapeCity.ActiveReports.SectionReportModel.Label label3;
    }
}
