﻿namespace jp.co.fsi.kb.kbmr1021
{
    partial class KBMR1022
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbxShohinKubunSelect = new System.Windows.Forms.GroupBox();
            this.rdoShohinBunrui3 = new System.Windows.Forms.RadioButton();
            this.rdoShohinKubun2 = new System.Windows.Forms.RadioButton();
            this.rdoShohinKubun1 = new System.Windows.Forms.RadioButton();
            this.gbxTanaoroshihyoInji = new System.Windows.Forms.GroupBox();
            this.rdoInjiAri = new System.Windows.Forms.RadioButton();
            this.rdoInjiNashi = new System.Windows.Forms.RadioButton();
            this.gbxTanaoroshiKingaku = new System.Windows.Forms.GroupBox();
            this.rdoSaishuSireTanka = new System.Windows.Forms.RadioButton();
            this.rdoShohinMasutaSireTanka = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rdoSort3 = new System.Windows.Forms.RadioButton();
            this.rdoSort2 = new System.Windows.Forms.RadioButton();
            this.rdoSort1 = new System.Windows.Forms.RadioButton();
            this.rdoSort4 = new System.Windows.Forms.RadioButton();
            this.pnlDebug.SuspendLayout();
            this.gbxShohinKubunSelect.SuspendLayout();
            this.gbxTanaoroshihyoInji.SuspendLayout();
            this.gbxTanaoroshiKingaku.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Size = new System.Drawing.Size(688, 23);
            this.lblTitle.Visible = false;
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(5, 358);
            this.pnlDebug.Size = new System.Drawing.Size(381, 100);
            // 
            // gbxShohinKubunSelect
            // 
            this.gbxShohinKubunSelect.Controls.Add(this.rdoShohinBunrui3);
            this.gbxShohinKubunSelect.Controls.Add(this.rdoShohinKubun2);
            this.gbxShohinKubunSelect.Controls.Add(this.rdoShohinKubun1);
            this.gbxShohinKubunSelect.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxShohinKubunSelect.Location = new System.Drawing.Point(17, 13);
            this.gbxShohinKubunSelect.Name = "gbxShohinKubunSelect";
            this.gbxShohinKubunSelect.Size = new System.Drawing.Size(351, 64);
            this.gbxShohinKubunSelect.TabIndex = 904;
            this.gbxShohinKubunSelect.TabStop = false;
            this.gbxShohinKubunSelect.Text = "商品区分絞込み条件";
            // 
            // rdoShohinBunrui3
            // 
            this.rdoShohinBunrui3.AutoSize = true;
            this.rdoShohinBunrui3.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoShohinBunrui3.Location = new System.Drawing.Point(229, 29);
            this.rdoShohinBunrui3.Name = "rdoShohinBunrui3";
            this.rdoShohinBunrui3.Size = new System.Drawing.Size(81, 17);
            this.rdoShohinBunrui3.TabIndex = 2;
            this.rdoShohinBunrui3.Text = "商品分類";
            this.rdoShohinBunrui3.UseVisualStyleBackColor = true;
            // 
            // rdoShohinKubun2
            // 
            this.rdoShohinKubun2.AutoSize = true;
            this.rdoShohinKubun2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoShohinKubun2.Location = new System.Drawing.Point(128, 29);
            this.rdoShohinKubun2.Name = "rdoShohinKubun2";
            this.rdoShohinKubun2.Size = new System.Drawing.Size(88, 17);
            this.rdoShohinKubun2.TabIndex = 1;
            this.rdoShohinKubun2.Text = "商品区分2";
            this.rdoShohinKubun2.UseVisualStyleBackColor = true;
            // 
            // rdoShohinKubun1
            // 
            this.rdoShohinKubun1.AutoSize = true;
            this.rdoShohinKubun1.Checked = true;
            this.rdoShohinKubun1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoShohinKubun1.Location = new System.Drawing.Point(27, 29);
            this.rdoShohinKubun1.Name = "rdoShohinKubun1";
            this.rdoShohinKubun1.Size = new System.Drawing.Size(88, 17);
            this.rdoShohinKubun1.TabIndex = 0;
            this.rdoShohinKubun1.TabStop = true;
            this.rdoShohinKubun1.Text = "商品区分1";
            this.rdoShohinKubun1.UseVisualStyleBackColor = true;
            // 
            // gbxTanaoroshihyoInji
            // 
            this.gbxTanaoroshihyoInji.Controls.Add(this.rdoInjiAri);
            this.gbxTanaoroshihyoInji.Controls.Add(this.rdoInjiNashi);
            this.gbxTanaoroshihyoInji.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxTanaoroshihyoInji.Location = new System.Drawing.Point(17, 163);
            this.gbxTanaoroshihyoInji.Name = "gbxTanaoroshihyoInji";
            this.gbxTanaoroshihyoInji.Size = new System.Drawing.Size(351, 64);
            this.gbxTanaoroshihyoInji.TabIndex = 903;
            this.gbxTanaoroshihyoInji.TabStop = false;
            this.gbxTanaoroshihyoInji.Text = "棚卸表数量合計印字";
            // 
            // rdoInjiAri
            // 
            this.rdoInjiAri.AutoSize = true;
            this.rdoInjiAri.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoInjiAri.Location = new System.Drawing.Point(229, 25);
            this.rdoInjiAri.Name = "rdoInjiAri";
            this.rdoInjiAri.Size = new System.Drawing.Size(53, 17);
            this.rdoInjiAri.TabIndex = 1;
            this.rdoInjiAri.Text = "あり";
            this.rdoInjiAri.UseVisualStyleBackColor = true;
            // 
            // rdoInjiNashi
            // 
            this.rdoInjiNashi.AutoSize = true;
            this.rdoInjiNashi.Checked = true;
            this.rdoInjiNashi.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoInjiNashi.Location = new System.Drawing.Point(27, 25);
            this.rdoInjiNashi.Name = "rdoInjiNashi";
            this.rdoInjiNashi.Size = new System.Drawing.Size(53, 17);
            this.rdoInjiNashi.TabIndex = 0;
            this.rdoInjiNashi.TabStop = true;
            this.rdoInjiNashi.Text = "なし";
            this.rdoInjiNashi.UseVisualStyleBackColor = true;
            // 
            // gbxTanaoroshiKingaku
            // 
            this.gbxTanaoroshiKingaku.Controls.Add(this.rdoSaishuSireTanka);
            this.gbxTanaoroshiKingaku.Controls.Add(this.rdoShohinMasutaSireTanka);
            this.gbxTanaoroshiKingaku.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxTanaoroshiKingaku.Location = new System.Drawing.Point(17, 88);
            this.gbxTanaoroshiKingaku.Name = "gbxTanaoroshiKingaku";
            this.gbxTanaoroshiKingaku.Size = new System.Drawing.Size(351, 64);
            this.gbxTanaoroshiKingaku.TabIndex = 902;
            this.gbxTanaoroshiKingaku.TabStop = false;
            this.gbxTanaoroshiKingaku.Text = "棚卸金額計算単価";
            // 
            // rdoSaishuSireTanka
            // 
            this.rdoSaishuSireTanka.AutoSize = true;
            this.rdoSaishuSireTanka.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoSaishuSireTanka.Location = new System.Drawing.Point(229, 26);
            this.rdoSaishuSireTanka.Name = "rdoSaishuSireTanka";
            this.rdoSaishuSireTanka.Size = new System.Drawing.Size(109, 17);
            this.rdoSaishuSireTanka.TabIndex = 1;
            this.rdoSaishuSireTanka.Text = "最終仕入単価";
            this.rdoSaishuSireTanka.UseVisualStyleBackColor = true;
            // 
            // rdoShohinMasutaSireTanka
            // 
            this.rdoShohinMasutaSireTanka.AutoSize = true;
            this.rdoShohinMasutaSireTanka.Checked = true;
            this.rdoShohinMasutaSireTanka.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoShohinMasutaSireTanka.Location = new System.Drawing.Point(27, 26);
            this.rdoShohinMasutaSireTanka.Name = "rdoShohinMasutaSireTanka";
            this.rdoShohinMasutaSireTanka.Size = new System.Drawing.Size(165, 17);
            this.rdoShohinMasutaSireTanka.TabIndex = 0;
            this.rdoShohinMasutaSireTanka.TabStop = true;
            this.rdoShohinMasutaSireTanka.Text = "商品マスター仕入単価";
            this.rdoShohinMasutaSireTanka.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rdoSort4);
            this.groupBox1.Controls.Add(this.rdoSort3);
            this.groupBox1.Controls.Add(this.rdoSort2);
            this.groupBox1.Controls.Add(this.rdoSort1);
            this.groupBox1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBox1.Location = new System.Drawing.Point(18, 235);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(351, 159);
            this.groupBox1.TabIndex = 905;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "棚卸入力ソート順";
            // 
            // rdoSort3
            // 
            this.rdoSort3.AutoSize = true;
            this.rdoSort3.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoSort3.Location = new System.Drawing.Point(27, 93);
            this.rdoSort3.Name = "rdoSort3";
            this.rdoSort3.Size = new System.Drawing.Size(221, 17);
            this.rdoSort3.TabIndex = 2;
            this.rdoSort3.Text = "棚番、商品区分１、商品コード";
            this.rdoSort3.UseVisualStyleBackColor = true;
            // 
            // rdoSort2
            // 
            this.rdoSort2.AutoSize = true;
            this.rdoSort2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoSort2.Location = new System.Drawing.Point(27, 61);
            this.rdoSort2.Name = "rdoSort2";
            this.rdoSort2.Size = new System.Drawing.Size(137, 17);
            this.rdoSort2.TabIndex = 1;
            this.rdoSort2.Text = "棚番、商品コード";
            this.rdoSort2.UseVisualStyleBackColor = true;
            // 
            // rdoSort1
            // 
            this.rdoSort1.AutoSize = true;
            this.rdoSort1.Checked = true;
            this.rdoSort1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoSort1.Location = new System.Drawing.Point(27, 29);
            this.rdoSort1.Name = "rdoSort1";
            this.rdoSort1.Size = new System.Drawing.Size(95, 17);
            this.rdoSort1.TabIndex = 0;
            this.rdoSort1.TabStop = true;
            this.rdoSort1.Text = "商品コード";
            this.rdoSort1.UseVisualStyleBackColor = true;
            // 
            // rdoSort4
            // 
            this.rdoSort4.AutoSize = true;
            this.rdoSort4.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoSort4.Location = new System.Drawing.Point(27, 125);
            this.rdoSort4.Name = "rdoSort4";
            this.rdoSort4.Size = new System.Drawing.Size(305, 17);
            this.rdoSort4.TabIndex = 3;
            this.rdoSort4.Text = "棚番、商品区分１、商品区分２、商品コード";
            this.rdoSort4.UseVisualStyleBackColor = true;
            // 
            // KBMR1022
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(387, 462);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.gbxShohinKubunSelect);
            this.Controls.Add(this.gbxTanaoroshihyoInji);
            this.Controls.Add(this.gbxTanaoroshiKingaku);
            this.Name = "KBMR1022";
            this.Text = "棚卸初期設定";
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.gbxTanaoroshiKingaku, 0);
            this.Controls.SetChildIndex(this.gbxTanaoroshihyoInji, 0);
            this.Controls.SetChildIndex(this.gbxShohinKubunSelect, 0);
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.pnlDebug.ResumeLayout(false);
            this.gbxShohinKubunSelect.ResumeLayout(false);
            this.gbxShohinKubunSelect.PerformLayout();
            this.gbxTanaoroshihyoInji.ResumeLayout(false);
            this.gbxTanaoroshihyoInji.PerformLayout();
            this.gbxTanaoroshiKingaku.ResumeLayout(false);
            this.gbxTanaoroshiKingaku.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbxShohinKubunSelect;
        private System.Windows.Forms.RadioButton rdoShohinBunrui3;
        private System.Windows.Forms.RadioButton rdoShohinKubun2;
        private System.Windows.Forms.RadioButton rdoShohinKubun1;
        private System.Windows.Forms.GroupBox gbxTanaoroshihyoInji;
        private System.Windows.Forms.RadioButton rdoInjiAri;
        private System.Windows.Forms.RadioButton rdoInjiNashi;
        private System.Windows.Forms.GroupBox gbxTanaoroshiKingaku;
        private System.Windows.Forms.RadioButton rdoSaishuSireTanka;
        private System.Windows.Forms.RadioButton rdoShohinMasutaSireTanka;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rdoSort4;
        private System.Windows.Forms.RadioButton rdoSort3;
        private System.Windows.Forms.RadioButton rdoSort2;
        private System.Windows.Forms.RadioButton rdoSort1;
    }
}