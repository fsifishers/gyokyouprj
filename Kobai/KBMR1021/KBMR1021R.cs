﻿using System;
using System.Data;

using jp.co.fsi.common.report;
using jp.co.fsi.common.util;

namespace jp.co.fsi.kb.kbmr1021
{
    /// <summary>
    /// KBMR1021R の帳票
    /// </summary>
    public partial class KBMR1021R : BaseReport
    {
        //件数カウンタ
        private int intRowNumber;

        public KBMR1021R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }

        private void detail_Format(object sender, EventArgs e)
        {
            // 合計金額をフォーマット
            /*this.txtIrisu.Text = Util.FormatNum(Util.ToDecimal(this.txtIrisu.Text));
            this.txtSouko.Text = Util.FormatNum(Util.ToDecimal(this.txtSouko.Text));
            this.txtKesusu01.Text = Util.FormatNum(Util.ToDecimal(this.txtKesusu01.Text));
            this.txtBarasu01.Text = Util.FormatNum(Util.ToDecimal(this.txtBarasu01.Text));
            this.txtKesusu02.Text = Util.FormatNum(Util.ToDecimal(this.txtKesusu02.Text));
            this.txtBarasu02.Text = Util.FormatNum(Util.ToDecimal(this.txtBarasu02.Text));
            this.txtKesusu03.Text = Util.FormatNum(Util.ToDecimal(this.txtKesusu03.Text));
            this.txtBarasu03.Text = Util.FormatNum(Util.ToDecimal(this.txtBarasu03.Text));
            this.txtZaikoKingaku.Text = Util.FormatNum(Util.ToDecimal(this.txtZaikoKingaku.Text));*/
            // 合計金額が0の場合、空表示とする
            if (this.txtIrisu.Text == "0")
            {
                this.txtIrisu.Text = "";
            }
            if (this.txtSouko.Text == "0")
            {
                this.txtSouko.Text = "";
            }
            if (this.txtKesusu01.Text == "0.00")
            {
                this.txtKesusu01.Text = "";
            }
            if (this.txtBarasu01.Text == "0.00")
            {
                this.txtBarasu01.Text = "";
            }
            if (this.txtKesusu02.Text == "0.00")
            {
                this.txtKesusu02.Text = "";
            }
            if (this.txtBarasu02.Text == "0.00")
            {
                this.txtBarasu02.Text = "";
            }
            if (this.txtKesusu03.Text == "0.00")
            {
                this.txtKesusu03.Text = "";
            }
            if (this.txtBarasu03.Text == "0.00")
            {
                this.txtBarasu03.Text = "";
            }
            if (this.txtZaikoKingaku.Text == "0")
            {
                this.txtZaikoKingaku.Text = "";
            }

            this.line16.Visible = false;
            intRowNumber = intRowNumber + 1;
            if (intRowNumber % 27 == 0 )
            {
                this.line16.Visible = true;
            }
        }

        // ページフッターの設定(合計)
        private void reportFooter1_Format(object sender, EventArgs e)
        {
            // 合計金額をフォーマット
            /*this.txtTotalIrisu.Text = Util.FormatNum(Util.ToDecimal(this.txtTotalIrisu.Text));
            this.txtTotalSouko.Text = Util.FormatNum(Util.ToDecimal(this.txtTotalSouko.Text));
            this.txtTotalKesusu01.Text = Util.FormatNum(Util.ToDecimal(this.txtTotalKesusu01.Text));
            this.txtTotalBarasu01.Text = Util.FormatNum(Util.ToDecimal(this.txtTotalBarasu01.Text));
            this.txtTotalKesusu02.Text = Util.FormatNum(Util.ToDecimal(this.txtTotalKesusu02.Text));
            this.txtTotalBarasu02.Text = Util.FormatNum(Util.ToDecimal(this.txtTotalBarasu02.Text));
            this.txtTotalKesusu03.Text = Util.FormatNum(Util.ToDecimal(this.txtTotalKesusu03.Text));
            this.txtTotalBarasu03.Text = Util.FormatNum(Util.ToDecimal(this.txtTotalBarasu03.Text));
            this.txtTotalZaikoKingaku.Text = Util.FormatNum(Util.ToDecimal(this.txtTotalZaikoKingaku.Text));*/
            // 合計金額が0の場合、空表示とする
            if (this.txtTotalIrisu.Text == "0")
            {
                this.txtTotalIrisu.Text= "";
            }
            if (this.txtTotalSouko.Text == "0")
            {
                this.txtTotalSouko.Text = "";
            }
            if (this.txtTotalKesusu01.Text == "0")
            {
                this.txtTotalKesusu01.Text = "";
            }
            if (this.txtTotalBarasu01.Text == "0.0")
            {
                this.txtTotalBarasu01.Text = "";
            }
            if (this.txtTotalKesusu02.Text == "0")
            {
                this.txtTotalKesusu02.Text = "";
            }
            if (this.txtTotalBarasu02.Text == "0.0")
            {
                this.txtTotalBarasu02.Text = "";
            }
            if (this.txtTotalKesusu03.Text == "0")
            {
                this.txtTotalKesusu03.Text = "";
            }
            if (this.txtTotalBarasu03.Text == "0.0")
            {
                this.txtTotalBarasu03.Text = "";
            }
            if (this.txtTotalZaikoKingaku.Text == "0")
            {
                this.txtTotalZaikoKingaku.Text = "";
            }
        }
    }
}
