﻿using System;
using System.Data;
using System.Text;

using jp.co.fsi.common.access;
using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.kob.kobc9031
{
    /// <summary>
    /// 商品の登録_売上自動仕訳の設定[発生](KOBC9033)
    /// </summary>
    public partial class KOBC9033 : BasePgForm
    {
        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KOBC9033()
        {
            InitializeComponent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.InitForm();は呼び出さなくて構いません。
        /// また、このメソッド内の処理を外出しでこのクラス内にメソッド化するのは構いませんが、
        /// 原則、独自で起動時のイベント処理を実装することは禁じます。
        /// </remarks>
        protected override void InitForm()
        {
            //MEMO:このプログラム個別で特に実装すべき処理が無いので
            //中身が空になってます。
            //例えばテキストボックスにデフォルト値を設定するなどの
            //仕様を実装する場合、ここで実装して下さい。
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        protected override void PressF1()
        {
            //MEMO:(参考)Escキー押下時は画面を閉じる処理が基盤側で実装されていますが、
            //PressEsc()をオーバーライドすることでプログラム個別に実装することも可能です。

            //MEMO:現状アクティブなコントロールごとに処理を実装してください。
            switch (this.ActiveControl.Name)
            {
                case "txtCodeFr":
                    Msg.Info("コード(自)のための検索画面を起動する必要があるけど、"
                        + Environment.NewLine + "出来てないので一旦保留です。");
                    break;

                case "txtCodeTo":
                    Msg.Info("コード(至)のための検索画面を起動する必要があるけど、"
                        + Environment.NewLine + "出来てないので一旦保留です。");
                    break;
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        protected override void PressF4()
        {
            // 印刷処理
            DoPrint(false);
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        protected override void PressF5()
        {
            // プレビュー処理
            DoPrint(true);
        }
        #endregion

        #region イベント
        /// <summary>
        /// 仕入先コード(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        
        private void DoEnter()
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtShiwakeCd.Text))
            {
                Msg.Notice("コード(自)は数値のみで入力してください。");
                this.txtShiwakeCd.SelectAll();
                //e.Cancel = true;
                return;
            }

            // チェックOKの場合、0埋めする
            //this.txtSenshuCd.Text = Util.PadZero(this.txtSenshuCd.Text, 4);
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview)
        {
            try
            {
                // 帳票出力用にワークテーブルにデータを作成
                MakeWkData();
                this.Dba.Commit();
            }
            finally
            {
                this.Dba.Rollback();
            }

            // 帳票出力
            Report rpt = new Report();
            //rpt.OutputReport(this.Config.LoadReportPath(Constants.SubSys.Test), "R_KOBC9032", isPreview);

            // ワークテーブルに作成したデータを削除
            try
            {
                // 帳票出力用に作成したデータを削除
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                this.Dba.Delete("TW_TEST_EMP", "GUID = @GUID", dpc);
                this.Dba.Commit();
            }
            finally
            {
                this.Dba.Rollback();
            }
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private void MakeWkData()
        {
            // 入力された情報を元にワークテーブルに更新をする
            Boolean whereFlg = false;
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder sbSql = new StringBuilder();
            sbSql.Append("INSERT INTO TW_TEST_EMP(");
            sbSql.Append("  GUID");
            sbSql.Append(" ,USERID");
            sbSql.Append(" ,USERNM");
            sbSql.Append(" ,DEPTNM");
            sbSql.Append(" ,POSITION");
            sbSql.Append(" ,REGIST_DATE");
            sbSql.Append(" ,UPDATE_DATE");
            sbSql.Append(") ");
            sbSql.Append("SELECT ");
            sbSql.Append("  @GUID");
            sbSql.Append(" ,USERID");
            sbSql.Append(" ,USERNM");
            sbSql.Append(" ,DEPTNM");
            sbSql.Append(" ,POSITION");
            sbSql.Append(" ,GETDATE()");
            sbSql.Append(" ,GETDATE()");
            sbSql.Append(" FROM ");
            sbSql.Append("  TM_TEST_EMP");
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);

            this.Dba.ModifyBySql(Util.ToString(sbSql), dpc);
        }
        #endregion

    }
}
