﻿namespace jp.co.fsi.kb.kbcm1021
{
    partial class KBCM1022
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblShohinCd = new System.Windows.Forms.Label();
            this.txtShohinCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShohinNm = new System.Windows.Forms.Label();
            this.txtShohinNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShohinKanaNm = new System.Windows.Forms.Label();
            this.txtShohinKanaNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShohinKikaku = new System.Windows.Forms.Label();
            this.txtShohinKikaku = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblTanaban = new System.Windows.Forms.Label();
            this.txtTanaban = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblZaikoKanriKbn = new System.Windows.Forms.Label();
            this.txtZaikoKanriKbn = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblUriageShiwakeCd = new System.Windows.Forms.Label();
            this.txtUriageShiwakeCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShiireShiwakeCd = new System.Windows.Forms.Label();
            this.txtShiireShiwakeCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblIrisu = new System.Windows.Forms.Label();
            this.txtIrisu = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblTani = new System.Windows.Forms.Label();
            this.txtTani = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblZaikoKanriKbnResults = new System.Windows.Forms.Label();
            this.lblUriageShiwakeCdResults = new System.Windows.Forms.Label();
            this.lblShiireShiwakeCdResults = new System.Windows.Forms.Label();
            this.txtShiireTanka = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShiireTanka = new System.Windows.Forms.Label();
            this.txtOroshiTanka = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblOroshiTanka = new System.Windows.Forms.Label();
            this.txtKouriTanka = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKouriTanka = new System.Windows.Forms.Label();
            this.lblShohinKbn1Results = new System.Windows.Forms.Label();
            this.txtShohinKbn1 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShohinKbn1 = new System.Windows.Forms.Label();
            this.lblShohinKbn2Results = new System.Windows.Forms.Label();
            this.txtShohinKbn2 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShohinKbn2 = new System.Windows.Forms.Label();
            this.lblShohinKbn3Results = new System.Windows.Forms.Label();
            this.txtShohinKbn3 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShohinKbn3 = new System.Windows.Forms.Label();
            this.lblShohinKbn4Results = new System.Windows.Forms.Label();
            this.txtShohinKbn4 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShohinKbn4 = new System.Windows.Forms.Label();
            this.txtBarcodeCase = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblBarcodeCase = new System.Windows.Forms.Label();
            this.txtBarcodeBara = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblBarcodeBara = new System.Windows.Forms.Label();
            this.txtTekiseisu = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblTekiseisu = new System.Windows.Forms.Label();
            this.txtShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShishoNm = new System.Windows.Forms.Label();
            this.lblShisho = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtShohizeiKbn = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShohiZeiKbn = new System.Windows.Forms.Label();
            this.lblChushiKbnNm = new System.Windows.Forms.Label();
            this.txtChushiKbn = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblChuchiKbn = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Text = "";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(5, 413);
            this.pnlDebug.Size = new System.Drawing.Size(847, 100);
            // 
            // lblShohinCd
            // 
            this.lblShohinCd.BackColor = System.Drawing.Color.Silver;
            this.lblShohinCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShohinCd.Location = new System.Drawing.Point(12, 78);
            this.lblShohinCd.Name = "lblShohinCd";
            this.lblShohinCd.Size = new System.Drawing.Size(224, 25);
            this.lblShohinCd.TabIndex = 4;
            this.lblShohinCd.Text = "商 品 コ ー ド";
            this.lblShohinCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShohinCd
            // 
            this.txtShohinCd.AllowDrop = true;
            this.txtShohinCd.AutoSizeFromLength = false;
            this.txtShohinCd.DisplayLength = null;
            this.txtShohinCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShohinCd.Location = new System.Drawing.Point(103, 80);
            this.txtShohinCd.MaxLength = 13;
            this.txtShohinCd.Name = "txtShohinCd";
            this.txtShohinCd.Size = new System.Drawing.Size(130, 20);
            this.txtShohinCd.TabIndex = 5;
            this.txtShohinCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShohinCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohinCd_Validating);
            // 
            // lblShohinNm
            // 
            this.lblShohinNm.BackColor = System.Drawing.Color.Silver;
            this.lblShohinNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShohinNm.Location = new System.Drawing.Point(12, 108);
            this.lblShohinNm.Name = "lblShohinNm";
            this.lblShohinNm.Size = new System.Drawing.Size(370, 25);
            this.lblShohinNm.TabIndex = 6;
            this.lblShohinNm.Text = "商    品    名";
            this.lblShohinNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShohinNm
            // 
            this.txtShohinNm.AutoSizeFromLength = false;
            this.txtShohinNm.DisplayLength = null;
            this.txtShohinNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShohinNm.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtShohinNm.Location = new System.Drawing.Point(103, 110);
            this.txtShohinNm.MaxLength = 40;
            this.txtShohinNm.Name = "txtShohinNm";
            this.txtShohinNm.Size = new System.Drawing.Size(276, 20);
            this.txtShohinNm.TabIndex = 7;
            this.txtShohinNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohinNm_Validating);
            // 
            // lblShohinKanaNm
            // 
            this.lblShohinKanaNm.BackColor = System.Drawing.Color.Silver;
            this.lblShohinKanaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShohinKanaNm.Location = new System.Drawing.Point(12, 138);
            this.lblShohinKanaNm.Name = "lblShohinKanaNm";
            this.lblShohinKanaNm.Size = new System.Drawing.Size(253, 25);
            this.lblShohinKanaNm.TabIndex = 8;
            this.lblShohinKanaNm.Text = "商 品 カ ナ 名";
            this.lblShohinKanaNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShohinKanaNm
            // 
            this.txtShohinKanaNm.AllowDrop = true;
            this.txtShohinKanaNm.AutoSizeFromLength = false;
            this.txtShohinKanaNm.DisplayLength = null;
            this.txtShohinKanaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShohinKanaNm.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtShohinKanaNm.Location = new System.Drawing.Point(103, 140);
            this.txtShohinKanaNm.MaxLength = 15;
            this.txtShohinKanaNm.Name = "txtShohinKanaNm";
            this.txtShohinKanaNm.Size = new System.Drawing.Size(159, 20);
            this.txtShohinKanaNm.TabIndex = 9;
            this.txtShohinKanaNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohinKanaNm_Validating);
            // 
            // lblShohinKikaku
            // 
            this.lblShohinKikaku.BackColor = System.Drawing.Color.Silver;
            this.lblShohinKikaku.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShohinKikaku.Location = new System.Drawing.Point(12, 168);
            this.lblShohinKikaku.Name = "lblShohinKikaku";
            this.lblShohinKikaku.Size = new System.Drawing.Size(370, 25);
            this.lblShohinKikaku.TabIndex = 10;
            this.lblShohinKikaku.Text = "商  品  規  格";
            this.lblShohinKikaku.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShohinKikaku
            // 
            this.txtShohinKikaku.AllowDrop = true;
            this.txtShohinKikaku.AutoSizeFromLength = false;
            this.txtShohinKikaku.DisplayLength = null;
            this.txtShohinKikaku.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShohinKikaku.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtShohinKikaku.Location = new System.Drawing.Point(103, 170);
            this.txtShohinKikaku.MaxLength = 30;
            this.txtShohinKikaku.Name = "txtShohinKikaku";
            this.txtShohinKikaku.Size = new System.Drawing.Size(276, 20);
            this.txtShohinKikaku.TabIndex = 11;
            this.txtShohinKikaku.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohinKikaku_Validating);
            // 
            // lblTanaban
            // 
            this.lblTanaban.BackColor = System.Drawing.Color.Silver;
            this.lblTanaban.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTanaban.Location = new System.Drawing.Point(12, 198);
            this.lblTanaban.Name = "lblTanaban";
            this.lblTanaban.Size = new System.Drawing.Size(152, 25);
            this.lblTanaban.TabIndex = 12;
            this.lblTanaban.Text = "棚          番";
            this.lblTanaban.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTanaban
            // 
            this.txtTanaban.AllowDrop = true;
            this.txtTanaban.AutoSizeFromLength = false;
            this.txtTanaban.DisplayLength = null;
            this.txtTanaban.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTanaban.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtTanaban.Location = new System.Drawing.Point(103, 200);
            this.txtTanaban.MaxLength = 6;
            this.txtTanaban.Name = "txtTanaban";
            this.txtTanaban.Size = new System.Drawing.Size(59, 20);
            this.txtTanaban.TabIndex = 13;
            this.txtTanaban.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTanaban.Validating += new System.ComponentModel.CancelEventHandler(this.txtTanaban_Validating);
            // 
            // lblZaikoKanriKbn
            // 
            this.lblZaikoKanriKbn.BackColor = System.Drawing.Color.Silver;
            this.lblZaikoKanriKbn.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblZaikoKanriKbn.Location = new System.Drawing.Point(12, 228);
            this.lblZaikoKanriKbn.Name = "lblZaikoKanriKbn";
            this.lblZaikoKanriKbn.Size = new System.Drawing.Size(132, 25);
            this.lblZaikoKanriKbn.TabIndex = 14;
            this.lblZaikoKanriKbn.Text = "在庫管理区分";
            this.lblZaikoKanriKbn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtZaikoKanriKbn
            // 
            this.txtZaikoKanriKbn.AllowDrop = true;
            this.txtZaikoKanriKbn.AutoSizeFromLength = false;
            this.txtZaikoKanriKbn.DisplayLength = null;
            this.txtZaikoKanriKbn.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtZaikoKanriKbn.Location = new System.Drawing.Point(103, 230);
            this.txtZaikoKanriKbn.MaxLength = 1;
            this.txtZaikoKanriKbn.Name = "txtZaikoKanriKbn";
            this.txtZaikoKanriKbn.Size = new System.Drawing.Size(39, 20);
            this.txtZaikoKanriKbn.TabIndex = 15;
            this.txtZaikoKanriKbn.Text = "0";
            this.txtZaikoKanriKbn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtZaikoKanriKbn.Validating += new System.ComponentModel.CancelEventHandler(this.txtZaikoKanriKbn_Validating);
            // 
            // lblUriageShiwakeCd
            // 
            this.lblUriageShiwakeCd.BackColor = System.Drawing.Color.Silver;
            this.lblUriageShiwakeCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblUriageShiwakeCd.Location = new System.Drawing.Point(12, 258);
            this.lblUriageShiwakeCd.Name = "lblUriageShiwakeCd";
            this.lblUriageShiwakeCd.Size = new System.Drawing.Size(132, 25);
            this.lblUriageShiwakeCd.TabIndex = 17;
            this.lblUriageShiwakeCd.Text = "売上仕訳コード";
            this.lblUriageShiwakeCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtUriageShiwakeCd
            // 
            this.txtUriageShiwakeCd.AllowDrop = true;
            this.txtUriageShiwakeCd.AutoSizeFromLength = false;
            this.txtUriageShiwakeCd.DisplayLength = null;
            this.txtUriageShiwakeCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtUriageShiwakeCd.Location = new System.Drawing.Point(103, 260);
            this.txtUriageShiwakeCd.MaxLength = 4;
            this.txtUriageShiwakeCd.Name = "txtUriageShiwakeCd";
            this.txtUriageShiwakeCd.Size = new System.Drawing.Size(39, 20);
            this.txtUriageShiwakeCd.TabIndex = 18;
            this.txtUriageShiwakeCd.Text = "0";
            this.txtUriageShiwakeCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtUriageShiwakeCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtUriageShiwakeCd_Validating);
            // 
            // lblShiireShiwakeCd
            // 
            this.lblShiireShiwakeCd.BackColor = System.Drawing.Color.Silver;
            this.lblShiireShiwakeCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShiireShiwakeCd.Location = new System.Drawing.Point(12, 288);
            this.lblShiireShiwakeCd.Name = "lblShiireShiwakeCd";
            this.lblShiireShiwakeCd.Size = new System.Drawing.Size(132, 25);
            this.lblShiireShiwakeCd.TabIndex = 20;
            this.lblShiireShiwakeCd.Text = "仕入仕訳コード";
            this.lblShiireShiwakeCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShiireShiwakeCd
            // 
            this.txtShiireShiwakeCd.AllowDrop = true;
            this.txtShiireShiwakeCd.AutoSizeFromLength = false;
            this.txtShiireShiwakeCd.DisplayLength = null;
            this.txtShiireShiwakeCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShiireShiwakeCd.Location = new System.Drawing.Point(103, 290);
            this.txtShiireShiwakeCd.MaxLength = 4;
            this.txtShiireShiwakeCd.Name = "txtShiireShiwakeCd";
            this.txtShiireShiwakeCd.Size = new System.Drawing.Size(39, 20);
            this.txtShiireShiwakeCd.TabIndex = 21;
            this.txtShiireShiwakeCd.Text = "0";
            this.txtShiireShiwakeCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShiireShiwakeCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtShiireShiwakeCd_Validating);
            // 
            // lblIrisu
            // 
            this.lblIrisu.BackColor = System.Drawing.Color.Silver;
            this.lblIrisu.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblIrisu.Location = new System.Drawing.Point(12, 350);
            this.lblIrisu.Name = "lblIrisu";
            this.lblIrisu.Size = new System.Drawing.Size(132, 25);
            this.lblIrisu.TabIndex = 26;
            this.lblIrisu.Text = "入          数";
            this.lblIrisu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtIrisu
            // 
            this.txtIrisu.AllowDrop = true;
            this.txtIrisu.AutoSizeFromLength = false;
            this.txtIrisu.DisplayLength = null;
            this.txtIrisu.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtIrisu.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtIrisu.Location = new System.Drawing.Point(103, 352);
            this.txtIrisu.MaxLength = 4;
            this.txtIrisu.Name = "txtIrisu";
            this.txtIrisu.Size = new System.Drawing.Size(39, 20);
            this.txtIrisu.TabIndex = 27;
            this.txtIrisu.Text = "0";
            this.txtIrisu.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtIrisu.Validating += new System.ComponentModel.CancelEventHandler(this.txtIrisu_Validating);
            // 
            // lblTani
            // 
            this.lblTani.BackColor = System.Drawing.Color.Silver;
            this.lblTani.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTani.Location = new System.Drawing.Point(12, 380);
            this.lblTani.Name = "lblTani";
            this.lblTani.Size = new System.Drawing.Size(132, 25);
            this.lblTani.TabIndex = 28;
            this.lblTani.Text = "単          位";
            this.lblTani.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTani
            // 
            this.txtTani.AllowDrop = true;
            this.txtTani.AutoSizeFromLength = false;
            this.txtTani.DisplayLength = null;
            this.txtTani.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTani.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtTani.Location = new System.Drawing.Point(103, 382);
            this.txtTani.MaxLength = 4;
            this.txtTani.Name = "txtTani";
            this.txtTani.Size = new System.Drawing.Size(39, 20);
            this.txtTani.TabIndex = 29;
            this.txtTani.Validating += new System.ComponentModel.CancelEventHandler(this.txtTani_Validating);
            // 
            // lblZaikoKanriKbnResults
            // 
            this.lblZaikoKanriKbnResults.BackColor = System.Drawing.Color.Silver;
            this.lblZaikoKanriKbnResults.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblZaikoKanriKbnResults.Location = new System.Drawing.Point(148, 228);
            this.lblZaikoKanriKbnResults.Name = "lblZaikoKanriKbnResults";
            this.lblZaikoKanriKbnResults.Size = new System.Drawing.Size(234, 25);
            this.lblZaikoKanriKbnResults.TabIndex = 16;
            this.lblZaikoKanriKbnResults.Text = " 0:管理しない  1:管理する";
            this.lblZaikoKanriKbnResults.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblUriageShiwakeCdResults
            // 
            this.lblUriageShiwakeCdResults.BackColor = System.Drawing.Color.Silver;
            this.lblUriageShiwakeCdResults.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblUriageShiwakeCdResults.Location = new System.Drawing.Point(148, 258);
            this.lblUriageShiwakeCdResults.Name = "lblUriageShiwakeCdResults";
            this.lblUriageShiwakeCdResults.Size = new System.Drawing.Size(234, 25);
            this.lblUriageShiwakeCdResults.TabIndex = 19;
            this.lblUriageShiwakeCdResults.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShiireShiwakeCdResults
            // 
            this.lblShiireShiwakeCdResults.BackColor = System.Drawing.Color.Silver;
            this.lblShiireShiwakeCdResults.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShiireShiwakeCdResults.Location = new System.Drawing.Point(148, 288);
            this.lblShiireShiwakeCdResults.Name = "lblShiireShiwakeCdResults";
            this.lblShiireShiwakeCdResults.Size = new System.Drawing.Size(234, 25);
            this.lblShiireShiwakeCdResults.TabIndex = 22;
            this.lblShiireShiwakeCdResults.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShiireTanka
            // 
            this.txtShiireTanka.AllowDrop = true;
            this.txtShiireTanka.AutoSizeFromLength = false;
            this.txtShiireTanka.DisplayLength = null;
            this.txtShiireTanka.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShiireTanka.Location = new System.Drawing.Point(481, 80);
            this.txtShiireTanka.MaxLength = 12;
            this.txtShiireTanka.Name = "txtShiireTanka";
            this.txtShiireTanka.Size = new System.Drawing.Size(130, 20);
            this.txtShiireTanka.TabIndex = 31;
            this.txtShiireTanka.Text = "0.00";
            this.txtShiireTanka.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShiireTanka.Validating += new System.ComponentModel.CancelEventHandler(this.txtShiireTanka_Validating);
            // 
            // lblShiireTanka
            // 
            this.lblShiireTanka.BackColor = System.Drawing.Color.Silver;
            this.lblShiireTanka.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShiireTanka.Location = new System.Drawing.Point(390, 78);
            this.lblShiireTanka.Name = "lblShiireTanka";
            this.lblShiireTanka.Size = new System.Drawing.Size(224, 25);
            this.lblShiireTanka.TabIndex = 30;
            this.lblShiireTanka.Text = "仕  入  単  価";
            this.lblShiireTanka.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtOroshiTanka
            // 
            this.txtOroshiTanka.AllowDrop = true;
            this.txtOroshiTanka.AutoSizeFromLength = false;
            this.txtOroshiTanka.DisplayLength = null;
            this.txtOroshiTanka.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtOroshiTanka.Location = new System.Drawing.Point(481, 110);
            this.txtOroshiTanka.MaxLength = 12;
            this.txtOroshiTanka.Name = "txtOroshiTanka";
            this.txtOroshiTanka.Size = new System.Drawing.Size(130, 20);
            this.txtOroshiTanka.TabIndex = 33;
            this.txtOroshiTanka.Text = "0.00";
            this.txtOroshiTanka.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtOroshiTanka.Validating += new System.ComponentModel.CancelEventHandler(this.txtOroshiTanka_Validating);
            // 
            // lblOroshiTanka
            // 
            this.lblOroshiTanka.BackColor = System.Drawing.Color.Silver;
            this.lblOroshiTanka.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblOroshiTanka.Location = new System.Drawing.Point(390, 108);
            this.lblOroshiTanka.Name = "lblOroshiTanka";
            this.lblOroshiTanka.Size = new System.Drawing.Size(224, 25);
            this.lblOroshiTanka.TabIndex = 32;
            this.lblOroshiTanka.Text = "卸    単    価";
            this.lblOroshiTanka.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKouriTanka
            // 
            this.txtKouriTanka.AllowDrop = true;
            this.txtKouriTanka.AutoSizeFromLength = false;
            this.txtKouriTanka.DisplayLength = null;
            this.txtKouriTanka.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKouriTanka.Location = new System.Drawing.Point(481, 140);
            this.txtKouriTanka.MaxLength = 12;
            this.txtKouriTanka.Name = "txtKouriTanka";
            this.txtKouriTanka.Size = new System.Drawing.Size(130, 20);
            this.txtKouriTanka.TabIndex = 35;
            this.txtKouriTanka.Text = "0.00";
            this.txtKouriTanka.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKouriTanka.Validating += new System.ComponentModel.CancelEventHandler(this.txtKouriTanka_Validating);
            // 
            // lblKouriTanka
            // 
            this.lblKouriTanka.BackColor = System.Drawing.Color.Silver;
            this.lblKouriTanka.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKouriTanka.Location = new System.Drawing.Point(390, 138);
            this.lblKouriTanka.Name = "lblKouriTanka";
            this.lblKouriTanka.Size = new System.Drawing.Size(224, 25);
            this.lblKouriTanka.TabIndex = 34;
            this.lblKouriTanka.Text = "小  売  単  価";
            this.lblKouriTanka.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShohinKbn1Results
            // 
            this.lblShohinKbn1Results.BackColor = System.Drawing.Color.Silver;
            this.lblShohinKbn1Results.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShohinKbn1Results.Location = new System.Drawing.Point(526, 168);
            this.lblShohinKbn1Results.Name = "lblShohinKbn1Results";
            this.lblShohinKbn1Results.Size = new System.Drawing.Size(214, 25);
            this.lblShohinKbn1Results.TabIndex = 38;
            this.lblShohinKbn1Results.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShohinKbn1
            // 
            this.txtShohinKbn1.AllowDrop = true;
            this.txtShohinKbn1.AutoSizeFromLength = false;
            this.txtShohinKbn1.DisplayLength = null;
            this.txtShohinKbn1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShohinKbn1.Location = new System.Drawing.Point(481, 170);
            this.txtShohinKbn1.MaxLength = 4;
            this.txtShohinKbn1.Name = "txtShohinKbn1";
            this.txtShohinKbn1.Size = new System.Drawing.Size(39, 20);
            this.txtShohinKbn1.TabIndex = 37;
            this.txtShohinKbn1.Text = "0";
            this.txtShohinKbn1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShohinKbn1.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohinKbn1_Validating);
            // 
            // lblShohinKbn1
            // 
            this.lblShohinKbn1.BackColor = System.Drawing.Color.Silver;
            this.lblShohinKbn1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShohinKbn1.Location = new System.Drawing.Point(390, 168);
            this.lblShohinKbn1.Name = "lblShohinKbn1";
            this.lblShohinKbn1.Size = new System.Drawing.Size(132, 25);
            this.lblShohinKbn1.TabIndex = 36;
            this.lblShohinKbn1.Text = "商 品 区 分 1";
            this.lblShohinKbn1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShohinKbn2Results
            // 
            this.lblShohinKbn2Results.BackColor = System.Drawing.Color.Silver;
            this.lblShohinKbn2Results.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShohinKbn2Results.Location = new System.Drawing.Point(526, 198);
            this.lblShohinKbn2Results.Name = "lblShohinKbn2Results";
            this.lblShohinKbn2Results.Size = new System.Drawing.Size(214, 25);
            this.lblShohinKbn2Results.TabIndex = 41;
            this.lblShohinKbn2Results.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShohinKbn2
            // 
            this.txtShohinKbn2.AllowDrop = true;
            this.txtShohinKbn2.AutoSizeFromLength = false;
            this.txtShohinKbn2.DisplayLength = null;
            this.txtShohinKbn2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShohinKbn2.Location = new System.Drawing.Point(481, 200);
            this.txtShohinKbn2.MaxLength = 4;
            this.txtShohinKbn2.Name = "txtShohinKbn2";
            this.txtShohinKbn2.Size = new System.Drawing.Size(39, 20);
            this.txtShohinKbn2.TabIndex = 40;
            this.txtShohinKbn2.Text = "0";
            this.txtShohinKbn2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShohinKbn2.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohinKbn2_Validating);
            // 
            // lblShohinKbn2
            // 
            this.lblShohinKbn2.BackColor = System.Drawing.Color.Silver;
            this.lblShohinKbn2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShohinKbn2.Location = new System.Drawing.Point(390, 198);
            this.lblShohinKbn2.Name = "lblShohinKbn2";
            this.lblShohinKbn2.Size = new System.Drawing.Size(132, 25);
            this.lblShohinKbn2.TabIndex = 39;
            this.lblShohinKbn2.Text = "商 品 区 分 2";
            this.lblShohinKbn2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShohinKbn3Results
            // 
            this.lblShohinKbn3Results.BackColor = System.Drawing.Color.Silver;
            this.lblShohinKbn3Results.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShohinKbn3Results.Location = new System.Drawing.Point(526, 228);
            this.lblShohinKbn3Results.Name = "lblShohinKbn3Results";
            this.lblShohinKbn3Results.Size = new System.Drawing.Size(214, 25);
            this.lblShohinKbn3Results.TabIndex = 44;
            this.lblShohinKbn3Results.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShohinKbn3
            // 
            this.txtShohinKbn3.AllowDrop = true;
            this.txtShohinKbn3.AutoSizeFromLength = false;
            this.txtShohinKbn3.DisplayLength = null;
            this.txtShohinKbn3.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShohinKbn3.Location = new System.Drawing.Point(481, 230);
            this.txtShohinKbn3.MaxLength = 4;
            this.txtShohinKbn3.Name = "txtShohinKbn3";
            this.txtShohinKbn3.Size = new System.Drawing.Size(39, 20);
            this.txtShohinKbn3.TabIndex = 43;
            this.txtShohinKbn3.Text = "0";
            this.txtShohinKbn3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShohinKbn3.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohinKbn3_Validating);
            // 
            // lblShohinKbn3
            // 
            this.lblShohinKbn3.BackColor = System.Drawing.Color.Silver;
            this.lblShohinKbn3.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShohinKbn3.Location = new System.Drawing.Point(390, 228);
            this.lblShohinKbn3.Name = "lblShohinKbn3";
            this.lblShohinKbn3.Size = new System.Drawing.Size(132, 25);
            this.lblShohinKbn3.TabIndex = 42;
            this.lblShohinKbn3.Text = "商 品 分 類";
            this.lblShohinKbn3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShohinKbn4Results
            // 
            this.lblShohinKbn4Results.BackColor = System.Drawing.Color.Silver;
            this.lblShohinKbn4Results.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShohinKbn4Results.Location = new System.Drawing.Point(526, 258);
            this.lblShohinKbn4Results.Name = "lblShohinKbn4Results";
            this.lblShohinKbn4Results.Size = new System.Drawing.Size(214, 25);
            this.lblShohinKbn4Results.TabIndex = 47;
            this.lblShohinKbn4Results.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShohinKbn4
            // 
            this.txtShohinKbn4.AllowDrop = true;
            this.txtShohinKbn4.AutoSizeFromLength = false;
            this.txtShohinKbn4.DisplayLength = null;
            this.txtShohinKbn4.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShohinKbn4.Location = new System.Drawing.Point(481, 260);
            this.txtShohinKbn4.MaxLength = 4;
            this.txtShohinKbn4.Name = "txtShohinKbn4";
            this.txtShohinKbn4.Size = new System.Drawing.Size(39, 20);
            this.txtShohinKbn4.TabIndex = 46;
            this.txtShohinKbn4.Text = "0";
            this.txtShohinKbn4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShohinKbn4.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohinKbn4_Validating);
            // 
            // lblShohinKbn4
            // 
            this.lblShohinKbn4.BackColor = System.Drawing.Color.Silver;
            this.lblShohinKbn4.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShohinKbn4.Location = new System.Drawing.Point(390, 258);
            this.lblShohinKbn4.Name = "lblShohinKbn4";
            this.lblShohinKbn4.Size = new System.Drawing.Size(132, 25);
            this.lblShohinKbn4.TabIndex = 45;
            this.lblShohinKbn4.Text = "商 品 区 分 4";
            this.lblShohinKbn4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtBarcodeCase
            // 
            this.txtBarcodeCase.AllowDrop = true;
            this.txtBarcodeCase.AutoSizeFromLength = false;
            this.txtBarcodeCase.DisplayLength = null;
            this.txtBarcodeCase.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtBarcodeCase.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtBarcodeCase.Location = new System.Drawing.Point(481, 290);
            this.txtBarcodeCase.MaxLength = 13;
            this.txtBarcodeCase.Name = "txtBarcodeCase";
            this.txtBarcodeCase.Size = new System.Drawing.Size(130, 20);
            this.txtBarcodeCase.TabIndex = 49;
            this.txtBarcodeCase.Validating += new System.ComponentModel.CancelEventHandler(this.txtBarcodeCase_Validating);
            // 
            // lblBarcodeCase
            // 
            this.lblBarcodeCase.BackColor = System.Drawing.Color.Silver;
            this.lblBarcodeCase.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBarcodeCase.Location = new System.Drawing.Point(390, 288);
            this.lblBarcodeCase.Name = "lblBarcodeCase";
            this.lblBarcodeCase.Size = new System.Drawing.Size(224, 25);
            this.lblBarcodeCase.TabIndex = 48;
            this.lblBarcodeCase.Text = "ﾊﾞｰｺｰﾄﾞ (ｹｰｽ)";
            this.lblBarcodeCase.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtBarcodeBara
            // 
            this.txtBarcodeBara.AllowDrop = true;
            this.txtBarcodeBara.AutoSizeFromLength = false;
            this.txtBarcodeBara.DisplayLength = null;
            this.txtBarcodeBara.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtBarcodeBara.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtBarcodeBara.Location = new System.Drawing.Point(481, 320);
            this.txtBarcodeBara.MaxLength = 13;
            this.txtBarcodeBara.Name = "txtBarcodeBara";
            this.txtBarcodeBara.Size = new System.Drawing.Size(130, 20);
            this.txtBarcodeBara.TabIndex = 51;
            this.txtBarcodeBara.Validating += new System.ComponentModel.CancelEventHandler(this.txtBarcodeBara_Validating);
            // 
            // lblBarcodeBara
            // 
            this.lblBarcodeBara.BackColor = System.Drawing.Color.Silver;
            this.lblBarcodeBara.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBarcodeBara.Location = new System.Drawing.Point(390, 318);
            this.lblBarcodeBara.Name = "lblBarcodeBara";
            this.lblBarcodeBara.Size = new System.Drawing.Size(224, 25);
            this.lblBarcodeBara.TabIndex = 50;
            this.lblBarcodeBara.Text = "ﾊﾞｰｺｰﾄﾞ (ﾊﾞﾗ)";
            this.lblBarcodeBara.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTekiseisu
            // 
            this.txtTekiseisu.AllowDrop = true;
            this.txtTekiseisu.AutoSizeFromLength = false;
            this.txtTekiseisu.DisplayLength = null;
            this.txtTekiseisu.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTekiseisu.Location = new System.Drawing.Point(481, 350);
            this.txtTekiseisu.MaxLength = 12;
            this.txtTekiseisu.Name = "txtTekiseisu";
            this.txtTekiseisu.Size = new System.Drawing.Size(130, 20);
            this.txtTekiseisu.TabIndex = 53;
            this.txtTekiseisu.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTekiseisu.Validating += new System.ComponentModel.CancelEventHandler(this.txtTekiseisu_Validating);
            // 
            // lblTekiseisu
            // 
            this.lblTekiseisu.BackColor = System.Drawing.Color.Silver;
            this.lblTekiseisu.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTekiseisu.Location = new System.Drawing.Point(390, 348);
            this.lblTekiseisu.Name = "lblTekiseisu";
            this.lblTekiseisu.Size = new System.Drawing.Size(224, 25);
            this.lblTekiseisu.TabIndex = 52;
            this.lblTekiseisu.Text = "適    正    数";
            this.lblTekiseisu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShishoCd
            // 
            this.txtShishoCd.AutoSizeFromLength = true;
            this.txtShishoCd.DisplayLength = null;
            this.txtShishoCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtShishoCd.Location = new System.Drawing.Point(89, 48);
            this.txtShishoCd.MaxLength = 4;
            this.txtShishoCd.Name = "txtShishoCd";
            this.txtShishoCd.Size = new System.Drawing.Size(34, 20);
            this.txtShishoCd.TabIndex = 2;
            this.txtShishoCd.TabStop = false;
            this.txtShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtShishoCd_Validating);
            // 
            // lblShishoNm
            // 
            this.lblShishoNm.BackColor = System.Drawing.Color.Silver;
            this.lblShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShishoNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShishoNm.Location = new System.Drawing.Point(145, 49);
            this.lblShishoNm.Name = "lblShishoNm";
            this.lblShishoNm.Size = new System.Drawing.Size(212, 20);
            this.lblShishoNm.TabIndex = 3;
            this.lblShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShisho
            // 
            this.lblShisho.BackColor = System.Drawing.Color.Silver;
            this.lblShisho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShisho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblShisho.Location = new System.Drawing.Point(12, 46);
            this.lblShisho.Name = "lblShisho";
            this.lblShisho.Size = new System.Drawing.Size(350, 25);
            this.lblShisho.TabIndex = 1;
            this.lblShisho.Text = "支　　所";
            this.lblShisho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Silver;
            this.label1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(148, 318);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(234, 25);
            this.label1.TabIndex = 25;
            this.label1.Text = " 0:外税　1:内税";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShohizeiKbn
            // 
            this.txtShohizeiKbn.AllowDrop = true;
            this.txtShohizeiKbn.AutoSizeFromLength = false;
            this.txtShohizeiKbn.DisplayLength = null;
            this.txtShohizeiKbn.Enabled = false;
            this.txtShohizeiKbn.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShohizeiKbn.Location = new System.Drawing.Point(103, 320);
            this.txtShohizeiKbn.MaxLength = 1;
            this.txtShohizeiKbn.Name = "txtShohizeiKbn";
            this.txtShohizeiKbn.Size = new System.Drawing.Size(39, 20);
            this.txtShohizeiKbn.TabIndex = 24;
            this.txtShohizeiKbn.Text = "0";
            this.txtShohizeiKbn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShohizeiKbn.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohizeiKbn_Validating);
            // 
            // lblShohiZeiKbn
            // 
            this.lblShohiZeiKbn.BackColor = System.Drawing.Color.Silver;
            this.lblShohiZeiKbn.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShohiZeiKbn.Location = new System.Drawing.Point(12, 318);
            this.lblShohiZeiKbn.Name = "lblShohiZeiKbn";
            this.lblShohiZeiKbn.Size = new System.Drawing.Size(132, 25);
            this.lblShohiZeiKbn.TabIndex = 23;
            this.lblShohiZeiKbn.Text = "消 費 税 区 分";
            this.lblShohiZeiKbn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblChushiKbnNm
            // 
            this.lblChushiKbnNm.BackColor = System.Drawing.Color.Silver;
            this.lblChushiKbnNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblChushiKbnNm.Location = new System.Drawing.Point(526, 377);
            this.lblChushiKbnNm.Name = "lblChushiKbnNm";
            this.lblChushiKbnNm.Size = new System.Drawing.Size(234, 25);
            this.lblChushiKbnNm.TabIndex = 56;
            this.lblChushiKbnNm.Text = " 1: 取引有　2:取引中止";
            this.lblChushiKbnNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtChushiKbn
            // 
            this.txtChushiKbn.AllowDrop = true;
            this.txtChushiKbn.AutoSizeFromLength = false;
            this.txtChushiKbn.DisplayLength = null;
            this.txtChushiKbn.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtChushiKbn.Location = new System.Drawing.Point(481, 379);
            this.txtChushiKbn.MaxLength = 1;
            this.txtChushiKbn.Name = "txtChushiKbn";
            this.txtChushiKbn.Size = new System.Drawing.Size(39, 20);
            this.txtChushiKbn.TabIndex = 55;
            this.txtChushiKbn.Text = "0";
            this.txtChushiKbn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtChushiKbn.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtChushiKbn_KeyDown);
            this.txtChushiKbn.Validating += new System.ComponentModel.CancelEventHandler(this.txtChushiKbn_Validating);
            // 
            // lblChuchiKbn
            // 
            this.lblChuchiKbn.BackColor = System.Drawing.Color.Silver;
            this.lblChuchiKbn.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblChuchiKbn.Location = new System.Drawing.Point(390, 377);
            this.lblChuchiKbn.Name = "lblChuchiKbn";
            this.lblChuchiKbn.Size = new System.Drawing.Size(132, 25);
            this.lblChuchiKbn.TabIndex = 54;
            this.lblChuchiKbn.Text = "中　止　区　分";
            this.lblChuchiKbn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // KBCM1022
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 516);
            this.Controls.Add(this.lblChushiKbnNm);
            this.Controls.Add(this.txtChushiKbn);
            this.Controls.Add(this.lblChuchiKbn);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtShohizeiKbn);
            this.Controls.Add(this.lblShohiZeiKbn);
            this.Controls.Add(this.txtShishoCd);
            this.Controls.Add(this.lblShishoNm);
            this.Controls.Add(this.lblShisho);
            this.Controls.Add(this.txtTekiseisu);
            this.Controls.Add(this.lblTekiseisu);
            this.Controls.Add(this.txtBarcodeBara);
            this.Controls.Add(this.lblBarcodeBara);
            this.Controls.Add(this.txtBarcodeCase);
            this.Controls.Add(this.lblBarcodeCase);
            this.Controls.Add(this.lblShohinKbn4Results);
            this.Controls.Add(this.txtShohinKbn4);
            this.Controls.Add(this.lblShohinKbn4);
            this.Controls.Add(this.lblShohinKbn3Results);
            this.Controls.Add(this.txtShohinKbn3);
            this.Controls.Add(this.lblShohinKbn3);
            this.Controls.Add(this.lblShohinKbn2Results);
            this.Controls.Add(this.txtShohinKbn2);
            this.Controls.Add(this.lblShohinKbn2);
            this.Controls.Add(this.lblShohinKbn1Results);
            this.Controls.Add(this.txtShohinKbn1);
            this.Controls.Add(this.lblShohinKbn1);
            this.Controls.Add(this.txtKouriTanka);
            this.Controls.Add(this.lblKouriTanka);
            this.Controls.Add(this.txtOroshiTanka);
            this.Controls.Add(this.lblOroshiTanka);
            this.Controls.Add(this.txtShiireTanka);
            this.Controls.Add(this.lblShiireTanka);
            this.Controls.Add(this.lblShiireShiwakeCdResults);
            this.Controls.Add(this.lblUriageShiwakeCdResults);
            this.Controls.Add(this.lblZaikoKanriKbnResults);
            this.Controls.Add(this.txtShohinNm);
            this.Controls.Add(this.lblShohinNm);
            this.Controls.Add(this.txtShohinKikaku);
            this.Controls.Add(this.txtShohinCd);
            this.Controls.Add(this.lblShohinKikaku);
            this.Controls.Add(this.lblShohinCd);
            this.Controls.Add(this.txtShohinKanaNm);
            this.Controls.Add(this.lblShohinKanaNm);
            this.Controls.Add(this.txtTanaban);
            this.Controls.Add(this.lblTanaban);
            this.Controls.Add(this.txtZaikoKanriKbn);
            this.Controls.Add(this.lblZaikoKanriKbn);
            this.Controls.Add(this.txtUriageShiwakeCd);
            this.Controls.Add(this.lblUriageShiwakeCd);
            this.Controls.Add(this.txtShiireShiwakeCd);
            this.Controls.Add(this.lblShiireShiwakeCd);
            this.Controls.Add(this.txtIrisu);
            this.Controls.Add(this.lblIrisu);
            this.Controls.Add(this.txtTani);
            this.Controls.Add(this.lblTani);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "KBCM1022";
            this.Par1 = "";
            this.Par2 = "";
            this.ShowFButton = true;
            this.Text = "";
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.lblTani, 0);
            this.Controls.SetChildIndex(this.txtTani, 0);
            this.Controls.SetChildIndex(this.lblIrisu, 0);
            this.Controls.SetChildIndex(this.txtIrisu, 0);
            this.Controls.SetChildIndex(this.lblShiireShiwakeCd, 0);
            this.Controls.SetChildIndex(this.txtShiireShiwakeCd, 0);
            this.Controls.SetChildIndex(this.lblUriageShiwakeCd, 0);
            this.Controls.SetChildIndex(this.txtUriageShiwakeCd, 0);
            this.Controls.SetChildIndex(this.lblZaikoKanriKbn, 0);
            this.Controls.SetChildIndex(this.txtZaikoKanriKbn, 0);
            this.Controls.SetChildIndex(this.lblTanaban, 0);
            this.Controls.SetChildIndex(this.txtTanaban, 0);
            this.Controls.SetChildIndex(this.lblShohinKanaNm, 0);
            this.Controls.SetChildIndex(this.txtShohinKanaNm, 0);
            this.Controls.SetChildIndex(this.lblShohinCd, 0);
            this.Controls.SetChildIndex(this.lblShohinKikaku, 0);
            this.Controls.SetChildIndex(this.txtShohinCd, 0);
            this.Controls.SetChildIndex(this.txtShohinKikaku, 0);
            this.Controls.SetChildIndex(this.lblShohinNm, 0);
            this.Controls.SetChildIndex(this.txtShohinNm, 0);
            this.Controls.SetChildIndex(this.lblZaikoKanriKbnResults, 0);
            this.Controls.SetChildIndex(this.lblUriageShiwakeCdResults, 0);
            this.Controls.SetChildIndex(this.lblShiireShiwakeCdResults, 0);
            this.Controls.SetChildIndex(this.lblShiireTanka, 0);
            this.Controls.SetChildIndex(this.txtShiireTanka, 0);
            this.Controls.SetChildIndex(this.lblOroshiTanka, 0);
            this.Controls.SetChildIndex(this.txtOroshiTanka, 0);
            this.Controls.SetChildIndex(this.lblKouriTanka, 0);
            this.Controls.SetChildIndex(this.txtKouriTanka, 0);
            this.Controls.SetChildIndex(this.lblShohinKbn1, 0);
            this.Controls.SetChildIndex(this.txtShohinKbn1, 0);
            this.Controls.SetChildIndex(this.lblShohinKbn1Results, 0);
            this.Controls.SetChildIndex(this.lblShohinKbn2, 0);
            this.Controls.SetChildIndex(this.txtShohinKbn2, 0);
            this.Controls.SetChildIndex(this.lblShohinKbn2Results, 0);
            this.Controls.SetChildIndex(this.lblShohinKbn3, 0);
            this.Controls.SetChildIndex(this.txtShohinKbn3, 0);
            this.Controls.SetChildIndex(this.lblShohinKbn3Results, 0);
            this.Controls.SetChildIndex(this.lblShohinKbn4, 0);
            this.Controls.SetChildIndex(this.txtShohinKbn4, 0);
            this.Controls.SetChildIndex(this.lblShohinKbn4Results, 0);
            this.Controls.SetChildIndex(this.lblBarcodeCase, 0);
            this.Controls.SetChildIndex(this.txtBarcodeCase, 0);
            this.Controls.SetChildIndex(this.lblBarcodeBara, 0);
            this.Controls.SetChildIndex(this.txtBarcodeBara, 0);
            this.Controls.SetChildIndex(this.lblTekiseisu, 0);
            this.Controls.SetChildIndex(this.txtTekiseisu, 0);
            this.Controls.SetChildIndex(this.lblShisho, 0);
            this.Controls.SetChildIndex(this.lblShishoNm, 0);
            this.Controls.SetChildIndex(this.txtShishoCd, 0);
            this.Controls.SetChildIndex(this.lblShohiZeiKbn, 0);
            this.Controls.SetChildIndex(this.txtShohizeiKbn, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.lblChuchiKbn, 0);
            this.Controls.SetChildIndex(this.txtChushiKbn, 0);
            this.Controls.SetChildIndex(this.lblChushiKbnNm, 0);
            this.pnlDebug.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblShohinCd;
        private jp.co.fsi.common.controls.FsiTextBox txtShohinCd;
        private System.Windows.Forms.Label lblShohinNm;
        private jp.co.fsi.common.controls.FsiTextBox txtShohinNm;
        private System.Windows.Forms.Label lblShohinKanaNm;
        private jp.co.fsi.common.controls.FsiTextBox txtShohinKanaNm;
        private System.Windows.Forms.Label lblShohinKikaku;
        private jp.co.fsi.common.controls.FsiTextBox txtShohinKikaku;
        private System.Windows.Forms.Label lblTanaban;
        private jp.co.fsi.common.controls.FsiTextBox txtTanaban;
        private System.Windows.Forms.Label lblZaikoKanriKbn;
        private jp.co.fsi.common.controls.FsiTextBox txtZaikoKanriKbn;
        private System.Windows.Forms.Label lblUriageShiwakeCd;
        private jp.co.fsi.common.controls.FsiTextBox txtUriageShiwakeCd;
        private System.Windows.Forms.Label lblShiireShiwakeCd;
        private jp.co.fsi.common.controls.FsiTextBox txtShiireShiwakeCd;
        private System.Windows.Forms.Label lblIrisu;
        private jp.co.fsi.common.controls.FsiTextBox txtIrisu;
        private System.Windows.Forms.Label lblTani;
        private jp.co.fsi.common.controls.FsiTextBox txtTani;
        private System.Windows.Forms.Label lblZaikoKanriKbnResults;
        private System.Windows.Forms.Label lblUriageShiwakeCdResults;
        private System.Windows.Forms.Label lblShiireShiwakeCdResults;
        private jp.co.fsi.common.controls.FsiTextBox txtShiireTanka;
        private System.Windows.Forms.Label lblShiireTanka;
        private jp.co.fsi.common.controls.FsiTextBox txtOroshiTanka;
        private System.Windows.Forms.Label lblOroshiTanka;
        private jp.co.fsi.common.controls.FsiTextBox txtKouriTanka;
        private System.Windows.Forms.Label lblKouriTanka;
        private System.Windows.Forms.Label lblShohinKbn1Results;
        private jp.co.fsi.common.controls.FsiTextBox txtShohinKbn1;
        private System.Windows.Forms.Label lblShohinKbn1;
        private System.Windows.Forms.Label lblShohinKbn2Results;
        private jp.co.fsi.common.controls.FsiTextBox txtShohinKbn2;
        private System.Windows.Forms.Label lblShohinKbn2;
        private System.Windows.Forms.Label lblShohinKbn3Results;
        private jp.co.fsi.common.controls.FsiTextBox txtShohinKbn3;
        private System.Windows.Forms.Label lblShohinKbn3;
        private System.Windows.Forms.Label lblShohinKbn4Results;
        private jp.co.fsi.common.controls.FsiTextBox txtShohinKbn4;
        private System.Windows.Forms.Label lblShohinKbn4;
        private jp.co.fsi.common.controls.FsiTextBox txtBarcodeCase;
        private System.Windows.Forms.Label lblBarcodeCase;
        private jp.co.fsi.common.controls.FsiTextBox txtBarcodeBara;
        private System.Windows.Forms.Label lblBarcodeBara;
        private jp.co.fsi.common.controls.FsiTextBox txtTekiseisu;
        private System.Windows.Forms.Label lblTekiseisu;
        private common.controls.FsiTextBox txtShishoCd;
        private System.Windows.Forms.Label lblShishoNm;
        private System.Windows.Forms.Label lblShisho;
        private System.Windows.Forms.Label label1;
        private common.controls.FsiTextBox txtShohizeiKbn;
        private System.Windows.Forms.Label lblShohiZeiKbn;
        private System.Windows.Forms.Label lblChushiKbnNm;
        private common.controls.FsiTextBox txtChushiKbn;
        private System.Windows.Forms.Label lblChuchiKbn;
    }
}