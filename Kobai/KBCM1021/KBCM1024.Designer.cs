﻿namespace jp.co.fsi.kb.kbcm1021
{
    partial class KBCM1024
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblShiireShiwakeCdResults = new System.Windows.Forms.Label();
            this.lblUriageShiwakeCdResults = new System.Windows.Forms.Label();
            this.txtUriageShiwakeCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblUriageShiwakeCd = new System.Windows.Forms.Label();
            this.txtShiireShiwakeCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShiireShiwakeCd = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtShohizeiKbn = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShohiZeiKbn = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Size = new System.Drawing.Size(383, 23);
            this.lblTitle.Text = "";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(5, 120);
            this.pnlDebug.Size = new System.Drawing.Size(416, 100);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtShohizeiKbn);
            this.groupBox1.Controls.Add(this.lblShohiZeiKbn);
            this.groupBox1.Controls.Add(this.lblShiireShiwakeCdResults);
            this.groupBox1.Controls.Add(this.lblUriageShiwakeCdResults);
            this.groupBox1.Controls.Add(this.txtUriageShiwakeCd);
            this.groupBox1.Controls.Add(this.lblUriageShiwakeCd);
            this.groupBox1.Controls.Add(this.txtShiireShiwakeCd);
            this.groupBox1.Controls.Add(this.lblShiireShiwakeCd);
            this.groupBox1.Location = new System.Drawing.Point(8, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(423, 148);
            this.groupBox1.TabIndex = 902;
            this.groupBox1.TabStop = false;
            // 
            // lblShiireShiwakeCdResults
            // 
            this.lblShiireShiwakeCdResults.BackColor = System.Drawing.Color.Silver;
            this.lblShiireShiwakeCdResults.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShiireShiwakeCdResults.Location = new System.Drawing.Point(149, 60);
            this.lblShiireShiwakeCdResults.Name = "lblShiireShiwakeCdResults";
            this.lblShiireShiwakeCdResults.Size = new System.Drawing.Size(234, 25);
            this.lblShiireShiwakeCdResults.TabIndex = 914;
            this.lblShiireShiwakeCdResults.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblUriageShiwakeCdResults
            // 
            this.lblUriageShiwakeCdResults.BackColor = System.Drawing.Color.Silver;
            this.lblUriageShiwakeCdResults.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblUriageShiwakeCdResults.Location = new System.Drawing.Point(149, 30);
            this.lblUriageShiwakeCdResults.Name = "lblUriageShiwakeCdResults";
            this.lblUriageShiwakeCdResults.Size = new System.Drawing.Size(234, 25);
            this.lblUriageShiwakeCdResults.TabIndex = 911;
            this.lblUriageShiwakeCdResults.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtUriageShiwakeCd
            // 
            this.txtUriageShiwakeCd.AllowDrop = true;
            this.txtUriageShiwakeCd.AutoSizeFromLength = false;
            this.txtUriageShiwakeCd.DisplayLength = null;
            this.txtUriageShiwakeCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtUriageShiwakeCd.Location = new System.Drawing.Point(104, 32);
            this.txtUriageShiwakeCd.MaxLength = 4;
            this.txtUriageShiwakeCd.Name = "txtUriageShiwakeCd";
            this.txtUriageShiwakeCd.Size = new System.Drawing.Size(39, 20);
            this.txtUriageShiwakeCd.TabIndex = 910;
            this.txtUriageShiwakeCd.Text = "0";
            this.txtUriageShiwakeCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtUriageShiwakeCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtUriageShiwakeCd_Validating);
            // 
            // lblUriageShiwakeCd
            // 
            this.lblUriageShiwakeCd.BackColor = System.Drawing.Color.Silver;
            this.lblUriageShiwakeCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblUriageShiwakeCd.Location = new System.Drawing.Point(13, 30);
            this.lblUriageShiwakeCd.Name = "lblUriageShiwakeCd";
            this.lblUriageShiwakeCd.Size = new System.Drawing.Size(132, 25);
            this.lblUriageShiwakeCd.TabIndex = 909;
            this.lblUriageShiwakeCd.Text = "売上仕訳コード";
            this.lblUriageShiwakeCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShiireShiwakeCd
            // 
            this.txtShiireShiwakeCd.AllowDrop = true;
            this.txtShiireShiwakeCd.AutoSizeFromLength = false;
            this.txtShiireShiwakeCd.DisplayLength = null;
            this.txtShiireShiwakeCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShiireShiwakeCd.Location = new System.Drawing.Point(104, 62);
            this.txtShiireShiwakeCd.MaxLength = 4;
            this.txtShiireShiwakeCd.Name = "txtShiireShiwakeCd";
            this.txtShiireShiwakeCd.Size = new System.Drawing.Size(39, 20);
            this.txtShiireShiwakeCd.TabIndex = 913;
            this.txtShiireShiwakeCd.Text = "0";
            this.txtShiireShiwakeCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShiireShiwakeCd.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtShiireShiwakeCd_KeyDown);
            this.txtShiireShiwakeCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtShiireShiwakeCd_Validating);
            // 
            // lblShiireShiwakeCd
            // 
            this.lblShiireShiwakeCd.BackColor = System.Drawing.Color.Silver;
            this.lblShiireShiwakeCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShiireShiwakeCd.Location = new System.Drawing.Point(13, 60);
            this.lblShiireShiwakeCd.Name = "lblShiireShiwakeCd";
            this.lblShiireShiwakeCd.Size = new System.Drawing.Size(132, 25);
            this.lblShiireShiwakeCd.TabIndex = 912;
            this.lblShiireShiwakeCd.Text = "仕入仕訳コード";
            this.lblShiireShiwakeCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Silver;
            this.label1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(149, 90);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(234, 25);
            this.label1.TabIndex = 917;
            this.label1.Text = " 0:外税　1:内税";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShohizeiKbn
            // 
            this.txtShohizeiKbn.AllowDrop = true;
            this.txtShohizeiKbn.AutoSizeFromLength = false;
            this.txtShohizeiKbn.DisplayLength = null;
            this.txtShohizeiKbn.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShohizeiKbn.Location = new System.Drawing.Point(104, 92);
            this.txtShohizeiKbn.MaxLength = 4;
            this.txtShohizeiKbn.Name = "txtShohizeiKbn";
            this.txtShohizeiKbn.Size = new System.Drawing.Size(39, 20);
            this.txtShohizeiKbn.TabIndex = 916;
            this.txtShohizeiKbn.Text = "0";
            this.txtShohizeiKbn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShohizeiKbn.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtShohizeiKbn_KeyDown);
            this.txtShohizeiKbn.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohizeiKbn_Validating);
            // 
            // lblShohiZeiKbn
            // 
            this.lblShohiZeiKbn.BackColor = System.Drawing.Color.Silver;
            this.lblShohiZeiKbn.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShohiZeiKbn.Location = new System.Drawing.Point(13, 90);
            this.lblShohiZeiKbn.Name = "lblShohiZeiKbn";
            this.lblShohiZeiKbn.Size = new System.Drawing.Size(132, 25);
            this.lblShohiZeiKbn.TabIndex = 915;
            this.lblShohiZeiKbn.Text = "消 費 税 区 分";
            this.lblShohiZeiKbn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // KBCM1024
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(408, 223);
            this.Controls.Add(this.groupBox1);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "KBCM1024";
            this.ShowFButton = true;
            this.Text = "商品マスタ初期設定";
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.pnlDebug.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblShiireShiwakeCdResults;
        private System.Windows.Forms.Label lblUriageShiwakeCdResults;
        private common.controls.FsiTextBox txtUriageShiwakeCd;
        private System.Windows.Forms.Label lblUriageShiwakeCd;
        private common.controls.FsiTextBox txtShiireShiwakeCd;
        private System.Windows.Forms.Label lblShiireShiwakeCd;
        private System.Windows.Forms.Label label1;
        private common.controls.FsiTextBox txtShohizeiKbn;
        private System.Windows.Forms.Label lblShohiZeiKbn;
    }
}