﻿using System;
using System.ComponentModel;
using System.Reflection;
using System.Windows.Forms;

using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.kb.kbcm1021
{
    /// <summary>
    /// 商品登録の初期値設定(KBCM1024)
    /// </summary>
    public partial class KBCM1024 : BasePgForm
    {

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KBCM1024()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {

            // タイトルは非表示
            this.lblTitle.Visible = false;

            this.btnEsc.Location = this.btnF1.Location;
            this.btnF1.Location = this.btnF2.Location;
            this.btnF6.Location = this.btnF3.Location;
            this.btnF3.Visible = false;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;

            // 初期設定値の表示
            InitDisp();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 売上仕訳コード,仕入仕訳コード,商品区分１～５にフォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtUriageShiwakeCd":
                case "txtShiireShiwakeCd":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        public override void PressF1()
        {
            System.Reflection.Assembly asm;
            // フォーム作成
            Type t;

            switch (this.ActiveCtlNm)
            {
                case "txtUriageShiwakeCd":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM2051.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2051.CMCM2053");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.Par2 = this.Par1;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtUriageShiwakeCd.Text = outData[0];
                                this.lblUriageShiwakeCdResults.Text = outData[1];
                            }
                        }
                    }
                    break;

                case "txtShiireShiwakeCd":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM2051.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2051.CMCM2053");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "2";
                            frm.Par2 = this.Par1;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtShiireShiwakeCd.Text = outData[0];
                                this.lblShiireShiwakeCdResults.Text = outData[1];
                            }
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        public override void PressF6()
        {
            // 確認メッセージを表示
            string msg = "登録しますか？";

            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            SaveData();
            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 売上仕訳コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtUriageShiwakeCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidUriageShiwakeCd())
            {
                e.Cancel = true;
                this.txtUriageShiwakeCd.SelectAll();
            }
        }

        /// <summary>
        /// 仕入仕訳コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShiireShiwakeCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShiireShiwakeCd())
            {
                e.Cancel = true;
                this.txtShiireShiwakeCd.SelectAll();
            }
        }

        private void txtShiireShiwakeCd_KeyDown(object sender, KeyEventArgs e)
        {
            //if (e.KeyData == Keys.Enter)
            //{
            //    this.PressF6();
            //}
        }

        private void txtShohizeiKbn_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShohizeiKbn())
            {
                e.Cancel = true;
                this.txtShohizeiKbn.SelectAll();
            }
        }

        private void txtShohizeiKbn_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                this.PressF6();
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 新規モードの初期表示
        /// </summary>
        private void InitDisp()
        {
            // 初期値、入力制御を実装
            string ret = "";
            // 各項目の初期値を設定
            // 売上仕訳コード
            ret = Util.ToString(this.Config.LoadPgConfig(common.constants.Constants.SubSys.Kob, this.ProductName, "Setting", "UriageShiwakeCd"));
            this.txtUriageShiwakeCd.Text = ret;
            this.lblUriageShiwakeCdResults.Text = this.Dba.GetName(this.UInfo, "VI_HN_URIAGE_SHIWAKE", this.Par1, this.txtUriageShiwakeCd.Text);

            // 仕入仕訳コード
            ret = Util.ToString(this.Config.LoadPgConfig(common.constants.Constants.SubSys.Kob, this.ProductName, "Setting", "ShiireShiwakeCd"));
            this.txtShiireShiwakeCd.Text = ret;
            this.lblShiireShiwakeCdResults.Text = this.Dba.GetName(this.UInfo, "VI_HN_SHIIRE_SHIWAKE", this.Par1, this.txtShiireShiwakeCd.Text);

            // 消費税区分
            ret = Util.ToString(this.Config.LoadPgConfig(common.constants.Constants.SubSys.Kob, this.ProductName, "Setting", "ShohizeiKbn"));
            this.txtShohizeiKbn.Text = ret;

            // 売上仕訳CDに初期フォーカス
            this.ActiveControl = this.txtUriageShiwakeCd;
            this.txtUriageShiwakeCd.Focus();
        }

        private void SaveData()
        {
            string ret = "";
            // 売上仕訳コード
            ret = this.txtUriageShiwakeCd.Text;
            this.Config.SetPgConfig(common.constants.Constants.SubSys.Kob, this.ProductName, "Setting", "UriageShiwakeCd", ret);
            // 仕入仕訳コード
            ret = this.txtShiireShiwakeCd.Text;
            this.Config.SetPgConfig(common.constants.Constants.SubSys.Kob, this.ProductName, "Setting", "ShiireShiwakeCd",ret);
            // 消費税区分
            ret = this.txtShohizeiKbn.Text;
            this.Config.SetPgConfig(common.constants.Constants.SubSys.Kob, this.ProductName, "Setting", "ShohizeiKbn", ret);
            // 設定値の保存
            this.Config.SaveConfig();
        }

        /// <summary>
        /// 売上仕訳コードの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidUriageShiwakeCd()
        {
            // 未入力は0とする
            if (ValChk.IsEmpty(this.txtUriageShiwakeCd.Text))
            {
                this.txtUriageShiwakeCd.Text = "0";
            }

            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtUriageShiwakeCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 存在しないコードを入力されたらエラー
            // 但し0は許可
            if ("0".Equals(this.txtUriageShiwakeCd.Text))
            {
                this.lblUriageShiwakeCdResults.Text = string.Empty;
            }
            else
            {
                string name = this.Dba.GetName(this.UInfo, "VI_HN_URIAGE_SHIWAKE", this.Par1, this.txtUriageShiwakeCd.Text);
                if (ValChk.IsEmpty(name))
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }

                this.lblUriageShiwakeCdResults.Text = name;
            }

            return true;
        }

        /// <summary>
        /// 仕入仕訳コードの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShiireShiwakeCd()
        {
            // 未入力は0とする
            if (ValChk.IsEmpty(this.txtShiireShiwakeCd.Text))
            {
                this.txtShiireShiwakeCd.Text = "0";
            }

            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtShiireShiwakeCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 存在しないコードを入力されたらエラー
            // 但し0は許可
            if ("0".Equals(this.txtShiireShiwakeCd.Text))
            {
                this.lblShiireShiwakeCdResults.Text = string.Empty;
            }
            else
            {
                string name = this.Dba.GetName(this.UInfo, "VI_HN_SHIIRE_SHIWAKE", this.Par1, this.txtShiireShiwakeCd.Text);
                if (ValChk.IsEmpty(name))
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }

                this.lblShiireShiwakeCdResults.Text = name;
            }

            return true;
        }

        /// <summary>
        /// 消費税区分の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShohizeiKbn()
        {
            // 空の場合は、デフォルト値を表示
            if (ValChk.IsEmpty(this.txtShohizeiKbn.Text))
            {
                this.txtShohizeiKbn.Text = "0";
            }

            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtShohizeiKbn.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 1バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtShohizeiKbn.Text, this.txtShohizeiKbn.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 1より大きい場合は、1として処理
            int shohizeiKbn = Util.ToInt(this.txtShohizeiKbn.Text);
            if (shohizeiKbn > 1)
            {
                this.txtShohizeiKbn.Text = "1";
            }

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 売上仕訳コードのチェック
            if (!IsValidUriageShiwakeCd())
            {
                this.txtUriageShiwakeCd.Focus();
                return false;
            }

            // 仕入仕訳コードのチェック
            if (!IsValidShiireShiwakeCd())
            {
                this.txtShiireShiwakeCd.Focus();
                return false;
            }

            // 消費税区分のチェック
            if (!IsValidShohizeiKbn())
            {
                this.txtShohizeiKbn.SelectAll();
                return false;
            }

            return true;
        }
        #endregion
    }
}
