﻿namespace jp.co.fsi.kb.kbcm1021
{
    partial class KBCM1023
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbxShohinCd = new System.Windows.Forms.GroupBox();
            this.lblShohinCdTo = new System.Windows.Forms.Label();
            this.lblShohinCdFr = new System.Windows.Forms.Label();
            this.txtShohinCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtShohinCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblShishoCdTo = new System.Windows.Forms.Label();
            this.lblShishoCdFr = new System.Windows.Forms.Label();
            this.txtShishoCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtShishoCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.pnlDebug.SuspendLayout();
            this.gbxShohinCd.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Size = new System.Drawing.Size(511, 23);
            this.lblTitle.Text = "";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(5, 93);
            this.pnlDebug.Size = new System.Drawing.Size(544, 100);
            // 
            // gbxShohinCd
            // 
            this.gbxShohinCd.Controls.Add(this.lblShohinCdTo);
            this.gbxShohinCd.Controls.Add(this.lblShohinCdFr);
            this.gbxShohinCd.Controls.Add(this.txtShohinCdTo);
            this.gbxShohinCd.Controls.Add(this.label1);
            this.gbxShohinCd.Controls.Add(this.txtShohinCdFr);
            this.gbxShohinCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxShohinCd.ForeColor = System.Drawing.SystemColors.ControlText;
            this.gbxShohinCd.Location = new System.Drawing.Point(8, 78);
            this.gbxShohinCd.Name = "gbxShohinCd";
            this.gbxShohinCd.Size = new System.Drawing.Size(520, 56);
            this.gbxShohinCd.TabIndex = 1;
            this.gbxShohinCd.TabStop = false;
            this.gbxShohinCd.Text = "商品コード範囲";
            // 
            // lblShohinCdTo
            // 
            this.lblShohinCdTo.BackColor = System.Drawing.Color.Silver;
            this.lblShohinCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShohinCdTo.Location = new System.Drawing.Point(389, 23);
            this.lblShohinCdTo.Name = "lblShohinCdTo";
            this.lblShohinCdTo.Size = new System.Drawing.Size(116, 20);
            this.lblShohinCdTo.TabIndex = 6;
            this.lblShohinCdTo.Text = "最  後";
            this.lblShohinCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShohinCdFr
            // 
            this.lblShohinCdFr.BackColor = System.Drawing.Color.Silver;
            this.lblShohinCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShohinCdFr.Location = new System.Drawing.Point(125, 23);
            this.lblShohinCdFr.Name = "lblShohinCdFr";
            this.lblShohinCdFr.Size = new System.Drawing.Size(116, 20);
            this.lblShohinCdFr.TabIndex = 3;
            this.lblShohinCdFr.Text = "先  頭";
            this.lblShohinCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShohinCdTo
            // 
            this.txtShohinCdTo.AutoSizeFromLength = false;
            this.txtShohinCdTo.DisplayLength = null;
            this.txtShohinCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShohinCdTo.Location = new System.Drawing.Point(275, 23);
            this.txtShohinCdTo.MaxLength = 13;
            this.txtShohinCdTo.Name = "txtShohinCdTo";
            this.txtShohinCdTo.Size = new System.Drawing.Size(108, 20);
            this.txtShohinCdTo.TabIndex = 5;
            this.txtShohinCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShohinCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohinCdTo_Validating);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(247, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(22, 20);
            this.label1.TabIndex = 4;
            this.label1.Text = "～";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtShohinCdFr
            // 
            this.txtShohinCdFr.AutoSizeFromLength = false;
            this.txtShohinCdFr.DisplayLength = null;
            this.txtShohinCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShohinCdFr.Location = new System.Drawing.Point(11, 23);
            this.txtShohinCdFr.MaxLength = 13;
            this.txtShohinCdFr.Name = "txtShohinCdFr";
            this.txtShohinCdFr.Size = new System.Drawing.Size(108, 20);
            this.txtShohinCdFr.TabIndex = 2;
            this.txtShohinCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShohinCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohinCdFr_Validating);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblShishoCdTo);
            this.groupBox1.Controls.Add(this.lblShishoCdFr);
            this.groupBox1.Controls.Add(this.txtShishoCdTo);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtShishoCdFr);
            this.groupBox1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBox1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox1.Location = new System.Drawing.Point(8, 10);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(520, 56);
            this.groupBox1.TabIndex = 902;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "支所コード範囲";
            // 
            // lblShishoCdTo
            // 
            this.lblShishoCdTo.BackColor = System.Drawing.Color.Silver;
            this.lblShishoCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShishoCdTo.Location = new System.Drawing.Point(389, 23);
            this.lblShishoCdTo.Name = "lblShishoCdTo";
            this.lblShishoCdTo.Size = new System.Drawing.Size(116, 20);
            this.lblShishoCdTo.TabIndex = 6;
            this.lblShishoCdTo.Text = "最  後";
            this.lblShishoCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShishoCdFr
            // 
            this.lblShishoCdFr.BackColor = System.Drawing.Color.Silver;
            this.lblShishoCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShishoCdFr.Location = new System.Drawing.Point(125, 23);
            this.lblShishoCdFr.Name = "lblShishoCdFr";
            this.lblShishoCdFr.Size = new System.Drawing.Size(116, 20);
            this.lblShishoCdFr.TabIndex = 3;
            this.lblShishoCdFr.Text = "先  頭";
            this.lblShishoCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShishoCdTo
            // 
            this.txtShishoCdTo.AutoSizeFromLength = false;
            this.txtShishoCdTo.DisplayLength = null;
            this.txtShishoCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShishoCdTo.Location = new System.Drawing.Point(275, 23);
            this.txtShishoCdTo.MaxLength = 4;
            this.txtShishoCdTo.Name = "txtShishoCdTo";
            this.txtShishoCdTo.Size = new System.Drawing.Size(108, 20);
            this.txtShishoCdTo.TabIndex = 5;
            this.txtShishoCdTo.TabStop = false;
            this.txtShishoCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(247, 23);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(22, 20);
            this.label4.TabIndex = 4;
            this.label4.Text = "～";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtShishoCdFr
            // 
            this.txtShishoCdFr.AutoSizeFromLength = false;
            this.txtShishoCdFr.DisplayLength = null;
            this.txtShishoCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShishoCdFr.Location = new System.Drawing.Point(11, 23);
            this.txtShishoCdFr.MaxLength = 4;
            this.txtShishoCdFr.Name = "txtShishoCdFr";
            this.txtShishoCdFr.Size = new System.Drawing.Size(108, 20);
            this.txtShishoCdFr.TabIndex = 2;
            this.txtShishoCdFr.TabStop = false;
            this.txtShishoCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShishoCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtShishoCdFr_Validating);
            // 
            // KBCM1023
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(536, 196);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.gbxShohinCd);
            this.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "KBCM1023";
            this.Text = "商品の登録";
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.gbxShohinCd, 0);
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.pnlDebug.ResumeLayout(false);
            this.gbxShohinCd.ResumeLayout(false);
            this.gbxShohinCd.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbxShohinCd;
        private jp.co.fsi.common.controls.FsiTextBox txtShohinCdFr;
        private jp.co.fsi.common.controls.FsiTextBox txtShohinCdTo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblShohinCdTo;
        private System.Windows.Forms.Label lblShohinCdFr;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblShishoCdTo;
        private System.Windows.Forms.Label lblShishoCdFr;
        private common.controls.FsiTextBox txtShishoCdTo;
        private System.Windows.Forms.Label label4;
        private common.controls.FsiTextBox txtShishoCdFr;
    }
}