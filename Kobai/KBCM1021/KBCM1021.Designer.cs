﻿namespace jp.co.fsi.kb.kbcm1021
{
    partial class KBCM1021
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblKanaNm = new System.Windows.Forms.Label();
            this.txtKanaNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.dgvList = new System.Windows.Forms.DataGridView();
            this.grpSearchCheck = new System.Windows.Forms.GroupBox();
            this.chkLikeSearch = new System.Windows.Forms.CheckBox();
            this.btnEnter = new System.Windows.Forms.Button();
            this.txtShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShishoNm = new System.Windows.Forms.Label();
            this.lblShisho = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvList)).BeginInit();
            this.grpSearchCheck.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Text = "";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Controls.Add(this.btnEnter);
            this.pnlDebug.Size = new System.Drawing.Size(847, 100);
            this.pnlDebug.Controls.SetChildIndex(this.btnF6, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF7, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF5, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF8, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF4, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF9, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF3, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF10, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF2, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF11, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF1, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF12, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnEsc, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnEnter, 0);
            // 
            // lblKanaNm
            // 
            this.lblKanaNm.BackColor = System.Drawing.Color.Silver;
            this.lblKanaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F);
            this.lblKanaNm.Location = new System.Drawing.Point(12, 85);
            this.lblKanaNm.Name = "lblKanaNm";
            this.lblKanaNm.Size = new System.Drawing.Size(295, 25);
            this.lblKanaNm.TabIndex = 1;
            this.lblKanaNm.Text = "カ　ナ　名";
            this.lblKanaNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKanaNm
            // 
            this.txtKanaNm.AllowDrop = true;
            this.txtKanaNm.AutoSizeFromLength = false;
            this.txtKanaNm.DisplayLength = null;
            this.txtKanaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKanaNm.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtKanaNm.Location = new System.Drawing.Point(89, 87);
            this.txtKanaNm.MaxLength = 15;
            this.txtKanaNm.Name = "txtKanaNm";
            this.txtKanaNm.Size = new System.Drawing.Size(215, 20);
            this.txtKanaNm.TabIndex = 2;
            this.txtKanaNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtKanaNm_Validating);
            // 
            // dgvList
            // 
            this.dgvList.AllowUserToAddRows = false;
            this.dgvList.AllowUserToDeleteRows = false;
            this.dgvList.AllowUserToResizeColumns = false;
            this.dgvList.AllowUserToResizeRows = false;
            this.dgvList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvList.Location = new System.Drawing.Point(12, 121);
            this.dgvList.MultiSelect = false;
            this.dgvList.Name = "dgvList";
            this.dgvList.ReadOnly = true;
            this.dgvList.RowHeadersVisible = false;
            this.dgvList.RowTemplate.Height = 21;
            this.dgvList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvList.Size = new System.Drawing.Size(815, 389);
            this.dgvList.TabIndex = 902;
            this.dgvList.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvList_CellDoubleClick);
            this.dgvList.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvList_KeyDown);
            // 
            // grpSearchCheck
            // 
            this.grpSearchCheck.Controls.Add(this.chkLikeSearch);
            this.grpSearchCheck.Location = new System.Drawing.Point(643, 76);
            this.grpSearchCheck.Name = "grpSearchCheck";
            this.grpSearchCheck.Size = new System.Drawing.Size(184, 34);
            this.grpSearchCheck.TabIndex = 903;
            this.grpSearchCheck.TabStop = false;
            this.grpSearchCheck.Text = "検索方法";
            // 
            // chkLikeSearch
            // 
            this.chkLikeSearch.AutoSize = true;
            this.chkLikeSearch.Checked = true;
            this.chkLikeSearch.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkLikeSearch.Location = new System.Drawing.Point(13, 14);
            this.chkLikeSearch.Name = "chkLikeSearch";
            this.chkLikeSearch.Size = new System.Drawing.Size(96, 16);
            this.chkLikeSearch.TabIndex = 0;
            this.chkLikeSearch.Text = "あいまい検索";
            this.chkLikeSearch.UseVisualStyleBackColor = true;
            // 
            // btnEnter
            // 
            this.btnEnter.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnEnter.Location = new System.Drawing.Point(67, 3);
            this.btnEnter.Name = "btnEnter";
            this.btnEnter.Size = new System.Drawing.Size(65, 45);
            this.btnEnter.TabIndex = 906;
            this.btnEnter.TabStop = false;
            this.btnEnter.Text = "Enter\r\n\r\n決定";
            this.btnEnter.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnEnter.UseVisualStyleBackColor = true;
            this.btnEnter.Visible = false;
            // 
            // txtShishoCd
            // 
            this.txtShishoCd.AutoSizeFromLength = true;
            this.txtShishoCd.DisplayLength = null;
            this.txtShishoCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtShishoCd.Location = new System.Drawing.Point(91, 50);
            this.txtShishoCd.MaxLength = 4;
            this.txtShishoCd.Name = "txtShishoCd";
            this.txtShishoCd.Size = new System.Drawing.Size(34, 20);
            this.txtShishoCd.TabIndex = 1;
            this.txtShishoCd.TabStop = false;
            this.txtShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShishoCd.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtShishoCd_KeyDown);
            this.txtShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtShishoCd_Validating);
            // 
            // lblShishoNm
            // 
            this.lblShishoNm.BackColor = System.Drawing.Color.Silver;
            this.lblShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShishoNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShishoNm.Location = new System.Drawing.Point(126, 50);
            this.lblShishoNm.Name = "lblShishoNm";
            this.lblShishoNm.Size = new System.Drawing.Size(212, 20);
            this.lblShishoNm.TabIndex = 2;
            this.lblShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShisho
            // 
            this.lblShisho.BackColor = System.Drawing.Color.Silver;
            this.lblShisho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShisho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblShisho.Location = new System.Drawing.Point(13, 48);
            this.lblShisho.Name = "lblShisho";
            this.lblShisho.Size = new System.Drawing.Size(329, 25);
            this.lblShisho.TabIndex = 0;
            this.lblShisho.Text = "支　　所";
            this.lblShisho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // KBCM1021
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 638);
            this.Controls.Add(this.txtShishoCd);
            this.Controls.Add(this.lblShishoNm);
            this.Controls.Add(this.lblShisho);
            this.Controls.Add(this.grpSearchCheck);
            this.Controls.Add(this.dgvList);
            this.Controls.Add(this.txtKanaNm);
            this.Controls.Add(this.lblKanaNm);
            this.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "KBCM1021";
            this.Par1 = "";
            this.Par2 = "";
            this.Text = "";
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.lblKanaNm, 0);
            this.Controls.SetChildIndex(this.txtKanaNm, 0);
            this.Controls.SetChildIndex(this.dgvList, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.grpSearchCheck, 0);
            this.Controls.SetChildIndex(this.lblShisho, 0);
            this.Controls.SetChildIndex(this.lblShishoNm, 0);
            this.Controls.SetChildIndex(this.txtShishoCd, 0);
            this.pnlDebug.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvList)).EndInit();
            this.grpSearchCheck.ResumeLayout(false);
            this.grpSearchCheck.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblKanaNm;
        private jp.co.fsi.common.controls.FsiTextBox txtKanaNm;
        private System.Windows.Forms.DataGridView dgvList;
        private System.Windows.Forms.GroupBox grpSearchCheck;
        private System.Windows.Forms.CheckBox chkLikeSearch;
        protected System.Windows.Forms.Button btnEnter;
        private common.controls.FsiTextBox txtShishoCd;
        private System.Windows.Forms.Label lblShishoNm;
        private System.Windows.Forms.Label lblShisho;
    }
}