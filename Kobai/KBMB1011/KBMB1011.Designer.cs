﻿namespace jp.co.fsi.kb.kbmb1011
{
    partial class KBMB1011
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gpbTnorsDt = new System.Windows.Forms.GroupBox();
            this.lblGengo = new System.Windows.Forms.Label();
            this.lblTnorsDtDay = new System.Windows.Forms.Label();
            this.txtDay = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblTnorsDtMonth = new System.Windows.Forms.Label();
            this.txtMonth = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblTnorsDtJpYear = new System.Windows.Forms.Label();
            this.txtYear = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblDate = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMizuageShishoNm = new System.Windows.Forms.Label();
            this.lblMizuageShisho = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.gpbTnorsDt.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Text = "棚卸更新";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(5, 531);
            this.pnlDebug.Size = new System.Drawing.Size(843, 100);
            // 
            // gpbTnorsDt
            // 
            this.gpbTnorsDt.Controls.Add(this.lblGengo);
            this.gpbTnorsDt.Controls.Add(this.lblTnorsDtDay);
            this.gpbTnorsDt.Controls.Add(this.txtDay);
            this.gpbTnorsDt.Controls.Add(this.lblTnorsDtMonth);
            this.gpbTnorsDt.Controls.Add(this.txtMonth);
            this.gpbTnorsDt.Controls.Add(this.lblTnorsDtJpYear);
            this.gpbTnorsDt.Controls.Add(this.txtYear);
            this.gpbTnorsDt.Controls.Add(this.lblDate);
            this.gpbTnorsDt.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gpbTnorsDt.Location = new System.Drawing.Point(12, 138);
            this.gpbTnorsDt.Name = "gpbTnorsDt";
            this.gpbTnorsDt.Size = new System.Drawing.Size(218, 62);
            this.gpbTnorsDt.TabIndex = 1;
            this.gpbTnorsDt.TabStop = false;
            this.gpbTnorsDt.Text = "棚卸日付";
            // 
            // lblGengo
            // 
            this.lblGengo.BackColor = System.Drawing.Color.Silver;
            this.lblGengo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGengo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGengo.Location = new System.Drawing.Point(25, 24);
            this.lblGengo.Name = "lblGengo";
            this.lblGengo.Size = new System.Drawing.Size(38, 20);
            this.lblGengo.TabIndex = 902;
            this.lblGengo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTnorsDtDay
            // 
            this.lblTnorsDtDay.BackColor = System.Drawing.Color.Silver;
            this.lblTnorsDtDay.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTnorsDtDay.Location = new System.Drawing.Point(170, 24);
            this.lblTnorsDtDay.Name = "lblTnorsDtDay";
            this.lblTnorsDtDay.Size = new System.Drawing.Size(18, 20);
            this.lblTnorsDtDay.TabIndex = 6;
            this.lblTnorsDtDay.Text = "日";
            this.lblTnorsDtDay.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDay
            // 
            this.txtDay.AutoSizeFromLength = true;
            this.txtDay.DisplayLength = null;
            this.txtDay.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDay.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtDay.Location = new System.Drawing.Point(147, 24);
            this.txtDay.MaxLength = 2;
            this.txtDay.Name = "txtDay";
            this.txtDay.Size = new System.Drawing.Size(20, 20);
            this.txtDay.TabIndex = 5;
            this.txtDay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDay.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtTnorsDtDay_KeyDown);
            this.txtDay.Validating += new System.ComponentModel.CancelEventHandler(this.txtDay_Validating);
            // 
            // lblTnorsDtMonth
            // 
            this.lblTnorsDtMonth.BackColor = System.Drawing.Color.Silver;
            this.lblTnorsDtMonth.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTnorsDtMonth.Location = new System.Drawing.Point(128, 24);
            this.lblTnorsDtMonth.Name = "lblTnorsDtMonth";
            this.lblTnorsDtMonth.Size = new System.Drawing.Size(18, 20);
            this.lblTnorsDtMonth.TabIndex = 4;
            this.lblTnorsDtMonth.Text = "月";
            this.lblTnorsDtMonth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtMonth
            // 
            this.txtMonth.AutoSizeFromLength = true;
            this.txtMonth.DisplayLength = null;
            this.txtMonth.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMonth.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtMonth.Location = new System.Drawing.Point(106, 24);
            this.txtMonth.MaxLength = 2;
            this.txtMonth.Name = "txtMonth";
            this.txtMonth.Size = new System.Drawing.Size(20, 20);
            this.txtMonth.TabIndex = 3;
            this.txtMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonth.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonth_Validating);
            // 
            // lblTnorsDtJpYear
            // 
            this.lblTnorsDtJpYear.BackColor = System.Drawing.Color.Silver;
            this.lblTnorsDtJpYear.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTnorsDtJpYear.Location = new System.Drawing.Point(87, 24);
            this.lblTnorsDtJpYear.Name = "lblTnorsDtJpYear";
            this.lblTnorsDtJpYear.Size = new System.Drawing.Size(18, 20);
            this.lblTnorsDtJpYear.TabIndex = 2;
            this.lblTnorsDtJpYear.Text = "年";
            this.lblTnorsDtJpYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtYear
            // 
            this.txtYear.AutoSizeFromLength = true;
            this.txtYear.DisplayLength = null;
            this.txtYear.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtYear.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtYear.Location = new System.Drawing.Point(65, 24);
            this.txtYear.MaxLength = 2;
            this.txtYear.Name = "txtYear";
            this.txtYear.Size = new System.Drawing.Size(21, 20);
            this.txtYear.TabIndex = 1;
            this.txtYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtYear.Validating += new System.ComponentModel.CancelEventHandler(this.txtYear_Validating);
            // 
            // lblDate
            // 
            this.lblDate.BackColor = System.Drawing.Color.Silver;
            this.lblDate.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDate.Location = new System.Drawing.Point(22, 21);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(168, 27);
            this.lblDate.TabIndex = 8;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtMizuageShishoCd);
            this.groupBox1.Controls.Add(this.lblMizuageShishoNm);
            this.groupBox1.Controls.Add(this.lblMizuageShisho);
            this.groupBox1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBox1.ForeColor = System.Drawing.Color.Black;
            this.groupBox1.Location = new System.Drawing.Point(12, 52);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(341, 68);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "支所";
            // 
            // txtMizuageShishoCd
            // 
            this.txtMizuageShishoCd.AutoSizeFromLength = true;
            this.txtMizuageShishoCd.DisplayLength = null;
            this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtMizuageShishoCd.Location = new System.Drawing.Point(68, 26);
            this.txtMizuageShishoCd.MaxLength = 4;
            this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
            this.txtMizuageShishoCd.Size = new System.Drawing.Size(34, 20);
            this.txtMizuageShishoCd.TabIndex = 1;
            this.txtMizuageShishoCd.TabStop = false;
            this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
            // 
            // lblMizuageShishoNm
            // 
            this.lblMizuageShishoNm.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMizuageShishoNm.Location = new System.Drawing.Point(104, 26);
            this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
            this.lblMizuageShishoNm.Size = new System.Drawing.Size(212, 20);
            this.lblMizuageShishoNm.TabIndex = 2;
            this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMizuageShisho
            // 
            this.lblMizuageShisho.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShisho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShisho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblMizuageShisho.Location = new System.Drawing.Point(26, 24);
            this.lblMizuageShisho.Name = "lblMizuageShisho";
            this.lblMizuageShisho.Size = new System.Drawing.Size(294, 25);
            this.lblMizuageShisho.TabIndex = 3;
            this.lblMizuageShisho.Text = "支所";
            this.lblMizuageShisho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // KBMB1011
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(835, 634);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.gpbTnorsDt);
            this.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.Name = "KBMB1011";
            this.Text = "棚卸更新";
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.gpbTnorsDt, 0);
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.pnlDebug.ResumeLayout(false);
            this.gpbTnorsDt.ResumeLayout(false);
            this.gpbTnorsDt.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gpbTnorsDt;
        private System.Windows.Forms.Label lblTnorsDtDay;
        private common.controls.FsiTextBox txtDay;
        private System.Windows.Forms.Label lblTnorsDtMonth;
        private common.controls.FsiTextBox txtMonth;
        private System.Windows.Forms.Label lblTnorsDtJpYear;
        private common.controls.FsiTextBox txtYear;
        private System.Windows.Forms.Label lblGengo;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.GroupBox groupBox1;
        private common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private System.Windows.Forms.Label lblMizuageShisho;
    }
}