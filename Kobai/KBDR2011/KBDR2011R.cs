﻿using System.Data;

using jp.co.fsi.common.report;

namespace jp.co.fsi.kb.kbdr2011
{
    /// <summary>
    /// KBDR2011R の概要の説明です。
    /// </summary>
    public partial class KBDR2011R : BaseReport
    {

        public KBDR2011R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }
    }
}
