﻿namespace jp.co.fsi.kb.kbdr2011
{
    partial class KBDR2011
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbxShiireCd = new System.Windows.Forms.GroupBox();
            this.lblShiireCdBet = new System.Windows.Forms.Label();
            this.lblShiireCdFr = new System.Windows.Forms.Label();
            this.lblShiireCdTo = new System.Windows.Forms.Label();
            this.txtShiireCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtShiireCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblDate = new System.Windows.Forms.Label();
            this.lblDateGengo = new System.Windows.Forms.Label();
            this.txtDateMonth = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtDateYear = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtDateDay = new jp.co.fsi.common.controls.FsiTextBox();
            this.labelDateYear = new System.Windows.Forms.Label();
            this.lblDateMonth = new System.Windows.Forms.Label();
            this.lblDateDay = new System.Windows.Forms.Label();
            this.gbxDate = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMizuageShishoNm = new System.Windows.Forms.Label();
            this.lblMizuageShisho = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.gbxShiireCd.SuspendLayout();
            this.gbxDate.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Text = "";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Size = new System.Drawing.Size(847, 100);
            // 
            // gbxShiireCd
            // 
            this.gbxShiireCd.Controls.Add(this.lblShiireCdBet);
            this.gbxShiireCd.Controls.Add(this.lblShiireCdFr);
            this.gbxShiireCd.Controls.Add(this.lblShiireCdTo);
            this.gbxShiireCd.Controls.Add(this.txtShiireCdTo);
            this.gbxShiireCd.Controls.Add(this.txtShiireCdFr);
            this.gbxShiireCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxShiireCd.ForeColor = System.Drawing.SystemColors.ControlText;
            this.gbxShiireCd.Location = new System.Drawing.Point(18, 127);
            this.gbxShiireCd.Name = "gbxShiireCd";
            this.gbxShiireCd.Size = new System.Drawing.Size(604, 73);
            this.gbxShiireCd.TabIndex = 2;
            this.gbxShiireCd.TabStop = false;
            this.gbxShiireCd.Text = "仕入先コード範囲";
            // 
            // lblShiireCdBet
            // 
            this.lblShiireCdBet.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShiireCdBet.Location = new System.Drawing.Point(291, 32);
            this.lblShiireCdBet.Name = "lblShiireCdBet";
            this.lblShiireCdBet.Size = new System.Drawing.Size(17, 18);
            this.lblShiireCdBet.TabIndex = 2;
            this.lblShiireCdBet.Text = "～";
            this.lblShiireCdBet.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblShiireCdFr
            // 
            this.lblShiireCdFr.BackColor = System.Drawing.Color.Silver;
            this.lblShiireCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShiireCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShiireCdFr.Location = new System.Drawing.Point(81, 30);
            this.lblShiireCdFr.Name = "lblShiireCdFr";
            this.lblShiireCdFr.Size = new System.Drawing.Size(204, 20);
            this.lblShiireCdFr.TabIndex = 1;
            this.lblShiireCdFr.Text = "先　頭";
            this.lblShiireCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShiireCdTo
            // 
            this.lblShiireCdTo.BackColor = System.Drawing.Color.Silver;
            this.lblShiireCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShiireCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShiireCdTo.Location = new System.Drawing.Point(370, 30);
            this.lblShiireCdTo.Name = "lblShiireCdTo";
            this.lblShiireCdTo.Size = new System.Drawing.Size(204, 20);
            this.lblShiireCdTo.TabIndex = 4;
            this.lblShiireCdTo.Text = "最　後";
            this.lblShiireCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShiireCdTo
            // 
            this.txtShiireCdTo.AutoSizeFromLength = false;
            this.txtShiireCdTo.DisplayLength = null;
            this.txtShiireCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShiireCdTo.Location = new System.Drawing.Point(314, 30);
            this.txtShiireCdTo.MaxLength = 4;
            this.txtShiireCdTo.Name = "txtShiireCdTo";
            this.txtShiireCdTo.Size = new System.Drawing.Size(50, 20);
            this.txtShiireCdTo.TabIndex = 3;
            this.txtShiireCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShiireCdTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtShiireCdTo_KeyDown);
            this.txtShiireCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtShiireCdTo_Validating);
            // 
            // txtShiireCdFr
            // 
            this.txtShiireCdFr.AutoSizeFromLength = false;
            this.txtShiireCdFr.DisplayLength = null;
            this.txtShiireCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShiireCdFr.Location = new System.Drawing.Point(25, 30);
            this.txtShiireCdFr.MaxLength = 4;
            this.txtShiireCdFr.Name = "txtShiireCdFr";
            this.txtShiireCdFr.Size = new System.Drawing.Size(50, 20);
            this.txtShiireCdFr.TabIndex = 0;
            this.txtShiireCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShiireCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtShiireCdFr_Validating);
            // 
            // lblDate
            // 
            this.lblDate.BackColor = System.Drawing.Color.Silver;
            this.lblDate.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDate.Location = new System.Drawing.Point(24, 24);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(199, 28);
            this.lblDate.TabIndex = 1;
            // 
            // lblDateGengo
            // 
            this.lblDateGengo.BackColor = System.Drawing.Color.Silver;
            this.lblDateGengo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDateGengo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateGengo.Location = new System.Drawing.Point(27, 28);
            this.lblDateGengo.Name = "lblDateGengo";
            this.lblDateGengo.Size = new System.Drawing.Size(41, 20);
            this.lblDateGengo.TabIndex = 1;
            this.lblDateGengo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDateMonth
            // 
            this.txtDateMonth.AutoSizeFromLength = false;
            this.txtDateMonth.DisplayLength = null;
            this.txtDateMonth.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateMonth.Location = new System.Drawing.Point(120, 28);
            this.txtDateMonth.MaxLength = 2;
            this.txtDateMonth.Name = "txtDateMonth";
            this.txtDateMonth.Size = new System.Drawing.Size(30, 20);
            this.txtDateMonth.TabIndex = 4;
            this.txtDateMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateMonth.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateMonth_Validating);
            // 
            // txtDateYear
            // 
            this.txtDateYear.AutoSizeFromLength = false;
            this.txtDateYear.DisplayLength = null;
            this.txtDateYear.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateYear.Location = new System.Drawing.Point(70, 28);
            this.txtDateYear.MaxLength = 2;
            this.txtDateYear.Name = "txtDateYear";
            this.txtDateYear.Size = new System.Drawing.Size(30, 20);
            this.txtDateYear.TabIndex = 2;
            this.txtDateYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateYear.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateYear_Validating);
            // 
            // txtDateDay
            // 
            this.txtDateDay.AutoSizeFromLength = false;
            this.txtDateDay.DisplayLength = null;
            this.txtDateDay.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateDay.Location = new System.Drawing.Point(169, 28);
            this.txtDateDay.MaxLength = 2;
            this.txtDateDay.Name = "txtDateDay";
            this.txtDateDay.Size = new System.Drawing.Size(30, 20);
            this.txtDateDay.TabIndex = 6;
            this.txtDateDay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateDay.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateDay_Validating);
            // 
            // labelDateYear
            // 
            this.labelDateYear.BackColor = System.Drawing.Color.Silver;
            this.labelDateYear.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.labelDateYear.Location = new System.Drawing.Point(101, 28);
            this.labelDateYear.Name = "labelDateYear";
            this.labelDateYear.Size = new System.Drawing.Size(17, 21);
            this.labelDateYear.TabIndex = 3;
            this.labelDateYear.Text = "年";
            this.labelDateYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateMonth
            // 
            this.lblDateMonth.BackColor = System.Drawing.Color.Silver;
            this.lblDateMonth.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateMonth.Location = new System.Drawing.Point(151, 28);
            this.lblDateMonth.Name = "lblDateMonth";
            this.lblDateMonth.Size = new System.Drawing.Size(15, 19);
            this.lblDateMonth.TabIndex = 5;
            this.lblDateMonth.Text = "月";
            this.lblDateMonth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateDay
            // 
            this.lblDateDay.BackColor = System.Drawing.Color.Silver;
            this.lblDateDay.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateDay.Location = new System.Drawing.Point(201, 28);
            this.lblDateDay.Name = "lblDateDay";
            this.lblDateDay.Size = new System.Drawing.Size(20, 18);
            this.lblDateDay.TabIndex = 7;
            this.lblDateDay.Text = "日";
            this.lblDateDay.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // gbxDate
            // 
            this.gbxDate.Controls.Add(this.lblDateDay);
            this.gbxDate.Controls.Add(this.lblDateMonth);
            this.gbxDate.Controls.Add(this.labelDateYear);
            this.gbxDate.Controls.Add(this.txtDateDay);
            this.gbxDate.Controls.Add(this.txtDateYear);
            this.gbxDate.Controls.Add(this.txtDateMonth);
            this.gbxDate.Controls.Add(this.lblDateGengo);
            this.gbxDate.Controls.Add(this.lblDate);
            this.gbxDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxDate.ForeColor = System.Drawing.SystemColors.ControlText;
            this.gbxDate.Location = new System.Drawing.Point(18, 52);
            this.gbxDate.Name = "gbxDate";
            this.gbxDate.Size = new System.Drawing.Size(247, 69);
            this.gbxDate.TabIndex = 1;
            this.gbxDate.TabStop = false;
            this.gbxDate.Text = "伝票日付";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtMizuageShishoCd);
            this.groupBox1.Controls.Add(this.lblMizuageShishoNm);
            this.groupBox1.Controls.Add(this.lblMizuageShisho);
            this.groupBox1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBox1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox1.Location = new System.Drawing.Point(282, 52);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(342, 71);
            this.groupBox1.TabIndex = 906;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "支所";
            // 
            // txtMizuageShishoCd
            // 
            this.txtMizuageShishoCd.AutoSizeFromLength = true;
            this.txtMizuageShishoCd.DisplayLength = null;
            this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtMizuageShishoCd.Location = new System.Drawing.Point(68, 28);
            this.txtMizuageShishoCd.MaxLength = 4;
            this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
            this.txtMizuageShishoCd.Size = new System.Drawing.Size(34, 20);
            this.txtMizuageShishoCd.TabIndex = 1;
            this.txtMizuageShishoCd.TabStop = false;
            this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
            // 
            // lblMizuageShishoNm
            // 
            this.lblMizuageShishoNm.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMizuageShishoNm.Location = new System.Drawing.Point(103, 29);
            this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
            this.lblMizuageShishoNm.Size = new System.Drawing.Size(212, 20);
            this.lblMizuageShishoNm.TabIndex = 907;
            this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMizuageShisho
            // 
            this.lblMizuageShisho.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShisho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShisho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblMizuageShisho.Location = new System.Drawing.Point(26, 26);
            this.lblMizuageShisho.Name = "lblMizuageShisho";
            this.lblMizuageShisho.Size = new System.Drawing.Size(293, 25);
            this.lblMizuageShisho.TabIndex = 906;
            this.lblMizuageShisho.Text = "支所";
            this.lblMizuageShisho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // KBDR2011
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 638);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.gbxDate);
            this.Controls.Add(this.gbxShiireCd);
            this.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "KBDR2011";
            this.Text = "";
            this.Controls.SetChildIndex(this.gbxShiireCd, 0);
            this.Controls.SetChildIndex(this.gbxDate, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.pnlDebug.ResumeLayout(false);
            this.gbxShiireCd.ResumeLayout(false);
            this.gbxShiireCd.PerformLayout();
            this.gbxDate.ResumeLayout(false);
            this.gbxDate.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbxShiireCd;
        private System.Windows.Forms.Label lblShiireCdFr;
        private jp.co.fsi.common.controls.FsiTextBox txtShiireCdFr;
        private System.Windows.Forms.Label lblShiireCdBet;
        private System.Windows.Forms.Label lblShiireCdTo;
        private jp.co.fsi.common.controls.FsiTextBox txtShiireCdTo;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.Label lblDateGengo;
        private jp.co.fsi.common.controls.FsiTextBox txtDateMonth;
        private jp.co.fsi.common.controls.FsiTextBox txtDateYear;
        private jp.co.fsi.common.controls.FsiTextBox txtDateDay;
        private System.Windows.Forms.Label labelDateYear;
        private System.Windows.Forms.Label lblDateMonth;
        private System.Windows.Forms.Label lblDateDay;
        private System.Windows.Forms.GroupBox gbxDate;
        private System.Windows.Forms.GroupBox groupBox1;
        private common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private System.Windows.Forms.Label lblMizuageShisho;
    }
}