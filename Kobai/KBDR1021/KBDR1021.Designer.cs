﻿namespace jp.co.fsi.kb.kbdr1021
{
    partial class KBDR1021
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbxTantoCd = new System.Windows.Forms.GroupBox();
            this.lblTantoCdBet = new System.Windows.Forms.Label();
            this.lblTantoCdFr = new System.Windows.Forms.Label();
            this.lblTantoCdTo = new System.Windows.Forms.Label();
            this.txtTantoCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtTantoCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.gbxFunanushiCd = new System.Windows.Forms.GroupBox();
            this.txtFunanushiCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblFunanushiCdFr = new System.Windows.Forms.Label();
            this.lblSenshuCdBet = new System.Windows.Forms.Label();
            this.txtFunanushiCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblFunanushiCdTo = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMizuageShishoNm = new System.Windows.Forms.Label();
            this.lblMizuageShisho = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cmbToriKbn = new System.Windows.Forms.ComboBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.rdoMeisai = new System.Windows.Forms.RadioButton();
            this.rdoGokei = new System.Windows.Forms.RadioButton();
            this.gbxSearchRangeDate = new System.Windows.Forms.GroupBox();
            this.lblJpDayFrTo = new System.Windows.Forms.Label();
            this.lblCodeBetDate = new System.Windows.Forms.Label();
            this.lblJpDayFrFr = new System.Windows.Forms.Label();
            this.lblJpMonthTo = new System.Windows.Forms.Label();
            this.lblJpMonthFr = new System.Windows.Forms.Label();
            this.lblJpearTo = new System.Windows.Forms.Label();
            this.txtJpDayTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtJpDayFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblJpearFr = new System.Windows.Forms.Label();
            this.txtJpMonthTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtJpMonthFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtJpYearTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtJpYearFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblJpTo = new System.Windows.Forms.Label();
            this.lblJpFr = new System.Windows.Forms.Label();
            this.lblEraBackTo = new System.Windows.Forms.Label();
            this.lblEraBackFr = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblDenpyoNoFr = new System.Windows.Forms.Label();
            this.lblDenpyoNoTo = new System.Windows.Forms.Label();
            this.sjTxtDenpyoNoTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.sjTxtDenpyoNoFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.pnlDebug.SuspendLayout();
            this.gbxTantoCd.SuspendLayout();
            this.gbxFunanushiCd.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.gbxSearchRangeDate.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Text = "";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Size = new System.Drawing.Size(847, 100);
            // 
            // gbxTantoCd
            // 
            this.gbxTantoCd.Controls.Add(this.lblTantoCdBet);
            this.gbxTantoCd.Controls.Add(this.lblTantoCdFr);
            this.gbxTantoCd.Controls.Add(this.lblTantoCdTo);
            this.gbxTantoCd.Controls.Add(this.txtTantoCdTo);
            this.gbxTantoCd.Controls.Add(this.txtTantoCdFr);
            this.gbxTantoCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxTantoCd.ForeColor = System.Drawing.SystemColors.ControlText;
            this.gbxTantoCd.Location = new System.Drawing.Point(18, 365);
            this.gbxTantoCd.Name = "gbxTantoCd";
            this.gbxTantoCd.Size = new System.Drawing.Size(606, 71);
            this.gbxTantoCd.TabIndex = 5;
            this.gbxTantoCd.TabStop = false;
            this.gbxTantoCd.Text = "担当者CD範囲";
            // 
            // lblTantoCdBet
            // 
            this.lblTantoCdBet.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTantoCdBet.Location = new System.Drawing.Point(293, 28);
            this.lblTantoCdBet.Name = "lblTantoCdBet";
            this.lblTantoCdBet.Size = new System.Drawing.Size(17, 20);
            this.lblTantoCdBet.TabIndex = 2;
            this.lblTantoCdBet.Text = "～";
            this.lblTantoCdBet.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblTantoCdFr
            // 
            this.lblTantoCdFr.BackColor = System.Drawing.Color.Silver;
            this.lblTantoCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTantoCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTantoCdFr.Location = new System.Drawing.Point(83, 28);
            this.lblTantoCdFr.Name = "lblTantoCdFr";
            this.lblTantoCdFr.Size = new System.Drawing.Size(204, 20);
            this.lblTantoCdFr.TabIndex = 1;
            this.lblTantoCdFr.Text = "先　頭";
            this.lblTantoCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTantoCdTo
            // 
            this.lblTantoCdTo.BackColor = System.Drawing.Color.Silver;
            this.lblTantoCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTantoCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTantoCdTo.Location = new System.Drawing.Point(372, 28);
            this.lblTantoCdTo.Name = "lblTantoCdTo";
            this.lblTantoCdTo.Size = new System.Drawing.Size(204, 20);
            this.lblTantoCdTo.TabIndex = 4;
            this.lblTantoCdTo.Text = "最　後";
            this.lblTantoCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTantoCdTo
            // 
            this.txtTantoCdTo.AutoSizeFromLength = false;
            this.txtTantoCdTo.DisplayLength = null;
            this.txtTantoCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTantoCdTo.Location = new System.Drawing.Point(316, 28);
            this.txtTantoCdTo.MaxLength = 4;
            this.txtTantoCdTo.Name = "txtTantoCdTo";
            this.txtTantoCdTo.Size = new System.Drawing.Size(50, 20);
            this.txtTantoCdTo.TabIndex = 3;
            this.txtTantoCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTantoCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtTantoCdTo_Validating);
            // 
            // txtTantoCdFr
            // 
            this.txtTantoCdFr.AutoSizeFromLength = false;
            this.txtTantoCdFr.DisplayLength = null;
            this.txtTantoCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTantoCdFr.Location = new System.Drawing.Point(27, 28);
            this.txtTantoCdFr.MaxLength = 4;
            this.txtTantoCdFr.Name = "txtTantoCdFr";
            this.txtTantoCdFr.Size = new System.Drawing.Size(50, 20);
            this.txtTantoCdFr.TabIndex = 0;
            this.txtTantoCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTantoCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtTantoCdFr_Validating);
            // 
            // gbxFunanushiCd
            // 
            this.gbxFunanushiCd.Controls.Add(this.txtFunanushiCdFr);
            this.gbxFunanushiCd.Controls.Add(this.lblFunanushiCdFr);
            this.gbxFunanushiCd.Controls.Add(this.lblSenshuCdBet);
            this.gbxFunanushiCd.Controls.Add(this.txtFunanushiCdTo);
            this.gbxFunanushiCd.Controls.Add(this.lblFunanushiCdTo);
            this.gbxFunanushiCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxFunanushiCd.ForeColor = System.Drawing.SystemColors.ControlText;
            this.gbxFunanushiCd.Location = new System.Drawing.Point(18, 442);
            this.gbxFunanushiCd.Name = "gbxFunanushiCd";
            this.gbxFunanushiCd.Size = new System.Drawing.Size(606, 73);
            this.gbxFunanushiCd.TabIndex = 6;
            this.gbxFunanushiCd.TabStop = false;
            this.gbxFunanushiCd.Text = "船主CD範囲";
            // 
            // txtFunanushiCdFr
            // 
            this.txtFunanushiCdFr.AutoSizeFromLength = false;
            this.txtFunanushiCdFr.DisplayLength = null;
            this.txtFunanushiCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFunanushiCdFr.Location = new System.Drawing.Point(27, 29);
            this.txtFunanushiCdFr.MaxLength = 4;
            this.txtFunanushiCdFr.Name = "txtFunanushiCdFr";
            this.txtFunanushiCdFr.Size = new System.Drawing.Size(50, 20);
            this.txtFunanushiCdFr.TabIndex = 0;
            this.txtFunanushiCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFunanushiCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtFunanushiCdFr_Validating);
            // 
            // lblFunanushiCdFr
            // 
            this.lblFunanushiCdFr.BackColor = System.Drawing.Color.Silver;
            this.lblFunanushiCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFunanushiCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFunanushiCdFr.Location = new System.Drawing.Point(83, 29);
            this.lblFunanushiCdFr.Name = "lblFunanushiCdFr";
            this.lblFunanushiCdFr.Size = new System.Drawing.Size(204, 20);
            this.lblFunanushiCdFr.TabIndex = 1;
            this.lblFunanushiCdFr.Text = "先　頭";
            this.lblFunanushiCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSenshuCdBet
            // 
            this.lblSenshuCdBet.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblSenshuCdBet.Location = new System.Drawing.Point(294, 29);
            this.lblSenshuCdBet.Name = "lblSenshuCdBet";
            this.lblSenshuCdBet.Size = new System.Drawing.Size(17, 20);
            this.lblSenshuCdBet.TabIndex = 2;
            this.lblSenshuCdBet.Text = "～";
            this.lblSenshuCdBet.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtFunanushiCdTo
            // 
            this.txtFunanushiCdTo.AutoSizeFromLength = false;
            this.txtFunanushiCdTo.DisplayLength = null;
            this.txtFunanushiCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFunanushiCdTo.Location = new System.Drawing.Point(317, 29);
            this.txtFunanushiCdTo.MaxLength = 4;
            this.txtFunanushiCdTo.Name = "txtFunanushiCdTo";
            this.txtFunanushiCdTo.Size = new System.Drawing.Size(50, 20);
            this.txtFunanushiCdTo.TabIndex = 3;
            this.txtFunanushiCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFunanushiCdTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtFunanushiCdTo_KeyDown);
            this.txtFunanushiCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtFunanushiCdTo_Validating);
            // 
            // lblFunanushiCdTo
            // 
            this.lblFunanushiCdTo.BackColor = System.Drawing.Color.Silver;
            this.lblFunanushiCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFunanushiCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFunanushiCdTo.Location = new System.Drawing.Point(373, 29);
            this.lblFunanushiCdTo.Name = "lblFunanushiCdTo";
            this.lblFunanushiCdTo.Size = new System.Drawing.Size(204, 20);
            this.lblFunanushiCdTo.TabIndex = 4;
            this.lblFunanushiCdTo.Text = "最　後";
            this.lblFunanushiCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtMizuageShishoCd);
            this.groupBox1.Controls.Add(this.lblMizuageShishoNm);
            this.groupBox1.Controls.Add(this.lblMizuageShisho);
            this.groupBox1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBox1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox1.Location = new System.Drawing.Point(18, 52);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(366, 71);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "支所";
            // 
            // txtMizuageShishoCd
            // 
            this.txtMizuageShishoCd.AutoSizeFromLength = true;
            this.txtMizuageShishoCd.DisplayLength = null;
            this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtMizuageShishoCd.Location = new System.Drawing.Point(68, 28);
            this.txtMizuageShishoCd.MaxLength = 4;
            this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
            this.txtMizuageShishoCd.Size = new System.Drawing.Size(34, 20);
            this.txtMizuageShishoCd.TabIndex = 1;
            this.txtMizuageShishoCd.TabStop = false;
            this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
            // 
            // lblMizuageShishoNm
            // 
            this.lblMizuageShishoNm.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMizuageShishoNm.Location = new System.Drawing.Point(103, 29);
            this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
            this.lblMizuageShishoNm.Size = new System.Drawing.Size(212, 20);
            this.lblMizuageShishoNm.TabIndex = 907;
            this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMizuageShisho
            // 
            this.lblMizuageShisho.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShisho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShisho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblMizuageShisho.Location = new System.Drawing.Point(26, 26);
            this.lblMizuageShisho.Name = "lblMizuageShisho";
            this.lblMizuageShisho.Size = new System.Drawing.Size(293, 25);
            this.lblMizuageShisho.TabIndex = 906;
            this.lblMizuageShisho.Text = "支所";
            this.lblMizuageShisho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cmbToriKbn);
            this.groupBox2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBox2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox2.Location = new System.Drawing.Point(18, 211);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(172, 70);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "取引区分";
            // 
            // cmbToriKbn
            // 
            this.cmbToriKbn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbToriKbn.FormattingEnabled = true;
            this.cmbToriKbn.Location = new System.Drawing.Point(27, 27);
            this.cmbToriKbn.Name = "cmbToriKbn";
            this.cmbToriKbn.Size = new System.Drawing.Size(122, 21);
            this.cmbToriKbn.TabIndex = 1;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.rdoMeisai);
            this.groupBox3.Controls.Add(this.rdoGokei);
            this.groupBox3.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBox3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox3.Location = new System.Drawing.Point(200, 211);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(184, 70);
            this.groupBox3.TabIndex = 7;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "表示方法";
            // 
            // rdoMeisai
            // 
            this.rdoMeisai.AutoSize = true;
            this.rdoMeisai.Location = new System.Drawing.Point(100, 31);
            this.rdoMeisai.Name = "rdoMeisai";
            this.rdoMeisai.Size = new System.Drawing.Size(53, 17);
            this.rdoMeisai.TabIndex = 2;
            this.rdoMeisai.Text = "明細";
            this.rdoMeisai.UseVisualStyleBackColor = true;
            // 
            // rdoGokei
            // 
            this.rdoGokei.AutoSize = true;
            this.rdoGokei.Location = new System.Drawing.Point(22, 31);
            this.rdoGokei.Name = "rdoGokei";
            this.rdoGokei.Size = new System.Drawing.Size(53, 17);
            this.rdoGokei.TabIndex = 1;
            this.rdoGokei.Text = "合計";
            this.rdoGokei.UseVisualStyleBackColor = true;
            // 
            // gbxSearchRangeDate
            // 
            this.gbxSearchRangeDate.Controls.Add(this.lblJpDayFrTo);
            this.gbxSearchRangeDate.Controls.Add(this.lblCodeBetDate);
            this.gbxSearchRangeDate.Controls.Add(this.lblJpDayFrFr);
            this.gbxSearchRangeDate.Controls.Add(this.lblJpMonthTo);
            this.gbxSearchRangeDate.Controls.Add(this.lblJpMonthFr);
            this.gbxSearchRangeDate.Controls.Add(this.lblJpearTo);
            this.gbxSearchRangeDate.Controls.Add(this.txtJpDayTo);
            this.gbxSearchRangeDate.Controls.Add(this.txtJpDayFr);
            this.gbxSearchRangeDate.Controls.Add(this.lblJpearFr);
            this.gbxSearchRangeDate.Controls.Add(this.txtJpMonthTo);
            this.gbxSearchRangeDate.Controls.Add(this.txtJpMonthFr);
            this.gbxSearchRangeDate.Controls.Add(this.txtJpYearTo);
            this.gbxSearchRangeDate.Controls.Add(this.txtJpYearFr);
            this.gbxSearchRangeDate.Controls.Add(this.lblJpTo);
            this.gbxSearchRangeDate.Controls.Add(this.lblJpFr);
            this.gbxSearchRangeDate.Controls.Add(this.lblEraBackTo);
            this.gbxSearchRangeDate.Controls.Add(this.lblEraBackFr);
            this.gbxSearchRangeDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxSearchRangeDate.ForeColor = System.Drawing.Color.Black;
            this.gbxSearchRangeDate.Location = new System.Drawing.Point(18, 129);
            this.gbxSearchRangeDate.Name = "gbxSearchRangeDate";
            this.gbxSearchRangeDate.Size = new System.Drawing.Size(596, 75);
            this.gbxSearchRangeDate.TabIndex = 2;
            this.gbxSearchRangeDate.TabStop = false;
            this.gbxSearchRangeDate.Text = "日付範囲";
            // 
            // lblJpDayFrTo
            // 
            this.lblJpDayFrTo.AutoSize = true;
            this.lblJpDayFrTo.BackColor = System.Drawing.Color.Silver;
            this.lblJpDayFrTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblJpDayFrTo.Location = new System.Drawing.Point(543, 34);
            this.lblJpDayFrTo.Name = "lblJpDayFrTo";
            this.lblJpDayFrTo.Size = new System.Drawing.Size(21, 13);
            this.lblJpDayFrTo.TabIndex = 14;
            this.lblJpDayFrTo.Text = "日";
            // 
            // lblCodeBetDate
            // 
            this.lblCodeBetDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblCodeBetDate.Location = new System.Drawing.Point(288, 29);
            this.lblCodeBetDate.Name = "lblCodeBetDate";
            this.lblCodeBetDate.Size = new System.Drawing.Size(18, 20);
            this.lblCodeBetDate.TabIndex = 7;
            this.lblCodeBetDate.Text = "～";
            this.lblCodeBetDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblJpDayFrFr
            // 
            this.lblJpDayFrFr.AutoSize = true;
            this.lblJpDayFrFr.BackColor = System.Drawing.Color.Silver;
            this.lblJpDayFrFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblJpDayFrFr.Location = new System.Drawing.Point(252, 34);
            this.lblJpDayFrFr.Name = "lblJpDayFrFr";
            this.lblJpDayFrFr.Size = new System.Drawing.Size(21, 13);
            this.lblJpDayFrFr.TabIndex = 6;
            this.lblJpDayFrFr.Text = "日";
            // 
            // lblJpMonthTo
            // 
            this.lblJpMonthTo.AutoSize = true;
            this.lblJpMonthTo.BackColor = System.Drawing.Color.Silver;
            this.lblJpMonthTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblJpMonthTo.Location = new System.Drawing.Point(475, 34);
            this.lblJpMonthTo.Name = "lblJpMonthTo";
            this.lblJpMonthTo.Size = new System.Drawing.Size(21, 13);
            this.lblJpMonthTo.TabIndex = 12;
            this.lblJpMonthTo.Text = "月";
            // 
            // lblJpMonthFr
            // 
            this.lblJpMonthFr.AutoSize = true;
            this.lblJpMonthFr.BackColor = System.Drawing.Color.Silver;
            this.lblJpMonthFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblJpMonthFr.Location = new System.Drawing.Point(184, 34);
            this.lblJpMonthFr.Name = "lblJpMonthFr";
            this.lblJpMonthFr.Size = new System.Drawing.Size(21, 13);
            this.lblJpMonthFr.TabIndex = 4;
            this.lblJpMonthFr.Text = "月";
            // 
            // lblJpearTo
            // 
            this.lblJpearTo.AutoSize = true;
            this.lblJpearTo.BackColor = System.Drawing.Color.Silver;
            this.lblJpearTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblJpearTo.Location = new System.Drawing.Point(407, 34);
            this.lblJpearTo.Name = "lblJpearTo";
            this.lblJpearTo.Size = new System.Drawing.Size(21, 13);
            this.lblJpearTo.TabIndex = 10;
            this.lblJpearTo.Text = "年";
            // 
            // txtJpDayTo
            // 
            this.txtJpDayTo.AutoSizeFromLength = false;
            this.txtJpDayTo.DisplayLength = null;
            this.txtJpDayTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtJpDayTo.Location = new System.Drawing.Point(497, 30);
            this.txtJpDayTo.MaxLength = 2;
            this.txtJpDayTo.Name = "txtJpDayTo";
            this.txtJpDayTo.Size = new System.Drawing.Size(40, 20);
            this.txtJpDayTo.TabIndex = 13;
            this.txtJpDayTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtJpDayTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDayTo_Validating);
            // 
            // txtJpDayFr
            // 
            this.txtJpDayFr.AutoSizeFromLength = false;
            this.txtJpDayFr.DisplayLength = null;
            this.txtJpDayFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtJpDayFr.Location = new System.Drawing.Point(206, 30);
            this.txtJpDayFr.MaxLength = 2;
            this.txtJpDayFr.Name = "txtJpDayFr";
            this.txtJpDayFr.Size = new System.Drawing.Size(40, 20);
            this.txtJpDayFr.TabIndex = 5;
            this.txtJpDayFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtJpDayFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDayFr_Validating);
            // 
            // lblJpearFr
            // 
            this.lblJpearFr.AutoSize = true;
            this.lblJpearFr.BackColor = System.Drawing.Color.Silver;
            this.lblJpearFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblJpearFr.Location = new System.Drawing.Point(117, 34);
            this.lblJpearFr.Name = "lblJpearFr";
            this.lblJpearFr.Size = new System.Drawing.Size(21, 13);
            this.lblJpearFr.TabIndex = 2;
            this.lblJpearFr.Text = "年";
            // 
            // txtJpMonthTo
            // 
            this.txtJpMonthTo.AutoSizeFromLength = false;
            this.txtJpMonthTo.DisplayLength = null;
            this.txtJpMonthTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtJpMonthTo.Location = new System.Drawing.Point(429, 30);
            this.txtJpMonthTo.MaxLength = 2;
            this.txtJpMonthTo.Name = "txtJpMonthTo";
            this.txtJpMonthTo.Size = new System.Drawing.Size(40, 20);
            this.txtJpMonthTo.TabIndex = 11;
            this.txtJpMonthTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtJpMonthTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthTo_Validating);
            // 
            // txtJpMonthFr
            // 
            this.txtJpMonthFr.AutoSizeFromLength = false;
            this.txtJpMonthFr.DisplayLength = null;
            this.txtJpMonthFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtJpMonthFr.Location = new System.Drawing.Point(139, 30);
            this.txtJpMonthFr.MaxLength = 2;
            this.txtJpMonthFr.Name = "txtJpMonthFr";
            this.txtJpMonthFr.Size = new System.Drawing.Size(40, 20);
            this.txtJpMonthFr.TabIndex = 3;
            this.txtJpMonthFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtJpMonthFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthFr_Validating);
            // 
            // txtJpYearTo
            // 
            this.txtJpYearTo.AutoSizeFromLength = false;
            this.txtJpYearTo.DisplayLength = null;
            this.txtJpYearTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtJpYearTo.Location = new System.Drawing.Point(361, 30);
            this.txtJpYearTo.MaxLength = 2;
            this.txtJpYearTo.Name = "txtJpYearTo";
            this.txtJpYearTo.Size = new System.Drawing.Size(40, 20);
            this.txtJpYearTo.TabIndex = 9;
            this.txtJpYearTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtJpYearTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtYearTo_Validating);
            // 
            // txtJpYearFr
            // 
            this.txtJpYearFr.AutoSizeFromLength = false;
            this.txtJpYearFr.DisplayLength = null;
            this.txtJpYearFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtJpYearFr.Location = new System.Drawing.Point(72, 30);
            this.txtJpYearFr.MaxLength = 2;
            this.txtJpYearFr.Name = "txtJpYearFr";
            this.txtJpYearFr.Size = new System.Drawing.Size(40, 20);
            this.txtJpYearFr.TabIndex = 1;
            this.txtJpYearFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtJpYearFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtYearFr_Validating);
            // 
            // lblJpTo
            // 
            this.lblJpTo.BackColor = System.Drawing.Color.Silver;
            this.lblJpTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblJpTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblJpTo.Location = new System.Drawing.Point(316, 30);
            this.lblJpTo.Name = "lblJpTo";
            this.lblJpTo.Size = new System.Drawing.Size(40, 20);
            this.lblJpTo.TabIndex = 8;
            this.lblJpTo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblJpFr
            // 
            this.lblJpFr.BackColor = System.Drawing.Color.Silver;
            this.lblJpFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblJpFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblJpFr.Location = new System.Drawing.Point(27, 30);
            this.lblJpFr.Name = "lblJpFr";
            this.lblJpFr.Size = new System.Drawing.Size(40, 20);
            this.lblJpFr.TabIndex = 0;
            this.lblJpFr.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblEraBackTo
            // 
            this.lblEraBackTo.BackColor = System.Drawing.Color.Silver;
            this.lblEraBackTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblEraBackTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblEraBackTo.Location = new System.Drawing.Point(313, 28);
            this.lblEraBackTo.Name = "lblEraBackTo";
            this.lblEraBackTo.Size = new System.Drawing.Size(258, 25);
            this.lblEraBackTo.TabIndex = 905;
            this.lblEraBackTo.Text = " ";
            // 
            // lblEraBackFr
            // 
            this.lblEraBackFr.BackColor = System.Drawing.Color.Silver;
            this.lblEraBackFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblEraBackFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblEraBackFr.Location = new System.Drawing.Point(24, 28);
            this.lblEraBackFr.Name = "lblEraBackFr";
            this.lblEraBackFr.Size = new System.Drawing.Size(258, 25);
            this.lblEraBackFr.TabIndex = 906;
            this.lblEraBackFr.Text = " ";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label1);
            this.groupBox4.Controls.Add(this.lblDenpyoNoFr);
            this.groupBox4.Controls.Add(this.lblDenpyoNoTo);
            this.groupBox4.Controls.Add(this.sjTxtDenpyoNoTo);
            this.groupBox4.Controls.Add(this.sjTxtDenpyoNoFr);
            this.groupBox4.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBox4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox4.Location = new System.Drawing.Point(17, 287);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(606, 71);
            this.groupBox4.TabIndex = 4;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "伝票番号範囲";
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(293, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(17, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "～";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblDenpyoNoFr
            // 
            this.lblDenpyoNoFr.BackColor = System.Drawing.Color.Silver;
            this.lblDenpyoNoFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDenpyoNoFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDenpyoNoFr.Location = new System.Drawing.Point(83, 28);
            this.lblDenpyoNoFr.Name = "lblDenpyoNoFr";
            this.lblDenpyoNoFr.Size = new System.Drawing.Size(204, 20);
            this.lblDenpyoNoFr.TabIndex = 1;
            this.lblDenpyoNoFr.Text = "先　頭";
            this.lblDenpyoNoFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDenpyoNoTo
            // 
            this.lblDenpyoNoTo.BackColor = System.Drawing.Color.Silver;
            this.lblDenpyoNoTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDenpyoNoTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDenpyoNoTo.Location = new System.Drawing.Point(372, 28);
            this.lblDenpyoNoTo.Name = "lblDenpyoNoTo";
            this.lblDenpyoNoTo.Size = new System.Drawing.Size(204, 20);
            this.lblDenpyoNoTo.TabIndex = 4;
            this.lblDenpyoNoTo.Text = "最　後";
            this.lblDenpyoNoTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // sjTxtDenpyoNoTo
            // 
            this.sjTxtDenpyoNoTo.AutoSizeFromLength = false;
            this.sjTxtDenpyoNoTo.DisplayLength = null;
            this.sjTxtDenpyoNoTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.sjTxtDenpyoNoTo.Location = new System.Drawing.Point(316, 28);
            this.sjTxtDenpyoNoTo.MaxLength = 6;
            this.sjTxtDenpyoNoTo.Name = "sjTxtDenpyoNoTo";
            this.sjTxtDenpyoNoTo.Size = new System.Drawing.Size(50, 20);
            this.sjTxtDenpyoNoTo.TabIndex = 3;
            this.sjTxtDenpyoNoTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.sjTxtDenpyoNoTo.Validating += new System.ComponentModel.CancelEventHandler(this.TxtDenpyoNoTo_Validating);
            // 
            // sjTxtDenpyoNoFr
            // 
            this.sjTxtDenpyoNoFr.AutoSizeFromLength = false;
            this.sjTxtDenpyoNoFr.DisplayLength = null;
            this.sjTxtDenpyoNoFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.sjTxtDenpyoNoFr.Location = new System.Drawing.Point(27, 28);
            this.sjTxtDenpyoNoFr.MaxLength = 6;
            this.sjTxtDenpyoNoFr.Name = "sjTxtDenpyoNoFr";
            this.sjTxtDenpyoNoFr.Size = new System.Drawing.Size(50, 20);
            this.sjTxtDenpyoNoFr.TabIndex = 0;
            this.sjTxtDenpyoNoFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.sjTxtDenpyoNoFr.Validating += new System.ComponentModel.CancelEventHandler(this.TxtDenpyoNoFr_Validating);
            // 
            // KBDR1021
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 638);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.gbxSearchRangeDate);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.gbxTantoCd);
            this.Controls.Add(this.gbxFunanushiCd);
            this.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "KBDR1021";
            this.Par1 = "1";
            this.Text = "";
            this.Controls.SetChildIndex(this.gbxFunanushiCd, 0);
            this.Controls.SetChildIndex(this.gbxTantoCd, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.Controls.SetChildIndex(this.groupBox2, 0);
            this.Controls.SetChildIndex(this.groupBox3, 0);
            this.Controls.SetChildIndex(this.gbxSearchRangeDate, 0);
            this.Controls.SetChildIndex(this.groupBox4, 0);
            this.pnlDebug.ResumeLayout(false);
            this.gbxTantoCd.ResumeLayout(false);
            this.gbxTantoCd.PerformLayout();
            this.gbxFunanushiCd.ResumeLayout(false);
            this.gbxFunanushiCd.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.gbxSearchRangeDate.ResumeLayout(false);
            this.gbxSearchRangeDate.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox gbxTantoCd;
        private System.Windows.Forms.Label lblTantoCdFr;
        private jp.co.fsi.common.controls.FsiTextBox txtTantoCdFr;
        private System.Windows.Forms.Label lblTantoCdBet;
        private System.Windows.Forms.Label lblTantoCdTo;
        private jp.co.fsi.common.controls.FsiTextBox txtTantoCdTo;
        private System.Windows.Forms.GroupBox gbxFunanushiCd;
        private jp.co.fsi.common.controls.FsiTextBox txtFunanushiCdFr;
        private System.Windows.Forms.Label lblFunanushiCdFr;
        private System.Windows.Forms.Label lblSenshuCdBet;
        private jp.co.fsi.common.controls.FsiTextBox txtFunanushiCdTo;
        private System.Windows.Forms.Label lblFunanushiCdTo;
        private System.Windows.Forms.GroupBox groupBox1;
        private common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private System.Windows.Forms.Label lblMizuageShisho;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox cmbToriKbn;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton rdoMeisai;
        private System.Windows.Forms.RadioButton rdoGokei;
        private System.Windows.Forms.GroupBox gbxSearchRangeDate;
        private System.Windows.Forms.Label lblJpDayFrTo;
        private System.Windows.Forms.Label lblCodeBetDate;
        private System.Windows.Forms.Label lblJpDayFrFr;
        private System.Windows.Forms.Label lblJpMonthTo;
        private System.Windows.Forms.Label lblJpMonthFr;
        private System.Windows.Forms.Label lblJpearTo;
        private common.controls.FsiTextBox txtJpDayTo;
        private common.controls.FsiTextBox txtJpDayFr;
        private System.Windows.Forms.Label lblJpearFr;
        private common.controls.FsiTextBox txtJpMonthTo;
        private common.controls.FsiTextBox txtJpMonthFr;
        private common.controls.FsiTextBox txtJpYearTo;
        private common.controls.FsiTextBox txtJpYearFr;
        private System.Windows.Forms.Label lblJpTo;
        private System.Windows.Forms.Label lblJpFr;
        private System.Windows.Forms.Label lblEraBackTo;
        private System.Windows.Forms.Label lblEraBackFr;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblDenpyoNoFr;
        private System.Windows.Forms.Label lblDenpyoNoTo;
        private common.controls.FsiTextBox sjTxtDenpyoNoTo;
        private common.controls.FsiTextBox sjTxtDenpyoNoFr;
    }
}