﻿namespace jp.co.fsi.kb.kbdr1021
{
    /// <summary>
    /// KBDR1022R の概要の説明です。
    /// </summary>
    partial class KBDR1022R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(KBDR1022R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.txtGrpPages = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.直線68 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.テキスト188 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル71 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.テキスト307 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト316 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト317 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル321 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル322 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル323 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル324 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル325 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル326 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル327 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル328 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル329 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル330 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.reportInfo1 = new GrapeCity.ActiveReports.SectionReportModel.ReportInfo();
            this.label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.judgeNo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.textBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト36 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト37 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト38 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト39 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト40 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト42 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト72 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.直線91 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線92 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線93 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線94 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線95 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線96 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線97 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線98 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線99 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線100 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線102 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.detailLine = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lastLine = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.textBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line22 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.テキスト32 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト33 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト35 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト43 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト253 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト308 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.reportHeader1 = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.reportFooter1 = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.textBox4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line23 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line24 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line25 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line26 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line27 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line28 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line29 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line30 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line31 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line32 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.textBox10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line33 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line34 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line35 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line36 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line37 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line38 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.groupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.groupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line15 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line16 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line17 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line18 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line19 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line20 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line21 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line39 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.textBox16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line40 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line41 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line42 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line43 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line44 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line45 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line46 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line47 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line48 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line49 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.textBox22 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line50 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line51 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line52 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line53 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line54 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            ((System.ComponentModel.ISupportInitialize)(this.txtGrpPages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト188)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル71)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト307)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト316)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト317)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル321)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル322)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル323)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル324)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル325)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル326)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル327)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル328)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル329)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル330)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportInfo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.judgeNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト72)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト253)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト308)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtGrpPages,
            this.直線68,
            this.テキスト188,
            this.ラベル71,
            this.テキスト307,
            this.テキスト316,
            this.テキスト317,
            this.ラベル321,
            this.ラベル322,
            this.ラベル323,
            this.ラベル324,
            this.ラベル325,
            this.ラベル326,
            this.ラベル327,
            this.ラベル328,
            this.ラベル329,
            this.ラベル330,
            this.reportInfo1,
            this.label3,
            this.judgeNo,
            this.label4,
            this.label5});
            this.pageHeader.Height = 0.8165136F;
            this.pageHeader.Name = "pageHeader";
            // 
            // txtGrpPages
            // 
            this.txtGrpPages.Height = 0.1875F;
            this.txtGrpPages.Left = 10.59348F;
            this.txtGrpPages.Name = "txtGrpPages";
            this.txtGrpPages.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 1";
            this.txtGrpPages.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtGrpPages.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.txtGrpPages.Tag = "";
            this.txtGrpPages.Text = null;
            this.txtGrpPages.Top = 0.08333334F;
            this.txtGrpPages.Width = 0.375F;
            // 
            // 直線68
            // 
            this.直線68.Height = 0F;
            this.直線68.Left = 4.5132F;
            this.直線68.LineWeight = 1F;
            this.直線68.Name = "直線68";
            this.直線68.Tag = "";
            this.直線68.Top = 0.3125F;
            this.直線68.Width = 2.473611F;
            this.直線68.X1 = 4.5132F;
            this.直線68.X2 = 6.986811F;
            this.直線68.Y1 = 0.3125F;
            this.直線68.Y2 = 0.3125F;
            // 
            // テキスト188
            // 
            this.テキスト188.DataField = "ITEM03";
            this.テキスト188.Height = 0.1770833F;
            this.テキスト188.Left = 4.027088F;
            this.テキスト188.Name = "テキスト188";
            this.テキスト188.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-weight: normal; text-alig" +
    "n: center; ddo-char-set: 1";
            this.テキスト188.Tag = "";
            this.テキスト188.Text = "ITEM03";
            this.テキスト188.Top = 0.3541667F;
            this.テキスト188.Width = 3.445833F;
            // 
            // ラベル71
            // 
            this.ラベル71.Height = 0.1972222F;
            this.ラベル71.HyperLink = null;
            this.ラベル71.Left = 10.97403F;
            this.ラベル71.Name = "ラベル71";
            this.ラベル71.Style = "color: #4C535C; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-" +
    "align: right; ddo-char-set: 1";
            this.ラベル71.Tag = "";
            this.ラベル71.Text = "頁";
            this.ラベル71.Top = 0.07847222F;
            this.ラベル71.Width = 0.2027778F;
            // 
            // テキスト307
            // 
            this.テキスト307.DataField = "ITEM19";
            this.テキスト307.Height = 0.28125F;
            this.テキスト307.Left = 4.614589F;
            this.テキスト307.Name = "テキスト307";
            this.テキスト307.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 18pt; font-weight: bold; text-align:" +
    " center; ddo-char-set: 128";
            this.テキスト307.Tag = "";
            this.テキスト307.Text = "Title";
            this.テキスト307.Top = 0F;
            this.テキスト307.Width = 2.270833F;
            // 
            // テキスト316
            // 
            this.テキスト316.CanGrow = false;
            this.テキスト316.DataField = "ITEM01";
            this.テキスト316.Height = 0.2006944F;
            this.テキスト316.Left = 0.02086614F;
            this.テキスト316.Name = "テキスト316";
            this.テキスト316.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; white-space: nowrap; ddo-char-set: 128; ddo-wrap-mode: nowrap";
            this.テキスト316.Tag = "";
            this.テキスト316.Text = "ITEM01";
            this.テキスト316.Top = 0.0452756F;
            this.テキスト316.Width = 2.262599F;
            // 
            // テキスト317
            // 
            this.テキスト317.DataField = "ITEM02";
            this.テキスト317.Height = 0.2006944F;
            this.テキスト317.Left = 0F;
            this.テキスト317.Name = "テキスト317";
            this.テキスト317.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 128";
            this.テキスト317.Tag = "";
            this.テキスト317.Text = "ITEM02";
            this.テキスト317.Top = 0.3854331F;
            this.テキスト317.Width = 1.533465F;
            // 
            // ラベル321
            // 
            this.ラベル321.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル321.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル321.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル321.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル321.Height = 0.198797F;
            this.ラベル321.HyperLink = null;
            this.ラベル321.Left = 0F;
            this.ラベル321.Name = "ラベル321";
            this.ラベル321.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font" +
    "-weight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.ラベル321.Tag = "";
            this.ラベル321.Text = "伝票日付";
            this.ラベル321.Top = 0.6169292F;
            this.ラベル321.Width = 0.5799213F;
            // 
            // ラベル322
            // 
            this.ラベル322.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル322.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル322.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル322.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル322.Height = 0.1972222F;
            this.ラベル322.HyperLink = null;
            this.ラベル322.Left = 0.5799213F;
            this.ラベル322.Name = "ラベル322";
            this.ラベル322.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font" +
    "-weight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.ラベル322.Tag = "";
            this.ラベル322.Text = "伝票№";
            this.ラベル322.Top = 0.6169292F;
            this.ラベル322.Width = 0.438189F;
            // 
            // ラベル323
            // 
            this.ラベル323.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル323.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル323.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル323.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル323.Height = 0.1972222F;
            this.ラベル323.HyperLink = null;
            this.ラベル323.Left = 2.66378F;
            this.ラベル323.Name = "ラベル323";
            this.ラベル323.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font" +
    "-weight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.ラベル323.Tag = "";
            this.ラベル323.Text = "取引区分";
            this.ラベル323.Top = 0.6169292F;
            this.ラベル323.Width = 0.6770833F;
            // 
            // ラベル324
            // 
            this.ラベル324.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル324.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル324.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル324.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル324.Height = 0.1972222F;
            this.ラベル324.HyperLink = null;
            this.ラベル324.Left = 3.340945F;
            this.ラベル324.Name = "ラベル324";
            this.ラベル324.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font" +
    "-weight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.ラベル324.Tag = "";
            this.ラベル324.Text = "商　品　名　・　規　格";
            this.ラベル324.Top = 0.6169292F;
            this.ラベル324.Width = 2.762205F;
            // 
            // ラベル325
            // 
            this.ラベル325.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル325.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル325.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル325.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル325.Height = 0.1972222F;
            this.ラベル325.HyperLink = null;
            this.ラベル325.Left = 7.110237F;
            this.ラベル325.Name = "ラベル325";
            this.ラベル325.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font" +
    "-weight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.ラベル325.Tag = "";
            this.ラベル325.Text = "数量(ﾊﾞﾗ)";
            this.ラベル325.Top = 0.6169292F;
            this.ラベル325.Width = 0.6066927F;
            // 
            // ラベル326
            // 
            this.ラベル326.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル326.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル326.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル326.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル326.Height = 0.1972222F;
            this.ラベル326.HyperLink = null;
            this.ラベル326.Left = 9.794489F;
            this.ラベル326.Name = "ラベル326";
            this.ラベル326.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font" +
    "-weight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.ラベル326.Tag = "";
            this.ラベル326.Text = "合計";
            this.ラベル326.Top = 0.6169292F;
            this.ラベル326.Width = 0.8614178F;
            // 
            // ラベル327
            // 
            this.ラベル327.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル327.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル327.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル327.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル327.Height = 0.1972222F;
            this.ラベル327.HyperLink = null;
            this.ラベル327.Left = 10.65591F;
            this.ラベル327.Name = "ラベル327";
            this.ラベル327.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font" +
    "-weight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.ラベル327.Tag = "";
            this.ラベル327.Text = "担当者";
            this.ラベル327.Top = 0.6169292F;
            this.ラベル327.Width = 0.8440914F;
            // 
            // ラベル328
            // 
            this.ラベル328.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル328.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル328.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル328.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル328.Height = 0.1972222F;
            this.ラベル328.HyperLink = null;
            this.ラベル328.Left = 6.10315F;
            this.ラベル328.Name = "ラベル328";
            this.ラベル328.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font" +
    "-weight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.ラベル328.Tag = "";
            this.ラベル328.Text = "入数";
            this.ラベル328.Top = 0.6169292F;
            this.ラベル328.Width = 0.3771655F;
            // 
            // ラベル329
            // 
            this.ラベル329.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル329.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル329.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル329.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル329.Height = 0.1972222F;
            this.ラベル329.HyperLink = null;
            this.ラベル329.Left = 8.358269F;
            this.ラベル329.Name = "ラベル329";
            this.ラベル329.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font" +
    "-weight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.ラベル329.Tag = "";
            this.ラベル329.Text = "金　額";
            this.ラベル329.Top = 0.6169292F;
            this.ラベル329.Width = 0.8094482F;
            // 
            // ラベル330
            // 
            this.ラベル330.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル330.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル330.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル330.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル330.Height = 0.1972222F;
            this.ラベル330.HyperLink = null;
            this.ラベル330.Left = 7.71693F;
            this.ラベル330.Name = "ラベル330";
            this.ラベル330.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font" +
    "-weight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.ラベル330.Tag = "";
            this.ラベル330.Text = "単　価";
            this.ラベル330.Top = 0.6169292F;
            this.ラベル330.Width = 0.6413236F;
            // 
            // reportInfo1
            // 
            this.reportInfo1.FormatString = "{RunDateTime:yyyy/MM/dd}";
            this.reportInfo1.Height = 0.1924869F;
            this.reportInfo1.Left = 9.207874F;
            this.reportInfo1.Name = "reportInfo1";
            this.reportInfo1.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; ddo-char-set: 1";
            this.reportInfo1.Top = 0.07834646F;
            this.reportInfo1.Width = 1.385466F;
            // 
            // label3
            // 
            this.label3.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label3.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label3.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label3.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label3.Height = 0.1972222F;
            this.label3.HyperLink = null;
            this.label3.Left = 9.167717F;
            this.label3.Name = "label3";
            this.label3.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font" +
    "-weight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.label3.Tag = "";
            this.label3.Text = "消費税";
            this.label3.Top = 0.6169292F;
            this.label3.Width = 0.6267719F;
            // 
            // judgeNo
            // 
            this.judgeNo.DataField = " ITEM19";
            this.judgeNo.Height = 0.1874016F;
            this.judgeNo.Left = 2.303937F;
            this.judgeNo.Name = "judgeNo";
            this.judgeNo.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: left; ddo-char-set: 1";
            this.judgeNo.Tag = "";
            this.judgeNo.Text = " ITEM19";
            this.judgeNo.Top = 0.0452756F;
            this.judgeNo.Visible = false;
            this.judgeNo.Width = 0.7141732F;
            // 
            // label4
            // 
            this.label4.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label4.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label4.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label4.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label4.Height = 0.1972222F;
            this.label4.HyperLink = null;
            this.label4.Left = 6.480315F;
            this.label4.Name = "label4";
            this.label4.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font" +
    "-weight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.label4.Tag = "";
            this.label4.Text = "数量(ｹｰｽ)";
            this.label4.Top = 0.6169292F;
            this.label4.Width = 0.6299213F;
            // 
            // label5
            // 
            this.label5.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label5.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label5.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label5.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label5.Height = 0.1972222F;
            this.label5.HyperLink = null;
            this.label5.Left = 1.01811F;
            this.label5.Name = "label5";
            this.label5.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font" +
    "-weight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.label5.Tag = "";
            this.label5.Text = "船　　　主";
            this.label5.Top = 0.6169292F;
            this.label5.Width = 1.645669F;
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.textBox2,
            this.textBox1,
            this.テキスト36,
            this.テキスト37,
            this.テキスト38,
            this.テキスト39,
            this.テキスト40,
            this.テキスト42,
            this.テキスト72,
            this.直線91,
            this.直線92,
            this.直線93,
            this.直線94,
            this.直線95,
            this.直線96,
            this.直線97,
            this.直線98,
            this.直線99,
            this.直線100,
            this.直線102,
            this.detailLine,
            this.lastLine,
            this.line1,
            this.line2,
            this.line3,
            this.line4,
            this.line5,
            this.line22});
            this.detail.Height = 0.1889764F;
            this.detail.Name = "detail";
            this.detail.Format += new System.EventHandler(this.detail_Format);
            // 
            // textBox1
            // 
            this.textBox1.DataField = " ITEM16";
            this.textBox1.Height = 0.1784722F;
            this.textBox1.Left = 9.167717F;
            this.textBox1.Name = "textBox1";
            this.textBox1.OutputFormat = resources.GetString("textBox1.OutputFormat");
            this.textBox1.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; vertical-align: middle; ddo-char-set: 128";
            this.textBox1.Tag = "";
            this.textBox1.Text = " ITEM16";
            this.textBox1.Top = -9.022187E-10F;
            this.textBox1.Width = 0.6062994F;
            // 
            // テキスト36
            // 
            this.テキスト36.DataField = "ITEM09";
            this.テキスト36.Height = 0.1784722F;
            this.テキスト36.Left = 3.340945F;
            this.テキスト36.Name = "テキスト36";
            this.テキスト36.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; vertical-align: middle; ddo-char-set: 128";
            this.テキスト36.Tag = "";
            this.テキスト36.Text = "ITEM09";
            this.テキスト36.Top = 0.0003937008F;
            this.テキスト36.Width = 0.8818898F;
            // 
            // テキスト37
            // 
            this.テキスト37.DataField = "ITEM11";
            this.テキスト37.Height = 0.1784722F;
            this.テキスト37.Left = 6.10315F;
            this.テキスト37.Name = "テキスト37";
            this.テキスト37.OutputFormat = resources.GetString("テキスト37.OutputFormat");
            this.テキスト37.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; vertical-align: middle; ddo-char-set: 128";
            this.テキスト37.Tag = "";
            this.テキスト37.Text = "ITEM11";
            this.テキスト37.Top = 0F;
            this.テキスト37.Width = 0.3515749F;
            // 
            // テキスト38
            // 
            this.テキスト38.DataField = "ITEM13";
            this.テキスト38.Height = 0.1784722F;
            this.テキスト38.Left = 7.110237F;
            this.テキスト38.Name = "テキスト38";
            this.テキスト38.OutputFormat = resources.GetString("テキスト38.OutputFormat");
            this.テキスト38.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; vertical-align: middle; ddo-char-set: 128";
            this.テキスト38.Tag = "";
            this.テキスト38.Text = "ITEM13";
            this.テキスト38.Top = 0.0003937008F;
            this.テキスト38.Width = 0.5779527F;
            // 
            // テキスト39
            // 
            this.テキスト39.DataField = "ITEM14";
            this.テキスト39.Height = 0.1784722F;
            this.テキスト39.Left = 7.71693F;
            this.テキスト39.Name = "テキスト39";
            this.テキスト39.OutputFormat = resources.GetString("テキスト39.OutputFormat");
            this.テキスト39.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; vertical-align: middle; ddo-char-set: 128";
            this.テキスト39.Tag = "";
            this.テキスト39.Text = "ITEM14";
            this.テキスト39.Top = 0F;
            this.テキスト39.Width = 0.6102362F;
            // 
            // テキスト40
            // 
            this.テキスト40.DataField = "ITEM15";
            this.テキスト40.Height = 0.1784722F;
            this.テキスト40.Left = 8.37874F;
            this.テキスト40.Name = "テキスト40";
            this.テキスト40.OutputFormat = resources.GetString("テキスト40.OutputFormat");
            this.テキスト40.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; vertical-align: middle; ddo-char-set: 128";
            this.テキスト40.Tag = "";
            this.テキスト40.Text = "ITEM15";
            this.テキスト40.Top = -9.022187E-10F;
            this.テキスト40.Width = 0.7661424F;
            // 
            // テキスト42
            // 
            this.テキスト42.DataField = " ITEM17";
            this.テキスト42.Height = 0.1787402F;
            this.テキスト42.Left = 9.794489F;
            this.テキスト42.Name = "テキスト42";
            this.テキスト42.OutputFormat = resources.GetString("テキスト42.OutputFormat");
            this.テキスト42.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; vertical-align: middle; ddo-char-set: 128";
            this.テキスト42.Tag = "";
            this.テキスト42.Text = " ITEM17";
            this.テキスト42.Top = 0F;
            this.テキスト42.Width = 0.8405514F;
            // 
            // テキスト72
            // 
            this.テキスト72.DataField = "ITEM10";
            this.テキスト72.Height = 0.1784722F;
            this.テキスト72.Left = 4.285433F;
            this.テキスト72.MultiLine = false;
            this.テキスト72.Name = "テキスト72";
            this.テキスト72.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; vertical-align: middle; ddo-char-set: 128";
            this.テキスト72.Tag = "";
            this.テキスト72.Text = "ITEM10";
            this.テキスト72.Top = 0.0003937008F;
            this.テキスト72.Width = 1.817717F;
            // 
            // 直線91
            // 
            this.直線91.Height = 0.1877953F;
            this.直線91.Left = 0.5799213F;
            this.直線91.LineWeight = 1F;
            this.直線91.Name = "直線91";
            this.直線91.Tag = "";
            this.直線91.Top = 0F;
            this.直線91.Width = 0F;
            this.直線91.X1 = 0.5799213F;
            this.直線91.X2 = 0.5799213F;
            this.直線91.Y1 = 0F;
            this.直線91.Y2 = 0.1877953F;
            // 
            // 直線92
            // 
            this.直線92.Height = 0.1877953F;
            this.直線92.Left = 0.0003937008F;
            this.直線92.LineWeight = 1F;
            this.直線92.Name = "直線92";
            this.直線92.Tag = "";
            this.直線92.Top = 0F;
            this.直線92.Width = 0F;
            this.直線92.X1 = 0.0003937008F;
            this.直線92.X2 = 0.0003937008F;
            this.直線92.Y1 = 0F;
            this.直線92.Y2 = 0.1877953F;
            // 
            // 直線93
            // 
            this.直線93.Height = 0.1877953F;
            this.直線93.Left = 3.340945F;
            this.直線93.LineWeight = 1F;
            this.直線93.Name = "直線93";
            this.直線93.Tag = "";
            this.直線93.Top = 0F;
            this.直線93.Width = 0F;
            this.直線93.X1 = 3.340945F;
            this.直線93.X2 = 3.340945F;
            this.直線93.Y1 = 0F;
            this.直線93.Y2 = 0.1877953F;
            // 
            // 直線94
            // 
            this.直線94.Height = 0.1877953F;
            this.直線94.Left = 6.103149F;
            this.直線94.LineWeight = 1F;
            this.直線94.Name = "直線94";
            this.直線94.Tag = "";
            this.直線94.Top = 2.793968E-09F;
            this.直線94.Width = 0F;
            this.直線94.X1 = 6.103149F;
            this.直線94.X2 = 6.103149F;
            this.直線94.Y1 = 2.793968E-09F;
            this.直線94.Y2 = 0.1877953F;
            // 
            // 直線95
            // 
            this.直線95.Height = 0.1877953F;
            this.直線95.Left = 7.110237F;
            this.直線95.LineWeight = 1F;
            this.直線95.Name = "直線95";
            this.直線95.Tag = "";
            this.直線95.Top = 0F;
            this.直線95.Width = 0F;
            this.直線95.X1 = 7.110237F;
            this.直線95.X2 = 7.110237F;
            this.直線95.Y1 = 0F;
            this.直線95.Y2 = 0.1877953F;
            // 
            // 直線96
            // 
            this.直線96.Height = 0.1877953F;
            this.直線96.Left = 7.71693F;
            this.直線96.LineWeight = 1F;
            this.直線96.Name = "直線96";
            this.直線96.Tag = "";
            this.直線96.Top = 0F;
            this.直線96.Width = 0F;
            this.直線96.X1 = 7.71693F;
            this.直線96.X2 = 7.71693F;
            this.直線96.Y1 = 0F;
            this.直線96.Y2 = 0.1877953F;
            // 
            // 直線97
            // 
            this.直線97.Height = 0.1877953F;
            this.直線97.Left = 11.5F;
            this.直線97.LineWeight = 1F;
            this.直線97.Name = "直線97";
            this.直線97.Tag = "";
            this.直線97.Top = 0F;
            this.直線97.Width = 0F;
            this.直線97.X1 = 11.5F;
            this.直線97.X2 = 11.5F;
            this.直線97.Y1 = 0F;
            this.直線97.Y2 = 0.1877953F;
            // 
            // 直線98
            // 
            this.直線98.Height = 0.1877953F;
            this.直線98.Left = 10.65591F;
            this.直線98.LineWeight = 1F;
            this.直線98.Name = "直線98";
            this.直線98.Tag = "";
            this.直線98.Top = 0F;
            this.直線98.Width = 0F;
            this.直線98.X1 = 10.65591F;
            this.直線98.X2 = 10.65591F;
            this.直線98.Y1 = 0F;
            this.直線98.Y2 = 0.1877953F;
            // 
            // 直線99
            // 
            this.直線99.Height = 0.1877953F;
            this.直線99.Left = 9.794489F;
            this.直線99.LineWeight = 1F;
            this.直線99.Name = "直線99";
            this.直線99.Tag = "";
            this.直線99.Top = 0F;
            this.直線99.Width = 0F;
            this.直線99.X1 = 9.794489F;
            this.直線99.X2 = 9.794489F;
            this.直線99.Y1 = 0F;
            this.直線99.Y2 = 0.1877953F;
            // 
            // 直線100
            // 
            this.直線100.Height = 0.1877953F;
            this.直線100.Left = 8.358269F;
            this.直線100.LineWeight = 1F;
            this.直線100.Name = "直線100";
            this.直線100.Tag = "";
            this.直線100.Top = 0F;
            this.直線100.Width = 0F;
            this.直線100.X1 = 8.358269F;
            this.直線100.X2 = 8.358269F;
            this.直線100.Y1 = 0F;
            this.直線100.Y2 = 0.1877953F;
            // 
            // 直線102
            // 
            this.直線102.Height = 0.1877953F;
            this.直線102.Left = 0F;
            this.直線102.LineWeight = 1F;
            this.直線102.Name = "直線102";
            this.直線102.Tag = "";
            this.直線102.Top = 0F;
            this.直線102.Width = 0F;
            this.直線102.X1 = 0F;
            this.直線102.X2 = 0F;
            this.直線102.Y1 = 0F;
            this.直線102.Y2 = 0.1877953F;
            // 
            // detailLine
            // 
            this.detailLine.Height = 9.840727E-05F;
            this.detailLine.Left = 0F;
            this.detailLine.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.detailLine.LineWeight = 1F;
            this.detailLine.Name = "detailLine";
            this.detailLine.Tag = "";
            this.detailLine.Top = 0.1877953F;
            this.detailLine.Width = 11.47559F;
            this.detailLine.X1 = 0F;
            this.detailLine.X2 = 11.47559F;
            this.detailLine.Y1 = 0.1878937F;
            this.detailLine.Y2 = 0.1877953F;
            // 
            // lastLine
            // 
            this.lastLine.Height = 0F;
            this.lastLine.Left = 0.003543307F;
            this.lastLine.LineWeight = 1F;
            this.lastLine.Name = "lastLine";
            this.lastLine.Tag = "";
            this.lastLine.Top = 0.188189F;
            this.lastLine.Width = 11.53465F;
            this.lastLine.X1 = 0.003543307F;
            this.lastLine.X2 = 11.53819F;
            this.lastLine.Y1 = 0.188189F;
            this.lastLine.Y2 = 0.188189F;
            // 
            // line1
            // 
            this.line1.Height = 0.1877953F;
            this.line1.Left = 9.167717F;
            this.line1.LineWeight = 1F;
            this.line1.Name = "line1";
            this.line1.Tag = "";
            this.line1.Top = 0.0003937008F;
            this.line1.Width = 0F;
            this.line1.X1 = 9.167717F;
            this.line1.X2 = 9.167717F;
            this.line1.Y1 = 0.0003937008F;
            this.line1.Y2 = 0.188189F;
            // 
            // textBox2
            // 
            this.textBox2.DataField = "ITEM12";
            this.textBox2.Height = 0.1784722F;
            this.textBox2.Left = 6.480315F;
            this.textBox2.Name = "textBox2";
            this.textBox2.OutputFormat = resources.GetString("textBox2.OutputFormat");
            this.textBox2.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; vertical-align: middle; ddo-char-set: 128";
            this.textBox2.Tag = "";
            this.textBox2.Text = "ITEM12";
            this.textBox2.Top = 0F;
            this.textBox2.Width = 0.5866145F;
            // 
            // line2
            // 
            this.line2.Height = 0.1877953F;
            this.line2.Left = 6.480315F;
            this.line2.LineWeight = 1F;
            this.line2.Name = "line2";
            this.line2.Tag = "";
            this.line2.Top = 0.0003937008F;
            this.line2.Width = 0F;
            this.line2.X1 = 6.480315F;
            this.line2.X2 = 6.480315F;
            this.line2.Y1 = 0.0003937008F;
            this.line2.Y2 = 0.188189F;
            // 
            // line3
            // 
            this.line3.Height = 0.1877953F;
            this.line3.Left = 2.66378F;
            this.line3.LineWeight = 1F;
            this.line3.Name = "line3";
            this.line3.Tag = "";
            this.line3.Top = 0F;
            this.line3.Width = 0F;
            this.line3.X1 = 2.66378F;
            this.line3.X2 = 2.66378F;
            this.line3.Y1 = 0F;
            this.line3.Y2 = 0.1877953F;
            // 
            // line4
            // 
            this.line4.Height = 0.1877953F;
            this.line4.Left = 1.01811F;
            this.line4.LineWeight = 1F;
            this.line4.Name = "line4";
            this.line4.Tag = "";
            this.line4.Top = 0F;
            this.line4.Width = 0F;
            this.line4.X1 = 1.01811F;
            this.line4.X2 = 1.01811F;
            this.line4.Y1 = 0F;
            this.line4.Y2 = 0.1877953F;
            // 
            // line5
            // 
            this.line5.Height = 0.1877953F;
            this.line5.Left = 2.66378F;
            this.line5.LineWeight = 1F;
            this.line5.Name = "line5";
            this.line5.Tag = "";
            this.line5.Top = 0F;
            this.line5.Width = 0F;
            this.line5.X1 = 2.66378F;
            this.line5.X2 = 2.66378F;
            this.line5.Y1 = 0F;
            this.line5.Y2 = 0.1877953F;
            // 
            // line22
            // 
            this.line22.Height = 0.1877953F;
            this.line22.Left = 0.003543307F;
            this.line22.LineWeight = 1F;
            this.line22.Name = "line22";
            this.line22.Tag = "";
            this.line22.Top = 0F;
            this.line22.Width = 0F;
            this.line22.X1 = 0.003543307F;
            this.line22.X2 = 0.003543307F;
            this.line22.Y1 = 0F;
            this.line22.Y2 = 0.1877953F;
            // 
            // テキスト32
            // 
            this.テキスト32.DataField = "ITEM04";
            this.テキスト32.Height = 0.1784722F;
            this.テキスト32.Left = 0.03543307F;
            this.テキスト32.Name = "テキスト32";
            this.テキスト32.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-al" +
    "ign: center; vertical-align: middle; ddo-char-set: 128";
            this.テキスト32.Tag = "";
            this.テキスト32.Text = "ITEM04";
            this.テキスト32.Top = 0F;
            this.テキスト32.Width = 0.5279856F;
            // 
            // テキスト33
            // 
            this.テキスト33.DataField = "ITEM05";
            this.テキスト33.Height = 0.1784722F;
            this.テキスト33.Left = 0.5799213F;
            this.テキスト33.Name = "テキスト33";
            this.テキスト33.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-al" +
    "ign: right; vertical-align: middle; ddo-char-set: 128";
            this.テキスト33.Tag = "";
            this.テキスト33.Text = "ITEM05";
            this.テキスト33.Top = 9.313226E-10F;
            this.テキスト33.Width = 0.4070866F;
            // 
            // テキスト35
            // 
            this.テキスト35.DataField = "ITEM08";
            this.テキスト35.Height = 0.1784722F;
            this.テキスト35.Left = 2.66378F;
            this.テキスト35.MultiLine = false;
            this.テキスト35.Name = "テキスト35";
            this.テキスト35.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-al" +
    "ign: center; vertical-align: middle; ddo-char-set: 128";
            this.テキスト35.Tag = "";
            this.テキスト35.Text = "ITEM08";
            this.テキスト35.Top = 9.313226E-10F;
            this.テキスト35.Width = 0.6771653F;
            // 
            // テキスト43
            // 
            this.テキスト43.CanGrow = false;
            this.テキスト43.DataField = " ITEM18";
            this.テキスト43.Height = 0.1784722F;
            this.テキスト43.Left = 10.68701F;
            this.テキスト43.Name = "テキスト43";
            this.テキスト43.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-al" +
    "ign: left; vertical-align: middle; white-space: nowrap; ddo-char-set: 128; ddo-w" +
    "rap-mode: nowrap";
            this.テキスト43.Tag = "";
            this.テキスト43.Text = " ITEM18";
            this.テキスト43.Top = 0F;
            this.テキスト43.Width = 0.7885819F;
            // 
            // テキスト253
            // 
            this.テキスト253.CanGrow = false;
            this.テキスト253.DataField = "ITEM06";
            this.テキスト253.Height = 0.1784722F;
            this.テキスト253.Left = 1.01811F;
            this.テキスト253.MultiLine = false;
            this.テキスト253.Name = "テキスト253";
            this.テキスト253.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-al" +
    "ign: right; vertical-align: middle; ddo-char-set: 128";
            this.テキスト253.Tag = "";
            this.テキスト253.Text = "ITEM06";
            this.テキスト253.Top = 0F;
            this.テキスト253.Width = 0.3161418F;
            // 
            // テキスト308
            // 
            this.テキスト308.DataField = "ITEM07";
            this.テキスト308.Height = 0.1784722F;
            this.テキスト308.Left = 1.365354F;
            this.テキスト308.MultiLine = false;
            this.テキスト308.Name = "テキスト308";
            this.テキスト308.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-al" +
    "ign: left; vertical-align: middle; ddo-char-set: 128";
            this.テキスト308.Tag = "";
            this.テキスト308.Text = "ITEM07";
            this.テキスト308.Top = 0F;
            this.テキスト308.Width = 1.298425F;
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0.1666667F;
            this.pageFooter.Name = "pageFooter";
            // 
            // reportHeader1
            // 
            this.reportHeader1.Height = 0F;
            this.reportHeader1.Name = "reportHeader1";
            // 
            // reportFooter1
            // 
            this.reportFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.textBox10,
            this.textBox4,
            this.textBox5,
            this.textBox6,
            this.textBox7,
            this.textBox8,
            this.line23,
            this.line24,
            this.line25,
            this.line26,
            this.line27,
            this.line28,
            this.line29,
            this.line30,
            this.line31,
            this.line32,
            this.line33,
            this.line34,
            this.line35,
            this.line36,
            this.line37,
            this.line38});
            this.reportFooter1.Height = 0.1889764F;
            this.reportFooter1.Name = "reportFooter1";
            // 
            // textBox4
            // 
            this.textBox4.DataField = " ITEM16";
            this.textBox4.Height = 0.1784722F;
            this.textBox4.Left = 9.162599F;
            this.textBox4.Name = "textBox4";
            this.textBox4.OutputFormat = resources.GetString("textBox4.OutputFormat");
            this.textBox4.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; vertical-align: middle; ddo-char-set: 128";
            this.textBox4.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.textBox4.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.textBox4.Tag = "";
            this.textBox4.Text = " ITEM16";
            this.textBox4.Top = 0F;
            this.textBox4.Width = 0.6062995F;
            // 
            // textBox5
            // 
            this.textBox5.DataField = "ITEM13";
            this.textBox5.Height = 0.1784722F;
            this.textBox5.Left = 7.105119F;
            this.textBox5.Name = "textBox5";
            this.textBox5.OutputFormat = resources.GetString("textBox5.OutputFormat");
            this.textBox5.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; vertical-align: middle; ddo-char-set: 128";
            this.textBox5.Tag = "";
            this.textBox5.Text = "ITEM13";
            this.textBox5.Top = 0.0003937036F;
            this.textBox5.Width = 0.5779528F;
            // 
            // textBox6
            // 
            this.textBox6.DataField = "ITEM15";
            this.textBox6.Height = 0.1784722F;
            this.textBox6.Left = 8.373626F;
            this.textBox6.Name = "textBox6";
            this.textBox6.OutputFormat = resources.GetString("textBox6.OutputFormat");
            this.textBox6.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; vertical-align: middle; ddo-char-set: 128";
            this.textBox6.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.textBox6.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.textBox6.Tag = "";
            this.textBox6.Text = "ITEM15";
            this.textBox6.Top = 0F;
            this.textBox6.Width = 0.7661424F;
            // 
            // textBox7
            // 
            this.textBox7.DataField = " ITEM17";
            this.textBox7.Height = 0.1787402F;
            this.textBox7.Left = 9.789369F;
            this.textBox7.Name = "textBox7";
            this.textBox7.OutputFormat = resources.GetString("textBox7.OutputFormat");
            this.textBox7.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; vertical-align: middle; ddo-char-set: 128";
            this.textBox7.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.textBox7.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.textBox7.Tag = "";
            this.textBox7.Text = " ITEM17";
            this.textBox7.Top = 0F;
            this.textBox7.Width = 0.8405514F;
            // 
            // textBox8
            // 
            this.textBox8.Height = 0.1784722F;
            this.textBox8.Left = 4.285552F;
            this.textBox8.MultiLine = false;
            this.textBox8.Name = "textBox8";
            this.textBox8.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; vertical-align: middle; ddo-char-set: 128";
            this.textBox8.Tag = "";
            this.textBox8.Text = "【　合　　　計　】";
            this.textBox8.Top = 0.0003937036F;
            this.textBox8.Width = 1.81248F;
            // 
            // line23
            // 
            this.line23.Height = 0.1877953F;
            this.line23.Left = 0.5800402F;
            this.line23.LineWeight = 1F;
            this.line23.Name = "line23";
            this.line23.Tag = "";
            this.line23.Top = 0F;
            this.line23.Width = 0F;
            this.line23.X1 = 0.5800402F;
            this.line23.X2 = 0.5800402F;
            this.line23.Y1 = 0F;
            this.line23.Y2 = 0.1877953F;
            // 
            // line24
            // 
            this.line24.Height = 0.1877953F;
            this.line24.Left = 3.341064F;
            this.line24.LineWeight = 1F;
            this.line24.Name = "line24";
            this.line24.Tag = "";
            this.line24.Top = 0F;
            this.line24.Width = 0F;
            this.line24.X1 = 3.341064F;
            this.line24.X2 = 3.341064F;
            this.line24.Y1 = 0F;
            this.line24.Y2 = 0.1877953F;
            // 
            // line25
            // 
            this.line25.Height = 0.1877953F;
            this.line25.Left = 6.103149F;
            this.line25.LineWeight = 1F;
            this.line25.Name = "line25";
            this.line25.Tag = "";
            this.line25.Top = 3.72529E-09F;
            this.line25.Width = 0F;
            this.line25.X1 = 6.103149F;
            this.line25.X2 = 6.103149F;
            this.line25.Y1 = 3.72529E-09F;
            this.line25.Y2 = 0.1877953F;
            // 
            // line26
            // 
            this.line26.Height = 0.1877953F;
            this.line26.Left = 7.110236F;
            this.line26.LineWeight = 1F;
            this.line26.Name = "line26";
            this.line26.Tag = "";
            this.line26.Top = 0F;
            this.line26.Width = 0F;
            this.line26.X1 = 7.110236F;
            this.line26.X2 = 7.110236F;
            this.line26.Y1 = 0F;
            this.line26.Y2 = 0.1877953F;
            // 
            // line27
            // 
            this.line27.Height = 0.1877953F;
            this.line27.Left = 7.716929F;
            this.line27.LineWeight = 1F;
            this.line27.Name = "line27";
            this.line27.Tag = "";
            this.line27.Top = 0F;
            this.line27.Width = 0F;
            this.line27.X1 = 7.716929F;
            this.line27.X2 = 7.716929F;
            this.line27.Y1 = 0F;
            this.line27.Y2 = 0.1877953F;
            // 
            // line28
            // 
            this.line28.Height = 0.1877953F;
            this.line28.Left = 11.5F;
            this.line28.LineWeight = 1F;
            this.line28.Name = "line28";
            this.line28.Tag = "";
            this.line28.Top = 0F;
            this.line28.Width = 0F;
            this.line28.X1 = 11.5F;
            this.line28.X2 = 11.5F;
            this.line28.Y1 = 0F;
            this.line28.Y2 = 0.1877953F;
            // 
            // line29
            // 
            this.line29.Height = 0.1877953F;
            this.line29.Left = 10.65591F;
            this.line29.LineWeight = 1F;
            this.line29.Name = "line29";
            this.line29.Tag = "";
            this.line29.Top = 0F;
            this.line29.Width = 0F;
            this.line29.X1 = 10.65591F;
            this.line29.X2 = 10.65591F;
            this.line29.Y1 = 0F;
            this.line29.Y2 = 0.1877953F;
            // 
            // line30
            // 
            this.line30.Height = 0.1877953F;
            this.line30.Left = 9.794489F;
            this.line30.LineWeight = 1F;
            this.line30.Name = "line30";
            this.line30.Tag = "";
            this.line30.Top = 0F;
            this.line30.Width = 0F;
            this.line30.X1 = 9.794489F;
            this.line30.X2 = 9.794489F;
            this.line30.Y1 = 0F;
            this.line30.Y2 = 0.1877953F;
            // 
            // line31
            // 
            this.line31.Height = 0.1877953F;
            this.line31.Left = 8.358268F;
            this.line31.LineWeight = 1F;
            this.line31.Name = "line31";
            this.line31.Tag = "";
            this.line31.Top = 0F;
            this.line31.Width = 0F;
            this.line31.X1 = 8.358268F;
            this.line31.X2 = 8.358268F;
            this.line31.Y1 = 0F;
            this.line31.Y2 = 0.1877953F;
            // 
            // line32
            // 
            this.line32.Height = 0.1877953F;
            this.line32.Left = 9.167716F;
            this.line32.LineWeight = 1F;
            this.line32.Name = "line32";
            this.line32.Tag = "";
            this.line32.Top = 0.006299213F;
            this.line32.Width = 0F;
            this.line32.X1 = 9.167716F;
            this.line32.X2 = 9.167716F;
            this.line32.Y1 = 0.006299213F;
            this.line32.Y2 = 0.1940945F;
            // 
            // textBox10
            // 
            this.textBox10.DataField = "ITEM12";
            this.textBox10.Height = 0.1784722F;
            this.textBox10.Left = 6.475198F;
            this.textBox10.Name = "textBox10";
            this.textBox10.OutputFormat = resources.GetString("textBox10.OutputFormat");
            this.textBox10.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; vertical-align: middle; ddo-char-set: 128";
            this.textBox10.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.textBox10.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.textBox10.Tag = "";
            this.textBox10.Text = "ITEM12";
            this.textBox10.Top = 0F;
            this.textBox10.Width = 0.5866145F;
            // 
            // line33
            // 
            this.line33.Height = 0.1877953F;
            this.line33.Left = 6.480315F;
            this.line33.LineWeight = 1F;
            this.line33.Name = "line33";
            this.line33.Tag = "";
            this.line33.Top = 0.0003936999F;
            this.line33.Width = 0F;
            this.line33.X1 = 6.480315F;
            this.line33.X2 = 6.480315F;
            this.line33.Y1 = 0.0003936999F;
            this.line33.Y2 = 0.188189F;
            // 
            // line34
            // 
            this.line34.Height = 0.1877953F;
            this.line34.Left = 2.663899F;
            this.line34.LineWeight = 1F;
            this.line34.Name = "line34";
            this.line34.Tag = "";
            this.line34.Top = 0F;
            this.line34.Width = 0F;
            this.line34.X1 = 2.663899F;
            this.line34.X2 = 2.663899F;
            this.line34.Y1 = 0F;
            this.line34.Y2 = 0.1877953F;
            // 
            // line35
            // 
            this.line35.Height = 0.1877953F;
            this.line35.Left = 1.018229F;
            this.line35.LineWeight = 1F;
            this.line35.Name = "line35";
            this.line35.Tag = "";
            this.line35.Top = 0F;
            this.line35.Width = 0F;
            this.line35.X1 = 1.018229F;
            this.line35.X2 = 1.018229F;
            this.line35.Y1 = 0F;
            this.line35.Y2 = 0.1877953F;
            // 
            // line36
            // 
            this.line36.Height = 0.1877953F;
            this.line36.Left = 2.663899F;
            this.line36.LineWeight = 1F;
            this.line36.Name = "line36";
            this.line36.Tag = "";
            this.line36.Top = 0F;
            this.line36.Width = 0F;
            this.line36.X1 = 2.663899F;
            this.line36.X2 = 2.663899F;
            this.line36.Y1 = 0F;
            this.line36.Y2 = 0.1877953F;
            // 
            // line37
            // 
            this.line37.Height = 0F;
            this.line37.Left = 0.007205486F;
            this.line37.LineWeight = 1F;
            this.line37.Name = "line37";
            this.line37.Tag = "";
            this.line37.Top = 0.1877953F;
            this.line37.Width = 11.53465F;
            this.line37.X1 = 0.007205486F;
            this.line37.X2 = 11.54185F;
            this.line37.Y1 = 0.1877953F;
            this.line37.Y2 = 0.1877953F;
            // 
            // line38
            // 
            this.line38.Height = 0.1877953F;
            this.line38.Left = 0.003543307F;
            this.line38.LineWeight = 1F;
            this.line38.Name = "line38";
            this.line38.Tag = "";
            this.line38.Top = 0F;
            this.line38.Width = 0F;
            this.line38.X1 = 0.003543307F;
            this.line38.X2 = 0.003543307F;
            this.line38.Y1 = 0F;
            this.line38.Y2 = 0.1877953F;
            // 
            // groupHeader1
            // 
            this.groupHeader1.CanGrow = false;
            this.groupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.テキスト33,
            this.テキスト253,
            this.テキスト308,
            this.テキスト35,
            this.テキスト43,
            this.テキスト32});
            this.groupHeader1.DataField = "ITEM05";
            this.groupHeader1.Height = 0.1784722F;
            this.groupHeader1.Name = "groupHeader1";
            this.groupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
            this.groupHeader1.UnderlayNext = true;
            // 
            // groupFooter1
            // 
            this.groupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.textBox20,
            this.textBox17,
            this.textBox19,
            this.textBox18,
            this.textBox22,
            this.line6,
            this.line8,
            this.line9,
            this.line10,
            this.line11,
            this.line12,
            this.line13,
            this.line14,
            this.line15,
            this.line16,
            this.line17,
            this.line18,
            this.line19,
            this.line20,
            this.line21,
            this.line7,
            this.line39,
            this.textBox16,
            this.line40,
            this.line41,
            this.line42,
            this.line43,
            this.line44,
            this.line45,
            this.line46,
            this.line47,
            this.line48,
            this.line49,
            this.line50,
            this.line51,
            this.line52,
            this.line53,
            this.line54});
            this.groupFooter1.Height = 0.3764764F;
            this.groupFooter1.Name = "groupFooter1";
            // 
            // line6
            // 
            this.line6.Height = 0.1877953F;
            this.line6.Left = 0.5799213F;
            this.line6.LineWeight = 1F;
            this.line6.Name = "line6";
            this.line6.Tag = "";
            this.line6.Top = 0F;
            this.line6.Width = 0F;
            this.line6.X1 = 0.5799213F;
            this.line6.X2 = 0.5799213F;
            this.line6.Y1 = 0F;
            this.line6.Y2 = 0.1877953F;
            // 
            // line8
            // 
            this.line8.Height = 0.1877953F;
            this.line8.Left = 3.340945F;
            this.line8.LineWeight = 1F;
            this.line8.Name = "line8";
            this.line8.Tag = "";
            this.line8.Top = 0F;
            this.line8.Width = 0F;
            this.line8.X1 = 3.340945F;
            this.line8.X2 = 3.340945F;
            this.line8.Y1 = 0F;
            this.line8.Y2 = 0.1877953F;
            // 
            // line9
            // 
            this.line9.Height = 0.1877953F;
            this.line9.Left = 6.103149F;
            this.line9.LineWeight = 1F;
            this.line9.Name = "line9";
            this.line9.Tag = "";
            this.line9.Top = 3.72529E-09F;
            this.line9.Width = 0F;
            this.line9.X1 = 6.103149F;
            this.line9.X2 = 6.103149F;
            this.line9.Y1 = 3.72529E-09F;
            this.line9.Y2 = 0.1877953F;
            // 
            // line10
            // 
            this.line10.Height = 0.1877953F;
            this.line10.Left = 7.110236F;
            this.line10.LineWeight = 1F;
            this.line10.Name = "line10";
            this.line10.Tag = "";
            this.line10.Top = 9.313226E-10F;
            this.line10.Width = 0F;
            this.line10.X1 = 7.110236F;
            this.line10.X2 = 7.110236F;
            this.line10.Y1 = 9.313226E-10F;
            this.line10.Y2 = 0.1877953F;
            // 
            // line11
            // 
            this.line11.Height = 0.1877953F;
            this.line11.Left = 7.716929F;
            this.line11.LineWeight = 1F;
            this.line11.Name = "line11";
            this.line11.Tag = "";
            this.line11.Top = 9.313226E-10F;
            this.line11.Width = 0F;
            this.line11.X1 = 7.716929F;
            this.line11.X2 = 7.716929F;
            this.line11.Y1 = 9.313226E-10F;
            this.line11.Y2 = 0.1877953F;
            // 
            // line12
            // 
            this.line12.Height = 0.1877953F;
            this.line12.Left = 11.5F;
            this.line12.LineWeight = 1F;
            this.line12.Name = "line12";
            this.line12.Tag = "";
            this.line12.Top = 0F;
            this.line12.Width = 0F;
            this.line12.X1 = 11.5F;
            this.line12.X2 = 11.5F;
            this.line12.Y1 = 0F;
            this.line12.Y2 = 0.1877953F;
            // 
            // line13
            // 
            this.line13.Height = 0.1877953F;
            this.line13.Left = 10.65591F;
            this.line13.LineWeight = 1F;
            this.line13.Name = "line13";
            this.line13.Tag = "";
            this.line13.Top = 9.313226E-10F;
            this.line13.Width = 0F;
            this.line13.X1 = 10.65591F;
            this.line13.X2 = 10.65591F;
            this.line13.Y1 = 9.313226E-10F;
            this.line13.Y2 = 0.1877953F;
            // 
            // line14
            // 
            this.line14.Height = 0.1877953F;
            this.line14.Left = 9.794489F;
            this.line14.LineWeight = 1F;
            this.line14.Name = "line14";
            this.line14.Tag = "";
            this.line14.Top = 9.313226E-10F;
            this.line14.Width = 0F;
            this.line14.X1 = 9.794489F;
            this.line14.X2 = 9.794489F;
            this.line14.Y1 = 9.313226E-10F;
            this.line14.Y2 = 0.1877953F;
            // 
            // line15
            // 
            this.line15.Height = 0.1877953F;
            this.line15.Left = 8.358268F;
            this.line15.LineWeight = 1F;
            this.line15.Name = "line15";
            this.line15.Tag = "";
            this.line15.Top = 9.313226E-10F;
            this.line15.Width = 0F;
            this.line15.X1 = 8.358268F;
            this.line15.X2 = 8.358268F;
            this.line15.Y1 = 9.313226E-10F;
            this.line15.Y2 = 0.1877953F;
            // 
            // line16
            // 
            this.line16.Height = 0.1877953F;
            this.line16.Left = 9.167716F;
            this.line16.LineWeight = 1F;
            this.line16.Name = "line16";
            this.line16.Tag = "";
            this.line16.Top = 0.006299214F;
            this.line16.Width = 0F;
            this.line16.X1 = 9.167716F;
            this.line16.X2 = 9.167716F;
            this.line16.Y1 = 0.006299214F;
            this.line16.Y2 = 0.1940945F;
            // 
            // line17
            // 
            this.line17.Height = 0.1877953F;
            this.line17.Left = 6.480315F;
            this.line17.LineWeight = 1F;
            this.line17.Name = "line17";
            this.line17.Tag = "";
            this.line17.Top = 0.0003937017F;
            this.line17.Width = 0F;
            this.line17.X1 = 6.480315F;
            this.line17.X2 = 6.480315F;
            this.line17.Y1 = 0.0003937017F;
            this.line17.Y2 = 0.188189F;
            // 
            // line18
            // 
            this.line18.Height = 0.1877953F;
            this.line18.Left = 2.66378F;
            this.line18.LineWeight = 1F;
            this.line18.Name = "line18";
            this.line18.Tag = "";
            this.line18.Top = 0F;
            this.line18.Width = 0F;
            this.line18.X1 = 2.66378F;
            this.line18.X2 = 2.66378F;
            this.line18.Y1 = 0F;
            this.line18.Y2 = 0.1877953F;
            // 
            // line19
            // 
            this.line19.Height = 0.1877953F;
            this.line19.Left = 1.01811F;
            this.line19.LineWeight = 1F;
            this.line19.Name = "line19";
            this.line19.Tag = "";
            this.line19.Top = 0F;
            this.line19.Width = 0F;
            this.line19.X1 = 1.01811F;
            this.line19.X2 = 1.01811F;
            this.line19.Y1 = 0F;
            this.line19.Y2 = 0.1877953F;
            // 
            // line20
            // 
            this.line20.Height = 0.1877953F;
            this.line20.Left = 2.66378F;
            this.line20.LineWeight = 1F;
            this.line20.Name = "line20";
            this.line20.Tag = "";
            this.line20.Top = 0F;
            this.line20.Width = 0F;
            this.line20.X1 = 2.66378F;
            this.line20.X2 = 2.66378F;
            this.line20.Y1 = 0F;
            this.line20.Y2 = 0.1877953F;
            // 
            // line21
            // 
            this.line21.Height = 0F;
            this.line21.Left = 0F;
            this.line21.LineWeight = 1F;
            this.line21.Name = "line21";
            this.line21.Tag = "";
            this.line21.Top = 0.1877953F;
            this.line21.Width = 11.54173F;
            this.line21.X1 = 0F;
            this.line21.X2 = 11.54173F;
            this.line21.Y1 = 0.1877953F;
            this.line21.Y2 = 0.1877953F;
            // 
            // line7
            // 
            this.line7.Height = 0.1877953F;
            this.line7.Left = 0F;
            this.line7.LineWeight = 1F;
            this.line7.Name = "line7";
            this.line7.Tag = "";
            this.line7.Top = 0F;
            this.line7.Width = 0F;
            this.line7.X1 = 0F;
            this.line7.X2 = 0F;
            this.line7.Y1 = 0F;
            this.line7.Y2 = 0.1877953F;
            // 
            // line39
            // 
            this.line39.Height = 0.3779528F;
            this.line39.Left = 0.003543307F;
            this.line39.LineWeight = 1F;
            this.line39.Name = "line39";
            this.line39.Tag = "";
            this.line39.Top = 0F;
            this.line39.Width = 0F;
            this.line39.X1 = 0.003543307F;
            this.line39.X2 = 0.003543307F;
            this.line39.Y1 = 0F;
            this.line39.Y2 = 0.3779528F;
            // 
            // textBox16
            // 
            this.textBox16.Height = 0.1784722F;
            this.textBox16.Left = 3.340945F;
            this.textBox16.Name = "textBox16";
            this.textBox16.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; vertical-align: middle; ddo-char-set: 128";
            this.textBox16.Tag = "";
            this.textBox16.Text = "【 伝 票 合 計 】";
            this.textBox16.Top = 0F;
            this.textBox16.Width = 1.173622F;
            // 
            // textBox17
            // 
            this.textBox17.DataField = " ITEM16";
            this.textBox17.Height = 0.1784722F;
            this.textBox17.Left = 9.162599F;
            this.textBox17.Name = "textBox17";
            this.textBox17.OutputFormat = resources.GetString("textBox17.OutputFormat");
            this.textBox17.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; vertical-align: middle; ddo-char-set: 128";
            this.textBox17.SummaryGroup = "groupHeader1";
            this.textBox17.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox17.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox17.Tag = "";
            this.textBox17.Text = " ITEM16";
            this.textBox17.Top = 0F;
            this.textBox17.Width = 0.6062995F;
            // 
            // textBox18
            // 
            this.textBox18.DataField = "ITEM13";
            this.textBox18.Height = 0.1784722F;
            this.textBox18.Left = 7.105119F;
            this.textBox18.Name = "textBox18";
            this.textBox18.OutputFormat = resources.GetString("textBox18.OutputFormat");
            this.textBox18.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; vertical-align: middle; ddo-char-set: 128";
            this.textBox18.SummaryGroup = "groupHeader1";
            this.textBox18.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox18.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox18.Tag = "";
            this.textBox18.Text = "ITEM13";
            this.textBox18.Top = 0F;
            this.textBox18.Width = 0.5779528F;
            // 
            // textBox19
            // 
            this.textBox19.DataField = "ITEM15";
            this.textBox19.Height = 0.1784722F;
            this.textBox19.Left = 8.37874F;
            this.textBox19.Name = "textBox19";
            this.textBox19.OutputFormat = resources.GetString("textBox19.OutputFormat");
            this.textBox19.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; vertical-align: middle; ddo-char-set: 128";
            this.textBox19.SummaryGroup = "groupHeader1";
            this.textBox19.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox19.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox19.Tag = "";
            this.textBox19.Text = "ITEM15";
            this.textBox19.Top = 0F;
            this.textBox19.Width = 0.7661424F;
            // 
            // textBox20
            // 
            this.textBox20.DataField = " ITEM17";
            this.textBox20.Height = 0.1787402F;
            this.textBox20.Left = 9.794489F;
            this.textBox20.Name = "textBox20";
            this.textBox20.OutputFormat = resources.GetString("textBox20.OutputFormat");
            this.textBox20.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; vertical-align: middle; ddo-char-set: 128";
            this.textBox20.SummaryGroup = "groupHeader1";
            this.textBox20.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox20.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox20.Tag = "";
            this.textBox20.Text = " ITEM17";
            this.textBox20.Top = 0F;
            this.textBox20.Width = 0.8405514F;
            // 
            // line40
            // 
            this.line40.Height = 0.1877953F;
            this.line40.Left = 0.5799212F;
            this.line40.LineWeight = 1F;
            this.line40.Name = "line40";
            this.line40.Tag = "";
            this.line40.Top = 0.1850394F;
            this.line40.Width = 0F;
            this.line40.X1 = 0.5799212F;
            this.line40.X2 = 0.5799212F;
            this.line40.Y1 = 0.1850394F;
            this.line40.Y2 = 0.3728347F;
            // 
            // line41
            // 
            this.line41.Height = 0.1877953F;
            this.line41.Left = 3.340945F;
            this.line41.LineWeight = 1F;
            this.line41.Name = "line41";
            this.line41.Tag = "";
            this.line41.Top = 0.1850394F;
            this.line41.Width = 0F;
            this.line41.X1 = 3.340945F;
            this.line41.X2 = 3.340945F;
            this.line41.Y1 = 0.1850394F;
            this.line41.Y2 = 0.3728347F;
            // 
            // line42
            // 
            this.line42.Height = 0.1877953F;
            this.line42.Left = 6.103149F;
            this.line42.LineWeight = 1F;
            this.line42.Name = "line42";
            this.line42.Tag = "";
            this.line42.Top = 0.1850394F;
            this.line42.Width = 0F;
            this.line42.X1 = 6.103149F;
            this.line42.X2 = 6.103149F;
            this.line42.Y1 = 0.1850394F;
            this.line42.Y2 = 0.3728347F;
            // 
            // line43
            // 
            this.line43.Height = 0.1877953F;
            this.line43.Left = 7.110236F;
            this.line43.LineWeight = 1F;
            this.line43.Name = "line43";
            this.line43.Tag = "";
            this.line43.Top = 0.1850394F;
            this.line43.Width = 0F;
            this.line43.X1 = 7.110236F;
            this.line43.X2 = 7.110236F;
            this.line43.Y1 = 0.1850394F;
            this.line43.Y2 = 0.3728347F;
            // 
            // line44
            // 
            this.line44.Height = 0.1877953F;
            this.line44.Left = 7.716929F;
            this.line44.LineWeight = 1F;
            this.line44.Name = "line44";
            this.line44.Tag = "";
            this.line44.Top = 0.1850394F;
            this.line44.Width = 0F;
            this.line44.X1 = 7.716929F;
            this.line44.X2 = 7.716929F;
            this.line44.Y1 = 0.1850394F;
            this.line44.Y2 = 0.3728347F;
            // 
            // line45
            // 
            this.line45.Height = 0.3779528F;
            this.line45.Left = 11.5F;
            this.line45.LineWeight = 1F;
            this.line45.Name = "line45";
            this.line45.Tag = "";
            this.line45.Top = 0F;
            this.line45.Width = 0F;
            this.line45.X1 = 11.5F;
            this.line45.X2 = 11.5F;
            this.line45.Y1 = 0F;
            this.line45.Y2 = 0.3779528F;
            // 
            // line46
            // 
            this.line46.Height = 0.1877953F;
            this.line46.Left = 10.65591F;
            this.line46.LineWeight = 1F;
            this.line46.Name = "line46";
            this.line46.Tag = "";
            this.line46.Top = 0.1850394F;
            this.line46.Width = 0F;
            this.line46.X1 = 10.65591F;
            this.line46.X2 = 10.65591F;
            this.line46.Y1 = 0.1850394F;
            this.line46.Y2 = 0.3728347F;
            // 
            // line47
            // 
            this.line47.Height = 0.1877953F;
            this.line47.Left = 9.794489F;
            this.line47.LineWeight = 1F;
            this.line47.Name = "line47";
            this.line47.Tag = "";
            this.line47.Top = 0.1850394F;
            this.line47.Width = 0F;
            this.line47.X1 = 9.794489F;
            this.line47.X2 = 9.794489F;
            this.line47.Y1 = 0.1850394F;
            this.line47.Y2 = 0.3728347F;
            // 
            // line48
            // 
            this.line48.Height = 0.1877953F;
            this.line48.Left = 8.358268F;
            this.line48.LineWeight = 1F;
            this.line48.Name = "line48";
            this.line48.Tag = "";
            this.line48.Top = 0.1850394F;
            this.line48.Width = 0F;
            this.line48.X1 = 8.358268F;
            this.line48.X2 = 8.358268F;
            this.line48.Y1 = 0.1850394F;
            this.line48.Y2 = 0.3728347F;
            // 
            // line49
            // 
            this.line49.Height = 0.1877953F;
            this.line49.Left = 9.167716F;
            this.line49.LineWeight = 1F;
            this.line49.Name = "line49";
            this.line49.Tag = "";
            this.line49.Top = 0.1913386F;
            this.line49.Width = 0F;
            this.line49.X1 = 9.167716F;
            this.line49.X2 = 9.167716F;
            this.line49.Y1 = 0.1913386F;
            this.line49.Y2 = 0.3791339F;
            // 
            // textBox22
            // 
            this.textBox22.DataField = "ITEM12";
            this.textBox22.Height = 0.1784722F;
            this.textBox22.Left = 6.480315F;
            this.textBox22.Name = "textBox22";
            this.textBox22.OutputFormat = resources.GetString("textBox22.OutputFormat");
            this.textBox22.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; vertical-align: middle; ddo-char-set: 128";
            this.textBox22.SummaryGroup = "groupHeader1";
            this.textBox22.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox22.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox22.Tag = "";
            this.textBox22.Text = "ITEM12";
            this.textBox22.Top = 0F;
            this.textBox22.Width = 0.5866145F;
            // 
            // line50
            // 
            this.line50.Height = 0.1877953F;
            this.line50.Left = 6.480315F;
            this.line50.LineWeight = 1F;
            this.line50.Name = "line50";
            this.line50.Tag = "";
            this.line50.Top = 0.1854331F;
            this.line50.Width = 0F;
            this.line50.X1 = 6.480315F;
            this.line50.X2 = 6.480315F;
            this.line50.Y1 = 0.1854331F;
            this.line50.Y2 = 0.3732284F;
            // 
            // line51
            // 
            this.line51.Height = 0.1877953F;
            this.line51.Left = 2.66378F;
            this.line51.LineWeight = 1F;
            this.line51.Name = "line51";
            this.line51.Tag = "";
            this.line51.Top = 0.1850394F;
            this.line51.Width = 0F;
            this.line51.X1 = 2.66378F;
            this.line51.X2 = 2.66378F;
            this.line51.Y1 = 0.1850394F;
            this.line51.Y2 = 0.3728347F;
            // 
            // line52
            // 
            this.line52.Height = 0.1877953F;
            this.line52.Left = 1.01811F;
            this.line52.LineWeight = 1F;
            this.line52.Name = "line52";
            this.line52.Tag = "";
            this.line52.Top = 0.1850394F;
            this.line52.Width = 0F;
            this.line52.X1 = 1.01811F;
            this.line52.X2 = 1.01811F;
            this.line52.Y1 = 0.1850394F;
            this.line52.Y2 = 0.3728347F;
            // 
            // line53
            // 
            this.line53.Height = 0.1877953F;
            this.line53.Left = 2.66378F;
            this.line53.LineWeight = 1F;
            this.line53.Name = "line53";
            this.line53.Tag = "";
            this.line53.Top = 0.1850394F;
            this.line53.Width = 0F;
            this.line53.X1 = 2.66378F;
            this.line53.X2 = 2.66378F;
            this.line53.Y1 = 0.1850394F;
            this.line53.Y2 = 0.3728347F;
            // 
            // line54
            // 
            this.line54.Height = 0F;
            this.line54.Left = 0F;
            this.line54.LineWeight = 1F;
            this.line54.Name = "line54";
            this.line54.Tag = "";
            this.line54.Top = 0.3728347F;
            this.line54.Width = 11.53464F;
            this.line54.X1 = 0F;
            this.line54.X2 = 11.53464F;
            this.line54.Y1 = 0.3728347F;
            this.line54.Y2 = 0.3728347F;
            // 
            // KBDR1022R
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0F;
            this.PageSettings.Margins.Left = 0.1568504F;
            this.PageSettings.Margins.Right = 0.1968504F;
            this.PageSettings.Margins.Top = 0.1968504F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
            this.PageSettings.PaperHeight = 11.69291F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.267716F;
            this.PrintWidth = 11.50001F;
            this.Sections.Add(this.reportHeader1);
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.groupHeader1);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.groupFooter1);
            this.Sections.Add(this.pageFooter);
            this.Sections.Add(this.reportFooter1);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.txtGrpPages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト188)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル71)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト307)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト316)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト317)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル321)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル322)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル323)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル324)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル325)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル326)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル327)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル328)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル329)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル330)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportInfo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.judgeNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト72)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト253)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト308)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGrpPages;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線68;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト188;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル71;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト307;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト316;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト317;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader groupHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter groupFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.ReportHeader reportHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.ReportFooter reportFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル321;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル322;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル323;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル324;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル325;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル326;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル327;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル328;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル329;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル330;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト32;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト33;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト35;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト36;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト37;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト38;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト39;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト40;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト42;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト43;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト72;
        private GrapeCity.ActiveReports.SectionReportModel.Line detailLine;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線91;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線92;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線93;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線94;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線95;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線96;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線97;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線98;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線99;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線100;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線102;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト253;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト308;
        private GrapeCity.ActiveReports.SectionReportModel.Line lastLine;
        private GrapeCity.ActiveReports.SectionReportModel.ReportInfo reportInfo1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox judgeNo;
        private GrapeCity.ActiveReports.SectionReportModel.Label label3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox1;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox2;
        private GrapeCity.ActiveReports.SectionReportModel.Line line2;
        private GrapeCity.ActiveReports.SectionReportModel.Line line3;
        private GrapeCity.ActiveReports.SectionReportModel.Label label5;
        private GrapeCity.ActiveReports.SectionReportModel.Line line4;
        private GrapeCity.ActiveReports.SectionReportModel.Line line5;
        private GrapeCity.ActiveReports.SectionReportModel.Line line22;
        private GrapeCity.ActiveReports.SectionReportModel.Line line6;
        private GrapeCity.ActiveReports.SectionReportModel.Line line8;
        private GrapeCity.ActiveReports.SectionReportModel.Line line9;
        private GrapeCity.ActiveReports.SectionReportModel.Line line10;
        private GrapeCity.ActiveReports.SectionReportModel.Line line11;
        private GrapeCity.ActiveReports.SectionReportModel.Line line12;
        private GrapeCity.ActiveReports.SectionReportModel.Line line13;
        private GrapeCity.ActiveReports.SectionReportModel.Line line14;
        private GrapeCity.ActiveReports.SectionReportModel.Line line15;
        private GrapeCity.ActiveReports.SectionReportModel.Line line16;
        private GrapeCity.ActiveReports.SectionReportModel.Line line17;
        private GrapeCity.ActiveReports.SectionReportModel.Line line18;
        private GrapeCity.ActiveReports.SectionReportModel.Line line19;
        private GrapeCity.ActiveReports.SectionReportModel.Line line20;
        private GrapeCity.ActiveReports.SectionReportModel.Line line21;
        private GrapeCity.ActiveReports.SectionReportModel.Line line7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox8;
        private GrapeCity.ActiveReports.SectionReportModel.Line line23;
        private GrapeCity.ActiveReports.SectionReportModel.Line line24;
        private GrapeCity.ActiveReports.SectionReportModel.Line line25;
        private GrapeCity.ActiveReports.SectionReportModel.Line line26;
        private GrapeCity.ActiveReports.SectionReportModel.Line line27;
        private GrapeCity.ActiveReports.SectionReportModel.Line line28;
        private GrapeCity.ActiveReports.SectionReportModel.Line line29;
        private GrapeCity.ActiveReports.SectionReportModel.Line line30;
        private GrapeCity.ActiveReports.SectionReportModel.Line line31;
        private GrapeCity.ActiveReports.SectionReportModel.Line line32;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox10;
        private GrapeCity.ActiveReports.SectionReportModel.Line line33;
        private GrapeCity.ActiveReports.SectionReportModel.Line line34;
        private GrapeCity.ActiveReports.SectionReportModel.Line line35;
        private GrapeCity.ActiveReports.SectionReportModel.Line line36;
        private GrapeCity.ActiveReports.SectionReportModel.Line line37;
        private GrapeCity.ActiveReports.SectionReportModel.Line line38;
        private GrapeCity.ActiveReports.SectionReportModel.Line line39;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox16;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox17;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox18;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox19;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox20;
        private GrapeCity.ActiveReports.SectionReportModel.Line line40;
        private GrapeCity.ActiveReports.SectionReportModel.Line line41;
        private GrapeCity.ActiveReports.SectionReportModel.Line line42;
        private GrapeCity.ActiveReports.SectionReportModel.Line line43;
        private GrapeCity.ActiveReports.SectionReportModel.Line line44;
        private GrapeCity.ActiveReports.SectionReportModel.Line line45;
        private GrapeCity.ActiveReports.SectionReportModel.Line line46;
        private GrapeCity.ActiveReports.SectionReportModel.Line line47;
        private GrapeCity.ActiveReports.SectionReportModel.Line line48;
        private GrapeCity.ActiveReports.SectionReportModel.Line line49;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox22;
        private GrapeCity.ActiveReports.SectionReportModel.Line line50;
        private GrapeCity.ActiveReports.SectionReportModel.Line line51;
        private GrapeCity.ActiveReports.SectionReportModel.Line line52;
        private GrapeCity.ActiveReports.SectionReportModel.Line line53;
        private GrapeCity.ActiveReports.SectionReportModel.Line line54;
    }
}
