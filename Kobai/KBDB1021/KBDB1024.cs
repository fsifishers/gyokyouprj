﻿using System.Data;
using System.Drawing;
using System.Text;

using systembase.table;

using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.kb.kbdb1021
{
    /// <summary>
    /// 仕訳データ作成 伝票表示(KBDB1024)
    /// </summary>
    public partial class KBDB1024 : BasePgForm
    {
        #region 構造体
        /// <summary>
        /// 合計情報
        /// </summary>
        private struct Summary
        {
            public decimal kariAmount;
            public decimal kariZei;
            public decimal kashiAmount;
            public decimal kashiZei;

            /// <summary>
            /// 金額をクリア
            /// </summary>
            public void Clear()
            {
                kariAmount = 0;
                kariZei = 0;
                kashiAmount = 0;
                kashiZei = 0;
            }
        }

        //// 全レコードの合計を保持する変数
        //Summary _sumTotalInfo = new Summary();
        #endregion

        #region private変数
        /// <summary>
        /// KBDB1021(条件画面)のオブジェクト(消費税設定、自動仕訳設定の取得のため)
        /// </summary>
        KBDB1021 _pForm1;

        /// <summary>
        /// KBDB1023(自動仕訳データ一覧)のオブジェクト(仕訳対象データの取得のため)
        /// </summary>
        KBDB1023 _pForm3;

        /// <summary>
        /// 現在表示しているDataTableのインデックス
        /// </summary>
        int _curDataIdx;
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KBDB1024(KBDB1021 frm1, KBDB1023 frm3)
        {
            this._pForm1 = frm1;
            this._pForm3 = frm3;

            InitializeComponent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 最初に表示するデータのインデックスを設定
            this._curDataIdx = 0;

            // デザインを作成
            InitDetailArea();

            // データを表示
            DispData();

            // 伝票日付を表示
            string[] aryDpyDt = Util.ConvJpDate(Util.ToDate(this._pForm1.Condition["SwkDpyDt"]), this.Dba);
            this.txtDpyDtGengo.Text = aryDpyDt[0];
            this.txtDpyDtJpYear.Text = aryDpyDt[2];
            this.txtDpyDtMonth.Text = aryDpyDt[3];
            this.txtDpyDtDay.Text = aryDpyDt[4];

            // 前伝票・次伝票の使用可否制御
            ControlPrevNext();
        }

        /// <summary>
        /// F7キー押下時処理
        /// </summary>
        public override void PressF7()
        {
            if (!this.btnF7.Enabled) return;

            // 前伝票を表示
            this._curDataIdx--;
            ControlPrevNext();
            DispData();
        }

        /// <summary>
        /// F8キー押下時処理
        /// </summary>
        public override void PressF8()
        {
            if (!this.btnF8.Enabled) return;

            // 次伝票を表示
            this._curDataIdx++;
            ControlPrevNext();
            DispData();
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 明細部の初期化
        /// </summary>
        private void InitDetailArea()
        {
            UTable.CRecordProvider rp = new UTable.CRecordProvider();
            CLayoutBuilder lb = new CLayoutBuilder(CLayoutBuilder.EOrientation.ROW);
            UTable.CFieldDesc fd;
            lb.Ascend();
            fd = rp.AddField("借方勘定科目コード", new CTextFieldProvider("科目"), lb.Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);
            fd = rp.AddField("借方補助科目コード", new CTextFieldProvider("補助"), lb.Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);
            fd = rp.AddField("借方部門コード", new CTextFieldProvider("部門"), lb.Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);
            fd = rp.AddField("借方税区分", new CTextFieldProvider("税"), lb.Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);
            fd = rp.AddField("借方事業区分", new CTextFieldProvider("業"), lb.Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);
            lb.Descend();

            fd = rp.AddField("借方科目名", new CTextFieldProvider("借　方　科　目"), lb.Next(1, 5));
            fd.Setting.HorizontalAlignment = UTable.EHAlign.LEFT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);

            lb.Break(5);

            fd = rp.AddField("借方金額", new CTextFieldProvider("金　額"), lb.Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);
            fd = rp.AddField("借方消費税", new CTextFieldProvider("消費税"), lb.Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);

            lb.Break();

            fd = rp.AddField("未設定", new CTextFieldProvider(), lb.Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.LEFT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);
            fd = rp.AddField("摘要", new CTextFieldProvider("摘　　要"), lb.Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.LEFT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);

            lb.Break();

            lb.Ascend();
            fd = rp.AddField("貸方勘定科目コード", new CTextFieldProvider("科目"), lb.Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);
            fd = rp.AddField("貸方補助科目コード", new CTextFieldProvider("補助"), lb.Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);
            fd = rp.AddField("貸方部門コード", new CTextFieldProvider("部門"), lb.Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);
            fd = rp.AddField("貸方税区分", new CTextFieldProvider("税"), lb.Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);
            fd = rp.AddField("貸方事業区分", new CTextFieldProvider("業"), lb.Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);
            lb.Descend();

            fd = rp.AddField("貸方科目名", new CTextFieldProvider("貸　方　科　目"), lb.Next(1, 5));
            fd.Setting.HorizontalAlignment = UTable.EHAlign.LEFT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);

            lb.Break(5);

            fd = rp.AddField("貸方金額", new CTextFieldProvider("金　額"), lb.Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);
            fd = rp.AddField("貸方消費税", new CTextFieldProvider("消費税"), lb.Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);

            this.mtbList.Content.SetRecordProvider(rp);
            this.mtbList.CreateCaption(UTable.EHAlign.MIDDLE);
            this.mtbList.Cols.SetSize(36, 36, 36, 20, 20, 100, 160, 36, 36, 36, 20, 20, 100);
            this.mtbList.Setting.ContentBackColor = SystemColors.AppWorkspace;
        }

        /// <summary>
        /// 仕訳データを表示
        /// </summary>
        private void DispData()
        {
            // 現在のインデックスのデータを表示
            DataTable dtTarget = this._pForm1.TaishakuData.Tables[this._curDataIdx];

            // 対象行のデータを特定するためのDataRow配列
            DataRow[] aryRow;

            // 条件句を格納する文字列バッファ
            StringBuilder srcCond = new StringBuilder();

            // 科目名を格納するためのテンポラリ変数
            string tmpKamokuNm = string.Empty;

            // 最終の行番号を取得
            int maxRowNo = Util.ToInt(dtTarget.Rows[dtTarget.Rows.Count - 1]["GYO_BANGO"]);

            // まず今表示されている情報はクリア
            this.mtbList.Content.ClearRecord();

            UTable.CRecord rec;
            Summary sumInfo = new Summary();

            // 行表示フラグ
            int flg1 = 0;
            int flg2 = 0;
            for (int i = 1; i <= maxRowNo; i++)
            {
                rec = this.mtbList.Content.AddRecord();

                // 借方の表示
                srcCond = new StringBuilder();
                srcCond.Append("GYO_BANGO = " + Util.ToString(i));
                srcCond.Append(" AND TAISHAKU_KUBUN = 1");
                srcCond.Append(" AND MEISAI_KUBUN = 0");
                aryRow = dtTarget.Select(srcCond.ToString());
                if (aryRow.Length > 0)
                {
                    rec.Fields["借方勘定科目コード"].Value = aryRow[0]["KANJO_KAMOKU_CD"];
                    rec.Fields["借方補助科目コード"].Value = aryRow[0]["HOJO_KAMOKU_CD"];
                    rec.Fields["借方部門コード"].Value = aryRow[0]["BUMON_CD"];
                    rec.Fields["借方税区分"].Value = aryRow[0]["ZEI_KUBUN"];
                    rec.Fields["借方事業区分"].Value = aryRow[0]["JIGYO_KUBUN"];
                    tmpKamokuNm = Util.ToString(aryRow[0]["KANJO_KAMOKU_NM"]);
                    if (!ValChk.IsEmpty(aryRow[0]["HOJO_KAMOKU_NM"]))
                    {
                        tmpKamokuNm += " " + Util.ToString(aryRow[0]["HOJO_KAMOKU_NM"]);
                    }
                    rec.Fields["借方科目名"].Value = tmpKamokuNm;
                    rec.Fields["借方金額"].Value = Util.FormatNum(Util.ToDecimal(aryRow[0]["ZEIKOMI_KINGAKU"]));
                    sumInfo.kariAmount += Util.ToDecimal(aryRow[0]["ZEIKOMI_KINGAKU"]);
                    if (Util.ToInt(aryRow[0]["KAZEI_KUBUN"]) == 1 && Util.ToDecimal(aryRow[0]["SHOHIZEI_KINGAKU"]) > 0)
                    {
                        rec.Fields["借方消費税"].Value = Util.FormatNum(Util.ToDecimal(aryRow[0]["SHOHIZEI_KINGAKU"]));
                        sumInfo.kariZei += Util.ToDecimal(aryRow[0]["SHOHIZEI_KINGAKU"]);
                    }
                    else
                    {
                        rec.Fields["借方消費税"].Value = string.Empty;
                    }
                    rec.Fields["摘要"].Value = Util.ToString(aryRow[0]["TEKIYO"]);
                }
                else
                {
                    flg1 = 1;
                }

                // 貸方の表示
                srcCond = new StringBuilder();
                srcCond.Append("GYO_BANGO = " + Util.ToString(i));
                srcCond.Append(" AND TAISHAKU_KUBUN = 2");
                srcCond.Append(" AND MEISAI_KUBUN = 0");
                aryRow = dtTarget.Select(srcCond.ToString());
                if (aryRow.Length > 0)
                {
                    rec.Fields["貸方勘定科目コード"].Value = aryRow[0]["KANJO_KAMOKU_CD"];
                    rec.Fields["貸方補助科目コード"].Value = aryRow[0]["HOJO_KAMOKU_CD"];
                    rec.Fields["貸方部門コード"].Value = aryRow[0]["BUMON_CD"];
                    rec.Fields["貸方税区分"].Value = aryRow[0]["ZEI_KUBUN"];
                    rec.Fields["貸方事業区分"].Value = aryRow[0]["JIGYO_KUBUN"];
                    tmpKamokuNm = Util.ToString(aryRow[0]["KANJO_KAMOKU_NM"]);
                    if (!ValChk.IsEmpty(aryRow[0]["HOJO_KAMOKU_NM"]))
                    {
                        tmpKamokuNm += " " + Util.ToString(aryRow[0]["HOJO_KAMOKU_NM"]);
                    }
                    rec.Fields["貸方科目名"].Value = tmpKamokuNm;
                    rec.Fields["貸方金額"].Value = Util.FormatNum(Util.ToDecimal(aryRow[0]["ZEIKOMI_KINGAKU"]));
                    sumInfo.kashiAmount += Util.ToDecimal(aryRow[0]["ZEIKOMI_KINGAKU"]);
                    if (Util.ToInt(aryRow[0]["KAZEI_KUBUN"]) == 1 && Util.ToDecimal(aryRow[0]["SHOHIZEI_KINGAKU"]) > 0)
                    {
                        rec.Fields["貸方消費税"].Value = Util.FormatNum(Util.ToDecimal(aryRow[0]["SHOHIZEI_KINGAKU"]));
                        sumInfo.kashiZei += Util.ToDecimal(aryRow[0]["SHOHIZEI_KINGAKU"]);
                    }
                    else
                    {
                        rec.Fields["貸方消費税"].Value = string.Empty;
                    }
                    rec.Fields["摘要"].Value = Util.ToString(aryRow[0]["TEKIYO"]);
                }
                else
                {
                    flg2 = 1;
                }
                if (flg1 == 1 && flg2 == 1)
                {
                    this.mtbList.Content.LastAddedRecord.Visible = false;
                }
                flg1 = 0;
                flg2 = 0;
            }

            // 合計表示
            this.lblKariAmount.Text = Util.FormatNum(sumInfo.kariAmount);
            this.lblKariZei.Text = Util.FormatNum(sumInfo.kariZei);
            this.lblKashiAmount.Text = Util.FormatNum(sumInfo.kashiAmount);
            this.lblKashiZei.Text = Util.FormatNum(sumInfo.kashiZei);
        }

        /// <summary>
        /// 前伝票・次伝票を使用可否を判断する
        /// </summary>
        private void ControlPrevNext()
        {
            if (this._pForm1.TaishakuData.Tables.Count > 1)
            {
                if (this._curDataIdx == 0)
                {
                    // 前伝票はない
                    this.btnF7.Enabled = false;
                    this.btnF8.Enabled = true;
                }
                else if (this._curDataIdx == (this._pForm1.TaishakuData.Tables.Count - 1))
                {
                    // 次伝票はない
                    this.btnF7.Enabled = true;
                    this.btnF8.Enabled = false;
                }
                else
                {
                    // 前伝票・次伝票ともに存在する
                    this.btnF7.Enabled = true;
                    this.btnF8.Enabled = true;
                }
            }
            else
            {
                this.btnF7.Enabled = false;
                this.btnF8.Enabled = false;
            }
        }
        #endregion
    }
}
