﻿using System;
using System.Collections;
using System.Data;
using System.Text;

using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.userinfo;
using jp.co.fsi.common.util;

namespace jp.co.fsi.kb.kbdb1021
{
    /// <summary>
    /// モジュール全体で使用するデータアクセスクラスです。
    /// </summary>
    public class KBDB1021DA
    {
        #region private変数
        /// <summary>
        /// ユーザー情報
        /// </summary>
        UserInfo _uInfo;

        /// <summary>
        /// データアクセスオブジェクト
        /// </summary>
        DbAccess _dba;

        /// <summary>
        /// 設定ファイルアクセスオブジェクト
        /// </summary>
        ConfigLoader _config;

        /// <summary>
        /// 支所コード
        /// </summary>
        int ShishoCode;
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="uInfo">操作中のユーザーの情報</param>
        /// <param name="dba">呼び出し元で保持するデータアクセスオブジェクト</param>
        /// <param name="config">呼び出し元で保持する設定ファイルアクセスオブジェクト</param>
        public KBDB1021DA(UserInfo uInfo, DbAccess dba, ConfigLoader config)
        {
            this._uInfo = uInfo;
            this._dba = dba;
            this._config = config;

            this.ShishoCode = Util.ToInt(this._uInfo.ShishoCd);
        }
        #endregion

        #region publicメソッド
        /// <summary>
        /// 仕訳対象データを取得
        /// </summary>
        /// <param name="mode">処理モード(1:新規、2:更新)</param>
        /// <param name="condition">条件画面の入力値</param>
        /// <returns>仕訳対象データ</returns>
        public DataTable GetSwkTgtData(int mode, Hashtable condition)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT ");
            sql.Append("  B.TOKUISAKI_CD          AS 会員番号 ");
            sql.Append(" ,B.TORIHIKI_KUBUN1       AS 取引区分１ ");
            sql.Append(" ,B.TORIHIKI_KUBUN2       AS 取引区分２ ");
            sql.Append(" ,MIN(B.TOKUISAKI_NM)     AS 会員名称 ");
            sql.Append(" ,MIN(A.SHOHIZEI_NYURYOKU_HOHO) AS 消費税入力方法 ");
            sql.Append(" ,MIN(C.SHOHIZEI_TENKA_HOHO) AS 消費税転嫁方法 ");
            sql.Append(" ,MIN(C.SHOHIZEI_HASU_SHORI) AS 消費税端数処理 ");
            sql.Append(" ,B.TANTOSHA_CD           AS 担当者コード ");
            sql.Append(" ,A.BUMON_CD              AS 部門コード ");
            sql.Append(" ,A.SHIWAKE_CD            AS 仕訳コード ");
            sql.Append(" ,A.ZEI_KUBUN             AS 税区分 ");
            sql.Append(" ,A.JIGYO_KUBUN           AS 事業区分 ");

            sql.Append(" ,A.ZEI_RITSU             AS 税率 ");

            sql.Append(" ,SUM(CASE WHEN C.SHOHIZEI_TENKA_HOHO = 3 THEN ");
            sql.Append("                CASE WHEN A.SHOHIZEI_NYURYOKU_HOHO = 1 THEN A.BAIKA_KINGAKU ");
            sql.Append("                     ELSE CASE WHEN B.TORIHIKI_KUBUN1 = 2 THEN A.BAIKA_KINGAKU ");
            sql.Append("                               ELSE 0 ");
            sql.Append("                          END ");
            sql.Append("                END ");
            sql.Append("           ELSE CASE WHEN A.SHOHIZEI_NYURYOKU_HOHO = 3 THEN (A.BAIKA_KINGAKU - A.SHOHIZEI) ");
            sql.Append("                     ELSE A.BAIKA_KINGAKU ");
            sql.Append("                END ");
            sql.Append("      END) AS 売上金額 ");
            sql.Append(" ,SUM(CASE WHEN C.SHOHIZEI_TENKA_HOHO = 3 THEN ");
            sql.Append("                CASE WHEN B.TORIHIKI_KUBUN1 = 2 THEN A.SHOHIZEI ");
            sql.Append("                     ELSE 0 ");
            sql.Append("                END ");
            sql.Append("           ELSE A.SHOHIZEI ");
            sql.Append("      END) AS 消費税 ");
            sql.Append(" ,SUM(CASE WHEN C.SHOHIZEI_TENKA_HOHO = 3 THEN ");
            sql.Append("                CASE WHEN A.SHOHIZEI_NYURYOKU_HOHO = 2 THEN ");
            sql.Append("                          CASE WHEN B.TORIHIKI_KUBUN1 = 2 THEN 0 ");
            sql.Append("                               ELSE A.BAIKA_KINGAKU ");
            sql.Append("                          END ");
            sql.Append("                     ELSE 0 ");
            sql.Append("                END ");
            sql.Append("           ELSE 0 ");
            sql.Append("      END) AS 外税売上金額 ");
            sql.Append(" ,SUM(CASE WHEN C.SHOHIZEI_TENKA_HOHO = 3 THEN ");
            sql.Append("                CASE WHEN A.SHOHIZEI_NYURYOKU_HOHO = 3 THEN ");
            sql.Append("                          CASE WHEN B.TORIHIKI_KUBUN1 = 2 THEN 0 ");
            sql.Append("                               ELSE A.BAIKA_KINGAKU ");
            sql.Append("                          END ");
            sql.Append("                     ELSE 0 ");
            sql.Append("                END ");
            sql.Append("           ELSE 0 ");
            sql.Append("      END) AS 内税売上金額 ");
            sql.Append("FROM ");
            sql.Append("    TB_HN_TORIHIKI_MEISAI AS A ");
            sql.Append("LEFT OUTER JOIN TB_HN_TORIHIKI_DENPYO AS B ");
            sql.Append("ON A.KAISHA_CD = B.KAISHA_CD ");
            sql.Append("AND A.SHISHO_CD = B.SHISHO_CD ");
            sql.Append("AND A.KAIKEI_NENDO = B.KAIKEI_NENDO ");
            sql.Append("AND A.DENPYO_KUBUN = B.DENPYO_KUBUN ");
            sql.Append("AND A.DENPYO_BANGO = B.DENPYO_BANGO ");
            sql.Append("LEFT OUTER JOIN TB_CM_TORIHIKISAKI AS C ");
            sql.Append("ON B.KAISHA_CD = C.KAISHA_CD ");
            sql.Append("AND B.TOKUISAKI_CD = C.TORIHIKISAKI_CD ");
            sql.Append("WHERE ");
            sql.Append("    A.KAISHA_CD = @KAISHA_CD ");
            sql.Append("AND A.SHISHO_CD = @SHISHO_CD ");
            sql.Append("AND A.KAIKEI_NENDO = @KAIKEI_NENDO ");
            sql.Append("AND A.DENPYO_KUBUN = 2 ");
            sql.Append("AND B.DENPYO_DATE BETWEEN @DENPYO_DATE_FR AND @DENPYO_DATE_TO ");
            sql.Append("AND B.TOKUISAKI_CD BETWEEN @KAIIN_BANGO_FR AND @KAIIN_BANGO_TO ");
            if (mode == 1)
            {
                sql.Append("AND ISNULL(B.IKKATSU_DENPYO_BANGO, 0) = 0 ");
            }
            if ((KBDB1021.SKbn)condition["SakuseiKbn"] == KBDB1021.SKbn.Shimebi)
            {
                sql.Append("AND C.SIMEBI = @SHIMEBI ");
            }
            sql.Append(CreateWhereByToriKbn(condition));
            sql.Append("AND A.BAIKA_KINGAKU <> 0 ");
            sql.Append("AND B.SHIWAKE_DENPYO_BANGO = 0 ");
            sql.Append("AND A.SHIWAKE_CD <> 0 ");
            // TODO:現金取引を除外↓
            // sql.Append("AND B.TORIHIKI_KUBUN1 <> 2 ");
            // TODO:現金取引を除外↑
            sql.Append("GROUP BY ");
            sql.Append("  B.TOKUISAKI_CD ");
            sql.Append(" ,B.TORIHIKI_KUBUN1 ");
            sql.Append(" ,B.TORIHIKI_KUBUN2 ");
            sql.Append(" ,B.TANTOSHA_CD ");
            sql.Append(" ,A.BUMON_CD ");
            sql.Append(" ,A.SHIWAKE_CD ");
            sql.Append(" ,A.ZEI_KUBUN ");
            sql.Append(" ,A.JIGYO_KUBUN ");
            sql.Append(" ,A.ZEI_RITSU ");
            sql.Append("ORDER BY ");
            sql.Append("  B.TOKUISAKI_CD ");
            sql.Append(" ,B.TORIHIKI_KUBUN1 ");
            sql.Append(" ,B.TORIHIKI_KUBUN2 ");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
            dpc.SetParam("@DENPYO_DATE_FR", SqlDbType.DateTime, Util.ToDate(condition["DpyDtFr"]));
            dpc.SetParam("@DENPYO_DATE_TO", SqlDbType.DateTime, Util.ToDate(condition["DpyDtTo"]));
            dpc.SetParam("@KAIIN_BANGO_FR", SqlDbType.VarChar, 4, Util.ToString(condition["ShiharaiCdFr"]));
            dpc.SetParam("@KAIIN_BANGO_TO", SqlDbType.VarChar, 4, Util.ToString(condition["ShiharaiCdTo"]));
            if ((KBDB1021.SKbn)condition["SakuseiKbn"] == KBDB1021.SKbn.Shimebi)
            {
                dpc.SetParam("@SHIMEBI", SqlDbType.Decimal, 2, Util.ToInt(condition["Shimebi"]));
            }

            DataTable dtResult = this._dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            return dtResult;
        }

        /// <summary>
        /// 貸借を仕訳したデータを取得
        /// </summary>
        /// <param name="condition">条件画面の入力値</param>
        /// <param name="swkTgtData">仕訳対象データ</param>
        /// <returns>貸借を仕訳したデータ(1伝票あたり1DataTable)</returns>
        public DataSet GetTaishakuData(Hashtable condition, DataTable zeiSetting, DataTable jdSwkSettingA,
            DataTable jdSwkSettingB, DataTable swkTgtData)
        {
            DataSet dsTaishakuData = new DataSet();
            DataTable dtTaishakuData;
            DataRow drTaishakuData;
            DataTable dtCommit;
            DataRow[] aryFormattedData;
            DataTable dtFormattedData;

            StringBuilder srcCond = new StringBuilder();
            DataRow[] aryAiteRow;
            DataRow[] aryAllRow;
            DataRow[] aryTaishakuRow;
            DataRow[] arySortedRow;
            DataRow[] aryDupRow;
            int prevRowNo;

            DataRow drSwkStgA;
            DataRow drSwkStgB;
            DataRow drZeiSetting;

            // 暫定（請求転嫁の消費税算出）
            decimal wZeiRt = 0;
            decimal wKingk = 0;
            decimal wZeigk = 0;

            bool kariKazeiKbn = false;
            bool kashiKazeiKbn = false;
            int rowNo = 0;
            string prevKaiinNo = string.Empty;

            // 仕訳伝票の作成方法によって処理を切り替える
            int swkDpMk = Util.ToInt(this._config.LoadPgConfig(Constants.SubSys.Kob, "KBDB1021", "Setting", "SwkDpMk"));

            // 基本情報
            int swkBmnCd = Util.ToInt(this._config.LoadPgConfig(Constants.SubSys.Kob, "KBDB1021", "Setting", "BumonCd"));
            DataRow BaseDataRow = this._dba.GetKaikeiSettingsByKessanKi(this._uInfo.KaishaCd, this._uInfo.KessanKi).Rows[0];
            int swkJgyKb = Util.ToInt(BaseDataRow["JIGYO_KUBUN"]);
            int swkZeiNh = Util.ToInt(BaseDataRow["SHOHIZEI_NYURYOKU_HOHO"]);
            // 伝票区分（仕訳作成区分に設定）
            int denpyoKbn = 2;

            // 更新する際のレイアウトをベースに定義を作成
            dtTaishakuData = new DataTable();

            dtTaishakuData.Columns.Add("GYO_BANGO", typeof(decimal));
            dtTaishakuData.Columns.Add("SHIHARAISAKI_CD", typeof(decimal));
            dtTaishakuData.Columns.Add("TAISHAKU_KUBUN", typeof(decimal));
            dtTaishakuData.Columns.Add("MEISAI_KUBUN", typeof(decimal));
            dtTaishakuData.Columns.Add("DENPYO_KUBUN", typeof(decimal));
            dtTaishakuData.Columns.Add("DENPYO_DATE", typeof(DateTime));
            dtTaishakuData.Columns.Add("KANJO_KAMOKU_CD", typeof(decimal));
            dtTaishakuData.Columns.Add("KANJO_KAMOKU_NM", typeof(string));
            dtTaishakuData.Columns.Add("HOJO_KAMOKU_CD", typeof(decimal));
            dtTaishakuData.Columns.Add("HOJO_KAMOKU_NM", typeof(string));
            dtTaishakuData.Columns.Add("BUMON_CD", typeof(decimal));
            dtTaishakuData.Columns.Add("TEKIYO_CD", typeof(decimal));
            dtTaishakuData.Columns.Add("TEKIYO", typeof(string));
            dtTaishakuData.Columns.Add("ZEIKOMI_KINGAKU", typeof(decimal));
            dtTaishakuData.Columns.Add("ZEINUKI_KINGAKU", typeof(decimal));
            dtTaishakuData.Columns.Add("SHOHIZEI_KINGAKU", typeof(decimal));
            dtTaishakuData.Columns.Add("ZEI_KUBUN", typeof(decimal));
            dtTaishakuData.Columns.Add("KAZEI_KUBUN", typeof(decimal));
            dtTaishakuData.Columns.Add("TORIHIKI_KUBUN", typeof(decimal));
            dtTaishakuData.Columns.Add("ZEI_RITSU", typeof(decimal));
            dtTaishakuData.Columns.Add("JIGYO_KUBUN", typeof(decimal));
            dtTaishakuData.Columns.Add("SHOHIZEI_NYURYOKU_HOHO", typeof(decimal));
            dtTaishakuData.Columns.Add("SHOHIZEI_HENKO", typeof(decimal));
            dtTaishakuData.Columns.Add("KESSAN_KUBUN", typeof(decimal));
            dtTaishakuData.Columns.Add("SHIWAKE_SAKUSEI_KUBUN", typeof(decimal));
            dtTaishakuData.Columns.Add("CHK_ZUMI", typeof(decimal));
            dtTaishakuData.Columns.Add("DEL_FLG", typeof(decimal));

            // とりあえず1行ずつ貸借及び課税区分によって起票
            for (int i = 0; i < swkTgtData.Rows.Count; i++)
            {
                // 支払先別に伝票を起票する場合のブレイク処理
                if (swkDpMk == 3 && (!ValChk.IsEmpty(prevKaiinNo) && !Util.ToString(swkTgtData.Rows[i]["会員番号"]).Equals(prevKaiinNo)))
                {
                    #region 古いコード（コメントアウト）ネストがおかしい
                    //// 仕入に対する返品を消し込む
                    //for (int j = 0; j < dtTaishakuData.Rows.Count; j++)
                    //{
                    //    // 削除フラグが立っている行は評価対象外
                    //    if (Util.ToInt(dtTaishakuData.Rows[j]["DEL_FLG"]) == 1)
                    //    {
                    //        continue;
                    //    }

                    //    // 同一の支払先コード・明細区分・勘定科目コード・補助科目コード・部門コード・税区分・事業区分で
                    //    // 貸借逆のデータを取得する
                    //    // ※チェック済みのデータは参照しない
                    //    srcCond = new StringBuilder();
                    //    srcCond.Append("SHIHARAISAKI_CD = " + Util.ToString(dtTaishakuData.Rows[j]["SHIHARAISAKI_CD"]));
                    //    srcCond.Append(" AND MEISAI_KUBUN = " + Util.ToString(dtTaishakuData.Rows[j]["MEISAI_KUBUN"]));
                    //    srcCond.Append(" AND KANJO_KAMOKU_CD = " + Util.ToString(dtTaishakuData.Rows[j]["KANJO_KAMOKU_CD"]));
                    //    srcCond.Append(" AND HOJO_KAMOKU_CD = " + Util.ToString(dtTaishakuData.Rows[j]["HOJO_KAMOKU_CD"]));
                    //    srcCond.Append(" AND BUMON_CD = " + Util.ToString(dtTaishakuData.Rows[j]["BUMON_CD"]));
                    //    srcCond.Append(" AND ZEI_KUBUN = " + Util.ToString(dtTaishakuData.Rows[j]["ZEI_KUBUN"]));
                    //    srcCond.Append(" AND JIGYO_KUBUN = " + Util.ToString(dtTaishakuData.Rows[j]["JIGYO_KUBUN"]));
                    //    srcCond.Append(" AND TAISHAKU_KUBUN <> " + Util.ToString(dtTaishakuData.Rows[j]["TAISHAKU_KUBUN"]));
                    //    srcCond.Append(" AND CHK_ZUMI = 0");
                    //    srcCond.Append(" AND DEL_FLG = 0");
                    //    aryAiteRow = dtTaishakuData.Select(srcCond.ToString());

                    //    for (int k = 0; k < aryAiteRow.Length; k++)
                    //    {
                    //        if (Util.ToDecimal(dtTaishakuData.Rows[j]["ZEINUKI_KINGAKU"]) >
                    //            Util.ToDecimal(aryAiteRow[k]["ZEINUKI_KINGAKU"]))
                    //        {
                    //            // 自分が多ければ相手を消す
                    //            dtTaishakuData.Rows[j]["ZEIKOMI_KINGAKU"] =
                    //                Util.ToDecimal(dtTaishakuData.Rows[j]["ZEIKOMI_KINGAKU"]) -
                    //                Util.ToDecimal(aryAiteRow[k]["ZEIKOMI_KINGAKU"]);
                    //            dtTaishakuData.Rows[j]["ZEINUKI_KINGAKU"] =
                    //                Util.ToDecimal(dtTaishakuData.Rows[j]["ZEINUKI_KINGAKU"]) -
                    //                Util.ToDecimal(aryAiteRow[k]["ZEINUKI_KINGAKU"]);
                    //            dtTaishakuData.Rows[j]["SHOHIZEI_KINGAKU"] =
                    //                Util.ToDecimal(dtTaishakuData.Rows[j]["SHOHIZEI_KINGAKU"]) -
                    //                Util.ToDecimal(aryAiteRow[k]["SHOHIZEI_KINGAKU"]);
                    //            aryAiteRow[k]["DEL_FLG"] = 1;
                    //        }
                    //        else if (Util.ToDecimal(dtTaishakuData.Rows[j]["ZEINUKI_KINGAKU"]) ==
                    //            Util.ToDecimal(aryAiteRow[k]["ZEINUKI_KINGAKU"]))
                    //        {
                    //            // 同一金額なら借方を残して貸方を消す
                    //            if (Util.ToInt(dtTaishakuData.Rows[j]["TAISHAKU_KUBUN"]) == 1)
                    //            {
                    //                dtTaishakuData.Rows[j]["ZEIKOMI_KINGAKU"] =
                    //                    Util.ToDecimal(dtTaishakuData.Rows[j]["ZEIKOMI_KINGAKU"]) -
                    //                    Util.ToDecimal(aryAiteRow[k]["ZEIKOMI_KINGAKU"]);
                    //                dtTaishakuData.Rows[j]["ZEINUKI_KINGAKU"] =
                    //                    Util.ToDecimal(dtTaishakuData.Rows[j]["ZEINUKI_KINGAKU"]) -
                    //                    Util.ToDecimal(aryAiteRow[k]["ZEINUKI_KINGAKU"]);
                    //                dtTaishakuData.Rows[j]["SHOHIZEI_KINGAKU"] =
                    //                    Util.ToDecimal(dtTaishakuData.Rows[j]["SHOHIZEI_KINGAKU"]) -
                    //                    Util.ToDecimal(aryAiteRow[k]["SHOHIZEI_KINGAKU"]);
                    //                aryAiteRow[k]["DEL_FLG"] = 1;
                    //                // 消費税行なら借方も消す
                    //                if (Util.ToInt(dtTaishakuData.Rows[j]["MEISAI_KUBUN"]) == 1)
                    //                {
                    //                    dtTaishakuData.Rows[j]["DEL_FLG"] = 1;
                    //                }
                    //            }
                    //            else
                    //            {
                    //                aryAiteRow[k]["ZEIKOMI_KINGAKU"] =
                    //                    Util.ToDecimal(aryAiteRow[k]["ZEIKOMI_KINGAKU"]) -
                    //                    Util.ToDecimal(dtTaishakuData.Rows[j]["ZEIKOMI_KINGAKU"]);
                    //                aryAiteRow[k]["ZEINUKI_KINGAKU"] =
                    //                    Util.ToDecimal(aryAiteRow[k]["ZEINUKI_KINGAKU"]) -
                    //                    Util.ToDecimal(dtTaishakuData.Rows[j]["ZEINUKI_KINGAKU"]);
                    //                aryAiteRow[k]["SHOHIZEI_KINGAKU"] =
                    //                    Util.ToDecimal(aryAiteRow[k]["SHOHIZEI_KINGAKU"]) -
                    //                    Util.ToDecimal(dtTaishakuData.Rows[j]["SHOHIZEI_KINGAKU"]);
                    //                dtTaishakuData.Rows[j]["DEL_FLG"] = 1;
                    //                // 消費税行なら借方も消す
                    //                if (Util.ToInt(dtTaishakuData.Rows[j]["MEISAI_KUBUN"]) == 1)
                    //                {
                    //                    aryAiteRow[k]["DEL_FLG"] = 1;
                    //                }
                    //            }
                    //        }
                    //        else
                    //        {
                    //            // 相手が多ければ自分を消す
                    //            aryAiteRow[k]["ZEIKOMI_KINGAKU"] =
                    //                Util.ToDecimal(aryAiteRow[k]["ZEIKOMI_KINGAKU"]) -
                    //                Util.ToDecimal(dtTaishakuData.Rows[j]["ZEIKOMI_KINGAKU"]);
                    //            aryAiteRow[k]["ZEINUKI_KINGAKU"] =
                    //                Util.ToDecimal(aryAiteRow[k]["ZEINUKI_KINGAKU"]) -
                    //                Util.ToDecimal(dtTaishakuData.Rows[j]["ZEINUKI_KINGAKU"]);
                    //            aryAiteRow[k]["SHOHIZEI_KINGAKU"] =
                    //                Util.ToDecimal(aryAiteRow[k]["SHOHIZEI_KINGAKU"]) -
                    //                Util.ToDecimal(dtTaishakuData.Rows[j]["SHOHIZEI_KINGAKU"]);
                    //            dtTaishakuData.Rows[j]["DEL_FLG"] = 1;
                    //        }
                    //    }
                    //    // ここまでの処理で行が消されていなければチェック済みフラグを立てる
                    //    if (Util.ToInt(dtTaishakuData.Rows[j]["DEL_FLG"]) == 0)
                    //    {
                    //        dtTaishakuData.Rows[j]["CHK_ZUMI"] = 1;
                    //    }

                    //    // 複合仕訳でない場合、同一の支払先に対し、貸借の科目単位でまとめ上げる
                    //    arySortedRow = dtTaishakuData.Select(null, "GYO_BANGO, TAISHAKU_KUBUN, MEISAI_KUBUN");


                    //        // 既に削除された行は評価しない
                    //        if (Util.ToInt(arySortedRow[j]["DEL_FLG"]) == 1) continue;

                    //        // 同一の支払先コード・貸借区分・明細区分・勘定科目コード・補助科目コード・部門コード・税区分・事業区分で
                    //        // ※チェック済みのデータは参照しない
                    //        srcCond = new StringBuilder();
                    //        srcCond.Append("SHIHARAISAKI_CD = " + Util.ToString(arySortedRow[j]["SHIHARAISAKI_CD"]));
                    //        srcCond.Append(" AND TAISHAKU_KUBUN = " + Util.ToString(arySortedRow[j]["TAISHAKU_KUBUN"]));
                    //        srcCond.Append(" AND MEISAI_KUBUN = " + Util.ToString(arySortedRow[j]["MEISAI_KUBUN"]));
                    //        srcCond.Append(" AND KANJO_KAMOKU_CD = " + Util.ToString(arySortedRow[j]["KANJO_KAMOKU_CD"]));
                    //        srcCond.Append(" AND HOJO_KAMOKU_CD = " + Util.ToString(arySortedRow[j]["HOJO_KAMOKU_CD"]));
                    //        srcCond.Append(" AND BUMON_CD = " + Util.ToString(arySortedRow[j]["BUMON_CD"]));
                    //        srcCond.Append(" AND ZEI_KUBUN = " + Util.ToString(arySortedRow[j]["ZEI_KUBUN"]));
                    //        srcCond.Append(" AND JIGYO_KUBUN = " + Util.ToString(arySortedRow[j]["JIGYO_KUBUN"]));

                    //        aryDupRow = dtTaishakuData.Select(srcCond.ToString(), "GYO_BANGO, TAISHAKU_KUBUN, MEISAI_KUBUN");

                    //        // 重複した1行目の情報に2,3行目の金額を加算していく
                    //        for (int k = 0; k < aryDupRow.Length; k++)
                    //        {
                    //            // 自身の行は読み飛ばす
                    //            if (k == 0) continue;

                    //            // 既に削除された行は評価しない
                    //            if (Util.ToInt(aryDupRow[k]["DEL_FLG"]) == 1) continue;

                    //            aryDupRow[0]["ZEIKOMI_KINGAKU"] = Util.ToDecimal(aryDupRow[0]["ZEIKOMI_KINGAKU"]) + Util.ToDecimal(aryDupRow[k]["ZEIKOMI_KINGAKU"]);
                    //            aryDupRow[0]["ZEINUKI_KINGAKU"] = Util.ToDecimal(aryDupRow[0]["ZEINUKI_KINGAKU"]) + Util.ToDecimal(aryDupRow[k]["ZEINUKI_KINGAKU"]);
                    //            aryDupRow[0]["SHOHIZEI_KINGAKU"] = Util.ToDecimal(aryDupRow[0]["SHOHIZEI_KINGAKU"]) + Util.ToDecimal(aryDupRow[k]["SHOHIZEI_KINGAKU"]);
                    //            aryDupRow[k]["DEL_FLG"] = 1;
                    //        }
                    //}
                    #endregion

                    // 仕入に対する返品を消し込む
                    for (int j = 0; j < dtTaishakuData.Rows.Count; j++)
                    {
                        // 削除フラグが立っている行は評価対象外
                        if (Util.ToInt(dtTaishakuData.Rows[j]["DEL_FLG"]) == 1)
                        {
                            continue;
                        }

                        // 同一の支払先コード・明細区分・勘定科目コード・補助科目コード・部門コード・税区分・事業区分で
                        // 貸借逆のデータを取得する
                        // ※チェック済みのデータは参照しない
                        srcCond = new StringBuilder();
                        srcCond.Append("SHIHARAISAKI_CD = " + Util.ToString(dtTaishakuData.Rows[j]["SHIHARAISAKI_CD"]));
                        srcCond.Append(" AND MEISAI_KUBUN = " + Util.ToString(dtTaishakuData.Rows[j]["MEISAI_KUBUN"]));
                        srcCond.Append(" AND KANJO_KAMOKU_CD = " + Util.ToString(dtTaishakuData.Rows[j]["KANJO_KAMOKU_CD"]));
                        srcCond.Append(" AND HOJO_KAMOKU_CD = " + Util.ToString(dtTaishakuData.Rows[j]["HOJO_KAMOKU_CD"]));
                        srcCond.Append(" AND BUMON_CD = " + Util.ToString(dtTaishakuData.Rows[j]["BUMON_CD"]));
                        srcCond.Append(" AND ZEI_KUBUN = " + Util.ToString(dtTaishakuData.Rows[j]["ZEI_KUBUN"]));
                        srcCond.Append(" AND JIGYO_KUBUN = " + Util.ToString(dtTaishakuData.Rows[j]["JIGYO_KUBUN"]));

                        // 複数税率対応
                        srcCond.Append(" AND ZEI_RITSU = " + Util.ToString(dtTaishakuData.Rows[j]["ZEI_RITSU"]));

                        srcCond.Append(" AND TAISHAKU_KUBUN <> " + Util.ToString(dtTaishakuData.Rows[j]["TAISHAKU_KUBUN"]));
                        srcCond.Append(" AND CHK_ZUMI = 0");
                        srcCond.Append(" AND DEL_FLG = 0");
                        aryAiteRow = dtTaishakuData.Select(srcCond.ToString());

                        for (int k = 0; k < aryAiteRow.Length; k++)
                        {
                            if (Util.ToDecimal(dtTaishakuData.Rows[j]["ZEINUKI_KINGAKU"]) >
                                Util.ToDecimal(aryAiteRow[k]["ZEINUKI_KINGAKU"]))
                            {
                                // 自分が多ければ相手を消す
                                dtTaishakuData.Rows[j]["ZEIKOMI_KINGAKU"] =
                                    Util.ToDecimal(dtTaishakuData.Rows[j]["ZEIKOMI_KINGAKU"]) -
                                    Util.ToDecimal(aryAiteRow[k]["ZEIKOMI_KINGAKU"]);
                                dtTaishakuData.Rows[j]["ZEINUKI_KINGAKU"] =
                                    Util.ToDecimal(dtTaishakuData.Rows[j]["ZEINUKI_KINGAKU"]) -
                                    Util.ToDecimal(aryAiteRow[k]["ZEINUKI_KINGAKU"]);
                                dtTaishakuData.Rows[j]["SHOHIZEI_KINGAKU"] =
                                    Util.ToDecimal(dtTaishakuData.Rows[j]["SHOHIZEI_KINGAKU"]) -
                                    Util.ToDecimal(aryAiteRow[k]["SHOHIZEI_KINGAKU"]);
                                aryAiteRow[k]["DEL_FLG"] = 1;
                            }
                            else if (Util.ToDecimal(dtTaishakuData.Rows[j]["ZEINUKI_KINGAKU"]) ==
                                Util.ToDecimal(aryAiteRow[k]["ZEINUKI_KINGAKU"]))
                            {
                                // 同一金額なら借方を残して貸方を消す
                                if (Util.ToInt(dtTaishakuData.Rows[j]["TAISHAKU_KUBUN"]) == 1)
                                {
                                    dtTaishakuData.Rows[j]["ZEIKOMI_KINGAKU"] =
                                        Util.ToDecimal(dtTaishakuData.Rows[j]["ZEIKOMI_KINGAKU"]) -
                                        Util.ToDecimal(aryAiteRow[k]["ZEIKOMI_KINGAKU"]);
                                    dtTaishakuData.Rows[j]["ZEINUKI_KINGAKU"] =
                                        Util.ToDecimal(dtTaishakuData.Rows[j]["ZEINUKI_KINGAKU"]) -
                                        Util.ToDecimal(aryAiteRow[k]["ZEINUKI_KINGAKU"]);
                                    dtTaishakuData.Rows[j]["SHOHIZEI_KINGAKU"] =
                                        Util.ToDecimal(dtTaishakuData.Rows[j]["SHOHIZEI_KINGAKU"]) -
                                        Util.ToDecimal(aryAiteRow[k]["SHOHIZEI_KINGAKU"]);
                                    aryAiteRow[k]["DEL_FLG"] = 1;
                                    // 消費税行なら借方も消す
                                    if (Util.ToInt(dtTaishakuData.Rows[j]["MEISAI_KUBUN"]) == 1)
                                    {
                                        dtTaishakuData.Rows[j]["DEL_FLG"] = 1;
                                    }
                                }
                                else
                                {
                                    aryAiteRow[k]["ZEIKOMI_KINGAKU"] =
                                        Util.ToDecimal(aryAiteRow[k]["ZEIKOMI_KINGAKU"]) -
                                        Util.ToDecimal(dtTaishakuData.Rows[j]["ZEIKOMI_KINGAKU"]);
                                    aryAiteRow[k]["ZEINUKI_KINGAKU"] =
                                        Util.ToDecimal(aryAiteRow[k]["ZEINUKI_KINGAKU"]) -
                                        Util.ToDecimal(dtTaishakuData.Rows[j]["ZEINUKI_KINGAKU"]);
                                    aryAiteRow[k]["SHOHIZEI_KINGAKU"] =
                                        Util.ToDecimal(aryAiteRow[k]["SHOHIZEI_KINGAKU"]) -
                                        Util.ToDecimal(dtTaishakuData.Rows[j]["SHOHIZEI_KINGAKU"]);
                                    dtTaishakuData.Rows[j]["DEL_FLG"] = 1;
                                    // 消費税行なら借方も消す
                                    if (Util.ToInt(dtTaishakuData.Rows[j]["MEISAI_KUBUN"]) == 1)
                                    {
                                        aryAiteRow[k]["DEL_FLG"] = 1;
                                    }
                                }
                            }
                            else
                            {
                                // 相手が多ければ自分を消す
                                aryAiteRow[k]["ZEIKOMI_KINGAKU"] =
                                    Util.ToDecimal(aryAiteRow[k]["ZEIKOMI_KINGAKU"]) -
                                    Util.ToDecimal(dtTaishakuData.Rows[j]["ZEIKOMI_KINGAKU"]);
                                aryAiteRow[k]["ZEINUKI_KINGAKU"] =
                                    Util.ToDecimal(aryAiteRow[k]["ZEINUKI_KINGAKU"]) -
                                    Util.ToDecimal(dtTaishakuData.Rows[j]["ZEINUKI_KINGAKU"]);
                                aryAiteRow[k]["SHOHIZEI_KINGAKU"] =
                                    Util.ToDecimal(aryAiteRow[k]["SHOHIZEI_KINGAKU"]) -
                                    Util.ToDecimal(dtTaishakuData.Rows[j]["SHOHIZEI_KINGAKU"]);
                                dtTaishakuData.Rows[j]["DEL_FLG"] = 1;
                            }
                        }
                        // ここまでの処理で行が消されていなければチェック済みフラグを立てる
                        if (Util.ToInt(dtTaishakuData.Rows[j]["DEL_FLG"]) == 0)
                        {
                            dtTaishakuData.Rows[j]["CHK_ZUMI"] = 1;
                        }
                    }

                    // 複合仕訳でない場合、同一の支払先に対し、貸借の科目単位でまとめ上げる
                    arySortedRow = dtTaishakuData.Select(null, "GYO_BANGO, TAISHAKU_KUBUN, MEISAI_KUBUN");
                    for (int j = 0; j < arySortedRow.Length; j++)
                    {
                        // 既に削除された行は評価しない
                        if (Util.ToInt(arySortedRow[j]["DEL_FLG"]) == 1)
                        {
                            continue;
                        }

                        // 同一の支払先コード・貸借区分・明細区分・勘定科目コード・補助科目コード・部門コード・税区分・事業区分で
                        // ※チェック済みのデータは参照しない
                        srcCond = new StringBuilder();
                        srcCond.Append("SHIHARAISAKI_CD = " + Util.ToString(arySortedRow[j]["SHIHARAISAKI_CD"]));
                        srcCond.Append(" AND TAISHAKU_KUBUN = " + Util.ToString(arySortedRow[j]["TAISHAKU_KUBUN"]));
                        srcCond.Append(" AND MEISAI_KUBUN = " + Util.ToString(arySortedRow[j]["MEISAI_KUBUN"]));
                        srcCond.Append(" AND KANJO_KAMOKU_CD = " + Util.ToString(arySortedRow[j]["KANJO_KAMOKU_CD"]));
                        srcCond.Append(" AND HOJO_KAMOKU_CD = " + Util.ToString(arySortedRow[j]["HOJO_KAMOKU_CD"]));
                        srcCond.Append(" AND BUMON_CD = " + Util.ToString(arySortedRow[j]["BUMON_CD"]));
                        srcCond.Append(" AND ZEI_KUBUN = " + Util.ToString(arySortedRow[j]["ZEI_KUBUN"]));
                        srcCond.Append(" AND JIGYO_KUBUN = " + Util.ToString(arySortedRow[j]["JIGYO_KUBUN"]));

                        // 複数税率対応
                        srcCond.Append(" AND ZEI_RITSU = " + Util.ToString(arySortedRow[j]["ZEI_RITSU"]));
                        srcCond.Append(" AND DEL_FLG = 0 ");

                        aryDupRow = dtTaishakuData.Select(srcCond.ToString(), "GYO_BANGO, TAISHAKU_KUBUN, MEISAI_KUBUN");

                        // 重複した1行目の情報に2,3行目の金額を加算していく
                        for (int k = 0; k < aryDupRow.Length; k++)
                        {
                            // 自身の行は読み飛ばす
                            if (k == 0) continue;

                            // 既に削除された行は評価しない
                            if (Util.ToInt(aryDupRow[k]["DEL_FLG"]) == 1) continue;

                            aryDupRow[0]["ZEIKOMI_KINGAKU"] = Util.ToDecimal(aryDupRow[0]["ZEIKOMI_KINGAKU"]) + Util.ToDecimal(aryDupRow[k]["ZEIKOMI_KINGAKU"]);
                            aryDupRow[0]["ZEINUKI_KINGAKU"] = Util.ToDecimal(aryDupRow[0]["ZEINUKI_KINGAKU"]) + Util.ToDecimal(aryDupRow[k]["ZEINUKI_KINGAKU"]);
                            aryDupRow[0]["SHOHIZEI_KINGAKU"] = Util.ToDecimal(aryDupRow[0]["SHOHIZEI_KINGAKU"]) + Util.ToDecimal(aryDupRow[k]["SHOHIZEI_KINGAKU"]);
                            aryDupRow[k]["DEL_FLG"] = 1;
                        }
                    }

                    // 削除フラグが1の行を削除(一旦削除確定用テンポラリテーブルに詰める)
                    dtCommit = dtTaishakuData.Clone();
                    foreach (DataRow row in dtTaishakuData.Rows)
                    {
                        // 作成データの調整
                        //if (Util.ToInt(row["DEL_FLG"]) == 0) dtCommit.ImportRow(row);

                        // 入力行のみを対象にして消費税行を別で追加
                        if (Util.ToInt(row["DEL_FLG"]) == 0 && Util.ToInt(row["MEISAI_KUBUN"]) == 0)
                        {
                            // マイナス時は貸借区分の反転しマイナスを無くして登録
                            if (Util.ToDecimal(row["ZEIKOMI_KINGAKU"]) < 0)
                            {
                                row["ZEIKOMI_KINGAKU"] = Math.Abs(Util.ToDecimal(row["ZEIKOMI_KINGAKU"]));
                                row["ZEINUKI_KINGAKU"] = Math.Abs(Util.ToDecimal(row["ZEINUKI_KINGAKU"]));
                                row["SHOHIZEI_KINGAKU"] = Math.Abs(Util.ToDecimal(row["SHOHIZEI_KINGAKU"]));
                                row["TAISHAKU_KUBUN"] = (Util.ToInt(row["TAISHAKU_KUBUN"]) == 1 ? 2 : 1);
                            }
                            dtCommit.ImportRow(row);

                            // 課税で且つ消費税がある場合に消費税レコードを追加
                            if (Util.ToInt(row["KAZEI_KUBUN"]) == 1 && Util.ToDecimal(row["SHOHIZEI_KINGAKU"]) > 0)
                            {
                                int taxTai = 2;
                                if (Util.ToInt(row["TORIHIKI_KUBUN"]) >= 10 && Util.ToInt(row["TORIHIKI_KUBUN"]) <= 19)
                                {
                                    taxTai = 1;
                                }
                                drZeiSetting = GetZeiSettingRow(zeiSetting, taxTai);
                                DataRow taxRow = dtCommit.NewRow();
                                taxRow["GYO_BANGO"] = row["GYO_BANGO"];
                                taxRow["SHIHARAISAKI_CD"] = row["SHIHARAISAKI_CD"];
                                taxRow["TAISHAKU_KUBUN"] = row["TAISHAKU_KUBUN"];
                                taxRow["MEISAI_KUBUN"] = 1;
                                taxRow["DENPYO_KUBUN"] = 1;
                                taxRow["DENPYO_DATE"] = row["DENPYO_DATE"];
                                taxRow["BUMON_CD"] = (Util.ToInt(drZeiSetting["BUMON_UMU"]) == 0 ? 0 : row["BUMON_CD"]);
                                taxRow["TEKIYO_CD"] = row["TEKIYO_CD"];
                                taxRow["TEKIYO"] = row["TEKIYO"];
                                taxRow["ZEIKOMI_KINGAKU"] = 0;
                                taxRow["ZEINUKI_KINGAKU"] = Util.ToDecimal(row["SHOHIZEI_KINGAKU"]);
                                taxRow["SHOHIZEI_KINGAKU"] = 0;
                                taxRow["SHOHIZEI_NYURYOKU_HOHO"] = swkZeiNh;
                                taxRow["SHOHIZEI_HENKO"] = row["SHOHIZEI_HENKO"];
                                taxRow["KESSAN_KUBUN"] = row["KESSAN_KUBUN"];
                                taxRow["SHIWAKE_SAKUSEI_KUBUN"] = row["SHIWAKE_SAKUSEI_KUBUN"];
                                taxRow["JIGYO_KUBUN"] = row["JIGYO_KUBUN"];
                                taxRow["CHK_ZUMI"] = 0;
                                taxRow["DEL_FLG"] = 0;
                                if (drZeiSetting != null)
                                {
                                    taxRow["KANJO_KAMOKU_CD"] = drZeiSetting["KANJO_KAMOKU_CD"];
                                    taxRow["KANJO_KAMOKU_NM"] = drZeiSetting["KANJO_KAMOKU_NM"];
                                    taxRow["HOJO_KAMOKU_CD"] = (Util.ToInt(Util.ToString(drZeiSetting["HOJO_KAMOKU_UMU"])) == 0 ? 0 : row["HOJO_KAMOKU_CD"]);
                                    taxRow["HOJO_KAMOKU_NM"] = (Util.ToInt(Util.ToString(drZeiSetting["HOJO_KAMOKU_UMU"])) == 0 ? DBNull.Value : row["HOJO_KAMOKU_NM"]);
                                    if (Util.ToInt(Util.ToString(row["TAISHAKU_KUBUN"])) == 1)
                                    {
                                        taxRow["ZEI_KUBUN"] = drZeiSetting["KARIKATA_ZEI_KUBUN"];
                                        taxRow["KAZEI_KUBUN"] = drZeiSetting["KARI_KAZEI_KUBUN"];
                                        taxRow["TORIHIKI_KUBUN"] = drZeiSetting["KARI_TORIHIKI_KUBUN"];
                                        taxRow["ZEI_RITSU"] = drZeiSetting["KARI_ZEI_RITSU"];
                                    }
                                    else
                                    {
                                        taxRow["ZEI_KUBUN"] = drZeiSetting["KASHIKATA_ZEI_KUBUN"];
                                        taxRow["KAZEI_KUBUN"] = drZeiSetting["KASHI_KAZEI_KUBUN"];
                                        taxRow["TORIHIKI_KUBUN"] = drZeiSetting["KASHI_TORIHIKI_KUBUN"];
                                        taxRow["ZEI_RITSU"] = drZeiSetting["KASHI_ZEI_RITSU"];
                                    }
                                }
                                else
                                {
                                    taxRow["KANJO_KAMOKU_CD"] = DBNull.Value;
                                    taxRow["KANJO_KAMOKU_NM"] = DBNull.Value;
                                    taxRow["HOJO_KAMOKU_CD"] = DBNull.Value;
                                    taxRow["HOJO_KAMOKU_NM"] = DBNull.Value;
                                    taxRow["ZEI_KUBUN"] = DBNull.Value;
                                    taxRow["KAZEI_KUBUN"] = DBNull.Value;
                                    taxRow["TORIHIKI_KUBUN"] = DBNull.Value;
                                    taxRow["ZEI_RITSU"] = DBNull.Value;
                                }
                                dtCommit.Rows.Add(taxRow);
                            }
                        }
                    }
                    dtTaishakuData.Clear();
                    dtTaishakuData = dtCommit.Copy();

                    // 消し込んだ結果を整理する(上の行に詰める)
                    // 借方
                    aryTaishakuRow = dtTaishakuData.Select("TAISHAKU_KUBUN = 1", "GYO_BANGO, MEISAI_KUBUN");
                    prevRowNo = 0;
                    for (int j = 0; j < aryTaishakuRow.Length; j++)
                    {
                        // 現在保持している行番号が前に振った行番号+1、あるいは変わっていない場合は特に
                        // 変更しない。飛んでる場合だけ変更する
                        if (Util.ToInt(aryTaishakuRow[j]["GYO_BANGO"]) > prevRowNo + 1)
                        {
                            aryTaishakuRow[j]["GYO_BANGO"] = prevRowNo + 1;
                        }

                        prevRowNo = Util.ToInt(aryTaishakuRow[j]["GYO_BANGO"]);
                    }

                    // 貸方
                    aryTaishakuRow = dtTaishakuData.Select("TAISHAKU_KUBUN = 2", "GYO_BANGO, MEISAI_KUBUN");
                    prevRowNo = 0;
                    for (int j = 0; j < aryTaishakuRow.Length; j++)
                    {
                        // 現在保持している行番号が前に振った行番号+1、あるいは変わっていない場合は特に
                        // 変更しない。飛んでる場合だけ変更する
                        if (Util.ToInt(aryTaishakuRow[j]["GYO_BANGO"]) > prevRowNo + 1)
                        {
                            aryTaishakuRow[j]["GYO_BANGO"] = prevRowNo + 1;
                        }

                        prevRowNo = Util.ToInt(aryTaishakuRow[j]["GYO_BANGO"]);
                    }

                    // 最後に整列して格納
                    aryFormattedData = dtTaishakuData.Select(null, "GYO_BANGO, TAISHAKU_KUBUN, MEISAI_KUBUN");
                    dtFormattedData = dtTaishakuData.Clone();
                    for (int j = 0; j < aryFormattedData.Length; j++)
                    {
                        dtFormattedData.ImportRow(aryFormattedData[j]);
                    }

                    // DataSetにDataTableを追加
                    dsTaishakuData.Tables.Add(dtFormattedData);

                    // 次の支払先の貸借情報格納用に新たにDataTableを作成
                    dtTaishakuData = new DataTable();

                    dtTaishakuData.Columns.Add("GYO_BANGO", typeof(decimal));
                    dtTaishakuData.Columns.Add("SHIHARAISAKI_CD", typeof(decimal));
                    dtTaishakuData.Columns.Add("TAISHAKU_KUBUN", typeof(decimal));
                    dtTaishakuData.Columns.Add("MEISAI_KUBUN", typeof(decimal));
                    dtTaishakuData.Columns.Add("DENPYO_KUBUN", typeof(decimal));
                    dtTaishakuData.Columns.Add("DENPYO_DATE", typeof(DateTime));
                    dtTaishakuData.Columns.Add("KANJO_KAMOKU_CD", typeof(decimal));
                    dtTaishakuData.Columns.Add("KANJO_KAMOKU_NM", typeof(string));
                    dtTaishakuData.Columns.Add("HOJO_KAMOKU_CD", typeof(decimal));
                    dtTaishakuData.Columns.Add("HOJO_KAMOKU_NM", typeof(string));
                    dtTaishakuData.Columns.Add("BUMON_CD", typeof(decimal));
                    dtTaishakuData.Columns.Add("TEKIYO_CD", typeof(decimal));
                    dtTaishakuData.Columns.Add("TEKIYO", typeof(string));
                    dtTaishakuData.Columns.Add("ZEIKOMI_KINGAKU", typeof(decimal));
                    dtTaishakuData.Columns.Add("ZEINUKI_KINGAKU", typeof(decimal));
                    dtTaishakuData.Columns.Add("SHOHIZEI_KINGAKU", typeof(decimal));
                    dtTaishakuData.Columns.Add("ZEI_KUBUN", typeof(decimal));
                    dtTaishakuData.Columns.Add("KAZEI_KUBUN", typeof(decimal));
                    dtTaishakuData.Columns.Add("TORIHIKI_KUBUN", typeof(decimal));
                    dtTaishakuData.Columns.Add("ZEI_RITSU", typeof(decimal));
                    dtTaishakuData.Columns.Add("JIGYO_KUBUN", typeof(decimal));
                    dtTaishakuData.Columns.Add("SHOHIZEI_NYURYOKU_HOHO", typeof(decimal));
                    dtTaishakuData.Columns.Add("SHOHIZEI_HENKO", typeof(decimal));
                    dtTaishakuData.Columns.Add("KESSAN_KUBUN", typeof(decimal));
                    dtTaishakuData.Columns.Add("SHIWAKE_SAKUSEI_KUBUN", typeof(decimal));
                    dtTaishakuData.Columns.Add("CHK_ZUMI", typeof(decimal));
                    dtTaishakuData.Columns.Add("DEL_FLG", typeof(decimal));

                    // 行番号をリセット
                    rowNo = 0;
                }






                // (借方科目)仕訳コードから自動仕訳設定Ｂ.仕訳コードに該当するレコードを引き当て
                drSwkStgB = GetSwkSettingBRec(jdSwkSettingB, Util.ToInt(swkTgtData.Rows[i]["仕訳コード"]));

                // (貸方科目)取引区分１、２から自動仕訳設定Ｂ.仕訳コードに該当するレコードを引き当て
                drSwkStgA = GetSwkSettingARec(jdSwkSettingA,
                    Util.ToInt(swkTgtData.Rows[i]["取引区分１"]), Util.ToInt(swkTgtData.Rows[i]["取引区分２"]));

                rowNo++;
                kariKazeiKbn = false;
                kashiKazeiKbn = false;






                // 暫定（請求転嫁の消費税算出）
                wZeiRt = 0m;

                // 借方の税別分の行
                drTaishakuData = dtTaishakuData.NewRow();
                drTaishakuData["GYO_BANGO"] = rowNo;
                drTaishakuData["SHIHARAISAKI_CD"] = swkTgtData.Rows[i]["会員番号"];
                // 返品(取引区分２=2の場合は貸借逆転)
                drTaishakuData["TAISHAKU_KUBUN"] = Util.ToInt(swkTgtData.Rows[i]["取引区分２"]) == 2 ? 2 : 1;
                drTaishakuData["MEISAI_KUBUN"] = 0;
                drTaishakuData["DENPYO_KUBUN"] = 1; // TODO:1固定でいいかどうか
                drTaishakuData["DENPYO_DATE"] = condition["SwkDpyDt"];
                drTaishakuData["TEKIYO_CD"] = Util.ToDecimal(condition["TekiyoCd"]);
                drTaishakuData["TEKIYO"] = condition["Tekiyo"];

                //drTaishakuData["SHOHIZEI_NYURYOKU_HOHO"] = swkTgtData.Rows[i]["消費税入力方法"];
                drTaishakuData["SHOHIZEI_NYURYOKU_HOHO"] = swkZeiNh; // 基本情報から設定
                drTaishakuData["SHOHIZEI_HENKO"] = 0;   // TODO:固定?
                drTaishakuData["KESSAN_KUBUN"] = 0;     // TODO:固定?
                //drTaishakuData["SHIWAKE_SAKUSEI_KUBUN"] = 8;    // TODO:固定?
                drTaishakuData["SHIWAKE_SAKUSEI_KUBUN"] = denpyoKbn; // 伝票区分を設定
                drTaishakuData["CHK_ZUMI"] = 0;
                drTaishakuData["DEL_FLG"] = 0;

                if (drSwkStgB != null)
                {
                    // 設定情報だけでは無く明細の情報も利用（GetSeiSwkPos）

                    //drTaishakuData["BUMON_CD"] = drSwkStgB["部門コード"];
                    //drTaishakuData["KANJO_KAMOKU_CD"] = drSwkStgB["勘定科目コード"];
                    //drTaishakuData["KANJO_KAMOKU_NM"] = drSwkStgB["勘定科目名"];
                    //if (Util.ToInt(drSwkStgB["補助使用区分"]) == 2)
                    //{
                    //    drTaishakuData["HOJO_KAMOKU_CD"] = swkTgtData.Rows[i]["会員番号"];
                    //    drTaishakuData["HOJO_KAMOKU_NM"] = swkTgtData.Rows[i]["会員名称"];
                    //}
                    //else
                    //{
                    //    drTaishakuData["HOJO_KAMOKU_CD"] = drSwkStgB["補助科目コード"];
                    //    drTaishakuData["HOJO_KAMOKU_NM"] = drSwkStgB["補助科目名"];
                    //}
                    //drTaishakuData["ZEI_KUBUN"] = drSwkStgB["税区分"];
                    //drTaishakuData["KAZEI_KUBUN"] = drSwkStgB["課税区分"];
                    //drTaishakuData["TORIHIKI_KUBUN"] = drSwkStgB["取引区分"];
                    //drTaishakuData["ZEI_RITSU"] = drSwkStgB["税率"];
                    //drTaishakuData["JIGYO_KUBUN"] = drSwkStgB["事業区分"];

                    //if (Util.ToInt(drSwkStgB["課税区分"]) == 1)
                    //{
                    //    kariKazeiKbn = true;
                    //}

                    // 部門コードに関しては仕訳設定の部門、取引明細側の部門、画面からの指定の省略時部門で優先をどれにするか？
                    drTaishakuData["BUMON_CD"] = swkTgtData.Rows[i]["部門コード"]; // swkBmnCd 部門はどれを優先にするか
                    drTaishakuData["KANJO_KAMOKU_CD"] = drSwkStgB["勘定科目コード"];
                    drTaishakuData["KANJO_KAMOKU_NM"] = drSwkStgB["勘定科目名"];
                    // 補助科目は省略時は船主
                    drTaishakuData["HOJO_KAMOKU_CD"] = (Util.ToInt(Util.ToString(drSwkStgB["補助科目コード"])) == -1 ? swkTgtData.Rows[i]["会員番号"] : drSwkStgB["補助科目コード"]);
                    drTaishakuData["HOJO_KAMOKU_NM"] = (Util.ToInt(Util.ToString(drSwkStgB["補助科目コード"])) == -1 ? swkTgtData.Rows[i]["会員名称"] : drSwkStgB["補助科目名"]);
                    // 税区分は明細側、かつ税率が違う場合は明細側の税率を設定
                    drTaishakuData["ZEI_KUBUN"] = swkTgtData.Rows[i]["税区分"];
                    DataRow r = GetZeiKbnRow(Util.ToInt(Util.ToString(swkTgtData.Rows[i]["税区分"])));
                    int KziKb = 0;
                    if (r != null)
                    {
                        drTaishakuData["KAZEI_KUBUN"] = r["KAZEI_KUBUN"];
                        KziKb = Util.ToInt(Util.ToString(r["KAZEI_KUBUN"]));
                        drTaishakuData["TORIHIKI_KUBUN"] = r["TORIHIKI_KUBUN"];
                        decimal ZeiRt = TaxUtil.GetTaxRate(Util.ToDate(condition["SwkDpyDt"]), Util.ToInt(Util.ToString(swkTgtData.Rows[i]["税区分"])), this._dba);
                        // 複数税率対応（取引の税率が取得した税率と相違の場合は明細の税率を設定）
                        if (ZeiRt != Util.ToDecimal(Util.ToString(swkTgtData.Rows[i]["税率"])))
                            drTaishakuData["ZEI_RITSU"] = Util.ToDecimal(Util.ToString(swkTgtData.Rows[i]["税率"]));
                        else
                            drTaishakuData["ZEI_RITSU"] = ZeiRt;
                    }
                    else
                    {
                        drTaishakuData["KAZEI_KUBUN"] = drSwkStgB["課税区分"];
                        KziKb = Util.ToInt(Util.ToString(drSwkStgB["課税区分"]));
                        drTaishakuData["TORIHIKI_KUBUN"] = drSwkStgB["取引区分"];
                        drTaishakuData["ZEI_RITSU"] = drSwkStgB["税率"];
                    }
                    // 事業区分は設定が無い場合は会社の事業区分を設定
                    drTaishakuData["JIGYO_KUBUN"] = (Util.ToInt(swkTgtData.Rows[i]["事業区分"]) == 0 ? swkJgyKb : swkTgtData.Rows[i]["事業区分"]);
                    // 金額の設定
                    if (KziKb == 1)
                    {
                        kariKazeiKbn = true;

                        // 暫定（請求転嫁の消費税算出）
                        //drTaishakuData["ZEIKOMI_KINGAKU"] = Util.ToDecimal(swkTgtData.Rows[i]["売上金額"]) + Util.ToDecimal(swkTgtData.Rows[i]["消費税"]);
                        //drTaishakuData["ZEINUKI_KINGAKU"] = Util.ToDecimal(swkTgtData.Rows[i]["売上金額"]);
                        //drTaishakuData["SHOHIZEI_KINGAKU"] = Util.ToDecimal(swkTgtData.Rows[i]["消費税"]);
                        if (Util.ToDecimal(swkTgtData.Rows[i]["外税売上金額"]) != 0)
                        {
                            wZeiRt = Util.ToDecimal(Util.ToString(drTaishakuData["ZEI_RITSU"]));
                            wKingk = Util.ToDecimal(swkTgtData.Rows[i]["外税売上金額"]);
                            wZeigk = wKingk * wZeiRt / 100;
                            wZeigk = TaxUtil.CalcFraction(wZeigk, 0, Util.ToInt(Util.ToString(swkTgtData.Rows[i]["消費税端数処理"])));
                            drTaishakuData["ZEIKOMI_KINGAKU"] = wKingk + wZeigk;
                            drTaishakuData["ZEINUKI_KINGAKU"] = wKingk;
                            drTaishakuData["SHOHIZEI_KINGAKU"] = wZeigk;
                            // 請求転嫁税抜きしない 
                            //drTaishakuData["ZEIKOMI_KINGAKU"] = wKingk;
                            //drTaishakuData["ZEINUKI_KINGAKU"] = wKingk;
                            //drTaishakuData["SHOHIZEI_KINGAKU"] = 0;
                        }
                        else if (Util.ToDecimal(swkTgtData.Rows[i]["内税売上金額"]) != 0)
                        {
                            wZeiRt = Util.ToDecimal(Util.ToString(drTaishakuData["ZEI_RITSU"]));
                            wKingk = Util.ToDecimal(swkTgtData.Rows[i]["内税売上金額"]);
                            wZeigk = (wKingk / (100 + wZeiRt)) * wZeiRt;
                            wZeigk = TaxUtil.CalcFraction(wZeigk, 0, Util.ToInt(Util.ToString(swkTgtData.Rows[i]["消費税端数処理"])));
                            // 請求転嫁税込み許可
                            drTaishakuData["ZEIKOMI_KINGAKU"] = wKingk;
                            drTaishakuData["ZEINUKI_KINGAKU"] = wKingk - wZeigk;
                            drTaishakuData["SHOHIZEI_KINGAKU"] = wZeigk;
                            // 請求転嫁税込みしない 
                            //drTaishakuData["ZEIKOMI_KINGAKU"] = wKingk;
                            //drTaishakuData["ZEINUKI_KINGAKU"] = wKingk;
                            //drTaishakuData["SHOHIZEI_KINGAKU"] = 0;
                        }
                        else
                        {
                            drTaishakuData["ZEIKOMI_KINGAKU"] = Util.ToDecimal(swkTgtData.Rows[i]["売上金額"]) + Util.ToDecimal(swkTgtData.Rows[i]["消費税"]);
                            drTaishakuData["ZEINUKI_KINGAKU"] = Util.ToDecimal(swkTgtData.Rows[i]["売上金額"]);
                            drTaishakuData["SHOHIZEI_KINGAKU"] = Util.ToDecimal(swkTgtData.Rows[i]["消費税"]);
                        }
                    }
                    else
                    {
                        // 暫定（請求転嫁の消費税算出）
                        //drTaishakuData["ZEIKOMI_KINGAKU"] = Util.ToDecimal(swkTgtData.Rows[i]["売上金額"]) + Util.ToDecimal(swkTgtData.Rows[i]["消費税"]);
                        //drTaishakuData["ZEINUKI_KINGAKU"] = Util.ToDecimal(swkTgtData.Rows[i]["売上金額"]) + Util.ToDecimal(swkTgtData.Rows[i]["消費税"]);
                        //drTaishakuData["SHOHIZEI_KINGAKU"] = 0;
                        if (Util.ToDecimal(swkTgtData.Rows[i]["外税売上金額"]) != 0)
                        {
                            drTaishakuData["ZEIKOMI_KINGAKU"] = Util.ToDecimal(swkTgtData.Rows[i]["外税売上金額"]);
                            drTaishakuData["ZEINUKI_KINGAKU"] = Util.ToDecimal(swkTgtData.Rows[i]["外税売上金額"]);
                            drTaishakuData["SHOHIZEI_KINGAKU"] = 0;
                        }
                        else if (Util.ToDecimal(swkTgtData.Rows[i]["内税売上金額"]) != 0)
                        {
                            drTaishakuData["ZEIKOMI_KINGAKU"] = Util.ToDecimal(swkTgtData.Rows[i]["内税売上金額"]);
                            drTaishakuData["ZEINUKI_KINGAKU"] = Util.ToDecimal(swkTgtData.Rows[i]["内税売上金額"]);
                            drTaishakuData["SHOHIZEI_KINGAKU"] = 0;
                        }
                        else
                        {
                            drTaishakuData["ZEIKOMI_KINGAKU"] = Util.ToDecimal(swkTgtData.Rows[i]["売上金額"]) + Util.ToDecimal(swkTgtData.Rows[i]["消費税"]);
                            drTaishakuData["ZEINUKI_KINGAKU"] = Util.ToDecimal(swkTgtData.Rows[i]["売上金額"]) + Util.ToDecimal(swkTgtData.Rows[i]["消費税"]);
                            drTaishakuData["SHOHIZEI_KINGAKU"] = 0;
                        }
                    }
                }
                else
                {
                    drTaishakuData["BUMON_CD"] = DBNull.Value;
                    drTaishakuData["KANJO_KAMOKU_CD"] = DBNull.Value;
                    drTaishakuData["KANJO_KAMOKU_NM"] = DBNull.Value;
                    drTaishakuData["HOJO_KAMOKU_CD"] = DBNull.Value;
                    drTaishakuData["HOJO_KAMOKU_NM"] = DBNull.Value;
                    drTaishakuData["ZEI_KUBUN"] = DBNull.Value;
                    drTaishakuData["KAZEI_KUBUN"] = DBNull.Value;
                    drTaishakuData["TORIHIKI_KUBUN"] = DBNull.Value;
                    drTaishakuData["ZEI_RITSU"] = DBNull.Value;
                    drTaishakuData["JIGYO_KUBUN"] = DBNull.Value;
                }
                dtTaishakuData.Rows.Add(drTaishakuData);







                // 借方の消費税の行(勘定科目に対する課税区分が1の場合のみ)
                if (kariKazeiKbn)
                {
                    // 消費税設定を取得(仮払。返品の場合は仮払のまま打消し)
                    drZeiSetting = GetZeiSettingRow(zeiSetting, 2);

                    drTaishakuData = dtTaishakuData.NewRow();
                    drTaishakuData["GYO_BANGO"] = rowNo;
                    drTaishakuData["SHIHARAISAKI_CD"] = swkTgtData.Rows[i]["会員番号"];
                    // 返品(取引区分２=2の場合は貸借逆転)
                    drTaishakuData["TAISHAKU_KUBUN"] = Util.ToInt(swkTgtData.Rows[i]["取引区分２"]) == 2 ? 2 : 1;
                    drTaishakuData["MEISAI_KUBUN"] = 1;
                    drTaishakuData["DENPYO_KUBUN"] = 1; // TODO:1固定でいいかどうか
                    drTaishakuData["DENPYO_DATE"] = condition["SwkDpyDt"];
                    drTaishakuData["BUMON_CD"] = 0;
                    drTaishakuData["TEKIYO_CD"] = Util.ToDecimal(condition["TekiyoCd"]);
                    drTaishakuData["TEKIYO"] = condition["Tekiyo"];
                    drTaishakuData["ZEIKOMI_KINGAKU"] = 0;

                    // 暫定（請求転嫁の消費税算出）
                    //drTaishakuData["ZEINUKI_KINGAKU"] = Util.ToDecimal(swkTgtData.Rows[i]["消費税"]);
                    if (Util.ToDecimal(swkTgtData.Rows[i]["外税売上金額"]) != 0)
                    {
                        wKingk = Util.ToDecimal(swkTgtData.Rows[i]["外税売上金額"]);
                        wZeigk = wKingk * wZeiRt / 100;
                        wZeigk = TaxUtil.CalcFraction(wZeigk, 0, Util.ToInt(Util.ToString(swkTgtData.Rows[i]["消費税端数処理"])));
                        drTaishakuData["ZEINUKI_KINGAKU"] = wZeigk;
                    }
                    else if (Util.ToDecimal(swkTgtData.Rows[i]["内税売上金額"]) != 0)
                    {
                        wKingk = Util.ToDecimal(swkTgtData.Rows[i]["内税売上金額"]);
                        wZeigk = (wKingk / (100 + wZeiRt)) * wZeiRt;
                        wZeigk = TaxUtil.CalcFraction(wZeigk, 0, Util.ToInt(Util.ToString(swkTgtData.Rows[i]["消費税端数処理"])));
                        drTaishakuData["ZEINUKI_KINGAKU"] = wZeigk;
                    }
                    else
                    {
                        drTaishakuData["ZEINUKI_KINGAKU"] = Util.ToDecimal(swkTgtData.Rows[i]["消費税"]);
                    }

                    drTaishakuData["SHOHIZEI_KINGAKU"] = 0;
                    drTaishakuData["SHOHIZEI_NYURYOKU_HOHO"] = swkTgtData.Rows[i]["消費税入力方法"];
                    drTaishakuData["SHOHIZEI_HENKO"] = 0;   // TODO:固定?
                    drTaishakuData["KESSAN_KUBUN"] = 0;     // TODO:固定?
                    drTaishakuData["SHIWAKE_SAKUSEI_KUBUN"] = 8;    // TODO:固定?
                    drTaishakuData["JIGYO_KUBUN"] = Util.ToDecimal(swkTgtData.Rows[i]["事業区分"]);
                    drTaishakuData["CHK_ZUMI"] = 0;
                    drTaishakuData["DEL_FLG"] = 0;
                    if (drZeiSetting != null)
                    {
                        drTaishakuData["KANJO_KAMOKU_CD"] = drZeiSetting["KANJO_KAMOKU_CD"];
                        drTaishakuData["KANJO_KAMOKU_NM"] = drZeiSetting["KANJO_KAMOKU_NM"];
                        drTaishakuData["HOJO_KAMOKU_CD"] = 0;
                        drTaishakuData["HOJO_KAMOKU_NM"] = DBNull.Value;
                        drTaishakuData["ZEI_KUBUN"] = drZeiSetting["KARIKATA_ZEI_KUBUN"];
                        drTaishakuData["KAZEI_KUBUN"] = drZeiSetting["KARI_KAZEI_KUBUN"];
                        drTaishakuData["TORIHIKI_KUBUN"] = drZeiSetting["KARI_TORIHIKI_KUBUN"];
                        drTaishakuData["ZEI_RITSU"] = drZeiSetting["KARI_ZEI_RITSU"];
                    }
                    else
                    {
                        drTaishakuData["KANJO_KAMOKU_CD"] = DBNull.Value;
                        drTaishakuData["KANJO_KAMOKU_NM"] = DBNull.Value;
                        drTaishakuData["HOJO_KAMOKU_CD"] = DBNull.Value;
                        drTaishakuData["HOJO_KAMOKU_NM"] = DBNull.Value;
                        drTaishakuData["ZEI_KUBUN"] = DBNull.Value;
                        drTaishakuData["KAZEI_KUBUN"] = DBNull.Value;
                        drTaishakuData["TORIHIKI_KUBUN"] = DBNull.Value;
                        drTaishakuData["ZEI_RITSU"] = DBNull.Value;
                    }
                    dtTaishakuData.Rows.Add(drTaishakuData);
                }

                // 貸方の税別分の行
                drTaishakuData = dtTaishakuData.NewRow();
                drTaishakuData["GYO_BANGO"] = rowNo;
                drTaishakuData["SHIHARAISAKI_CD"] = swkTgtData.Rows[i]["会員番号"];
                // 返品(取引区分２=2の場合は貸借逆転)
                drTaishakuData["TAISHAKU_KUBUN"] = Util.ToInt(swkTgtData.Rows[i]["取引区分２"]) == 2 ? 1 : 2;
                drTaishakuData["MEISAI_KUBUN"] = 0;
                drTaishakuData["DENPYO_KUBUN"] = 1; // TODO:1固定でいいかどうか
                drTaishakuData["DENPYO_DATE"] = condition["SwkDpyDt"];
                drTaishakuData["TEKIYO_CD"] = Util.ToDecimal(condition["TekiyoCd"]);
                drTaishakuData["TEKIYO"] = condition["Tekiyo"];
                if (drSwkStgA != null && Util.ToInt(drSwkStgA["課税区分"]) == 1)
                {
                    // 暫定（請求転嫁の消費税算出）
                    //drTaishakuData["ZEIKOMI_KINGAKU"] = Util.ToDecimal(swkTgtData.Rows[i]["売上金額"]) + Util.ToDecimal(swkTgtData.Rows[i]["消費税"]);
                    //drTaishakuData["ZEINUKI_KINGAKU"] = Util.ToDecimal(swkTgtData.Rows[i]["売上金額"]);
                    //drTaishakuData["SHOHIZEI_KINGAKU"] = Util.ToDecimal(swkTgtData.Rows[i]["消費税"]);
                    if (Util.ToDecimal(swkTgtData.Rows[i]["外税売上金額"]) != 0)
                    {
                        wKingk = Util.ToDecimal(swkTgtData.Rows[i]["外税売上金額"]);
                        wZeigk = wKingk * wZeiRt / 100;
                        wZeigk = TaxUtil.CalcFraction(wZeigk, 0, Util.ToInt(Util.ToString(swkTgtData.Rows[i]["消費税端数処理"])));
                        drTaishakuData["ZEIKOMI_KINGAKU"] = wKingk + wZeigk;
                        drTaishakuData["ZEINUKI_KINGAKU"] = wKingk;
                        drTaishakuData["SHOHIZEI_KINGAKU"] = wZeigk;
                    }
                    else if (Util.ToDecimal(swkTgtData.Rows[i]["内税売上金額"]) != 0)
                    {
                        wKingk = Util.ToDecimal(swkTgtData.Rows[i]["内税売上金額"]);
                        wZeigk = (wKingk / (100 + wZeiRt)) * wZeiRt;
                        wZeigk = TaxUtil.CalcFraction(wZeigk, 0, Util.ToInt(Util.ToString(swkTgtData.Rows[i]["消費税端数処理"])));
                        drTaishakuData["ZEIKOMI_KINGAKU"] = wKingk;
                        drTaishakuData["ZEINUKI_KINGAKU"] = wKingk - wZeigk;
                        drTaishakuData["SHOHIZEI_KINGAKU"] = wZeigk;
                    }
                    else
                    {
                        drTaishakuData["ZEIKOMI_KINGAKU"] = Util.ToDecimal(swkTgtData.Rows[i]["売上金額"]) + Util.ToDecimal(swkTgtData.Rows[i]["消費税"]);
                        drTaishakuData["ZEINUKI_KINGAKU"] = Util.ToDecimal(swkTgtData.Rows[i]["売上金額"]);
                        drTaishakuData["SHOHIZEI_KINGAKU"] = Util.ToDecimal(swkTgtData.Rows[i]["消費税"]);
                    }
                }
                else
                {
                    // 暫定（請求転嫁の消費税算出）
                    //drTaishakuData["ZEIKOMI_KINGAKU"] = Util.ToDecimal(swkTgtData.Rows[i]["売上金額"]) + Util.ToDecimal(swkTgtData.Rows[i]["消費税"]);
                    //drTaishakuData["ZEINUKI_KINGAKU"] = Util.ToDecimal(swkTgtData.Rows[i]["売上金額"]) + Util.ToDecimal(swkTgtData.Rows[i]["消費税"]);
                    //drTaishakuData["SHOHIZEI_KINGAKU"] = 0;
                    if (Util.ToDecimal(swkTgtData.Rows[i]["外税売上金額"]) != 0)
                    {
                        wKingk = Util.ToDecimal(swkTgtData.Rows[i]["外税売上金額"]);
                        wZeigk = wKingk * wZeiRt / 100;
                        wZeigk = TaxUtil.CalcFraction(wZeigk, 0, Util.ToInt(Util.ToString(swkTgtData.Rows[i]["消費税端数処理"])));
                        drTaishakuData["ZEIKOMI_KINGAKU"] = wKingk + wZeigk;
                        drTaishakuData["ZEINUKI_KINGAKU"] = wKingk + wZeigk;
                        drTaishakuData["SHOHIZEI_KINGAKU"] = 0;
                    }
                    else if (Util.ToDecimal(swkTgtData.Rows[i]["内税売上金額"]) != 0)
                    {
                        wKingk = Util.ToDecimal(swkTgtData.Rows[i]["内税売上金額"]);
                        //wZeigk = (wKingk / (100 + wZeiRt)) * wZeiRt;
                        //wZeigk = TaxUtil.CalcFraction(wZeigk, 0, Util.ToInt(Util.ToString(swkTgtData.Rows[i]["消費税端数処理"])));
                        drTaishakuData["ZEIKOMI_KINGAKU"] = wKingk;
                        drTaishakuData["ZEINUKI_KINGAKU"] = wKingk;
                        drTaishakuData["SHOHIZEI_KINGAKU"] = 0;
                    }
                    else
                    {
                        drTaishakuData["ZEIKOMI_KINGAKU"] = Util.ToDecimal(swkTgtData.Rows[i]["売上金額"]) + Util.ToDecimal(swkTgtData.Rows[i]["消費税"]);
                        drTaishakuData["ZEINUKI_KINGAKU"] = Util.ToDecimal(swkTgtData.Rows[i]["売上金額"]) + Util.ToDecimal(swkTgtData.Rows[i]["消費税"]);
                        drTaishakuData["SHOHIZEI_KINGAKU"] = 0;
                    }
                }
                //drTaishakuData["SHOHIZEI_NYURYOKU_HOHO"] = swkTgtData.Rows[i]["消費税入力方法"];
                drTaishakuData["SHOHIZEI_NYURYOKU_HOHO"] = swkZeiNh; // 基本情報から設定
                drTaishakuData["SHOHIZEI_HENKO"] = 0;   // TODO:固定?
                drTaishakuData["KESSAN_KUBUN"] = 0;     // TODO:固定?
                //drTaishakuData["SHIWAKE_SAKUSEI_KUBUN"] = 8;    // TODO:固定?
                drTaishakuData["SHIWAKE_SAKUSEI_KUBUN"] = denpyoKbn; // 伝票区分を設定
                drTaishakuData["CHK_ZUMI"] = 0;
                drTaishakuData["DEL_FLG"] = 0;
                if (drSwkStgA != null)
                {
                    // 相手処理（SetMeisai）

                    // 部門コードに関しては仕訳設定の部門、取引明細側の部門、画面からの指定の省略時部門で優先をどれにするか？
                    //drTaishakuData["BUMON_CD"] = drSwkStgA["部門コード"];
                    drTaishakuData["BUMON_CD"] = (Util.ToInt(Util.ToString(drSwkStgA["部門コード"])) == -1 ? swkTgtData.Rows[i]["部門コード"] : drSwkStgA["部門コード"]);
                    drTaishakuData["KANJO_KAMOKU_CD"] = drSwkStgA["勘定科目コード"];
                    drTaishakuData["KANJO_KAMOKU_NM"] = drSwkStgA["勘定科目名"];
                    // 補助科目は省略時は仕入先
                    //if (Util.ToInt(drSwkStgA["補助使用区分"]) == 2)
                    //{
                    //    drTaishakuData["HOJO_KAMOKU_CD"] = swkTgtData.Rows[i]["会員番号"];
                    //    drTaishakuData["HOJO_KAMOKU_NM"] = swkTgtData.Rows[i]["会員名称"];
                    //}
                    //else
                    //{
                    //    drTaishakuData["HOJO_KAMOKU_CD"] = drSwkStgA["補助科目コード"];
                    //    drTaishakuData["HOJO_KAMOKU_NM"] = drSwkStgA["補助科目名"];
                    //}
                    drTaishakuData["HOJO_KAMOKU_CD"] = (Util.ToInt(Util.ToString(drSwkStgA["補助科目コード"])) == -1 ? swkTgtData.Rows[i]["会員番号"] : drSwkStgA["補助科目コード"]);
                    drTaishakuData["HOJO_KAMOKU_NM"] = (Util.ToInt(Util.ToString(drSwkStgA["補助科目コード"])) == -1 ? swkTgtData.Rows[i]["会員名称"] : drSwkStgA["補助科目名"]);

                    drTaishakuData["ZEI_KUBUN"] = drSwkStgA["税区分"];
                    drTaishakuData["KAZEI_KUBUN"] = drSwkStgA["課税区分"];
                    drTaishakuData["TORIHIKI_KUBUN"] = drSwkStgA["取引区分"];
                    drTaishakuData["ZEI_RITSU"] = drSwkStgA["税率"];
                    drTaishakuData["JIGYO_KUBUN"] = drSwkStgA["事業区分"];

                    if (Util.ToInt(drSwkStgA["課税区分"]) == 1)
                    {
                        kashiKazeiKbn = true;
                    }

                    dtTaishakuData.Rows.Add(drTaishakuData);
                }
                else
                {
                    drTaishakuData["BUMON_CD"] = DBNull.Value;
                    drTaishakuData["KANJO_KAMOKU_CD"] = DBNull.Value;
                    drTaishakuData["KANJO_KAMOKU_NM"] = DBNull.Value;
                    drTaishakuData["HOJO_KAMOKU_CD"] = DBNull.Value;
                    drTaishakuData["HOJO_KAMOKU_NM"] = DBNull.Value;
                    drTaishakuData["ZEI_KUBUN"] = DBNull.Value;
                    drTaishakuData["KAZEI_KUBUN"] = DBNull.Value;
                    drTaishakuData["TORIHIKI_KUBUN"] = DBNull.Value;
                    drTaishakuData["ZEI_RITSU"] = DBNull.Value;
                    drTaishakuData["JIGYO_KUBUN"] = DBNull.Value;
                }

                // 貸方の消費税の行(勘定科目に対する課税区分が1の場合のみ)
                if (kashiKazeiKbn)
                {
                    // 消費税設定を取得(仮受。返品の場合は仮受のまま打消し。)
                    drZeiSetting = GetZeiSettingRow(zeiSetting, 1);

                    drTaishakuData = dtTaishakuData.NewRow();
                    drTaishakuData["GYO_BANGO"] = rowNo;
                    drTaishakuData["SHIHARAISAKI_CD"] = swkTgtData.Rows[i]["会員番号"];
                    // 返品(取引区分２=2の場合は貸借逆転)
                    drTaishakuData["TAISHAKU_KUBUN"] = Util.ToInt(swkTgtData.Rows[i]["取引区分２"]) == 2 ? 1 : 2;
                    drTaishakuData["MEISAI_KUBUN"] = 1;
                    drTaishakuData["DENPYO_KUBUN"] = 1; // TODO:1固定でいいかどうか
                    drTaishakuData["DENPYO_DATE"] = condition["SwkDpyDt"];
                    drTaishakuData["BUMON_CD"] = 0;
                    drTaishakuData["TEKIYO_CD"] = Util.ToDecimal(condition["TekiyoCd"]);
                    drTaishakuData["TEKIYO"] = condition["Tekiyo"];
                    drTaishakuData["ZEIKOMI_KINGAKU"] = 0;

                    // 暫定（請求転嫁の消費税算出）
                    //drTaishakuData["ZEINUKI_KINGAKU"] = Util.ToDecimal(swkTgtData.Rows[i]["消費税"]);
                    if (Util.ToDecimal(swkTgtData.Rows[i]["外税売上金額"]) != 0)
                    {
                        wKingk = Util.ToDecimal(swkTgtData.Rows[i]["外税売上金額"]);
                        wZeigk = wKingk * wZeiRt / 100;
                        wZeigk = TaxUtil.CalcFraction(wZeigk, 0, Util.ToInt(Util.ToString(swkTgtData.Rows[i]["消費税端数処理"])));
                        drTaishakuData["ZEINUKI_KINGAKU"] = wZeigk;
                    }
                    else if (Util.ToDecimal(swkTgtData.Rows[i]["内税売上金額"]) != 0)
                    {
                        wKingk = Util.ToDecimal(swkTgtData.Rows[i]["内税売上金額"]);
                        wZeigk = (wKingk / (100 + wZeiRt)) * wZeiRt;
                        wZeigk = TaxUtil.CalcFraction(wZeigk, 0, Util.ToInt(Util.ToString(swkTgtData.Rows[i]["消費税端数処理"])));
                        drTaishakuData["ZEINUKI_KINGAKU"] = wZeigk;
                    }
                    else
                    {
                        drTaishakuData["ZEINUKI_KINGAKU"] = Util.ToDecimal(swkTgtData.Rows[i]["消費税"]);
                    }

                    drTaishakuData["SHOHIZEI_KINGAKU"] = 0;
                    drTaishakuData["SHOHIZEI_NYURYOKU_HOHO"] = swkTgtData.Rows[i]["消費税入力方法"];
                    drTaishakuData["SHOHIZEI_HENKO"] = 0;   // TODO:固定?
                    drTaishakuData["KESSAN_KUBUN"] = 0;     // TODO:固定?
                    drTaishakuData["SHIWAKE_SAKUSEI_KUBUN"] = 8;    // TODO:固定?
                    drTaishakuData["JIGYO_KUBUN"] = Util.ToDecimal(swkTgtData.Rows[i]["事業区分"]);
                    drTaishakuData["CHK_ZUMI"] = 0;
                    drTaishakuData["DEL_FLG"] = 0;
                    if (drZeiSetting != null)
                    {
                        drTaishakuData["KANJO_KAMOKU_CD"] = drZeiSetting["KANJO_KAMOKU_CD"];
                        drTaishakuData["KANJO_KAMOKU_NM"] = drZeiSetting["KANJO_KAMOKU_NM"];
                        drTaishakuData["HOJO_KAMOKU_CD"] = 0;
                        drTaishakuData["HOJO_KAMOKU_NM"] = DBNull.Value;
                        drTaishakuData["ZEI_KUBUN"] = drZeiSetting["KASHIKATA_ZEI_KUBUN"];
                        drTaishakuData["KAZEI_KUBUN"] = drZeiSetting["KASHI_KAZEI_KUBUN"];
                        drTaishakuData["TORIHIKI_KUBUN"] = drZeiSetting["KASHI_TORIHIKI_KUBUN"];
                        drTaishakuData["ZEI_RITSU"] = drZeiSetting["KASHI_ZEI_RITSU"];
                    }
                    else
                    {
                        drTaishakuData["KANJO_KAMOKU_CD"] = DBNull.Value;
                        drTaishakuData["KANJO_KAMOKU_NM"] = DBNull.Value;
                        drTaishakuData["HOJO_KAMOKU_CD"] = DBNull.Value;
                        drTaishakuData["HOJO_KAMOKU_NM"] = DBNull.Value;
                        drTaishakuData["ZEI_KUBUN"] = DBNull.Value;
                        drTaishakuData["KAZEI_KUBUN"] = DBNull.Value;
                        drTaishakuData["TORIHIKI_KUBUN"] = DBNull.Value;
                        drTaishakuData["ZEI_RITSU"] = DBNull.Value;
                    }
                    dtTaishakuData.Rows.Add(drTaishakuData);
                }

                // 会員番号の退避
                prevKaiinNo = Util.ToString(swkTgtData.Rows[i]["会員番号"]);
            }

            // 仕入に対する返品を消し込む
            for (int j = 0; j < dtTaishakuData.Rows.Count; j++)
            {
                // 削除フラグが立っている行は評価対象外
                if (Util.ToInt(dtTaishakuData.Rows[j]["DEL_FLG"]) == 1)
                {
                    continue;
                }

                // 同一の支払先コード・明細区分・勘定科目コード・補助科目コード・部門コード・税区分・事業区分で
                // 貸借逆のデータを取得する
                // ※チェック済みのデータは参照しない
                srcCond = new StringBuilder();
                srcCond.Append("SHIHARAISAKI_CD = " + Util.ToString(dtTaishakuData.Rows[j]["SHIHARAISAKI_CD"]));
                srcCond.Append(" AND MEISAI_KUBUN = " + Util.ToString(dtTaishakuData.Rows[j]["MEISAI_KUBUN"]));
                srcCond.Append(" AND KANJO_KAMOKU_CD = " + Util.ToString(dtTaishakuData.Rows[j]["KANJO_KAMOKU_CD"]));
                srcCond.Append(" AND HOJO_KAMOKU_CD = " + Util.ToString(dtTaishakuData.Rows[j]["HOJO_KAMOKU_CD"]));
                srcCond.Append(" AND BUMON_CD = " + Util.ToString(dtTaishakuData.Rows[j]["BUMON_CD"]));
                srcCond.Append(" AND ZEI_KUBUN = " + Util.ToString(dtTaishakuData.Rows[j]["ZEI_KUBUN"]));
                srcCond.Append(" AND JIGYO_KUBUN = " + Util.ToString(dtTaishakuData.Rows[j]["JIGYO_KUBUN"]));

                // 複数税率対応
                srcCond.Append(" AND ZEI_RITSU = " + Util.ToString(dtTaishakuData.Rows[j]["ZEI_RITSU"]));

                srcCond.Append(" AND TAISHAKU_KUBUN <> " + Util.ToString(dtTaishakuData.Rows[j]["TAISHAKU_KUBUN"]));
                srcCond.Append(" AND CHK_ZUMI = 0");
                srcCond.Append(" AND DEL_FLG = 0");
                aryAiteRow = dtTaishakuData.Select(srcCond.ToString());

                for (int k = 0; k < aryAiteRow.Length; k++)
                {
                    if (Util.ToDecimal(dtTaishakuData.Rows[j]["ZEINUKI_KINGAKU"]) >
                        Util.ToDecimal(aryAiteRow[k]["ZEINUKI_KINGAKU"]))
                    {
                        // 自分が多ければ相手を消す
                        dtTaishakuData.Rows[j]["ZEIKOMI_KINGAKU"] =
                            Util.ToDecimal(dtTaishakuData.Rows[j]["ZEIKOMI_KINGAKU"]) -
                            Util.ToDecimal(aryAiteRow[k]["ZEIKOMI_KINGAKU"]);
                        dtTaishakuData.Rows[j]["ZEINUKI_KINGAKU"] =
                            Util.ToDecimal(dtTaishakuData.Rows[j]["ZEINUKI_KINGAKU"]) -
                            Util.ToDecimal(aryAiteRow[k]["ZEINUKI_KINGAKU"]);
                        dtTaishakuData.Rows[j]["SHOHIZEI_KINGAKU"] =
                            Util.ToDecimal(dtTaishakuData.Rows[j]["SHOHIZEI_KINGAKU"]) -
                            Util.ToDecimal(aryAiteRow[k]["SHOHIZEI_KINGAKU"]);
                        aryAiteRow[k]["DEL_FLG"] = 1;
                    }
                    else if (Util.ToDecimal(dtTaishakuData.Rows[j]["ZEINUKI_KINGAKU"]) ==
                        Util.ToDecimal(aryAiteRow[k]["ZEINUKI_KINGAKU"]))
                    {
                        // 同一金額なら借方を残して貸方を消す
                        if (Util.ToInt(dtTaishakuData.Rows[j]["TAISHAKU_KUBUN"]) == 1)
                        {
                            dtTaishakuData.Rows[j]["ZEIKOMI_KINGAKU"] =
                                Util.ToDecimal(dtTaishakuData.Rows[j]["ZEIKOMI_KINGAKU"]) -
                                Util.ToDecimal(aryAiteRow[k]["ZEIKOMI_KINGAKU"]);
                            dtTaishakuData.Rows[j]["ZEINUKI_KINGAKU"] =
                                Util.ToDecimal(dtTaishakuData.Rows[j]["ZEINUKI_KINGAKU"]) -
                                Util.ToDecimal(aryAiteRow[k]["ZEINUKI_KINGAKU"]);
                            dtTaishakuData.Rows[j]["SHOHIZEI_KINGAKU"] =
                                Util.ToDecimal(dtTaishakuData.Rows[j]["SHOHIZEI_KINGAKU"]) -
                                Util.ToDecimal(aryAiteRow[k]["SHOHIZEI_KINGAKU"]);
                            aryAiteRow[k]["DEL_FLG"] = 1;
                            // 消費税行なら借方も消す
                            if (Util.ToInt(dtTaishakuData.Rows[j]["MEISAI_KUBUN"]) == 1)
                            {
                                dtTaishakuData.Rows[j]["DEL_FLG"] = 1;
                            }
                        }
                        else
                        {
                            aryAiteRow[k]["ZEIKOMI_KINGAKU"] =
                                Util.ToDecimal(aryAiteRow[k]["ZEIKOMI_KINGAKU"]) -
                                Util.ToDecimal(dtTaishakuData.Rows[j]["ZEIKOMI_KINGAKU"]);
                            aryAiteRow[k]["ZEINUKI_KINGAKU"] =
                                Util.ToDecimal(aryAiteRow[k]["ZEINUKI_KINGAKU"]) -
                                Util.ToDecimal(dtTaishakuData.Rows[j]["ZEINUKI_KINGAKU"]);
                            aryAiteRow[k]["SHOHIZEI_KINGAKU"] =
                                Util.ToDecimal(aryAiteRow[k]["SHOHIZEI_KINGAKU"]) -
                                Util.ToDecimal(dtTaishakuData.Rows[j]["SHOHIZEI_KINGAKU"]);
                            dtTaishakuData.Rows[j]["DEL_FLG"] = 1;
                            // 消費税行なら借方も消す
                            if (Util.ToInt(dtTaishakuData.Rows[j]["MEISAI_KUBUN"]) == 1)
                            {
                                aryAiteRow[k]["DEL_FLG"] = 1;
                            }
                        }
                    }
                    else
                    {
                        // 相手が多ければ自分を消す
                        aryAiteRow[k]["ZEIKOMI_KINGAKU"] =
                            Util.ToDecimal(aryAiteRow[k]["ZEIKOMI_KINGAKU"]) -
                            Util.ToDecimal(dtTaishakuData.Rows[j]["ZEIKOMI_KINGAKU"]);
                        aryAiteRow[k]["ZEINUKI_KINGAKU"] =
                            Util.ToDecimal(aryAiteRow[k]["ZEINUKI_KINGAKU"]) -
                            Util.ToDecimal(dtTaishakuData.Rows[j]["ZEINUKI_KINGAKU"]);
                        aryAiteRow[k]["SHOHIZEI_KINGAKU"] =
                            Util.ToDecimal(aryAiteRow[k]["SHOHIZEI_KINGAKU"]) -
                            Util.ToDecimal(dtTaishakuData.Rows[j]["SHOHIZEI_KINGAKU"]);
                        dtTaishakuData.Rows[j]["DEL_FLG"] = 1;
                    }
                }

                // ここまでの処理で行が消されていなければチェック済みフラグを立てる
                if (Util.ToInt(dtTaishakuData.Rows[j]["DEL_FLG"]) == 0)
                {
                    dtTaishakuData.Rows[j]["CHK_ZUMI"] = 1;
                }
            }

            if (swkDpMk == 1)
            {
                // 複合仕訳の場合、貸借の科目単位でまとめ上げる
                arySortedRow = dtTaishakuData.Select(null, "GYO_BANGO, TAISHAKU_KUBUN, MEISAI_KUBUN");

                for (int j = 0; j < arySortedRow.Length; j++)
                {
                    // 既に削除された行は評価しない
                    if (Util.ToInt(arySortedRow[j]["DEL_FLG"]) == 1) continue;

                    // 同一の貸借区分・明細区分・勘定科目コード・補助科目コード・部門コード・税区分・事業区分で
                    // ※チェック済みのデータは参照しない
                    srcCond = new StringBuilder();
                    srcCond.Append("TAISHAKU_KUBUN = " + Util.ToString(arySortedRow[j]["TAISHAKU_KUBUN"]));
                    srcCond.Append(" AND MEISAI_KUBUN = " + Util.ToString(arySortedRow[j]["MEISAI_KUBUN"]));
                    srcCond.Append(" AND KANJO_KAMOKU_CD = " + Util.ToString(arySortedRow[j]["KANJO_KAMOKU_CD"]));
                    srcCond.Append(" AND HOJO_KAMOKU_CD = " + Util.ToString(arySortedRow[j]["HOJO_KAMOKU_CD"]));
                    srcCond.Append(" AND BUMON_CD = " + Util.ToString(arySortedRow[j]["BUMON_CD"]));
                    srcCond.Append(" AND ZEI_KUBUN = " + Util.ToString(arySortedRow[j]["ZEI_KUBUN"]));
                    srcCond.Append(" AND JIGYO_KUBUN = " + Util.ToString(arySortedRow[j]["JIGYO_KUBUN"]));

                    // 複数税率対応（及び自身の行読み飛ばし時の不具合調整）
                    srcCond.Append(" AND ZEI_RITSU = " + Util.ToString(arySortedRow[j]["ZEI_RITSU"]));
                    srcCond.Append(" AND DEL_FLG = 0 ");

                    aryDupRow = dtTaishakuData.Select(srcCond.ToString(), "GYO_BANGO, TAISHAKU_KUBUN, MEISAI_KUBUN");

                    // 重複した1行目の情報に2,3行目の金額を加算していく
                    for (int k = 0; k < aryDupRow.Length; k++)
                    {
                        // 自身の行は読み飛ばす
                        if (k == 0)
                        {
                            // 行に持っている支払先コードをクリア
                            aryDupRow[k]["SHIHARAISAKI_CD"] = DBNull.Value;
                            continue;
                        }

                        // 既に削除された行は評価しない
                        if (Util.ToInt(aryDupRow[k]["DEL_FLG"]) == 1) continue;

                        aryDupRow[0]["ZEIKOMI_KINGAKU"] = Util.ToDecimal(aryDupRow[0]["ZEIKOMI_KINGAKU"]) + Util.ToDecimal(aryDupRow[k]["ZEIKOMI_KINGAKU"]);
                        aryDupRow[0]["ZEINUKI_KINGAKU"] = Util.ToDecimal(aryDupRow[0]["ZEINUKI_KINGAKU"]) + Util.ToDecimal(aryDupRow[k]["ZEINUKI_KINGAKU"]);
                        aryDupRow[0]["SHOHIZEI_KINGAKU"] = Util.ToDecimal(aryDupRow[0]["SHOHIZEI_KINGAKU"]) + Util.ToDecimal(aryDupRow[k]["SHOHIZEI_KINGAKU"]);
                        aryDupRow[k]["DEL_FLG"] = 1;
                    }
                }
            }
            else
            {
                // 複合仕訳でない場合、同一の支払先に対し、貸借の科目単位でまとめ上げる
                arySortedRow = dtTaishakuData.Select(null, "GYO_BANGO, TAISHAKU_KUBUN, MEISAI_KUBUN");

                for (int j = 0; j < arySortedRow.Length; j++)
                {
                    // 既に削除された行は評価しない
                    if (Util.ToInt(arySortedRow[j]["DEL_FLG"]) == 1) continue;

                    // 同一の支払先コード・貸借区分・明細区分・勘定科目コード・補助科目コード・部門コード・税区分・事業区分で
                    // ※チェック済みのデータは参照しない
                    srcCond = new StringBuilder();
                    srcCond.Append("SHIHARAISAKI_CD = " + Util.ToString(arySortedRow[j]["SHIHARAISAKI_CD"]));
                    srcCond.Append(" AND TAISHAKU_KUBUN = " + Util.ToString(arySortedRow[j]["TAISHAKU_KUBUN"]));
                    srcCond.Append(" AND MEISAI_KUBUN = " + Util.ToString(arySortedRow[j]["MEISAI_KUBUN"]));
                    srcCond.Append(" AND KANJO_KAMOKU_CD = " + Util.ToString(arySortedRow[j]["KANJO_KAMOKU_CD"]));
                    srcCond.Append(" AND HOJO_KAMOKU_CD = " + Util.ToString(arySortedRow[j]["HOJO_KAMOKU_CD"]));
                    srcCond.Append(" AND BUMON_CD = " + Util.ToString(arySortedRow[j]["BUMON_CD"]));
                    srcCond.Append(" AND ZEI_KUBUN = " + Util.ToString(arySortedRow[j]["ZEI_KUBUN"]));
                    srcCond.Append(" AND JIGYO_KUBUN = " + Util.ToString(arySortedRow[j]["JIGYO_KUBUN"]));

                    // 複数税率対応
                    srcCond.Append(" AND ZEI_RITSU = " + Util.ToString(arySortedRow[j]["ZEI_RITSU"]));
                    srcCond.Append(" AND DEL_FLG = 0 ");

                    aryDupRow = dtTaishakuData.Select(srcCond.ToString(), "GYO_BANGO, TAISHAKU_KUBUN, MEISAI_KUBUN");

                    // 重複した1行目の情報に2,3行目の金額を加算していく
                    for (int k = 0; k < aryDupRow.Length; k++)
                    {
                        // 自身の行は読み飛ばす
                        if (k == 0) continue;

                        // 既に削除された行は評価しない
                        if (Util.ToInt(aryDupRow[k]["DEL_FLG"]) == 1) continue;

                        aryDupRow[0]["ZEIKOMI_KINGAKU"] = Util.ToDecimal(aryDupRow[0]["ZEIKOMI_KINGAKU"]) + Util.ToDecimal(aryDupRow[k]["ZEIKOMI_KINGAKU"]);
                        aryDupRow[0]["ZEINUKI_KINGAKU"] = Util.ToDecimal(aryDupRow[0]["ZEINUKI_KINGAKU"]) + Util.ToDecimal(aryDupRow[k]["ZEINUKI_KINGAKU"]);
                        aryDupRow[0]["SHOHIZEI_KINGAKU"] = Util.ToDecimal(aryDupRow[0]["SHOHIZEI_KINGAKU"]) + Util.ToDecimal(aryDupRow[k]["SHOHIZEI_KINGAKU"]);
                        aryDupRow[k]["DEL_FLG"] = 1;
                    }
                }
            }

            // 削除フラグが1の行を削除(一旦削除確定用テンポラリテーブルに詰める)
            dtCommit = dtTaishakuData.Clone();
            foreach (DataRow row in dtTaishakuData.Rows)
            {
                // 作成データの調整
                //if (Util.ToInt(row["DEL_FLG"]) == 0) dtCommit.ImportRow(row);

                // 入力行のみを対象にして消費税行を別で追加
                if (Util.ToInt(row["DEL_FLG"]) == 0 && Util.ToInt(row["MEISAI_KUBUN"]) == 0)
                {
                    // マイナス時は貸借区分の反転しマイナスを無くして登録
                    if (Util.ToDecimal(row["ZEINUKI_KINGAKU"]) < 0)
                    {
                        row["ZEIKOMI_KINGAKU"] = Math.Abs(Util.ToDecimal(row["ZEIKOMI_KINGAKU"]));
                        row["ZEINUKI_KINGAKU"] = Math.Abs(Util.ToDecimal(row["ZEINUKI_KINGAKU"]));
                        row["SHOHIZEI_KINGAKU"] = Math.Abs(Util.ToDecimal(row["SHOHIZEI_KINGAKU"]));
                        row["TAISHAKU_KUBUN"] = (Util.ToInt(row["TAISHAKU_KUBUN"]) == 1 ? 2 : 1);
                    }
                    dtCommit.ImportRow(row);

                    // 課税で且つ消費税がある場合に消費税レコードを追加
                    if (Util.ToInt(row["KAZEI_KUBUN"]) == 1 && Util.ToDecimal(row["SHOHIZEI_KINGAKU"]) > 0)
                    {
                        int taxTai = 2;
                        if (Util.ToInt(row["TORIHIKI_KUBUN"]) >= 10 && Util.ToInt(row["TORIHIKI_KUBUN"]) <= 19)
                        {
                            taxTai = 1;
                        }
                        drZeiSetting = GetZeiSettingRow(zeiSetting, taxTai);
                        DataRow taxRow = dtCommit.NewRow();
                        taxRow["GYO_BANGO"] = row["GYO_BANGO"];
                        taxRow["SHIHARAISAKI_CD"] = row["SHIHARAISAKI_CD"];
                        taxRow["TAISHAKU_KUBUN"] = row["TAISHAKU_KUBUN"];
                        taxRow["MEISAI_KUBUN"] = 1;
                        taxRow["DENPYO_KUBUN"] = 1;
                        taxRow["DENPYO_DATE"] = row["DENPYO_DATE"];
                        taxRow["BUMON_CD"] = (Util.ToInt(drZeiSetting["BUMON_UMU"]) == 0 ? 0 : row["BUMON_CD"]);
                        taxRow["TEKIYO_CD"] = row["TEKIYO_CD"];
                        taxRow["TEKIYO"] = row["TEKIYO"];
                        taxRow["ZEIKOMI_KINGAKU"] = 0;
                        taxRow["ZEINUKI_KINGAKU"] = Util.ToDecimal(row["SHOHIZEI_KINGAKU"]);
                        taxRow["SHOHIZEI_KINGAKU"] = 0;
                        taxRow["SHOHIZEI_NYURYOKU_HOHO"] = swkZeiNh;
                        taxRow["SHOHIZEI_HENKO"] = row["SHOHIZEI_HENKO"];
                        taxRow["KESSAN_KUBUN"] = row["KESSAN_KUBUN"];
                        taxRow["SHIWAKE_SAKUSEI_KUBUN"] = row["SHIWAKE_SAKUSEI_KUBUN"];
                        taxRow["JIGYO_KUBUN"] = row["JIGYO_KUBUN"];
                        taxRow["CHK_ZUMI"] = 0;
                        taxRow["DEL_FLG"] = 0;
                        if (drZeiSetting != null)
                        {
                            taxRow["KANJO_KAMOKU_CD"] = drZeiSetting["KANJO_KAMOKU_CD"];
                            taxRow["KANJO_KAMOKU_NM"] = drZeiSetting["KANJO_KAMOKU_NM"];
                            taxRow["HOJO_KAMOKU_CD"] = (Util.ToInt(Util.ToString(drZeiSetting["HOJO_KAMOKU_UMU"])) == 0 ? 0 : row["HOJO_KAMOKU_CD"]);
                            taxRow["HOJO_KAMOKU_NM"] = (Util.ToInt(Util.ToString(drZeiSetting["HOJO_KAMOKU_UMU"])) == 0 ? DBNull.Value : row["HOJO_KAMOKU_NM"]);
                            if (Util.ToInt(Util.ToString(row["TAISHAKU_KUBUN"])) == 1)
                            {
                                taxRow["ZEI_KUBUN"] = drZeiSetting["KARIKATA_ZEI_KUBUN"];
                                taxRow["KAZEI_KUBUN"] = drZeiSetting["KARI_KAZEI_KUBUN"];
                                taxRow["TORIHIKI_KUBUN"] = drZeiSetting["KARI_TORIHIKI_KUBUN"];
                                taxRow["ZEI_RITSU"] = drZeiSetting["KARI_ZEI_RITSU"];
                            }
                            else
                            {
                                taxRow["ZEI_KUBUN"] = drZeiSetting["KASHIKATA_ZEI_KUBUN"];
                                taxRow["KAZEI_KUBUN"] = drZeiSetting["KASHI_KAZEI_KUBUN"];
                                taxRow["TORIHIKI_KUBUN"] = drZeiSetting["KASHI_TORIHIKI_KUBUN"];
                                taxRow["ZEI_RITSU"] = drZeiSetting["KASHI_ZEI_RITSU"];
                            }
                        }
                        else
                        {
                            taxRow["KANJO_KAMOKU_CD"] = DBNull.Value;
                            taxRow["KANJO_KAMOKU_NM"] = DBNull.Value;
                            taxRow["HOJO_KAMOKU_CD"] = DBNull.Value;
                            taxRow["HOJO_KAMOKU_NM"] = DBNull.Value;
                            taxRow["ZEI_KUBUN"] = DBNull.Value;
                            taxRow["KAZEI_KUBUN"] = DBNull.Value;
                            taxRow["TORIHIKI_KUBUN"] = DBNull.Value;
                            taxRow["ZEI_RITSU"] = DBNull.Value;
                        }
                        dtCommit.Rows.Add(taxRow);
                    }
                }
            }
            dtTaishakuData.Clear();
            dtTaishakuData = dtCommit.Copy();

            // 消し込んだ結果を整理する(上の行に詰める)
            if (swkDpMk == 1)
            {
                // 複合仕訳の場合、貸借それぞれ上に詰める。
                // 借方
                aryTaishakuRow = dtTaishakuData.Select("TAISHAKU_KUBUN = 1", "GYO_BANGO, MEISAI_KUBUN");
                prevRowNo = 0;
                for (int j = 0; j < aryTaishakuRow.Length; j++)
                {
                    // 現在保持している行番号が前に振った行番号+1、あるいは変わっていない場合は特に
                    // 変更しない。飛んでる場合だけ変更する
                    if (Util.ToInt(aryTaishakuRow[j]["GYO_BANGO"]) > prevRowNo + 1)
                    {
                        aryTaishakuRow[j]["GYO_BANGO"] = prevRowNo + 1;
                    }

                    prevRowNo = Util.ToInt(aryTaishakuRow[j]["GYO_BANGO"]);
                }

                // 貸方
                aryTaishakuRow = dtTaishakuData.Select("TAISHAKU_KUBUN = 2", "GYO_BANGO, MEISAI_KUBUN");
                prevRowNo = 0;
                for (int j = 0; j < aryTaishakuRow.Length; j++)
                {
                    // 現在保持している行番号が前に振った行番号+1、あるいは変わっていない場合は特に
                    // 変更しない。飛んでる場合だけ変更する
                    if (Util.ToInt(aryTaishakuRow[j]["GYO_BANGO"]) > prevRowNo + 1)
                    {
                        aryTaishakuRow[j]["GYO_BANGO"] = prevRowNo + 1;
                    }

                    prevRowNo = Util.ToInt(aryTaishakuRow[j]["GYO_BANGO"]);
                }
            }
            else
            {
                // 複合仕訳でない場合、、支払先単位で貸借それぞれ上に詰める
                aryAllRow = dtTaishakuData.Select(null, "GYO_BANGO, MEISAI_KUBUN");
                prevKaiinNo = string.Empty;
                prevRowNo = 0;

                for (int j = 0; j < aryAllRow.Length; j++)
                {
                    if (!prevKaiinNo.Equals(Util.ToString(aryAllRow[j]["SHIHARAISAKI_CD"])))
                    {
                        // 借方
                        aryTaishakuRow = dtTaishakuData.Select(
                            "SHIHARAISAKI_CD = " + Util.ToString(aryAllRow[j]["SHIHARAISAKI_CD"]) + " AND TAISHAKU_KUBUN = 1",
                            "GYO_BANGO, MEISAI_KUBUN");
                        if (j == 0)
                        {
                            prevRowNo = 0;
                        }
                        else
                        {
                            prevRowNo = Util.ToInt(aryAllRow[j - 1]["GYO_BANGO"]);
                        }
                        for (int k = 0; k < aryTaishakuRow.Length; k++)
                        {
                            // 現在保持している行番号が前に振った行番号+1、あるいは変わっていない場合は特に
                            // 変更しない。飛んでる場合だけ変更する
                            if (Util.ToInt(aryTaishakuRow[k]["GYO_BANGO"]) > prevRowNo + 1)
                            {
                                aryTaishakuRow[k]["GYO_BANGO"] = prevRowNo + 1;
                            }

                            prevRowNo = Util.ToInt(aryTaishakuRow[k]["GYO_BANGO"]);
                        }

                        // 貸方
                        aryTaishakuRow = dtTaishakuData.Select(
                            "SHIHARAISAKI_CD = " + Util.ToString(aryAllRow[j]["SHIHARAISAKI_CD"]) + " AND TAISHAKU_KUBUN = 2",
                            "GYO_BANGO, MEISAI_KUBUN");
                        if (j == 0)
                        {
                            prevRowNo = 0;
                        }
                        else
                        {
                            prevRowNo = Util.ToInt(aryAllRow[j - 1]["GYO_BANGO"]);
                        }
                        for (int k = 0; k < aryTaishakuRow.Length; k++)
                        {
                            // 現在保持している行番号が前に振った行番号+1、あるいは変わっていない場合は特に
                            // 変更しない。飛んでる場合だけ変更する
                            if (Util.ToInt(aryTaishakuRow[k]["GYO_BANGO"]) > prevRowNo + 1)
                            {
                                aryTaishakuRow[k]["GYO_BANGO"] = prevRowNo + 1;
                            }

                            prevRowNo = Util.ToInt(aryTaishakuRow[k]["GYO_BANGO"]);
                        }
                    }

                    prevKaiinNo = Util.ToString(aryAllRow[j]["SHIHARAISAKI_CD"]);
                }
            }

            // 最後に整列して格納
            aryFormattedData = dtTaishakuData.Select(null, "GYO_BANGO, TAISHAKU_KUBUN, MEISAI_KUBUN");
            dtFormattedData = dtTaishakuData.Clone();
            for (int j = 0; j < aryFormattedData.Length; j++)
            {
                dtFormattedData.ImportRow(aryFormattedData[j]);
            }

            dsTaishakuData.Tables.Add(dtFormattedData);

            return dsTaishakuData;
        }

        /// <summary>
        /// 仕訳データを作成
        /// </summary>
        /// <param name="mode">モード(1:登録、2:更新、3:削除)</param>
        /// <param name="packDpyNo">一括伝票番号</param>
        /// <param name="condition">条件画面で入力された条件</param>
        /// <param name="swkData">仕訳データ</param>
        /// <returns>更新の成否(true:成功、false:失敗)</returns>
        /// <remarks>
        /// コミットやロールバックは呼び出し元で
        /// 例外発生時は親クラスで捕捉
        /// </remarks>
        public bool MakeSwkData(int mode, int packDpyNo,
            Hashtable condition, DataSet swkData)
        {
            try
            {
                DataTable dtDenpyo; // 各伝票のデータを格納するDataTable
                int swkDpyNo = 0;       // 仕訳伝票番号
                DataTable dtTemp;   // 汎用的にテンポラリとして使うDataTable

                // 自動仕訳履歴から仕訳伝票番号を取得する
                DataTable dtDpyNo = GetChkDpyNo(packDpyNo);

                if (mode == 1)
                {
                    // (新規モードの場合のみ)仕訳伝票番号を取得する
                    packDpyNo = this._dba.GetHNDenpyoNo(this._uInfo, this.ShishoCode, 2, 1);

                    // (新規モードの場合のみ)自動仕訳履歴の存在チェックをする
                    if (Util.ToInt(GetJidoSwkRirekiExistChk(packDpyNo).Rows[0]["件数"]) > 0)
                    {
                        // 更新失敗
                        return false;
                    }

                    // (新規モードの場合のみ)伝票番号のUPDATE
                    this._dba.UpdateHNDenpyoNo(this._uInfo, this.ShishoCode, 2, 1, packDpyNo);
                }

                // 仕訳伝票のDELETE
                DeleteTB_ZM_SHIWAKE_DENPYO(packDpyNo);

                // 仕訳明細のDELETE
                DeleteTB_ZM_SHIWAKE_MEISAI(packDpyNo);

                // 自動仕訳履歴のDELETE
                DeleteTB_HN_ZIDO_SHIWAKE_RIREKI(packDpyNo);

                // 取引伝票への取消処理
                UpdateCancelTB_HN_TORIHIKI_DENPYO(packDpyNo);

                if (mode != 3)
                {
                    // [伝票の件数分]↓
                    for (int i = 0; i < swkData.Tables.Count; i++)
                    {
                        dtDenpyo = swkData.Tables[i];

                        // 新規に採番する必要があるときのみ伝票番号のMAX値を取得
                        if ((i + 1) > dtDpyNo.Rows.Count)
                        {
                            dtTemp = GetSaisyuDenpyoNo();
                            swkDpyNo = Util.ToInt(dtTemp.Rows[0]["最終伝票番号"]) + 1;
                        }
                        else
                        {
                            //自動仕訳履歴から仕訳伝票番号からの伝番
                            swkDpyNo = Util.ToInt(dtDpyNo.Rows[i]["SHIWAKE_DENPYO_BANGO"]);
                        }

                        // 仕訳伝票の存在チェック
                        if (GetChkShiwakeDenpyo(swkDpyNo).Rows.Count > 0)
                        {
                            // 更新失敗
                            return false;
                        }

                        // 仕訳伝票の登録
                        InsertTB_ZM_SHIWAKE_DENPYO(swkDpyNo, condition);

                        // [明細(貸借それぞれ、課税対象の仕訳は消費税も)の件数分]↓
                        for (int j = 0; j < dtDenpyo.Rows.Count; j++)
                        {
                            // 明細行の存在チェック
                            if (GetChkShiwakeMeisai(swkDpyNo, dtDenpyo.Rows[j]).Rows.Count > 0)
                            {
                                // 更新失敗
                                return false;
                            }

                            // 明細行の登録
                            InsertTB_ZM_SHIWAKE_MEISAI(swkDpyNo, condition, dtDenpyo.Rows[j]);
                        }
                        // [明細(貸借それぞれ、課税対象の仕訳は消費税も)の件数分]↑

                        // 自動仕訳履歴の存在チェック
                        if (GetChkJidoSwkRireki(packDpyNo, swkDpyNo).Rows.Count > 0)
                        {
                            // 更新失敗
                            return false;
                        }

                        // 自動仕訳履歴の登録
                        InsertTB_HN_ZIDO_SHIWAKE_RIREKI(condition, packDpyNo, swkDpyNo);

                    }
                    // [伝票の件数分]↑

                    // 財務の伝票番号テーブルへの更新
                    this._dba.UpdateZMDenpyoNo(this._uInfo, this.ShishoCode, 0, swkDpyNo);

                    // 取引伝票テーブルに一括伝票番号を更新
                    UpdateTorihikiDenpyo(packDpyNo, condition);
                }

                return true;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 取引区分によってwhere句を切り替える
        /// </summary>
        /// <param name="condition">条件画面の入力値</param>
        /// <returns>取引区分に関する抽出条件</returns>
        private string CreateWhereByToriKbn(Hashtable condition)
        {
            int kakeTori = 0;   // 掛取引
            int kakeHen = 0;    // 掛返品
            int genTori = 0;    // 現金取引
            int genHen = 0;     // 現金返品
            int condKind = 0;   // WHERE句の種類を切る分けるための数値

            StringBuilder cond = new StringBuilder();

            switch ((KBDB1021.SKbn)condition["SakuseiKbn"])
            {
                case KBDB1021.SKbn.Genkin:
                    genTori = Util.ToInt(this._config.LoadPgConfig(Constants.SubSys.Kob, "KBDB1021", "Setting", "GenGenToriDp"));
                    genHen = Util.ToInt(this._config.LoadPgConfig(Constants.SubSys.Kob, "KBDB1021", "Setting", "GenGenHnDp"));
                    break;

                case KBDB1021.SKbn.GenkinKake:
                    kakeTori = Util.ToInt(this._config.LoadPgConfig(Constants.SubSys.Kob, "KBDB1021", "Setting", "GenKakeKakeToriDp"));
                    kakeHen = Util.ToInt(this._config.LoadPgConfig(Constants.SubSys.Kob, "KBDB1021", "Setting", "GenKakeKakeHnDp"));
                    genTori = Util.ToInt(this._config.LoadPgConfig(Constants.SubSys.Kob, "KBDB1021", "Setting", "GenKakeGenToriDp"));
                    genHen = Util.ToInt(this._config.LoadPgConfig(Constants.SubSys.Kob, "KBDB1021", "Setting", "GenKakeGenHnDp"));
                    break;

                case KBDB1021.SKbn.Shimebi:
                    kakeTori = Util.ToInt(this._config.LoadPgConfig(Constants.SubSys.Kob, "KBDB1021", "Setting", "SmbKakeToriDp"));
                    kakeHen = Util.ToInt(this._config.LoadPgConfig(Constants.SubSys.Kob, "KBDB1021", "Setting", "SmbKakeHnDp"));
                    genTori = Util.ToInt(this._config.LoadPgConfig(Constants.SubSys.Kob, "KBDB1021", "Setting", "SmbGenToriDp"));
                    genHen = Util.ToInt(this._config.LoadPgConfig(Constants.SubSys.Kob, "KBDB1021", "Setting", "SmbGenHnDp"));
                    break;
            }

            // WHERE句の種類を設定
            condKind = 8 * kakeTori + 4 * kakeHen + 2 * genTori + 1 * genHen;

            switch (condKind)
            {
                case 1:
                    // 現金返品のみ
                    cond.Append("AND (B.TORIHIKI_KUBUN1 = 2 ");
                    cond.Append("     AND B.TORIHIKI_KUBUN2 = 2) ");
                    break;

                case 2:
                    // 現金取引のみ
                    cond.Append("AND (B.TORIHIKI_KUBUN1 = 2 ");
                    cond.Append("     AND B.TORIHIKI_KUBUN2 IN (1,3,4,5,6,7,8,9)) ");
                    break;

                case 3:
                    // 現金取引・現金返品
                    cond.Append("AND (B.TORIHIKI_KUBUN1 = 2 ");
                    cond.Append("     AND B.TORIHIKI_KUBUN2 BETWEEN 1 AND 9) ");
                    break;

                case 4:
                    // 掛返品のみ
                    cond.Append("AND (B.TORIHIKI_KUBUN1 IN (1,3,4) ");
                    cond.Append("     AND B.TORIHIKI_KUBUN2 = 2) ");
                    break;

                case 5:
                    // 掛返品・現金返品
                    cond.Append("AND (B.TORIHIKI_KUBUN1 BETWEEN 1 AND 4 ");
                    cond.Append("     AND B.TORIHIKI_KUBUN2 = 2) ");
                    break;

                case 6:
                    // 掛返品・現金取引
                    cond.Append("AND ((B.TORIHIKI_KUBUN1 IN (1,3,4) ");
                    cond.Append("      AND B.TORIHIKI_KUBUN2 = 2) ");
                    cond.Append("     OR ");
                    cond.Append("     (B.TORIHIKI_KUBUN1 = 2 ");
                    cond.Append("      AND B.TORIHIKI_KUBUN2 IN (1,3,4,5,6,7,8,9))) ");
                    break;

                case 7:
                    // 掛返品・現金取引・現金返品
                    cond.Append("AND ((B.TORIHIKI_KUBUN1 IN (1,3,4) ");
                    cond.Append("      AND B.TORIHIKI_KUBUN2 = 2) ");
                    cond.Append("     OR ");
                    cond.Append("     (B.TORIHIKI_KUBUN1 = 2 ");
                    cond.Append("      AND B.TORIHIKI_KUBUN2 BETWEEN 1 AND 9)) ");
                    break;

                case 8:
                    // 掛取引のみ
                    cond.Append("AND (B.TORIHIKI_KUBUN1 IN (1,3,4) ");
                    cond.Append("     AND B.TORIHIKI_KUBUN2 IN (1,3,4,5,6,7,8,9)) ");
                    break;

                case 9:
                    // 掛取引・現金返品
                    cond.Append("AND ((B.TORIHIKI_KUBUN1 IN (1,3,4) ");
                    cond.Append("      AND B.TORIHIKI_KUBUN2 IN (1,3,4,5,6,7,8,9)) ");
                    cond.Append("     OR ");
                    cond.Append("     (B.TORIHIKI_KUBUN1 = 2 ");
                    cond.Append("      AND B.TORIHIKI_KUBUN2 = 2)) ");
                    break;

                case 10:
                    // 掛取引・現金取引
                    cond.Append("AND (B.TORIHIKI_KUBUN1 BETWEEN 1 AND 4 ");
                    cond.Append("     AND B.TORIHIKI_KUBUN2 IN (1,3,4,5,6,7,8,9)) ");
                    break;

                case 11:
                    // 掛取引・現金取引・現金返品
                    cond.Append("AND ((B.TORIHIKI_KUBUN1 IN (1,3,4) ");
                    cond.Append("      AND B.TORIHIKI_KUBUN2 IN (1,3,4,5,6,7,8,9)) ");
                    cond.Append("     OR ");
                    cond.Append("     (B.TORIHIKI_KUBUN1 = 2 ");
                    cond.Append("      AND B.TORIHIKI_KUBUN2 BETWEEN 1 AND 9)) ");
                    break;

                case 12:
                    // 掛取引・掛返品
                    cond.Append("AND (B.TORIHIKI_KUBUN1 IN (1,3,4) ");
                    cond.Append("     AND B.TORIHIKI_KUBUN2 BETWEEN 1 AND 9) ");
                    break;

                case 13:
                    // 掛取引・掛返品・現金返品
                    cond.Append("AND ((B.TORIHIKI_KUBUN1 IN (1,3,4) ");
                    cond.Append("      AND B.TORIHIKI_KUBUN2 BETWEEN 1 AND 9) ");
                    cond.Append("     OR ");
                    cond.Append("     (B.TORIHIKI_KUBUN1 = 2 ");
                    cond.Append("      AND B.TORIHIKI_KUBUN2 = 2)) ");
                    break;

                case 14:
                    // 掛取引・掛返品・現金取引
                    cond.Append("AND ((B.TORIHIKI_KUBUN1 IN (1,3,4) ");
                    cond.Append("      AND B.TORIHIKI_KUBUN2 BETWEEN 1 AND 9) ");
                    cond.Append("     OR ");
                    cond.Append("     (B.TORIHIKI_KUBUN1 = 2 ");
                    cond.Append("      AND B.TORIHIKI_KUBUN2 IN (1,3,4,5,6,7,8,9))) ");
                    break;

                case 15:
                    // 掛取引・掛返品・現金取引・現金返品
                    cond.Append("AND (B.TORIHIKI_KUBUN1 BETWEEN 1 AND 4 ");
                    cond.Append("     AND B.TORIHIKI_KUBUN2 BETWEEN 1 AND 9) ");
                    break;
            }

            return cond.ToString();
        }

        /// <summary>
        /// 自動仕訳設定Ａから情報を取得する
        /// </summary>
        /// <param name="dtSwkSettingA">予め取得した自動仕訳設定Ａ</param>
        /// <param name="toriKbn1">取引区分１</param>
        /// <param name="toriKbn2">取引区分２</param>
        /// <returns>設定レコード</returns>
        private DataRow GetSwkSettingARec(DataTable dtSwkSettingA, int toriKbn1, int toriKbn2)
        {
            int swkCd = toriKbn1 * 10 + toriKbn2;
            DataRow[] aryRow = dtSwkSettingA.Select("仕訳コード = " + Util.ToString(swkCd));

            if (aryRow.Length > 0)
            {
                return aryRow[0];
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 自動仕訳設定Ｂから情報を取得する
        /// </summary>
        /// <param name="dtSwkSettingB">予め取得した自動仕訳設定Ｂ</param>
        /// <param name="swkCd">仕訳コード</param>
        /// <returns>設定レコード</returns>
        private DataRow GetSwkSettingBRec(DataTable dtSwkSettingB, int swkCd)
        {
            DataRow[] aryRow = dtSwkSettingB.Select("仕訳コード = " + Util.ToString(swkCd));

            if (aryRow.Length > 0)
            {
                return aryRow[0];
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 消費税設定から情報を取得する
        /// </summary>
        /// <param name="dtZeiSetting">dtZeiSetting</param>
        /// <param name="ukeBriKbn">仮受／仮払区分(1:仮受、2:仮払)</param>
        /// <returns></returns>
        private DataRow GetZeiSettingRow(DataTable dtZeiSetting, int ukeBriKbn)
        {
            DataRow[] aryRow = dtZeiSetting.Select("KBN = " + Util.ToString(ukeBriKbn));

            if (aryRow.Length > 0)
            {
                return aryRow[0];
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 伝票番号の存在チェック
        /// </summary>
        /// <param name="packDpyNo">一括伝票番号</param>
        /// <returns>取得したデータ</returns>
        private DataTable GetChkDpyNo(int packDpyNo)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT ");
            sql.Append("  DENPYO_BANGO ");
            sql.Append(" ,SHIWAKE_DENPYO_BANGO ");
            sql.Append("FROM ");
            sql.Append("  TB_HN_ZIDO_SHIWAKE_RIREKI ");
            sql.Append("WHERE ");
            sql.Append("    KAISHA_CD    = @KAISHA_CD ");
            sql.Append("AND SHISHO_CD    = @SHISHO_CD ");
            sql.Append("AND KAIKEI_NENDO = @KAIKEI_NENDO ");
            sql.Append("AND DENPYO_KUBUN = 2 ");
            sql.Append("AND DENPYO_BANGO = @DENPYO_BANGO ");
            sql.Append("ORDER BY ");
            sql.Append("  DENPYO_BANGO ");
            sql.Append(" ,SHIWAKE_DENPYO_BANGO ");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
            dpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 6, packDpyNo);

            DataTable dtResult = this._dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            return dtResult;
        }

        /// <summary>
        /// 自動仕訳履歴の存在チェック
        /// </summary>
        /// <param name="packDpyNo">一括伝票番号</param>
        /// <returns></returns>
        private DataTable GetJidoSwkRirekiExistChk(int packDpyNo)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT ");
            sql.Append("  COUNT(*) AS 件数 ");
            sql.Append("FROM ");
            sql.Append("  TB_HN_ZIDO_SHIWAKE_RIREKI ");
            sql.Append("WHERE ");
            sql.Append("    KAISHA_CD    = @KAISHA_CD ");
            sql.Append("AND SHISHO_CD    = @SHISHO_CD ");
            sql.Append("AND KAIKEI_NENDO = @KAIKEI_NENDO ");
            sql.Append("AND DENPYO_KUBUN = 2 ");
            sql.Append("AND DENPYO_BANGO = @DENPYO_BANGO ");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
            dpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 6, packDpyNo);

            DataTable dtResult = this._dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            return dtResult;
        }

        /// <summary>
        /// TB_ZM_SHIWAKE_DENPYOを削除する
        /// </summary>
        /// <param name="packDpyNo">一括伝票番号</param>
        /// <returns>更新件数</returns>
        private int DeleteTB_ZM_SHIWAKE_DENPYO(int packDpyNo)
        {
            StringBuilder where = new StringBuilder();
            where.Append("    KAISHA_CD = @KAISHA_CD ");
            where.Append("AND SHISHO_CD = @SHISHO_CD ");
            where.Append("AND KAIKEI_NENDO = @KAIKEI_NENDO ");
            where.Append("AND DENPYO_BANGO IN (SELECT DISTINCT ");
            where.Append("                       X.SHIWAKE_DENPYO_BANGO ");
            where.Append("                     FROM ");
            where.Append("                       TB_HN_ZIDO_SHIWAKE_RIREKI AS X ");
            where.Append("                     WHERE ");
            where.Append("                         X.KAISHA_CD = @KAISHA_CD ");
            where.Append("                     AND X.SHISHO_CD = @SHISHO_CD ");
            where.Append("                     AND X.KAIKEI_NENDO = @KAIKEI_NENDO ");
            where.Append("                     AND X.DENPYO_KUBUN = 2 ");
            where.Append("                     AND X.DENPYO_BANGO = @DENPYO_BANGO ");
            where.Append("                    ) ");

            DbParamCollection whereDpc = new DbParamCollection();
            whereDpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            whereDpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
            whereDpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
            whereDpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 6, packDpyNo);

            return this._dba.Delete("TB_ZM_SHIWAKE_DENPYO", where.ToString(), whereDpc);
        }

        /// <summary>
        /// TB_ZM_SHIWAKE_MEISAIを削除する
        /// </summary>
        /// <param name="packDpyNo">一括伝票番号</param>
        /// <returns>更新件数</returns>
        private int DeleteTB_ZM_SHIWAKE_MEISAI(int packDpyNo)
        {
            StringBuilder where = new StringBuilder();
            where.Append("    KAISHA_CD = @KAISHA_CD ");
            where.Append("AND SHISHO_CD = @SHISHO_CD ");
            where.Append("AND KAIKEI_NENDO = @KAIKEI_NENDO ");
            where.Append("AND DENPYO_BANGO IN (SELECT DISTINCT ");
            where.Append("                       X.SHIWAKE_DENPYO_BANGO ");
            where.Append("                     FROM ");
            where.Append("                       TB_HN_ZIDO_SHIWAKE_RIREKI AS X ");
            where.Append("                     WHERE ");
            where.Append("                         X.KAISHA_CD = @KAISHA_CD ");
            where.Append("                     AND X.SHISHO_CD = @SHISHO_CD ");
            where.Append("                     AND X.KAIKEI_NENDO = @KAIKEI_NENDO ");
            where.Append("                     AND X.DENPYO_KUBUN = 2 ");
            where.Append("                     AND X.DENPYO_BANGO = @DENPYO_BANGO ");
            where.Append("                    ) ");

            DbParamCollection whereDpc = new DbParamCollection();
            whereDpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            whereDpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
            whereDpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
            whereDpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 6, packDpyNo);

            return this._dba.Delete("TB_ZM_SHIWAKE_MEISAI", where.ToString(), whereDpc);
        }

        /// <summary>
        /// TB_HN_ZIDO_SHIWAKE_RIREKIを削除する
        /// </summary>
        /// <param name="packDpyNo">一括伝票番号</param>
        /// <returns>更新件数</returns>
        private int DeleteTB_HN_ZIDO_SHIWAKE_RIREKI(int packDpyNo)
        {
            StringBuilder where = new StringBuilder();
            where.Append("    KAISHA_CD = @KAISHA_CD ");
            where.Append("AND SHISHO_CD = @SHISHO_CD ");
            where.Append("AND KAIKEI_NENDO = @KAIKEI_NENDO ");
            where.Append("AND DENPYO_KUBUN = 2 ");
            where.Append("AND DENPYO_BANGO = @DENPYO_BANGO ");

            DbParamCollection whereDpc = new DbParamCollection();
            whereDpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            whereDpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
            whereDpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
            whereDpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 6, packDpyNo);

            return this._dba.Delete("TB_HN_ZIDO_SHIWAKE_RIREKI", where.ToString(), whereDpc);
        }

        /// <summary>
        /// 取引伝票テーブルへの取消処理を実行
        /// </summary>
        /// <param name="packDpyNo">取消対象の一括伝票番号</param>
        /// <returns>更新件数</returns>
        private int UpdateCancelTB_HN_TORIHIKI_DENPYO(int packDpyNo)
        {
            StringBuilder where = new StringBuilder();
            where.Append("    KAISHA_CD = @KAISHA_CD ");
            where.Append("AND SHISHO_CD = @SHISHO_CD ");
            where.Append("AND KAIKEI_NENDO = @KAIKEI_NENDO ");
            where.Append("AND DENPYO_KUBUN = 2 ");
            where.Append("AND IKKATSU_DENPYO_BANGO = @CNCL_IKKATSU_DENPYO_BANGO ");

            DbParamCollection updDpc = new DbParamCollection();
            updDpc.SetParam("@IKKATSU_DENPYO_BANGO", SqlDbType.Decimal, 6, 0);
            updDpc.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWTIME");

            DbParamCollection whereDpc = new DbParamCollection();
            whereDpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            whereDpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
            whereDpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
            whereDpc.SetParam("@CNCL_IKKATSU_DENPYO_BANGO", SqlDbType.Decimal, 6, packDpyNo);

            return this._dba.Update("TB_HN_TORIHIKI_DENPYO", updDpc, where.ToString(), whereDpc);
        }

        /// <summary>
        /// 最終伝票番号を取得する
        /// </summary>
        /// <returns>取得したデータ</returns>
        private DataTable GetSaisyuDenpyoNo()
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT ");
            sql.Append("  MAX(DENPYO_BANGO) AS 最終伝票番号 ");
            sql.Append("FROM ");
            sql.Append("  TB_ZM_SHIWAKE_DENPYO ");
            sql.Append("WHERE ");
            sql.Append("    KAISHA_CD = @KAISHA_CD ");
            sql.Append("AND SHISHO_CD = @SHISHO_CD ");
            sql.Append("AND KAIKEI_NENDO = @KAIKEI_NENDO ");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);

            DataTable dtResult = this._dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            return dtResult;
        }

        /// <summary>
        /// 仕訳伝票の存在チェックのためのデータ取得
        /// </summary>
        /// <param name="swkDpyNo">仕訳伝票番号</param>
        /// <returns>取得したデータ</returns>
        private DataTable GetChkShiwakeDenpyo(int swkDpyNo)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT ");
            sql.Append("  * ");
            sql.Append("FROM ");
            sql.Append("  TB_ZM_SHIWAKE_DENPYO ");
            sql.Append("WHERE ");
            sql.Append("    KAISHA_CD = @KAISHA_CD ");
            sql.Append("AND SHISHO_CD = @SHISHO_CD ");
            sql.Append("AND KAIKEI_NENDO = @KAIKEI_NENDO ");
            sql.Append("AND DENPYO_BANGO = @DENPYO_BANGO ");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
            dpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 6, swkDpyNo);

            DataTable dtResult = this._dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            return dtResult;
        }

        /// <summary>
        /// 仕訳伝票の登録
        /// </summary>
        /// <param name="swkDpyNo">仕訳伝票番号</param>
        /// <param name="condition">条件画面の入力内容</param>
        /// <returns>登録件数</returns>
        private int InsertTB_ZM_SHIWAKE_DENPYO(int swkDpyNo, Hashtable condition)
        {
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
            dpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 6, swkDpyNo);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
            dpc.SetParam("@DENPYO_DATE", SqlDbType.DateTime, condition["SwkDpyDt"]);
            dpc.SetParam("@TANTOSHA_CD", SqlDbType.Decimal, 4, condition["TantoCd"]);
            dpc.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWTIME");

            return this._dba.Insert("TB_ZM_SHIWAKE_DENPYO", dpc);
        }

        /// <summary>
        /// 仕訳明細の存在チェックのためのデータ取得
        /// </summary>
        /// <param name="swkDpyNo">仕訳伝票番号</param>
        /// <param name="meisaiInfo">明細情報</param>
        /// <returns>取得したデータ</returns>
        private DataTable GetChkShiwakeMeisai(int swkDpyNo, DataRow meisaiInfo)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT ");
            sql.Append("  * ");
            sql.Append("FROM ");
            sql.Append("  TB_ZM_SHIWAKE_MEISAI ");
            sql.Append("WHERE ");
            sql.Append("    KAISHA_CD = @KAISHA_CD ");
            sql.Append("AND SHISHO_CD = @SHISHO_CD ");
            sql.Append("AND KAIKEI_NENDO = @KAIKEI_NENDO ");
            sql.Append("AND DENPYO_BANGO = @DENPYO_BANGO ");
            sql.Append("AND GYO_BANGO = @GYO_BANGO ");
            sql.Append("AND TAISHAKU_KUBUN = @TAISHAKU_KUBUN ");
            sql.Append("AND MEISAI_KUBUN = @MEISAI_KUBUN ");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
            dpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 6, swkDpyNo);
            dpc.SetParam("@GYO_BANGO", SqlDbType.Decimal, 10, meisaiInfo["GYO_BANGO"]);
            dpc.SetParam("@TAISHAKU_KUBUN", SqlDbType.Decimal, 6, meisaiInfo["TAISHAKU_KUBUN"]);
            dpc.SetParam("@MEISAI_KUBUN", SqlDbType.Decimal, 6, meisaiInfo["MEISAI_KUBUN"]);

            DataTable dtResult = this._dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            return dtResult;
        }

        /// <summary>
        /// 仕訳明細の登録
        /// </summary>
        /// <param name="swkDpyNo">仕訳伝票番号</param>
        /// <param name="condition">条件画面の入力内容</param>
        /// <param name="meisaiInfo">明細情報</param>
        /// <returns>登録件数</returns>
        private int InsertTB_ZM_SHIWAKE_MEISAI(int swkDpyNo, Hashtable condition, DataRow meisaiInfo)
        {
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
            dpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 6, swkDpyNo);
            dpc.SetParam("@GYO_BANGO", SqlDbType.Decimal, 10, meisaiInfo["GYO_BANGO"]);
            dpc.SetParam("@TAISHAKU_KUBUN", SqlDbType.Decimal, 1, meisaiInfo["TAISHAKU_KUBUN"]);
            dpc.SetParam("@MEISAI_KUBUN", SqlDbType.Decimal, 1, meisaiInfo["MEISAI_KUBUN"]);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
            dpc.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 1, meisaiInfo["DENPYO_KUBUN"]);
            dpc.SetParam("@DENPYO_DATE", SqlDbType.DateTime, condition["SwkDpyDt"]);
            dpc.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 4, meisaiInfo["KANJO_KAMOKU_CD"]);
            dpc.SetParam("@HOJO_KAMOKU_CD", SqlDbType.Decimal, 4, meisaiInfo["HOJO_KAMOKU_CD"]);
            dpc.SetParam("@BUMON_CD", SqlDbType.Decimal, 4, meisaiInfo["BUMON_CD"]);
            //dpc.SetParam("@KOJI_CD", SqlDbType.Decimal, 4, 0);
            //dpc.SetParam("@KOSHU_CD", SqlDbType.Decimal, 4, 0);
            dpc.SetParam("@TEKIYO_CD", SqlDbType.Decimal, 4, meisaiInfo["TEKIYO_CD"]);
            dpc.SetParam("@TEKIYO", SqlDbType.VarChar, 40, meisaiInfo["TEKIYO"]);
            dpc.SetParam("@ZEIKOMI_KINGAKU", SqlDbType.Decimal, 15, meisaiInfo["ZEIKOMI_KINGAKU"]);
            dpc.SetParam("@ZEINUKI_KINGAKU", SqlDbType.Decimal, 15, meisaiInfo["ZEINUKI_KINGAKU"]);
            dpc.SetParam("@SHOHIZEI_KINGAKU", SqlDbType.Decimal, 15, meisaiInfo["SHOHIZEI_KINGAKU"]);
            dpc.SetParam("@ZEI_KUBUN", SqlDbType.Decimal, 2, meisaiInfo["ZEI_KUBUN"]);
            dpc.SetParam("@KAZEI_KUBUN", SqlDbType.Decimal, 1, meisaiInfo["KAZEI_KUBUN"]);
            dpc.SetParam("@TORIHIKI_KUBUN", SqlDbType.Decimal, 2, meisaiInfo["TORIHIKI_KUBUN"]);
            dpc.SetParam("@ZEI_RITSU", SqlDbType.Decimal, 4, 2, meisaiInfo["ZEI_RITSU"]);
            dpc.SetParam("@JIGYO_KUBUN", SqlDbType.Decimal, 1, meisaiInfo["JIGYO_KUBUN"]);
            dpc.SetParam("@SHOHIZEI_NYURYOKU_HOHO", SqlDbType.Decimal, 1, meisaiInfo["SHOHIZEI_NYURYOKU_HOHO"]);
            dpc.SetParam("@SHOHIZEI_HENKO", SqlDbType.Decimal, 1, meisaiInfo["SHOHIZEI_HENKO"]);
            dpc.SetParam("@KESSAN_KUBUN", SqlDbType.Decimal, 1, meisaiInfo["KESSAN_KUBUN"]);
            dpc.SetParam("@SHIWAKE_SAKUSEI_KUBUN", SqlDbType.Decimal, 1, meisaiInfo["SHIWAKE_SAKUSEI_KUBUN"]);
            dpc.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWTIME");

            return this._dba.Insert("TB_ZM_SHIWAKE_MEISAI", dpc);
        }

        /// <summary>
        /// 自動仕訳履歴の存在チェックのためのデータ取得
        /// </summary>
        /// <param name="packDpyNo">一括伝票番号</param>
        /// <param name="swkDpyNo">仕訳伝票番号</param>
        /// <returns>取得したデータ</returns>
        private DataTable GetChkJidoSwkRireki(int packDpyNo, int swkDpyNo)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT ");
            sql.Append("  A.DENPYO_BANGO            AS 伝票番号 ");
            sql.Append(" ,A.SHIWAKE_DENPYO_BANGO    AS 仕訳伝票番号 ");
            sql.Append(" ,A.DENPYO_DATE             AS 伝票日付 ");
            sql.Append(" ,A.SHORI_KUBUN             AS 処理区分 ");
            sql.Append(" ,CASE WHEN A.SHORI_KUBUN = 1 THEN '現金一括' ");
            sql.Append("       ELSE CASE WHEN A.SHORI_KUBUN = 2 THEN '月末一括' ");
            sql.Append("                 ELSE CASE WHEN A.SHORI_KUBUN = 3 THEN '締日一括' ");
            sql.Append("                           ELSE '' ");
            sql.Append("                      END ");
            sql.Append("            END ");
            sql.Append("  END AS 処理区分名称 ");
            sql.Append(" ,A.SHIMEBI                 AS 締日 ");
            sql.Append(" ,A.KAISHI_DENPYO_DATE      AS 開始伝票日付 ");
            sql.Append(" ,A.SHURYO_DENPYO_DATE      AS 終了伝票日付 ");
            sql.Append(" ,A.KAISHI_SEIKYUSAKI_CD    AS 開始請求先コード ");
            sql.Append(" ,CASE WHEN ISNULL(A.KAISHI_SEIKYUSAKI_CD, '') = '' THEN '先　頭' ELSE B.TORIHIKISAKI_NM END AS 開始請求先名 ");
            sql.Append(" ,A.SHURYO_SEIKYUSAKI_CD  AS 終了請求先コード ");
            sql.Append(" ,CASE WHEN ISNULL(A.SHURYO_SEIKYUSAKI_CD, '') = '' THEN '最　後' ELSE C.TORIHIKISAKI_NM END AS 終了請求先名 ");
            sql.Append(" ,A.TEKIYO_CD               AS 摘要コード ");
            sql.Append(" ,A.TEKIYO                  AS 摘要名 ");
            sql.Append("FROM ");
            sql.Append("  TB_HN_ZIDO_SHIWAKE_RIREKI AS A ");
            sql.Append("LEFT OUTER JOIN VI_HN_TORIHIKISAKI_JOHO AS B ");
            sql.Append("ON  A.KAISHA_CD            = B.KAISHA_CD ");
            sql.Append("AND A.KAISHI_SEIKYUSAKI_CD = B.TORIHIKISAKI_CD ");
            sql.Append("AND B.TORIHIKISAKI_KUBUN3  = 3 ");
            sql.Append("LEFT OUTER JOIN VI_HN_TORIHIKISAKI_JOHO AS C ");
            sql.Append("ON  A.KAISHA_CD            = C.KAISHA_CD ");
            sql.Append("AND A.SHURYO_SEIKYUSAKI_CD = C.TORIHIKISAKI_CD ");
            sql.Append("AND C.TORIHIKISAKI_KUBUN3  = 3 ");
            sql.Append("WHERE ");
            sql.Append("    A.KAISHA_CD            = @KAISHA_CD ");
            sql.Append("AND A.SHISHO_CD            = @SHISHO_CD ");
            sql.Append("AND A.KAIKEI_NENDO         = @KAIKEI_NENDO ");
            sql.Append("AND A.DENPYO_KUBUN         = 2 ");
            sql.Append("AND A.DENPYO_BANGO         = @DENPYO_BANGO ");
            sql.Append("AND A.SHIWAKE_DENPYO_BANGO = @SHIWAKE_DENPYO_BANGO ");
            sql.Append("ORDER BY ");
            sql.Append("  A.DENPYO_BANGO ");
            sql.Append(" ,A.SHIWAKE_DENPYO_BANGO ");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
            dpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 6, packDpyNo);
            dpc.SetParam("@SHIWAKE_DENPYO_BANGO", SqlDbType.Decimal, 6, swkDpyNo);

            DataTable dtResult = this._dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            return dtResult;
        }

        /// <summary>
        /// 自動仕訳履歴の登録
        /// </summary>
        /// <param name="condition">条件画面の入力内容</param>
        /// <param name="packDpyNo">一括伝票番号</param>
        /// <param name="swkDpyNo">仕訳伝票番号</param>
        /// <returns>登録件数</returns>
        private int InsertTB_HN_ZIDO_SHIWAKE_RIREKI(Hashtable condition, int packDpyNo, int swkDpyNo)
        {
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
            dpc.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 1, 2);
            dpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 6, packDpyNo);
            dpc.SetParam("@SHIWAKE_DENPYO_BANGO", SqlDbType.Decimal, 6, swkDpyNo);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
            dpc.SetParam("@DENPYO_DATE", SqlDbType.DateTime, condition["SwkDpyDt"]);
            dpc.SetParam("@TEKIYO_CD", SqlDbType.Decimal, 4, Util.ToInt(condition["TekiyoCd"]));
            dpc.SetParam("@TEKIYO", SqlDbType.VarChar, 40, ValChk.IsEmpty(condition["Tekiyo"]) ? DBNull.Value : condition["Tekiyo"]);
            dpc.SetParam("@SHORI_KUBUN", SqlDbType.Decimal, 2, condition["SakuseiKbn"]);
            dpc.SetParam("@SHIMEBI", SqlDbType.VarChar, 2, condition["Shimebi"]);
            dpc.SetParam("@KAISHI_DENPYO_DATE", SqlDbType.DateTime, condition["DpyDtFr"]);
            dpc.SetParam("@SHURYO_DENPYO_DATE", SqlDbType.DateTime, condition["DpyDtTo"]);
            dpc.SetParam("@KAISHI_SEIKYUSAKI_CD", SqlDbType.VarChar, 4, Util.ToInt(condition["ShiharaiCdFr"]) == 0 ? DBNull.Value : condition["ShiharaiCdFr"]);
            dpc.SetParam("@SHURYO_SEIKYUSAKI_CD", SqlDbType.VarChar, 4, Util.ToInt(condition["ShiharaiCdTo"]) == 9999 ? DBNull.Value : condition["ShiharaiCdTo"]);
            dpc.SetParam("@TANTOSHA_CD", SqlDbType.Decimal, 4, Util.ToInt(condition["TantoCd"]));
            dpc.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWTIME");

            return this._dba.Insert("TB_HN_ZIDO_SHIWAKE_RIREKI", dpc);
        }

        /// <summary>
        /// 取引伝票の更新
        /// </summary>
        /// <param name="packDpyNo">一括伝票番号</param>
        /// <param name="condition">条件画面の入力内容</param>
        /// <returns>更新件数</returns>
        private int UpdateTorihikiDenpyo(int packDpyNo, Hashtable condition)
        {
            StringBuilder where = new StringBuilder();
            where.Append("    KAISHA_CD = @KAISHA_CD ");
            where.Append("AND SHISHO_CD = @SHISHO_CD ");
            where.Append("AND KAIKEI_NENDO = @KAIKEI_NENDO ");
            where.Append("AND DENPYO_KUBUN = 2 ");
            where.Append("AND DENPYO_BANGO IN (SELECT DISTINCT ");
            where.Append("                       A.DENPYO_BANGO ");
            where.Append("                     FROM ");
            where.Append("                       TB_HN_TORIHIKI_MEISAI AS A ");
            where.Append("                     LEFT OUTER JOIN TB_HN_TORIHIKI_DENPYO AS B ");
            where.Append("                     ON  A.KAISHA_CD    = B.KAISHA_CD ");
            where.Append("                     AND A.SHISHO_CD    = B.SHISHO_CD ");
            where.Append("                     AND A.KAIKEI_NENDO = B.KAIKEI_NENDO ");
            where.Append("                     AND A.DENPYO_KUBUN = B.DENPYO_KUBUN ");
            where.Append("                     AND A.DENPYO_BANGO = B.DENPYO_BANGO ");
            where.Append("                     LEFT OUTER JOIN VI_HN_TORIHIKISAKI_JOHO AS C ");
            where.Append("                     ON  B.KAISHA_CD   = C.KAISHA_CD ");
            where.Append("                     AND B.TOKUISAKI_CD = C.TORIHIKISAKI_CD ");
            where.Append("                     LEFT OUTER JOIN TB_HN_ZIDO_SHIWAKE_SETTEI_B AS D ");
            where.Append("                     ON  A.KAISHA_CD    = D.KAISHA_CD ");
            where.Append("                     AND A.SHISHO_CD    = D.SHISHO_CD ");
            where.Append("                     AND A.DENPYO_KUBUN = D.DENPYO_KUBUN ");
            where.Append("                     AND A.SHIWAKE_CD   = D.SHIWAKE_CD ");
            where.Append("                     LEFT OUTER JOIN TB_HN_ZIDO_SHIWAKE_SETTEI_A AS H ");
            where.Append("                     ON  B.KAISHA_CD    = H.KAISHA_CD ");
            where.Append("                     AND B.SHISHO_CD    = H.SHISHO_CD ");
            where.Append("                     AND B.DENPYO_KUBUN = H.DENPYO_KUBUN ");
            where.Append("                     AND ((B.TORIHIKI_KUBUN1 * 10) + B.TORIHIKI_KUBUN2) = H.SHIWAKE_CD ");
            where.Append("                     WHERE ");
            where.Append("                         A.KAISHA_CD = @KAISHA_CD ");
            where.Append("                     AND A.SHISHO_CD = @SHISHO_CD ");
            where.Append("                     AND A.KAIKEI_NENDO = @KAIKEI_NENDO ");
            //where.Append("                     AND A.DENPYO_KUBUN = 1 ");
            where.Append("                     AND A.DENPYO_KUBUN = 2 ");
            where.Append("                     AND B.DENPYO_DATE BETWEEN @DENPYO_DATE_FR AND @DENPYO_DATE_TO ");
            where.Append("                     AND B.TOKUISAKI_CD BETWEEN @KAIIN_BANGO_FR AND @KAIIN_BANGO_TO ");
            where.Append("                     AND B.SHIWAKE_DENPYO_BANGO = 0 ");
            where.Append("                     AND A.BAIKA_KINGAKU <> 0 ");
            where.Append("                     AND ISNULL(B.IKKATSU_DENPYO_BANGO, 0) = 0 ");
            if ((KBDB1021.SKbn)condition["SakuseiKbn"] == KBDB1021.SKbn.Shimebi)
            {
                where.Append("AND C.SHIMEBI = @SHIMEBI ");
            }
            where.Append(CreateWhereByToriKbn(condition));
            where.Append("                     AND ISNULL(D.KANJO_KAMOKU_CD, 0) <> 0 ");
            where.Append("                     AND ISNULL(H.KANJO_KAMOKU_CD, 0) <> 0 ");
            where.Append("                    ) ");

            DbParamCollection whereDpc = new DbParamCollection();
            whereDpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            whereDpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
            whereDpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
            whereDpc.SetParam("@DENPYO_DATE_FR", SqlDbType.DateTime, Util.ToDate(condition["DpyDtFr"]));
            whereDpc.SetParam("@DENPYO_DATE_TO", SqlDbType.DateTime, Util.ToDate(condition["DpyDtTo"]));
            whereDpc.SetParam("@KAIIN_BANGO_FR", SqlDbType.VarChar, 4, Util.ToString(condition["ShiharaiCdFr"]));
            whereDpc.SetParam("@KAIIN_BANGO_TO", SqlDbType.VarChar, 4, Util.ToString(condition["ShiharaiCdTo"]));
            if ((KBDB1021.SKbn)condition["SakuseiKbn"] == KBDB1021.SKbn.Shimebi)
            {
                whereDpc.SetParam("@SHIMEBI", SqlDbType.Decimal, 2, Util.ToInt(condition["Shimebi"]));
            }

            DbParamCollection updDpc = new DbParamCollection();
            updDpc.SetParam("@IKKATSU_DENPYO_BANGO", SqlDbType.Decimal, 6, packDpyNo);
            updDpc.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWTIME");

            return this._dba.Update("TB_HN_TORIHIKI_DENPYO", updDpc, where.ToString(), whereDpc);
        }

        /// <summary>
        /// 税区分行の取得
        /// </summary>
        /// <param name="ZeiKbn">税区分</param>
        /// <returns></returns>
        private DataRow GetZeiKbnRow(int ZeiKbn)
        {
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@ZEI_KUBUN", SqlDbType.Decimal, 4, ZeiKbn);
            DataTable dt = this._dba.GetDataTableByConditionWithParams(
                "*",
                "TB_ZM_F_ZEI_KUBUN",
                "ZEI_KUBUN = @ZEI_KUBUN",
                dpc);
            if (dt.Rows.Count > 0)
                return dt.Rows[0];

            return null;
        }
        #endregion
    }
}
