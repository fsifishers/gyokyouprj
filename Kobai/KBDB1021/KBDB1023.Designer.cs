﻿namespace jp.co.fsi.kb.kbdb1021
{
    partial class KBDB1023
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvList = new System.Windows.Forms.DataGridView();
            this.lblTerm = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblGenkinShiire = new System.Windows.Forms.Label();
            this.lblGenkinZei = new System.Windows.Forms.Label();
            this.lblKakeShiire = new System.Windows.Forms.Label();
            this.lblKakeZei = new System.Windows.Forms.Label();
            this.lblKeiShiire = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvList)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Size = new System.Drawing.Size(622, 23);
            this.lblTitle.Text = "仕訳データ参照";
            // 
            // btnEsc
            // 
            this.btnEsc.Location = new System.Drawing.Point(3, 49);
            // 
            // btnF1
            // 
            this.btnF1.Location = new System.Drawing.Point(67, 49);
            // 
            // btnF2
            // 
            this.btnF2.Visible = false;
            // 
            // btnF3
            // 
            this.btnF3.Visible = false;
            // 
            // btnF4
            // 
            this.btnF4.Visible = false;
            // 
            // btnF5
            // 
            this.btnF5.Visible = false;
            // 
            // btnF7
            // 
            this.btnF7.Visible = false;
            // 
            // btnF6
            // 
            this.btnF6.Location = new System.Drawing.Point(131, 49);
            // 
            // btnF8
            // 
            this.btnF8.Visible = false;
            // 
            // btnF9
            // 
            this.btnF9.Visible = false;
            // 
            // btnF12
            // 
            this.btnF12.Visible = false;
            // 
            // btnF11
            // 
            this.btnF11.Visible = false;
            // 
            // btnF10
            // 
            this.btnF10.Visible = false;
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(5, 324);
            this.pnlDebug.Size = new System.Drawing.Size(639, 100);
            // 
            // dgvList
            // 
            this.dgvList.AllowUserToAddRows = false;
            this.dgvList.AllowUserToDeleteRows = false;
            this.dgvList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvList.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.dgvList.Location = new System.Drawing.Point(13, 29);
            this.dgvList.MultiSelect = false;
            this.dgvList.Name = "dgvList";
            this.dgvList.ReadOnly = true;
            this.dgvList.RowHeadersVisible = false;
            this.dgvList.RowTemplate.Height = 21;
            this.dgvList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvList.Size = new System.Drawing.Size(610, 316);
            this.dgvList.TabIndex = 22;
            // 
            // lblTerm
            // 
            this.lblTerm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTerm.ForeColor = System.Drawing.Color.Blue;
            this.lblTerm.Location = new System.Drawing.Point(14, 13);
            this.lblTerm.Name = "lblTerm";
            this.lblTerm.Size = new System.Drawing.Size(240, 12);
            this.lblTerm.TabIndex = 903;
            this.lblTerm.Text = " 平成 21年 4月 1日 ～ 平成 22年 3月31日";
            this.lblTerm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(13, 344);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(192, 20);
            this.label1.TabIndex = 904;
            this.label1.Text = "[ 合 計 ] ";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblGenkinShiire
            // 
            this.lblGenkinShiire.BackColor = System.Drawing.Color.White;
            this.lblGenkinShiire.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblGenkinShiire.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGenkinShiire.ForeColor = System.Drawing.Color.Black;
            this.lblGenkinShiire.Location = new System.Drawing.Point(204, 344);
            this.lblGenkinShiire.Name = "lblGenkinShiire";
            this.lblGenkinShiire.Size = new System.Drawing.Size(81, 20);
            this.lblGenkinShiire.TabIndex = 905;
            this.lblGenkinShiire.Text = "-999,999,999";
            this.lblGenkinShiire.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblGenkinZei
            // 
            this.lblGenkinZei.BackColor = System.Drawing.Color.White;
            this.lblGenkinZei.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblGenkinZei.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGenkinZei.ForeColor = System.Drawing.Color.Black;
            this.lblGenkinZei.Location = new System.Drawing.Point(284, 344);
            this.lblGenkinZei.Name = "lblGenkinZei";
            this.lblGenkinZei.Size = new System.Drawing.Size(81, 20);
            this.lblGenkinZei.TabIndex = 906;
            this.lblGenkinZei.Text = "-999,999,999";
            this.lblGenkinZei.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblKakeShiire
            // 
            this.lblKakeShiire.BackColor = System.Drawing.Color.White;
            this.lblKakeShiire.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblKakeShiire.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKakeShiire.ForeColor = System.Drawing.Color.Black;
            this.lblKakeShiire.Location = new System.Drawing.Point(364, 344);
            this.lblKakeShiire.Name = "lblKakeShiire";
            this.lblKakeShiire.Size = new System.Drawing.Size(81, 20);
            this.lblKakeShiire.TabIndex = 907;
            this.lblKakeShiire.Text = "-999,999,999";
            this.lblKakeShiire.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblKakeZei
            // 
            this.lblKakeZei.BackColor = System.Drawing.Color.White;
            this.lblKakeZei.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblKakeZei.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKakeZei.ForeColor = System.Drawing.Color.Black;
            this.lblKakeZei.Location = new System.Drawing.Point(444, 344);
            this.lblKakeZei.Name = "lblKakeZei";
            this.lblKakeZei.Size = new System.Drawing.Size(81, 20);
            this.lblKakeZei.TabIndex = 908;
            this.lblKakeZei.Text = "-999,999,999";
            this.lblKakeZei.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblKeiShiire
            // 
            this.lblKeiShiire.BackColor = System.Drawing.Color.White;
            this.lblKeiShiire.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblKeiShiire.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKeiShiire.ForeColor = System.Drawing.Color.Black;
            this.lblKeiShiire.Location = new System.Drawing.Point(524, 344);
            this.lblKeiShiire.Name = "lblKeiShiire";
            this.lblKeiShiire.Size = new System.Drawing.Size(81, 20);
            this.lblKeiShiire.TabIndex = 909;
            this.lblKeiShiire.Text = "-999,999,999";
            this.lblKeiShiire.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // KBDB1023
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(647, 427);
            this.Controls.Add(this.lblKeiShiire);
            this.Controls.Add(this.lblKakeZei);
            this.Controls.Add(this.lblKakeShiire);
            this.Controls.Add(this.lblGenkinZei);
            this.Controls.Add(this.lblGenkinShiire);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblTerm);
            this.Controls.Add(this.dgvList);
            this.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.Name = "KBDB1023";
            this.ShowFButton = true;
            this.ShowTitle = false;
            this.Text = "仕訳データ参照";
            this.Shown += new System.EventHandler(this.KBDB1023_Shown);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.dgvList, 0);
            this.Controls.SetChildIndex(this.lblTerm, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.lblGenkinShiire, 0);
            this.Controls.SetChildIndex(this.lblGenkinZei, 0);
            this.Controls.SetChildIndex(this.lblKakeShiire, 0);
            this.Controls.SetChildIndex(this.lblKakeZei, 0);
            this.Controls.SetChildIndex(this.lblKeiShiire, 0);
            this.pnlDebug.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvList;
        private System.Windows.Forms.Label lblTerm;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblGenkinShiire;
        private System.Windows.Forms.Label lblGenkinZei;
        private System.Windows.Forms.Label lblKakeShiire;
        private System.Windows.Forms.Label lblKakeZei;
        private System.Windows.Forms.Label lblKeiShiire;



    }
}