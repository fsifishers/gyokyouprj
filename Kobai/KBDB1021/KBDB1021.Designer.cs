﻿namespace jp.co.fsi.kb.kbdb1021
{
    partial class KBDB1021
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblMessage = new System.Windows.Forms.Label();
            this.gpbSakuseiKbn = new System.Windows.Forms.GroupBox();
            this.rdbShimebi = new System.Windows.Forms.RadioButton();
            this.rdbGenkinKake = new System.Windows.Forms.RadioButton();
            this.rdbGenkin = new System.Windows.Forms.RadioButton();
            this.gpbShimebi = new System.Windows.Forms.GroupBox();
            this.lblShimebiMemo = new System.Windows.Forms.Label();
            this.txtShimebi = new jp.co.fsi.common.controls.FsiTextBox();
            this.gpbDenpyoHizuke = new System.Windows.Forms.GroupBox();
            this.lblDpyDtToGengo = new System.Windows.Forms.Label();
            this.lblDpyDtFrGengo = new System.Windows.Forms.Label();
            this.lblDpyDtToDay = new System.Windows.Forms.Label();
            this.txtDpyDtToDay = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblDpyDtToMonth = new System.Windows.Forms.Label();
            this.txtDpyDtToMonth = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblDpyDtToJpYear = new System.Windows.Forms.Label();
            this.txtDpyDtToJpYear = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblDpyDtBetween = new System.Windows.Forms.Label();
            this.lblDpyDtFrDay = new System.Windows.Forms.Label();
            this.txtDpyDtFrDay = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblDpyDtFrMonth = new System.Windows.Forms.Label();
            this.txtDpyDtFrMonth = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblDpyDtFrJpYear = new System.Windows.Forms.Label();
            this.txtDpyDtFrJpYear = new jp.co.fsi.common.controls.FsiTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.gpbShiharaiCd = new System.Windows.Forms.GroupBox();
            this.lblShiharaiCdBetween = new System.Windows.Forms.Label();
            this.lblShiharaiNmTo = new System.Windows.Forms.Label();
            this.txtShiharaiCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShiharaiNmFr = new System.Windows.Forms.Label();
            this.txtShiharaiCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.gpbShiwakeDenpyo = new System.Windows.Forms.GroupBox();
            this.gpbTekiyo = new System.Windows.Forms.GroupBox();
            this.txtTekiyo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtTekiyoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.gpbSwkDpyHzk = new System.Windows.Forms.GroupBox();
            this.lblSwkDpyDtGengo = new System.Windows.Forms.Label();
            this.lblSwkDpyDtDay = new System.Windows.Forms.Label();
            this.txtSwkDpyDtDay = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblSwkDpyDtMonth = new System.Windows.Forms.Label();
            this.txtSwkDpyDtMonth = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblSwkDpyDtJpYear = new System.Windows.Forms.Label();
            this.txtSwkDpyDtJpYear = new jp.co.fsi.common.controls.FsiTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.gpbTantosha = new System.Windows.Forms.GroupBox();
            this.lblTantoNm = new System.Windows.Forms.Label();
            this.txtTantoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblUpdMode = new System.Windows.Forms.Label();
            this.gbxMizuageShisho = new System.Windows.Forms.GroupBox();
            this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMizuageShishoNm = new System.Windows.Forms.Label();
            this.lblMizuageShisho = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.gpbSakuseiKbn.SuspendLayout();
            this.gpbShimebi.SuspendLayout();
            this.gpbDenpyoHizuke.SuspendLayout();
            this.gpbShiharaiCd.SuspendLayout();
            this.gpbShiwakeDenpyo.SuspendLayout();
            this.gpbTekiyo.SuspendLayout();
            this.gpbSwkDpyHzk.SuspendLayout();
            this.gpbTantosha.SuspendLayout();
            this.gbxMizuageShisho.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Text = "仕入仕訳データ作成";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Size = new System.Drawing.Size(847, 100);
            // 
            // lblMessage
            // 
            this.lblMessage.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMessage.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMessage.ForeColor = System.Drawing.Color.Blue;
            this.lblMessage.Location = new System.Drawing.Point(130, 49);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(289, 20);
            this.lblMessage.TabIndex = 2;
            this.lblMessage.Text = "『会計期間第25期が選択されています』";
            this.lblMessage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // gpbSakuseiKbn
            // 
            this.gpbSakuseiKbn.Controls.Add(this.rdbShimebi);
            this.gpbSakuseiKbn.Controls.Add(this.rdbGenkinKake);
            this.gpbSakuseiKbn.Controls.Add(this.rdbGenkin);
            this.gpbSakuseiKbn.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gpbSakuseiKbn.Location = new System.Drawing.Point(12, 164);
            this.gpbSakuseiKbn.Name = "gpbSakuseiKbn";
            this.gpbSakuseiKbn.Size = new System.Drawing.Size(340, 60);
            this.gpbSakuseiKbn.TabIndex = 3;
            this.gpbSakuseiKbn.TabStop = false;
            this.gpbSakuseiKbn.Text = "作成区分";
            // 
            // rdbShimebi
            // 
            this.rdbShimebi.AutoSize = true;
            this.rdbShimebi.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdbShimebi.Location = new System.Drawing.Point(248, 23);
            this.rdbShimebi.Name = "rdbShimebi";
            this.rdbShimebi.Size = new System.Drawing.Size(81, 17);
            this.rdbShimebi.TabIndex = 2;
            this.rdbShimebi.TabStop = true;
            this.rdbShimebi.Text = "締日基準";
            this.rdbShimebi.UseVisualStyleBackColor = true;
            this.rdbShimebi.CheckedChanged += new System.EventHandler(this.rdbShimebi_CheckedChanged);
            // 
            // rdbGenkinKake
            // 
            this.rdbGenkinKake.AutoSize = true;
            this.rdbGenkinKake.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdbGenkinKake.Location = new System.Drawing.Point(123, 23);
            this.rdbGenkinKake.Name = "rdbGenkinKake";
            this.rdbGenkinKake.Size = new System.Drawing.Size(109, 17);
            this.rdbGenkinKake.TabIndex = 1;
            this.rdbGenkinKake.TabStop = true;
            this.rdbGenkinKake.Text = "現金、掛取引";
            this.rdbGenkinKake.UseVisualStyleBackColor = true;
            // 
            // rdbGenkin
            // 
            this.rdbGenkin.AutoSize = true;
            this.rdbGenkin.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdbGenkin.Location = new System.Drawing.Point(19, 23);
            this.rdbGenkin.Name = "rdbGenkin";
            this.rdbGenkin.Size = new System.Drawing.Size(81, 17);
            this.rdbGenkin.TabIndex = 0;
            this.rdbGenkin.TabStop = true;
            this.rdbGenkin.Text = "現金取引";
            this.rdbGenkin.UseVisualStyleBackColor = true;
            // 
            // gpbShimebi
            // 
            this.gpbShimebi.Controls.Add(this.lblShimebiMemo);
            this.gpbShimebi.Controls.Add(this.txtShimebi);
            this.gpbShimebi.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gpbShimebi.Location = new System.Drawing.Point(365, 164);
            this.gpbShimebi.Name = "gpbShimebi";
            this.gpbShimebi.Size = new System.Drawing.Size(142, 60);
            this.gpbShimebi.TabIndex = 4;
            this.gpbShimebi.TabStop = false;
            this.gpbShimebi.Text = "締日";
            // 
            // lblShimebiMemo
            // 
            this.lblShimebiMemo.BackColor = System.Drawing.Color.Silver;
            this.lblShimebiMemo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShimebiMemo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShimebiMemo.Location = new System.Drawing.Point(42, 21);
            this.lblShimebiMemo.Name = "lblShimebiMemo";
            this.lblShimebiMemo.Size = new System.Drawing.Size(80, 20);
            this.lblShimebiMemo.TabIndex = 1;
            this.lblShimebiMemo.Text = "99：末締め";
            this.lblShimebiMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShimebi
            // 
            this.txtShimebi.AutoSizeFromLength = true;
            this.txtShimebi.DisplayLength = null;
            this.txtShimebi.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShimebi.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtShimebi.Location = new System.Drawing.Point(20, 21);
            this.txtShimebi.MaxLength = 2;
            this.txtShimebi.Name = "txtShimebi";
            this.txtShimebi.Size = new System.Drawing.Size(20, 20);
            this.txtShimebi.TabIndex = 0;
            this.txtShimebi.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShimebi.Validating += new System.ComponentModel.CancelEventHandler(this.txtShimebi_Validating);
            // 
            // gpbDenpyoHizuke
            // 
            this.gpbDenpyoHizuke.Controls.Add(this.lblDpyDtToGengo);
            this.gpbDenpyoHizuke.Controls.Add(this.lblDpyDtFrGengo);
            this.gpbDenpyoHizuke.Controls.Add(this.lblDpyDtToDay);
            this.gpbDenpyoHizuke.Controls.Add(this.txtDpyDtToDay);
            this.gpbDenpyoHizuke.Controls.Add(this.lblDpyDtToMonth);
            this.gpbDenpyoHizuke.Controls.Add(this.txtDpyDtToMonth);
            this.gpbDenpyoHizuke.Controls.Add(this.lblDpyDtToJpYear);
            this.gpbDenpyoHizuke.Controls.Add(this.txtDpyDtToJpYear);
            this.gpbDenpyoHizuke.Controls.Add(this.lblDpyDtBetween);
            this.gpbDenpyoHizuke.Controls.Add(this.lblDpyDtFrDay);
            this.gpbDenpyoHizuke.Controls.Add(this.txtDpyDtFrDay);
            this.gpbDenpyoHizuke.Controls.Add(this.lblDpyDtFrMonth);
            this.gpbDenpyoHizuke.Controls.Add(this.txtDpyDtFrMonth);
            this.gpbDenpyoHizuke.Controls.Add(this.lblDpyDtFrJpYear);
            this.gpbDenpyoHizuke.Controls.Add(this.txtDpyDtFrJpYear);
            this.gpbDenpyoHizuke.Controls.Add(this.label2);
            this.gpbDenpyoHizuke.Controls.Add(this.label3);
            this.gpbDenpyoHizuke.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gpbDenpyoHizuke.Location = new System.Drawing.Point(12, 236);
            this.gpbDenpyoHizuke.Name = "gpbDenpyoHizuke";
            this.gpbDenpyoHizuke.Size = new System.Drawing.Size(495, 60);
            this.gpbDenpyoHizuke.TabIndex = 5;
            this.gpbDenpyoHizuke.TabStop = false;
            this.gpbDenpyoHizuke.Text = "伝票日付範囲";
            // 
            // lblDpyDtToGengo
            // 
            this.lblDpyDtToGengo.BackColor = System.Drawing.Color.Silver;
            this.lblDpyDtToGengo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDpyDtToGengo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDpyDtToGengo.Location = new System.Drawing.Point(242, 24);
            this.lblDpyDtToGengo.Name = "lblDpyDtToGengo";
            this.lblDpyDtToGengo.Size = new System.Drawing.Size(39, 20);
            this.lblDpyDtToGengo.TabIndex = 903;
            this.lblDpyDtToGengo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblDpyDtFrGengo
            // 
            this.lblDpyDtFrGengo.BackColor = System.Drawing.Color.Silver;
            this.lblDpyDtFrGengo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDpyDtFrGengo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDpyDtFrGengo.Location = new System.Drawing.Point(12, 24);
            this.lblDpyDtFrGengo.Name = "lblDpyDtFrGengo";
            this.lblDpyDtFrGengo.Size = new System.Drawing.Size(39, 20);
            this.lblDpyDtFrGengo.TabIndex = 902;
            this.lblDpyDtFrGengo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblDpyDtToDay
            // 
            this.lblDpyDtToDay.BackColor = System.Drawing.Color.Silver;
            this.lblDpyDtToDay.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDpyDtToDay.Location = new System.Drawing.Point(386, 24);
            this.lblDpyDtToDay.Name = "lblDpyDtToDay";
            this.lblDpyDtToDay.Size = new System.Drawing.Size(18, 20);
            this.lblDpyDtToDay.TabIndex = 14;
            this.lblDpyDtToDay.Text = "日";
            this.lblDpyDtToDay.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDpyDtToDay
            // 
            this.txtDpyDtToDay.AutoSizeFromLength = true;
            this.txtDpyDtToDay.DisplayLength = null;
            this.txtDpyDtToDay.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDpyDtToDay.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtDpyDtToDay.Location = new System.Drawing.Point(364, 24);
            this.txtDpyDtToDay.MaxLength = 2;
            this.txtDpyDtToDay.Name = "txtDpyDtToDay";
            this.txtDpyDtToDay.Size = new System.Drawing.Size(20, 20);
            this.txtDpyDtToDay.TabIndex = 6;
            this.txtDpyDtToDay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDpyDtToDay.Validating += new System.ComponentModel.CancelEventHandler(this.txtDpyDtToDay_Validating);
            // 
            // lblDpyDtToMonth
            // 
            this.lblDpyDtToMonth.BackColor = System.Drawing.Color.Silver;
            this.lblDpyDtToMonth.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDpyDtToMonth.Location = new System.Drawing.Point(345, 24);
            this.lblDpyDtToMonth.Name = "lblDpyDtToMonth";
            this.lblDpyDtToMonth.Size = new System.Drawing.Size(18, 20);
            this.lblDpyDtToMonth.TabIndex = 12;
            this.lblDpyDtToMonth.Text = "月";
            this.lblDpyDtToMonth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDpyDtToMonth
            // 
            this.txtDpyDtToMonth.AutoSizeFromLength = true;
            this.txtDpyDtToMonth.DisplayLength = null;
            this.txtDpyDtToMonth.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDpyDtToMonth.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtDpyDtToMonth.Location = new System.Drawing.Point(323, 24);
            this.txtDpyDtToMonth.MaxLength = 2;
            this.txtDpyDtToMonth.Name = "txtDpyDtToMonth";
            this.txtDpyDtToMonth.Size = new System.Drawing.Size(20, 20);
            this.txtDpyDtToMonth.TabIndex = 5;
            this.txtDpyDtToMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDpyDtToMonth.Validating += new System.ComponentModel.CancelEventHandler(this.txtDpyDtToMonth_Validating);
            // 
            // lblDpyDtToJpYear
            // 
            this.lblDpyDtToJpYear.BackColor = System.Drawing.Color.Silver;
            this.lblDpyDtToJpYear.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDpyDtToJpYear.Location = new System.Drawing.Point(304, 24);
            this.lblDpyDtToJpYear.Name = "lblDpyDtToJpYear";
            this.lblDpyDtToJpYear.Size = new System.Drawing.Size(18, 20);
            this.lblDpyDtToJpYear.TabIndex = 10;
            this.lblDpyDtToJpYear.Text = "年";
            this.lblDpyDtToJpYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDpyDtToJpYear
            // 
            this.txtDpyDtToJpYear.AutoSizeFromLength = true;
            this.txtDpyDtToJpYear.DisplayLength = null;
            this.txtDpyDtToJpYear.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDpyDtToJpYear.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtDpyDtToJpYear.Location = new System.Drawing.Point(282, 24);
            this.txtDpyDtToJpYear.MaxLength = 2;
            this.txtDpyDtToJpYear.Name = "txtDpyDtToJpYear";
            this.txtDpyDtToJpYear.Size = new System.Drawing.Size(20, 20);
            this.txtDpyDtToJpYear.TabIndex = 4;
            this.txtDpyDtToJpYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDpyDtToJpYear.Validating += new System.ComponentModel.CancelEventHandler(this.txtDpyDtToJpYear_Validating);
            // 
            // lblDpyDtBetween
            // 
            this.lblDpyDtBetween.BackColor = System.Drawing.Color.Transparent;
            this.lblDpyDtBetween.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDpyDtBetween.Location = new System.Drawing.Point(200, 24);
            this.lblDpyDtBetween.Name = "lblDpyDtBetween";
            this.lblDpyDtBetween.Size = new System.Drawing.Size(20, 20);
            this.lblDpyDtBetween.TabIndex = 7;
            this.lblDpyDtBetween.Text = "～";
            this.lblDpyDtBetween.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblDpyDtFrDay
            // 
            this.lblDpyDtFrDay.BackColor = System.Drawing.Color.Silver;
            this.lblDpyDtFrDay.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDpyDtFrDay.Location = new System.Drawing.Point(157, 24);
            this.lblDpyDtFrDay.Name = "lblDpyDtFrDay";
            this.lblDpyDtFrDay.Size = new System.Drawing.Size(18, 20);
            this.lblDpyDtFrDay.TabIndex = 6;
            this.lblDpyDtFrDay.Text = "日";
            this.lblDpyDtFrDay.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDpyDtFrDay
            // 
            this.txtDpyDtFrDay.AutoSizeFromLength = true;
            this.txtDpyDtFrDay.DisplayLength = null;
            this.txtDpyDtFrDay.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDpyDtFrDay.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtDpyDtFrDay.Location = new System.Drawing.Point(135, 24);
            this.txtDpyDtFrDay.MaxLength = 2;
            this.txtDpyDtFrDay.Name = "txtDpyDtFrDay";
            this.txtDpyDtFrDay.Size = new System.Drawing.Size(20, 20);
            this.txtDpyDtFrDay.TabIndex = 3;
            this.txtDpyDtFrDay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDpyDtFrDay.Validating += new System.ComponentModel.CancelEventHandler(this.txtDpyDtFrDay_Validating);
            // 
            // lblDpyDtFrMonth
            // 
            this.lblDpyDtFrMonth.BackColor = System.Drawing.Color.Silver;
            this.lblDpyDtFrMonth.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDpyDtFrMonth.Location = new System.Drawing.Point(116, 24);
            this.lblDpyDtFrMonth.Name = "lblDpyDtFrMonth";
            this.lblDpyDtFrMonth.Size = new System.Drawing.Size(18, 20);
            this.lblDpyDtFrMonth.TabIndex = 4;
            this.lblDpyDtFrMonth.Text = "月";
            this.lblDpyDtFrMonth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDpyDtFrMonth
            // 
            this.txtDpyDtFrMonth.AutoSizeFromLength = true;
            this.txtDpyDtFrMonth.DisplayLength = null;
            this.txtDpyDtFrMonth.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDpyDtFrMonth.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtDpyDtFrMonth.Location = new System.Drawing.Point(94, 24);
            this.txtDpyDtFrMonth.MaxLength = 2;
            this.txtDpyDtFrMonth.Name = "txtDpyDtFrMonth";
            this.txtDpyDtFrMonth.Size = new System.Drawing.Size(20, 20);
            this.txtDpyDtFrMonth.TabIndex = 2;
            this.txtDpyDtFrMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDpyDtFrMonth.Validating += new System.ComponentModel.CancelEventHandler(this.txtDpyDtFrMonth_Validating);
            // 
            // lblDpyDtFrJpYear
            // 
            this.lblDpyDtFrJpYear.BackColor = System.Drawing.Color.Silver;
            this.lblDpyDtFrJpYear.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDpyDtFrJpYear.Location = new System.Drawing.Point(75, 24);
            this.lblDpyDtFrJpYear.Name = "lblDpyDtFrJpYear";
            this.lblDpyDtFrJpYear.Size = new System.Drawing.Size(18, 20);
            this.lblDpyDtFrJpYear.TabIndex = 2;
            this.lblDpyDtFrJpYear.Text = "年";
            this.lblDpyDtFrJpYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDpyDtFrJpYear
            // 
            this.txtDpyDtFrJpYear.AutoSizeFromLength = true;
            this.txtDpyDtFrJpYear.DisplayLength = null;
            this.txtDpyDtFrJpYear.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDpyDtFrJpYear.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtDpyDtFrJpYear.Location = new System.Drawing.Point(53, 24);
            this.txtDpyDtFrJpYear.MaxLength = 2;
            this.txtDpyDtFrJpYear.Name = "txtDpyDtFrJpYear";
            this.txtDpyDtFrJpYear.Size = new System.Drawing.Size(20, 20);
            this.txtDpyDtFrJpYear.TabIndex = 1;
            this.txtDpyDtFrJpYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDpyDtFrJpYear.Validating += new System.ComponentModel.CancelEventHandler(this.txtDpyDtFrJpYear_Validating);
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Silver;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Location = new System.Drawing.Point(10, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(167, 25);
            this.label2.TabIndex = 905;
            this.label2.Text = " ";
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Silver;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Location = new System.Drawing.Point(240, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(167, 25);
            this.label3.TabIndex = 906;
            this.label3.Text = " ";
            // 
            // gpbShiharaiCd
            // 
            this.gpbShiharaiCd.Controls.Add(this.lblShiharaiCdBetween);
            this.gpbShiharaiCd.Controls.Add(this.lblShiharaiNmTo);
            this.gpbShiharaiCd.Controls.Add(this.txtShiharaiCdTo);
            this.gpbShiharaiCd.Controls.Add(this.lblShiharaiNmFr);
            this.gpbShiharaiCd.Controls.Add(this.txtShiharaiCdFr);
            this.gpbShiharaiCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gpbShiharaiCd.Location = new System.Drawing.Point(12, 308);
            this.gpbShiharaiCd.Name = "gpbShiharaiCd";
            this.gpbShiharaiCd.Size = new System.Drawing.Size(495, 60);
            this.gpbShiharaiCd.TabIndex = 6;
            this.gpbShiharaiCd.TabStop = false;
            this.gpbShiharaiCd.Text = "支払先コード範囲";
            // 
            // lblShiharaiCdBetween
            // 
            this.lblShiharaiCdBetween.BackColor = System.Drawing.Color.Transparent;
            this.lblShiharaiCdBetween.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShiharaiCdBetween.Location = new System.Drawing.Point(242, 25);
            this.lblShiharaiCdBetween.Name = "lblShiharaiCdBetween";
            this.lblShiharaiCdBetween.Size = new System.Drawing.Size(20, 20);
            this.lblShiharaiCdBetween.TabIndex = 2;
            this.lblShiharaiCdBetween.Text = "～";
            this.lblShiharaiCdBetween.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblShiharaiNmTo
            // 
            this.lblShiharaiNmTo.BackColor = System.Drawing.Color.Silver;
            this.lblShiharaiNmTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShiharaiNmTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShiharaiNmTo.Location = new System.Drawing.Point(299, 25);
            this.lblShiharaiNmTo.Name = "lblShiharaiNmTo";
            this.lblShiharaiNmTo.Size = new System.Drawing.Size(179, 20);
            this.lblShiharaiNmTo.TabIndex = 4;
            this.lblShiharaiNmTo.Text = "最　後";
            this.lblShiharaiNmTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShiharaiCdTo
            // 
            this.txtShiharaiCdTo.AutoSizeFromLength = true;
            this.txtShiharaiCdTo.DisplayLength = null;
            this.txtShiharaiCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShiharaiCdTo.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtShiharaiCdTo.Location = new System.Drawing.Point(263, 25);
            this.txtShiharaiCdTo.MaxLength = 4;
            this.txtShiharaiCdTo.Name = "txtShiharaiCdTo";
            this.txtShiharaiCdTo.Size = new System.Drawing.Size(34, 20);
            this.txtShiharaiCdTo.TabIndex = 8;
            this.txtShiharaiCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShiharaiCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtShiharaiCdTo_Validating);
            // 
            // lblShiharaiNmFr
            // 
            this.lblShiharaiNmFr.BackColor = System.Drawing.Color.Silver;
            this.lblShiharaiNmFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShiharaiNmFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShiharaiNmFr.Location = new System.Drawing.Point(59, 25);
            this.lblShiharaiNmFr.Name = "lblShiharaiNmFr";
            this.lblShiharaiNmFr.Size = new System.Drawing.Size(179, 20);
            this.lblShiharaiNmFr.TabIndex = 1;
            this.lblShiharaiNmFr.Text = "先　頭";
            this.lblShiharaiNmFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShiharaiCdFr
            // 
            this.txtShiharaiCdFr.AutoSizeFromLength = true;
            this.txtShiharaiCdFr.DisplayLength = null;
            this.txtShiharaiCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShiharaiCdFr.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtShiharaiCdFr.Location = new System.Drawing.Point(23, 25);
            this.txtShiharaiCdFr.MaxLength = 4;
            this.txtShiharaiCdFr.Name = "txtShiharaiCdFr";
            this.txtShiharaiCdFr.Size = new System.Drawing.Size(34, 20);
            this.txtShiharaiCdFr.TabIndex = 7;
            this.txtShiharaiCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShiharaiCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtShiharaiCdFr_Validating);
            // 
            // gpbShiwakeDenpyo
            // 
            this.gpbShiwakeDenpyo.Controls.Add(this.gpbTekiyo);
            this.gpbShiwakeDenpyo.Controls.Add(this.gpbSwkDpyHzk);
            this.gpbShiwakeDenpyo.Controls.Add(this.gpbTantosha);
            this.gpbShiwakeDenpyo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gpbShiwakeDenpyo.Location = new System.Drawing.Point(12, 380);
            this.gpbShiwakeDenpyo.Name = "gpbShiwakeDenpyo";
            this.gpbShiwakeDenpyo.Size = new System.Drawing.Size(495, 146);
            this.gpbShiwakeDenpyo.TabIndex = 7;
            this.gpbShiwakeDenpyo.TabStop = false;
            this.gpbShiwakeDenpyo.Text = "仕訳伝票";
            // 
            // gpbTekiyo
            // 
            this.gpbTekiyo.Controls.Add(this.txtTekiyo);
            this.gpbTekiyo.Controls.Add(this.txtTekiyoCd);
            this.gpbTekiyo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gpbTekiyo.Location = new System.Drawing.Point(14, 85);
            this.gpbTekiyo.Name = "gpbTekiyo";
            this.gpbTekiyo.Size = new System.Drawing.Size(305, 52);
            this.gpbTekiyo.TabIndex = 2;
            this.gpbTekiyo.TabStop = false;
            this.gpbTekiyo.Text = "摘要";
            // 
            // txtTekiyo
            // 
            this.txtTekiyo.AutoSizeFromLength = true;
            this.txtTekiyo.DisplayLength = 30;
            this.txtTekiyo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTekiyo.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtTekiyo.Location = new System.Drawing.Point(58, 20);
            this.txtTekiyo.MaxLength = 40;
            this.txtTekiyo.Name = "txtTekiyo";
            this.txtTekiyo.Size = new System.Drawing.Size(216, 20);
            this.txtTekiyo.TabIndex = 14;
            this.txtTekiyo.Validating += new System.ComponentModel.CancelEventHandler(this.txtTekiyo_Validating);
            // 
            // txtTekiyoCd
            // 
            this.txtTekiyoCd.AutoSizeFromLength = true;
            this.txtTekiyoCd.DisplayLength = null;
            this.txtTekiyoCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTekiyoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtTekiyoCd.Location = new System.Drawing.Point(22, 20);
            this.txtTekiyoCd.MaxLength = 4;
            this.txtTekiyoCd.Name = "txtTekiyoCd";
            this.txtTekiyoCd.Size = new System.Drawing.Size(34, 20);
            this.txtTekiyoCd.TabIndex = 13;
            this.txtTekiyoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTekiyoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtTekiyoCd_Validating);
            // 
            // gpbSwkDpyHzk
            // 
            this.gpbSwkDpyHzk.Controls.Add(this.lblSwkDpyDtGengo);
            this.gpbSwkDpyHzk.Controls.Add(this.lblSwkDpyDtDay);
            this.gpbSwkDpyHzk.Controls.Add(this.txtSwkDpyDtDay);
            this.gpbSwkDpyHzk.Controls.Add(this.lblSwkDpyDtMonth);
            this.gpbSwkDpyHzk.Controls.Add(this.txtSwkDpyDtMonth);
            this.gpbSwkDpyHzk.Controls.Add(this.lblSwkDpyDtJpYear);
            this.gpbSwkDpyHzk.Controls.Add(this.txtSwkDpyDtJpYear);
            this.gpbSwkDpyHzk.Controls.Add(this.label1);
            this.gpbSwkDpyHzk.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gpbSwkDpyHzk.Location = new System.Drawing.Point(254, 22);
            this.gpbSwkDpyHzk.Name = "gpbSwkDpyHzk";
            this.gpbSwkDpyHzk.Size = new System.Drawing.Size(224, 52);
            this.gpbSwkDpyHzk.TabIndex = 1;
            this.gpbSwkDpyHzk.TabStop = false;
            this.gpbSwkDpyHzk.Text = "仕訳伝票日付";
            // 
            // lblSwkDpyDtGengo
            // 
            this.lblSwkDpyDtGengo.BackColor = System.Drawing.Color.Silver;
            this.lblSwkDpyDtGengo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSwkDpyDtGengo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblSwkDpyDtGengo.Location = new System.Drawing.Point(26, 20);
            this.lblSwkDpyDtGengo.Name = "lblSwkDpyDtGengo";
            this.lblSwkDpyDtGengo.Size = new System.Drawing.Size(39, 20);
            this.lblSwkDpyDtGengo.TabIndex = 904;
            this.lblSwkDpyDtGengo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblSwkDpyDtDay
            // 
            this.lblSwkDpyDtDay.BackColor = System.Drawing.Color.Silver;
            this.lblSwkDpyDtDay.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblSwkDpyDtDay.Location = new System.Drawing.Point(170, 20);
            this.lblSwkDpyDtDay.Name = "lblSwkDpyDtDay";
            this.lblSwkDpyDtDay.Size = new System.Drawing.Size(18, 20);
            this.lblSwkDpyDtDay.TabIndex = 6;
            this.lblSwkDpyDtDay.Text = "日";
            this.lblSwkDpyDtDay.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtSwkDpyDtDay
            // 
            this.txtSwkDpyDtDay.AutoSizeFromLength = true;
            this.txtSwkDpyDtDay.DisplayLength = null;
            this.txtSwkDpyDtDay.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtSwkDpyDtDay.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtSwkDpyDtDay.Location = new System.Drawing.Point(148, 20);
            this.txtSwkDpyDtDay.MaxLength = 2;
            this.txtSwkDpyDtDay.Name = "txtSwkDpyDtDay";
            this.txtSwkDpyDtDay.Size = new System.Drawing.Size(20, 20);
            this.txtSwkDpyDtDay.TabIndex = 12;
            this.txtSwkDpyDtDay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSwkDpyDtDay.Validating += new System.ComponentModel.CancelEventHandler(this.txtSwkDpyDtDay_Validating);
            // 
            // lblSwkDpyDtMonth
            // 
            this.lblSwkDpyDtMonth.BackColor = System.Drawing.Color.Silver;
            this.lblSwkDpyDtMonth.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblSwkDpyDtMonth.Location = new System.Drawing.Point(129, 20);
            this.lblSwkDpyDtMonth.Name = "lblSwkDpyDtMonth";
            this.lblSwkDpyDtMonth.Size = new System.Drawing.Size(18, 20);
            this.lblSwkDpyDtMonth.TabIndex = 4;
            this.lblSwkDpyDtMonth.Text = "月";
            this.lblSwkDpyDtMonth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtSwkDpyDtMonth
            // 
            this.txtSwkDpyDtMonth.AutoSizeFromLength = true;
            this.txtSwkDpyDtMonth.DisplayLength = null;
            this.txtSwkDpyDtMonth.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtSwkDpyDtMonth.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtSwkDpyDtMonth.Location = new System.Drawing.Point(107, 20);
            this.txtSwkDpyDtMonth.MaxLength = 2;
            this.txtSwkDpyDtMonth.Name = "txtSwkDpyDtMonth";
            this.txtSwkDpyDtMonth.Size = new System.Drawing.Size(20, 20);
            this.txtSwkDpyDtMonth.TabIndex = 11;
            this.txtSwkDpyDtMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSwkDpyDtMonth.Validating += new System.ComponentModel.CancelEventHandler(this.txtSwkDpyDtMonth_Validating);
            // 
            // lblSwkDpyDtJpYear
            // 
            this.lblSwkDpyDtJpYear.BackColor = System.Drawing.Color.Silver;
            this.lblSwkDpyDtJpYear.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblSwkDpyDtJpYear.Location = new System.Drawing.Point(88, 20);
            this.lblSwkDpyDtJpYear.Name = "lblSwkDpyDtJpYear";
            this.lblSwkDpyDtJpYear.Size = new System.Drawing.Size(18, 20);
            this.lblSwkDpyDtJpYear.TabIndex = 2;
            this.lblSwkDpyDtJpYear.Text = "年";
            this.lblSwkDpyDtJpYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtSwkDpyDtJpYear
            // 
            this.txtSwkDpyDtJpYear.AutoSizeFromLength = true;
            this.txtSwkDpyDtJpYear.DisplayLength = null;
            this.txtSwkDpyDtJpYear.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtSwkDpyDtJpYear.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtSwkDpyDtJpYear.Location = new System.Drawing.Point(66, 20);
            this.txtSwkDpyDtJpYear.MaxLength = 2;
            this.txtSwkDpyDtJpYear.Name = "txtSwkDpyDtJpYear";
            this.txtSwkDpyDtJpYear.Size = new System.Drawing.Size(20, 20);
            this.txtSwkDpyDtJpYear.TabIndex = 10;
            this.txtSwkDpyDtJpYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSwkDpyDtJpYear.Validating += new System.ComponentModel.CancelEventHandler(this.txtSwkDpyDtJpYear_Validating);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Silver;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Location = new System.Drawing.Point(24, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(167, 25);
            this.label1.TabIndex = 905;
            this.label1.Text = " ";
            // 
            // gpbTantosha
            // 
            this.gpbTantosha.Controls.Add(this.lblTantoNm);
            this.gpbTantosha.Controls.Add(this.txtTantoCd);
            this.gpbTantosha.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gpbTantosha.Location = new System.Drawing.Point(14, 22);
            this.gpbTantosha.Name = "gpbTantosha";
            this.gpbTantosha.Size = new System.Drawing.Size(226, 52);
            this.gpbTantosha.TabIndex = 0;
            this.gpbTantosha.TabStop = false;
            this.gpbTantosha.Text = "担当者";
            // 
            // lblTantoNm
            // 
            this.lblTantoNm.BackColor = System.Drawing.Color.Silver;
            this.lblTantoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTantoNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTantoNm.Location = new System.Drawing.Point(59, 22);
            this.lblTantoNm.Name = "lblTantoNm";
            this.lblTantoNm.Size = new System.Drawing.Size(140, 20);
            this.lblTantoNm.TabIndex = 6;
            this.lblTantoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTantoCd
            // 
            this.txtTantoCd.AutoSizeFromLength = true;
            this.txtTantoCd.DisplayLength = null;
            this.txtTantoCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTantoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtTantoCd.Location = new System.Drawing.Point(23, 22);
            this.txtTantoCd.MaxLength = 4;
            this.txtTantoCd.Name = "txtTantoCd";
            this.txtTantoCd.Size = new System.Drawing.Size(34, 20);
            this.txtTantoCd.TabIndex = 9;
            this.txtTantoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTantoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtTantoCd_Validating);
            // 
            // lblUpdMode
            // 
            this.lblUpdMode.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblUpdMode.ForeColor = System.Drawing.Color.Red;
            this.lblUpdMode.Location = new System.Drawing.Point(14, 49);
            this.lblUpdMode.Name = "lblUpdMode";
            this.lblUpdMode.Size = new System.Drawing.Size(110, 20);
            this.lblUpdMode.TabIndex = 1;
            this.lblUpdMode.Text = "【更新モード】";
            this.lblUpdMode.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // gbxMizuageShisho
            // 
            this.gbxMizuageShisho.Controls.Add(this.txtMizuageShishoCd);
            this.gbxMizuageShisho.Controls.Add(this.lblMizuageShishoNm);
            this.gbxMizuageShisho.Controls.Add(this.lblMizuageShisho);
            this.gbxMizuageShisho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.gbxMizuageShisho.ForeColor = System.Drawing.Color.Black;
            this.gbxMizuageShisho.Location = new System.Drawing.Point(12, 86);
            this.gbxMizuageShisho.Name = "gbxMizuageShisho";
            this.gbxMizuageShisho.Size = new System.Drawing.Size(381, 66);
            this.gbxMizuageShisho.TabIndex = 903;
            this.gbxMizuageShisho.TabStop = false;
            this.gbxMizuageShisho.Text = "支所";
            // 
            // txtMizuageShishoCd
            // 
            this.txtMizuageShishoCd.AutoSizeFromLength = true;
            this.txtMizuageShishoCd.DisplayLength = null;
            this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtMizuageShishoCd.Location = new System.Drawing.Point(53, 27);
            this.txtMizuageShishoCd.MaxLength = 5;
            this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
            this.txtMizuageShishoCd.Size = new System.Drawing.Size(51, 20);
            this.txtMizuageShishoCd.TabIndex = 0;
            this.txtMizuageShishoCd.TabStop = false;
            this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
            // 
            // lblMizuageShishoNm
            // 
            this.lblMizuageShishoNm.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMizuageShishoNm.Location = new System.Drawing.Point(109, 27);
            this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
            this.lblMizuageShishoNm.Size = new System.Drawing.Size(250, 20);
            this.lblMizuageShishoNm.TabIndex = 2;
            this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMizuageShisho
            // 
            this.lblMizuageShisho.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShisho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShisho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblMizuageShisho.Location = new System.Drawing.Point(10, 25);
            this.lblMizuageShisho.Name = "lblMizuageShisho";
            this.lblMizuageShisho.Size = new System.Drawing.Size(354, 25);
            this.lblMizuageShisho.TabIndex = 0;
            this.lblMizuageShisho.Text = "支所";
            this.lblMizuageShisho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // KBDB1021
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 638);
            this.Controls.Add(this.gbxMizuageShisho);
            this.Controls.Add(this.lblMessage);
            this.Controls.Add(this.lblUpdMode);
            this.Controls.Add(this.gpbShiwakeDenpyo);
            this.Controls.Add(this.gpbShiharaiCd);
            this.Controls.Add(this.gpbDenpyoHizuke);
            this.Controls.Add(this.gpbShimebi);
            this.Controls.Add(this.gpbSakuseiKbn);
            this.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.Name = "KBDB1021";
            this.Text = "仕入仕訳データ作成";
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.gpbSakuseiKbn, 0);
            this.Controls.SetChildIndex(this.gpbShimebi, 0);
            this.Controls.SetChildIndex(this.gpbDenpyoHizuke, 0);
            this.Controls.SetChildIndex(this.gpbShiharaiCd, 0);
            this.Controls.SetChildIndex(this.gpbShiwakeDenpyo, 0);
            this.Controls.SetChildIndex(this.lblUpdMode, 0);
            this.Controls.SetChildIndex(this.lblMessage, 0);
            this.Controls.SetChildIndex(this.gbxMizuageShisho, 0);
            this.pnlDebug.ResumeLayout(false);
            this.gpbSakuseiKbn.ResumeLayout(false);
            this.gpbSakuseiKbn.PerformLayout();
            this.gpbShimebi.ResumeLayout(false);
            this.gpbShimebi.PerformLayout();
            this.gpbDenpyoHizuke.ResumeLayout(false);
            this.gpbDenpyoHizuke.PerformLayout();
            this.gpbShiharaiCd.ResumeLayout(false);
            this.gpbShiharaiCd.PerformLayout();
            this.gpbShiwakeDenpyo.ResumeLayout(false);
            this.gpbTekiyo.ResumeLayout(false);
            this.gpbTekiyo.PerformLayout();
            this.gpbSwkDpyHzk.ResumeLayout(false);
            this.gpbSwkDpyHzk.PerformLayout();
            this.gpbTantosha.ResumeLayout(false);
            this.gpbTantosha.PerformLayout();
            this.gbxMizuageShisho.ResumeLayout(false);
            this.gbxMizuageShisho.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.GroupBox gpbSakuseiKbn;
        private System.Windows.Forms.GroupBox gpbShimebi;
        private System.Windows.Forms.GroupBox gpbDenpyoHizuke;
        private System.Windows.Forms.GroupBox gpbShiharaiCd;
        private System.Windows.Forms.GroupBox gpbShiwakeDenpyo;
        private System.Windows.Forms.GroupBox gpbSwkDpyHzk;
        private System.Windows.Forms.GroupBox gpbTantosha;
        private System.Windows.Forms.GroupBox gpbTekiyo;
        private System.Windows.Forms.RadioButton rdbGenkin;
        private System.Windows.Forms.RadioButton rdbGenkinKake;
        private System.Windows.Forms.RadioButton rdbShimebi;
        private System.Windows.Forms.Label lblShimebiMemo;
        private jp.co.fsi.common.controls.FsiTextBox txtShimebi;
        private jp.co.fsi.common.controls.FsiTextBox txtDpyDtFrJpYear;
        private System.Windows.Forms.Label lblDpyDtFrJpYear;
        private System.Windows.Forms.Label lblDpyDtFrMonth;
        private jp.co.fsi.common.controls.FsiTextBox txtDpyDtFrMonth;
        private System.Windows.Forms.Label lblDpyDtFrDay;
        private jp.co.fsi.common.controls.FsiTextBox txtDpyDtFrDay;
        private System.Windows.Forms.Label lblDpyDtToDay;
        private jp.co.fsi.common.controls.FsiTextBox txtDpyDtToDay;
        private System.Windows.Forms.Label lblDpyDtToMonth;
        private jp.co.fsi.common.controls.FsiTextBox txtDpyDtToMonth;
        private System.Windows.Forms.Label lblDpyDtToJpYear;
        private jp.co.fsi.common.controls.FsiTextBox txtDpyDtToJpYear;
        private System.Windows.Forms.Label lblDpyDtBetween;
        private System.Windows.Forms.Label lblShiharaiNmFr;
        private jp.co.fsi.common.controls.FsiTextBox txtShiharaiCdFr;
        private System.Windows.Forms.Label lblShiharaiNmTo;
        private jp.co.fsi.common.controls.FsiTextBox txtShiharaiCdTo;
        private System.Windows.Forms.Label lblShiharaiCdBetween;
        private System.Windows.Forms.Label lblTantoNm;
        private jp.co.fsi.common.controls.FsiTextBox txtTantoCd;
        private System.Windows.Forms.Label lblSwkDpyDtDay;
        private jp.co.fsi.common.controls.FsiTextBox txtSwkDpyDtDay;
        private System.Windows.Forms.Label lblSwkDpyDtMonth;
        private jp.co.fsi.common.controls.FsiTextBox txtSwkDpyDtMonth;
        private System.Windows.Forms.Label lblSwkDpyDtJpYear;
        private jp.co.fsi.common.controls.FsiTextBox txtSwkDpyDtJpYear;
        private jp.co.fsi.common.controls.FsiTextBox txtTekiyoCd;
        private jp.co.fsi.common.controls.FsiTextBox txtTekiyo;
        private System.Windows.Forms.Label lblUpdMode;
        private System.Windows.Forms.Label lblDpyDtFrGengo;
        private System.Windows.Forms.Label lblDpyDtToGengo;
        private System.Windows.Forms.Label lblSwkDpyDtGengo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox gbxMizuageShisho;
        private common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private System.Windows.Forms.Label lblMizuageShisho;
    }
}