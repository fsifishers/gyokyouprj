﻿namespace jp.co.fsi.kb.kbcm1011
{
    partial class KBCM1014
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTankaShutokuHoho = new System.Windows.Forms.Label();
            this.txtTankaShutokuHoho = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblTnkStkHohoMemo = new System.Windows.Forms.Label();
            this.lblKingakuHasuShori = new System.Windows.Forms.Label();
            this.txtKingakuHasuShori = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKgkHsShrMemo = new System.Windows.Forms.Label();
            this.lblShohizeiNyuryokuHoho = new System.Windows.Forms.Label();
            this.txtShohizeiNyuryokuHoho = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShzNrkHohoNm = new System.Windows.Forms.Label();
            this.lblShohizeiHasuShori = new System.Windows.Forms.Label();
            this.txtShohizeiHasuShori = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShzHsSrMemo = new System.Windows.Forms.Label();
            this.lblShohizeiTenkaHoho = new System.Windows.Forms.Label();
            this.txtShohizeiTenkaHoho = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShzTnkHohoMemo = new System.Windows.Forms.Label();
            this.pnlMain = new jp.co.fsi.common.FsiPanel();
            this.pnlDebug.SuspendLayout();
            this.pnlMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Size = new System.Drawing.Size(439, 23);
            this.lblTitle.Text = "";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(5, 150);
            this.pnlDebug.Size = new System.Drawing.Size(472, 100);
            // 
            // lblTankaShutokuHoho
            // 
            this.lblTankaShutokuHoho.BackColor = System.Drawing.Color.Silver;
            this.lblTankaShutokuHoho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTankaShutokuHoho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblTankaShutokuHoho.Location = new System.Drawing.Point(8, 11);
            this.lblTankaShutokuHoho.Name = "lblTankaShutokuHoho";
            this.lblTankaShutokuHoho.Size = new System.Drawing.Size(115, 20);
            this.lblTankaShutokuHoho.TabIndex = 19;
            this.lblTankaShutokuHoho.Text = "単価取得方法";
            this.lblTankaShutokuHoho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTankaShutokuHoho
            // 
            this.txtTankaShutokuHoho.AutoSizeFromLength = true;
            this.txtTankaShutokuHoho.DisplayLength = 2;
            this.txtTankaShutokuHoho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTankaShutokuHoho.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtTankaShutokuHoho.Location = new System.Drawing.Point(123, 11);
            this.txtTankaShutokuHoho.MaxLength = 1;
            this.txtTankaShutokuHoho.Name = "txtTankaShutokuHoho";
            this.txtTankaShutokuHoho.Size = new System.Drawing.Size(20, 20);
            this.txtTankaShutokuHoho.TabIndex = 20;
            this.txtTankaShutokuHoho.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTankaShutokuHoho.Validating += new System.ComponentModel.CancelEventHandler(this.txtTankaShutokuHoho_Validating);
            // 
            // lblTnkStkHohoMemo
            // 
            this.lblTnkStkHohoMemo.BackColor = System.Drawing.Color.Silver;
            this.lblTnkStkHohoMemo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTnkStkHohoMemo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTnkStkHohoMemo.Location = new System.Drawing.Point(144, 11);
            this.lblTnkStkHohoMemo.Name = "lblTnkStkHohoMemo";
            this.lblTnkStkHohoMemo.Size = new System.Drawing.Size(248, 20);
            this.lblTnkStkHohoMemo.TabIndex = 21;
            this.lblTnkStkHohoMemo.Text = "0:卸単価 1:小売単価 2:前回単価";
            this.lblTnkStkHohoMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKingakuHasuShori
            // 
            this.lblKingakuHasuShori.BackColor = System.Drawing.Color.Silver;
            this.lblKingakuHasuShori.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKingakuHasuShori.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblKingakuHasuShori.Location = new System.Drawing.Point(8, 34);
            this.lblKingakuHasuShori.Name = "lblKingakuHasuShori";
            this.lblKingakuHasuShori.Size = new System.Drawing.Size(115, 20);
            this.lblKingakuHasuShori.TabIndex = 22;
            this.lblKingakuHasuShori.Text = "金額端数処理";
            this.lblKingakuHasuShori.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKingakuHasuShori
            // 
            this.txtKingakuHasuShori.AutoSizeFromLength = true;
            this.txtKingakuHasuShori.DisplayLength = 2;
            this.txtKingakuHasuShori.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKingakuHasuShori.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtKingakuHasuShori.Location = new System.Drawing.Point(123, 34);
            this.txtKingakuHasuShori.MaxLength = 1;
            this.txtKingakuHasuShori.Name = "txtKingakuHasuShori";
            this.txtKingakuHasuShori.Size = new System.Drawing.Size(20, 20);
            this.txtKingakuHasuShori.TabIndex = 23;
            this.txtKingakuHasuShori.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKingakuHasuShori.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingakuHasuShori_Validating);
            // 
            // lblKgkHsShrMemo
            // 
            this.lblKgkHsShrMemo.BackColor = System.Drawing.Color.Silver;
            this.lblKgkHsShrMemo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKgkHsShrMemo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKgkHsShrMemo.Location = new System.Drawing.Point(144, 34);
            this.lblKgkHsShrMemo.Name = "lblKgkHsShrMemo";
            this.lblKgkHsShrMemo.Size = new System.Drawing.Size(248, 20);
            this.lblKgkHsShrMemo.TabIndex = 24;
            this.lblKgkHsShrMemo.Text = "1:切り捨て 2:四捨五入 3:切り上げ";
            this.lblKgkHsShrMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShohizeiNyuryokuHoho
            // 
            this.lblShohizeiNyuryokuHoho.BackColor = System.Drawing.Color.Silver;
            this.lblShohizeiNyuryokuHoho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShohizeiNyuryokuHoho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblShohizeiNyuryokuHoho.Location = new System.Drawing.Point(8, 57);
            this.lblShohizeiNyuryokuHoho.Name = "lblShohizeiNyuryokuHoho";
            this.lblShohizeiNyuryokuHoho.Size = new System.Drawing.Size(115, 20);
            this.lblShohizeiNyuryokuHoho.TabIndex = 25;
            this.lblShohizeiNyuryokuHoho.Text = "消費税入力方法";
            this.lblShohizeiNyuryokuHoho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShohizeiNyuryokuHoho
            // 
            this.txtShohizeiNyuryokuHoho.AutoSizeFromLength = true;
            this.txtShohizeiNyuryokuHoho.DisplayLength = 2;
            this.txtShohizeiNyuryokuHoho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShohizeiNyuryokuHoho.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtShohizeiNyuryokuHoho.Location = new System.Drawing.Point(123, 57);
            this.txtShohizeiNyuryokuHoho.MaxLength = 1;
            this.txtShohizeiNyuryokuHoho.Name = "txtShohizeiNyuryokuHoho";
            this.txtShohizeiNyuryokuHoho.Size = new System.Drawing.Size(20, 20);
            this.txtShohizeiNyuryokuHoho.TabIndex = 26;
            this.txtShohizeiNyuryokuHoho.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShohizeiNyuryokuHoho.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohizeiNyuryokuHoho_Validating);
            // 
            // lblShzNrkHohoNm
            // 
            this.lblShzNrkHohoNm.BackColor = System.Drawing.Color.Silver;
            this.lblShzNrkHohoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShzNrkHohoNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShzNrkHohoNm.Location = new System.Drawing.Point(144, 57);
            this.lblShzNrkHohoNm.Name = "lblShzNrkHohoNm";
            this.lblShzNrkHohoNm.Size = new System.Drawing.Size(248, 20);
            this.lblShzNrkHohoNm.TabIndex = 27;
            this.lblShzNrkHohoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShohizeiHasuShori
            // 
            this.lblShohizeiHasuShori.BackColor = System.Drawing.Color.Silver;
            this.lblShohizeiHasuShori.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShohizeiHasuShori.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblShohizeiHasuShori.Location = new System.Drawing.Point(8, 80);
            this.lblShohizeiHasuShori.Name = "lblShohizeiHasuShori";
            this.lblShohizeiHasuShori.Size = new System.Drawing.Size(115, 20);
            this.lblShohizeiHasuShori.TabIndex = 40;
            this.lblShohizeiHasuShori.Text = "消費税端数処理";
            this.lblShohizeiHasuShori.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShohizeiHasuShori
            // 
            this.txtShohizeiHasuShori.AutoSizeFromLength = true;
            this.txtShohizeiHasuShori.DisplayLength = 2;
            this.txtShohizeiHasuShori.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShohizeiHasuShori.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtShohizeiHasuShori.Location = new System.Drawing.Point(123, 80);
            this.txtShohizeiHasuShori.MaxLength = 1;
            this.txtShohizeiHasuShori.Name = "txtShohizeiHasuShori";
            this.txtShohizeiHasuShori.Size = new System.Drawing.Size(20, 20);
            this.txtShohizeiHasuShori.TabIndex = 41;
            this.txtShohizeiHasuShori.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShohizeiHasuShori.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohizeiHasuShori_Validating);
            // 
            // lblShzHsSrMemo
            // 
            this.lblShzHsSrMemo.BackColor = System.Drawing.Color.Silver;
            this.lblShzHsSrMemo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShzHsSrMemo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShzHsSrMemo.Location = new System.Drawing.Point(144, 80);
            this.lblShzHsSrMemo.Name = "lblShzHsSrMemo";
            this.lblShzHsSrMemo.Size = new System.Drawing.Size(248, 20);
            this.lblShzHsSrMemo.TabIndex = 42;
            this.lblShzHsSrMemo.Text = "1:切り捨て 2:四捨五入 3:切り上げ";
            this.lblShzHsSrMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShohizeiTenkaHoho
            // 
            this.lblShohizeiTenkaHoho.BackColor = System.Drawing.Color.Silver;
            this.lblShohizeiTenkaHoho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShohizeiTenkaHoho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblShohizeiTenkaHoho.Location = new System.Drawing.Point(8, 103);
            this.lblShohizeiTenkaHoho.Name = "lblShohizeiTenkaHoho";
            this.lblShohizeiTenkaHoho.Size = new System.Drawing.Size(115, 20);
            this.lblShohizeiTenkaHoho.TabIndex = 43;
            this.lblShohizeiTenkaHoho.Text = "消費税転嫁方法";
            this.lblShohizeiTenkaHoho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShohizeiTenkaHoho
            // 
            this.txtShohizeiTenkaHoho.AutoSizeFromLength = true;
            this.txtShohizeiTenkaHoho.DisplayLength = 2;
            this.txtShohizeiTenkaHoho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShohizeiTenkaHoho.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtShohizeiTenkaHoho.Location = new System.Drawing.Point(123, 103);
            this.txtShohizeiTenkaHoho.MaxLength = 1;
            this.txtShohizeiTenkaHoho.Name = "txtShohizeiTenkaHoho";
            this.txtShohizeiTenkaHoho.Size = new System.Drawing.Size(20, 20);
            this.txtShohizeiTenkaHoho.TabIndex = 44;
            this.txtShohizeiTenkaHoho.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShohizeiTenkaHoho.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtShohizeiTenkaHoho_KeyDown);
            this.txtShohizeiTenkaHoho.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohizeiTenkaHoho_Validating);
            // 
            // lblShzTnkHohoMemo
            // 
            this.lblShzTnkHohoMemo.BackColor = System.Drawing.Color.Silver;
            this.lblShzTnkHohoMemo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShzTnkHohoMemo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShzTnkHohoMemo.Location = new System.Drawing.Point(144, 103);
            this.lblShzTnkHohoMemo.Name = "lblShzTnkHohoMemo";
            this.lblShzTnkHohoMemo.Size = new System.Drawing.Size(248, 20);
            this.lblShzTnkHohoMemo.TabIndex = 45;
            this.lblShzTnkHohoMemo.Text = "1:明細転嫁 2:伝票転嫁 3:請求転嫁";
            this.lblShzTnkHohoMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnlMain
            // 
            this.pnlMain.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlMain.Controls.Add(this.lblShzTnkHohoMemo);
            this.pnlMain.Controls.Add(this.txtShohizeiTenkaHoho);
            this.pnlMain.Controls.Add(this.lblShohizeiTenkaHoho);
            this.pnlMain.Controls.Add(this.lblShzHsSrMemo);
            this.pnlMain.Controls.Add(this.txtShohizeiHasuShori);
            this.pnlMain.Controls.Add(this.lblShohizeiHasuShori);
            this.pnlMain.Controls.Add(this.lblShzNrkHohoNm);
            this.pnlMain.Controls.Add(this.txtShohizeiNyuryokuHoho);
            this.pnlMain.Controls.Add(this.lblShohizeiNyuryokuHoho);
            this.pnlMain.Controls.Add(this.lblKgkHsShrMemo);
            this.pnlMain.Controls.Add(this.txtKingakuHasuShori);
            this.pnlMain.Controls.Add(this.lblKingakuHasuShori);
            this.pnlMain.Controls.Add(this.lblTnkStkHohoMemo);
            this.pnlMain.Controls.Add(this.txtTankaShutokuHoho);
            this.pnlMain.Controls.Add(this.lblTankaShutokuHoho);
            this.pnlMain.Location = new System.Drawing.Point(12, 10);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(445, 183);
            this.pnlMain.TabIndex = 3;
            // 
            // KBCM1014
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(464, 253);
            this.Controls.Add(this.pnlMain);
            this.Name = "KBCM1014";
            this.ShowFButton = true;
            this.Text = "仕入先マスタ初期値設定";
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.pnlMain, 0);
            this.pnlDebug.ResumeLayout(false);
            this.pnlMain.ResumeLayout(false);
            this.pnlMain.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblTankaShutokuHoho;
        private common.controls.FsiTextBox txtTankaShutokuHoho;
        private System.Windows.Forms.Label lblTnkStkHohoMemo;
        private System.Windows.Forms.Label lblKingakuHasuShori;
        private common.controls.FsiTextBox txtKingakuHasuShori;
        private System.Windows.Forms.Label lblKgkHsShrMemo;
        private System.Windows.Forms.Label lblShohizeiNyuryokuHoho;
        private common.controls.FsiTextBox txtShohizeiNyuryokuHoho;
        private System.Windows.Forms.Label lblShzNrkHohoNm;
        private System.Windows.Forms.Label lblShohizeiHasuShori;
        private common.controls.FsiTextBox txtShohizeiHasuShori;
        private System.Windows.Forms.Label lblShzHsSrMemo;
        private System.Windows.Forms.Label lblShohizeiTenkaHoho;
        private common.controls.FsiTextBox txtShohizeiTenkaHoho;
        private System.Windows.Forms.Label lblShzTnkHohoMemo;
        private jp.co.fsi.common.FsiPanel pnlMain;
    }
}