﻿namespace jp.co.fsi.kb.kbdr2031
{
    partial class KBDR2031
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtShiiresakiCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShiiresakiCdFr = new System.Windows.Forms.Label();
            this.lblCodeBet = new System.Windows.Forms.Label();
            this.lblShiiresakiCdTo = new System.Windows.Forms.Label();
            this.txtShiiresakiCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblJpFr = new System.Windows.Forms.Label();
            this.lblJpTo = new System.Windows.Forms.Label();
            this.txtDateYearFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtDateYearTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.grpDateRange = new System.Windows.Forms.GroupBox();
            this.lblJpDayTo = new System.Windows.Forms.Label();
            this.lblCodeBetDate = new System.Windows.Forms.Label();
            this.lblJpDayFr = new System.Windows.Forms.Label();
            this.lblJpMonthTo = new System.Windows.Forms.Label();
            this.lblJpMonthFr = new System.Windows.Forms.Label();
            this.lblJpYearTo = new System.Windows.Forms.Label();
            this.txtDateDayTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtDateDayFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblJpearFr = new System.Windows.Forms.Label();
            this.txtDateMonthTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtDateMonthFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblEraBackFr = new System.Windows.Forms.Label();
            this.lblEraBackTo = new System.Windows.Forms.Label();
            this.gbxDisplayMethod = new System.Windows.Forms.GroupBox();
            this.rdo2 = new System.Windows.Forms.RadioButton();
            this.rdo1 = new System.Windows.Forms.RadioButton();
            this.gbxShiiresakiCd = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMizuageShishoNm = new System.Windows.Forms.Label();
            this.lblMizuageShisho = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.grpDateRange.SuspendLayout();
            this.gbxDisplayMethod.SuspendLayout();
            this.gbxShiiresakiCd.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Text = "買掛元帳問合せ";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.pnlDebug.Size = new System.Drawing.Size(847, 100);
            // 
            // txtShiiresakiCdFr
            // 
            this.txtShiiresakiCdFr.AutoSizeFromLength = false;
            this.txtShiiresakiCdFr.DisplayLength = null;
            this.txtShiiresakiCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShiiresakiCdFr.Location = new System.Drawing.Point(26, 30);
            this.txtShiiresakiCdFr.MaxLength = 4;
            this.txtShiiresakiCdFr.Name = "txtShiiresakiCdFr";
            this.txtShiiresakiCdFr.Size = new System.Drawing.Size(40, 20);
            this.txtShiiresakiCdFr.TabIndex = 7;
            this.txtShiiresakiCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtCodeFr_Validating);
            // 
            // lblShiiresakiCdFr
            // 
            this.lblShiiresakiCdFr.BackColor = System.Drawing.Color.Silver;
            this.lblShiiresakiCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShiiresakiCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShiiresakiCdFr.Location = new System.Drawing.Point(72, 30);
            this.lblShiiresakiCdFr.Name = "lblShiiresakiCdFr";
            this.lblShiiresakiCdFr.Size = new System.Drawing.Size(300, 20);
            this.lblShiiresakiCdFr.TabIndex = 1;
            this.lblShiiresakiCdFr.Text = "先　頭";
            this.lblShiiresakiCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCodeBet
            // 
            this.lblCodeBet.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblCodeBet.Location = new System.Drawing.Point(386, 30);
            this.lblCodeBet.Name = "lblCodeBet";
            this.lblCodeBet.Size = new System.Drawing.Size(18, 20);
            this.lblCodeBet.TabIndex = 2;
            this.lblCodeBet.Text = "～";
            this.lblCodeBet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShiiresakiCdTo
            // 
            this.lblShiiresakiCdTo.BackColor = System.Drawing.Color.Silver;
            this.lblShiiresakiCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShiiresakiCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShiiresakiCdTo.Location = new System.Drawing.Point(465, 31);
            this.lblShiiresakiCdTo.Name = "lblShiiresakiCdTo";
            this.lblShiiresakiCdTo.Size = new System.Drawing.Size(300, 20);
            this.lblShiiresakiCdTo.TabIndex = 4;
            this.lblShiiresakiCdTo.Text = "最　後";
            this.lblShiiresakiCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShiiresakiCdTo
            // 
            this.txtShiiresakiCdTo.AutoSizeFromLength = false;
            this.txtShiiresakiCdTo.DisplayLength = null;
            this.txtShiiresakiCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShiiresakiCdTo.Location = new System.Drawing.Point(419, 30);
            this.txtShiiresakiCdTo.MaxLength = 4;
            this.txtShiiresakiCdTo.Name = "txtShiiresakiCdTo";
            this.txtShiiresakiCdTo.Size = new System.Drawing.Size(40, 20);
            this.txtShiiresakiCdTo.TabIndex = 8;
            this.txtShiiresakiCdTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtShiiresakiCdTo_KeyDown);
            this.txtShiiresakiCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtCodeTo_Validating);
            // 
            // lblJpFr
            // 
            this.lblJpFr.BackColor = System.Drawing.Color.Silver;
            this.lblJpFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblJpFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblJpFr.Location = new System.Drawing.Point(29, 33);
            this.lblJpFr.Name = "lblJpFr";
            this.lblJpFr.Size = new System.Drawing.Size(40, 20);
            this.lblJpFr.TabIndex = 0;
            this.lblJpFr.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblJpTo
            // 
            this.lblJpTo.BackColor = System.Drawing.Color.Silver;
            this.lblJpTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblJpTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblJpTo.Location = new System.Drawing.Point(317, 33);
            this.lblJpTo.Name = "lblJpTo";
            this.lblJpTo.Size = new System.Drawing.Size(40, 20);
            this.lblJpTo.TabIndex = 8;
            this.lblJpTo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtDateYearFr
            // 
            this.txtDateYearFr.AutoSizeFromLength = false;
            this.txtDateYearFr.DisplayLength = null;
            this.txtDateYearFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateYearFr.Location = new System.Drawing.Point(74, 33);
            this.txtDateYearFr.MaxLength = 2;
            this.txtDateYearFr.Name = "txtDateYearFr";
            this.txtDateYearFr.Size = new System.Drawing.Size(40, 20);
            this.txtDateYearFr.TabIndex = 1;
            this.txtDateYearFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateYearFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtYearFr_Validating);
            // 
            // txtDateYearTo
            // 
            this.txtDateYearTo.AutoSizeFromLength = false;
            this.txtDateYearTo.DisplayLength = null;
            this.txtDateYearTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateYearTo.Location = new System.Drawing.Point(362, 33);
            this.txtDateYearTo.MaxLength = 2;
            this.txtDateYearTo.Name = "txtDateYearTo";
            this.txtDateYearTo.Size = new System.Drawing.Size(40, 20);
            this.txtDateYearTo.TabIndex = 4;
            this.txtDateYearTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateYearTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtYearTo_Validating);
            // 
            // grpDateRange
            // 
            this.grpDateRange.Controls.Add(this.lblJpDayTo);
            this.grpDateRange.Controls.Add(this.lblCodeBetDate);
            this.grpDateRange.Controls.Add(this.lblJpDayFr);
            this.grpDateRange.Controls.Add(this.lblJpMonthTo);
            this.grpDateRange.Controls.Add(this.lblJpMonthFr);
            this.grpDateRange.Controls.Add(this.lblJpYearTo);
            this.grpDateRange.Controls.Add(this.txtDateDayTo);
            this.grpDateRange.Controls.Add(this.txtDateDayFr);
            this.grpDateRange.Controls.Add(this.lblJpearFr);
            this.grpDateRange.Controls.Add(this.txtDateMonthTo);
            this.grpDateRange.Controls.Add(this.txtDateMonthFr);
            this.grpDateRange.Controls.Add(this.txtDateYearTo);
            this.grpDateRange.Controls.Add(this.txtDateYearFr);
            this.grpDateRange.Controls.Add(this.lblJpTo);
            this.grpDateRange.Controls.Add(this.lblJpFr);
            this.grpDateRange.Controls.Add(this.lblEraBackFr);
            this.grpDateRange.Controls.Add(this.lblEraBackTo);
            this.grpDateRange.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.grpDateRange.ForeColor = System.Drawing.Color.Black;
            this.grpDateRange.Location = new System.Drawing.Point(12, 133);
            this.grpDateRange.Name = "grpDateRange";
            this.grpDateRange.Size = new System.Drawing.Size(658, 83);
            this.grpDateRange.TabIndex = 1;
            this.grpDateRange.TabStop = false;
            this.grpDateRange.Text = "日付範囲";
            // 
            // lblJpDayTo
            // 
            this.lblJpDayTo.AutoSize = true;
            this.lblJpDayTo.BackColor = System.Drawing.Color.Silver;
            this.lblJpDayTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblJpDayTo.Location = new System.Drawing.Point(544, 37);
            this.lblJpDayTo.Name = "lblJpDayTo";
            this.lblJpDayTo.Size = new System.Drawing.Size(21, 13);
            this.lblJpDayTo.TabIndex = 14;
            this.lblJpDayTo.Text = "日";
            // 
            // lblCodeBetDate
            // 
            this.lblCodeBetDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblCodeBetDate.Location = new System.Drawing.Point(290, 32);
            this.lblCodeBetDate.Name = "lblCodeBetDate";
            this.lblCodeBetDate.Size = new System.Drawing.Size(18, 20);
            this.lblCodeBetDate.TabIndex = 7;
            this.lblCodeBetDate.Text = "～";
            this.lblCodeBetDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblJpDayFr
            // 
            this.lblJpDayFr.AutoSize = true;
            this.lblJpDayFr.BackColor = System.Drawing.Color.Silver;
            this.lblJpDayFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblJpDayFr.Location = new System.Drawing.Point(254, 38);
            this.lblJpDayFr.Name = "lblJpDayFr";
            this.lblJpDayFr.Size = new System.Drawing.Size(21, 13);
            this.lblJpDayFr.TabIndex = 6;
            this.lblJpDayFr.Text = "日";
            // 
            // lblJpMonthTo
            // 
            this.lblJpMonthTo.AutoSize = true;
            this.lblJpMonthTo.BackColor = System.Drawing.Color.Silver;
            this.lblJpMonthTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblJpMonthTo.Location = new System.Drawing.Point(476, 37);
            this.lblJpMonthTo.Name = "lblJpMonthTo";
            this.lblJpMonthTo.Size = new System.Drawing.Size(21, 13);
            this.lblJpMonthTo.TabIndex = 12;
            this.lblJpMonthTo.Text = "月";
            // 
            // lblJpMonthFr
            // 
            this.lblJpMonthFr.AutoSize = true;
            this.lblJpMonthFr.BackColor = System.Drawing.Color.Silver;
            this.lblJpMonthFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblJpMonthFr.Location = new System.Drawing.Point(185, 38);
            this.lblJpMonthFr.Name = "lblJpMonthFr";
            this.lblJpMonthFr.Size = new System.Drawing.Size(21, 13);
            this.lblJpMonthFr.TabIndex = 4;
            this.lblJpMonthFr.Text = "月";
            // 
            // lblJpYearTo
            // 
            this.lblJpYearTo.AutoSize = true;
            this.lblJpYearTo.BackColor = System.Drawing.Color.Silver;
            this.lblJpYearTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblJpYearTo.Location = new System.Drawing.Point(408, 37);
            this.lblJpYearTo.Name = "lblJpYearTo";
            this.lblJpYearTo.Size = new System.Drawing.Size(21, 13);
            this.lblJpYearTo.TabIndex = 10;
            this.lblJpYearTo.Text = "年";
            // 
            // txtDateDayTo
            // 
            this.txtDateDayTo.AutoSizeFromLength = false;
            this.txtDateDayTo.DisplayLength = null;
            this.txtDateDayTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateDayTo.Location = new System.Drawing.Point(498, 33);
            this.txtDateDayTo.MaxLength = 2;
            this.txtDateDayTo.Name = "txtDateDayTo";
            this.txtDateDayTo.Size = new System.Drawing.Size(40, 20);
            this.txtDateDayTo.TabIndex = 6;
            this.txtDateDayTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateDayTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDayTo_Validating);
            // 
            // txtDateDayFr
            // 
            this.txtDateDayFr.AutoSizeFromLength = false;
            this.txtDateDayFr.DisplayLength = null;
            this.txtDateDayFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateDayFr.Location = new System.Drawing.Point(208, 33);
            this.txtDateDayFr.MaxLength = 2;
            this.txtDateDayFr.Name = "txtDateDayFr";
            this.txtDateDayFr.Size = new System.Drawing.Size(40, 20);
            this.txtDateDayFr.TabIndex = 3;
            this.txtDateDayFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateDayFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDayFr_Validating);
            // 
            // lblJpearFr
            // 
            this.lblJpearFr.AutoSize = true;
            this.lblJpearFr.BackColor = System.Drawing.Color.Silver;
            this.lblJpearFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblJpearFr.Location = new System.Drawing.Point(118, 38);
            this.lblJpearFr.Name = "lblJpearFr";
            this.lblJpearFr.Size = new System.Drawing.Size(21, 13);
            this.lblJpearFr.TabIndex = 2;
            this.lblJpearFr.Text = "年";
            // 
            // txtDateMonthTo
            // 
            this.txtDateMonthTo.AutoSizeFromLength = false;
            this.txtDateMonthTo.DisplayLength = null;
            this.txtDateMonthTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateMonthTo.Location = new System.Drawing.Point(430, 33);
            this.txtDateMonthTo.MaxLength = 2;
            this.txtDateMonthTo.Name = "txtDateMonthTo";
            this.txtDateMonthTo.Size = new System.Drawing.Size(40, 20);
            this.txtDateMonthTo.TabIndex = 5;
            this.txtDateMonthTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateMonthTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthTo_Validating);
            // 
            // txtDateMonthFr
            // 
            this.txtDateMonthFr.AutoSizeFromLength = false;
            this.txtDateMonthFr.DisplayLength = null;
            this.txtDateMonthFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateMonthFr.Location = new System.Drawing.Point(141, 33);
            this.txtDateMonthFr.MaxLength = 2;
            this.txtDateMonthFr.Name = "txtDateMonthFr";
            this.txtDateMonthFr.Size = new System.Drawing.Size(40, 20);
            this.txtDateMonthFr.TabIndex = 2;
            this.txtDateMonthFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateMonthFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthFr_Validating);
            // 
            // lblEraBackFr
            // 
            this.lblEraBackFr.BackColor = System.Drawing.Color.Silver;
            this.lblEraBackFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblEraBackFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblEraBackFr.Location = new System.Drawing.Point(26, 31);
            this.lblEraBackFr.Name = "lblEraBackFr";
            this.lblEraBackFr.Size = new System.Drawing.Size(258, 25);
            this.lblEraBackFr.TabIndex = 902;
            this.lblEraBackFr.Text = " ";
            // 
            // lblEraBackTo
            // 
            this.lblEraBackTo.BackColor = System.Drawing.Color.Silver;
            this.lblEraBackTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblEraBackTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblEraBackTo.Location = new System.Drawing.Point(314, 31);
            this.lblEraBackTo.Name = "lblEraBackTo";
            this.lblEraBackTo.Size = new System.Drawing.Size(258, 25);
            this.lblEraBackTo.TabIndex = 903;
            this.lblEraBackTo.Text = " ";
            // 
            // gbxDisplayMethod
            // 
            this.gbxDisplayMethod.Controls.Add(this.rdo2);
            this.gbxDisplayMethod.Controls.Add(this.rdo1);
            this.gbxDisplayMethod.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxDisplayMethod.ForeColor = System.Drawing.Color.Black;
            this.gbxDisplayMethod.Location = new System.Drawing.Point(676, 133);
            this.gbxDisplayMethod.Name = "gbxDisplayMethod";
            this.gbxDisplayMethod.Size = new System.Drawing.Size(131, 83);
            this.gbxDisplayMethod.TabIndex = 3;
            this.gbxDisplayMethod.TabStop = false;
            this.gbxDisplayMethod.Text = "表示方法";
            // 
            // rdo2
            // 
            this.rdo2.AutoSize = true;
            this.rdo2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdo2.Location = new System.Drawing.Point(24, 47);
            this.rdo2.Name = "rdo2";
            this.rdo2.Size = new System.Drawing.Size(81, 17);
            this.rdo2.TabIndex = 10;
            this.rdo2.Text = "伝票合計";
            this.rdo2.UseVisualStyleBackColor = true;
            // 
            // rdo1
            // 
            this.rdo1.AutoSize = true;
            this.rdo1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdo1.Location = new System.Drawing.Point(24, 24);
            this.rdo1.Name = "rdo1";
            this.rdo1.Size = new System.Drawing.Size(81, 17);
            this.rdo1.TabIndex = 9;
            this.rdo1.Text = "伝票明細";
            this.rdo1.UseVisualStyleBackColor = true;
            // 
            // gbxShiiresakiCd
            // 
            this.gbxShiiresakiCd.Controls.Add(this.lblShiiresakiCdTo);
            this.gbxShiiresakiCd.Controls.Add(this.lblCodeBet);
            this.gbxShiiresakiCd.Controls.Add(this.txtShiiresakiCdFr);
            this.gbxShiiresakiCd.Controls.Add(this.lblShiiresakiCdFr);
            this.gbxShiiresakiCd.Controls.Add(this.txtShiiresakiCdTo);
            this.gbxShiiresakiCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxShiiresakiCd.ForeColor = System.Drawing.Color.Black;
            this.gbxShiiresakiCd.Location = new System.Drawing.Point(12, 222);
            this.gbxShiiresakiCd.Name = "gbxShiiresakiCd";
            this.gbxShiiresakiCd.Size = new System.Drawing.Size(795, 75);
            this.gbxShiiresakiCd.TabIndex = 2;
            this.gbxShiiresakiCd.TabStop = false;
            this.gbxShiiresakiCd.Text = "仕入先コード範囲";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtMizuageShishoCd);
            this.groupBox1.Controls.Add(this.lblMizuageShishoNm);
            this.groupBox1.Controls.Add(this.lblMizuageShisho);
            this.groupBox1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBox1.ForeColor = System.Drawing.Color.Black;
            this.groupBox1.Location = new System.Drawing.Point(12, 44);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(341, 83);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "支所";
            // 
            // txtMizuageShishoCd
            // 
            this.txtMizuageShishoCd.AutoSizeFromLength = true;
            this.txtMizuageShishoCd.DisplayLength = null;
            this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtMizuageShishoCd.Location = new System.Drawing.Point(68, 31);
            this.txtMizuageShishoCd.MaxLength = 4;
            this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
            this.txtMizuageShishoCd.Size = new System.Drawing.Size(34, 20);
            this.txtMizuageShishoCd.TabIndex = 1;
            this.txtMizuageShishoCd.TabStop = false;
            this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
            // 
            // lblMizuageShishoNm
            // 
            this.lblMizuageShishoNm.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMizuageShishoNm.Location = new System.Drawing.Point(104, 31);
            this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
            this.lblMizuageShishoNm.Size = new System.Drawing.Size(212, 20);
            this.lblMizuageShishoNm.TabIndex = 2;
            this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMizuageShisho
            // 
            this.lblMizuageShisho.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShisho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShisho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblMizuageShisho.Location = new System.Drawing.Point(26, 29);
            this.lblMizuageShisho.Name = "lblMizuageShisho";
            this.lblMizuageShisho.Size = new System.Drawing.Size(294, 25);
            this.lblMizuageShisho.TabIndex = 3;
            this.lblMizuageShisho.Text = "支所";
            this.lblMizuageShisho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // KBDR2031
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 638);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.gbxDisplayMethod);
            this.Controls.Add(this.grpDateRange);
            this.Controls.Add(this.gbxShiiresakiCd);
            this.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "KBDR2031";
            this.Par1 = "2";
            this.Text = "買掛元帳問合せ";
            this.Controls.SetChildIndex(this.gbxShiiresakiCd, 0);
            this.Controls.SetChildIndex(this.grpDateRange, 0);
            this.Controls.SetChildIndex(this.gbxDisplayMethod, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.pnlDebug.ResumeLayout(false);
            this.grpDateRange.ResumeLayout(false);
            this.grpDateRange.PerformLayout();
            this.gbxDisplayMethod.ResumeLayout(false);
            this.gbxDisplayMethod.PerformLayout();
            this.gbxShiiresakiCd.ResumeLayout(false);
            this.gbxShiiresakiCd.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private jp.co.fsi.common.controls.FsiTextBox txtShiiresakiCdFr;
        private System.Windows.Forms.Label lblShiiresakiCdFr;
        private System.Windows.Forms.Label lblCodeBet;
        private System.Windows.Forms.Label lblShiiresakiCdTo;
        private jp.co.fsi.common.controls.FsiTextBox txtShiiresakiCdTo;
        private System.Windows.Forms.Label lblJpFr;
        private System.Windows.Forms.Label lblJpTo;
        private jp.co.fsi.common.controls.FsiTextBox txtDateYearFr;
        private jp.co.fsi.common.controls.FsiTextBox txtDateYearTo;
        private System.Windows.Forms.GroupBox grpDateRange;
        private jp.co.fsi.common.controls.FsiTextBox txtDateDayTo;
        private jp.co.fsi.common.controls.FsiTextBox txtDateDayFr;
        private jp.co.fsi.common.controls.FsiTextBox txtDateMonthTo;
        private jp.co.fsi.common.controls.FsiTextBox txtDateMonthFr;
        private System.Windows.Forms.Label lblJpMonthTo;
        private System.Windows.Forms.Label lblJpMonthFr;
        private System.Windows.Forms.Label lblJpYearTo;
        private System.Windows.Forms.Label lblJpearFr;
        private System.Windows.Forms.Label lblJpDayFr;
        private System.Windows.Forms.Label lblJpDayTo;
        private System.Windows.Forms.Label lblCodeBetDate;
        private System.Windows.Forms.GroupBox gbxDisplayMethod;
        private System.Windows.Forms.RadioButton rdo2;
        private System.Windows.Forms.RadioButton rdo1;
        private System.Windows.Forms.GroupBox gbxShiiresakiCd;
        private System.Windows.Forms.Label lblEraBackFr;
        private System.Windows.Forms.Label lblEraBackTo;
        private System.Windows.Forms.GroupBox groupBox1;
        private common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private System.Windows.Forms.Label lblMizuageShisho;
    }
}