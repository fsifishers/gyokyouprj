﻿namespace jp.co.fsi.kob.kobc9011
{
    partial class KOBC9013
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbxShiresakiCd = new System.Windows.Forms.GroupBox();
            this.lblNakagaininCdTo = new System.Windows.Forms.Label();
            this.lblCodeBet = new System.Windows.Forms.Label();
            this.txtShiiresakiCdFr = new jp.co.fsi.common.controls.SosTextBox();
            this.lblNakagaininCdFr = new System.Windows.Forms.Label();
            this.txtShiiresakiCdTo = new jp.co.fsi.common.controls.SosTextBox();
            this.pnlDebug.SuspendLayout();
            this.gbxShiresakiCd.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Size = new System.Drawing.Size(574, 23);
            this.lblTitle.Text = "仕入先の登録";
            // 
            // btnF2
            // 
            this.btnF2.Visible = false;
            // 
            // btnF3
            // 
            this.btnF3.Visible = false;
            // 
            // btnF7
            // 
            this.btnF7.Visible = false;
            // 
            // btnF6
            // 
            this.btnF6.Visible = false;
            // 
            // btnF8
            // 
            this.btnF8.Visible = false;
            // 
            // btnF9
            // 
            this.btnF9.Visible = false;
            // 
            // btnF10
            // 
            this.btnF10.Visible = false;
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(5, 43);
            this.pnlDebug.Size = new System.Drawing.Size(607, 100);
            // 
            // gbxShiresakiCd
            // 
            this.gbxShiresakiCd.Controls.Add(this.lblNakagaininCdTo);
            this.gbxShiresakiCd.Controls.Add(this.lblCodeBet);
            this.gbxShiresakiCd.Controls.Add(this.txtShiiresakiCdFr);
            this.gbxShiresakiCd.Controls.Add(this.lblNakagaininCdFr);
            this.gbxShiresakiCd.Controls.Add(this.txtShiiresakiCdTo);
            this.gbxShiresakiCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxShiresakiCd.ForeColor = System.Drawing.Color.Black;
            this.gbxShiresakiCd.Location = new System.Drawing.Point(12, 12);
            this.gbxShiresakiCd.Name = "gbxShiresakiCd";
            this.gbxShiresakiCd.Size = new System.Drawing.Size(579, 75);
            this.gbxShiresakiCd.TabIndex = 904;
            this.gbxShiresakiCd.TabStop = false;
            this.gbxShiresakiCd.Text = "仕入先CD範囲";
            // 
            // lblNakagaininCdTo
            // 
            this.lblNakagaininCdTo.BackColor = System.Drawing.Color.Silver;
            this.lblNakagaininCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblNakagaininCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblNakagaininCdTo.Location = new System.Drawing.Point(349, 32);
            this.lblNakagaininCdTo.Name = "lblNakagaininCdTo";
            this.lblNakagaininCdTo.Size = new System.Drawing.Size(217, 20);
            this.lblNakagaininCdTo.TabIndex = 4;
            this.lblNakagaininCdTo.Text = "最　後";
            this.lblNakagaininCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCodeBet
            // 
            this.lblCodeBet.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblCodeBet.Location = new System.Drawing.Point(280, 31);
            this.lblCodeBet.Name = "lblCodeBet";
            this.lblCodeBet.Size = new System.Drawing.Size(18, 20);
            this.lblCodeBet.TabIndex = 2;
            this.lblCodeBet.Text = "～";
            this.lblCodeBet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShiiresakiCdFr
            // 
            this.txtShiiresakiCdFr.AutoSizeFromLength = false;
            this.txtShiiresakiCdFr.DisplayLength = null;
            this.txtShiiresakiCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShiiresakiCdFr.Location = new System.Drawing.Point(13, 32);
            this.txtShiiresakiCdFr.MaxLength = 4;
            this.txtShiiresakiCdFr.Name = "txtShiiresakiCdFr";
            this.txtShiiresakiCdFr.Size = new System.Drawing.Size(40, 20);
            this.txtShiiresakiCdFr.TabIndex = 2;
            this.txtShiiresakiCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtShiiresakiCdFr_Validating);
            // 
            // lblNakagaininCdFr
            // 
            this.lblNakagaininCdFr.BackColor = System.Drawing.Color.Silver;
            this.lblNakagaininCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblNakagaininCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblNakagaininCdFr.Location = new System.Drawing.Point(56, 32);
            this.lblNakagaininCdFr.Name = "lblNakagaininCdFr";
            this.lblNakagaininCdFr.Size = new System.Drawing.Size(217, 20);
            this.lblNakagaininCdFr.TabIndex = 1;
            this.lblNakagaininCdFr.Text = "先　頭";
            this.lblNakagaininCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShiiresakiCdTo
            // 
            this.txtShiiresakiCdTo.AutoSizeFromLength = false;
            this.txtShiiresakiCdTo.DisplayLength = null;
            this.txtShiiresakiCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShiiresakiCdTo.Location = new System.Drawing.Point(306, 32);
            this.txtShiiresakiCdTo.MaxLength = 4;
            this.txtShiiresakiCdTo.Name = "txtShiiresakiCdTo";
            this.txtShiiresakiCdTo.Size = new System.Drawing.Size(40, 20);
            this.txtShiiresakiCdTo.TabIndex = 3;
            this.txtShiiresakiCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtShiiresakiCdTo_Validating);
            // 
            // KOBC9013
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(599, 146);
            this.Controls.Add(this.gbxShiresakiCd);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "KOBC9013";
            this.ShowFButton = true;
            this.Text = "仕入先の登録";
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.gbxShiresakiCd, 0);
            this.pnlDebug.ResumeLayout(false);
            this.gbxShiresakiCd.ResumeLayout(false);
            this.gbxShiresakiCd.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbxShiresakiCd;
        private System.Windows.Forms.Label lblNakagaininCdTo;
        private System.Windows.Forms.Label lblCodeBet;
        private common.controls.SosTextBox txtShiiresakiCdFr;
        private System.Windows.Forms.Label lblNakagaininCdFr;
        private common.controls.SosTextBox txtShiiresakiCdTo;

    }
}