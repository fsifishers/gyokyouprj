﻿using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;

using System.Linq;
using System.Collections;
using System.Collections.Generic;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;
using jp.co.fsi.common.constants;

namespace jp.co.fsi.kb.kbde1021
{
    #region 構造体
    /// <summary>
    /// 伝票明細計算情報のデータ構造体
    /// </summary>
    struct MeisaiCalcInfo
    {
        public decimal TANKA_SHUTOKU_HOHO;　   // 単価取得方法
        public decimal KINGAKU_HASU_SHORI;     // 金額端数処理
        public decimal SHOHIZEI_NYURYOKU_HOHO; // 消費税入力方法
        public decimal SHOHIZEI_HASU_SHORI;    // 消費税端数処理
        public decimal SHOHIZEI_TENKA_HOHO;    // 消費税転嫁方法

        public int BUMON_CD;                   // 基本部門コード
        public int JIGYO_KUBUN;                // 基本事業区分
        public string DENPYO_DATE;             // 伝票日付保持
        public int TOKUISAKI_CD;               // 変更前の取引先コード
        //public string SHOHIN_SHOHIZEI_KUBUN;   // 商品マスタ消費税区分

        public int KETA_SURYO2;                // バラ数少数桁
        public int KETA_URI_TANKA;             // 売単価少数桁

        public void Clear()
        {
            TANKA_SHUTOKU_HOHO = 0;
            KINGAKU_HASU_SHORI = 1;            // 初期は四捨五入
            SHOHIZEI_NYURYOKU_HOHO = 2;        // 初期は税抜き
            SHOHIZEI_HASU_SHORI = 2;           // 初期は四捨五入
            SHOHIZEI_TENKA_HOHO = 1;           // 初期は明細転嫁

            BUMON_CD = 0;
            //JIGYO_KUBUN = 0;
            DENPYO_DATE = string.Empty;
            TOKUISAKI_CD = 0;
            //SHOHIN_SHOHIZEI_KUBUN = string.Empty;

            KETA_SURYO2 = 2;                   // 少数２桁
            KETA_URI_TANKA = 2;                // 少数２桁
        }
    }
    #endregion

    /// <summary>
    /// 仕入伝票入力(KBDE1021)
    /// </summary>
    public partial class KBDE1021 : BasePgForm
    {
        // 入力モード設定
        private int MODE_EDIT = 1; // 1:登録,2:修正
        // データ入力フラグ(フォームを閉じる時に確認画面を出力時使用)
        private int IS_INPUT = 1;  // 1:未入力,2:入力済
        // 伝票明細計算情報を設定
        MeisaiCalcInfo CalcInfo = new MeisaiCalcInfo();
        // 伝票検索条件
        private string DenpyoCondition = string.Empty;
        // 列の番号
        const int COL_GYO_NO = 0;             // 行No.
        const int COL_KUBUN = 1;              // 区分
        const int COL_SHOHIN_CD = 2;          // 商品CD
        const int COL_SHOHIN_NM = 3;          // 商品名
        const int COL_IRISU = 4;              // 入数
        const int COL_TANI = 5;               // 単位
        const int COL_SURYO1 = 6;             // ケース数
        const int COL_SURYO2 = 7;             // バラ数
        const int COL_URI_TANKA = 8;          // 売単価
        const int COL_BAIKA_KINGAKU = 9;      // 売価金額
        const int COL_SHOHIZEI = 10;          // 消費税
        const int COL_ZEI_RITSU = 11;         // 税率
        const int COL_SHIWAKE_CD = 12;        // 仕訳CD
        const int COL_BUMON_CD = 13;          // 部門CD
        const int COL_ZEI_KUBUN = 14;         // 税区分
        const int COL_JIGYO_KUBUN = 15;       // 事業区分
        const int COL_GEN_TANKA = 16;         // 原単価
        const int COL_KAZEI_KUBUN = 17;       // 課税区分
        const int COL_ZAIKO_KANRI_KUBUN = 18; // 在庫管理区分

        private bool _personCheck = false;    // 担当者チェック

        private bool _lastUpdCheck = false;   // 更新前確認

        #region 追加機能分
        // 商品中止区分
        private int HIN_CHUSHI_KUBUN = 0;     // 1:取引有,その他は中止区分を見ない

        // 消費税計算用
        class MeisaiTbl
        {
            public int Row { get; set; }
            public decimal Kingk { get; set; }
            public decimal Zeigk { get; set; }
            public decimal ZeiRt { get; set; }
            public decimal Gentan { get; set; }
            public decimal Genka { get; set; }
        }

        private int SHIFT_CATEGORY_SLIP_TOTAL = 0; // 強制伝票転嫁
        private int ROUND_CATEGORY_DOWN = 0;       // 強制消費税端数切捨て

        private int _zaikoDisplay = 1;             // 在庫数表示

        // 伝票訂正時の商品毎数量保持用
        private Dictionary<string, decimal> _itemStock;

        private int _torihikiKubun2 = 1;           // 取引区分２（伝票訂正時）
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KBDE1021()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();

            //MessageForwarder(マウスホール用) のインスタンス生成．対象はtxtGridEditとdgvInputList
            new MessageForwarder(this.txtGridEdit, 0x20A);
            new MessageForwarder(this.dgvInputList, 0x20A);
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.InitForm();は呼び出さなくて構いません。
        /// また、このメソッド内の処理を外出しでこのクラス内にメソッド化するのは構いませんが、
        /// 原則、独自で起動時のイベント処理を実装することは禁じます。
        /// </remarks>
        protected override void InitForm()
        {
            // 新規モードの初期表示
            InitDispOnNew();

            // 中止区分対応
            try
            {
                this.HIN_CHUSHI_KUBUN = Util.ToInt(Util.ToString(this.Config.LoadPgConfig(Constants.SubSys.Kob, "KBDE1021", "Setting", "CHUSHI_KUBUN")));
            }
            catch (Exception)
            {
                this.HIN_CHUSHI_KUBUN = 1;
            }
            // 強制設定
            try
            {
                this.SHIFT_CATEGORY_SLIP_TOTAL = Util.ToInt(Util.ToString(this.Config.LoadPgConfig(Constants.SubSys.Kob, "KBDE1021", "Setting", "SHIFT_CATEGORY_SLIP_TOTAL")));
                this.ROUND_CATEGORY_DOWN = Util.ToInt(Util.ToString(this.Config.LoadPgConfig(Constants.SubSys.Kob, "KBDE1021", "Setting", "ROUND_CATEGORY_DOWN")));
            }
            catch (Exception)
            {
                // 設定無しは通常
                this.SHIFT_CATEGORY_SLIP_TOTAL = 0;
                this.ROUND_CATEGORY_DOWN = 0;
            }

            #region 在庫数表示
            try
            {
                this._zaikoDisplay = Util.ToInt(Util.ToString(this.Config.LoadPgConfig(Constants.SubSys.Kob, "KBDE1021", "Setting", "ZaikoDisplay")));
            }
            catch (Exception)
            {
                this._zaikoDisplay = 0;
            }
            if (this._zaikoDisplay != 0)
            {
                this.lblShohinNm.Visible = true;
                this.lblZaikoSu.Visible = true;
            }
            #endregion

            // 基本事業区分の取得
            DataRow r = this.Dba.GetKaikeiSettingsByKessanKi(this.UInfo.KaishaCd, this.UInfo.KessanKi).Rows[0];
            CalcInfo.JIGYO_KUBUN = Util.ToInt(r["JIGYO_KUBUN"]);
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // フォーカス時のみF1～F12を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd":
                case "txtDenpyoNo":
                case "txtGengoYear":
                case "txtTorihikiKubunCd":
                case "txtSiiresakiCd":
                case "txtSiharaisakiCd":
                case "txtTantoshaCd":
                    this.btnF1.Enabled = true;
                    #region 在庫数表示
                    this.zaikoClear();
                    #endregion
                    break;
                case "txtGridEdit":
                    switch (this.dgvInputList.CurrentCell.ColumnIndex)
                    {
                        case COL_KUBUN:     //  1:
                        case COL_SHOHIN_CD: //  2:
                        case COL_BAIKA_KINGAKU:
                            this.btnF1.Enabled = true;
                            break;
                        case COL_URI_TANKA: //  8:
                            if (!ValChk.IsEmpty(this.lblSiiresakiNm.Text))
                            {
                                this.btnF1.Enabled = true;
                            }
                            break;
                        default:
                            this.btnF1.Enabled = false;
                            break;
                    }
                    #region 在庫数表示
                    this.SetZaiko();
                    #endregion
                    break;
                default:
                    this.btnF1.Enabled = false;
                    #region 在庫数表示
                    this.zaikoClear();
                    #endregion
                    break;
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            //MEMO:(参考)Escキー押下時は画面を閉じる処理が基盤側で実装されていますが、
            //PressEsc()をオーバーライドすることでプログラム個別に実装することも可能です。
            System.Reflection.Assembly asm;
            Type t;

            //MEMO:現状アクティブなコントロールごとに処理を実装してください。
            switch (this.ActiveCtlNm)
            {
                #region 元号
                case "txtGengoYear":
                    // アセンブリのロード
                    //asm = System.Reflection.Assembly.LoadFrom("COMC9011.exe");
                    asm = System.Reflection.Assembly.LoadFrom("CMCM1021.exe");
                    // フォーム作成
                    //t = asm.GetType("jp.co.fsi.com.comc9011.COMC9011");
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblGengo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.lblGengo.Text = result[1];

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                SetJp();
                            }
                        }
                    }
                    break;
                #endregion

                #region 伝票番号
                case "txtDenpyoNo":
                    // 仕入伝票検索選択画面
                    using (KBDE1022 frm1022 = new KBDE1022())
                    {
                        // 伝票検索条件
                        frm1022.InData = this.DenpyoCondition;
                        frm1022.ShowDialog(this);
                        if (frm1022.DialogResult == DialogResult.OK)
                        {
                            string[] result = (string[])frm1022.OutData;

                            // 伝票検索条件
                            this.DenpyoCondition = result[1];

                            this.txtDenpyoNo.Text = result[0];
                            // 伝票データチェック後、伝票修正処理へ
                            this.GetDenpyoData();
                        }
                        else
                        {
                            // 伝票検索条件クリア
                            this.DenpyoCondition = string.Empty;
                        }
                    }
                    break;
                #endregion

                #region 取引区分
                case "txtTorihikiKubunCd":
                    // アセンブリのロード
                    //asm = System.Reflection.Assembly.LoadFrom("COMC8011.exe");
                    asm = System.Reflection.Assembly.LoadFrom("CMCM1041.exe");
                    // フォーム作成
                    //t = asm.GetType("jp.co.fsi.com.comc8011.COMC8011");
                    t = asm.GetType("jp.co.fsi.cm.cmcm1041.CMCM1041");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.txtTorihikiKubunCd.Text;
                            frm.Par1 = "TB_HN_F_TORIHIKI_KUBUN2";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtTorihikiKubunCd.Text = outData[0];
                                this.lblTorihikiKubunNm.Text = outData[1];
                            }
                        }
                    }
                    break;
                #endregion

                #region 仕入先CD
                case "txtSiiresakiCd":
                case "txtSiharaisakiCd":
                    // アセンブリのロード
                    //asm = System.Reflection.Assembly.LoadFrom("KOBC9011.exe");
                    asm = System.Reflection.Assembly.LoadFrom("KBCM1011.exe");
                    // フォーム作成
                    //t = asm.GetType("jp.co.fsi.kob.kobc9011.KOBC9011");
                    t = asm.GetType("jp.co.fsi.kb.kbcm1011.KBCM1011");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                if (this.ActiveCtlNm == "txtSiiresakiCd")
                                {
                                    this.txtSiiresakiCd.Text = outData[0];
                                    this.lblSiiresakiNm.Text = outData[1];
                                }
                                
                                this.txtSiharaisakiCd.Text = outData[0];
                                this.SetSiiresakiInfo();
                            }
                        }
                    }
                    break;
                #endregion

                #region 担当者CD
                case "txtTantoshaCd":
                    // アセンブリのロード
                    //asm = System.Reflection.Assembly.LoadFrom("KOBC9041.exe");
                    asm = System.Reflection.Assembly.LoadFrom("CMCM2021.exe");
                    // フォーム作成
                    //t = asm.GetType("jp.co.fsi.kob.kobc9041.KOBC9041");
                    t = asm.GetType("jp.co.fsi.cm.cmcm2021.CMCM2021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtTantoshaCd.Text = outData[0];
                                this.lblTantoshaNm.Text = outData[1];
                            }
                        }
                    }
                    break;
                #endregion

                #region 水揚支所
                case "txtMizuageShishoCd": // 水揚支所
                    asm = System.Reflection.Assembly.LoadFrom("CMCM2031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2031.CMCM2031");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtMizuageShishoCd.Text = outData[0];
                                this.lblMizuageShishoNm.Text = outData[1];

                                // 次の項目(船主コード)にフォーカス
                                this.txtSiiresakiCd.Focus();
                            }
                        }
                    }
                    break;
                #endregion

                #region 明細
                case "txtGridEdit":
                    switch (this.dgvInputList.CurrentCell.ColumnIndex)
                    {
                        case COL_KUBUN:  // 1:
                            // アセンブリのロード
                            //asm = System.Reflection.Assembly.LoadFrom("COMC8011.exe");
                            asm = System.Reflection.Assembly.LoadFrom("CMCM1041.exe");
                            // フォーム作成
                            //t = asm.GetType("jp.co.fsi.com.comc8011.COMC8011");
                            t = asm.GetType("jp.co.fsi.cm.cmcm1041.CMCM1041");
                            if (t != null)
                            {
                                Object obj = System.Activator.CreateInstance(t);
                                if (obj != null)
                                {
                                    BasePgForm frm = (BasePgForm)obj;
                                    frm.Par1 = "VI_HN_HANBAI_KUBUN_NM";
                                    frm.ShowDialog(this);

                                    if (frm.DialogResult == DialogResult.OK)
                                    {
                                        string[] outData = (string[])frm.OutData;
                                        this.txtGridEdit.Text = outData[0];
                                    }
                                }
                            }
                            this.txtGridEdit.Focus();
                            break;
                        case COL_SHOHIN_CD:  // 2:
                            // 商品一覧選択KOBC9031を修正必要有
                            // アセンブリのロード
                            //asm = System.Reflection.Assembly.LoadFrom("KOBC9031.exe");
                            asm = System.Reflection.Assembly.LoadFrom("KBCM1021.exe");
                            // フォーム作成
                            //t = asm.GetType("jp.co.fsi.kob.kobc9031.KOBC9031");
                            t = asm.GetType("jp.co.fsi.kb.kbcm1021.KBCM1021");
                            if (t != null)
                            {
                                Object obj = System.Activator.CreateInstance(t);
                                if (obj != null)
                                {
                                    BasePgForm frm = (BasePgForm)obj;
                                    frm.Par1 = "1";
                                    // 中止区分対応
                                    if (this.HIN_CHUSHI_KUBUN == 1)
                                    {
                                        frm.Par3 = this.HIN_CHUSHI_KUBUN.ToString();
                                    }
                                    frm.ShowDialog(this);

                                    if (frm.DialogResult == DialogResult.OK)
                                    {
                                        string[] outData = (string[])frm.OutData;
                                        this.txtGridEdit.Text = outData[0];
                                        // 商品情報取得
                                        this.GetShohinInfo(2);
                                    }
                                }
                            }
                            this.txtGridEdit.Focus();
                            break;
                        case COL_URI_TANKA:  // 8:
                            if (!ValChk.IsEmpty(this.lblSiiresakiNm.Text) && !ValChk.IsEmpty(this.dgvInputList[COL_SHOHIN_CD, this.dgvInputList.CurrentCell.RowIndex].Value))
                            {
                                // 仕入単価参照選択画面
                                using (KBDE1023 frm1023 = new KBDE1023(Util.ToDecimal(this.txtSiiresakiCd.Text), Util.ToDecimal(this.txtSiharaisakiCd.Text), Util.ToDecimal(this.dgvInputList[COL_SHOHIN_CD, this.dgvInputList.CurrentCell.RowIndex].Value)))
                                {
                                    frm1023.ShowDialog(this);

                                    if (frm1023.DialogResult == DialogResult.OK)
                                    {
                                        string[] result = (string[])frm1023.OutData;

                                        this.txtGridEdit.Text = Util.FormatNum(result[0], this.CalcInfo.KETA_URI_TANKA);
                                        this.dgvInputList[COL_URI_TANKA, this.dgvInputList.CurrentCell.RowIndex].Value = Util.FormatNum(result[0], this.CalcInfo.KETA_URI_TANKA);

                                        // 金額と消費税計算
                                        if (!IsCalc(this.dgvInputList.CurrentCell.RowIndex))
                                        {
                                            this.dgvInputList[COL_BAIKA_KINGAKU, this.dgvInputList.CurrentCell.RowIndex].Value = "";
                                            this.dgvInputList[COL_SHOHIZEI, this.dgvInputList.CurrentCell.RowIndex].Value = "";
                                        }
                                        else
                                        {
                                            CellKingkSet(this.dgvInputList.CurrentCell.RowIndex);
                                        }
                                        this.DataGridSum();
                                    }
                                }
                            }
                            this.txtGridEdit.Focus();
                            break;
                        case COL_BAIKA_KINGAKU:
                            // 消費税率の検索
                            // アセンブリのロード
                            asm = System.Reflection.Assembly.LoadFrom("ZMCM1061.exe");
                            // フォーム作成
                            t = asm.GetType("jp.co.fsi.zm.zmcm1061.ZMCM1065");
                            if (t != null)
                            {
                                Object obj = System.Activator.CreateInstance(t);
                                if (obj != null)
                                {
                                    bool selected = false;

                                    BasePgForm frm = (BasePgForm)obj;
                                    frm.InData = Util.ToString(this.dgvInputList[COL_ZEI_KUBUN, this.dgvInputList.CurrentCell.RowIndex].Value);

                                    while (!selected)
                                    {
                                        frm.ShowDialog(this);

                                        if (frm.DialogResult == DialogResult.OK)
                                        {
                                            string[] outData = (string[])frm.OutData;

                                            DateTime DENPYO_DATE = Util.ConvAdDate(this.lblGengo.Text, this.txtGengoYear.Text,
                                                                                   this.txtMonth.Text, this.txtDay.Text, this.Dba);
                                            // 選択結果を基準日適合チェック
                                            decimal taxRate = 0;
                                            DialogResult ans = ConfPastZeiKbn(DENPYO_DATE, outData[0], ref taxRate, Util.ToDecimal(Util.ToString(outData[2])));
                                            if (ans == DialogResult.OK)
                                            {
                                                this.dgvInputList[COL_ZEI_KUBUN, this.dgvInputList.CurrentCell.RowIndex].Value = outData[0];
                                                this.dgvInputList[COL_ZEI_RITSU, this.dgvInputList.CurrentCell.RowIndex].Value = Util.FormatNum(Util.ToDecimal(outData[2]), 1);
                                                this.dgvInputList[COL_KAZEI_KUBUN, this.dgvInputList.CurrentCell.RowIndex].Value = outData[3];
                                                CellTaxSet(this.dgvInputList.CurrentCell.RowIndex);
                                                selected = true;
                                            }
                                        }
                                        else
                                        {
                                            selected = true;
                                        }
                                    }
                                }
                            }
                            this.txtGridEdit.Focus();
                            break;
                    }
                    break;
                #endregion
            }
        }

        /// <summary>
        /// F3キー押下時処理
        /// </summary>
        public override void PressF3()
        {
            if (!this.btnF3.Enabled)
                return;

            DbParamCollection dpc;
            DataTable dtCheck;

            // 登録モードは処理しない
            if (this.MODE_EDIT == 1)
            {
                return;
            }

            // 伝票の入力チェック
            if (!this.isValidDenpyoNo())
            {
                this.txtDenpyoNo.SelectAll();
                this.txtDenpyoNo.Focus();
                return;
            }
            // 空の場合、特に何もしない
            else if (ValChk.IsEmpty(this.txtDenpyoNo.Text))
            {
                return;
            }

            // 会計年度の凍結処理チェック 凍結されていた場合はアラートを表示し処理を行なわない
            if (Util.GetKaikeiNendoFixedFlg(this.UInfo.KaikeiNendo, this.Dba))
            {
                Msg.Error("この会計年度は凍結されています。");
                return;
            }

            // 伝票番号確認処理
            // Han.TB_取引伝票(TB_HN_TORIHIKI_DENPYO)
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToInt(Util.ToString(txtMizuageShishoCd.Text)));
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            dpc.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 4, 2);
            dpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 8, Util.ToDecimal(this.txtDenpyoNo.Text));
            dtCheck = this.Dba.GetDataTableByConditionWithParams(
                "COUNT(*) AS CNT",
                "TB_HN_TORIHIKI_DENPYO",
                "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND KAIKEI_NENDO = @KAIKEI_NENDO AND DENPYO_KUBUN = @DENPYO_KUBUN AND DENPYO_BANGO = @DENPYO_BANGO",
                dpc);
            if (Util.ToLong(dtCheck.Rows[0]["CNT"]) == 0)
            {
                return;
            }

            string msg = "削除しますか？";
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 削除処理
            this.DeleteData();

            // 登録初期表示処理へ
            this.InitDispOnNew();
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        public override void PressF6()
        {
            if (!this.btnF6.Enabled)
                return;

            // 会計年度の凍結処理チェック 凍結されていた場合はアラートを表示し処理を行なわない
            if (Util.GetKaikeiNendoFixedFlg(this.UInfo.KaikeiNendo, this.Dba))
            {
                Msg.Error("この会計年度は凍結されています。");
                return;
            }

            this._lastUpdCheck = true;   // 更新前確認

            // 入力チェック
            if (!this.ValidateAll())
            {
                return;
            }
            this.DataGridSum();

            this._lastUpdCheck = false;   // 更新前確認

            DateTime DENPYO_DATE = Util.ConvAdDate(this.lblGengo.Text, this.txtGengoYear.Text,
                    this.txtMonth.Text, this.txtDay.Text, this.Dba);
            // 会計年度の凍結処理チェック 凍結されていた場合はアラートを表示し処理を行なわない
            if (Util.GetKaikeiNendo(DENPYO_DATE, this.Dba) != this.UInfo.KaikeiNendo)
            {
                Msg.Error("入力日付が選択会計年度の範囲外です。");
                return;
            }

            string msg = (this.MODE_EDIT == 1 ? "登録" : "更新") + "しますか？";
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }
            // 登録処理
            this.UpdateData();

            // 登録初期表示処理へ
            this.InitDispOnNew();
        }

        /// <summary>
        /// F8キー押下時処理
        /// </summary>
        public override void PressF8()
        {
            // 初期起動時
            if (this.dgvInputList.CurrentCell == null)
                return;

            // 行削除処理
            if (this.dgvInputList.RowCount > 1 && this.dgvInputList.RowCount > this.dgvInputList.CurrentCell.RowIndex + 1)
            {
                this.dgvInputList.Rows.RemoveAt(this.dgvInputList.CurrentCell.RowIndex);
                int i = 0;
                while (this.dgvInputList.RowCount > i)
                {
                    this.dgvInputList[COL_GYO_NO, i].Value = i + 1;
                    i++;
                }
                this.DataGridSum();
            }
            if (this.ActiveControl != dgvInputList)
                dgvInputList.Focus();

            try
            {
                DataGridViewCell dgvCell = (DataGridViewCell)dgvInputList.CurrentCell;
                if (dgvCell != null)
                {
                    this.dgvInputList.CurrentCell = this.dgvInputList[COL_SHOHIN_CD, dgvCell.RowIndex];
                }
            }
            catch (Exception) { }
        }

        /// <summary>
        /// F9キー押下時処理
        /// </summary>
        public override void PressF9()
        {
            // 初期起動時
            if (this.dgvInputList.CurrentCell == null)
                return;
            // 複写処理(上の行データを選択行データにコピー)
            if (this.dgvInputList.CurrentCell.RowIndex > 0)
            {
                int col = 1;
                int rowCurPos = this.dgvInputList.CurrentCell.RowIndex;
                DataGridViewRow getRow = this.dgvInputList.Rows[rowCurPos - 1];
                while (getRow.Cells.Count > col)
                {
                    this.dgvInputList[col, rowCurPos].Value = this.dgvInputList[col, rowCurPos - 1].Value;
                    col++;
                }
                if (this.txtGridEdit.Visible == true)
                {
                    this.txtGridEdit.Text = Util.ToString(this.dgvInputList[this.dgvInputList.CurrentCell.ColumnIndex, this.dgvInputList.CurrentCell.RowIndex].Value);
                }
                if (this.dgvInputList.Rows.Count == rowCurPos + 1)
                {
                    this.dgvInputList.RowCount = this.dgvInputList.RowCount + 1;
                    this.dgvInputList[COL_GYO_NO, this.dgvInputList.RowCount - 1].Value = this.dgvInputList.RowCount;
                }
                this.DataGridSum();

                if (this.ActiveControl != dgvInputList)
                    dgvInputList.Focus();
            }
        }

        /// <summary>
        /// F10キー押下時処理
        /// </summary>
        public override void PressF10()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }
        #endregion

        #region イベント
        private decimal MEISAI_DENPYO_BANGO = 0;
        /// <summary>
        /// 伝票番号の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDenpyoNo_Validating(object sender, CancelEventArgs e)
        {
            if (!this.isValidDenpyoNo())
            {
                this.txtDenpyoNo.SelectAll();
                this.txtDenpyoNo.Focus();
                e.Cancel = true;
                return;
            }
            // 空の場合、特に何もしない
            else if (ValChk.IsEmpty(this.txtDenpyoNo.Text))
            {
                e.Cancel = false;
                return;
            }

            this.IS_INPUT = 2;

            // 伝票データチェック後、伝票修正処理へ
            e.Cancel = this.GetDenpyoData();
        }

        /// <summary>
        /// 年の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGengoYear_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtGengoYear.Text, this.txtGengoYear.MaxLength))
            {
                e.Cancel = true;
                this.txtGengoYear.SelectAll();
            }
            else
            {
                this.txtGengoYear.Text = Util.ToString(IsValid.SetYear(this.txtGengoYear.Text));
                CheckJp();
                SetJp();

                this.IS_INPUT = 2;
            }
        }

        /// <summary>
        /// 月の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMonth_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtMonth.Text, this.txtMonth.MaxLength))
            {
                e.Cancel = true;
                this.txtMonth.SelectAll();
            }
            else
            {
                this.txtMonth.Text = Util.ToString(IsValid.SetMonth(this.txtMonth.Text));
                CheckJp();
                SetJp();

                this.IS_INPUT = 2;
            }
        }

        /// <summary>
        /// 日の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDay_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsDay(this.txtDay.Text, this.txtDay.MaxLength))
            {
                e.Cancel = true;
                this.txtDay.SelectAll();
            }
            else
            {
                this.txtDay.Text = Util.ToString(IsValid.SetDay(this.txtDay.Text));
                CheckJp();
                SetJp();

                this.IS_INPUT = 2;
            }
        }

        /// <summary>
        /// 取引区分の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTorihikiKubunCd_Validating(object sender, CancelEventArgs e)
        {
            if (!this.isValidTorihikiKubunCd())
            {
                this.txtTorihikiKubunCd.SelectAll();
                this.txtTorihikiKubunCd.Focus();
                e.Cancel = true;
                return;
            }

            this.IS_INPUT = 2;
        }

        /// <summary>
        /// 仕入先コードの値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSiiresakiCd_Validating(object sender, CancelEventArgs e)
        {
            // 消費税転嫁方法、消費税入力方法をクリア
            this.lblShohizeiInputHoho2.Text = "";
            this.lblShohizeiTenka2.Text = "";

            if (!this.isValidSiiresakiCd())
            {
                this.txtSiiresakiCd.SelectAll();
                this.txtSiiresakiCd.Focus();
                e.Cancel = true;
                return;
            }

            // 仕入先変更時（事前保持）
            int bef = this.CalcInfo.TOKUISAKI_CD;

            this.SetSiiresakiInfo();

            // 仕入先変更時の明細再計算 
            if (bef != 0 && !ValChk.IsEmpty(this.txtSiiresakiCd.Text) && (Util.ToInt(this.txtSiiresakiCd.Text) != bef))
            {
                CellTaxRateSet();
            }

            this.IS_INPUT = 2;
        }

        /// <summary>
        /// 支払先コードの値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSiharaisakiCd_Validating(object sender, CancelEventArgs e)
        {
            //// 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            //if (!ValChk.IsNumber(this.txtSiharaisakiCd.Text))
            //{
            //    Msg.Notice("数値のみで入力してください。");
            //    this.txtSiharaisakiCd.SelectAll();
            //    e.Cancel = true;
            //    return;
            //}

            //this.IS_INPUT = 2;

            //// コードを元に名称を取得する
            //// 取得された場合、名称をラベルに反映する
            //e.Cancel = this.SetSiiresakiInfo();
        }

        /// <summary>
        /// 担当者コードの値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTantoshaCd_Validating(object sender, CancelEventArgs e)
        {
            this._personCheck = false;
            if (!this.isValidTantoshaCd())
            {
                this.txtTantoshaCd.SelectAll();
                this.txtTantoshaCd.Focus();
                e.Cancel = true;
                return;
            }
            this._personCheck = true;

            this.IS_INPUT = 2;
        }

        /// <summary>
        /// 担当者コードからの移動制御
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTantoshaCd_KeyDown(object sender, KeyEventArgs e)
        {
            // ↑キーの場合、船主CDにフォーカス
            if (e.KeyCode == Keys.Up)
            {
                txtSiiresakiCd.Focus();
            }

            // Enterキーの場合、山Noにフォーカス
            if (this._personCheck && e.KeyCode == Keys.Enter)
            {
                this._personCheck = false;
                dgvInputList.Focus();
                dgvInputList.CurrentCell = dgvInputList[COL_SHOHIN_CD, 0];
            }
        }

        /// <summary>
        /// 水揚支所入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMizuageShishoCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                e.Cancel = true;
                this.txtMizuageShishoCd.SelectAll();
                this.txtMizuageShishoCd.Focus();
            }

            this.IS_INPUT = 2;

            // 次の項目(仕入先)にフォーカス
            this.txtSiiresakiCd.Focus();// 要確認
        }

        /// <summary>
        /// データグリッドにフォーカス時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvInputList_Enter(object sender, EventArgs e)
        {
            try
            {
                DataGridViewCell dgvCell = (DataGridViewCell)dgvInputList.CurrentCell;
                if (dgvCell != null && dgvCell.ColumnIndex == 0)
                {
                    this.dgvInputList.CurrentCell = this.dgvInputList[COL_SHOHIN_CD, dgvCell.RowIndex];
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        /// <summary>
        /// データグリッドのセルにフォーカス時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvInputList_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            this.txtGridEdit.TextAlign = HorizontalAlignment.Right;
            this.txtGridEdit.MaxLength = 10;
            this.txtGridEdit.ImeMode = ImeMode.Disable;

            switch (e.ColumnIndex)
            {
                case COL_KUBUN:        //  1:  // 区分
                case COL_SHOHIN_CD:    //  2:  // 商品CD
                case COL_SHOHIN_NM:    //  3:  // 商品名（在庫管理無し時は編集可に）
                //case 4: // 入数
                //case 5: // 単位
                case COL_SURYO1:       //  6:  // ケース（入数が存在する場合）
                case COL_SURYO2:       //  7:  // バラ
                case COL_URI_TANKA:    //  8:  // 売価
                case COL_BAIKA_KINGAKU://  9:  // 金額
                                       //case 10: // 消費税の部分を入力不可に
                case COL_SHOHIZEI:     // 10:  // 消費税（明細転嫁時は編集可に）
                    if (Util.ToInt(this.CalcInfo.SHOHIZEI_TENKA_HOHO) != 1)
                    {
                        if (e.ColumnIndex == COL_SHOHIZEI)
                        {
                            // 明細転嫁以外は消費税スキップ
                            break;
                        }
                    }
                    if ((e.ColumnIndex == COL_IRISU || e.ColumnIndex == COL_SURYO1) && Util.ToInt(Util.ToString(this.dgvInputList[COL_IRISU, e.RowIndex].Value)) == 0)
                    {
                        // 入数が無い場合はケース数スキップ
                        break;
                    }

                    if (e.ColumnIndex == COL_SHOHIZEI && ValChk.IsEmpty(this.dgvInputList[COL_ZEI_RITSU, e.RowIndex].Value))
                    {
                        if (ValChk.IsEmpty(this.dgvInputList[COL_ZEI_KUBUN, e.RowIndex].Value))
                        {
                            // 税率、税区分が無い場合は消費税スキップ
                            break;
                        }
                    }
                    // 在庫管理区分
                    if (e.ColumnIndex == COL_SHOHIN_NM && Util.ToInt(Util.ToString(this.dgvInputList[COL_ZAIKO_KANRI_KUBUN, e.RowIndex].Value)) == 1)
                    {
                        // 在庫管理する場合は商品名スキップ
                        break;
                    }
                    else if (e.ColumnIndex == COL_SHOHIN_NM && ValChk.IsEmpty(this.dgvInputList[COL_SHOHIN_CD, e.RowIndex].Value))
                        break;
                    else if (e.ColumnIndex == COL_SHOHIN_NM)
                    {
                        this.txtGridEdit.TextAlign = HorizontalAlignment.Left;
                        this.txtGridEdit.MaxLength = 40;
                        this.txtGridEdit.ImeMode = ImeMode.Hiragana;
                    }

                    this.txtGridEdit.BackColor = Color.White;
                    if (e.RowIndex % 2 == 1)
                    {
                        this.txtGridEdit.BackColor = ColorTranslator.FromHtml("#BBFFFF");
                    }
                    this.txtGridEdit.Visible = true;
                    DataGridView dgv = (DataGridView)sender;
                    Rectangle rctCell = dgv.GetCellDisplayRectangle(e.ColumnIndex, e.RowIndex, false);

                    this.txtGridEdit.Size = rctCell.Size;
                    this.txtGridEdit.Top = rctCell.Top + this.dgvInputList.Top;
                    this.txtGridEdit.Left = rctCell.Left + this.dgvInputList.Left;
                    this.txtGridEdit.Text = Util.ToString(dgvInputList[e.ColumnIndex, e.RowIndex].Value).Replace(",", "");

                    this.txtGridEdit.SelectAll();

                    this.txtGridEdit.Focus();
                    break;
                default:
                    this.txtGridEdit.Visible = false;
                    break;
            }
        }

        /// <summary>
        /// データグリッドのマウスダウン時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvInputList_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            // 検索制御
            switch (e.ColumnIndex)
            {
                case COL_SHOHIN_NM:    // 3:   // 商品名
                case COL_IRISU:        // 4    // 入数
                case COL_TANI:         // 5:   // 単位
                case COL_SURYO1:       // 6:   // ケース
                case COL_SURYO2:       // 7:   // バラ
                case COL_SHOHIZEI:     // 10:  // 消費税
                case COL_ZEI_RITSU:    // 11:  // 消費税
                    this.btnF1.Enabled = false;
                    break;
                case COL_KUBUN:        //  1:  // 区分
                case COL_SHOHIN_CD:    //  2:  // 商品CD
                case COL_URI_TANKA:    //  8:  // 売価
                case COL_BAIKA_KINGAKU://  9:  // 金額
                    this.btnF1.Enabled = true;
                    break;
            }
        }

        /// <summary>
        /// データグリッドがスクロールした時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvInputList_Scroll(object sender, ScrollEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            Rectangle rctCell = dgv.GetCellDisplayRectangle(this.dgvInputList.CurrentCell.ColumnIndex, this.dgvInputList.CurrentCell.RowIndex, false);

            this.txtGridEdit.Size = rctCell.Size;
            this.txtGridEdit.Top = rctCell.Top + this.dgvInputList.Top;
            this.txtGridEdit.Left = rctCell.Left + this.dgvInputList.Left;

        }

        /// <summary>
        /// データグリッドの書式設定
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvInputList_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            switch (e.ColumnIndex)
            {
                case COL_SURYO1:
                case COL_SURYO2:// 7:
                case COL_BAIKA_KINGAKU:// 9:
                case COL_SHOHIZEI:// 10:
                    decimal d = Util.ToDecimal(Util.ToString(e.Value));
                    if (d < 0)
                    {
                        e.CellStyle.ForeColor = Color.Red;
                    }
                    else
                    {
                        e.CellStyle.ForeColor = Color.Black;
                    }
                    break;
            }
        }

        /// <summary>
        /// データグリッド用のテキストにキーダウン時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGridEdit_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Down || e.KeyCode == Keys.Up || e.KeyCode == Keys.Right)
            {
                DataGridViewCell dgvCell = (DataGridViewCell)dgvInputList.CurrentCell;
                decimal inputData = 0;

                // 新規行へ下カーソルは商品のみ
                switch (dgvCell.ColumnIndex)
                {
                    case COL_KUBUN:
                    case COL_TANI:
                    case COL_SURYO1:
                    case COL_SURYO2:
                    case COL_URI_TANKA:
                    case COL_BAIKA_KINGAKU:
                    case COL_SHOHIZEI:
                        {
                            if (e.KeyCode == Keys.Down)
                            {
                                int rows = this.dgvInputList.Rows.Count - 1;
                                if ((dgvCell.RowIndex + 1) == rows)
                                {
                                    this.txtGridEdit.Focus();
                                    return;
                                }
                            }
                        }
                        break;
                }

                switch (dgvCell.ColumnIndex)
                {
                    // 区（販売区分名称）
                    case COL_KUBUN: //  1:
                        if (ValChk.IsEmpty(txtGridEdit.Text))
                        {
                            this.dgvInputList[COL_KUBUN, dgvCell.RowIndex].Value = "";
                        }
                        else if (!ValChk.IsNumber(txtGridEdit.Text))
                        {
                            this.dgvInputList[COL_KUBUN, dgvCell.RowIndex].Value = Regex.Replace(this.txtGridEdit.Text, "\\D", "");
                            this.txtGridEdit.Focus();
                            return;
                        }
                        else
                        {
                            // 販売区分名称情報取得
                            string get_name = this.Dba.GetName(this.UInfo, "VI_HN_HANBAI_KUBUN_NM", this.txtMizuageShishoCd.Text, this.txtGridEdit.Text);
                            if (get_name == null)
                            {
                                this.dgvInputList[COL_KUBUN, dgvCell.RowIndex].Value = "";
                                this.txtGridEdit.Text = "";
                                Msg.Error("入力に誤りがあります。");
                                this.txtGridEdit.Focus();
                                return;
                            }
                            else
                            {
                                this.dgvInputList[COL_KUBUN, dgvCell.RowIndex].Value = this.txtGridEdit.Text;
                            }
                        }

                        // カーソル移動制御
                        if (this.GetDenpyoMeisaiKeyEnter(e.KeyCode, dgvCell))
                        {
                            this.dgvInputList.CurrentCell = dgvInputList[COL_SHOHIN_CD, dgvCell.RowIndex];
                        }
                        else
                        {
                            if(dgvCell.RowIndex > 0)
                                this.dgvInputList.CurrentCell = dgvInputList[COL_KUBUN, dgvCell.RowIndex - 1];

                            // 担当者へ
                            if (e.KeyCode == Keys.Up)
                                this.txtTantoshaCd.Focus();

                            this.txtGridEdit.Focus();

                        }

                        if (e.KeyCode == Keys.Right)
                        {
                            dgvInputList.CurrentCell = dgvInputList[COL_SHOHIN_CD, dgvCell.RowIndex];
                            this.txtDummy.Focus();
                            this.txtGridEdit.Focus();
                        }

                        break;
                    // 商品コード
                    case COL_SHOHIN_CD: //  2:
                        if (ValChk.IsEmpty(txtGridEdit.Text) || !ValChk.IsNumber(txtGridEdit.Text))
                        {
                            this.dgvInputList[COL_SHOHIN_CD, dgvCell.RowIndex].Value = "";
                            this.dgvInputList[COL_SHOHIN_NM, dgvCell.RowIndex].Value = "";
                            this.dgvInputList[COL_SHIWAKE_CD, dgvCell.RowIndex].Value = "";
                            this.dgvInputList[COL_BUMON_CD, dgvCell.RowIndex].Value = "";
                            this.dgvInputList[COL_ZEI_KUBUN, dgvCell.RowIndex].Value = "";
                            this.dgvInputList[COL_JIGYO_KUBUN, dgvCell.RowIndex].Value = "";
                            this.dgvInputList[COL_ZEI_RITSU, dgvCell.RowIndex].Value = "";
                            this.dgvInputList[COL_GEN_TANKA, dgvCell.RowIndex].Value = "";
                            this.dgvInputList[COL_KAZEI_KUBUN, dgvCell.RowIndex].Value = "";
                            this.dgvInputList[COL_ZAIKO_KANRI_KUBUN, dgvCell.RowIndex].Value = "";
                            this.dgvInputList[COL_IRISU, dgvCell.RowIndex].Value = "";
                            this.dgvInputList[COL_TANI, dgvCell.RowIndex].Value = "";
                            Msg.Error("入力に誤りがあります。");
                            this.txtGridEdit.Focus();
                            return;
                        }
                        
                        // 商品情報取得
                        this.GetShohinInfo(1);

                        // カーソル移動制御
                        if (!this.GetDenpyoMeisaiKeyEnter(e.KeyCode, dgvCell))
                        {
                            this.dgvInputList.CurrentCell = this.dgvInputList[COL_SHOHIN_CD, this.dgvInputList.CurrentCell.RowIndex];
                            // 担当者へ
                            if (e.KeyCode == Keys.Up)
                                this.txtTantoshaCd.Focus();
                        }

                        if (e.KeyCode == Keys.Right)
                        {
                            dgvInputList.CurrentCell = dgvInputList[COL_SURYO2, dgvCell.RowIndex];
                            this.txtDummy.Focus();
                            this.txtGridEdit.Focus();
                        }
                        break;
                    // 商品名
                    case COL_SHOHIN_NM: //  3:
                        // チェック
                        // 40バイトを超えていたらエラー
                        if (!ValChk.IsWithinLength(txtGridEdit.Text, txtGridEdit.MaxLength))
                        {
                            Msg.Error("入力に誤りがあります。");
                            this.txtGridEdit.Focus();
                            return;
                        }

                        dgvInputList[COL_SHOHIN_NM, dgvCell.RowIndex].Value = Util.ToString(txtGridEdit.Text);

                        // カーソル移動制御
                        if (this.GetDenpyoMeisaiKeyEnter(e.KeyCode, dgvCell))
                        {
                            if (Util.ToInt(Util.ToString(this.dgvInputList[COL_IRISU, dgvCell.RowIndex].Value)) == 0)
                                this.dgvInputList.CurrentCell = this.dgvInputList[COL_SURYO2, dgvCell.RowIndex];
                            else
                                this.dgvInputList.CurrentCell = this.dgvInputList[COL_SURYO1, dgvCell.RowIndex];
                        }

                        if (e.KeyCode == Keys.Right)
                        {
                            dgvInputList.CurrentCell = dgvInputList[COL_TANI, dgvCell.RowIndex];
                            this.txtDummy.Focus();
                            this.txtGridEdit.Focus();
                        }

                        break;
                    // 単位
                    case COL_TANI: //  5:
                        if (this.GetDenpyoMeisaiKeyEnter(e.KeyCode, dgvCell))
                        {
                            this.dgvInputList.CurrentCell = this.dgvInputList[COL_SURYO2, dgvCell.RowIndex];
                        }
                        break;
                    // ケース数
                    case COL_SURYO1: //  6:
                        if (!ValChk.IsDecNumWithinLength(txtGridEdit.Text, 19, 2, true))
                        {
                            this.txtGridEdit.Text = Regex.Replace(this.txtGridEdit.Text, "^[a-zA-Z]+$", "");
                            this.txtGridEdit.Focus();
                            return;
                        }

                        // 変更時
                        if (Util.ToDecimal(Util.ToString(dgvInputList[COL_SURYO1, dgvCell.RowIndex].Value)) != Util.ToDecimal(Util.ToString(txtGridEdit.Text)))
                        {
                            dgvInputList[COL_SURYO1, dgvCell.RowIndex].Value = Util.FormatNum(Util.ToDecimal(Util.ToString(txtGridEdit.Text)));

                            // 消費税計算
                            if (!CellKingkSet(dgvCell.RowIndex))
                            {
                                Msg.Error("金額が大きすぎる為、複数明細へ分けて入力を行って下さい。");
                                this.txtGridEdit.Focus();
                                return;
                            }
                            this.DataGridSum();
                        }

                        // カーソル移動制御
                        if (this.GetDenpyoMeisaiKeyEnter(e.KeyCode, dgvCell))
                        {
                            dgvInputList.CurrentCell = dgvInputList[COL_SURYO2, dgvCell.RowIndex];
                        }

                        if (e.KeyCode == Keys.Right)
                        {
                            dgvInputList.CurrentCell = dgvInputList[COL_SURYO2, dgvCell.RowIndex];
                            this.txtDummy.Focus();
                            this.txtGridEdit.Focus();
                        }

                        break;
                    // バラ数
                    case COL_SURYO2: //  7:
                        //if (!ValChk.IsDecNumWithinLength(txtGridEdit.Text, 19, 1, true))
                        if (!ValChk.IsDecNumWithinLength(txtGridEdit.Text, 19, 3, true))
                        {
                            this.txtGridEdit.Text = Regex.Replace(this.txtGridEdit.Text, "^[a-zA-Z]+$", "");
                            this.txtGridEdit.Focus();
                            return;
                        }

                        // 小数部整理
                        inputData = Util.ToDecimal(Util.ToString(txtGridEdit.Text)) * 100;
                        inputData = TaxUtil.CalcFraction(inputData, 0, 1) / 100; // 切り捨て
                        decimal w = Util.ToDecimal(Util.FormatNum(inputData, this.CalcInfo.KETA_SURYO2));

                        // 入数あり
                        if (Util.ToInt(Util.ToString(dgvInputList[COL_IRISU, dgvCell.RowIndex].Value)) != 0)
                        {
                            decimal wk = 0;
                            // 入数より入力バラ数が多い
                            if (Util.ToDecimal(Util.ToString(dgvInputList[COL_IRISU, dgvCell.RowIndex].Value)) <= w)
                            {
                                wk = Util.ToDecimal(Util.ToString(dgvInputList[COL_IRISU, dgvCell.RowIndex].Value)) * Util.ToDecimal(Util.ToString(dgvInputList[COL_SURYO1, dgvCell.RowIndex].Value));
                                wk += w;
                                w = wk / Util.ToDecimal(Util.ToString(dgvInputList[COL_IRISU, dgvCell.RowIndex].Value));
                                // 再計算ケース数
                                dgvInputList[COL_SURYO1, dgvCell.RowIndex].Value = TaxUtil.CalcFraction(w, 0, 1);
                                // 再計算バラ数（総バラ数-ケース数×入数）
                                w = wk - (Util.ToDecimal(Util.ToString(dgvInputList[COL_IRISU, dgvCell.RowIndex].Value)) * Util.ToDecimal(Util.ToString(dgvInputList[COL_SURYO1, dgvCell.RowIndex].Value)));
                            }
                        }

                        // 変更時
                        if (Util.ToDecimal(Util.ToString(dgvInputList[COL_SURYO2, dgvCell.RowIndex].Value)) != w)
                        {
                            dgvInputList[COL_SURYO2, dgvCell.RowIndex].Value = Util.FormatNum(w.ToString(), this.CalcInfo.KETA_SURYO2);

                            // 金額と消費税計算
                            if (!IsCalc(dgvCell.RowIndex))
                            {
                                dgvInputList[COL_BAIKA_KINGAKU, dgvCell.RowIndex].Value = "";
                                dgvInputList[COL_SHOHIZEI, dgvCell.RowIndex].Value = "";
                            }
                            else
                            {
                                // 消費税計算
                                if (!CellKingkSet(dgvCell.RowIndex))
                                {
                                    Msg.Error("金額が大きすぎる為、複数明細へ分けて入力を行って下さい。");
                                    this.txtGridEdit.Focus();
                                    return;
                                }
                            }
                            this.DataGridSum();
                        }

                        // カーソル移動制御
                        if (this.GetDenpyoMeisaiKeyEnter(e.KeyCode, dgvCell))
                        {
                            dgvInputList.CurrentCell = dgvInputList[COL_URI_TANKA, dgvCell.RowIndex];
                        }
                        if (e.KeyCode == Keys.Right)
                        {
                            dgvInputList.CurrentCell = dgvInputList[COL_URI_TANKA, dgvCell.RowIndex];
                            this.txtDummy.Focus();
                            this.txtGridEdit.Focus();
                        }
                        break;
                    // 単価
                    case COL_URI_TANKA: //  8:
                        //if (!ValChk.IsDecNumWithinLength(txtGridEdit.Text, 12, 1, true))
                        if (!ValChk.IsDecNumWithinLength(txtGridEdit.Text, 12, 3, true))
                        {
                            this.txtGridEdit.Text = Regex.Replace(this.txtGridEdit.Text, "^[a-zA-Z]+$", "");
                            this.txtGridEdit.Focus();
                            return;
                        }
                        // マイナスは入力不可
                        if (Util.ToDecimal(txtGridEdit.Text) < 0)
                        {
                            this.txtGridEdit.Text = string.Empty;
                            this.txtGridEdit.Focus();
                            return;
                        }

                        // 小数部整理
                        inputData = Util.ToDecimal(Util.ToString(txtGridEdit.Text)) * 100;
                        inputData = TaxUtil.CalcFraction(inputData, 0, 1) / 100; // 切り捨て

                        // 変更時
                        if (Util.ToDecimal(Util.ToString(dgvInputList[COL_URI_TANKA, dgvCell.RowIndex].Value)) != inputData)
                        {
                            dgvInputList[COL_URI_TANKA, dgvCell.RowIndex].Value = Util.FormatNum(inputData, this.CalcInfo.KETA_URI_TANKA);

                            // 金額と消費税計算
                            if (!IsCalc(dgvCell.RowIndex))
                            {
                                dgvInputList[COL_BAIKA_KINGAKU, dgvCell.RowIndex].Value = "";
                                dgvInputList[COL_SHOHIZEI, dgvCell.RowIndex].Value = "";
                            }
                            else
                            {
                                // 消費税計算
                                if (!CellKingkSet(dgvCell.RowIndex))
                                {
                                    Msg.Error("金額が大きすぎる為、複数明細へ分けて入力を行って下さい。");
                                    this.txtGridEdit.Focus();
                                    return;
                                }
                            }
                            this.DataGridSum();
                        }

                        // カーソル移動制御
                        if (this.GetDenpyoMeisaiKeyEnter(e.KeyCode, dgvCell))
                        {
                            dgvInputList.CurrentCell = dgvInputList[COL_BAIKA_KINGAKU, dgvCell.RowIndex];
                        }
                        if (e.KeyCode == Keys.Right)
                        {
                            dgvInputList.CurrentCell = dgvInputList[COL_BAIKA_KINGAKU, dgvCell.RowIndex];
                            this.txtDummy.Focus();
                            this.txtGridEdit.Focus();
                        }
                        break;
                    // 金額
                    case COL_BAIKA_KINGAKU: //  9:
                        if (!ValChk.IsDecNumWithinLength(txtGridEdit.Text, 15, 0, true))
                        {
                            this.txtGridEdit.Text = Regex.Replace(this.txtGridEdit.Text, "^[a-zA-Z]+$", "");
                            this.txtGridEdit.Focus();
                            return;
                        }

                        bool chk = false;
                        if (Util.ToDecimal(Util.ToString(dgvInputList[COL_BAIKA_KINGAKU, dgvCell.RowIndex].Value)) != Util.ToDecimal(txtGridEdit.Text))
                            chk = true;
                        dgvInputList[COL_BAIKA_KINGAKU, dgvCell.RowIndex].Value = Util.FormatNum(txtGridEdit.Text);

                        // 金額と消費税計算
                        if (!ValChk.IsEmpty(dgvInputList[COL_BAIKA_KINGAKU, dgvCell.RowIndex].Value))
                        {
                            if (!ValChk.IsEmpty(dgvInputList[COL_ZEI_RITSU, dgvCell.RowIndex].Value) && Util.ToDecimal(this.dgvInputList[COL_KAZEI_KUBUN, dgvCell.RowIndex].Value).Equals(1))
                            {
                                // 消費税計算
                                if (chk)  CellTaxSet(dgvCell.RowIndex);
                            }
                            else
                            {
                                this.DataGridSum();
                                if (this.dgvInputList.RowCount > dgvCell.RowIndex + 1)
                                {
                                    if (this.GetDenpyoMeisaiKeyEnter(e.KeyCode, dgvCell))
                                    {
                                        // 明細転嫁時は消費税、その他は次行の商品へ
                                        if (Util.ToInt(this.CalcInfo.SHOHIZEI_TENKA_HOHO) == 1)
                                            dgvInputList.CurrentCell = dgvInputList[COL_SHOHIZEI, dgvCell.RowIndex]; // 消費税
                                        else
                                            dgvInputList.CurrentCell = dgvInputList[COL_SHOHIN_CD, dgvCell.RowIndex + 1];
                                    }
                                }
                                break;
                            }
                        }
                        else
                        {
                            dgvInputList[COL_SHOHIZEI, dgvCell.RowIndex].Value = "";
                        }
                        this.DataGridSum();

                        // カーソル移動制御
                        if (this.GetDenpyoMeisaiKeyEnter(e.KeyCode, dgvCell))
                        {
                            // 明細転嫁時は消費税、その他は次行の商品へ
                            if (Util.ToInt(this.CalcInfo.SHOHIZEI_TENKA_HOHO) == 1)
                                dgvInputList.CurrentCell = dgvInputList[COL_SHOHIZEI, dgvCell.RowIndex]; // 消費税
                            else
                                dgvInputList.CurrentCell = dgvInputList[COL_SHOHIN_CD, dgvCell.RowIndex + 1]; // 次の商品
                        }

                        if (e.KeyCode == Keys.Right)
                        {
                            dgvInputList.CurrentCell = dgvInputList[COL_SHOHIN_CD, dgvCell.RowIndex];
                            this.txtDummy.Focus();
                            this.txtGridEdit.Focus();
                        }


                        break;
                    // 消費税
                    case COL_SHOHIZEI: // 10:
                        if (!ValChk.IsDecNumWithinLength(txtGridEdit.Text, 15, 0, true))
                        {
                            this.txtGridEdit.Text = Regex.Replace(this.txtGridEdit.Text, "^[a-zA-Z]+$", "");
                            this.txtGridEdit.Focus();
                            return;
                        }

                        if (Util.ToInt(this.CalcInfo.SHOHIZEI_TENKA_HOHO) == 1)
                            dgvInputList[COL_SHOHIZEI, dgvCell.RowIndex].Value = Util.FormatNum(txtGridEdit.Text);

                        // カーソル移動制御
                        if (this.dgvInputList.RowCount > dgvCell.RowIndex + 1)
                        {
                            if (this.GetDenpyoMeisaiKeyEnter(e.KeyCode, dgvCell))
                            {
                                dgvInputList.CurrentCell = dgvInputList[COL_SHOHIN_CD, dgvCell.RowIndex + 1];
                            }
                        }

                        if (e.KeyCode == Keys.Right)
                        {
                            dgvInputList.CurrentCell = dgvInputList[COL_SHOHIN_CD, dgvCell.RowIndex];
                            this.txtDummy.Focus();
                            this.txtGridEdit.Focus();
                        }

                        this.DataGridSum();
                        break;
                }
            }
            else
            {
                // [←キー]が押された
                // if (e.KeyCode == Keys.Left && this.txtGridEdit.SelectionLength == 0 && this.txtGridEdit.SelectionStart == 0)
                if (e.KeyCode == Keys.Left)
                {
                    DataGridViewCell dgvCell = (DataGridViewCell)dgvInputList.CurrentCell;
                    switch (dgvCell.ColumnIndex)
                    {
                        case COL_SHOHIN_CD:
                            dgvInputList.CurrentCell = dgvInputList[COL_KUBUN, dgvCell.RowIndex];
                            break;
                        case COL_SURYO1:
                            // 在庫管理無しなら商品名、ありなら商品コード
                            if (Util.ToInt(this.dgvInputList[COL_ZAIKO_KANRI_KUBUN, dgvCell.RowIndex].Value) == 0)
                                dgvInputList.CurrentCell = dgvInputList[COL_SHOHIN_NM, dgvCell.RowIndex];
                            else
                                dgvInputList.CurrentCell = dgvInputList[COL_SHOHIN_CD, dgvCell.RowIndex];
                            break;
                        case COL_SURYO2: //  7:
                            // 入り数がある場合はケース、在庫管理無しなら商品名、ありなら商品コード
                            if (Util.ToInt(Util.ToString(this.dgvInputList[COL_IRISU, dgvCell.RowIndex].Value)) != 0)
                                dgvInputList.CurrentCell = dgvInputList[COL_SURYO1, dgvCell.RowIndex];
                            else
                            {
                                if (Util.ToInt(this.dgvInputList[COL_ZAIKO_KANRI_KUBUN, dgvCell.RowIndex].Value) == 0)
                                    dgvInputList.CurrentCell = dgvInputList[COL_SHOHIN_NM, dgvCell.RowIndex];
                                else
                                    dgvInputList.CurrentCell = dgvInputList[COL_SHOHIN_CD, dgvCell.RowIndex];
                            }
                            break;
                        case COL_URI_TANKA: //  8:
                            dgvInputList.CurrentCell = dgvInputList[COL_SURYO2, dgvCell.RowIndex];
                            break;
                        case COL_BAIKA_KINGAKU: //  9:
                            dgvInputList.CurrentCell = dgvInputList[COL_URI_TANKA, dgvCell.RowIndex];
                            break;
                        case COL_SHOHIZEI: // 10:
                            dgvInputList.CurrentCell = dgvInputList[COL_BAIKA_KINGAKU, dgvCell.RowIndex];
                            break;
                    }
                    this.txtDummy.Focus();
                    this.txtGridEdit.Focus();
                }
            }

            //// 商品在庫
            //this.SetZaiko();
        }

        /// <summary>
        /// フォーム閉じる前に発生
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void KBDE1021_FormClosing(object sender, FormClosingEventArgs e)
        {
            // データ入力済時に確認します
            if (this.IS_INPUT == 2)
            {
                string msg = "未更新データが存在します！　処理を終了しますか？";
                if (Msg.ConfYesNo(msg) == DialogResult.No)
                {
                    // 「いいえ」を押されたらフォームを閉じない
                    // コントロールの検証処理(Validata)を有効にする
                    base.AutoValidate = AutoValidate.EnablePreventFocusChange;
                    e.Cancel = true;
                    return;
                }
            }
        }

        /// <summary>
        /// データグリッド用のテキストにEnter時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGridEdit_Enter(object sender, EventArgs e)
        {
            BeginInvoke(new EventHandler(TextBoxSelectAll), sender, e);
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 年月日の月末入力チェック
        /// </summary>
        /// 
        private void CheckJp()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblGengo.Text, this.txtGengoYear.Text,
                this.txtMonth.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDay.Text) > lastDayInMonth)
            {
                this.txtDay.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetJp()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJp(Util.FixJpDate(this.lblGengo.Text, this.txtGengoYear.Text,
                this.txtMonth.Text, this.txtDay.Text, this.Dba));
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJp(string[] arrJpDate)
        {
            this.lblGengo.Text = arrJpDate[0];
            this.txtGengoYear.Text = arrJpDate[2];
            this.txtMonth.Text = arrJpDate[3];
            this.txtDay.Text = arrJpDate[4];

            DateTime d = Util.ConvAdDate(this.lblGengo.Text, this.txtGengoYear.Text,
                    this.txtMonth.Text, this.txtDay.Text, this.Dba);
            // 新規は無条件、修正時は日付が変わったら行う（日付で税率が変わる対応）
            if ((this.MODE_EDIT == 2 && this.CalcInfo.DENPYO_DATE != d.ToString("yyyy/MM/dd")) ||
                 this.MODE_EDIT == 1)
            {
                CellTaxRateSet();
                this.CalcInfo.DENPYO_DATE = d.ToString("yyyy/MM/dd");
            }
        }

        /// <summary>
        /// 仕入先CD設定情報
        /// </summary>
        private bool SetSiiresakiInfo()
        {
            // コードを元に名称を取得する
            // 取得された場合、名称をラベルに反映する
            if (!ValChk.IsEmpty(this.txtSiiresakiCd.Text))
            {
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
                dpc.SetParam("@TORIHIKISAKI_CD", SqlDbType.Decimal, 6, this.txtSiiresakiCd.Text);
                DataTable dtVI_HN_TORIHIKISAKI_JOHO = this.Dba.GetDataTableByConditionWithParams(
                    "TORIHIKISAKI_NM, TANKA_SHUTOKU_HOHO, KINGAKU_HASU_SHORI, SHOHIZEI_NYURYOKU_HOHO, SHOHIZEI_HASU_SHORI, SHOHIZEI_TENKA_HOHO, SHOHIZEI_TENKA_HOHO_NM, SHOHIZEI_NYURYOKU_HOHO_NM, SEIKYUSAKI_CD, SEIKYUSAKI_NM",
                    "VI_HN_TORIHIKISAKI_JOHO", "KAISHA_CD = @KAISHA_CD AND TORIHIKISAKI_CD = @TORIHIKISAKI_CD", dpc);

                if (dtVI_HN_TORIHIKISAKI_JOHO.Rows.Count == 0)
                {
                    //Msg.Error("存在しない船主CDです。開発者へご連絡ください。");
                    Msg.Error("入力に誤りがあります。");
                    return true;
                }

                this.lblShohizeiInputHoho2.Text = dtVI_HN_TORIHIKISAKI_JOHO.Rows[0]["SHOHIZEI_NYURYOKU_HOHO"].ToString() + ":" + dtVI_HN_TORIHIKISAKI_JOHO.Rows[0]["SHOHIZEI_NYURYOKU_HOHO_NM"].ToString();
                this.lblShohizeiTenka2.Text = dtVI_HN_TORIHIKISAKI_JOHO.Rows[0]["SHOHIZEI_TENKA_HOHO"].ToString() + ":" + dtVI_HN_TORIHIKISAKI_JOHO.Rows[0]["SHOHIZEI_TENKA_HOHO_NM"].ToString();

                if (this.CalcInfo.TOKUISAKI_CD != Util.ToInt(this.txtSiiresakiCd.Text))
                {
                    this.lblSiiresakiNm.Text = Util.ToString(dtVI_HN_TORIHIKISAKI_JOHO.Rows[0]["TORIHIKISAKI_NM"]);
                    this.CalcInfo.TANKA_SHUTOKU_HOHO = Util.ToDecimal(dtVI_HN_TORIHIKISAKI_JOHO.Rows[0]["TANKA_SHUTOKU_HOHO"]);
                    this.CalcInfo.KINGAKU_HASU_SHORI = Util.ToDecimal(dtVI_HN_TORIHIKISAKI_JOHO.Rows[0]["KINGAKU_HASU_SHORI"]);
                    this.CalcInfo.SHOHIZEI_NYURYOKU_HOHO = Util.ToDecimal(dtVI_HN_TORIHIKISAKI_JOHO.Rows[0]["SHOHIZEI_NYURYOKU_HOHO"]);
                    this.CalcInfo.SHOHIZEI_HASU_SHORI = Util.ToDecimal(dtVI_HN_TORIHIKISAKI_JOHO.Rows[0]["SHOHIZEI_HASU_SHORI"]);
                    this.CalcInfo.SHOHIZEI_TENKA_HOHO = Util.ToDecimal(dtVI_HN_TORIHIKISAKI_JOHO.Rows[0]["SHOHIZEI_TENKA_HOHO"]);

                    this.txtSiharaisakiCd.Text = Util.ToString(dtVI_HN_TORIHIKISAKI_JOHO.Rows[0]["SEIKYUSAKI_CD"]);
                    this.lblSiharaisakiNm.Text = Util.ToString(dtVI_HN_TORIHIKISAKI_JOHO.Rows[0]["SEIKYUSAKI_NM"]);

                    // 強制設定
                    if (this.SHIFT_CATEGORY_SLIP_TOTAL != 0 && (this.SHIFT_CATEGORY_SLIP_TOTAL >= 1 && this.SHIFT_CATEGORY_SLIP_TOTAL <= 2))
                    {
                        this.CalcInfo.SHOHIZEI_TENKA_HOHO = this.SHIFT_CATEGORY_SLIP_TOTAL;
                        SetTaxShiftCategory((int)this.CalcInfo.SHOHIZEI_TENKA_HOHO);
                    }
                    if (this.ROUND_CATEGORY_DOWN != 0 && (this.ROUND_CATEGORY_DOWN >= 1 && this.ROUND_CATEGORY_DOWN <= 3))
                        this.CalcInfo.SHOHIZEI_HASU_SHORI = this.ROUND_CATEGORY_DOWN;

                    // 変更前仕入先CDに入力値を設定する
                    this.CalcInfo.TOKUISAKI_CD = Util.ToInt(this.txtSiiresakiCd.Text);

                    this.DataGridSum();
                }
            }
            else
            {
                Msg.Error("入力に誤りがあります。");
                this.txtSiiresakiCd.SelectAll();
                this.txtSiiresakiCd.Focus();
                this.txtSiharaisakiCd.Text = "";
                this.lblSiharaisakiNm.Text = "";
                return true;
                //this.lblFunanushiNm.Text = "";
                //this.UriageInfo.Clear();
            }

            return false;
        }

        /// <summary>
        /// 新規モードの初期表示
        /// </summary>
        private void InitDispOnNew()
        {
            // 初期値、入力制御を実装

            // 登録に変更
            this.MODE_EDIT = 1;
            this.lblMode.Text = "【登録】";
            this.MEISAI_DENPYO_BANGO = 0;

            this.CalcInfo.Clear();
            this.CalcInfo.DENPYO_DATE = DateTime.Now.ToString("yyyy/MM/dd");

            // 伝票番号
            this.txtDenpyoNo.Text = "";
            this.txtDenpyoNo.Focus();

            // 水揚支所
            //this.txtMizuageShishoCd.Text = "1";　// ベースの支所が本所以外の場合は支所区分を触れない様にロックが必要
            //this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
            this.txtMizuageShishoCd.Text = this.UInfo.ShishoCd;
            this.lblMizuageShishoNm.Text = this.UInfo.ShishoNm;
            if (Util.ToInt(this.txtMizuageShishoCd.Text) == 0)
            {
                DataRow r = GetPersonInfo(this.UInfo.UserCd);
                if (r != null)
                {
                    this.UInfo.ShishoCd = r["SHISHO_CD"].ToString();
                    this.UInfo.ShishoNm = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.UInfo.ShishoCd, this.UInfo.ShishoCd);
                    this.txtMizuageShishoCd.Text = this.UInfo.ShishoCd;
                    this.lblMizuageShishoNm.Text = this.UInfo.ShishoNm;
                }
            }
            // 取り合えず入力、更新系は触れない様に
            txtMizuageShishoCd.Enabled = false;

            // 日付範囲の和暦設定
            if (ValChk.IsEmpty(this.txtGengoYear.Text) && ValChk.IsEmpty(this.txtMonth.Text) && ValChk.IsEmpty(this.txtDay.Text))
            {
                // 日付範囲の和暦設定
                string[] jpDate = Util.ConvJpDate(DateTime.Now, this.Dba);
                // 伝票日付
                this.lblGengo.Text = jpDate[0];
                this.txtGengoYear.Text = jpDate[2];
                this.txtMonth.Text = jpDate[3];
                this.txtDay.Text = jpDate[4];

                this.CalcInfo.DENPYO_DATE = DateTime.Now.ToString("yyyy/MM/dd");
            }

            // 取引区分
            if (ValChk.IsEmpty(this.txtTorihikiKubunCd.Text))
            {
                this.txtTorihikiKubunCd.Text = "11"; // 掛仕入
                this.lblTorihikiKubunNm.Text = this.Dba.GetName(this.UInfo, "TB_HN_F_TORIHIKI_KUBUN2", this.txtMizuageShishoCd.Text, this.txtTorihikiKubunCd.Text); ;
            }

            // 仕入先
            this.txtSiiresakiCd.Text = "";
            this.lblSiiresakiNm.Text = "";

            // 支払先
            this.txtSiharaisakiCd.Text = "";
            this.lblSiharaisakiNm.Text = "";

            // 担当者
            //this.txtTantoshaCd.Text = this.UInfo.UserCd;
            //this.lblTantoshaNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_TANTOSHA", this.txtTantoshaCd.Text);
            this.txtTantoshaCd.Text = "0";
            this.lblTantoshaNm.Text = "";

            // 削除ボタン使用不可
            this.btnF3.Enabled = false;

            //奇数行を淡い水色にする
            this.dgvInputList.AlternatingRowsDefaultCellStyle.BackColor = ColorTranslator.FromHtml("#BBFFFF");
            this.dgvInputList.DefaultCellStyle.SelectionBackColor = Color.Transparent;

            // ユーザーによるソートを禁止させる
            foreach (DataGridViewColumn c in this.dgvInputList.Columns)
                c.SortMode = DataGridViewColumnSortMode.NotSortable;

            // フォントを設定する
            this.dgvInputList.ColumnHeadersDefaultCellStyle.Font = this.txtGridEdit.Font;
            this.dgvInputList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.dgvInputList.DefaultCellStyle.Font = this.txtGridEdit.Font;
            this.dgvInputList.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvInputList.Columns[COL_SHOHIN_NM].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            // ユーザ操作による行追加を無効(禁止)
            this.dgvInputList.AllowUserToAddRows = false;
            // 入力モード設定
            this.dgvInputList.EditMode = DataGridViewEditMode.EditProgrammatically;

            // 行追加(1行目)
            this.dgvInputList.Rows.Clear();
            this.dgvInputList.RowCount = 1;
            //this.dgvInputList.Rows.Add();
            this.dgvInputList[COL_GYO_NO, 0].Value = "1";

            this.dgvInputList.CurrentCell = this.dgvInputList[COL_GYO_NO, 0];
            this.dgvInputList.CurrentCell = null;

            this.lblTaxInfo.Text = string.Empty;
            // 消費税転嫁方法、消費税入力方法をクリア
            this.lblShohizeiInputHoho2.Text = "";
            this.lblShohizeiTenka2.Text = "";

            // 小計
            this.txtSyokei.Text = "0";
            // 消費税（外）
            this.txtSyohiZei.Text = "0";
            // 合計額
            this.txtTotalGaku.Text = "0";

            //foreach (DataGridViewColumn c in this.dgvInputList.Columns)
            //    System.Diagnostics.Debug.Print("Idx:" + c.Index.ToString() + " Name:" + c.Name);

            // DataGridViewの編集用テキストコントロールを非表示
            this.txtGridEdit.Visible = false;

            #region 在庫数表示
            this._itemStock = new Dictionary<string, decimal>();
            this._torihikiKubun2 = 1; // 初期値
            #endregion

            this.IS_INPUT = 1;

            this._lastUpdCheck = false;   // 更新前確認
        }

    /// <summary>
    /// 修正モードの初期表示
    /// </summary>
    private void InitDispOnEdit(DataTable dtDENPYO)
        {
            // 初期値、入力制御を実装
            DbParamCollection dpc;

            // 修正に変更
            this.MODE_EDIT = 2;
            this.lblMode.Text = "【修正】";

            this.MEISAI_DENPYO_BANGO = Util.ToDecimal(dtDENPYO.Rows[0]["DENPYO_BANGO"]);
            this.CalcInfo.DENPYO_DATE = Util.ToDate(dtDENPYO.Rows[0]["DENPYO_DATE"]).ToString("yyyy/MM/dd");

            // 日付範囲の和暦設定
            DateTime DENPYO_DATE = DateTime.Parse(dtDENPYO.Rows[0]["DENPYO_DATE"].ToString());
            // 日付範囲の和暦設定
            string[] jpDate = Util.ConvJpDate(DENPYO_DATE, this.Dba);
            // 元号と年月日
            this.lblGengo.Text = jpDate[0];
            this.txtGengoYear.Text = jpDate[2];
            this.txtMonth.Text = jpDate[3];
            this.txtDay.Text = jpDate[4];

            // 取引区分
            this.txtTorihikiKubunCd.Text = dtDENPYO.Rows[0]["TORIHIKI_KUBUN1"].ToString() + dtDENPYO.Rows[0]["TORIHIKI_KUBUN2"].ToString();
            this.lblTorihikiKubunNm.Text = this.Dba.GetName(this.UInfo, "TB_HN_F_TORIHIKI_KUBUN2", this.txtMizuageShishoCd.Text, this.txtTorihikiKubunCd.Text);

            // 仕入先
            this.txtSiiresakiCd.Text = dtDENPYO.Rows[0]["TOKUISAKI_CD"].ToString();
            this.SetSiiresakiInfo();
            this.lblSiiresakiNm.Text = dtDENPYO.Rows[0]["TOKUISAKI_NM"].ToString();

            // 支払先
            this.txtSiharaisakiCd.Text = dtDENPYO.Rows[0]["SEIKYUSAKI_CD"].ToString();
            //this.SetSiiresakiInfo();
            this.lblSiharaisakiNm.Text = this.Dba.GetName(this.UInfo, "VI_HN_SHIIRESK", this.txtMizuageShishoCd.Text, this.txtSiharaisakiCd.Text); ;

            // 水揚支所
            this.txtMizuageShishoCd.Text = dtDENPYO.Rows[0]["SHISHO_CD"].ToString();
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);

            // 担当者
            this.txtTantoshaCd.Text = dtDENPYO.Rows[0]["TANTOSHA_CD"].ToString();
            this.lblTantoshaNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_TANTOSHA", this.txtMizuageShishoCd.Text, this.txtTantoshaCd.Text);
            SetBumonInfo(Util.ToInt(this.txtTantoshaCd.Text));

            // 削除ボタン使用可能
            this.btnF3.Enabled = true;
            
            // 小計
            this.txtSyokei.Text = Util.FormatNum(dtDENPYO.Rows[0]["URIAGE_KINGAKU"]);
            // 消費税（外）
            this.txtSyohiZei.Text = Util.FormatNum(dtDENPYO.Rows[0]["SHOHIZEIGAKU"]);
            // 合計額
            this.txtTotalGaku.Text = Util.FormatNum(dtDENPYO.Rows[0]["NYUKIN_GOKEIGAKU"]);

            // DataGridViewの編集用テキストコントロールを非表示
            this.txtGridEdit.Visible = false;

            #region 在庫数表示
            this._itemStock = new Dictionary<string, decimal>();
            this._torihikiKubun2 = Util.ToInt(dtDENPYO.Rows[0]["TORIHIKI_KUBUN2"].ToString()); // 取引区分２（在庫表示用）
            #endregion

            // グリッドをクリア
            this.dgvInputList.Rows.Clear();
            // 伝票明細取得してグリッドに設定
            // Han.TB_取引明細(TB_HN_TORIHIKI_MEISAI)
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToInt(Util.ToString(txtMizuageShishoCd.Text)));
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            dpc.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 4, 2);
            dpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 8, Util.ToDecimal(dtDENPYO.Rows[0]["DENPYO_BANGO"]));
            DataTable dtTB_HN_TORIHIKI_MEISAI = this.Dba.GetDataTableByConditionWithParams(
                "M.GYO_BANGO " +
                ",H.NM AS KUBUN_NM " +
                ",M.SHOHIN_CD " +
                ",M.SHOHIN_NM " +
                ",M.IRISU " +
                ",M.TANI " +
                ",M.SURYO1 " +
                ",M.SURYO2 " +
                ",M.URI_TANKA " +
                ",M.BAIKA_KINGAKU " +
                ",M.SHOHIZEI " +
                ",M.SHIWAKE_CD " +
                ",M.BUMON_CD " +
                ",M.ZEI_KUBUN " +
                ",M.JIGYO_KUBUN " +
                ",Z.KAZEI_KUBUN " +
                ",M.ZEI_RITSU " +
                ",M.SHOHIZEI_NYURYOKU_HOHO " +
                ",M.ZAIKO_KANRI_KUBUN " +
                ",M.KUBUN ",
                //"TB_HN_TORIHIKI_MEISAI AS M " +
                "VI_HN_TORIHIKI_MEISAI AS M " +
                "LEFT JOIN TB_ZM_F_ZEI_KUBUN AS Z " +
                "    ON M.ZEI_KUBUN = Z.ZEI_KUBUN " +
                "LEFT JOIN VI_HN_HANBAI_KUBUN_NM AS H " +
                "    ON M.KAISHA_CD = H.KAISHA_CD " +
                "    AND H.SHUBETSU_CD = 2 " +
                "    AND H.KUBUN_SHUBETSU = 2 " +
                "    AND H.KUBUN = M.KUBUN",
                "M.KAISHA_CD = @KAISHA_CD AND M.SHISHO_CD = @SHISHO_CD AND M.KAIKEI_NENDO = @KAIKEI_NENDO AND M.DENPYO_KUBUN = @DENPYO_KUBUN AND M.DENPYO_BANGO = @DENPYO_BANGO",
                dpc);
            if (dtTB_HN_TORIHIKI_MEISAI.Rows.Count > 0)
            {
                this.dgvInputList.RowCount = dtTB_HN_TORIHIKI_MEISAI.Rows.Count + 1;
                int i = 0;
                while (dtTB_HN_TORIHIKI_MEISAI.Rows.Count > i)
                {
                    this.dgvInputList[COL_GYO_NO, i].Value = Util.ToString(dtTB_HN_TORIHIKI_MEISAI.Rows[i]["GYO_BANGO"]);
                    this.dgvInputList[COL_KUBUN, i].Value = (Util.ToDecimal(dtTB_HN_TORIHIKI_MEISAI.Rows[i]["KUBUN"]) == 0 ? "" : Util.ToString(dtTB_HN_TORIHIKI_MEISAI.Rows[i]["KUBUN"]));
                    this.dgvInputList[COL_SHOHIN_CD, i].Value = Util.ToString(dtTB_HN_TORIHIKI_MEISAI.Rows[i]["SHOHIN_CD"]);
                    this.dgvInputList[COL_SHOHIN_NM, i].Value = Util.ToString(dtTB_HN_TORIHIKI_MEISAI.Rows[i]["SHOHIN_NM"]);
                    this.dgvInputList[COL_IRISU, i].Value = (Util.ToDecimal(dtTB_HN_TORIHIKI_MEISAI.Rows[i]["IRISU"]) == 0 ? "" : dtTB_HN_TORIHIKI_MEISAI.Rows[i]["IRISU"]);
                    this.dgvInputList[COL_TANI, i].Value = Util.ToString(dtTB_HN_TORIHIKI_MEISAI.Rows[i]["TANI"]);
                    this.dgvInputList[COL_SURYO1, i].Value = Util.FormatNum(dtTB_HN_TORIHIKI_MEISAI.Rows[i]["SURYO1"]);
                    this.dgvInputList[COL_SURYO2, i].Value = Util.FormatNum(dtTB_HN_TORIHIKI_MEISAI.Rows[i]["SURYO2"], this.CalcInfo.KETA_SURYO2);
                    this.dgvInputList[COL_URI_TANKA, i].Value = Util.FormatNum(dtTB_HN_TORIHIKI_MEISAI.Rows[i]["URI_TANKA"], this.CalcInfo.KETA_URI_TANKA);
                    this.dgvInputList[COL_BAIKA_KINGAKU, i].Value = Util.FormatNum(dtTB_HN_TORIHIKI_MEISAI.Rows[i]["BAIKA_KINGAKU"]);
                    this.dgvInputList[COL_SHOHIZEI, i].Value = Util.FormatNum(dtTB_HN_TORIHIKI_MEISAI.Rows[i]["SHOHIZEI"]);
                    this.dgvInputList[COL_SHIWAKE_CD, i].Value = Util.ToString(dtTB_HN_TORIHIKI_MEISAI.Rows[i]["SHIWAKE_CD"]);
                    this.dgvInputList[COL_BUMON_CD, i].Value = Util.ToString(dtTB_HN_TORIHIKI_MEISAI.Rows[i]["BUMON_CD"]);
                    this.dgvInputList[COL_ZEI_KUBUN, i].Value = Util.ToString(dtTB_HN_TORIHIKI_MEISAI.Rows[i]["ZEI_KUBUN"]);
                    this.dgvInputList[COL_JIGYO_KUBUN, i].Value = Util.ToString(dtTB_HN_TORIHIKI_MEISAI.Rows[i]["JIGYO_KUBUN"]);
                    this.dgvInputList[COL_ZEI_RITSU, i].Value = Util.FormatNum(Util.ToDecimal(Util.ToString(dtTB_HN_TORIHIKI_MEISAI.Rows[i]["ZEI_RITSU"])), 1);
                    //this.dgvInputList[COL_GEN_TANKA, i].Value = Util.ToString(dtTB_HN_TORIHIKI_MEISAI.Rows[i]["GEN_TANKA"]);
                    this.dgvInputList[COL_KAZEI_KUBUN, i].Value = Util.ToString(dtTB_HN_TORIHIKI_MEISAI.Rows[i]["KAZEI_KUBUN"]);

                    // 消費税が発生しない場合はクリア
                    if (Util.ToInt(this.CalcInfo.SHOHIZEI_TENKA_HOHO) != 1)
                    {
                        this.dgvInputList[COL_SHOHIZEI, i].Value = 0;
                    }
                    if (Util.ToInt(this.dgvInputList[COL_ZEI_KUBUN, i].Value) == 0)
                    {
                        this.dgvInputList[COL_SHOHIZEI, i].Value = 0;
                    }
                    // 旧システムは売上のみの為仕入には組み込まない
                    //// 商品の消費税区分は伝票単位でのグループ入力のみ対応（混在は不可）
                    //if (i == 0)
                    //{
                    //    if (this.SiireInfo.SHOHIZEI_NYURYOKU_HOHO != Util.ToDecimal(dtTB_HN_TORIHIKI_MEISAI.Rows[i]["SHOHIZEI_NYURYOKU_HOHO"]))
                    //    {
                    //        this.SiireInfo.SHOHIZEI_NYURYOKU_HOHO = Util.ToDecimal(dtTB_HN_TORIHIKI_MEISAI.Rows[i]["SHOHIZEI_NYURYOKU_HOHO"]);

                    //        this.SetTaxInputCategory(Util.ToInt(Util.ToString(this.UriageInfo.SHOHIZEI_NYURYOKU_HOHO)));
                    //    }
                    //    // 明細でのチェック処理用に税込みの場合は調整 -> 値の方は既存のままか調整が入る場合は調整
                    //    this.SiireInfo.SHOHIN_SHOHIZEI_KUBUN = (Util.ToDecimal(dtTB_HN_TORIHIKI_MEISAI.Rows[i]["SHOHIZEI_NYURYOKU_HOHO"]) == 3 ? "0" : "");
                    //}
                    // 在庫管理区分
                    this.dgvInputList[COL_ZAIKO_KANRI_KUBUN, i].Value = Util.ToString(dtTB_HN_TORIHIKI_MEISAI.Rows[i]["ZAIKO_KANRI_KUBUN"]);
                    // 非課税の場合は税情報クリア
                    //if (Util.ToInt(this.dgvInputList[COL_KAZEI_KUBUN, i].Value) == 0)
                    if (Util.ToInt(this.dgvInputList[COL_KAZEI_KUBUN, i].Value) != 1)
                    {
                        this.dgvInputList[COL_SHOHIZEI, i].Value = 0;
                        this.dgvInputList[COL_ZEI_RITSU, i].Value = string.Empty;
                    }

                    #region 在庫数表示
                    if (Util.ToString(this.dgvInputList[COL_ZAIKO_KANRI_KUBUN, i].Value) == "1")
                    {
                        string code = Util.ToString(this.dgvInputList[COL_SHOHIN_CD, i].Value);
                        decimal cus = Util.ToDecimal(Util.ToString(dgvInputList[COL_SURYO1, i].Value)) * Util.ToDecimal(Util.ToString(dgvInputList[COL_IRISU, i].Value));
                        decimal num = cus + Util.ToDecimal(Util.ToString(dgvInputList[COL_SURYO2, i].Value));
                        // 数量の保持
                        if (!this._itemStock.ContainsKey(code))
                        {
                            this._itemStock.Add(code, num);
                        }
                        else
                        {
                            this._itemStock[code] += num;
                        }
                    }
                    #endregion

                    i++;
                }
                // 行追加(最後の行目)
                this.dgvInputList[COL_GYO_NO, this.dgvInputList.Rows.Count - 1].Value = this.dgvInputList.Rows.Count;

                SetTaxInfo();
            }
        }

        /// <summary>
        /// データグリッドを小計、消費税（外）、合計額に集計
        /// </summary>
        private void DataGridSum()
        {
            decimal[] total = { 0, 0, 0 };
            decimal work = 0;
            bool setFlg = false;
            int taxIdx = 0;
            List<MeisaiTbl> taxTbl = new List<MeisaiTbl>();
            int i = 0;
            while (this.dgvInputList.RowCount > i)
            {
                // 税区分が未設定のデータが出る場合に注意
                if (!ValChk.IsEmpty(Util.ToString(this.dgvInputList[COL_ZEI_KUBUN, i].Value)))
                {
                    if (Util.ToInt(Util.ToString(dgvInputList[COL_ZEI_KUBUN, i].Value)) == 0)
                    {
                        // 非課税額
                        total[0] += Util.ToDecimal(Util.ToString(dgvInputList[COL_BAIKA_KINGAKU, i].Value));
                    }
                    else
                    {
                        // 課税額
                        switch (Util.ToInt(this.CalcInfo.SHOHIZEI_TENKA_HOHO))
                        {
                            case 1: // 明細転嫁
                            case 3: // 請求転嫁
                                switch (Util.ToInt(this.CalcInfo.SHOHIZEI_NYURYOKU_HOHO))
                                {
                                    case 2: // 税抜き
                                        total[1] += Util.ToDecimal(Util.ToString(dgvInputList[COL_BAIKA_KINGAKU, i].Value));
                                        total[2] += Util.ToDecimal(Util.ToString(dgvInputList[COL_SHOHIZEI, i].Value));
                                        break;

                                    case 3: // 税込み
                                        total[1] += Util.ToDecimal(Util.ToString(dgvInputList[COL_BAIKA_KINGAKU, i].Value)) - Util.ToDecimal(Util.ToString(dgvInputList[COL_SHOHIZEI, i].Value));
                                        total[2] += Util.ToDecimal(Util.ToString(dgvInputList[COL_SHOHIZEI, i].Value));
                                        break;
                                    default:
                                        total[1] += Util.ToDecimal(Util.ToString(dgvInputList[COL_BAIKA_KINGAKU, i].Value));
                                        total[2] += Util.ToDecimal(Util.ToString(dgvInputList[COL_SHOHIZEI, i].Value));
                                        break;
                                }
                                break;
                            case 2: // 伝票転嫁
                                setFlg = false;
                                if (taxIdx != 0)
                                {
                                    for (int j = 0; j < taxTbl.Count; j++)
                                    {
                                        if (taxTbl[j].ZeiRt == Util.ToDecimal(Util.ToString(dgvInputList[COL_ZEI_RITSU, i].Value)))
                                        {
                                            taxTbl[j].Kingk += Util.ToDecimal(Util.ToString(dgvInputList[COL_BAIKA_KINGAKU, i].Value));
                                            setFlg = true;
                                            break;
                                        }
                                    }
                                }
                                if (!setFlg)
                                {
                                    MeisaiTbl m = new MeisaiTbl();
                                    m.ZeiRt = Util.ToDecimal(Util.ToString(dgvInputList[COL_ZEI_RITSU, i].Value));
                                    m.Kingk = Util.ToDecimal(Util.ToString(dgvInputList[COL_BAIKA_KINGAKU, i].Value));
                                    taxTbl.Add(m);
                                    taxIdx += 1;
                                }
                                break;
                        }
                    }
                }
                i++;
            }
            // 伝票転嫁消費税確定
            if (Util.ToInt(this.CalcInfo.SHOHIZEI_TENKA_HOHO) == 2 && taxIdx > 0)
            {
                for (int j = 0; j < taxTbl.Count; j++)
                {
                    total[1] += taxTbl[j].Kingk;
                    if (taxTbl[j].ZeiRt != 0 && taxTbl[j].Kingk != 0)
                    {
                        switch (Util.ToInt(this.CalcInfo.SHOHIZEI_NYURYOKU_HOHO))
                        {
                            case 2: // 税抜き
                                work = (taxTbl[j].Kingk * taxTbl[j].ZeiRt) / 100;
                                total[2] += TaxUtil.CalcFraction(work, 0, Util.ToInt(CalcInfo.SHOHIZEI_HASU_SHORI));
                                break;

                            case 3: // 税込み
                                work = (taxTbl[j].Kingk / (100 + taxTbl[j].ZeiRt)) * taxTbl[j].ZeiRt;
                                total[1] = total[1] - TaxUtil.CalcFraction(work, 0, Util.ToInt(CalcInfo.SHOHIZEI_HASU_SHORI));
                                total[2] += TaxUtil.CalcFraction(work, 0, Util.ToInt(CalcInfo.SHOHIZEI_HASU_SHORI));
                                break;
                        }
                    }
                }
            }

            this.txtSyokei.Text = Util.FormatNum(total[0] + total[1]);
            this.txtSyohiZei.Text = Util.FormatNum(total[2]);
            this.txtTotalGaku.Text = Util.FormatNum(total[0] + total[1] + total[2]);

            SetTaxInfo();
        }

        #region 在庫数表示

        /// <summary>
        /// 表示商品の在庫数表示
        /// </summary>
        private void SetZaiko()
        {
            if (this._zaikoDisplay != 0)
            {
                try
                {
                    this.zaikoClear();
                    // 在庫数表示
                    if (this.dgvInputList.CurrentCell != null)
                    {
                        DbParamCollection dpc;
                        DataGridViewCell dgvCell = (DataGridViewCell)this.dgvInputList.CurrentCell;
                        // 商品が設定され且つ在庫管理対象
                        if (this.dgvInputList[COL_SHOHIN_CD, dgvCell.RowIndex].Value != null &&
                            Util.ToString(this.dgvInputList[COL_ZAIKO_KANRI_KUBUN, dgvCell.RowIndex].Value) == "1")
                        {
                            string hinCode = Util.ToString(this.dgvInputList[COL_SHOHIN_CD, dgvCell.RowIndex].Value);
                            dpc = new DbParamCollection();
                            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
                            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToInt(Util.ToString(txtMizuageShishoCd.Text)));
                            dpc.SetParam("@SHOHIN_CD", SqlDbType.Decimal, 15, Util.ToDecimal(hinCode));
                            DataTable dt = this.Dba.GetDataTableByConditionWithParams(
                                "*",
                                "VI_HN_SHOHIN_ZAIKO",
                                "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND SHOHIN_CD = @SHOHIN_CD",
                                dpc);
                            if (dt.Rows.Count == 0)
                            {
                                if (Util.ToDecimal(this.dgvInputList[COL_SHOHIN_CD, dgvCell.RowIndex].Value) != 0)
                                {
                                    this.lblShohinNm.Text = Util.ToString(this.dgvInputList[COL_SHOHIN_NM, dgvCell.RowIndex].Value);
                                    this.lblZaikoSu.Text = "";
                                }
                            }
                            else
                            {
                                if (Util.ToDecimal(this.dgvInputList[COL_SHOHIN_CD, dgvCell.RowIndex].Value) != 0)
                                {
                                    // 画面の対象商品の合計を取得
                                    decimal inData = getSuryo(dgvCell.RowIndex, hinCode);
                                    this.lblShohinNm.Text = "在庫数：" + Util.ToString(this.dgvInputList[COL_SHOHIN_NM, dgvCell.RowIndex].Value);

                                    // 在庫テーブルの合計を取得
                                    decimal cus = Util.ToDecimal(Util.ToString(dt.Rows[0]["IRISU"])) * Util.ToDecimal(Util.ToString(dt.Rows[0]["SURYO1"]));
                                    decimal num = cus + Util.ToDecimal(Util.ToString(dt.Rows[0]["SURYO2"]));

                                    // 伝票訂正時は元数の差し引き
                                    if (this._itemStock.ContainsKey(hinCode))
                                    {
                                        if (this._torihikiKubun2 == 2)
                                            num += this._itemStock[hinCode];
                                        else
                                            num -= this._itemStock[hinCode];
                                    }

                                    // 在庫数から入力数の差し引き
                                    decimal target = 0m;
                                    if (this.txtTorihikiKubunCd.Text.EndsWith("2"))
                                        target = num - inData;
                                    else
                                        target = num + inData;

                                    //this.lblZaikoSu.Text = (target == 0 ? "" : Util.FormatNum(target, 2));
                                    this.lblZaikoSu.Text = Util.FormatNum(target, 2);

                                    if (target < 0)
                                    {
                                        this.lblZaikoSu.ForeColor = Color.Red;
                                    }
                                    else
                                    {
                                        this.lblZaikoSu.ForeColor = Color.Black;
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception) { }
            }
        }

        /// <summary>
        /// 明細表示数量の抽出
        /// </summary>
        /// <param name="Row"></param>
        /// <param name="hinCode"></param>
        /// <returns></returns>
        private decimal getSuryo(int Row, string hinCode)
        {
            decimal cus = 0m;
            decimal num = 0m;
            int i = 0;
            try
            {
                while (this.dgvInputList.RowCount > i)
                {
                    if (Util.ToString(this.dgvInputList[COL_SHOHIN_CD, i].Value) == hinCode)
                    {
                        cus = Util.ToDecimal(Util.ToString(dgvInputList[COL_SURYO1, i].Value)) * Util.ToDecimal(Util.ToString(dgvInputList[COL_IRISU, i].Value));
                        num += cus + Util.ToDecimal(Util.ToString(dgvInputList[COL_SURYO2, i].Value));
                    }
                    i++;
                }
            }
            catch (Exception) { }

            return num;
        }

        /// <summary>
        /// 在庫数表示クリア
        /// </summary>
        private void zaikoClear()
        {
            if (this._zaikoDisplay != 0)
            {
                this.lblShohinNm.Text = "";
                this.lblZaikoSu.Text = "";
            }
        }
        #endregion

        /// <summary>
        /// 入力チェック処理
        /// </summary>
        private bool ValidateAll()
        {
            // 明細の選択を消す
            if (this.dgvInputList.CurrentCell != null)
                this.dgvInputList.CurrentCell.Selected = false;

            DbParamCollection dpc;
            DataTable dtCheck;

            // 伝票の入力チェック
            if (!this.isValidDenpyoNo())
            {
                this.txtDenpyoNo.SelectAll();
                this.txtDenpyoNo.Focus();
                return false;
            }

            // 年の入力チェック
            if (!IsValid.IsYear(this.txtGengoYear.Text, this.txtGengoYear.MaxLength))
            {
                this.txtGengoYear.Focus();
                this.txtGengoYear.SelectAll();
                return false;
            }

            // 月の入力チェック
            if (!IsValid.IsMonth(this.txtMonth.Text, this.txtMonth.MaxLength))
            {
                this.txtMonth.Focus();
                this.txtMonth.SelectAll();
                return false;
            }

            // 日の入力チェック
            if (!IsValid.IsDay(this.txtDay.Text, this.txtDay.MaxLength))
            {
                this.txtDay.Focus();
                this.txtDay.SelectAll();
                return false;
            }
            // 日付範囲月末入力チェック処理
            CheckJp();
            // 日付範囲正しい和暦への変換処理
            SetJp();

            // 伝票日付が会計期間内かチェック
            //DateTime DENPYO_DATE = Util.ConvAdDate(this.lblGengo.Text, this.txtGengoYear.Text,
            //                                                   this.txtMonth.Text, this.txtDay.Text, this.Dba);
            //DataRow settings = this.Dba.GetKaikeiSettingsByDate(this.UInfo.KaishaCd, DENPYO_DATE).Rows[0];
            //dpc = new DbParamCollection();
            //dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
            //dpc.SetParam("@KESSANKI", SqlDbType.Decimal, 6, this.UInfo.KessanKi);
            //dpc.SetParam("@DENPYO_DATE", SqlDbType.DateTime, DENPYO_DATE);
            //dtCheck = this.Dba.GetDataTableByConditionWithParams(
            //    "*",
            //    "VI_ZM_KAISHA_JOHO",
            //    "KAISHA_CD = @KAISHA_CD AND KESSANKI = @KESSANKI AND (@DENPYO_DATE BETWEEN KAIKEI_KIKAN_KAISHIBI AND KAIKEI_KIKAN_SHURYOBI)",
            //    dpc);
            //if (dtCheck.Rows.Count == 0)
            //{
            //    Msg.Error("伝票日付が会計期間外になってます。");
            //    this.txtGengoYear.Focus();
            //    this.txtGengoYear.SelectAll();
            //    return false;
            //}

            // 水揚支所の入力チェック
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                this.txtMizuageShishoCd.Focus();
                this.txtMizuageShishoCd.SelectAll();
                return false;
            }

            // 取引区分チェック
            if (!this.isValidTorihikiKubunCd())
            {
                this.txtTorihikiKubunCd.SelectAll();
                this.txtTorihikiKubunCd.Focus();
                return false;
            }

            // 仕入先チェック
            if (!this.isValidSiiresakiCd())
            {
                this.txtSiiresakiCd.SelectAll();
                this.txtSiiresakiCd.Focus();
                return false;
            }

            // 担当者チェック
            if (!this.isValidTantoshaCd())
            {
                this.txtTantoshaCd.SelectAll();
                this.txtTantoshaCd.Focus();
                return false;
            }

            // 明細入力チェック
            int rows = this.dgvInputList.Rows.Count;
            // 明細行がない場合は仕入先CD入力待ちにする
            if (rows == 0)
            {
                Msg.Notice("明細が入力されていません。");
                this.txtSiiresakiCd.Focus();
                this.txtSiiresakiCd.SelectAll();
                return false;
            }

            // 入力金額チェック
            if (!ValChk.IsDecNumWithinLength(this.txtSyokei.Text, 9, 0, true))
            {
                Msg.Error("金額が大きすぎる為、伝票を分けて入力を行って下さい。");
                this.txtGridEdit.Focus();
                return false;
            }

            // 明細チェック
            int i = 0;
            while (this.dgvInputList.RowCount > i)
            {
                // 区(販売区分)
                if (!ValChk.IsEmpty(this.dgvInputList[COL_KUBUN, i].Value))
                {
                    // Han.VI_販売区分名称(VI_HN_HANBAI_KUBUN_NM)
                    dpc = new DbParamCollection();
                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
                    dpc.SetParam("@KUBUN", SqlDbType.Decimal, 6, Util.ToDecimal(this.dgvInputList[COL_KUBUN, i].Value));
                    dtCheck = this.Dba.GetDataTableByConditionWithParams(
                        "*",
                        "VI_HN_HANBAI_KUBUN_NM",
                        "KAISHA_CD = @KAISHA_CD AND SHUBETSU_CD = 2 AND KUBUN_SHUBETSU = 2 AND KUBUN = @KUBUN",
                        dpc);
                    if (dtCheck.Rows.Count == 0)
                    {
                        if (this.dgvInputList.RowCount > i + 1)
                        {
                            Msg.Error("入力に誤りがあります。");
                            this.dgvInputList.CurrentCell = this.dgvInputList[COL_KUBUN, i];
                            this.txtGridEdit.SelectAll();
                            return false;
                        }
                    }
                }

                // 商品コード
                if (ValChk.IsEmpty(this.dgvInputList[COL_SHOHIN_CD, i].Value))
                {
                    if (this.dgvInputList.RowCount > i + 1)
                    {
                        Msg.Error("入力に誤りがあります。");
                        this.dgvInputList.CurrentCell = this.dgvInputList[COL_SHOHIN_CD, i];
                        this.txtGridEdit.SelectAll();
                        return false;
                    }
                }
                else
                {
                    // Han.VI_商品(VI_HN_SHOHIN)
                    dpc = new DbParamCollection();
                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
                    dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToInt(Util.ToString(txtMizuageShishoCd.Text)));
                    dpc.SetParam("@SHOHIN_CD", SqlDbType.Decimal, 15, Util.ToDecimal(this.dgvInputList[COL_SHOHIN_CD, i].Value));
                    dtCheck = this.Dba.GetDataTableByConditionWithParams(
                        "*",
                        "VI_HN_SHOHIN",
                        "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND SHOHIN_KUBUN5 <> 1 AND SHOHIN_CD = @SHOHIN_CD",
                        dpc);
                    if (dtCheck.Rows.Count == 0)
                    {
                        if (this.dgvInputList.RowCount > i + 1)
                        {
                            Msg.Error("入力に誤りがあります。");
                            this.dgvInputList.CurrentCell = this.dgvInputList[COL_SHOHIN_CD, i];
                            this.txtGridEdit.SelectAll();
                            return false;
                        }

                        // ZAM01025.VI_部門(VI_ZM_BUMON)
                        dpc = new DbParamCollection();
                        dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
                        dpc.SetParam("@BUMON_CD", SqlDbType.Decimal, 6, Util.ToDecimal(this.dgvInputList[COL_BUMON_CD, i].Value));
                        dtCheck = this.Dba.GetDataTableByConditionWithParams(
                            "*",
                            "VI_ZM_BUMON",
                            "KAISHA_CD = @KAISHA_CD AND BUMON_CD = @BUMON_CD",
                            dpc);
                        if (dtCheck.Rows.Count == 0)
                        {
                            if (this.dgvInputList.RowCount > i + 1)
                            {
                                Msg.Error("入力に誤りがあります。");
                                this.dgvInputList.CurrentCell = this.dgvInputList[COL_SHOHIN_CD, i];
                                this.txtGridEdit.SelectAll();
                                return false;
                            }
                        }
                    }
                }

                i++;
            }

            // 合計額チェック
            if (Util.ToDecimal(this.txtTotalGaku.Text) == 0)
            {
                //Msg.Error("精算できません。支払い金額が0です。");
                Msg.Error("精算できません。金額が0です。");
                this.txtGridEdit.Focus();
                this.txtGridEdit.SelectAll();
                return false;
            }

            return true;
        }

        /// <summary>
        /// 登録処理
        /// </summary>
        private void UpdateData()
        {
            DbParamCollection dpc;
            DataTable dtCheck;
            DbParamCollection updParam;
            DbParamCollection whereParam;

            DateTime DENPYO_DATE = Util.ConvAdDate(this.lblGengo.Text, this.txtGengoYear.Text,
                    this.txtMonth.Text, this.txtDay.Text, this.Dba);
            Decimal DENPYO_BANGO = Util.ToDecimal(this.txtDenpyoNo.Text);
            int i;

            // 消費税振分処理（伝票転嫁）
            List<MeisaiTbl> taxTbl = new List<MeisaiTbl>();
            TaxSorting(ref taxTbl);
            // 内訳表示
            SetTaxInfo();

            try
            {
                // トランザクション開始
                this.Dba.BeginTransaction();

                // 伝票番号確定処理
                
                // Han.TB_取引伝票(TB_HN_TORIHIKI_DENPYO)
                dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToInt(Util.ToString(txtMizuageShishoCd.Text)));
                dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
                dpc.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 4, 2);
                dpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 8, DENPYO_BANGO);
                dtCheck = this.Dba.GetDataTableByConditionWithParams(
                    "COUNT(*) AS CNT",
                    "TB_HN_TORIHIKI_DENPYO",
                    "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND KAIKEI_NENDO = @KAIKEI_NENDO AND DENPYO_KUBUN = @DENPYO_KUBUN AND DENPYO_BANGO = @DENPYO_BANGO",
                    dpc);
                if (Util.ToLong(dtCheck.Rows[0]["CNT"]) == 0)
                {
                    // 仕入伝票新規作成
                    DENPYO_BANGO = Util.ToDecimal(this.Dba.GetHNDenpyoNo(this.UInfo, Util.ToDecimal(Util.ToString(txtMizuageShishoCd.Text)), 2, 0));

                    if (this.Dba.UpdateHNDenpyoNo(this.UInfo, Util.ToDecimal(Util.ToString(txtMizuageShishoCd.Text)), 2, 0, Util.ToInt(DENPYO_BANGO)) != 1)
                    {
                        Msg.Error("伝票番号の発番に失敗しました。");
                        this.txtDenpyoNo.SelectAll();
                        return;
                    }
                }

                // 事前在庫更新
                ZaikoKousin(DENPYO_BANGO, 3);

                // 伝票明細削除
                // Han.TB_取引明細(TB_HN_TORIHIKI_MEISAI)
                whereParam = new DbParamCollection();

                whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                whereParam.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToInt(Util.ToString(txtMizuageShishoCd.Text)));
                whereParam.SetParam("@KAIKEI_NENDO", SqlDbType.Int, 4, this.UInfo.KaikeiNendo);
                whereParam.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 4, 2);
                whereParam.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 8, DENPYO_BANGO);
                this.Dba.Delete("TB_HN_TORIHIKI_MEISAI",
                    "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND KAIKEI_NENDO = @KAIKEI_NENDO AND DENPYO_KUBUN = @DENPYO_KUBUN AND DENPYO_BANGO = @DENPYO_BANGO",
                    whereParam);

                // Han.TB_取引伝票(TB_HN_TORIHIKI_DENPYO)
                dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToInt(Util.ToString(txtMizuageShishoCd.Text)));
                dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Int, 4, this.UInfo.KaikeiNendo);
                dpc.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 4, 2);
                dpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 8, DENPYO_BANGO);
                dtCheck = this.Dba.GetDataTableByConditionWithParams(
                    "COUNT(*) AS CNT",
                    "TB_HN_TORIHIKI_DENPYO",
                    "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND KAIKEI_NENDO = @KAIKEI_NENDO AND DENPYO_KUBUN = @DENPYO_KUBUN AND DENPYO_BANGO = @DENPYO_BANGO",
                    dpc);
                if ((int)dtCheck.Rows[0]["CNT"] == 0)
                {
                    // 伝票登録
                    updParam = new DbParamCollection();

                    updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                    updParam.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToInt(Util.ToString(txtMizuageShishoCd.Text)));
                    updParam.SetParam("@KAIKEI_NENDO", SqlDbType.Int, 4, this.UInfo.KaikeiNendo);
                    updParam.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 4, 2);
                    updParam.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 8, DENPYO_BANGO);
                    updParam.SetParam("@DENPYO_DATE", SqlDbType.DateTime, DENPYO_DATE);
                    updParam.SetParam("@TORIHIKI_KUBUN1", SqlDbType.Decimal, 3, Util.ToDecimal(this.txtTorihikiKubunCd.Text.Substring(0, 1)));
                    updParam.SetParam("@TORIHIKI_KUBUN2", SqlDbType.Decimal, 3, Util.ToDecimal(this.txtTorihikiKubunCd.Text.Substring(1, 1)));
                    //updParam.SetParam("@KAIIN_BANGO", SqlDbType.VarChar, 4, this.txtSiiresakiCd.Text);
                    //updParam.SetParam("@KAIIN_NM", SqlDbType.VarChar, 30, this.lblSiiresakiNm.Text);
                    updParam.SetParam("@TOKUISAKI_CD", SqlDbType.VarChar, 4, this.txtSiiresakiCd.Text);
                    updParam.SetParam("@TOKUISAKI_NM", SqlDbType.VarChar, 40, this.lblSiiresakiNm.Text);
                    updParam.SetParam("@SEIKYUSAKI_CD", SqlDbType.Decimal, 6, Util.ToDecimal(this.txtSiharaisakiCd.Text));
                    updParam.SetParam("@TANTOSHA_CD", SqlDbType.Decimal, 6, Util.ToDecimal(this.txtTantoshaCd.Text));
                    updParam.SetParam("@SHIWAKE_DENPYO_BANGO", SqlDbType.Decimal, 8, 0);
                    //updParam.SetParam("@TENPO_CD", SqlDbType.VarChar, 10, "0");
                    updParam.SetParam("@URIAGE_KINGAKU", SqlDbType.Decimal, 11, Util.ToDecimal(this.txtSyokei.Text));
                    updParam.SetParam("@SHOHIZEIGAKU", SqlDbType.Decimal, 11, Util.ToDecimal(this.txtSyohiZei.Text));
                    //updParam.SetParam("@GENKIN_NYUKINGAKU", SqlDbType.Decimal, 11, 0);
                    //updParam.SetParam("@NEBIKIGAKU", SqlDbType.Decimal, 11, 0);
                    updParam.SetParam("@NYUKIN_GOKEIGAKU", SqlDbType.Decimal, 11, Util.ToDecimal(this.txtTotalGaku.Text));
                    //updParam.SetParam("@OTSURI", SqlDbType.Decimal, 11, 0);
                    updParam.SetParam("@TOROKU_JIKAN", SqlDbType.DateTime, DateTime.Now);
                    updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWTIME");

                    this.Dba.Insert("TB_HN_TORIHIKI_DENPYO", updParam);
                }
                else
                {
                    // 伝票更新
                    updParam = new DbParamCollection();
                    whereParam = new DbParamCollection();

                    updParam.SetParam("@DENPYO_DATE", SqlDbType.DateTime, DENPYO_DATE);
                    updParam.SetParam("@TORIHIKI_KUBUN1", SqlDbType.Decimal, 3, Util.ToDecimal(txtTorihikiKubunCd.Text.Substring(0, 1)));
                    updParam.SetParam("@TORIHIKI_KUBUN2", SqlDbType.Decimal, 3, Util.ToDecimal(txtTorihikiKubunCd.Text.Substring(1, 1)));
                    //updParam.SetParam("@KAIIN_BANGO", SqlDbType.VarChar, 4, this.txtSiiresakiCd.Text);
                    //updParam.SetParam("@KAIIN_NM", SqlDbType.VarChar, 30, this.lblSiiresakiNm.Text);
                    updParam.SetParam("@TOKUISAKI_CD", SqlDbType.VarChar, 4, this.txtSiiresakiCd.Text);
                    updParam.SetParam("@TOKUISAKI_NM", SqlDbType.VarChar, 40, this.lblSiiresakiNm.Text);
                    updParam.SetParam("@SEIKYUSAKI_CD", SqlDbType.Decimal, 6, Util.ToDecimal(this.txtSiharaisakiCd.Text));
                    updParam.SetParam("@TANTOSHA_CD", SqlDbType.Decimal, 6, Util.ToDecimal(this.txtTantoshaCd.Text));
                    //updParam.SetParam("@SHIWAKE_DENPYO_BANGO", SqlDbType.Decimal, 8, 0);
                    //updParam.SetParam("@TENPO_CD", SqlDbType.VarChar, 10, "0");
                    updParam.SetParam("@URIAGE_KINGAKU", SqlDbType.Decimal, 11, Util.ToDecimal(this.txtSyokei.Text));
                    updParam.SetParam("@SHOHIZEIGAKU", SqlDbType.Decimal, 11, Util.ToDecimal(this.txtSyohiZei.Text));
                    //updParam.SetParam("@GENKIN_NYUKINGAKU", SqlDbType.Decimal, 11, 0);
                    //updParam.SetParam("@NEBIKIGAKU", SqlDbType.Decimal, 11, 0);
                    updParam.SetParam("@NYUKIN_GOKEIGAKU", SqlDbType.Decimal, 11, Util.ToDecimal(this.txtTotalGaku.Text));
                    //updParam.SetParam("@OTSURI", SqlDbType.Decimal, 11, 0);
                    updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWTIME");

                    whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                    whereParam.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToInt(Util.ToString(txtMizuageShishoCd.Text)));
                    whereParam.SetParam("@KAIKEI_NENDO", SqlDbType.Int, 4, this.UInfo.KaikeiNendo);
                    whereParam.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 4, 2);
                    whereParam.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 8, DENPYO_BANGO);

                    this.Dba.Update("TB_HN_TORIHIKI_DENPYO",
                                    updParam,
                                    "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND KAIKEI_NENDO = @KAIKEI_NENDO AND DENPYO_KUBUN = @DENPYO_KUBUN AND DENPYO_BANGO = @DENPYO_BANGO",
                                    whereParam
                                    );
                }

                // 伝票明細(DataGridView[name=dgvInputList]を取得)
                i = 0;
                while (this.dgvInputList.RowCount - 1 > i)
                {
                    // Han.TB_取引明細(TB_HN_TORIHIKI_MEISAI)
                    dpc = new DbParamCollection();
                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                    dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToInt(Util.ToString(txtMizuageShishoCd.Text)));
                    dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Int, 4, this.UInfo.KaikeiNendo);
                    dpc.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 4, 2);
                    dpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 8, DENPYO_BANGO);
                    dpc.SetParam("@GYO_BANGO", SqlDbType.Decimal, 6, Util.ToDecimal(this.dgvInputList[0, i].Value));
                    //dtCheck = this.Dba.GetDataTableByConditionWithParams(
                    //    "COUNT(*) AS CNT",
                    //    "TB_HN_TORIHIKI_MEISAI",
                    //    "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND KAIKEI_NENDO = @KAIKEI_NENDO AND DENPYO_KUBUN = 2 AND DENPYO_BANGO = @DENPYO_BANGO AND GYO_BANGO = @GYO_BANGO",
                    //    dpc);
                    dtCheck = this.Dba.GetDataTableByConditionWithParams(
                        "COUNT(*) AS CNT",
                        "TB_HN_TORIHIKI_MEISAI",
                        "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND KAIKEI_NENDO = @KAIKEI_NENDO AND DENPYO_KUBUN = @DENPYO_KUBUN AND DENPYO_BANGO = @DENPYO_BANGO AND GYO_BANGO = @GYO_BANGO",
                        dpc);
                    if ((int)dtCheck.Rows[0]["CNT"] == 0)
                    {
                        // 伝票明細登録
                        updParam = new DbParamCollection();

                        updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                        updParam.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToInt(Util.ToString(txtMizuageShishoCd.Text)));
                        updParam.SetParam("@KAIKEI_NENDO", SqlDbType.Int, 4, this.UInfo.KaikeiNendo);
                        updParam.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 4, 2);
                        updParam.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 8, DENPYO_BANGO);
                        updParam.SetParam("@GYO_BANGO", SqlDbType.Decimal, 6, Util.ToDecimal(this.dgvInputList[COL_GYO_NO, i].Value));
                        updParam.SetParam("@SHOHIN_CD", SqlDbType.Decimal, 15, Util.ToDecimal(this.dgvInputList[COL_SHOHIN_CD, i].Value));
                        updParam.SetParam("@SHOHIN_NM", SqlDbType.VarChar, 40, Util.ToString(this.dgvInputList[COL_SHOHIN_NM, i].Value));
                        updParam.SetParam("@IRISU", SqlDbType.Decimal, 6, Util.ToDecimal(Util.ToString(this.dgvInputList[COL_IRISU, i].Value)));
                        updParam.SetParam("@TANI", SqlDbType.VarChar, 4, Util.ToString(this.dgvInputList[COL_TANI, i].Value));
                        updParam.SetParam("@SURYO1", SqlDbType.Decimal, 14, Util.ToDecimal(Util.ToString(this.dgvInputList[COL_SURYO1, i].Value)));
                        updParam.SetParam("@SURYO2", SqlDbType.Decimal, 14, Util.ToDecimal(Util.ToString(this.dgvInputList[COL_SURYO2, i].Value)));
                        updParam.SetParam("@KUBUN", SqlDbType.Decimal, 6, Util.ToDecimal(Util.ToString(this.dgvInputList[COL_KUBUN, i].Value)));
                        updParam.SetParam("@GEN_TANKA", SqlDbType.Decimal, 13, 0);
                        updParam.SetParam("@GENKA_KINGAKU", SqlDbType.Decimal, 14, 0);
                        updParam.SetParam("@URI_TANKA", SqlDbType.Decimal, 13, Util.ToDecimal(Util.ToString(this.dgvInputList[COL_URI_TANKA, i].Value)));
                        updParam.SetParam("@BAIKA_KINGAKU", SqlDbType.Decimal, 14, Util.ToDecimal(Util.ToString(this.dgvInputList[COL_BAIKA_KINGAKU, i].Value)));
                        updParam.SetParam("@HANBAI_TANKA", SqlDbType.Decimal, 13, 0);
                        updParam.SetParam("@HANBAI_KINGAKU", SqlDbType.Decimal, 14, 0);
                        updParam.SetParam("@SHOHIZEI", SqlDbType.Decimal, 14, taxTbl[i].Zeigk);
                        updParam.SetParam("@SHOHIZEI_NYURYOKU_HOHO", SqlDbType.Decimal, 3, this.CalcInfo.SHOHIZEI_NYURYOKU_HOHO);
                        updParam.SetParam("@SHIWAKE_CD", SqlDbType.Decimal, 6, Util.ToDecimal(Util.ToString(this.dgvInputList[COL_SHIWAKE_CD, i].Value)));
                        updParam.SetParam("@BUMON_CD", SqlDbType.Decimal, 6, Util.ToDecimal(Util.ToString(this.dgvInputList[COL_BUMON_CD, i].Value)));
                        updParam.SetParam("@ZEI_KUBUN", SqlDbType.Decimal, 4, Util.ToDecimal(Util.ToString(this.dgvInputList[COL_ZEI_KUBUN, i].Value)));
                        updParam.SetParam("@JIGYO_KUBUN", SqlDbType.Decimal, 3, Util.ToDecimal(Util.ToString(this.dgvInputList[COL_JIGYO_KUBUN, i].Value)));
                        updParam.SetParam("@ZEI_RITSU", SqlDbType.Decimal, 14, Util.ToDecimal(Util.ToString(this.dgvInputList[COL_ZEI_RITSU, i].Value)));
                        updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWTIME");

                        this.Dba.Insert("TB_HN_TORIHIKI_MEISAI", updParam);
                    }

                    // 仕入単価履歴マスタ登録
                    // HAN.TB_仕入単価履歴(TB_HN_SHIIRE_TANKA_RIREKI)
                    dpc = new DbParamCollection();
                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                    dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToInt(Util.ToString(txtMizuageShishoCd.Text)));
                    dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
                    dpc.SetParam("@SEIKYUSAKI_CD", SqlDbType.Decimal, 6, Util.ToDecimal(this.txtSiharaisakiCd.Text));
                    dpc.SetParam("@TOKUISAKI_CD", SqlDbType.Decimal, 6, Util.ToDecimal(this.txtSiiresakiCd.Text));
                    dpc.SetParam("@SHOHIN_CD", SqlDbType.Decimal, 13, Util.ToDecimal(this.dgvInputList[COL_SHOHIN_CD, i].Value));
                    dpc.SetParam("@TANKA", SqlDbType.Decimal, 10, Util.ToDecimal(Util.ToString(this.dgvInputList[COL_URI_TANKA, i].Value)));
                    dtCheck = this.Dba.GetDataTableByConditionWithParams(
                        "*",
                        "TB_HN_SHIIRE_TANKA_RIREKI",
                        "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND SEIKYUSAKI_CD = @SEIKYUSAKI_CD AND TOKUISAKI_CD = @TOKUISAKI_CD AND SHOHIN_CD = @SHOHIN_CD AND KAIKEI_NENDO = @KAIKEI_NENDO AND TANKA = @TANKA",
                        dpc);
                    if (dtCheck.Rows.Count == 0)
                    {
                        updParam = new DbParamCollection();

                        updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                        updParam.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToInt(Util.ToString(txtMizuageShishoCd.Text)));
                        updParam.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
                        updParam.SetParam("@SEIKYUSAKI_CD", SqlDbType.Decimal, 6, Util.ToDecimal(this.txtSiharaisakiCd.Text));
                        updParam.SetParam("@TOKUISAKI_CD", SqlDbType.Decimal, 6, Util.ToDecimal(this.txtSiiresakiCd.Text));
                        updParam.SetParam("@SHOHIN_CD", SqlDbType.Decimal, 13, Util.ToDecimal(this.dgvInputList[COL_SHOHIN_CD, i].Value));
                        updParam.SetParam("@TANKA", SqlDbType.Decimal, 10, Util.ToDecimal(Util.ToString(this.dgvInputList[COL_URI_TANKA, i].Value)));
                        updParam.SetParam("@SAISHU_TORIHIKI_DATE", SqlDbType.DateTime, DENPYO_DATE);
                        updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWTIME");

                        this.Dba.Insert("TB_HN_SHIIRE_TANKA_RIREKI", updParam);
                    }
                    else
                    {
                        updParam = new DbParamCollection();
                        whereParam = new DbParamCollection();

                        updParam.SetParam("@SAISHU_TORIHIKI_DATE", SqlDbType.DateTime, DENPYO_DATE);
                        updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWTIME");

                        whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                        whereParam.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToInt(Util.ToString(txtMizuageShishoCd.Text)));
                        whereParam.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
                        whereParam.SetParam("@SEIKYUSAKI_CD", SqlDbType.Decimal, 6, Util.ToDecimal(this.txtSiharaisakiCd.Text));
                        whereParam.SetParam("@TOKUISAKI_CD", SqlDbType.Decimal, 6, Util.ToDecimal(this.txtSiiresakiCd.Text));
                        whereParam.SetParam("@SHOHIN_CD", SqlDbType.Decimal, 13, Util.ToDecimal(this.dgvInputList[COL_SHOHIN_CD, i].Value));
                        whereParam.SetParam("@TANKA", SqlDbType.Decimal, 10, Util.ToDecimal(Util.ToString(this.dgvInputList[COL_URI_TANKA, i].Value)));

                        // 単価があり伝票日付が最終取引日付より新しい場合
                        if (Util.ToDecimal(Util.ToString(this.dgvInputList[COL_URI_TANKA, i].Value)) > 0 &&
                            DENPYO_DATE > Util.ToDate(dtCheck.Rows[0]["SAISHU_TORIHIKI_DATE"]))
                        {
                            this.Dba.Update("TB_HN_SHIIRE_TANKA_RIREKI",
                                            updParam,
                                            "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND SEIKYUSAKI_CD = @SEIKYUSAKI_CD AND TOKUISAKI_CD = @TOKUISAKI_CD AND SHOHIN_CD = @SHOHIN_CD AND KAIKEI_NENDO = @KAIKEI_NENDO AND TANKA = @TANKA",
                                            whereParam
                                            );
                        }
                    }

                    i++;
                }
                // 在庫更新
                ZaikoKousin(DENPYO_BANGO, 1);

                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            this.lblBeforeDenpyoNo.Text = "【前回" + (this.MODE_EDIT == 1 ? "登録" : "更新") + "伝票番号：" + DENPYO_BANGO.ToString().PadLeft(6) + "】";
            this.lblBeforeDenpyoNo.Visible = true;
        }

        /// <summary>
        /// 削除処理
        /// </summary>
        private void DeleteData()
        {
            DbParamCollection whereParam;

            Decimal DENPYO_BANGO = Util.ToDecimal(this.txtDenpyoNo.Text);

            try
            {
                // トランザクション開始
                this.Dba.BeginTransaction();

                // 在庫更新
                ZaikoKousin(DENPYO_BANGO, 3);

                // 伝票削除
                // Han.TB_取引伝票(TB_HN_TORIHIKI_DENPYO)
                whereParam = new DbParamCollection();

                whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                whereParam.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToInt(Util.ToString(txtMizuageShishoCd.Text)));
                whereParam.SetParam("@KAIKEI_NENDO", SqlDbType.Int, 4, this.UInfo.KaikeiNendo);
                whereParam.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 4, 2);
                whereParam.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 8, DENPYO_BANGO);
                this.Dba.Delete("TB_HN_TORIHIKI_DENPYO",
                    "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND KAIKEI_NENDO = @KAIKEI_NENDO AND DENPYO_KUBUN = @DENPYO_KUBUN AND DENPYO_BANGO = @DENPYO_BANGO",
                    whereParam);

                // 伝票明細削除
                // Han.TB_取引明細(TB_HN_TORIHIKI_MEISAI)
                whereParam = new DbParamCollection();

                whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                whereParam.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToInt(Util.ToString(txtMizuageShishoCd.Text)));
                whereParam.SetParam("@KAIKEI_NENDO", SqlDbType.Int, 4, this.UInfo.KaikeiNendo);
                whereParam.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 4, 2);
                whereParam.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 8, DENPYO_BANGO);
                this.Dba.Delete("TB_HN_TORIHIKI_MEISAI",
                    "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND KAIKEI_NENDO = @KAIKEI_NENDO AND DENPYO_KUBUN = @DENPYO_KUBUN AND DENPYO_BANGO = @DENPYO_BANGO",
                    whereParam);

                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            this.lblBeforeDenpyoNo.Text = "【前回削除伝票番号：" + DENPYO_BANGO.ToString().PadLeft(6) + "】";
            this.lblBeforeDenpyoNo.Visible = true;
        }

        /// <summary>
        /// 商品コードから商品情報取得
        /// </summary>
        /// <param name="input_type">1:手入力,2:一覧画面から取得</param>
        private void GetShohinInfo(int input_type)
        {
            DbParamCollection dpc;
            DataGridViewCell dgvCell = (DataGridViewCell)dgvInputList.CurrentCell;

            // 商品情報取得
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Int, 6, Util.ToInt(Util.ToString(txtMizuageShishoCd.Text)));
            dpc.SetParam("@SHOHIN_CD", SqlDbType.VarChar, 15, this.txtGridEdit.Text);
            // 中止区分対応
            dpc.SetParam("@CHUSHI_KUBUN", SqlDbType.Decimal, 4, this.HIN_CHUSHI_KUBUN);
            string condition = "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND SHOHIN_CD = @SHOHIN_CD AND SHOHIN_KUBUN5 <> 1";
            //DataTable dtVI_HN_SHOHIN = this.Dba.GetDataTableByConditionWithParams(
            //    "*",
            //    "VI_HN_SHOHIN",
            //    "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND SHOHIN_CD = @SHOHIN_CD AND SHOHIN_KUBUN5 <> 1",
            //    dpc);
            if (this.HIN_CHUSHI_KUBUN == 1)
            {
                condition = "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND SHOHIN_CD = @SHOHIN_CD AND ISNULL(CHUSHI_KUBUN, 0) = @CHUSHI_KUBUN AND SHOHIN_KUBUN5 <> 1";
            }
            DataTable dtVI_HN_SHOHIN = this.Dba.GetDataTableByConditionWithParams(
                "*",
                "VI_HN_SHOHIN",
                condition,
                dpc);
            if (dtVI_HN_SHOHIN.Rows.Count == 0)
            {
                this.dgvInputList[COL_SHOHIN_CD, dgvCell.RowIndex].Value = "";
                this.dgvInputList[COL_SHOHIN_NM, dgvCell.RowIndex].Value = "";
                this.dgvInputList[COL_SHIWAKE_CD, dgvCell.RowIndex].Value = "";
                this.dgvInputList[COL_BUMON_CD, dgvCell.RowIndex].Value = "";
                this.dgvInputList[COL_ZEI_KUBUN, dgvCell.RowIndex].Value = "";
                this.dgvInputList[COL_JIGYO_KUBUN, dgvCell.RowIndex].Value = "";
                this.dgvInputList[COL_ZEI_RITSU, dgvCell.RowIndex].Value = "";
                this.dgvInputList[COL_GEN_TANKA, dgvCell.RowIndex].Value = "";
                this.dgvInputList[COL_KAZEI_KUBUN, dgvCell.RowIndex].Value = "";
                this.dgvInputList[COL_ZAIKO_KANRI_KUBUN, dgvCell.RowIndex].Value = "";
                this.dgvInputList[COL_IRISU, dgvCell.RowIndex].Value = "";
                this.dgvInputList[COL_TANI, dgvCell.RowIndex].Value = "";
                Msg.Error("入力に誤りがあります。");
                this.txtGridEdit.Focus();
                return;
            }
            else
            {
                if (Util.ToString(this.dgvInputList[COL_SHOHIN_CD, dgvCell.RowIndex].Value) != this.txtGridEdit.Text)
                {
                    // 旧システムは売上のみの為仕入には組み込まない
                    //// 商品の消費税区分による税込み税抜き切り替え
                    //// 漁協により商品マスタの消費税区分で税抜き税込みを行う場所への対応
                    //// １行目が税込み商品の場合は以降は全て税込みの商品のみの入力を許す形
                    //// 違う商品が入力時はエラー扱い
                    //// 但し伝票内で税抜き税込みとなる様に調整（帳票では必ず伝票に入力方法を見る事）
                    //if (dgvCell.RowIndex == 0)
                    //{
                    //    this.SiireInfo.SHOHIN_SHOHIZEI_KUBUN = Util.ToString(dtVI_HN_SHOHIN.Rows[0]["SHOHIZEI_KUBUN"]);

                    //    // 旧システムではゼロが設定時は税込みとなってた（変更時は調整）
                    //    if (this.SiireInfo.SHOHIN_SHOHIZEI_KUBUN == "0")
                    //    {
                    //        this.SiireInfo.SHOHIZEI_NYURYOKU_HOHO = 3;
                    //        this.CellTaxRateSet();

                    //        // 名称の設定
                    //        this.SetTaxInputCategory(Util.ToInt(Util.ToString(this.SiireInfo.SHOHIZEI_NYURYOKU_HOHO)));
                    //    }
                    //}
                    //else
                    //{
                    //    // 商品消費税区分が違う商品の場合はエラー（入力方法は伝票で統一）
                    //    if (this.SiireInfo.SHOHIN_SHOHIZEI_KUBUN != Util.ToString(dtVI_HN_SHOHIN.Rows[0]["SHOHIZEI_KUBUN"]))
                    //    {
                    //        this.dgvInputList[COL_SHOHIN_CD, dgvCell.RowIndex].Value = "";
                    //        this.dgvInputList[COL_SHOHIN_NM, dgvCell.RowIndex].Value = "";
                    //        this.dgvInputList[COL_SHIWAKE_CD, dgvCell.RowIndex].Value = "";
                    //        this.dgvInputList[COL_BUMON_CD, dgvCell.RowIndex].Value = "";
                    //        this.dgvInputList[COL_ZEI_KUBUN, dgvCell.RowIndex].Value = "";
                    //        this.dgvInputList[COL_JIGYO_KUBUN, dgvCell.RowIndex].Value = "";
                    //        this.dgvInputList[COL_ZEI_RITSU, dgvCell.RowIndex].Value = "";
                    //        this.dgvInputList[COL_GEN_TANKA, dgvCell.RowIndex].Value = "";
                    //        this.dgvInputList[COL_KAZEI_KUBUN, dgvCell.RowIndex].Value = "";
                    //        this.dgvInputList[COL_ZAIKO_KANRI_KUBUN, dgvCell.RowIndex].Value = "";
                    //        this.dgvInputList[COL_IRISU, dgvCell.RowIndex].Value = "";
                    //        this.dgvInputList[COL_TANI, dgvCell.RowIndex].Value = "";

                    //        Msg.Error("入力に誤りがあります。");
                    //        this.txtGridEdit.Focus();
                    //        return;
                    //    }
                    //}

                    // 商品コード設定
                    this.dgvInputList[COL_SHOHIN_CD, dgvCell.RowIndex].Value = txtGridEdit.Text;
                    // 商品名設定
                    this.dgvInputList[COL_SHOHIN_NM, dgvCell.RowIndex].Value = dtVI_HN_SHOHIN.Rows[0]["SHOHIN_NM"];
                    //// 単価設定
                    //if (ValChk.IsEmpty(dgvInputList[8, dgvCell.RowIndex].Value))
                    //{
                    //    this.dgvInputList[8, dgvCell.RowIndex].Value = Util.FormatNum(dtVI_HN_SHOHIN.Rows[0]["SHIIRE_TANKA"], 1);
                    //}
                    // 単価はマスタの設定により取得先の切り替え？
                    // ベースは仕入単価
                    Decimal tanka = Util.ToDecimal(Util.FormatNum(dtVI_HN_SHOHIN.Rows[0]["SHIIRE_TANKA"], this.CalcInfo.KETA_URI_TANKA));
                    switch (Util.ToInt(this.CalcInfo.TANKA_SHUTOKU_HOHO))
                    {
                        case 2: // 単価履歴から取得出来た場合は前回単価、取得出来ない場合は仕入単価
                            if (!GetTanka(Util.ToInt(this.txtSiiresakiCd.Text), Util.ToInt(this.txtSiharaisakiCd.Text), Util.ToDecimal(txtGridEdit.Text), ref tanka))
                            {
                                tanka = Util.ToDecimal(Util.FormatNum(dtVI_HN_SHOHIN.Rows[0]["SHIIRE_TANKA"], this.CalcInfo.KETA_URI_TANKA));
                            }
                            break;
                    }
                    this.dgvInputList[COL_URI_TANKA, dgvCell.RowIndex].Value = Util.FormatNum(tanka, this.CalcInfo.KETA_URI_TANKA);
                    // 仕訳コード
                    this.dgvInputList[COL_SHIWAKE_CD, dgvCell.RowIndex].Value = Util.ToString(dtVI_HN_SHOHIN.Rows[0]["SHIIRE_SHIWAKE_CD"]);
                    // 部門コード
                    this.dgvInputList[COL_BUMON_CD, dgvCell.RowIndex].Value = Util.ToString(CalcInfo.BUMON_CD != 0 ? CalcInfo.BUMON_CD : dtVI_HN_SHOHIN.Rows[0]["SHIIRE_BUMON_CD"]);
                    // 税区分
                    this.dgvInputList[COL_ZEI_KUBUN, dgvCell.RowIndex].Value = Util.ToString(dtVI_HN_SHOHIN.Rows[0]["SHIIRE_ZEI_KUBUN"]);
                    // 事業区分
                    this.dgvInputList[COL_JIGYO_KUBUN, dgvCell.RowIndex].Value = Util.ToString(CalcInfo.JIGYO_KUBUN != 0 ? CalcInfo.JIGYO_KUBUN : dtVI_HN_SHOHIN.Rows[0]["SHIIRE_JIGYO_KUBUN"]);
                    // 入数
                    this.dgvInputList[COL_IRISU, dgvCell.RowIndex].Value = (Util.ToDecimal(dtVI_HN_SHOHIN.Rows[0]["IRISU"]) == 0 ? "" : dtVI_HN_SHOHIN.Rows[0]["IRISU"]);
                    // 単位
                    this.dgvInputList[COL_TANI, dgvCell.RowIndex].Value = Util.ToString(dtVI_HN_SHOHIN.Rows[0]["TANI"]);
                    // 在庫管理区分
                    this.dgvInputList[COL_ZAIKO_KANRI_KUBUN, dgvCell.RowIndex].Value = Util.ToInt(dtVI_HN_SHOHIN.Rows[0]["ZAIKO_KANRI_KUBUN"]);

                    // 仕入税区分チェック
                    if (!ValChk.IsEmpty(dtVI_HN_SHOHIN.Rows[0]["SHIIRE_ZEI_KUBUN"]))
                    {
                        // zam.TB_Ｆ税区分(TB_ZM_F_ZEI_KUBUN)
                        // 課税区分を取得
                        dpc = new DbParamCollection();
                        dpc.SetParam("@ZEI_KUBUN", SqlDbType.Decimal, 4, Util.ToDecimal(dtVI_HN_SHOHIN.Rows[0]["SHIIRE_ZEI_KUBUN"]));
                        DataTable dtTB_ZM_F_ZEI_KUBUN = this.Dba.GetDataTableByConditionWithParams(
                            "*",
                            "TB_ZM_F_ZEI_KUBUN",
                            "ZEI_KUBUN = @ZEI_KUBUN",
                            dpc);
                        // 税率
                        DateTime DENPYO_DATE = Util.ConvAdDate(this.lblGengo.Text, this.txtGengoYear.Text,
                                                               this.txtMonth.Text, this.txtDay.Text, this.Dba);
                        this.dgvInputList[COL_ZEI_RITSU, dgvCell.RowIndex].Value = Util.FormatNum(TaxUtil.GetTaxRate(DENPYO_DATE, Util.ToInt(Util.ToString(this.dgvInputList[COL_ZEI_KUBUN, dgvCell.RowIndex].Value)), this.Dba), 1);
                        // 課税区分
                        this.dgvInputList[COL_KAZEI_KUBUN, dgvCell.RowIndex].Value = Util.ToString(dtTB_ZM_F_ZEI_KUBUN.Rows[0]["KAZEI_KUBUN"]);
                    }
                    else
                    {
                        this.dgvInputList[COL_ZEI_RITSU, dgvCell.RowIndex].Value = "";
                        this.dgvInputList[COL_KAZEI_KUBUN, dgvCell.RowIndex].Value = 0;
                    }

                    if (this.dgvInputList.RowCount == dgvCell.RowIndex + 1)
                    {
                        // 行追加
                        //this.dgvInputList.Rows.Add();
                        this.dgvInputList.RowCount = this.dgvInputList.RowCount + 1;
                        this.dgvInputList[COL_GYO_NO, this.dgvInputList.RowCount - 1].Value = this.dgvInputList.RowCount;
                    }
                }
                if (input_type == 1)
                {
                    //this.dgvInputList.CurrentCell = this.dgvInputList[7, dgvCell.RowIndex];
                    // 在庫管理区分
                    if (Util.ToInt(this.dgvInputList[COL_ZAIKO_KANRI_KUBUN, dgvCell.RowIndex].Value) == 0)
                    {
                        // 在庫管理無しの場合は商品名
                        this.dgvInputList.CurrentCell = this.dgvInputList[COL_SHOHIN_NM, dgvCell.RowIndex];
                    }
                    else
                    {
                        // 入数が無い場合はバラ数、入数がある場合はケースへ
                        if (Util.ToInt(Util.ToString(this.dgvInputList[COL_IRISU, dgvCell.RowIndex].Value)) == 0)
                            this.dgvInputList.CurrentCell = this.dgvInputList[COL_SURYO2, dgvCell.RowIndex];
                        else
                            this.dgvInputList.CurrentCell = this.dgvInputList[COL_SURYO1, dgvCell.RowIndex];
                    }
                }
            }

            this.IS_INPUT = 2;
        }

        /// <summary>
        /// 伝票データチェック後、伝票修正処理へ
        /// </summary>
        /// <returns>イベントをキャンセルする真偽値(真:キャンセル,偽:続行)</param>
        private bool GetDenpyoData()
        {
            DbParamCollection dpc;
            DataTable dtTB_HN_TORIHIKI_DENPYO;
            dpc = new DbParamCollection();
            // 仕入伝票情報確認
            // Han.TB_取引伝票(TB_HN_TORIHIKI_DENPYO)
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToInt(Util.ToString(txtMizuageShishoCd.Text)));
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            dpc.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 4, 2);
            dpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 8, Util.ToDecimal(this.txtDenpyoNo.Text));
            dtTB_HN_TORIHIKI_DENPYO = this.Dba.GetDataTableByConditionWithParams(
                "*",
                "TB_HN_TORIHIKI_DENPYO",
                "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND KAIKEI_NENDO = @KAIKEI_NENDO AND DENPYO_KUBUN = @DENPYO_KUBUN AND DENPYO_BANGO = @DENPYO_BANGO",
                dpc);

            if (dtTB_HN_TORIHIKI_DENPYO.Rows.Count == 0)
            {
                //Msg.Error("入力に誤りがあります。");
                Msg.Info("該当データがありません。");
                this.txtDenpyoNo.SelectAll();
                return true;
            }
            // 既に修正モードへ切り替え時には処理しない
            if (this.MEISAI_DENPYO_BANGO != Util.ToDecimal(this.txtDenpyoNo.Text))
            {
                this.MEISAI_DENPYO_BANGO = Util.ToDecimal(this.txtDenpyoNo.Text);
                // 修正モードへ切り替え
                this.InitDispOnEdit(dtTB_HN_TORIHIKI_DENPYO);
            }
            return false;
        }

        /// <summary>
        /// 伝票明細エンターキー以外の処理
        /// </summary>
        /// <returns>エンターキーを押したかの真偽値(真:押した,偽:押してない)</param>
        private bool GetDenpyoMeisaiKeyEnter(Keys KeyCode, DataGridViewCell dgvCell)
        {
            if ((KeyCode == Keys.Enter) || (KeyCode == Keys.Right))
            {
                return true;
            }
            if (KeyCode == Keys.Down)
            {
                if (this.dgvInputList.RowCount > dgvCell.RowIndex + 1)
                {
                    this.dgvInputList.CurrentCell = dgvInputList[dgvCell.ColumnIndex, dgvCell.RowIndex + 1];
                }
            }
            else if (KeyCode == Keys.Up)
            {
                if (dgvCell.RowIndex > 0)
                {
                    this.dgvInputList.CurrentCell = dgvInputList[dgvCell.ColumnIndex, dgvCell.RowIndex - 1];

                }
                else if (dgvCell.ColumnIndex == 1 && dgvCell.RowIndex == 0)
                {
                    this.txtTantoshaCd.Focus();
                }
            }
            return false;
        }

        /// <summary>
        /// 伝票番号の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool isValidDenpyoNo()
        {
            // 数値チェック
            if (!ValChk.IsNumber(this.txtDenpyoNo.Text))
            {
                //this.txtDenpyoNo.Text = Regex.Replace(this.txtDenpyoNo.Text, "\\D", "");
                Msg.Notice("数値のみで入力してください。");
                return false;
            }
            // 最大桁数チェック
            else if (!ValChk.IsWithinLength(this.txtDenpyoNo.Text, this.txtDenpyoNo.MaxLength))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            // 番号変更時のクリア
            if (this.MEISAI_DENPYO_BANGO != 0 && ValChk.IsEmpty(this.txtDenpyoNo.Text))
            {
                this.InitDispOnNew();
                return true;
            }
            else if (ValChk.IsEmpty(this.txtDenpyoNo.Text) && this.MEISAI_DENPYO_BANGO == 0)
            {
                // 空の場合
                return true;
            }

            return true;
        }

        /// <summary>
        /// 水揚支所の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool IsValidMizuageShishoCd()
        {
            // 空 又は 0入力の場合
            if (ValChk.IsEmpty(this.txtMizuageShishoCd.Text) || Equals(this.txtMizuageShishoCd.Text, "0"))
            {
                // 伝票入力時は必須
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            // 水揚支所名称を表示する
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);

            if (ValChk.IsEmpty(this.lblMizuageShishoNm.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            // 支所の切り替え
            if (Util.ToInt(this.UInfo.ShishoCd) != Util.ToInt(this.txtMizuageShishoCd.Text))
            {
                this.UInfo.ShishoCd = this.txtMizuageShishoCd.Text;
                this.UInfo.ShishoNm = this.lblMizuageShishoNm.Text;
            }

            return true;
        }

        /// <summary>
        /// 取引区分の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool isValidTorihikiKubunCd()
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtTorihikiKubunCd.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                return false;
            }
            // 最大桁数チェック
            else if (!ValChk.IsWithinLength(this.txtTorihikiKubunCd.Text, this.txtTorihikiKubunCd.MaxLength))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }
            // 空の場合、0として処理
            else if (ValChk.IsEmpty(this.txtTorihikiKubunCd.Text))
            {
                //this.txtDay.Text = "0";
                this.txtTorihikiKubunCd.Text = "0";
                Msg.Notice("入力に誤りがあります。");
                return false;
            }
            // コードを元に名称を取得する
            else
            {
                this.lblTorihikiKubunNm.Text = this.Dba.GetName(this.UInfo, "TB_HN_F_TORIHIKI_KUBUN2", this.txtMizuageShishoCd.Text, this.txtTorihikiKubunCd.Text);
                // 存在しない区分が入力された場合(名称が空の場合)
                if (this.lblTorihikiKubunNm.Text == "")
                {
                    Msg.Notice("入力に誤りがあります。");
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// 仕入先CDの値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool isValidSiiresakiCd()
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtSiiresakiCd.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                return false;
            }
            // 最大桁数チェック
            else if (!ValChk.IsWithinLength(this.txtSiiresakiCd.Text, this.txtSiiresakiCd.MaxLength))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }
            // 空チェック
            else if (ValChk.IsEmpty(this.txtSiiresakiCd.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }
            // コードを元に名称を取得する
            else
            {
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
                dpc.SetParam("@TORIHIKISAKI_CD", SqlDbType.Decimal, 6, this.txtSiiresakiCd.Text);
                DataTable dtVI_HN_TORIHIKISAKI_JOHO = this.Dba.GetDataTableByConditionWithParams(
                    "TORIHIKISAKI_NM", "VI_HN_TORIHIKISAKI_JOHO", "KAISHA_CD = @KAISHA_CD AND TORIHIKISAKI_CD = @TORIHIKISAKI_CD AND TORIHIKISAKI_KUBUN3 = 3", dpc);

                // 存在しないコードが入力された場合
                if (dtVI_HN_TORIHIKISAKI_JOHO.Rows.Count == 0)
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }
                else
                {
                    this.lblSiiresakiNm.Text = Util.ToString(dtVI_HN_TORIHIKISAKI_JOHO.Rows[0]["TORIHIKISAKI_NM"]);
                }
            }

            return true;
        }

        /// <summary>
        /// 担当者CDの値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool isValidTantoshaCd()
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtTantoshaCd.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                return false;
            }
            // 最大桁数チェック
            else if (!ValChk.IsWithinLength(this.txtTantoshaCd.Text, this.txtTantoshaCd.MaxLength))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }
            // 空の場合、0として処理
            else if (ValChk.IsEmpty(this.txtTantoshaCd.Text))
            {
                //this.txtDay.Text = "0";
                this.txtTantoshaCd.Text = "0";
                Msg.Notice("入力に誤りがあります。");
                return false;
            }
            // コードを元に名称を取得する
            else
            {
                this.lblTantoshaNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_TANTOSHA", this.txtMizuageShishoCd.Text, this.txtTantoshaCd.Text);
                // 存在しないコードが入力された場合(名称が空の場合)
                if (this.lblTantoshaNm.Text == "")
                {
                    Msg.Notice("入力に誤りがあります。");
                    return false;
                }
                SetBumonInfo(Util.ToInt(this.txtTantoshaCd.Text));
            }

            return true;
        }

        /// <summary>
        /// 金額の設定
        /// </summary>
        /// <param name="Row"></param>
        /// <returns></returns>
        private bool CellKingkSet(int Row)
        {
            decimal cus = Util.ToDecimal(Util.ToString(dgvInputList[COL_SURYO1, Row].Value)) * Util.ToDecimal(Util.ToString(dgvInputList[COL_IRISU, Row].Value));
            decimal num = cus + Util.ToDecimal(Util.ToString(dgvInputList[COL_SURYO2, Row].Value));
            decimal tan = Util.ToDecimal(Util.ToString(dgvInputList[COL_URI_TANKA, Row].Value));
            decimal kin = num * tan;
            kin = TaxUtil.CalcFraction(kin, 0, Util.ToInt(CalcInfo.KINGAKU_HASU_SHORI));
            if (!ValChk.IsDecNumWithinLength(kin, 12, 0, true))
            {
                return false;
            }
            dgvInputList[COL_BAIKA_KINGAKU, Row].Value = Util.FormatNum(kin);
            CellTaxSet(Row);

            DataGridSum();
            return true;
        }

        /// <summary>
        /// 税情報の設定及び明細転嫁時の税額設定
        /// </summary>
        /// <param name="Row"></param>
        private void CellTaxSet(int Row)
        {
            // 更新確認時で明細転嫁時は再計算を行わない（手動調整を反映）
            if (this._lastUpdCheck && this.CalcInfo.SHOHIZEI_TENKA_HOHO == 1)
            {
                return;
            }

            int ZeiKb = 0;
            int KziKb = 0;
            decimal ZeiRt = 0;
            decimal wKingk = 0;
            decimal wZeigk = 0;
            // 税区分
            if (dgvInputList[COL_ZEI_KUBUN, Row].Value != null)
            {
                ZeiKb = Util.ToInt(dgvInputList[COL_ZEI_KUBUN, Row].Value);
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@ZEI_KUBUN", SqlDbType.Decimal, 4, ZeiKb);
                DataTable dt = this.Dba.GetDataTableByConditionWithParams(
                    "*",
                    "TB_ZM_F_ZEI_KUBUN",
                    "ZEI_KUBUN = @ZEI_KUBUN",
                    dpc);
                // 課税区分
                if (dt.Rows.Count > 0)
                {
                    KziKb = Util.ToInt(Util.ToString(dt.Rows[0]["KAZEI_KUBUN"]));
                }

                // 税率
                DateTime DENPYO_DATE = Util.ConvAdDate(this.lblGengo.Text, this.txtGengoYear.Text,
                                                       this.txtMonth.Text, this.txtDay.Text, this.Dba);
                //dpc = new DbParamCollection();
                //StringBuilder sql = new StringBuilder();
                //sql.Append("SELECT dbo.FNC_GetTaxRate( @ZEI_KUBUN, @DENPYO_DATE )");
                //dpc.SetParam("@ZEI_KUBUN", SqlDbType.Decimal, 4, ZeiKb);
                //dpc.SetParam("@DENPYO_DATE", SqlDbType.DateTime, DENPYO_DATE);
                //dt = this.Dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);
                //if (dt.Rows.Count == 0)
                //    ZeiRt = 0;
                //else
                //    ZeiRt = Util.ToDecimal(dt.Rows[0][0]);
                ZeiRt = TaxUtil.GetTaxRate(DENPYO_DATE, ZeiKb, this.Dba);

                // 軽減税率（税率が設定済の場合はそっちを優先）
                if (Util.ToString(dgvInputList[COL_ZEI_RITSU, Row].Value) != "")
                {
                    ZeiRt = Util.ToDecimal(Util.ToString(dgvInputList[COL_ZEI_RITSU, Row].Value));
                }
            }

            //if (KziKb != 0)
            if (KziKb == 1)
            {
                wKingk = Util.ToDecimal(Util.ToString(dgvInputList[COL_BAIKA_KINGAKU, Row].Value));
                if (dgvInputList[COL_SHOHIZEI, Row].Value != null)
                {
                    wZeigk = Util.ToDecimal(Util.ToString(dgvInputList[COL_SHOHIZEI, Row].Value));
                }
                if (Util.ToInt(this.CalcInfo.SHOHIZEI_TENKA_HOHO) != 1)
                    wZeigk = 0;

                // 税額の算出
                switch (Util.ToInt(this.CalcInfo.SHOHIZEI_TENKA_HOHO))
                {
                    case 1:// 明細転嫁
                        switch (Util.ToInt(this.CalcInfo.SHOHIZEI_NYURYOKU_HOHO))
                        {
                            case 2: // 税抜き
                                wZeigk = wKingk * ZeiRt / 100;
                                break;

                            case 3: // 税込み
                                wZeigk = (wKingk / (100 + ZeiRt)) * ZeiRt;
                                break;
                        }
                        break;
                }
                dgvInputList[COL_BAIKA_KINGAKU, Row].Value = Util.FormatNum(wKingk);
                dgvInputList[COL_SHOHIZEI, Row].Value = Util.FormatNum(TaxUtil.CalcFraction(wZeigk, 0, Util.ToInt(CalcInfo.SHOHIZEI_HASU_SHORI)));
                dgvInputList[COL_ZEI_RITSU, Row].Value = ZeiRt;
                dgvInputList[COL_KAZEI_KUBUN, Row].Value = KziKb;
            }
            else
            {
                dgvInputList[COL_SHOHIZEI, Row].Value = null;
                dgvInputList[COL_ZEI_RITSU, Row].Value = string.Empty;
                dgvInputList[COL_KAZEI_KUBUN, Row].Value = string.Empty;
            }
            DataGridSum();
        }

        /// <summary>
        /// 税情報の一括設定
        /// </summary>
        private void CellTaxRateSet()
        {
            int i = 0;
            while (this.dgvInputList.RowCount > i)
            {
                if (dgvInputList[COL_ZEI_KUBUN, i].Value != null && Util.ToInt(dgvInputList[COL_ZEI_KUBUN, i].Value) != 0)
                {
                    CellTaxSet(i);
                }
                i++;
            }
        }

        /// <summary>
        /// 伝票転嫁消費税の振り分け
        /// </summary>
        /// <param name="tb"></param>
        private void TaxSorting(ref List<MeisaiTbl> tb)
        {
            decimal work = 0;
            decimal workTax = 0;
            bool setFlg = false;
            int taxIdx = 0;
            List<MeisaiTbl> taxTbl = new List<MeisaiTbl>();
            int i = 0;

            tb = new List<MeisaiTbl>();
            while (this.dgvInputList.RowCount > i)
            {
                if (!ValChk.IsEmpty(Util.ToString(this.dgvInputList[COL_ZEI_KUBUN, i].Value)))
                {
                    MeisaiTbl m = new MeisaiTbl();
                    m.Kingk = Util.ToDecimal(Util.ToString(dgvInputList[COL_BAIKA_KINGAKU, i].Value));
                    m.Zeigk = Util.ToDecimal(Util.ToString(dgvInputList[COL_SHOHIZEI, i].Value));
                    m.ZeiRt = Util.ToDecimal(Util.ToString(dgvInputList[COL_ZEI_RITSU, i].Value));
                    m.Gentan = Util.ToDecimal(Util.ToString(dgvInputList[COL_GEN_TANKA, i].Value));
                    m.Genka = 0;

                    work = Util.ToDecimal(Util.ToString(dgvInputList[COL_SURYO1, i].Value)) * Util.ToDecimal(Util.ToString(dgvInputList[COL_IRISU, i].Value));
                    work = work + Util.ToDecimal(Util.ToString(dgvInputList[COL_SURYO2, i].Value));
                    if (work != 0 && m.Gentan != 0)
                    {
                        work = work + m.Gentan;
                        work = TaxUtil.CalcFraction(work, 0, Util.ToInt(CalcInfo.KINGAKU_HASU_SHORI));
                        if (Util.ToInt(this.CalcInfo.SHOHIZEI_NYURYOKU_HOHO) == 3)
                        {
                            // 税込み時は税抜き額へ
                            work = (m.Genka / (100 + m.ZeiRt) * m.ZeiRt);
                            workTax = TaxUtil.CalcFraction(work, 0, Util.ToInt(CalcInfo.SHOHIZEI_HASU_SHORI));
                            m.Genka = workTax;
                        }
                    }
                    // 伝票転嫁
                    if (Util.ToInt(this.CalcInfo.SHOHIZEI_TENKA_HOHO) == 2 &&
                        Util.ToInt(dgvInputList[COL_KAZEI_KUBUN, i].Value) == 1 &&
                        m.Kingk != 0 && m.ZeiRt != 0)
                    {
                        switch (Util.ToInt(this.CalcInfo.SHOHIZEI_NYURYOKU_HOHO))
                        {
                            case 2: // 税抜き
                                work = (m.Kingk * m.ZeiRt) / 100;
                                m.Zeigk = TaxUtil.CalcFraction(work, 0, Util.ToInt(CalcInfo.SHOHIZEI_HASU_SHORI));
                                break;

                            case 3: // 税込み
                                work = (m.Kingk / (100 + m.ZeiRt)) * m.ZeiRt;
                                m.Zeigk = TaxUtil.CalcFraction(work, 0, Util.ToInt(CalcInfo.SHOHIZEI_HASU_SHORI));
                                break;
                        }
                        // 税率毎に集計
                        setFlg = false;
                        if (taxIdx > 0)
                        {
                            for (int j = 0; j < taxTbl.Count; j++)
                            {
                                if (taxTbl[j].ZeiRt == Util.ToDecimal(Util.ToString(dgvInputList[COL_ZEI_RITSU, i].Value)))
                                {
                                    taxTbl[j].Kingk += m.Kingk;
                                    taxTbl[j].Zeigk += m.Zeigk;
                                    taxTbl[j].Row = i;
                                    setFlg = true;
                                    break;
                                }
                            }
                        }
                        if (!setFlg)
                        {
                            MeisaiTbl z = new MeisaiTbl();
                            z.Kingk = m.Kingk;
                            z.Zeigk = m.Zeigk;
                            z.ZeiRt = m.ZeiRt;
                            z.Row = i;
                            taxTbl.Add(z);
                            taxIdx += 1;
                        }
                    }
                    tb.Add(m);
                }
                i++;
            }
            // 伝票転嫁消費税の振り分け
            if (Util.ToInt(this.CalcInfo.SHOHIZEI_TENKA_HOHO) == 2 && taxIdx > 0)
            {
                for (int j = 0; j < taxTbl.Count; j++)
                {
                    work = 0;
                    switch (Util.ToInt(this.CalcInfo.SHOHIZEI_NYURYOKU_HOHO))
                    {
                        case 2: // 税抜き
                            work = (taxTbl[j].Kingk * taxTbl[j].ZeiRt) / 100;
                            break;

                        case 3: // 税込み
                            work = (taxTbl[j].Kingk / (100 + taxTbl[j].ZeiRt)) * taxTbl[j].ZeiRt;
                            break;
                    }
                    // 合計消費税額が一致しない場合最終行より減算
                    if (taxTbl[j].Zeigk != TaxUtil.CalcFraction(work, 0, Util.ToInt(CalcInfo.SHOHIZEI_HASU_SHORI)))
                    {
                        taxIdx = taxTbl[j].Row;
                        tb[taxIdx].Zeigk = tb[taxIdx].Zeigk - (taxTbl[j].Zeigk - TaxUtil.CalcFraction(work, 0, Util.ToInt(CalcInfo.SHOHIZEI_HASU_SHORI)));
                    }
                }
            }
        }

        /// <summary>
        /// 税率毎の内訳表示
        /// </summary>
        private void SetTaxInfo()
        {

            List<MeisaiTbl> taxTbl = new List<MeisaiTbl>();
            TaxSorting(ref taxTbl);

            if (taxTbl == null)
                return;

            try
            {
                // 明細のリストより税率毎に集計
                List<MeisaiTbl> sumTaxTbl = (from a in taxTbl
                                             orderby a.ZeiRt
                                             group a by new { a.ZeiRt } into g
                                             select new MeisaiTbl
                                             {
                                                 ZeiRt = g.Max(s => s.ZeiRt),
                                                 Kingk = g.Sum(s => s.Kingk),
                                                 Zeigk = g.Sum(s => s.Zeigk)
                                             }
                    ).ToList();

                string inf = string.Empty;
                for (int i = 0; i < sumTaxTbl.Count; i++)
                {
                    var l = sumTaxTbl[i].ZeiRt.ToString("#0") + "%:\\" + Util.FormatNum(sumTaxTbl[i].Kingk) + "(税 \\" + Util.FormatNum(sumTaxTbl[i].Zeigk) + ")";
                    inf += l + Environment.NewLine;
                }
                lblTaxInfo.Text = string.Empty;
                if (inf.Length != 0)
                    lblTaxInfo.Text = "消費税内訳" + Environment.NewLine + inf;
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
                Console.WriteLine(ex.Message);
            }
        }

        /// <summary>
        /// データグリッド用のテキストの全選択
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TextBoxSelectAll(object sender, EventArgs e)
        {
            TextBox textBox = sender as TextBox;
            if (textBox == null) return;

            //// 少し長めにクリックされた場合にも一応対処
            //while ((Control.MouseButtons & MouseButtons.Left) != MouseButtons.None)
            //    Application.DoEvents();

            textBox.SelectAll();
        }

        /// <summary>
        /// 担当者より部門コード設定
        /// </summary>
        /// <param name="TantoshaCd"></param>
        private void SetBumonInfo(int TantoshaCd)
        {
            CalcInfo.BUMON_CD = 0;
            if (!ValChk.IsEmpty(this.txtTantoshaCd.Text))
            {
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 6, this.UInfo.KaishaCd);
                dpc.SetParam("@TANTOSHA_CD", SqlDbType.Int, 4, TantoshaCd);
                DataTable dt = this.Dba.GetDataTableByConditionWithParams(
                    "*",
                    "TB_CM_TANTOSHA",
                    "KAISHA_CD = @KAISHA_CD AND TANTOSHA_CD = @TANTOSHA_CD",
                    dpc);
                if (dt.Rows.Count != 0)
                {
                    CalcInfo.BUMON_CD = Util.ToInt(dt.Rows[0]["BUMON_CD"]);
                }
            }
        }

        /// <summary>
        /// 単価履歴より直近の単価を取得
        /// </summary>
        /// <param name="TokuisakiCd"></param>
        /// <param name="SeikyusakiCd"></param>
        /// <param name="shohinCd"></param>
        /// <param name="Tanka"></param>
        /// <returns></returns>
        private bool GetTanka(int TokuisakiCd, int SeikyusakiCd, decimal shohinCd, ref Decimal Tanka)
        {
            DbParamCollection dpc;
            DataTable dt;
            // HAN.TB_売上単価履歴(TB_HN_URIAGE_TANKA_RIREKI)
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToInt(Util.ToString(txtMizuageShishoCd.Text)));
            dpc.SetParam("@SEIKYUSAKI_CD", SqlDbType.VarChar, 4, SeikyusakiCd);
            dpc.SetParam("@TOKUISAKI_CD", SqlDbType.VarChar, 4, TokuisakiCd);
            dpc.SetParam("@SHOHIN_CD", SqlDbType.Decimal, 13, shohinCd);
            // 取得の際は会計年度を無視しておく
            //dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            dt = this.Dba.GetDataTableByConditionWithParams(
                "*",
                "TB_HN_SHIIRE_TANKA_RIREKI",
                //"KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND SEIKYUSAKI_CD = @SEIKYUSAKI_CD AND TOKUISAKI_CD = @TOKUISAKI_CD AND SHOHIN_CD = @SHOHIN_CD AND KAIKEI_NENDO = @KAIKEI_NENDO",
                "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND SEIKYUSAKI_CD = @SEIKYUSAKI_CD AND TOKUISAKI_CD = @TOKUISAKI_CD AND SHOHIN_CD = @SHOHIN_CD",
                "SAISHU_TORIHIKI_DATE desc",
                dpc);
            if (dt.Rows.Count != 0)
            {
                Tanka = Util.ToDecimal(dt.Rows[0]["TANKA"]);
            }
            else
                return false;

            return true;
        }

        /// <summary>
        /// 消費税入力方法名の表示
        /// </summary>
        /// <param name="cat">消費税入力方法</param>
        private void SetTaxInputCategory(int cat)
        {
            string name = this.Dba.GetName(this.UInfo, "TB_ZM_F_SHOHIZEI_NYURYOKU_HOHO", this.txtMizuageShishoCd.Text, cat.ToString());
            this.lblShohizeiInputHoho2.Text = cat.ToString() + ":" + name;
        }

        /// <summary>
        /// 消費税転嫁方法名の表示
        /// </summary>
        /// <param name="cat">消費税転嫁方法</param>
        private void SetTaxShiftCategory(int cat)
        {
            string name = "";
            switch (cat)
            {
                case 1:
                    name = "明細転嫁";
                    break;
                case 2:
                    name = "伝票転嫁";
                    break;
                case 3:
                    name = "請求転嫁";
                    break;
            }
            this.lblShohizeiTenka2.Text = cat.ToString() + ":" + name;
        }

        /// <summary>
        /// 消費税計算の判定（数量と単価が入って居るか）
        /// </summary>
        /// <param name="row">対象行</param>
        /// <returns></returns>
        private bool IsCalc(int row)
        {
            // 数量が無い場合は無し
            if (ValChk.IsEmpty(dgvInputList[COL_SURYO1, row].Value) && ValChk.IsEmpty(dgvInputList[COL_SURYO2, row].Value))
            {
                return false;
            }

            // 単価も無い場合は無し
            if (ValChk.IsEmpty(dgvInputList[COL_URI_TANKA, row].Value))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// 在庫の更新処理
        /// </summary>
        /// <param name="DENPYO_BANGO">伝票番号</param>
        /// <param name="updMode">更新モード、３：削除（逆算）、以外は登録（通常）</param>
        /// <returns></returns>
        private bool ZaikoKousin(Decimal DENPYO_BANGO, int updMode)
        {
            DataTable dt = GetZaikoUpdateData(DENPYO_BANGO);
            if (dt == null || dt.Rows.Count == 0)
                return false;

            bool lastUpdate = false;
            int sgn = 1;        // 入庫
            // 削除時（仕入なので減）
            if (updMode == 3)
                sgn = -1;       // 出庫

            // 返品取引時は逆計算
            if (Util.ToInt(dt.Rows[0]["TORIHIKI_KUBUN2"]) == 2)
                sgn = (sgn == 1 ? -1 : 1);

            // 通常仕入の時のみ最終仕入情報更新
            if (updMode != 3 && Util.ToInt(dt.Rows[0]["TORIHIKI_KUBUN2"]) != 2)
            {
                lastUpdate = true;
            }

            DbParamCollection updParam;
            DbParamCollection whereParam;
            DataTable dtCheck;
            decimal Suryo1 = 0;
            decimal Suryo2 = 0;

            // 最終仕入日付、最終仕入単価は仕入の時のみ更新

            int i = 0;
            while (dt.Rows.Count > i)
            {
                // 在庫管理対象のみ？（要相談か）
                if (Util.ToInt(dt.Rows[i]["ZAIKO_KANRI_KUBUN"]) == 1)
                {
                    //在庫データ取得
                    dtCheck = GetZaiko(Util.ToDecimal(dt.Rows[i]["SHOHIN_CD"]));

                    // 取引数の設定
                    Suryo1 = Util.ToDecimal(Util.ToString(dt.Rows[i]["SURYO1"])) * sgn;
                    Suryo2 = Util.ToDecimal(Util.ToString(dt.Rows[i]["SURYO2"])) * sgn;

                    if (dtCheck.Rows.Count == 0)
                    {

                        // 数量設定
                        ZaikoSu(Util.ToInt(Util.ToString(dt.Rows[i]["IRISU"])), ref Suryo1, ref Suryo2);

                        //　新規でTB_HN_ZAIKOに追加
                        updParam = new DbParamCollection();
                        updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                        updParam.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToInt(Util.ToString(txtMizuageShishoCd.Text)));
                        updParam.SetParam("@SHOHIN_CD", SqlDbType.Decimal, 13, Util.ToDecimal(dt.Rows[i]["SHOHIN_CD"]));
                        updParam.SetParam("@SOKO_CD", SqlDbType.Decimal, 4, 0);
                        updParam.SetParam("@SURYO1", SqlDbType.Decimal, 14, Util.ToDecimal(Util.ToString(dt.Rows[i]["SURYO1"])) * sgn);
                        updParam.SetParam("@SURYO2", SqlDbType.Decimal, 14, Util.ToDecimal(Util.ToString(dt.Rows[i]["SURYO2"])) * sgn);
                        updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWTIME");

                        // 最終仕入情報の更新
                        if (lastUpdate)
                        {
                            updParam.SetParam("@SAISHU_SHIIRE_DATE", SqlDbType.DateTime, Util.ToDate(dt.Rows[i]["DENPYO_DATE"]));
                            updParam.SetParam("@SAISHU_SHIIRE_TANKA", SqlDbType.Decimal, 13, Util.ToDecimal(Util.ToString(dt.Rows[i]["URI_TANKA"])));
                        }

                        this.Dba.Insert("TB_HN_ZAIKO", updParam);
                    }
                    else
                    {
                        // 在庫テーブル分の加算
                        Suryo1 += Util.ToDecimal(Util.ToString(dtCheck.Rows[0]["SURYO1"]));
                        Suryo2 += Util.ToDecimal(Util.ToString(dtCheck.Rows[0]["SURYO2"]));

                        // 数量設定
                        ZaikoSu(Util.ToInt(Util.ToString(dt.Rows[i]["IRISU"])), ref Suryo1, ref Suryo2);

                        updParam = new DbParamCollection();
                        whereParam = new DbParamCollection();
                        updParam.SetParam("@SURYO1", SqlDbType.Decimal, 14, Suryo1);
                        updParam.SetParam("@SURYO2", SqlDbType.Decimal, 14, Suryo2);
                        updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWTIME");

                        // 最終仕入情報の更新（変更時は最終仕入日付より伝票日付が新しい場合に更新）
                        if (lastUpdate && 
                            Util.ToDate(dt.Rows[i]["DENPYO_DATE"]) > Util.ToDate(Util.ToString(dtCheck.Rows[0]["SAISHU_SHIIRE_DATE"])))
                        {
                            updParam.SetParam("@SAISHU_SHIIRE_DATE", SqlDbType.DateTime, Util.ToDate(dt.Rows[i]["DENPYO_DATE"]));
                            updParam.SetParam("@SAISHU_SHIIRE_TANKA", SqlDbType.Decimal, 13, Util.ToDecimal(Util.ToString(dt.Rows[i]["URI_TANKA"])));
                        }

                        whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                        whereParam.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToInt(Util.ToString(txtMizuageShishoCd.Text)));
                        whereParam.SetParam("@SHOHIN_CD", SqlDbType.Decimal, 15, Util.ToDecimal(dt.Rows[i]["SHOHIN_CD"]));
                        whereParam.SetParam("@SOKO_CD", SqlDbType.Decimal, 4, 0);

                        this.Dba.Update("TB_HN_ZAIKO",
                                        updParam,
                                        "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND SHOHIN_CD = @SHOHIN_CD AND SOKO_CD = @SOKO_CD",
                                        whereParam
                                        );
                    }
                }

                i++;
            }

            return true;
        }

        /// <summary>
        /// 数量の再計算
        /// </summary>
        /// <param name="Irisu">入数</param>
        /// <param name="Suryo1">ケース数</param>
        /// <param name="Suryo2">バラ数</param>
        private void ZaikoSu(int Irisu, ref Decimal Suryo1, ref Decimal Suryo2)
        {
            decimal w = 0;
            if (Irisu == 0)
            {
                w = Suryo1 + Suryo2;
                Suryo1 = 0;
                Suryo2 = w;
            }
            else
            {
                w = Suryo2 + (Suryo1 * Irisu);
                decimal s = w / Irisu;
                Suryo1 = TaxUtil.CalcFraction(s, 0, 1);
                Suryo2 = w - (Suryo1 * Irisu);
            }
        }

        /// <summary>
        /// 在庫更新の元となる取引明細の抽出
        /// </summary>
        /// <param name="DENPYO_BANGO">伝票番号</param>
        /// <returns></returns>
        private DataTable GetZaikoUpdateData(Decimal DENPYO_BANGO)
        {

            StringBuilder sql;
            sql = new StringBuilder();
            sql.Append("SELECT");
            sql.Append(" A.KAISHA_CD,");
            sql.Append(" A.SHISHO_CD,");
            sql.Append(" A.DENPYO_KUBUN,");
            sql.Append(" A.KAIKEI_NENDO,");
            sql.Append(" A.DENPYO_BANGO,");
            sql.Append(" A.SHOHIN_CD,");
            //sql.Append(" A.TENPO_CD,");
            sql.Append(" A.TORIHIKI_KUBUN2,");
            sql.Append(" 0 AS SURYO1,");
            sql.Append(" SUM( ((A.SURYO1 * A.IRISU) + A.SURYO2) ) AS SURYO2,");
            sql.Append(" MIN( A.DENPYO_DATE ) AS DENPYO_DATE,");
            sql.Append(" MIN( B.IRISU ) AS IRISU,");
            sql.Append(" MIN( B.ZAIKO_KANRI_KUBUN ) AS ZAIKO_KANRI_KUBUN,");
            sql.Append(" ( SELECT X.URI_TANKA ");
            sql.Append("   FROM TB_HN_TORIHIKI_MEISAI AS X");
            sql.Append("   WHERE X.KAISHA_CD    = A.KAISHA_CD ");
            sql.Append("     AND X.SHISHO_CD    = A.SHISHO_CD ");
            sql.Append("     AND X.DENPYO_KUBUN = A.DENPYO_KUBUN ");
            sql.Append("     AND X.KAIKEI_NENDO = A.KAIKEI_NENDO ");
            sql.Append("     AND X.DENPYO_BANGO = A.DENPYO_BANGO ");
            sql.Append("     AND X.GYO_BANGO = ( SELECT MAX( Y.GYO_BANGO ) ");
            sql.Append("                         FROM TB_HN_TORIHIKI_MEISAI AS Y ");
            sql.Append("                         WHERE Y.KAISHA_CD = A.KAISHA_CD ");
            sql.Append("                           AND Y.SHISHO_CD = A.SHISHO_CD ");
            sql.Append("                           AND Y.DENPYO_KUBUN = A.DENPYO_KUBUN ");
            sql.Append("                           AND Y.KAIKEI_NENDO = A.KAIKEI_NENDO ");
            sql.Append("                           AND Y.DENPYO_BANGO = A.DENPYO_BANGO ");
            sql.Append("                           AND Y.SHOHIN_CD = A.SHOHIN_CD ");
            //sql.Append("                           AND Y.TENPO_CD = A.TENPO_CD ");
            sql.Append("                           AND Y.URI_TANKA > 0 ");
            sql.Append("                         GROUP BY ");
            sql.Append("                           Y.KAISHA_CD,");
            sql.Append("                           Y.SHISHO_CD,");
            sql.Append("                           Y.DENPYO_KUBUN,");
            sql.Append("                           Y.KAIKEI_NENDO,");
            sql.Append("                           Y.DENPYO_BANGO,");
            sql.Append("                           Y.SHOHIN_CD)");
            //sql.Append("                           Y.TENPO_CD) ");
            sql.Append(" ) AS URI_TANKA ");
            sql.Append("FROM ");
            sql.Append("    VI_HN_TORIHIKI_MEISAI AS A ");
            sql.Append("LEFT OUTER JOIN ");
            sql.Append("    TB_HN_SHOHIN  AS B ");
            sql.Append("     ON A.KAISHA_CD = B.KAISHA_CD ");
            sql.Append("    AND A.SHISHO_CD = B.SHISHO_CD ");
            sql.Append("    AND A.SHOHIN_CD = B.SHOHIN_CD ");
            sql.Append("WHERE ");
            sql.Append("    A.KAISHA_CD      = @KAISHA_CD AND");
            sql.Append("    A.SHISHO_CD      = @SHISHO_CD AND");
            sql.Append("    A.DENPYO_KUBUN   = @DENPYO_KUBUN AND");
            sql.Append("    A.KAIKEI_NENDO   = @KAIKEI_NENDO AND");
            sql.Append("    A.DENPYO_BANGO   = @DENPYO_BANGO ");
            sql.Append("GROUP BY ");
            sql.Append(" A.KAISHA_CD,");
            sql.Append(" A.SHISHO_CD,");
            sql.Append(" A.DENPYO_KUBUN,");
            sql.Append(" A.KAIKEI_NENDO,");
            sql.Append(" A.DENPYO_BANGO,");
            sql.Append(" A.SHOHIN_CD,");
            //sql.Append(" A.TENPO_CD,");
            sql.Append(" A.TORIHIKI_KUBUN2 ");
            DbParamCollection dpc;
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToInt(Util.ToString(txtMizuageShishoCd.Text)));
            dpc.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 4, 2);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            dpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 8, DENPYO_BANGO);

            DataTable dt = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);

            return dt;
        }

        /// <summary>
        /// 指定商品の在庫データ抽出
        /// </summary>
        /// <param name="shohinCd"></param>
        /// <returns></returns>
        private DataTable GetZaiko(decimal shohinCd)
        {
            DbParamCollection dpc;
            DataTable dt;
            // HAN.TB_在庫(TB_HN_ZAIKO)
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, Util.ToInt(Util.ToString(txtMizuageShishoCd.Text)));
            dpc.SetParam("@SHOHIN_CD", SqlDbType.Decimal, 13, shohinCd);
            dpc.SetParam("@SOKO_CD", SqlDbType.Decimal, 4, 0);
            dt = this.Dba.GetDataTableByConditionWithParams(
                "*",
                "TB_HN_ZAIKO",
                "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND SHOHIN_CD = @SHOHIN_CD AND SOKO_CD = @SOKO_CD",
                dpc);

            return dt;
        }

        /// <summary>
        /// 税率変更時の確認処理
        /// </summary>
        /// <param name="baseDate"></param>
        /// <param name="zeiKbn"></param>
        /// <param name="zeiritsu"></param>
        /// <param name="taxRate"></param>
        /// <returns></returns>
        private DialogResult ConfPastZeiKbn(DateTime baseDate, string zeiKbn, ref decimal zeiritsu, decimal taxRate)
        {
            zeiritsu = 0;

            // 概要：日付を元に、現在の税率と異なる税率を持つ税区分が選択されていれば確認メッセージを出す

            // 税区分を元にTB_ZM_F_ZEI_KUBUNから設定値を取得
            // ※万が一取得できなければNGを返す
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@ZEI_KUBUN", SqlDbType.Decimal, 2, zeiKbn);
            DataTable dtZeiKbn = this.Dba.GetDataTableByConditionWithParams(
                "*", "TB_ZM_F_ZEI_KUBUN", "ZEI_KUBUN = @ZEI_KUBUN", dpc);

            if (dtZeiKbn.Rows.Count == 0) return DialogResult.No;

            // 非課税であればダイアログを出さない
            //if (Util.ToInt(dtZeiKbn.Rows[0]["KAZEI_KUBUN"]) == 0) return DialogResult.OK;
            if (Util.ToInt(dtZeiKbn.Rows[0]["KAZEI_KUBUN"]) != 1) return DialogResult.OK;

            // 適用開始日が基準日より小さいレコードを摘要開始日の降順で取得し、
            // その1件目の新消費税を保持
            // 税区分から取得した税率と消費税情報テーブルから取得した税率を比較し、
            // 異なっていれば確認メッセージを表示
            decimal taxRateBase = GetZeiritsu(baseDate);
            //decimal taxRateJudge = Util.ToDecimal(dtZeiKbn.Rows[0]["ZEI_RITSU"]);
            dpc = new DbParamCollection();
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT dbo.FNC_GetTaxRate( @ZEI_KUBUN, @DENPYO_DATE )");
            dpc.SetParam("@ZEI_KUBUN", SqlDbType.Decimal, 4, zeiKbn);
            dpc.SetParam("@DENPYO_DATE", SqlDbType.DateTime, baseDate);
            DataTable dt = this.Dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);
            decimal taxRateJudge = 0;
            if (dt.Rows.Count == 0)
                taxRateJudge = 0;
            else
                taxRateJudge = Util.ToDecimal(dt.Rows[0][0]);// 新税率から取得
            // 税率指定時はそちらを優先
            if (taxRate != -1) taxRateJudge = taxRate;

            if (taxRateBase != taxRateJudge)
            {
                DialogResult result = Msg.ConfOKCancel("基準日時点の税率と異なる税率を持つ税区分が選択されています。"
                    + Environment.NewLine + "処理を続行してよろしいですか？");

                if (result == DialogResult.OK) zeiritsu = taxRateJudge;

                return result;
            }
            else
            {
                zeiritsu = taxRateJudge;
                return DialogResult.OK;
            }
        }
        private new DialogResult ConfPastZeiKbn(DateTime baseDate, string zeiKbn, ref decimal zeiritsu)
        {
            return ConfPastZeiKbn(baseDate, zeiKbn, ref zeiritsu, -1);
        }

        private DataRow GetPersonInfo(string code)
        {
            DataRow r = null;
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@TANTOSHA_CD", SqlDbType.Decimal, 4, Util.ToDecimal(code));
            DataTable dt = this.Dba.GetDataTableByConditionWithParams(
                "*",
                "TB_CM_TANTOSHA",
                "KAISHA_CD = @KAISHA_CD AND TANTOSHA_CD = @TANTOSHA_CD ",
                dpc);
            if (dt.Rows.Count != 0)
            {
                r = dt.Rows[0];
            }
            return r;
        }
        #endregion

    }
}
