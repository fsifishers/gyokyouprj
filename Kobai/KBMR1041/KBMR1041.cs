﻿using System;
using System.ComponentModel;
using System.Data;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

using GrapeCity.ActiveReports;

using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.kb.kbmr1041
{
    /// <summary>
    /// 商品売上順位表(KBMR1041)
    /// </summary>
    public partial class KBMR1041 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// 資材返品
        /// </summary>
        private const string KAKE_HENPIN = "2";

        /// <summary>
        /// 印刷ワーク更新用列数
        /// </summary>
        private const int prtCols = 12;
        #endregion

        #region プロパティ
        /// <summary>
        /// 画面上最後となるフォーカスのEnterボタン押下時処理用変数
        /// </summary>
        private bool _dtFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._dtFlg;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KBMR1041()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 水揚支所
#if DEBUG
            this.txtMizuageShishoCd.Text = "1";
#else
            this.txtMizuageShishoCd.Text = Uinfo.shishoCd;
#endif
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
            this.txtMizuageShishoCd.Enabled = (this.txtMizuageShishoCd.Text == "1") ? true : false;

            // 現在の年号を取得する
            string[] jpDate = Util.ConvJpDate(DateTime.Now, this.Dba);
            // 取得された場合、年号をラベルに反映する
            this.lblEra.Text = Util.ToString(jpDate[0]);
            this.txtYear.Text = jpDate[2];
            this.txtMonth.Text = jpDate[3];
            // 表示するにチェック
            rdoDateview1.Checked = true;

            // 初期フォーカス
            this.txtYear.Focus();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 日付にフォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd":
                case "txtYear":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;
            String[] result;

            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd": // 水揚支所
                    // アセンブリのロード
                    asm = Assembly.LoadFrom("CMCM2031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2031.CMCM2031");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.InData = this.txtMizuageShishoCd.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.txtMizuageShishoCd.Text = result[0];
                                this.lblMizuageShishoNm.Text = result[1];
                            }
                        }
                    }
                    break;

                case "txtYear":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom("CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblEra.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (string[])frm.OutData;
                                this.lblEra.Text = result[1];

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblEra.Text,
                                        this.txtYear.Text,
                                        this.txtMonth.Text,
                                        "1",
                                        this.Dba);
                                this.lblEra.Text = arrJpDate[0];
                                this.txtYear.Text = arrJpDate[2];
                                this.txtMonth.Text = arrJpDate[3];
                            }
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }
            if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
            {
                // プレビュー処理
                DoPrint(true);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfYesNo("実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }

        /// <summary>
        /// F6キー押下時処理
        /// PDF出力
        /// </summary>
        public override void PressF6()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("PDF出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, true);
            }
        }

        /// <summary>
        /// F7キー押下時処理
        /// EXCEL出力
        /// </summary>
        public override void PressF7()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("EXCEL出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, false, true);
            }
        }

        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            // 設定画面の起動
            // MEMO:原則としてここで渡す帳票IDの設定はReport.csvに保持していることが前提ですが、
            // 保持していない場合は、設定画面での保存(F6)時に新規に設定が保持されます。
            PrintSettingForm psForm = new PrintSettingForm(new string[1] { "KBMR1041R" });
            psForm.ShowDialog();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 水揚支所入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMizuageShishoCd_Validating(object sender, CancelEventArgs e)
        {
            if (!this.IsValidMizuageShishoCd())
            {
                e.Cancel = true;

                this.lblMizuageShishoNm.Text = "";
                this.txtMizuageShishoCd.SelectAll();
                this.txtMizuageShishoCd.Focus();
            }
        }

        /// <summary>
        /// 年の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtYear_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidYear())
            {
                e.Cancel = true;
                this.txtYear.SelectAll();
            }
        }
        /// <summary>
        /// 月の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMonth_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidMonth())
            {
                e.Cancel = true;
                this.txtMonth.SelectAll();
            }
        }
        /// <summary>
        /// 売上順位範囲指定コード(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtUriRankFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidUriRankFr())
            {
                e.Cancel = true;
                this.txtUriRankFr.SelectAll();
            }
        }

        /// <summary>
        /// 売上順位範囲指定コード(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtUriRankTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidUriRankTo())
            {
                e.Cancel = true;
                this.txtUriRankTo.SelectAll();

                // Enter処理を無効化
                this._dtFlg = false;
            }
            else
            {
                // Enter処理を有効化
                this._dtFlg = true;
            }
        }

        private void txtUriRankTo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.Flg)
            {
                // Enter処理を無効化
                this._dtFlg = false;

                // 全項目を再度入力値チェック
                if (!ValidateAll())
                {
                    // エラーありの場合ここで処理終了
                    return;
                }

                if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
                {
                    // ﾌﾟﾚﾋﾞｭｰ処理
                    DoPrint(true);
                }
                else
                {
                    this.txtUriRankTo.Focus();
                }
            }

        }

        #endregion

        #region privateメソッド
        /// <summary>
        /// 水揚支所の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool IsValidMizuageShishoCd()
        {
            // 空入力の場合
            if (ValChk.IsEmpty(this.txtMizuageShishoCd.Text))
            {
                // 水揚支所名称を表示する
                this.txtMizuageShishoCd.Text = "0";
                this.lblMizuageShishoNm.Text = "全て";
                return true;
            }

            // 最大桁数チェック
            if (!ValChk.IsWithinLength(this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.MaxLength))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            // 数値チェック
            if (!ValChk.IsNumber(this.txtMizuageShishoCd.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                return false;
            }

            // 0入力の場合
            if (Equals(this.txtMizuageShishoCd.Text, "0"))
            {
                // 水揚支所名称を表示する
                this.lblMizuageShishoNm.Text = "全て";
                return true;
            }

            // 水揚支所名称を表示する
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);

            if (ValChk.IsEmpty(this.lblMizuageShishoNm.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 年の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidYear()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtYear.Text))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }
            // 空の場合、0年として処理
            if (ValChk.IsEmpty(this.txtYear.Text))
            {
                this.txtYear.Text = "0";
            }

            return true;
        }

        /// <summary>
        /// 月(自)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidMonth()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtMonth.Text))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }
            // 空の場合、1月として処理
            if (ValChk.IsEmpty(this.txtMonth.Text))
            {
                this.txtMonth.Text = "1";
            }
            else
            {
                // 12を超える月が入力された場合、12月として処理
                if (Util.ToInt(this.txtMonth.Text) > 12)
                {
                    this.txtMonth.Text = "12";
                }
                // 1より小さい月が入力された場合、1月として処理
                else if (Util.ToInt(this.txtMonth.Text) < 1)
                {
                    this.txtMonth.Text = "1";
                }
            }

            return true;
        }

        /// <summary>
        /// 売上順位(自)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidUriRankFr()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtUriRankFr.Text))
            {
                Msg.Error("売上順位は数値のみで入力してください。");
                return false;
            }
            return true;
        }

        /// <summary>
        /// 売上順位(至)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidUriRankTo()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtUriRankTo.Text))
            {
                Msg.Error("売上順位は数値のみで入力してください。");
                return false;
            }
            return true;
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpDate(string[] arrJpDate)
        {
            this.lblEra.Text = arrJpDate[0];
            this.txtYear.Text = arrJpDate[2];
            this.txtMonth.Text = arrJpDate[3];
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 年(自)のチェック
            if (!IsValidYear())
            {
                this.txtYear.Focus();
                this.txtYear.SelectAll();
                return false;
            }
            // 月(自)のチェック
            if (!IsValidMonth())
            {
                this.txtMonth.Focus();
                this.txtMonth.SelectAll();
                return false;
            }

            // 売上順位(自)の入力チェック
            if (!IsValidUriRankFr())
            {
                this.txtUriRankFr.Focus();
                this.txtUriRankFr.SelectAll();
                return false;
            }
            // 売上順位(至)の入力チェック
            if (!IsValidUriRankTo())
            {
                this.txtUriRankTo.Focus();
                this.txtUriRankTo.SelectAll();
                return false;
            }

            return true;
        }

        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview, bool isPdf = false, bool isExcel = false, bool isCsv = false)
        {
            bool dataFlag;
            try
            {
                // 帳票出力用にワークテーブルにデータを作成
                dataFlag = MakeWkData();

                // 帳票出力
                if (dataFlag)
                {
                    // 取得列の定義
                    StringBuilder cols = Util.ColsArray(prtCols, "");

                    // バインドパラメータの設定
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);

                    // データの取得
                    DataTable dtOutput = this.Dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_HN_TBL", "GUID = @GUID", "SORT ASC", dpc);

                    // 帳票オブジェクトをインスタンス化
                    KBMR1041R rpt = new KBMR1041R(dtOutput);
                    rpt.Document.Printer.DocumentName = this.lblTitle.Text;
                    rpt.Document.Name = this.lblTitle.Text;

                    if (isExcel)
                    {
                        GrapeCity.ActiveReports.Export.Excel.Section.XlsExport xlsExport1 = new GrapeCity.ActiveReports.Export.Excel.Section.XlsExport();
                        //SetExcelSetting(xlsExport1);
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Kob, rpt.Document.Name, 2);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            xlsExport1.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("EXCEL出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPdf)
                    {
                        GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport p = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Kob, rpt.Document.Name, 1);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            p.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("PDF出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPreview)
                    {
                        // 帳票オブジェクトをインスタンス化
                        PreviewForm pFrm = new PreviewForm(rpt, this.UnqId);
                        // プレビュー画面表示
                        pFrm.WindowState = FormWindowState.Maximized;
                        pFrm.Show();
                    }
                    else
                    {
                        // 直接印刷
                        rpt.Run(false);
                        rpt.Document.Print(true, true, false);
                    }
                }
            }
            finally
            {
                this.Dba.Rollback();
            }
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData()
        {

            DbParamCollection dpc = new DbParamCollection();
            // 日付を西暦にして取得
            DateTime tmpDateFr = Util.ConvAdDate(this.lblEra.Text, this.txtYear.Text,
                    this.txtMonth.Text, "1", this.Dba);

            // 月の最終日を取得
            int year = int.Parse(tmpDateFr.Date.ToString("yyyy"));
            int month = int.Parse(tmpDateFr.Date.ToString("MM"));
            string mlast = DateTime.DaysInMonth(year, month).ToString();
            DateTime tmpDateTo = Util.ConvAdDate(this.lblEra.Text, this.txtYear.Text,
                    this.txtMonth.Text, mlast, this.Dba);

            // 日付を和暦で保持
            string[] tmpjpDate = Util.ConvJpDate(tmpDateFr, this.Dba);

            int i; // ループ用カウント変数
            int rank; // 売上順位
            // 売上順位範囲設定
            int cRankFr;
            int cRankTo;
            if (Util.ToString(txtUriRankFr.Text) != "")
            {
                cRankFr = Util.ToInt(txtUriRankFr.Text);
            }
            else
            {
                cRankFr = 1;
            }
            if (Util.ToString(txtUriRankTo.Text) != "")
            {
                cRankTo = Util.ToInt(txtUriRankTo.Text);
            }
            else
            {
                cRankTo = 99999;
            }
            string shishoCd = this.txtMizuageShishoCd.Text;

            // 検索する日付をセット
            dpc.SetParam("@DATE_FR", SqlDbType.VarChar, 10, tmpDateFr.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@DATE_TO", SqlDbType.VarChar, 10, tmpDateTo.Date.ToString("yyyy/MM/dd"));

            // han.VI_取引明細(VI_HN_TORIHIKI_MEISAI)
            // の検索日付に発生しているデータを取得
            StringBuilder Sql = new StringBuilder();
            Sql.Append(" SELECT");
            Sql.Append("     KAISHA_CD,");
            Sql.Append("     MONTH(DENPYO_DATE) AS SHUKEI_TSUKI,");
            Sql.Append("     SHOHIN_CD,");
            Sql.Append("     MIN(SHOHIN_NM) AS SHOHIN_NM,");
            //Sql.Append("     SUM(SURYO2) AS SURYO2,");
            Sql.Append("     SUM((ISNULL(SURYO1, 0) * ISNULL(IRISU, 0)) + SURYO2) AS SURYO2,");
            Sql.Append("     MAX(URI_TANKA) AS URI_TANKA,");
            Sql.Append("     TORIHIKI_KUBUN1,");
            Sql.Append("     TORIHIKI_KUBUN2,");
            Sql.Append("     SUM(ZEINUKI_KINGAKU) AS ZEINUKI_KINGAKU,");
            Sql.Append("     SUM(SHOHIZEI) AS SHOHIZEI,");
            //Sql.Append("     CASE WHEN TORIHIKI_KUBUN2 = 2 THEN SUM(ZEINUKI_KINGAKU + SHOHIZEI)*-1  ELSE SUM(ZEINUKI_KINGAKU + SHOHIZEI) END AS KINGAKU_KEI,");
            //Sql.Append("     SHOHIZEI_TENKA_HOHO,");
            //Sql.Append("     DENPYO_KUBUN");
            Sql.Append("     CASE WHEN TORIHIKI_KUBUN2 = 2 THEN SUM(ZEINUKI_KINGAKU + SHOHIZEI)*-1  ELSE SUM(ZEINUKI_KINGAKU + SHOHIZEI) END AS KINGAKU_KEI ");
            Sql.Append(" FROM");
            Sql.Append("     VI_HN_TORIHIKI_MEISAI");
            Sql.Append(" WHERE");
            Sql.Append("     KAISHA_CD   = @KAISHA_CD AND");
            Sql.Append("     SHISHO_CD   = @SHISHO_CD AND");
            Sql.Append("     DENPYO_KUBUN   = 1 AND");
            Sql.Append("     DENPYO_DATE BETWEEN @DATE_FR AND @DATE_TO");
            Sql.Append(" GROUP BY");
            Sql.Append("     KAISHA_CD,");
            Sql.Append("     SHISHO_CD,");
            Sql.Append("     TORIHIKI_KUBUN1,");
            Sql.Append("     TORIHIKI_KUBUN2,");
            Sql.Append("     SHOHIN_CD,");
            //Sql.Append("     MONTH(DENPYO_DATE),");
            //Sql.Append("     SHOHIZEI_TENKA_HOHO,");
            //Sql.Append("     DENPYO_KUBUN");
            Sql.Append("     MONTH(DENPYO_DATE) ");
            Sql.Append(" ORDER BY");
            Sql.Append("     KINGAKU_KEI desc");

            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);

            DataTable dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            if (dtMainLoop.Rows.Count == 0)
            {
                Msg.Info("該当データがありません。");
                return false;
            }
            else
            {
                dpc = new DbParamCollection();

                i = 1;
                rank = 1;

                #region 印刷ワークテーブルに登録
                foreach (DataRow dr in dtMainLoop.Rows)
                {
                    if (cRankFr <= rank && cRankTo >= rank)
                    {
                        Sql = new StringBuilder();
                        dpc = new DbParamCollection();
                        Sql.Append("INSERT INTO PR_HN_TBL(");
                        Sql.Append("  GUID");
                        Sql.Append(" ,SORT");
                        Sql.Append(" ,ITEM01");
                        Sql.Append(" ,ITEM02");
                        Sql.Append(" ,ITEM03");
                        Sql.Append(" ,ITEM04");
                        Sql.Append(" ,ITEM05");
                        Sql.Append(" ,ITEM06");
                        Sql.Append(" ,ITEM07");
                        Sql.Append(" ,ITEM08");
                        Sql.Append(" ,ITEM09");
                        Sql.Append(" ,ITEM10");
                        Sql.Append(" ,ITEM11");
                        Sql.Append(") ");
                        Sql.Append("VALUES(");
                        Sql.Append("  @GUID");
                        Sql.Append(" ,@SORT");
                        Sql.Append(" ,@ITEM01");
                        Sql.Append(" ,@ITEM02");
                        Sql.Append(" ,@ITEM03");
                        Sql.Append(" ,@ITEM04");
                        Sql.Append(" ,@ITEM05");
                        Sql.Append(" ,@ITEM06");
                        Sql.Append(" ,@ITEM07");
                        Sql.Append(" ,@ITEM08");
                        Sql.Append(" ,@ITEM09");
                        Sql.Append(" ,@ITEM10");
                        Sql.Append(" ,@ITEM11");
                        Sql.Append(") ");

                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                        dpc.SetParam("@SORT", SqlDbType.VarChar, 4, i);
                        // ページヘッダーデータを設定
                        dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);
                        dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, string.Format("{0}{1}年{2}月分", tmpjpDate[0], tmpjpDate[2], tmpjpDate[3]));
                        dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, "");

                        // 返品の場合は-を付ける
                        if (Util.ToString(dr["TORIHIKI_KUBUN2"]) == KAKE_HENPIN)
                        {
                            dr["SURYO2"] = "-" + Util.ToString(dr["SURYO2"]);
                            dr["ZEINUKI_KINGAKU"] = "-" + Util.ToString(dr["ZEINUKI_KINGAKU"]);
                            dr["SHOHIZEI"] = "-" + Util.ToString(dr["SHOHIZEI"]);
                        }
                        // データを設定
                        //dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, rank);
                        //dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dr["SHOHIN_CD"]);
                        //dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, dr["SHOHIN_NM"]);
                        //dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, Util.FormatNum(dr["SURYO2"]));
                        //dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, Util.FormatNum(dr["URI_TANKA"]));
                        //dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, Util.FormatNum(dr["ZEINUKI_KINGAKU"]));
                        //dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, Util.FormatNum(dr["SHOHIZEI"]));
                        //dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, Util.FormatNum(dr["KINGAKU_KEI"]));
                        dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, rank);
                        dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, dr["SHOHIN_CD"]);
                        dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, dr["SHOHIN_NM"]);
                        dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, dr["SURYO2"]);
                        dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, Util.FormatNum(dr["URI_TANKA"]));
                        dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, Util.FormatNum(dr["ZEINUKI_KINGAKU"]));
                        dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, Util.FormatNum(dr["SHOHIZEI"]));
                        dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, Util.FormatNum(dr["KINGAKU_KEI"]));

                        this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                        i++;
                    }
                    rank++;
                }
                #endregion
            }

            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
                "SORT",
                "PR_HN_TBL",
                "GUID = @GUID",
                dpc);

            bool dataFlag;
            if (tmpdtPR_HN_TBL.Rows.Count > 0)
            {
                dataFlag = true;
            }
            else
            {
                Msg.Info("該当データがありません。");
                dataFlag = false;
            }

            return dataFlag;
        }
        #endregion

        private void gbxDateview_Enter(object sender, EventArgs e)
        {

        }
    }
}
