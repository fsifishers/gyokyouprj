﻿namespace jp.co.fsi.kb.kbmr1041
{
    partial class KBMR1041
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtUriRankFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblCodeBet = new System.Windows.Forms.Label();
            this.txtUriRankTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblEra = new System.Windows.Forms.Label();
            this.txtYear = new jp.co.fsi.common.controls.FsiTextBox();
            this.gbxSearchRangeDate = new System.Windows.Forms.GroupBox();
            this.lblMonth = new System.Windows.Forms.Label();
            this.lblYear = new System.Windows.Forms.Label();
            this.txtMonth = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblEraBack = new System.Windows.Forms.Label();
            this.gbxRankingRange = new System.Windows.Forms.GroupBox();
            this.gbxDateview = new System.Windows.Forms.GroupBox();
            this.rdoDateview2 = new System.Windows.Forms.RadioButton();
            this.rdoDateview1 = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMizuageShishoNm = new System.Windows.Forms.Label();
            this.lblMizuageShisho = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.gbxSearchRangeDate.SuspendLayout();
            this.gbxRankingRange.SuspendLayout();
            this.gbxDateview.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Text = "商品売上順位表";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.pnlDebug.Size = new System.Drawing.Size(847, 100);
            // 
            // txtUriRankFr
            // 
            this.txtUriRankFr.AutoSizeFromLength = false;
            this.txtUriRankFr.DisplayLength = null;
            this.txtUriRankFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtUriRankFr.Location = new System.Drawing.Point(19, 22);
            this.txtUriRankFr.MaxLength = 5;
            this.txtUriRankFr.Name = "txtUriRankFr";
            this.txtUriRankFr.Size = new System.Drawing.Size(50, 20);
            this.txtUriRankFr.TabIndex = 0;
            this.txtUriRankFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtUriRankFr_Validating);
            // 
            // lblCodeBet
            // 
            this.lblCodeBet.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblCodeBet.Location = new System.Drawing.Point(83, 22);
            this.lblCodeBet.Name = "lblCodeBet";
            this.lblCodeBet.Size = new System.Drawing.Size(18, 20);
            this.lblCodeBet.TabIndex = 1;
            this.lblCodeBet.Text = "～";
            this.lblCodeBet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtUriRankTo
            // 
            this.txtUriRankTo.AutoSizeFromLength = false;
            this.txtUriRankTo.DisplayLength = null;
            this.txtUriRankTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtUriRankTo.Location = new System.Drawing.Point(118, 22);
            this.txtUriRankTo.MaxLength = 5;
            this.txtUriRankTo.Name = "txtUriRankTo";
            this.txtUriRankTo.Size = new System.Drawing.Size(50, 20);
            this.txtUriRankTo.TabIndex = 2;
            this.txtUriRankTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtUriRankTo_KeyDown);
            this.txtUriRankTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtUriRankTo_Validating);
            // 
            // lblEra
            // 
            this.lblEra.BackColor = System.Drawing.Color.Silver;
            this.lblEra.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblEra.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblEra.Location = new System.Drawing.Point(33, 21);
            this.lblEra.Name = "lblEra";
            this.lblEra.Size = new System.Drawing.Size(40, 20);
            this.lblEra.TabIndex = 0;
            this.lblEra.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtYear
            // 
            this.txtYear.AutoSizeFromLength = false;
            this.txtYear.DisplayLength = null;
            this.txtYear.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtYear.Location = new System.Drawing.Point(78, 21);
            this.txtYear.MaxLength = 2;
            this.txtYear.Name = "txtYear";
            this.txtYear.Size = new System.Drawing.Size(40, 20);
            this.txtYear.TabIndex = 1;
            this.txtYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtYear.Validating += new System.ComponentModel.CancelEventHandler(this.txtYear_Validating);
            // 
            // gbxSearchRangeDate
            // 
            this.gbxSearchRangeDate.Controls.Add(this.lblMonth);
            this.gbxSearchRangeDate.Controls.Add(this.lblYear);
            this.gbxSearchRangeDate.Controls.Add(this.txtMonth);
            this.gbxSearchRangeDate.Controls.Add(this.txtYear);
            this.gbxSearchRangeDate.Controls.Add(this.lblEra);
            this.gbxSearchRangeDate.Controls.Add(this.lblEraBack);
            this.gbxSearchRangeDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.gbxSearchRangeDate.ForeColor = System.Drawing.SystemColors.ControlText;
            this.gbxSearchRangeDate.Location = new System.Drawing.Point(12, 148);
            this.gbxSearchRangeDate.Name = "gbxSearchRangeDate";
            this.gbxSearchRangeDate.Size = new System.Drawing.Size(246, 59);
            this.gbxSearchRangeDate.TabIndex = 1;
            this.gbxSearchRangeDate.TabStop = false;
            this.gbxSearchRangeDate.Text = "日付範囲";
            // 
            // lblMonth
            // 
            this.lblMonth.AutoSize = true;
            this.lblMonth.BackColor = System.Drawing.Color.Silver;
            this.lblMonth.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMonth.Location = new System.Drawing.Point(190, 25);
            this.lblMonth.Name = "lblMonth";
            this.lblMonth.Size = new System.Drawing.Size(21, 13);
            this.lblMonth.TabIndex = 4;
            this.lblMonth.Text = "月";
            // 
            // lblYear
            // 
            this.lblYear.AutoSize = true;
            this.lblYear.BackColor = System.Drawing.Color.Silver;
            this.lblYear.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblYear.Location = new System.Drawing.Point(123, 25);
            this.lblYear.Name = "lblYear";
            this.lblYear.Size = new System.Drawing.Size(21, 13);
            this.lblYear.TabIndex = 2;
            this.lblYear.Text = "年";
            // 
            // txtMonth
            // 
            this.txtMonth.AutoSizeFromLength = false;
            this.txtMonth.DisplayLength = null;
            this.txtMonth.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMonth.Location = new System.Drawing.Point(145, 21);
            this.txtMonth.MaxLength = 2;
            this.txtMonth.Name = "txtMonth";
            this.txtMonth.Size = new System.Drawing.Size(40, 20);
            this.txtMonth.TabIndex = 3;
            this.txtMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonth.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonth_Validating);
            // 
            // lblEraBack
            // 
            this.lblEraBack.BackColor = System.Drawing.Color.Silver;
            this.lblEraBack.Location = new System.Drawing.Point(30, 19);
            this.lblEraBack.Name = "lblEraBack";
            this.lblEraBack.Size = new System.Drawing.Size(188, 24);
            this.lblEraBack.TabIndex = 902;
            this.lblEraBack.Text = " ";
            // 
            // gbxRankingRange
            // 
            this.gbxRankingRange.Controls.Add(this.lblCodeBet);
            this.gbxRankingRange.Controls.Add(this.txtUriRankFr);
            this.gbxRankingRange.Controls.Add(this.txtUriRankTo);
            this.gbxRankingRange.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.gbxRankingRange.ForeColor = System.Drawing.SystemColors.ControlText;
            this.gbxRankingRange.Location = new System.Drawing.Point(264, 148);
            this.gbxRankingRange.Name = "gbxRankingRange";
            this.gbxRankingRange.Size = new System.Drawing.Size(186, 59);
            this.gbxRankingRange.TabIndex = 2;
            this.gbxRankingRange.TabStop = false;
            this.gbxRankingRange.Text = "売上順位範囲指定";
            // 
            // gbxDateview
            // 
            this.gbxDateview.Controls.Add(this.rdoDateview2);
            this.gbxDateview.Controls.Add(this.rdoDateview1);
            this.gbxDateview.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.gbxDateview.ForeColor = System.Drawing.Color.Black;
            this.gbxDateview.Location = new System.Drawing.Point(456, 148);
            this.gbxDateview.Name = "gbxDateview";
            this.gbxDateview.Size = new System.Drawing.Size(245, 59);
            this.gbxDateview.TabIndex = 3;
            this.gbxDateview.TabStop = false;
            this.gbxDateview.Text = "日付表示";
            this.gbxDateview.Visible = false;
            this.gbxDateview.Enter += new System.EventHandler(this.gbxDateview_Enter);
            // 
            // rdoDateview2
            // 
            this.rdoDateview2.AutoSize = true;
            this.rdoDateview2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoDateview2.Location = new System.Drawing.Point(124, 23);
            this.rdoDateview2.Name = "rdoDateview2";
            this.rdoDateview2.Size = new System.Drawing.Size(95, 17);
            this.rdoDateview2.TabIndex = 4;
            this.rdoDateview2.TabStop = true;
            this.rdoDateview2.Text = "表示しない";
            this.rdoDateview2.UseVisualStyleBackColor = true;
            // 
            // rdoDateview1
            // 
            this.rdoDateview1.AutoSize = true;
            this.rdoDateview1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoDateview1.Location = new System.Drawing.Point(25, 23);
            this.rdoDateview1.Name = "rdoDateview1";
            this.rdoDateview1.Size = new System.Drawing.Size(81, 17);
            this.rdoDateview1.TabIndex = 3;
            this.rdoDateview1.TabStop = true;
            this.rdoDateview1.Text = "表示する";
            this.rdoDateview1.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtMizuageShishoCd);
            this.groupBox1.Controls.Add(this.lblMizuageShishoNm);
            this.groupBox1.Controls.Add(this.lblMizuageShisho);
            this.groupBox1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.groupBox1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox1.Location = new System.Drawing.Point(12, 63);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(348, 66);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // txtMizuageShishoCd
            // 
            this.txtMizuageShishoCd.AutoSizeFromLength = true;
            this.txtMizuageShishoCd.DisplayLength = null;
            this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtMizuageShishoCd.Location = new System.Drawing.Point(71, 25);
            this.txtMizuageShishoCd.MaxLength = 4;
            this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
            this.txtMizuageShishoCd.Size = new System.Drawing.Size(34, 20);
            this.txtMizuageShishoCd.TabIndex = 908;
            this.txtMizuageShishoCd.TabStop = false;
            this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
            // 
            // lblMizuageShishoNm
            // 
            this.lblMizuageShishoNm.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMizuageShishoNm.Location = new System.Drawing.Point(108, 25);
            this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
            this.lblMizuageShishoNm.Size = new System.Drawing.Size(212, 20);
            this.lblMizuageShishoNm.TabIndex = 910;
            this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMizuageShisho
            // 
            this.lblMizuageShisho.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShisho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShisho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblMizuageShisho.Location = new System.Drawing.Point(29, 23);
            this.lblMizuageShisho.Name = "lblMizuageShisho";
            this.lblMizuageShisho.Size = new System.Drawing.Size(298, 25);
            this.lblMizuageShisho.TabIndex = 909;
            this.lblMizuageShisho.Text = "支所";
            this.lblMizuageShisho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // KBMR1041
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 638);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.gbxDateview);
            this.Controls.Add(this.gbxSearchRangeDate);
            this.Controls.Add(this.gbxRankingRange);
            this.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "KBMR1041";
            this.Text = "";
            this.Controls.SetChildIndex(this.gbxRankingRange, 0);
            this.Controls.SetChildIndex(this.gbxSearchRangeDate, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.gbxDateview, 0);
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.pnlDebug.ResumeLayout(false);
            this.gbxSearchRangeDate.ResumeLayout(false);
            this.gbxSearchRangeDate.PerformLayout();
            this.gbxRankingRange.ResumeLayout(false);
            this.gbxRankingRange.PerformLayout();
            this.gbxDateview.ResumeLayout(false);
            this.gbxDateview.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private jp.co.fsi.common.controls.FsiTextBox txtUriRankFr;
        private System.Windows.Forms.Label lblCodeBet;
        private jp.co.fsi.common.controls.FsiTextBox txtUriRankTo;
        private System.Windows.Forms.Label lblEra;
        private jp.co.fsi.common.controls.FsiTextBox txtYear;
        private System.Windows.Forms.GroupBox gbxSearchRangeDate;
        private jp.co.fsi.common.controls.FsiTextBox txtMonth;
        private System.Windows.Forms.Label lblMonth;
        private System.Windows.Forms.Label lblYear;
        private System.Windows.Forms.GroupBox gbxRankingRange;
        private System.Windows.Forms.Label lblEraBack;
        private System.Windows.Forms.GroupBox gbxDateview;
        private System.Windows.Forms.RadioButton rdoDateview2;
        private System.Windows.Forms.RadioButton rdoDateview1;
        private System.Windows.Forms.GroupBox groupBox1;
        private common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private System.Windows.Forms.Label lblMizuageShisho;
    }
}