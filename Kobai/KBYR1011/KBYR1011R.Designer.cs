﻿namespace jp.co.fsi.kb.kbyr1011
{
    /// <summary>
    /// KBYR1011R の概要の説明です。
    /// </summary>
    partial class KBYR1011R
    {
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(KBYR1011R));
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.groupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.textBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox21 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox22 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox23 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox24 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox25 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox26 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label25 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label26 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label27 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label28 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label29 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label30 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label31 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label32 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label33 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label34 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label35 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label36 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label37 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label38 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label39 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.直線2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.ITEM02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト003 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル137 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル138 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル140 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ITEM01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル143 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.テキスト144 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト146 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト147 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル151 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル152 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.テキスト153 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル155 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.テキスト156 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル157 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.テキスト158 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル159 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル168 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル169 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル170 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル171 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.groupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト003)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル137)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル138)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル140)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル143)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト144)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト146)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト147)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル151)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル152)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト153)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル155)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト156)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル157)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト158)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル159)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル168)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル169)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル170)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル171)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // detail
            // 
            this.detail.Height = 0F;
            this.detail.Name = "detail";
            // 
            // groupHeader1
            // 
            this.groupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.textBox1,
            this.textBox2,
            this.textBox3,
            this.textBox4,
            this.textBox5,
            this.textBox6,
            this.textBox7,
            this.textBox8,
            this.textBox9,
            this.textBox10,
            this.textBox11,
            this.textBox12,
            this.textBox13,
            this.textBox14,
            this.textBox15,
            this.textBox16,
            this.textBox17,
            this.textBox18,
            this.textBox19,
            this.textBox20,
            this.textBox21,
            this.textBox22,
            this.textBox23,
            this.textBox24,
            this.label14,
            this.textBox25,
            this.textBox26,
            this.label15,
            this.label16,
            this.label17,
            this.label18,
            this.label19,
            this.label20,
            this.label21,
            this.label22,
            this.label23,
            this.label24,
            this.label25,
            this.label26,
            this.label27,
            this.label28,
            this.label29,
            this.label30,
            this.label31,
            this.label32,
            this.label33,
            this.label34,
            this.label35,
            this.label36,
            this.label37,
            this.label38,
            this.label39,
            this.直線2,
            this.ITEM02,
            this.テキスト003,
            this.ラベル10,
            this.ラベル137,
            this.ラベル138,
            this.ラベル140,
            this.ITEM01,
            this.ラベル143,
            this.テキスト144,
            this.テキスト146,
            this.テキスト147,
            this.ラベル151,
            this.ラベル152,
            this.テキスト153,
            this.ラベル155,
            this.テキスト156,
            this.ラベル157,
            this.テキスト158,
            this.ラベル159,
            this.ラベル168,
            this.ラベル169,
            this.ラベル170,
            this.ラベル171});
            this.groupHeader1.DataField = "ITEM36";
            this.groupHeader1.Height = 10.11457F;
            this.groupHeader1.Name = "groupHeader1";
            // 
            // textBox1
            // 
            this.textBox1.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox1.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox1.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox1.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox1.DataField = "ITEM10";
            this.textBox1.Height = 0.3937007F;
            this.textBox1.Left = 1.174803F;
            this.textBox1.Name = "textBox1";
            this.textBox1.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 5, 0);
            this.textBox1.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 18pt; font-" +
    "weight: bold; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox1.Tag = "";
            this.textBox1.Text = "ITEM10";
            this.textBox1.Top = 4.698032F;
            this.textBox1.Width = 1.844488F;
            // 
            // textBox2
            // 
            this.textBox2.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox2.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox2.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox2.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox2.DataField = "ITEM23";
            this.textBox2.Height = 0.3937007F;
            this.textBox2.Left = 3.020473F;
            this.textBox2.Name = "textBox2";
            this.textBox2.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 5, 0);
            this.textBox2.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 18pt; font-" +
    "weight: bold; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox2.Tag = "";
            this.textBox2.Text = "ITEM23";
            this.textBox2.Top = 4.698032F;
            this.textBox2.Width = 1.844488F;
            // 
            // textBox3
            // 
            this.textBox3.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox3.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox3.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox3.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox3.DataField = "ITEM11";
            this.textBox3.Height = 0.3937007F;
            this.textBox3.Left = 1.175985F;
            this.textBox3.Name = "textBox3";
            this.textBox3.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 5, 0);
            this.textBox3.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 18pt; font-" +
    "weight: bold; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox3.Tag = "";
            this.textBox3.Text = "ITEM11";
            this.textBox3.Top = 5.091733F;
            this.textBox3.Width = 1.844488F;
            // 
            // textBox4
            // 
            this.textBox4.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox4.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox4.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox4.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox4.DataField = "ITEM12";
            this.textBox4.Height = 0.3937007F;
            this.textBox4.Left = 1.175985F;
            this.textBox4.Name = "textBox4";
            this.textBox4.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 5, 0);
            this.textBox4.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 18pt; font-" +
    "weight: bold; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox4.Tag = "";
            this.textBox4.Text = "ITEM12";
            this.textBox4.Top = 5.485433F;
            this.textBox4.Width = 1.844488F;
            // 
            // textBox5
            // 
            this.textBox5.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox5.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox5.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox5.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox5.DataField = "ITEM13";
            this.textBox5.Height = 0.3937007F;
            this.textBox5.Left = 1.175985F;
            this.textBox5.Name = "textBox5";
            this.textBox5.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 5, 0);
            this.textBox5.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 18pt; font-" +
    "weight: bold; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox5.Tag = "";
            this.textBox5.Text = "ITEM13";
            this.textBox5.Top = 5.879134F;
            this.textBox5.Width = 1.844488F;
            // 
            // textBox6
            // 
            this.textBox6.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox6.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox6.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox6.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox6.DataField = "ITEM14";
            this.textBox6.Height = 0.3937007F;
            this.textBox6.Left = 1.175985F;
            this.textBox6.Name = "textBox6";
            this.textBox6.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 5, 0);
            this.textBox6.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 18pt; font-" +
    "weight: bold; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox6.Tag = "";
            this.textBox6.Text = "ITEM14";
            this.textBox6.Top = 6.270867F;
            this.textBox6.Width = 1.844488F;
            // 
            // textBox7
            // 
            this.textBox7.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox7.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox7.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox7.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox7.DataField = "ITEM15";
            this.textBox7.Height = 0.3937007F;
            this.textBox7.Left = 1.175985F;
            this.textBox7.Name = "textBox7";
            this.textBox7.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 5, 0);
            this.textBox7.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 18pt; font-" +
    "weight: bold; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox7.Tag = "";
            this.textBox7.Text = "ITEM15";
            this.textBox7.Top = 6.664568F;
            this.textBox7.Width = 1.844488F;
            // 
            // textBox8
            // 
            this.textBox8.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox8.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox8.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox8.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox8.DataField = "ITEM16";
            this.textBox8.Height = 0.3937007F;
            this.textBox8.Left = 1.175985F;
            this.textBox8.Name = "textBox8";
            this.textBox8.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 5, 0);
            this.textBox8.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 18pt; font-" +
    "weight: bold; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox8.Tag = "";
            this.textBox8.Text = "ITEM16";
            this.textBox8.Top = 7.058268F;
            this.textBox8.Width = 1.844488F;
            // 
            // textBox9
            // 
            this.textBox9.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox9.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox9.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox9.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox9.DataField = "ITEM17";
            this.textBox9.Height = 0.3937007F;
            this.textBox9.Left = 1.174803F;
            this.textBox9.Name = "textBox9";
            this.textBox9.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 5, 0);
            this.textBox9.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 18pt; font-" +
    "weight: bold; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox9.Tag = "";
            this.textBox9.Text = "ITEM17";
            this.textBox9.Top = 7.452363F;
            this.textBox9.Width = 1.844488F;
            // 
            // textBox10
            // 
            this.textBox10.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox10.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox10.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox10.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox10.DataField = "ITEM18";
            this.textBox10.Height = 0.3937007F;
            this.textBox10.Left = 1.175985F;
            this.textBox10.Name = "textBox10";
            this.textBox10.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 5, 0);
            this.textBox10.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 18pt; font-" +
    "weight: bold; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox10.Tag = "";
            this.textBox10.Text = "ITEM18";
            this.textBox10.Top = 7.846072F;
            this.textBox10.Width = 1.844488F;
            // 
            // textBox11
            // 
            this.textBox11.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox11.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox11.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox11.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox11.DataField = "ITEM19";
            this.textBox11.Height = 0.3937007F;
            this.textBox11.Left = 1.175985F;
            this.textBox11.Name = "textBox11";
            this.textBox11.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 5, 0);
            this.textBox11.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 18pt; font-" +
    "weight: bold; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox11.Tag = "";
            this.textBox11.Text = "ITEM19";
            this.textBox11.Top = 8.23978F;
            this.textBox11.Width = 1.844488F;
            // 
            // textBox12
            // 
            this.textBox12.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox12.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox12.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox12.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox12.DataField = "ITEM20";
            this.textBox12.Height = 0.3937007F;
            this.textBox12.Left = 1.175985F;
            this.textBox12.Name = "textBox12";
            this.textBox12.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 5, 0);
            this.textBox12.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 18pt; font-" +
    "weight: bold; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox12.Tag = "";
            this.textBox12.Text = "ITEM20";
            this.textBox12.Top = 8.63348F;
            this.textBox12.Width = 1.844488F;
            // 
            // textBox13
            // 
            this.textBox13.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox13.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox13.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox13.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox13.DataField = "ITEM21";
            this.textBox13.Height = 0.3937007F;
            this.textBox13.Left = 1.175985F;
            this.textBox13.Name = "textBox13";
            this.textBox13.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 5, 0);
            this.textBox13.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 18pt; font-" +
    "weight: bold; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox13.Tag = "";
            this.textBox13.Text = "ITEM21";
            this.textBox13.Top = 9.027182F;
            this.textBox13.Width = 1.844488F;
            // 
            // textBox14
            // 
            this.textBox14.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox14.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox14.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox14.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox14.DataField = "ITEM24";
            this.textBox14.Height = 0.3937007F;
            this.textBox14.Left = 3.020473F;
            this.textBox14.Name = "textBox14";
            this.textBox14.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 5, 0);
            this.textBox14.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 18pt; font-" +
    "weight: bold; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox14.Tag = "";
            this.textBox14.Text = "ITEM24";
            this.textBox14.Top = 5.091733F;
            this.textBox14.Width = 1.844488F;
            // 
            // textBox15
            // 
            this.textBox15.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox15.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox15.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox15.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox15.DataField = "ITEM25";
            this.textBox15.Height = 0.3937007F;
            this.textBox15.Left = 3.020473F;
            this.textBox15.Name = "textBox15";
            this.textBox15.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 5, 0);
            this.textBox15.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 18pt; font-" +
    "weight: bold; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox15.Tag = "";
            this.textBox15.Text = "ITEM25";
            this.textBox15.Top = 5.485433F;
            this.textBox15.Width = 1.844488F;
            // 
            // textBox16
            // 
            this.textBox16.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox16.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox16.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox16.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox16.DataField = "ITEM26";
            this.textBox16.Height = 0.3937007F;
            this.textBox16.Left = 3.020473F;
            this.textBox16.Name = "textBox16";
            this.textBox16.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 5, 0);
            this.textBox16.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 18pt; font-" +
    "weight: bold; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox16.Tag = "";
            this.textBox16.Text = "ITEM26";
            this.textBox16.Top = 5.879134F;
            this.textBox16.Width = 1.844488F;
            // 
            // textBox17
            // 
            this.textBox17.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox17.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox17.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox17.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox17.DataField = "ITEM27";
            this.textBox17.Height = 0.3937007F;
            this.textBox17.Left = 3.020473F;
            this.textBox17.Name = "textBox17";
            this.textBox17.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 5, 0);
            this.textBox17.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 18pt; font-" +
    "weight: bold; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox17.Tag = "";
            this.textBox17.Text = "ITEM27";
            this.textBox17.Top = 6.270867F;
            this.textBox17.Width = 1.844488F;
            // 
            // textBox18
            // 
            this.textBox18.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox18.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox18.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox18.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox18.DataField = "ITEM28";
            this.textBox18.Height = 0.3937007F;
            this.textBox18.Left = 3.020473F;
            this.textBox18.Name = "textBox18";
            this.textBox18.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 5, 0);
            this.textBox18.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 18pt; font-" +
    "weight: bold; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox18.Tag = "";
            this.textBox18.Text = "ITEM28";
            this.textBox18.Top = 6.664568F;
            this.textBox18.Width = 1.844488F;
            // 
            // textBox19
            // 
            this.textBox19.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox19.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox19.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox19.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox19.DataField = "ITEM29";
            this.textBox19.Height = 0.3937007F;
            this.textBox19.Left = 3.020473F;
            this.textBox19.Name = "textBox19";
            this.textBox19.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 5, 0);
            this.textBox19.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 18pt; font-" +
    "weight: bold; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox19.Tag = "";
            this.textBox19.Text = "ITEM29";
            this.textBox19.Top = 7.058662F;
            this.textBox19.Width = 1.844488F;
            // 
            // textBox20
            // 
            this.textBox20.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox20.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox20.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox20.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox20.DataField = "ITEM30";
            this.textBox20.Height = 0.3937007F;
            this.textBox20.Left = 3.020473F;
            this.textBox20.Name = "textBox20";
            this.textBox20.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 5, 0);
            this.textBox20.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 18pt; font-" +
    "weight: bold; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox20.Tag = "";
            this.textBox20.Text = "ITEM30";
            this.textBox20.Top = 7.452363F;
            this.textBox20.Width = 1.844488F;
            // 
            // textBox21
            // 
            this.textBox21.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox21.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox21.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox21.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox21.DataField = "ITEM31";
            this.textBox21.Height = 0.3937007F;
            this.textBox21.Left = 3.020473F;
            this.textBox21.Name = "textBox21";
            this.textBox21.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 5, 0);
            this.textBox21.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 18pt; font-" +
    "weight: bold; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox21.Tag = "";
            this.textBox21.Text = "ITEM31";
            this.textBox21.Top = 7.846072F;
            this.textBox21.Width = 1.844488F;
            // 
            // textBox22
            // 
            this.textBox22.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox22.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox22.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox22.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox22.DataField = "ITEM32";
            this.textBox22.Height = 0.3937007F;
            this.textBox22.Left = 3.020473F;
            this.textBox22.Name = "textBox22";
            this.textBox22.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 5, 0);
            this.textBox22.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 18pt; font-" +
    "weight: bold; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox22.Tag = "";
            this.textBox22.Text = "ITEM32";
            this.textBox22.Top = 8.23978F;
            this.textBox22.Width = 1.844488F;
            // 
            // textBox23
            // 
            this.textBox23.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox23.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox23.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox23.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox23.DataField = "ITEM33";
            this.textBox23.Height = 0.3937007F;
            this.textBox23.Left = 3.020473F;
            this.textBox23.Name = "textBox23";
            this.textBox23.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 5, 0);
            this.textBox23.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 18pt; font-" +
    "weight: bold; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox23.Tag = "";
            this.textBox23.Text = "ITEM33";
            this.textBox23.Top = 8.63348F;
            this.textBox23.Width = 1.844488F;
            // 
            // textBox24
            // 
            this.textBox24.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox24.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox24.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox24.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox24.DataField = "ITEM34";
            this.textBox24.Height = 0.3937007F;
            this.textBox24.Left = 3.020473F;
            this.textBox24.Name = "textBox24";
            this.textBox24.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 5, 0);
            this.textBox24.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 18pt; font-" +
    "weight: bold; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox24.Tag = "";
            this.textBox24.Text = "ITEM34";
            this.textBox24.Top = 9.027182F;
            this.textBox24.Width = 1.844488F;
            // 
            // label14
            // 
            this.label14.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label14.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label14.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label14.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label14.Height = 0.3937007F;
            this.label14.HyperLink = null;
            this.label14.Left = 0.3968505F;
            this.label14.Name = "label14";
            this.label14.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 18pt; fon" +
    "t-weight: bold; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.label14.Tag = "";
            this.label14.Text = "小計";
            this.label14.Top = 9.420883F;
            this.label14.Width = 0.7807083F;
            // 
            // textBox25
            // 
            this.textBox25.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox25.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox25.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox25.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox25.DataField = "ITEM22";
            this.textBox25.Height = 0.3937007F;
            this.textBox25.Left = 1.175985F;
            this.textBox25.Name = "textBox25";
            this.textBox25.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 5, 0);
            this.textBox25.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 18pt; font-" +
    "weight: bold; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox25.Tag = "";
            this.textBox25.Text = "ITEM22";
            this.textBox25.Top = 9.420883F;
            this.textBox25.Width = 1.844488F;
            // 
            // textBox26
            // 
            this.textBox26.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox26.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox26.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox26.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox26.DataField = "ITEM35";
            this.textBox26.Height = 0.3937007F;
            this.textBox26.Left = 3.020473F;
            this.textBox26.Name = "textBox26";
            this.textBox26.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 5, 0);
            this.textBox26.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 18pt; font-" +
    "weight: bold; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox26.Tag = "";
            this.textBox26.Text = "ITEM35";
            this.textBox26.Top = 9.420883F;
            this.textBox26.Width = 1.844488F;
            // 
            // label15
            // 
            this.label15.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label15.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label15.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label15.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label15.Height = 0.3937007F;
            this.label15.HyperLink = null;
            this.label15.Left = 0.3968505F;
            this.label15.Name = "label15";
            this.label15.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 18pt; font-weight: bold; text-align:" +
    " center; vertical-align: middle; ddo-char-set: 1";
            this.label15.Tag = "";
            this.label15.Text = "1";
            this.label15.Top = 4.698032F;
            this.label15.Width = 0.7807083F;
            // 
            // label16
            // 
            this.label16.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label16.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label16.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label16.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label16.Height = 0.3937007F;
            this.label16.HyperLink = null;
            this.label16.Left = 0.3968505F;
            this.label16.Name = "label16";
            this.label16.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 18pt; font-weight: bold; text-align:" +
    " center; vertical-align: middle; ddo-char-set: 1";
            this.label16.Tag = "";
            this.label16.Text = "2";
            this.label16.Top = 5.091733F;
            this.label16.Width = 0.7807083F;
            // 
            // label17
            // 
            this.label17.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label17.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label17.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label17.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label17.Height = 0.3937007F;
            this.label17.HyperLink = null;
            this.label17.Left = 0.3968505F;
            this.label17.Name = "label17";
            this.label17.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 18pt; font-weight: bold; text-align:" +
    " center; vertical-align: middle; ddo-char-set: 1";
            this.label17.Tag = "";
            this.label17.Text = "3";
            this.label17.Top = 5.485433F;
            this.label17.Width = 0.7807083F;
            // 
            // label18
            // 
            this.label18.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label18.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label18.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label18.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label18.Height = 0.3937007F;
            this.label18.HyperLink = null;
            this.label18.Left = 0.3968505F;
            this.label18.Name = "label18";
            this.label18.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 18pt; font-weight: bold; text-align:" +
    " center; vertical-align: middle; ddo-char-set: 1";
            this.label18.Tag = "";
            this.label18.Text = "4";
            this.label18.Top = 5.879134F;
            this.label18.Width = 0.7807083F;
            // 
            // label19
            // 
            this.label19.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label19.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label19.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label19.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label19.Height = 0.3937007F;
            this.label19.HyperLink = null;
            this.label19.Left = 0.3968505F;
            this.label19.Name = "label19";
            this.label19.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 18pt; font-weight: bold; text-align:" +
    " center; vertical-align: middle; ddo-char-set: 1";
            this.label19.Tag = "";
            this.label19.Text = "5";
            this.label19.Top = 6.270867F;
            this.label19.Width = 0.7807083F;
            // 
            // label20
            // 
            this.label20.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label20.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label20.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label20.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label20.Height = 0.3937007F;
            this.label20.HyperLink = null;
            this.label20.Left = 0.3968505F;
            this.label20.Name = "label20";
            this.label20.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 18pt; font-weight: bold; text-align:" +
    " center; vertical-align: middle; ddo-char-set: 1";
            this.label20.Tag = "";
            this.label20.Text = "6";
            this.label20.Top = 6.664568F;
            this.label20.Width = 0.7807083F;
            // 
            // label21
            // 
            this.label21.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label21.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label21.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label21.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label21.Height = 0.3937007F;
            this.label21.HyperLink = null;
            this.label21.Left = 0.3968505F;
            this.label21.Name = "label21";
            this.label21.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 18pt; font-weight: bold; text-align:" +
    " center; vertical-align: middle; ddo-char-set: 1";
            this.label21.Tag = "";
            this.label21.Text = "7";
            this.label21.Top = 7.058268F;
            this.label21.Width = 0.7807083F;
            // 
            // label22
            // 
            this.label22.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label22.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label22.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label22.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label22.Height = 0.3937007F;
            this.label22.HyperLink = null;
            this.label22.Left = 0.3968505F;
            this.label22.Name = "label22";
            this.label22.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 18pt; font-weight: bold; text-align:" +
    " center; vertical-align: middle; ddo-char-set: 1";
            this.label22.Tag = "";
            this.label22.Text = "8";
            this.label22.Top = 7.451969F;
            this.label22.Width = 0.7807083F;
            // 
            // label23
            // 
            this.label23.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label23.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label23.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label23.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label23.Height = 0.3937007F;
            this.label23.HyperLink = null;
            this.label23.Left = 0.3968505F;
            this.label23.Name = "label23";
            this.label23.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 18pt; font-weight: bold; text-align:" +
    " center; vertical-align: middle; ddo-char-set: 1";
            this.label23.Tag = "";
            this.label23.Text = "9";
            this.label23.Top = 7.846072F;
            this.label23.Width = 0.7807083F;
            // 
            // label24
            // 
            this.label24.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label24.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label24.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label24.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label24.Height = 0.3937007F;
            this.label24.HyperLink = null;
            this.label24.Left = 0.3968505F;
            this.label24.Name = "label24";
            this.label24.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 18pt; font-weight: bold; text-align:" +
    " center; vertical-align: middle; ddo-char-set: 1";
            this.label24.Tag = "";
            this.label24.Text = "10";
            this.label24.Top = 8.23978F;
            this.label24.Width = 0.7807083F;
            // 
            // label25
            // 
            this.label25.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label25.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label25.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label25.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label25.Height = 0.3937007F;
            this.label25.HyperLink = null;
            this.label25.Left = 0.3968505F;
            this.label25.Name = "label25";
            this.label25.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 18pt; font-weight: bold; text-align:" +
    " center; vertical-align: middle; ddo-char-set: 1";
            this.label25.Tag = "";
            this.label25.Text = "11";
            this.label25.Top = 8.63348F;
            this.label25.Width = 0.7807083F;
            // 
            // label26
            // 
            this.label26.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label26.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label26.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label26.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label26.Height = 0.3937007F;
            this.label26.HyperLink = null;
            this.label26.Left = 0.3968505F;
            this.label26.Name = "label26";
            this.label26.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 18pt; font-weight: bold; text-align:" +
    " center; vertical-align: middle; ddo-char-set: 1";
            this.label26.Tag = "";
            this.label26.Text = "12";
            this.label26.Top = 9.027182F;
            this.label26.Width = 0.7807083F;
            // 
            // label27
            // 
            this.label27.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label27.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label27.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label27.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label27.Height = 0.3937007F;
            this.label27.HyperLink = null;
            this.label27.Left = 4.864962F;
            this.label27.Name = "label27";
            this.label27.Style = "font-family: ＭＳ 明朝; vertical-align: middle; ddo-char-set: 1";
            this.label27.Text = "";
            this.label27.Top = 4.698032F;
            this.label27.Width = 1.83189F;
            // 
            // label28
            // 
            this.label28.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label28.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label28.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label28.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label28.Height = 0.3937007F;
            this.label28.HyperLink = null;
            this.label28.Left = 4.864173F;
            this.label28.Name = "label28";
            this.label28.Style = "font-family: ＭＳ 明朝; vertical-align: middle; ddo-char-set: 1";
            this.label28.Text = "";
            this.label28.Top = 5.091733F;
            this.label28.Width = 1.83189F;
            // 
            // label29
            // 
            this.label29.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label29.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label29.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label29.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label29.Height = 0.3937007F;
            this.label29.HyperLink = null;
            this.label29.Left = 4.864962F;
            this.label29.Name = "label29";
            this.label29.Style = "font-family: ＭＳ 明朝; vertical-align: middle; ddo-char-set: 1";
            this.label29.Text = "";
            this.label29.Top = 5.485433F;
            this.label29.Width = 1.83189F;
            // 
            // label30
            // 
            this.label30.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label30.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label30.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label30.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label30.Height = 0.3937007F;
            this.label30.HyperLink = null;
            this.label30.Left = 4.865355F;
            this.label30.Name = "label30";
            this.label30.Style = "font-family: ＭＳ 明朝; vertical-align: middle; ddo-char-set: 1";
            this.label30.Text = "";
            this.label30.Top = 5.879134F;
            this.label30.Width = 1.83189F;
            // 
            // label31
            // 
            this.label31.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label31.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label31.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label31.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label31.Height = 0.3937007F;
            this.label31.HyperLink = null;
            this.label31.Left = 4.865355F;
            this.label31.Name = "label31";
            this.label31.Style = "font-family: ＭＳ 明朝; vertical-align: middle; ddo-char-set: 1";
            this.label31.Text = "";
            this.label31.Top = 6.270867F;
            this.label31.Width = 1.83189F;
            // 
            // label32
            // 
            this.label32.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label32.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label32.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label32.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label32.Height = 0.3937007F;
            this.label32.HyperLink = null;
            this.label32.Left = 4.865355F;
            this.label32.Name = "label32";
            this.label32.Style = "font-family: ＭＳ 明朝; vertical-align: middle; ddo-char-set: 1";
            this.label32.Text = "";
            this.label32.Top = 6.664568F;
            this.label32.Width = 1.83189F;
            // 
            // label33
            // 
            this.label33.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label33.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label33.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label33.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label33.Height = 0.3937007F;
            this.label33.HyperLink = null;
            this.label33.Left = 4.865355F;
            this.label33.Name = "label33";
            this.label33.Style = "font-family: ＭＳ 明朝; vertical-align: middle; ddo-char-set: 1";
            this.label33.Text = "";
            this.label33.Top = 7.058662F;
            this.label33.Width = 1.83189F;
            // 
            // label34
            // 
            this.label34.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label34.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label34.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label34.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label34.Height = 0.3937007F;
            this.label34.HyperLink = null;
            this.label34.Left = 4.864173F;
            this.label34.Name = "label34";
            this.label34.Style = "font-family: ＭＳ 明朝; vertical-align: middle; ddo-char-set: 1";
            this.label34.Text = "";
            this.label34.Top = 7.452363F;
            this.label34.Width = 1.83189F;
            // 
            // label35
            // 
            this.label35.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label35.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label35.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label35.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label35.Height = 0.3937007F;
            this.label35.HyperLink = null;
            this.label35.Left = 4.865355F;
            this.label35.Name = "label35";
            this.label35.Style = "font-family: ＭＳ 明朝; vertical-align: middle; ddo-char-set: 1";
            this.label35.Text = "";
            this.label35.Top = 7.846072F;
            this.label35.Width = 1.83189F;
            // 
            // label36
            // 
            this.label36.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label36.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label36.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label36.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label36.Height = 0.3937007F;
            this.label36.HyperLink = null;
            this.label36.Left = 4.865355F;
            this.label36.Name = "label36";
            this.label36.Style = "font-family: ＭＳ 明朝; vertical-align: middle; ddo-char-set: 1";
            this.label36.Text = "";
            this.label36.Top = 8.23978F;
            this.label36.Width = 1.83189F;
            // 
            // label37
            // 
            this.label37.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label37.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label37.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label37.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label37.Height = 0.3937007F;
            this.label37.HyperLink = null;
            this.label37.Left = 4.864173F;
            this.label37.Name = "label37";
            this.label37.Style = "font-family: ＭＳ 明朝; vertical-align: middle; ddo-char-set: 1";
            this.label37.Text = "";
            this.label37.Top = 8.63348F;
            this.label37.Width = 1.83189F;
            // 
            // label38
            // 
            this.label38.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label38.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label38.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label38.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label38.Height = 0.3937007F;
            this.label38.HyperLink = null;
            this.label38.Left = 4.865355F;
            this.label38.Name = "label38";
            this.label38.Style = "font-family: ＭＳ 明朝; vertical-align: middle; ddo-char-set: 1";
            this.label38.Text = "";
            this.label38.Top = 9.027182F;
            this.label38.Width = 1.83189F;
            // 
            // label39
            // 
            this.label39.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label39.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label39.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label39.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label39.Height = 0.3937007F;
            this.label39.HyperLink = null;
            this.label39.Left = 4.865355F;
            this.label39.Name = "label39";
            this.label39.Style = "font-family: ＭＳ 明朝; vertical-align: middle; ddo-char-set: 1";
            this.label39.Text = "";
            this.label39.Top = 9.420883F;
            this.label39.Width = 1.83189F;
            // 
            // 直線2
            // 
            this.直線2.Height = 0F;
            this.直線2.Left = 0.4059055F;
            this.直線2.LineWeight = 1F;
            this.直線2.Name = "直線2";
            this.直線2.Tag = "";
            this.直線2.Top = 1.581102F;
            this.直線2.Width = 2.614911F;
            this.直線2.X1 = 0.4059055F;
            this.直線2.X2 = 3.020816F;
            this.直線2.Y1 = 1.581102F;
            this.直線2.Y2 = 1.581102F;
            // 
            // ITEM02
            // 
            this.ITEM02.DataField = "ITEM02";
            this.ITEM02.Height = 0.2291667F;
            this.ITEM02.Left = 4.491525F;
            this.ITEM02.MultiLine = false;
            this.ITEM02.Name = "ITEM02";
            this.ITEM02.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 128";
            this.ITEM02.Tag = "";
            this.ITEM02.Text = "ITEM02";
            this.ITEM02.Top = 1.060214F;
            this.ITEM02.Width = 2.160417F;
            // 
            // テキスト003
            // 
            this.テキスト003.DataField = "ITEM03";
            this.テキスト003.Height = 0.2153872F;
            this.テキスト003.Left = 0.4059055F;
            this.テキスト003.MultiLine = false;
            this.テキスト003.Name = "テキスト003";
            this.テキスト003.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 14.25pt; fo" +
    "nt-weight: bold; text-align: center; ddo-char-set: 128";
            this.テキスト003.Tag = "";
            this.テキスト003.Text = "ITEM03";
            this.テキスト003.Top = 1.344095F;
            this.テキスト003.Width = 2.334252F;
            // 
            // ラベル10
            // 
            this.ラベル10.Height = 0.2258039F;
            this.ラベル10.HyperLink = null;
            this.ラベル10.Left = 2.783858F;
            this.ラベル10.Name = "ラベル10";
            this.ラベル10.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 14.25pt; font-weight: bold; text-ali" +
    "gn: left; ddo-char-set: 128";
            this.ラベル10.Tag = "";
            this.ラベル10.Text = "殿";
            this.ラベル10.Top = 1.333465F;
            this.ラベル10.Width = 0.28125F;
            // 
            // ラベル137
            // 
            this.ラベル137.Height = 0.1715278F;
            this.ラベル137.HyperLink = null;
            this.ラベル137.Left = 4.926941F;
            this.ラベル137.Name = "ラベル137";
            this.ラベル137.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: normal; text-a" +
    "lign: left; ddo-char-set: 1";
            this.ラベル137.Tag = "";
            this.ラベル137.Text = "証";
            this.ラベル137.Top = 0.7914646F;
            this.ラベル137.Width = 0.1701389F;
            // 
            // ラベル138
            // 
            this.ラベル138.Height = 0.1576389F;
            this.ラベル138.HyperLink = null;
            this.ラベル138.Left = 5.358267F;
            this.ラベル138.Name = "ラベル138";
            this.ラベル138.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: normal; text-a" +
    "lign: left; ddo-char-set: 1";
            this.ラベル138.Tag = "";
            this.ラベル138.Text = "第";
            this.ラベル138.Top = 0.7913389F;
            this.ラベル138.Width = 0.1548611F;
            // 
            // ラベル140
            // 
            this.ラベル140.Height = 0.2027778F;
            this.ラベル140.HyperLink = null;
            this.ラベル140.Left = 6.502636F;
            this.ラベル140.Name = "ラベル140";
            this.ラベル140.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: normal; text-a" +
    "lign: left; ddo-char-set: 1";
            this.ラベル140.Tag = "";
            this.ラベル140.Text = "号";
            this.ラベル140.Top = 0.7859089F;
            this.ラベル140.Width = 0.15625F;
            // 
            // ITEM01
            // 
            this.ITEM01.DataField = "ITEM01";
            this.ITEM01.Height = 0.2083333F;
            this.ITEM01.Left = 5.513053F;
            this.ITEM01.MultiLine = false;
            this.ITEM01.Name = "ITEM01";
            this.ITEM01.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.ITEM01.Tag = "";
            this.ITEM01.Text = "ITEM01";
            this.ITEM01.Top = 0.7859089F;
            this.ITEM01.Width = 0.9520833F;
            // 
            // ラベル143
            // 
            this.ラベル143.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル143.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル143.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル143.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル143.Height = 0.551181F;
            this.ラベル143.HyperLink = null;
            this.ラベル143.Left = 0.3964567F;
            this.ラベル143.Name = "ラベル143";
            this.ラベル143.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 24pt; fon" +
    "t-weight: bold; text-align: center; vertical-align: middle; ddo-char-set: 128";
            this.ラベル143.Tag = "";
            this.ラベル143.Text = "購買・製氷　売上証明書";
            this.ラベル143.Top = 0F;
            this.ラベル143.Width = 6.301575F;
            // 
            // テキスト144
            // 
            this.テキスト144.DataField = "ITEM04";
            this.テキスト144.Height = 0.1979167F;
            this.テキスト144.Left = 3.966929F;
            this.テキスト144.MultiLine = false;
            this.テキスト144.Name = "テキスト144";
            this.テキスト144.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: bold; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.テキスト144.Tag = "";
            this.テキスト144.Text = "ITEM04";
            this.テキスト144.Top = 1.683071F;
            this.テキスト144.Width = 2.730715F;
            // 
            // テキスト146
            // 
            this.テキスト146.DataField = "ITEM05";
            this.テキスト146.Height = 0.1979167F;
            this.テキスト146.Left = 3.966929F;
            this.テキスト146.MultiLine = false;
            this.テキスト146.Name = "テキスト146";
            this.テキスト146.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: bold; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.テキスト146.Tag = "";
            this.テキスト146.Text = "ITEM05";
            this.テキスト146.Top = 1.922654F;
            this.テキスト146.Width = 2.730715F;
            // 
            // テキスト147
            // 
            this.テキスト147.DataField = "ITEM06";
            this.テキスト147.Height = 0.2083333F;
            this.テキスト147.Left = 3.966936F;
            this.テキスト147.MultiLine = false;
            this.テキスト147.Name = "テキスト147";
            this.テキスト147.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: bold; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.テキスト147.Tag = "";
            this.テキスト147.Text = "ITEM06";
            this.テキスト147.Top = 2.183071F;
            this.テキスト147.Width = 2.730688F;
            // 
            // ラベル151
            // 
            this.ラベル151.Height = 0.2027778F;
            this.ラベル151.HyperLink = null;
            this.ラベル151.Left = 0.4059055F;
            this.ラベル151.Name = "ラベル151";
            this.ラベル151.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 14.25pt; font-weight: normal; text-a" +
    "lign: left; ddo-char-set: 1";
            this.ラベル151.Tag = "";
            this.ラベル151.Text = "下記の通り証明します。";
            this.ラベル151.Top = 2.875591F;
            this.ラベル151.Width = 2.238583F;
            // 
            // ラベル152
            // 
            this.ラベル152.Height = 0.2027778F;
            this.ラベル152.HyperLink = null;
            this.ラベル152.Left = 0.4444882F;
            this.ラベル152.Name = "ラベル152";
            this.ラベル152.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 14.25pt; font-weight: normal; text-a" +
    "lign: left; ddo-char-set: 1";
            this.ラベル152.Tag = "";
            this.ラベル152.Text = "期　間: 自";
            this.ラベル152.Top = 3.156693F;
            this.ラベル152.Width = 1.026378F;
            // 
            // テキスト153
            // 
            this.テキスト153.DataField = "ITEM07";
            this.テキスト153.Height = 0.1875F;
            this.テキスト153.Left = 1.470866F;
            this.テキスト153.MultiLine = false;
            this.テキスト153.Name = "テキスト153";
            this.テキスト153.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 14.25pt; fo" +
    "nt-weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト153.Tag = "";
            this.テキスト153.Text = "ITEM07";
            this.テキスト153.Top = 3.161024F;
            this.テキスト153.Width = 1.673229F;
            // 
            // ラベル155
            // 
            this.ラベル155.Height = 0.2027778F;
            this.ラベル155.HyperLink = null;
            this.ラベル155.Left = 3.144095F;
            this.ラベル155.Name = "ラベル155";
            this.ラベル155.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 14.25pt; font-weight: normal; text-a" +
    "lign: left; ddo-char-set: 1";
            this.ラベル155.Tag = "";
            this.ラベル155.Text = "～ 至";
            this.ラベル155.Top = 3.156693F;
            this.ラベル155.Width = 0.5512856F;
            // 
            // テキスト156
            // 
            this.テキスト156.DataField = "ITEM08";
            this.テキスト156.Height = 0.1874016F;
            this.テキスト156.Left = 3.695276F;
            this.テキスト156.MultiLine = false;
            this.テキスト156.Name = "テキスト156";
            this.テキスト156.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 14.25pt; fo" +
    "nt-weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト156.Tag = "";
            this.テキスト156.Text = "ITEM08";
            this.テキスト156.Top = 3.156693F;
            this.テキスト156.Width = 1.673228F;
            // 
            // ラベル157
            // 
            this.ラベル157.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル157.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル157.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル157.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル157.Height = 0.3958333F;
            this.ラベル157.HyperLink = null;
            this.ラベル157.Left = 0.3957131F;
            this.ラベル157.Name = "ラベル157";
            this.ラベル157.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 18pt; fon" +
    "t-weight: bold; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.ラベル157.Tag = "";
            this.ラベル157.Text = "合　計";
            this.ラベル157.Top = 3.466016F;
            this.ラベル157.Width = 1.661111F;
            // 
            // テキスト158
            // 
            this.テキスト158.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト158.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト158.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト158.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト158.DataField = "ITEM09";
            this.テキスト158.Height = 0.3958333F;
            this.テキスト158.Left = 2.054741F;
            this.テキスト158.MultiLine = false;
            this.テキスト158.Name = "テキスト158";
            this.テキスト158.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 20, 0);
            this.テキスト158.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 18pt; font-" +
    "weight: bold; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.テキスト158.Tag = "";
            this.テキスト158.Text = "ITEM09";
            this.テキスト158.Top = 3.466016F;
            this.テキスト158.Width = 2.868056F;
            // 
            // ラベル159
            // 
            this.ラベル159.Height = 0.1979167F;
            this.ラベル159.HyperLink = null;
            this.ラベル159.Left = 3.314173F;
            this.ラベル159.Name = "ラベル159";
            this.ラベル159.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 14.25pt; font-weight: normal; text-a" +
    "lign: left; vertical-align: middle; ddo-char-set: 1";
            this.ラベル159.Tag = "";
            this.ラベル159.Text = "※ 現金売上分と掛代金の回収分のみ";
            this.ラベル159.Top = 4.050789F;
            this.ラベル159.Width = 3.383448F;
            // 
            // ラベル168
            // 
            this.ラベル168.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル168.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル168.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル168.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル168.Height = 0.3958333F;
            this.ラベル168.HyperLink = null;
            this.ラベル168.Left = 0.3957131F;
            this.ラベル168.Name = "ラベル168";
            this.ラベル168.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 20.25pt; " +
    "font-weight: bold; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.ラベル168.Tag = "";
            this.ラベル168.Text = "月";
            this.ラベル168.Top = 4.302128F;
            this.ラベル168.Width = 0.7798611F;
            // 
            // ラベル169
            // 
            this.ラベル169.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル169.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル169.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル169.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル169.Height = 0.3958333F;
            this.ラベル169.HyperLink = null;
            this.ラベル169.Left = 1.176964F;
            this.ラベル169.Name = "ラベル169";
            this.ラベル169.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 20.25pt; " +
    "font-weight: bold; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.ラベル169.Tag = "";
            this.ラベル169.Text = "購 買";
            this.ラベル169.Top = 4.302128F;
            this.ラベル169.Width = 1.84375F;
            // 
            // ラベル170
            // 
            this.ラベル170.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル170.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル170.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル170.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル170.Height = 0.3958333F;
            this.ラベル170.HyperLink = null;
            this.ラベル170.Left = 3.021261F;
            this.ラベル170.Name = "ラベル170";
            this.ラベル170.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 20.25pt; " +
    "font-weight: bold; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.ラベル170.Tag = "";
            this.ラベル170.Text = "製 氷 ";
            this.ラベル170.Top = 4.30197F;
            this.ラベル170.Width = 1.84375F;
            // 
            // ラベル171
            // 
            this.ラベル171.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル171.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル171.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル171.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル171.Height = 0.3958333F;
            this.ラベル171.HyperLink = null;
            this.ラベル171.Left = 4.865748F;
            this.ラベル171.Name = "ラベル171";
            this.ラベル171.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 20.25pt; " +
    "font-weight: bold; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.ラベル171.Tag = "";
            this.ラベル171.Text = "備 考";
            this.ラベル171.Top = 4.30197F;
            this.ラベル171.Width = 1.83189F;
            // 
            // groupFooter1
            // 
            this.groupFooter1.Height = 0F;
            this.groupFooter1.Name = "groupFooter1";
            // 
            // KBYR1011R
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.7874016F;
            this.PageSettings.Margins.Left = 0.5905512F;
            this.PageSettings.Margins.Right = 0.5905512F;
            this.PageSettings.Margins.Top = 0.6692914F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Portrait;
            this.PageSettings.PaperHeight = 11.69291F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.267716F;
            this.PrintWidth = 7.09375F;
            this.Sections.Add(this.groupHeader1);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.groupFooter1);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト003)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル137)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル138)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル140)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル143)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト144)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト146)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト147)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル151)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル152)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト153)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル155)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト156)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル157)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト158)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル159)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル168)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル169)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル170)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル171)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader groupHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter groupFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox13;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox14;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox15;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox16;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox17;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox18;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox19;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox20;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox21;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox22;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox23;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox24;
        private GrapeCity.ActiveReports.SectionReportModel.Label label14;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox25;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox26;
        private GrapeCity.ActiveReports.SectionReportModel.Label label15;
        private GrapeCity.ActiveReports.SectionReportModel.Label label16;
        private GrapeCity.ActiveReports.SectionReportModel.Label label17;
        private GrapeCity.ActiveReports.SectionReportModel.Label label18;
        private GrapeCity.ActiveReports.SectionReportModel.Label label19;
        private GrapeCity.ActiveReports.SectionReportModel.Label label20;
        private GrapeCity.ActiveReports.SectionReportModel.Label label21;
        private GrapeCity.ActiveReports.SectionReportModel.Label label22;
        private GrapeCity.ActiveReports.SectionReportModel.Label label23;
        private GrapeCity.ActiveReports.SectionReportModel.Label label24;
        private GrapeCity.ActiveReports.SectionReportModel.Label label25;
        private GrapeCity.ActiveReports.SectionReportModel.Label label26;
        private GrapeCity.ActiveReports.SectionReportModel.Label label27;
        private GrapeCity.ActiveReports.SectionReportModel.Label label28;
        private GrapeCity.ActiveReports.SectionReportModel.Label label29;
        private GrapeCity.ActiveReports.SectionReportModel.Label label30;
        private GrapeCity.ActiveReports.SectionReportModel.Label label31;
        private GrapeCity.ActiveReports.SectionReportModel.Label label32;
        private GrapeCity.ActiveReports.SectionReportModel.Label label33;
        private GrapeCity.ActiveReports.SectionReportModel.Label label34;
        private GrapeCity.ActiveReports.SectionReportModel.Label label35;
        private GrapeCity.ActiveReports.SectionReportModel.Label label36;
        private GrapeCity.ActiveReports.SectionReportModel.Label label37;
        private GrapeCity.ActiveReports.SectionReportModel.Label label38;
        private GrapeCity.ActiveReports.SectionReportModel.Label label39;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM02;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト003;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル10;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル137;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル138;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル140;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM01;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル143;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト144;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト146;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト147;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル151;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル152;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト153;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル155;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト156;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル157;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト158;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル159;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル168;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル169;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル170;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル171;
    }
}
