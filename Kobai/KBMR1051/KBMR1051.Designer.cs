﻿namespace jp.co.fsi.kb.kbmr1051
{
    partial class KBMR1051
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbxDate = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtJpYearTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtJpMonthTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblJpTo = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblDateMonth = new System.Windows.Forms.Label();
            this.lblDateYear = new System.Windows.Forms.Label();
            this.txtJpYearFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtJpMonthFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblJpFr = new System.Windows.Forms.Label();
            this.lblDate = new System.Windows.Forms.Label();
            this.gbxShohinKubun = new System.Windows.Forms.GroupBox();
            this.lblShohinKubunBet = new System.Windows.Forms.Label();
            this.lblShohinKubunFr = new System.Windows.Forms.Label();
            this.lblShohinKubunTo = new System.Windows.Forms.Label();
            this.txtShohinKubunTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtShohinKubunFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.gbxZei = new System.Windows.Forms.GroupBox();
            this.rdoZeikomi = new System.Windows.Forms.RadioButton();
            this.rdoZeinuki = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMizuageShishoNm = new System.Windows.Forms.Label();
            this.lblMizuageShisho = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.rdoAll = new System.Windows.Forms.RadioButton();
            this.rdoMonth = new System.Windows.Forms.RadioButton();
            this.chkMode = new System.Windows.Forms.CheckBox();
            this.pnlDebug.SuspendLayout();
            this.gbxDate.SuspendLayout();
            this.gbxShohinKubun.SuspendLayout();
            this.gbxZei.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Text = "";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(5, 531);
            this.pnlDebug.Size = new System.Drawing.Size(843, 100);
            // 
            // gbxDate
            // 
            this.gbxDate.Controls.Add(this.label5);
            this.gbxDate.Controls.Add(this.label1);
            this.gbxDate.Controls.Add(this.label2);
            this.gbxDate.Controls.Add(this.txtJpYearTo);
            this.gbxDate.Controls.Add(this.txtJpMonthTo);
            this.gbxDate.Controls.Add(this.lblJpTo);
            this.gbxDate.Controls.Add(this.label4);
            this.gbxDate.Controls.Add(this.lblDateMonth);
            this.gbxDate.Controls.Add(this.lblDateYear);
            this.gbxDate.Controls.Add(this.txtJpYearFr);
            this.gbxDate.Controls.Add(this.txtJpMonthFr);
            this.gbxDate.Controls.Add(this.lblJpFr);
            this.gbxDate.Controls.Add(this.lblDate);
            this.gbxDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxDate.ForeColor = System.Drawing.SystemColors.ControlText;
            this.gbxDate.Location = new System.Drawing.Point(18, 125);
            this.gbxDate.Name = "gbxDate";
            this.gbxDate.Size = new System.Drawing.Size(490, 67);
            this.gbxDate.TabIndex = 1;
            this.gbxDate.TabStop = false;
            this.gbxDate.Text = "出力月";
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label5.Location = new System.Drawing.Point(198, 25);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(17, 20);
            this.label5.TabIndex = 12;
            this.label5.Text = "～";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Silver;
            this.label1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(362, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(15, 19);
            this.label1.TabIndex = 11;
            this.label1.Text = "月";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Silver;
            this.label2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(309, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(17, 21);
            this.label2.TabIndex = 9;
            this.label2.Text = "年";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtJpYearTo
            // 
            this.txtJpYearTo.AutoSizeFromLength = false;
            this.txtJpYearTo.DisplayLength = null;
            this.txtJpYearTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtJpYearTo.Location = new System.Drawing.Point(277, 25);
            this.txtJpYearTo.MaxLength = 2;
            this.txtJpYearTo.Name = "txtJpYearTo";
            this.txtJpYearTo.Size = new System.Drawing.Size(30, 20);
            this.txtJpYearTo.TabIndex = 8;
            this.txtJpYearTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtJpYearTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtJpYearTo_Validating);
            // 
            // txtJpMonthTo
            // 
            this.txtJpMonthTo.AutoSizeFromLength = false;
            this.txtJpMonthTo.DisplayLength = null;
            this.txtJpMonthTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtJpMonthTo.Location = new System.Drawing.Point(329, 25);
            this.txtJpMonthTo.MaxLength = 2;
            this.txtJpMonthTo.Name = "txtJpMonthTo";
            this.txtJpMonthTo.Size = new System.Drawing.Size(30, 20);
            this.txtJpMonthTo.TabIndex = 10;
            this.txtJpMonthTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtJpMonthTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateMonthTo_Validating);
            // 
            // lblJpTo
            // 
            this.lblJpTo.BackColor = System.Drawing.Color.Silver;
            this.lblJpTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblJpTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblJpTo.Location = new System.Drawing.Point(234, 25);
            this.lblJpTo.Name = "lblJpTo";
            this.lblJpTo.Size = new System.Drawing.Size(38, 21);
            this.lblJpTo.TabIndex = 6;
            this.lblJpTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Silver;
            this.label4.Location = new System.Drawing.Point(230, 21);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(153, 29);
            this.label4.TabIndex = 7;
            // 
            // lblDateMonth
            // 
            this.lblDateMonth.BackColor = System.Drawing.Color.Silver;
            this.lblDateMonth.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateMonth.Location = new System.Drawing.Point(164, 26);
            this.lblDateMonth.Name = "lblDateMonth";
            this.lblDateMonth.Size = new System.Drawing.Size(15, 19);
            this.lblDateMonth.TabIndex = 5;
            this.lblDateMonth.Text = "月";
            this.lblDateMonth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateYear
            // 
            this.lblDateYear.BackColor = System.Drawing.Color.Silver;
            this.lblDateYear.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateYear.Location = new System.Drawing.Point(111, 24);
            this.lblDateYear.Name = "lblDateYear";
            this.lblDateYear.Size = new System.Drawing.Size(17, 21);
            this.lblDateYear.TabIndex = 3;
            this.lblDateYear.Text = "年";
            this.lblDateYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtJpYearFr
            // 
            this.txtJpYearFr.AutoSizeFromLength = false;
            this.txtJpYearFr.DisplayLength = null;
            this.txtJpYearFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtJpYearFr.Location = new System.Drawing.Point(79, 25);
            this.txtJpYearFr.MaxLength = 2;
            this.txtJpYearFr.Name = "txtJpYearFr";
            this.txtJpYearFr.Size = new System.Drawing.Size(30, 20);
            this.txtJpYearFr.TabIndex = 2;
            this.txtJpYearFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtJpYearFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtJpYearFr_Validating);
            // 
            // txtJpMonthFr
            // 
            this.txtJpMonthFr.AutoSizeFromLength = false;
            this.txtJpMonthFr.DisplayLength = null;
            this.txtJpMonthFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtJpMonthFr.Location = new System.Drawing.Point(131, 25);
            this.txtJpMonthFr.MaxLength = 2;
            this.txtJpMonthFr.Name = "txtJpMonthFr";
            this.txtJpMonthFr.Size = new System.Drawing.Size(30, 20);
            this.txtJpMonthFr.TabIndex = 4;
            this.txtJpMonthFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtJpMonthFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateMonthFr_Validating);
            // 
            // lblJpFr
            // 
            this.lblJpFr.BackColor = System.Drawing.Color.Silver;
            this.lblJpFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblJpFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblJpFr.Location = new System.Drawing.Point(36, 25);
            this.lblJpFr.Name = "lblJpFr";
            this.lblJpFr.Size = new System.Drawing.Size(38, 21);
            this.lblJpFr.TabIndex = 1;
            this.lblJpFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDate
            // 
            this.lblDate.BackColor = System.Drawing.Color.Silver;
            this.lblDate.Location = new System.Drawing.Point(32, 21);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(153, 29);
            this.lblDate.TabIndex = 1;
            // 
            // gbxShohinKubun
            // 
            this.gbxShohinKubun.Controls.Add(this.lblShohinKubunBet);
            this.gbxShohinKubun.Controls.Add(this.lblShohinKubunFr);
            this.gbxShohinKubun.Controls.Add(this.lblShohinKubunTo);
            this.gbxShohinKubun.Controls.Add(this.txtShohinKubunTo);
            this.gbxShohinKubun.Controls.Add(this.txtShohinKubunFr);
            this.gbxShohinKubun.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxShohinKubun.ForeColor = System.Drawing.SystemColors.ControlText;
            this.gbxShohinKubun.Location = new System.Drawing.Point(18, 198);
            this.gbxShohinKubun.Name = "gbxShohinKubun";
            this.gbxShohinKubun.Size = new System.Drawing.Size(490, 72);
            this.gbxShohinKubun.TabIndex = 3;
            this.gbxShohinKubun.TabStop = false;
            this.gbxShohinKubun.Text = "商品区分１の検索";
            // 
            // lblShohinKubunBet
            // 
            this.lblShohinKubunBet.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShohinKubunBet.Location = new System.Drawing.Point(240, 29);
            this.lblShohinKubunBet.Name = "lblShohinKubunBet";
            this.lblShohinKubunBet.Size = new System.Drawing.Size(17, 20);
            this.lblShohinKubunBet.TabIndex = 2;
            this.lblShohinKubunBet.Text = "～";
            this.lblShohinKubunBet.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblShohinKubunFr
            // 
            this.lblShohinKubunFr.BackColor = System.Drawing.Color.Silver;
            this.lblShohinKubunFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShohinKubunFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShohinKubunFr.Location = new System.Drawing.Point(81, 29);
            this.lblShohinKubunFr.Name = "lblShohinKubunFr";
            this.lblShohinKubunFr.Size = new System.Drawing.Size(153, 20);
            this.lblShohinKubunFr.TabIndex = 1;
            this.lblShohinKubunFr.Text = "先　頭";
            this.lblShohinKubunFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShohinKubunTo
            // 
            this.lblShohinKubunTo.BackColor = System.Drawing.Color.Silver;
            this.lblShohinKubunTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShohinKubunTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShohinKubunTo.Location = new System.Drawing.Point(319, 29);
            this.lblShohinKubunTo.Name = "lblShohinKubunTo";
            this.lblShohinKubunTo.Size = new System.Drawing.Size(153, 20);
            this.lblShohinKubunTo.TabIndex = 4;
            this.lblShohinKubunTo.Text = "最　後";
            this.lblShohinKubunTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShohinKubunTo
            // 
            this.txtShohinKubunTo.AutoSizeFromLength = false;
            this.txtShohinKubunTo.DisplayLength = null;
            this.txtShohinKubunTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShohinKubunTo.Location = new System.Drawing.Point(263, 29);
            this.txtShohinKubunTo.MaxLength = 4;
            this.txtShohinKubunTo.Name = "txtShohinKubunTo";
            this.txtShohinKubunTo.Size = new System.Drawing.Size(50, 20);
            this.txtShohinKubunTo.TabIndex = 3;
            this.txtShohinKubunTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShohinKubunTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtShohinKubunTo_KeyDown);
            this.txtShohinKubunTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohinKubunTo_Validating);
            // 
            // txtShohinKubunFr
            // 
            this.txtShohinKubunFr.AutoSizeFromLength = false;
            this.txtShohinKubunFr.DisplayLength = null;
            this.txtShohinKubunFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShohinKubunFr.Location = new System.Drawing.Point(25, 29);
            this.txtShohinKubunFr.MaxLength = 4;
            this.txtShohinKubunFr.Name = "txtShohinKubunFr";
            this.txtShohinKubunFr.Size = new System.Drawing.Size(50, 20);
            this.txtShohinKubunFr.TabIndex = 0;
            this.txtShohinKubunFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShohinKubunFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohinKubunFr_Validating);
            // 
            // gbxZei
            // 
            this.gbxZei.Controls.Add(this.rdoZeikomi);
            this.gbxZei.Controls.Add(this.rdoZeinuki);
            this.gbxZei.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxZei.ForeColor = System.Drawing.SystemColors.ControlText;
            this.gbxZei.Location = new System.Drawing.Point(520, 198);
            this.gbxZei.Name = "gbxZei";
            this.gbxZei.Size = new System.Drawing.Size(186, 67);
            this.gbxZei.TabIndex = 2;
            this.gbxZei.TabStop = false;
            // 
            // rdoZeikomi
            // 
            this.rdoZeikomi.AutoSize = true;
            this.rdoZeikomi.BackColor = System.Drawing.Color.Transparent;
            this.rdoZeikomi.Location = new System.Drawing.Point(101, 27);
            this.rdoZeikomi.Name = "rdoZeikomi";
            this.rdoZeikomi.Size = new System.Drawing.Size(53, 17);
            this.rdoZeikomi.TabIndex = 1;
            this.rdoZeikomi.TabStop = true;
            this.rdoZeikomi.Text = "税込";
            this.rdoZeikomi.UseVisualStyleBackColor = false;
            // 
            // rdoZeinuki
            // 
            this.rdoZeinuki.AutoSize = true;
            this.rdoZeinuki.BackColor = System.Drawing.Color.Transparent;
            this.rdoZeinuki.Location = new System.Drawing.Point(18, 27);
            this.rdoZeinuki.Name = "rdoZeinuki";
            this.rdoZeinuki.Size = new System.Drawing.Size(53, 17);
            this.rdoZeinuki.TabIndex = 0;
            this.rdoZeinuki.TabStop = true;
            this.rdoZeinuki.Text = "税抜";
            this.rdoZeinuki.UseVisualStyleBackColor = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtMizuageShishoCd);
            this.groupBox1.Controls.Add(this.lblMizuageShishoNm);
            this.groupBox1.Controls.Add(this.lblMizuageShisho);
            this.groupBox1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBox1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox1.Location = new System.Drawing.Point(18, 53);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(348, 66);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "支所";
            // 
            // txtMizuageShishoCd
            // 
            this.txtMizuageShishoCd.AutoSizeFromLength = true;
            this.txtMizuageShishoCd.DisplayLength = null;
            this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtMizuageShishoCd.Location = new System.Drawing.Point(71, 25);
            this.txtMizuageShishoCd.MaxLength = 4;
            this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
            this.txtMizuageShishoCd.Size = new System.Drawing.Size(34, 20);
            this.txtMizuageShishoCd.TabIndex = 908;
            this.txtMizuageShishoCd.TabStop = false;
            this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
            // 
            // lblMizuageShishoNm
            // 
            this.lblMizuageShishoNm.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMizuageShishoNm.Location = new System.Drawing.Point(106, 25);
            this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
            this.lblMizuageShishoNm.Size = new System.Drawing.Size(212, 20);
            this.lblMizuageShishoNm.TabIndex = 910;
            this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMizuageShisho
            // 
            this.lblMizuageShisho.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShisho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShisho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblMizuageShisho.Location = new System.Drawing.Point(29, 23);
            this.lblMizuageShisho.Name = "lblMizuageShisho";
            this.lblMizuageShisho.Size = new System.Drawing.Size(295, 25);
            this.lblMizuageShisho.TabIndex = 909;
            this.lblMizuageShisho.Text = "支所";
            this.lblMizuageShisho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.comboBox1);
            this.groupBox2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBox2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox2.Location = new System.Drawing.Point(347, 287);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(270, 62);
            this.groupBox2.TabIndex = 902;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "中止区分";
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "0: 全ての商品",
            "1: 取引有りの商品のみ",
            "2: 取引中止の商品のみ"});
            this.comboBox1.Location = new System.Drawing.Point(26, 25);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(222, 21);
            this.comboBox1.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.rdoAll);
            this.groupBox3.Controls.Add(this.rdoMonth);
            this.groupBox3.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBox3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox3.Location = new System.Drawing.Point(520, 125);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(186, 67);
            this.groupBox3.TabIndex = 903;
            this.groupBox3.TabStop = false;
            // 
            // rdoAll
            // 
            this.rdoAll.AutoSize = true;
            this.rdoAll.BackColor = System.Drawing.Color.Transparent;
            this.rdoAll.Location = new System.Drawing.Point(101, 27);
            this.rdoAll.Name = "rdoAll";
            this.rdoAll.Size = new System.Drawing.Size(53, 17);
            this.rdoAll.TabIndex = 1;
            this.rdoAll.TabStop = true;
            this.rdoAll.Text = "累計";
            this.rdoAll.UseVisualStyleBackColor = false;
            this.rdoAll.CheckedChanged += new System.EventHandler(this.rdoAll_CheckedChanged);
            // 
            // rdoMonth
            // 
            this.rdoMonth.AutoSize = true;
            this.rdoMonth.BackColor = System.Drawing.Color.Transparent;
            this.rdoMonth.Location = new System.Drawing.Point(18, 27);
            this.rdoMonth.Name = "rdoMonth";
            this.rdoMonth.Size = new System.Drawing.Size(53, 17);
            this.rdoMonth.TabIndex = 0;
            this.rdoMonth.TabStop = true;
            this.rdoMonth.Text = "月計";
            this.rdoMonth.UseVisualStyleBackColor = false;
            this.rdoMonth.CheckedChanged += new System.EventHandler(this.rdoMonth_CheckedChanged);
            // 
            // chkMode
            // 
            this.chkMode.AutoSize = true;
            this.chkMode.Checked = true;
            this.chkMode.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkMode.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.chkMode.Location = new System.Drawing.Point(43, 302);
            this.chkMode.Name = "chkMode";
            this.chkMode.Size = new System.Drawing.Size(152, 17);
            this.chkMode.TabIndex = 904;
            this.chkMode.Text = "商品明細を表示する";
            this.chkMode.UseVisualStyleBackColor = true;
            // 
            // KBMR1051
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(835, 634);
            this.Controls.Add(this.chkMode);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.gbxDate);
            this.Controls.Add(this.gbxShohinKubun);
            this.Controls.Add(this.gbxZei);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "KBMR1051";
            this.Text = "";
            this.Controls.SetChildIndex(this.gbxZei, 0);
            this.Controls.SetChildIndex(this.gbxShohinKubun, 0);
            this.Controls.SetChildIndex(this.gbxDate, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.Controls.SetChildIndex(this.groupBox2, 0);
            this.Controls.SetChildIndex(this.groupBox3, 0);
            this.Controls.SetChildIndex(this.chkMode, 0);
            this.pnlDebug.ResumeLayout(false);
            this.gbxDate.ResumeLayout(false);
            this.gbxDate.PerformLayout();
            this.gbxShohinKubun.ResumeLayout(false);
            this.gbxShohinKubun.PerformLayout();
            this.gbxZei.ResumeLayout(false);
            this.gbxZei.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gbxDate;
        private jp.co.fsi.common.controls.FsiTextBox txtJpYearFr;
        private System.Windows.Forms.Label lblJpFr;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.Label lblDateMonth;
        private System.Windows.Forms.Label lblDateYear;
        private jp.co.fsi.common.controls.FsiTextBox txtJpMonthFr;
        private System.Windows.Forms.GroupBox gbxShohinKubun;
        private System.Windows.Forms.Label lblShohinKubunFr;
        private jp.co.fsi.common.controls.FsiTextBox txtShohinKubunFr;
        private System.Windows.Forms.Label lblShohinKubunBet;
        private System.Windows.Forms.Label lblShohinKubunTo;
        private jp.co.fsi.common.controls.FsiTextBox txtShohinKubunTo;
        private System.Windows.Forms.GroupBox gbxZei;
        private System.Windows.Forms.RadioButton rdoZeikomi;
        private System.Windows.Forms.RadioButton rdoZeinuki;
        private System.Windows.Forms.GroupBox groupBox1;
        private common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private System.Windows.Forms.Label lblMizuageShisho;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private common.controls.FsiTextBox txtJpYearTo;
        private common.controls.FsiTextBox txtJpMonthTo;
        private System.Windows.Forms.Label lblJpTo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton rdoAll;
        private System.Windows.Forms.RadioButton rdoMonth;
        private System.Windows.Forms.CheckBox chkMode;
    }
}