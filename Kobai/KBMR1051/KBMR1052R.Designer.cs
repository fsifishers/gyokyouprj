﻿namespace jp.co.fsi.kb.kbmr1051
{
    /// <summary>
    /// KBMR1051R の概要の説明です。
    /// </summary>
    partial class KBMR1052R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(KBMR1052R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtToday = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDateTo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblJojun = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTyujun = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblGejun = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSekisu01 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSuryo01 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblKingaku01 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSekisu02 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSuryo02 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblKingaku02 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSekisu03 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblKingaku03 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.textBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblTitle01 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtGenkaKingaku5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.label23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label25 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label26 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label27 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label28 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label29 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtShohinCd = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtShohinNm = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtKurikoshiSuryo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtKurikoshiKingaku = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtKeitoKingaku = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTanaSuryo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtUkeireSuryo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtUkeireKingaku = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtKeitoSuryo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTanaKingaku = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGenkaSuryo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGenkaKingaku = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtUriSuryo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtUriKingaku = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line15 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line16 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line17 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line18 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line19 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line20 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line21 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line22 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line23 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.textBox4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.reportHeader1 = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.reportFooter1 = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line32 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line33 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line34 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line35 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line36 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line37 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line38 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line39 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtRptKriSu = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtRptKriKin = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtRptKeitoKin = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtRptTanaSu = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtRptUkeSu = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtRptUkeKin = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtRptTanaKin = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtRptGenkSu = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtRptGenkKin = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtRptUriSu = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtRptUriKin = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtRptKeitoSu = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line40 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtUkeSZei = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtKeitoSZei = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtUriSZei = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox47 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.groupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.groupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.textBox43 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox44 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line24 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line25 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line26 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line27 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line28 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line29 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line30 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line31 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtGrpKriSu = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGrpKriKin = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGrpKeitoKin = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGrpTanaSu = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGrpUkeSu = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGrpUkeKin = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGrpTanaKin = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGrpGenkSu = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGrpGenkKin = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGrpUriSu = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGrpUriKin = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGrpKeitoSu = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line51 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.groupHeader2 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.groupFooter2 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.line41 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.textBox5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line42 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line43 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line44 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line45 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line46 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line47 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line48 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line49 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtGenkaKingaku3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGenkaKingaku4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line50 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtToday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblJojun)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTyujun)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGejun)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSekisu01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSuryo01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKingaku01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSekisu02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSuryo02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKingaku02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSekisu03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKingaku03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGenkaKingaku5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShohinCd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShohinNm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKurikoshiSuryo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKurikoshiKingaku)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKeitoKingaku)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTanaSuryo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUkeireSuryo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUkeireKingaku)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKeitoSuryo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTanaKingaku)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGenkaSuryo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGenkaKingaku)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUriSuryo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUriKingaku)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRptKriSu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRptKriKin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRptKeitoKin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRptTanaSu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRptUkeSu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRptUkeKin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRptTanaKin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRptGenkSu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRptGenkKin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRptUriSu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRptUriKin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRptKeitoSu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUkeSZei)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKeitoSZei)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUriSZei)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGrpKriSu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGrpKriKin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGrpKeitoKin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGrpTanaSu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGrpUkeSu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGrpUkeKin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGrpTanaKin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGrpGenkSu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGrpGenkKin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGrpUriSu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGrpUriKin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGrpKeitoSu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGenkaKingaku3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGenkaKingaku4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblDate,
            this.txtToday,
            this.txtPage,
            this.lblPage,
            this.txtDateTo,
            this.lblJojun,
            this.lblTyujun,
            this.lblGejun,
            this.lblSekisu01,
            this.lblSuryo01,
            this.lblKingaku01,
            this.lblSekisu02,
            this.lblSuryo02,
            this.lblKingaku02,
            this.lblSekisu03,
            this.lblKingaku03,
            this.label2,
            this.line10,
            this.textBox1,
            this.lblTitle01,
            this.line13,
            this.txtGenkaKingaku5,
            this.label4,
            this.label5,
            this.label21,
            this.label22,
            this.line1,
            this.line2,
            this.label23,
            this.label24,
            this.label25,
            this.label26,
            this.label27,
            this.label28,
            this.label29,
            this.line4,
            this.line5,
            this.line6,
            this.line7,
            this.line8,
            this.line12,
            this.line14,
            this.label3});
            this.pageHeader.Height = 1.31519F;
            this.pageHeader.Name = "pageHeader";
            this.pageHeader.Format += new System.EventHandler(this.pageHeader_Format);
            // 
            // lblDate
            // 
            this.lblDate.Height = 0.2F;
            this.lblDate.HyperLink = null;
            this.lblDate.Left = 8.254332F;
            this.lblDate.Name = "lblDate";
            this.lblDate.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; ddo-char-set: 1";
            this.lblDate.Text = "作 成 日";
            this.lblDate.Top = 0.1212599F;
            this.lblDate.Width = 0.6299214F;
            // 
            // txtToday
            // 
            this.txtToday.DataField = "ITEM30";
            this.txtToday.Height = 0.2F;
            this.txtToday.Left = 8.962995F;
            this.txtToday.MultiLine = false;
            this.txtToday.Name = "txtToday";
            this.txtToday.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; ddo-char-set: 1";
            this.txtToday.Text = "gy年MM月dd日";
            this.txtToday.Top = 0.1212599F;
            this.txtToday.Width = 1.181103F;
            // 
            // txtPage
            // 
            this.txtPage.Height = 0.2F;
            this.txtPage.Left = 10.16929F;
            this.txtPage.Name = "txtPage";
            this.txtPage.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtPage.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtPage.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.txtPage.Text = "Page";
            this.txtPage.Top = 0.1212599F;
            this.txtPage.Width = 0.3748035F;
            // 
            // lblPage
            // 
            this.lblPage.Height = 0.2F;
            this.lblPage.HyperLink = null;
            this.lblPage.Left = 10.5441F;
            this.lblPage.Name = "lblPage";
            this.lblPage.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; ddo-char-set: 1";
            this.lblPage.Text = "頁";
            this.lblPage.Top = 0.1212599F;
            this.lblPage.Width = 0.1666665F;
            // 
            // txtDateTo
            // 
            this.txtDateTo.DataField = "ITEM01";
            this.txtDateTo.Height = 0.1728346F;
            this.txtDateTo.Left = 8.772836F;
            this.txtDateTo.MultiLine = false;
            this.txtDateTo.Name = "txtDateTo";
            this.txtDateTo.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-style: normal; font-weight: bold; tex" +
    "t-align: left; ddo-char-set: 1";
            this.txtDateTo.Text = "<税出力条件>";
            this.txtDateTo.Top = 0.5173229F;
            this.txtDateTo.Width = 1.337008F;
            // 
            // lblJojun
            // 
            this.lblJojun.Height = 0.1452756F;
            this.lblJojun.HyperLink = null;
            this.lblJojun.Left = 2.938583F;
            this.lblJojun.Name = "lblJojun";
            this.lblJojun.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; ddo" +
    "-char-set: 1";
            this.lblJojun.Text = "繰  越";
            this.lblJojun.Top = 0.7409449F;
            this.lblJojun.Width = 0.9720473F;
            // 
            // lblTyujun
            // 
            this.lblTyujun.Height = 0.1452756F;
            this.lblTyujun.HyperLink = null;
            this.lblTyujun.Left = 3.953937F;
            this.lblTyujun.Name = "lblTyujun";
            this.lblTyujun.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; ddo" +
    "-char-set: 1";
            this.lblTyujun.Text = "-  受  入  -";
            this.lblTyujun.Top = 0.7409449F;
            this.lblTyujun.Width = 0.96063F;
            // 
            // lblGejun
            // 
            this.lblGejun.Height = 0.1452756F;
            this.lblGejun.HyperLink = null;
            this.lblGejun.Left = 4.972835F;
            this.lblGejun.Name = "lblGejun";
            this.lblGejun.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; ddo" +
    "-char-set: 1";
            this.lblGejun.Text = "- うち系統 -";
            this.lblGejun.Top = 0.7409449F;
            this.lblGejun.Width = 0.9523621F;
            // 
            // lblSekisu01
            // 
            this.lblSekisu01.Height = 0.1574803F;
            this.lblSekisu01.HyperLink = null;
            this.lblSekisu01.Left = 0.7515749F;
            this.lblSekisu01.Name = "lblSekisu01";
            this.lblSekisu01.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; ddo" +
    "-char-set: 1";
            this.lblSekisu01.Text = "分類名";
            this.lblSekisu01.Top = 1.134252F;
            this.lblSekisu01.Width = 2.148032F;
            // 
            // lblSuryo01
            // 
            this.lblSuryo01.Height = 0.1574803F;
            this.lblSuryo01.HyperLink = null;
            this.lblSuryo01.Left = 0.08070867F;
            this.lblSuryo01.Name = "lblSuryo01";
            this.lblSuryo01.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; ddo" +
    "-char-set: 1";
            this.lblSuryo01.Text = "商品CD";
            this.lblSuryo01.Top = 0.7531497F;
            this.lblSuryo01.Visible = false;
            this.lblSuryo01.Width = 0.9326773F;
            // 
            // lblKingaku01
            // 
            this.lblKingaku01.Height = 0.1574803F;
            this.lblKingaku01.HyperLink = null;
            this.lblKingaku01.Left = 1.04685F;
            this.lblKingaku01.Name = "lblKingaku01";
            this.lblKingaku01.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; ddo" +
    "-char-set: 1";
            this.lblKingaku01.Text = "商品名";
            this.lblKingaku01.Top = 0.7409449F;
            this.lblKingaku01.Visible = false;
            this.lblKingaku01.Width = 1.806693F;
            // 
            // lblSekisu02
            // 
            this.lblSekisu02.Height = 0.1574803F;
            this.lblSekisu02.HyperLink = null;
            this.lblSekisu02.Left = 2.938583F;
            this.lblSekisu02.Name = "lblSekisu02";
            this.lblSekisu02.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; ddo" +
    "-char-set: 1";
            this.lblSekisu02.Text = "金  額";
            this.lblSekisu02.Top = 1.137008F;
            this.lblSekisu02.Width = 0.9720473F;
            // 
            // lblSuryo02
            // 
            this.lblSuryo02.Height = 0.1574803F;
            this.lblSuryo02.HyperLink = null;
            this.lblSuryo02.Left = 2.938583F;
            this.lblSuryo02.Name = "lblSuryo02";
            this.lblSuryo02.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; ddo" +
    "-char-set: 1";
            this.lblSuryo02.Text = "数  量";
            this.lblSuryo02.Top = 0.9362205F;
            this.lblSuryo02.Width = 0.9720473F;
            // 
            // lblKingaku02
            // 
            this.lblKingaku02.Height = 0.1574803F;
            this.lblKingaku02.HyperLink = null;
            this.lblKingaku02.Left = 3.953937F;
            this.lblKingaku02.Name = "lblKingaku02";
            this.lblKingaku02.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; ddo" +
    "-char-set: 1";
            this.lblKingaku02.Text = "金  額";
            this.lblKingaku02.Top = 1.137008F;
            this.lblKingaku02.Width = 0.9488187F;
            // 
            // lblSekisu03
            // 
            this.lblSekisu03.Height = 0.1574803F;
            this.lblSekisu03.HyperLink = null;
            this.lblSekisu03.Left = 3.953937F;
            this.lblSekisu03.Name = "lblSekisu03";
            this.lblSekisu03.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; ddo" +
    "-char-set: 1";
            this.lblSekisu03.Text = "数  量";
            this.lblSekisu03.Top = 0.9362205F;
            this.lblSekisu03.Width = 0.9488192F;
            // 
            // lblKingaku03
            // 
            this.lblKingaku03.Height = 0.1574803F;
            this.lblKingaku03.HyperLink = null;
            this.lblKingaku03.Left = 5.975985F;
            this.lblKingaku03.Name = "lblKingaku03";
            this.lblKingaku03.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; ddo" +
    "-char-set: 1";
            this.lblKingaku03.Text = "- 棚卸高 -";
            this.lblKingaku03.Top = 0.7409449F;
            this.lblKingaku03.Width = 0.9566927F;
            // 
            // label2
            // 
            this.label2.Height = 0.1452756F;
            this.label2.HyperLink = null;
            this.label2.Left = 7F;
            this.label2.Name = "label2";
            this.label2.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; ddo" +
    "-char-set: 1";
            this.label2.Text = "- 供給原価 -";
            this.label2.Top = 0.7409449F;
            this.label2.Width = 0.9358268F;
            // 
            // line10
            // 
            this.line10.Height = 0F;
            this.line10.Left = 0.08070867F;
            this.line10.LineWeight = 2F;
            this.line10.Name = "line10";
            this.line10.Top = 1.311417F;
            this.line10.Width = 10.62992F;
            this.line10.X1 = 0.08070867F;
            this.line10.X2 = 10.71063F;
            this.line10.Y1 = 1.311417F;
            this.line10.Y2 = 1.311417F;
            // 
            // textBox1
            // 
            this.textBox1.DataField = "ITEM03";
            this.textBox1.Height = 0.2F;
            this.textBox1.Left = 0.07677166F;
            this.textBox1.MultiLine = false;
            this.textBox1.Name = "textBox1";
            this.textBox1.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-style: normal; font-weight: normal; d" +
    "do-char-set: 128";
            this.textBox1.Text = "会社名";
            this.textBox1.Top = 0.07992126F;
            this.textBox1.Width = 2.97874F;
            // 
            // lblTitle01
            // 
            this.lblTitle01.Height = 0.2625984F;
            this.lblTitle01.HyperLink = null;
            this.lblTitle01.Left = 3.368988F;
            this.lblTitle01.Name = "lblTitle01";
            this.lblTitle01.Style = "font-family: ＭＳ 明朝; font-size: 18pt; font-weight: bold; text-align: center; ddo-c" +
    "har-set: 128";
            this.lblTitle01.Text = "＊＊＊ 事業集計一覧 ＊＊＊";
            this.lblTitle01.Top = 0.09409447F;
            this.lblTitle01.Width = 3.979134F;
            // 
            // line13
            // 
            this.line13.Height = 0F;
            this.line13.Left = 0.08070867F;
            this.line13.LineWeight = 1F;
            this.line13.Name = "line13";
            this.line13.Top = 0.7173229F;
            this.line13.Width = 10.62992F;
            this.line13.X1 = 0.08070867F;
            this.line13.X2 = 10.71063F;
            this.line13.Y1 = 0.7173229F;
            this.line13.Y2 = 0.7173229F;
            // 
            // txtGenkaKingaku5
            // 
            this.txtGenkaKingaku5.DataField = "ITEM02";
            this.txtGenkaKingaku5.Height = 0.2F;
            this.txtGenkaKingaku5.Left = 3.368988F;
            this.txtGenkaKingaku5.MultiLine = false;
            this.txtGenkaKingaku5.Name = "txtGenkaKingaku5";
            this.txtGenkaKingaku5.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-style: normal; font-weight: bold; tex" +
    "t-align: center; ddo-char-set: 128";
            this.txtGenkaKingaku5.Text = "出力対象期間";
            this.txtGenkaKingaku5.Top = 0.407874F;
            this.txtGenkaKingaku5.Width = 3.979134F;
            // 
            // label4
            // 
            this.label4.Height = 0.1574803F;
            this.label4.HyperLink = null;
            this.label4.Left = 0.1322835F;
            this.label4.Name = "label4";
            this.label4.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; ddo" +
    "-char-set: 1";
            this.label4.Text = "コード";
            this.label4.Top = 1.134252F;
            this.label4.Width = 0.5F;
            // 
            // label5
            // 
            this.label5.Height = 0.1574803F;
            this.label5.HyperLink = null;
            this.label5.Left = 4.972835F;
            this.label5.Name = "label5";
            this.label5.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; ddo" +
    "-char-set: 1";
            this.label5.Text = "金  額";
            this.label5.Top = 1.134252F;
            this.label5.Width = 0.9488187F;
            // 
            // label21
            // 
            this.label21.Height = 0.1574803F;
            this.label21.HyperLink = null;
            this.label21.Left = 4.972835F;
            this.label21.Name = "label21";
            this.label21.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; ddo" +
    "-char-set: 1";
            this.label21.Text = "数  量";
            this.label21.Top = 0.9334645F;
            this.label21.Width = 0.9488195F;
            // 
            // label22
            // 
            this.label22.Height = 0.1452756F;
            this.label22.HyperLink = null;
            this.label22.Left = 8.018111F;
            this.label22.Name = "label22";
            this.label22.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; ddo" +
    "-char-set: 1";
            this.label22.Text = "- 供 給 高 -";
            this.label22.Top = 0.7409449F;
            this.label22.Width = 0.935827F;
            // 
            // line1
            // 
            this.line1.Height = 0F;
            this.line1.Left = 0.08070867F;
            this.line1.LineWeight = 1F;
            this.line1.Name = "line1";
            this.line1.Top = 0.9153544F;
            this.line1.Width = 10.62992F;
            this.line1.X1 = 0.08070867F;
            this.line1.X2 = 10.71063F;
            this.line1.Y1 = 0.9153544F;
            this.line1.Y2 = 0.9153544F;
            // 
            // line2
            // 
            this.line2.Height = 0F;
            this.line2.Left = 0.08070867F;
            this.line2.LineWeight = 1F;
            this.line2.Name = "line2";
            this.line2.Top = 1.113386F;
            this.line2.Width = 10.62992F;
            this.line2.X1 = 0.08070867F;
            this.line2.X2 = 10.71063F;
            this.line2.Y1 = 1.113386F;
            this.line2.Y2 = 1.113386F;
            // 
            // label23
            // 
            this.label23.Height = 0.1452756F;
            this.label23.HyperLink = null;
            this.label23.Left = 9.064568F;
            this.label23.Name = "label23";
            this.label23.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; ddo" +
    "-char-set: 1";
            this.label23.Text = "備　　　考";
            this.label23.Top = 0.7531497F;
            this.label23.Width = 1.565354F;
            // 
            // label24
            // 
            this.label24.Height = 0.1574803F;
            this.label24.HyperLink = null;
            this.label24.Left = 5.975985F;
            this.label24.Name = "label24";
            this.label24.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; ddo" +
    "-char-set: 1";
            this.label24.Text = "金  額";
            this.label24.Top = 1.134252F;
            this.label24.Width = 0.9488187F;
            // 
            // label25
            // 
            this.label25.Height = 0.1574803F;
            this.label25.HyperLink = null;
            this.label25.Left = 5.975985F;
            this.label25.Name = "label25";
            this.label25.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; ddo" +
    "-char-set: 1";
            this.label25.Text = "数  量";
            this.label25.Top = 0.9334646F;
            this.label25.Width = 0.9488195F;
            // 
            // label26
            // 
            this.label26.Height = 0.1574803F;
            this.label26.HyperLink = null;
            this.label26.Left = 7F;
            this.label26.Name = "label26";
            this.label26.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; ddo" +
    "-char-set: 1";
            this.label26.Text = "金  額";
            this.label26.Top = 1.137008F;
            this.label26.Width = 0.9488187F;
            // 
            // label27
            // 
            this.label27.Height = 0.1574803F;
            this.label27.HyperLink = null;
            this.label27.Left = 7F;
            this.label27.Name = "label27";
            this.label27.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; ddo" +
    "-char-set: 1";
            this.label27.Text = "数  量";
            this.label27.Top = 0.9362205F;
            this.label27.Width = 0.9488195F;
            // 
            // label28
            // 
            this.label28.Height = 0.1574803F;
            this.label28.HyperLink = null;
            this.label28.Left = 8.018111F;
            this.label28.Name = "label28";
            this.label28.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; ddo" +
    "-char-set: 1";
            this.label28.Text = "金  額";
            this.label28.Top = 1.137008F;
            this.label28.Width = 0.9488187F;
            // 
            // label29
            // 
            this.label29.Height = 0.1574803F;
            this.label29.HyperLink = null;
            this.label29.Left = 8.018111F;
            this.label29.Name = "label29";
            this.label29.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; ddo" +
    "-char-set: 1";
            this.label29.Text = "数  量";
            this.label29.Top = 0.9362205F;
            this.label29.Width = 0.9488195F;
            // 
            // line4
            // 
            this.line4.Height = 0.5940778F;
            this.line4.Left = 2.927166F;
            this.line4.LineWeight = 1F;
            this.line4.Name = "line4";
            this.line4.Top = 0.7173392F;
            this.line4.Width = 0F;
            this.line4.X1 = 2.927166F;
            this.line4.X2 = 2.927166F;
            this.line4.Y1 = 1.311417F;
            this.line4.Y2 = 0.7173392F;
            // 
            // line5
            // 
            this.line5.Height = 0.594078F;
            this.line5.Left = 3.942126F;
            this.line5.LineWeight = 1F;
            this.line5.Name = "line5";
            this.line5.Top = 0.717339F;
            this.line5.Width = 0F;
            this.line5.X1 = 3.942126F;
            this.line5.X2 = 3.942126F;
            this.line5.Y1 = 1.311417F;
            this.line5.Y2 = 0.717339F;
            // 
            // line6
            // 
            this.line6.Height = 0.594078F;
            this.line6.Left = 4.949213F;
            this.line6.LineWeight = 1F;
            this.line6.Name = "line6";
            this.line6.Top = 0.717339F;
            this.line6.Width = 0F;
            this.line6.X1 = 4.949213F;
            this.line6.X2 = 4.949213F;
            this.line6.Y1 = 1.311417F;
            this.line6.Y2 = 0.717339F;
            // 
            // line7
            // 
            this.line7.Height = 0.594078F;
            this.line7.Left = 5.947245F;
            this.line7.LineWeight = 1F;
            this.line7.Name = "line7";
            this.line7.Top = 0.717339F;
            this.line7.Width = 0F;
            this.line7.X1 = 5.947245F;
            this.line7.X2 = 5.947245F;
            this.line7.Y1 = 1.311417F;
            this.line7.Y2 = 0.717339F;
            // 
            // line8
            // 
            this.line8.Height = 0.594078F;
            this.line8.Left = 7.986221F;
            this.line8.LineWeight = 1F;
            this.line8.Name = "line8";
            this.line8.Top = 0.717339F;
            this.line8.Width = 0F;
            this.line8.X1 = 7.986221F;
            this.line8.X2 = 7.986221F;
            this.line8.Y1 = 1.311417F;
            this.line8.Y2 = 0.717339F;
            // 
            // line12
            // 
            this.line12.Height = 0.594078F;
            this.line12.Left = 6.971261F;
            this.line12.LineWeight = 1F;
            this.line12.Name = "line12";
            this.line12.Top = 0.717339F;
            this.line12.Width = 0F;
            this.line12.X1 = 6.971261F;
            this.line12.X2 = 6.971261F;
            this.line12.Y1 = 1.311417F;
            this.line12.Y2 = 0.717339F;
            // 
            // line14
            // 
            this.line14.Height = 0.594078F;
            this.line14.Left = 8.993308F;
            this.line14.LineWeight = 1F;
            this.line14.Name = "line14";
            this.line14.Top = 0.717339F;
            this.line14.Width = 0F;
            this.line14.X1 = 8.993308F;
            this.line14.X2 = 8.993308F;
            this.line14.Y1 = 1.311417F;
            this.line14.Y2 = 0.717339F;
            // 
            // label3
            // 
            this.label3.Height = 0.1688977F;
            this.label3.HyperLink = null;
            this.label3.Left = 10.10669F;
            this.label3.Name = "label3";
            this.label3.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; ddo-char-set: 1";
            this.label3.Text = "単位：円";
            this.label3.Top = 0.5212599F;
            this.label3.Width = 0.6039371F;
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.line3,
            this.txtShohinCd,
            this.txtShohinNm,
            this.txtKurikoshiSuryo,
            this.txtKurikoshiKingaku,
            this.txtKeitoKingaku,
            this.txtTanaSuryo,
            this.txtUkeireSuryo,
            this.txtUkeireKingaku,
            this.txtKeitoSuryo,
            this.txtTanaKingaku,
            this.txtGenkaSuryo,
            this.txtGenkaKingaku,
            this.txtUriSuryo,
            this.txtUriKingaku,
            this.line15,
            this.line16,
            this.line17,
            this.line18,
            this.line19,
            this.line20,
            this.line21,
            this.line22,
            this.line23,
            this.textBox4,
            this.textBox7,
            this.textBox8,
            this.textBox9});
            this.detail.Height = 0.3717574F;
            this.detail.KeepTogether = true;
            this.detail.Name = "detail";
            this.detail.Visible = false;
            this.detail.Format += new System.EventHandler(this.detail_Format);
            // 
            // line3
            // 
            this.line3.Height = 0F;
            this.line3.Left = 0.08070867F;
            this.line3.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.line3.LineWeight = 1F;
            this.line3.Name = "line3";
            this.line3.Top = 0.1980315F;
            this.line3.Width = 10.62992F;
            this.line3.X1 = 0.08070867F;
            this.line3.X2 = 10.71063F;
            this.line3.Y1 = 0.1980315F;
            this.line3.Y2 = 0.1980315F;
            // 
            // txtShohinCd
            // 
            this.txtShohinCd.DataField = "ITEM04";
            this.txtShohinCd.Height = 0.1688977F;
            this.txtShohinCd.Left = 0.07677166F;
            this.txtShohinCd.Name = "txtShohinCd";
            this.txtShohinCd.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtShohinCd.Tag = "商品コード";
            this.txtShohinCd.Text = "ITEM04";
            this.txtShohinCd.Top = 0.01181102F;
            this.txtShohinCd.Width = 0.9326772F;
            // 
            // txtShohinNm
            // 
            this.txtShohinNm.CanGrow = false;
            this.txtShohinNm.DataField = "ITEM05";
            this.txtShohinNm.Height = 0.1688977F;
            this.txtShohinNm.Left = 1.072047F;
            this.txtShohinNm.MultiLine = false;
            this.txtShohinNm.Name = "txtShohinNm";
            this.txtShohinNm.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; white-space: nowrap; ddo-char-set: 1; ddo-" +
    "wrap-mode: nowrap";
            this.txtShohinNm.Tag = "商品名";
            this.txtShohinNm.Text = "ITEM05";
            this.txtShohinNm.Top = 0.01181102F;
            this.txtShohinNm.Width = 1.827559F;
            // 
            // txtKurikoshiSuryo
            // 
            this.txtKurikoshiSuryo.DataField = "ITEM06";
            this.txtKurikoshiSuryo.Height = 0.1688976F;
            this.txtKurikoshiSuryo.Left = 2.973228F;
            this.txtKurikoshiSuryo.Name = "txtKurikoshiSuryo";
            this.txtKurikoshiSuryo.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtKurikoshiSuryo.Text = "05";
            this.txtKurikoshiSuryo.Top = 0.01181102F;
            this.txtKurikoshiSuryo.Width = 0.9374016F;
            // 
            // txtKurikoshiKingaku
            // 
            this.txtKurikoshiKingaku.DataField = "ITEM07";
            this.txtKurikoshiKingaku.Height = 0.1688976F;
            this.txtKurikoshiKingaku.Left = 2.973228F;
            this.txtKurikoshiKingaku.Name = "txtKurikoshiKingaku";
            this.txtKurikoshiKingaku.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtKurikoshiKingaku.Text = "05";
            this.txtKurikoshiKingaku.Top = 0.211811F;
            this.txtKurikoshiKingaku.Width = 0.9374014F;
            // 
            // txtKeitoKingaku
            // 
            this.txtKeitoKingaku.DataField = "ITEM11";
            this.txtKeitoKingaku.Height = 0.1688976F;
            this.txtKeitoKingaku.Left = 4.984252F;
            this.txtKeitoKingaku.Name = "txtKeitoKingaku";
            this.txtKeitoKingaku.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtKeitoKingaku.Text = "10";
            this.txtKeitoKingaku.Top = 0.207874F;
            this.txtKeitoKingaku.Width = 0.9374016F;
            // 
            // txtTanaSuryo
            // 
            this.txtTanaSuryo.DataField = "ITEM12";
            this.txtTanaSuryo.Height = 0.1688976F;
            this.txtTanaSuryo.Left = 5.987402F;
            this.txtTanaSuryo.Name = "txtTanaSuryo";
            this.txtTanaSuryo.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtTanaSuryo.Text = "11";
            this.txtTanaSuryo.Top = 0.01181102F;
            this.txtTanaSuryo.Width = 0.9374016F;
            // 
            // txtUkeireSuryo
            // 
            this.txtUkeireSuryo.DataField = "ITEM08";
            this.txtUkeireSuryo.Height = 0.1688976F;
            this.txtUkeireSuryo.Left = 3.965355F;
            this.txtUkeireSuryo.Name = "txtUkeireSuryo";
            this.txtUkeireSuryo.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtUkeireSuryo.Text = "07";
            this.txtUkeireSuryo.Top = 0.01181102F;
            this.txtUkeireSuryo.Width = 0.9374016F;
            // 
            // txtUkeireKingaku
            // 
            this.txtUkeireKingaku.DataField = "ITEM09";
            this.txtUkeireKingaku.Height = 0.1688976F;
            this.txtUkeireKingaku.Left = 3.965355F;
            this.txtUkeireKingaku.Name = "txtUkeireKingaku";
            this.txtUkeireKingaku.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtUkeireKingaku.Text = "08";
            this.txtUkeireKingaku.Top = 0.2043307F;
            this.txtUkeireKingaku.Width = 0.9374016F;
            // 
            // txtKeitoSuryo
            // 
            this.txtKeitoSuryo.DataField = "ITEM10";
            this.txtKeitoSuryo.Height = 0.1688976F;
            this.txtKeitoSuryo.Left = 4.984252F;
            this.txtKeitoSuryo.Name = "txtKeitoSuryo";
            this.txtKeitoSuryo.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtKeitoSuryo.Text = "09";
            this.txtKeitoSuryo.Top = 0.01181102F;
            this.txtKeitoSuryo.Width = 0.9374016F;
            // 
            // txtTanaKingaku
            // 
            this.txtTanaKingaku.DataField = "ITEM13";
            this.txtTanaKingaku.Height = 0.1688976F;
            this.txtTanaKingaku.Left = 5.987402F;
            this.txtTanaKingaku.Name = "txtTanaKingaku";
            this.txtTanaKingaku.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtTanaKingaku.Text = "12";
            this.txtTanaKingaku.Top = 0.207874F;
            this.txtTanaKingaku.Width = 0.9374016F;
            // 
            // txtGenkaSuryo
            // 
            this.txtGenkaSuryo.Height = 0.1688976F;
            this.txtGenkaSuryo.Left = 7.011418F;
            this.txtGenkaSuryo.Name = "txtGenkaSuryo";
            this.txtGenkaSuryo.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtGenkaSuryo.Text = "11";
            this.txtGenkaSuryo.Top = 0.01181102F;
            this.txtGenkaSuryo.Width = 0.9374014F;
            // 
            // txtGenkaKingaku
            // 
            this.txtGenkaKingaku.Height = 0.1688976F;
            this.txtGenkaKingaku.Left = 7.011418F;
            this.txtGenkaKingaku.Name = "txtGenkaKingaku";
            this.txtGenkaKingaku.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtGenkaKingaku.Text = "12";
            this.txtGenkaKingaku.Top = 0.211811F;
            this.txtGenkaKingaku.Width = 0.9374014F;
            // 
            // txtUriSuryo
            // 
            this.txtUriSuryo.DataField = "ITEM16";
            this.txtUriSuryo.Height = 0.1688976F;
            this.txtUriSuryo.Left = 8.029528F;
            this.txtUriSuryo.Name = "txtUriSuryo";
            this.txtUriSuryo.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtUriSuryo.Text = "11";
            this.txtUriSuryo.Top = 0.01338583F;
            this.txtUriSuryo.Width = 0.9374014F;
            // 
            // txtUriKingaku
            // 
            this.txtUriKingaku.DataField = "ITEM17";
            this.txtUriKingaku.Height = 0.1688976F;
            this.txtUriKingaku.Left = 8.029528F;
            this.txtUriKingaku.Name = "txtUriKingaku";
            this.txtUriKingaku.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtUriKingaku.Text = "12";
            this.txtUriKingaku.Top = 0.211811F;
            this.txtUriKingaku.Width = 0.9374014F;
            // 
            // line15
            // 
            this.line15.Height = 0F;
            this.line15.Left = 0.08070867F;
            this.line15.LineWeight = 1F;
            this.line15.Name = "line15";
            this.line15.Top = 0F;
            this.line15.Width = 10.62992F;
            this.line15.X1 = 0.08070867F;
            this.line15.X2 = 10.71063F;
            this.line15.Y1 = 0F;
            this.line15.Y2 = 0F;
            // 
            // line16
            // 
            this.line16.Height = 0F;
            this.line16.Left = 0.08070867F;
            this.line16.LineWeight = 1F;
            this.line16.Name = "line16";
            this.line16.Top = 0.396063F;
            this.line16.Width = 10.62992F;
            this.line16.X1 = 0.08070867F;
            this.line16.X2 = 10.71063F;
            this.line16.Y1 = 0.396063F;
            this.line16.Y2 = 0.396063F;
            // 
            // line17
            // 
            this.line17.Height = 0.3960469F;
            this.line17.Left = 2.927166F;
            this.line17.LineWeight = 1F;
            this.line17.Name = "line17";
            this.line17.Top = 1.609325E-05F;
            this.line17.Width = 0F;
            this.line17.X1 = 2.927166F;
            this.line17.X2 = 2.927166F;
            this.line17.Y1 = 0.396063F;
            this.line17.Y2 = 1.609325E-05F;
            // 
            // line18
            // 
            this.line18.Height = 0.3960468F;
            this.line18.Left = 3.942126F;
            this.line18.LineWeight = 1F;
            this.line18.Name = "line18";
            this.line18.Top = 1.615286E-05F;
            this.line18.Width = 0F;
            this.line18.X1 = 3.942126F;
            this.line18.X2 = 3.942126F;
            this.line18.Y1 = 0.396063F;
            this.line18.Y2 = 1.615286E-05F;
            // 
            // line19
            // 
            this.line19.Height = 0.3960468F;
            this.line19.Left = 4.949213F;
            this.line19.LineWeight = 1F;
            this.line19.Name = "line19";
            this.line19.Top = 1.615286E-05F;
            this.line19.Width = 0F;
            this.line19.X1 = 4.949213F;
            this.line19.X2 = 4.949213F;
            this.line19.Y1 = 0.396063F;
            this.line19.Y2 = 1.615286E-05F;
            // 
            // line20
            // 
            this.line20.Height = 0.3960468F;
            this.line20.Left = 5.947245F;
            this.line20.LineWeight = 1F;
            this.line20.Name = "line20";
            this.line20.Top = 1.615286E-05F;
            this.line20.Width = 0F;
            this.line20.X1 = 5.947245F;
            this.line20.X2 = 5.947245F;
            this.line20.Y1 = 0.396063F;
            this.line20.Y2 = 1.615286E-05F;
            // 
            // line21
            // 
            this.line21.Height = 0.3960468F;
            this.line21.Left = 6.971261F;
            this.line21.LineWeight = 1F;
            this.line21.Name = "line21";
            this.line21.Top = 1.615286E-05F;
            this.line21.Width = 0F;
            this.line21.X1 = 6.971261F;
            this.line21.X2 = 6.971261F;
            this.line21.Y1 = 0.396063F;
            this.line21.Y2 = 1.615286E-05F;
            // 
            // line22
            // 
            this.line22.Height = 0.3960468F;
            this.line22.Left = 7.986221F;
            this.line22.LineWeight = 1F;
            this.line22.Name = "line22";
            this.line22.Top = 1.615286E-05F;
            this.line22.Width = 0F;
            this.line22.X1 = 7.986221F;
            this.line22.X2 = 7.986221F;
            this.line22.Y1 = 0.396063F;
            this.line22.Y2 = 1.615286E-05F;
            // 
            // line23
            // 
            this.line23.Height = 0.3960468F;
            this.line23.Left = 8.993308F;
            this.line23.LineWeight = 1F;
            this.line23.Name = "line23";
            this.line23.Top = 1.615286E-05F;
            this.line23.Width = 0F;
            this.line23.X1 = 8.993308F;
            this.line23.X2 = 8.993308F;
            this.line23.Y1 = 0.396063F;
            this.line23.Y2 = 1.615286E-05F;
            // 
            // textBox4
            // 
            this.textBox4.CanGrow = false;
            this.textBox4.DataField = "ITEM18";
            this.textBox4.Height = 0.1688976F;
            this.textBox4.Left = 9.064568F;
            this.textBox4.MultiLine = false;
            this.textBox4.Name = "textBox4";
            this.textBox4.OutputFormat = resources.GetString("textBox4.OutputFormat");
            this.textBox4.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; white-space: nowrap; dd" +
    "o-char-set: 1; ddo-wrap-mode: nowrap";
            this.textBox4.Text = "受入消費税";
            this.textBox4.Top = 0.01338583F;
            this.textBox4.Visible = false;
            this.textBox4.Width = 0.9374014F;
            // 
            // textBox7
            // 
            this.textBox7.CanGrow = false;
            this.textBox7.DataField = "ITEM19";
            this.textBox7.Height = 0.1688976F;
            this.textBox7.Left = 9.66063F;
            this.textBox7.MultiLine = false;
            this.textBox7.Name = "textBox7";
            this.textBox7.OutputFormat = resources.GetString("textBox7.OutputFormat");
            this.textBox7.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; white-space: nowrap; dd" +
    "o-char-set: 1; ddo-wrap-mode: nowrap";
            this.textBox7.Text = "系統消費税";
            this.textBox7.Top = 0.01338583F;
            this.textBox7.Visible = false;
            this.textBox7.Width = 0.9374014F;
            // 
            // textBox8
            // 
            this.textBox8.CanGrow = false;
            this.textBox8.DataField = "ITEM20";
            this.textBox8.Height = 0.1688976F;
            this.textBox8.Left = 9.064568F;
            this.textBox8.MultiLine = false;
            this.textBox8.Name = "textBox8";
            this.textBox8.OutputFormat = resources.GetString("textBox8.OutputFormat");
            this.textBox8.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; white-space: nowrap; dd" +
    "o-char-set: 1; ddo-wrap-mode: nowrap";
            this.textBox8.Tag = "売上消費税";
            this.textBox8.Text = "払出消費税";
            this.textBox8.Top = 0.1980315F;
            this.textBox8.Visible = false;
            this.textBox8.Width = 0.9374014F;
            // 
            // textBox9
            // 
            this.textBox9.CanGrow = false;
            this.textBox9.DataField = "ITEM21";
            this.textBox9.Height = 0.1688976F;
            this.textBox9.Left = 9.69252F;
            this.textBox9.MultiLine = false;
            this.textBox9.Name = "textBox9";
            this.textBox9.OutputFormat = resources.GetString("textBox9.OutputFormat");
            this.textBox9.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; white-space: nowrap; dd" +
    "o-char-set: 1; ddo-wrap-mode: nowrap";
            this.textBox9.Tag = "在庫金額";
            this.textBox9.Text = "ITEM21";
            this.textBox9.Top = 0.211811F;
            this.textBox9.Visible = false;
            this.textBox9.Width = 0.9374014F;
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            // 
            // reportHeader1
            // 
            this.reportHeader1.Height = 0F;
            this.reportHeader1.Name = "reportHeader1";
            // 
            // reportFooter1
            // 
            this.reportFooter1.BackColor = System.Drawing.Color.Silver;
            this.reportFooter1.CanGrow = false;
            this.reportFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.label9,
            this.line9,
            this.line32,
            this.line33,
            this.line34,
            this.line35,
            this.line36,
            this.line37,
            this.line38,
            this.line39,
            this.txtRptKriSu,
            this.txtRptKriKin,
            this.txtRptKeitoKin,
            this.txtRptTanaSu,
            this.txtRptUkeSu,
            this.txtRptUkeKin,
            this.txtRptTanaKin,
            this.txtRptGenkSu,
            this.txtRptGenkKin,
            this.txtRptUriSu,
            this.txtRptUriKin,
            this.txtRptKeitoSu,
            this.line40,
            this.txtUkeSZei,
            this.txtKeitoSZei,
            this.txtUriSZei,
            this.label7,
            this.label8,
            this.textBox47});
            this.reportFooter1.Height = 0.6003937F;
            this.reportFooter1.KeepTogether = true;
            this.reportFooter1.Name = "reportFooter1";
            this.reportFooter1.Format += new System.EventHandler(this.reportFooter1_Format);
            // 
            // label9
            // 
            this.label9.Height = 0.1543307F;
            this.label9.HyperLink = null;
            this.label9.Left = 1.072047F;
            this.label9.Name = "label9";
            this.label9.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; ddo" +
    "-char-set: 128";
            this.label9.Text = "総 合 計";
            this.label9.Top = 0.4244095F;
            this.label9.Width = 1.417323F;
            // 
            // line9
            // 
            this.line9.Height = 0F;
            this.line9.Left = 0.07677166F;
            this.line9.LineWeight = 2F;
            this.line9.Name = "line9";
            this.line9.Top = 0F;
            this.line9.Width = 10.62992F;
            this.line9.X1 = 0.07677166F;
            this.line9.X2 = 10.70669F;
            this.line9.Y1 = 0F;
            this.line9.Y2 = 0F;
            // 
            // line32
            // 
            this.line32.Height = 0.578724F;
            this.line32.Left = 2.927166F;
            this.line32.LineWeight = 1F;
            this.line32.Name = "line32";
            this.line32.Top = 1.621246E-05F;
            this.line32.Width = 9.536743E-07F;
            this.line32.X1 = 2.927166F;
            this.line32.X2 = 2.927167F;
            this.line32.Y1 = 0.5787402F;
            this.line32.Y2 = 1.621246E-05F;
            // 
            // line33
            // 
            this.line33.Height = 0.5787239F;
            this.line33.Left = 3.942127F;
            this.line33.LineWeight = 1F;
            this.line33.Name = "line33";
            this.line33.Top = 1.628697E-05F;
            this.line33.Width = 9.536743E-07F;
            this.line33.X1 = 3.942127F;
            this.line33.X2 = 3.942128F;
            this.line33.Y1 = 0.5787402F;
            this.line33.Y2 = 1.628697E-05F;
            // 
            // line34
            // 
            this.line34.Height = 0.5787239F;
            this.line34.Left = 4.949213F;
            this.line34.LineWeight = 1F;
            this.line34.Name = "line34";
            this.line34.Top = 1.627207E-05F;
            this.line34.Width = 1.907349E-06F;
            this.line34.X1 = 4.949213F;
            this.line34.X2 = 4.949215F;
            this.line34.Y1 = 0.5787402F;
            this.line34.Y2 = 1.627207E-05F;
            // 
            // line35
            // 
            this.line35.Height = 0.5787239F;
            this.line35.Left = 5.947245F;
            this.line35.LineWeight = 1F;
            this.line35.Name = "line35";
            this.line35.Top = 1.627207E-05F;
            this.line35.Width = 1.907349E-06F;
            this.line35.X1 = 5.947245F;
            this.line35.X2 = 5.947247F;
            this.line35.Y1 = 0.5787402F;
            this.line35.Y2 = 1.627207E-05F;
            // 
            // line36
            // 
            this.line36.Height = 0.5787239F;
            this.line36.Left = 6.971264F;
            this.line36.LineWeight = 1F;
            this.line36.Name = "line36";
            this.line36.Top = 1.627207E-05F;
            this.line36.Width = 1.907349E-06F;
            this.line36.X1 = 6.971264F;
            this.line36.X2 = 6.971266F;
            this.line36.Y1 = 0.5787402F;
            this.line36.Y2 = 1.627207E-05F;
            // 
            // line37
            // 
            this.line37.Height = 0.5787239F;
            this.line37.Left = 7.986222F;
            this.line37.LineWeight = 1F;
            this.line37.Name = "line37";
            this.line37.Top = 1.628697E-05F;
            this.line37.Width = 2.384186E-06F;
            this.line37.X1 = 7.986222F;
            this.line37.X2 = 7.986224F;
            this.line37.Y1 = 0.5787402F;
            this.line37.Y2 = 1.628697E-05F;
            // 
            // line38
            // 
            this.line38.Height = 0.5787239F;
            this.line38.Left = 8.993306F;
            this.line38.LineWeight = 1F;
            this.line38.Name = "line38";
            this.line38.Top = 1.628697E-05F;
            this.line38.Width = 1.907349E-06F;
            this.line38.X1 = 8.993306F;
            this.line38.X2 = 8.993308F;
            this.line38.Y1 = 0.5787402F;
            this.line38.Y2 = 1.628697E-05F;
            // 
            // line39
            // 
            this.line39.Height = 0F;
            this.line39.Left = 2.927167F;
            this.line39.LineWeight = 1F;
            this.line39.Name = "line39";
            this.line39.Top = 0.2114173F;
            this.line39.Width = 6.066141F;
            this.line39.X1 = 2.927167F;
            this.line39.X2 = 8.993308F;
            this.line39.Y1 = 0.2114173F;
            this.line39.Y2 = 0.2114173F;
            // 
            // txtRptKriSu
            // 
            this.txtRptKriSu.DataField = "ITEM06";
            this.txtRptKriSu.Height = 0.1688976F;
            this.txtRptKriSu.Left = 2.973228F;
            this.txtRptKriSu.Name = "txtRptKriSu";
            this.txtRptKriSu.OutputFormat = resources.GetString("txtRptKriSu.OutputFormat");
            this.txtRptKriSu.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtRptKriSu.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtRptKriSu.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtRptKriSu.Text = "05";
            this.txtRptKriSu.Top = 0.01889764F;
            this.txtRptKriSu.Width = 0.9374014F;
            // 
            // txtRptKriKin
            // 
            this.txtRptKriKin.DataField = "ITEM07";
            this.txtRptKriKin.Height = 0.1688976F;
            this.txtRptKriKin.Left = 2.973228F;
            this.txtRptKriKin.Name = "txtRptKriKin";
            this.txtRptKriKin.OutputFormat = resources.GetString("txtRptKriKin.OutputFormat");
            this.txtRptKriKin.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtRptKriKin.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtRptKriKin.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtRptKriKin.Text = "06";
            this.txtRptKriKin.Top = 0.4314961F;
            this.txtRptKriKin.Width = 0.9374014F;
            // 
            // txtRptKeitoKin
            // 
            this.txtRptKeitoKin.DataField = "ITEM11";
            this.txtRptKeitoKin.Height = 0.1688976F;
            this.txtRptKeitoKin.Left = 4.984252F;
            this.txtRptKeitoKin.Name = "txtRptKeitoKin";
            this.txtRptKeitoKin.OutputFormat = resources.GetString("txtRptKeitoKin.OutputFormat");
            this.txtRptKeitoKin.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtRptKeitoKin.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtRptKeitoKin.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtRptKeitoKin.Text = "10";
            this.txtRptKeitoKin.Top = 0.4311024F;
            this.txtRptKeitoKin.Width = 0.9374014F;
            // 
            // txtRptTanaSu
            // 
            this.txtRptTanaSu.DataField = "ITEM12";
            this.txtRptTanaSu.Height = 0.1688976F;
            this.txtRptTanaSu.Left = 5.987401F;
            this.txtRptTanaSu.Name = "txtRptTanaSu";
            this.txtRptTanaSu.OutputFormat = resources.GetString("txtRptTanaSu.OutputFormat");
            this.txtRptTanaSu.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtRptTanaSu.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtRptTanaSu.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtRptTanaSu.Text = "11";
            this.txtRptTanaSu.Top = 0.01889764F;
            this.txtRptTanaSu.Width = 0.9374014F;
            // 
            // txtRptUkeSu
            // 
            this.txtRptUkeSu.DataField = "ITEM08";
            this.txtRptUkeSu.Height = 0.1688976F;
            this.txtRptUkeSu.Left = 3.965355F;
            this.txtRptUkeSu.Name = "txtRptUkeSu";
            this.txtRptUkeSu.OutputFormat = resources.GetString("txtRptUkeSu.OutputFormat");
            this.txtRptUkeSu.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtRptUkeSu.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtRptUkeSu.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtRptUkeSu.Text = "07";
            this.txtRptUkeSu.Top = 0.01889764F;
            this.txtRptUkeSu.Width = 0.9374014F;
            // 
            // txtRptUkeKin
            // 
            this.txtRptUkeKin.DataField = "ITEM09";
            this.txtRptUkeKin.Height = 0.1688976F;
            this.txtRptUkeKin.Left = 3.965355F;
            this.txtRptUkeKin.Name = "txtRptUkeKin";
            this.txtRptUkeKin.OutputFormat = resources.GetString("txtRptUkeKin.OutputFormat");
            this.txtRptUkeKin.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtRptUkeKin.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtRptUkeKin.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtRptUkeKin.Text = "08";
            this.txtRptUkeKin.Top = 0.4314961F;
            this.txtRptUkeKin.Width = 0.9374014F;
            // 
            // txtRptTanaKin
            // 
            this.txtRptTanaKin.DataField = "ITEM13";
            this.txtRptTanaKin.Height = 0.1688976F;
            this.txtRptTanaKin.Left = 5.987401F;
            this.txtRptTanaKin.Name = "txtRptTanaKin";
            this.txtRptTanaKin.OutputFormat = resources.GetString("txtRptTanaKin.OutputFormat");
            this.txtRptTanaKin.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtRptTanaKin.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtRptTanaKin.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtRptTanaKin.Text = "12";
            this.txtRptTanaKin.Top = 0.4311024F;
            this.txtRptTanaKin.Width = 0.9374014F;
            // 
            // txtRptGenkSu
            // 
            this.txtRptGenkSu.Height = 0.1688976F;
            this.txtRptGenkSu.Left = 7.011416F;
            this.txtRptGenkSu.Name = "txtRptGenkSu";
            this.txtRptGenkSu.OutputFormat = resources.GetString("txtRptGenkSu.OutputFormat");
            this.txtRptGenkSu.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtRptGenkSu.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtRptGenkSu.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtRptGenkSu.Text = "11";
            this.txtRptGenkSu.Top = 0.01889764F;
            this.txtRptGenkSu.Width = 0.9374014F;
            // 
            // txtRptGenkKin
            // 
            this.txtRptGenkKin.Height = 0.1688976F;
            this.txtRptGenkKin.Left = 7.011418F;
            this.txtRptGenkKin.Name = "txtRptGenkKin";
            this.txtRptGenkKin.OutputFormat = resources.GetString("txtRptGenkKin.OutputFormat");
            this.txtRptGenkKin.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtRptGenkKin.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtRptGenkKin.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtRptGenkKin.Text = "12";
            this.txtRptGenkKin.Top = 0.4314961F;
            this.txtRptGenkKin.Width = 0.9374014F;
            // 
            // txtRptUriSu
            // 
            this.txtRptUriSu.DataField = "ITEM16";
            this.txtRptUriSu.Height = 0.1688976F;
            this.txtRptUriSu.Left = 8.029528F;
            this.txtRptUriSu.Name = "txtRptUriSu";
            this.txtRptUriSu.OutputFormat = resources.GetString("txtRptUriSu.OutputFormat");
            this.txtRptUriSu.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtRptUriSu.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtRptUriSu.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtRptUriSu.Text = "11";
            this.txtRptUriSu.Top = 0.01889764F;
            this.txtRptUriSu.Width = 0.9374014F;
            // 
            // txtRptUriKin
            // 
            this.txtRptUriKin.DataField = "ITEM17";
            this.txtRptUriKin.Height = 0.1688976F;
            this.txtRptUriKin.Left = 8.029528F;
            this.txtRptUriKin.Name = "txtRptUriKin";
            this.txtRptUriKin.OutputFormat = resources.GetString("txtRptUriKin.OutputFormat");
            this.txtRptUriKin.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtRptUriKin.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtRptUriKin.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtRptUriKin.Text = "12";
            this.txtRptUriKin.Top = 0.4314961F;
            this.txtRptUriKin.Width = 0.9374014F;
            // 
            // txtRptKeitoSu
            // 
            this.txtRptKeitoSu.DataField = "ITEM10";
            this.txtRptKeitoSu.Height = 0.1688976F;
            this.txtRptKeitoSu.Left = 4.987796F;
            this.txtRptKeitoSu.Name = "txtRptKeitoSu";
            this.txtRptKeitoSu.OutputFormat = resources.GetString("txtRptKeitoSu.OutputFormat");
            this.txtRptKeitoSu.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtRptKeitoSu.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtRptKeitoSu.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtRptKeitoSu.Text = "09";
            this.txtRptKeitoSu.Top = 0.01889764F;
            this.txtRptKeitoSu.Width = 0.9374014F;
            // 
            // line40
            // 
            this.line40.Height = 0F;
            this.line40.Left = 0.07677166F;
            this.line40.LineWeight = 2F;
            this.line40.Name = "line40";
            this.line40.Top = 0.5787402F;
            this.line40.Width = 10.62992F;
            this.line40.X1 = 0.07677166F;
            this.line40.X2 = 10.70669F;
            this.line40.Y1 = 0.5787402F;
            this.line40.Y2 = 0.5787402F;
            // 
            // txtUkeSZei
            // 
            this.txtUkeSZei.CanGrow = false;
            this.txtUkeSZei.DataField = "ITEM18";
            this.txtUkeSZei.Height = 0.1688976F;
            this.txtUkeSZei.Left = 3.977166F;
            this.txtUkeSZei.MultiLine = false;
            this.txtUkeSZei.Name = "txtUkeSZei";
            this.txtUkeSZei.OutputFormat = resources.GetString("txtUkeSZei.OutputFormat");
            this.txtUkeSZei.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; white-space: nowrap; dd" +
    "o-char-set: 1; ddo-wrap-mode: nowrap";
            this.txtUkeSZei.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtUkeSZei.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtUkeSZei.Text = "受入消費税";
            this.txtUkeSZei.Top = 0.2311024F;
            this.txtUkeSZei.Width = 0.9374014F;
            // 
            // txtKeitoSZei
            // 
            this.txtKeitoSZei.CanGrow = false;
            this.txtKeitoSZei.DataField = "ITEM19";
            this.txtKeitoSZei.Height = 0.1688976F;
            this.txtKeitoSZei.Left = 4.987796F;
            this.txtKeitoSZei.MultiLine = false;
            this.txtKeitoSZei.Name = "txtKeitoSZei";
            this.txtKeitoSZei.OutputFormat = resources.GetString("txtKeitoSZei.OutputFormat");
            this.txtKeitoSZei.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; white-space: nowrap; dd" +
    "o-char-set: 1; ddo-wrap-mode: nowrap";
            this.txtKeitoSZei.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtKeitoSZei.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtKeitoSZei.Text = "系統消費税";
            this.txtKeitoSZei.Top = 0.2311024F;
            this.txtKeitoSZei.Width = 0.9374014F;
            // 
            // txtUriSZei
            // 
            this.txtUriSZei.CanGrow = false;
            this.txtUriSZei.DataField = "ITEM20";
            this.txtUriSZei.Height = 0.1688976F;
            this.txtUriSZei.Left = 8.029528F;
            this.txtUriSZei.MultiLine = false;
            this.txtUriSZei.Name = "txtUriSZei";
            this.txtUriSZei.OutputFormat = resources.GetString("txtUriSZei.OutputFormat");
            this.txtUriSZei.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; white-space: nowrap; dd" +
    "o-char-set: 1; ddo-wrap-mode: nowrap";
            this.txtUriSZei.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtUriSZei.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtUriSZei.Tag = "売上消費税";
            this.txtUriSZei.Text = "払出消費税";
            this.txtUriSZei.Top = 0.2240158F;
            this.txtUriSZei.Width = 0.9374014F;
            // 
            // label7
            // 
            this.label7.Height = 0.1543307F;
            this.label7.HyperLink = null;
            this.label7.Left = 2.48937F;
            this.label7.Name = "label7";
            this.label7.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: left";
            this.label7.Text = "＞＞";
            this.label7.Top = 0.4244095F;
            this.label7.Width = 0.3893702F;
            // 
            // label8
            // 
            this.label8.Height = 0.1543307F;
            this.label8.HyperLink = null;
            this.label8.Left = 0.6393701F;
            this.label8.Name = "label8";
            this.label8.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: right; ddo-" +
    "char-set: 128";
            this.label8.Text = "＜＜";
            this.label8.Top = 0.4244095F;
            this.label8.Width = 0.4145668F;
            // 
            // textBox47
            // 
            this.textBox47.DataField = "ITEM22";
            this.textBox47.Height = 0.1688977F;
            this.textBox47.Left = 1.946063F;
            this.textBox47.Name = "textBox47";
            this.textBox47.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; ddo-char-set: 1";
            this.textBox47.Tag = "消費税タイトル";
            this.textBox47.Text = "消費税名称";
            this.textBox47.Top = 0.2240158F;
            this.textBox47.Width = 0.9326771F;
            // 
            // groupHeader1
            // 
            this.groupHeader1.DataField = "ITEM23";
            this.groupHeader1.Height = 0F;
            this.groupHeader1.Name = "groupHeader1";
            // 
            // groupFooter1
            // 
            this.groupFooter1.BackColor = System.Drawing.Color.Silver;
            this.groupFooter1.CanGrow = false;
            this.groupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.textBox43,
            this.textBox44,
            this.label1,
            this.label6,
            this.line11,
            this.line24,
            this.line25,
            this.line26,
            this.line27,
            this.line28,
            this.line29,
            this.line30,
            this.line31,
            this.txtGrpKriSu,
            this.txtGrpKriKin,
            this.txtGrpKeitoKin,
            this.txtGrpTanaSu,
            this.txtGrpUkeSu,
            this.txtGrpUkeKin,
            this.txtGrpTanaKin,
            this.txtGrpGenkSu,
            this.txtGrpGenkKin,
            this.txtGrpUriSu,
            this.txtGrpUriKin,
            this.txtGrpKeitoSu,
            this.line51});
            this.groupFooter1.Height = 0.4030074F;
            this.groupFooter1.KeepTogether = true;
            this.groupFooter1.Name = "groupFooter1";
            this.groupFooter1.Format += new System.EventHandler(this.groupFooter1_Format);
            // 
            // textBox43
            // 
            this.textBox43.DataField = "ITEM23";
            this.textBox43.Height = 0.1688976F;
            this.textBox43.Left = 1.04685F;
            this.textBox43.MultiLine = false;
            this.textBox43.Name = "textBox43";
            this.textBox43.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; dd" +
    "o-char-set: 128";
            this.textBox43.Text = "ITEM23";
            this.textBox43.Top = 0.2110236F;
            this.textBox43.Width = 0.4775591F;
            // 
            // textBox44
            // 
            this.textBox44.DataField = "ITEM24";
            this.textBox44.Height = 0.1688976F;
            this.textBox44.Left = 1.541733F;
            this.textBox44.MultiLine = false;
            this.textBox44.Name = "textBox44";
            this.textBox44.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; ddo-char-set: 128";
            this.textBox44.Text = "ITEM24";
            this.textBox44.Top = 0.2110236F;
            this.textBox44.Width = 0.947638F;
            // 
            // label1
            // 
            this.label1.Height = 0.1688976F;
            this.label1.HyperLink = null;
            this.label1.Left = 0.6322835F;
            this.label1.Name = "label1";
            this.label1.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; dd" +
    "o-char-set: 128";
            this.label1.Text = "＜＜";
            this.label1.Top = 0.2110236F;
            this.label1.Width = 0.4145668F;
            // 
            // label6
            // 
            this.label6.Height = 0.1688976F;
            this.label6.HyperLink = null;
            this.label6.Left = 2.48937F;
            this.label6.Name = "label6";
            this.label6.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left";
            this.label6.Text = "＞＞";
            this.label6.Top = 0.2110236F;
            this.label6.Width = 0.3893702F;
            // 
            // line11
            // 
            this.line11.Height = 0F;
            this.line11.Left = 0.07677166F;
            this.line11.LineWeight = 1F;
            this.line11.Name = "line11";
            this.line11.Top = 0.396063F;
            this.line11.Width = 10.62992F;
            this.line11.X1 = 0.07677166F;
            this.line11.X2 = 10.70669F;
            this.line11.Y1 = 0.396063F;
            this.line11.Y2 = 0.396063F;
            // 
            // line24
            // 
            this.line24.Height = 0.3960468F;
            this.line24.Left = 2.927166F;
            this.line24.LineWeight = 1F;
            this.line24.Name = "line24";
            this.line24.Top = 1.615286E-05F;
            this.line24.Width = 0F;
            this.line24.X1 = 2.927166F;
            this.line24.X2 = 2.927166F;
            this.line24.Y1 = 0.396063F;
            this.line24.Y2 = 1.615286E-05F;
            // 
            // line25
            // 
            this.line25.Height = 0.3960468F;
            this.line25.Left = 3.942126F;
            this.line25.LineWeight = 1F;
            this.line25.Name = "line25";
            this.line25.Top = 1.622736E-05F;
            this.line25.Width = 0F;
            this.line25.X1 = 3.942126F;
            this.line25.X2 = 3.942126F;
            this.line25.Y1 = 0.396063F;
            this.line25.Y2 = 1.622736E-05F;
            // 
            // line26
            // 
            this.line26.Height = 0.3960468F;
            this.line26.Left = 4.949213F;
            this.line26.LineWeight = 1F;
            this.line26.Name = "line26";
            this.line26.Top = 1.621246E-05F;
            this.line26.Width = 0F;
            this.line26.X1 = 4.949213F;
            this.line26.X2 = 4.949213F;
            this.line26.Y1 = 0.396063F;
            this.line26.Y2 = 1.621246E-05F;
            // 
            // line27
            // 
            this.line27.Height = 0.3960468F;
            this.line27.Left = 5.947245F;
            this.line27.LineWeight = 1F;
            this.line27.Name = "line27";
            this.line27.Top = 1.621246E-05F;
            this.line27.Width = 0F;
            this.line27.X1 = 5.947245F;
            this.line27.X2 = 5.947245F;
            this.line27.Y1 = 0.396063F;
            this.line27.Y2 = 1.621246E-05F;
            // 
            // line28
            // 
            this.line28.Height = 0.3960468F;
            this.line28.Left = 6.971261F;
            this.line28.LineWeight = 1F;
            this.line28.Name = "line28";
            this.line28.Top = 1.621246E-05F;
            this.line28.Width = 0F;
            this.line28.X1 = 6.971261F;
            this.line28.X2 = 6.971261F;
            this.line28.Y1 = 0.396063F;
            this.line28.Y2 = 1.621246E-05F;
            // 
            // line29
            // 
            this.line29.Height = 0.3960468F;
            this.line29.Left = 7.986221F;
            this.line29.LineWeight = 1F;
            this.line29.Name = "line29";
            this.line29.Top = 1.622736E-05F;
            this.line29.Width = 0F;
            this.line29.X1 = 7.986221F;
            this.line29.X2 = 7.986221F;
            this.line29.Y1 = 0.396063F;
            this.line29.Y2 = 1.622736E-05F;
            // 
            // line30
            // 
            this.line30.Height = 0.3960468F;
            this.line30.Left = 8.993308F;
            this.line30.LineWeight = 1F;
            this.line30.Name = "line30";
            this.line30.Top = 1.622736E-05F;
            this.line30.Width = 0F;
            this.line30.X1 = 8.993308F;
            this.line30.X2 = 8.993308F;
            this.line30.Y1 = 0.396063F;
            this.line30.Y2 = 1.622736E-05F;
            // 
            // line31
            // 
            this.line31.Height = 0F;
            this.line31.Left = 2.927165F;
            this.line31.LineWeight = 1F;
            this.line31.Name = "line31";
            this.line31.Top = 0.2114173F;
            this.line31.Width = 6.066142F;
            this.line31.X1 = 2.927165F;
            this.line31.X2 = 8.993307F;
            this.line31.Y1 = 0.2114173F;
            this.line31.Y2 = 0.2114173F;
            // 
            // txtGrpKriSu
            // 
            this.txtGrpKriSu.DataField = "ITEM06";
            this.txtGrpKriSu.Height = 0.1688976F;
            this.txtGrpKriSu.Left = 2.973228F;
            this.txtGrpKriSu.Name = "txtGrpKriSu";
            this.txtGrpKriSu.OutputFormat = resources.GetString("txtGrpKriSu.OutputFormat");
            this.txtGrpKriSu.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtGrpKriSu.SummaryGroup = "groupHeader1";
            this.txtGrpKriSu.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtGrpKriSu.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtGrpKriSu.Text = "05";
            this.txtGrpKriSu.Top = 0.01574803F;
            this.txtGrpKriSu.Width = 0.9374014F;
            // 
            // txtGrpKriKin
            // 
            this.txtGrpKriKin.DataField = "ITEM07";
            this.txtGrpKriKin.Height = 0.1688976F;
            this.txtGrpKriKin.Left = 2.973228F;
            this.txtGrpKriKin.Name = "txtGrpKriKin";
            this.txtGrpKriKin.OutputFormat = resources.GetString("txtGrpKriKin.OutputFormat");
            this.txtGrpKriKin.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtGrpKriKin.SummaryGroup = "groupHeader1";
            this.txtGrpKriKin.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtGrpKriKin.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtGrpKriKin.Text = "06";
            this.txtGrpKriKin.Top = 0.2271654F;
            this.txtGrpKriKin.Width = 0.9374014F;
            // 
            // txtGrpKeitoKin
            // 
            this.txtGrpKeitoKin.DataField = "ITEM11";
            this.txtGrpKeitoKin.Height = 0.1688976F;
            this.txtGrpKeitoKin.Left = 4.984252F;
            this.txtGrpKeitoKin.Name = "txtGrpKeitoKin";
            this.txtGrpKeitoKin.OutputFormat = resources.GetString("txtGrpKeitoKin.OutputFormat");
            this.txtGrpKeitoKin.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtGrpKeitoKin.SummaryGroup = "groupHeader1";
            this.txtGrpKeitoKin.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtGrpKeitoKin.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtGrpKeitoKin.Text = "10";
            this.txtGrpKeitoKin.Top = 0.2271654F;
            this.txtGrpKeitoKin.Width = 0.9374014F;
            // 
            // txtGrpTanaSu
            // 
            this.txtGrpTanaSu.DataField = "ITEM12";
            this.txtGrpTanaSu.Height = 0.1688976F;
            this.txtGrpTanaSu.Left = 5.987401F;
            this.txtGrpTanaSu.Name = "txtGrpTanaSu";
            this.txtGrpTanaSu.OutputFormat = resources.GetString("txtGrpTanaSu.OutputFormat");
            this.txtGrpTanaSu.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtGrpTanaSu.SummaryGroup = "groupHeader1";
            this.txtGrpTanaSu.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtGrpTanaSu.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtGrpTanaSu.Text = "11";
            this.txtGrpTanaSu.Top = 0.01417323F;
            this.txtGrpTanaSu.Width = 0.9374014F;
            // 
            // txtGrpUkeSu
            // 
            this.txtGrpUkeSu.DataField = "ITEM08";
            this.txtGrpUkeSu.Height = 0.1688976F;
            this.txtGrpUkeSu.Left = 3.965355F;
            this.txtGrpUkeSu.Name = "txtGrpUkeSu";
            this.txtGrpUkeSu.OutputFormat = resources.GetString("txtGrpUkeSu.OutputFormat");
            this.txtGrpUkeSu.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtGrpUkeSu.SummaryGroup = "groupHeader1";
            this.txtGrpUkeSu.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtGrpUkeSu.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtGrpUkeSu.Text = "07";
            this.txtGrpUkeSu.Top = 0.01417323F;
            this.txtGrpUkeSu.Width = 0.9374014F;
            // 
            // txtGrpUkeKin
            // 
            this.txtGrpUkeKin.DataField = "ITEM09";
            this.txtGrpUkeKin.Height = 0.1688976F;
            this.txtGrpUkeKin.Left = 3.965355F;
            this.txtGrpUkeKin.Name = "txtGrpUkeKin";
            this.txtGrpUkeKin.OutputFormat = resources.GetString("txtGrpUkeKin.OutputFormat");
            this.txtGrpUkeKin.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtGrpUkeKin.SummaryGroup = "groupHeader1";
            this.txtGrpUkeKin.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtGrpUkeKin.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtGrpUkeKin.Text = "08";
            this.txtGrpUkeKin.Top = 0.2271654F;
            this.txtGrpUkeKin.Width = 0.9374014F;
            // 
            // txtGrpTanaKin
            // 
            this.txtGrpTanaKin.DataField = "ITEM13";
            this.txtGrpTanaKin.Height = 0.1688976F;
            this.txtGrpTanaKin.Left = 5.987402F;
            this.txtGrpTanaKin.Name = "txtGrpTanaKin";
            this.txtGrpTanaKin.OutputFormat = resources.GetString("txtGrpTanaKin.OutputFormat");
            this.txtGrpTanaKin.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtGrpTanaKin.SummaryGroup = "groupHeader1";
            this.txtGrpTanaKin.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtGrpTanaKin.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtGrpTanaKin.Text = "12";
            this.txtGrpTanaKin.Top = 0.2271654F;
            this.txtGrpTanaKin.Width = 0.9374014F;
            // 
            // txtGrpGenkSu
            // 
            this.txtGrpGenkSu.Height = 0.1688976F;
            this.txtGrpGenkSu.Left = 7.011416F;
            this.txtGrpGenkSu.Name = "txtGrpGenkSu";
            this.txtGrpGenkSu.OutputFormat = resources.GetString("txtGrpGenkSu.OutputFormat");
            this.txtGrpGenkSu.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtGrpGenkSu.SummaryGroup = "groupHeader1";
            this.txtGrpGenkSu.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtGrpGenkSu.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtGrpGenkSu.Text = "11";
            this.txtGrpGenkSu.Top = 0.01417323F;
            this.txtGrpGenkSu.Width = 0.9374014F;
            // 
            // txtGrpGenkKin
            // 
            this.txtGrpGenkKin.Height = 0.1688976F;
            this.txtGrpGenkKin.Left = 7.011418F;
            this.txtGrpGenkKin.Name = "txtGrpGenkKin";
            this.txtGrpGenkKin.OutputFormat = resources.GetString("txtGrpGenkKin.OutputFormat");
            this.txtGrpGenkKin.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtGrpGenkKin.SummaryGroup = "groupHeader1";
            this.txtGrpGenkKin.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtGrpGenkKin.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtGrpGenkKin.Text = "12";
            this.txtGrpGenkKin.Top = 0.2271654F;
            this.txtGrpGenkKin.Width = 0.9374014F;
            // 
            // txtGrpUriSu
            // 
            this.txtGrpUriSu.DataField = "ITEM16";
            this.txtGrpUriSu.Height = 0.1688976F;
            this.txtGrpUriSu.Left = 8.029528F;
            this.txtGrpUriSu.Name = "txtGrpUriSu";
            this.txtGrpUriSu.OutputFormat = resources.GetString("txtGrpUriSu.OutputFormat");
            this.txtGrpUriSu.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtGrpUriSu.SummaryGroup = "groupHeader1";
            this.txtGrpUriSu.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtGrpUriSu.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtGrpUriSu.Text = "11";
            this.txtGrpUriSu.Top = 0.01417323F;
            this.txtGrpUriSu.Width = 0.9374014F;
            // 
            // txtGrpUriKin
            // 
            this.txtGrpUriKin.DataField = "ITEM17";
            this.txtGrpUriKin.Height = 0.1688976F;
            this.txtGrpUriKin.Left = 8.029528F;
            this.txtGrpUriKin.Name = "txtGrpUriKin";
            this.txtGrpUriKin.OutputFormat = resources.GetString("txtGrpUriKin.OutputFormat");
            this.txtGrpUriKin.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtGrpUriKin.SummaryGroup = "groupHeader1";
            this.txtGrpUriKin.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtGrpUriKin.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtGrpUriKin.Text = "12";
            this.txtGrpUriKin.Top = 0.2271654F;
            this.txtGrpUriKin.Width = 0.9374014F;
            // 
            // txtGrpKeitoSu
            // 
            this.txtGrpKeitoSu.DataField = "ITEM10";
            this.txtGrpKeitoSu.Height = 0.1688976F;
            this.txtGrpKeitoSu.Left = 4.987796F;
            this.txtGrpKeitoSu.Name = "txtGrpKeitoSu";
            this.txtGrpKeitoSu.OutputFormat = resources.GetString("txtGrpKeitoSu.OutputFormat");
            this.txtGrpKeitoSu.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtGrpKeitoSu.SummaryGroup = "groupHeader1";
            this.txtGrpKeitoSu.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtGrpKeitoSu.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtGrpKeitoSu.Text = "09";
            this.txtGrpKeitoSu.Top = 0.01574803F;
            this.txtGrpKeitoSu.Width = 0.9374014F;
            // 
            // line51
            // 
            this.line51.Height = 0F;
            this.line51.Left = 0.08070867F;
            this.line51.LineWeight = 1F;
            this.line51.Name = "line51";
            this.line51.Top = 0F;
            this.line51.Width = 10.62992F;
            this.line51.X1 = 0.08070867F;
            this.line51.X2 = 10.71063F;
            this.line51.Y1 = 0F;
            this.line51.Y2 = 0F;
            // 
            // groupHeader2
            // 
            this.groupHeader2.DataField = "ITEM25";
            this.groupHeader2.Height = 0F;
            this.groupHeader2.Name = "groupHeader2";
            // 
            // groupFooter2
            // 
            this.groupFooter2.CanGrow = false;
            this.groupFooter2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.line41,
            this.textBox5,
            this.textBox6,
            this.textBox10,
            this.textBox11,
            this.textBox12,
            this.textBox13,
            this.textBox14,
            this.textBox15,
            this.textBox16,
            this.textBox17,
            this.textBox18,
            this.textBox19,
            this.line42,
            this.line43,
            this.line44,
            this.line45,
            this.line46,
            this.line47,
            this.line48,
            this.line49,
            this.txtGenkaKingaku3,
            this.txtGenkaKingaku4,
            this.line50});
            this.groupFooter2.Height = 0.4007874F;
            this.groupFooter2.KeepTogether = true;
            this.groupFooter2.Name = "groupFooter2";
            this.groupFooter2.Format += new System.EventHandler(this.groupFooter2_Format);
            // 
            // line41
            // 
            this.line41.Height = 0F;
            this.line41.Left = 0.08070867F;
            this.line41.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.line41.LineWeight = 1F;
            this.line41.Name = "line41";
            this.line41.Top = 0.1980315F;
            this.line41.Width = 10.62992F;
            this.line41.X1 = 0.08070867F;
            this.line41.X2 = 10.71063F;
            this.line41.Y1 = 0.1980315F;
            this.line41.Y2 = 0.1980315F;
            // 
            // textBox5
            // 
            this.textBox5.DataField = "ITEM06";
            this.textBox5.Height = 0.1688976F;
            this.textBox5.Left = 2.973228F;
            this.textBox5.Name = "textBox5";
            this.textBox5.OutputFormat = resources.GetString("textBox5.OutputFormat");
            this.textBox5.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox5.SummaryGroup = "groupHeader2";
            this.textBox5.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox5.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox5.Text = "05";
            this.textBox5.Top = 0.01181099F;
            this.textBox5.Width = 0.9374014F;
            // 
            // textBox6
            // 
            this.textBox6.DataField = "ITEM07";
            this.textBox6.Height = 0.1688976F;
            this.textBox6.Left = 2.973228F;
            this.textBox6.Name = "textBox6";
            this.textBox6.OutputFormat = resources.GetString("textBox6.OutputFormat");
            this.textBox6.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox6.SummaryGroup = "groupHeader2";
            this.textBox6.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox6.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox6.Text = "05";
            this.textBox6.Top = 0.2118109F;
            this.textBox6.Width = 0.9374014F;
            // 
            // textBox10
            // 
            this.textBox10.DataField = "ITEM11";
            this.textBox10.Height = 0.1688976F;
            this.textBox10.Left = 4.984252F;
            this.textBox10.Name = "textBox10";
            this.textBox10.OutputFormat = resources.GetString("textBox10.OutputFormat");
            this.textBox10.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox10.SummaryGroup = "groupHeader2";
            this.textBox10.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox10.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox10.Text = "10";
            this.textBox10.Top = 0.207874F;
            this.textBox10.Width = 0.9374014F;
            // 
            // textBox11
            // 
            this.textBox11.DataField = "ITEM12";
            this.textBox11.Height = 0.1688976F;
            this.textBox11.Left = 5.987401F;
            this.textBox11.Name = "textBox11";
            this.textBox11.OutputFormat = resources.GetString("textBox11.OutputFormat");
            this.textBox11.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox11.SummaryGroup = "groupHeader2";
            this.textBox11.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox11.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox11.Text = "11";
            this.textBox11.Top = 0.01181099F;
            this.textBox11.Width = 0.9374014F;
            // 
            // textBox12
            // 
            this.textBox12.DataField = "ITEM08";
            this.textBox12.Height = 0.1688976F;
            this.textBox12.Left = 3.965355F;
            this.textBox12.Name = "textBox12";
            this.textBox12.OutputFormat = resources.GetString("textBox12.OutputFormat");
            this.textBox12.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox12.SummaryGroup = "groupHeader2";
            this.textBox12.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox12.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox12.Text = "07";
            this.textBox12.Top = 0.01181099F;
            this.textBox12.Width = 0.9374014F;
            // 
            // textBox13
            // 
            this.textBox13.DataField = "ITEM09";
            this.textBox13.Height = 0.1688976F;
            this.textBox13.Left = 3.965355F;
            this.textBox13.Name = "textBox13";
            this.textBox13.OutputFormat = resources.GetString("textBox13.OutputFormat");
            this.textBox13.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox13.SummaryGroup = "groupHeader2";
            this.textBox13.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox13.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox13.Text = "08";
            this.textBox13.Top = 0.2043307F;
            this.textBox13.Width = 0.9374014F;
            // 
            // textBox14
            // 
            this.textBox14.DataField = "ITEM10";
            this.textBox14.Height = 0.1688976F;
            this.textBox14.Left = 4.984252F;
            this.textBox14.Name = "textBox14";
            this.textBox14.OutputFormat = resources.GetString("textBox14.OutputFormat");
            this.textBox14.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox14.SummaryGroup = "groupHeader2";
            this.textBox14.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox14.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox14.Text = "09";
            this.textBox14.Top = 0.01181099F;
            this.textBox14.Width = 0.9374014F;
            // 
            // textBox15
            // 
            this.textBox15.DataField = "ITEM13";
            this.textBox15.Height = 0.1688976F;
            this.textBox15.Left = 5.987401F;
            this.textBox15.Name = "textBox15";
            this.textBox15.OutputFormat = resources.GetString("textBox15.OutputFormat");
            this.textBox15.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox15.SummaryGroup = "groupHeader2";
            this.textBox15.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox15.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox15.Text = "12";
            this.textBox15.Top = 0.207874F;
            this.textBox15.Width = 0.9374014F;
            // 
            // textBox16
            // 
            this.textBox16.Height = 0.1688976F;
            this.textBox16.Left = 7.011417F;
            this.textBox16.Name = "textBox16";
            this.textBox16.OutputFormat = resources.GetString("textBox16.OutputFormat");
            this.textBox16.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox16.SummaryGroup = "groupHeader2";
            this.textBox16.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox16.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox16.Text = "11";
            this.textBox16.Top = 0.01181099F;
            this.textBox16.Width = 0.9374014F;
            // 
            // textBox17
            // 
            this.textBox17.Height = 0.1688976F;
            this.textBox17.Left = 7.011417F;
            this.textBox17.Name = "textBox17";
            this.textBox17.OutputFormat = resources.GetString("textBox17.OutputFormat");
            this.textBox17.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox17.SummaryGroup = "groupHeader2";
            this.textBox17.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox17.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox17.Text = "12";
            this.textBox17.Top = 0.2118109F;
            this.textBox17.Width = 0.9374014F;
            // 
            // textBox18
            // 
            this.textBox18.DataField = "ITEM16";
            this.textBox18.Height = 0.1688976F;
            this.textBox18.Left = 8.029528F;
            this.textBox18.Name = "textBox18";
            this.textBox18.OutputFormat = resources.GetString("textBox18.OutputFormat");
            this.textBox18.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox18.SummaryGroup = "groupHeader2";
            this.textBox18.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox18.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox18.Text = "11";
            this.textBox18.Top = 0.0133858F;
            this.textBox18.Width = 0.9374014F;
            // 
            // textBox19
            // 
            this.textBox19.DataField = "ITEM17";
            this.textBox19.Height = 0.1688976F;
            this.textBox19.Left = 8.029528F;
            this.textBox19.Name = "textBox19";
            this.textBox19.OutputFormat = resources.GetString("textBox19.OutputFormat");
            this.textBox19.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox19.SummaryGroup = "groupHeader2";
            this.textBox19.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox19.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox19.Text = "12";
            this.textBox19.Top = 0.2118109F;
            this.textBox19.Width = 0.9374014F;
            // 
            // line42
            // 
            this.line42.Height = 0F;
            this.line42.Left = 0.08070867F;
            this.line42.LineWeight = 1F;
            this.line42.Name = "line42";
            this.line42.Top = 0.3960629F;
            this.line42.Width = 10.62992F;
            this.line42.X1 = 0.08070867F;
            this.line42.X2 = 10.71063F;
            this.line42.Y1 = 0.3960629F;
            this.line42.Y2 = 0.3960629F;
            // 
            // line43
            // 
            this.line43.Height = 0.3960468F;
            this.line43.Left = 2.927166F;
            this.line43.LineWeight = 1F;
            this.line43.Name = "line43";
            this.line43.Top = 1.615286E-05F;
            this.line43.Width = 0F;
            this.line43.X1 = 2.927166F;
            this.line43.X2 = 2.927166F;
            this.line43.Y1 = 0.396063F;
            this.line43.Y2 = 1.615286E-05F;
            // 
            // line44
            // 
            this.line44.Height = 0.3960468F;
            this.line44.Left = 3.942127F;
            this.line44.LineWeight = 1F;
            this.line44.Name = "line44";
            this.line44.Top = 1.612306E-05F;
            this.line44.Width = 0F;
            this.line44.X1 = 3.942127F;
            this.line44.X2 = 3.942127F;
            this.line44.Y1 = 0.3960629F;
            this.line44.Y2 = 1.612306E-05F;
            // 
            // line45
            // 
            this.line45.Height = 0.3960468F;
            this.line45.Left = 4.949214F;
            this.line45.LineWeight = 1F;
            this.line45.Name = "line45";
            this.line45.Top = 1.612306E-05F;
            this.line45.Width = 0F;
            this.line45.X1 = 4.949214F;
            this.line45.X2 = 4.949214F;
            this.line45.Y1 = 0.3960629F;
            this.line45.Y2 = 1.612306E-05F;
            // 
            // line46
            // 
            this.line46.Height = 0.3960468F;
            this.line46.Left = 5.947245F;
            this.line46.LineWeight = 1F;
            this.line46.Name = "line46";
            this.line46.Top = 1.612306E-05F;
            this.line46.Width = 0F;
            this.line46.X1 = 5.947245F;
            this.line46.X2 = 5.947245F;
            this.line46.Y1 = 0.3960629F;
            this.line46.Y2 = 1.612306E-05F;
            // 
            // line47
            // 
            this.line47.Height = 0.3960468F;
            this.line47.Left = 6.971264F;
            this.line47.LineWeight = 1F;
            this.line47.Name = "line47";
            this.line47.Top = 1.612306E-05F;
            this.line47.Width = 0F;
            this.line47.X1 = 6.971264F;
            this.line47.X2 = 6.971264F;
            this.line47.Y1 = 0.3960629F;
            this.line47.Y2 = 1.612306E-05F;
            // 
            // line48
            // 
            this.line48.Height = 0.3960468F;
            this.line48.Left = 7.986222F;
            this.line48.LineWeight = 1F;
            this.line48.Name = "line48";
            this.line48.Top = 1.612306E-05F;
            this.line48.Width = 0F;
            this.line48.X1 = 7.986222F;
            this.line48.X2 = 7.986222F;
            this.line48.Y1 = 0.3960629F;
            this.line48.Y2 = 1.612306E-05F;
            // 
            // line49
            // 
            this.line49.Height = 0.3960468F;
            this.line49.Left = 8.993306F;
            this.line49.LineWeight = 1F;
            this.line49.Name = "line49";
            this.line49.Top = 1.612306E-05F;
            this.line49.Width = 0F;
            this.line49.X1 = 8.993306F;
            this.line49.X2 = 8.993306F;
            this.line49.Y1 = 0.3960629F;
            this.line49.Y2 = 1.612306E-05F;
            // 
            // txtGenkaKingaku3
            // 
            this.txtGenkaKingaku3.DataField = "ITEM25";
            this.txtGenkaKingaku3.Height = 0.1574803F;
            this.txtGenkaKingaku3.Left = 0.1326772F;
            this.txtGenkaKingaku3.MultiLine = false;
            this.txtGenkaKingaku3.Name = "txtGenkaKingaku3";
            this.txtGenkaKingaku3.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: bold; text-align: right; ddo" +
    "-char-set: 128";
            this.txtGenkaKingaku3.Tag = "商品区分2コード";
            this.txtGenkaKingaku3.Text = "ITEM25";
            this.txtGenkaKingaku3.Top = 0.2043307F;
            this.txtGenkaKingaku3.Width = 0.5555118F;
            // 
            // txtGenkaKingaku4
            // 
            this.txtGenkaKingaku4.DataField = "ITEM26";
            this.txtGenkaKingaku4.Height = 0.1574803F;
            this.txtGenkaKingaku4.Left = 0.7515749F;
            this.txtGenkaKingaku4.MultiLine = false;
            this.txtGenkaKingaku4.Name = "txtGenkaKingaku4";
            this.txtGenkaKingaku4.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: bold; ddo-char-set: 128";
            this.txtGenkaKingaku4.Tag = "商品区分2名称";
            this.txtGenkaKingaku4.Text = "ITEM26";
            this.txtGenkaKingaku4.Top = 0.2043307F;
            this.txtGenkaKingaku4.Width = 1.407874F;
            // 
            // line50
            // 
            this.line50.Height = 0F;
            this.line50.Left = 0.08070867F;
            this.line50.LineWeight = 1F;
            this.line50.Name = "line50";
            this.line50.Top = 0F;
            this.line50.Width = 10.62992F;
            this.line50.X1 = 0.08070867F;
            this.line50.X2 = 10.71063F;
            this.line50.Y1 = 0F;
            this.line50.Y2 = 0F;
            // 
            // KBMR1052R
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.2755905F;
            this.PageSettings.Margins.Left = 0.5275591F;
            this.PageSettings.Margins.Right = 0.5275591F;
            this.PageSettings.Margins.Top = 0.2755905F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
            this.PageSettings.PaperHeight = 11.69291F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.267716F;
            this.PrintWidth = 10.71711F;
            this.Sections.Add(this.reportHeader1);
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.groupHeader1);
            this.Sections.Add(this.groupHeader2);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.groupFooter2);
            this.Sections.Add(this.groupFooter1);
            this.Sections.Add(this.pageFooter);
            this.Sections.Add(this.reportFooter1);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtToday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblJojun)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTyujun)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGejun)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSekisu01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSuryo01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKingaku01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSekisu02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSuryo02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKingaku02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSekisu03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKingaku03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGenkaKingaku5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShohinCd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShohinNm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKurikoshiSuryo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKurikoshiKingaku)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKeitoKingaku)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTanaSuryo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUkeireSuryo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUkeireKingaku)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKeitoSuryo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTanaKingaku)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGenkaSuryo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGenkaKingaku)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUriSuryo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUriKingaku)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRptKriSu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRptKriKin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRptKeitoKin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRptTanaSu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRptUkeSu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRptUkeKin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRptTanaKin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRptGenkSu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRptGenkKin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRptUriSu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRptUriKin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRptKeitoSu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUkeSZei)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKeitoSZei)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUriSZei)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGrpKriSu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGrpKriKin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGrpKeitoKin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGrpTanaSu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGrpUkeSu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGrpUkeKin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGrpTanaKin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGrpGenkSu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGrpGenkKin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGrpUriSu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGrpUriKin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGrpKeitoSu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGenkaKingaku3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGenkaKingaku4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.ReportHeader reportHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.ReportFooter reportFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader groupHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter groupFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtToday;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDateTo;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle01;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblJojun;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTyujun;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblGejun;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSekisu01;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSuryo01;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblKingaku01;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSekisu02;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSuryo02;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblKingaku02;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSekisu03;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblKingaku03;
        private GrapeCity.ActiveReports.SectionReportModel.Label label2;
        private GrapeCity.ActiveReports.SectionReportModel.Line line10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtShohinCd;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtShohinNm;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKurikoshiSuryo;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKurikoshiKingaku;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKeitoKingaku;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTanaSuryo;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUkeireSuryo;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUkeireKingaku;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTanaKingaku;
        private GrapeCity.ActiveReports.SectionReportModel.Label label9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox43;
        private GrapeCity.ActiveReports.SectionReportModel.Line line9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox44;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label6;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader groupHeader2;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter groupFooter2;
        private GrapeCity.ActiveReports.SectionReportModel.Line line11;
        private GrapeCity.ActiveReports.SectionReportModel.Line line13;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGenkaKingaku5;
        private GrapeCity.ActiveReports.SectionReportModel.Label label4;
        private GrapeCity.ActiveReports.SectionReportModel.Label label5;
        private GrapeCity.ActiveReports.SectionReportModel.Label label21;
        private GrapeCity.ActiveReports.SectionReportModel.Label label22;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
        private GrapeCity.ActiveReports.SectionReportModel.Line line2;
        private GrapeCity.ActiveReports.SectionReportModel.Label label23;
        private GrapeCity.ActiveReports.SectionReportModel.Label label24;
        private GrapeCity.ActiveReports.SectionReportModel.Label label25;
        private GrapeCity.ActiveReports.SectionReportModel.Label label26;
        private GrapeCity.ActiveReports.SectionReportModel.Label label27;
        private GrapeCity.ActiveReports.SectionReportModel.Label label28;
        private GrapeCity.ActiveReports.SectionReportModel.Label label29;
        private GrapeCity.ActiveReports.SectionReportModel.Line line4;
        private GrapeCity.ActiveReports.SectionReportModel.Line line5;
        private GrapeCity.ActiveReports.SectionReportModel.Line line6;
        private GrapeCity.ActiveReports.SectionReportModel.Line line7;
        private GrapeCity.ActiveReports.SectionReportModel.Line line8;
        private GrapeCity.ActiveReports.SectionReportModel.Line line12;
        private GrapeCity.ActiveReports.SectionReportModel.Line line14;
        private GrapeCity.ActiveReports.SectionReportModel.Line line3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKeitoSuryo;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGenkaSuryo;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGenkaKingaku;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUriSuryo;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUriKingaku;
        private GrapeCity.ActiveReports.SectionReportModel.Line line15;
        private GrapeCity.ActiveReports.SectionReportModel.Line line16;
        private GrapeCity.ActiveReports.SectionReportModel.Line line17;
        private GrapeCity.ActiveReports.SectionReportModel.Line line18;
        private GrapeCity.ActiveReports.SectionReportModel.Line line19;
        private GrapeCity.ActiveReports.SectionReportModel.Line line20;
        private GrapeCity.ActiveReports.SectionReportModel.Line line21;
        private GrapeCity.ActiveReports.SectionReportModel.Line line22;
        private GrapeCity.ActiveReports.SectionReportModel.Line line23;
        private GrapeCity.ActiveReports.SectionReportModel.Line line32;
        private GrapeCity.ActiveReports.SectionReportModel.Line line33;
        private GrapeCity.ActiveReports.SectionReportModel.Line line34;
        private GrapeCity.ActiveReports.SectionReportModel.Line line35;
        private GrapeCity.ActiveReports.SectionReportModel.Line line36;
        private GrapeCity.ActiveReports.SectionReportModel.Line line37;
        private GrapeCity.ActiveReports.SectionReportModel.Line line38;
        private GrapeCity.ActiveReports.SectionReportModel.Line line39;
        private GrapeCity.ActiveReports.SectionReportModel.Line line24;
        private GrapeCity.ActiveReports.SectionReportModel.Line line25;
        private GrapeCity.ActiveReports.SectionReportModel.Line line26;
        private GrapeCity.ActiveReports.SectionReportModel.Line line27;
        private GrapeCity.ActiveReports.SectionReportModel.Line line28;
        private GrapeCity.ActiveReports.SectionReportModel.Line line29;
        private GrapeCity.ActiveReports.SectionReportModel.Line line30;
        private GrapeCity.ActiveReports.SectionReportModel.Line line31;
        private GrapeCity.ActiveReports.SectionReportModel.Label label3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRptKriSu;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRptKriKin;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRptKeitoKin;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRptTanaSu;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRptUkeSu;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRptUkeKin;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRptTanaKin;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRptGenkSu;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRptGenkKin;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRptUriSu;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRptUriKin;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRptKeitoSu;
        private GrapeCity.ActiveReports.SectionReportModel.Line line40;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUkeSZei;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKeitoSZei;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUriSZei;
        private GrapeCity.ActiveReports.SectionReportModel.Label label7;
        private GrapeCity.ActiveReports.SectionReportModel.Label label8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox47;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGrpKriSu;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGrpKriKin;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGrpKeitoKin;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGrpTanaSu;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGrpUkeSu;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGrpUkeKin;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGrpTanaKin;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGrpGenkSu;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGrpGenkKin;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGrpUriSu;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGrpUriKin;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGrpKeitoSu;
        private GrapeCity.ActiveReports.SectionReportModel.Line line41;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox13;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox14;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox15;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox16;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox17;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox18;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox19;
        private GrapeCity.ActiveReports.SectionReportModel.Line line42;
        private GrapeCity.ActiveReports.SectionReportModel.Line line43;
        private GrapeCity.ActiveReports.SectionReportModel.Line line44;
        private GrapeCity.ActiveReports.SectionReportModel.Line line45;
        private GrapeCity.ActiveReports.SectionReportModel.Line line46;
        private GrapeCity.ActiveReports.SectionReportModel.Line line47;
        private GrapeCity.ActiveReports.SectionReportModel.Line line48;
        private GrapeCity.ActiveReports.SectionReportModel.Line line49;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGenkaKingaku3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGenkaKingaku4;
        private GrapeCity.ActiveReports.SectionReportModel.Line line50;
        private GrapeCity.ActiveReports.SectionReportModel.Line line51;
    }
}
