﻿namespace jp.co.fsi.kb.kbmr1051
{
    partial class KBMR1052
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtShiiresakiCd6 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShiiresakiNm6 = new System.Windows.Forms.Label();
            this.lblShiiresaki6 = new System.Windows.Forms.Label();
            this.txtShiiresakiCd5 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShiiresakiNm5 = new System.Windows.Forms.Label();
            this.lblShiiresaki5 = new System.Windows.Forms.Label();
            this.txtShiiresakiCd4 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShiiresakiNm4 = new System.Windows.Forms.Label();
            this.lblShiiresaki4 = new System.Windows.Forms.Label();
            this.txtShiiresakiCd3 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShiiresakiNm3 = new System.Windows.Forms.Label();
            this.lblShiiresaki3 = new System.Windows.Forms.Label();
            this.txtShiiresakiCd2 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShiiresakiNm2 = new System.Windows.Forms.Label();
            this.lblShiiresaki2 = new System.Windows.Forms.Label();
            this.txtShiiresakiCd1 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShiiresakiNm1 = new System.Windows.Forms.Label();
            this.lblShiiresaki1 = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Size = new System.Drawing.Size(760, 23);
            this.lblTitle.Visible = false;
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(5, 213);
            this.pnlDebug.Size = new System.Drawing.Size(453, 100);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtShiiresakiCd6);
            this.groupBox1.Controls.Add(this.lblShiiresakiNm6);
            this.groupBox1.Controls.Add(this.lblShiiresaki6);
            this.groupBox1.Controls.Add(this.txtShiiresakiCd5);
            this.groupBox1.Controls.Add(this.lblShiiresakiNm5);
            this.groupBox1.Controls.Add(this.lblShiiresaki5);
            this.groupBox1.Controls.Add(this.txtShiiresakiCd4);
            this.groupBox1.Controls.Add(this.lblShiiresakiNm4);
            this.groupBox1.Controls.Add(this.lblShiiresaki4);
            this.groupBox1.Controls.Add(this.txtShiiresakiCd3);
            this.groupBox1.Controls.Add(this.lblShiiresakiNm3);
            this.groupBox1.Controls.Add(this.lblShiiresaki3);
            this.groupBox1.Controls.Add(this.txtShiiresakiCd2);
            this.groupBox1.Controls.Add(this.lblShiiresakiNm2);
            this.groupBox1.Controls.Add(this.lblShiiresaki2);
            this.groupBox1.Controls.Add(this.txtShiiresakiCd1);
            this.groupBox1.Controls.Add(this.lblShiiresakiNm1);
            this.groupBox1.Controls.Add(this.lblShiiresaki1);
            this.groupBox1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBox1.Location = new System.Drawing.Point(12, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(435, 241);
            this.groupBox1.TabIndex = 905;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "県漁連コード設定";
            // 
            // txtShiiresakiCd6
            // 
            this.txtShiiresakiCd6.AutoSizeFromLength = true;
            this.txtShiiresakiCd6.DisplayLength = null;
            this.txtShiiresakiCd6.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShiiresakiCd6.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtShiiresakiCd6.Location = new System.Drawing.Point(119, 189);
            this.txtShiiresakiCd6.MaxLength = 5;
            this.txtShiiresakiCd6.Name = "txtShiiresakiCd6";
            this.txtShiiresakiCd6.Size = new System.Drawing.Size(45, 20);
            this.txtShiiresakiCd6.TabIndex = 926;
            this.txtShiiresakiCd6.TabStop = false;
            this.txtShiiresakiCd6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShiiresakiCd6.Validating += new System.ComponentModel.CancelEventHandler(this.txtShiiresakiCd6_Validating);
            // 
            // lblShiiresakiNm6
            // 
            this.lblShiiresakiNm6.BackColor = System.Drawing.Color.Silver;
            this.lblShiiresakiNm6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShiiresakiNm6.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShiiresakiNm6.Location = new System.Drawing.Point(167, 189);
            this.lblShiiresakiNm6.Name = "lblShiiresakiNm6";
            this.lblShiiresakiNm6.Size = new System.Drawing.Size(243, 20);
            this.lblShiiresakiNm6.TabIndex = 928;
            this.lblShiiresakiNm6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShiiresaki6
            // 
            this.lblShiiresaki6.BackColor = System.Drawing.Color.Silver;
            this.lblShiiresaki6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShiiresaki6.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblShiiresaki6.Location = new System.Drawing.Point(17, 186);
            this.lblShiiresaki6.Name = "lblShiiresaki6";
            this.lblShiiresaki6.Size = new System.Drawing.Size(399, 25);
            this.lblShiiresaki6.TabIndex = 927;
            this.lblShiiresaki6.Text = "仕入先コード6";
            this.lblShiiresaki6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShiiresakiCd5
            // 
            this.txtShiiresakiCd5.AutoSizeFromLength = true;
            this.txtShiiresakiCd5.DisplayLength = null;
            this.txtShiiresakiCd5.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShiiresakiCd5.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtShiiresakiCd5.Location = new System.Drawing.Point(119, 157);
            this.txtShiiresakiCd5.MaxLength = 5;
            this.txtShiiresakiCd5.Name = "txtShiiresakiCd5";
            this.txtShiiresakiCd5.Size = new System.Drawing.Size(45, 20);
            this.txtShiiresakiCd5.TabIndex = 923;
            this.txtShiiresakiCd5.TabStop = false;
            this.txtShiiresakiCd5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShiiresakiCd5.Validating += new System.ComponentModel.CancelEventHandler(this.txtShiiresakiCd5_Validating);
            // 
            // lblShiiresakiNm5
            // 
            this.lblShiiresakiNm5.BackColor = System.Drawing.Color.Silver;
            this.lblShiiresakiNm5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShiiresakiNm5.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShiiresakiNm5.Location = new System.Drawing.Point(167, 157);
            this.lblShiiresakiNm5.Name = "lblShiiresakiNm5";
            this.lblShiiresakiNm5.Size = new System.Drawing.Size(243, 20);
            this.lblShiiresakiNm5.TabIndex = 925;
            this.lblShiiresakiNm5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShiiresaki5
            // 
            this.lblShiiresaki5.BackColor = System.Drawing.Color.Silver;
            this.lblShiiresaki5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShiiresaki5.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblShiiresaki5.Location = new System.Drawing.Point(17, 155);
            this.lblShiiresaki5.Name = "lblShiiresaki5";
            this.lblShiiresaki5.Size = new System.Drawing.Size(399, 25);
            this.lblShiiresaki5.TabIndex = 924;
            this.lblShiiresaki5.Text = "仕入先コード5";
            this.lblShiiresaki5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShiiresakiCd4
            // 
            this.txtShiiresakiCd4.AutoSizeFromLength = true;
            this.txtShiiresakiCd4.DisplayLength = null;
            this.txtShiiresakiCd4.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShiiresakiCd4.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtShiiresakiCd4.Location = new System.Drawing.Point(119, 125);
            this.txtShiiresakiCd4.MaxLength = 5;
            this.txtShiiresakiCd4.Name = "txtShiiresakiCd4";
            this.txtShiiresakiCd4.Size = new System.Drawing.Size(45, 20);
            this.txtShiiresakiCd4.TabIndex = 920;
            this.txtShiiresakiCd4.TabStop = false;
            this.txtShiiresakiCd4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShiiresakiCd4.Validating += new System.ComponentModel.CancelEventHandler(this.txtShiiresakiCd4_Validating);
            // 
            // lblShiiresakiNm4
            // 
            this.lblShiiresakiNm4.BackColor = System.Drawing.Color.Silver;
            this.lblShiiresakiNm4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShiiresakiNm4.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShiiresakiNm4.Location = new System.Drawing.Point(167, 125);
            this.lblShiiresakiNm4.Name = "lblShiiresakiNm4";
            this.lblShiiresakiNm4.Size = new System.Drawing.Size(243, 20);
            this.lblShiiresakiNm4.TabIndex = 922;
            this.lblShiiresakiNm4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShiiresaki4
            // 
            this.lblShiiresaki4.BackColor = System.Drawing.Color.Silver;
            this.lblShiiresaki4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShiiresaki4.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblShiiresaki4.Location = new System.Drawing.Point(17, 124);
            this.lblShiiresaki4.Name = "lblShiiresaki4";
            this.lblShiiresaki4.Size = new System.Drawing.Size(399, 25);
            this.lblShiiresaki4.TabIndex = 921;
            this.lblShiiresaki4.Text = "仕入先コード4";
            this.lblShiiresaki4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShiiresakiCd3
            // 
            this.txtShiiresakiCd3.AutoSizeFromLength = true;
            this.txtShiiresakiCd3.DisplayLength = null;
            this.txtShiiresakiCd3.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShiiresakiCd3.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtShiiresakiCd3.Location = new System.Drawing.Point(119, 95);
            this.txtShiiresakiCd3.MaxLength = 5;
            this.txtShiiresakiCd3.Name = "txtShiiresakiCd3";
            this.txtShiiresakiCd3.Size = new System.Drawing.Size(45, 20);
            this.txtShiiresakiCd3.TabIndex = 917;
            this.txtShiiresakiCd3.TabStop = false;
            this.txtShiiresakiCd3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShiiresakiCd3.Validating += new System.ComponentModel.CancelEventHandler(this.txtShiiresakiCd3_Validating);
            // 
            // lblShiiresakiNm3
            // 
            this.lblShiiresakiNm3.BackColor = System.Drawing.Color.Silver;
            this.lblShiiresakiNm3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShiiresakiNm3.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShiiresakiNm3.Location = new System.Drawing.Point(167, 95);
            this.lblShiiresakiNm3.Name = "lblShiiresakiNm3";
            this.lblShiiresakiNm3.Size = new System.Drawing.Size(243, 20);
            this.lblShiiresakiNm3.TabIndex = 919;
            this.lblShiiresakiNm3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShiiresaki3
            // 
            this.lblShiiresaki3.BackColor = System.Drawing.Color.Silver;
            this.lblShiiresaki3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShiiresaki3.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblShiiresaki3.Location = new System.Drawing.Point(17, 93);
            this.lblShiiresaki3.Name = "lblShiiresaki3";
            this.lblShiiresaki3.Size = new System.Drawing.Size(399, 25);
            this.lblShiiresaki3.TabIndex = 918;
            this.lblShiiresaki3.Text = "仕入先コード3";
            this.lblShiiresaki3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShiiresakiCd2
            // 
            this.txtShiiresakiCd2.AutoSizeFromLength = true;
            this.txtShiiresakiCd2.DisplayLength = null;
            this.txtShiiresakiCd2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShiiresakiCd2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtShiiresakiCd2.Location = new System.Drawing.Point(118, 64);
            this.txtShiiresakiCd2.MaxLength = 5;
            this.txtShiiresakiCd2.Name = "txtShiiresakiCd2";
            this.txtShiiresakiCd2.Size = new System.Drawing.Size(45, 20);
            this.txtShiiresakiCd2.TabIndex = 914;
            this.txtShiiresakiCd2.TabStop = false;
            this.txtShiiresakiCd2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShiiresakiCd2.Validating += new System.ComponentModel.CancelEventHandler(this.txtShiiresakiCd2_Validating);
            // 
            // lblShiiresakiNm2
            // 
            this.lblShiiresakiNm2.BackColor = System.Drawing.Color.Silver;
            this.lblShiiresakiNm2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShiiresakiNm2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShiiresakiNm2.Location = new System.Drawing.Point(166, 64);
            this.lblShiiresakiNm2.Name = "lblShiiresakiNm2";
            this.lblShiiresakiNm2.Size = new System.Drawing.Size(243, 20);
            this.lblShiiresakiNm2.TabIndex = 916;
            this.lblShiiresakiNm2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShiiresaki2
            // 
            this.lblShiiresaki2.BackColor = System.Drawing.Color.Silver;
            this.lblShiiresaki2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShiiresaki2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblShiiresaki2.Location = new System.Drawing.Point(16, 62);
            this.lblShiiresaki2.Name = "lblShiiresaki2";
            this.lblShiiresaki2.Size = new System.Drawing.Size(399, 25);
            this.lblShiiresaki2.TabIndex = 915;
            this.lblShiiresaki2.Text = "仕入先コード2";
            this.lblShiiresaki2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShiiresakiCd1
            // 
            this.txtShiiresakiCd1.AutoSizeFromLength = true;
            this.txtShiiresakiCd1.DisplayLength = null;
            this.txtShiiresakiCd1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShiiresakiCd1.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtShiiresakiCd1.Location = new System.Drawing.Point(119, 33);
            this.txtShiiresakiCd1.MaxLength = 5;
            this.txtShiiresakiCd1.Name = "txtShiiresakiCd1";
            this.txtShiiresakiCd1.Size = new System.Drawing.Size(45, 20);
            this.txtShiiresakiCd1.TabIndex = 911;
            this.txtShiiresakiCd1.TabStop = false;
            this.txtShiiresakiCd1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShiiresakiCd1.Validating += new System.ComponentModel.CancelEventHandler(this.txtShiiresakiCd1_Validating);
            // 
            // lblShiiresakiNm1
            // 
            this.lblShiiresakiNm1.BackColor = System.Drawing.Color.Silver;
            this.lblShiiresakiNm1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShiiresakiNm1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShiiresakiNm1.Location = new System.Drawing.Point(167, 33);
            this.lblShiiresakiNm1.Name = "lblShiiresakiNm1";
            this.lblShiiresakiNm1.Size = new System.Drawing.Size(243, 20);
            this.lblShiiresakiNm1.TabIndex = 913;
            this.lblShiiresakiNm1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShiiresaki1
            // 
            this.lblShiiresaki1.BackColor = System.Drawing.Color.Silver;
            this.lblShiiresaki1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShiiresaki1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblShiiresaki1.Location = new System.Drawing.Point(17, 31);
            this.lblShiiresaki1.Name = "lblShiiresaki1";
            this.lblShiiresaki1.Size = new System.Drawing.Size(399, 25);
            this.lblShiiresaki1.TabIndex = 912;
            this.lblShiiresaki1.Text = "仕入先コード1";
            this.lblShiiresaki1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // KBMR1052
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(459, 317);
            this.Controls.Add(this.groupBox1);
            this.Name = "KBMR1052";
            this.Text = "県漁連コード設定";
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.pnlDebug.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox1;
        private common.controls.FsiTextBox txtShiiresakiCd6;
        private System.Windows.Forms.Label lblShiiresakiNm6;
        private System.Windows.Forms.Label lblShiiresaki6;
        private common.controls.FsiTextBox txtShiiresakiCd5;
        private System.Windows.Forms.Label lblShiiresakiNm5;
        private System.Windows.Forms.Label lblShiiresaki5;
        private common.controls.FsiTextBox txtShiiresakiCd4;
        private System.Windows.Forms.Label lblShiiresakiNm4;
        private System.Windows.Forms.Label lblShiiresaki4;
        private common.controls.FsiTextBox txtShiiresakiCd3;
        private System.Windows.Forms.Label lblShiiresakiNm3;
        private System.Windows.Forms.Label lblShiiresaki3;
        private common.controls.FsiTextBox txtShiiresakiCd2;
        private System.Windows.Forms.Label lblShiiresakiNm2;
        private System.Windows.Forms.Label lblShiiresaki2;
        private common.controls.FsiTextBox txtShiiresakiCd1;
        private System.Windows.Forms.Label lblShiiresakiNm1;
        private System.Windows.Forms.Label lblShiiresaki1;
    }
}