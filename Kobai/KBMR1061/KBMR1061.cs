﻿using System;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Reflection;
using System.Windows.Forms;

using GrapeCity.ActiveReports;

using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.kb.kbmr1061
{
    #region 棚卸設定構造体
    // 棚卸設定構造体
    public struct Settei
    {
        public int kbn;
        public decimal tanka;
        public decimal gokei;
        public int sort;
        public void Clear()
        {
            kbn = 0;
            tanka = 0;
            gokei = 0;
            sort = 0;
        }
    }
    #endregion

    /// <summary>
    /// 商品管理日報(KBMR1061)
    /// </summary>
    public partial class KBMR1061 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// 印刷ワーク更新用列数
        /// </summary>
        private const int prtCols = 16;
        #endregion

        /// <summary>
        /// 棚卸設定退避用変数
        /// </summary>
        public Settei gSettei;

        #region プロパティ
        /// <summary>
        /// 画面上最後となるフォーカスのEnterボタン押下時処理用変数
        /// </summary>
        private bool _dtFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._dtFlg;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KBMR1061()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 水揚支所
#if DEBUG
            this.txtMizuageShishoCd.Text = "1";
#else
            this.txtMizuageShishoCd.Text = Uinfo.shishoCd;
#endif
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
            this.txtMizuageShishoCd.Enabled = (this.txtMizuageShishoCd.Text == "1") ? true : false;

            // 日付範囲の和暦設定
            string[] jpDate = Util.ConvJpDate(DateTime.Now, this.Dba);
            // 開始
            lblDateGengoFr.Text = jpDate[0];
            txtDateYearFr.Text = jpDate[2];
            txtDateMonthFr.Text = jpDate[3];
            txtDateDayFr.Text = "1";
            // 終了
            lblDateGengoTo.Text = jpDate[0];
            txtDateYearTo.Text = jpDate[2];
            txtDateMonthTo.Text = jpDate[3];
            txtDateDayTo.Text = jpDate[4];

            rdoZeroNashi.Checked = true;

            this.comboBox1.SelectedIndex = 1;
            // 初期フォーカス
            txtDateYearFr.Focus();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 日付、商品区分に
            // フォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd":
                case "txtDateYearFr":
                case "txtDateYearTo":
                case "txtShohinCdFr":
                case "txtShohinCdTo":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;
            String[] result;

            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd": // 水揚支所
                    #region 水揚支所
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM2031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2031.CMCM2031");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.InData = this.txtMizuageShishoCd.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.txtMizuageShishoCd.Text = result[0];
                                this.lblMizuageShishoNm.Text = result[1];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtShohinCdFr":
                    #region 商品CD
                    // 商品コード検索を起動
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "KBCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.kb.kbcm1021.KBCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtShohinCdFr.Text = outData[0];
                                this.lblShohinNmFr.Text = outData[1];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtShohinCdTo":
                    #region 商品CD
                    // 商品コード検索を起動
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "KBCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.kb.kbcm1021.KBCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtShohinCdTo.Text = outData[0];
                                this.lblShohinNmTo.Text = outData[1];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtDateYearFr":
                    #region 元号
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblDateGengoFr.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (string[])frm.OutData;
                                this.lblDateGengoFr.Text = result[1];

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblDateGengoFr.Text,
                                        this.txtDateYearFr.Text,
                                        this.txtDateMonthFr.Text,
                                        this.txtDateDayFr.Text,
                                        this.Dba);
                                this.lblDateGengoFr.Text = arrJpDate[0];
                                this.txtDateYearFr.Text = arrJpDate[2];
                                this.txtDateMonthFr.Text = arrJpDate[3];
                                this.txtDateDayFr.Text = arrJpDate[4];
                            }
                        }
                    }
                    #endregion
                    break;
                case "txtDateYearTo":
                    #region 元号
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblDateGengoTo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (string[])frm.OutData;
                                this.lblDateGengoTo.Text = result[1];

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblDateGengoTo.Text,
                                        this.txtDateYearTo.Text,
                                        this.txtDateMonthTo.Text,
                                        this.txtDateDayTo.Text,
                                        this.Dba);
                                this.lblDateGengoTo.Text = arrJpDate[0];
                                this.txtDateYearTo.Text = arrJpDate[2];
                                this.txtDateMonthTo.Text = arrJpDate[3];
                                this.txtDateDayTo.Text = arrJpDate[4];
                            }
                        }
                    }
                    #endregion
                    break;
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
            {
                // プレビュー処理
                DoPrint(true);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("印刷", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }

        /// <summary>
        /// F6キー押下時処理
        /// PDF出力
        /// </summary>
        public override void PressF6()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("PDF出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, true);
            }
        }

        /// <summary>
        /// F7キー押下時処理
        /// EXCEL出力
        /// </summary>
        public override void PressF7()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("EXCEL出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, false, true);
            }
        }

        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            // 設定画面の起動
            // MEMO:原則としてここで渡す帳票IDの設定はReport.csvに保持していることが前提ですが、
            // 保持していない場合は、設定画面での保存(F6)時に新規に設定が保持されます。
            PrintSettingForm psForm = new PrintSettingForm(new string[1] { "KBMR1061R" });
            psForm.ShowDialog();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 水揚支所入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMizuageShishoCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                e.Cancel = true;
                this.txtMizuageShishoCd.SelectAll();
                this.txtMizuageShishoCd.Focus();
            }
        }

        /// <summary>
        /// 商品区分１(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShohinFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShohinCdFr())
            {
                e.Cancel = true;
                this.txtShohinCdFr.SelectAll();
            }
        }

        /// <summary>
        /// 商品区分１(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShohinTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShohinCdTo())
            {
                e.Cancel = true;
                this.txtShohinCdTo.SelectAll();

                // Enter処理を無効化
                this._dtFlg = false;
            }
            else
            {
                // Enter処理を有効化
                this._dtFlg = true;
            }
        }

        /// <summary>
        /// 和暦(年)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateYearFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtDateYearFr.Text, this.txtDateYearFr.MaxLength))
            {
                e.Cancel = true;
                this.txtDateYearFr.SelectAll();
            }
            else
            {
                this.txtDateYearFr.Text = Util.ToString(IsValid.SetYear(this.txtDateYearFr.Text));
                CheckDateFr();
                SetDateFr();
            }
        }

        /// <summary>
        /// 和暦(月)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateMonthFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtDateMonthFr.Text, this.txtDateMonthFr.MaxLength))
            {
                e.Cancel = true;
                this.txtDateMonthFr.SelectAll();
            }
            else
            {
                this.txtDateMonthFr.Text = Util.ToString(IsValid.SetMonth(this.txtDateMonthFr.Text));
                CheckDateFr();
                SetDateFr();
            }
        }

        /// <summary>
        /// 和暦(日)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateDayFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsDay(this.txtDateDayFr.Text, this.txtDateDayFr.MaxLength))
            {
                e.Cancel = true;
                this.txtDateDayFr.SelectAll();
            }
            else
            {
                this.txtDateDayFr.Text = Util.ToString(IsValid.SetDay(this.txtDateDayFr.Text));
                CheckDateFr();
                SetDateFr();
            }
        }

        /// <summary>
        /// 和暦(年)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateYearTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtDateYearTo.Text, this.txtDateYearTo.MaxLength))
            {
                e.Cancel = true;
                this.txtDateYearTo.SelectAll();
            }
            else
            {
                this.txtDateYearTo.Text = Util.ToString(IsValid.SetYear(this.txtDateYearTo.Text));
                CheckDateTo();
                SetDateTo();
            }
        }

        /// <summary>
        /// 和暦(月)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateMonthTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtDateMonthTo.Text, this.txtDateMonthTo.MaxLength))
            {
                e.Cancel = true;
                this.txtDateMonthTo.SelectAll();
            }
            else
            {
                this.txtDateMonthTo.Text = Util.ToString(IsValid.SetMonth(this.txtDateMonthTo.Text));
                CheckDateTo();
                SetDateTo();
            }
        }

        /// <summary>
        /// 和暦(日)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateDayTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsDay(this.txtDateDayTo.Text, this.txtDateDayTo.MaxLength))
            {
                e.Cancel = true;
                this.txtDateDayTo.SelectAll();
            }
            else
            {
                this.txtDateDayTo.Text = Util.ToString(IsValid.SetDay(this.txtDateDayTo.Text));
                CheckDateTo();
                SetDateTo();
            }
        }

        /// <summary>
        /// 商品コード(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShohinCdFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShohinCdFr())
            {
                e.Cancel = true;
                this.txtShohinCdFr.SelectAll();
            }
        }

        /// <summary>
        /// 商品コード(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShohinCdTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShohinCdTo())
            {
                e.Cancel = true;
                this.txtShohinCdTo.SelectAll();
            }
        }

        private void txtShohinCdTo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.Flg)
            {
                // Enter処理を無効化
                this._dtFlg = false;

                // 全項目を再度入力値チェック
                if (!ValidateAll())
                {
                    // エラーありの場合ここで処理終了
                    return;
                }

                if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
                {
                    // ﾌﾟﾚﾋﾞｭｰ処理
                    DoPrint(true);
                }
                else
                {
                    this.txtShohinCdTo.Focus();
                }
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 水揚支所の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool IsValidMizuageShishoCd()
        {
            // 空 又は 0入力の場合
            if (ValChk.IsEmpty(this.txtMizuageShishoCd.Text) || Equals(this.txtMizuageShishoCd.Text, "0"))
            {
                // 水揚支所名称を表示する
                this.txtMizuageShishoCd.Text = "0";
                this.lblMizuageShishoNm.Text = "全て";
                return true;
            }
            // 水揚支所名称を表示する
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
            if (ValChk.IsEmpty(this.lblMizuageShishoNm.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 年月日(自)の月末入力チェック
        /// </summary>
        /// 
        private void CheckDateFr()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                this.txtDateMonthFr.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDateDayFr.Text) > lastDayInMonth)
            {
                this.txtDateDayFr.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(自)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetDateFr()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDateFr(Util.FixJpDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                this.txtDateMonthFr.Text, this.txtDateDayFr.Text, this.Dba));
        }

        /// <summary>
        /// 年月日(至)の月末入力チェック
        /// </summary>
        /// 
        private void CheckDateTo()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                this.txtDateMonthTo.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDateDayTo.Text) > lastDayInMonth)
            {
                this.txtDateDayTo.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(至)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetDateTo()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDateTo(Util.FixJpDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                this.txtDateMonthTo.Text, this.txtDateDayTo.Text, this.Dba));
        }

        /// <summary>
        /// 商品コード(自)の入力チェック
        /// </summary>
        /// <returns>true:OK/false:NG</returns>
        private bool IsValidShohinCdFr()
        {
            // 未入力の場合、「先頭」を表示
            if (ValChk.IsEmpty(this.txtShohinCdFr.Text))
            {
                this.lblShohinNmFr.Text = "先　頭";
                return true;
            }
            // 数値のみの入力を許可
            else if (!ValChk.IsNumber(this.txtShohinCdFr.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtShohinCdFr.SelectAll();
                return false;
            }
            // 名称を表示
            else
            {
                this.lblShohinNmFr.Text = this.Dba.GetName(this.UInfo, "TB_HN_SHOHIN", this.txtMizuageShishoCd.Text, this.txtShohinCdFr.Text);
            }

            return true;
        }

        /// <summary>
        /// 商品コード(至)の入力チェック
        /// </summary>
        /// <returns>true:OK/false:NG</returns>
        private bool IsValidShohinCdTo()
        {
            // 未入力の場合、「最後」を表示
            if (ValChk.IsEmpty(this.txtShohinCdTo.Text))
            {
                this.lblShohinNmTo.Text = "最　後";
                return true;
            }
            // 数値のみの入力を許可
            else if (!ValChk.IsNumber(this.txtShohinCdTo.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtShohinCdTo.SelectAll();
                return false;
            }
            // 名称を表示
            else
            {
                this.lblShohinNmTo.Text = this.Dba.GetName(this.UInfo, "TB_HN_SHOHIN", this.txtMizuageShishoCd.Text, this.txtShohinCdTo.Text);
            }

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 水揚支所の入力チェック
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                this.txtMizuageShishoCd.Focus();
                this.txtMizuageShishoCd.SelectAll();
                return false;
            }
            // 年(自)のチェック
            if (!IsValid.IsYear(this.txtDateYearFr.Text, this.txtDateYearFr.MaxLength))
            {
                this.txtDateYearFr.Focus();
                this.txtDateYearFr.SelectAll();
                return false;
            }
            // 月(自)のチェック
            if (!IsValid.IsMonth(this.txtDateMonthFr.Text, this.txtDateMonthFr.MaxLength))
            {
                this.txtDateMonthFr.Focus();
                this.txtDateMonthFr.SelectAll();
                return false;
            }
            // 日(自)のチェック
            if (!IsValid.IsDay(this.txtDateDayFr.Text, this.txtDateDayFr.MaxLength))
            {
                this.txtDateDayFr.Focus();
                this.txtDateDayFr.SelectAll();
                return false;
            }
            // 年月日(自)の月末入力チェック処理
            CheckDateFr();
            // 年月日(自)の正しい和暦への変換処理
            SetDateFr();

            // 年(至)のチェック
            if (!IsValid.IsYear(this.txtDateYearTo.Text, this.txtDateYearTo.MaxLength))
            {
                this.txtDateYearTo.Focus();
                this.txtDateYearTo.SelectAll();
                return false;
            }
            // 月(至)のチェック
            if (!IsValid.IsMonth(this.txtDateMonthTo.Text, this.txtDateMonthTo.MaxLength))
            {
                this.txtDateMonthTo.Focus();
                this.txtDateMonthTo.SelectAll();
                return false;
            }
            // 日(至)のチェック
            if (!IsValid.IsDay(this.txtDateDayTo.Text, this.txtDateDayTo.MaxLength))
            {
                this.txtDateDayTo.Focus();
                this.txtDateDayTo.SelectAll();
                return false;
            }
            // 年月日(至)の月末入力チェック処理
            CheckDateTo();
            // 年月日(至)の正しい和暦への変換処理
            SetDateTo();

            return true;
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpDateFr(string[] arrJpDate)
        {
            this.lblDateGengoFr.Text = arrJpDate[0];
            this.txtDateYearFr.Text = arrJpDate[2];
            this.txtDateMonthFr.Text = arrJpDate[3];
            this.txtDateDayFr.Text = arrJpDate[4];
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpDateTo(string[] arrJpDate)
        {
            this.lblDateGengoTo.Text = arrJpDate[0];
            this.txtDateYearTo.Text = arrJpDate[2];
            this.txtDateMonthTo.Text = arrJpDate[3];
            this.txtDateDayTo.Text = arrJpDate[4];
        }

        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview, bool isPdf = false, bool isExcel = false, bool isCsv = false)
        {
            try
            {
#if DEBUG
                //印刷用ワークテーブルの削除
                this.Dba.DeleteWork("PR_HN_TBL", this.UnqId);
#endif

                bool dataFlag;
                
                this.Dba.BeginTransaction();

                // 帳票出力用にワークテーブルにデータを作成
                dataFlag = MakeWkData();

                // 帳票出力
                if (dataFlag)
                {
                    // 取得列の定義
                    StringBuilder cols = Util.ColsArray(prtCols, "");

                    // バインドパラメータの設定
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);

                    // データの取得
                    DataTable dtOutput = this.Dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_HN_TBL", "GUID = @GUID", "ITEM23 ASC , ITEM26 ASC", dpc);

                    // 帳票オブジェクトをインスタンス化
                    KBMR1061R rpt = new KBMR1061R(dtOutput);
                    //rpt.Document.Name = this.lblTitle.Text;
                    //rpt.Document.Printer.DocumentName = this.lblTitle.Text;

                    if (isExcel)
                    {
                        GrapeCity.ActiveReports.Export.Excel.Section.XlsExport xlsExport1 = new GrapeCity.ActiveReports.Export.Excel.Section.XlsExport();
                        //SetExcelSetting(xlsExport1);
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Kob, rpt.Document.Name, 2);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            xlsExport1.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("EXCEL出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPdf)
                    {
                        GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport p = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Kob, rpt.Document.Name, 1);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            p.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("PDF出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPreview)
                    {
                        // プレビュー画面表示
                        PreviewForm pFrm = new PreviewForm(rpt, this.UnqId);
                        pFrm.WindowState = FormWindowState.Maximized;
                        pFrm.Show();
                    }
                    else
                    {
                        // 直接印刷
                        rpt.Run(false);
                        rpt.Document.Print(true, true, false);
                    }
                }
                else
                {
                    Msg.Error("データがありません。");
                }
            }
            finally
            {
#if DEBUG
                this.Dba.Commit();
#else
                this.Dba.Rollback();
#endif
            }
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        /// <param name="zenkaiTnorsDt">前回棚卸日</param>
        private bool MakeWkData()
        {
            DbParamCollection dpc = new DbParamCollection();
            // 日付範囲を西暦にして取得
            DateTime tmpDateFr = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                    this.txtDateMonthFr.Text, this.txtDateDayFr.Text, this.Dba);
            DateTime tmpDateTo = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                    this.txtDateMonthTo.Text, this.txtDateDayTo.Text, this.Dba);
            DateTime tmpDateTo2 = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                    this.txtDateMonthTo.Text, Util.ToString(DateTime.DaysInMonth(tmpDateTo.Year, Util.ToInt(this.txtDateMonthTo.Text))), this.Dba);

            // 日付範囲を和暦で保持
            string[] tmpjpDateFr = Util.ConvJpDate(tmpDateFr, this.Dba);
            string[] tmpjpDateTo = Util.ConvJpDate(tmpDateTo, this.Dba);
            string[] tmpjpDateTo2 = Util.ConvJpDate(tmpDateTo2, this.Dba);

            // 表示する日付を設定する
            string hyojiDate; // 表示用日付
            string dateFrChk = string.Format("{0}{1}{2}{3}", tmpjpDateFr[0], tmpjpDateFr[2], tmpjpDateFr[3], tmpjpDateFr[4]);
            string dateToChk = string.Format("{0}{1}{2}{3}", tmpjpDateTo[0], tmpjpDateTo[2], tmpjpDateTo[3], tmpjpDateTo[4]);
            string dateFrChk2 = string.Format("{0}{1}{2}{3}", tmpjpDateTo[0], tmpjpDateTo[2], tmpjpDateTo[3], "1");
            string dateToChk2 = string.Format("{0}{1}{2}{3}", tmpjpDateTo[0], tmpjpDateTo[2], tmpjpDateTo[3], tmpjpDateTo2[4]);
            if (dateFrChk == dateFrChk2 && dateToChk == dateToChk2)
            {
                hyojiDate = string.Format("{0}{1}年{2}月", tmpjpDateFr[0], tmpjpDateFr[2], tmpjpDateFr[3]) + "度";
            }
            else
            {
                hyojiDate = string.Format("{0}{1}年{2}月{3}日", tmpjpDateFr[0], tmpjpDateFr[2], tmpjpDateFr[3], tmpjpDateFr[4])
                          + "～"
                          + string.Format("{0}{1}年{2}月{3}日", tmpjpDateTo[0], tmpjpDateTo[2], tmpjpDateTo[3], tmpjpDateTo[4]);
            }

            // 出力日付設定
            string[] nowDate = Util.ConvJpDate(DateTime.Now, this.Dba);
            string outputDate = nowDate[5];

            int i; // ループ用カウント変数

            // 商品区分設定
            string shohinCdFr;
            string shohinCdTo;
            if (Util.ToDecimal(txtShohinCdFr.Text) > 0)
            {
                shohinCdFr = txtShohinCdFr.Text;
            }
            else
            {
                shohinCdFr = "0";
            }
            if (Util.ToDecimal(txtShohinCdTo.Text) > 0)
            {
                shohinCdTo = txtShohinCdTo.Text;
            }
            else
            {
                shohinCdTo = "9999999999999";
            }
            string shishoCd = this.txtMizuageShishoCd.Text;
            StringBuilder sql;

            i = 1;
            //中止区分
            int Chushikubun = this.comboBox1.SelectedIndex;
            #region データインサート,アップデート処理
            // 商品情報取得
            dpc = new DbParamCollection();
            sql = new StringBuilder();
            sql.Append(" SELECT");
            sql.Append("     A.KAISHA_CD,");
            sql.Append("     A.SHOHIN_CD,");
            sql.Append("     A.SHOHIN_NM,");
            sql.Append("     A.TANI,");
            sql.Append("     A.SHIIRE_TANKA, ");
            sql.Append("     A.SAISHU_SHIIRE_TANKA,");
            sql.Append("     A.SURYO1, ");
            sql.Append("     A.SURYO2, ");
            sql.Append("     A.SURYO1 * A.IRISU + A.SURYO2 AS BARASU ");
            sql.Append(" FROM");
            sql.Append("     VI_HN_SHOHIN_ZAIKO AS A");
            sql.Append(" WHERE");
            sql.Append("     A.KAISHA_CD = @KAISHA_CD AND");
            if (shishoCd != "0")
            {
                // 支所コードがゼロなら合算表示
                sql.Append("     A.SHISHO_CD = @SHISHO_CD AND");
            }
            sql.Append("     A.SHOHIN_CD BETWEEN @SHOHIN_CD_FR AND @SHOHIN_CD_TO AND ");
            sql.Append("     A.SHOHIN_KUBUN5 <> 1 AND");
            if (Chushikubun != 0)
            {
                sql.Append("     ISNULL(A.CHUSHI_KUBUN, 0) = @CHUSHI_KUBUN AND");
            }
            if (rdoZeroNashi.Checked)
            {
                sql.Append("     (A.SURYO1 <> 0 OR A.SURYO2 <> 0) AND  ");
            }
            sql.Append("     A.ZAIKO_KANRI_KUBUN = 1");
            sql.Append(" ORDER BY");
            sql.Append("     A.SHOHIN_CD");

            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            dpc.SetParam("@SHOHIN_CD_FR", SqlDbType.Decimal, 13, shohinCdFr);
            dpc.SetParam("@SHOHIN_CD_TO", SqlDbType.Decimal, 13, shohinCdTo);
            dpc.SetParam("@CHUSHI_KUBUN", SqlDbType.Decimal, 4, Chushikubun);


            DataTable dt_SHOHIN = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);

            foreach (DataRow dr in dt_SHOHIN.Rows)
            {
                #region インサートテーブル
                sql = new StringBuilder();
                dpc = new DbParamCollection();
                sql.Append("INSERT INTO PR_HN_TBL(");
                sql.Append("  GUID");
                sql.Append(" ,SORT");
                sql.Append(" ," + Util.ColsArray(prtCols, ""));
                sql.Append(") ");
                sql.Append("VALUES(");
                sql.Append("  @GUID");
                sql.Append(" ,@SORT");
                sql.Append(" ," + Util.ColsArray(prtCols, "@"));
                sql.Append(") ");
                #endregion

                #region データ登録
                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                dpc.SetParam("@SORT", SqlDbType.Int, i);
                // ページヘッダーデータを設定
                dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, UInfo.KaishaNm);
                dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, hyojiDate);

                //データを設定
                dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dr["SHOHIN_CD"]);
                dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dr["SHOHIN_NM"]);
                dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, "0.00");
                dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, "0.00");
                dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, dr["BARASU"]);
                dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, dr["TANI"]);
                dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, dr["SURYO1"]); ;
                dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, dr["SURYO2"]);
                decimal tanka = Util.ToDecimal(dr["SHIIRE_TANKA"]);
                if (tanka == 0 && Util.ToDecimal(dr["SAISHU_SHIIRE_TANKA"]) != 0)
                {
                    tanka = Util.ToDecimal(dr["SAISHU_SHIIRE_TANKA"]);
                }
                dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, tanka);
                decimal zaiko_kingaku = tanka * Util.ToDecimal(dr["BARASU"]);
                dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, zaiko_kingaku);
                dpc.SetParam("@ITEM13", SqlDbType.VarChar, 200, dr["KAISHA_CD"]);
                dpc.SetParam("@ITEM14", SqlDbType.VarChar, 200, dr["SHOHIN_CD"]);
                dpc.SetParam("@ITEM15", SqlDbType.VarChar, 200, dr["SHOHIN_NM"]);
                dpc.SetParam("@ITEM16", SqlDbType.VarChar, 200, outputDate);
                
                this.Dba.ModifyBySql(Util.ToString(sql), dpc);
                i++;
                #endregion
            }

            // 月計払出数量、月計払出金額、月計受入数量、月計受入金額の更新(前回棚卸日の翌日から条件月の末)
            dpc = new DbParamCollection();
            sql = new StringBuilder();
            sql.Append(" SELECT");
            sql.Append("     B.SHOHIN_CD AS SHOHIN_CD,");
            sql.Append("     SUM(CASE WHEN A.DENPYO_KUBUN = 1 THEN (CASE WHEN A.TORIHIKI_KUBUN2 = 2 THEN (((B.SURYO1 * B.IRISU) + B.SURYO2) * -1) ELSE ((B.SURYO1 * B.IRISU) + B.SURYO2) END) ELSE 0 END) AS URIAGE_SURYO,");
            sql.Append("     SUM(CASE WHEN A.DENPYO_KUBUN = 2 THEN (CASE WHEN A.TORIHIKI_KUBUN2 = 2 THEN (((B.SURYO1 * B.IRISU) + B.SURYO2) * -1) ELSE ((B.SURYO1 * B.IRISU) + B.SURYO2) END) ELSE 0 END) AS SHIIRE_SURYO,");
            sql.Append("     SUM(CASE WHEN A.DENPYO_KUBUN = 2 THEN (CASE WHEN A.TORIHIKI_KUBUN2 = 2 THEN B.SHOHIZEI * -1 ELSE B.SHOHIZEI END) ELSE 0 END) AS SHIIRE_SHOHIZEI");
            sql.Append(" FROM TB_HN_TORIHIKI_DENPYO AS A");
            sql.Append(" LEFT OUTER JOIN TB_HN_TORIHIKI_MEISAI AS B");
            sql.Append(" 	ON  A.KAISHA_CD = B.KAISHA_CD");
            sql.Append(" 	AND A.SHISHO_CD = B.SHISHO_CD");
            sql.Append(" 	AND A.KAIKEI_NENDO = B.KAIKEI_NENDO");
            sql.Append(" 	AND A.DENPYO_KUBUN = B.DENPYO_KUBUN");
            sql.Append(" 	AND A.DENPYO_BANGO = B.DENPYO_BANGO");
            sql.Append(" LEFT OUTER JOIN  TB_HN_SHOHIN AS C");
            sql.Append(" 	ON  B.KAISHA_CD = C.KAISHA_CD");
            sql.Append(" 	AND B.SHISHO_CD = C.SHISHO_CD");
            sql.Append(" 	AND B.SHOHIN_CD = C.SHOHIN_CD");
            sql.Append(" WHERE");
            sql.Append("     A.KAISHA_CD = @KAISHA_CD");
            if (shishoCd != "0")
            {
                // 支所コードがゼロなら合算表示
                sql.Append("     AND A.SHISHO_CD = @SHISHO_CD");
            }
            sql.Append(" AND B.SHOHIN_CD BETWEEN @SHOHIN_CD_FR AND @SHOHIN_CD_TO ");
            sql.Append(" AND A.DENPYO_DATE BETWEEN @DATE_FR AND @DATE_TO ");
            sql.Append(" AND C.SHOHIN_KUBUN5 <> 1 ");
            if (Chushikubun != 0)
            {
                sql.Append(" AND ISNULL(C.CHUSHI_KUBUN, 0) = @CHUSHI_KUBUN ");
            }
            sql.Append(" AND C.ZAIKO_KANRI_KUBUN = 1 ");
            sql.Append(" GROUP BY");
            sql.Append("     B.SHOHIN_CD");
            sql.Append(" ORDER BY");
            sql.Append("     B.SHOHIN_CD");

            dpc.SetParam("@DATE_FR", SqlDbType.DateTime, tmpDateFr);
            dpc.SetParam("@DATE_TO", SqlDbType.DateTime, tmpDateTo);
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHOHIN_CD_FR", SqlDbType.Decimal, 13, shohinCdFr);
            dpc.SetParam("@SHOHIN_CD_TO", SqlDbType.Decimal, 13, shohinCdTo);
            dpc.SetParam("@CHUSHI_KUBUN", SqlDbType.Decimal, 4, Chushikubun);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);

            DataTable dtM_TORIHIKI = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);

            foreach (DataRow dr in dtM_TORIHIKI.Rows)
            {
                // 四捨五入処理
                decimal sirSuryo = Util.Round(Util.ToDecimal(dr["SHIIRE_SURYO"]), 2);
                decimal uragSuryo = Util.Round(Util.ToDecimal(dr["URIAGE_SURYO"]), 2);

                sql = new StringBuilder();
                dpc = new DbParamCollection();
                sql.Append("UPDATE PR_HN_TBL SET");
                sql.Append("  ITEM05 = @ITEM05");
                sql.Append(" ,ITEM06 = @ITEM06");
                sql.Append(" WHERE");
                sql.Append("  GUID = @GUID");
                sql.Append(" AND");
                sql.Append("  ITEM03 = @ITEM03");

                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dr["SHOHIN_CD"]);
                dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, sirSuryo);
                dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, uragSuryo);

                this.Dba.ModifyBySql(Util.ToString(sql), dpc);
            }

            #endregion

            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
                "SORT",
                "PR_HN_TBL",
                "GUID = @GUID",
                dpc);

            bool dataFlag;
            if (tmpdtPR_HN_TBL.Rows.Count > 0)
            {
                dataFlag = true;
            }
            else
            {
                dataFlag = false;
            }

            return dataFlag;
        }

        private void GetSettei()
        {
            // 使用する商品区分取得
            gSettei.kbn = Util.ToInt(this.Config.LoadPgConfig(Constants.SubSys.Kob, "TANAOROSHI", "Setting", "ShohinKbn"));
            // 在庫金額計算用単価
            gSettei.tanka = Util.ToInt(this.Config.LoadPgConfig(Constants.SubSys.Kob, "TANAOROSHI", "Setting", "UseTanka"));
            // 棚卸表・記入票の合計印字設定
            gSettei.gokei = Util.ToInt(this.Config.LoadPgConfig(Constants.SubSys.Kob, "TANAOROSHI", "Setting", "PrintGokei"));
            // 棚卸入力ソート順
            gSettei.sort = Util.ToInt(this.Config.LoadPgConfig(Constants.SubSys.Kob, "TANAOROSHI", "Setting", "Sort"));
        }
        private DataRow getSaishuSiireTanka(DateTime tanaoroshiDate, DataRow drShohin)
        {
            #region 最終仕入単価選択時 取得用SQL
            // の検索日付に発生しているデータを取得
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder Sql = new StringBuilder();
            Sql.Append(" EXEC SP_SAISHU_SHIIRE_TANKA @KAISHA_CD, @SHISHO_CD, @SHOHIN_CD, @TANAOROSHI_DATE");

            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.txtMizuageShishoCd.Text);
            // 検索する棚卸日付をセット
            dpc.SetParam("@TANAOROSHI_DATE", SqlDbType.VarChar, 10, tanaoroshiDate);
            // 検索する商品コードをセット
            dpc.SetParam("@SHOHIN_CD", SqlDbType.VarChar, 6, drShohin["SHOHIN_CD"]);
            DataTable dtSaishushiireTanka = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            // 最終仕入単価を取得
            DataRow drSaishushiireTanka;
            if (dtSaishushiireTanka.Rows.Count != 0)
            {
                drSaishushiireTanka = dtSaishushiireTanka.Rows[0];
            }
            else
            {
                drSaishushiireTanka = drShohin;
            }
            #endregion
            return drSaishushiireTanka;
        }
        #endregion
    }
}
