﻿namespace jp.co.fsi.kb.kbmr1061
{
    /// <summary>
    /// KBMR1061R の概要の説明です。
    /// </summary>
    partial class KBMR1061R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(KBMR1061R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.shape4 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.shape3 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.shape2 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.shape1 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtToday = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSuryo01 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblKingaku01 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblTitle01 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.shape5 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.shape6 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.shape7 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.shape8 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.shape9 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.shape11 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.textBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.reportHeader1 = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.reportFooter1 = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtToday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSuryo01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKingaku01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.shape4,
            this.shape3,
            this.shape2,
            this.shape1,
            this.lblDate,
            this.txtToday,
            this.txtPage,
            this.lblPage,
            this.lblSuryo01,
            this.lblKingaku01,
            this.textBox1,
            this.lblTitle01,
            this.label1,
            this.shape5,
            this.shape6,
            this.shape7,
            this.shape8,
            this.shape9,
            this.shape11,
            this.label2,
            this.label3,
            this.label4,
            this.label5,
            this.label6,
            this.label7,
            this.label8,
            this.textBox11});
            this.pageHeader.Height = 0.9506069F;
            this.pageHeader.Name = "pageHeader";
            this.pageHeader.Format += new System.EventHandler(this.pageHeader_Format);
            // 
            // shape4
            // 
            this.shape4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.shape4.Height = 0.2604167F;
            this.shape4.Left = 4.347244F;
            this.shape4.Name = "shape4";
            this.shape4.RoundingRadius = 9.999999F;
            this.shape4.Top = 0.6846457F;
            this.shape4.Width = 0.8507875F;
            // 
            // shape3
            // 
            this.shape3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.shape3.Height = 0.2604167F;
            this.shape3.Left = 3.496457F;
            this.shape3.Name = "shape3";
            this.shape3.RoundingRadius = 9.999999F;
            this.shape3.Top = 0.6846457F;
            this.shape3.Width = 0.8507878F;
            // 
            // shape2
            // 
            this.shape2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.shape2.Height = 0.2604167F;
            this.shape2.Left = 1.103937F;
            this.shape2.Name = "shape2";
            this.shape2.RoundingRadius = 9.999999F;
            this.shape2.Top = 0.6846457F;
            this.shape2.Width = 2.39252F;
            // 
            // shape1
            // 
            this.shape1.BackColor = System.Drawing.Color.Aqua;
            this.shape1.Height = 0.2604167F;
            this.shape1.Left = 0F;
            this.shape1.Name = "shape1";
            this.shape1.RoundingRadius = 9.999999F;
            this.shape1.Top = 0.6846457F;
            this.shape1.Width = 1.103937F;
            // 
            // lblDate
            // 
            this.lblDate.Height = 0.2F;
            this.lblDate.HyperLink = null;
            this.lblDate.Left = 7.927953F;
            this.lblDate.Name = "lblDate";
            this.lblDate.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; ddo-char-set: 1";
            this.lblDate.Text = "作 成 日";
            this.lblDate.Top = 0.1212599F;
            this.lblDate.Width = 0.6299214F;
            // 
            // txtToday
            // 
            this.txtToday.DataField = "ITEM16";
            this.txtToday.Height = 0.2F;
            this.txtToday.Left = 8.636615F;
            this.txtToday.MultiLine = false;
            this.txtToday.Name = "txtToday";
            this.txtToday.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; ddo-char-set: 1";
            this.txtToday.Text = "gy年MM月dd日";
            this.txtToday.Top = 0.1212599F;
            this.txtToday.Width = 1.181103F;
            // 
            // txtPage
            // 
            this.txtPage.Height = 0.2F;
            this.txtPage.Left = 9.842914F;
            this.txtPage.Name = "txtPage";
            this.txtPage.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtPage.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtPage.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.txtPage.Text = "Page";
            this.txtPage.Top = 0.1212599F;
            this.txtPage.Width = 0.3748035F;
            // 
            // lblPage
            // 
            this.lblPage.Height = 0.2F;
            this.lblPage.HyperLink = null;
            this.lblPage.Left = 10.21772F;
            this.lblPage.Name = "lblPage";
            this.lblPage.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; ddo-char-set: 1";
            this.lblPage.Text = "頁";
            this.lblPage.Top = 0.1212599F;
            this.lblPage.Width = 0.1666665F;
            // 
            // lblSuryo01
            // 
            this.lblSuryo01.Height = 0.1574803F;
            this.lblSuryo01.HyperLink = null;
            this.lblSuryo01.Left = 0.06259844F;
            this.lblSuryo01.Name = "lblSuryo01";
            this.lblSuryo01.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; d" +
    "do-char-set: 1";
            this.lblSuryo01.Text = "商品コード";
            this.lblSuryo01.Top = 0.7240158F;
            this.lblSuryo01.Width = 0.9791338F;
            // 
            // lblKingaku01
            // 
            this.lblKingaku01.Height = 0.1574803F;
            this.lblKingaku01.HyperLink = null;
            this.lblKingaku01.Left = 1.166142F;
            this.lblKingaku01.Name = "lblKingaku01";
            this.lblKingaku01.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; d" +
    "do-char-set: 1";
            this.lblKingaku01.Text = "商　　品　　名";
            this.lblKingaku01.Top = 0.7240158F;
            this.lblKingaku01.Width = 2.302362F;
            // 
            // textBox1
            // 
            this.textBox1.DataField = "ITEM01";
            this.textBox1.Height = 0.2F;
            this.textBox1.Left = 0.07874016F;
            this.textBox1.MultiLine = false;
            this.textBox1.Name = "textBox1";
            this.textBox1.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-style: normal; font-weight: normal; d" +
    "do-char-set: 128";
            this.textBox1.Text = null;
            this.textBox1.Top = 0.07874016F;
            this.textBox1.Width = 2.926378F;
            // 
            // lblTitle01
            // 
            this.lblTitle01.Height = 0.2625984F;
            this.lblTitle01.HyperLink = null;
            this.lblTitle01.Left = 3.710629F;
            this.lblTitle01.Name = "lblTitle01";
            this.lblTitle01.Style = "font-family: ＭＳ 明朝; font-size: 15pt; font-weight: bold; ddo-char-set: 1";
            this.lblTitle01.Text = "＊＊＊ 商品管理日報 ＊＊＊";
            this.lblTitle01.Top = 0.1212599F;
            this.lblTitle01.Width = 3.208661F;
            // 
            // label1
            // 
            this.label1.Height = 0.1574803F;
            this.label1.HyperLink = null;
            this.label1.Left = 3.555906F;
            this.label1.Name = "label1";
            this.label1.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; d" +
    "do-char-set: 1";
            this.label1.Text = "入庫数";
            this.label1.Top = 0.7397639F;
            this.label1.Width = 0.7515748F;
            // 
            // shape5
            // 
            this.shape5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.shape5.Height = 0.2604167F;
            this.shape5.Left = 5.198031F;
            this.shape5.Name = "shape5";
            this.shape5.RoundingRadius = 9.999999F;
            this.shape5.Top = 0.6846457F;
            this.shape5.Width = 0.8279527F;
            // 
            // shape6
            // 
            this.shape6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.shape6.Height = 0.2604167F;
            this.shape6.Left = 6.025985F;
            this.shape6.Name = "shape6";
            this.shape6.RoundingRadius = 9.999999F;
            this.shape6.Top = 0.6846457F;
            this.shape6.Width = 0.4448818F;
            // 
            // shape7
            // 
            this.shape7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.shape7.Height = 0.2604167F;
            this.shape7.Left = 6.470867F;
            this.shape7.Name = "shape7";
            this.shape7.RoundingRadius = 9.999999F;
            this.shape7.Top = 0.6846457F;
            this.shape7.Width = 0.9685038F;
            // 
            // shape8
            // 
            this.shape8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.shape8.Height = 0.2604167F;
            this.shape8.Left = 7.439371F;
            this.shape8.Name = "shape8";
            this.shape8.RoundingRadius = 9.999999F;
            this.shape8.Top = 0.6846457F;
            this.shape8.Width = 0.9570864F;
            // 
            // shape9
            // 
            this.shape9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.shape9.Height = 0.2604167F;
            this.shape9.Left = 8.396458F;
            this.shape9.Name = "shape9";
            this.shape9.RoundingRadius = 9.999999F;
            this.shape9.Top = 0.6846457F;
            this.shape9.Width = 0.9570864F;
            // 
            // shape11
            // 
            this.shape11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.shape11.Height = 0.2604167F;
            this.shape11.Left = 9.353544F;
            this.shape11.Name = "shape11";
            this.shape11.RoundingRadius = 9.999999F;
            this.shape11.Top = 0.6846457F;
            this.shape11.Width = 1.030709F;
            // 
            // label2
            // 
            this.label2.Height = 0.1574803F;
            this.label2.HyperLink = null;
            this.label2.Left = 4.383071F;
            this.label2.Name = "label2";
            this.label2.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; d" +
    "do-char-set: 1";
            this.label2.Text = "出庫数";
            this.label2.Top = 0.7397639F;
            this.label2.Width = 0.7874016F;
            // 
            // label3
            // 
            this.label3.Height = 0.1574803F;
            this.label3.HyperLink = null;
            this.label3.Left = 5.230709F;
            this.label3.Name = "label3";
            this.label3.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; d" +
    "do-char-set: 1";
            this.label3.Text = "在庫数";
            this.label3.Top = 0.7397639F;
            this.label3.Width = 0.7515749F;
            // 
            // label4
            // 
            this.label4.Height = 0.1574803F;
            this.label4.HyperLink = null;
            this.label4.Left = 6.043702F;
            this.label4.Name = "label4";
            this.label4.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; d" +
    "do-char-set: 1";
            this.label4.Text = "単位";
            this.label4.Top = 0.7397639F;
            this.label4.Width = 0.4033474F;
            // 
            // label5
            // 
            this.label5.Height = 0.1574803F;
            this.label5.HyperLink = null;
            this.label5.Left = 6.509843F;
            this.label5.Name = "label5";
            this.label5.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; d" +
    "do-char-set: 1";
            this.label5.Text = "数量(ｹｰｽ)";
            this.label5.Top = 0.7397639F;
            this.label5.Width = 0.8976378F;
            // 
            // label6
            // 
            this.label6.Height = 0.1574803F;
            this.label6.HyperLink = null;
            this.label6.Left = 7.459055F;
            this.label6.Name = "label6";
            this.label6.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; d" +
    "do-char-set: 1";
            this.label6.Text = "数量(ﾊﾞﾗ)";
            this.label6.Top = 0.7397639F;
            this.label6.Width = 0.9094486F;
            // 
            // label7
            // 
            this.label7.Height = 0.1574803F;
            this.label7.HyperLink = null;
            this.label7.Left = 8.44252F;
            this.label7.Name = "label7";
            this.label7.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; d" +
    "do-char-set: 1";
            this.label7.Text = "原単価";
            this.label7.Top = 0.7397639F;
            this.label7.Width = 0.8480324F;
            // 
            // label8
            // 
            this.label8.Height = 0.1574803F;
            this.label8.HyperLink = null;
            this.label8.Left = 9.422048F;
            this.label8.Name = "label8";
            this.label8.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; d" +
    "do-char-set: 1";
            this.label8.Text = "原価金額";
            this.label8.Top = 0.7240158F;
            this.label8.Width = 0.9267724F;
            // 
            // textBox11
            // 
            this.textBox11.DataField = "ITEM02";
            this.textBox11.Height = 0.2F;
            this.textBox11.Left = 3.851771F;
            this.textBox11.MultiLine = false;
            this.textBox11.Name = "textBox11";
            this.textBox11.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-style: normal; font-weight: normal; d" +
    "do-char-set: 128";
            this.textBox11.Text = null;
            this.textBox11.Top = 0.403937F;
            this.textBox11.Width = 2.926378F;
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.textBox2,
            this.textBox3,
            this.textBox4,
            this.textBox5,
            this.textBox6,
            this.textBox7,
            this.textBox8,
            this.textBox9,
            this.textBox10,
            this.textBox12,
            this.line1,
            this.line2,
            this.line3,
            this.line4,
            this.line5,
            this.line6,
            this.line7,
            this.line8,
            this.line9,
            this.line10,
            this.line11,
            this.line12});
            this.detail.Height = 0.2440945F;
            this.detail.KeepTogether = true;
            this.detail.Name = "detail";
            this.detail.RepeatToFill = true;
            // 
            // textBox2
            // 
            this.textBox2.DataField = "ITEM03";
            this.textBox2.Height = 0.1665355F;
            this.textBox2.Left = 0.06259843F;
            this.textBox2.Name = "textBox2";
            this.textBox2.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right";
            this.textBox2.Text = "1234567890123";
            this.textBox2.Top = 0.04173229F;
            this.textBox2.Width = 0.9791667F;
            // 
            // textBox3
            // 
            this.textBox3.DataField = "ITEM04";
            this.textBox3.Height = 0.1665355F;
            this.textBox3.Left = 1.13189F;
            this.textBox3.Name = "textBox3";
            this.textBox3.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left";
            this.textBox3.Text = "１２３４５６７８９０１２３４５６７８９０";
            this.textBox3.Top = 0.04173229F;
            this.textBox3.Width = 2.336614F;
            // 
            // textBox4
            // 
            this.textBox4.CanGrow = false;
            this.textBox4.DataField = "ITEM05";
            this.textBox4.Height = 0.1665355F;
            this.textBox4.Left = 3.555906F;
            this.textBox4.MultiLine = false;
            this.textBox4.Name = "textBox4";
            this.textBox4.OutputFormat = resources.GetString("textBox4.OutputFormat");
            this.textBox4.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right";
            this.textBox4.Text = "999,999,999";
            this.textBox4.Top = 0.04173229F;
            this.textBox4.Width = 0.7755906F;
            // 
            // textBox5
            // 
            this.textBox5.CanGrow = false;
            this.textBox5.DataField = "ITEM06";
            this.textBox5.Height = 0.1665355F;
            this.textBox5.Left = 4.383071F;
            this.textBox5.MultiLine = false;
            this.textBox5.Name = "textBox5";
            this.textBox5.OutputFormat = resources.GetString("textBox5.OutputFormat");
            this.textBox5.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right";
            this.textBox5.Text = "999,999,999";
            this.textBox5.Top = 0.04173229F;
            this.textBox5.Width = 0.7874016F;
            // 
            // textBox6
            // 
            this.textBox6.CanGrow = false;
            this.textBox6.DataField = "ITEM07";
            this.textBox6.Height = 0.1665355F;
            this.textBox6.Left = 5.222835F;
            this.textBox6.MultiLine = false;
            this.textBox6.Name = "textBox6";
            this.textBox6.OutputFormat = resources.GetString("textBox6.OutputFormat");
            this.textBox6.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right";
            this.textBox6.Text = "999,999,999";
            this.textBox6.Top = 0.04173229F;
            this.textBox6.Width = 0.7755908F;
            // 
            // textBox7
            // 
            this.textBox7.CanGrow = false;
            this.textBox7.DataField = "ITEM08";
            this.textBox7.Height = 0.1665355F;
            this.textBox7.Left = 6.043701F;
            this.textBox7.MultiLine = false;
            this.textBox7.Name = "textBox7";
            this.textBox7.OutputFormat = resources.GetString("textBox7.OutputFormat");
            this.textBox7.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: center";
            this.textBox7.Text = "9999";
            this.textBox7.Top = 0.04173229F;
            this.textBox7.Width = 0.4035435F;
            // 
            // textBox8
            // 
            this.textBox8.CanGrow = false;
            this.textBox8.DataField = "ITEM09";
            this.textBox8.Height = 0.1665355F;
            this.textBox8.Left = 6.509843F;
            this.textBox8.MultiLine = false;
            this.textBox8.Name = "textBox8";
            this.textBox8.OutputFormat = resources.GetString("textBox8.OutputFormat");
            this.textBox8.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right";
            this.textBox8.Text = "999,999,999";
            this.textBox8.Top = 0.04173229F;
            this.textBox8.Width = 0.8976378F;
            // 
            // textBox9
            // 
            this.textBox9.CanGrow = false;
            this.textBox9.DataField = "ITEM10";
            this.textBox9.Height = 0.1665355F;
            this.textBox9.Left = 7.459056F;
            this.textBox9.MultiLine = false;
            this.textBox9.Name = "textBox9";
            this.textBox9.OutputFormat = resources.GetString("textBox9.OutputFormat");
            this.textBox9.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right";
            this.textBox9.Text = "999,999,999";
            this.textBox9.Top = 0.04173229F;
            this.textBox9.Width = 0.8976378F;
            // 
            // textBox10
            // 
            this.textBox10.CanGrow = false;
            this.textBox10.DataField = "ITEM11";
            this.textBox10.Height = 0.1665355F;
            this.textBox10.Left = 8.44252F;
            this.textBox10.MultiLine = false;
            this.textBox10.Name = "textBox10";
            this.textBox10.OutputFormat = resources.GetString("textBox10.OutputFormat");
            this.textBox10.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right";
            this.textBox10.Text = "999,999,999";
            this.textBox10.Top = 0.04173229F;
            this.textBox10.Width = 0.848031F;
            // 
            // textBox12
            // 
            this.textBox12.CanGrow = false;
            this.textBox12.DataField = "ITEM12";
            this.textBox12.Height = 0.1665355F;
            this.textBox12.Left = 9.422048F;
            this.textBox12.MultiLine = false;
            this.textBox12.Name = "textBox12";
            this.textBox12.OutputFormat = resources.GetString("textBox12.OutputFormat");
            this.textBox12.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right";
            this.textBox12.Text = "9,999,999,999";
            this.textBox12.Top = 0.04173229F;
            this.textBox12.Width = 0.9267712F;
            // 
            // line1
            // 
            this.line1.Height = 0.2397638F;
            this.line1.Left = 0F;
            this.line1.LineWeight = 2F;
            this.line1.Name = "line1";
            this.line1.Top = 0F;
            this.line1.Width = 0F;
            this.line1.X1 = 0F;
            this.line1.X2 = 0F;
            this.line1.Y1 = 0F;
            this.line1.Y2 = 0.2397638F;
            // 
            // line2
            // 
            this.line2.Height = 0.003543302F;
            this.line2.Left = 0F;
            this.line2.LineWeight = 1F;
            this.line2.Name = "line2";
            this.line2.Top = 0.2362205F;
            this.line2.Width = 10.38425F;
            this.line2.X1 = 0F;
            this.line2.X2 = 10.38425F;
            this.line2.Y1 = 0.2362205F;
            this.line2.Y2 = 0.2397638F;
            // 
            // line3
            // 
            this.line3.Height = 0.2397638F;
            this.line3.Left = 1.103937F;
            this.line3.LineWeight = 1F;
            this.line3.Name = "line3";
            this.line3.Top = 0F;
            this.line3.Width = 0F;
            this.line3.X1 = 1.103937F;
            this.line3.X2 = 1.103937F;
            this.line3.Y1 = 0F;
            this.line3.Y2 = 0.2397638F;
            // 
            // line4
            // 
            this.line4.Height = 0.2397638F;
            this.line4.Left = 3.496457F;
            this.line4.LineWeight = 1F;
            this.line4.Name = "line4";
            this.line4.Top = 0F;
            this.line4.Width = 0F;
            this.line4.X1 = 3.496457F;
            this.line4.X2 = 3.496457F;
            this.line4.Y1 = 0F;
            this.line4.Y2 = 0.2397638F;
            // 
            // line5
            // 
            this.line5.Height = 0.2397638F;
            this.line5.Left = 4.347244F;
            this.line5.LineWeight = 1F;
            this.line5.Name = "line5";
            this.line5.Top = 0F;
            this.line5.Width = 0F;
            this.line5.X1 = 4.347244F;
            this.line5.X2 = 4.347244F;
            this.line5.Y1 = 0F;
            this.line5.Y2 = 0.2397638F;
            // 
            // line6
            // 
            this.line6.Height = 0.2397638F;
            this.line6.Left = 5.198032F;
            this.line6.LineWeight = 1F;
            this.line6.Name = "line6";
            this.line6.Top = 0F;
            this.line6.Width = 0F;
            this.line6.X1 = 5.198032F;
            this.line6.X2 = 5.198032F;
            this.line6.Y1 = 0F;
            this.line6.Y2 = 0.2397638F;
            // 
            // line7
            // 
            this.line7.Height = 0.2397638F;
            this.line7.Left = 6.025985F;
            this.line7.LineWeight = 1F;
            this.line7.Name = "line7";
            this.line7.Top = 0F;
            this.line7.Width = 0F;
            this.line7.X1 = 6.025985F;
            this.line7.X2 = 6.025985F;
            this.line7.Y1 = 0F;
            this.line7.Y2 = 0.2397638F;
            // 
            // line8
            // 
            this.line8.Height = 0.2397638F;
            this.line8.Left = 6.470867F;
            this.line8.LineWeight = 1F;
            this.line8.Name = "line8";
            this.line8.Top = 0F;
            this.line8.Width = 0F;
            this.line8.X1 = 6.470867F;
            this.line8.X2 = 6.470867F;
            this.line8.Y1 = 0F;
            this.line8.Y2 = 0.2397638F;
            // 
            // line9
            // 
            this.line9.Height = 0.2397638F;
            this.line9.Left = 7.439371F;
            this.line9.LineWeight = 1F;
            this.line9.Name = "line9";
            this.line9.Top = 0F;
            this.line9.Width = 0F;
            this.line9.X1 = 7.439371F;
            this.line9.X2 = 7.439371F;
            this.line9.Y1 = 0F;
            this.line9.Y2 = 0.2397638F;
            // 
            // line10
            // 
            this.line10.Height = 0.2397638F;
            this.line10.Left = 8.396458F;
            this.line10.LineWeight = 1F;
            this.line10.Name = "line10";
            this.line10.Top = 0F;
            this.line10.Width = 0F;
            this.line10.X1 = 8.396458F;
            this.line10.X2 = 8.396458F;
            this.line10.Y1 = 0F;
            this.line10.Y2 = 0.2397638F;
            // 
            // line11
            // 
            this.line11.Height = 0.2397638F;
            this.line11.Left = 9.353544F;
            this.line11.LineWeight = 1F;
            this.line11.Name = "line11";
            this.line11.Top = 0F;
            this.line11.Width = 0F;
            this.line11.X1 = 9.353544F;
            this.line11.X2 = 9.353544F;
            this.line11.Y1 = 0F;
            this.line11.Y2 = 0.2397638F;
            // 
            // line12
            // 
            this.line12.Height = 0.2397638F;
            this.line12.Left = 10.38425F;
            this.line12.LineWeight = 1F;
            this.line12.Name = "line12";
            this.line12.Top = 0F;
            this.line12.Width = 0F;
            this.line12.X1 = 10.38425F;
            this.line12.X2 = 10.38425F;
            this.line12.Y1 = 0F;
            this.line12.Y2 = 0.2397638F;
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            // 
            // reportHeader1
            // 
            this.reportHeader1.Height = 0F;
            this.reportHeader1.Name = "reportHeader1";
            // 
            // reportFooter1
            // 
            this.reportFooter1.Height = 0F;
            this.reportFooter1.Name = "reportFooter1";
            // 
            // KBMR1061R
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.2755905F;
            this.PageSettings.Margins.Left = 0.5275591F;
            this.PageSettings.Margins.Right = 0.5275591F;
            this.PageSettings.Margins.Top = 0.2755905F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
            this.PageSettings.PaperHeight = 11.69291F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.267716F;
            this.PrintWidth = 10.62992F;
            this.Sections.Add(this.reportHeader1);
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.pageFooter);
            this.Sections.Add(this.reportFooter1);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtToday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSuryo01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKingaku01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.ReportHeader reportHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.ReportFooter reportFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtToday;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle01;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSuryo01;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblKingaku01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox1;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape4;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape3;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape2;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape5;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape6;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape7;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape8;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape9;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape11;
        private GrapeCity.ActiveReports.SectionReportModel.Label label2;
        private GrapeCity.ActiveReports.SectionReportModel.Label label3;
        private GrapeCity.ActiveReports.SectionReportModel.Label label4;
        private GrapeCity.ActiveReports.SectionReportModel.Label label5;
        private GrapeCity.ActiveReports.SectionReportModel.Label label6;
        private GrapeCity.ActiveReports.SectionReportModel.Label label7;
        private GrapeCity.ActiveReports.SectionReportModel.Label label8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox12;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
        private GrapeCity.ActiveReports.SectionReportModel.Line line2;
        private GrapeCity.ActiveReports.SectionReportModel.Line line3;
        private GrapeCity.ActiveReports.SectionReportModel.Line line4;
        private GrapeCity.ActiveReports.SectionReportModel.Line line5;
        private GrapeCity.ActiveReports.SectionReportModel.Line line6;
        private GrapeCity.ActiveReports.SectionReportModel.Line line7;
        private GrapeCity.ActiveReports.SectionReportModel.Line line8;
        private GrapeCity.ActiveReports.SectionReportModel.Line line9;
        private GrapeCity.ActiveReports.SectionReportModel.Line line10;
        private GrapeCity.ActiveReports.SectionReportModel.Line line11;
        private GrapeCity.ActiveReports.SectionReportModel.Line line12;
    }
}
