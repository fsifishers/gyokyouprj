﻿using System;
using System.Collections;
using System.Data;
using System.Text;
using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.userinfo;
using jp.co.fsi.common.util;

namespace jp.co.fsi.kb.kbdb1011
{
	// Token: 0x02000002 RID: 2
	public class KBDB1011DA
	{
		// Token: 0x06000001 RID: 1 RVA: 0x00002050 File Offset: 0x00000250
		public KBDB1011DA(UserInfo uInfo, DbAccess dba, ConfigLoader config)
		{
			this._uInfo = uInfo;
			this._dba = dba;
			this._config = config;
			this.ShishoCode = Util.ToInt(this._uInfo.ShishoCd);
		}

		// Token: 0x06000002 RID: 2 RVA: 0x00002084 File Offset: 0x00000284
		public DataTable GetSwkTgtData(int mode, Hashtable condition)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("SELECT ");
			stringBuilder.Append("  B.TOKUISAKI_CD          AS 会員番号 ");
			stringBuilder.Append(" ,B.TORIHIKI_KUBUN1       AS 取引区分１ ");
			stringBuilder.Append(" ,B.TORIHIKI_KUBUN2       AS 取引区分２ ");
			stringBuilder.Append(" ,MIN(B.TOKUISAKI_NM)     AS 会員名称 ");
			stringBuilder.Append(" ,MIN(A.SHOHIZEI_NYURYOKU_HOHO) AS 消費税入力方法 ");
			stringBuilder.Append(" ,MIN(C.SHOHIZEI_TENKA_HOHO) AS 消費税転嫁方法 ");
			stringBuilder.Append(" ,MIN(C.SHOHIZEI_HASU_SHORI) AS 消費税端数処理 ");
			stringBuilder.Append(" ,B.TANTOSHA_CD           AS 担当者コード ");
			stringBuilder.Append(" ,A.BUMON_CD              AS 部門コード ");
			stringBuilder.Append(" ,A.SHIWAKE_CD            AS 仕訳コード ");
			stringBuilder.Append(" ,A.ZEI_KUBUN             AS 税区分 ");
			stringBuilder.Append(" ,A.JIGYO_KUBUN           AS 事業区分 ");
			stringBuilder.Append(" ,A.ZEI_RITSU             AS 税率 ");
			stringBuilder.Append(" ,SUM(CASE WHEN C.SHOHIZEI_TENKA_HOHO = 3 THEN ");
			stringBuilder.Append("                CASE WHEN A.SHOHIZEI_NYURYOKU_HOHO = 1 THEN A.BAIKA_KINGAKU ");
			stringBuilder.Append("                     ELSE CASE WHEN B.TORIHIKI_KUBUN1 = 2 THEN A.BAIKA_KINGAKU ");
			stringBuilder.Append("                               ELSE 0 ");
			stringBuilder.Append("                          END ");
			stringBuilder.Append("                END ");
			stringBuilder.Append("           ELSE CASE WHEN A.SHOHIZEI_NYURYOKU_HOHO = 3 THEN (A.BAIKA_KINGAKU - A.SHOHIZEI) ");
			stringBuilder.Append("                     ELSE A.BAIKA_KINGAKU ");
			stringBuilder.Append("                END ");
			stringBuilder.Append("      END) AS 売上金額 ");
			stringBuilder.Append(" ,SUM(CASE WHEN C.SHOHIZEI_TENKA_HOHO = 3 THEN ");
			stringBuilder.Append("                CASE WHEN B.TORIHIKI_KUBUN1 = 2 THEN A.SHOHIZEI ");
			stringBuilder.Append("                     ELSE 0 ");
			stringBuilder.Append("                END ");
			stringBuilder.Append("           ELSE A.SHOHIZEI ");
			stringBuilder.Append("      END) AS 消費税 ");
			stringBuilder.Append(" ,SUM(CASE WHEN C.SHOHIZEI_TENKA_HOHO = 3 THEN ");
			stringBuilder.Append("                CASE WHEN A.SHOHIZEI_NYURYOKU_HOHO = 2 THEN ");
			stringBuilder.Append("                          CASE WHEN B.TORIHIKI_KUBUN1 = 2 THEN 0 ");
			stringBuilder.Append("                               ELSE A.BAIKA_KINGAKU ");
			stringBuilder.Append("                          END ");
			stringBuilder.Append("                     ELSE 0 ");
			stringBuilder.Append("                END ");
			stringBuilder.Append("           ELSE 0 ");
			stringBuilder.Append("      END) AS 外税売上金額 ");
			stringBuilder.Append(" ,SUM(CASE WHEN C.SHOHIZEI_TENKA_HOHO = 3 THEN ");
			stringBuilder.Append("                CASE WHEN A.SHOHIZEI_NYURYOKU_HOHO = 3 THEN ");
			stringBuilder.Append("                          CASE WHEN B.TORIHIKI_KUBUN1 = 2 THEN 0 ");
			stringBuilder.Append("                               ELSE A.BAIKA_KINGAKU ");
			stringBuilder.Append("                          END ");
			stringBuilder.Append("                     ELSE 0 ");
			stringBuilder.Append("                END ");
			stringBuilder.Append("           ELSE 0 ");
			stringBuilder.Append("      END) AS 内税売上金額 ");
			stringBuilder.Append("FROM ");
			stringBuilder.Append("    TB_HN_TORIHIKI_MEISAI AS A ");
			stringBuilder.Append("LEFT OUTER JOIN TB_HN_TORIHIKI_DENPYO AS B ");
			stringBuilder.Append("ON A.KAISHA_CD = B.KAISHA_CD ");
			stringBuilder.Append("AND A.SHISHO_CD = B.SHISHO_CD ");
			stringBuilder.Append("AND A.KAIKEI_NENDO = B.KAIKEI_NENDO ");
			stringBuilder.Append("AND A.DENPYO_KUBUN = B.DENPYO_KUBUN ");
			stringBuilder.Append("AND A.DENPYO_BANGO = B.DENPYO_BANGO ");
			stringBuilder.Append("LEFT OUTER JOIN VI_HN_TORIHIKISAKI_JOHO AS C ");
			stringBuilder.Append("ON B.KAISHA_CD = C.KAISHA_CD ");
			stringBuilder.Append("AND B.TOKUISAKI_CD = C.TORIHIKISAKI_CD ");
			stringBuilder.Append("AND C.SHUBETU_KUBUN= 1 ");
			stringBuilder.Append("WHERE ");
			stringBuilder.Append("    A.KAISHA_CD = @KAISHA_CD ");
			stringBuilder.Append("AND A.SHISHO_CD = @SHISHO_CD ");
			stringBuilder.Append("AND A.KAIKEI_NENDO = @KAIKEI_NENDO ");
			stringBuilder.Append("AND A.DENPYO_KUBUN = 1 ");
			stringBuilder.Append("AND B.DENPYO_DATE BETWEEN @DENPYO_DATE_FR AND @DENPYO_DATE_TO ");
			stringBuilder.Append("AND B.TOKUISAKI_CD BETWEEN @KAIIN_BANGO_FR AND @KAIIN_BANGO_TO ");
			if (mode == 1)
			{
				stringBuilder.Append("AND ISNULL(B.IKKATSU_DENPYO_BANGO, 0) = 0 ");
			}
			if ((KBDB1011.SKbn)condition["SakuseiKbn"] == KBDB1011.SKbn.Shimebi)
			{
				stringBuilder.Append("AND C.SIMEBI = @SHIMEBI ");
			}
			stringBuilder.Append(this.CreateWhereByToriKbn(condition));
			stringBuilder.Append("AND A.BAIKA_KINGAKU <> 0 ");
			stringBuilder.Append("AND B.SHIWAKE_DENPYO_BANGO = 0 ");
			stringBuilder.Append("AND A.SHIWAKE_CD <> 0 ");
			stringBuilder.Append("GROUP BY ");
			stringBuilder.Append("  B.TOKUISAKI_CD ");
			stringBuilder.Append(" ,B.TORIHIKI_KUBUN1 ");
			stringBuilder.Append(" ,B.TORIHIKI_KUBUN2 ");
			stringBuilder.Append(" ,B.TANTOSHA_CD ");
			stringBuilder.Append(" ,A.BUMON_CD ");
			stringBuilder.Append(" ,A.SHIWAKE_CD ");
			stringBuilder.Append(" ,A.ZEI_KUBUN ");
			stringBuilder.Append(" ,A.JIGYO_KUBUN ");
			stringBuilder.Append(" ,A.ZEI_RITSU ");
			stringBuilder.Append("ORDER BY ");
			stringBuilder.Append("  B.TOKUISAKI_CD ");
			stringBuilder.Append(" ,B.TORIHIKI_KUBUN1 ");
			stringBuilder.Append(" ,B.TORIHIKI_KUBUN2 ");
			DbParamCollection dbParamCollection = new DbParamCollection();
			dbParamCollection.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
			dbParamCollection.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
			dbParamCollection.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
			dbParamCollection.SetParam("@DENPYO_DATE_FR", SqlDbType.DateTime, Util.ToDate(condition["DpyDtFr"]));
			dbParamCollection.SetParam("@DENPYO_DATE_TO", SqlDbType.DateTime, Util.ToDate(condition["DpyDtTo"]));
			dbParamCollection.SetParam("@KAIIN_BANGO_FR", SqlDbType.VarChar, 4, Util.ToString(condition["FunanushiCdFr"]));
			dbParamCollection.SetParam("@KAIIN_BANGO_TO", SqlDbType.VarChar, 4, Util.ToString(condition["FunanushiCdTo"]));
			if ((KBDB1011.SKbn)condition["SakuseiKbn"] == KBDB1011.SKbn.Shimebi)
			{
				dbParamCollection.SetParam("@SHIMEBI", SqlDbType.Decimal, 2, Util.ToInt(condition["Shimebi"]));
			}
			return this._dba.GetDataTableFromSqlWithParams(stringBuilder.ToString(), dbParamCollection);
		}

		// Token: 0x06000003 RID: 3 RVA: 0x000025DC File Offset: 0x000007DC
		public DataSet GetTaishakuData(Hashtable condition, DataTable zeiSetting, DataTable jdSwkSettingA, DataTable jdSwkSettingB, DataTable swkTgtData)
		{
			DataSet dataSet = new DataSet();
			StringBuilder stringBuilder = new StringBuilder();
			decimal d = 0m;
			decimal num = 0m;
			decimal num2 = 0m;
			int num3 = 0;
			string text = string.Empty;
			int num4 = Util.ToInt(this._config.LoadPgConfig(Constants.SubSys.Kob, "KBDB1011", "Setting", "SwkDpMk"));
			Util.ToInt(this._config.LoadPgConfig(Constants.SubSys.Kob, "KBDB1011", "Setting", "BumonCd"));
			DataRow dataRow = this._dba.GetKaikeiSettingsByKessanKi(this._uInfo.KaishaCd, this._uInfo.KessanKi).Rows[0];
			int num5 = Util.ToInt(dataRow["JIGYO_KUBUN"]);
			int num6 = Util.ToInt(dataRow["SHOHIZEI_NYURYOKU_HOHO"]);
			int num7 = 1;
			DataTable dataTable = new DataTable();
			dataTable.Columns.Add("GYO_BANGO", typeof(decimal));
			dataTable.Columns.Add("SEIKYUSAKI_CD", typeof(decimal));
			dataTable.Columns.Add("TAISHAKU_KUBUN", typeof(decimal));
			dataTable.Columns.Add("MEISAI_KUBUN", typeof(decimal));
			dataTable.Columns.Add("DENPYO_KUBUN", typeof(decimal));
			dataTable.Columns.Add("DENPYO_DATE", typeof(DateTime));
			dataTable.Columns.Add("KANJO_KAMOKU_CD", typeof(decimal));
			dataTable.Columns.Add("KANJO_KAMOKU_NM", typeof(string));
			dataTable.Columns.Add("HOJO_KAMOKU_CD", typeof(decimal));
			dataTable.Columns.Add("HOJO_KAMOKU_NM", typeof(string));
			dataTable.Columns.Add("BUMON_CD", typeof(decimal));
			dataTable.Columns.Add("TEKIYO_CD", typeof(decimal));
			dataTable.Columns.Add("TEKIYO", typeof(string));
			dataTable.Columns.Add("ZEIKOMI_KINGAKU", typeof(decimal));
			dataTable.Columns.Add("ZEINUKI_KINGAKU", typeof(decimal));
			dataTable.Columns.Add("SHOHIZEI_KINGAKU", typeof(decimal));
			dataTable.Columns.Add("ZEI_KUBUN", typeof(decimal));
			dataTable.Columns.Add("KAZEI_KUBUN", typeof(decimal));
			dataTable.Columns.Add("TORIHIKI_KUBUN", typeof(decimal));
			dataTable.Columns.Add("ZEI_RITSU", typeof(decimal));
			dataTable.Columns.Add("JIGYO_KUBUN", typeof(decimal));
			dataTable.Columns.Add("SHOHIZEI_NYURYOKU_HOHO", typeof(decimal));
			dataTable.Columns.Add("SHOHIZEI_HENKO", typeof(decimal));
			dataTable.Columns.Add("KESSAN_KUBUN", typeof(decimal));
			dataTable.Columns.Add("SHIWAKE_SAKUSEI_KUBUN", typeof(decimal));
			dataTable.Columns.Add("CHK_ZUMI", typeof(decimal));
			dataTable.Columns.Add("DEL_FLG", typeof(decimal));
			DataTable dataTable2;
			DataRow[] array4;
			DataTable dataTable3;
			for (int i = 0; i < swkTgtData.Rows.Count; i++)
			{
				if (num4 == 3 && !ValChk.IsEmpty(text) && !Util.ToString(swkTgtData.Rows[i]["会員番号"]).Equals(text))
				{
					DataRow[] array = dataTable.Select(null, "GYO_BANGO, TAISHAKU_KUBUN, MEISAI_KUBUN");
					for (int j = 0; j < array.Length; j++)
					{
						if (Util.ToInt(array[j]["DEL_FLG"]) != 1)
						{
							stringBuilder = new StringBuilder();
							stringBuilder.Append("SEIKYUSAKI_CD = " + Util.ToString(array[j]["SEIKYUSAKI_CD"]));
							stringBuilder.Append(" AND TAISHAKU_KUBUN = " + Util.ToString(array[j]["TAISHAKU_KUBUN"]));
							stringBuilder.Append(" AND MEISAI_KUBUN = " + Util.ToString(array[j]["MEISAI_KUBUN"]));
							stringBuilder.Append(" AND KANJO_KAMOKU_CD = " + Util.ToString(array[j]["KANJO_KAMOKU_CD"]));
							stringBuilder.Append(" AND HOJO_KAMOKU_CD = " + Util.ToString(array[j]["HOJO_KAMOKU_CD"]));
							stringBuilder.Append(" AND BUMON_CD = " + Util.ToString(array[j]["BUMON_CD"]));
							stringBuilder.Append(" AND ZEI_KUBUN = " + Util.ToString(array[j]["ZEI_KUBUN"]));
							stringBuilder.Append(" AND JIGYO_KUBUN = " + Util.ToString(array[j]["JIGYO_KUBUN"]));
							stringBuilder.Append(" AND ZEI_RITSU = " + Util.ToString(array[j]["ZEI_RITSU"]));
							stringBuilder.Append(" AND DEL_FLG = 0 ");
							DataRow[] array2 = dataTable.Select(stringBuilder.ToString(), "GYO_BANGO, TAISHAKU_KUBUN, MEISAI_KUBUN");
							for (int k = 0; k < array2.Length; k++)
							{
								if (k != 0 && Util.ToInt(array2[k]["DEL_FLG"]) != 1)
								{
									array2[0]["ZEIKOMI_KINGAKU"] = Util.ToDecimal(array2[0]["ZEIKOMI_KINGAKU"]) + Util.ToDecimal(array2[k]["ZEIKOMI_KINGAKU"]);
									array2[0]["ZEINUKI_KINGAKU"] = Util.ToDecimal(array2[0]["ZEINUKI_KINGAKU"]) + Util.ToDecimal(array2[k]["ZEINUKI_KINGAKU"]);
									array2[0]["SHOHIZEI_KINGAKU"] = Util.ToDecimal(array2[0]["SHOHIZEI_KINGAKU"]) + Util.ToDecimal(array2[k]["SHOHIZEI_KINGAKU"]);
									array2[k]["DEL_FLG"] = 1;
								}
							}
						}
					}
					dataTable2 = dataTable.Clone();
					foreach (object obj in dataTable.Rows)
					{
						DataRow dataRow2 = (DataRow)obj;
						if (Util.ToInt(dataRow2["DEL_FLG"]) == 0 && Util.ToInt(dataRow2["MEISAI_KUBUN"]) == 0)
						{
							if (Util.ToDecimal(dataRow2["ZEIKOMI_KINGAKU"]) < 0m)
							{
								dataRow2["ZEIKOMI_KINGAKU"] = Math.Abs(Util.ToDecimal(dataRow2["ZEIKOMI_KINGAKU"]));
								dataRow2["ZEINUKI_KINGAKU"] = Math.Abs(Util.ToDecimal(dataRow2["ZEINUKI_KINGAKU"]));
								dataRow2["SHOHIZEI_KINGAKU"] = Math.Abs(Util.ToDecimal(dataRow2["SHOHIZEI_KINGAKU"]));
								dataRow2["TAISHAKU_KUBUN"] = ((Util.ToInt(dataRow2["TAISHAKU_KUBUN"]) == 1) ? 2 : 1);
							}
							dataTable2.ImportRow(dataRow2);
							if (Util.ToInt(dataRow2["KAZEI_KUBUN"]) == 1 && Util.ToDecimal(dataRow2["SHOHIZEI_KINGAKU"]) > 0m)
							{
								int ukeBriKbn = 2;
								if (Util.ToInt(dataRow2["TORIHIKI_KUBUN"]) >= 10 && Util.ToInt(dataRow2["TORIHIKI_KUBUN"]) <= 19)
								{
									ukeBriKbn = 1;
								}
								DataRow zeiSettingRow = this.GetZeiSettingRow(zeiSetting, ukeBriKbn);
								DataRow dataRow3 = dataTable2.NewRow();
								dataRow3["GYO_BANGO"] = dataRow2["GYO_BANGO"];
								dataRow3["SEIKYUSAKI_CD"] = dataRow2["SEIKYUSAKI_CD"];
								dataRow3["TAISHAKU_KUBUN"] = dataRow2["TAISHAKU_KUBUN"];
								dataRow3["MEISAI_KUBUN"] = 1;
								dataRow3["DENPYO_KUBUN"] = 1;
								dataRow3["DENPYO_DATE"] = dataRow2["DENPYO_DATE"];
								dataRow3["BUMON_CD"] = ((Util.ToInt(zeiSettingRow["BUMON_UMU"]) == 0) ? 0 : dataRow2["BUMON_CD"]);
								dataRow3["TEKIYO_CD"] = dataRow2["TEKIYO_CD"];
								dataRow3["TEKIYO"] = dataRow2["TEKIYO"];
								dataRow3["ZEIKOMI_KINGAKU"] = 0;
								dataRow3["ZEINUKI_KINGAKU"] = Util.ToDecimal(dataRow2["SHOHIZEI_KINGAKU"]);
								dataRow3["SHOHIZEI_KINGAKU"] = 0;
								dataRow3["SHOHIZEI_NYURYOKU_HOHO"] = num6;
								dataRow3["SHOHIZEI_HENKO"] = dataRow2["SHOHIZEI_HENKO"];
								dataRow3["KESSAN_KUBUN"] = dataRow2["KESSAN_KUBUN"];
								dataRow3["SHIWAKE_SAKUSEI_KUBUN"] = dataRow2["SHIWAKE_SAKUSEI_KUBUN"];
								dataRow3["JIGYO_KUBUN"] = dataRow2["JIGYO_KUBUN"];
								dataRow3["CHK_ZUMI"] = 0;
								dataRow3["DEL_FLG"] = 0;
								if (zeiSettingRow != null)
								{
									dataRow3["KANJO_KAMOKU_CD"] = zeiSettingRow["KANJO_KAMOKU_CD"];
									dataRow3["KANJO_KAMOKU_NM"] = zeiSettingRow["KANJO_KAMOKU_NM"];
									dataRow3["HOJO_KAMOKU_CD"] = ((Util.ToInt(Util.ToString(zeiSettingRow["HOJO_KAMOKU_UMU"])) == 0) ? 0 : dataRow2["HOJO_KAMOKU_CD"]);
									dataRow3["HOJO_KAMOKU_NM"] = ((Util.ToInt(Util.ToString(zeiSettingRow["HOJO_KAMOKU_UMU"])) == 0) ? DBNull.Value : dataRow2["HOJO_KAMOKU_NM"]);
									if (Util.ToInt(Util.ToString(dataRow2["TAISHAKU_KUBUN"])) == 1)
									{
										dataRow3["ZEI_KUBUN"] = zeiSettingRow["KARIKATA_ZEI_KUBUN"];
										dataRow3["KAZEI_KUBUN"] = zeiSettingRow["KARI_KAZEI_KUBUN"];
										dataRow3["TORIHIKI_KUBUN"] = zeiSettingRow["KARI_TORIHIKI_KUBUN"];
										dataRow3["ZEI_RITSU"] = zeiSettingRow["KARI_ZEI_RITSU"];
									}
									else
									{
										dataRow3["ZEI_KUBUN"] = zeiSettingRow["KASHIKATA_ZEI_KUBUN"];
										dataRow3["KAZEI_KUBUN"] = zeiSettingRow["KASHI_KAZEI_KUBUN"];
										dataRow3["TORIHIKI_KUBUN"] = zeiSettingRow["KASHI_TORIHIKI_KUBUN"];
										dataRow3["ZEI_RITSU"] = zeiSettingRow["KASHI_ZEI_RITSU"];
									}
								}
								else
								{
									dataRow3["KANJO_KAMOKU_CD"] = DBNull.Value;
									dataRow3["KANJO_KAMOKU_NM"] = DBNull.Value;
									dataRow3["HOJO_KAMOKU_CD"] = DBNull.Value;
									dataRow3["HOJO_KAMOKU_NM"] = DBNull.Value;
									dataRow3["ZEI_KUBUN"] = DBNull.Value;
									dataRow3["KAZEI_KUBUN"] = DBNull.Value;
									dataRow3["TORIHIKI_KUBUN"] = DBNull.Value;
									dataRow3["ZEI_RITSU"] = DBNull.Value;
								}
								dataTable2.Rows.Add(dataRow3);
							}
						}
					}
					dataTable.Clear();
					dataTable = dataTable2.Copy();
					DataRow[] array3 = dataTable.Select("TAISHAKU_KUBUN = 1", "GYO_BANGO, MEISAI_KUBUN");
					int num8 = 0;
					for (int l = 0; l < array3.Length; l++)
					{
						if (Util.ToInt(array3[l]["GYO_BANGO"]) > num8 + 1)
						{
							array3[l]["GYO_BANGO"] = num8 + 1;
						}
						num8 = Util.ToInt(array3[l]["GYO_BANGO"]);
					}
					array3 = dataTable.Select("TAISHAKU_KUBUN = 2", "GYO_BANGO, MEISAI_KUBUN");
					num8 = 0;
					for (int m = 0; m < array3.Length; m++)
					{
						if (Util.ToInt(array3[m]["GYO_BANGO"]) > num8 + 1)
						{
							array3[m]["GYO_BANGO"] = num8 + 1;
						}
						num8 = Util.ToInt(array3[m]["GYO_BANGO"]);
					}
					array4 = dataTable.Select(null, "GYO_BANGO, TAISHAKU_KUBUN, MEISAI_KUBUN");
					dataTable3 = dataTable.Clone();
					for (int n = 0; n < array4.Length; n++)
					{
						dataTable3.ImportRow(array4[n]);
					}
					dataSet.Tables.Add(dataTable3);
					dataTable = new DataTable();
					dataTable.Columns.Add("GYO_BANGO", typeof(decimal));
					dataTable.Columns.Add("SEIKYUSAKI_CD", typeof(decimal));
					dataTable.Columns.Add("TAISHAKU_KUBUN", typeof(decimal));
					dataTable.Columns.Add("MEISAI_KUBUN", typeof(decimal));
					dataTable.Columns.Add("DENPYO_KUBUN", typeof(decimal));
					dataTable.Columns.Add("DENPYO_DATE", typeof(DateTime));
					dataTable.Columns.Add("KANJO_KAMOKU_CD", typeof(decimal));
					dataTable.Columns.Add("KANJO_KAMOKU_NM", typeof(string));
					dataTable.Columns.Add("HOJO_KAMOKU_CD", typeof(decimal));
					dataTable.Columns.Add("HOJO_KAMOKU_NM", typeof(string));
					dataTable.Columns.Add("BUMON_CD", typeof(decimal));
					dataTable.Columns.Add("TEKIYO_CD", typeof(decimal));
					dataTable.Columns.Add("TEKIYO", typeof(string));
					dataTable.Columns.Add("ZEIKOMI_KINGAKU", typeof(decimal));
					dataTable.Columns.Add("ZEINUKI_KINGAKU", typeof(decimal));
					dataTable.Columns.Add("SHOHIZEI_KINGAKU", typeof(decimal));
					dataTable.Columns.Add("ZEI_KUBUN", typeof(decimal));
					dataTable.Columns.Add("KAZEI_KUBUN", typeof(decimal));
					dataTable.Columns.Add("TORIHIKI_KUBUN", typeof(decimal));
					dataTable.Columns.Add("ZEI_RITSU", typeof(decimal));
					dataTable.Columns.Add("JIGYO_KUBUN", typeof(decimal));
					dataTable.Columns.Add("SHOHIZEI_NYURYOKU_HOHO", typeof(decimal));
					dataTable.Columns.Add("SHOHIZEI_HENKO", typeof(decimal));
					dataTable.Columns.Add("KESSAN_KUBUN", typeof(decimal));
					dataTable.Columns.Add("SHIWAKE_SAKUSEI_KUBUN", typeof(decimal));
					dataTable.Columns.Add("CHK_ZUMI", typeof(decimal));
					dataTable.Columns.Add("DEL_FLG", typeof(decimal));
					num3 = 0;
				}
				DataRow swkSettingBRec = this.GetSwkSettingBRec(jdSwkSettingB, Util.ToInt(swkTgtData.Rows[i]["仕訳コード"]));
				DataRow swkSettingARec = this.GetSwkSettingARec(jdSwkSettingA, Util.ToInt(swkTgtData.Rows[i]["取引区分１"]), Util.ToInt(swkTgtData.Rows[i]["取引区分２"]));
				num3++;
				bool flag = false;
				bool flag2 = false;
				d = 0m;
				DataRow dataRow4 = dataTable.NewRow();
				dataRow4["GYO_BANGO"] = num3;
				dataRow4["SEIKYUSAKI_CD"] = swkTgtData.Rows[i]["会員番号"];
				dataRow4["TAISHAKU_KUBUN"] = ((Util.ToInt(swkTgtData.Rows[i]["取引区分２"]) == 2) ? 1 : 2);
				dataRow4["MEISAI_KUBUN"] = 0;
				dataRow4["DENPYO_KUBUN"] = 1;
				dataRow4["DENPYO_DATE"] = condition["SwkDpyDt"];
				dataRow4["TEKIYO_CD"] = Util.ToDecimal(condition["TekiyoCd"]);
				dataRow4["TEKIYO"] = condition["Tekiyo"];
				dataRow4["SHOHIZEI_NYURYOKU_HOHO"] = num6;
				dataRow4["SHOHIZEI_HENKO"] = 0;
				dataRow4["KESSAN_KUBUN"] = 0;
				dataRow4["SHIWAKE_SAKUSEI_KUBUN"] = num7;
				dataRow4["CHK_ZUMI"] = 0;
				dataRow4["DEL_FLG"] = 0;
				if (swkSettingBRec != null)
				{
					dataRow4["BUMON_CD"] = swkTgtData.Rows[i]["部門コード"];
					dataRow4["KANJO_KAMOKU_CD"] = swkSettingBRec["勘定科目コード"];
					dataRow4["KANJO_KAMOKU_NM"] = swkSettingBRec["勘定科目名"];
					dataRow4["HOJO_KAMOKU_CD"] = ((Util.ToInt(Util.ToString(swkSettingBRec["補助科目コード"])) == -1) ? swkTgtData.Rows[i]["会員番号"] : swkSettingBRec["補助科目コード"]);
					dataRow4["HOJO_KAMOKU_NM"] = ((Util.ToInt(Util.ToString(swkSettingBRec["補助科目コード"])) == -1) ? swkTgtData.Rows[i]["会員名称"] : swkSettingBRec["補助科目名"]);
					dataRow4["ZEI_KUBUN"] = swkTgtData.Rows[i]["税区分"];
					DataRow zeiKbnRow = this.GetZeiKbnRow(Util.ToInt(Util.ToString(swkTgtData.Rows[i]["税区分"])));
					int num9;
					if (zeiKbnRow != null)
					{
						dataRow4["KAZEI_KUBUN"] = zeiKbnRow["KAZEI_KUBUN"];
						num9 = Util.ToInt(Util.ToString(zeiKbnRow["KAZEI_KUBUN"]));
						dataRow4["TORIHIKI_KUBUN"] = zeiKbnRow["TORIHIKI_KUBUN"];
						decimal taxRate = TaxUtil.GetTaxRate(Util.ToDate(condition["SwkDpyDt"]), Util.ToInt(Util.ToString(swkTgtData.Rows[i]["税区分"])), this._dba);
						if (taxRate != Util.ToDecimal(Util.ToString(swkTgtData.Rows[i]["税率"])))
						{
							dataRow4["ZEI_RITSU"] = Util.ToDecimal(Util.ToString(swkTgtData.Rows[i]["税率"]));
						}
						else
						{
							dataRow4["ZEI_RITSU"] = taxRate;
						}
					}
					else
					{
						dataRow4["KAZEI_KUBUN"] = swkSettingBRec["課税区分"];
						num9 = Util.ToInt(Util.ToString(swkSettingBRec["課税区分"]));
						dataRow4["TORIHIKI_KUBUN"] = swkSettingBRec["取引区分"];
						dataRow4["ZEI_RITSU"] = swkSettingBRec["税率"];
					}
					dataRow4["JIGYO_KUBUN"] = ((Util.ToInt(swkTgtData.Rows[i]["事業区分"]) == 0) ? num5 : swkTgtData.Rows[i]["事業区分"]);
					if (num9 == 1)
					{
						flag = true;
						if (Util.ToDecimal(swkTgtData.Rows[i]["外税売上金額"]) != 0m)
						{
							d = Util.ToDecimal(Util.ToString(dataRow4["ZEI_RITSU"]));
							num = Util.ToDecimal(swkTgtData.Rows[i]["外税売上金額"]);
							num2 = num * d / 100m;
							num2 = TaxUtil.CalcFraction(num2, 0, Util.ToInt(Util.ToString(swkTgtData.Rows[i]["消費税端数処理"])));
							dataRow4["ZEIKOMI_KINGAKU"] = num + num2;
							dataRow4["ZEINUKI_KINGAKU"] = num;
							dataRow4["SHOHIZEI_KINGAKU"] = num2;
						}
						else if (Util.ToDecimal(swkTgtData.Rows[i]["内税売上金額"]) != 0m)
						{
							d = Util.ToDecimal(Util.ToString(dataRow4["ZEI_RITSU"]));
							num = Util.ToDecimal(swkTgtData.Rows[i]["内税売上金額"]);
							num2 = num / (100m + d) * d;
							num2 = TaxUtil.CalcFraction(num2, 0, Util.ToInt(Util.ToString(swkTgtData.Rows[i]["消費税端数処理"])));
							dataRow4["ZEIKOMI_KINGAKU"] = num;
							dataRow4["ZEINUKI_KINGAKU"] = num - num2;
							dataRow4["SHOHIZEI_KINGAKU"] = num2;
						}
						else
						{
							dataRow4["ZEIKOMI_KINGAKU"] = Util.ToDecimal(swkTgtData.Rows[i]["売上金額"]) + Util.ToDecimal(swkTgtData.Rows[i]["消費税"]);
							dataRow4["ZEINUKI_KINGAKU"] = Util.ToDecimal(swkTgtData.Rows[i]["売上金額"]);
							dataRow4["SHOHIZEI_KINGAKU"] = Util.ToDecimal(swkTgtData.Rows[i]["消費税"]);
						}
					}
					else if (Util.ToDecimal(swkTgtData.Rows[i]["外税売上金額"]) != 0m)
					{
						dataRow4["ZEIKOMI_KINGAKU"] = Util.ToDecimal(swkTgtData.Rows[i]["外税売上金額"]);
						dataRow4["ZEINUKI_KINGAKU"] = Util.ToDecimal(swkTgtData.Rows[i]["外税売上金額"]);
						dataRow4["SHOHIZEI_KINGAKU"] = 0;
					}
					else if (Util.ToDecimal(swkTgtData.Rows[i]["内税売上金額"]) != 0m)
					{
						dataRow4["ZEIKOMI_KINGAKU"] = Util.ToDecimal(swkTgtData.Rows[i]["内税売上金額"]);
						dataRow4["ZEINUKI_KINGAKU"] = Util.ToDecimal(swkTgtData.Rows[i]["内税売上金額"]);
						dataRow4["SHOHIZEI_KINGAKU"] = 0;
					}
					else
					{
						dataRow4["ZEIKOMI_KINGAKU"] = Util.ToDecimal(swkTgtData.Rows[i]["売上金額"]) + Util.ToDecimal(swkTgtData.Rows[i]["消費税"]);
						dataRow4["ZEINUKI_KINGAKU"] = Util.ToDecimal(swkTgtData.Rows[i]["売上金額"]) + Util.ToDecimal(swkTgtData.Rows[i]["消費税"]);
						dataRow4["SHOHIZEI_KINGAKU"] = 0;
					}
				}
				else
				{
					dataRow4["BUMON_CD"] = DBNull.Value;
					dataRow4["KANJO_KAMOKU_CD"] = DBNull.Value;
					dataRow4["KANJO_KAMOKU_NM"] = DBNull.Value;
					dataRow4["HOJO_KAMOKU_CD"] = DBNull.Value;
					dataRow4["HOJO_KAMOKU_NM"] = DBNull.Value;
					dataRow4["ZEI_KUBUN"] = DBNull.Value;
					dataRow4["KAZEI_KUBUN"] = DBNull.Value;
					dataRow4["TORIHIKI_KUBUN"] = DBNull.Value;
					dataRow4["ZEI_RITSU"] = DBNull.Value;
					dataRow4["JIGYO_KUBUN"] = DBNull.Value;
				}
				dataTable.Rows.Add(dataRow4);
				if (flag)
				{
					DataRow zeiSettingRow = this.GetZeiSettingRow(zeiSetting, 1);
					dataRow4 = dataTable.NewRow();
					dataRow4["GYO_BANGO"] = num3;
					dataRow4["SEIKYUSAKI_CD"] = swkTgtData.Rows[i]["会員番号"];
					dataRow4["TAISHAKU_KUBUN"] = ((Util.ToInt(swkTgtData.Rows[i]["取引区分２"]) == 2) ? 1 : 2);
					dataRow4["MEISAI_KUBUN"] = 1;
					dataRow4["DENPYO_KUBUN"] = 1;
					dataRow4["DENPYO_DATE"] = condition["SwkDpyDt"];
					dataRow4["BUMON_CD"] = 0;
					dataRow4["TEKIYO_CD"] = Util.ToDecimal(condition["TekiyoCd"]);
					dataRow4["TEKIYO"] = condition["Tekiyo"];
					dataRow4["ZEIKOMI_KINGAKU"] = 0;
					if (Util.ToDecimal(swkTgtData.Rows[i]["外税売上金額"]) != 0m)
					{
						num = Util.ToDecimal(swkTgtData.Rows[i]["外税売上金額"]);
						num2 = num * d / 100m;
						num2 = TaxUtil.CalcFraction(num2, 0, Util.ToInt(Util.ToString(swkTgtData.Rows[i]["消費税端数処理"])));
						dataRow4["ZEINUKI_KINGAKU"] = num2;
					}
					else if (Util.ToDecimal(swkTgtData.Rows[i]["内税売上金額"]) != 0m)
					{
						num = Util.ToDecimal(swkTgtData.Rows[i]["内税売上金額"]);
						num2 = num / (100m + d) * d;
						num2 = TaxUtil.CalcFraction(num2, 0, Util.ToInt(Util.ToString(swkTgtData.Rows[i]["消費税端数処理"])));
						dataRow4["ZEINUKI_KINGAKU"] = num2;
					}
					else
					{
						dataRow4["ZEINUKI_KINGAKU"] = Util.ToDecimal(swkTgtData.Rows[i]["消費税"]);
					}
					dataRow4["SHOHIZEI_KINGAKU"] = 0;
					dataRow4["SHOHIZEI_NYURYOKU_HOHO"] = swkTgtData.Rows[i]["消費税入力方法"];
					dataRow4["SHOHIZEI_HENKO"] = 0;
					dataRow4["KESSAN_KUBUN"] = 0;
					dataRow4["SHIWAKE_SAKUSEI_KUBUN"] = 8;
					dataRow4["JIGYO_KUBUN"] = Util.ToDecimal(swkTgtData.Rows[i]["事業区分"]);
					dataRow4["CHK_ZUMI"] = 0;
					dataRow4["DEL_FLG"] = 0;
					if (zeiSettingRow != null)
					{
						dataRow4["KANJO_KAMOKU_CD"] = zeiSettingRow["KANJO_KAMOKU_CD"];
						dataRow4["KANJO_KAMOKU_NM"] = zeiSettingRow["KANJO_KAMOKU_NM"];
						dataRow4["HOJO_KAMOKU_CD"] = 0;
						dataRow4["HOJO_KAMOKU_NM"] = DBNull.Value;
						dataRow4["ZEI_KUBUN"] = zeiSettingRow["KARIKATA_ZEI_KUBUN"];
						dataRow4["KAZEI_KUBUN"] = zeiSettingRow["KARI_KAZEI_KUBUN"];
						dataRow4["TORIHIKI_KUBUN"] = zeiSettingRow["KARI_TORIHIKI_KUBUN"];
						dataRow4["ZEI_RITSU"] = zeiSettingRow["KARI_ZEI_RITSU"];
					}
					else
					{
						dataRow4["KANJO_KAMOKU_CD"] = DBNull.Value;
						dataRow4["KANJO_KAMOKU_NM"] = DBNull.Value;
						dataRow4["HOJO_KAMOKU_CD"] = DBNull.Value;
						dataRow4["HOJO_KAMOKU_NM"] = DBNull.Value;
						dataRow4["ZEI_KUBUN"] = DBNull.Value;
						dataRow4["KAZEI_KUBUN"] = DBNull.Value;
						dataRow4["TORIHIKI_KUBUN"] = DBNull.Value;
						dataRow4["ZEI_RITSU"] = DBNull.Value;
					}
					dataTable.Rows.Add(dataRow4);
				}
				dataRow4 = dataTable.NewRow();
				dataRow4["GYO_BANGO"] = num3;
				dataRow4["SEIKYUSAKI_CD"] = swkTgtData.Rows[i]["会員番号"];
				dataRow4["TAISHAKU_KUBUN"] = ((Util.ToInt(swkTgtData.Rows[i]["取引区分２"]) == 2) ? 2 : 1);
				dataRow4["MEISAI_KUBUN"] = 0;
				dataRow4["DENPYO_KUBUN"] = 1;
				dataRow4["DENPYO_DATE"] = condition["SwkDpyDt"];
				dataRow4["TEKIYO_CD"] = Util.ToDecimal(condition["TekiyoCd"]);
				dataRow4["TEKIYO"] = condition["Tekiyo"];
				if (swkSettingARec != null && Util.ToInt(swkSettingARec["課税区分"]) == 1)
				{
					if (Util.ToDecimal(swkTgtData.Rows[i]["外税売上金額"]) != 0m)
					{
						num = Util.ToDecimal(swkTgtData.Rows[i]["外税売上金額"]);
						num2 = num * d / 100m;
						num2 = TaxUtil.CalcFraction(num2, 0, Util.ToInt(Util.ToString(swkTgtData.Rows[i]["消費税端数処理"])));
						dataRow4["ZEIKOMI_KINGAKU"] = num + num2;
						dataRow4["ZEINUKI_KINGAKU"] = num;
						dataRow4["SHOHIZEI_KINGAKU"] = num2;
					}
					else if (Util.ToDecimal(swkTgtData.Rows[i]["内税売上金額"]) != 0m)
					{
						num = Util.ToDecimal(swkTgtData.Rows[i]["内税売上金額"]);
						num2 = num / (100m + d) * d;
						num2 = TaxUtil.CalcFraction(num2, 0, Util.ToInt(Util.ToString(swkTgtData.Rows[i]["消費税端数処理"])));
						dataRow4["ZEIKOMI_KINGAKU"] = num;
						dataRow4["ZEINUKI_KINGAKU"] = num - num2;
						dataRow4["SHOHIZEI_KINGAKU"] = num2;
					}
					else
					{
						dataRow4["ZEIKOMI_KINGAKU"] = Util.ToDecimal(swkTgtData.Rows[i]["売上金額"]) + Util.ToDecimal(swkTgtData.Rows[i]["消費税"]);
						dataRow4["ZEINUKI_KINGAKU"] = Util.ToDecimal(swkTgtData.Rows[i]["売上金額"]);
						dataRow4["SHOHIZEI_KINGAKU"] = Util.ToDecimal(swkTgtData.Rows[i]["消費税"]);
					}
				}
				else if (Util.ToDecimal(swkTgtData.Rows[i]["外税売上金額"]) != 0m)
				{
					num = Util.ToDecimal(swkTgtData.Rows[i]["外税売上金額"]);
					num2 = num * d / 100m;
					num2 = TaxUtil.CalcFraction(num2, 0, Util.ToInt(Util.ToString(swkTgtData.Rows[i]["消費税端数処理"])));
					dataRow4["ZEIKOMI_KINGAKU"] = num + num2;
					dataRow4["ZEINUKI_KINGAKU"] = num + num2;
					dataRow4["SHOHIZEI_KINGAKU"] = 0;
				}
				else if (Util.ToDecimal(swkTgtData.Rows[i]["内税売上金額"]) != 0m)
				{
					num = Util.ToDecimal(swkTgtData.Rows[i]["内税売上金額"]);
					dataRow4["ZEIKOMI_KINGAKU"] = num;
					dataRow4["ZEINUKI_KINGAKU"] = num;
					dataRow4["SHOHIZEI_KINGAKU"] = 0;
				}
				else
				{
					dataRow4["ZEIKOMI_KINGAKU"] = Util.ToDecimal(swkTgtData.Rows[i]["売上金額"]) + Util.ToDecimal(swkTgtData.Rows[i]["消費税"]);
					dataRow4["ZEINUKI_KINGAKU"] = Util.ToDecimal(swkTgtData.Rows[i]["売上金額"]) + Util.ToDecimal(swkTgtData.Rows[i]["消費税"]);
					dataRow4["SHOHIZEI_KINGAKU"] = 0;
				}
				dataRow4["SHOHIZEI_NYURYOKU_HOHO"] = num6;
				dataRow4["SHOHIZEI_HENKO"] = 0;
				dataRow4["KESSAN_KUBUN"] = 0;
				dataRow4["SHIWAKE_SAKUSEI_KUBUN"] = num7;
				dataRow4["CHK_ZUMI"] = 0;
				dataRow4["DEL_FLG"] = 0;
				if (swkSettingARec != null)
				{
					dataRow4["BUMON_CD"] = ((Util.ToInt(Util.ToString(swkSettingARec["部門コード"])) == -1) ? swkTgtData.Rows[i]["部門コード"] : swkSettingARec["部門コード"]);
					dataRow4["KANJO_KAMOKU_CD"] = swkSettingARec["勘定科目コード"];
					dataRow4["KANJO_KAMOKU_NM"] = swkSettingARec["勘定科目名"];
					dataRow4["HOJO_KAMOKU_CD"] = ((Util.ToInt(Util.ToString(swkSettingARec["補助科目コード"])) == -1) ? swkTgtData.Rows[i]["会員番号"] : swkSettingARec["補助科目コード"]);
					dataRow4["HOJO_KAMOKU_NM"] = ((Util.ToInt(Util.ToString(swkSettingARec["補助科目コード"])) == -1) ? swkTgtData.Rows[i]["会員名称"] : swkSettingARec["補助科目名"]);
					dataRow4["ZEI_KUBUN"] = swkSettingARec["税区分"];
					dataRow4["KAZEI_KUBUN"] = swkSettingARec["課税区分"];
					dataRow4["TORIHIKI_KUBUN"] = swkSettingARec["取引区分"];
					dataRow4["ZEI_RITSU"] = swkSettingARec["税率"];
					dataRow4["JIGYO_KUBUN"] = swkSettingARec["事業区分"];
					if (Util.ToInt(swkSettingARec["課税区分"]) == 1)
					{
						flag2 = true;
					}
					dataTable.Rows.Add(dataRow4);
				}
				else
				{
					dataRow4["BUMON_CD"] = DBNull.Value;
					dataRow4["KANJO_KAMOKU_CD"] = DBNull.Value;
					dataRow4["KANJO_KAMOKU_NM"] = DBNull.Value;
					dataRow4["HOJO_KAMOKU_CD"] = DBNull.Value;
					dataRow4["HOJO_KAMOKU_NM"] = DBNull.Value;
					dataRow4["ZEI_KUBUN"] = DBNull.Value;
					dataRow4["KAZEI_KUBUN"] = DBNull.Value;
					dataRow4["TORIHIKI_KUBUN"] = DBNull.Value;
					dataRow4["ZEI_RITSU"] = DBNull.Value;
					dataRow4["JIGYO_KUBUN"] = DBNull.Value;
				}
				if (flag2)
				{
					DataRow zeiSettingRow = this.GetZeiSettingRow(zeiSetting, 2);
					dataRow4 = dataTable.NewRow();
					dataRow4["GYO_BANGO"] = num3;
					dataRow4["SEIKYUSAKI_CD"] = swkTgtData.Rows[i]["会員番号"];
					dataRow4["TAISHAKU_KUBUN"] = ((Util.ToInt(swkTgtData.Rows[i]["取引区分２"]) == 2) ? 2 : 1);
					dataRow4["MEISAI_KUBUN"] = 1;
					dataRow4["DENPYO_KUBUN"] = 1;
					dataRow4["DENPYO_DATE"] = condition["SwkDpyDt"];
					dataRow4["BUMON_CD"] = 0;
					dataRow4["TEKIYO_CD"] = Util.ToDecimal(condition["TekiyoCd"]);
					dataRow4["TEKIYO"] = condition["Tekiyo"];
					dataRow4["ZEIKOMI_KINGAKU"] = 0;
					if (Util.ToDecimal(swkTgtData.Rows[i]["外税売上金額"]) != 0m)
					{
						num = Util.ToDecimal(swkTgtData.Rows[i]["外税売上金額"]);
						num2 = num * d / 100m;
						num2 = TaxUtil.CalcFraction(num2, 0, Util.ToInt(Util.ToString(swkTgtData.Rows[i]["消費税端数処理"])));
						dataRow4["ZEINUKI_KINGAKU"] = num2;
					}
					else if (Util.ToDecimal(swkTgtData.Rows[i]["内税売上金額"]) != 0m)
					{
						num = Util.ToDecimal(swkTgtData.Rows[i]["内税売上金額"]);
						num2 = num / (100m + d) * d;
						num2 = TaxUtil.CalcFraction(num2, 0, Util.ToInt(Util.ToString(swkTgtData.Rows[i]["消費税端数処理"])));
						dataRow4["ZEINUKI_KINGAKU"] = num2;
					}
					else
					{
						dataRow4["ZEINUKI_KINGAKU"] = Util.ToDecimal(swkTgtData.Rows[i]["消費税"]);
					}
					dataRow4["SHOHIZEI_KINGAKU"] = 0;
					dataRow4["SHOHIZEI_NYURYOKU_HOHO"] = swkTgtData.Rows[i]["消費税入力方法"];
					dataRow4["SHOHIZEI_HENKO"] = 0;
					dataRow4["KESSAN_KUBUN"] = 0;
					dataRow4["SHIWAKE_SAKUSEI_KUBUN"] = 8;
					dataRow4["JIGYO_KUBUN"] = Util.ToDecimal(swkTgtData.Rows[i]["事業区分"]);
					dataRow4["CHK_ZUMI"] = 0;
					dataRow4["DEL_FLG"] = 0;
					if (zeiSettingRow != null)
					{
						dataRow4["KANJO_KAMOKU_CD"] = zeiSettingRow["KANJO_KAMOKU_CD"];
						dataRow4["KANJO_KAMOKU_NM"] = zeiSettingRow["KANJO_KAMOKU_NM"];
						dataRow4["HOJO_KAMOKU_CD"] = 0;
						dataRow4["HOJO_KAMOKU_NM"] = DBNull.Value;
						dataRow4["ZEI_KUBUN"] = zeiSettingRow["KASHIKATA_ZEI_KUBUN"];
						dataRow4["KAZEI_KUBUN"] = zeiSettingRow["KASHI_KAZEI_KUBUN"];
						dataRow4["TORIHIKI_KUBUN"] = zeiSettingRow["KASHI_TORIHIKI_KUBUN"];
						dataRow4["ZEI_RITSU"] = zeiSettingRow["KASHI_ZEI_RITSU"];
					}
					else
					{
						dataRow4["KANJO_KAMOKU_CD"] = DBNull.Value;
						dataRow4["KANJO_KAMOKU_NM"] = DBNull.Value;
						dataRow4["HOJO_KAMOKU_CD"] = DBNull.Value;
						dataRow4["HOJO_KAMOKU_NM"] = DBNull.Value;
						dataRow4["ZEI_KUBUN"] = DBNull.Value;
						dataRow4["KAZEI_KUBUN"] = DBNull.Value;
						dataRow4["TORIHIKI_KUBUN"] = DBNull.Value;
						dataRow4["ZEI_RITSU"] = DBNull.Value;
					}
					dataTable.Rows.Add(dataRow4);
				}
				text = Util.ToString(swkTgtData.Rows[i]["会員番号"]);
			}
			if (num4 == 1)
			{
				DataRow[] array = dataTable.Select(null, "GYO_BANGO, TAISHAKU_KUBUN, MEISAI_KUBUN");
				for (int num10 = 0; num10 < array.Length; num10++)
				{
					if (Util.ToInt(array[num10]["DEL_FLG"]) != 1)
					{
						stringBuilder = new StringBuilder();
						stringBuilder.Append("TAISHAKU_KUBUN = " + Util.ToString(array[num10]["TAISHAKU_KUBUN"]));
						stringBuilder.Append(" AND MEISAI_KUBUN = " + Util.ToString(array[num10]["MEISAI_KUBUN"]));
						stringBuilder.Append(" AND KANJO_KAMOKU_CD = " + Util.ToString(array[num10]["KANJO_KAMOKU_CD"]));
						stringBuilder.Append(" AND HOJO_KAMOKU_CD = " + Util.ToString(array[num10]["HOJO_KAMOKU_CD"]));
						stringBuilder.Append(" AND BUMON_CD = " + Util.ToString(array[num10]["BUMON_CD"]));
						stringBuilder.Append(" AND ZEI_KUBUN = " + Util.ToString(array[num10]["ZEI_KUBUN"]));
						stringBuilder.Append(" AND JIGYO_KUBUN = " + Util.ToString(array[num10]["JIGYO_KUBUN"]));
						stringBuilder.Append(" AND ZEI_RITSU = " + Util.ToString(array[num10]["ZEI_RITSU"]));
						stringBuilder.Append(" AND DEL_FLG = 0 ");
						DataRow[] array2 = dataTable.Select(stringBuilder.ToString(), "GYO_BANGO, TAISHAKU_KUBUN, MEISAI_KUBUN");
						for (int num11 = 0; num11 < array2.Length; num11++)
						{
							if (num11 == 0)
							{
								array2[num11]["SEIKYUSAKI_CD"] = DBNull.Value;
							}
							else if (Util.ToInt(array2[num11]["DEL_FLG"]) != 1)
							{
								array2[0]["ZEIKOMI_KINGAKU"] = Util.ToDecimal(array2[0]["ZEIKOMI_KINGAKU"]) + Util.ToDecimal(array2[num11]["ZEIKOMI_KINGAKU"]);
								array2[0]["ZEINUKI_KINGAKU"] = Util.ToDecimal(array2[0]["ZEINUKI_KINGAKU"]) + Util.ToDecimal(array2[num11]["ZEINUKI_KINGAKU"]);
								array2[0]["SHOHIZEI_KINGAKU"] = Util.ToDecimal(array2[0]["SHOHIZEI_KINGAKU"]) + Util.ToDecimal(array2[num11]["SHOHIZEI_KINGAKU"]);
								array2[num11]["DEL_FLG"] = 1;
							}
						}
					}
				}
			}
			else
			{
				DataRow[] array = dataTable.Select(null, "GYO_BANGO, TAISHAKU_KUBUN, MEISAI_KUBUN");
				for (int num12 = 0; num12 < array.Length; num12++)
				{
					if (Util.ToInt(array[num12]["DEL_FLG"]) != 1)
					{
						stringBuilder = new StringBuilder();
						stringBuilder.Append("SEIKYUSAKI_CD = " + Util.ToString(array[num12]["SEIKYUSAKI_CD"]));
						stringBuilder.Append(" AND TAISHAKU_KUBUN = " + Util.ToString(array[num12]["TAISHAKU_KUBUN"]));
						stringBuilder.Append(" AND MEISAI_KUBUN = " + Util.ToString(array[num12]["MEISAI_KUBUN"]));
						stringBuilder.Append(" AND KANJO_KAMOKU_CD = " + Util.ToString(array[num12]["KANJO_KAMOKU_CD"]));
						stringBuilder.Append(" AND HOJO_KAMOKU_CD = " + Util.ToString(array[num12]["HOJO_KAMOKU_CD"]));
						stringBuilder.Append(" AND BUMON_CD = " + Util.ToString(array[num12]["BUMON_CD"]));
						stringBuilder.Append(" AND ZEI_KUBUN = " + Util.ToString(array[num12]["ZEI_KUBUN"]));
						stringBuilder.Append(" AND JIGYO_KUBUN = " + Util.ToString(array[num12]["JIGYO_KUBUN"]));
						stringBuilder.Append(" AND ZEI_RITSU = " + Util.ToString(array[num12]["ZEI_RITSU"]));
						stringBuilder.Append(" AND DEL_FLG = 0 ");
						DataRow[] array2 = dataTable.Select(stringBuilder.ToString(), "GYO_BANGO, TAISHAKU_KUBUN, MEISAI_KUBUN");
						for (int num13 = 0; num13 < array2.Length; num13++)
						{
							if (num13 != 0 && Util.ToInt(array2[num13]["DEL_FLG"]) != 1)
							{
								array2[0]["ZEIKOMI_KINGAKU"] = Util.ToDecimal(array2[0]["ZEIKOMI_KINGAKU"]) + Util.ToDecimal(array2[num13]["ZEIKOMI_KINGAKU"]);
								array2[0]["ZEINUKI_KINGAKU"] = Util.ToDecimal(array2[0]["ZEINUKI_KINGAKU"]) + Util.ToDecimal(array2[num13]["ZEINUKI_KINGAKU"]);
								array2[0]["SHOHIZEI_KINGAKU"] = Util.ToDecimal(array2[0]["SHOHIZEI_KINGAKU"]) + Util.ToDecimal(array2[num13]["SHOHIZEI_KINGAKU"]);
								array2[num13]["DEL_FLG"] = 1;
							}
						}
					}
				}
			}
			dataTable2 = dataTable.Clone();
			foreach (object obj2 in dataTable.Rows)
			{
				DataRow dataRow5 = (DataRow)obj2;
				if (Util.ToInt(dataRow5["DEL_FLG"]) == 0 && Util.ToInt(dataRow5["MEISAI_KUBUN"]) == 0)
				{
					if (Util.ToDecimal(dataRow5["ZEINUKI_KINGAKU"]) < 0m)
					{
						dataRow5["ZEIKOMI_KINGAKU"] = Math.Abs(Util.ToDecimal(dataRow5["ZEIKOMI_KINGAKU"]));
						dataRow5["ZEINUKI_KINGAKU"] = Math.Abs(Util.ToDecimal(dataRow5["ZEINUKI_KINGAKU"]));
						dataRow5["SHOHIZEI_KINGAKU"] = Math.Abs(Util.ToDecimal(dataRow5["SHOHIZEI_KINGAKU"]));
						dataRow5["TAISHAKU_KUBUN"] = ((Util.ToInt(dataRow5["TAISHAKU_KUBUN"]) == 1) ? 2 : 1);
					}
					dataTable2.ImportRow(dataRow5);
					if (Util.ToInt(dataRow5["KAZEI_KUBUN"]) == 1 && Util.ToDecimal(dataRow5["SHOHIZEI_KINGAKU"]) > 0m)
					{
						int ukeBriKbn2 = 2;
						if (Util.ToInt(dataRow5["TORIHIKI_KUBUN"]) >= 10 && Util.ToInt(dataRow5["TORIHIKI_KUBUN"]) <= 19)
						{
							ukeBriKbn2 = 1;
						}
						DataRow zeiSettingRow = this.GetZeiSettingRow(zeiSetting, ukeBriKbn2);
						DataRow dataRow6 = dataTable2.NewRow();
						dataRow6["GYO_BANGO"] = dataRow5["GYO_BANGO"];
						dataRow6["SEIKYUSAKI_CD"] = dataRow5["SEIKYUSAKI_CD"];
						dataRow6["TAISHAKU_KUBUN"] = dataRow5["TAISHAKU_KUBUN"];
						dataRow6["MEISAI_KUBUN"] = 1;
						dataRow6["DENPYO_KUBUN"] = 1;
						dataRow6["DENPYO_DATE"] = dataRow5["DENPYO_DATE"];
						dataRow6["BUMON_CD"] = ((Util.ToInt(zeiSettingRow["BUMON_UMU"]) == 0) ? 0 : dataRow5["BUMON_CD"]);
						dataRow6["TEKIYO_CD"] = dataRow5["TEKIYO_CD"];
						dataRow6["TEKIYO"] = dataRow5["TEKIYO"];
						dataRow6["ZEIKOMI_KINGAKU"] = 0;
						dataRow6["ZEINUKI_KINGAKU"] = Util.ToDecimal(dataRow5["SHOHIZEI_KINGAKU"]);
						dataRow6["SHOHIZEI_KINGAKU"] = 0;
						dataRow6["SHOHIZEI_NYURYOKU_HOHO"] = num6;
						dataRow6["SHOHIZEI_HENKO"] = dataRow5["SHOHIZEI_HENKO"];
						dataRow6["KESSAN_KUBUN"] = dataRow5["KESSAN_KUBUN"];
						dataRow6["SHIWAKE_SAKUSEI_KUBUN"] = dataRow5["SHIWAKE_SAKUSEI_KUBUN"];
						dataRow6["JIGYO_KUBUN"] = dataRow5["JIGYO_KUBUN"];
						dataRow6["CHK_ZUMI"] = 0;
						dataRow6["DEL_FLG"] = 0;
						if (zeiSettingRow != null)
						{
							dataRow6["KANJO_KAMOKU_CD"] = zeiSettingRow["KANJO_KAMOKU_CD"];
							dataRow6["KANJO_KAMOKU_NM"] = zeiSettingRow["KANJO_KAMOKU_NM"];
							dataRow6["HOJO_KAMOKU_CD"] = ((Util.ToInt(Util.ToString(zeiSettingRow["HOJO_KAMOKU_UMU"])) == 0) ? 0 : dataRow5["HOJO_KAMOKU_CD"]);
							dataRow6["HOJO_KAMOKU_NM"] = ((Util.ToInt(Util.ToString(zeiSettingRow["HOJO_KAMOKU_UMU"])) == 0) ? DBNull.Value : dataRow5["HOJO_KAMOKU_NM"]);
							if (Util.ToInt(Util.ToString(dataRow5["TAISHAKU_KUBUN"])) == 1)
							{
								dataRow6["ZEI_KUBUN"] = zeiSettingRow["KARIKATA_ZEI_KUBUN"];
								dataRow6["KAZEI_KUBUN"] = zeiSettingRow["KARI_KAZEI_KUBUN"];
								dataRow6["TORIHIKI_KUBUN"] = zeiSettingRow["KARI_TORIHIKI_KUBUN"];
								dataRow6["ZEI_RITSU"] = zeiSettingRow["KARI_ZEI_RITSU"];
							}
							else
							{
								dataRow6["ZEI_KUBUN"] = zeiSettingRow["KASHIKATA_ZEI_KUBUN"];
								dataRow6["KAZEI_KUBUN"] = zeiSettingRow["KASHI_KAZEI_KUBUN"];
								dataRow6["TORIHIKI_KUBUN"] = zeiSettingRow["KASHI_TORIHIKI_KUBUN"];
								dataRow6["ZEI_RITSU"] = zeiSettingRow["KASHI_ZEI_RITSU"];
							}
						}
						else
						{
							dataRow6["KANJO_KAMOKU_CD"] = DBNull.Value;
							dataRow6["KANJO_KAMOKU_NM"] = DBNull.Value;
							dataRow6["HOJO_KAMOKU_CD"] = DBNull.Value;
							dataRow6["HOJO_KAMOKU_NM"] = DBNull.Value;
							dataRow6["ZEI_KUBUN"] = DBNull.Value;
							dataRow6["KAZEI_KUBUN"] = DBNull.Value;
							dataRow6["TORIHIKI_KUBUN"] = DBNull.Value;
							dataRow6["ZEI_RITSU"] = DBNull.Value;
						}
						dataTable2.Rows.Add(dataRow6);
					}
				}
			}
			dataTable.Clear();
			dataTable = dataTable2.Copy();
			if (num4 == 1)
			{
				DataRow[] array3 = dataTable.Select("TAISHAKU_KUBUN = 1", "GYO_BANGO, MEISAI_KUBUN");
				int num8 = 0;
				for (int num14 = 0; num14 < array3.Length; num14++)
				{
					if (Util.ToInt(array3[num14]["GYO_BANGO"]) > num8 + 1)
					{
						array3[num14]["GYO_BANGO"] = num8 + 1;
					}
					num8 = Util.ToInt(array3[num14]["GYO_BANGO"]);
				}
				array3 = dataTable.Select("TAISHAKU_KUBUN = 2", "GYO_BANGO, MEISAI_KUBUN");
				num8 = 0;
				for (int num15 = 0; num15 < array3.Length; num15++)
				{
					if (Util.ToInt(array3[num15]["GYO_BANGO"]) > num8 + 1)
					{
						array3[num15]["GYO_BANGO"] = num8 + 1;
					}
					num8 = Util.ToInt(array3[num15]["GYO_BANGO"]);
				}
			}
			else
			{
				DataRow[] array5 = dataTable.Select(null, "GYO_BANGO, MEISAI_KUBUN");
				text = string.Empty;
				for (int num16 = 0; num16 < array5.Length; num16++)
				{
					if (!text.Equals(Util.ToString(array5[num16]["SEIKYUSAKI_CD"])))
					{
						DataRow[] array3 = dataTable.Select("SEIKYUSAKI_CD = " + Util.ToString(array5[num16]["SEIKYUSAKI_CD"]) + " AND TAISHAKU_KUBUN = 1", "GYO_BANGO, MEISAI_KUBUN");
						int num8;
						if (num16 != 0)
						{
							num8 = Util.ToInt(array5[num16 - 1]["GYO_BANGO"]);
						}
						for (int num17 = 0; num17 < array3.Length; num17++)
						{
							num8 = Util.ToInt(array3[num17]["GYO_BANGO"]);
						}
						array3 = dataTable.Select("SEIKYUSAKI_CD = " + Util.ToString(array5[num16]["SEIKYUSAKI_CD"]) + " AND TAISHAKU_KUBUN = 2", "GYO_BANGO, MEISAI_KUBUN");
						if (num16 == 0)
						{
							num8 = 0;
						}
						else
						{
							num8 = Util.ToInt(array5[num16 - 1]["GYO_BANGO"]);
						}
						for (int num18 = 0; num18 < array3.Length; num18++)
						{
							if (Util.ToInt(array3[num18]["GYO_BANGO"]) > num8 + 1)
							{
								array3[num18]["GYO_BANGO"] = num8 + 1;
							}
							num8 = Util.ToInt(array3[num18]["GYO_BANGO"]);
						}
					}
					text = Util.ToString(array5[num16]["SEIKYUSAKI_CD"]);
				}
			}
			array4 = dataTable.Select(null, "GYO_BANGO, TAISHAKU_KUBUN, MEISAI_KUBUN");
			dataTable3 = dataTable.Clone();
			for (int num19 = 0; num19 < array4.Length; num19++)
			{
				dataTable3.ImportRow(array4[num19]);
			}
			dataSet.Tables.Add(dataTable3);
			return dataSet;
		}

		// Token: 0x06000004 RID: 4 RVA: 0x00005F84 File Offset: 0x00004184
		public bool MakeSwkData(int mode, int packDpyNo, Hashtable condition, DataSet swkData)
		{
			bool result;
			try
			{
				int num = 0;
				DataTable chkDpyNo = this.GetChkDpyNo(packDpyNo);
				if (mode == 1)
				{
					packDpyNo = this._dba.GetHNDenpyoNo(this._uInfo, this.ShishoCode, 1, 1);
					if (Util.ToInt(this.GetJidoSwkRirekiExistChk(packDpyNo).Rows[0]["件数"]) > 0)
					{
						return false;
					}
					this._dba.UpdateHNDenpyoNo(this._uInfo, this.ShishoCode, 1, 1, packDpyNo);
				}
				this.DeleteTB_ZM_SHIWAKE_DENPYO(packDpyNo);
				this.DeleteTB_ZM_SHIWAKE_MEISAI(packDpyNo);
				this.DeleteTB_HN_ZIDO_SHIWAKE_RIREKI(packDpyNo);
				this.UpdateCancelTB_HN_TORIHIKI_DENPYO(packDpyNo);
				if (mode != 3)
				{
					for (int i = 0; i < swkData.Tables.Count; i++)
					{
						DataTable dataTable = swkData.Tables[i];
						if (i + 1 > chkDpyNo.Rows.Count)
						{
							num = Util.ToInt(this.GetSaisyuDenpyoNo().Rows[0]["最終伝票番号"]) + 1;
						}
						else
						{
							num = Util.ToInt(chkDpyNo.Rows[i]["SHIWAKE_DENPYO_BANGO"]);
						}
						if (this.GetChkShiwakeDenpyo(num).Rows.Count > 0)
						{
							return false;
						}
						this.InsertTB_ZM_SHIWAKE_DENPYO(num, condition);
						for (int j = 0; j < dataTable.Rows.Count; j++)
						{
							if (this.GetChkShiwakeMeisai(num, dataTable.Rows[j]).Rows.Count > 0)
							{
								return false;
							}
							this.InsertTB_ZM_SHIWAKE_MEISAI(num, condition, dataTable.Rows[j]);
						}
						if (this.GetChkJidoSwkRireki(packDpyNo, num).Rows.Count > 0)
						{
							return false;
						}
						this.InsertTB_HN_ZIDO_SHIWAKE_RIREKI(condition, packDpyNo, num);
					}
					this._dba.UpdateZMDenpyoNo(this._uInfo, this.ShishoCode, 0, num);
					this.UpdateTorihikiDenpyo(packDpyNo, condition);
				}
				result = true;
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return result;
		}

		// Token: 0x06000005 RID: 5 RVA: 0x000061A0 File Offset: 0x000043A0
		private string CreateWhereByToriKbn(Hashtable condition)
		{
			int num = 0;
			int num2 = 0;
			int num3 = 0;
			int num4 = 0;
			StringBuilder stringBuilder = new StringBuilder();
			switch ((KBDB1011.SKbn)condition["SakuseiKbn"])
			{
			case KBDB1011.SKbn.Genkin:
				num3 = Util.ToInt(this._config.LoadPgConfig(Constants.SubSys.Kob, "KBDB1011", "Setting", "GenGenToriDp"));
				num4 = Util.ToInt(this._config.LoadPgConfig(Constants.SubSys.Kob, "KBDB1011", "Setting", "GenGenHnDp"));
				break;
			case KBDB1011.SKbn.GenkinKake:
				num = Util.ToInt(this._config.LoadPgConfig(Constants.SubSys.Kob, "KBDB1011", "Setting", "GenKakeKakeToriDp"));
				num2 = Util.ToInt(this._config.LoadPgConfig(Constants.SubSys.Kob, "KBDB1011", "Setting", "GenKakeKakeHnDp"));
				num3 = Util.ToInt(this._config.LoadPgConfig(Constants.SubSys.Kob, "KBDB1011", "Setting", "GenKakeGenToriDp"));
				num4 = Util.ToInt(this._config.LoadPgConfig(Constants.SubSys.Kob, "KBDB1011", "Setting", "GenKakeGenHnDp"));
				break;
			case KBDB1011.SKbn.Shimebi:
				num = Util.ToInt(this._config.LoadPgConfig(Constants.SubSys.Kob, "KBDB1011", "Setting", "SmbKakeToriDp"));
				num2 = Util.ToInt(this._config.LoadPgConfig(Constants.SubSys.Kob, "KBDB1011", "Setting", "SmbKakeHnDp"));
				num3 = Util.ToInt(this._config.LoadPgConfig(Constants.SubSys.Kob, "KBDB1011", "Setting", "SmbGenToriDp"));
				num4 = Util.ToInt(this._config.LoadPgConfig(Constants.SubSys.Kob, "KBDB1011", "Setting", "SmbGenHnDp"));
				break;
			}
			int num5 = 2;
			try
			{
				num5 = Util.ToInt(this._config.LoadPgConfig(Constants.SubSys.Kob, "KBDB1011", "Setting", "GenkinTorihikiKbn"));
			}
			catch (Exception)
			{
				num5 = 2;
			}
			int num6 = 8 * num + 4 * num2 + 2 * num3 + num4;
			if (num5 == 3)
			{
				switch (num6)
				{
				case 1:
					stringBuilder.Append("AND (B.TORIHIKI_KUBUN1 = 3 ");
					stringBuilder.Append("     AND B.TORIHIKI_KUBUN2 = 2) ");
					break;
				case 2:
					stringBuilder.Append("AND (B.TORIHIKI_KUBUN1 = 3 ");
					stringBuilder.Append("     AND B.TORIHIKI_KUBUN2 IN (1,3,4,5,6,7,8,9)) ");
					break;
				case 3:
					stringBuilder.Append("AND (B.TORIHIKI_KUBUN1 = 3 ");
					stringBuilder.Append("     AND B.TORIHIKI_KUBUN2 BETWEEN 1 AND 9) ");
					break;
				case 4:
					stringBuilder.Append("AND (B.TORIHIKI_KUBUN1 IN (1,2) ");
					stringBuilder.Append("     AND B.TORIHIKI_KUBUN2 = 2) ");
					break;
				case 5:
					stringBuilder.Append("AND (B.TORIHIKI_KUBUN1 BETWEEN 1 AND 3 ");
					stringBuilder.Append("     AND B.TORIHIKI_KUBUN2 = 2) ");
					break;
				case 6:
					stringBuilder.Append("AND ((B.TORIHIKI_KUBUN1 IN (1,2) ");
					stringBuilder.Append("      AND B.TORIHIKI_KUBUN2 = 2) ");
					stringBuilder.Append("     OR ");
					stringBuilder.Append("     (B.TORIHIKI_KUBUN1 = 3 ");
					stringBuilder.Append("      AND B.TORIHIKI_KUBUN2 IN (1,3,4,5,6,7,8,9))) ");
					break;
				case 7:
					stringBuilder.Append("AND ((B.TORIHIKI_KUBUN1 IN (1,2) ");
					stringBuilder.Append("      AND B.TORIHIKI_KUBUN2 = 2) ");
					stringBuilder.Append("     OR ");
					stringBuilder.Append("     (B.TORIHIKI_KUBUN1 = 3 ");
					stringBuilder.Append("      AND B.TORIHIKI_KUBUN2 BETWEEN 1 AND 9)) ");
					break;
				case 8:
					stringBuilder.Append("AND (B.TORIHIKI_KUBUN1 IN (1,2) ");
					stringBuilder.Append("     AND B.TORIHIKI_KUBUN2 IN (1,3,4,5,6,7,8,9)) ");
					break;
				case 9:
					stringBuilder.Append("AND ((B.TORIHIKI_KUBUN1 IN (1,2) ");
					stringBuilder.Append("      AND B.TORIHIKI_KUBUN2 IN (1,3,4,5,6,7,8,9)) ");
					stringBuilder.Append("     OR ");
					stringBuilder.Append("     (B.TORIHIKI_KUBUN1 = 3 ");
					stringBuilder.Append("      AND B.TORIHIKI_KUBUN2 = 2)) ");
					break;
				case 10:
					stringBuilder.Append("AND (B.TORIHIKI_KUBUN1 BETWEEN 1 AND 3 ");
					stringBuilder.Append("     AND B.TORIHIKI_KUBUN2 IN (1,3,4,5,6,7,8,9)) ");
					break;
				case 11:
					stringBuilder.Append("AND ((B.TORIHIKI_KUBUN1 IN (1,2) ");
					stringBuilder.Append("      AND B.TORIHIKI_KUBUN2 IN (1,3,4,5,6,7,8,9)) ");
					stringBuilder.Append("     OR ");
					stringBuilder.Append("     (B.TORIHIKI_KUBUN1 = 3 ");
					stringBuilder.Append("      AND B.TORIHIKI_KUBUN2 BETWEEN 1 AND 9)) ");
					break;
				case 12:
					stringBuilder.Append("AND (B.TORIHIKI_KUBUN1 IN (1,2) ");
					stringBuilder.Append("     AND B.TORIHIKI_KUBUN2 BETWEEN 1 AND 9) ");
					break;
				case 13:
					stringBuilder.Append("AND ((B.TORIHIKI_KUBUN1 IN (1,2) ");
					stringBuilder.Append("      AND B.TORIHIKI_KUBUN2 BETWEEN 1 AND 9) ");
					stringBuilder.Append("     OR ");
					stringBuilder.Append("     (B.TORIHIKI_KUBUN1 = 3 ");
					stringBuilder.Append("      AND B.TORIHIKI_KUBUN2 = 2)) ");
					break;
				case 14:
					stringBuilder.Append("AND ((B.TORIHIKI_KUBUN1 IN (1,2) ");
					stringBuilder.Append("      AND B.TORIHIKI_KUBUN2 BETWEEN 1 AND 9) ");
					stringBuilder.Append("     OR ");
					stringBuilder.Append("     (B.TORIHIKI_KUBUN1 = 3 ");
					stringBuilder.Append("      AND B.TORIHIKI_KUBUN2 IN (1,3,4,5,6,7,8,9))) ");
					break;
				case 15:
					stringBuilder.Append("AND (B.TORIHIKI_KUBUN1 BETWEEN 1 AND 3 ");
					stringBuilder.Append("     AND B.TORIHIKI_KUBUN2 BETWEEN 1 AND 9) ");
					break;
				}
			}
			else
			{
				switch (num6)
				{
				case 1:
					stringBuilder.Append("AND (B.TORIHIKI_KUBUN1 = 2 ");
					stringBuilder.Append("     AND B.TORIHIKI_KUBUN2 = 2) ");
					break;
				case 2:
					stringBuilder.Append("AND (B.TORIHIKI_KUBUN1 = 2 ");
					stringBuilder.Append("     AND B.TORIHIKI_KUBUN2 IN (1,3,4,5,6,7,8,9)) ");
					break;
				case 3:
					stringBuilder.Append("AND (B.TORIHIKI_KUBUN1 = 2 ");
					stringBuilder.Append("     AND B.TORIHIKI_KUBUN2 BETWEEN 1 AND 9) ");
					break;
				case 4:
					stringBuilder.Append("AND (B.TORIHIKI_KUBUN1 IN (1,3,4) ");
					stringBuilder.Append("     AND B.TORIHIKI_KUBUN2 = 2) ");
					break;
				case 5:
					stringBuilder.Append("AND (B.TORIHIKI_KUBUN1 BETWEEN 1 AND 4 ");
					stringBuilder.Append("     AND B.TORIHIKI_KUBUN2 = 2) ");
					break;
				case 6:
					stringBuilder.Append("AND ((B.TORIHIKI_KUBUN1 IN (1,3,4) ");
					stringBuilder.Append("      AND B.TORIHIKI_KUBUN2 = 2) ");
					stringBuilder.Append("     OR ");
					stringBuilder.Append("     (B.TORIHIKI_KUBUN1 = 2 ");
					stringBuilder.Append("      AND B.TORIHIKI_KUBUN2 IN (1,3,4,5,6,7,8,9))) ");
					break;
				case 7:
					stringBuilder.Append("AND ((B.TORIHIKI_KUBUN1 IN (1,3,4) ");
					stringBuilder.Append("      AND B.TORIHIKI_KUBUN2 = 2) ");
					stringBuilder.Append("     OR ");
					stringBuilder.Append("     (B.TORIHIKI_KUBUN1 = 2 ");
					stringBuilder.Append("      AND B.TORIHIKI_KUBUN2 BETWEEN 1 AND 9)) ");
					break;
				case 8:
					stringBuilder.Append("AND (B.TORIHIKI_KUBUN1 IN (1,3,4) ");
					stringBuilder.Append("     AND B.TORIHIKI_KUBUN2 IN (1,3,4,5,6,7,8,9)) ");
					break;
				case 9:
					stringBuilder.Append("AND ((B.TORIHIKI_KUBUN1 IN (1,3,4) ");
					stringBuilder.Append("      AND B.TORIHIKI_KUBUN2 IN (1,3,4,5,6,7,8,9)) ");
					stringBuilder.Append("     OR ");
					stringBuilder.Append("     (B.TORIHIKI_KUBUN1 = 2 ");
					stringBuilder.Append("      AND B.TORIHIKI_KUBUN2 = 2)) ");
					break;
				case 10:
					stringBuilder.Append("AND (B.TORIHIKI_KUBUN1 BETWEEN 1 AND 4 ");
					stringBuilder.Append("     AND B.TORIHIKI_KUBUN2 IN (1,3,4,5,6,7,8,9)) ");
					break;
				case 11:
					stringBuilder.Append("AND ((B.TORIHIKI_KUBUN1 IN (1,3,4) ");
					stringBuilder.Append("      AND B.TORIHIKI_KUBUN2 IN (1,3,4,5,6,7,8,9)) ");
					stringBuilder.Append("     OR ");
					stringBuilder.Append("     (B.TORIHIKI_KUBUN1 = 2 ");
					stringBuilder.Append("      AND B.TORIHIKI_KUBUN2 BETWEEN 1 AND 9)) ");
					break;
				case 12:
					stringBuilder.Append("AND (B.TORIHIKI_KUBUN1 IN (1,3,4) ");
					stringBuilder.Append("     AND B.TORIHIKI_KUBUN2 BETWEEN 1 AND 9) ");
					break;
				case 13:
					stringBuilder.Append("AND ((B.TORIHIKI_KUBUN1 IN (1,3,4) ");
					stringBuilder.Append("      AND B.TORIHIKI_KUBUN2 BETWEEN 1 AND 9) ");
					stringBuilder.Append("     OR ");
					stringBuilder.Append("     (B.TORIHIKI_KUBUN1 = 2 ");
					stringBuilder.Append("      AND B.TORIHIKI_KUBUN2 = 2)) ");
					break;
				case 14:
					stringBuilder.Append("AND ((B.TORIHIKI_KUBUN1 IN (1,3,4) ");
					stringBuilder.Append("      AND B.TORIHIKI_KUBUN2 BETWEEN 1 AND 9) ");
					stringBuilder.Append("     OR ");
					stringBuilder.Append("     (B.TORIHIKI_KUBUN1 = 2 ");
					stringBuilder.Append("      AND B.TORIHIKI_KUBUN2 IN (1,3,4,5,6,7,8,9))) ");
					break;
				case 15:
					stringBuilder.Append("AND (B.TORIHIKI_KUBUN1 BETWEEN 1 AND 4 ");
					stringBuilder.Append("     AND B.TORIHIKI_KUBUN2 BETWEEN 1 AND 9) ");
					break;
				}
			}
			return stringBuilder.ToString();
		}

		// Token: 0x06000006 RID: 6 RVA: 0x0000699C File Offset: 0x00004B9C
		private DataRow GetSwkSettingARec(DataTable dtSwkSettingA, int toriKbn1, int toriKbn2)
		{
			int num = toriKbn1 * 10 + toriKbn2;
			DataRow[] array = dtSwkSettingA.Select("仕訳コード = " + Util.ToString(num));
			if (array.Length != 0)
			{
				return array[0];
			}
			return null;
		}

		// Token: 0x06000007 RID: 7 RVA: 0x000069D8 File Offset: 0x00004BD8
		private DataRow GetSwkSettingBRec(DataTable dtSwkSettingB, int swkCd)
		{
			DataRow[] array = dtSwkSettingB.Select("仕訳コード = " + Util.ToString(swkCd));
			if (array.Length != 0)
			{
				return array[0];
			}
			return null;
		}

		// Token: 0x06000008 RID: 8 RVA: 0x00006A0C File Offset: 0x00004C0C
		private DataRow GetZeiSettingRow(DataTable dtZeiSetting, int ukeBriKbn)
		{
			DataRow[] array = dtZeiSetting.Select("KBN = " + Util.ToString(ukeBriKbn));
			if (array.Length != 0)
			{
				return array[0];
			}
			return null;
		}

		// Token: 0x06000009 RID: 9 RVA: 0x00006A40 File Offset: 0x00004C40
		private DataTable GetChkDpyNo(int packDpyNo)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("SELECT ");
			stringBuilder.Append("  DENPYO_BANGO ");
			stringBuilder.Append(" ,SHIWAKE_DENPYO_BANGO ");
			stringBuilder.Append("FROM ");
			stringBuilder.Append("  TB_HN_ZIDO_SHIWAKE_RIREKI ");
			stringBuilder.Append("WHERE ");
			stringBuilder.Append("    KAISHA_CD    = @KAISHA_CD ");
			stringBuilder.Append("AND SHISHO_CD    = @SHISHO_CD ");
			stringBuilder.Append("AND KAIKEI_NENDO = @KAIKEI_NENDO ");
			stringBuilder.Append("AND DENPYO_KUBUN = 1 ");
			stringBuilder.Append("AND DENPYO_BANGO = @DENPYO_BANGO ");
			stringBuilder.Append("ORDER BY ");
			stringBuilder.Append("  DENPYO_BANGO ");
			stringBuilder.Append(" ,SHIWAKE_DENPYO_BANGO ");
			DbParamCollection dbParamCollection = new DbParamCollection();
			dbParamCollection.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
			dbParamCollection.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
			dbParamCollection.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
			dbParamCollection.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 6, packDpyNo);
			return this._dba.GetDataTableFromSqlWithParams(stringBuilder.ToString(), dbParamCollection);
		}

		// Token: 0x0600000A RID: 10 RVA: 0x00006B74 File Offset: 0x00004D74
		private DataTable GetJidoSwkRirekiExistChk(int packDpyNo)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("SELECT ");
			stringBuilder.Append("  COUNT(*) AS 件数 ");
			stringBuilder.Append("FROM ");
			stringBuilder.Append("  TB_HN_ZIDO_SHIWAKE_RIREKI ");
			stringBuilder.Append("WHERE ");
			stringBuilder.Append("    KAISHA_CD    = @KAISHA_CD ");
			stringBuilder.Append("AND SHISHO_CD    = @SHISHO_CD ");
			stringBuilder.Append("AND KAIKEI_NENDO = @KAIKEI_NENDO ");
			stringBuilder.Append("AND DENPYO_KUBUN = 1 ");
			stringBuilder.Append("AND DENPYO_BANGO = @DENPYO_BANGO ");
			DbParamCollection dbParamCollection = new DbParamCollection();
			dbParamCollection.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
			dbParamCollection.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
			dbParamCollection.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
			dbParamCollection.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 6, packDpyNo);
			return this._dba.GetDataTableFromSqlWithParams(stringBuilder.ToString(), dbParamCollection);
		}

		// Token: 0x0600000B RID: 11 RVA: 0x00006C78 File Offset: 0x00004E78
		private int DeleteTB_ZM_SHIWAKE_DENPYO(int packDpyNo)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("    KAISHA_CD = @KAISHA_CD ");
			stringBuilder.Append("AND SHISHO_CD = @SHISHO_CD ");
			stringBuilder.Append("AND KAIKEI_NENDO = @KAIKEI_NENDO ");
			stringBuilder.Append("AND DENPYO_BANGO IN (SELECT DISTINCT ");
			stringBuilder.Append("                       X.SHIWAKE_DENPYO_BANGO ");
			stringBuilder.Append("                     FROM ");
			stringBuilder.Append("                       TB_HN_ZIDO_SHIWAKE_RIREKI AS X ");
			stringBuilder.Append("                     WHERE ");
			stringBuilder.Append("                         X.KAISHA_CD = @KAISHA_CD ");
			stringBuilder.Append("                     AND X.SHISHO_CD = @SHISHO_CD ");
			stringBuilder.Append("                     AND X.KAIKEI_NENDO = @KAIKEI_NENDO ");
			stringBuilder.Append("                     AND X.DENPYO_KUBUN = 1 ");
			stringBuilder.Append("                     AND X.DENPYO_BANGO = @DENPYO_BANGO ");
			stringBuilder.Append("                    ) ");
			DbParamCollection dbParamCollection = new DbParamCollection();
			dbParamCollection.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
			dbParamCollection.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
			dbParamCollection.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
			dbParamCollection.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 6, packDpyNo);
			return this._dba.Delete("TB_ZM_SHIWAKE_DENPYO", stringBuilder.ToString(), dbParamCollection);
		}

		// Token: 0x0600000C RID: 12 RVA: 0x00006DB0 File Offset: 0x00004FB0
		private int DeleteTB_ZM_SHIWAKE_MEISAI(int packDpyNo)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("    KAISHA_CD = @KAISHA_CD ");
			stringBuilder.Append("AND SHISHO_CD = @SHISHO_CD ");
			stringBuilder.Append("AND KAIKEI_NENDO = @KAIKEI_NENDO ");
			stringBuilder.Append("AND DENPYO_BANGO IN (SELECT DISTINCT ");
			stringBuilder.Append("                       X.SHIWAKE_DENPYO_BANGO ");
			stringBuilder.Append("                     FROM ");
			stringBuilder.Append("                       TB_HN_ZIDO_SHIWAKE_RIREKI AS X ");
			stringBuilder.Append("                     WHERE ");
			stringBuilder.Append("                         X.KAISHA_CD = @KAISHA_CD ");
			stringBuilder.Append("                     AND X.SHISHO_CD = @SHISHO_CD ");
			stringBuilder.Append("                     AND X.KAIKEI_NENDO = @KAIKEI_NENDO ");
			stringBuilder.Append("                     AND X.DENPYO_KUBUN = 1 ");
			stringBuilder.Append("                     AND X.DENPYO_BANGO = @DENPYO_BANGO ");
			stringBuilder.Append("                    ) ");
			DbParamCollection dbParamCollection = new DbParamCollection();
			dbParamCollection.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
			dbParamCollection.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
			dbParamCollection.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
			dbParamCollection.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 6, packDpyNo);
			return this._dba.Delete("TB_ZM_SHIWAKE_MEISAI", stringBuilder.ToString(), dbParamCollection);
		}

		// Token: 0x0600000D RID: 13 RVA: 0x00006EE8 File Offset: 0x000050E8
		private int DeleteTB_HN_ZIDO_SHIWAKE_RIREKI(int packDpyNo)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("    KAISHA_CD = @KAISHA_CD ");
			stringBuilder.Append("AND SHISHO_CD = @SHISHO_CD ");
			stringBuilder.Append("AND KAIKEI_NENDO = @KAIKEI_NENDO ");
			stringBuilder.Append("AND DENPYO_KUBUN = 1 ");
			stringBuilder.Append("AND DENPYO_BANGO = @DENPYO_BANGO ");
			DbParamCollection dbParamCollection = new DbParamCollection();
			dbParamCollection.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
			dbParamCollection.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
			dbParamCollection.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
			dbParamCollection.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 6, packDpyNo);
			return this._dba.Delete("TB_HN_ZIDO_SHIWAKE_RIREKI", stringBuilder.ToString(), dbParamCollection);
		}

		// Token: 0x0600000E RID: 14 RVA: 0x00006FB4 File Offset: 0x000051B4
		private int UpdateCancelTB_HN_TORIHIKI_DENPYO(int packDpyNo)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("    KAISHA_CD = @KAISHA_CD ");
			stringBuilder.Append("AND SHISHO_CD = @SHISHO_CD ");
			stringBuilder.Append("AND KAIKEI_NENDO = @KAIKEI_NENDO ");
			stringBuilder.Append("AND DENPYO_KUBUN = 1 ");
			stringBuilder.Append("AND IKKATSU_DENPYO_BANGO = @CNCL_IKKATSU_DENPYO_BANGO ");
			DbParamCollection dbParamCollection = new DbParamCollection();
			dbParamCollection.SetParam("@IKKATSU_DENPYO_BANGO", SqlDbType.Decimal, 6, 0);
			dbParamCollection.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWTIME");
			DbParamCollection dbParamCollection2 = new DbParamCollection();
			dbParamCollection2.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
			dbParamCollection2.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
			dbParamCollection2.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
			dbParamCollection2.SetParam("@CNCL_IKKATSU_DENPYO_BANGO", SqlDbType.Decimal, 6, packDpyNo);
			return this._dba.Update("TB_HN_TORIHIKI_DENPYO", dbParamCollection, stringBuilder.ToString(), dbParamCollection2);
		}

		// Token: 0x0600000F RID: 15 RVA: 0x000070AC File Offset: 0x000052AC
		private DataTable GetSaisyuDenpyoNo()
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("SELECT ");
			stringBuilder.Append("  MAX(DENPYO_BANGO) AS 最終伝票番号 ");
			stringBuilder.Append("FROM ");
			stringBuilder.Append("  TB_ZM_SHIWAKE_DENPYO ");
			stringBuilder.Append("WHERE ");
			stringBuilder.Append("    KAISHA_CD = @KAISHA_CD ");
			stringBuilder.Append("AND SHISHO_CD = @SHISHO_CD ");
			stringBuilder.Append("AND KAIKEI_NENDO = @KAIKEI_NENDO ");
			DbParamCollection dbParamCollection = new DbParamCollection();
			dbParamCollection.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
			dbParamCollection.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
			dbParamCollection.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
			return this._dba.GetDataTableFromSqlWithParams(stringBuilder.ToString(), dbParamCollection);
		}

		// Token: 0x06000010 RID: 16 RVA: 0x00007184 File Offset: 0x00005384
		private DataTable GetChkShiwakeDenpyo(int swkDpyNo)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("SELECT ");
			stringBuilder.Append("  * ");
			stringBuilder.Append("FROM ");
			stringBuilder.Append("  TB_ZM_SHIWAKE_DENPYO ");
			stringBuilder.Append("WHERE ");
			stringBuilder.Append("    KAISHA_CD = @KAISHA_CD ");
			stringBuilder.Append("AND SHISHO_CD = @SHISHO_CD ");
			stringBuilder.Append("AND KAIKEI_NENDO = @KAIKEI_NENDO ");
			stringBuilder.Append("AND DENPYO_BANGO = @DENPYO_BANGO ");
			DbParamCollection dbParamCollection = new DbParamCollection();
			dbParamCollection.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
			dbParamCollection.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
			dbParamCollection.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
			dbParamCollection.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 6, swkDpyNo);
			return this._dba.GetDataTableFromSqlWithParams(stringBuilder.ToString(), dbParamCollection);
		}

		// Token: 0x06000011 RID: 17 RVA: 0x0000727C File Offset: 0x0000547C
		private int InsertTB_ZM_SHIWAKE_DENPYO(int swkDpyNo, Hashtable condition)
		{
			DbParamCollection dbParamCollection = new DbParamCollection();
			dbParamCollection.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
			dbParamCollection.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
			dbParamCollection.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 6, swkDpyNo);
			dbParamCollection.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
			dbParamCollection.SetParam("@DENPYO_DATE", SqlDbType.DateTime, condition["SwkDpyDt"]);
			dbParamCollection.SetParam("@TANTOSHA_CD", SqlDbType.Decimal, 4, condition["TantoCd"]);
			dbParamCollection.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWTIME");
			return this._dba.Insert("TB_ZM_SHIWAKE_DENPYO", dbParamCollection);
		}

		// Token: 0x06000012 RID: 18 RVA: 0x00007340 File Offset: 0x00005540
		private DataTable GetChkShiwakeMeisai(int swkDpyNo, DataRow meisaiInfo)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("SELECT ");
			stringBuilder.Append("  * ");
			stringBuilder.Append("FROM ");
			stringBuilder.Append("  TB_ZM_SHIWAKE_MEISAI ");
			stringBuilder.Append("WHERE ");
			stringBuilder.Append("    KAISHA_CD = @KAISHA_CD ");
			stringBuilder.Append("AND SHISHO_CD = @SHISHO_CD ");
			stringBuilder.Append("AND KAIKEI_NENDO = @KAIKEI_NENDO ");
			stringBuilder.Append("AND DENPYO_BANGO = @DENPYO_BANGO ");
			stringBuilder.Append("AND GYO_BANGO = @GYO_BANGO ");
			stringBuilder.Append("AND TAISHAKU_KUBUN = @TAISHAKU_KUBUN ");
			stringBuilder.Append("AND MEISAI_KUBUN = @MEISAI_KUBUN ");
			DbParamCollection dbParamCollection = new DbParamCollection();
			dbParamCollection.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
			dbParamCollection.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
			dbParamCollection.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
			dbParamCollection.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 6, swkDpyNo);
			dbParamCollection.SetParam("@GYO_BANGO", SqlDbType.Decimal, 10, meisaiInfo["GYO_BANGO"]);
			dbParamCollection.SetParam("@TAISHAKU_KUBUN", SqlDbType.Decimal, 6, meisaiInfo["TAISHAKU_KUBUN"]);
			dbParamCollection.SetParam("@MEISAI_KUBUN", SqlDbType.Decimal, 6, meisaiInfo["MEISAI_KUBUN"]);
			return this._dba.GetDataTableFromSqlWithParams(stringBuilder.ToString(), dbParamCollection);
		}

		// Token: 0x06000013 RID: 19 RVA: 0x000074A4 File Offset: 0x000056A4
		private int InsertTB_ZM_SHIWAKE_MEISAI(int swkDpyNo, Hashtable condition, DataRow meisaiInfo)
		{
			DbParamCollection dbParamCollection = new DbParamCollection();
			dbParamCollection.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
			dbParamCollection.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
			dbParamCollection.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 6, swkDpyNo);
			dbParamCollection.SetParam("@GYO_BANGO", SqlDbType.Decimal, 10, meisaiInfo["GYO_BANGO"]);
			dbParamCollection.SetParam("@TAISHAKU_KUBUN", SqlDbType.Decimal, 1, meisaiInfo["TAISHAKU_KUBUN"]);
			dbParamCollection.SetParam("@MEISAI_KUBUN", SqlDbType.Decimal, 1, meisaiInfo["MEISAI_KUBUN"]);
			dbParamCollection.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
			dbParamCollection.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 1, meisaiInfo["DENPYO_KUBUN"]);
			dbParamCollection.SetParam("@DENPYO_DATE", SqlDbType.DateTime, condition["SwkDpyDt"]);
			dbParamCollection.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 6, meisaiInfo["KANJO_KAMOKU_CD"]);
			dbParamCollection.SetParam("@HOJO_KAMOKU_CD", SqlDbType.Decimal, 10, meisaiInfo["HOJO_KAMOKU_CD"]);
			dbParamCollection.SetParam("@BUMON_CD", SqlDbType.Decimal, 4, meisaiInfo["BUMON_CD"]);
			dbParamCollection.SetParam("@TEKIYO_CD", SqlDbType.Decimal, 4, meisaiInfo["TEKIYO_CD"]);
			dbParamCollection.SetParam("@TEKIYO", SqlDbType.VarChar, 40, meisaiInfo["TEKIYO"]);
			dbParamCollection.SetParam("@ZEIKOMI_KINGAKU", SqlDbType.Decimal, 15, meisaiInfo["ZEIKOMI_KINGAKU"]);
			dbParamCollection.SetParam("@ZEINUKI_KINGAKU", SqlDbType.Decimal, 15, meisaiInfo["ZEINUKI_KINGAKU"]);
			dbParamCollection.SetParam("@SHOHIZEI_KINGAKU", SqlDbType.Decimal, 15, meisaiInfo["SHOHIZEI_KINGAKU"]);
			dbParamCollection.SetParam("@ZEI_KUBUN", SqlDbType.Decimal, 2, meisaiInfo["ZEI_KUBUN"]);
			dbParamCollection.SetParam("@KAZEI_KUBUN", SqlDbType.Decimal, 1, meisaiInfo["KAZEI_KUBUN"]);
			dbParamCollection.SetParam("@TORIHIKI_KUBUN", SqlDbType.Decimal, 2, meisaiInfo["TORIHIKI_KUBUN"]);
			dbParamCollection.SetParam("@ZEI_RITSU", SqlDbType.Decimal, 4, 2, meisaiInfo["ZEI_RITSU"]);
			dbParamCollection.SetParam("@JIGYO_KUBUN", SqlDbType.Decimal, 1, meisaiInfo["JIGYO_KUBUN"]);
			dbParamCollection.SetParam("@SHOHIZEI_NYURYOKU_HOHO", SqlDbType.Decimal, 1, meisaiInfo["SHOHIZEI_NYURYOKU_HOHO"]);
			dbParamCollection.SetParam("@SHOHIZEI_HENKO", SqlDbType.Decimal, 1, meisaiInfo["SHOHIZEI_HENKO"]);
			dbParamCollection.SetParam("@KESSAN_KUBUN", SqlDbType.Decimal, 1, meisaiInfo["KESSAN_KUBUN"]);
			dbParamCollection.SetParam("@SHIWAKE_SAKUSEI_KUBUN", SqlDbType.Decimal, 1, meisaiInfo["SHIWAKE_SAKUSEI_KUBUN"]);
			dbParamCollection.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWTIME");
			return this._dba.Insert("TB_ZM_SHIWAKE_MEISAI", dbParamCollection);
		}

		// Token: 0x06000014 RID: 20 RVA: 0x00007750 File Offset: 0x00005950
		private DataTable GetChkJidoSwkRireki(int packDpyNo, int swkDpyNo)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("SELECT ");
			stringBuilder.Append("  A.DENPYO_BANGO            AS 伝票番号 ");
			stringBuilder.Append(" ,A.SHIWAKE_DENPYO_BANGO    AS 仕訳伝票番号 ");
			stringBuilder.Append(" ,A.DENPYO_DATE             AS 伝票日付 ");
			stringBuilder.Append(" ,A.SHORI_KUBUN             AS 処理区分 ");
			stringBuilder.Append(" ,CASE WHEN A.SHORI_KUBUN = 1 THEN '現金一括' ");
			stringBuilder.Append("       ELSE CASE WHEN A.SHORI_KUBUN = 2 THEN '月末一括' ");
			stringBuilder.Append("                 ELSE CASE WHEN A.SHORI_KUBUN = 3 THEN '締日一括' ");
			stringBuilder.Append("                           ELSE '' ");
			stringBuilder.Append("                      END ");
			stringBuilder.Append("            END ");
			stringBuilder.Append("  END AS 処理区分名称 ");
			stringBuilder.Append(" ,A.SHIMEBI                 AS 締日 ");
			stringBuilder.Append(" ,A.KAISHI_DENPYO_DATE      AS 開始伝票日付 ");
			stringBuilder.Append(" ,A.SHURYO_DENPYO_DATE      AS 終了伝票日付 ");
			stringBuilder.Append(" ,A.KAISHI_SEIKYUSAKI_CD    AS 開始請求先コード ");
			stringBuilder.Append(" ,CASE WHEN ISNULL(A.KAISHI_SEIKYUSAKI_CD, '') = '' THEN '先\u3000頭' ELSE B.TORIHIKISAKI_NM END AS 開始請求先名 ");
			stringBuilder.Append(" ,A.SHURYO_SEIKYUSAKI_CD  AS 終了請求先コード ");
			stringBuilder.Append(" ,CASE WHEN ISNULL(A.SHURYO_SEIKYUSAKI_CD, '') = '' THEN '最\u3000後' ELSE C.TORIHIKISAKI_NM END AS 終了請求先名 ");
			stringBuilder.Append(" ,A.TEKIYO_CD               AS 摘要コード ");
			stringBuilder.Append(" ,A.TEKIYO                  AS 摘要名 ");
			stringBuilder.Append("FROM ");
			stringBuilder.Append("  TB_HN_ZIDO_SHIWAKE_RIREKI AS A ");
			stringBuilder.Append("LEFT OUTER JOIN VI_HN_TORIHIKISAKI_JOHO AS B ");
			stringBuilder.Append("ON  A.KAISHA_CD            = B.KAISHA_CD ");
			stringBuilder.Append("AND A.KAISHI_SEIKYUSAKI_CD = B.TORIHIKISAKI_CD ");
			stringBuilder.Append("AND B.SHUBETU_KUBUN  = 1 ");
			stringBuilder.Append("LEFT OUTER JOIN VI_HN_TORIHIKISAKI_JOHO AS C ");
			stringBuilder.Append("ON  A.KAISHA_CD            = C.KAISHA_CD ");
			stringBuilder.Append("AND A.SHURYO_SEIKYUSAKI_CD = C.TORIHIKISAKI_CD ");
			stringBuilder.Append("AND C.SHUBETU_KUBUN  = 1 ");
			stringBuilder.Append("WHERE ");
			stringBuilder.Append("    A.KAISHA_CD            = @KAISHA_CD ");
			stringBuilder.Append("AND A.SHISHO_CD            = @SHISHO_CD ");
			stringBuilder.Append("AND A.KAIKEI_NENDO         = @KAIKEI_NENDO ");
			stringBuilder.Append("AND A.DENPYO_KUBUN         = 1 ");
			stringBuilder.Append("AND A.DENPYO_BANGO         = @DENPYO_BANGO ");
			stringBuilder.Append("AND A.SHIWAKE_DENPYO_BANGO = @SHIWAKE_DENPYO_BANGO ");
			stringBuilder.Append("ORDER BY ");
			stringBuilder.Append("  A.DENPYO_BANGO ");
			stringBuilder.Append(" ,A.SHIWAKE_DENPYO_BANGO ");
			DbParamCollection dbParamCollection = new DbParamCollection();
			dbParamCollection.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
			dbParamCollection.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
			dbParamCollection.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
			dbParamCollection.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 6, packDpyNo);
			dbParamCollection.SetParam("@SHIWAKE_DENPYO_BANGO", SqlDbType.Decimal, 6, swkDpyNo);
			return this._dba.GetDataTableFromSqlWithParams(stringBuilder.ToString(), dbParamCollection);
		}

		// Token: 0x06000015 RID: 21 RVA: 0x000079DC File Offset: 0x00005BDC
		private int InsertTB_HN_ZIDO_SHIWAKE_RIREKI(Hashtable condition, int packDpyNo, int swkDpyNo)
		{
			DbParamCollection dbParamCollection = new DbParamCollection();
			dbParamCollection.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
			dbParamCollection.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
			dbParamCollection.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 1, 1);
			dbParamCollection.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 6, packDpyNo);
			dbParamCollection.SetParam("@SHIWAKE_DENPYO_BANGO", SqlDbType.Decimal, 6, swkDpyNo);
			dbParamCollection.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
			dbParamCollection.SetParam("@DENPYO_DATE", SqlDbType.DateTime, condition["SwkDpyDt"]);
			dbParamCollection.SetParam("@TEKIYO_CD", SqlDbType.Decimal, 4, Util.ToInt(condition["TekiyoCd"]));
			dbParamCollection.SetParam("@TEKIYO", SqlDbType.VarChar, 40, ValChk.IsEmpty(condition["Tekiyo"]) ? DBNull.Value : condition["Tekiyo"]);
			dbParamCollection.SetParam("@SHORI_KUBUN", SqlDbType.Decimal, 2, condition["SakuseiKbn"]);
			dbParamCollection.SetParam("@SHIMEBI", SqlDbType.VarChar, 2, condition["Shimebi"]);
			dbParamCollection.SetParam("@KAISHI_DENPYO_DATE", SqlDbType.DateTime, condition["DpyDtFr"]);
			dbParamCollection.SetParam("@SHURYO_DENPYO_DATE", SqlDbType.DateTime, condition["DpyDtTo"]);
			dbParamCollection.SetParam("@KAISHI_SEIKYUSAKI_CD", SqlDbType.VarChar, 4, (Util.ToInt(condition["FunanushiCdFr"]) == 0) ? DBNull.Value : condition["FunanushiCdFr"]);
			dbParamCollection.SetParam("@SHURYO_SEIKYUSAKI_CD", SqlDbType.VarChar, 4, (Util.ToInt(condition["FunanushiCdTo"]) == 9999) ? DBNull.Value : condition["FunanushiCdTo"]);
			dbParamCollection.SetParam("@TANTOSHA_CD", SqlDbType.Decimal, 4, Util.ToInt(condition["TantoCd"]));
			dbParamCollection.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWTIME");
			return this._dba.Insert("TB_HN_ZIDO_SHIWAKE_RIREKI", dbParamCollection);
		}

		// Token: 0x06000016 RID: 22 RVA: 0x00007BF0 File Offset: 0x00005DF0
		private int UpdateTorihikiDenpyo(int packDpyNo, Hashtable condition)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("    KAISHA_CD = @KAISHA_CD ");
			stringBuilder.Append("AND SHISHO_CD = @SHISHO_CD ");
			stringBuilder.Append("AND KAIKEI_NENDO = @KAIKEI_NENDO ");
			stringBuilder.Append("AND DENPYO_KUBUN = 1 ");
			stringBuilder.Append("AND DENPYO_BANGO IN (SELECT DISTINCT ");
			stringBuilder.Append("                       A.DENPYO_BANGO ");
			stringBuilder.Append("                     FROM ");
			stringBuilder.Append("                       TB_HN_TORIHIKI_MEISAI AS A ");
			stringBuilder.Append("                     LEFT OUTER JOIN TB_HN_TORIHIKI_DENPYO AS B ");
			stringBuilder.Append("                     ON  A.KAISHA_CD    = B.KAISHA_CD ");
			stringBuilder.Append("                     AND A.SHISHO_CD    = B.SHISHO_CD ");
			stringBuilder.Append("                     AND A.KAIKEI_NENDO = B.KAIKEI_NENDO ");
			stringBuilder.Append("                     AND A.DENPYO_KUBUN = B.DENPYO_KUBUN ");
			stringBuilder.Append("                     AND A.DENPYO_BANGO = B.DENPYO_BANGO ");
			stringBuilder.Append("                     LEFT OUTER JOIN VI_HN_TORIHIKISAKI_JOHO AS C ");
			stringBuilder.Append("                     ON  B.KAISHA_CD   = C.KAISHA_CD ");
			stringBuilder.Append("                     AND B.TOKUISAKI_CD = C.TORIHIKISAKI_CD ");
			stringBuilder.Append("                     AND C.SHUBETU_KUBUN= 1 ");
			stringBuilder.Append("                     LEFT OUTER JOIN TB_HN_ZIDO_SHIWAKE_SETTEI_B AS D ");
			stringBuilder.Append("                     ON  A.KAISHA_CD    = D.KAISHA_CD ");
			stringBuilder.Append("                     AND A.SHISHO_CD    = D.SHISHO_CD ");
			stringBuilder.Append("                     AND A.DENPYO_KUBUN = D.DENPYO_KUBUN ");
			stringBuilder.Append("                     AND A.SHIWAKE_CD   = D.SHIWAKE_CD ");
			stringBuilder.Append("                     LEFT OUTER JOIN TB_HN_ZIDO_SHIWAKE_SETTEI_A AS H ");
			stringBuilder.Append("                     ON  B.KAISHA_CD    = H.KAISHA_CD ");
			stringBuilder.Append("                     AND B.SHISHO_CD    = H.SHISHO_CD ");
			stringBuilder.Append("                     AND B.DENPYO_KUBUN = H.DENPYO_KUBUN ");
			stringBuilder.Append("                     AND ((B.TORIHIKI_KUBUN1 * 10) + B.TORIHIKI_KUBUN2) = H.SHIWAKE_CD ");
			stringBuilder.Append("                     WHERE ");
			stringBuilder.Append("                         A.KAISHA_CD = @KAISHA_CD ");
			stringBuilder.Append("                     AND A.SHISHO_CD = @SHISHO_CD ");
			stringBuilder.Append("                     AND A.KAIKEI_NENDO = @KAIKEI_NENDO ");
			stringBuilder.Append("                     AND A.DENPYO_KUBUN = 1 ");
			stringBuilder.Append("                     AND B.DENPYO_DATE BETWEEN @DENPYO_DATE_FR AND @DENPYO_DATE_TO ");
			stringBuilder.Append("                     AND B.TOKUISAKI_CD BETWEEN @KAIIN_BANGO_FR AND @KAIIN_BANGO_TO ");
			stringBuilder.Append("                     AND B.SHIWAKE_DENPYO_BANGO = 0 ");
			stringBuilder.Append("                     AND A.BAIKA_KINGAKU <> 0 ");
			stringBuilder.Append("                     AND ISNULL(B.IKKATSU_DENPYO_BANGO, 0) = 0 ");
			if ((KBDB1011.SKbn)condition["SakuseiKbn"] == KBDB1011.SKbn.Shimebi)
			{
				stringBuilder.Append("AND C.SHIMEBI = @SHIMEBI ");
			}
			stringBuilder.Append(this.CreateWhereByToriKbn(condition));
			stringBuilder.Append("                     AND ISNULL(D.KANJO_KAMOKU_CD, 0) <> 0 ");
			stringBuilder.Append("                     AND ISNULL(H.KANJO_KAMOKU_CD, 0) <> 0 ");
			stringBuilder.Append("                    ) ");
			DbParamCollection dbParamCollection = new DbParamCollection();
			dbParamCollection.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
			dbParamCollection.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
			dbParamCollection.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
			dbParamCollection.SetParam("@DENPYO_DATE_FR", SqlDbType.DateTime, Util.ToDate(condition["DpyDtFr"]));
			dbParamCollection.SetParam("@DENPYO_DATE_TO", SqlDbType.DateTime, Util.ToDate(condition["DpyDtTo"]));
			dbParamCollection.SetParam("@KAIIN_BANGO_FR", SqlDbType.VarChar, 4, Util.ToString(condition["FunanushiCdFr"]));
			dbParamCollection.SetParam("@KAIIN_BANGO_TO", SqlDbType.VarChar, 4, Util.ToString(condition["FunanushiCdTo"]));
			if ((KBDB1011.SKbn)condition["SakuseiKbn"] == KBDB1011.SKbn.Shimebi)
			{
				dbParamCollection.SetParam("@SHIMEBI", SqlDbType.Decimal, 2, Util.ToInt(condition["Shimebi"]));
			}
			DbParamCollection dbParamCollection2 = new DbParamCollection();
			dbParamCollection2.SetParam("@IKKATSU_DENPYO_BANGO", SqlDbType.Decimal, 6, packDpyNo);
			dbParamCollection2.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWTIME");
			return this._dba.Update("TB_HN_TORIHIKI_DENPYO", dbParamCollection2, stringBuilder.ToString(), dbParamCollection);
		}

		// Token: 0x06000017 RID: 23 RVA: 0x00007F64 File Offset: 0x00006164
		private DataRow GetZeiKbnRow(int ZeiKbn)
		{
			DbParamCollection dbParamCollection = new DbParamCollection();
			dbParamCollection.SetParam("@ZEI_KUBUN", SqlDbType.Decimal, 4, ZeiKbn);
			DataTable dataTableByConditionWithParams = this._dba.GetDataTableByConditionWithParams("*", "TB_ZM_F_ZEI_KUBUN", "ZEI_KUBUN = @ZEI_KUBUN", dbParamCollection);
			if (dataTableByConditionWithParams.Rows.Count > 0)
			{
				return dataTableByConditionWithParams.Rows[0];
			}
			return null;
		}

		// Token: 0x04000001 RID: 1
		private UserInfo _uInfo;

		// Token: 0x04000002 RID: 2
		private DbAccess _dba;

		// Token: 0x04000003 RID: 3
		private ConfigLoader _config;

		// Token: 0x04000004 RID: 4
		private int ShishoCode;
	}
}
