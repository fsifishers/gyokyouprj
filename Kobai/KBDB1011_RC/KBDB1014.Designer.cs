﻿namespace jp.co.fsi.kb.kbdb1011
{
	// Token: 0x02000009 RID: 9
	public partial class KBDB1014 : global::jp.co.fsi.common.forms.BasePgForm
	{
		// Token: 0x06000092 RID: 146 RVA: 0x00013C83 File Offset: 0x00011E83
		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		// Token: 0x06000093 RID: 147 RVA: 0x00013CA4 File Offset: 0x00011EA4
		private void InitializeComponent()
		{
			this.lblKarikataSum = new global::System.Windows.Forms.Label();
			this.lblKashikataSum = new global::System.Windows.Forms.Label();
			this.lblDpyDtDay = new global::System.Windows.Forms.Label();
			this.txtDpyDtDay = new global::jp.co.fsi.common.controls.FsiTextBox();
			this.lblDpyDtMonth = new global::System.Windows.Forms.Label();
			this.txtDpyDtMonth = new global::jp.co.fsi.common.controls.FsiTextBox();
			this.lblDpyDtJpYear = new global::System.Windows.Forms.Label();
			this.txtDpyDtJpYear = new global::jp.co.fsi.common.controls.FsiTextBox();
			this.txtDpyDtGengo = new global::jp.co.fsi.common.controls.FsiTextBox();
			this.lblDpyDt = new global::System.Windows.Forms.Label();
			this.mtbList = new global::jp.co.fsi.common.controls.SjMultiTable();
			this.lblKariAmount = new global::System.Windows.Forms.Label();
			this.lblKariZei = new global::System.Windows.Forms.Label();
			this.lblKashiZei = new global::System.Windows.Forms.Label();
			this.lblKashiAmount = new global::System.Windows.Forms.Label();
			this.pnlDebug.SuspendLayout();
			base.SuspendLayout();
			this.lblTitle.Size = new global::System.Drawing.Size(673, 23);
			this.lblTitle.Text = "仕訳データ参照";
			this.btnEsc.Location = new global::System.Drawing.Point(3, 49);
			this.btnF1.Visible = false;
			this.btnF2.Visible = false;
			this.btnF3.Visible = false;
			this.btnF4.Visible = false;
			this.btnF5.Visible = false;
			this.btnF7.Location = new global::System.Drawing.Point(67, 49);
			this.btnF6.Visible = false;
			this.btnF8.Location = new global::System.Drawing.Point(131, 49);
			this.btnF9.Visible = false;
			this.btnF12.Visible = false;
			this.btnF11.Visible = false;
			this.btnF10.Visible = false;
			this.pnlDebug.Location = new global::System.Drawing.Point(5, 315);
			this.pnlDebug.Size = new global::System.Drawing.Size(690, 100);
			this.lblKarikataSum.BackColor = global::System.Drawing.SystemColors.AppWorkspace;
			this.lblKarikataSum.BorderStyle = global::System.Windows.Forms.BorderStyle.FixedSingle;
			this.lblKarikataSum.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9f, global::System.Drawing.FontStyle.Bold, global::System.Drawing.GraphicsUnit.Point, 128);
			this.lblKarikataSum.ForeColor = global::System.Drawing.Color.White;
			this.lblKarikataSum.Location = new global::System.Drawing.Point(13, 317);
			this.lblKarikataSum.Name = "lblKarikataSum";
			this.lblKarikataSum.Size = new global::System.Drawing.Size(149, 38);
			this.lblKarikataSum.TabIndex = 10;
			this.lblKarikataSum.Text = "[借方合計] ";
			this.lblKarikataSum.TextAlign = global::System.Drawing.ContentAlignment.MiddleRight;
			this.lblKashikataSum.BackColor = global::System.Drawing.SystemColors.AppWorkspace;
			this.lblKashikataSum.BorderStyle = global::System.Windows.Forms.BorderStyle.FixedSingle;
			this.lblKashikataSum.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9f, global::System.Drawing.FontStyle.Bold, global::System.Drawing.GraphicsUnit.Point, 128);
			this.lblKashikataSum.ForeColor = global::System.Drawing.Color.White;
			this.lblKashikataSum.Location = new global::System.Drawing.Point(261, 317);
			this.lblKashikataSum.Name = "lblKashikataSum";
			this.lblKashikataSum.Size = new global::System.Drawing.Size(309, 38);
			this.lblKashikataSum.TabIndex = 13;
			this.lblKashikataSum.Text = "[貸方合計] ";
			this.lblKashikataSum.TextAlign = global::System.Drawing.ContentAlignment.MiddleRight;
			this.lblDpyDtDay.BackColor = global::System.Drawing.Color.Silver;
			this.lblDpyDtDay.BorderStyle = global::System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblDpyDtDay.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.lblDpyDtDay.Location = new global::System.Drawing.Point(229, 13);
			this.lblDpyDtDay.Name = "lblDpyDtDay";
			this.lblDpyDtDay.Size = new global::System.Drawing.Size(18, 20);
			this.lblDpyDtDay.TabIndex = 8;
			this.lblDpyDtDay.Text = "日";
			this.lblDpyDtDay.TextAlign = global::System.Drawing.ContentAlignment.MiddleLeft;
			this.txtDpyDtDay.AutoSizeFromLength = true;
			this.txtDpyDtDay.DisplayLength = null;
			this.txtDpyDtDay.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.txtDpyDtDay.ImeMode = global::System.Windows.Forms.ImeMode.Disable;
			this.txtDpyDtDay.Location = new global::System.Drawing.Point(207, 13);
			this.txtDpyDtDay.MaxLength = 2;
			this.txtDpyDtDay.Name = "txtDpyDtDay";
			this.txtDpyDtDay.ReadOnly = true;
			this.txtDpyDtDay.Size = new global::System.Drawing.Size(20, 20);
			this.txtDpyDtDay.TabIndex = 7;
			this.txtDpyDtDay.TabStop = false;
			this.txtDpyDtDay.TextAlign = global::System.Windows.Forms.HorizontalAlignment.Right;
			this.lblDpyDtMonth.BackColor = global::System.Drawing.Color.Silver;
			this.lblDpyDtMonth.BorderStyle = global::System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblDpyDtMonth.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.lblDpyDtMonth.Location = new global::System.Drawing.Point(188, 13);
			this.lblDpyDtMonth.Name = "lblDpyDtMonth";
			this.lblDpyDtMonth.Size = new global::System.Drawing.Size(18, 20);
			this.lblDpyDtMonth.TabIndex = 6;
			this.lblDpyDtMonth.Text = "月";
			this.lblDpyDtMonth.TextAlign = global::System.Drawing.ContentAlignment.MiddleLeft;
			this.txtDpyDtMonth.AutoSizeFromLength = true;
			this.txtDpyDtMonth.DisplayLength = null;
			this.txtDpyDtMonth.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.txtDpyDtMonth.ImeMode = global::System.Windows.Forms.ImeMode.Disable;
			this.txtDpyDtMonth.Location = new global::System.Drawing.Point(166, 13);
			this.txtDpyDtMonth.MaxLength = 2;
			this.txtDpyDtMonth.Name = "txtDpyDtMonth";
			this.txtDpyDtMonth.ReadOnly = true;
			this.txtDpyDtMonth.Size = new global::System.Drawing.Size(20, 20);
			this.txtDpyDtMonth.TabIndex = 5;
			this.txtDpyDtMonth.TabStop = false;
			this.txtDpyDtMonth.TextAlign = global::System.Windows.Forms.HorizontalAlignment.Right;
			this.lblDpyDtJpYear.BackColor = global::System.Drawing.Color.Silver;
			this.lblDpyDtJpYear.BorderStyle = global::System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblDpyDtJpYear.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.lblDpyDtJpYear.Location = new global::System.Drawing.Point(147, 13);
			this.lblDpyDtJpYear.Name = "lblDpyDtJpYear";
			this.lblDpyDtJpYear.Size = new global::System.Drawing.Size(18, 20);
			this.lblDpyDtJpYear.TabIndex = 4;
			this.lblDpyDtJpYear.Text = "年";
			this.lblDpyDtJpYear.TextAlign = global::System.Drawing.ContentAlignment.MiddleLeft;
			this.txtDpyDtJpYear.AutoSizeFromLength = true;
			this.txtDpyDtJpYear.DisplayLength = null;
			this.txtDpyDtJpYear.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.txtDpyDtJpYear.ImeMode = global::System.Windows.Forms.ImeMode.Disable;
			this.txtDpyDtJpYear.Location = new global::System.Drawing.Point(125, 13);
			this.txtDpyDtJpYear.MaxLength = 2;
			this.txtDpyDtJpYear.Name = "txtDpyDtJpYear";
			this.txtDpyDtJpYear.ReadOnly = true;
			this.txtDpyDtJpYear.Size = new global::System.Drawing.Size(20, 20);
			this.txtDpyDtJpYear.TabIndex = 3;
			this.txtDpyDtJpYear.TabStop = false;
			this.txtDpyDtJpYear.TextAlign = global::System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDpyDtGengo.AutoSizeFromLength = true;
			this.txtDpyDtGengo.BackColor = global::System.Drawing.Color.Silver;
			this.txtDpyDtGengo.DisplayLength = null;
			this.txtDpyDtGengo.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.txtDpyDtGengo.Location = new global::System.Drawing.Point(89, 13);
			this.txtDpyDtGengo.MaxLength = 4;
			this.txtDpyDtGengo.Name = "txtDpyDtGengo";
			this.txtDpyDtGengo.ReadOnly = true;
			this.txtDpyDtGengo.Size = new global::System.Drawing.Size(34, 20);
			this.txtDpyDtGengo.TabIndex = 2;
			this.txtDpyDtGengo.TabStop = false;
			this.txtDpyDtGengo.Text = "平成";
			this.lblDpyDt.BackColor = global::System.Drawing.Color.Silver;
			this.lblDpyDt.BorderStyle = global::System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblDpyDt.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.lblDpyDt.Location = new global::System.Drawing.Point(13, 13);
			this.lblDpyDt.Name = "lblDpyDt";
			this.lblDpyDt.Size = new global::System.Drawing.Size(74, 20);
			this.lblDpyDt.TabIndex = 1;
			this.lblDpyDt.Text = "伝 票 日 付";
			this.lblDpyDt.TextAlign = global::System.Drawing.ContentAlignment.MiddleLeft;
			this.mtbList.BackColor = global::System.Drawing.SystemColors.AppWorkspace;
			this.mtbList.FixedCols = 0;
			this.mtbList.FocusField = null;
			this.mtbList.Location = new global::System.Drawing.Point(13, 36);
			this.mtbList.Name = "mtbList";
			this.mtbList.NotSelectableCols = 0;
			this.mtbList.SelectRange = null;
			this.mtbList.Size = new global::System.Drawing.Size(673, 281);
			this.mtbList.TabIndex = 9;
			this.mtbList.Text = "sosMultiTable1";
			this.mtbList.UndoBufferEnabled = false;
			this.lblKariAmount.BackColor = global::System.Drawing.Color.White;
			this.lblKariAmount.BorderStyle = global::System.Windows.Forms.BorderStyle.FixedSingle;
			this.lblKariAmount.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.lblKariAmount.ForeColor = global::System.Drawing.Color.Black;
			this.lblKariAmount.Location = new global::System.Drawing.Point(161, 317);
			this.lblKariAmount.Name = "lblKariAmount";
			this.lblKariAmount.Size = new global::System.Drawing.Size(101, 20);
			this.lblKariAmount.TabIndex = 11;
			this.lblKariAmount.Text = "-99,999,999,999";
			this.lblKariAmount.TextAlign = global::System.Drawing.ContentAlignment.MiddleRight;
			this.lblKariZei.BackColor = global::System.Drawing.Color.White;
			this.lblKariZei.BorderStyle = global::System.Windows.Forms.BorderStyle.FixedSingle;
			this.lblKariZei.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.lblKariZei.ForeColor = global::System.Drawing.Color.Black;
			this.lblKariZei.Location = new global::System.Drawing.Point(161, 335);
			this.lblKariZei.Name = "lblKariZei";
			this.lblKariZei.Size = new global::System.Drawing.Size(101, 20);
			this.lblKariZei.TabIndex = 12;
			this.lblKariZei.Text = "-99,999,999,999";
			this.lblKariZei.TextAlign = global::System.Drawing.ContentAlignment.MiddleRight;
			this.lblKashiZei.BackColor = global::System.Drawing.Color.White;
			this.lblKashiZei.BorderStyle = global::System.Windows.Forms.BorderStyle.FixedSingle;
			this.lblKashiZei.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.lblKashiZei.ForeColor = global::System.Drawing.Color.Black;
			this.lblKashiZei.Location = new global::System.Drawing.Point(569, 335);
			this.lblKashiZei.Name = "lblKashiZei";
			this.lblKashiZei.Size = new global::System.Drawing.Size(101, 20);
			this.lblKashiZei.TabIndex = 15;
			this.lblKashiZei.Text = "-99,999,999,999";
			this.lblKashiZei.TextAlign = global::System.Drawing.ContentAlignment.MiddleRight;
			this.lblKashiAmount.BackColor = global::System.Drawing.Color.White;
			this.lblKashiAmount.BorderStyle = global::System.Windows.Forms.BorderStyle.FixedSingle;
			this.lblKashiAmount.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.lblKashiAmount.ForeColor = global::System.Drawing.Color.Black;
			this.lblKashiAmount.Location = new global::System.Drawing.Point(569, 317);
			this.lblKashiAmount.Name = "lblKashiAmount";
			this.lblKashiAmount.Size = new global::System.Drawing.Size(101, 20);
			this.lblKashiAmount.TabIndex = 14;
			this.lblKashiAmount.Text = "-99,999,999,999";
			this.lblKashiAmount.TextAlign = global::System.Drawing.ContentAlignment.MiddleRight;
			base.AutoScaleDimensions = new global::System.Drawing.SizeF(6f, 12f);
			base.AutoScaleMode = global::System.Windows.Forms.AutoScaleMode.Font;
			base.ClientSize = new global::System.Drawing.Size(698, 418);
			base.Controls.Add(this.lblKashiZei);
			base.Controls.Add(this.lblKashiAmount);
			base.Controls.Add(this.lblKariZei);
			base.Controls.Add(this.lblKariAmount);
			base.Controls.Add(this.mtbList);
			base.Controls.Add(this.lblDpyDt);
			base.Controls.Add(this.lblDpyDtDay);
			base.Controls.Add(this.txtDpyDtDay);
			base.Controls.Add(this.lblDpyDtMonth);
			base.Controls.Add(this.txtDpyDtMonth);
			base.Controls.Add(this.lblDpyDtJpYear);
			base.Controls.Add(this.txtDpyDtJpYear);
			base.Controls.Add(this.txtDpyDtGengo);
			base.Controls.Add(this.lblKashikataSum);
			base.Controls.Add(this.lblKarikataSum);
			base.ImeMode = global::System.Windows.Forms.ImeMode.KatakanaHalf;
			base.Name = "KBDB1014";
			base.ShowFButton = true;
			base.ShowTitle = false;
			this.Text = "仕訳データ参照";
			base.Controls.SetChildIndex(this.pnlDebug, 0);
			base.Controls.SetChildIndex(this.lblTitle, 0);
			base.Controls.SetChildIndex(this.lblKarikataSum, 0);
			base.Controls.SetChildIndex(this.lblKashikataSum, 0);
			base.Controls.SetChildIndex(this.txtDpyDtGengo, 0);
			base.Controls.SetChildIndex(this.txtDpyDtJpYear, 0);
			base.Controls.SetChildIndex(this.lblDpyDtJpYear, 0);
			base.Controls.SetChildIndex(this.txtDpyDtMonth, 0);
			base.Controls.SetChildIndex(this.lblDpyDtMonth, 0);
			base.Controls.SetChildIndex(this.txtDpyDtDay, 0);
			base.Controls.SetChildIndex(this.lblDpyDtDay, 0);
			base.Controls.SetChildIndex(this.lblDpyDt, 0);
			base.Controls.SetChildIndex(this.mtbList, 0);
			base.Controls.SetChildIndex(this.lblKariAmount, 0);
			base.Controls.SetChildIndex(this.lblKariZei, 0);
			base.Controls.SetChildIndex(this.lblKashiAmount, 0);
			base.Controls.SetChildIndex(this.lblKashiZei, 0);
			this.pnlDebug.ResumeLayout(false);
			base.ResumeLayout(false);
			base.PerformLayout();
		}

		// Token: 0x04000074 RID: 116
		private global::System.ComponentModel.IContainer components;

		// Token: 0x04000075 RID: 117
		private global::System.Windows.Forms.Label lblKarikataSum;

		// Token: 0x04000076 RID: 118
		private global::System.Windows.Forms.Label lblKashikataSum;

		// Token: 0x04000077 RID: 119
		private global::System.Windows.Forms.Label lblDpyDtDay;

		// Token: 0x04000078 RID: 120
		private global::jp.co.fsi.common.controls.FsiTextBox txtDpyDtDay;

		// Token: 0x04000079 RID: 121
		private global::System.Windows.Forms.Label lblDpyDtMonth;

		// Token: 0x0400007A RID: 122
		private global::jp.co.fsi.common.controls.FsiTextBox txtDpyDtMonth;

		// Token: 0x0400007B RID: 123
		private global::System.Windows.Forms.Label lblDpyDtJpYear;

		// Token: 0x0400007C RID: 124
		private global::jp.co.fsi.common.controls.FsiTextBox txtDpyDtJpYear;

		// Token: 0x0400007D RID: 125
		private global::jp.co.fsi.common.controls.FsiTextBox txtDpyDtGengo;

		// Token: 0x0400007E RID: 126
		private global::System.Windows.Forms.Label lblDpyDt;

		// Token: 0x0400007F RID: 127
		private global::jp.co.fsi.common.controls.SjMultiTable mtbList;

		// Token: 0x04000080 RID: 128
		private global::System.Windows.Forms.Label lblKariAmount;

		// Token: 0x04000081 RID: 129
		private global::System.Windows.Forms.Label lblKariZei;

		// Token: 0x04000082 RID: 130
		private global::System.Windows.Forms.Label lblKashiZei;

		// Token: 0x04000083 RID: 131
		private global::System.Windows.Forms.Label lblKashiAmount;
	}
}
