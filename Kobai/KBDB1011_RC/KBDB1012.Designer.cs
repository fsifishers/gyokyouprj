﻿namespace jp.co.fsi.kb.kbdb1011
{
	// Token: 0x02000006 RID: 6
	public partial class KBDB1012 : global::jp.co.fsi.common.forms.BasePgForm
	{
		// Token: 0x0600006D RID: 109 RVA: 0x0000F266 File Offset: 0x0000D466
		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		// Token: 0x0600006E RID: 110 RVA: 0x0000F288 File Offset: 0x0000D488
		private void InitializeComponent()
		{
			this.dgvList = new global::System.Windows.Forms.DataGridView();
			this.btnEnter = new global::System.Windows.Forms.Button();
			this.pnlDebug.SuspendLayout();
			((global::System.ComponentModel.ISupportInitialize)this.dgvList).BeginInit();
			base.SuspendLayout();
			this.lblTitle.Size = new global::System.Drawing.Size(719, 23);
			this.lblTitle.Text = "作成済 仕訳データ検索";
			this.btnEsc.Location = new global::System.Drawing.Point(3, 49);
			this.btnF1.Visible = false;
			this.btnF2.Visible = false;
			this.btnF3.Visible = false;
			this.btnF4.Visible = false;
			this.btnF5.Visible = false;
			this.btnF7.Visible = false;
			this.btnF6.Visible = false;
			this.btnF8.Visible = false;
			this.btnF9.Visible = false;
			this.btnF12.Visible = false;
			this.btnF11.Visible = false;
			this.btnF10.Visible = false;
			this.pnlDebug.Controls.Add(this.btnEnter);
			this.pnlDebug.Location = new global::System.Drawing.Point(5, 310);
			this.pnlDebug.Size = new global::System.Drawing.Size(736, 100);
			this.pnlDebug.Controls.SetChildIndex(this.btnF6, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF7, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF5, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF8, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF4, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF9, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF3, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF10, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF2, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF11, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF1, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF12, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnEsc, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnEnter, 0);
			this.dgvList.AllowUserToAddRows = false;
			this.dgvList.AllowUserToDeleteRows = false;
			this.dgvList.ColumnHeadersHeightSizeMode = global::System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvList.ImeMode = global::System.Windows.Forms.ImeMode.Disable;
			this.dgvList.Location = new global::System.Drawing.Point(12, 13);
			this.dgvList.MultiSelect = false;
			this.dgvList.Name = "dgvList";
			this.dgvList.ReadOnly = true;
			this.dgvList.RowHeadersVisible = false;
			this.dgvList.RowTemplate.Height = 21;
			this.dgvList.ScrollBars = global::System.Windows.Forms.ScrollBars.Vertical;
			this.dgvList.SelectionMode = global::System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dgvList.Size = new global::System.Drawing.Size(720, 337);
			this.dgvList.TabIndex = 1;
			this.dgvList.CellDoubleClick += new global::System.Windows.Forms.DataGridViewCellEventHandler(this.dgvList_CellDoubleClick);
			this.dgvList.KeyDown += new global::System.Windows.Forms.KeyEventHandler(this.dgvList_KeyDown);
			this.btnEnter.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.btnEnter.Location = new global::System.Drawing.Point(67, 49);
			this.btnEnter.Name = "btnEnter";
			this.btnEnter.Size = new global::System.Drawing.Size(65, 45);
			this.btnEnter.TabIndex = 904;
			this.btnEnter.TabStop = false;
			this.btnEnter.Text = "Enter\r\n\r\n決定";
			this.btnEnter.TextAlign = global::System.Drawing.ContentAlignment.TopLeft;
			this.btnEnter.UseVisualStyleBackColor = true;
			this.btnEnter.Click += new global::System.EventHandler(this.btnEnter_Click);
			base.AutoScaleDimensions = new global::System.Drawing.SizeF(6f, 12f);
			base.AutoScaleMode = global::System.Windows.Forms.AutoScaleMode.Font;
			base.ClientSize = new global::System.Drawing.Size(744, 413);
			base.Controls.Add(this.dgvList);
			base.ImeMode = global::System.Windows.Forms.ImeMode.KatakanaHalf;
			base.Name = "KBDB1012";
			base.ShowFButton = true;
			base.ShowTitle = false;
			this.Text = "作成済 仕訳データ検索";
			base.Shown += new global::System.EventHandler(this.KBDB1012_Shown);
			base.Controls.SetChildIndex(this.pnlDebug, 0);
			base.Controls.SetChildIndex(this.lblTitle, 0);
			base.Controls.SetChildIndex(this.dgvList, 0);
			this.pnlDebug.ResumeLayout(false);
			((global::System.ComponentModel.ISupportInitialize)this.dgvList).EndInit();
			base.ResumeLayout(false);
		}

		// Token: 0x04000045 RID: 69
		private global::System.ComponentModel.IContainer components;

		// Token: 0x04000046 RID: 70
		private global::System.Windows.Forms.DataGridView dgvList;

		// Token: 0x04000047 RID: 71
		protected global::System.Windows.Forms.Button btnEnter;
	}
}
