﻿namespace jp.co.fsi.kb.kbdb1011
{
	// Token: 0x02000005 RID: 5
	public partial class KBDB1011 : global::jp.co.fsi.common.forms.BasePgForm
	{
		// Token: 0x06000061 RID: 97 RVA: 0x0000B628 File Offset: 0x00009828
		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		// Token: 0x06000062 RID: 98 RVA: 0x0000B648 File Offset: 0x00009848
		private void InitializeComponent()
		{
			this.lblMessage = new global::System.Windows.Forms.Label();
			this.gpbSakuseiKbn = new global::System.Windows.Forms.GroupBox();
			this.rdbShimebi = new global::System.Windows.Forms.RadioButton();
			this.rdbGenkinKake = new global::System.Windows.Forms.RadioButton();
			this.rdbGenkin = new global::System.Windows.Forms.RadioButton();
			this.gpbShimebi = new global::System.Windows.Forms.GroupBox();
			this.lblShimebiMemo = new global::System.Windows.Forms.Label();
			this.txtShimebi = new global::jp.co.fsi.common.controls.FsiTextBox();
			this.gpbDenpyoHizuke = new global::System.Windows.Forms.GroupBox();
			this.lblDpyDtToGengo = new global::System.Windows.Forms.Label();
			this.lblDpyDtFrGengo = new global::System.Windows.Forms.Label();
			this.lblDpyDtToDay = new global::System.Windows.Forms.Label();
			this.txtDpyDtToDay = new global::jp.co.fsi.common.controls.FsiTextBox();
			this.lblDpyDtToMonth = new global::System.Windows.Forms.Label();
			this.txtDpyDtToMonth = new global::jp.co.fsi.common.controls.FsiTextBox();
			this.lblDpyDtToJpYear = new global::System.Windows.Forms.Label();
			this.txtDpyDtToJpYear = new global::jp.co.fsi.common.controls.FsiTextBox();
			this.lblDpyDtFrDay = new global::System.Windows.Forms.Label();
			this.txtDpyDtFrDay = new global::jp.co.fsi.common.controls.FsiTextBox();
			this.lblDpyDtFrMonth = new global::System.Windows.Forms.Label();
			this.txtDpyDtFrMonth = new global::jp.co.fsi.common.controls.FsiTextBox();
			this.lblDpyDtFrJpYear = new global::System.Windows.Forms.Label();
			this.txtDpyDtFrJpYear = new global::jp.co.fsi.common.controls.FsiTextBox();
			this.lblEraBackFr = new global::System.Windows.Forms.Label();
			this.lblEraBackTo = new global::System.Windows.Forms.Label();
			this.lblDpyDtBetween = new global::System.Windows.Forms.Label();
			this.gpbFunanushiCd = new global::System.Windows.Forms.GroupBox();
			this.lblFunanushiCdBetween = new global::System.Windows.Forms.Label();
			this.lblFunanushiNmTo = new global::System.Windows.Forms.Label();
			this.txtFunanushiCdTo = new global::jp.co.fsi.common.controls.FsiTextBox();
			this.lblFunanushiNmFr = new global::System.Windows.Forms.Label();
			this.txtFunanushiCdFr = new global::jp.co.fsi.common.controls.FsiTextBox();
			this.gpbShiwakeDenpyo = new global::System.Windows.Forms.GroupBox();
			this.gpbTekiyo = new global::System.Windows.Forms.GroupBox();
			this.txtTekiyo = new global::jp.co.fsi.common.controls.FsiTextBox();
			this.txtTekiyoCd = new global::jp.co.fsi.common.controls.FsiTextBox();
			this.gpbSwkDpyHzk = new global::System.Windows.Forms.GroupBox();
			this.lblSwkDpyDtGengo = new global::System.Windows.Forms.Label();
			this.lblSwkDpyDtDay = new global::System.Windows.Forms.Label();
			this.txtSwkDpyDtDay = new global::jp.co.fsi.common.controls.FsiTextBox();
			this.lblSwkDpyDtMonth = new global::System.Windows.Forms.Label();
			this.txtSwkDpyDtMonth = new global::jp.co.fsi.common.controls.FsiTextBox();
			this.lblSwkDpyDtJpYear = new global::System.Windows.Forms.Label();
			this.txtSwkDpyDtJpYear = new global::jp.co.fsi.common.controls.FsiTextBox();
			this.label1 = new global::System.Windows.Forms.Label();
			this.gpbTantosha = new global::System.Windows.Forms.GroupBox();
			this.lblTantoNm = new global::System.Windows.Forms.Label();
			this.txtTantoCd = new global::jp.co.fsi.common.controls.FsiTextBox();
			this.lblUpdMode = new global::System.Windows.Forms.Label();
			this.gbxMizuageShisho = new global::System.Windows.Forms.GroupBox();
			this.txtMizuageShishoCd = new global::jp.co.fsi.common.controls.FsiTextBox();
			this.lblMizuageShishoNm = new global::System.Windows.Forms.Label();
			this.lblMizuageShisho = new global::System.Windows.Forms.Label();
			this.pnlDebug.SuspendLayout();
			this.gpbSakuseiKbn.SuspendLayout();
			this.gpbShimebi.SuspendLayout();
			this.gpbDenpyoHizuke.SuspendLayout();
			this.gpbFunanushiCd.SuspendLayout();
			this.gpbShiwakeDenpyo.SuspendLayout();
			this.gpbTekiyo.SuspendLayout();
			this.gpbSwkDpyHzk.SuspendLayout();
			this.gpbTantosha.SuspendLayout();
			this.gbxMizuageShisho.SuspendLayout();
			base.SuspendLayout();
			this.lblTitle.Text = "売上仕訳データ作成";
			this.pnlDebug.Size = new global::System.Drawing.Size(847, 100);
			this.lblMessage.BorderStyle = global::System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblMessage.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.lblMessage.ForeColor = global::System.Drawing.Color.Blue;
			this.lblMessage.Location = new global::System.Drawing.Point(130, 49);
			this.lblMessage.Name = "lblMessage";
			this.lblMessage.Size = new global::System.Drawing.Size(289, 20);
			this.lblMessage.TabIndex = 2;
			this.lblMessage.Text = "『会計期間第25期が選択されています』";
			this.lblMessage.TextAlign = global::System.Drawing.ContentAlignment.MiddleCenter;
			this.gpbSakuseiKbn.Controls.Add(this.rdbShimebi);
			this.gpbSakuseiKbn.Controls.Add(this.rdbGenkinKake);
			this.gpbSakuseiKbn.Controls.Add(this.rdbGenkin);
			this.gpbSakuseiKbn.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.gpbSakuseiKbn.Location = new global::System.Drawing.Point(12, 164);
			this.gpbSakuseiKbn.Name = "gpbSakuseiKbn";
			this.gpbSakuseiKbn.Size = new global::System.Drawing.Size(340, 60);
			this.gpbSakuseiKbn.TabIndex = 3;
			this.gpbSakuseiKbn.TabStop = false;
			this.gpbSakuseiKbn.Text = "作成区分";
			this.rdbShimebi.AutoSize = true;
			this.rdbShimebi.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.rdbShimebi.Location = new global::System.Drawing.Point(248, 23);
			this.rdbShimebi.Name = "rdbShimebi";
			this.rdbShimebi.Size = new global::System.Drawing.Size(81, 17);
			this.rdbShimebi.TabIndex = 2;
			this.rdbShimebi.TabStop = true;
			this.rdbShimebi.Text = "締日基準";
			this.rdbShimebi.UseVisualStyleBackColor = true;
			this.rdbShimebi.CheckedChanged += new global::System.EventHandler(this.rdbShimebi_CheckedChanged);
			this.rdbGenkinKake.AutoSize = true;
			this.rdbGenkinKake.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.rdbGenkinKake.Location = new global::System.Drawing.Point(123, 23);
			this.rdbGenkinKake.Name = "rdbGenkinKake";
			this.rdbGenkinKake.Size = new global::System.Drawing.Size(109, 17);
			this.rdbGenkinKake.TabIndex = 1;
			this.rdbGenkinKake.TabStop = true;
			this.rdbGenkinKake.Text = "現金、掛取引";
			this.rdbGenkinKake.UseVisualStyleBackColor = true;
			this.rdbGenkin.AutoSize = true;
			this.rdbGenkin.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.rdbGenkin.Location = new global::System.Drawing.Point(19, 23);
			this.rdbGenkin.Name = "rdbGenkin";
			this.rdbGenkin.Size = new global::System.Drawing.Size(81, 17);
			this.rdbGenkin.TabIndex = 0;
			this.rdbGenkin.TabStop = true;
			this.rdbGenkin.Text = "現金取引";
			this.rdbGenkin.UseVisualStyleBackColor = true;
			this.gpbShimebi.Controls.Add(this.lblShimebiMemo);
			this.gpbShimebi.Controls.Add(this.txtShimebi);
			this.gpbShimebi.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.gpbShimebi.Location = new global::System.Drawing.Point(365, 164);
			this.gpbShimebi.Name = "gpbShimebi";
			this.gpbShimebi.Size = new global::System.Drawing.Size(142, 60);
			this.gpbShimebi.TabIndex = 4;
			this.gpbShimebi.TabStop = false;
			this.gpbShimebi.Text = "締日";
			this.lblShimebiMemo.BackColor = global::System.Drawing.Color.Silver;
			this.lblShimebiMemo.BorderStyle = global::System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblShimebiMemo.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.lblShimebiMemo.Location = new global::System.Drawing.Point(42, 21);
			this.lblShimebiMemo.Name = "lblShimebiMemo";
			this.lblShimebiMemo.Size = new global::System.Drawing.Size(80, 20);
			this.lblShimebiMemo.TabIndex = 1;
			this.lblShimebiMemo.Text = "99：末締め";
			this.lblShimebiMemo.TextAlign = global::System.Drawing.ContentAlignment.MiddleLeft;
			this.txtShimebi.AutoSizeFromLength = true;
			this.txtShimebi.DisplayLength = null;
			this.txtShimebi.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.txtShimebi.ImeMode = global::System.Windows.Forms.ImeMode.Disable;
			this.txtShimebi.Location = new global::System.Drawing.Point(20, 21);
			this.txtShimebi.MaxLength = 2;
			this.txtShimebi.Name = "txtShimebi";
			this.txtShimebi.Size = new global::System.Drawing.Size(20, 20);
			this.txtShimebi.TabIndex = 0;
			this.txtShimebi.TextAlign = global::System.Windows.Forms.HorizontalAlignment.Right;
			this.txtShimebi.Validating += new global::System.ComponentModel.CancelEventHandler(this.txtShimebi_Validating);
			this.gpbDenpyoHizuke.Controls.Add(this.lblDpyDtToGengo);
			this.gpbDenpyoHizuke.Controls.Add(this.lblDpyDtFrGengo);
			this.gpbDenpyoHizuke.Controls.Add(this.lblDpyDtToDay);
			this.gpbDenpyoHizuke.Controls.Add(this.txtDpyDtToDay);
			this.gpbDenpyoHizuke.Controls.Add(this.lblDpyDtToMonth);
			this.gpbDenpyoHizuke.Controls.Add(this.txtDpyDtToMonth);
			this.gpbDenpyoHizuke.Controls.Add(this.lblDpyDtToJpYear);
			this.gpbDenpyoHizuke.Controls.Add(this.txtDpyDtToJpYear);
			this.gpbDenpyoHizuke.Controls.Add(this.lblDpyDtFrDay);
			this.gpbDenpyoHizuke.Controls.Add(this.txtDpyDtFrDay);
			this.gpbDenpyoHizuke.Controls.Add(this.lblDpyDtFrMonth);
			this.gpbDenpyoHizuke.Controls.Add(this.txtDpyDtFrMonth);
			this.gpbDenpyoHizuke.Controls.Add(this.lblDpyDtFrJpYear);
			this.gpbDenpyoHizuke.Controls.Add(this.txtDpyDtFrJpYear);
			this.gpbDenpyoHizuke.Controls.Add(this.lblEraBackFr);
			this.gpbDenpyoHizuke.Controls.Add(this.lblEraBackTo);
			this.gpbDenpyoHizuke.Controls.Add(this.lblDpyDtBetween);
			this.gpbDenpyoHizuke.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.gpbDenpyoHizuke.Location = new global::System.Drawing.Point(12, 236);
			this.gpbDenpyoHizuke.Name = "gpbDenpyoHizuke";
			this.gpbDenpyoHizuke.Size = new global::System.Drawing.Size(495, 60);
			this.gpbDenpyoHizuke.TabIndex = 5;
			this.gpbDenpyoHizuke.TabStop = false;
			this.gpbDenpyoHizuke.Text = "伝票日付範囲";
			this.lblDpyDtToGengo.BackColor = global::System.Drawing.Color.Silver;
			this.lblDpyDtToGengo.BorderStyle = global::System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblDpyDtToGengo.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.lblDpyDtToGengo.Location = new global::System.Drawing.Point(242, 24);
			this.lblDpyDtToGengo.Name = "lblDpyDtToGengo";
			this.lblDpyDtToGengo.Size = new global::System.Drawing.Size(39, 20);
			this.lblDpyDtToGengo.TabIndex = 903;
			this.lblDpyDtToGengo.TextAlign = global::System.Drawing.ContentAlignment.MiddleCenter;
			this.lblDpyDtFrGengo.BackColor = global::System.Drawing.Color.Silver;
			this.lblDpyDtFrGengo.BorderStyle = global::System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblDpyDtFrGengo.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.lblDpyDtFrGengo.Location = new global::System.Drawing.Point(12, 24);
			this.lblDpyDtFrGengo.Name = "lblDpyDtFrGengo";
			this.lblDpyDtFrGengo.Size = new global::System.Drawing.Size(39, 20);
			this.lblDpyDtFrGengo.TabIndex = 902;
			this.lblDpyDtFrGengo.TextAlign = global::System.Drawing.ContentAlignment.MiddleCenter;
			this.lblDpyDtToDay.BackColor = global::System.Drawing.Color.Silver;
			this.lblDpyDtToDay.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.lblDpyDtToDay.Location = new global::System.Drawing.Point(386, 24);
			this.lblDpyDtToDay.Name = "lblDpyDtToDay";
			this.lblDpyDtToDay.Size = new global::System.Drawing.Size(18, 20);
			this.lblDpyDtToDay.TabIndex = 14;
			this.lblDpyDtToDay.Text = "日";
			this.lblDpyDtToDay.TextAlign = global::System.Drawing.ContentAlignment.MiddleLeft;
			this.txtDpyDtToDay.AutoSizeFromLength = true;
			this.txtDpyDtToDay.DisplayLength = null;
			this.txtDpyDtToDay.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.txtDpyDtToDay.ImeMode = global::System.Windows.Forms.ImeMode.Disable;
			this.txtDpyDtToDay.Location = new global::System.Drawing.Point(364, 24);
			this.txtDpyDtToDay.MaxLength = 2;
			this.txtDpyDtToDay.Name = "txtDpyDtToDay";
			this.txtDpyDtToDay.Size = new global::System.Drawing.Size(20, 20);
			this.txtDpyDtToDay.TabIndex = 13;
			this.txtDpyDtToDay.TextAlign = global::System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDpyDtToDay.Validating += new global::System.ComponentModel.CancelEventHandler(this.txtDpyDtToDay_Validating);
			this.lblDpyDtToMonth.BackColor = global::System.Drawing.Color.Silver;
			this.lblDpyDtToMonth.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.lblDpyDtToMonth.Location = new global::System.Drawing.Point(345, 24);
			this.lblDpyDtToMonth.Name = "lblDpyDtToMonth";
			this.lblDpyDtToMonth.Size = new global::System.Drawing.Size(18, 20);
			this.lblDpyDtToMonth.TabIndex = 12;
			this.lblDpyDtToMonth.Text = "月";
			this.lblDpyDtToMonth.TextAlign = global::System.Drawing.ContentAlignment.MiddleLeft;
			this.txtDpyDtToMonth.AutoSizeFromLength = true;
			this.txtDpyDtToMonth.DisplayLength = null;
			this.txtDpyDtToMonth.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.txtDpyDtToMonth.ImeMode = global::System.Windows.Forms.ImeMode.Disable;
			this.txtDpyDtToMonth.Location = new global::System.Drawing.Point(323, 24);
			this.txtDpyDtToMonth.MaxLength = 2;
			this.txtDpyDtToMonth.Name = "txtDpyDtToMonth";
			this.txtDpyDtToMonth.Size = new global::System.Drawing.Size(20, 20);
			this.txtDpyDtToMonth.TabIndex = 11;
			this.txtDpyDtToMonth.TextAlign = global::System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDpyDtToMonth.Validating += new global::System.ComponentModel.CancelEventHandler(this.txtDpyDtToMonth_Validating);
			this.lblDpyDtToJpYear.BackColor = global::System.Drawing.Color.Silver;
			this.lblDpyDtToJpYear.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.lblDpyDtToJpYear.Location = new global::System.Drawing.Point(304, 24);
			this.lblDpyDtToJpYear.Name = "lblDpyDtToJpYear";
			this.lblDpyDtToJpYear.Size = new global::System.Drawing.Size(18, 20);
			this.lblDpyDtToJpYear.TabIndex = 10;
			this.lblDpyDtToJpYear.Text = "年";
			this.lblDpyDtToJpYear.TextAlign = global::System.Drawing.ContentAlignment.MiddleLeft;
			this.txtDpyDtToJpYear.AutoSizeFromLength = true;
			this.txtDpyDtToJpYear.DisplayLength = null;
			this.txtDpyDtToJpYear.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.txtDpyDtToJpYear.ImeMode = global::System.Windows.Forms.ImeMode.Disable;
			this.txtDpyDtToJpYear.Location = new global::System.Drawing.Point(282, 24);
			this.txtDpyDtToJpYear.MaxLength = 2;
			this.txtDpyDtToJpYear.Name = "txtDpyDtToJpYear";
			this.txtDpyDtToJpYear.Size = new global::System.Drawing.Size(20, 20);
			this.txtDpyDtToJpYear.TabIndex = 9;
			this.txtDpyDtToJpYear.TextAlign = global::System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDpyDtToJpYear.Validating += new global::System.ComponentModel.CancelEventHandler(this.txtDpyDtToJpYear_Validating);
			this.lblDpyDtFrDay.BackColor = global::System.Drawing.Color.Silver;
			this.lblDpyDtFrDay.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.lblDpyDtFrDay.Location = new global::System.Drawing.Point(157, 24);
			this.lblDpyDtFrDay.Name = "lblDpyDtFrDay";
			this.lblDpyDtFrDay.Size = new global::System.Drawing.Size(18, 20);
			this.lblDpyDtFrDay.TabIndex = 6;
			this.lblDpyDtFrDay.Text = "日";
			this.lblDpyDtFrDay.TextAlign = global::System.Drawing.ContentAlignment.MiddleLeft;
			this.txtDpyDtFrDay.AutoSizeFromLength = true;
			this.txtDpyDtFrDay.DisplayLength = null;
			this.txtDpyDtFrDay.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.txtDpyDtFrDay.ImeMode = global::System.Windows.Forms.ImeMode.Disable;
			this.txtDpyDtFrDay.Location = new global::System.Drawing.Point(135, 24);
			this.txtDpyDtFrDay.MaxLength = 2;
			this.txtDpyDtFrDay.Name = "txtDpyDtFrDay";
			this.txtDpyDtFrDay.Size = new global::System.Drawing.Size(20, 20);
			this.txtDpyDtFrDay.TabIndex = 5;
			this.txtDpyDtFrDay.TextAlign = global::System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDpyDtFrDay.Validating += new global::System.ComponentModel.CancelEventHandler(this.txtDpyDtFrDay_Validating);
			this.lblDpyDtFrMonth.BackColor = global::System.Drawing.Color.Silver;
			this.lblDpyDtFrMonth.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.lblDpyDtFrMonth.Location = new global::System.Drawing.Point(116, 24);
			this.lblDpyDtFrMonth.Name = "lblDpyDtFrMonth";
			this.lblDpyDtFrMonth.Size = new global::System.Drawing.Size(18, 20);
			this.lblDpyDtFrMonth.TabIndex = 4;
			this.lblDpyDtFrMonth.Text = "月";
			this.lblDpyDtFrMonth.TextAlign = global::System.Drawing.ContentAlignment.MiddleLeft;
			this.txtDpyDtFrMonth.AutoSizeFromLength = true;
			this.txtDpyDtFrMonth.DisplayLength = null;
			this.txtDpyDtFrMonth.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.txtDpyDtFrMonth.ImeMode = global::System.Windows.Forms.ImeMode.Disable;
			this.txtDpyDtFrMonth.Location = new global::System.Drawing.Point(94, 24);
			this.txtDpyDtFrMonth.MaxLength = 2;
			this.txtDpyDtFrMonth.Name = "txtDpyDtFrMonth";
			this.txtDpyDtFrMonth.Size = new global::System.Drawing.Size(20, 20);
			this.txtDpyDtFrMonth.TabIndex = 3;
			this.txtDpyDtFrMonth.TextAlign = global::System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDpyDtFrMonth.Validating += new global::System.ComponentModel.CancelEventHandler(this.txtDpyDtFrMonth_Validating);
			this.lblDpyDtFrJpYear.BackColor = global::System.Drawing.Color.Silver;
			this.lblDpyDtFrJpYear.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.lblDpyDtFrJpYear.Location = new global::System.Drawing.Point(75, 24);
			this.lblDpyDtFrJpYear.Name = "lblDpyDtFrJpYear";
			this.lblDpyDtFrJpYear.Size = new global::System.Drawing.Size(18, 20);
			this.lblDpyDtFrJpYear.TabIndex = 2;
			this.lblDpyDtFrJpYear.Text = "年";
			this.lblDpyDtFrJpYear.TextAlign = global::System.Drawing.ContentAlignment.MiddleLeft;
			this.txtDpyDtFrJpYear.AutoSizeFromLength = true;
			this.txtDpyDtFrJpYear.DisplayLength = null;
			this.txtDpyDtFrJpYear.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.txtDpyDtFrJpYear.ImeMode = global::System.Windows.Forms.ImeMode.Disable;
			this.txtDpyDtFrJpYear.Location = new global::System.Drawing.Point(53, 24);
			this.txtDpyDtFrJpYear.MaxLength = 2;
			this.txtDpyDtFrJpYear.Name = "txtDpyDtFrJpYear";
			this.txtDpyDtFrJpYear.Size = new global::System.Drawing.Size(20, 20);
			this.txtDpyDtFrJpYear.TabIndex = 1;
			this.txtDpyDtFrJpYear.TextAlign = global::System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDpyDtFrJpYear.Validating += new global::System.ComponentModel.CancelEventHandler(this.txtDpyDtFrJpYear_Validating);
			this.lblEraBackFr.BackColor = global::System.Drawing.Color.Silver;
			this.lblEraBackFr.BorderStyle = global::System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblEraBackFr.Location = new global::System.Drawing.Point(10, 22);
			this.lblEraBackFr.Name = "lblEraBackFr";
			this.lblEraBackFr.Size = new global::System.Drawing.Size(167, 25);
			this.lblEraBackFr.TabIndex = 902;
			this.lblEraBackFr.Text = " ";
			this.lblEraBackTo.BackColor = global::System.Drawing.Color.Silver;
			this.lblEraBackTo.BorderStyle = global::System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblEraBackTo.Location = new global::System.Drawing.Point(240, 22);
			this.lblEraBackTo.Name = "lblEraBackTo";
			this.lblEraBackTo.Size = new global::System.Drawing.Size(167, 25);
			this.lblEraBackTo.TabIndex = 903;
			this.lblEraBackTo.Text = " ";
			this.lblDpyDtBetween.BackColor = global::System.Drawing.Color.Transparent;
			this.lblDpyDtBetween.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.lblDpyDtBetween.Location = new global::System.Drawing.Point(200, 24);
			this.lblDpyDtBetween.Name = "lblDpyDtBetween";
			this.lblDpyDtBetween.Size = new global::System.Drawing.Size(20, 20);
			this.lblDpyDtBetween.TabIndex = 7;
			this.lblDpyDtBetween.Text = "～";
			this.lblDpyDtBetween.TextAlign = global::System.Drawing.ContentAlignment.MiddleCenter;
			this.gpbFunanushiCd.Controls.Add(this.lblFunanushiCdBetween);
			this.gpbFunanushiCd.Controls.Add(this.lblFunanushiNmTo);
			this.gpbFunanushiCd.Controls.Add(this.txtFunanushiCdTo);
			this.gpbFunanushiCd.Controls.Add(this.lblFunanushiNmFr);
			this.gpbFunanushiCd.Controls.Add(this.txtFunanushiCdFr);
			this.gpbFunanushiCd.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.gpbFunanushiCd.Location = new global::System.Drawing.Point(12, 308);
			this.gpbFunanushiCd.Name = "gpbFunanushiCd";
			this.gpbFunanushiCd.Size = new global::System.Drawing.Size(495, 60);
			this.gpbFunanushiCd.TabIndex = 7;
			this.gpbFunanushiCd.TabStop = false;
			this.gpbFunanushiCd.Text = "船主CD範囲";
			this.lblFunanushiCdBetween.BackColor = global::System.Drawing.Color.Transparent;
			this.lblFunanushiCdBetween.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.lblFunanushiCdBetween.Location = new global::System.Drawing.Point(242, 25);
			this.lblFunanushiCdBetween.Name = "lblFunanushiCdBetween";
			this.lblFunanushiCdBetween.Size = new global::System.Drawing.Size(20, 20);
			this.lblFunanushiCdBetween.TabIndex = 2;
			this.lblFunanushiCdBetween.Text = "～";
			this.lblFunanushiCdBetween.TextAlign = global::System.Drawing.ContentAlignment.MiddleCenter;
			this.lblFunanushiNmTo.BackColor = global::System.Drawing.Color.Silver;
			this.lblFunanushiNmTo.BorderStyle = global::System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblFunanushiNmTo.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.lblFunanushiNmTo.Location = new global::System.Drawing.Point(299, 25);
			this.lblFunanushiNmTo.Name = "lblFunanushiNmTo";
			this.lblFunanushiNmTo.Size = new global::System.Drawing.Size(179, 20);
			this.lblFunanushiNmTo.TabIndex = 4;
			this.lblFunanushiNmTo.Text = "最\u3000後";
			this.lblFunanushiNmTo.TextAlign = global::System.Drawing.ContentAlignment.MiddleLeft;
			this.txtFunanushiCdTo.AutoSizeFromLength = true;
			this.txtFunanushiCdTo.DisplayLength = null;
			this.txtFunanushiCdTo.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.txtFunanushiCdTo.ImeMode = global::System.Windows.Forms.ImeMode.Disable;
			this.txtFunanushiCdTo.Location = new global::System.Drawing.Point(263, 25);
			this.txtFunanushiCdTo.MaxLength = 4;
			this.txtFunanushiCdTo.Name = "txtFunanushiCdTo";
			this.txtFunanushiCdTo.Size = new global::System.Drawing.Size(34, 20);
			this.txtFunanushiCdTo.TabIndex = 3;
			this.txtFunanushiCdTo.TextAlign = global::System.Windows.Forms.HorizontalAlignment.Right;
			this.txtFunanushiCdTo.Validating += new global::System.ComponentModel.CancelEventHandler(this.txtFunanushiCdTo_Validating);
			this.lblFunanushiNmFr.BackColor = global::System.Drawing.Color.Silver;
			this.lblFunanushiNmFr.BorderStyle = global::System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblFunanushiNmFr.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.lblFunanushiNmFr.Location = new global::System.Drawing.Point(59, 25);
			this.lblFunanushiNmFr.Name = "lblFunanushiNmFr";
			this.lblFunanushiNmFr.Size = new global::System.Drawing.Size(179, 20);
			this.lblFunanushiNmFr.TabIndex = 1;
			this.lblFunanushiNmFr.Text = "先\u3000頭";
			this.lblFunanushiNmFr.TextAlign = global::System.Drawing.ContentAlignment.MiddleLeft;
			this.txtFunanushiCdFr.AutoSizeFromLength = true;
			this.txtFunanushiCdFr.DisplayLength = null;
			this.txtFunanushiCdFr.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.txtFunanushiCdFr.ImeMode = global::System.Windows.Forms.ImeMode.Disable;
			this.txtFunanushiCdFr.Location = new global::System.Drawing.Point(23, 25);
			this.txtFunanushiCdFr.MaxLength = 4;
			this.txtFunanushiCdFr.Name = "txtFunanushiCdFr";
			this.txtFunanushiCdFr.Size = new global::System.Drawing.Size(34, 20);
			this.txtFunanushiCdFr.TabIndex = 0;
			this.txtFunanushiCdFr.TextAlign = global::System.Windows.Forms.HorizontalAlignment.Right;
			this.txtFunanushiCdFr.Validating += new global::System.ComponentModel.CancelEventHandler(this.txtFunanushiCdFr_Validating);
			this.gpbShiwakeDenpyo.Controls.Add(this.gpbTekiyo);
			this.gpbShiwakeDenpyo.Controls.Add(this.gpbSwkDpyHzk);
			this.gpbShiwakeDenpyo.Controls.Add(this.gpbTantosha);
			this.gpbShiwakeDenpyo.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.gpbShiwakeDenpyo.Location = new global::System.Drawing.Point(12, 380);
			this.gpbShiwakeDenpyo.Name = "gpbShiwakeDenpyo";
			this.gpbShiwakeDenpyo.Size = new global::System.Drawing.Size(495, 146);
			this.gpbShiwakeDenpyo.TabIndex = 8;
			this.gpbShiwakeDenpyo.TabStop = false;
			this.gpbShiwakeDenpyo.Text = "仕訳伝票";
			this.gpbTekiyo.Controls.Add(this.txtTekiyo);
			this.gpbTekiyo.Controls.Add(this.txtTekiyoCd);
			this.gpbTekiyo.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.gpbTekiyo.Location = new global::System.Drawing.Point(14, 85);
			this.gpbTekiyo.Name = "gpbTekiyo";
			this.gpbTekiyo.Size = new global::System.Drawing.Size(305, 52);
			this.gpbTekiyo.TabIndex = 2;
			this.gpbTekiyo.TabStop = false;
			this.gpbTekiyo.Text = "摘要";
			this.txtTekiyo.AutoSizeFromLength = true;
			this.txtTekiyo.DisplayLength = new int?(30);
			this.txtTekiyo.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.txtTekiyo.ImeMode = global::System.Windows.Forms.ImeMode.On;
			this.txtTekiyo.Location = new global::System.Drawing.Point(58, 20);
			this.txtTekiyo.MaxLength = 40;
			this.txtTekiyo.Name = "txtTekiyo";
			this.txtTekiyo.Size = new global::System.Drawing.Size(216, 20);
			this.txtTekiyo.TabIndex = 1;
			this.txtTekiyo.KeyDown += new global::System.Windows.Forms.KeyEventHandler(this.txtTekiyo_KeyDown);
			this.txtTekiyo.Validating += new global::System.ComponentModel.CancelEventHandler(this.txtTekiyo_Validating);
			this.txtTekiyoCd.AutoSizeFromLength = true;
			this.txtTekiyoCd.DisplayLength = null;
			this.txtTekiyoCd.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.txtTekiyoCd.ImeMode = global::System.Windows.Forms.ImeMode.Disable;
			this.txtTekiyoCd.Location = new global::System.Drawing.Point(22, 20);
			this.txtTekiyoCd.MaxLength = 4;
			this.txtTekiyoCd.Name = "txtTekiyoCd";
			this.txtTekiyoCd.Size = new global::System.Drawing.Size(34, 20);
			this.txtTekiyoCd.TabIndex = 0;
			this.txtTekiyoCd.TextAlign = global::System.Windows.Forms.HorizontalAlignment.Right;
			this.txtTekiyoCd.Validating += new global::System.ComponentModel.CancelEventHandler(this.txtTekiyoCd_Validating);
			this.gpbSwkDpyHzk.Controls.Add(this.lblSwkDpyDtGengo);
			this.gpbSwkDpyHzk.Controls.Add(this.lblSwkDpyDtDay);
			this.gpbSwkDpyHzk.Controls.Add(this.txtSwkDpyDtDay);
			this.gpbSwkDpyHzk.Controls.Add(this.lblSwkDpyDtMonth);
			this.gpbSwkDpyHzk.Controls.Add(this.txtSwkDpyDtMonth);
			this.gpbSwkDpyHzk.Controls.Add(this.lblSwkDpyDtJpYear);
			this.gpbSwkDpyHzk.Controls.Add(this.txtSwkDpyDtJpYear);
			this.gpbSwkDpyHzk.Controls.Add(this.label1);
			this.gpbSwkDpyHzk.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.gpbSwkDpyHzk.Location = new global::System.Drawing.Point(254, 22);
			this.gpbSwkDpyHzk.Name = "gpbSwkDpyHzk";
			this.gpbSwkDpyHzk.Size = new global::System.Drawing.Size(224, 52);
			this.gpbSwkDpyHzk.TabIndex = 1;
			this.gpbSwkDpyHzk.TabStop = false;
			this.gpbSwkDpyHzk.Text = "仕訳伝票日付";
			this.lblSwkDpyDtGengo.BackColor = global::System.Drawing.Color.Silver;
			this.lblSwkDpyDtGengo.BorderStyle = global::System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblSwkDpyDtGengo.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.lblSwkDpyDtGengo.Location = new global::System.Drawing.Point(26, 20);
			this.lblSwkDpyDtGengo.Name = "lblSwkDpyDtGengo";
			this.lblSwkDpyDtGengo.Size = new global::System.Drawing.Size(39, 20);
			this.lblSwkDpyDtGengo.TabIndex = 904;
			this.lblSwkDpyDtGengo.TextAlign = global::System.Drawing.ContentAlignment.MiddleCenter;
			this.lblSwkDpyDtDay.BackColor = global::System.Drawing.Color.Silver;
			this.lblSwkDpyDtDay.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.lblSwkDpyDtDay.Location = new global::System.Drawing.Point(170, 20);
			this.lblSwkDpyDtDay.Name = "lblSwkDpyDtDay";
			this.lblSwkDpyDtDay.Size = new global::System.Drawing.Size(18, 20);
			this.lblSwkDpyDtDay.TabIndex = 6;
			this.lblSwkDpyDtDay.Text = "日";
			this.lblSwkDpyDtDay.TextAlign = global::System.Drawing.ContentAlignment.MiddleLeft;
			this.txtSwkDpyDtDay.AutoSizeFromLength = true;
			this.txtSwkDpyDtDay.DisplayLength = null;
			this.txtSwkDpyDtDay.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.txtSwkDpyDtDay.ImeMode = global::System.Windows.Forms.ImeMode.Disable;
			this.txtSwkDpyDtDay.Location = new global::System.Drawing.Point(148, 20);
			this.txtSwkDpyDtDay.MaxLength = 2;
			this.txtSwkDpyDtDay.Name = "txtSwkDpyDtDay";
			this.txtSwkDpyDtDay.Size = new global::System.Drawing.Size(20, 20);
			this.txtSwkDpyDtDay.TabIndex = 5;
			this.txtSwkDpyDtDay.TextAlign = global::System.Windows.Forms.HorizontalAlignment.Right;
			this.txtSwkDpyDtDay.Validating += new global::System.ComponentModel.CancelEventHandler(this.txtSwkDpyDtDay_Validating);
			this.lblSwkDpyDtMonth.BackColor = global::System.Drawing.Color.Silver;
			this.lblSwkDpyDtMonth.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.lblSwkDpyDtMonth.Location = new global::System.Drawing.Point(129, 20);
			this.lblSwkDpyDtMonth.Name = "lblSwkDpyDtMonth";
			this.lblSwkDpyDtMonth.Size = new global::System.Drawing.Size(18, 20);
			this.lblSwkDpyDtMonth.TabIndex = 4;
			this.lblSwkDpyDtMonth.Text = "月";
			this.lblSwkDpyDtMonth.TextAlign = global::System.Drawing.ContentAlignment.MiddleLeft;
			this.txtSwkDpyDtMonth.AutoSizeFromLength = true;
			this.txtSwkDpyDtMonth.DisplayLength = null;
			this.txtSwkDpyDtMonth.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.txtSwkDpyDtMonth.ImeMode = global::System.Windows.Forms.ImeMode.Disable;
			this.txtSwkDpyDtMonth.Location = new global::System.Drawing.Point(107, 20);
			this.txtSwkDpyDtMonth.MaxLength = 2;
			this.txtSwkDpyDtMonth.Name = "txtSwkDpyDtMonth";
			this.txtSwkDpyDtMonth.Size = new global::System.Drawing.Size(20, 20);
			this.txtSwkDpyDtMonth.TabIndex = 3;
			this.txtSwkDpyDtMonth.TextAlign = global::System.Windows.Forms.HorizontalAlignment.Right;
			this.txtSwkDpyDtMonth.Validating += new global::System.ComponentModel.CancelEventHandler(this.txtSwkDpyDtMonth_Validating);
			this.lblSwkDpyDtJpYear.BackColor = global::System.Drawing.Color.Silver;
			this.lblSwkDpyDtJpYear.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.lblSwkDpyDtJpYear.Location = new global::System.Drawing.Point(88, 20);
			this.lblSwkDpyDtJpYear.Name = "lblSwkDpyDtJpYear";
			this.lblSwkDpyDtJpYear.Size = new global::System.Drawing.Size(18, 20);
			this.lblSwkDpyDtJpYear.TabIndex = 2;
			this.lblSwkDpyDtJpYear.Text = "年";
			this.lblSwkDpyDtJpYear.TextAlign = global::System.Drawing.ContentAlignment.MiddleLeft;
			this.txtSwkDpyDtJpYear.AutoSizeFromLength = true;
			this.txtSwkDpyDtJpYear.DisplayLength = null;
			this.txtSwkDpyDtJpYear.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.txtSwkDpyDtJpYear.ImeMode = global::System.Windows.Forms.ImeMode.Disable;
			this.txtSwkDpyDtJpYear.Location = new global::System.Drawing.Point(66, 20);
			this.txtSwkDpyDtJpYear.MaxLength = 2;
			this.txtSwkDpyDtJpYear.Name = "txtSwkDpyDtJpYear";
			this.txtSwkDpyDtJpYear.Size = new global::System.Drawing.Size(20, 20);
			this.txtSwkDpyDtJpYear.TabIndex = 1;
			this.txtSwkDpyDtJpYear.TextAlign = global::System.Windows.Forms.HorizontalAlignment.Right;
			this.txtSwkDpyDtJpYear.Validating += new global::System.ComponentModel.CancelEventHandler(this.txtSwkDpyDtJpYear_Validating);
			this.label1.BackColor = global::System.Drawing.Color.Silver;
			this.label1.BorderStyle = global::System.Windows.Forms.BorderStyle.Fixed3D;
			this.label1.Location = new global::System.Drawing.Point(24, 18);
			this.label1.Name = "label1";
			this.label1.Size = new global::System.Drawing.Size(167, 25);
			this.label1.TabIndex = 904;
			this.label1.Text = " ";
			this.gpbTantosha.Controls.Add(this.lblTantoNm);
			this.gpbTantosha.Controls.Add(this.txtTantoCd);
			this.gpbTantosha.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.gpbTantosha.Location = new global::System.Drawing.Point(14, 22);
			this.gpbTantosha.Name = "gpbTantosha";
			this.gpbTantosha.Size = new global::System.Drawing.Size(226, 52);
			this.gpbTantosha.TabIndex = 0;
			this.gpbTantosha.TabStop = false;
			this.gpbTantosha.Text = "担当者";
			this.lblTantoNm.BackColor = global::System.Drawing.Color.Silver;
			this.lblTantoNm.BorderStyle = global::System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblTantoNm.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.lblTantoNm.Location = new global::System.Drawing.Point(59, 22);
			this.lblTantoNm.Name = "lblTantoNm";
			this.lblTantoNm.Size = new global::System.Drawing.Size(140, 20);
			this.lblTantoNm.TabIndex = 6;
			this.lblTantoNm.TextAlign = global::System.Drawing.ContentAlignment.MiddleLeft;
			this.txtTantoCd.AutoSizeFromLength = true;
			this.txtTantoCd.DisplayLength = null;
			this.txtTantoCd.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.txtTantoCd.ImeMode = global::System.Windows.Forms.ImeMode.Disable;
			this.txtTantoCd.Location = new global::System.Drawing.Point(23, 22);
			this.txtTantoCd.MaxLength = 4;
			this.txtTantoCd.Name = "txtTantoCd";
			this.txtTantoCd.Size = new global::System.Drawing.Size(34, 20);
			this.txtTantoCd.TabIndex = 5;
			this.txtTantoCd.TextAlign = global::System.Windows.Forms.HorizontalAlignment.Right;
			this.txtTantoCd.Validating += new global::System.ComponentModel.CancelEventHandler(this.txtTantoCd_Validating);
			this.lblUpdMode.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.lblUpdMode.ForeColor = global::System.Drawing.Color.Red;
			this.lblUpdMode.Location = new global::System.Drawing.Point(14, 49);
			this.lblUpdMode.Name = "lblUpdMode";
			this.lblUpdMode.Size = new global::System.Drawing.Size(110, 20);
			this.lblUpdMode.TabIndex = 1;
			this.lblUpdMode.Text = "【更新モード】";
			this.lblUpdMode.TextAlign = global::System.Drawing.ContentAlignment.MiddleCenter;
			this.gbxMizuageShisho.Controls.Add(this.txtMizuageShishoCd);
			this.gbxMizuageShisho.Controls.Add(this.lblMizuageShishoNm);
			this.gbxMizuageShisho.Controls.Add(this.lblMizuageShisho);
			this.gbxMizuageShisho.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f);
			this.gbxMizuageShisho.ForeColor = global::System.Drawing.Color.Black;
			this.gbxMizuageShisho.Location = new global::System.Drawing.Point(12, 86);
			this.gbxMizuageShisho.Name = "gbxMizuageShisho";
			this.gbxMizuageShisho.Size = new global::System.Drawing.Size(381, 66);
			this.gbxMizuageShisho.TabIndex = 902;
			this.gbxMizuageShisho.TabStop = false;
			this.gbxMizuageShisho.Text = "支所";
			this.txtMizuageShishoCd.AutoSizeFromLength = true;
			this.txtMizuageShishoCd.DisplayLength = null;
			this.txtMizuageShishoCd.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.txtMizuageShishoCd.ImeMode = global::System.Windows.Forms.ImeMode.Disable;
			this.txtMizuageShishoCd.Location = new global::System.Drawing.Point(53, 27);
			this.txtMizuageShishoCd.MaxLength = 5;
			this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
			this.txtMizuageShishoCd.Size = new global::System.Drawing.Size(51, 20);
			this.txtMizuageShishoCd.TabIndex = 1;
			this.txtMizuageShishoCd.TabStop = false;
			this.txtMizuageShishoCd.TextAlign = global::System.Windows.Forms.HorizontalAlignment.Right;
			this.txtMizuageShishoCd.Validating += new global::System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
			this.lblMizuageShishoNm.BackColor = global::System.Drawing.Color.Silver;
			this.lblMizuageShishoNm.BorderStyle = global::System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblMizuageShishoNm.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.lblMizuageShishoNm.Location = new global::System.Drawing.Point(109, 27);
			this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
			this.lblMizuageShishoNm.Size = new global::System.Drawing.Size(250, 20);
			this.lblMizuageShishoNm.TabIndex = 2;
			this.lblMizuageShishoNm.TextAlign = global::System.Drawing.ContentAlignment.MiddleLeft;
			this.lblMizuageShisho.BackColor = global::System.Drawing.Color.Silver;
			this.lblMizuageShisho.BorderStyle = global::System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblMizuageShisho.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f);
			this.lblMizuageShisho.Location = new global::System.Drawing.Point(10, 25);
			this.lblMizuageShisho.Name = "lblMizuageShisho";
			this.lblMizuageShisho.Size = new global::System.Drawing.Size(354, 25);
			this.lblMizuageShisho.TabIndex = 0;
			this.lblMizuageShisho.Text = "支所";
			this.lblMizuageShisho.TextAlign = global::System.Drawing.ContentAlignment.MiddleLeft;
			base.AutoScaleDimensions = new global::System.Drawing.SizeF(6f, 12f);
			base.AutoScaleMode = global::System.Windows.Forms.AutoScaleMode.Font;
			base.ClientSize = new global::System.Drawing.Size(839, 638);
			base.Controls.Add(this.gbxMizuageShisho);
			base.Controls.Add(this.lblMessage);
			base.Controls.Add(this.lblUpdMode);
			base.Controls.Add(this.gpbShiwakeDenpyo);
			base.Controls.Add(this.gpbFunanushiCd);
			base.Controls.Add(this.gpbDenpyoHizuke);
			base.Controls.Add(this.gpbShimebi);
			base.Controls.Add(this.gpbSakuseiKbn);
			base.ImeMode = global::System.Windows.Forms.ImeMode.KatakanaHalf;
			base.Name = "KBDB1011";
			this.Text = "売上仕訳データ作成";
			base.Controls.SetChildIndex(this.gpbSakuseiKbn, 0);
			base.Controls.SetChildIndex(this.gpbShimebi, 0);
			base.Controls.SetChildIndex(this.gpbDenpyoHizuke, 0);
			base.Controls.SetChildIndex(this.gpbFunanushiCd, 0);
			base.Controls.SetChildIndex(this.gpbShiwakeDenpyo, 0);
			base.Controls.SetChildIndex(this.lblUpdMode, 0);
			base.Controls.SetChildIndex(this.lblMessage, 0);
			base.Controls.SetChildIndex(this.pnlDebug, 0);
			base.Controls.SetChildIndex(this.lblTitle, 0);
			base.Controls.SetChildIndex(this.gbxMizuageShisho, 0);
			this.pnlDebug.ResumeLayout(false);
			this.gpbSakuseiKbn.ResumeLayout(false);
			this.gpbSakuseiKbn.PerformLayout();
			this.gpbShimebi.ResumeLayout(false);
			this.gpbShimebi.PerformLayout();
			this.gpbDenpyoHizuke.ResumeLayout(false);
			this.gpbDenpyoHizuke.PerformLayout();
			this.gpbFunanushiCd.ResumeLayout(false);
			this.gpbFunanushiCd.PerformLayout();
			this.gpbShiwakeDenpyo.ResumeLayout(false);
			this.gpbTekiyo.ResumeLayout(false);
			this.gpbTekiyo.PerformLayout();
			this.gpbSwkDpyHzk.ResumeLayout(false);
			this.gpbSwkDpyHzk.PerformLayout();
			this.gpbTantosha.ResumeLayout(false);
			this.gpbTantosha.PerformLayout();
			this.gbxMizuageShisho.ResumeLayout(false);
			this.gbxMizuageShisho.PerformLayout();
			base.ResumeLayout(false);
		}

		// Token: 0x0400000F RID: 15
		private global::System.ComponentModel.IContainer components;

		// Token: 0x04000010 RID: 16
		private global::System.Windows.Forms.Label lblMessage;

		// Token: 0x04000011 RID: 17
		private global::System.Windows.Forms.GroupBox gpbSakuseiKbn;

		// Token: 0x04000012 RID: 18
		private global::System.Windows.Forms.GroupBox gpbShimebi;

		// Token: 0x04000013 RID: 19
		private global::System.Windows.Forms.GroupBox gpbDenpyoHizuke;

		// Token: 0x04000014 RID: 20
		private global::System.Windows.Forms.GroupBox gpbFunanushiCd;

		// Token: 0x04000015 RID: 21
		private global::System.Windows.Forms.GroupBox gpbShiwakeDenpyo;

		// Token: 0x04000016 RID: 22
		private global::System.Windows.Forms.GroupBox gpbSwkDpyHzk;

		// Token: 0x04000017 RID: 23
		private global::System.Windows.Forms.GroupBox gpbTantosha;

		// Token: 0x04000018 RID: 24
		private global::System.Windows.Forms.GroupBox gpbTekiyo;

		// Token: 0x04000019 RID: 25
		private global::System.Windows.Forms.RadioButton rdbGenkin;

		// Token: 0x0400001A RID: 26
		private global::System.Windows.Forms.RadioButton rdbGenkinKake;

		// Token: 0x0400001B RID: 27
		private global::System.Windows.Forms.RadioButton rdbShimebi;

		// Token: 0x0400001C RID: 28
		private global::System.Windows.Forms.Label lblShimebiMemo;

		// Token: 0x0400001D RID: 29
		private global::jp.co.fsi.common.controls.FsiTextBox txtShimebi;

		// Token: 0x0400001E RID: 30
		private global::jp.co.fsi.common.controls.FsiTextBox txtDpyDtFrJpYear;

		// Token: 0x0400001F RID: 31
		private global::System.Windows.Forms.Label lblDpyDtFrJpYear;

		// Token: 0x04000020 RID: 32
		private global::System.Windows.Forms.Label lblDpyDtFrMonth;

		// Token: 0x04000021 RID: 33
		private global::jp.co.fsi.common.controls.FsiTextBox txtDpyDtFrMonth;

		// Token: 0x04000022 RID: 34
		private global::System.Windows.Forms.Label lblDpyDtFrDay;

		// Token: 0x04000023 RID: 35
		private global::jp.co.fsi.common.controls.FsiTextBox txtDpyDtFrDay;

		// Token: 0x04000024 RID: 36
		private global::System.Windows.Forms.Label lblDpyDtToDay;

		// Token: 0x04000025 RID: 37
		private global::jp.co.fsi.common.controls.FsiTextBox txtDpyDtToDay;

		// Token: 0x04000026 RID: 38
		private global::System.Windows.Forms.Label lblDpyDtToMonth;

		// Token: 0x04000027 RID: 39
		private global::jp.co.fsi.common.controls.FsiTextBox txtDpyDtToMonth;

		// Token: 0x04000028 RID: 40
		private global::System.Windows.Forms.Label lblDpyDtToJpYear;

		// Token: 0x04000029 RID: 41
		private global::jp.co.fsi.common.controls.FsiTextBox txtDpyDtToJpYear;

		// Token: 0x0400002A RID: 42
		private global::System.Windows.Forms.Label lblDpyDtBetween;

		// Token: 0x0400002B RID: 43
		private global::System.Windows.Forms.Label lblFunanushiNmFr;

		// Token: 0x0400002C RID: 44
		private global::jp.co.fsi.common.controls.FsiTextBox txtFunanushiCdFr;

		// Token: 0x0400002D RID: 45
		private global::System.Windows.Forms.Label lblFunanushiNmTo;

		// Token: 0x0400002E RID: 46
		private global::jp.co.fsi.common.controls.FsiTextBox txtFunanushiCdTo;

		// Token: 0x0400002F RID: 47
		private global::System.Windows.Forms.Label lblFunanushiCdBetween;

		// Token: 0x04000030 RID: 48
		private global::System.Windows.Forms.Label lblTantoNm;

		// Token: 0x04000031 RID: 49
		private global::jp.co.fsi.common.controls.FsiTextBox txtTantoCd;

		// Token: 0x04000032 RID: 50
		private global::System.Windows.Forms.Label lblSwkDpyDtDay;

		// Token: 0x04000033 RID: 51
		private global::jp.co.fsi.common.controls.FsiTextBox txtSwkDpyDtDay;

		// Token: 0x04000034 RID: 52
		private global::System.Windows.Forms.Label lblSwkDpyDtMonth;

		// Token: 0x04000035 RID: 53
		private global::jp.co.fsi.common.controls.FsiTextBox txtSwkDpyDtMonth;

		// Token: 0x04000036 RID: 54
		private global::System.Windows.Forms.Label lblSwkDpyDtJpYear;

		// Token: 0x04000037 RID: 55
		private global::jp.co.fsi.common.controls.FsiTextBox txtSwkDpyDtJpYear;

		// Token: 0x04000038 RID: 56
		private global::jp.co.fsi.common.controls.FsiTextBox txtTekiyoCd;

		// Token: 0x04000039 RID: 57
		private global::jp.co.fsi.common.controls.FsiTextBox txtTekiyo;

		// Token: 0x0400003A RID: 58
		private global::System.Windows.Forms.Label lblUpdMode;

		// Token: 0x0400003B RID: 59
		private global::System.Windows.Forms.Label lblDpyDtFrGengo;

		// Token: 0x0400003C RID: 60
		private global::System.Windows.Forms.Label lblDpyDtToGengo;

		// Token: 0x0400003D RID: 61
		private global::System.Windows.Forms.Label lblSwkDpyDtGengo;

		// Token: 0x0400003E RID: 62
		private global::System.Windows.Forms.Label lblEraBackFr;

		// Token: 0x0400003F RID: 63
		private global::System.Windows.Forms.Label lblEraBackTo;

		// Token: 0x04000040 RID: 64
		private global::System.Windows.Forms.Label label1;

		// Token: 0x04000041 RID: 65
		private global::System.Windows.Forms.GroupBox gbxMizuageShisho;

		// Token: 0x04000042 RID: 66
		private global::jp.co.fsi.common.controls.FsiTextBox txtMizuageShishoCd;

		// Token: 0x04000043 RID: 67
		private global::System.Windows.Forms.Label lblMizuageShishoNm;

		// Token: 0x04000044 RID: 68
		private global::System.Windows.Forms.Label lblMizuageShisho;
	}
}
