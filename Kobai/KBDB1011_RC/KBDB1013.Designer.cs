﻿namespace jp.co.fsi.kb.kbdb1011
{
	// Token: 0x02000007 RID: 7
	public partial class KBDB1013 : global::jp.co.fsi.common.forms.BasePgForm
	{
		// Token: 0x06000075 RID: 117 RVA: 0x00010304 File Offset: 0x0000E504
		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		// Token: 0x06000076 RID: 118 RVA: 0x00010324 File Offset: 0x0000E524
		private void InitializeComponent()
		{
			this.dgvList = new global::System.Windows.Forms.DataGridView();
			this.lblTerm = new global::System.Windows.Forms.Label();
			this.label1 = new global::System.Windows.Forms.Label();
			this.lblGenkinUriage = new global::System.Windows.Forms.Label();
			this.lblGenkinZei = new global::System.Windows.Forms.Label();
			this.lblKakeUriage = new global::System.Windows.Forms.Label();
			this.lblKakeZei = new global::System.Windows.Forms.Label();
			this.lblKeiUriage = new global::System.Windows.Forms.Label();
			this.pnlDebug.SuspendLayout();
			((global::System.ComponentModel.ISupportInitialize)this.dgvList).BeginInit();
			base.SuspendLayout();
			this.lblTitle.Size = new global::System.Drawing.Size(613, 23);
			this.lblTitle.Text = "仕訳データ参照";
			this.btnEsc.Location = new global::System.Drawing.Point(3, 49);
			this.btnF1.Location = new global::System.Drawing.Point(67, 49);
			this.btnF2.Visible = false;
			this.btnF3.Visible = false;
			this.btnF4.Visible = false;
			this.btnF5.Visible = false;
			this.btnF7.Visible = false;
			this.btnF6.Location = new global::System.Drawing.Point(131, 49);
			this.btnF8.Visible = false;
			this.btnF9.Visible = false;
			this.btnF12.Visible = false;
			this.btnF11.Visible = false;
			this.btnF10.Visible = false;
			this.pnlDebug.Location = new global::System.Drawing.Point(5, 321);
			this.pnlDebug.Size = new global::System.Drawing.Size(630, 100);
			this.dgvList.AllowUserToAddRows = false;
			this.dgvList.AllowUserToDeleteRows = false;
			this.dgvList.ColumnHeadersHeightSizeMode = global::System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvList.ImeMode = global::System.Windows.Forms.ImeMode.Disable;
			this.dgvList.Location = new global::System.Drawing.Point(13, 29);
			this.dgvList.MultiSelect = false;
			this.dgvList.Name = "dgvList";
			this.dgvList.ReadOnly = true;
			this.dgvList.RowHeadersVisible = false;
			this.dgvList.RowTemplate.Height = 21;
			this.dgvList.ScrollBars = global::System.Windows.Forms.ScrollBars.Vertical;
			this.dgvList.SelectionMode = global::System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dgvList.Size = new global::System.Drawing.Size(610, 316);
			this.dgvList.TabIndex = 22;
			this.lblTerm.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.lblTerm.ForeColor = global::System.Drawing.Color.Blue;
			this.lblTerm.Location = new global::System.Drawing.Point(14, 13);
			this.lblTerm.Name = "lblTerm";
			this.lblTerm.Size = new global::System.Drawing.Size(240, 12);
			this.lblTerm.TabIndex = 903;
			this.lblTerm.Text = " 平成 21年 4月 1日 ～ 平成 22年 3月31日";
			this.lblTerm.TextAlign = global::System.Drawing.ContentAlignment.MiddleLeft;
			this.label1.BackColor = global::System.Drawing.SystemColors.AppWorkspace;
			this.label1.BorderStyle = global::System.Windows.Forms.BorderStyle.FixedSingle;
			this.label1.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9f, global::System.Drawing.FontStyle.Bold, global::System.Drawing.GraphicsUnit.Point, 128);
			this.label1.ForeColor = global::System.Drawing.Color.White;
			this.label1.Location = new global::System.Drawing.Point(13, 344);
			this.label1.Name = "label1";
			this.label1.Size = new global::System.Drawing.Size(192, 20);
			this.label1.TabIndex = 904;
			this.label1.Text = "[ 合 計 ] ";
			this.label1.TextAlign = global::System.Drawing.ContentAlignment.MiddleRight;
			this.lblGenkinUriage.BackColor = global::System.Drawing.Color.White;
			this.lblGenkinUriage.BorderStyle = global::System.Windows.Forms.BorderStyle.FixedSingle;
			this.lblGenkinUriage.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.lblGenkinUriage.ForeColor = global::System.Drawing.Color.Black;
			this.lblGenkinUriage.Location = new global::System.Drawing.Point(204, 344);
			this.lblGenkinUriage.Name = "lblGenkinUriage";
			this.lblGenkinUriage.Size = new global::System.Drawing.Size(81, 20);
			this.lblGenkinUriage.TabIndex = 905;
			this.lblGenkinUriage.Text = "-999,999,999";
			this.lblGenkinUriage.TextAlign = global::System.Drawing.ContentAlignment.MiddleRight;
			this.lblGenkinZei.BackColor = global::System.Drawing.Color.White;
			this.lblGenkinZei.BorderStyle = global::System.Windows.Forms.BorderStyle.FixedSingle;
			this.lblGenkinZei.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.lblGenkinZei.ForeColor = global::System.Drawing.Color.Black;
			this.lblGenkinZei.Location = new global::System.Drawing.Point(284, 344);
			this.lblGenkinZei.Name = "lblGenkinZei";
			this.lblGenkinZei.Size = new global::System.Drawing.Size(81, 20);
			this.lblGenkinZei.TabIndex = 906;
			this.lblGenkinZei.Text = "-999,999,999";
			this.lblGenkinZei.TextAlign = global::System.Drawing.ContentAlignment.MiddleRight;
			this.lblKakeUriage.BackColor = global::System.Drawing.Color.White;
			this.lblKakeUriage.BorderStyle = global::System.Windows.Forms.BorderStyle.FixedSingle;
			this.lblKakeUriage.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.lblKakeUriage.ForeColor = global::System.Drawing.Color.Black;
			this.lblKakeUriage.Location = new global::System.Drawing.Point(364, 344);
			this.lblKakeUriage.Name = "lblKakeUriage";
			this.lblKakeUriage.Size = new global::System.Drawing.Size(81, 20);
			this.lblKakeUriage.TabIndex = 907;
			this.lblKakeUriage.Text = "-999,999,999";
			this.lblKakeUriage.TextAlign = global::System.Drawing.ContentAlignment.MiddleRight;
			this.lblKakeZei.BackColor = global::System.Drawing.Color.White;
			this.lblKakeZei.BorderStyle = global::System.Windows.Forms.BorderStyle.FixedSingle;
			this.lblKakeZei.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.lblKakeZei.ForeColor = global::System.Drawing.Color.Black;
			this.lblKakeZei.Location = new global::System.Drawing.Point(444, 344);
			this.lblKakeZei.Name = "lblKakeZei";
			this.lblKakeZei.Size = new global::System.Drawing.Size(81, 20);
			this.lblKakeZei.TabIndex = 908;
			this.lblKakeZei.Text = "-999,999,999";
			this.lblKakeZei.TextAlign = global::System.Drawing.ContentAlignment.MiddleRight;
			this.lblKeiUriage.BackColor = global::System.Drawing.Color.White;
			this.lblKeiUriage.BorderStyle = global::System.Windows.Forms.BorderStyle.FixedSingle;
			this.lblKeiUriage.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.lblKeiUriage.ForeColor = global::System.Drawing.Color.Black;
			this.lblKeiUriage.Location = new global::System.Drawing.Point(524, 344);
			this.lblKeiUriage.Name = "lblKeiUriage";
			this.lblKeiUriage.Size = new global::System.Drawing.Size(81, 20);
			this.lblKeiUriage.TabIndex = 909;
			this.lblKeiUriage.Text = "-999,999,999";
			this.lblKeiUriage.TextAlign = global::System.Drawing.ContentAlignment.MiddleRight;
			base.AutoScaleDimensions = new global::System.Drawing.SizeF(6f, 12f);
			base.AutoScaleMode = global::System.Windows.Forms.AutoScaleMode.Font;
			base.ClientSize = new global::System.Drawing.Size(638, 424);
			base.Controls.Add(this.lblKeiUriage);
			base.Controls.Add(this.lblKakeZei);
			base.Controls.Add(this.lblKakeUriage);
			base.Controls.Add(this.lblGenkinZei);
			base.Controls.Add(this.lblGenkinUriage);
			base.Controls.Add(this.label1);
			base.Controls.Add(this.lblTerm);
			base.Controls.Add(this.dgvList);
			base.ImeMode = global::System.Windows.Forms.ImeMode.KatakanaHalf;
			base.Name = "KBDB1013";
			base.ShowFButton = true;
			base.ShowTitle = false;
			this.Text = "仕訳データ参照";
			base.Shown += new global::System.EventHandler(this.KBDB1013_Shown);
			base.Controls.SetChildIndex(this.pnlDebug, 0);
			base.Controls.SetChildIndex(this.lblTitle, 0);
			base.Controls.SetChildIndex(this.dgvList, 0);
			base.Controls.SetChildIndex(this.lblTerm, 0);
			base.Controls.SetChildIndex(this.label1, 0);
			base.Controls.SetChildIndex(this.lblGenkinUriage, 0);
			base.Controls.SetChildIndex(this.lblGenkinZei, 0);
			base.Controls.SetChildIndex(this.lblKakeUriage, 0);
			base.Controls.SetChildIndex(this.lblKakeZei, 0);
			base.Controls.SetChildIndex(this.lblKeiUriage, 0);
			this.pnlDebug.ResumeLayout(false);
			((global::System.ComponentModel.ISupportInitialize)this.dgvList).EndInit();
			base.ResumeLayout(false);
		}

		// Token: 0x0400004C RID: 76
		private global::System.ComponentModel.IContainer components;

		// Token: 0x0400004D RID: 77
		private global::System.Windows.Forms.DataGridView dgvList;

		// Token: 0x0400004E RID: 78
		private global::System.Windows.Forms.Label lblTerm;

		// Token: 0x0400004F RID: 79
		private global::System.Windows.Forms.Label label1;

		// Token: 0x04000050 RID: 80
		private global::System.Windows.Forms.Label lblGenkinUriage;

		// Token: 0x04000051 RID: 81
		private global::System.Windows.Forms.Label lblGenkinZei;

		// Token: 0x04000052 RID: 82
		private global::System.Windows.Forms.Label lblKakeUriage;

		// Token: 0x04000053 RID: 83
		private global::System.Windows.Forms.Label lblKakeZei;

		// Token: 0x04000054 RID: 84
		private global::System.Windows.Forms.Label lblKeiUriage;
	}
}
