﻿using System;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.kb.kbdb1011
{
	// Token: 0x02000004 RID: 4
	internal static class Program
	{
		// Token: 0x0600001B RID: 27 RVA: 0x0000811E File Offset: 0x0000631E
		[STAThread]
		private static void Main()
		{
			Application.ThreadException += Program.Application_ThreadException;
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			Application.Run(new KBDB1011());
		}

		// Token: 0x0600001C RID: 28 RVA: 0x00008146 File Offset: 0x00006346
		private static void Application_ThreadException(object sender, ThreadExceptionEventArgs e)
		{
			new Logger().Output(Path.GetFileName(Application.ExecutablePath), e.Exception);
			Msg.Error("エラーが発生しました。\n開発元までお問い合わせ下さい。");
			Application.Exit();
		}
	}
}
