﻿namespace jp.co.fsi.kb.kbdb1011
{
	// Token: 0x02000008 RID: 8
	public partial class KBDB1015 : global::jp.co.fsi.common.forms.BasePgForm
	{
		// Token: 0x06000089 RID: 137 RVA: 0x000119C1 File Offset: 0x0000FBC1
		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		// Token: 0x0600008A RID: 138 RVA: 0x000119E0 File Offset: 0x0000FBE0
		private void InitializeComponent()
		{
			this.gpbGenkin = new global::System.Windows.Forms.GroupBox();
			this.chkGenGenHnDp = new global::System.Windows.Forms.CheckBox();
			this.chkGenGenToriDp = new global::System.Windows.Forms.CheckBox();
			this.chkGenKakeHnDp = new global::System.Windows.Forms.CheckBox();
			this.chkGenKakeToriDp = new global::System.Windows.Forms.CheckBox();
			this.gpbGenkinKake = new global::System.Windows.Forms.GroupBox();
			this.chkGenKakeGenHnDp = new global::System.Windows.Forms.CheckBox();
			this.chkGenKakeGenToriDp = new global::System.Windows.Forms.CheckBox();
			this.chkGenKakeKakeHnDp = new global::System.Windows.Forms.CheckBox();
			this.chkGenKakeKakeToriDp = new global::System.Windows.Forms.CheckBox();
			this.gpbShimebi = new global::System.Windows.Forms.GroupBox();
			this.chkSmbGenHnDp = new global::System.Windows.Forms.CheckBox();
			this.chkSmbGenToriDp = new global::System.Windows.Forms.CheckBox();
			this.chkSmbKakeHnDp = new global::System.Windows.Forms.CheckBox();
			this.chkSmbKakeToriDp = new global::System.Windows.Forms.CheckBox();
			this.gpbSwkDpMk = new global::System.Windows.Forms.GroupBox();
			this.rdbSeikyuDp = new global::System.Windows.Forms.RadioButton();
			this.rdbTanitsuDp = new global::System.Windows.Forms.RadioButton();
			this.rdbFukugoDp = new global::System.Windows.Forms.RadioButton();
			this.gpbSrkStBmn = new global::System.Windows.Forms.GroupBox();
			this.lblBumonNm = new global::System.Windows.Forms.Label();
			this.txtBumonCd = new global::jp.co.fsi.common.controls.FsiTextBox();
			this.groupBox1 = new global::System.Windows.Forms.GroupBox();
			this.txtTekiyo = new global::jp.co.fsi.common.controls.FsiTextBox();
			this.txtTekiyoCd = new global::jp.co.fsi.common.controls.FsiTextBox();
			this.pnlDebug.SuspendLayout();
			this.gpbGenkin.SuspendLayout();
			this.gpbGenkinKake.SuspendLayout();
			this.gpbShimebi.SuspendLayout();
			this.gpbSwkDpMk.SuspendLayout();
			this.gpbSrkStBmn.SuspendLayout();
			this.groupBox1.SuspendLayout();
			base.SuspendLayout();
			this.lblTitle.Size = new global::System.Drawing.Size(470, 23);
			this.lblTitle.Text = "仕訳データ作成 動作設定";
			this.btnEsc.Location = new global::System.Drawing.Point(3, 49);
			this.btnF1.Location = new global::System.Drawing.Point(67, 49);
			this.btnF2.Visible = false;
			this.btnF3.Visible = false;
			this.btnF4.Visible = false;
			this.btnF5.Visible = false;
			this.btnF7.Visible = false;
			this.btnF6.Location = new global::System.Drawing.Point(131, 49);
			this.btnF8.Visible = false;
			this.btnF9.Visible = false;
			this.btnF12.Visible = false;
			this.btnF11.Visible = false;
			this.btnF10.Visible = false;
			this.pnlDebug.Location = new global::System.Drawing.Point(5, 388);
			this.pnlDebug.Size = new global::System.Drawing.Size(487, 100);
			this.gpbGenkin.Controls.Add(this.chkGenGenHnDp);
			this.gpbGenkin.Controls.Add(this.chkGenGenToriDp);
			this.gpbGenkin.Controls.Add(this.chkGenKakeHnDp);
			this.gpbGenkin.Controls.Add(this.chkGenKakeToriDp);
			this.gpbGenkin.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.gpbGenkin.Location = new global::System.Drawing.Point(13, 13);
			this.gpbGenkin.Name = "gpbGenkin";
			this.gpbGenkin.Size = new global::System.Drawing.Size(469, 55);
			this.gpbGenkin.TabIndex = 1;
			this.gpbGenkin.TabStop = false;
			this.gpbGenkin.Text = "現金取引";
			this.chkGenGenHnDp.AutoSize = true;
			this.chkGenGenHnDp.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.chkGenGenHnDp.Location = new global::System.Drawing.Point(349, 22);
			this.chkGenGenHnDp.Name = "chkGenGenHnDp";
			this.chkGenGenHnDp.Size = new global::System.Drawing.Size(96, 16);
			this.chkGenGenHnDp.TabIndex = 3;
			this.chkGenGenHnDp.Text = "現金返品伝票";
			this.chkGenGenHnDp.UseVisualStyleBackColor = true;
			this.chkGenGenToriDp.AutoSize = true;
			this.chkGenGenToriDp.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.chkGenGenToriDp.Location = new global::System.Drawing.Point(234, 22);
			this.chkGenGenToriDp.Name = "chkGenGenToriDp";
			this.chkGenGenToriDp.Size = new global::System.Drawing.Size(96, 16);
			this.chkGenGenToriDp.TabIndex = 2;
			this.chkGenGenToriDp.Text = "現金取引伝票";
			this.chkGenGenToriDp.UseVisualStyleBackColor = true;
			this.chkGenKakeHnDp.AutoSize = true;
			this.chkGenKakeHnDp.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.chkGenKakeHnDp.Location = new global::System.Drawing.Point(127, 22);
			this.chkGenKakeHnDp.Name = "chkGenKakeHnDp";
			this.chkGenKakeHnDp.Size = new global::System.Drawing.Size(84, 16);
			this.chkGenKakeHnDp.TabIndex = 1;
			this.chkGenKakeHnDp.Text = "掛返品伝票";
			this.chkGenKakeHnDp.UseVisualStyleBackColor = true;
			this.chkGenKakeToriDp.AutoSize = true;
			this.chkGenKakeToriDp.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.chkGenKakeToriDp.Location = new global::System.Drawing.Point(21, 22);
			this.chkGenKakeToriDp.Name = "chkGenKakeToriDp";
			this.chkGenKakeToriDp.Size = new global::System.Drawing.Size(84, 16);
			this.chkGenKakeToriDp.TabIndex = 0;
			this.chkGenKakeToriDp.Text = "掛取引伝票";
			this.chkGenKakeToriDp.UseVisualStyleBackColor = true;
			this.gpbGenkinKake.Controls.Add(this.chkGenKakeGenHnDp);
			this.gpbGenkinKake.Controls.Add(this.chkGenKakeGenToriDp);
			this.gpbGenkinKake.Controls.Add(this.chkGenKakeKakeHnDp);
			this.gpbGenkinKake.Controls.Add(this.chkGenKakeKakeToriDp);
			this.gpbGenkinKake.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.gpbGenkinKake.Location = new global::System.Drawing.Point(13, 74);
			this.gpbGenkinKake.Name = "gpbGenkinKake";
			this.gpbGenkinKake.Size = new global::System.Drawing.Size(469, 54);
			this.gpbGenkinKake.TabIndex = 2;
			this.gpbGenkinKake.TabStop = false;
			this.gpbGenkinKake.Text = "現金、掛取引";
			this.chkGenKakeGenHnDp.AutoSize = true;
			this.chkGenKakeGenHnDp.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.chkGenKakeGenHnDp.Location = new global::System.Drawing.Point(349, 22);
			this.chkGenKakeGenHnDp.Name = "chkGenKakeGenHnDp";
			this.chkGenKakeGenHnDp.Size = new global::System.Drawing.Size(96, 16);
			this.chkGenKakeGenHnDp.TabIndex = 3;
			this.chkGenKakeGenHnDp.Text = "現金返品伝票";
			this.chkGenKakeGenHnDp.UseVisualStyleBackColor = true;
			this.chkGenKakeGenToriDp.AutoSize = true;
			this.chkGenKakeGenToriDp.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.chkGenKakeGenToriDp.Location = new global::System.Drawing.Point(234, 22);
			this.chkGenKakeGenToriDp.Name = "chkGenKakeGenToriDp";
			this.chkGenKakeGenToriDp.Size = new global::System.Drawing.Size(96, 16);
			this.chkGenKakeGenToriDp.TabIndex = 2;
			this.chkGenKakeGenToriDp.Text = "現金取引伝票";
			this.chkGenKakeGenToriDp.UseVisualStyleBackColor = true;
			this.chkGenKakeKakeHnDp.AutoSize = true;
			this.chkGenKakeKakeHnDp.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.chkGenKakeKakeHnDp.Location = new global::System.Drawing.Point(127, 22);
			this.chkGenKakeKakeHnDp.Name = "chkGenKakeKakeHnDp";
			this.chkGenKakeKakeHnDp.Size = new global::System.Drawing.Size(84, 16);
			this.chkGenKakeKakeHnDp.TabIndex = 1;
			this.chkGenKakeKakeHnDp.Text = "掛返品伝票";
			this.chkGenKakeKakeHnDp.UseVisualStyleBackColor = true;
			this.chkGenKakeKakeToriDp.AutoSize = true;
			this.chkGenKakeKakeToriDp.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.chkGenKakeKakeToriDp.Location = new global::System.Drawing.Point(21, 22);
			this.chkGenKakeKakeToriDp.Name = "chkGenKakeKakeToriDp";
			this.chkGenKakeKakeToriDp.Size = new global::System.Drawing.Size(84, 16);
			this.chkGenKakeKakeToriDp.TabIndex = 0;
			this.chkGenKakeKakeToriDp.Text = "掛取引伝票";
			this.chkGenKakeKakeToriDp.UseVisualStyleBackColor = true;
			this.gpbShimebi.Controls.Add(this.chkSmbGenHnDp);
			this.gpbShimebi.Controls.Add(this.chkSmbGenToriDp);
			this.gpbShimebi.Controls.Add(this.chkSmbKakeHnDp);
			this.gpbShimebi.Controls.Add(this.chkSmbKakeToriDp);
			this.gpbShimebi.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.gpbShimebi.Location = new global::System.Drawing.Point(13, 134);
			this.gpbShimebi.Name = "gpbShimebi";
			this.gpbShimebi.Size = new global::System.Drawing.Size(469, 59);
			this.gpbShimebi.TabIndex = 3;
			this.gpbShimebi.TabStop = false;
			this.gpbShimebi.Text = "締日基準";
			this.chkSmbGenHnDp.AutoSize = true;
			this.chkSmbGenHnDp.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.chkSmbGenHnDp.Location = new global::System.Drawing.Point(349, 27);
			this.chkSmbGenHnDp.Name = "chkSmbGenHnDp";
			this.chkSmbGenHnDp.Size = new global::System.Drawing.Size(96, 16);
			this.chkSmbGenHnDp.TabIndex = 3;
			this.chkSmbGenHnDp.Text = "現金返品伝票";
			this.chkSmbGenHnDp.UseVisualStyleBackColor = true;
			this.chkSmbGenToriDp.AutoSize = true;
			this.chkSmbGenToriDp.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.chkSmbGenToriDp.Location = new global::System.Drawing.Point(234, 27);
			this.chkSmbGenToriDp.Name = "chkSmbGenToriDp";
			this.chkSmbGenToriDp.Size = new global::System.Drawing.Size(96, 16);
			this.chkSmbGenToriDp.TabIndex = 2;
			this.chkSmbGenToriDp.Text = "現金取引伝票";
			this.chkSmbGenToriDp.UseVisualStyleBackColor = true;
			this.chkSmbKakeHnDp.AutoSize = true;
			this.chkSmbKakeHnDp.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.chkSmbKakeHnDp.Location = new global::System.Drawing.Point(127, 27);
			this.chkSmbKakeHnDp.Name = "chkSmbKakeHnDp";
			this.chkSmbKakeHnDp.Size = new global::System.Drawing.Size(84, 16);
			this.chkSmbKakeHnDp.TabIndex = 1;
			this.chkSmbKakeHnDp.Text = "掛返品伝票";
			this.chkSmbKakeHnDp.UseVisualStyleBackColor = true;
			this.chkSmbKakeToriDp.AutoSize = true;
			this.chkSmbKakeToriDp.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.chkSmbKakeToriDp.Location = new global::System.Drawing.Point(21, 27);
			this.chkSmbKakeToriDp.Name = "chkSmbKakeToriDp";
			this.chkSmbKakeToriDp.Size = new global::System.Drawing.Size(84, 16);
			this.chkSmbKakeToriDp.TabIndex = 0;
			this.chkSmbKakeToriDp.Text = "掛取引伝票";
			this.chkSmbKakeToriDp.UseVisualStyleBackColor = true;
			this.gpbSwkDpMk.Controls.Add(this.rdbSeikyuDp);
			this.gpbSwkDpMk.Controls.Add(this.rdbTanitsuDp);
			this.gpbSwkDpMk.Controls.Add(this.rdbFukugoDp);
			this.gpbSwkDpMk.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.gpbSwkDpMk.Location = new global::System.Drawing.Point(13, 199);
			this.gpbSwkDpMk.Name = "gpbSwkDpMk";
			this.gpbSwkDpMk.Size = new global::System.Drawing.Size(469, 98);
			this.gpbSwkDpMk.TabIndex = 4;
			this.gpbSwkDpMk.TabStop = false;
			this.gpbSwkDpMk.Text = "仕訳伝票作成";
			this.rdbSeikyuDp.AutoSize = true;
			this.rdbSeikyuDp.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.rdbSeikyuDp.Location = new global::System.Drawing.Point(21, 69);
			this.rdbSeikyuDp.Name = "rdbSeikyuDp";
			this.rdbSeikyuDp.Size = new global::System.Drawing.Size(203, 16);
			this.rdbSeikyuDp.TabIndex = 2;
			this.rdbSeikyuDp.TabStop = true;
			this.rdbSeikyuDp.Text = "請求先毎に仕訳伝票を作成する。";
			this.rdbSeikyuDp.UseVisualStyleBackColor = true;
			this.rdbTanitsuDp.AutoSize = true;
			this.rdbTanitsuDp.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.rdbTanitsuDp.Location = new global::System.Drawing.Point(21, 47);
			this.rdbTanitsuDp.Name = "rdbTanitsuDp";
			this.rdbTanitsuDp.Size = new global::System.Drawing.Size(275, 16);
			this.rdbTanitsuDp.TabIndex = 1;
			this.rdbTanitsuDp.TabStop = true;
			this.rdbTanitsuDp.Text = "１枚の仕訳伝票で、単一仕訳伝票を作成する。";
			this.rdbTanitsuDp.UseVisualStyleBackColor = true;
			this.rdbFukugoDp.AutoSize = true;
			this.rdbFukugoDp.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.rdbFukugoDp.Location = new global::System.Drawing.Point(21, 25);
			this.rdbFukugoDp.Name = "rdbFukugoDp";
			this.rdbFukugoDp.Size = new global::System.Drawing.Size(275, 16);
			this.rdbFukugoDp.TabIndex = 0;
			this.rdbFukugoDp.TabStop = true;
			this.rdbFukugoDp.Text = "１枚の仕訳伝票で、複合仕訳伝票を作成する。";
			this.rdbFukugoDp.UseVisualStyleBackColor = true;
			this.gpbSrkStBmn.Controls.Add(this.lblBumonNm);
			this.gpbSrkStBmn.Controls.Add(this.txtBumonCd);
			this.gpbSrkStBmn.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.gpbSrkStBmn.Location = new global::System.Drawing.Point(13, 303);
			this.gpbSrkStBmn.Name = "gpbSrkStBmn";
			this.gpbSrkStBmn.Size = new global::System.Drawing.Size(469, 63);
			this.gpbSrkStBmn.TabIndex = 5;
			this.gpbSrkStBmn.TabStop = false;
			this.gpbSrkStBmn.Text = "省略時設定部門";
			this.lblBumonNm.BackColor = global::System.Drawing.Color.Silver;
			this.lblBumonNm.BorderStyle = global::System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblBumonNm.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.lblBumonNm.Location = new global::System.Drawing.Point(57, 26);
			this.lblBumonNm.Name = "lblBumonNm";
			this.lblBumonNm.Size = new global::System.Drawing.Size(216, 20);
			this.lblBumonNm.TabIndex = 1;
			this.lblBumonNm.TextAlign = global::System.Drawing.ContentAlignment.MiddleLeft;
			this.txtBumonCd.AutoSizeFromLength = true;
			this.txtBumonCd.DisplayLength = null;
			this.txtBumonCd.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.txtBumonCd.ImeMode = global::System.Windows.Forms.ImeMode.Disable;
			this.txtBumonCd.Location = new global::System.Drawing.Point(21, 26);
			this.txtBumonCd.MaxLength = 4;
			this.txtBumonCd.Name = "txtBumonCd";
			this.txtBumonCd.Size = new global::System.Drawing.Size(34, 20);
			this.txtBumonCd.TabIndex = 0;
			this.txtBumonCd.TextAlign = global::System.Windows.Forms.HorizontalAlignment.Right;
			this.txtBumonCd.Validating += new global::System.ComponentModel.CancelEventHandler(this.txtBumonCd_Validating);
			this.groupBox1.Controls.Add(this.txtTekiyo);
			this.groupBox1.Controls.Add(this.txtTekiyoCd);
			this.groupBox1.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.groupBox1.Location = new global::System.Drawing.Point(13, 372);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new global::System.Drawing.Size(469, 60);
			this.groupBox1.TabIndex = 6;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "摘要";
			this.txtTekiyo.AutoSizeFromLength = true;
			this.txtTekiyo.DisplayLength = null;
			this.txtTekiyo.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.txtTekiyo.ImeMode = global::System.Windows.Forms.ImeMode.On;
			this.txtTekiyo.Location = new global::System.Drawing.Point(57, 23);
			this.txtTekiyo.MaxLength = 30;
			this.txtTekiyo.Name = "txtTekiyo";
			this.txtTekiyo.Size = new global::System.Drawing.Size(216, 20);
			this.txtTekiyo.TabIndex = 1;
			this.txtTekiyo.KeyDown += new global::System.Windows.Forms.KeyEventHandler(this.txtTekiyo_KeyDown);
			this.txtTekiyo.Validating += new global::System.ComponentModel.CancelEventHandler(this.txtTekiyo_Validating);
			this.txtTekiyoCd.AutoSizeFromLength = true;
			this.txtTekiyoCd.DisplayLength = null;
			this.txtTekiyoCd.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.txtTekiyoCd.ImeMode = global::System.Windows.Forms.ImeMode.Disable;
			this.txtTekiyoCd.Location = new global::System.Drawing.Point(21, 23);
			this.txtTekiyoCd.MaxLength = 4;
			this.txtTekiyoCd.Name = "txtTekiyoCd";
			this.txtTekiyoCd.Size = new global::System.Drawing.Size(34, 20);
			this.txtTekiyoCd.TabIndex = 0;
			this.txtTekiyoCd.TextAlign = global::System.Windows.Forms.HorizontalAlignment.Right;
			this.txtTekiyoCd.Validating += new global::System.ComponentModel.CancelEventHandler(this.txtTekiyoCd_Validating);
			base.AutoScaleDimensions = new global::System.Drawing.SizeF(6f, 12f);
			base.AutoScaleMode = global::System.Windows.Forms.AutoScaleMode.Font;
			base.ClientSize = new global::System.Drawing.Size(495, 491);
			base.Controls.Add(this.groupBox1);
			base.Controls.Add(this.gpbSrkStBmn);
			base.Controls.Add(this.gpbSwkDpMk);
			base.Controls.Add(this.gpbShimebi);
			base.Controls.Add(this.gpbGenkinKake);
			base.Controls.Add(this.gpbGenkin);
			base.ImeMode = global::System.Windows.Forms.ImeMode.KatakanaHalf;
			base.Name = "KBDB1015";
			base.ShowFButton = true;
			base.ShowTitle = false;
			this.Text = "仕訳データ作成 動作設定";
			base.Controls.SetChildIndex(this.pnlDebug, 0);
			base.Controls.SetChildIndex(this.lblTitle, 0);
			base.Controls.SetChildIndex(this.gpbGenkin, 0);
			base.Controls.SetChildIndex(this.gpbGenkinKake, 0);
			base.Controls.SetChildIndex(this.gpbShimebi, 0);
			base.Controls.SetChildIndex(this.gpbSwkDpMk, 0);
			base.Controls.SetChildIndex(this.gpbSrkStBmn, 0);
			base.Controls.SetChildIndex(this.groupBox1, 0);
			this.pnlDebug.ResumeLayout(false);
			this.gpbGenkin.ResumeLayout(false);
			this.gpbGenkin.PerformLayout();
			this.gpbGenkinKake.ResumeLayout(false);
			this.gpbGenkinKake.PerformLayout();
			this.gpbShimebi.ResumeLayout(false);
			this.gpbShimebi.PerformLayout();
			this.gpbSwkDpMk.ResumeLayout(false);
			this.gpbSwkDpMk.PerformLayout();
			this.gpbSrkStBmn.ResumeLayout(false);
			this.gpbSrkStBmn.PerformLayout();
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			base.ResumeLayout(false);
		}

		// Token: 0x04000057 RID: 87
		private global::System.ComponentModel.IContainer components;

		// Token: 0x04000058 RID: 88
		private global::System.Windows.Forms.GroupBox gpbGenkin;

		// Token: 0x04000059 RID: 89
		private global::System.Windows.Forms.GroupBox gpbGenkinKake;

		// Token: 0x0400005A RID: 90
		private global::System.Windows.Forms.GroupBox gpbShimebi;

		// Token: 0x0400005B RID: 91
		private global::System.Windows.Forms.GroupBox gpbSwkDpMk;

		// Token: 0x0400005C RID: 92
		private global::System.Windows.Forms.GroupBox gpbSrkStBmn;

		// Token: 0x0400005D RID: 93
		private global::System.Windows.Forms.GroupBox groupBox1;

		// Token: 0x0400005E RID: 94
		private global::System.Windows.Forms.CheckBox chkGenKakeToriDp;

		// Token: 0x0400005F RID: 95
		private global::System.Windows.Forms.CheckBox chkGenKakeHnDp;

		// Token: 0x04000060 RID: 96
		private global::System.Windows.Forms.CheckBox chkGenGenToriDp;

		// Token: 0x04000061 RID: 97
		private global::System.Windows.Forms.CheckBox chkGenGenHnDp;

		// Token: 0x04000062 RID: 98
		private global::System.Windows.Forms.CheckBox chkGenKakeGenHnDp;

		// Token: 0x04000063 RID: 99
		private global::System.Windows.Forms.CheckBox chkGenKakeGenToriDp;

		// Token: 0x04000064 RID: 100
		private global::System.Windows.Forms.CheckBox chkGenKakeKakeHnDp;

		// Token: 0x04000065 RID: 101
		private global::System.Windows.Forms.CheckBox chkGenKakeKakeToriDp;

		// Token: 0x04000066 RID: 102
		private global::System.Windows.Forms.CheckBox chkSmbGenHnDp;

		// Token: 0x04000067 RID: 103
		private global::System.Windows.Forms.CheckBox chkSmbGenToriDp;

		// Token: 0x04000068 RID: 104
		private global::System.Windows.Forms.CheckBox chkSmbKakeHnDp;

		// Token: 0x04000069 RID: 105
		private global::System.Windows.Forms.CheckBox chkSmbKakeToriDp;

		// Token: 0x0400006A RID: 106
		private global::System.Windows.Forms.RadioButton rdbFukugoDp;

		// Token: 0x0400006B RID: 107
		private global::System.Windows.Forms.RadioButton rdbTanitsuDp;

		// Token: 0x0400006C RID: 108
		private global::System.Windows.Forms.RadioButton rdbSeikyuDp;

		// Token: 0x0400006D RID: 109
		private global::jp.co.fsi.common.controls.FsiTextBox txtTekiyo;

		// Token: 0x0400006E RID: 110
		private global::jp.co.fsi.common.controls.FsiTextBox txtTekiyoCd;

		// Token: 0x0400006F RID: 111
		private global::System.Windows.Forms.Label lblBumonNm;

		// Token: 0x04000070 RID: 112
		private global::jp.co.fsi.common.controls.FsiTextBox txtBumonCd;
	}
}
