using System;
using System.Runtime.CompilerServices;

// Token: 0x0200000D RID: 13
[CompilerGenerated]
internal sealed class PrivateImplementationDetails
{
	// Token: 0x060000AC RID: 172 RVA: 0x00030668 File Offset: 0x0002E868
	internal static uint ComputeStringHash(string s)
	{
		uint num;
        if (s != null)
        {
            num = 2166136261u;
            for (int i = 0; i < s.Length; i++)
            {
                num = ((uint)s[i] ^ num) * 16777619u;
            }
            return num;
        }
        else
        {
            return default(uint);
        }
    }
}