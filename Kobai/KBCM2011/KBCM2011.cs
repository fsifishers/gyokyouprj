﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.kb.kbcm2011
{
    /// <summary>
    /// 棚番選択(KBCM2011)
    /// </summary>
    public partial class KBCM2011 : BasePgForm
    {
        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KBCM2011()
        {
            InitializeComponent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.InitForm();は呼び出さなくて構いません。
        /// また、このメソッド内の処理を外出しでこのクラス内にメソッド化するのは構いませんが、
        /// 原則、独自で起動時のイベント処理を実装することは禁じます。
        /// </remarks>
        protected override void InitForm()
        {
#if DEBUG
            string shishoCd = "1";
#else
            string shishoCd = this.Par2;
#endif
            // han.TB_商品からデータを取得して表示
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            DataTable dtTanaban = this.Dba.GetDataTableByConditionWithParams(
                "TANABAN AS 棚番", 
                "TB_HN_SHOHIN",
                "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND ISNULL(TANABAN, '') <> ''",
                "TANABAN ASC", 
                "TANABAN",
                dpc);

            this.dgvList.DataSource = dtTanaban;

            // ユーザーによるソートを禁止させる
            foreach (DataGridViewColumn c in this.dgvList.Columns)
                c.SortMode = DataGridViewColumnSortMode.NotSortable;

            // フォントを設定する
            this.dgvList.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F, FontStyle.Regular);
            this.dgvList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.dgvList.DefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F);

            // 列幅を設定する
            this.dgvList.Columns[0].Width = 135;

            // 引数で渡された元号と一致する行を初期選択する
            if (this.dgvList.Rows.Count > 0)
            {
                this.dgvList.Rows[0].Selected = true;
                if (!ValChk.IsEmpty(this.InData))
                {
                    for (int i = 0; i < this.dgvList.Rows.Count; i++)
                    {
                        if (Util.ToString(this.InData).Equals(
                            Util.ToString(this.dgvList.Rows[i].Cells["棚番"].Value)))
                        {
                            this.dgvList.Rows[i].Selected = true;
                            break;
                        }
                    }
                    this.dgvList.FirstDisplayedScrollingRowIndex = this.dgvList.SelectedCells[0].RowIndex;
                }
            }
            else
            {
                // データが無い時
                //this.btnEnter.Enabled = false;
                //this.dgvList.Enabled = false;
                MessageBox.Show("該当データがありません。", "棚番検索");
                if (!ValChk.IsEmpty(this.InData))
                {
                    this.OutData = new string[1] {Util.ToString(this.InData)};
                }
                else
                {
                    this.OutData = new string[1] { "" };
                }
                this.DialogResult = DialogResult.OK;
                this.Close();
            }

            // Gridに初期フォーカス
            this.dgvList.Focus();
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }
#endregion

#region イベント
        /// <summary>
        /// グリッドでのキーダウン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                ReturnVal();
            }
        }

        /// <summary>
        /// グリッドのセルダブルクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            ReturnVal();
        }

        /// <summary>
        /// Enterボタンクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEnter_Click(object sender, EventArgs e)
        {
            ReturnVal();
        }
#endregion

#region privateメソッド
        /// <summary>
        /// 呼び出し元に戻り値を返す
        /// </summary>
        private void ReturnVal()
        {
            this.OutData = new string[1] { 
                Util.ToString(this.dgvList.SelectedRows[0].Cells["棚番"].Value)
            };
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
#endregion
    }
}
