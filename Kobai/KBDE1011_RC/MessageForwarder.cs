﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace jp.co.fsi.kb.kbde1011
{
	// Token: 0x02000007 RID: 7
	public class MessageForwarder : NativeWindow, IMessageFilter
	{
		// Token: 0x06000044 RID: 68 RVA: 0x000022E9 File Offset: 0x000004E9
		public MessageForwarder(Control control, int message) : this(control, new int[]
		{
			message
		})
		{
		}

		// Token: 0x06000045 RID: 69 RVA: 0x0002293C File Offset: 0x00020B3C
		public MessageForwarder(Control control, IEnumerable<int> messages)
		{
			this._Control = control;
			base.AssignHandle(control.Handle);
			this._Messages = new HashSet<int>(messages);
			this._PreviousParent = control.Parent;
			this._IsMouseOverControl = false;
			control.ParentChanged += this.control_ParentChanged;
			control.MouseEnter += this.control_MouseEnter;
			control.MouseLeave += this.control_MouseLeave;
			control.Leave += this.control_Leave;
			if (control.Parent != null)
			{
				Application.AddMessageFilter(this);
			}
		}

		// Token: 0x06000046 RID: 70 RVA: 0x000229D8 File Offset: 0x00020BD8
		public bool PreFilterMessage(ref Message m)
		{
			if (this._Messages.Contains(m.Msg) && this._Control.CanFocus && !this._Control.Focused && this._IsMouseOverControl)
			{
				m.HWnd = this._Control.Handle;
				this.WndProc(ref m);
				return true;
			}
			return false;
		}

		// Token: 0x06000047 RID: 71 RVA: 0x000022FC File Offset: 0x000004FC
		private void control_ParentChanged(object sender, EventArgs e)
		{
			if (this._Control.Parent == null)
			{
				Application.RemoveMessageFilter(this);
			}
			else if (this._PreviousParent == null)
			{
				Application.AddMessageFilter(this);
			}
			this._PreviousParent = this._Control.Parent;
		}

		// Token: 0x06000048 RID: 72 RVA: 0x00002332 File Offset: 0x00000532
		private void control_MouseEnter(object sender, EventArgs e)
		{
			this._IsMouseOverControl = true;
		}

		// Token: 0x06000049 RID: 73 RVA: 0x0000233B File Offset: 0x0000053B
		private void control_MouseLeave(object sender, EventArgs e)
		{
			this._IsMouseOverControl = false;
		}

		// Token: 0x0600004A RID: 74 RVA: 0x0000233B File Offset: 0x0000053B
		private void control_Leave(object sender, EventArgs e)
		{
			this._IsMouseOverControl = false;
		}

		// Token: 0x0400027E RID: 638
		private Control _Control;

		// Token: 0x0400027F RID: 639
		private Control _PreviousParent;

		// Token: 0x04000280 RID: 640
		private HashSet<int> _Messages;

		// Token: 0x04000281 RID: 641
		private bool _IsMouseOverControl;
	}
}
