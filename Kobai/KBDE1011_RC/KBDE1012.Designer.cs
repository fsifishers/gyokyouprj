﻿namespace jp.co.fsi.kb.kbde1011
{
	// Token: 0x02000003 RID: 3
	public partial class KBDE1012 : global::jp.co.fsi.common.forms.BasePgForm
	{
		// Token: 0x0600002B RID: 43 RVA: 0x0000221F File Offset: 0x0000041F
		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		// Token: 0x0600002C RID: 44 RVA: 0x0000468C File Offset: 0x0000288C
		private void InitializeComponent()
		{
			global::System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle = new global::System.Windows.Forms.DataGridViewCellStyle();
			this.lblTantoshaNmFr = new global::System.Windows.Forms.Label();
			this.txtTantoshaCdFr = new global::jp.co.fsi.common.controls.FsiTextBox();
			this.lblTantoshaCd = new global::System.Windows.Forms.Label();
			this.lblFunanushiNmFr = new global::System.Windows.Forms.Label();
			this.txtFunanushiCdFr = new global::jp.co.fsi.common.controls.FsiTextBox();
			this.lblFunanushiCd = new global::System.Windows.Forms.Label();
			this.txtSearchCode = new global::jp.co.fsi.common.controls.FsiTextBox();
			this.lblSearchCode = new global::System.Windows.Forms.Label();
			this.lblDayFr = new global::System.Windows.Forms.Label();
			this.lblMonthFr = new global::System.Windows.Forms.Label();
			this.txtDayFr = new global::jp.co.fsi.common.controls.FsiTextBox();
			this.lblYearFr = new global::System.Windows.Forms.Label();
			this.txtMonthFr = new global::jp.co.fsi.common.controls.FsiTextBox();
			this.txtGengoYearFr = new global::jp.co.fsi.common.controls.FsiTextBox();
			this.lblGengoFr = new global::System.Windows.Forms.Label();
			this.lblEraBackFr = new global::System.Windows.Forms.Label();
			this.lblDenpyoDate = new global::System.Windows.Forms.Label();
			this.dgvList = new global::System.Windows.Forms.DataGridView();
			this.lblDateBet1 = new global::System.Windows.Forms.Label();
			this.lblDayTo = new global::System.Windows.Forms.Label();
			this.lblMonthTo = new global::System.Windows.Forms.Label();
			this.txtDayTo = new global::jp.co.fsi.common.controls.FsiTextBox();
			this.lblYearTo = new global::System.Windows.Forms.Label();
			this.txtMonthTo = new global::jp.co.fsi.common.controls.FsiTextBox();
			this.txtGengoYearTo = new global::jp.co.fsi.common.controls.FsiTextBox();
			this.lblGengoTo = new global::System.Windows.Forms.Label();
			this.label6 = new global::System.Windows.Forms.Label();
			this.lblDateBet2 = new global::System.Windows.Forms.Label();
			this.lblFunanushiNmTo = new global::System.Windows.Forms.Label();
			this.txtFunanushiCdTo = new global::jp.co.fsi.common.controls.FsiTextBox();
			this.lblDateBet3 = new global::System.Windows.Forms.Label();
			this.lblTantoshaNmTo = new global::System.Windows.Forms.Label();
			this.txtTantoshaCdTo = new global::jp.co.fsi.common.controls.FsiTextBox();
			this.btnEnter = new global::System.Windows.Forms.Button();
			this.lblShohinNmTo = new global::System.Windows.Forms.Label();
			this.txtShohinCdTo = new global::jp.co.fsi.common.controls.FsiTextBox();
			this.label2 = new global::System.Windows.Forms.Label();
			this.lblShohinNmFr = new global::System.Windows.Forms.Label();
			this.txtShohinCdFr = new global::jp.co.fsi.common.controls.FsiTextBox();
			this.lblShohinCd = new global::System.Windows.Forms.Label();
			this.pnlDebug.SuspendLayout();
			((global::System.ComponentModel.ISupportInitialize)this.dgvList).BeginInit();
			base.SuspendLayout();
			this.lblTitle.Size = new global::System.Drawing.Size(754, 23);
			this.lblTitle.Text = "仕入伝票検索";
			this.btnEsc.Location = new global::System.Drawing.Point(3, 49);
			this.btnF1.Location = new global::System.Drawing.Point(132, 49);
			this.btnF2.Visible = false;
			this.btnF3.Location = new global::System.Drawing.Point(196, 49);
			this.btnF3.Visible = false;
			this.btnF4.Location = new global::System.Drawing.Point(196, 49);
			this.btnF5.Location = new global::System.Drawing.Point(324, 49);
			this.btnF5.Visible = false;
			this.btnF7.Location = new global::System.Drawing.Point(452, 49);
			this.btnF7.Visible = false;
			this.btnF6.Location = new global::System.Drawing.Point(260, 49);
			this.btnF8.Location = new global::System.Drawing.Point(516, 49);
			this.btnF8.Visible = false;
			this.btnF9.Location = new global::System.Drawing.Point(580, 49);
			this.btnF9.Visible = false;
			this.btnF12.Location = new global::System.Drawing.Point(772, 49);
			this.btnF12.Visible = false;
			this.btnF11.Location = new global::System.Drawing.Point(708, 49);
			this.btnF11.Visible = false;
			this.btnF10.Location = new global::System.Drawing.Point(644, 49);
			this.btnF10.Visible = false;
			this.pnlDebug.Controls.Add(this.btnEnter);
			this.pnlDebug.Location = new global::System.Drawing.Point(5, 356);
			this.pnlDebug.Size = new global::System.Drawing.Size(762, 101);
			this.pnlDebug.Controls.SetChildIndex(this.btnF6, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF7, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF5, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF8, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF4, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF9, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF3, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF10, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF2, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF11, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF1, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF12, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnEsc, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnEnter, 0);
			this.lblTantoshaNmFr.BackColor = global::System.Drawing.Color.Silver;
			this.lblTantoshaNmFr.BorderStyle = global::System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblTantoshaNmFr.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.lblTantoshaNmFr.Location = new global::System.Drawing.Point(119, 58);
			this.lblTantoshaNmFr.Name = "lblTantoshaNmFr";
			this.lblTantoshaNmFr.Size = new global::System.Drawing.Size(210, 20);
			this.lblTantoshaNmFr.TabIndex = 26;
			this.lblTantoshaNmFr.Text = "先\u3000頭";
			this.lblTantoshaNmFr.TextAlign = global::System.Drawing.ContentAlignment.MiddleLeft;
			this.txtTantoshaCdFr.AutoSizeFromLength = true;
			this.txtTantoshaCdFr.DisplayLength = null;
			this.txtTantoshaCdFr.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.txtTantoshaCdFr.ImeMode = global::System.Windows.Forms.ImeMode.Disable;
			this.txtTantoshaCdFr.Location = new global::System.Drawing.Point(84, 58);
			this.txtTantoshaCdFr.MaxLength = 4;
			this.txtTantoshaCdFr.Name = "txtTantoshaCdFr";
			this.txtTantoshaCdFr.Size = new global::System.Drawing.Size(34, 20);
			this.txtTantoshaCdFr.TabIndex = 25;
			this.txtTantoshaCdFr.TextAlign = global::System.Windows.Forms.HorizontalAlignment.Right;
			this.txtTantoshaCdFr.Validating += new global::System.ComponentModel.CancelEventHandler(this.txtTantoshaCdFr_Validating);
			this.lblTantoshaCd.BackColor = global::System.Drawing.Color.Silver;
			this.lblTantoshaCd.BorderStyle = global::System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblTantoshaCd.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.lblTantoshaCd.Location = new global::System.Drawing.Point(12, 58);
			this.lblTantoshaCd.Name = "lblTantoshaCd";
			this.lblTantoshaCd.Size = new global::System.Drawing.Size(72, 20);
			this.lblTantoshaCd.TabIndex = 24;
			this.lblTantoshaCd.Text = "担 当 者";
			this.lblTantoshaCd.TextAlign = global::System.Drawing.ContentAlignment.MiddleLeft;
			this.lblFunanushiNmFr.BackColor = global::System.Drawing.Color.Silver;
			this.lblFunanushiNmFr.BorderStyle = global::System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblFunanushiNmFr.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.lblFunanushiNmFr.Location = new global::System.Drawing.Point(119, 38);
			this.lblFunanushiNmFr.Name = "lblFunanushiNmFr";
			this.lblFunanushiNmFr.Size = new global::System.Drawing.Size(210, 20);
			this.lblFunanushiNmFr.TabIndex = 20;
			this.lblFunanushiNmFr.Text = "先\u3000頭";
			this.lblFunanushiNmFr.TextAlign = global::System.Drawing.ContentAlignment.MiddleLeft;
			this.txtFunanushiCdFr.AutoSizeFromLength = true;
			this.txtFunanushiCdFr.DisplayLength = null;
			this.txtFunanushiCdFr.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.txtFunanushiCdFr.ImeMode = global::System.Windows.Forms.ImeMode.Disable;
			this.txtFunanushiCdFr.Location = new global::System.Drawing.Point(84, 38);
			this.txtFunanushiCdFr.MaxLength = 4;
			this.txtFunanushiCdFr.Name = "txtFunanushiCdFr";
			this.txtFunanushiCdFr.Size = new global::System.Drawing.Size(34, 20);
			this.txtFunanushiCdFr.TabIndex = 19;
			this.txtFunanushiCdFr.TextAlign = global::System.Windows.Forms.HorizontalAlignment.Right;
			this.txtFunanushiCdFr.Validating += new global::System.ComponentModel.CancelEventHandler(this.txtFunanushiCdFr_Validating);
			this.lblFunanushiCd.BackColor = global::System.Drawing.Color.Silver;
			this.lblFunanushiCd.BorderStyle = global::System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblFunanushiCd.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.lblFunanushiCd.Location = new global::System.Drawing.Point(12, 38);
			this.lblFunanushiCd.Name = "lblFunanushiCd";
			this.lblFunanushiCd.Size = new global::System.Drawing.Size(72, 20);
			this.lblFunanushiCd.TabIndex = 18;
			this.lblFunanushiCd.Text = "船主CD";
			this.lblFunanushiCd.TextAlign = global::System.Drawing.ContentAlignment.MiddleLeft;
			this.txtSearchCode.AutoSizeFromLength = true;
			this.txtSearchCode.DisplayLength = null;
			this.txtSearchCode.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.txtSearchCode.ImeMode = global::System.Windows.Forms.ImeMode.Disable;
			this.txtSearchCode.Location = new global::System.Drawing.Point(699, 58);
			this.txtSearchCode.MaxLength = 10;
			this.txtSearchCode.Name = "txtSearchCode";
			this.txtSearchCode.Size = new global::System.Drawing.Size(76, 20);
			this.txtSearchCode.TabIndex = 37;
			this.txtSearchCode.Validating += new global::System.ComponentModel.CancelEventHandler(this.txtSearchCode_Validating);
			this.lblSearchCode.BackColor = global::System.Drawing.Color.Silver;
			this.lblSearchCode.BorderStyle = global::System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblSearchCode.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.lblSearchCode.Location = new global::System.Drawing.Point(611, 58);
			this.lblSearchCode.Name = "lblSearchCode";
			this.lblSearchCode.Size = new global::System.Drawing.Size(88, 20);
			this.lblSearchCode.TabIndex = 36;
			this.lblSearchCode.Text = "検索コード";
			this.lblSearchCode.TextAlign = global::System.Drawing.ContentAlignment.MiddleLeft;
			this.lblDayFr.AutoSize = true;
			this.lblDayFr.BackColor = global::System.Drawing.Color.Silver;
			this.lblDayFr.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.lblDayFr.Location = new global::System.Drawing.Point(307, 19);
			this.lblDayFr.Name = "lblDayFr";
			this.lblDayFr.Size = new global::System.Drawing.Size(21, 13);
			this.lblDayFr.TabIndex = 8;
			this.lblDayFr.Text = "日";
			this.lblMonthFr.AutoSize = true;
			this.lblMonthFr.BackColor = global::System.Drawing.Color.Silver;
			this.lblMonthFr.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.lblMonthFr.Location = new global::System.Drawing.Point(240, 19);
			this.lblMonthFr.Name = "lblMonthFr";
			this.lblMonthFr.Size = new global::System.Drawing.Size(21, 13);
			this.lblMonthFr.TabIndex = 6;
			this.lblMonthFr.Text = "月";
			this.txtDayFr.AutoSizeFromLength = false;
			this.txtDayFr.DisplayLength = null;
			this.txtDayFr.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.txtDayFr.Location = new global::System.Drawing.Point(263, 15);
			this.txtDayFr.MaxLength = 2;
			this.txtDayFr.Name = "txtDayFr";
			this.txtDayFr.Size = new global::System.Drawing.Size(40, 20);
			this.txtDayFr.TabIndex = 7;
			this.txtDayFr.TextAlign = global::System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDayFr.Validating += new global::System.ComponentModel.CancelEventHandler(this.txtDayFr_Validating);
			this.lblYearFr.AutoSize = true;
			this.lblYearFr.BackColor = global::System.Drawing.Color.Silver;
			this.lblYearFr.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.lblYearFr.Location = new global::System.Drawing.Point(173, 19);
			this.lblYearFr.Name = "lblYearFr";
			this.lblYearFr.Size = new global::System.Drawing.Size(21, 13);
			this.lblYearFr.TabIndex = 4;
			this.lblYearFr.Text = "年";
			this.txtMonthFr.AutoSizeFromLength = false;
			this.txtMonthFr.DisplayLength = null;
			this.txtMonthFr.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.txtMonthFr.Location = new global::System.Drawing.Point(196, 15);
			this.txtMonthFr.MaxLength = 2;
			this.txtMonthFr.Name = "txtMonthFr";
			this.txtMonthFr.Size = new global::System.Drawing.Size(40, 20);
			this.txtMonthFr.TabIndex = 5;
			this.txtMonthFr.TextAlign = global::System.Windows.Forms.HorizontalAlignment.Right;
			this.txtMonthFr.Validating += new global::System.ComponentModel.CancelEventHandler(this.txtMonthFr_Validating);
			this.txtGengoYearFr.AutoSizeFromLength = false;
			this.txtGengoYearFr.DisplayLength = null;
			this.txtGengoYearFr.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.txtGengoYearFr.Location = new global::System.Drawing.Point(129, 15);
			this.txtGengoYearFr.MaxLength = 2;
			this.txtGengoYearFr.Name = "txtGengoYearFr";
			this.txtGengoYearFr.Size = new global::System.Drawing.Size(40, 20);
			this.txtGengoYearFr.TabIndex = 3;
			this.txtGengoYearFr.TextAlign = global::System.Windows.Forms.HorizontalAlignment.Right;
			this.txtGengoYearFr.Validating += new global::System.ComponentModel.CancelEventHandler(this.txtGengoYearFr_Validating);
			this.lblGengoFr.BackColor = global::System.Drawing.Color.Silver;
			this.lblGengoFr.BorderStyle = global::System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblGengoFr.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.lblGengoFr.Location = new global::System.Drawing.Point(86, 15);
			this.lblGengoFr.Name = "lblGengoFr";
			this.lblGengoFr.Size = new global::System.Drawing.Size(40, 20);
			this.lblGengoFr.TabIndex = 2;
			this.lblGengoFr.TextAlign = global::System.Drawing.ContentAlignment.MiddleRight;
			this.lblEraBackFr.BackColor = global::System.Drawing.Color.Silver;
			this.lblEraBackFr.Location = new global::System.Drawing.Point(83, 13);
			this.lblEraBackFr.Name = "lblEraBackFr";
			this.lblEraBackFr.Size = new global::System.Drawing.Size(245, 24);
			this.lblEraBackFr.TabIndex = 6;
			this.lblEraBackFr.Text = " ";
			this.lblDenpyoDate.BackColor = global::System.Drawing.Color.Silver;
			this.lblDenpyoDate.BorderStyle = global::System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblDenpyoDate.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.lblDenpyoDate.Location = new global::System.Drawing.Point(12, 14);
			this.lblDenpyoDate.Name = "lblDenpyoDate";
			this.lblDenpyoDate.Size = new global::System.Drawing.Size(72, 24);
			this.lblDenpyoDate.TabIndex = 0;
			this.lblDenpyoDate.Text = "伝票日付";
			this.lblDenpyoDate.TextAlign = global::System.Drawing.ContentAlignment.MiddleLeft;
			this.dgvList.AllowUserToAddRows = false;
			this.dgvList.AllowUserToDeleteRows = false;
			this.dgvList.AllowUserToResizeColumns = false;
			this.dgvList.AllowUserToResizeRows = false;
			this.dgvList.ColumnHeadersHeightSizeMode = global::System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			dataGridViewCellStyle.Alignment = global::System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle.BackColor = global::System.Drawing.SystemColors.Window;
			dataGridViewCellStyle.Font = new global::System.Drawing.Font("MS UI Gothic", 9f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			dataGridViewCellStyle.ForeColor = global::System.Drawing.SystemColors.ControlText;
			dataGridViewCellStyle.SelectionBackColor = global::System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle.SelectionForeColor = global::System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle.WrapMode = global::System.Windows.Forms.DataGridViewTriState.False;
			this.dgvList.DefaultCellStyle = dataGridViewCellStyle;
			this.dgvList.EditMode = global::System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
			this.dgvList.ImeMode = global::System.Windows.Forms.ImeMode.Disable;
			this.dgvList.Location = new global::System.Drawing.Point(12, 103);
			this.dgvList.MultiSelect = false;
			this.dgvList.Name = "dgvList";
			this.dgvList.RowHeadersVisible = false;
			this.dgvList.RowHeadersWidthSizeMode = global::System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.dgvList.RowTemplate.Height = 21;
			this.dgvList.ScrollBars = global::System.Windows.Forms.ScrollBars.Vertical;
			this.dgvList.SelectionMode = global::System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dgvList.Size = new global::System.Drawing.Size(760, 296);
			this.dgvList.TabIndex = 38;
			this.dgvList.CellDoubleClick += new global::System.Windows.Forms.DataGridViewCellEventHandler(this.dgvList_CellDoubleClick);
			this.dgvList.CellFormatting += new global::System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvList_CellFormatting);
			this.dgvList.Enter += new global::System.EventHandler(this.dgvList_Enter);
			this.dgvList.KeyDown += new global::System.Windows.Forms.KeyEventHandler(this.dgvList_KeyDown);
			this.lblDateBet1.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 12f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.lblDateBet1.Location = new global::System.Drawing.Point(334, 15);
			this.lblDateBet1.Name = "lblDateBet1";
			this.lblDateBet1.Size = new global::System.Drawing.Size(18, 20);
			this.lblDateBet1.TabIndex = 9;
			this.lblDateBet1.Text = "～";
			this.lblDateBet1.TextAlign = global::System.Drawing.ContentAlignment.MiddleLeft;
			this.lblDayTo.AutoSize = true;
			this.lblDayTo.BackColor = global::System.Drawing.Color.Silver;
			this.lblDayTo.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.lblDayTo.Location = new global::System.Drawing.Point(584, 18);
			this.lblDayTo.Name = "lblDayTo";
			this.lblDayTo.Size = new global::System.Drawing.Size(21, 13);
			this.lblDayTo.TabIndex = 17;
			this.lblDayTo.Text = "日";
			this.lblMonthTo.AutoSize = true;
			this.lblMonthTo.BackColor = global::System.Drawing.Color.Silver;
			this.lblMonthTo.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.lblMonthTo.Location = new global::System.Drawing.Point(517, 18);
			this.lblMonthTo.Name = "lblMonthTo";
			this.lblMonthTo.Size = new global::System.Drawing.Size(21, 13);
			this.lblMonthTo.TabIndex = 15;
			this.lblMonthTo.Text = "月";
			this.txtDayTo.AutoSizeFromLength = false;
			this.txtDayTo.DisplayLength = null;
			this.txtDayTo.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.txtDayTo.Location = new global::System.Drawing.Point(540, 14);
			this.txtDayTo.MaxLength = 2;
			this.txtDayTo.Name = "txtDayTo";
			this.txtDayTo.Size = new global::System.Drawing.Size(40, 20);
			this.txtDayTo.TabIndex = 16;
			this.txtDayTo.TextAlign = global::System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDayTo.Validating += new global::System.ComponentModel.CancelEventHandler(this.txtDayTo_Validating);
			this.lblYearTo.AutoSize = true;
			this.lblYearTo.BackColor = global::System.Drawing.Color.Silver;
			this.lblYearTo.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.lblYearTo.Location = new global::System.Drawing.Point(450, 18);
			this.lblYearTo.Name = "lblYearTo";
			this.lblYearTo.Size = new global::System.Drawing.Size(21, 13);
			this.lblYearTo.TabIndex = 13;
			this.lblYearTo.Text = "年";
			this.txtMonthTo.AutoSizeFromLength = false;
			this.txtMonthTo.DisplayLength = null;
			this.txtMonthTo.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.txtMonthTo.Location = new global::System.Drawing.Point(473, 14);
			this.txtMonthTo.MaxLength = 2;
			this.txtMonthTo.Name = "txtMonthTo";
			this.txtMonthTo.Size = new global::System.Drawing.Size(40, 20);
			this.txtMonthTo.TabIndex = 14;
			this.txtMonthTo.TextAlign = global::System.Windows.Forms.HorizontalAlignment.Right;
			this.txtMonthTo.Validating += new global::System.ComponentModel.CancelEventHandler(this.txtMonthTo_Validating);
			this.txtGengoYearTo.AutoSizeFromLength = false;
			this.txtGengoYearTo.DisplayLength = null;
			this.txtGengoYearTo.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.txtGengoYearTo.Location = new global::System.Drawing.Point(406, 14);
			this.txtGengoYearTo.MaxLength = 2;
			this.txtGengoYearTo.Name = "txtGengoYearTo";
			this.txtGengoYearTo.Size = new global::System.Drawing.Size(40, 20);
			this.txtGengoYearTo.TabIndex = 12;
			this.txtGengoYearTo.TextAlign = global::System.Windows.Forms.HorizontalAlignment.Right;
			this.txtGengoYearTo.Validating += new global::System.ComponentModel.CancelEventHandler(this.txtGengoYearTo_Validating);
			this.lblGengoTo.BackColor = global::System.Drawing.Color.Silver;
			this.lblGengoTo.BorderStyle = global::System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblGengoTo.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.lblGengoTo.Location = new global::System.Drawing.Point(363, 14);
			this.lblGengoTo.Name = "lblGengoTo";
			this.lblGengoTo.Size = new global::System.Drawing.Size(40, 20);
			this.lblGengoTo.TabIndex = 11;
			this.lblGengoTo.TextAlign = global::System.Drawing.ContentAlignment.MiddleRight;
			this.label6.BackColor = global::System.Drawing.Color.Silver;
			this.label6.Location = new global::System.Drawing.Point(360, 12);
			this.label6.Name = "label6";
			this.label6.Size = new global::System.Drawing.Size(245, 24);
			this.label6.TabIndex = 908;
			this.label6.Text = " ";
			this.lblDateBet2.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 12f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.lblDateBet2.Location = new global::System.Drawing.Point(334, 37);
			this.lblDateBet2.Name = "lblDateBet2";
			this.lblDateBet2.Size = new global::System.Drawing.Size(18, 20);
			this.lblDateBet2.TabIndex = 21;
			this.lblDateBet2.Text = "～";
			this.lblDateBet2.TextAlign = global::System.Drawing.ContentAlignment.MiddleLeft;
			this.lblFunanushiNmTo.BackColor = global::System.Drawing.Color.Silver;
			this.lblFunanushiNmTo.BorderStyle = global::System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblFunanushiNmTo.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.lblFunanushiNmTo.Location = new global::System.Drawing.Point(396, 38);
			this.lblFunanushiNmTo.Name = "lblFunanushiNmTo";
			this.lblFunanushiNmTo.Size = new global::System.Drawing.Size(211, 20);
			this.lblFunanushiNmTo.TabIndex = 23;
			this.lblFunanushiNmTo.Text = "最\u3000後";
			this.lblFunanushiNmTo.TextAlign = global::System.Drawing.ContentAlignment.MiddleLeft;
			this.txtFunanushiCdTo.AutoSizeFromLength = true;
			this.txtFunanushiCdTo.DisplayLength = null;
			this.txtFunanushiCdTo.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.txtFunanushiCdTo.ImeMode = global::System.Windows.Forms.ImeMode.Disable;
			this.txtFunanushiCdTo.Location = new global::System.Drawing.Point(361, 38);
			this.txtFunanushiCdTo.MaxLength = 4;
			this.txtFunanushiCdTo.Name = "txtFunanushiCdTo";
			this.txtFunanushiCdTo.Size = new global::System.Drawing.Size(34, 20);
			this.txtFunanushiCdTo.TabIndex = 22;
			this.txtFunanushiCdTo.TextAlign = global::System.Windows.Forms.HorizontalAlignment.Right;
			this.txtFunanushiCdTo.Validating += new global::System.ComponentModel.CancelEventHandler(this.txtFunanushiCdTo_Validating);
			this.lblDateBet3.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 12f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.lblDateBet3.Location = new global::System.Drawing.Point(334, 57);
			this.lblDateBet3.Name = "lblDateBet3";
			this.lblDateBet3.Size = new global::System.Drawing.Size(18, 20);
			this.lblDateBet3.TabIndex = 27;
			this.lblDateBet3.Text = "～";
			this.lblDateBet3.TextAlign = global::System.Drawing.ContentAlignment.MiddleLeft;
			this.lblTantoshaNmTo.BackColor = global::System.Drawing.Color.Silver;
			this.lblTantoshaNmTo.BorderStyle = global::System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblTantoshaNmTo.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.lblTantoshaNmTo.Location = new global::System.Drawing.Point(396, 58);
			this.lblTantoshaNmTo.Name = "lblTantoshaNmTo";
			this.lblTantoshaNmTo.Size = new global::System.Drawing.Size(211, 20);
			this.lblTantoshaNmTo.TabIndex = 29;
			this.lblTantoshaNmTo.Text = "最\u3000後";
			this.lblTantoshaNmTo.TextAlign = global::System.Drawing.ContentAlignment.MiddleLeft;
			this.txtTantoshaCdTo.AutoSizeFromLength = true;
			this.txtTantoshaCdTo.DisplayLength = null;
			this.txtTantoshaCdTo.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.txtTantoshaCdTo.ImeMode = global::System.Windows.Forms.ImeMode.Disable;
			this.txtTantoshaCdTo.Location = new global::System.Drawing.Point(361, 58);
			this.txtTantoshaCdTo.MaxLength = 4;
			this.txtTantoshaCdTo.Name = "txtTantoshaCdTo";
			this.txtTantoshaCdTo.Size = new global::System.Drawing.Size(34, 20);
			this.txtTantoshaCdTo.TabIndex = 28;
			this.txtTantoshaCdTo.TextAlign = global::System.Windows.Forms.HorizontalAlignment.Right;
			this.txtTantoshaCdTo.Validating += new global::System.ComponentModel.CancelEventHandler(this.txtTantoshaCdTo_Validating);
			this.btnEnter.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.btnEnter.Location = new global::System.Drawing.Point(67, 49);
			this.btnEnter.Name = "btnEnter";
			this.btnEnter.Size = new global::System.Drawing.Size(65, 45);
			this.btnEnter.TabIndex = 905;
			this.btnEnter.TabStop = false;
			this.btnEnter.Text = "Enter\r\n\r\n決定";
			this.btnEnter.TextAlign = global::System.Drawing.ContentAlignment.TopLeft;
			this.btnEnter.UseVisualStyleBackColor = true;
			this.btnEnter.Click += new global::System.EventHandler(this.btnEnter_Click);
			this.lblShohinNmTo.BackColor = global::System.Drawing.Color.Silver;
			this.lblShohinNmTo.BorderStyle = global::System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblShohinNmTo.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.lblShohinNmTo.Location = new global::System.Drawing.Point(512, 78);
			this.lblShohinNmTo.Name = "lblShohinNmTo";
			this.lblShohinNmTo.Size = new global::System.Drawing.Size(213, 20);
			this.lblShohinNmTo.TabIndex = 35;
			this.lblShohinNmTo.Text = "最\u3000後";
			this.lblShohinNmTo.TextAlign = global::System.Drawing.ContentAlignment.MiddleLeft;
			this.txtShohinCdTo.AutoSizeFromLength = true;
			this.txtShohinCdTo.DisplayLength = null;
			this.txtShohinCdTo.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.txtShohinCdTo.ImeMode = global::System.Windows.Forms.ImeMode.Disable;
			this.txtShohinCdTo.Location = new global::System.Drawing.Point(414, 78);
			this.txtShohinCdTo.MaxLength = 13;
			this.txtShohinCdTo.Name = "txtShohinCdTo";
			this.txtShohinCdTo.Size = new global::System.Drawing.Size(97, 20);
			this.txtShohinCdTo.TabIndex = 34;
			this.txtShohinCdTo.TextAlign = global::System.Windows.Forms.HorizontalAlignment.Right;
			this.txtShohinCdTo.Validating += new global::System.ComponentModel.CancelEventHandler(this.txtShohinCdTo_Validating);
			this.label2.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 12f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.label2.Location = new global::System.Drawing.Point(389, 79);
			this.label2.Name = "label2";
			this.label2.Size = new global::System.Drawing.Size(18, 20);
			this.label2.TabIndex = 33;
			this.label2.Text = "～";
			this.label2.TextAlign = global::System.Drawing.ContentAlignment.MiddleLeft;
			this.lblShohinNmFr.BackColor = global::System.Drawing.Color.Silver;
			this.lblShohinNmFr.BorderStyle = global::System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblShohinNmFr.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.lblShohinNmFr.Location = new global::System.Drawing.Point(181, 78);
			this.lblShohinNmFr.Name = "lblShohinNmFr";
			this.lblShohinNmFr.Size = new global::System.Drawing.Size(206, 20);
			this.lblShohinNmFr.TabIndex = 32;
			this.lblShohinNmFr.Text = "先\u3000頭";
			this.lblShohinNmFr.TextAlign = global::System.Drawing.ContentAlignment.MiddleLeft;
			this.txtShohinCdFr.AutoSizeFromLength = true;
			this.txtShohinCdFr.DisplayLength = null;
			this.txtShohinCdFr.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.txtShohinCdFr.ImeMode = global::System.Windows.Forms.ImeMode.Disable;
			this.txtShohinCdFr.Location = new global::System.Drawing.Point(84, 78);
			this.txtShohinCdFr.MaxLength = 13;
			this.txtShohinCdFr.Name = "txtShohinCdFr";
			this.txtShohinCdFr.Size = new global::System.Drawing.Size(97, 20);
			this.txtShohinCdFr.TabIndex = 31;
			this.txtShohinCdFr.TextAlign = global::System.Windows.Forms.HorizontalAlignment.Right;
			this.txtShohinCdFr.Validating += new global::System.ComponentModel.CancelEventHandler(this.txtShohinCdFr_Validating);
			this.lblShohinCd.BackColor = global::System.Drawing.Color.Silver;
			this.lblShohinCd.BorderStyle = global::System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblShohinCd.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.lblShohinCd.Location = new global::System.Drawing.Point(12, 78);
			this.lblShohinCd.Name = "lblShohinCd";
			this.lblShohinCd.Size = new global::System.Drawing.Size(72, 20);
			this.lblShohinCd.TabIndex = 30;
			this.lblShohinCd.Text = "商品CD";
			this.lblShohinCd.TextAlign = global::System.Drawing.ContentAlignment.MiddleLeft;
			base.AutoScaleDimensions = new global::System.Drawing.SizeF(6f, 12f);
			base.AutoScaleMode = global::System.Windows.Forms.AutoScaleMode.Font;
			base.ClientSize = new global::System.Drawing.Size(779, 460);
			base.Controls.Add(this.lblShohinNmTo);
			base.Controls.Add(this.txtShohinCdTo);
			base.Controls.Add(this.label2);
			base.Controls.Add(this.lblShohinNmFr);
			base.Controls.Add(this.txtShohinCdFr);
			base.Controls.Add(this.lblShohinCd);
			base.Controls.Add(this.lblTantoshaNmTo);
			base.Controls.Add(this.txtTantoshaCdTo);
			base.Controls.Add(this.lblDateBet3);
			base.Controls.Add(this.lblFunanushiNmTo);
			base.Controls.Add(this.txtFunanushiCdTo);
			base.Controls.Add(this.lblDateBet2);
			base.Controls.Add(this.lblDayTo);
			base.Controls.Add(this.lblMonthTo);
			base.Controls.Add(this.txtDayTo);
			base.Controls.Add(this.lblYearTo);
			base.Controls.Add(this.txtMonthTo);
			base.Controls.Add(this.txtGengoYearTo);
			base.Controls.Add(this.lblGengoTo);
			base.Controls.Add(this.label6);
			base.Controls.Add(this.lblDateBet1);
			base.Controls.Add(this.dgvList);
			base.Controls.Add(this.lblDenpyoDate);
			base.Controls.Add(this.lblDayFr);
			base.Controls.Add(this.lblMonthFr);
			base.Controls.Add(this.txtDayFr);
			base.Controls.Add(this.lblYearFr);
			base.Controls.Add(this.txtMonthFr);
			base.Controls.Add(this.txtGengoYearFr);
			base.Controls.Add(this.lblGengoFr);
			base.Controls.Add(this.lblEraBackFr);
			base.Controls.Add(this.txtSearchCode);
			base.Controls.Add(this.lblSearchCode);
			base.Controls.Add(this.lblFunanushiNmFr);
			base.Controls.Add(this.txtFunanushiCdFr);
			base.Controls.Add(this.lblFunanushiCd);
			base.Controls.Add(this.lblTantoshaNmFr);
			base.Controls.Add(this.txtTantoshaCdFr);
			base.Controls.Add(this.lblTantoshaCd);
			base.ImeMode = global::System.Windows.Forms.ImeMode.Disable;
			base.Name = "KBDE1012";
			base.ShowFButton = true;
			this.Text = "売上伝票検索";
			base.Controls.SetChildIndex(this.lblTitle, 0);
			base.Controls.SetChildIndex(this.pnlDebug, 0);
			base.Controls.SetChildIndex(this.lblTantoshaCd, 0);
			base.Controls.SetChildIndex(this.txtTantoshaCdFr, 0);
			base.Controls.SetChildIndex(this.lblTantoshaNmFr, 0);
			base.Controls.SetChildIndex(this.lblFunanushiCd, 0);
			base.Controls.SetChildIndex(this.txtFunanushiCdFr, 0);
			base.Controls.SetChildIndex(this.lblFunanushiNmFr, 0);
			base.Controls.SetChildIndex(this.lblSearchCode, 0);
			base.Controls.SetChildIndex(this.txtSearchCode, 0);
			base.Controls.SetChildIndex(this.lblEraBackFr, 0);
			base.Controls.SetChildIndex(this.lblGengoFr, 0);
			base.Controls.SetChildIndex(this.txtGengoYearFr, 0);
			base.Controls.SetChildIndex(this.txtMonthFr, 0);
			base.Controls.SetChildIndex(this.lblYearFr, 0);
			base.Controls.SetChildIndex(this.txtDayFr, 0);
			base.Controls.SetChildIndex(this.lblMonthFr, 0);
			base.Controls.SetChildIndex(this.lblDayFr, 0);
			base.Controls.SetChildIndex(this.lblDenpyoDate, 0);
			base.Controls.SetChildIndex(this.dgvList, 0);
			base.Controls.SetChildIndex(this.lblDateBet1, 0);
			base.Controls.SetChildIndex(this.label6, 0);
			base.Controls.SetChildIndex(this.lblGengoTo, 0);
			base.Controls.SetChildIndex(this.txtGengoYearTo, 0);
			base.Controls.SetChildIndex(this.txtMonthTo, 0);
			base.Controls.SetChildIndex(this.lblYearTo, 0);
			base.Controls.SetChildIndex(this.txtDayTo, 0);
			base.Controls.SetChildIndex(this.lblMonthTo, 0);
			base.Controls.SetChildIndex(this.lblDayTo, 0);
			base.Controls.SetChildIndex(this.lblDateBet2, 0);
			base.Controls.SetChildIndex(this.txtFunanushiCdTo, 0);
			base.Controls.SetChildIndex(this.lblFunanushiNmTo, 0);
			base.Controls.SetChildIndex(this.lblDateBet3, 0);
			base.Controls.SetChildIndex(this.txtTantoshaCdTo, 0);
			base.Controls.SetChildIndex(this.lblTantoshaNmTo, 0);
			base.Controls.SetChildIndex(this.lblShohinCd, 0);
			base.Controls.SetChildIndex(this.txtShohinCdFr, 0);
			base.Controls.SetChildIndex(this.lblShohinNmFr, 0);
			base.Controls.SetChildIndex(this.label2, 0);
			base.Controls.SetChildIndex(this.txtShohinCdTo, 0);
			base.Controls.SetChildIndex(this.lblShohinNmTo, 0);
			this.pnlDebug.ResumeLayout(false);
			((global::System.ComponentModel.ISupportInitialize)this.dgvList).EndInit();
			base.ResumeLayout(false);
			base.PerformLayout();
		}

		// Token: 0x04000005 RID: 5
		private global::System.ComponentModel.IContainer components;

		// Token: 0x04000006 RID: 6
		private global::System.Windows.Forms.Label lblTantoshaNmFr;

		// Token: 0x04000007 RID: 7
		private global::jp.co.fsi.common.controls.FsiTextBox txtTantoshaCdFr;

		// Token: 0x04000008 RID: 8
		private global::System.Windows.Forms.Label lblTantoshaCd;

		// Token: 0x04000009 RID: 9
		private global::System.Windows.Forms.Label lblFunanushiNmFr;

		// Token: 0x0400000A RID: 10
		private global::jp.co.fsi.common.controls.FsiTextBox txtFunanushiCdFr;

		// Token: 0x0400000B RID: 11
		private global::System.Windows.Forms.Label lblFunanushiCd;

		// Token: 0x0400000C RID: 12
		private global::jp.co.fsi.common.controls.FsiTextBox txtSearchCode;

		// Token: 0x0400000D RID: 13
		private global::System.Windows.Forms.Label lblSearchCode;

		// Token: 0x0400000E RID: 14
		private global::System.Windows.Forms.Label lblDayFr;

		// Token: 0x0400000F RID: 15
		private global::System.Windows.Forms.Label lblMonthFr;

		// Token: 0x04000010 RID: 16
		private global::jp.co.fsi.common.controls.FsiTextBox txtDayFr;

		// Token: 0x04000011 RID: 17
		private global::System.Windows.Forms.Label lblYearFr;

		// Token: 0x04000012 RID: 18
		private global::jp.co.fsi.common.controls.FsiTextBox txtMonthFr;

		// Token: 0x04000013 RID: 19
		private global::jp.co.fsi.common.controls.FsiTextBox txtGengoYearFr;

		// Token: 0x04000014 RID: 20
		private global::System.Windows.Forms.Label lblGengoFr;

		// Token: 0x04000015 RID: 21
		private global::System.Windows.Forms.Label lblEraBackFr;

		// Token: 0x04000016 RID: 22
		private global::System.Windows.Forms.Label lblDenpyoDate;

		// Token: 0x04000017 RID: 23
		private global::System.Windows.Forms.DataGridView dgvList;

		// Token: 0x04000018 RID: 24
		private global::System.Windows.Forms.Label lblDateBet1;

		// Token: 0x04000019 RID: 25
		private global::System.Windows.Forms.Label lblDayTo;

		// Token: 0x0400001A RID: 26
		private global::System.Windows.Forms.Label lblMonthTo;

		// Token: 0x0400001B RID: 27
		private global::jp.co.fsi.common.controls.FsiTextBox txtDayTo;

		// Token: 0x0400001C RID: 28
		private global::System.Windows.Forms.Label lblYearTo;

		// Token: 0x0400001D RID: 29
		private global::jp.co.fsi.common.controls.FsiTextBox txtMonthTo;

		// Token: 0x0400001E RID: 30
		private global::jp.co.fsi.common.controls.FsiTextBox txtGengoYearTo;

		// Token: 0x0400001F RID: 31
		private global::System.Windows.Forms.Label lblGengoTo;

		// Token: 0x04000020 RID: 32
		private global::System.Windows.Forms.Label label6;

		// Token: 0x04000021 RID: 33
		private global::System.Windows.Forms.Label lblDateBet2;

		// Token: 0x04000022 RID: 34
		private global::System.Windows.Forms.Label lblFunanushiNmTo;

		// Token: 0x04000023 RID: 35
		private global::jp.co.fsi.common.controls.FsiTextBox txtFunanushiCdTo;

		// Token: 0x04000024 RID: 36
		private global::System.Windows.Forms.Label lblDateBet3;

		// Token: 0x04000025 RID: 37
		private global::System.Windows.Forms.Label lblTantoshaNmTo;

		// Token: 0x04000026 RID: 38
		private global::jp.co.fsi.common.controls.FsiTextBox txtTantoshaCdTo;

		// Token: 0x04000027 RID: 39
		protected global::System.Windows.Forms.Button btnEnter;

		// Token: 0x04000028 RID: 40
		private global::System.Windows.Forms.Label lblShohinNmTo;

		// Token: 0x04000029 RID: 41
		private global::jp.co.fsi.common.controls.FsiTextBox txtShohinCdTo;

		// Token: 0x0400002A RID: 42
		private global::System.Windows.Forms.Label label2;

		// Token: 0x0400002B RID: 43
		private global::System.Windows.Forms.Label lblShohinNmFr;

		// Token: 0x0400002C RID: 44
		private global::jp.co.fsi.common.controls.FsiTextBox txtShohinCdFr;

		// Token: 0x0400002D RID: 45
		private global::System.Windows.Forms.Label lblShohinCd;
	}
}
