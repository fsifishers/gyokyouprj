﻿using System;

namespace jp.co.fsi.kb.kbde1011
{
	// Token: 0x02000009 RID: 9
	internal struct MeisaiCalcInfo
	{
		// Token: 0x0600004D RID: 77 RVA: 0x00022A38 File Offset: 0x00020C38
		public void Clear()
		{
			this.TANKA_SHUTOKU_HOHO = 0m;
			this.KINGAKU_HASU_SHORI = 1m;
			this.SHOHIZEI_NYURYOKU_HOHO = 2m;
			this.SHOHIZEI_HASU_SHORI = 2m;
			this.SHOHIZEI_TENKA_HOHO = 1m;
			this.BUMON_CD = 0;
			this.DENPYO_DATE = string.Empty;
			this.TOKUISAKI_CD = 0;
			this.SHOHIN_SHOHIZEI_KUBUN = string.Empty;
			this.KETA_SURYO2 = 2;
			this.KETA_URI_TANKA = 2;
		}

		// Token: 0x04000282 RID: 642
		public decimal TANKA_SHUTOKU_HOHO;

		// Token: 0x04000283 RID: 643
		public decimal KINGAKU_HASU_SHORI;

		// Token: 0x04000284 RID: 644
		public decimal SHOHIZEI_NYURYOKU_HOHO;

		// Token: 0x04000285 RID: 645
		public decimal SHOHIZEI_HASU_SHORI;

		// Token: 0x04000286 RID: 646
		public decimal SHOHIZEI_TENKA_HOHO;

		// Token: 0x04000287 RID: 647
		public int BUMON_CD;

		// Token: 0x04000288 RID: 648
		public int JIGYO_KUBUN;

		// Token: 0x04000289 RID: 649
		public string DENPYO_DATE;

		// Token: 0x0400028A RID: 650
		public int TOKUISAKI_CD;

		// Token: 0x0400028B RID: 651
		public string SHOHIN_SHOHIZEI_KUBUN;

		// Token: 0x0400028C RID: 652
		public int KETA_SURYO2;

		// Token: 0x0400028D RID: 653
		public int KETA_URI_TANKA;
	}
}
