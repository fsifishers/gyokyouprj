﻿using System;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.kb.kbde1011
{
	// Token: 0x02000008 RID: 8
	internal static class Program
	{
		// Token: 0x0600004B RID: 75 RVA: 0x00002344 File Offset: 0x00000544
		[STAThread]
		private static void Main()
		{
			Application.ThreadException += Program.Application_ThreadException;
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			Application.Run(new KBDE1011());
		}

		// Token: 0x0600004C RID: 76 RVA: 0x0000236C File Offset: 0x0000056C
		private static void Application_ThreadException(object sender, ThreadExceptionEventArgs e)
		{
			new Logger().Output(Path.GetFileName(Application.ExecutablePath), e.Exception);
			Msg.Error("エラーが発生しました。\n開発元までお問い合わせ下さい。");
			Application.Exit();
		}
	}
}
