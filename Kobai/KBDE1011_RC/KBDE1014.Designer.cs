﻿namespace jp.co.fsi.kb.kbde1011
{
	// Token: 0x02000006 RID: 6
	public partial class KBDE1014 : global::jp.co.fsi.common.forms.BasePgForm
	{
		// Token: 0x06000042 RID: 66 RVA: 0x000022CA File Offset: 0x000004CA
		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		// Token: 0x06000043 RID: 67 RVA: 0x00022138 File Offset: 0x00020338
		private void InitializeComponent()
		{
			global::System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle = new global::System.Windows.Forms.DataGridViewCellStyle();
			this.lblShohinNm = new global::System.Windows.Forms.Label();
			this.lblEraBackFr = new global::System.Windows.Forms.Label();
			this.dgvList = new global::System.Windows.Forms.DataGridView();
			this.btnEnter = new global::System.Windows.Forms.Button();
			this.pnlDebug.SuspendLayout();
			((global::System.ComponentModel.ISupportInitialize)this.dgvList).BeginInit();
			base.SuspendLayout();
			this.lblTitle.Location = new global::System.Drawing.Point(13, 7);
			this.lblTitle.Size = new global::System.Drawing.Size(241, 23);
			this.lblTitle.Text = "仕入伝票検索";
			this.btnEsc.Location = new global::System.Drawing.Point(3, 49);
			this.btnF1.Location = new global::System.Drawing.Point(132, 49);
			this.btnF1.Visible = false;
			this.btnF2.Location = new global::System.Drawing.Point(132, 49);
			this.btnF3.Location = new global::System.Drawing.Point(196, 49);
			this.btnF3.Visible = false;
			this.btnF4.Location = new global::System.Drawing.Point(196, 49);
			this.btnF4.Visible = false;
			this.btnF5.Location = new global::System.Drawing.Point(324, 49);
			this.btnF5.Visible = false;
			this.btnF7.Location = new global::System.Drawing.Point(452, 49);
			this.btnF7.Visible = false;
			this.btnF6.Location = new global::System.Drawing.Point(260, 49);
			this.btnF6.Visible = false;
			this.btnF8.Location = new global::System.Drawing.Point(516, 49);
			this.btnF8.Visible = false;
			this.btnF9.Location = new global::System.Drawing.Point(580, 49);
			this.btnF9.Visible = false;
			this.btnF12.Location = new global::System.Drawing.Point(772, 49);
			this.btnF12.Visible = false;
			this.btnF11.Location = new global::System.Drawing.Point(708, 49);
			this.btnF11.Visible = false;
			this.btnF10.Location = new global::System.Drawing.Point(644, 49);
			this.btnF10.Visible = false;
			this.pnlDebug.Controls.Add(this.btnEnter);
			this.pnlDebug.Location = new global::System.Drawing.Point(6, 191);
			this.pnlDebug.Size = new global::System.Drawing.Size(249, 100);
			this.pnlDebug.Controls.SetChildIndex(this.btnF6, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF7, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF5, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF8, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF4, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF9, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF3, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF10, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF2, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF11, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF1, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnF12, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnEsc, 0);
			this.pnlDebug.Controls.SetChildIndex(this.btnEnter, 0);
			this.lblShohinNm.BackColor = global::System.Drawing.Color.Silver;
			this.lblShohinNm.BorderStyle = global::System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblShohinNm.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9.75f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.lblShohinNm.Location = new global::System.Drawing.Point(9, 9);
			this.lblShohinNm.Name = "lblShohinNm";
			this.lblShohinNm.Size = new global::System.Drawing.Size(248, 20);
			this.lblShohinNm.TabIndex = 2;
			this.lblShohinNm.TextAlign = global::System.Drawing.ContentAlignment.MiddleLeft;
			this.lblEraBackFr.BackColor = global::System.Drawing.Color.Silver;
			this.lblEraBackFr.Location = new global::System.Drawing.Point(6, 7);
			this.lblEraBackFr.Name = "lblEraBackFr";
			this.lblEraBackFr.Size = new global::System.Drawing.Size(254, 24);
			this.lblEraBackFr.TabIndex = 6;
			this.lblEraBackFr.Text = " ";
			this.dgvList.AllowUserToAddRows = false;
			this.dgvList.AllowUserToDeleteRows = false;
			this.dgvList.AllowUserToResizeColumns = false;
			this.dgvList.AllowUserToResizeRows = false;
			this.dgvList.ColumnHeadersHeightSizeMode = global::System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			dataGridViewCellStyle.Alignment = global::System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle.BackColor = global::System.Drawing.SystemColors.Window;
			dataGridViewCellStyle.Font = new global::System.Drawing.Font("MS UI Gothic", 9f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			dataGridViewCellStyle.ForeColor = global::System.Drawing.SystemColors.ControlText;
			dataGridViewCellStyle.SelectionBackColor = global::System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle.SelectionForeColor = global::System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle.WrapMode = global::System.Windows.Forms.DataGridViewTriState.False;
			this.dgvList.DefaultCellStyle = dataGridViewCellStyle;
			this.dgvList.EditMode = global::System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
			this.dgvList.ImeMode = global::System.Windows.Forms.ImeMode.Disable;
			this.dgvList.Location = new global::System.Drawing.Point(6, 35);
			this.dgvList.MultiSelect = false;
			this.dgvList.Name = "dgvList";
			this.dgvList.RowHeadersVisible = false;
			this.dgvList.RowHeadersWidthSizeMode = global::System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.dgvList.RowTemplate.Height = 21;
			this.dgvList.ScrollBars = global::System.Windows.Forms.ScrollBars.Vertical;
			this.dgvList.SelectionMode = global::System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dgvList.Size = new global::System.Drawing.Size(253, 200);
			this.dgvList.TabIndex = 0;
			this.dgvList.CellDoubleClick += new global::System.Windows.Forms.DataGridViewCellEventHandler(this.dgvList_CellDoubleClick);
			this.dgvList.RowEnter += new global::System.Windows.Forms.DataGridViewCellEventHandler(this.dgvList_RowEnter);
			this.dgvList.KeyDown += new global::System.Windows.Forms.KeyEventHandler(this.dgvList_KeyDown);
			this.btnEnter.Font = new global::System.Drawing.Font("ＭＳ ゴシック", 9f, global::System.Drawing.FontStyle.Regular, global::System.Drawing.GraphicsUnit.Point, 128);
			this.btnEnter.Location = new global::System.Drawing.Point(67, 49);
			this.btnEnter.Name = "btnEnter";
			this.btnEnter.Size = new global::System.Drawing.Size(65, 45);
			this.btnEnter.TabIndex = 905;
			this.btnEnter.TabStop = false;
			this.btnEnter.Text = "Enter\r\n\r\n決定";
			this.btnEnter.TextAlign = global::System.Drawing.ContentAlignment.TopLeft;
			this.btnEnter.UseVisualStyleBackColor = true;
			this.btnEnter.Click += new global::System.EventHandler(this.btnEnter_Click);
			base.AutoScaleDimensions = new global::System.Drawing.SizeF(6f, 12f);
			base.AutoScaleMode = global::System.Windows.Forms.AutoScaleMode.Font;
			base.ClientSize = new global::System.Drawing.Size(266, 292);
			base.Controls.Add(this.dgvList);
			base.Controls.Add(this.lblShohinNm);
			base.Controls.Add(this.lblEraBackFr);
			base.ImeMode = global::System.Windows.Forms.ImeMode.Disable;
			base.Name = "KBDE1014";
			base.ShowFButton = true;
			this.Text = "単価参照";
			base.Controls.SetChildIndex(this.lblTitle, 0);
			base.Controls.SetChildIndex(this.pnlDebug, 0);
			base.Controls.SetChildIndex(this.lblEraBackFr, 0);
			base.Controls.SetChildIndex(this.lblShohinNm, 0);
			base.Controls.SetChildIndex(this.dgvList, 0);
			this.pnlDebug.ResumeLayout(false);
			((global::System.ComponentModel.ISupportInitialize)this.dgvList).EndInit();
			base.ResumeLayout(false);
		}

		// Token: 0x04000279 RID: 633
		private global::System.ComponentModel.IContainer components;

		// Token: 0x0400027A RID: 634
		private global::System.Windows.Forms.Label lblShohinNm;

		// Token: 0x0400027B RID: 635
		private global::System.Windows.Forms.Label lblEraBackFr;

		// Token: 0x0400027C RID: 636
		private global::System.Windows.Forms.DataGridView dgvList;

		// Token: 0x0400027D RID: 637
		protected global::System.Windows.Forms.Button btnEnter;
	}
}
