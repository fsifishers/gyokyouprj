﻿using System.Data;

using jp.co.fsi.common.report;

namespace jp.co.fsi.kb.kbdr2041
{
    /// <summary>
    /// KBDR2041R の概要の説明です。
    /// </summary>
    public partial class KBDR2041R : BaseReport
    {

        public KBDR2041R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }
    }
}
