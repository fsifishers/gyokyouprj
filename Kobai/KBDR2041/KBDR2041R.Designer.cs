﻿namespace jp.co.fsi.kb.kbdr2041
{
    /// <summary>
    /// KBDR2041R の概要の説明です。
    /// </summary>
    partial class KBDR2041R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(KBDR2041R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.Auto_Header0 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル44 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル45 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル46 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル47 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル48 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル49 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル51 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル52 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.直線53 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.テキスト56 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト58 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.直線59 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.rptDate = new GrapeCity.ActiveReports.SectionReportModel.ReportInfo();
            this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.テキスト60 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト61 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト64 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト65 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト66 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト67 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト68 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト69 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト70 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト71 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト72 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト73 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト74 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト75 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト76 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト77 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            ((System.ComponentModel.ISupportInitialize)(this.Auto_Header0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト56)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト58)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rptDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト60)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト61)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト64)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト65)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト66)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト67)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト68)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト69)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト70)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト71)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト72)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト73)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト74)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト75)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト76)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト77)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Auto_Header0,
            this.ラベル44,
            this.ラベル45,
            this.ラベル46,
            this.ラベル47,
            this.ラベル48,
            this.ラベル49,
            this.ラベル51,
            this.ラベル52,
            this.直線53,
            this.テキスト56,
            this.テキスト58,
            this.直線59,
            this.rptDate,
            this.txtPage,
            this.lblPage});
            this.pageHeader.Height = 1.698425F;
            this.pageHeader.Name = "pageHeader";
            // 
            // Auto_Header0
            // 
            this.Auto_Header0.Height = 0.2479167F;
            this.Auto_Header0.HyperLink = null;
            this.Auto_Header0.Left = 4.411112F;
            this.Auto_Header0.Name = "Auto_Header0";
            this.Auto_Header0.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 18pt; font-weight: bold; text-align:" +
    " center; ddo-char-set: 1";
            this.Auto_Header0.Tag = "";
            this.Auto_Header0.Text = "購買未払金一覧表";
            this.Auto_Header0.Top = 0.7F;
            this.Auto_Header0.Width = 2.219445F;
            // 
            // ラベル44
            // 
            this.ラベル44.Height = 0.1972222F;
            this.ラベル44.HyperLink = null;
            this.ラベル44.Left = 0.9456697F;
            this.ラベル44.Name = "ラベル44";
            this.ラベル44.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ラベル44.Tag = "";
            this.ラベル44.Text = "ｺｰﾄﾞ";
            this.ラベル44.Top = 1.499305F;
            this.ラベル44.Width = 0.3034722F;
            // 
            // ラベル45
            // 
            this.ラベル45.Height = 0.1979167F;
            this.ラベル45.HyperLink = null;
            this.ラベル45.Left = 1.29567F;
            this.ラベル45.Name = "ラベル45";
            this.ラベル45.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ラベル45.Tag = "";
            this.ラベル45.Text = "仕　入　先　名";
            this.ラベル45.Top = 1.499305F;
            this.ラベル45.Width = 2.602778F;
            // 
            // ラベル46
            // 
            this.ラベル46.Height = 0.1979167F;
            this.ラベル46.HyperLink = null;
            this.ラベル46.Left = 4.697753F;
            this.ラベル46.Name = "ラベル46";
            this.ラベル46.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ラベル46.Tag = "";
            this.ラベル46.Text = "前月繰越金額";
            this.ラベル46.Top = 1.499305F;
            this.ラベル46.Width = 0.8979167F;
            // 
            // ラベル47
            // 
            this.ラベル47.Height = 0.1979167F;
            this.ラベル47.HyperLink = null;
            this.ラベル47.Left = 5.847753F;
            this.ラベル47.Name = "ラベル47";
            this.ラベル47.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ラベル47.Tag = "";
            this.ラベル47.Text = "仕入金額";
            this.ラベル47.Top = 1.499305F;
            this.ラベル47.Width = 0.7479166F;
            // 
            // ラベル48
            // 
            this.ラベル48.Height = 0.1979167F;
            this.ラベル48.HyperLink = null;
            this.ラベル48.Left = 6.899142F;
            this.ラベル48.Name = "ラベル48";
            this.ラベル48.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ラベル48.Tag = "";
            this.ラベル48.Text = "消費税額";
            this.ラベル48.Top = 1.499305F;
            this.ラベル48.Width = 0.8506944F;
            // 
            // ラベル49
            // 
            this.ラベル49.Height = 0.1979167F;
            this.ラベル49.HyperLink = null;
            this.ラベル49.Left = 7.997058F;
            this.ラベル49.Name = "ラベル49";
            this.ラベル49.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ラベル49.Tag = "";
            this.ラベル49.Text = "税込金額";
            this.ラベル49.Top = 1.499305F;
            this.ラベル49.Width = 0.7520834F;
            // 
            // ラベル51
            // 
            this.ラベル51.Height = 0.1979167F;
            this.ラベル51.HyperLink = null;
            this.ラベル51.Left = 8.997728F;
            this.ラベル51.Name = "ラベル51";
            this.ラベル51.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ラベル51.Tag = "";
            this.ラベル51.Text = "支払金額";
            this.ラベル51.Top = 1.499305F;
            this.ラベル51.Width = 0.7993056F;
            // 
            // ラベル52
            // 
            this.ラベル52.Height = 0.1979167F;
            this.ラベル52.HyperLink = null;
            this.ラベル52.Left = 9.997728F;
            this.ラベル52.Name = "ラベル52";
            this.ラベル52.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ラベル52.Tag = "";
            this.ラベル52.Text = "支払残高";
            this.ラベル52.Top = 1.499305F;
            this.ラベル52.Width = 0.9013889F;
            // 
            // 直線53
            // 
            this.直線53.Height = 0F;
            this.直線53.Left = 4.344793F;
            this.直線53.LineWeight = 2F;
            this.直線53.Name = "直線53";
            this.直線53.Tag = "";
            this.直線53.Top = 0.9881945F;
            this.直線53.Width = 2.352083F;
            this.直線53.X1 = 4.344793F;
            this.直線53.X2 = 6.696876F;
            this.直線53.Y1 = 0.9881945F;
            this.直線53.Y2 = 0.9881945F;
            // 
            // テキスト56
            // 
            this.テキスト56.DataField = "ITEM01";
            this.テキスト56.Height = 0.2006944F;
            this.テキスト56.Left = 0.4523622F;
            this.テキスト56.Name = "テキスト56";
            this.テキスト56.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: normal; text-a" +
    "lign: left; ddo-char-set: 128";
            this.テキスト56.Tag = "";
            this.テキスト56.Text = "ITEM01";
            this.テキスト56.Top = 0.7F;
            this.テキスト56.Width = 3.754609F;
            // 
            // テキスト58
            // 
            this.テキスト58.DataField = "ITEM04";
            this.テキスト58.Height = 0.2604167F;
            this.テキスト58.Left = 3.735418F;
            this.テキスト58.Name = "テキスト58";
            this.テキスト58.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-weight: normal; text-alig" +
    "n: center; ddo-char-set: 1";
            this.テキスト58.Tag = "";
            this.テキスト58.Text = "ITEM04";
            this.テキスト58.Top = 1.05F;
            this.テキスト58.Width = 3.570833F;
            // 
            // 直線59
            // 
            this.直線59.Height = 0F;
            this.直線59.Left = 0.8713641F;
            this.直線59.LineWeight = 2F;
            this.直線59.Name = "直線59";
            this.直線59.Tag = "";
            this.直線59.Top = 1.686111F;
            this.直線59.Width = 10.09998F;
            this.直線59.X1 = 0.8713641F;
            this.直線59.X2 = 10.97134F;
            this.直線59.Y1 = 1.686111F;
            this.直線59.Y2 = 1.686111F;
            // 
            // rptDate
            // 
            this.rptDate.FormatString = "{RunDateTime:yyyy/MM/dd}";
            this.rptDate.Height = 0.1874016F;
            this.rptDate.Left = 9.672048F;
            this.rptDate.Name = "rptDate";
            this.rptDate.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; ddo-char-set: 1";
            this.rptDate.Top = 0.8007874F;
            this.rptDate.Width = 0.7811021F;
            // 
            // txtPage
            // 
            this.txtPage.Height = 0.1874836F;
            this.txtPage.Left = 10.52126F;
            this.txtPage.Name = "txtPage";
            this.txtPage.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtPage.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtPage.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.txtPage.Text = "Page";
            this.txtPage.Top = 0.8007874F;
            this.txtPage.Width = 0.2834645F;
            // 
            // lblPage
            // 
            this.lblPage.Height = 0.1874836F;
            this.lblPage.HyperLink = null;
            this.lblPage.Left = 10.80484F;
            this.lblPage.Name = "lblPage";
            this.lblPage.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; ddo-char-set: 1";
            this.lblPage.Text = "頁";
            this.lblPage.Top = 0.8007874F;
            this.lblPage.Width = 0.1666665F;
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.テキスト60,
            this.テキスト61,
            this.テキスト64,
            this.テキスト65,
            this.テキスト66,
            this.テキスト67,
            this.テキスト68,
            this.テキスト69,
            this.テキスト70,
            this.テキスト71,
            this.テキスト72,
            this.テキスト73,
            this.テキスト74,
            this.テキスト75,
            this.テキスト76,
            this.テキスト77});
            this.detail.Height = 0.3922572F;
            this.detail.Name = "detail";
            // 
            // テキスト60
            // 
            this.テキスト60.DataField = "ITEM05";
            this.テキスト60.Height = 0.2006944F;
            this.テキスト60.Left = 0.9031497F;
            this.テキスト60.Name = "テキスト60";
            this.テキスト60.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 1";
            this.テキスト60.Tag = "";
            this.テキスト60.Text = "ITEM05";
            this.テキスト60.Top = 0.05511811F;
            this.テキスト60.Width = 0.2993056F;
            // 
            // テキスト61
            // 
            this.テキスト61.DataField = "ITEM06";
            this.テキスト61.Height = 0.2006944F;
            this.テキスト61.Left = 1.301066F;
            this.テキスト61.Name = "テキスト61";
            this.テキスト61.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-weight: normal; text-alig" +
    "n: left; ddo-char-set: 1";
            this.テキスト61.Tag = "";
            this.テキスト61.Text = "ITEM06";
            this.テキスト61.Top = 0.05511811F;
            this.テキスト61.Width = 2.602778F;
            // 
            // テキスト64
            // 
            this.テキスト64.DataField = "ITEM08";
            this.テキスト64.Height = 0.2006944F;
            this.テキスト64.Left = 4.652436F;
            this.テキスト64.Name = "テキスト64";
            this.テキスト64.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 1";
            this.テキスト64.Tag = "";
            this.テキスト64.Text = "ITEM08";
            this.テキスト64.Top = 0.05511811F;
            this.テキスト64.Width = 0.9493055F;
            // 
            // テキスト65
            // 
            this.テキスト65.DataField = "ITEM09";
            this.テキスト65.Height = 0.2006944F;
            this.テキスト65.Left = 5.703825F;
            this.テキスト65.Name = "テキスト65";
            this.テキスト65.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 1";
            this.テキスト65.Tag = "";
            this.テキスト65.Text = "ITEM09";
            this.テキスト65.Top = 0.05511811F;
            this.テキスト65.Width = 1.000694F;
            // 
            // テキスト66
            // 
            this.テキスト66.DataField = "ITEM10";
            this.テキスト66.Height = 0.2006944F;
            this.テキスト66.Left = 6.801742F;
            this.テキスト66.Name = "テキスト66";
            this.テキスト66.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 1";
            this.テキスト66.Tag = "";
            this.テキスト66.Text = "ITEM10";
            this.テキスト66.Top = 0.05511811F;
            this.テキスト66.Width = 0.9527778F;
            // 
            // テキスト67
            // 
            this.テキスト67.DataField = "ITEM11";
            this.テキスト67.Height = 0.2006944F;
            this.テキスト67.Left = 7.844797F;
            this.テキスト67.Name = "テキスト67";
            this.テキスト67.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 1";
            this.テキスト67.Tag = "";
            this.テキスト67.Text = "ITEM11";
            this.テキスト67.Top = 0.05511811F;
            this.テキスト67.Width = 0.9493055F;
            // 
            // テキスト68
            // 
            this.テキスト68.DataField = "ITEM12";
            this.テキスト68.Height = 0.2006944F;
            this.テキスト68.Left = 8.904515F;
            this.テキスト68.Name = "テキスト68";
            this.テキスト68.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 1";
            this.テキスト68.Tag = "";
            this.テキスト68.Text = "ITEM12";
            this.テキスト68.Top = 0.05511811F;
            this.テキスト68.Width = 0.9493055F;
            // 
            // テキスト69
            // 
            this.テキスト69.DataField = "ITEM13";
            this.テキスト69.Height = 0.2006944F;
            this.テキスト69.Left = 9.951737F;
            this.テキスト69.Name = "テキスト69";
            this.テキスト69.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 1";
            this.テキスト69.Tag = "";
            this.テキスト69.Text = "ITEM13";
            this.テキスト69.Top = 0.05511811F;
            this.テキスト69.Width = 0.9527778F;
            // 
            // テキスト70
            // 
            this.テキスト70.DataField = "ITEM15";
            this.テキスト70.Height = 0.2006944F;
            this.テキスト70.Left = 4.652436F;
            this.テキスト70.Name = "テキスト70";
            this.テキスト70.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 1";
            this.テキスト70.Tag = "";
            this.テキスト70.Text = "ITEM15";
            this.テキスト70.Top = 0.251646F;
            this.テキスト70.Width = 0.9493055F;
            // 
            // テキスト71
            // 
            this.テキスト71.DataField = "ITEM16";
            this.テキスト71.Height = 0.2006944F;
            this.テキスト71.Left = 5.704519F;
            this.テキスト71.Name = "テキスト71";
            this.テキスト71.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 1";
            this.テキスト71.Tag = "";
            this.テキスト71.Text = "ITEM16";
            this.テキスト71.Top = 0.251646F;
            this.テキスト71.Width = 1.000694F;
            // 
            // テキスト72
            // 
            this.テキスト72.DataField = "ITEM17";
            this.テキスト72.Height = 0.2006944F;
            this.テキスト72.Left = 6.801742F;
            this.テキスト72.Name = "テキスト72";
            this.テキスト72.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 1";
            this.テキスト72.Tag = "";
            this.テキスト72.Text = "ITEM17";
            this.テキスト72.Top = 0.251646F;
            this.テキスト72.Width = 0.9527778F;
            // 
            // テキスト73
            // 
            this.テキスト73.DataField = "ITEM18";
            this.テキスト73.Height = 0.2006944F;
            this.テキスト73.Left = 7.844797F;
            this.テキスト73.Name = "テキスト73";
            this.テキスト73.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 1";
            this.テキスト73.Tag = "";
            this.テキスト73.Text = "ITEM18";
            this.テキスト73.Top = 0.251646F;
            this.テキスト73.Width = 0.9493055F;
            // 
            // テキスト74
            // 
            this.テキスト74.DataField = "ITEM19";
            this.テキスト74.Height = 0.2006944F;
            this.テキスト74.Left = 8.904515F;
            this.テキスト74.Name = "テキスト74";
            this.テキスト74.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 1";
            this.テキスト74.Tag = "";
            this.テキスト74.Text = "ITEM19";
            this.テキスト74.Top = 0.251646F;
            this.テキスト74.Width = 0.9493055F;
            // 
            // テキスト75
            // 
            this.テキスト75.DataField = "ITEM20";
            this.テキスト75.Height = 0.2006944F;
            this.テキスト75.Left = 9.951737F;
            this.テキスト75.Name = "テキスト75";
            this.テキスト75.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 1";
            this.テキスト75.Tag = "";
            this.テキスト75.Text = "ITEM20";
            this.テキスト75.Top = 0.2474791F;
            this.テキスト75.Width = 0.9527778F;
            // 
            // テキスト76
            // 
            this.テキスト76.DataField = "ITEM07";
            this.テキスト76.Height = 0.2006944F;
            this.テキスト76.Left = 3.93926F;
            this.テキスト76.Name = "テキスト76";
            this.テキスト76.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.テキスト76.Tag = "";
            this.テキスト76.Text = "ITEM07";
            this.テキスト76.Top = 0.05511811F;
            this.テキスト76.Width = 0.6027778F;
            // 
            // テキスト77
            // 
            this.テキスト77.DataField = "ITEM14";
            this.テキスト77.Height = 0.2006944F;
            this.テキスト77.Left = 3.93926F;
            this.テキスト77.Name = "テキスト77";
            this.テキスト77.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.テキスト77.Tag = "";
            this.テキスト77.Text = "ITEM14";
            this.テキスト77.Top = 0.2474791F;
            this.テキスト77.Width = 0.6027778F;
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            // 
            // KBDR2041R
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0F;
            this.PageSettings.Margins.Left = 0F;
            this.PageSettings.Margins.Right = 0F;
            this.PageSettings.Margins.Top = 0F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
            this.PageSettings.PaperHeight = 11.69291F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.267716F;
            this.PrintWidth = 11.04167F;
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.pageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.Auto_Header0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト56)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト58)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rptDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト60)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト61)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト64)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト65)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト66)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト67)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト68)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト69)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト70)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト71)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト72)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト73)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト74)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト75)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト76)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト77)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.Label Auto_Header0;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル44;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル45;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル46;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル47;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル48;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル49;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル51;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル52;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線53;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト56;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト58;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線59;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト60;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト61;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト64;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト65;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト66;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト67;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト68;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト69;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト70;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト71;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト72;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト73;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト74;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト75;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト76;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト77;
        private GrapeCity.ActiveReports.SectionReportModel.ReportInfo rptDate;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
    }
}
