﻿using System;
using System.ComponentModel;
using System.Data;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

using GrapeCity.ActiveReports;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.constants;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.kb.kbdr2041
{
    #region 構造体
    /// <summary>
    /// 印刷ワークテーブルのデータ構造体
    /// </summary>
    struct NumVals
    {
        public decimal KISHUZAN;
        public decimal SHIIRE_KINGAKU;
        public decimal SYOUHIZEI_GAKU;
        public decimal SHIHARAI_KINGAKU;
        public decimal HENPIN_KINGAKU;

        public void Clear()
        {
            KISHUZAN = 0;
            SHIIRE_KINGAKU = 0;
            SYOUHIZEI_GAKU = 0;
            SHIHARAI_KINGAKU = 0;
            HENPIN_KINGAKU = 0;
        }
    }
    #endregion

    /// <summary>
    /// 購買未払金一覧(KBDR2041)
    /// </summary>
    public partial class KBDR2041 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// 印刷ワーク更新用列数
        /// </summary>
        private const int prtCols = 20;
        #endregion

        #region プロパティ
        /// <summary>
        /// 画面上最後となるフォーカスのEnterボタン押下時処理用変数
        /// </summary>
        private bool _dtFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._dtFlg;
            }
        }

        // 元帳区分設定変数
        private decimal MOTOCHO_KUBUN = 2;
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KBDR2041()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.InitForm();は呼び出さなくて構いません。
        /// また、このメソッド内の処理を外出しでこのクラス内にメソッド化するのは構いませんが、
        /// 原則、独自で起動時のイベント処理を実装することは禁じます。
        /// </remarks>
        protected override void InitForm()
        {
            // 水揚支所
#if DEBUG
            this.txtMizuageShishoCd.Text = "1";
#else
            this.txtMizuageShishoCd.Text = Uinfo.shishoCd;
#endif
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
            this.txtMizuageShishoCd.Enabled = (this.txtMizuageShishoCd.Text == "1") ? true : false;

            // 日付範囲の和暦設定
            string[] jpDate = Util.ConvJpDate(DateTime.Now, this.Dba);
            // 開始
            lblDateGengoFr.Text     = jpDate[0];
            txtDateYearFr.Text = jpDate[2];
            txtDateMonthFr.Text     = jpDate[3];
            txtDateDayFr.Text       = "1";
            // 終了
            lblDateGengoTo.Text = jpDate[0];
            txtDateYearTo.Text = jpDate[2];
            txtDateMonthTo.Text = jpDate[3];
            txtDateDayTo.Text = jpDate[4];

            // 元帳区分取得
            MOTOCHO_KUBUN = (Util.ToDecimal(this.Par1) == 0) ? MOTOCHO_KUBUN : Util.ToDecimal(this.Par1);

            // 初期フォーカス
            this.ActiveControl = txtDateYearFr;
            // パネル非表示
            //this.pnlDebug.Visible = false;
        }
        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 元号年と取引先コードに
            // フォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd":
                case "txtDateYearFr":
                case "txtDateYearTo":
                case "txtTorihikisakiCdFr":
                case "txtTorihikisakiCdTo":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            //MEMO:(参考)Escキー押下時は画面を閉じる処理が基盤側で実装されていますが、
            //PressEsc()をオーバーライドすることでプログラム個別に実装することも可能です。
            System.Reflection.Assembly asm;
            Type t;
            String[] result;
            string stCurrentDir = System.IO.Directory.GetCurrentDirectory();

            //MEMO:現状アクティブなコントロールごとに処理を実装してください。
            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd": // 水揚支所
                    #region 水揚支所
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM2031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2031.CMCM2031");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.InData = this.txtMizuageShishoCd.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.txtMizuageShishoCd.Text = result[0];
                                this.lblMizuageShishoNm.Text = result[1];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtTorihikisakiCdFr":
                case "txtTorihikisakiCdTo":
                    #region 仕入先CD
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"KBCM1011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.kb.kbcm1011.KBCM1011");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                if (this.ActiveCtlNm == "txtTorihikisakiCdFr")
                                {
                                    this.txtTorihikisakiCdFr.Text = outData[0];
                                    this.lblTorihikisakiNmFr.Text = outData[1];
                                }
                                else if (this.ActiveCtlNm == "txtTorihikisakiCdTo")
                                {
                                    this.txtTorihikisakiCdTo.Text = outData[0];
                                    this.lblTorihikisakiNmTo.Text = outData[1];
                                }

                            }
                        }
                    }
                    #endregion
                    break;

                case "txtDateYearFr":
                    #region 元号
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblDateGengoFr.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (string[])frm.OutData;
                                this.lblDateGengoFr.Text = result[1];

                                // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
                                DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                                    this.txtDateMonthFr.Text, "1", this.Dba);
                                int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);
                                if (Util.ToInt(this.txtDateDayFr.Text) > lastDayInMonth)
                                {
                                    this.txtDateDayFr.Text = Util.ToString(lastDayInMonth);
                                }

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblDateGengoFr.Text,
                                        this.txtDateYearFr.Text,
                                        this.txtDateMonthFr.Text,
                                        this.txtDateDayFr.Text,
                                        this.Dba);
                                this.lblDateGengoFr.Text = arrJpDate[0];
                                this.txtDateYearFr.Text = arrJpDate[2];
                                this.txtDateMonthFr.Text = arrJpDate[3];
                                this.txtDateDayFr.Text = arrJpDate[4];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtDateYearTo":
                    #region 元号
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblDateGengoTo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (string[])frm.OutData;
                                this.lblDateGengoTo.Text = result[1];

                                // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
                                DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                                    this.txtDateMonthTo.Text, "1", this.Dba);
                                int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);
                                if (Util.ToInt(this.txtDateDayTo.Text) > lastDayInMonth)
                                {
                                    this.txtDateDayTo.Text = Util.ToString(lastDayInMonth);
                                }

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblDateGengoTo.Text,
                                        this.txtDateYearTo.Text,
                                        this.txtDateMonthTo.Text,
                                        this.txtDateDayTo.Text,
                                        this.Dba);
                                this.lblDateGengoTo.Text = arrJpDate[0];
                                this.txtDateYearTo.Text = arrJpDate[2];
                                this.txtDateMonthTo.Text = arrJpDate[3];
                                this.txtDateDayTo.Text = arrJpDate[4];
                            }
                        }
                    }
                    #endregion
                    break;
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
            {
                // プレビュー処理
                DoPrint(true);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("印刷", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }

        /// <summary>
        /// F6キー押下時処理
        /// PDF出力
        /// </summary>
        public override void PressF6()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("PDF出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, true);
            }
        }

        /// <summary>
        /// F7キー押下時処理
        /// EXCEL出力
        /// </summary>
        public override void PressF7()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("EXCEL出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, false, true);
            }
        }

        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            // 設定画面の起動
            // MEMO:原則としてここで渡す帳票IDの設定はReport.csvに保持していることが前提ですが、
            // 保持していない場合は、設定画面での保存(F6)時に新規に設定が保持されます。
            PrintSettingForm psForm = new PrintSettingForm(new string[1] { "KBDR2041R" });
            psForm.ShowDialog();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 水揚支所入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMizuageShishoCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                e.Cancel = true;
                this.txtMizuageShishoCd.SelectAll();
                this.txtMizuageShishoCd.Focus();
            }
        }

        /// <summary>
        /// コード(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTorihikisakiCdFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidTorihikisakiCdFr())
            {
                e.Cancel = true;
                this.txtTorihikisakiCdFr.SelectAll();
            }
        }

        /// <summary>
        /// コード(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTorihikisakiCdTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidTorihikisakiCdTo())
            {
                e.Cancel = true;
                this.txtTorihikisakiCdTo.SelectAll();

                // Enter処理を無効化
                this._dtFlg = false;
            }
            else
            {
                // Enter処理を有効化
                this._dtFlg = true;
            }
        }

        /// <summary>
        /// 和暦(年)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateYearFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtDateYearFr.Text, this.txtDateYearFr.MaxLength))
            {
                e.Cancel = true;
                this.txtDateYearFr.SelectAll();
            }
            else
            {
                this.txtDateYearFr.Text = Util.ToString(IsValid.SetYear(this.txtDateYearFr.Text));
                CheckDateFr();
                SetDateFr();
            }
        }

        /// <summary>
        /// 和暦(月)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateMonthFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtDateMonthFr.Text, this.txtDateMonthFr.MaxLength))
            {
                e.Cancel = true;
                this.txtDateMonthFr.SelectAll();
            }
            else
            {
                this.txtDateMonthFr.Text = Util.ToString(IsValid.SetMonth(this.txtDateMonthFr.Text));
                CheckDateFr();
                SetDateFr();
            }
        }

        /// <summary>
        /// 和暦(日)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateDayFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsDay(this.txtDateDayFr.Text, this.txtDateDayFr.MaxLength))
            {
                e.Cancel = true;
                this.txtDateDayFr.SelectAll();
            }
            else
            {
                this.txtDateDayFr.Text = Util.ToString(IsValid.SetDay(this.txtDateDayFr.Text));
                CheckDateFr();
                SetDateFr();
            }
        }

        /// <summary>
        /// 和暦(年)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateYearTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtDateYearTo.Text, this.txtDateYearTo.MaxLength))
            {
                e.Cancel = true;
                this.txtDateYearTo.SelectAll();
            }
            else
            {
                this.txtDateYearTo.Text = Util.ToString(IsValid.SetYear(this.txtDateYearTo.Text));
                CheckDateTo();
                SetDateTo();
            }
        }

        /// <summary>
        /// 和暦(月)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateMonthTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtDateMonthTo.Text, this.txtDateMonthTo.MaxLength))
            {
                e.Cancel = true;
                this.txtDateMonthTo.SelectAll();
            }
            else
            {
                this.txtDateMonthTo.Text = Util.ToString(IsValid.SetMonth(this.txtDateMonthTo.Text));
                CheckDateTo();
                SetDateTo();
            }
        }

        /// <summary>
        /// 和暦(日)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateDayTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsDay(this.txtDateDayTo.Text, this.txtDateDayTo.MaxLength))
            {
                e.Cancel = true;
                this.txtDateDayTo.SelectAll();
            }
            else
            {
                this.txtDateDayTo.Text = Util.ToString(IsValid.SetDay(this.txtDateDayTo.Text));
                CheckDateTo();
                SetDateTo();
            }
        }

        /// <summary>
        /// 取引先CD(至)のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTorihikisakiCdTo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.Flg)
            {
                // Enter処理を無効化
                this._dtFlg = false;

                // 全項目を再度入力値チェック
                if (!ValidateAll())
                {
                    // エラーありの場合ここで処理終了
                    return;
                }

                if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
                {
                    // ﾌﾟﾚﾋﾞｭｰ処理
                    DoPrint(true);
                }
                else
                {
                    this.txtTorihikisakiCdTo.Focus();
                }
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 水揚支所の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool IsValidMizuageShishoCd()
        {
            // 空 又は 0入力の場合
            if (ValChk.IsEmpty(this.txtMizuageShishoCd.Text) || Equals(this.txtMizuageShishoCd.Text, "0"))
            {
                // 水揚支所名称を表示する
                this.txtMizuageShishoCd.Text = "0";
                this.lblMizuageShishoNm.Text = "全て";
                return true;
            }
            // 水揚支所名称を表示する
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);

            if (ValChk.IsEmpty(this.lblMizuageShishoNm.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 年月日(自)の月末入力チェック
        /// </summary>
        /// 
        private void CheckDateFr()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                this.txtDateMonthFr.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDateDayFr.Text) > lastDayInMonth)
            {
                this.txtDateDayFr.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(自)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetDateFr()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDateFr(Util.FixJpDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                this.txtDateMonthFr.Text, this.txtDateDayFr.Text, this.Dba));
        }

        /// <summary>
        /// 年月日(至)の月末入力チェック
        /// </summary>
        /// 
        private void CheckDateTo()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                this.txtDateMonthTo.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDateDayTo.Text) > lastDayInMonth)
            {
                this.txtDateDayTo.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(至)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetDateTo()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDateTo(Util.FixJpDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                this.txtDateMonthTo.Text, this.txtDateDayTo.Text, this.Dba));
        }

        /// <summary>
        /// 取引先(自)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidTorihikisakiCdFr()
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtTorihikisakiCdFr.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtTorihikisakiCdFr.SelectAll();
                return false;
            }

            // コードを元に名称を取得する
            // 取得された場合、名称をラベルに反映する
            if (this.txtTorihikisakiCdFr.Text.Length > 0)
            {
                this.lblTorihikisakiNmFr.Text = this.Dba.GetName(this.UInfo, "TB_CM_TORIHIKISAKI", this.txtMizuageShishoCd.Text, this.txtTorihikisakiCdFr.Text);
            }
            else
            {
                this.lblTorihikisakiNmFr.Text = "先　頭";
            }
            return true;
        }

        /// <summary>
        /// 取引先(至)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidTorihikisakiCdTo()
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtTorihikisakiCdTo.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtTorihikisakiCdTo.SelectAll();
                return false;
            }

            // コードを元に名称を取得する
            // 取得された場合、名称をラベルに反映する
            if (this.txtTorihikisakiCdTo.Text.Length > 0)
            {
                this.lblTorihikisakiNmTo.Text = this.Dba.GetName(this.UInfo, "TB_CM_TORIHIKISAKI", this.txtMizuageShishoCd.Text, this.txtTorihikisakiCdTo.Text);
            }
            else
            {
                this.lblTorihikisakiNmTo.Text = "最　後";
            }
            return true;
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpDateFr(string[] arrJpDate)
        {
            this.lblDateGengoFr.Text = arrJpDate[0];
            this.txtDateYearFr.Text = arrJpDate[2];
            this.txtDateMonthFr.Text = arrJpDate[3];
            this.txtDateDayFr.Text = arrJpDate[4];
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpDateTo(string[] arrJpDate)
        {
            this.lblDateGengoTo.Text = arrJpDate[0];
            this.txtDateYearTo.Text = arrJpDate[2];
            this.txtDateMonthTo.Text = arrJpDate[3];
            this.txtDateDayTo.Text = arrJpDate[4];
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 水揚支所の入力チェック
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                this.txtMizuageShishoCd.Focus();
                this.txtMizuageShishoCd.SelectAll();
                return false;
            }
            // 年(自)のチェック
            if (!IsValid.IsYear(this.txtDateYearFr.Text, this.txtDateYearFr.MaxLength))
            {
                this.txtDateYearFr.Focus();
                this.txtDateYearFr.SelectAll();
                return false;
            }
            // 月(自)のチェック
            if (!IsValid.IsMonth(this.txtDateMonthFr.Text, this.txtDateMonthFr.MaxLength))
            {
                this.txtDateMonthFr.Focus();
                this.txtDateMonthFr.SelectAll();
                return false;
            }
            // 日(自)のチェック
            if (!IsValid.IsDay(this.txtDateDayFr.Text, this.txtDateDayFr.MaxLength))
            {
                this.txtDateDayFr.Focus();
                this.txtDateDayFr.SelectAll();
                return false;
            }
            // 年月日(自)の月末入力チェック処理
            CheckDateFr();
            // 年月日(自)の正しい和暦への変換処理
            SetDateFr();

            // 年(至)のチェック
            if (!IsValid.IsYear(this.txtDateYearTo.Text, this.txtDateYearTo.MaxLength))
            {
                this.txtDateYearTo.Focus();
                this.txtDateYearTo.SelectAll();
                return false;
            }
            // 月(至)のチェック
            if (!IsValid.IsMonth(this.txtDateMonthTo.Text, this.txtDateMonthTo.MaxLength))
            {
                this.txtDateMonthTo.Focus();
                this.txtDateMonthTo.SelectAll();
                return false;
            }
            // 日(至)のチェック
            if (!IsValid.IsDay(this.txtDateDayTo.Text, this.txtDateDayTo.MaxLength))
            {
                this.txtDateDayTo.Focus();
                this.txtDateDayTo.SelectAll();
                return false;
            }
            // 年月日(至)の月末入力チェック処理
            CheckDateTo();
            // 年月日(至)の正しい和暦への変換処理
            SetDateTo();

            // 取引先(自)の入力チェック
            if (!IsValidTorihikisakiCdFr())
            {
                this.txtTorihikisakiCdFr.Focus();
                this.txtTorihikisakiCdFr.SelectAll();
                return false;
            }
            // 取引先(至)の入力チェック
            if (!IsValidTorihikisakiCdTo())
            {
                this.txtTorihikisakiCdTo.Focus();
                this.txtTorihikisakiCdTo.SelectAll();
                return false;
            }

            return true;
        }

        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview, bool isPdf = false, bool isExcel = false, bool isCsv = false)
        {
            try
            {
#if DEBUG
                //印刷用ワークテーブルの削除
                this.Dba.DeleteWork("PR_HN_TBL", this.UnqId);
#endif

                bool dataFlag;

                this.Dba.BeginTransaction();

                // 帳票出力用にワークテーブルにデータを作成
                dataFlag = MakeWkData();

                // 帳票出力
                if (dataFlag)
                {
                    // 取得列の定義
                    StringBuilder cols = Util.ColsArray(prtCols, "");

                    // バインドパラメータの設定
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);

                    // データの取得
                    DataTable dtOutput = this.Dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_HN_TBL", "GUID = @GUID", "SORT ASC", dpc);

                    // 帳票オブジェクトをインスタンス化
                    KBDR2041R rpt = new KBDR2041R(dtOutput);
                    rpt.Document.Printer.DocumentName = this.lblTitle.Text;
                    rpt.Document.Name = this.lblTitle.Text;

                    if (isExcel)
                    {
                        GrapeCity.ActiveReports.Export.Excel.Section.XlsExport xlsExport1 = new GrapeCity.ActiveReports.Export.Excel.Section.XlsExport();
                        //SetExcelSetting(xlsExport1);
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Kob, rpt.Document.Name, 2);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            xlsExport1.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("EXCEL出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPdf)
                    {
                        GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport p = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Kob, rpt.Document.Name, 1);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            p.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("PDF出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPreview)
                    {
                        // プレビュー画面表示
                        PreviewForm pFrm = new PreviewForm(rpt, this.UnqId);
                        pFrm.WindowState = FormWindowState.Maximized;
                        pFrm.Show();
                    }
                    else
                    {
                        // 直接印刷
                        rpt.Run(false);
                        rpt.Document.Print(true, true, false);
                    }
                }
                else
                {
                    Msg.Info("該当データがありません。");
                    //Msg.Error("データがありません。");
                }
            }
            finally
            {
#if DEBUG
                this.Dba.Commit();
#else
                this.Dba.Rollback();
#endif

            }
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData()
        {

            DbParamCollection dpc = new DbParamCollection();
            // 日付範囲を西暦にして取得
            DateTime tmpDateFr = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                    this.txtDateMonthFr.Text, this.txtDateDayFr.Text, this.Dba);
            DateTime tmpDateTo = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                    this.txtDateMonthTo.Text, this.txtDateDayTo.Text, this.Dba);
            DateTime tmpDateTo2 = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                    this.txtDateMonthTo.Text, Util.ToString(DateTime.DaysInMonth(tmpDateTo.Year, Util.ToInt(this.txtDateMonthTo.Text))), this.Dba);
            // 日付範囲を和暦で保持
            string[] tmpjpDateFr = Util.ConvJpDate(tmpDateFr, this.Dba);
            string[] tmpjpDateTo = Util.ConvJpDate(tmpDateTo, this.Dba);
            string[] tmpjpDateTo2 = Util.ConvJpDate(tmpDateTo2, this.Dba);
            // 取引先コード設定
            string TORIHIKISAKI_CD_FR;
            string TORIHIKISAKI_CD_TO;
            if (Util.ToDecimal(txtTorihikisakiCdFr.Text) > 0) 
            {
                TORIHIKISAKI_CD_FR = txtTorihikisakiCdFr.Text;
            }
            else
            {
                TORIHIKISAKI_CD_FR = "0";
            }
            if (Util.ToDecimal(txtTorihikisakiCdTo.Text) > 0) 
            {
                TORIHIKISAKI_CD_TO = txtTorihikisakiCdTo.Text;
            }
            else
            {
                TORIHIKISAKI_CD_TO = "9999";
            }
            string shishoCd = this.txtMizuageShishoCd.Text;

            StringBuilder Sql;
            int i; // ループ用カウント変数
            
            #region メインループデータ取得準備
            // Zam.TB_会社情報(TB_ZM_KAISHA_JOHO)
            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.VarChar, 6, this.UInfo.KaikeiNendo);
            DataTable dtZM_KAISHA_JOHO = this.Dba.GetDataTableByConditionWithParams(
                "*",
                "TB_ZM_KAISHA_JOHO",
                "KAISHA_CD = @KAISHA_CD AND KAIKEI_NENDO = @KAIKEI_NENDO",
                "KAISHA_CD,KESSANKI",
                dpc);

            if (dtZM_KAISHA_JOHO.Rows.Count == 0)
            {
                return false;
            }

            // 月計 支払金額=税込み金額
            dpc = new DbParamCollection();
            Sql = new StringBuilder();
            Sql.Append(" SELECT");
            Sql.Append("     B.HOJO_KAMOKU_CD    AS TORIHIKISAKI_CD,");
            Sql.Append("     CASE WHEN B.TAISHAKU_KUBUN  <>  C.TAISHAKU_KUBUN THEN B.ZEIKOMI_KINGAKU ELSE -1 * B.ZEIKOMI_KINGAKU END    AS ZEIKOMI_KINGAKU");
            Sql.Append(" FROM");
            Sql.Append("     TB_HN_NYUKIN_KAMOKU AS A  ");
            Sql.Append(" LEFT OUTER JOIN  ");
            Sql.Append("     TB_ZM_SHIWAKE_MEISAI  AS B");
            Sql.Append("  ON A.KAISHA_CD     = B.KAISHA_CD ");
            Sql.Append("  AND A.SHISHO_CD     = B.SHISHO_CD ");
            Sql.Append("  AND 1                = B.DENPYO_KUBUN   ");
            Sql.Append("  AND A.KANJO_KAMOKU_CD = B.KANJO_KAMOKU_CD  ");
            Sql.Append(" LEFT OUTER JOIN ");
            Sql.Append("     TB_ZM_KANJO_KAMOKU AS C    ");
            Sql.Append("  ON  B.KAISHA_CD     = C.KAISHA_CD ");
            Sql.Append("  AND B.KANJO_KAMOKU_CD = C.KANJO_KAMOKU_CD");
            Sql.Append("  AND B.KAIKEI_NENDO = C.KAIKEI_NENDO");
            Sql.Append(" WHERE");
            Sql.Append("     A.KAISHA_CD     = @KAISHA_CD AND");
            if (shishoCd != "0")
                Sql.Append("     A.SHISHO_CD     = @SHISHO_CD AND");
            Sql.Append("     A.MOTOCHO_KUBUN       = @MOTOCHO_KUBUN AND");
            Sql.Append("     B.HOJO_KAMOKU_CD  BETWEEN @TORIHIKISAKI_CD_FR AND @TORIHIKISAKI_CD_TO AND");
            Sql.Append("     B.DENPYO_DATE BETWEEN @DATE_FR AND @DATE_TO AND");
            Sql.Append("     B.TAISHAKU_KUBUN  <>  C.TAISHAKU_KUBUN AND");
            Sql.Append("     (B.SHIWAKE_SAKUSEI_KUBUN IS NULL OR B.SHIWAKE_SAKUSEI_KUBUN = 9)");

            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@DATE_FR", SqlDbType.VarChar, 10, tmpDateFr.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@DATE_TO", SqlDbType.VarChar, 10, tmpDateTo.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.VarChar, 6, this.UInfo.KaikeiNendo);
            dpc.SetParam("@TORIHIKISAKI_CD_FR", SqlDbType.VarChar, 6, TORIHIKISAKI_CD_FR);
            dpc.SetParam("@TORIHIKISAKI_CD_TO", SqlDbType.VarChar, 6, TORIHIKISAKI_CD_TO);
            dpc.SetParam("@SHISHO_CD", SqlDbType.VarChar, 6, shishoCd);
            dpc.SetParam("@MOTOCHO_KUBUN", SqlDbType.Decimal, 4, MOTOCHO_KUBUN);
            DataTable dtM_SHIHARAI_KINGAKU = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            // 月計 SUM(売上金額-返品金額)=仕入金額,SUM(売上消費税-返品消費税)=消費税額
            dpc = new DbParamCollection();
            Sql = new StringBuilder();
            Sql.Append(" SELECT");
            Sql.Append("     A.DENPYO_DATE   AS DENPYO_DATE,");
            Sql.Append("     A.TOKUISAKI_CD   AS TORIHIKISAKI_CD,");
            Sql.Append("     A.ZEI_RITSU       AS ZEI_RITSU,");
            Sql.Append("     A.SHOHIZEI_NYURYOKU_HOHO AS SHOHIZEI_NYURYOKU_HOHO,");
            Sql.Append("     SUM( CASE WHEN A.TORIHIKI_KUBUN2 = 2 THEN 0 ELSE A.BARA_SOSU END )  AS URIAGE_SURYO,");
            Sql.Append("     SUM( CASE WHEN A.TORIHIKI_KUBUN2 <> 2 THEN 0 ELSE A.BARA_SOSU END )  AS HENPEN_SURYO,");
            Sql.Append("     SUM( CASE WHEN A.TORIHIKI_KUBUN2 <> 2 THEN 0 ELSE CASE WHEN A.SHOHIZEI_NYURYOKU_HOHO  = 3 THEN A.ZEINUKI_KINGAKU ELSE 0 END END )  AS ZEINUKI_HENPINGAKU,");
            Sql.Append("     SUM( CASE WHEN A.TORIHIKI_KUBUN2 = 2 THEN 0 ELSE CASE WHEN A.SHOHIZEI_NYURYOKU_HOHO  = 3 THEN A.ZEINUKI_KINGAKU ELSE 0 END END )  AS ZEINUKI_KINGAKU,");
            Sql.Append("     SUM( CASE WHEN A.TORIHIKI_KUBUN2 = 2 THEN 0       ELSE CASE WHEN A.SHOHIZEI_NYURYOKU_HOHO  = 3 THEN A.SHOHIZEI ELSE A.SHOHIZEI END END )  AS URIAGE_SHOHIZEI,");
            Sql.Append("     SUM( CASE WHEN A.TORIHIKI_KUBUN2 <> 2 THEN 0       ELSE CASE WHEN A.SHOHIZEI_NYURYOKU_HOHO  = 3 THEN A.SHOHIZEI ELSE A.SHOHIZEI END END )  AS HENPIN_SHOHIZEI,");
            Sql.Append("     0 AS SHOHIZEI,");
            Sql.Append("     SUM( CASE WHEN A.TORIHIKI_KUBUN2 = 2 THEN 0 ELSE CASE WHEN A.SHOHIZEI_NYURYOKU_HOHO <> 3 THEN A.BAIKA_KINGAKU ELSE 0 END END ) AS URIAGE_KINGAKU,");
            Sql.Append("     SUM( CASE WHEN A.TORIHIKI_KUBUN2 <> 2 THEN 0 ELSE CASE WHEN A.SHOHIZEI_NYURYOKU_HOHO <> 3 THEN A.BAIKA_KINGAKU ELSE 0 END END ) AS HENPIN_KINGAKU");
            Sql.Append(" FROM");
            Sql.Append("     VI_HN_TORIHIKI_MEISAI AS A");
            Sql.Append(" WHERE");
            Sql.Append("     A.KAISHA_CD     = @KAISHA_CD AND");
            if (shishoCd != "0")
                Sql.Append("     A.SHISHO_CD     = @SHISHO_CD AND");
            Sql.Append("     A.DENPYO_KUBUN       = 2 AND");
            Sql.Append("     A.TORIHIKI_KUBUN1     = 1 AND");
            Sql.Append("     A.DENPYO_DATE BETWEEN @DATE_FR AND @DATE_TO AND");
            Sql.Append("     (CAST(A.TOKUISAKI_CD AS decimal(4)) BETWEEN @TORIHIKISAKI_CD_FR AND @TORIHIKISAKI_CD_TO)");
            Sql.Append(" GROUP BY");
            Sql.Append("     A.DENPYO_DATE,");
            Sql.Append("     A.TOKUISAKI_CD,");
            Sql.Append("     A.ZEI_RITSU,");
            Sql.Append("     A.SHOHIZEI_NYURYOKU_HOHO");
            Sql.Append(" ORDER BY");
            Sql.Append("     A.TOKUISAKI_CD");
            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@DATE_FR", SqlDbType.VarChar, 10, tmpDateFr.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@DATE_TO", SqlDbType.VarChar, 10, tmpDateTo.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@TORIHIKISAKI_CD_FR", SqlDbType.VarChar, 6, TORIHIKISAKI_CD_FR);
            dpc.SetParam("@TORIHIKISAKI_CD_TO", SqlDbType.VarChar, 6, TORIHIKISAKI_CD_TO);
            dpc.SetParam("@SHISHO_CD", SqlDbType.VarChar, 6, shishoCd);
            DataTable dtM_TORIHIKI_MEISAI = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            // 累計 支払金額=SUM(税込み金額)
            dpc = new DbParamCollection();
            Sql = new StringBuilder();
            Sql.Append(" SELECT");
            Sql.Append("     B.HOJO_KAMOKU_CD    AS TORIHIKISAKI_CD,");
            Sql.Append("     CASE WHEN B.TAISHAKU_KUBUN  <>  C.TAISHAKU_KUBUN THEN B.ZEIKOMI_KINGAKU ELSE -1 * B.ZEIKOMI_KINGAKU END    AS ZEIKOMI_KINGAKU");
            Sql.Append(" FROM");
            Sql.Append("     TB_HN_NYUKIN_KAMOKU AS A  ");
            Sql.Append(" LEFT OUTER JOIN  ");
            Sql.Append("     TB_ZM_SHIWAKE_MEISAI  AS B");
            Sql.Append("  ON A.KAISHA_CD     = B.KAISHA_CD ");
            Sql.Append("  AND A.SHISHO_CD     = B.SHISHO_CD ");
            Sql.Append("  AND 1                = B.DENPYO_KUBUN   ");
            Sql.Append("  AND A.KANJO_KAMOKU_CD = B.KANJO_KAMOKU_CD  ");
            Sql.Append(" LEFT OUTER JOIN ");
            Sql.Append("     TB_ZM_KANJO_KAMOKU AS C    ");
            Sql.Append("  ON  B.KAISHA_CD     = C.KAISHA_CD ");
            Sql.Append("  AND B.KANJO_KAMOKU_CD = C.KANJO_KAMOKU_CD");
            Sql.Append("  AND B.KAIKEI_NENDO = C.KAIKEI_NENDO");
            Sql.Append(" WHERE");
            Sql.Append("     A.KAISHA_CD     = @KAISHA_CD AND");
            if (shishoCd != "0")
                Sql.Append("     A.SHISHO_CD     = @SHISHO_CD AND");
            Sql.Append("     A.MOTOCHO_KUBUN       = @MOTOCHO_KUBUN AND");
            Sql.Append("     B.HOJO_KAMOKU_CD  BETWEEN @TORIHIKISAKI_CD_FR AND @TORIHIKISAKI_CD_TO AND");
            Sql.Append("     B.DENPYO_DATE BETWEEN @DATE_FR AND @DATE_TO AND");
            Sql.Append("     B.TAISHAKU_KUBUN  <>  C.TAISHAKU_KUBUN AND");
            Sql.Append("     (B.SHIWAKE_SAKUSEI_KUBUN IS NULL OR B.SHIWAKE_SAKUSEI_KUBUN = 9)");
            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@DATE_FR", SqlDbType.VarChar, 10, Util.ToDateStr(dtZM_KAISHA_JOHO.Rows[0]["KAIKEI_KIKAN_KAISHIBI"]));
            dpc.SetParam("@DATE_TO", SqlDbType.VarChar, 10, tmpDateTo.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.VarChar, 6, this.UInfo.KaikeiNendo);
            dpc.SetParam("@TORIHIKISAKI_CD_FR", SqlDbType.VarChar, 6, TORIHIKISAKI_CD_FR);
            dpc.SetParam("@TORIHIKISAKI_CD_TO", SqlDbType.VarChar, 6, TORIHIKISAKI_CD_TO);
            dpc.SetParam("@SHISHO_CD", SqlDbType.VarChar, 6, shishoCd);
            dpc.SetParam("@MOTOCHO_KUBUN", SqlDbType.Decimal, 4, MOTOCHO_KUBUN);
            DataTable dtR_SHIHARAI_KINGAKU = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            // 累計 SUM(売上金額-返品金額)=仕入金額,SUM(売上消費税-返品消費税)=消費税額
            dpc = new DbParamCollection();
            Sql = new StringBuilder();
            Sql.Append(" SELECT");
            Sql.Append("     A.DENPYO_DATE   AS DENPYO_DATE,");
            Sql.Append("     A.TOKUISAKI_CD   AS TORIHIKISAKI_CD,");
            Sql.Append("     A.ZEI_RITSU       AS ZEI_RITSU,");
            Sql.Append("     A.SHOHIZEI_NYURYOKU_HOHO AS SHOHIZEI_NYURYOKU_HOHO,");
            Sql.Append("     SUM( CASE WHEN A.TORIHIKI_KUBUN2 = 2 THEN 0 ELSE A.BARA_SOSU END )  AS URIAGE_SURYO,");
            Sql.Append("     SUM( CASE WHEN A.TORIHIKI_KUBUN2 <> 2 THEN 0 ELSE A.BARA_SOSU END )  AS HENPEN_SURYO,");
            Sql.Append("     SUM( CASE WHEN A.TORIHIKI_KUBUN2 <> 2 THEN 0 ELSE CASE WHEN A.SHOHIZEI_NYURYOKU_HOHO  = 3 THEN A.ZEINUKI_KINGAKU ELSE 0 END END )  AS ZEINUKI_HENPINGAKU,");
            Sql.Append("     SUM( CASE WHEN A.TORIHIKI_KUBUN2 = 2 THEN 0 ELSE CASE WHEN A.SHOHIZEI_NYURYOKU_HOHO  = 3 THEN A.ZEINUKI_KINGAKU ELSE 0 END END )  AS ZEINUKI_KINGAKU,");
            Sql.Append("     SUM( CASE WHEN A.TORIHIKI_KUBUN2 = 2 THEN 0       ELSE CASE WHEN A.SHOHIZEI_NYURYOKU_HOHO  = 3 THEN A.SHOHIZEI ELSE A.SHOHIZEI END END )  AS URIAGE_SHOHIZEI,");
            Sql.Append("     SUM( CASE WHEN A.TORIHIKI_KUBUN2 <> 2 THEN 0       ELSE CASE WHEN A.SHOHIZEI_NYURYOKU_HOHO  = 3 THEN A.SHOHIZEI ELSE A.SHOHIZEI END END )  AS HENPIN_SHOHIZEI,");
            Sql.Append("     0 AS SHOHIZEI,");
            Sql.Append("     SUM( CASE WHEN A.TORIHIKI_KUBUN2 = 2 THEN 0 ELSE CASE WHEN A.SHOHIZEI_NYURYOKU_HOHO <> 3 THEN A.BAIKA_KINGAKU ELSE 0 END END ) AS URIAGE_KINGAKU,");
            Sql.Append("     SUM( CASE WHEN A.TORIHIKI_KUBUN2 <> 2 THEN 0 ELSE CASE WHEN A.SHOHIZEI_NYURYOKU_HOHO <> 3 THEN A.BAIKA_KINGAKU ELSE 0 END END ) AS HENPIN_KINGAKU");
            Sql.Append(" FROM");
            Sql.Append("     VI_HN_TORIHIKI_MEISAI AS A");
            Sql.Append(" WHERE");
            Sql.Append("     A.KAISHA_CD     = @KAISHA_CD AND");
            if (shishoCd != "0")
                Sql.Append("     A.SHISHO_CD     = @SHISHO_CD AND");
            Sql.Append("     A.DENPYO_KUBUN       = 2 AND");
            Sql.Append("     A.TORIHIKI_KUBUN1     = 1 AND");
            Sql.Append("     A.DENPYO_DATE BETWEEN @DATE_FR AND @DATE_TO AND");
            Sql.Append("     (CAST(A.TOKUISAKI_CD AS decimal(4)) BETWEEN @TORIHIKISAKI_CD_FR AND @TORIHIKISAKI_CD_TO)");
            Sql.Append(" GROUP BY");
            Sql.Append("     A.DENPYO_DATE,");
            Sql.Append("     A.TOKUISAKI_CD,");
            Sql.Append("     A.ZEI_RITSU,");
            Sql.Append("     A.SHOHIZEI_NYURYOKU_HOHO");
            Sql.Append(" ORDER BY");
            Sql.Append("     A.TOKUISAKI_CD");

            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@DATE_FR", SqlDbType.VarChar, 10, Util.ToDateStr(dtZM_KAISHA_JOHO.Rows[0]["KAIKEI_KIKAN_KAISHIBI"]));
            dpc.SetParam("@DATE_TO", SqlDbType.VarChar, 10, tmpDateTo.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@TORIHIKISAKI_CD_FR", SqlDbType.VarChar, 6, TORIHIKISAKI_CD_FR);
            dpc.SetParam("@TORIHIKISAKI_CD_TO", SqlDbType.VarChar, 6, TORIHIKISAKI_CD_TO);
            dpc.SetParam("@SHISHO_CD", SqlDbType.VarChar, 6, shishoCd);
            DataTable dtR_TORIHIKI_MEISAI = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            // 累計 コード=会員番号,仕入先名=会員名称,前月繰越金額=期首残,支払残高=残高
            dpc = new DbParamCollection();
            Sql = new StringBuilder();
            Sql.Append(" SELECT");
            Sql.Append(" T.TORIHIKISAKI_CD AS TORIHIKISAKI_CD,");
            Sql.Append(" T.TORIHIKISAKI_NM,");
            Sql.Append(" T.CHIKU_CD,");
            Sql.Append(" T.CHIKU_NM,");
            Sql.Append(" ISNULL(Z.ZANDAKA, 0) AS ZANDAKA,");
            Sql.Append(" ISNULL(Z.KISHUZAN, 0) AS KISHUZAN");
            Sql.Append(" FROM (");
            Sql.Append(" 	SELECT ");
            Sql.Append(" 		A.KAISHA_CD,");
            Sql.Append(" 		A.TORIHIKISAKI_CD,");
            Sql.Append(" 		A.TORIHIKISAKI_NM,");
            Sql.Append(" 		A.CHIKU_CD,");
            Sql.Append(" 		C.CHIKU_NM");
            Sql.Append(" 	FROM TB_CM_TORIHIKISAKI A");
            Sql.Append(" 	LEFT JOIN TB_HN_TORIHIKISAKI_JOHO B");
            Sql.Append(" 		ON	A.KAISHA_CD = B.KAISHA_CD");
            Sql.Append(" 		AND A.TORIHIKISAKI_CD = B.TORIHIKISAKI_CD");
            Sql.Append(" 	LEFT JOIN TB_HN_CHIKU_MST C");
            Sql.Append(" 		ON	A.KAISHA_CD = C.KAISHA_CD");
            Sql.Append(" 		AND A.CHIKU_CD	= C.CHIKU_CD");
            Sql.Append(" 	WHERE");
            Sql.Append(" 		B.TORIHIKISAKI_KUBUN3 = 3");
            Sql.Append(" ) T");
            Sql.Append(" LEFT JOIN (");
            Sql.Append(" 	SELECT");
            Sql.Append(" 		D.KAISHA_CD,");
            if (shishoCd != "0")
                Sql.Append(" 		D.SHISHO_CD,");
            Sql.Append(" 		D.HOJO_KAMOKU_CD,");
            Sql.Append(" 		SUM(CASE WHEN D.TAISHAKU_KUBUN = F.TAISHAKU_KUBUN ");
            Sql.Append(" 				 THEN D.ZEIKOMI_KINGAKU ");
            Sql.Append(" 				 ELSE (D.ZEIKOMI_KINGAKU * -1) ");
            Sql.Append(" 				 END) AS ZANDAKA,");
            Sql.Append(" 		SUM(CASE WHEN D.DENPYO_DATE < @DATE_FR  ");
            Sql.Append(" 				 THEN (");
            Sql.Append(" 						CASE WHEN D.TAISHAKU_KUBUN = F.TAISHAKU_KUBUN ");
            Sql.Append(" 							 THEN D.ZEIKOMI_KINGAKU ");
            Sql.Append(" 							 ELSE (D.ZEIKOMI_KINGAKU * -1) ");
            Sql.Append(" 						END)     ");
            Sql.Append(" 				 ELSE 0 END)     AS KISHUZAN ");
            Sql.Append(" 	FROM TB_ZM_SHIWAKE_MEISAI D");
            Sql.Append(" 	INNER JOIN TB_HN_KISHU_ZAN_KAMOKU E");
            Sql.Append(" 		ON	D.KAISHA_CD = E.KAISHA_CD");
            Sql.Append(" 		AND D.SHISHO_CD = E.SHISHO_CD");
            Sql.Append(" 		AND D.KANJO_KAMOKU_CD = E.KANJO_KAMOKU_CD");
            Sql.Append(" 		AND E.MOTOCHO_KUBUN = @MOTOCHO_KUBUN");
            Sql.Append(" 	LEFT OUTER JOIN TB_ZM_KANJO_KAMOKU F");
            Sql.Append(" 		ON	D.KAISHA_CD = F.KAISHA_CD");
            Sql.Append(" 		AND D.KANJO_KAMOKU_CD = F.KANJO_KAMOKU_CD");
            Sql.Append(" 		AND D.KAIKEI_NENDO = F.KAIKEI_NENDO");
            Sql.Append(" 	WHERE");
            Sql.Append(" 		D.KAIKEI_NENDO = @KAIKEI_NENDO AND ");
            Sql.Append(" 		D.DENPYO_DATE <= @DATE_TO");
            Sql.Append(" 	GROUP BY");
            Sql.Append(" 		D.KAISHA_CD,");
            if (shishoCd != "0")
                Sql.Append(" 		D.SHISHO_CD,");
            Sql.Append(" 		D.HOJO_KAMOKU_CD");
            Sql.Append(" ) Z ");
            Sql.Append(" ON	T.KAISHA_CD = Z.KAISHA_CD");
            Sql.Append(" AND T.TORIHIKISAKI_CD = Z.HOJO_KAMOKU_CD");
            Sql.Append(" WHERE");
            Sql.Append(" 	T.KAISHA_CD = @KAISHA_CD AND ");
            if (shishoCd != "0")
                Sql.Append(" 	ISNULL(Z.SHISHO_CD, @SHISHO_CD) = @SHISHO_CD AND ");
            Sql.Append(" 	T.TORIHIKISAKI_CD >= @TORIHIKISAKI_CD_FR AND ");
            Sql.Append(" 	T.TORIHIKISAKI_CD <= @TORIHIKISAKI_CD_TO");
            Sql.Append(" ORDER BY");
            Sql.Append(" 	CHIKU_CD,");
            Sql.Append(" 	TORIHIKISAKI_CD");

            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@DATE_FR", SqlDbType.VarChar, 10, Util.ToDateStr(dtZM_KAISHA_JOHO.Rows[0]["KAIKEI_KIKAN_KAISHIBI"]));
            dpc.SetParam("@DATE_TO", SqlDbType.VarChar, 10, tmpDateTo.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.VarChar, 6, this.UInfo.KaikeiNendo);
            dpc.SetParam("@KESSANKI", SqlDbType.VarChar, 5, dtZM_KAISHA_JOHO.Rows[0]["KESSANKI"]);
            dpc.SetParam("@TORIHIKISAKI_CD_FR", SqlDbType.VarChar, 6, TORIHIKISAKI_CD_FR);
            dpc.SetParam("@TORIHIKISAKI_CD_TO", SqlDbType.VarChar, 6, TORIHIKISAKI_CD_TO);
            dpc.SetParam("@SHISHO_CD", SqlDbType.VarChar, 6, shishoCd);
            dpc.SetParam("@MOTOCHO_KUBUN", SqlDbType.Decimal, 4, MOTOCHO_KUBUN);
            DataTable dtR_MainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
            #endregion

            // 月計 コード=会員番号,仕入先名=会員名称,前月繰越金額=期首残,支払残高=残高
            dpc = new DbParamCollection();
            Sql = new StringBuilder();
            Sql.Append(" SELECT");
            Sql.Append(" T.TORIHIKISAKI_CD AS TORIHIKISAKI_CD,");
            Sql.Append(" T.TORIHIKISAKI_NM,");
            Sql.Append(" T.CHIKU_CD,");
            Sql.Append(" T.CHIKU_NM,");
            Sql.Append(" ISNULL(Z.ZANDAKA, 0) AS ZANDAKA,");
            Sql.Append(" ISNULL(Z.KISHUZAN, 0) AS KISHUZAN");
            Sql.Append(" FROM (");
            Sql.Append(" 	SELECT ");
            Sql.Append(" 		A.KAISHA_CD,");
            Sql.Append(" 		A.TORIHIKISAKI_CD,");
            Sql.Append(" 		A.TORIHIKISAKI_NM,");
            Sql.Append(" 		A.CHIKU_CD,");
            Sql.Append(" 		C.CHIKU_NM");
            Sql.Append(" 	FROM TB_CM_TORIHIKISAKI A");
            Sql.Append(" 	LEFT JOIN TB_HN_TORIHIKISAKI_JOHO B");
            Sql.Append(" 		ON	A.KAISHA_CD = B.KAISHA_CD");
            Sql.Append(" 		AND A.TORIHIKISAKI_CD = B.TORIHIKISAKI_CD");
            Sql.Append(" 	LEFT JOIN TB_HN_CHIKU_MST C");
            Sql.Append(" 		ON	A.KAISHA_CD = C.KAISHA_CD");
            Sql.Append(" 		AND A.CHIKU_CD	= C.CHIKU_CD");
            Sql.Append(" 	WHERE");
            Sql.Append(" 		B.TORIHIKISAKI_KUBUN3 = 3");
            Sql.Append(" ) T");
            Sql.Append(" LEFT JOIN (");
            Sql.Append(" 	SELECT");
            Sql.Append(" 		D.KAISHA_CD,");
            if (shishoCd != "0")
                Sql.Append(" 		D.SHISHO_CD,");
            Sql.Append(" 		D.HOJO_KAMOKU_CD,");
            Sql.Append(" 		SUM(CASE WHEN D.TAISHAKU_KUBUN = F.TAISHAKU_KUBUN ");
            Sql.Append(" 				 THEN D.ZEIKOMI_KINGAKU ");
            Sql.Append(" 				 ELSE (D.ZEIKOMI_KINGAKU * -1) ");
            Sql.Append(" 				 END) AS ZANDAKA,");
            Sql.Append(" 		SUM(CASE WHEN D.DENPYO_DATE < @DATE_FR  ");
            Sql.Append(" 				 THEN (");
            Sql.Append(" 						CASE WHEN D.TAISHAKU_KUBUN = F.TAISHAKU_KUBUN ");
            Sql.Append(" 							 THEN D.ZEIKOMI_KINGAKU ");
            Sql.Append(" 							 ELSE (D.ZEIKOMI_KINGAKU * -1) ");
            Sql.Append(" 						END)     ");
            Sql.Append(" 				 ELSE 0 END)     AS KISHUZAN ");
            Sql.Append(" 	FROM TB_ZM_SHIWAKE_MEISAI D");
            Sql.Append(" 	INNER JOIN TB_HN_KISHU_ZAN_KAMOKU E");
            Sql.Append(" 		ON	D.KAISHA_CD = E.KAISHA_CD");
            Sql.Append(" 		AND D.SHISHO_CD = E.SHISHO_CD");
            Sql.Append(" 		AND D.KANJO_KAMOKU_CD = E.KANJO_KAMOKU_CD");
            Sql.Append(" 		AND E.MOTOCHO_KUBUN = @MOTOCHO_KUBUN");
            Sql.Append(" 	LEFT OUTER JOIN TB_ZM_KANJO_KAMOKU F");
            Sql.Append(" 		ON	D.KAISHA_CD = F.KAISHA_CD");
            Sql.Append(" 		AND D.KANJO_KAMOKU_CD = F.KANJO_KAMOKU_CD");
            Sql.Append(" 		AND D.KAIKEI_NENDO = F.KAIKEI_NENDO");
            Sql.Append(" 	WHERE");
            Sql.Append(" 		D.KAIKEI_NENDO = @KAIKEI_NENDO AND ");
            Sql.Append(" 		D.DENPYO_DATE <= @DATE_TO");
            Sql.Append(" 	GROUP BY");
            Sql.Append(" 		D.KAISHA_CD,");
            if (shishoCd != "0")
                Sql.Append(" 		D.SHISHO_CD,");
            Sql.Append(" 		D.HOJO_KAMOKU_CD");
            Sql.Append(" ) Z ");
            Sql.Append(" ON	T.KAISHA_CD = Z.KAISHA_CD");
            Sql.Append(" AND T.TORIHIKISAKI_CD = Z.HOJO_KAMOKU_CD");
            Sql.Append(" WHERE");
            Sql.Append(" 	T.KAISHA_CD = @KAISHA_CD AND ");
            if (shishoCd != "0")
                Sql.Append(" 	ISNULL(Z.SHISHO_CD, @SHISHO_CD) = @SHISHO_CD AND ");
            Sql.Append(" 	T.TORIHIKISAKI_CD >= @TORIHIKISAKI_CD_FR AND ");
            Sql.Append(" 	T.TORIHIKISAKI_CD <= @TORIHIKISAKI_CD_TO");
            Sql.Append(" ORDER BY");
            Sql.Append(" 	CHIKU_CD,");
            Sql.Append(" 	TORIHIKISAKI_CD");

            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@DATE_FR", SqlDbType.VarChar, 10, tmpDateFr.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@DATE_TO", SqlDbType.VarChar, 10, tmpDateTo.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.VarChar, 6, this.UInfo.KaikeiNendo);
            dpc.SetParam("@KESSANKI", SqlDbType.VarChar, 5, dtZM_KAISHA_JOHO.Rows[0]["KESSANKI"]);
            dpc.SetParam("@TORIHIKISAKI_CD_FR", SqlDbType.VarChar, 6, TORIHIKISAKI_CD_FR);
            dpc.SetParam("@TORIHIKISAKI_CD_TO", SqlDbType.VarChar, 6, TORIHIKISAKI_CD_TO);
            dpc.SetParam("@SHISHO_CD", SqlDbType.VarChar, 6, shishoCd);
            dpc.SetParam("@MOTOCHO_KUBUN", SqlDbType.Decimal, 4, MOTOCHO_KUBUN);
            DataTable dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            if (dtMainLoop.Rows.Count == 0)
            {
                return false;
            }
            else
            {
                
                NumVals MonthNum = new NumVals(); // 月計
                NumVals TMonthNum = new NumVals(); // 月計合計
                NumVals RuikeiNum = new NumVals(); // 累計
                NumVals TRuikeiNum = new NumVals(); // 累計合計

                i = 0;
                int j;
                int dbSORT = 1;
                DataRow[] dtShiharai;
                DataRow[] dtTorihiki;
                int flg;
                // 月計合計リセット
                TMonthNum.Clear();
                // 累計合計リセット
                TRuikeiNum.Clear();

                while (dtMainLoop.Rows.Count > i)
                {
                    // 月計をリセット
                    MonthNum.Clear();
                    // 累計をリセット
                    RuikeiNum.Clear();

                    dtShiharai = dtM_SHIHARAI_KINGAKU.Select("TORIHIKISAKI_CD = " + dtMainLoop.Rows[i]["TORIHIKISAKI_CD"]);
                    dtTorihiki = dtM_TORIHIKI_MEISAI.Select("TORIHIKISAKI_CD = " + dtMainLoop.Rows[i]["TORIHIKISAKI_CD"]);

                    // 取引フラグの確認
                    if (dtShiharai.Length > 0 || dtTorihiki.Length > 0)
                    {
                        flg = 1;
                    }
                    else
                    {
                        flg = 0;
                    }

                    // 登録するデータを判別
                    if ((Util.ToDecimal(dtMainLoop.Rows[i]["ZANDAKA"]) > 0 && Util.ToDecimal(dtMainLoop.Rows[i]["KISHUZAN"]) > 0) || flg == 1)
                    {
                        // 月計 前月繰越金額
                        MonthNum.KISHUZAN = Util.ToDecimal(dtMainLoop.Rows[i]["KISHUZAN"]);

                        // 月計 SUM(売上金額-返品金額)=仕入金額,SUM(売上消費税-返品消費税)=消費税額
                        j = 0;
                        while (dtM_TORIHIKI_MEISAI.Rows.Count > j)
                        {
                            // 取引先コードを比較
                            if (Util.ToDecimal(dtMainLoop.Rows[i]["TORIHIKISAKI_CD"]) == Util.ToDecimal(dtM_TORIHIKI_MEISAI.Rows[j]["TORIHIKISAKI_CD"]))
                            {
                                if (Util.ToDecimal(dtM_TORIHIKI_MEISAI.Rows[j]["SHOHIZEI_NYURYOKU_HOHO"]) == 3)
                                {
                                    // 仕入金額
                                    MonthNum.SHIIRE_KINGAKU += (Util.ToDecimal(dtM_TORIHIKI_MEISAI.Rows[j]["ZEINUKI_KINGAKU"]) - Util.ToDecimal(dtM_TORIHIKI_MEISAI.Rows[j]["ZEINUKI_HENPINGAKU"]));
                                    // 返品金額
                                    MonthNum.HENPIN_KINGAKU += (Util.ToDecimal(dtM_TORIHIKI_MEISAI.Rows[j]["ZEINUKI_HENPINGAKU"]) + Util.ToDecimal(dtM_TORIHIKI_MEISAI.Rows[j]["HENPIN_SHOHIZEI"]));
                                }
                                else
                                {
                                    // 仕入金額
                                    MonthNum.SHIIRE_KINGAKU += (Util.ToDecimal(dtM_TORIHIKI_MEISAI.Rows[j]["URIAGE_KINGAKU"]) - Util.ToDecimal(dtM_TORIHIKI_MEISAI.Rows[j]["HENPIN_KINGAKU"]));
                                    // 返品金額
                                    MonthNum.HENPIN_KINGAKU += (Util.ToDecimal(dtM_TORIHIKI_MEISAI.Rows[j]["HENPIN_KINGAKU"]) + Util.ToDecimal(dtM_TORIHIKI_MEISAI.Rows[j]["HENPIN_SHOHIZEI"]));
                                }
                                // 消費税額
                                MonthNum.SYOUHIZEI_GAKU += (Util.ToDecimal(dtM_TORIHIKI_MEISAI.Rows[j]["URIAGE_SHOHIZEI"]) - Util.ToDecimal(dtM_TORIHIKI_MEISAI.Rows[j]["HENPIN_SHOHIZEI"]));

                            }
                            j++;
                        }

                        // 月計 支払金額=SUM(税込み金額)
                        j = 0;
                        while (dtM_SHIHARAI_KINGAKU.Rows.Count > j)
                        {
                            // 取引先コードを比較
                            if (Util.ToDecimal(dtMainLoop.Rows[i]["TORIHIKISAKI_CD"]) == Util.ToDecimal(dtM_SHIHARAI_KINGAKU.Rows[j]["TORIHIKISAKI_CD"]))
                            {
                                // 支払金額
                                MonthNum.SHIHARAI_KINGAKU += Util.ToDecimal(dtM_SHIHARAI_KINGAKU.Rows[j]["ZEIKOMI_KINGAKU"]);
                            }
                            j++;
                        }

                        // 累計 前月繰越金額
                        RuikeiNum.KISHUZAN = Util.ToDecimal(dtR_MainLoop.Rows[i]["KISHUZAN"]);

                        // 累計 SUM(売上金額-返品金額)=仕入金額,SUM(売上消費税-返品消費税)=消費税額
                        j = 0;
                        while (dtR_TORIHIKI_MEISAI.Rows.Count > j)
                        {
                            // 取引先コードを比較
                            if (Util.ToDecimal(dtMainLoop.Rows[i]["TORIHIKISAKI_CD"]) == Util.ToDecimal(dtR_TORIHIKI_MEISAI.Rows[j]["TORIHIKISAKI_CD"]))
                            {
                                if (Util.ToDecimal(dtR_TORIHIKI_MEISAI.Rows[j]["SHOHIZEI_NYURYOKU_HOHO"]) == 3)
                                {
                                    // 仕入金額
                                    RuikeiNum.SHIIRE_KINGAKU += (Util.ToDecimal(dtR_TORIHIKI_MEISAI.Rows[j]["ZEINUKI_KINGAKU"]) - Util.ToDecimal(dtR_TORIHIKI_MEISAI.Rows[j]["ZEINUKI_HENPINGAKU"]));
                                    // 返品金額
                                    RuikeiNum.HENPIN_KINGAKU += (Util.ToDecimal(dtR_TORIHIKI_MEISAI.Rows[j]["ZEINUKI_HENPINGAKU"]) + Util.ToDecimal(dtR_TORIHIKI_MEISAI.Rows[j]["HENPIN_SHOHIZEI"]));
                                }
                                else
                                {
                                    // 仕入金額
                                    RuikeiNum.SHIIRE_KINGAKU += (Util.ToDecimal(dtR_TORIHIKI_MEISAI.Rows[j]["URIAGE_KINGAKU"]) - Util.ToDecimal(dtR_TORIHIKI_MEISAI.Rows[j]["HENPIN_KINGAKU"]));
                                    // 返品金額
                                    RuikeiNum.HENPIN_KINGAKU += (Util.ToDecimal(dtR_TORIHIKI_MEISAI.Rows[j]["HENPIN_KINGAKU"]) + Util.ToDecimal(dtR_TORIHIKI_MEISAI.Rows[j]["HENPIN_SHOHIZEI"]));
                                }
                                // 消費税額
                                RuikeiNum.SYOUHIZEI_GAKU += (Util.ToDecimal(dtR_TORIHIKI_MEISAI.Rows[j]["URIAGE_SHOHIZEI"]) - Util.ToDecimal(dtR_TORIHIKI_MEISAI.Rows[j]["HENPIN_SHOHIZEI"]));

                            }
                            j++;
                        }

                        // 累計 支払金額=SUM(税込み金額)
                        j = 0;
                        while (dtR_SHIHARAI_KINGAKU.Rows.Count > j)
                        {
                            // 取引先コードを比較
                            if (Util.ToDecimal(dtMainLoop.Rows[i]["TORIHIKISAKI_CD"]) == Util.ToDecimal(dtR_SHIHARAI_KINGAKU.Rows[j]["TORIHIKISAKI_CD"]))
                            {
                                // 支払金額
                                RuikeiNum.SHIHARAI_KINGAKU += Util.ToDecimal(dtR_SHIHARAI_KINGAKU.Rows[j]["ZEIKOMI_KINGAKU"]);
                            }
                            j++;
                        }


                        #region 印刷ワークテーブルに登録
                        Sql = new StringBuilder();
                        dpc = new DbParamCollection();
                        Sql.Append("INSERT INTO PR_HN_TBL(");
                        Sql.Append("  GUID");
                        Sql.Append(" ,SORT");
                        //Sql.Append(" ,ITEM01");
                        //Sql.Append(" ,ITEM02");
                        //Sql.Append(" ,ITEM03");
                        //Sql.Append(" ,ITEM04");
                        //Sql.Append(" ,ITEM05");
                        //Sql.Append(" ,ITEM06");
                        //Sql.Append(" ,ITEM07");
                        //Sql.Append(" ,ITEM08");
                        //Sql.Append(" ,ITEM09");
                        //Sql.Append(" ,ITEM10");
                        //Sql.Append(" ,ITEM11");
                        //Sql.Append(" ,ITEM12");
                        //Sql.Append(" ,ITEM13");
                        //Sql.Append(" ,ITEM14");
                        //Sql.Append(" ,ITEM15");
                        //Sql.Append(" ,ITEM16");
                        //Sql.Append(" ,ITEM17");
                        //Sql.Append(" ,ITEM18");
                        //Sql.Append(" ,ITEM19");
                        //Sql.Append(" ,ITEM20");
                        //Sql.Append(") ");
                        //Sql.Append("VALUES(");
                        //Sql.Append("  @GUID");
                        //Sql.Append(" ,@SORT");
                        //Sql.Append(" ,@ITEM01");
                        //Sql.Append(" ,@ITEM02");
                        //Sql.Append(" ,@ITEM03");
                        //Sql.Append(" ,@ITEM04");
                        //Sql.Append(" ,@ITEM05");
                        //Sql.Append(" ,@ITEM06");
                        //Sql.Append(" ,@ITEM07");
                        //Sql.Append(" ,@ITEM08");
                        //Sql.Append(" ,@ITEM09");
                        //Sql.Append(" ,@ITEM10");
                        //Sql.Append(" ,@ITEM11");
                        //Sql.Append(" ,@ITEM12");
                        //Sql.Append(" ,@ITEM13");
                        //Sql.Append(" ,@ITEM14");
                        //Sql.Append(" ,@ITEM15");
                        //Sql.Append(" ,@ITEM16");
                        //Sql.Append(" ,@ITEM17");
                        //Sql.Append(" ,@ITEM18");
                        //Sql.Append(" ,@ITEM19");
                        //Sql.Append(" ,@ITEM20");
                        //Sql.Append(") ");
                        Sql.Append(" ," + Util.ColsArray(prtCols, ""));
                        Sql.Append(") ");
                        Sql.Append("VALUES(");
                        Sql.Append("  @GUID");
                        Sql.Append(" ,@SORT");
                        Sql.Append(" ," + Util.ColsArray(prtCols, "@"));
                        Sql.Append(") ");
                        // 月計登録
                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                        dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                        // ページヘッダーデータを設定
                        dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);
                        dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpjpDateFr[5]);
                        dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpjpDateFr[5]);
                        if (tmpDateFr.Year == tmpDateTo.Year && tmpDateFr.Month == tmpDateTo.Month && tmpDateFr.Day == 1 && Util.ToDecimal(tmpjpDateTo2[4]) == tmpDateTo.Day)
                        {
                            dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200,
                                string.Format("{0}{1}年{2}月度", tmpjpDateFr[0], tmpjpDateFr[2], tmpjpDateFr[3]));
                        }
                        else
                        {
                            dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200,
                                string.Format(tmpjpDateFr[5])
                                + " ～ " +
                                tmpjpDateTo[5]);
                        }

                        // 月計登録を設定
                        dpc.SetParam("@ITEM05", SqlDbType.VarChar, 4, dtMainLoop.Rows[i]["TORIHIKISAKI_CD"]); // ｺｰﾄﾞ
                        dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["TORIHIKISAKI_NM"]); // 仕入先名
                        dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, "月　計"); // 月計
                        dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, Util.FormatNum(MonthNum.KISHUZAN)); // 前月繰越金額
                        dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, Util.FormatNum(MonthNum.SHIIRE_KINGAKU)); // 仕入金額
                        dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, Util.FormatNum(MonthNum.SYOUHIZEI_GAKU)); // 消費税額
                        dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, Util.FormatNum(MonthNum.SHIIRE_KINGAKU + MonthNum.SYOUHIZEI_GAKU)); // 税込金額
                        dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, Util.FormatNum(MonthNum.SHIHARAI_KINGAKU)); // 支払金額
                        dpc.SetParam("@ITEM13", SqlDbType.VarChar, 200,
                            Util.FormatNum(
                                (MonthNum.KISHUZAN + MonthNum.SHIIRE_KINGAKU + MonthNum.SYOUHIZEI_GAKU) - MonthNum.SHIHARAI_KINGAKU)); // 支払残高

                        // 累計登録を設定
                        dpc.SetParam("@ITEM14", SqlDbType.VarChar, 200, "累　計"); // 累計
                        dpc.SetParam("@ITEM15", SqlDbType.VarChar, 200, Util.FormatNum(RuikeiNum.KISHUZAN)); // 前月繰越金額
                        dpc.SetParam("@ITEM16", SqlDbType.VarChar, 200, Util.FormatNum(RuikeiNum.SHIIRE_KINGAKU)); // 仕入金額
                        dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, Util.FormatNum(RuikeiNum.SYOUHIZEI_GAKU)); // 消費税額
                        dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, Util.FormatNum(RuikeiNum.SHIIRE_KINGAKU + RuikeiNum.SYOUHIZEI_GAKU)); // 税込金額
                        dpc.SetParam("@ITEM19", SqlDbType.VarChar, 200, Util.FormatNum(RuikeiNum.SHIHARAI_KINGAKU)); // 支払金額
                        dpc.SetParam("@ITEM20", SqlDbType.VarChar, 200,
                            Util.FormatNum(
                                (RuikeiNum.KISHUZAN + RuikeiNum.SHIIRE_KINGAKU + RuikeiNum.SYOUHIZEI_GAKU) - RuikeiNum.SHIHARAI_KINGAKU)); // 支払残高

                        this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                        // 月計を加算
                        TMonthNum.KISHUZAN += MonthNum.KISHUZAN;
                        TMonthNum.SHIIRE_KINGAKU += MonthNum.SHIIRE_KINGAKU;
                        TMonthNum.SYOUHIZEI_GAKU += MonthNum.SYOUHIZEI_GAKU;
                        TMonthNum.SHIHARAI_KINGAKU += MonthNum.SHIHARAI_KINGAKU;
                        // 累計を加算
                        TRuikeiNum.KISHUZAN += RuikeiNum.KISHUZAN;
                        TRuikeiNum.SHIIRE_KINGAKU += RuikeiNum.SHIIRE_KINGAKU;
                        TRuikeiNum.SYOUHIZEI_GAKU += RuikeiNum.SYOUHIZEI_GAKU;
                        TRuikeiNum.SHIHARAI_KINGAKU += RuikeiNum.SHIHARAI_KINGAKU;

                        dbSORT++;
                        #endregion                        
                    }
                    i++;
                }

                #region 印刷ワークテーブルに合計登録
                dpc = new DbParamCollection();
                Sql = new StringBuilder();
                Sql.Append("INSERT INTO PR_HN_TBL(");
                Sql.Append("  GUID");
                Sql.Append(" ,SORT");
                //Sql.Append(" ,ITEM01");
                //Sql.Append(" ,ITEM02");
                //Sql.Append(" ,ITEM03");
                //Sql.Append(" ,ITEM04");
                //Sql.Append(") ");
                //Sql.Append("VALUES(");
                //Sql.Append("  @GUID");
                //Sql.Append(" ,@SORT");
                //Sql.Append(" ,@ITEM01");
                //Sql.Append(" ,@ITEM02");
                //Sql.Append(" ,@ITEM03");
                //Sql.Append(" ,@ITEM04");
                //Sql.Append(") ");
                Sql.Append(" ," + Util.ColsArray(4, ""));
                Sql.Append(") ");
                Sql.Append("VALUES(");
                Sql.Append("  @GUID");
                Sql.Append(" ,@SORT");
                Sql.Append(" ," + Util.ColsArray(4, "@"));
                Sql.Append(") ");

                // 空行登録 ２つ
                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT + 1);
                // ページヘッダーデータを設定
                dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);
                dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpjpDateFr[5]);
                dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpjpDateFr[5]);
                if (tmpDateFr.Year == tmpDateTo.Year && tmpDateFr.Month == tmpDateTo.Month && tmpDateFr.Day == 1 && Util.ToDecimal(tmpjpDateTo2[4]) == tmpDateTo.Day)
                {
                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200,
                        string.Format("{0}{1}年{2}月度", tmpjpDateFr[0], tmpjpDateFr[2], tmpjpDateFr[3]));
                }
                else
                {
                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200,
                        string.Format(tmpjpDateFr[5])
                        + " ～ " +
                        tmpjpDateTo[5]);
                }
                this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                dpc = new DbParamCollection();
                Sql = new StringBuilder();
                Sql.Append("INSERT INTO PR_HN_TBL(");
                Sql.Append("  GUID");
                Sql.Append(" ,SORT");
                //Sql.Append(" ,ITEM01");
                //Sql.Append(" ,ITEM02");
                //Sql.Append(" ,ITEM03");
                //Sql.Append(" ,ITEM04");
                //Sql.Append(" ,ITEM05");
                //Sql.Append(" ,ITEM06");
                //Sql.Append(" ,ITEM07");
                //Sql.Append(" ,ITEM08");
                //Sql.Append(" ,ITEM09");
                //Sql.Append(" ,ITEM10");
                //Sql.Append(" ,ITEM11");
                //Sql.Append(" ,ITEM12");
                //Sql.Append(" ,ITEM13");
                //Sql.Append(" ,ITEM14");
                //Sql.Append(" ,ITEM15");
                //Sql.Append(" ,ITEM16");
                //Sql.Append(" ,ITEM17");
                //Sql.Append(" ,ITEM18");
                //Sql.Append(" ,ITEM19");
                //Sql.Append(" ,ITEM20");
                //Sql.Append(") ");
                //Sql.Append("VALUES(");
                //Sql.Append("  @GUID");
                //Sql.Append(" ,@SORT");
                //Sql.Append(" ,@ITEM01");
                //Sql.Append(" ,@ITEM02");
                //Sql.Append(" ,@ITEM03");
                //Sql.Append(" ,@ITEM04");
                //Sql.Append(" ,@ITEM05");
                //Sql.Append(" ,@ITEM06");
                //Sql.Append(" ,@ITEM07");
                //Sql.Append(" ,@ITEM08");
                //Sql.Append(" ,@ITEM09");
                //Sql.Append(" ,@ITEM10");
                //Sql.Append(" ,@ITEM11");
                //Sql.Append(" ,@ITEM12");
                //Sql.Append(" ,@ITEM13");
                //Sql.Append(" ,@ITEM14");
                //Sql.Append(" ,@ITEM15");
                //Sql.Append(" ,@ITEM16");
                //Sql.Append(" ,@ITEM17");
                //Sql.Append(" ,@ITEM18");
                //Sql.Append(" ,@ITEM19");
                //Sql.Append(" ,@ITEM20");
                //Sql.Append(") ");
                Sql.Append(" ," + Util.ColsArray(prtCols, ""));
                Sql.Append(") ");
                Sql.Append("VALUES(");
                Sql.Append("  @GUID");
                Sql.Append(" ,@SORT");
                Sql.Append(" ," + Util.ColsArray(prtCols, "@"));
                Sql.Append(") ");
                // 月計登録
                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT + 2);
                // ページヘッダーデータを設定
                dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);
                dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpjpDateFr[5]);
                dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpjpDateFr[5]);
                if (tmpDateFr.Year == tmpDateTo.Year && tmpDateFr.Month == tmpDateTo.Month)
                {
                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200,
                        string.Format("{0}{1}年{2}月度", tmpjpDateFr[0], tmpjpDateFr[2], tmpjpDateFr[3]));
                }
                else
                {
                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, "");
                }
                // 月計登録の設定
                dpc.SetParam("@ITEM05", SqlDbType.VarChar, 4, ""); // ｺｰﾄﾞ
                dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, "　　【　合　計　】"); // 仕入先名
                dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, "月　計"); // 月計
                dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, Util.FormatNum(TMonthNum.KISHUZAN)); // 前月繰越金額
                dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, Util.FormatNum(TMonthNum.SHIIRE_KINGAKU)); // 仕入金額
                dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, Util.FormatNum(TMonthNum.SYOUHIZEI_GAKU)); // 消費税額
                dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, Util.FormatNum(TMonthNum.SHIIRE_KINGAKU + TMonthNum.SYOUHIZEI_GAKU)); // 税込金額
                dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, Util.FormatNum(TMonthNum.SHIHARAI_KINGAKU)); // 支払金額
                dpc.SetParam("@ITEM13", SqlDbType.VarChar, 200,
                    Util.FormatNum(
                        (TMonthNum.KISHUZAN + TMonthNum.SHIIRE_KINGAKU + TMonthNum.SYOUHIZEI_GAKU) - TMonthNum.SHIHARAI_KINGAKU)); // 支払残高
                
                // 累計登録を設定
                dpc.SetParam("@ITEM14", SqlDbType.VarChar, 200, "累　計"); // 累計
                dpc.SetParam("@ITEM15", SqlDbType.VarChar, 200, Util.FormatNum(TRuikeiNum.KISHUZAN)); // 前月繰越金額
                dpc.SetParam("@ITEM16", SqlDbType.VarChar, 200, Util.FormatNum(TRuikeiNum.SHIIRE_KINGAKU)); // 仕入金額
                dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, Util.FormatNum(TRuikeiNum.SYOUHIZEI_GAKU)); // 消費税額
                dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, Util.FormatNum(TRuikeiNum.SHIIRE_KINGAKU + TRuikeiNum.SYOUHIZEI_GAKU)); // 税込金額
                dpc.SetParam("@ITEM19", SqlDbType.VarChar, 200, Util.FormatNum(TRuikeiNum.SHIHARAI_KINGAKU)); // 支払金額
                dpc.SetParam("@ITEM20", SqlDbType.VarChar, 200,
                    Util.FormatNum(
                        (TRuikeiNum.KISHUZAN + TRuikeiNum.SHIIRE_KINGAKU + TRuikeiNum.SYOUHIZEI_GAKU) - TRuikeiNum.SHIHARAI_KINGAKU)); // 支払残高

                this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                #endregion
            }

            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
                "SORT",
                "PR_HN_TBL",
                "GUID = @GUID",
                dpc);
            
            bool dataFlag;
            if (tmpdtPR_HN_TBL.Rows.Count > 0)
            {
                dataFlag = true;

                // 空データ時の対応
                dpc = new DbParamCollection();
                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
                    "SORT",
                    "PR_HN_TBL",
                    "GUID = @GUID AND LTRIM(RTRIM(ITEM05)) <> ''",
                    dpc);
                if (tmpdtPR_HN_TBL.Rows.Count == 0)
                {
                    dataFlag = false;
                }
            }
            else
            {
                dataFlag = false;
            }

            return dataFlag;
        }
        #endregion

    }
}
