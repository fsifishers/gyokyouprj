﻿namespace jp.co.fsi.kb.kbdr2041
{
    partial class KBDR2041
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpTorihikisakiCd = new System.Windows.Forms.GroupBox();
            this.lblTorihikisakiNmTo = new System.Windows.Forms.Label();
            this.txtTorihikisakiCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblCodeBet = new System.Windows.Forms.Label();
            this.lblTorihikisakiNmFr = new System.Windows.Forms.Label();
            this.txtTorihikisakiCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.gbxDate = new System.Windows.Forms.GroupBox();
            this.lblBet = new System.Windows.Forms.Label();
            this.lblDateDayTo = new System.Windows.Forms.Label();
            this.lblDateDayFr = new System.Windows.Forms.Label();
            this.lblDateMonthTo = new System.Windows.Forms.Label();
            this.lblDateMonthFr = new System.Windows.Forms.Label();
            this.labelDateYearTo = new System.Windows.Forms.Label();
            this.labelDateYearFr = new System.Windows.Forms.Label();
            this.txtDateDayTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtDateDayFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtDateYearTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtDateYearFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtDateMonthTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtDateMonthFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblDateGengoTo = new System.Windows.Forms.Label();
            this.lblDateTo1 = new System.Windows.Forms.Label();
            this.lblDateGengoFr = new System.Windows.Forms.Label();
            this.lblDateFr1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMizuageShishoNm = new System.Windows.Forms.Label();
            this.lblMizuageShisho = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.grpTorihikisakiCd.SuspendLayout();
            this.gbxDate.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Text = "購買未払金一覧表";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Size = new System.Drawing.Size(847, 100);
            // 
            // grpTorihikisakiCd
            // 
            this.grpTorihikisakiCd.Controls.Add(this.lblTorihikisakiNmTo);
            this.grpTorihikisakiCd.Controls.Add(this.txtTorihikisakiCdTo);
            this.grpTorihikisakiCd.Controls.Add(this.lblCodeBet);
            this.grpTorihikisakiCd.Controls.Add(this.lblTorihikisakiNmFr);
            this.grpTorihikisakiCd.Controls.Add(this.txtTorihikisakiCdFr);
            this.grpTorihikisakiCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.grpTorihikisakiCd.Location = new System.Drawing.Point(12, 214);
            this.grpTorihikisakiCd.Name = "grpTorihikisakiCd";
            this.grpTorihikisakiCd.Size = new System.Drawing.Size(615, 78);
            this.grpTorihikisakiCd.TabIndex = 2;
            this.grpTorihikisakiCd.TabStop = false;
            this.grpTorihikisakiCd.Text = "仕入先コード範囲";
            // 
            // lblTorihikisakiNmTo
            // 
            this.lblTorihikisakiNmTo.BackColor = System.Drawing.Color.Silver;
            this.lblTorihikisakiNmTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTorihikisakiNmTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTorihikisakiNmTo.Location = new System.Drawing.Point(382, 33);
            this.lblTorihikisakiNmTo.Name = "lblTorihikisakiNmTo";
            this.lblTorihikisakiNmTo.Size = new System.Drawing.Size(194, 20);
            this.lblTorihikisakiNmTo.TabIndex = 4;
            this.lblTorihikisakiNmTo.Text = "最　後";
            this.lblTorihikisakiNmTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTorihikisakiCdTo
            // 
            this.txtTorihikisakiCdTo.AutoSizeFromLength = false;
            this.txtTorihikisakiCdTo.DisplayLength = null;
            this.txtTorihikisakiCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTorihikisakiCdTo.Location = new System.Drawing.Point(321, 31);
            this.txtTorihikisakiCdTo.MaxLength = 4;
            this.txtTorihikisakiCdTo.Name = "txtTorihikisakiCdTo";
            this.txtTorihikisakiCdTo.Size = new System.Drawing.Size(55, 22);
            this.txtTorihikisakiCdTo.TabIndex = 9;
            this.txtTorihikisakiCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTorihikisakiCdTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtTorihikisakiCdTo_KeyDown);
            this.txtTorihikisakiCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtTorihikisakiCdTo_Validating);
            // 
            // lblCodeBet
            // 
            this.lblCodeBet.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblCodeBet.Location = new System.Drawing.Point(293, 31);
            this.lblCodeBet.Name = "lblCodeBet";
            this.lblCodeBet.Size = new System.Drawing.Size(18, 20);
            this.lblCodeBet.TabIndex = 2;
            this.lblCodeBet.Text = "～";
            this.lblCodeBet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTorihikisakiNmFr
            // 
            this.lblTorihikisakiNmFr.BackColor = System.Drawing.Color.Silver;
            this.lblTorihikisakiNmFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTorihikisakiNmFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTorihikisakiNmFr.Location = new System.Drawing.Point(93, 33);
            this.lblTorihikisakiNmFr.Name = "lblTorihikisakiNmFr";
            this.lblTorihikisakiNmFr.Size = new System.Drawing.Size(194, 20);
            this.lblTorihikisakiNmFr.TabIndex = 1;
            this.lblTorihikisakiNmFr.Text = "先　頭";
            this.lblTorihikisakiNmFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTorihikisakiCdFr
            // 
            this.txtTorihikisakiCdFr.AutoSizeFromLength = false;
            this.txtTorihikisakiCdFr.DisplayLength = null;
            this.txtTorihikisakiCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTorihikisakiCdFr.Location = new System.Drawing.Point(32, 31);
            this.txtTorihikisakiCdFr.MaxLength = 4;
            this.txtTorihikisakiCdFr.Name = "txtTorihikisakiCdFr";
            this.txtTorihikisakiCdFr.Size = new System.Drawing.Size(55, 22);
            this.txtTorihikisakiCdFr.TabIndex = 8;
            this.txtTorihikisakiCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTorihikisakiCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtTorihikisakiCdFr_Validating);
            // 
            // gbxDate
            // 
            this.gbxDate.Controls.Add(this.lblBet);
            this.gbxDate.Controls.Add(this.lblDateDayTo);
            this.gbxDate.Controls.Add(this.lblDateDayFr);
            this.gbxDate.Controls.Add(this.lblDateMonthTo);
            this.gbxDate.Controls.Add(this.lblDateMonthFr);
            this.gbxDate.Controls.Add(this.labelDateYearTo);
            this.gbxDate.Controls.Add(this.labelDateYearFr);
            this.gbxDate.Controls.Add(this.txtDateDayTo);
            this.gbxDate.Controls.Add(this.txtDateDayFr);
            this.gbxDate.Controls.Add(this.txtDateYearTo);
            this.gbxDate.Controls.Add(this.txtDateYearFr);
            this.gbxDate.Controls.Add(this.txtDateMonthTo);
            this.gbxDate.Controls.Add(this.txtDateMonthFr);
            this.gbxDate.Controls.Add(this.lblDateGengoTo);
            this.gbxDate.Controls.Add(this.lblDateTo1);
            this.gbxDate.Controls.Add(this.lblDateGengoFr);
            this.gbxDate.Controls.Add(this.lblDateFr1);
            this.gbxDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.gbxDate.ForeColor = System.Drawing.SystemColors.ControlText;
            this.gbxDate.Location = new System.Drawing.Point(12, 127);
            this.gbxDate.Name = "gbxDate";
            this.gbxDate.Size = new System.Drawing.Size(483, 81);
            this.gbxDate.TabIndex = 1;
            this.gbxDate.TabStop = false;
            this.gbxDate.Text = "日付範囲";
            // 
            // lblBet
            // 
            this.lblBet.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBet.Location = new System.Drawing.Point(231, 29);
            this.lblBet.Name = "lblBet";
            this.lblBet.Size = new System.Drawing.Size(17, 28);
            this.lblBet.TabIndex = 8;
            this.lblBet.Text = "～";
            this.lblBet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateDayTo
            // 
            this.lblDateDayTo.BackColor = System.Drawing.Color.Silver;
            this.lblDateDayTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateDayTo.Location = new System.Drawing.Point(431, 33);
            this.lblDateDayTo.Name = "lblDateDayTo";
            this.lblDateDayTo.Size = new System.Drawing.Size(20, 18);
            this.lblDateDayTo.TabIndex = 15;
            this.lblDateDayTo.Text = "日";
            this.lblDateDayTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateDayFr
            // 
            this.lblDateDayFr.BackColor = System.Drawing.Color.Silver;
            this.lblDateDayFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateDayFr.Location = new System.Drawing.Point(207, 33);
            this.lblDateDayFr.Name = "lblDateDayFr";
            this.lblDateDayFr.Size = new System.Drawing.Size(20, 18);
            this.lblDateDayFr.TabIndex = 7;
            this.lblDateDayFr.Text = "日";
            this.lblDateDayFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateMonthTo
            // 
            this.lblDateMonthTo.BackColor = System.Drawing.Color.Silver;
            this.lblDateMonthTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateMonthTo.Location = new System.Drawing.Point(381, 34);
            this.lblDateMonthTo.Name = "lblDateMonthTo";
            this.lblDateMonthTo.Size = new System.Drawing.Size(15, 19);
            this.lblDateMonthTo.TabIndex = 13;
            this.lblDateMonthTo.Text = "月";
            this.lblDateMonthTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateMonthFr
            // 
            this.lblDateMonthFr.BackColor = System.Drawing.Color.Silver;
            this.lblDateMonthFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateMonthFr.Location = new System.Drawing.Point(157, 33);
            this.lblDateMonthFr.Name = "lblDateMonthFr";
            this.lblDateMonthFr.Size = new System.Drawing.Size(15, 19);
            this.lblDateMonthFr.TabIndex = 5;
            this.lblDateMonthFr.Text = "月";
            this.lblDateMonthFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelDateYearTo
            // 
            this.labelDateYearTo.BackColor = System.Drawing.Color.Silver;
            this.labelDateYearTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.labelDateYearTo.Location = new System.Drawing.Point(331, 33);
            this.labelDateYearTo.Name = "labelDateYearTo";
            this.labelDateYearTo.Size = new System.Drawing.Size(17, 21);
            this.labelDateYearTo.TabIndex = 11;
            this.labelDateYearTo.Text = "年";
            this.labelDateYearTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelDateYearFr
            // 
            this.labelDateYearFr.BackColor = System.Drawing.Color.Silver;
            this.labelDateYearFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.labelDateYearFr.Location = new System.Drawing.Point(107, 32);
            this.labelDateYearFr.Name = "labelDateYearFr";
            this.labelDateYearFr.Size = new System.Drawing.Size(17, 21);
            this.labelDateYearFr.TabIndex = 3;
            this.labelDateYearFr.Text = "年";
            this.labelDateYearFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDateDayTo
            // 
            this.txtDateDayTo.AutoSizeFromLength = false;
            this.txtDateDayTo.DisplayLength = null;
            this.txtDateDayTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateDayTo.Location = new System.Drawing.Point(399, 32);
            this.txtDateDayTo.MaxLength = 2;
            this.txtDateDayTo.Name = "txtDateDayTo";
            this.txtDateDayTo.Size = new System.Drawing.Size(30, 20);
            this.txtDateDayTo.TabIndex = 7;
            this.txtDateDayTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateDayTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateDayTo_Validating);
            // 
            // txtDateDayFr
            // 
            this.txtDateDayFr.AutoSizeFromLength = false;
            this.txtDateDayFr.DisplayLength = null;
            this.txtDateDayFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateDayFr.Location = new System.Drawing.Point(175, 32);
            this.txtDateDayFr.MaxLength = 2;
            this.txtDateDayFr.Name = "txtDateDayFr";
            this.txtDateDayFr.Size = new System.Drawing.Size(30, 20);
            this.txtDateDayFr.TabIndex = 4;
            this.txtDateDayFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateDayFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateDayFr_Validating);
            // 
            // txtDateYearTo
            // 
            this.txtDateYearTo.AutoSizeFromLength = false;
            this.txtDateYearTo.DisplayLength = null;
            this.txtDateYearTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateYearTo.Location = new System.Drawing.Point(299, 32);
            this.txtDateYearTo.MaxLength = 2;
            this.txtDateYearTo.Name = "txtDateYearTo";
            this.txtDateYearTo.Size = new System.Drawing.Size(30, 20);
            this.txtDateYearTo.TabIndex = 5;
            this.txtDateYearTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateYearTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateYearTo_Validating);
            // 
            // txtDateYearFr
            // 
            this.txtDateYearFr.AutoSizeFromLength = false;
            this.txtDateYearFr.DisplayLength = null;
            this.txtDateYearFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateYearFr.Location = new System.Drawing.Point(75, 32);
            this.txtDateYearFr.MaxLength = 2;
            this.txtDateYearFr.Name = "txtDateYearFr";
            this.txtDateYearFr.Size = new System.Drawing.Size(30, 20);
            this.txtDateYearFr.TabIndex = 2;
            this.txtDateYearFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateYearFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateYearFr_Validating);
            // 
            // txtDateMonthTo
            // 
            this.txtDateMonthTo.AutoSizeFromLength = false;
            this.txtDateMonthTo.DisplayLength = null;
            this.txtDateMonthTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateMonthTo.Location = new System.Drawing.Point(349, 32);
            this.txtDateMonthTo.MaxLength = 2;
            this.txtDateMonthTo.Name = "txtDateMonthTo";
            this.txtDateMonthTo.Size = new System.Drawing.Size(30, 20);
            this.txtDateMonthTo.TabIndex = 6;
            this.txtDateMonthTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateMonthTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateMonthTo_Validating);
            // 
            // txtDateMonthFr
            // 
            this.txtDateMonthFr.AutoSizeFromLength = false;
            this.txtDateMonthFr.DisplayLength = null;
            this.txtDateMonthFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateMonthFr.Location = new System.Drawing.Point(125, 32);
            this.txtDateMonthFr.MaxLength = 2;
            this.txtDateMonthFr.Name = "txtDateMonthFr";
            this.txtDateMonthFr.Size = new System.Drawing.Size(30, 20);
            this.txtDateMonthFr.TabIndex = 3;
            this.txtDateMonthFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateMonthFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateMonthFr_Validating);
            // 
            // lblDateGengoTo
            // 
            this.lblDateGengoTo.BackColor = System.Drawing.Color.Silver;
            this.lblDateGengoTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDateGengoTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateGengoTo.Location = new System.Drawing.Point(258, 32);
            this.lblDateGengoTo.Name = "lblDateGengoTo";
            this.lblDateGengoTo.Size = new System.Drawing.Size(37, 21);
            this.lblDateGengoTo.TabIndex = 9;
            this.lblDateGengoTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateTo1
            // 
            this.lblDateTo1.BackColor = System.Drawing.Color.Silver;
            this.lblDateTo1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDateTo1.Location = new System.Drawing.Point(254, 29);
            this.lblDateTo1.Name = "lblDateTo1";
            this.lblDateTo1.Size = new System.Drawing.Size(199, 28);
            this.lblDateTo1.TabIndex = 2;
            // 
            // lblDateGengoFr
            // 
            this.lblDateGengoFr.BackColor = System.Drawing.Color.Silver;
            this.lblDateGengoFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDateGengoFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateGengoFr.Location = new System.Drawing.Point(34, 32);
            this.lblDateGengoFr.Name = "lblDateGengoFr";
            this.lblDateGengoFr.Size = new System.Drawing.Size(37, 21);
            this.lblDateGengoFr.TabIndex = 1;
            this.lblDateGengoFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateFr1
            // 
            this.lblDateFr1.BackColor = System.Drawing.Color.Silver;
            this.lblDateFr1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDateFr1.Location = new System.Drawing.Point(30, 28);
            this.lblDateFr1.Name = "lblDateFr1";
            this.lblDateFr1.Size = new System.Drawing.Size(199, 28);
            this.lblDateFr1.TabIndex = 1;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtMizuageShishoCd);
            this.groupBox1.Controls.Add(this.lblMizuageShishoNm);
            this.groupBox1.Controls.Add(this.lblMizuageShisho);
            this.groupBox1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.groupBox1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox1.Location = new System.Drawing.Point(12, 55);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(348, 66);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "支所";
            // 
            // txtMizuageShishoCd
            // 
            this.txtMizuageShishoCd.AutoSizeFromLength = true;
            this.txtMizuageShishoCd.DisplayLength = null;
            this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtMizuageShishoCd.Location = new System.Drawing.Point(71, 25);
            this.txtMizuageShishoCd.MaxLength = 4;
            this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
            this.txtMizuageShishoCd.Size = new System.Drawing.Size(34, 20);
            this.txtMizuageShishoCd.TabIndex = 908;
            this.txtMizuageShishoCd.TabStop = false;
            this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
            // 
            // lblMizuageShishoNm
            // 
            this.lblMizuageShishoNm.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMizuageShishoNm.Location = new System.Drawing.Point(106, 25);
            this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
            this.lblMizuageShishoNm.Size = new System.Drawing.Size(212, 20);
            this.lblMizuageShishoNm.TabIndex = 910;
            this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMizuageShisho
            // 
            this.lblMizuageShisho.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShisho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShisho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblMizuageShisho.Location = new System.Drawing.Point(29, 23);
            this.lblMizuageShisho.Name = "lblMizuageShisho";
            this.lblMizuageShisho.Size = new System.Drawing.Size(293, 25);
            this.lblMizuageShisho.TabIndex = 909;
            this.lblMizuageShisho.Text = "支所";
            this.lblMizuageShisho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // KBDR2041
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 638);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.gbxDate);
            this.Controls.Add(this.grpTorihikisakiCd);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "KBDR2041";
            this.Par1 = "2";
            this.ShowFButton = true;
            this.Text = "購買未払金一覧表";
            this.Controls.SetChildIndex(this.grpTorihikisakiCd, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.gbxDate, 0);
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.pnlDebug.ResumeLayout(false);
            this.grpTorihikisakiCd.ResumeLayout(false);
            this.grpTorihikisakiCd.PerformLayout();
            this.gbxDate.ResumeLayout(false);
            this.gbxDate.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpTorihikisakiCd;
        private System.Windows.Forms.Label lblTorihikisakiNmTo;
        private jp.co.fsi.common.controls.FsiTextBox txtTorihikisakiCdTo;
        private System.Windows.Forms.Label lblCodeBet;
        private System.Windows.Forms.Label lblTorihikisakiNmFr;
        private jp.co.fsi.common.controls.FsiTextBox txtTorihikisakiCdFr;
        private System.Windows.Forms.GroupBox gbxDate;
        private System.Windows.Forms.Label lblBet;
        private System.Windows.Forms.Label lblDateDayTo;
        private System.Windows.Forms.Label lblDateDayFr;
        private System.Windows.Forms.Label lblDateMonthTo;
        private System.Windows.Forms.Label lblDateMonthFr;
        private System.Windows.Forms.Label labelDateYearTo;
        private System.Windows.Forms.Label labelDateYearFr;
        private common.controls.FsiTextBox txtDateDayTo;
        private common.controls.FsiTextBox txtDateDayFr;
        private common.controls.FsiTextBox txtDateYearTo;
        private common.controls.FsiTextBox txtDateYearFr;
        private common.controls.FsiTextBox txtDateMonthTo;
        private common.controls.FsiTextBox txtDateMonthFr;
        private System.Windows.Forms.Label lblDateGengoTo;
        private System.Windows.Forms.Label lblDateTo1;
        private System.Windows.Forms.Label lblDateGengoFr;
        private System.Windows.Forms.Label lblDateFr1;
        private System.Windows.Forms.GroupBox groupBox1;
        private common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private System.Windows.Forms.Label lblMizuageShisho;
    }
}