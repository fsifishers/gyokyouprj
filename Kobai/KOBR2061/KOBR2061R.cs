﻿using System.Data;

using jp.co.fsi.common.report;
namespace jp.co.fsi.kob.kobr2061
{
    /// <summary>
    /// KOBR2061R の概要の説明です。
    /// </summary>
    public partial class KOBR2061R : BaseReport
    {

        public KOBR2061R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }
    }
}
