﻿namespace jp.co.fsi.kob.kobr2061
{
    /// <summary>
    /// KOBR2061R の概要の説明です。
    /// </summary>
    partial class KOBR2061R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(KOBR2061R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.lblTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtKaishaNm = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.shape1 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblRank = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtRank = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.lblCd = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblShohinNm = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSuryo = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTanka = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblKingaku = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblShohizei = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblKingakuKei = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtCd = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtShohinNm = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSuryo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTanka = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtKingaku = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtShohizei = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtKingakukei = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.rptDate = new GrapeCity.ActiveReports.SectionReportModel.ReportInfo();
            this.line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line15 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line16 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line17 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line18 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.reportHeader1 = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.reportFooter1 = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.txtKingakuGokei = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtKingakuKeiKei = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line19 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line20 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line21 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line22 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line23 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line24 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line25 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line26 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line27 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line28 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblGokei = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtSuryoKei = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKaishaNm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRank)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRank)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblShohinNm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSuryo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTanka)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKingaku)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblShohizei)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKingakuKei)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShohinNm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSuryo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTanka)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKingaku)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShohizei)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKingakukei)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rptDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKingakuGokei)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKingakuKeiKei)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGokei)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSuryoKei)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblTitle,
            this.txtKaishaNm,
            this.txtPage,
            this.lblPage,
            this.line1,
            this.shape1,
            this.txtDate,
            this.line2,
            this.line3,
            this.line4,
            this.line5,
            this.line6,
            this.line7,
            this.line8,
            this.lblRank,
            this.lblCd,
            this.lblShohinNm,
            this.lblSuryo,
            this.lblTanka,
            this.lblKingaku,
            this.lblShohizei,
            this.lblKingakuKei,
            this.rptDate});
            this.pageHeader.Height = 0.8854165F;
            this.pageHeader.Name = "pageHeader";
            // 
            // lblTitle
            // 
            this.lblTitle.Height = 0.2814961F;
            this.lblTitle.HyperLink = null;
            this.lblTitle.Left = 2.354126F;
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Style = "font-family: ＭＳ ゴシック; font-size: 15.75pt; text-align: center";
            this.lblTitle.Text = "商品売上順位表";
            this.lblTitle.Top = 0F;
            this.lblTitle.Width = 2.562697F;
            // 
            // txtKaishaNm
            // 
            this.txtKaishaNm.DataField = "ITEM01";
            this.txtKaishaNm.Height = 0.1770833F;
            this.txtKaishaNm.Left = 0.09409449F;
            this.txtKaishaNm.Name = "txtKaishaNm";
            this.txtKaishaNm.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt";
            this.txtKaishaNm.Text = "kashaNm";
            this.txtKaishaNm.Top = 0.1200787F;
            this.txtKaishaNm.Width = 0.9479167F;
            // 
            // txtPage
            // 
            this.txtPage.Height = 0.1770833F;
            this.txtPage.Left = 6.728889F;
            this.txtPage.Name = "txtPage";
            this.txtPage.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; text-align: right";
            this.txtPage.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtPage.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.txtPage.Text = "page";
            this.txtPage.Top = 0.1200787F;
            this.txtPage.Width = 0.4062998F;
            // 
            // lblPage
            // 
            this.lblPage.Height = 0.1772145F;
            this.lblPage.HyperLink = null;
            this.lblPage.Left = 7.135088F;
            this.lblPage.Name = "lblPage";
            this.lblPage.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt";
            this.lblPage.Text = "頁";
            this.lblPage.Top = 0.1200787F;
            this.lblPage.Width = 0.1979828F;
            // 
            // line1
            // 
            this.line1.Height = 0F;
            this.line1.Left = 2.365748F;
            this.line1.LineWeight = 1F;
            this.line1.Name = "line1";
            this.line1.Top = 0.2870079F;
            this.line1.Width = 2.519686F;
            this.line1.X1 = 2.365748F;
            this.line1.X2 = 4.885434F;
            this.line1.Y1 = 0.2870079F;
            this.line1.Y2 = 0.2870079F;
            // 
            // shape1
            // 
            this.shape1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.shape1.Height = 0.2358268F;
            this.shape1.Left = 0F;
            this.shape1.Name = "shape1";
            this.shape1.RoundingRadius = 9.999999F;
            this.shape1.Top = 0.6492127F;
            this.shape1.Width = 7.458268F;
            // 
            // txtDate
            // 
            this.txtDate.DataField = "ITEM02";
            this.txtDate.Height = 0.1770833F;
            this.txtDate.Left = 2.365748F;
            this.txtDate.Name = "txtDate";
            this.txtDate.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; text-align: center";
            this.txtDate.Text = "date";
            this.txtDate.Top = 0.3669292F;
            this.txtDate.Width = 2.551181F;
            // 
            // line2
            // 
            this.line2.Height = 0.2358267F;
            this.line2.Left = 0.7102363F;
            this.line2.LineWeight = 1F;
            this.line2.Name = "line2";
            this.line2.Top = 0.6492127F;
            this.line2.Width = 0F;
            this.line2.X1 = 0.7102363F;
            this.line2.X2 = 0.7102363F;
            this.line2.Y1 = 0.6492127F;
            this.line2.Y2 = 0.8850393F;
            // 
            // line3
            // 
            this.line3.Height = 0.2358267F;
            this.line3.Left = 1.376772F;
            this.line3.LineWeight = 1F;
            this.line3.Name = "line3";
            this.line3.Top = 0.6492127F;
            this.line3.Width = 0F;
            this.line3.X1 = 1.376772F;
            this.line3.X2 = 1.376772F;
            this.line3.Y1 = 0.6492127F;
            this.line3.Y2 = 0.8850393F;
            // 
            // line4
            // 
            this.line4.Height = 0.2358267F;
            this.line4.Left = 3.621654F;
            this.line4.LineWeight = 1F;
            this.line4.Name = "line4";
            this.line4.Top = 0.6492127F;
            this.line4.Width = 0F;
            this.line4.X1 = 3.621654F;
            this.line4.X2 = 3.621654F;
            this.line4.Y1 = 0.6492127F;
            this.line4.Y2 = 0.8850393F;
            // 
            // line5
            // 
            this.line5.Height = 0.2358267F;
            this.line5.Left = 4.333465F;
            this.line5.LineWeight = 1F;
            this.line5.Name = "line5";
            this.line5.Top = 0.6492127F;
            this.line5.Width = 0F;
            this.line5.X1 = 4.333465F;
            this.line5.X2 = 4.333465F;
            this.line5.Y1 = 0.6492127F;
            this.line5.Y2 = 0.8850394F;
            // 
            // line6
            // 
            this.line6.Height = 0.2358267F;
            this.line6.Left = 5.038977F;
            this.line6.LineWeight = 1F;
            this.line6.Name = "line6";
            this.line6.Top = 0.6492127F;
            this.line6.Width = 0F;
            this.line6.X1 = 5.038977F;
            this.line6.X2 = 5.038977F;
            this.line6.Y1 = 0.6492127F;
            this.line6.Y2 = 0.8850393F;
            // 
            // line7
            // 
            this.line7.Height = 0.2358267F;
            this.line7.Left = 5.885434F;
            this.line7.LineWeight = 1F;
            this.line7.Name = "line7";
            this.line7.Top = 0.6492127F;
            this.line7.Width = 0F;
            this.line7.X1 = 5.885434F;
            this.line7.X2 = 5.885434F;
            this.line7.Y1 = 0.6492127F;
            this.line7.Y2 = 0.8850394F;
            // 
            // line8
            // 
            this.line8.Height = 0.2358267F;
            this.line8.Left = 6.572442F;
            this.line8.LineWeight = 1F;
            this.line8.Name = "line8";
            this.line8.Top = 0.6492127F;
            this.line8.Width = 0F;
            this.line8.X1 = 6.572442F;
            this.line8.X2 = 6.572442F;
            this.line8.Y1 = 0.6492127F;
            this.line8.Y2 = 0.8850393F;
            // 
            // lblRank
            // 
            this.lblRank.Height = 0.1770833F;
            this.lblRank.HyperLink = null;
            this.lblRank.Left = 0.06653544F;
            this.lblRank.Name = "lblRank";
            this.lblRank.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; text-align: center";
            this.lblRank.Text = "売上順位";
            this.lblRank.Top = 0.7078741F;
            this.lblRank.Width = 0.5833333F;
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtRank,
            this.txtCd,
            this.txtShohinNm,
            this.txtSuryo,
            this.txtTanka,
            this.txtKingaku,
            this.txtShohizei,
            this.txtKingakukei,
            this.line9,
            this.line10,
            this.line11,
            this.line12,
            this.line13,
            this.line14,
            this.line15,
            this.line16,
            this.line17,
            this.line18});
            this.detail.Height = 0.1841098F;
            this.detail.Name = "detail";
            // 
            // txtRank
            // 
            this.txtRank.DataField = "ITEM03";
            this.txtRank.Height = 0.1770833F;
            this.txtRank.Left = 0.02755906F;
            this.txtRank.Name = "txtRank";
            this.txtRank.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; vertical-align: middle";
            this.txtRank.Text = "rank";
            this.txtRank.Top = 0F;
            this.txtRank.Width = 0.65F;
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0.01041667F;
            this.pageFooter.Name = "pageFooter";
            // 
            // lblCd
            // 
            this.lblCd.Height = 0.1770833F;
            this.lblCd.HyperLink = null;
            this.lblCd.Left = 0.7692914F;
            this.lblCd.Name = "lblCd";
            this.lblCd.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; text-align: center";
            this.lblCd.Text = "コード";
            this.lblCd.Top = 0.7078741F;
            this.lblCd.Width = 0.5417323F;
            // 
            // lblShohinNm
            // 
            this.lblShohinNm.Height = 0.1770833F;
            this.lblShohinNm.HyperLink = null;
            this.lblShohinNm.Left = 1.458662F;
            this.lblShohinNm.Name = "lblShohinNm";
            this.lblShohinNm.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; text-align: center";
            this.lblShohinNm.Text = "商　　品　　名";
            this.lblShohinNm.Top = 0.7082677F;
            this.lblShohinNm.Width = 1.97874F;
            // 
            // lblSuryo
            // 
            this.lblSuryo.Height = 0.1770833F;
            this.lblSuryo.HyperLink = null;
            this.lblSuryo.Left = 3.686615F;
            this.lblSuryo.Name = "lblSuryo";
            this.lblSuryo.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; text-align: center";
            this.lblSuryo.Text = "数　量";
            this.lblSuryo.Top = 0.7082677F;
            this.lblSuryo.Width = 0.5833333F;
            // 
            // lblTanka
            // 
            this.lblTanka.Height = 0.1770833F;
            this.lblTanka.HyperLink = null;
            this.lblTanka.Left = 4.395669F;
            this.lblTanka.Name = "lblTanka";
            this.lblTanka.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; text-align: center";
            this.lblTanka.Text = "単　価";
            this.lblTanka.Top = 0.7082677F;
            this.lblTanka.Width = 0.5833333F;
            // 
            // lblKingaku
            // 
            this.lblKingaku.Height = 0.1770833F;
            this.lblKingaku.HyperLink = null;
            this.lblKingaku.Left = 5.145669F;
            this.lblKingaku.Name = "lblKingaku";
            this.lblKingaku.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; text-align: center";
            this.lblKingaku.Text = "金　額";
            this.lblKingaku.Top = 0.7078741F;
            this.lblKingaku.Width = 0.6456693F;
            // 
            // lblShohizei
            // 
            this.lblShohizei.Height = 0.1770833F;
            this.lblShohizei.HyperLink = null;
            this.lblShohizei.Left = 5.921654F;
            this.lblShohizei.Name = "lblShohizei";
            this.lblShohizei.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; text-align: center";
            this.lblShohizei.Text = "消費税";
            this.lblShohizei.Top = 0.7082677F;
            this.lblShohizei.Width = 0.5833333F;
            // 
            // lblKingakuKei
            // 
            this.lblKingakuKei.Height = 0.1770833F;
            this.lblKingakuKei.HyperLink = null;
            this.lblKingakuKei.Left = 6.749607F;
            this.lblKingakuKei.Name = "lblKingakuKei";
            this.lblKingakuKei.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; text-align: center";
            this.lblKingakuKei.Text = "金額計";
            this.lblKingakuKei.Top = 0.7078741F;
            this.lblKingakuKei.Width = 0.5833333F;
            // 
            // txtCd
            // 
            this.txtCd.DataField = "ITEM04";
            this.txtCd.Height = 0.1770833F;
            this.txtCd.Left = 0.7338583F;
            this.txtCd.Name = "txtCd";
            this.txtCd.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; vertical-align: middle";
            this.txtCd.Text = "cd";
            this.txtCd.Top = 0F;
            this.txtCd.Width = 0.6429135F;
            // 
            // txtShohinNm
            // 
            this.txtShohinNm.DataField = "ITEM05";
            this.txtShohinNm.Height = 0.1770833F;
            this.txtShohinNm.Left = 1.395669F;
            this.txtShohinNm.MultiLine = false;
            this.txtShohinNm.Name = "txtShohinNm";
            this.txtShohinNm.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; vertical-align: middle";
            this.txtShohinNm.Text = "shohiNm";
            this.txtShohinNm.Top = 0F;
            this.txtShohinNm.Width = 2.225984F;
            // 
            // txtSuryo
            // 
            this.txtSuryo.DataField = "ITEM06";
            this.txtSuryo.Height = 0.1770833F;
            this.txtSuryo.Left = 3.635433F;
            this.txtSuryo.Name = "txtSuryo";
            this.txtSuryo.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; text-align: right; vertical-align: middl" +
    "e";
            this.txtSuryo.Text = "suryo";
            this.txtSuryo.Top = 0F;
            this.txtSuryo.Width = 0.6980319F;
            // 
            // txtTanka
            // 
            this.txtTanka.DataField = "ITEM07";
            this.txtTanka.Height = 0.1770833F;
            this.txtTanka.Left = 4.395669F;
            this.txtTanka.Name = "txtTanka";
            this.txtTanka.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; text-align: right; vertical-align: middl" +
    "e";
            this.txtTanka.Text = "tanka";
            this.txtTanka.Top = 0F;
            this.txtTanka.Width = 0.6433072F;
            // 
            // txtKingaku
            // 
            this.txtKingaku.DataField = "ITEM08";
            this.txtKingaku.Height = 0.1770833F;
            this.txtKingaku.Left = 5.093307F;
            this.txtKingaku.Name = "txtKingaku";
            this.txtKingaku.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; text-align: right; vertical-align: middl" +
    "e";
            this.txtKingaku.Text = "kinagku";
            this.txtKingaku.Top = 0F;
            this.txtKingaku.Width = 0.7921262F;
            // 
            // txtShohizei
            // 
            this.txtShohizei.DataField = "ITEM09";
            this.txtShohizei.Height = 0.1770833F;
            this.txtShohizei.Left = 5.921654F;
            this.txtShohizei.Name = "txtShohizei";
            this.txtShohizei.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; text-align: right; vertical-align: middl" +
    "e";
            this.txtShohizei.Text = "shohizei";
            this.txtShohizei.Top = 0F;
            this.txtShohizei.Width = 0.6507878F;
            // 
            // txtKingakukei
            // 
            this.txtKingakukei.DataField = "ITEM10";
            this.txtKingakukei.Height = 0.1770833F;
            this.txtKingakukei.Left = 6.640552F;
            this.txtKingakukei.Name = "txtKingakukei";
            this.txtKingakukei.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; text-align: right; vertical-align: middl" +
    "e";
            this.txtKingakukei.Text = "kingakuKei";
            this.txtKingakukei.Top = 0F;
            this.txtKingakukei.Width = 0.8177166F;
            // 
            // rptDate
            // 
            this.rptDate.FormatString = "{RunDateTime:yyyy/MM/dd}";
            this.rptDate.Height = 0.1771654F;
            this.rptDate.Left = 5.859449F;
            this.rptDate.Name = "rptDate";
            this.rptDate.Style = "";
            this.rptDate.Top = 0.1200787F;
            this.rptDate.Width = 0.7811022F;
            // 
            // line9
            // 
            this.line9.Height = 0.1771654F;
            this.line9.Left = 0.7102363F;
            this.line9.LineWeight = 1F;
            this.line9.Name = "line9";
            this.line9.Top = 0F;
            this.line9.Width = 0F;
            this.line9.X1 = 0.7102363F;
            this.line9.X2 = 0.7102363F;
            this.line9.Y1 = 0F;
            this.line9.Y2 = 0.1771654F;
            // 
            // line10
            // 
            this.line10.Height = 0.1771653F;
            this.line10.Left = 1.376772F;
            this.line10.LineWeight = 1F;
            this.line10.Name = "line10";
            this.line10.Top = 0F;
            this.line10.Width = 0F;
            this.line10.X1 = 1.376772F;
            this.line10.X2 = 1.376772F;
            this.line10.Y1 = 0F;
            this.line10.Y2 = 0.1771653F;
            // 
            // line11
            // 
            this.line11.Height = 0.1771653F;
            this.line11.Left = 3.621654F;
            this.line11.LineWeight = 1F;
            this.line11.Name = "line11";
            this.line11.Top = 0F;
            this.line11.Width = 0F;
            this.line11.X1 = 3.621654F;
            this.line11.X2 = 3.621654F;
            this.line11.Y1 = 0F;
            this.line11.Y2 = 0.1771653F;
            // 
            // line12
            // 
            this.line12.Height = 0.1771653F;
            this.line12.Left = 4.333465F;
            this.line12.LineWeight = 1F;
            this.line12.Name = "line12";
            this.line12.Top = 0F;
            this.line12.Width = 0F;
            this.line12.X1 = 4.333465F;
            this.line12.X2 = 4.333465F;
            this.line12.Y1 = 0F;
            this.line12.Y2 = 0.1771653F;
            // 
            // line13
            // 
            this.line13.Height = 0.1771653F;
            this.line13.Left = 5.038977F;
            this.line13.LineWeight = 1F;
            this.line13.Name = "line13";
            this.line13.Top = 0F;
            this.line13.Width = 0F;
            this.line13.X1 = 5.038977F;
            this.line13.X2 = 5.038977F;
            this.line13.Y1 = 0F;
            this.line13.Y2 = 0.1771653F;
            // 
            // line14
            // 
            this.line14.Height = 0.1771653F;
            this.line14.Left = 5.885434F;
            this.line14.LineWeight = 1F;
            this.line14.Name = "line14";
            this.line14.Top = 0F;
            this.line14.Width = 0F;
            this.line14.X1 = 5.885434F;
            this.line14.X2 = 5.885434F;
            this.line14.Y1 = 0F;
            this.line14.Y2 = 0.1771653F;
            // 
            // line15
            // 
            this.line15.Height = 0.1771653F;
            this.line15.Left = 6.572442F;
            this.line15.LineWeight = 1F;
            this.line15.Name = "line15";
            this.line15.Top = 0F;
            this.line15.Width = 0F;
            this.line15.X1 = 6.572442F;
            this.line15.X2 = 6.572442F;
            this.line15.Y1 = 0F;
            this.line15.Y2 = 0.1771653F;
            // 
            // line16
            // 
            this.line16.Height = 0.1771653F;
            this.line16.Left = 0.003937008F;
            this.line16.LineWeight = 1F;
            this.line16.Name = "line16";
            this.line16.Top = 0F;
            this.line16.Width = 0F;
            this.line16.X1 = 0.003937008F;
            this.line16.X2 = 0.003937008F;
            this.line16.Y1 = 0F;
            this.line16.Y2 = 0.1771653F;
            // 
            // line17
            // 
            this.line17.Height = 0.1771653F;
            this.line17.Left = 7.454331F;
            this.line17.LineWeight = 1F;
            this.line17.Name = "line17";
            this.line17.Top = 0F;
            this.line17.Width = 0F;
            this.line17.X1 = 7.454331F;
            this.line17.X2 = 7.454331F;
            this.line17.Y1 = 0F;
            this.line17.Y2 = 0.1771653F;
            // 
            // line18
            // 
            this.line18.Height = 0F;
            this.line18.Left = 0F;
            this.line18.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.line18.LineWeight = 1F;
            this.line18.Name = "line18";
            this.line18.Top = 0.1771654F;
            this.line18.Width = 7.460629F;
            this.line18.X1 = 0F;
            this.line18.X2 = 7.460629F;
            this.line18.Y1 = 0.1771654F;
            this.line18.Y2 = 0.1771654F;
            // 
            // reportHeader1
            // 
            this.reportHeader1.Height = 0F;
            this.reportHeader1.Name = "reportHeader1";
            // 
            // reportFooter1
            // 
            this.reportFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtKingakuGokei,
            this.txtKingakuKeiKei,
            this.line19,
            this.line20,
            this.line21,
            this.line22,
            this.line23,
            this.line24,
            this.line25,
            this.line26,
            this.line27,
            this.line28,
            this.lblGokei,
            this.txtSuryoKei});
            this.reportFooter1.Height = 0.1875F;
            this.reportFooter1.Name = "reportFooter1";
            // 
            // txtKingakuGokei
            // 
            this.txtKingakuGokei.DataField = "ITEM08";
            this.txtKingakuGokei.Height = 0.1770833F;
            this.txtKingakuGokei.Left = 5.093307F;
            this.txtKingakuGokei.Name = "txtKingakuGokei";
            this.txtKingakuGokei.OutputFormat = resources.GetString("txtKingakuGokei.OutputFormat");
            this.txtKingakuGokei.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; text-align: right; vertical-align: middl" +
    "e";
            this.txtKingakuGokei.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtKingakuGokei.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtKingakuGokei.Text = "kinagku";
            this.txtKingakuGokei.Top = 0F;
            this.txtKingakuGokei.Width = 0.7921264F;
            // 
            // txtKingakuKeiKei
            // 
            this.txtKingakuKeiKei.DataField = "ITEM10";
            this.txtKingakuKeiKei.Height = 0.1770833F;
            this.txtKingakuKeiKei.Left = 6.640552F;
            this.txtKingakuKeiKei.Name = "txtKingakuKeiKei";
            this.txtKingakuKeiKei.OutputFormat = resources.GetString("txtKingakuKeiKei.OutputFormat");
            this.txtKingakuKeiKei.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; text-align: right; vertical-align: middl" +
    "e";
            this.txtKingakuKeiKei.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtKingakuKeiKei.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtKingakuKeiKei.Text = "kingakuKei";
            this.txtKingakuKeiKei.Top = 0F;
            this.txtKingakuKeiKei.Width = 0.8177167F;
            // 
            // line19
            // 
            this.line19.Height = 0.1771653F;
            this.line19.Left = 0.7102364F;
            this.line19.LineWeight = 1F;
            this.line19.Name = "line19";
            this.line19.Top = 0F;
            this.line19.Width = 0F;
            this.line19.X1 = 0.7102364F;
            this.line19.X2 = 0.7102364F;
            this.line19.Y1 = 0F;
            this.line19.Y2 = 0.1771653F;
            // 
            // line20
            // 
            this.line20.Height = 0.1771653F;
            this.line20.Left = 1.376772F;
            this.line20.LineWeight = 1F;
            this.line20.Name = "line20";
            this.line20.Top = 0F;
            this.line20.Width = 0F;
            this.line20.X1 = 1.376772F;
            this.line20.X2 = 1.376772F;
            this.line20.Y1 = 0F;
            this.line20.Y2 = 0.1771653F;
            // 
            // line21
            // 
            this.line21.Height = 0.1771653F;
            this.line21.Left = 3.621655F;
            this.line21.LineWeight = 1F;
            this.line21.Name = "line21";
            this.line21.Top = 0F;
            this.line21.Width = 0F;
            this.line21.X1 = 3.621655F;
            this.line21.X2 = 3.621655F;
            this.line21.Y1 = 0F;
            this.line21.Y2 = 0.1771653F;
            // 
            // line22
            // 
            this.line22.Height = 0.1771653F;
            this.line22.Left = 4.333466F;
            this.line22.LineWeight = 1F;
            this.line22.Name = "line22";
            this.line22.Top = 0F;
            this.line22.Width = 0F;
            this.line22.X1 = 4.333466F;
            this.line22.X2 = 4.333466F;
            this.line22.Y1 = 0F;
            this.line22.Y2 = 0.1771653F;
            // 
            // line23
            // 
            this.line23.Height = 0.1771653F;
            this.line23.Left = 5.038977F;
            this.line23.LineWeight = 1F;
            this.line23.Name = "line23";
            this.line23.Top = 0F;
            this.line23.Width = 0F;
            this.line23.X1 = 5.038977F;
            this.line23.X2 = 5.038977F;
            this.line23.Y1 = 0F;
            this.line23.Y2 = 0.1771653F;
            // 
            // line24
            // 
            this.line24.Height = 0.1771653F;
            this.line24.Left = 5.885434F;
            this.line24.LineWeight = 1F;
            this.line24.Name = "line24";
            this.line24.Top = 0F;
            this.line24.Width = 0F;
            this.line24.X1 = 5.885434F;
            this.line24.X2 = 5.885434F;
            this.line24.Y1 = 0F;
            this.line24.Y2 = 0.1771653F;
            // 
            // line25
            // 
            this.line25.Height = 0.1771653F;
            this.line25.Left = 6.572441F;
            this.line25.LineWeight = 1F;
            this.line25.Name = "line25";
            this.line25.Top = 0F;
            this.line25.Width = 0F;
            this.line25.X1 = 6.572441F;
            this.line25.X2 = 6.572441F;
            this.line25.Y1 = 0F;
            this.line25.Y2 = 0.1771653F;
            // 
            // line26
            // 
            this.line26.Height = 0.1771653F;
            this.line26.Left = 0.003937247F;
            this.line26.LineWeight = 1F;
            this.line26.Name = "line26";
            this.line26.Top = 0F;
            this.line26.Width = 0F;
            this.line26.X1 = 0.003937247F;
            this.line26.X2 = 0.003937247F;
            this.line26.Y1 = 0F;
            this.line26.Y2 = 0.1771653F;
            // 
            // line27
            // 
            this.line27.Height = 0.1771653F;
            this.line27.Left = 7.454333F;
            this.line27.LineWeight = 1F;
            this.line27.Name = "line27";
            this.line27.Top = 0F;
            this.line27.Width = 0F;
            this.line27.X1 = 7.454333F;
            this.line27.X2 = 7.454333F;
            this.line27.Y1 = 0F;
            this.line27.Y2 = 0.1771653F;
            // 
            // line28
            // 
            this.line28.Height = 0F;
            this.line28.Left = 2.384186E-07F;
            this.line28.LineWeight = 1F;
            this.line28.Name = "line28";
            this.line28.Top = 0.1771653F;
            this.line28.Width = 7.460632F;
            this.line28.X1 = 2.384186E-07F;
            this.line28.X2 = 7.460632F;
            this.line28.Y1 = 0.1771653F;
            this.line28.Y2 = 0.1771653F;
            // 
            // lblGokei
            // 
            this.lblGokei.Height = 0.1767717F;
            this.lblGokei.HyperLink = null;
            this.lblGokei.Left = 1.666929F;
            this.lblGokei.Name = "lblGokei";
            this.lblGokei.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; text-align: center";
            this.lblGokei.Text = "【　合　計　】";
            this.lblGokei.Top = 0F;
            this.lblGokei.Width = 1.53125F;
            // 
            // txtSuryoKei
            // 
            this.txtSuryoKei.DataField = "ITEM06";
            this.txtSuryoKei.Height = 0.1770833F;
            this.txtSuryoKei.Left = 3.635433F;
            this.txtSuryoKei.Name = "txtSuryoKei";
            this.txtSuryoKei.OutputFormat = resources.GetString("txtSuryoKei.OutputFormat");
            this.txtSuryoKei.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; text-align: right; vertical-align: middl" +
    "e";
            this.txtSuryoKei.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtSuryoKei.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtSuryoKei.Text = "suryo";
            this.txtSuryoKei.Top = 0F;
            this.txtSuryoKei.Width = 0.698032F;
            // 
            // KOBR2061R
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.3937007F;
            this.PageSettings.Margins.Left = 0.3937008F;
            this.PageSettings.Margins.Right = 0.3937008F;
            this.PageSettings.Margins.Top = 0.3937007F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Portrait;
            this.PageSettings.PaperHeight = 11.69291F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.267716F;
            this.PrintWidth = 7.459482F;
            this.Sections.Add(this.reportHeader1);
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.pageFooter);
            this.Sections.Add(this.reportFooter1);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKaishaNm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRank)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRank)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblShohinNm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSuryo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTanka)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKingaku)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblShohizei)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKingakuKei)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShohinNm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSuryo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTanka)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKingaku)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShohizei)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKingakukei)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rptDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKingakuGokei)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKingakuKeiKei)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGokei)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSuryoKei)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKaishaNm;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
        private GrapeCity.ActiveReports.SectionReportModel.Line line2;
        private GrapeCity.ActiveReports.SectionReportModel.Line line3;
        private GrapeCity.ActiveReports.SectionReportModel.Line line4;
        private GrapeCity.ActiveReports.SectionReportModel.Line line5;
        private GrapeCity.ActiveReports.SectionReportModel.Line line6;
        private GrapeCity.ActiveReports.SectionReportModel.Line line7;
        private GrapeCity.ActiveReports.SectionReportModel.Line line8;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblRank;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRank;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblCd;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblShohinNm;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSuryo;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTanka;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblKingaku;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblShohizei;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblKingakuKei;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCd;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtShohinNm;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSuryo;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTanka;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKingaku;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtShohizei;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKingakukei;
        private GrapeCity.ActiveReports.SectionReportModel.ReportInfo rptDate;
        private GrapeCity.ActiveReports.SectionReportModel.Line line9;
        private GrapeCity.ActiveReports.SectionReportModel.Line line10;
        private GrapeCity.ActiveReports.SectionReportModel.Line line11;
        private GrapeCity.ActiveReports.SectionReportModel.Line line12;
        private GrapeCity.ActiveReports.SectionReportModel.Line line13;
        private GrapeCity.ActiveReports.SectionReportModel.Line line14;
        private GrapeCity.ActiveReports.SectionReportModel.Line line15;
        private GrapeCity.ActiveReports.SectionReportModel.Line line16;
        private GrapeCity.ActiveReports.SectionReportModel.Line line17;
        private GrapeCity.ActiveReports.SectionReportModel.Line line18;
        private GrapeCity.ActiveReports.SectionReportModel.ReportHeader reportHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.ReportFooter reportFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKingakuGokei;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKingakuKeiKei;
        private GrapeCity.ActiveReports.SectionReportModel.Line line19;
        private GrapeCity.ActiveReports.SectionReportModel.Line line20;
        private GrapeCity.ActiveReports.SectionReportModel.Line line21;
        private GrapeCity.ActiveReports.SectionReportModel.Line line22;
        private GrapeCity.ActiveReports.SectionReportModel.Line line23;
        private GrapeCity.ActiveReports.SectionReportModel.Line line24;
        private GrapeCity.ActiveReports.SectionReportModel.Line line25;
        private GrapeCity.ActiveReports.SectionReportModel.Line line26;
        private GrapeCity.ActiveReports.SectionReportModel.Line line27;
        private GrapeCity.ActiveReports.SectionReportModel.Line line28;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblGokei;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSuryoKei;
    }
}
