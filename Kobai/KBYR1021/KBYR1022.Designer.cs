﻿namespace jp.co.fsi.kb.kbyr1021
{
    partial class KBYR1022
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbxAll = new System.Windows.Forms.GroupBox();
            this.btnLMove = new System.Windows.Forms.Button();
            this.btnRMove = new System.Windows.Forms.Button();
            this.gbxShuturyokuCd = new System.Windows.Forms.GroupBox();
            this.lbxSentakuKomoku = new System.Windows.Forms.ListBox();
            this.gbxGyoshuIchiran = new System.Windows.Forms.GroupBox();
            this.lbxShukeiKubun = new System.Windows.Forms.ListBox();
            this.gbxJuni = new System.Windows.Forms.GroupBox();
            this.rdoKojun = new System.Windows.Forms.RadioButton();
            this.rdoShojun = new System.Windows.Forms.RadioButton();
            this.rdoNashi = new System.Windows.Forms.RadioButton();
            this.lblTitleCd = new System.Windows.Forms.Label();
            this.txtTitleNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.nudTitleCd = new System.Windows.Forms.NumericUpDown();
            this.pnlDebug.SuspendLayout();
            this.gbxAll.SuspendLayout();
            this.gbxShuturyokuCd.SuspendLayout();
            this.gbxGyoshuIchiran.SuspendLayout();
            this.gbxJuni.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudTitleCd)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Size = new System.Drawing.Size(540, 23);
            this.lblTitle.Text = "";
            // 
            // btnF1
            // 
            this.btnF1.Text = "F1";
            // 
            // btnF2
            // 
            this.btnF2.Text = "F2";
            // 
            // btnF3
            // 
            this.btnF3.Text = "F3";
            // 
            // btnF4
            // 
            this.btnF4.Text = "F4";
            // 
            // btnF5
            // 
            this.btnF5.Text = "F5";
            // 
            // btnF6
            // 
            this.btnF6.Text = "F6";
            // 
            // btnF12
            // 
            this.btnF12.Text = "F12";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(5, 395);
            this.pnlDebug.Size = new System.Drawing.Size(557, 100);
            // 
            // gbxAll
            // 
            this.gbxAll.Controls.Add(this.btnLMove);
            this.gbxAll.Controls.Add(this.btnRMove);
            this.gbxAll.Controls.Add(this.gbxShuturyokuCd);
            this.gbxAll.Controls.Add(this.gbxGyoshuIchiran);
            this.gbxAll.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxAll.Location = new System.Drawing.Point(12, 67);
            this.gbxAll.Name = "gbxAll";
            this.gbxAll.Size = new System.Drawing.Size(524, 323);
            this.gbxAll.TabIndex = 5;
            this.gbxAll.TabStop = false;
            // 
            // btnLMove
            // 
            this.btnLMove.BackColor = System.Drawing.Color.Silver;
            this.btnLMove.Font = new System.Drawing.Font("ＭＳ ゴシック", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnLMove.Location = new System.Drawing.Point(245, 174);
            this.btnLMove.Name = "btnLMove";
            this.btnLMove.Size = new System.Drawing.Size(44, 39);
            this.btnLMove.TabIndex = 3;
            this.btnLMove.Text = "＜";
            this.btnLMove.UseVisualStyleBackColor = false;
            this.btnLMove.Click += new System.EventHandler(this.btnLMove_Click);
            // 
            // btnRMove
            // 
            this.btnRMove.BackColor = System.Drawing.Color.Silver;
            this.btnRMove.Font = new System.Drawing.Font("ＭＳ ゴシック", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnRMove.Location = new System.Drawing.Point(245, 120);
            this.btnRMove.Name = "btnRMove";
            this.btnRMove.Size = new System.Drawing.Size(44, 39);
            this.btnRMove.TabIndex = 2;
            this.btnRMove.Text = "＞";
            this.btnRMove.UseVisualStyleBackColor = false;
            this.btnRMove.Click += new System.EventHandler(this.btnRMove_Click);
            // 
            // gbxShuturyokuCd
            // 
            this.gbxShuturyokuCd.Controls.Add(this.lbxSentakuKomoku);
            this.gbxShuturyokuCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxShuturyokuCd.ForeColor = System.Drawing.SystemColors.ControlText;
            this.gbxShuturyokuCd.Location = new System.Drawing.Point(300, 15);
            this.gbxShuturyokuCd.Name = "gbxShuturyokuCd";
            this.gbxShuturyokuCd.Size = new System.Drawing.Size(215, 299);
            this.gbxShuturyokuCd.TabIndex = 1;
            this.gbxShuturyokuCd.TabStop = false;
            this.gbxShuturyokuCd.Text = "選択済み";
            // 
            // lbxSentakuKomoku
            // 
            this.lbxSentakuKomoku.FormattingEnabled = true;
            this.lbxSentakuKomoku.Location = new System.Drawing.Point(14, 20);
            this.lbxSentakuKomoku.Name = "lbxSentakuKomoku";
            this.lbxSentakuKomoku.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.lbxSentakuKomoku.Size = new System.Drawing.Size(179, 264);
            this.lbxSentakuKomoku.TabIndex = 1;
            // 
            // gbxGyoshuIchiran
            // 
            this.gbxGyoshuIchiran.Controls.Add(this.lbxShukeiKubun);
            this.gbxGyoshuIchiran.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxGyoshuIchiran.ForeColor = System.Drawing.SystemColors.ControlText;
            this.gbxGyoshuIchiran.Location = new System.Drawing.Point(17, 15);
            this.gbxGyoshuIchiran.Name = "gbxGyoshuIchiran";
            this.gbxGyoshuIchiran.Size = new System.Drawing.Size(216, 299);
            this.gbxGyoshuIchiran.TabIndex = 0;
            this.gbxGyoshuIchiran.TabStop = false;
            this.gbxGyoshuIchiran.Text = "集計区分";
            // 
            // lbxShukeiKubun
            // 
            this.lbxShukeiKubun.FormattingEnabled = true;
            this.lbxShukeiKubun.Location = new System.Drawing.Point(12, 20);
            this.lbxShukeiKubun.Name = "lbxShukeiKubun";
            this.lbxShukeiKubun.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.lbxShukeiKubun.Size = new System.Drawing.Size(179, 264);
            this.lbxShukeiKubun.TabIndex = 0;
            // 
            // gbxJuni
            // 
            this.gbxJuni.Controls.Add(this.rdoKojun);
            this.gbxJuni.Controls.Add(this.rdoShojun);
            this.gbxJuni.Controls.Add(this.rdoNashi);
            this.gbxJuni.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxJuni.ForeColor = System.Drawing.Color.Black;
            this.gbxJuni.Location = new System.Drawing.Point(358, 14);
            this.gbxJuni.Name = "gbxJuni";
            this.gbxJuni.Size = new System.Drawing.Size(178, 47);
            this.gbxJuni.TabIndex = 4;
            this.gbxJuni.TabStop = false;
            this.gbxJuni.Text = "順位";
            this.gbxJuni.Visible = false;
            // 
            // rdoKojun
            // 
            this.rdoKojun.AutoSize = true;
            this.rdoKojun.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoKojun.ForeColor = System.Drawing.Color.Black;
            this.rdoKojun.Location = new System.Drawing.Point(118, 19);
            this.rdoKojun.Name = "rdoKojun";
            this.rdoKojun.Size = new System.Drawing.Size(53, 17);
            this.rdoKojun.TabIndex = 2;
            this.rdoKojun.TabStop = true;
            this.rdoKojun.Text = "降順";
            this.rdoKojun.UseVisualStyleBackColor = true;
            // 
            // rdoShojun
            // 
            this.rdoShojun.AutoSize = true;
            this.rdoShojun.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoShojun.ForeColor = System.Drawing.Color.Black;
            this.rdoShojun.Location = new System.Drawing.Point(59, 19);
            this.rdoShojun.Name = "rdoShojun";
            this.rdoShojun.Size = new System.Drawing.Size(53, 17);
            this.rdoShojun.TabIndex = 1;
            this.rdoShojun.TabStop = true;
            this.rdoShojun.Text = "昇順";
            this.rdoShojun.UseVisualStyleBackColor = true;
            // 
            // rdoNashi
            // 
            this.rdoNashi.AutoSize = true;
            this.rdoNashi.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoNashi.ForeColor = System.Drawing.Color.Black;
            this.rdoNashi.Location = new System.Drawing.Point(14, 19);
            this.rdoNashi.Name = "rdoNashi";
            this.rdoNashi.Size = new System.Drawing.Size(39, 17);
            this.rdoNashi.TabIndex = 0;
            this.rdoNashi.TabStop = true;
            this.rdoNashi.Text = "無";
            this.rdoNashi.UseVisualStyleBackColor = true;
            // 
            // lblTitleCd
            // 
            this.lblTitleCd.BackColor = System.Drawing.Color.Silver;
            this.lblTitleCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTitleCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTitleCd.ForeColor = System.Drawing.Color.Black;
            this.lblTitleCd.Location = new System.Drawing.Point(12, 22);
            this.lblTitleCd.Name = "lblTitleCd";
            this.lblTitleCd.Size = new System.Drawing.Size(319, 24);
            this.lblTitleCd.TabIndex = 3;
            this.lblTitleCd.Text = "グループ";
            this.lblTitleCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTitleNm
            // 
            this.txtTitleNm.AutoSizeFromLength = false;
            this.txtTitleNm.DisplayLength = null;
            this.txtTitleNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTitleNm.ForeColor = System.Drawing.Color.Black;
            this.txtTitleNm.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtTitleNm.Location = new System.Drawing.Point(125, 23);
            this.txtTitleNm.MaxLength = 20;
            this.txtTitleNm.Name = "txtTitleNm";
            this.txtTitleNm.Size = new System.Drawing.Size(202, 20);
            this.txtTitleNm.TabIndex = 1;
            this.txtTitleNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtTitleNm_Validating);
            // 
            // nudTitleCd
            // 
            this.nudTitleCd.Location = new System.Drawing.Point(79, 24);
            this.nudTitleCd.Maximum = new decimal(new int[] {
            7,
            0,
            0,
            0});
            this.nudTitleCd.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudTitleCd.Name = "nudTitleCd";
            this.nudTitleCd.Size = new System.Drawing.Size(45, 19);
            this.nudTitleCd.TabIndex = 2;
            this.nudTitleCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nudTitleCd.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudTitleCd.ValueChanged += new System.EventHandler(this.nudTitleCd_ValueChanged);
            this.nudTitleCd.KeyUp += new System.Windows.Forms.KeyEventHandler(this.nudTitleCd_KeyUp);
            // 
            // KBYR1022
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(549, 498);
            this.Controls.Add(this.nudTitleCd);
            this.Controls.Add(this.txtTitleNm);
            this.Controls.Add(this.lblTitleCd);
            this.Controls.Add(this.gbxJuni);
            this.Controls.Add(this.gbxAll);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "KBYR1022";
            this.ShowFButton = true;
            this.Text = "グループ設定";
            this.Controls.SetChildIndex(this.gbxAll, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.gbxJuni, 0);
            this.Controls.SetChildIndex(this.lblTitleCd, 0);
            this.Controls.SetChildIndex(this.txtTitleNm, 0);
            this.Controls.SetChildIndex(this.nudTitleCd, 0);
            this.pnlDebug.ResumeLayout(false);
            this.gbxAll.ResumeLayout(false);
            this.gbxShuturyokuCd.ResumeLayout(false);
            this.gbxGyoshuIchiran.ResumeLayout(false);
            this.gbxJuni.ResumeLayout(false);
            this.gbxJuni.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudTitleCd)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gbxAll;
        private System.Windows.Forms.Button btnLMove;
        private System.Windows.Forms.Button btnRMove;
        private System.Windows.Forms.GroupBox gbxShuturyokuCd;
        private System.Windows.Forms.GroupBox gbxGyoshuIchiran;
        private System.Windows.Forms.ListBox lbxSentakuKomoku;
        private System.Windows.Forms.ListBox lbxShukeiKubun;
        private System.Windows.Forms.GroupBox gbxJuni;
        private System.Windows.Forms.RadioButton rdoKojun;
        private System.Windows.Forms.RadioButton rdoShojun;
        private System.Windows.Forms.RadioButton rdoNashi;
        private System.Windows.Forms.Label lblTitleCd;
        private jp.co.fsi.common.controls.FsiTextBox txtTitleNm;
        private System.Windows.Forms.NumericUpDown nudTitleCd;




    }
}