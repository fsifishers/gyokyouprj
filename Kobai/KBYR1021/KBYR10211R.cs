﻿using System;
using System.Data;

using jp.co.fsi.common.report;
using jp.co.fsi.common.util;

namespace jp.co.fsi.kb.kbyr1021
{
    /// <summary>
    /// KBYR10211R
    /// </summary>
    public partial class KBYR10211R : BaseReport
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="tgtData">出力対象データ</param>
        public KBYR10211R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }
        public KBYR10211R(DataTable tgtData, string repID) : base(tgtData, repID)
        {
            InitializeComponent();
        }

        /// <summary>
        /// ページヘッダーの設定
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pageHeader_Format(object sender, EventArgs e)
        {
            //txtToday.Text = DateTime.Now.ToString("yyyy/MM/dd");
        }
    }
}
