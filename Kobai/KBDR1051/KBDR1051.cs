﻿using System;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

using GrapeCity.ActiveReports;

using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.kb.kbdr1051
{
    /// <summary>
    /// 船主別商品売上順位表(KBDR1051)
    /// </summary>
    public partial class KBDR1051 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// 資材返品
        /// </summary>
        private const string KAKE_HENPIN = "2";

        /// <summary>
        /// 印刷ワーク更新用列数
        /// </summary>
        private const int prtCols = 12;
        #endregion

        #region プロパティ
        /// <summary>
        /// 画面上最後となるフォーカスのEnterボタン押下時処理用変数
        /// </summary>
        private bool _dtFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._dtFlg;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KBDR1051()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 水揚支所
#if DEBUG
            this.txtMizuageShishoCd.Text = "1";
#else
            this.txtMizuageShishoCd.Text = Uinfo.shishoCd;
#endif
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
            this.txtMizuageShishoCd.Enabled = (this.txtMizuageShishoCd.Text == "1") ? true : false;

            string[] jpDate = Util.ConvJpDate(DateTime.Now, this.Dba);
            lblDateGengoFr.Text = jpDate[0];
            txtDateYearFr.Text = jpDate[2];
            txtDateMonthFr.Text = jpDate[3];
            txtDateDayFr.Text = jpDate[4];
            lblDateGengoTo.Text = jpDate[0];
            txtDateYearTo.Text = jpDate[2];
            txtDateMonthTo.Text = jpDate[3];
            txtDateDayTo.Text = jpDate[4];

            // 初期フォーカス
            txtDateYearFr.Focus();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 日付、船主コード１・２に
            // フォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd":
                case "txtDateYearFr":
                case "txtDateYearTo":
                case "txtFunanushiCdFr":
                case "txtFunanushiCdTo":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;
            String[] result;

            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd": // 水揚支所
                    #region 水揚支所
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM2031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2031.CMCM2031");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.InData = this.txtMizuageShishoCd.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.txtMizuageShishoCd.Text = result[0];
                                this.lblMizuageShishoNm.Text = result[1];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtFunanushiCdFr":
                    #region 船主CD
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM2011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2011.CMCM2011");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtFunanushiCdFr.Text = outData[0];
                                this.lblFunanushiCdFr.Text = outData[1];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtFunanushiCdTo":
                    #region 船主CD
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM2011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2011.CMCM2011");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtFunanushiCdTo.Text = outData[0];
                                this.lblFunanushiCdTo.Text = outData[1];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtDateYearFr":
                    #region 元号
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblDateGengoFr.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (string[])frm.OutData;
                                this.lblDateGengoFr.Text = result[1];

                                // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
                                DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                                    this.txtDateMonthFr.Text, "1", this.Dba);
                                int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);
                                if (Util.ToInt(this.txtDateDayFr.Text) > lastDayInMonth)
                                {
                                    this.txtDateDayFr.Text = Util.ToString(lastDayInMonth);
                                }

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblDateGengoFr.Text,
                                        this.txtDateYearFr.Text,
                                        this.txtDateMonthFr.Text,
                                        this.txtDateDayFr.Text,
                                        this.Dba);
                                this.lblDateGengoFr.Text = arrJpDate[0];
                                this.txtDateYearFr.Text = arrJpDate[2];
                                this.txtDateMonthFr.Text = arrJpDate[3];
                                this.txtDateDayFr.Text = arrJpDate[4];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtDateYearTo":
                    #region 元号
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\" + "CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblDateGengoTo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (string[])frm.OutData;
                                this.lblDateGengoTo.Text = result[1];

                                // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
                                DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                                    this.txtDateMonthTo.Text, "1", this.Dba);
                                int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);
                                if (Util.ToInt(this.txtDateDayTo.Text) > lastDayInMonth)
                                {
                                    this.txtDateDayTo.Text = Util.ToString(lastDayInMonth);
                                }

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblDateGengoTo.Text,
                                        this.txtDateYearTo.Text,
                                        this.txtDateMonthTo.Text,
                                        this.txtDateDayTo.Text,
                                        this.Dba);
                                this.lblDateGengoTo.Text = arrJpDate[0];
                                this.txtDateYearTo.Text = arrJpDate[2];
                                this.txtDateMonthTo.Text = arrJpDate[3];
                                this.txtDateDayTo.Text = arrJpDate[4];
                            }
                        }
                    }
                    #endregion
                    break;
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
            {
                // プレビュー処理
                DoPrint(true);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("印刷", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }

        /// <summary>
        /// F6キー押下時処理
        /// PDF出力
        /// </summary>
        public override void PressF6()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("PDF出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, true);
            }
        }

        /// <summary>
        /// F7キー押下時処理
        /// EXCEL出力
        /// </summary>
        public override void PressF7()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("EXCEL出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, false, true);
            }
        }

        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            // 設定画面の起動
            // MEMO:原則としてここで渡す帳票IDの設定はReport.csvに保持していることが前提ですが、
            // 保持していない場合は、設定画面での保存(F6)時に新規に設定が保持されます。
            PrintSettingForm psForm = new PrintSettingForm(new string[1] { "KBDR1051R" });
            psForm.ShowDialog();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 水揚支所入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMizuageShishoCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                e.Cancel = true;
                this.txtMizuageShishoCd.SelectAll();
                this.txtMizuageShishoCd.Focus();
            }
        }

        /// <summary>
        /// 船主コード(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFunanushiCdFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidFunanushiCdFr())
            {
                e.Cancel = true;
                this.txtFunanushiCdFr.SelectAll();
            }
        }

        /// <summary>
        /// 船主コード(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFunanushiCdTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidFunanushiCdTo())
            {
                e.Cancel = true;
                this.txtFunanushiCdTo.SelectAll();

                // Enter処理を無効化
                this._dtFlg = false;
            }
            else
            {
                // Enter処理を有効化
                this._dtFlg = true;
            }
        }

        /// <summary>
        /// 和暦(年)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateYearFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtDateYearFr.Text, this.txtDateYearFr.MaxLength))
            {
                e.Cancel = true;
                this.txtDateYearFr.SelectAll();
            }
            else
            {
                this.txtDateYearFr.Text = Util.ToString(IsValid.SetYear(this.txtDateYearFr.Text));
                CheckDateFr();
                SetDateFr();
            }
        }

        /// <summary>
        /// 和暦(月)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateMonthFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtDateMonthFr.Text, this.txtDateMonthFr.MaxLength))
            {
                e.Cancel = true;
                this.txtDateMonthFr.SelectAll();
            }
            else
            {
                this.txtDateMonthFr.Text = Util.ToString(IsValid.SetMonth(this.txtDateMonthFr.Text));
                CheckDateFr();
                SetDateFr();
            }
        }

        /// <summary>
        /// 和暦(日)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateDayFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsDay(this.txtDateDayFr.Text, this.txtDateDayFr.MaxLength))
            {
                e.Cancel = true;
                this.txtDateDayFr.SelectAll();
            }
            else
            {
                this.txtDateDayFr.Text = Util.ToString(IsValid.SetDay(this.txtDateDayFr.Text));
                CheckDateFr();
                SetDateFr();
            }
        }

        /// <summary>
        /// 和暦(年)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateYearTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtDateYearTo.Text, this.txtDateYearTo.MaxLength))
            {
                e.Cancel = true;
                this.txtDateYearTo.SelectAll();
            }
            else
            {
                this.txtDateYearTo.Text = Util.ToString(IsValid.SetYear(this.txtDateYearTo.Text));
                CheckDateTo();
                SetDateTo();
            }
        }

        /// <summary>
        /// 和暦(月)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateMonthTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtDateMonthTo.Text, this.txtDateMonthTo.MaxLength))
            {
                e.Cancel = true;
                this.txtDateMonthTo.SelectAll();
            }
            else
            {
                this.txtDateMonthTo.Text = Util.ToString(IsValid.SetMonth(this.txtDateMonthTo.Text));
                CheckDateTo();
                SetDateTo();
            }
        }

        /// <summary>
        /// 和暦(日)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateDayTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsDay(this.txtDateDayTo.Text, this.txtDateDayTo.MaxLength))
            {
                e.Cancel = true;
                this.txtDateDayTo.SelectAll();
            }
            else
            {
                this.txtDateDayTo.Text = Util.ToString(IsValid.SetDay(this.txtDateDayTo.Text));
                CheckDateTo();
                SetDateTo();
            }
        }

        /// <summary>
        /// 売上順位範囲指定コード(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtUriRankFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidUriRankFr())
            {
                e.Cancel = true;
                this.txtUriRankFr.SelectAll();
            }
        }

        /// <summary>
        /// 売上順位範囲指定コード(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtUriRankTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidUriRankTo())
            {
                e.Cancel = true;
                this.txtUriRankTo.SelectAll();
            }
        }

        /// <summary>
        /// 船主CD(至)のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFunanushiCdTo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.Flg)
            {
                // Enter処理を無効化
                this._dtFlg = false;

                // 全項目を再度入力値チェック
                if (!ValidateAll())
                {
                    // エラーありの場合ここで処理終了
                    return;
                }

                if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
                {
                    // ﾌﾟﾚﾋﾞｭｰ処理
                    DoPrint(true);
                }
                else
                {
                    this.txtFunanushiCdTo.Focus();
                }
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 水揚支所の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool IsValidMizuageShishoCd()
        {
            // 空 又は 0入力の場合
            if (ValChk.IsEmpty(this.txtMizuageShishoCd.Text) || Equals(this.txtMizuageShishoCd.Text, "0"))
            {
                // 水揚支所名称を表示する
                this.txtMizuageShishoCd.Text = "0";
                this.lblMizuageShishoNm.Text = "全て";
                return true;
            }
            // 水揚支所名称を表示する
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
            if (ValChk.IsEmpty(this.lblMizuageShishoNm.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }
            return true;
        }

        /// <summary>
        /// 年月日(自)の月末入力チェック
        /// </summary>
        /// 
        private void CheckDateFr()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                this.txtDateMonthFr.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDateDayFr.Text) > lastDayInMonth)
            {
                this.txtDateDayFr.Text = Util.ToString(lastDayInMonth);
            }
        }
        /// <summary>
        /// 年月日(自)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetDateFr()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDateFr(Util.FixJpDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                this.txtDateMonthFr.Text, this.txtDateDayFr.Text, this.Dba));
        }

        /// <summary>
        /// 年月日(至)の月末入力チェック
        /// </summary>
        /// 
        private void CheckDateTo()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                this.txtDateMonthTo.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDateDayTo.Text) > lastDayInMonth)
            {
                this.txtDateDayTo.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(至)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetDateTo()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDateTo(Util.FixJpDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                this.txtDateMonthTo.Text, this.txtDateDayTo.Text, this.Dba));
        }

        /// <summary>
        /// 船主コード(自)の入力チェック
        /// </summary>
        private bool IsValidFunanushiCdFr()
        {
            if (ValChk.IsEmpty(this.txtFunanushiCdFr.Text))
            {
                this.lblFunanushiCdFr.Text = "先　頭";
            }
            else
                // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
                if (!ValChk.IsNumber(this.txtFunanushiCdFr.Text))
                {
                    Msg.Error("船主コードは数値のみで入力してください。");
                    return false;
                }
                else
                {
                    // コードを元に名称を取得する
                    this.lblFunanushiCdFr.Text = this.Dba.GetName(this.UInfo, "VI_HN_FUNANUSHI", this.txtMizuageShishoCd.Text, this.txtFunanushiCdFr.Text);
                }

            return true;
        }

        /// <summary>
        /// 船主コード(至)の入力チェック
        /// </summary>
        private bool IsValidFunanushiCdTo()
        {
            if (ValChk.IsEmpty(this.txtFunanushiCdTo.Text))
            {
                this.lblFunanushiCdTo.Text = "最　後";
            }
            else
                // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
                if (!ValChk.IsNumber(this.txtFunanushiCdTo.Text))
                {
                    Msg.Error("船主コードは数値のみで入力してください。");
                    return false;
                }
                else
                {
                    // コードを元に名称を取得する
                    this.lblFunanushiCdTo.Text = this.Dba.GetName(this.UInfo, "VI_HN_FUNANUSHI", this.txtMizuageShishoCd.Text, this.txtFunanushiCdTo.Text);
                }

            return true;
        }

        /// <summary>
        /// 売上順位(自)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidUriRankFr()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtUriRankFr.Text))
            {
                Msg.Error("売上順位は数値のみで入力してください。");
                return false;
            }
            return true;
        }

        /// <summary>
        /// 売上順位(至)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidUriRankTo()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtUriRankTo.Text))
            {
                Msg.Error("売上順位は数値のみで入力してください。");
                return false;
            }
            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 水揚支所の入力チェック
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                this.txtMizuageShishoCd.Focus();
                this.txtMizuageShishoCd.SelectAll();
                return false;
            }
            // 年(自)のチェック
            if (!IsValid.IsYear(this.txtDateYearFr.Text, this.txtDateYearFr.MaxLength))
            {
                this.txtDateYearFr.Focus();
                this.txtDateYearFr.SelectAll();
                return false;
            }
            // 月(自)のチェック
            if (!IsValid.IsMonth(this.txtDateMonthFr.Text, this.txtDateMonthFr.MaxLength))
            {
                this.txtDateMonthFr.Focus();
                this.txtDateMonthFr.SelectAll();
                return false;
            }
            // 日(自)のチェック
            if (!IsValid.IsDay(this.txtDateDayFr.Text, this.txtDateDayFr.MaxLength))
            {
                this.txtDateDayFr.Focus();
                this.txtDateDayFr.SelectAll();
                return false;
            }
            // 年月日(自)の月末入力チェック処理
            CheckDateFr();
            // 年月日(自)の正しい和暦への変換処理
            SetDateFr();

            // 年(至)のチェック
            if (!IsValid.IsYear(this.txtDateYearTo.Text, this.txtDateYearTo.MaxLength))
            {
                this.txtDateYearTo.Focus();
                this.txtDateYearTo.SelectAll();
                return false;
            }
            // 月(至)のチェック
            if (!IsValid.IsMonth(this.txtDateMonthTo.Text, this.txtDateMonthTo.MaxLength))
            {
                this.txtDateMonthTo.Focus();
                this.txtDateMonthTo.SelectAll();
                return false;
            }
            // 日(至)のチェック
            if (!IsValid.IsDay(this.txtDateDayTo.Text, this.txtDateDayTo.MaxLength))
            {
                this.txtDateDayTo.Focus();
                this.txtDateDayTo.SelectAll();
                return false;
            }
            // 年月日(至)の月末入力チェック処理
            CheckDateTo();
            // 年月日(至)の正しい和暦への変換処理
            SetDateTo();

            // 船主コード(自)の入力チェック
            if (!IsValidFunanushiCdFr())
            {
                this.txtFunanushiCdFr.Focus();
                this.txtFunanushiCdFr.SelectAll();
                return false;
            }
            // 船主コード(至)の入力チェック
            if (!IsValidFunanushiCdTo())
            {
                this.txtFunanushiCdTo.Focus();
                this.txtFunanushiCdTo.SelectAll();
                return false;
            }

            // 売上順位(自)の入力チェック
            if (!IsValidUriRankFr())
            {
                this.txtUriRankFr.Focus();
                this.txtUriRankFr.SelectAll();
                return false;
            }
            // 売上順位(至)の入力チェック
            if (!IsValidUriRankTo())
            {
                this.txtUriRankTo.Focus();
                this.txtUriRankTo.SelectAll();
                return false;
            }
            return true;
        }


        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpDateFr(string[] arrJpDate)
        {
            this.lblDateGengoFr.Text = arrJpDate[0];
            this.txtDateYearFr.Text = arrJpDate[2];
            this.txtDateMonthFr.Text = arrJpDate[3];
            this.txtDateDayFr.Text = arrJpDate[4];
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpDateTo(string[] arrJpDate)
        {
            this.lblDateGengoTo.Text = arrJpDate[0];
            this.txtDateYearTo.Text = arrJpDate[2];
            this.txtDateMonthTo.Text = arrJpDate[3];
            this.txtDateDayTo.Text = arrJpDate[4];
        }

        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview, bool isPdf = false, bool isExcel = false, bool isCsv = false)
        {
            try
            {
#if DEBUG
                //印刷用ワークテーブルの削除
                this.Dba.DeleteWork("PR_HN_TBL", this.UnqId);
#endif

                this.Dba.BeginTransaction();

                // 帳票出力用にワークテーブルにデータを作成
                bool dataFlag = MakeWkData();

                // 帳票出力
                if (dataFlag)
                {
                    // 取得列の定義
                    StringBuilder cols = Util.ColsArray(prtCols, "");

                    // バインドパラメータの設定
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);

                    // データの取得
                    DataTable dtOutput = this.Dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_HN_TBL", "GUID = @GUID", "SORT ASC", dpc);

                    // 帳票オブジェクトをインスタンス化
                    KBDR1051R rpt = new KBDR1051R(dtOutput);
                    rpt.Document.Printer.DocumentName = this.lblTitle.Text;
                    rpt.Document.Name = this.lblTitle.Text;

                    if (isExcel)
                    {
                        GrapeCity.ActiveReports.Export.Excel.Section.XlsExport xlsExport1 = new GrapeCity.ActiveReports.Export.Excel.Section.XlsExport();
                        //SetExcelSetting(xlsExport1);
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Kob, rpt.Document.Name, 2);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            xlsExport1.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("EXCEL出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPdf)
                    {
                        GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport p = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Kob, rpt.Document.Name, 1);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            p.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("PDF出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPreview)
                    {
                        // 帳票オブジェクトをインスタンス化
                        PreviewForm pFrm = new PreviewForm(rpt, this.UnqId);
                        // プレビュー画面表示
                        pFrm.WindowState = FormWindowState.Maximized;
                        pFrm.Show();
                    }
                    else
                    {
                        // 直接印刷
                        rpt.Run(false);
                        rpt.Document.Print(true, true, false);
                    }
                }
            }
            finally
            {
#if DEBUG
                this.Dba.Commit();
#else
                this.Dba.Rollback();
#endif
            }
        }

        struct NumVals
        {
            public decimal suryo;
            public decimal gokei;

            public void Clear()
            {
                suryo = 0;
                gokei = 0;
            }
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData()
        {
            DbParamCollection dpc = new DbParamCollection();
            // 日付を西暦にして取得
            DateTime tmpDateFr = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                    this.txtDateMonthFr.Text, this.txtDateDayFr.Text, this.Dba);
            DateTime tmpDateTo = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                    this.txtDateMonthTo.Text, this.txtDateDayTo.Text, this.Dba);
            // 日付を和暦で保持
            string[] tmpjpDateFr = Util.ConvJpDate(tmpDateFr, this.Dba);
            string[] tmpjpDateTo = Util.ConvJpDate(tmpDateTo, this.Dba);
            int i; // ループ用カウント変数
            int rank; // 売上順位
            int hyojiRank;
            // 売上順位範囲設定
            int cRankFr;
            int cRankTo;
            if (Util.ToString(txtUriRankFr.Text) != "")
            {
                cRankFr = Util.ToInt(txtUriRankFr.Text);
            }
            else
            {
                cRankFr = 1;
            }
            if (Util.ToString(txtUriRankTo.Text) != "")
            {
                cRankTo = Util.ToInt(txtUriRankTo.Text);
            }
            else
            {
                cRankTo = 99999;
            }
            // 船主コード設定
            Decimal funanushiCdFr;
            Decimal funanushiCdTo;
            if (Util.ToString(txtFunanushiCdFr.Text) != "")
            {
                funanushiCdFr = Util.ToDecimal(txtFunanushiCdFr.Text);
            }
            else
            {
                funanushiCdFr = 0;
            }
            if (Util.ToString(txtFunanushiCdTo.Text) != "")
            {
                funanushiCdTo = Util.ToDecimal(txtFunanushiCdTo.Text);
            }
            else
            {
                funanushiCdTo = 9999;
            }
            string shishoCd = this.txtMizuageShishoCd.Text;

            // han.VI_取引明細(VI_HN_TORIHIKI_MEISAI)
            // の検索日付に発生しているデータを取得
            StringBuilder Sql = new StringBuilder();
            Sql.Append("SELECT");
            Sql.Append("    KAISHA_CD,");
            Sql.Append("    TOKUISAKI_CD AS KAIIN_BANGO,");
            Sql.Append("    TOKUISAKI_NM,");
            Sql.Append("    SHOHIN_CD,");
            Sql.Append("    MIN(SHOHIN_NM) AS SHOHIN_NM,");
            Sql.Append("    SUM(SURYO2) AS SURYO2,");
            Sql.Append("    MAX(URI_TANKA) AS URI_TANKA,");
            Sql.Append("    TORIHIKI_KUBUN1,");
            Sql.Append("    TORIHIKI_KUBUN2,");
            Sql.Append("    SUM(ZEINUKI_KINGAKU) AS ZEINUKI_KINGAKU,");
            Sql.Append("    SUM(SHOHIZEI) AS SHOHIZEI,");
            Sql.Append("    SUM(ZEINUKI_KINGAKU + SHOHIZEI) AS KINGAKU_KEI,");
            Sql.Append("    SHOHIZEI_TENKA_HOHO,");
            Sql.Append("    DENPYO_KUBUN");
            Sql.Append(" FROM");
            Sql.Append("    VI_HN_TORIHIKI_MEISAI");
            Sql.Append(" WHERE");
            Sql.Append("    KAISHA_CD   = @KAISHA_CD AND");
            if (shishoCd != "0")
                Sql.Append("    SHISHO_CD   = @SHISHO_CD AND");
            Sql.Append("    DENPYO_KUBUN   = 1 AND");
            Sql.Append("    DENPYO_DATE BETWEEN @DATE_FR AND @DATE_TO  AND");
            Sql.Append("    TOKUISAKI_CD  BETWEEN @FUNANUSHI_CD_FR AND @FUNANUSHI_CD_TO ");
            Sql.Append(" GROUP BY");
            Sql.Append("    KAISHA_CD,");
            if (shishoCd != "0")
                Sql.Append("    SHISHO_CD,");
            Sql.Append("    TOKUISAKI_CD,");
            Sql.Append("    TOKUISAKI_NM,");
            Sql.Append("    SHOHIN_CD,");
            Sql.Append("    TORIHIKI_KUBUN1,");
            Sql.Append("    TORIHIKI_KUBUN2,");
            Sql.Append("    SHOHIZEI_TENKA_HOHO,");
            Sql.Append("    DENPYO_KUBUN");
            Sql.Append(" ORDER BY");
            Sql.Append("    KAIIN_BANGO,");
            Sql.Append("    KINGAKU_KEI desc");

            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            // 検索する日付をセット
            dpc.SetParam("@DATE_FR", SqlDbType.VarChar, 10, tmpDateFr.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@DATE_TO", SqlDbType.VarChar, 10, tmpDateTo.Date.ToString("yyyy/MM/dd"));
            // 検索する船主コードをセット
            dpc.SetParam("@FUNANUSHI_CD_FR", SqlDbType.Decimal, 4, funanushiCdFr);
            dpc.SetParam("@FUNANUSHI_CD_TO", SqlDbType.Decimal, 4, funanushiCdTo);

            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);

            DataTable dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            if (dtMainLoop.Rows.Count == 0)
            {
                Msg.Info("該当データがありません。");
                return false;
            }
            else
            {
                dpc = new DbParamCollection();

                NumVals shokei = new NumVals(); ;
                NumVals sokei = new NumVals(); ;
                i = 1;
                rank = 1;
                hyojiRank = 1;
                string kaiinNo = "";
                string torihikiKubun = "";

                #region 印刷ワークテーブルに登録
                foreach (DataRow dr in dtMainLoop.Rows)
                {
                    if (ValChk.IsEmpty(kaiinNo))
                    {
                        dpc = new DbParamCollection();
                        Sql = new StringBuilder();
                        Sql.Append("INSERT INTO PR_HN_TBL(");
                        Sql.Append("  GUID");
                        Sql.Append(" ,SORT");
                        Sql.Append(" ,ITEM01");
                        Sql.Append(" ,ITEM02");
                        Sql.Append(" ,ITEM03");
                        Sql.Append(" ,ITEM04");
                        Sql.Append(" ,ITEM05");
                        Sql.Append(" ,ITEM06");
                        Sql.Append(") ");
                        Sql.Append("VALUES(");
                        Sql.Append("  @GUID");
                        Sql.Append(" ,@SORT");
                        Sql.Append(" ,@ITEM01");
                        Sql.Append(" ,@ITEM02");
                        Sql.Append(" ,@ITEM03");
                        Sql.Append(" ,@ITEM04");
                        Sql.Append(" ,@ITEM05");
                        Sql.Append(" ,@ITEM06");
                        Sql.Append(") ");

                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                        dpc.SetParam("@SORT", SqlDbType.VarChar, 4, i);
                        // ページヘッダーデータを設定
                        dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);
                        dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpjpDateFr[5]);
                        dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpjpDateTo[5]);
                        dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, "《 "+ Util.ToString(dr["KAIIN_BANGO"]) + "　" + dr["TOKUISAKI_NM"] + " 》");

                        this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                        i++;

                        // 会員番号の保持
                        kaiinNo = dr["KAIIN_BANGO"].ToString();
                    }

                    if (!ValChk.IsEmpty(kaiinNo) && !Util.ToString(dr["KAIIN_BANGO"]).Equals(kaiinNo))
                    {
                        Sql = new StringBuilder();
                        dpc = new DbParamCollection();
                        Sql.Append("INSERT INTO PR_HN_TBL(");
                        Sql.Append("  GUID");
                        Sql.Append(" ,SORT");
                        Sql.Append(" ,ITEM01");
                        Sql.Append(" ,ITEM02");
                        Sql.Append(" ,ITEM03");
                        Sql.Append(" ,ITEM04");
                        Sql.Append(" ,ITEM05");
                        Sql.Append(" ,ITEM06");
                        Sql.Append(" ,ITEM07");
                        Sql.Append(" ,ITEM08");
                        Sql.Append(" ,ITEM09");
                        Sql.Append(" ,ITEM10");
                        Sql.Append(" ,ITEM11");
                        Sql.Append(") ");
                        Sql.Append("VALUES(");
                        Sql.Append("  @GUID");
                        Sql.Append(" ,@SORT");
                        Sql.Append(" ,@ITEM01");
                        Sql.Append(" ,@ITEM02");
                        Sql.Append(" ,@ITEM03");
                        Sql.Append(" ,@ITEM04");
                        Sql.Append(" ,@ITEM05");
                        Sql.Append(" ,@ITEM06");
                        Sql.Append(" ,@ITEM07");
                        Sql.Append(" ,@ITEM08");
                        Sql.Append(" ,@ITEM09");
                        Sql.Append(" ,@ITEM10");
                        Sql.Append(" ,@ITEM11");
                        Sql.Append(") ");

                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                        dpc.SetParam("@SORT", SqlDbType.VarChar, 4, i);
                        // ページヘッダーデータを設定
                        dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);
                        dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpjpDateFr[5]);
                        dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpjpDateTo[5]);
                        // 数値にカンマをセット
                        string shokeiSu = shokei.suryo.ToString("N0");
                        string shokeiGo = shokei.gokei.ToString("N0");
                        // データを設定
                        dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, torihikiKubun);
                        dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, "【　小　　計　】");
                        dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, shokeiSu);
                        dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, shokeiGo);

                        this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                        // 小計をリセット
                        shokei.Clear();
                        i++;

                        dpc = new DbParamCollection();
                        Sql = new StringBuilder();
                        Sql.Append("INSERT INTO PR_HN_TBL(");
                        Sql.Append("  GUID");
                        Sql.Append(" ,SORT");
                        Sql.Append(" ,ITEM01");
                        Sql.Append(" ,ITEM02");
                        Sql.Append(" ,ITEM03");
                        Sql.Append(") ");
                        Sql.Append("VALUES(");
                        Sql.Append("  @GUID");
                        Sql.Append(" ,@SORT");
                        Sql.Append(" ,@ITEM01");
                        Sql.Append(" ,@ITEM02");
                        Sql.Append(" ,@ITEM03");
                        Sql.Append(") ");

                        // 空行登録 １つ
                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                        dpc.SetParam("@SORT", SqlDbType.VarChar, 4, i);
                        // ページヘッダーデータを設定
                        dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);
                        dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpjpDateFr[5]);
                        dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpjpDateTo[5]);

                        this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                        i++;

                        dpc = new DbParamCollection();
                        Sql = new StringBuilder();
                        Sql.Append("INSERT INTO PR_HN_TBL(");
                        Sql.Append("  GUID");
                        Sql.Append(" ,SORT");
                        Sql.Append(" ,ITEM01");
                        Sql.Append(" ,ITEM02");
                        Sql.Append(" ,ITEM03");
                        Sql.Append(" ,ITEM04");
                        Sql.Append(" ,ITEM05");
                        Sql.Append(" ,ITEM06");
                        Sql.Append(") ");
                        Sql.Append("VALUES(");
                        Sql.Append("  @GUID");
                        Sql.Append(" ,@SORT");
                        Sql.Append(" ,@ITEM01");
                        Sql.Append(" ,@ITEM02");
                        Sql.Append(" ,@ITEM03");
                        Sql.Append(" ,@ITEM04");
                        Sql.Append(" ,@ITEM05");
                        Sql.Append(" ,@ITEM06");
                        Sql.Append(") ");

                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                        dpc.SetParam("@SORT", SqlDbType.VarChar, 4, i);
                        // ページヘッダーデータを設定
                        dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);
                        dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpjpDateFr[5]);
                        dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpjpDateTo[5]);
                        dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, "");
                        dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, "《 " + Util.ToString(dr["KAIIN_BANGO"]) + "　" + dr["TOKUISAKI_NM"] + " 》");

                        this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                        i++;
                        // 順位のリセット
                        rank = 1;
                        hyojiRank = 1;
                        // 会員番号の保持
                        kaiinNo = dr["KAIIN_BANGO"].ToString();
                    }

                    if (cRankFr <= rank && cRankTo >= rank)
                    {
                        Sql = new StringBuilder();
                        dpc = new DbParamCollection();
                        Sql.Append("INSERT INTO PR_HN_TBL(");
                        Sql.Append("  GUID");
                        Sql.Append(" ,SORT");
                        Sql.Append(" ,ITEM01");
                        Sql.Append(" ,ITEM02");
                        Sql.Append(" ,ITEM03");
                        Sql.Append(" ,ITEM04");
                        Sql.Append(" ,ITEM05");
                        Sql.Append(" ,ITEM06");
                        Sql.Append(" ,ITEM07");
                        Sql.Append(" ,ITEM08");
                        Sql.Append(" ,ITEM09");
                        Sql.Append(" ,ITEM10");
                        Sql.Append(" ,ITEM11");
                        Sql.Append(") ");
                        Sql.Append("VALUES(");
                        Sql.Append("  @GUID");
                        Sql.Append(" ,@SORT");
                        Sql.Append(" ,@ITEM01");
                        Sql.Append(" ,@ITEM02");
                        Sql.Append(" ,@ITEM03");
                        Sql.Append(" ,@ITEM04");
                        Sql.Append(" ,@ITEM05");
                        Sql.Append(" ,@ITEM06");
                        Sql.Append(" ,@ITEM07");
                        Sql.Append(" ,@ITEM08");
                        Sql.Append(" ,@ITEM09");
                        Sql.Append(" ,@ITEM10");
                        Sql.Append(" ,@ITEM11");
                        Sql.Append(") ");

                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                        dpc.SetParam("@SORT", SqlDbType.VarChar, 4, i);
                        // ページヘッダーデータを設定
                        dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);
                        dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpjpDateFr[5]);
                        dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpjpDateTo[5]);

                        // 小数点以下が存在するデータの四捨五入
                        decimal suryo = Util.Round((decimal)dr["SURYO2"], 1);
                        decimal uriTanka = Util.Round((decimal)dr["URI_TANKA"], 1);
                        decimal zeinukiKingaku = (decimal)dr["ZEINUKI_KINGAKU"];
                        decimal shohizei = (decimal)dr["SHOHIZEI"];
                        decimal kingakuKei = (decimal)dr["KINGAKU_KEI"];
                        if (Util.ToString(dr["TORIHIKI_KUBUN2"]) == KAKE_HENPIN)
                        {
                            suryo = suryo * -1;
                            zeinukiKingaku = zeinukiKingaku * -1;
                            shohizei = shohizei * -1;
                        }
                        // 数値にカンマをセット
                        string suryo1 = suryo.ToString("N1");
                        string uriTanka1 = uriTanka.ToString("N1");
                        string zeinukiKingaku1 = zeinukiKingaku.ToString("N0");
                        string shohizei1 = shohizei.ToString("N0");
                        string kingakuKei1 = kingakuKei.ToString("N0");
                        if (Util.ToString(dr["SHOHIZEI_TENKA_HOHO"]) != "1")
                        {
                            shohizei1 = "";
                        }
                        //データを設定
                        dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, hyojiRank);
                        dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, dr["SHOHIN_CD"]);
                        dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, dr["SHOHIN_NM"]);
                        dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, suryo1);
                        dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, uriTanka1);
                        dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, zeinukiKingaku1);
                        dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, shohizei1);
                        dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, kingakuKei1);


                        this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                        i++;
                        hyojiRank++;
                    }
                        rank++;
                    

                    // 明細データから合計の値を足しこむ
                    if (Util.ToString(dr["TORIHIKI_KUBUN2"]) == KAKE_HENPIN)
                    {
                        shokei.suryo += Util.ToDecimal(dr["SURYO2"]) * -1;
                        shokei.gokei += Util.ToDecimal(dr["KINGAKU_KEI"]) * -1;
                        sokei.suryo += Util.ToDecimal(dr["SURYO2"]) * -1;
                        sokei.gokei += Util.ToDecimal(dr["KINGAKU_KEI"]) * -1;
                    }
                    else
                    {
                        shokei.suryo += Util.ToDecimal(dr["SURYO2"]);
                        shokei.gokei += Util.ToDecimal(dr["KINGAKU_KEI"]);
                        sokei.suryo += Util.ToDecimal(dr["SURYO2"]);
                        sokei.gokei += Util.ToDecimal(dr["KINGAKU_KEI"]);
                    }

                    // 会員番号の保持
                    kaiinNo = dr["KAIIN_BANGO"].ToString();
                }

                // ループ終了後に小計を登録
                Sql = new StringBuilder();
                dpc = new DbParamCollection();
                Sql.Append("INSERT INTO PR_HN_TBL(");
                Sql.Append("  GUID");
                Sql.Append(" ,SORT");
                Sql.Append(" ,ITEM01");
                Sql.Append(" ,ITEM02");
                Sql.Append(" ,ITEM03");
                Sql.Append(" ,ITEM04");
                Sql.Append(" ,ITEM05");
                Sql.Append(" ,ITEM06");
                Sql.Append(" ,ITEM07");
                Sql.Append(" ,ITEM08");
                Sql.Append(" ,ITEM09");
                Sql.Append(" ,ITEM10");
                Sql.Append(" ,ITEM11");
                Sql.Append(") ");
                Sql.Append("VALUES(");
                Sql.Append("  @GUID");
                Sql.Append(" ,@SORT");
                Sql.Append(" ,@ITEM01");
                Sql.Append(" ,@ITEM02");
                Sql.Append(" ,@ITEM03");
                Sql.Append(" ,@ITEM04");
                Sql.Append(" ,@ITEM05");
                Sql.Append(" ,@ITEM06");
                Sql.Append(" ,@ITEM07");
                Sql.Append(" ,@ITEM08");
                Sql.Append(" ,@ITEM09");
                Sql.Append(" ,@ITEM10");
                Sql.Append(" ,@ITEM11");
                Sql.Append(") ");

                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                dpc.SetParam("@SORT", SqlDbType.VarChar, 4, i);
                // ページヘッダーデータを設定
                dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);
                dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpjpDateFr[5]);
                dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpjpDateTo[5]);
                // 数値にカンマをセット
                string shokeiSu2 = shokei.suryo.ToString("N0");
                string shokeiGo2 = shokei.gokei.ToString("N0");
                // データを設定
                dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, "");
                dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, torihikiKubun);
                dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, "【　小　　計　】");
                dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, shokeiSu2);
                dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, "");
                dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, "");
                dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, "");
                dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, shokeiGo2);

                this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                // 小計をリセット
                shokei.Clear();
                i++;

                dpc = new DbParamCollection();
                Sql = new StringBuilder();
                Sql.Append("INSERT INTO PR_HN_TBL(");
                Sql.Append("  GUID");
                Sql.Append(" ,SORT");
                Sql.Append(" ,ITEM01");
                Sql.Append(" ,ITEM02");
                Sql.Append(" ,ITEM03");
                Sql.Append(") ");
                Sql.Append("VALUES(");
                Sql.Append("  @GUID");
                Sql.Append(" ,@SORT");
                Sql.Append(" ,@ITEM01");
                Sql.Append(" ,@ITEM02");
                Sql.Append(" ,@ITEM03");
                Sql.Append(") ");

                // 空行登録 １つ
                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                dpc.SetParam("@SORT", SqlDbType.VarChar, 4, i);
                // ページヘッダーデータを設定
                dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);
                dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpjpDateFr[5]);
                dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpjpDateTo[5]);

                this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                i++;

                // 売上合計を登録
                Sql = new StringBuilder();
                dpc = new DbParamCollection();
                Sql.Append("INSERT INTO PR_HN_TBL(");
                Sql.Append("  GUID");
                Sql.Append(" ,SORT");
                Sql.Append(" ,ITEM01");
                Sql.Append(" ,ITEM02");
                Sql.Append(" ,ITEM03");
                Sql.Append(" ,ITEM04");
                Sql.Append(" ,ITEM05");
                Sql.Append(" ,ITEM06");
                Sql.Append(" ,ITEM07");
                Sql.Append(" ,ITEM08");
                Sql.Append(" ,ITEM09");
                Sql.Append(" ,ITEM10");
                Sql.Append(" ,ITEM11");
                Sql.Append(") ");
                Sql.Append("VALUES(");
                Sql.Append("  @GUID");
                Sql.Append(" ,@SORT");
                Sql.Append(" ,@ITEM01");
                Sql.Append(" ,@ITEM02");
                Sql.Append(" ,@ITEM03");
                Sql.Append(" ,@ITEM04");
                Sql.Append(" ,@ITEM05");
                Sql.Append(" ,@ITEM06");
                Sql.Append(" ,@ITEM07");
                Sql.Append(" ,@ITEM08");
                Sql.Append(" ,@ITEM09");
                Sql.Append(" ,@ITEM10");
                Sql.Append(" ,@ITEM11");
                Sql.Append(") ");

                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                dpc.SetParam("@SORT", SqlDbType.VarChar, 4, i);
                // ページヘッダーデータを設定
                dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);
                dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpjpDateFr[5]);
                dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpjpDateTo[5]);
                // 数値にカンマをセット
                string soSuryo = sokei.suryo.ToString("N0");
                string soKingakuKei = sokei.gokei.ToString("N0");
                // データをセット
                dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, "");
                dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, "");
                dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, "【　合　　計　】");
                dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, soSuryo);
                dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, "");
                dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, "");
                dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, "");
                dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, soKingakuKei);

                this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                i++;
                #endregion
            }

            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
                "SORT",
                "PR_HN_TBL",
                "GUID = @GUID",
                dpc);

            DbParamCollection whereParam = new DbParamCollection();
            whereParam.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DbParamCollection updParam = new DbParamCollection();
            updParam.SetParam("@ITEM12", SqlDbType.VarChar, 40, tmpdtPR_HN_TBL.Rows.Count);
            this.Dba.Update("PR_HN_TBL", updParam, "GUID = @GUID", whereParam);

            bool dataFlag;
            if (tmpdtPR_HN_TBL.Rows.Count > 0)
            {
                dataFlag = true;
            }
            else
            {
                dataFlag = false;
            }

            return dataFlag;
        }
        #endregion

    }
}
