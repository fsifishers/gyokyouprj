﻿using System.Data;

using jp.co.fsi.common.report;

namespace jp.co.fsi.edi.edi2031
{
    /// <summary>
    /// EDI2031R の概要の説明です。
    /// </summary>
    public partial class EDI2031R : BaseReport
    {

        public EDI2031R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }
    }
}
