﻿namespace jp.co.fsi.edi.edi1021
{
    partial class EDI1021
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            this.lblMode = new System.Windows.Forms.Label();
            this.lblTantoshaNm = new System.Windows.Forms.Label();
            this.txtTantoshaCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblTantoshaCd = new System.Windows.Forms.Label();
            this.lblFunanushiNm = new System.Windows.Forms.Label();
            this.txtFunanushiCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblFunanushiCd = new System.Windows.Forms.Label();
            this.txtDenpyoNo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblDenpyoNo = new System.Windows.Forms.Label();
            this.lblDay = new System.Windows.Forms.Label();
            this.lblMonth = new System.Windows.Forms.Label();
            this.txtDay = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblYear = new System.Windows.Forms.Label();
            this.txtMonth = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtGengoYear = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblGengo = new System.Windows.Forms.Label();
            this.lblDenpyoDate = new System.Windows.Forms.Label();
            this.lblTorihikiKubunNm = new System.Windows.Forms.Label();
            this.txtTorihikiKubunCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblTorihikiKubunCd = new System.Windows.Forms.Label();
            this.dgvInputList = new System.Windows.Forms.DataGridView();
            this.txtGridEdit = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblBeforeDenpyoNo = new System.Windows.Forms.Label();
            this.GYO_BANGO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KUBUN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SHOHIN_CD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SHOHIN_NM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IRISU = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TANI = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KESUSU = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BARA_SOSU = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.URI_TANKA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BAIKA_KINGAKU = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SHOHIZEI = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SHIWAKE_CD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BUMON_CD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ZEI_KUBUN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.JIGYO_KUBUN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ZEI_RITSU = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KUBUN_CD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GEN_TANKA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KAZEI_KUBUN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlDebug.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInputList)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.TabIndex = 999;
            this.lblTitle.Text = "購買売上入力";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Size = new System.Drawing.Size(847, 100);
            // 
            // lblMode
            // 
            this.lblMode.BackColor = System.Drawing.Color.Transparent;
            this.lblMode.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMode.Location = new System.Drawing.Point(1, 45);
            this.lblMode.Name = "lblMode";
            this.lblMode.Size = new System.Drawing.Size(83, 20);
            this.lblMode.TabIndex = 0;
            this.lblMode.Text = "【登録】";
            this.lblMode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTantoshaNm
            // 
            this.lblTantoshaNm.BackColor = System.Drawing.Color.Silver;
            this.lblTantoshaNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTantoshaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblTantoshaNm.Location = new System.Drawing.Point(525, 73);
            this.lblTantoshaNm.Name = "lblTantoshaNm";
            this.lblTantoshaNm.Size = new System.Drawing.Size(234, 20);
            this.lblTantoshaNm.TabIndex = 20;
            this.lblTantoshaNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTantoshaCd
            // 
            this.txtTantoshaCd.AutoSizeFromLength = true;
            this.txtTantoshaCd.DisplayLength = null;
            this.txtTantoshaCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtTantoshaCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtTantoshaCd.Location = new System.Drawing.Point(489, 73);
            this.txtTantoshaCd.MaxLength = 4;
            this.txtTantoshaCd.Name = "txtTantoshaCd";
            this.txtTantoshaCd.Size = new System.Drawing.Size(34, 20);
            this.txtTantoshaCd.TabIndex = 19;
            this.txtTantoshaCd.Text = "0";
            this.txtTantoshaCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTantoshaCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtTantoshaCd_Validating);
            // 
            // lblTantoshaCd
            // 
            this.lblTantoshaCd.BackColor = System.Drawing.Color.Silver;
            this.lblTantoshaCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTantoshaCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblTantoshaCd.Location = new System.Drawing.Point(417, 71);
            this.lblTantoshaCd.Name = "lblTantoshaCd";
            this.lblTantoshaCd.Size = new System.Drawing.Size(345, 25);
            this.lblTantoshaCd.TabIndex = 18;
            this.lblTantoshaCd.Text = "担 当 者";
            this.lblTantoshaCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblFunanushiNm
            // 
            this.lblFunanushiNm.BackColor = System.Drawing.Color.Silver;
            this.lblFunanushiNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFunanushiNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblFunanushiNm.Location = new System.Drawing.Point(133, 101);
            this.lblFunanushiNm.Name = "lblFunanushiNm";
            this.lblFunanushiNm.Size = new System.Drawing.Size(234, 20);
            this.lblFunanushiNm.TabIndex = 17;
            this.lblFunanushiNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblFunanushiNm.Visible = false;
            // 
            // txtFunanushiCd
            // 
            this.txtFunanushiCd.AutoSizeFromLength = true;
            this.txtFunanushiCd.DisplayLength = null;
            this.txtFunanushiCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtFunanushiCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtFunanushiCd.Location = new System.Drawing.Point(79, 101);
            this.txtFunanushiCd.MaxLength = 4;
            this.txtFunanushiCd.Name = "txtFunanushiCd";
            this.txtFunanushiCd.Size = new System.Drawing.Size(50, 20);
            this.txtFunanushiCd.TabIndex = 16;
            this.txtFunanushiCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFunanushiCd.Visible = false;
            this.txtFunanushiCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtFunanushiCd_Validating);
            // 
            // lblFunanushiCd
            // 
            this.lblFunanushiCd.BackColor = System.Drawing.Color.Silver;
            this.lblFunanushiCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFunanushiCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblFunanushiCd.Location = new System.Drawing.Point(12, 99);
            this.lblFunanushiCd.Name = "lblFunanushiCd";
            this.lblFunanushiCd.Size = new System.Drawing.Size(358, 25);
            this.lblFunanushiCd.TabIndex = 15;
            this.lblFunanushiCd.Text = "船主CD";
            this.lblFunanushiCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblFunanushiCd.Visible = false;
            // 
            // txtDenpyoNo
            // 
            this.txtDenpyoNo.AutoSizeFromLength = true;
            this.txtDenpyoNo.DisplayLength = null;
            this.txtDenpyoNo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtDenpyoNo.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtDenpyoNo.Location = new System.Drawing.Point(78, 73);
            this.txtDenpyoNo.MaxLength = 8;
            this.txtDenpyoNo.Name = "txtDenpyoNo";
            this.txtDenpyoNo.Size = new System.Drawing.Size(50, 20);
            this.txtDenpyoNo.TabIndex = 3;
            this.txtDenpyoNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDenpyoNo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDenpyoNo_Validating);
            // 
            // lblDenpyoNo
            // 
            this.lblDenpyoNo.BackColor = System.Drawing.Color.Silver;
            this.lblDenpyoNo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDenpyoNo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblDenpyoNo.Location = new System.Drawing.Point(12, 71);
            this.lblDenpyoNo.Name = "lblDenpyoNo";
            this.lblDenpyoNo.Size = new System.Drawing.Size(120, 25);
            this.lblDenpyoNo.TabIndex = 2;
            this.lblDenpyoNo.Text = "伝票番号";
            this.lblDenpyoNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDay
            // 
            this.lblDay.AutoSize = true;
            this.lblDay.BackColor = System.Drawing.Color.Silver;
            this.lblDay.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblDay.Location = new System.Drawing.Point(389, 77);
            this.lblDay.Name = "lblDay";
            this.lblDay.Size = new System.Drawing.Size(21, 13);
            this.lblDay.TabIndex = 11;
            this.lblDay.Text = "日";
            // 
            // lblMonth
            // 
            this.lblMonth.AutoSize = true;
            this.lblMonth.BackColor = System.Drawing.Color.Silver;
            this.lblMonth.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblMonth.Location = new System.Drawing.Point(326, 77);
            this.lblMonth.Name = "lblMonth";
            this.lblMonth.Size = new System.Drawing.Size(21, 13);
            this.lblMonth.TabIndex = 9;
            this.lblMonth.Text = "月";
            // 
            // txtDay
            // 
            this.txtDay.AutoSizeFromLength = false;
            this.txtDay.DisplayLength = null;
            this.txtDay.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtDay.Location = new System.Drawing.Point(347, 73);
            this.txtDay.MaxLength = 2;
            this.txtDay.Name = "txtDay";
            this.txtDay.Size = new System.Drawing.Size(40, 20);
            this.txtDay.TabIndex = 10;
            this.txtDay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDay.Validating += new System.ComponentModel.CancelEventHandler(this.txtDay_Validating);
            // 
            // lblYear
            // 
            this.lblYear.AutoSize = true;
            this.lblYear.BackColor = System.Drawing.Color.Silver;
            this.lblYear.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblYear.Location = new System.Drawing.Point(273, 77);
            this.lblYear.Name = "lblYear";
            this.lblYear.Size = new System.Drawing.Size(21, 13);
            this.lblYear.TabIndex = 7;
            this.lblYear.Text = "年";
            // 
            // txtMonth
            // 
            this.txtMonth.AutoSizeFromLength = false;
            this.txtMonth.DisplayLength = null;
            this.txtMonth.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtMonth.Location = new System.Drawing.Point(294, 73);
            this.txtMonth.MaxLength = 2;
            this.txtMonth.Name = "txtMonth";
            this.txtMonth.Size = new System.Drawing.Size(29, 20);
            this.txtMonth.TabIndex = 8;
            this.txtMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonth.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonth_Validating);
            // 
            // txtGengoYear
            // 
            this.txtGengoYear.AutoSizeFromLength = false;
            this.txtGengoYear.DisplayLength = null;
            this.txtGengoYear.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtGengoYear.Location = new System.Drawing.Point(241, 73);
            this.txtGengoYear.MaxLength = 2;
            this.txtGengoYear.Name = "txtGengoYear";
            this.txtGengoYear.Size = new System.Drawing.Size(30, 20);
            this.txtGengoYear.TabIndex = 6;
            this.txtGengoYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGengoYear.Validating += new System.ComponentModel.CancelEventHandler(this.txtGengoYear_Validating);
            // 
            // lblGengo
            // 
            this.lblGengo.BackColor = System.Drawing.Color.Silver;
            this.lblGengo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGengo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblGengo.Location = new System.Drawing.Point(199, 73);
            this.lblGengo.Name = "lblGengo";
            this.lblGengo.Size = new System.Drawing.Size(38, 20);
            this.lblGengo.TabIndex = 5;
            this.lblGengo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblDenpyoDate
            // 
            this.lblDenpyoDate.BackColor = System.Drawing.Color.Silver;
            this.lblDenpyoDate.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDenpyoDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblDenpyoDate.Location = new System.Drawing.Point(133, 71);
            this.lblDenpyoDate.Name = "lblDenpyoDate";
            this.lblDenpyoDate.Size = new System.Drawing.Size(281, 25);
            this.lblDenpyoDate.TabIndex = 4;
            this.lblDenpyoDate.Text = "伝票日付";
            this.lblDenpyoDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTorihikiKubunNm
            // 
            this.lblTorihikiKubunNm.BackColor = System.Drawing.Color.Silver;
            this.lblTorihikiKubunNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTorihikiKubunNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblTorihikiKubunNm.Location = new System.Drawing.Point(583, 41);
            this.lblTorihikiKubunNm.Name = "lblTorihikiKubunNm";
            this.lblTorihikiKubunNm.Size = new System.Drawing.Size(127, 20);
            this.lblTorihikiKubunNm.TabIndex = 14;
            this.lblTorihikiKubunNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblTorihikiKubunNm.Visible = false;
            // 
            // txtTorihikiKubunCd
            // 
            this.txtTorihikiKubunCd.AutoSizeFromLength = true;
            this.txtTorihikiKubunCd.DisplayLength = null;
            this.txtTorihikiKubunCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtTorihikiKubunCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtTorihikiKubunCd.Location = new System.Drawing.Point(540, 41);
            this.txtTorihikiKubunCd.MaxLength = 4;
            this.txtTorihikiKubunCd.Name = "txtTorihikiKubunCd";
            this.txtTorihikiKubunCd.Size = new System.Drawing.Size(36, 20);
            this.txtTorihikiKubunCd.TabIndex = 13;
            this.txtTorihikiKubunCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTorihikiKubunCd.Visible = false;
            this.txtTorihikiKubunCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtTorihikiKubunCd_Validating);
            // 
            // lblTorihikiKubunCd
            // 
            this.lblTorihikiKubunCd.BackColor = System.Drawing.Color.Silver;
            this.lblTorihikiKubunCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTorihikiKubunCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblTorihikiKubunCd.Location = new System.Drawing.Point(458, 39);
            this.lblTorihikiKubunCd.Name = "lblTorihikiKubunCd";
            this.lblTorihikiKubunCd.Size = new System.Drawing.Size(256, 25);
            this.lblTorihikiKubunCd.TabIndex = 12;
            this.lblTorihikiKubunCd.Text = "取引区分";
            this.lblTorihikiKubunCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblTorihikiKubunCd.Visible = false;
            // 
            // dgvInputList
            // 
            this.dgvInputList.AllowUserToAddRows = false;
            this.dgvInputList.AllowUserToDeleteRows = false;
            this.dgvInputList.AllowUserToResizeColumns = false;
            this.dgvInputList.AllowUserToResizeRows = false;
            this.dgvInputList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvInputList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.GYO_BANGO,
            this.KUBUN,
            this.SHOHIN_CD,
            this.SHOHIN_NM,
            this.IRISU,
            this.TANI,
            this.KESUSU,
            this.BARA_SOSU,
            this.URI_TANKA,
            this.BAIKA_KINGAKU,
            this.SHOHIZEI,
            this.SHIWAKE_CD,
            this.BUMON_CD,
            this.ZEI_KUBUN,
            this.JIGYO_KUBUN,
            this.ZEI_RITSU,
            this.KUBUN_CD,
            this.GEN_TANKA,
            this.KAZEI_KUBUN});
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvInputList.DefaultCellStyle = dataGridViewCellStyle12;
            this.dgvInputList.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvInputList.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.dgvInputList.Location = new System.Drawing.Point(12, 99);
            this.dgvInputList.MultiSelect = false;
            this.dgvInputList.Name = "dgvInputList";
            this.dgvInputList.RowHeadersVisible = false;
            this.dgvInputList.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvInputList.RowTemplate.Height = 21;
            this.dgvInputList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvInputList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvInputList.Size = new System.Drawing.Size(754, 421);
            this.dgvInputList.TabIndex = 21;
            this.dgvInputList.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvInputList_CellEnter);
            this.dgvInputList.Scroll += new System.Windows.Forms.ScrollEventHandler(this.dgvInputList_Scroll);
            this.dgvInputList.Enter += new System.EventHandler(this.dgvInputList_Enter);
            // 
            // txtGridEdit
            // 
            this.txtGridEdit.AutoSizeFromLength = true;
            this.txtGridEdit.BackColor = System.Drawing.Color.White;
            this.txtGridEdit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtGridEdit.DisplayLength = null;
            this.txtGridEdit.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGridEdit.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtGridEdit.Location = new System.Drawing.Point(394, 309);
            this.txtGridEdit.MaxLength = 10;
            this.txtGridEdit.Name = "txtGridEdit";
            this.txtGridEdit.Size = new System.Drawing.Size(76, 20);
            this.txtGridEdit.TabIndex = 22;
            this.txtGridEdit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGridEdit.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtGridEdit_KeyDown);
            // 
            // lblBeforeDenpyoNo
            // 
            this.lblBeforeDenpyoNo.BackColor = System.Drawing.Color.White;
            this.lblBeforeDenpyoNo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBeforeDenpyoNo.Location = new System.Drawing.Point(90, 45);
            this.lblBeforeDenpyoNo.Name = "lblBeforeDenpyoNo";
            this.lblBeforeDenpyoNo.Size = new System.Drawing.Size(342, 20);
            this.lblBeforeDenpyoNo.TabIndex = 1;
            this.lblBeforeDenpyoNo.Text = "【前回登録伝票番号：】";
            this.lblBeforeDenpyoNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblBeforeDenpyoNo.Visible = false;
            // 
            // GYO_BANGO
            // 
            dataGridViewCellStyle1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            this.GYO_BANGO.DefaultCellStyle = dataGridViewCellStyle1;
            this.GYO_BANGO.HeaderText = "行";
            this.GYO_BANGO.Name = "GYO_BANGO";
            this.GYO_BANGO.ReadOnly = true;
            this.GYO_BANGO.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.GYO_BANGO.Width = 25;
            // 
            // KUBUN
            // 
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            this.KUBUN.DefaultCellStyle = dataGridViewCellStyle2;
            this.KUBUN.HeaderText = "区";
            this.KUBUN.Name = "KUBUN";
            this.KUBUN.ReadOnly = true;
            this.KUBUN.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.KUBUN.Width = 25;
            // 
            // SHOHIN_CD
            // 
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            this.SHOHIN_CD.DefaultCellStyle = dataGridViewCellStyle3;
            this.SHOHIN_CD.HeaderText = "ｺｰﾄﾞ";
            this.SHOHIN_CD.Name = "SHOHIN_CD";
            this.SHOHIN_CD.ReadOnly = true;
            this.SHOHIN_CD.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.SHOHIN_CD.Width = 90;
            // 
            // SHOHIN_NM
            // 
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            this.SHOHIN_NM.DefaultCellStyle = dataGridViewCellStyle4;
            this.SHOHIN_NM.HeaderText = "商品名";
            this.SHOHIN_NM.Name = "SHOHIN_NM";
            this.SHOHIN_NM.ReadOnly = true;
            this.SHOHIN_NM.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.SHOHIN_NM.Width = 165;
            // 
            // IRISU
            // 
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            this.IRISU.DefaultCellStyle = dataGridViewCellStyle5;
            this.IRISU.HeaderText = "入数";
            this.IRISU.Name = "IRISU";
            this.IRISU.ReadOnly = true;
            this.IRISU.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.IRISU.Width = 55;
            // 
            // TANI
            // 
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            this.TANI.DefaultCellStyle = dataGridViewCellStyle6;
            this.TANI.HeaderText = "単位";
            this.TANI.Name = "TANI";
            this.TANI.ReadOnly = true;
            this.TANI.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.TANI.Width = 55;
            // 
            // KESUSU
            // 
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Black;
            this.KESUSU.DefaultCellStyle = dataGridViewCellStyle7;
            this.KESUSU.HeaderText = "ｹｰｽ数";
            this.KESUSU.Name = "KESUSU";
            this.KESUSU.ReadOnly = true;
            this.KESUSU.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.KESUSU.Width = 63;
            // 
            // BARA_SOSU
            // 
            dataGridViewCellStyle8.NullValue = null;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.Black;
            this.BARA_SOSU.DefaultCellStyle = dataGridViewCellStyle8;
            this.BARA_SOSU.HeaderText = "ﾊﾞﾗ数";
            this.BARA_SOSU.Name = "BARA_SOSU";
            this.BARA_SOSU.ReadOnly = true;
            this.BARA_SOSU.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.BARA_SOSU.Width = 75;
            // 
            // URI_TANKA
            // 
            dataGridViewCellStyle9.NullValue = null;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.Black;
            this.URI_TANKA.DefaultCellStyle = dataGridViewCellStyle9;
            this.URI_TANKA.HeaderText = "単価";
            this.URI_TANKA.Name = "URI_TANKA";
            this.URI_TANKA.ReadOnly = true;
            this.URI_TANKA.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.URI_TANKA.Width = 80;
            // 
            // BAIKA_KINGAKU
            // 
            dataGridViewCellStyle10.NullValue = null;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.Black;
            this.BAIKA_KINGAKU.DefaultCellStyle = dataGridViewCellStyle10;
            this.BAIKA_KINGAKU.HeaderText = "金額";
            this.BAIKA_KINGAKU.Name = "BAIKA_KINGAKU";
            this.BAIKA_KINGAKU.ReadOnly = true;
            this.BAIKA_KINGAKU.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.BAIKA_KINGAKU.Width = 87;
            // 
            // SHOHIZEI
            // 
            dataGridViewCellStyle11.NullValue = null;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.Color.Black;
            this.SHOHIZEI.DefaultCellStyle = dataGridViewCellStyle11;
            this.SHOHIZEI.HeaderText = "消費税";
            this.SHOHIZEI.Name = "SHOHIZEI";
            this.SHOHIZEI.ReadOnly = true;
            this.SHOHIZEI.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.SHOHIZEI.Visible = false;
            this.SHOHIZEI.Width = 85;
            // 
            // SHIWAKE_CD
            // 
            this.SHIWAKE_CD.HeaderText = "仕訳コード";
            this.SHIWAKE_CD.Name = "SHIWAKE_CD";
            this.SHIWAKE_CD.Visible = false;
            // 
            // BUMON_CD
            // 
            this.BUMON_CD.HeaderText = "部門コード";
            this.BUMON_CD.Name = "BUMON_CD";
            this.BUMON_CD.Visible = false;
            // 
            // ZEI_KUBUN
            // 
            this.ZEI_KUBUN.HeaderText = "税区分";
            this.ZEI_KUBUN.Name = "ZEI_KUBUN";
            this.ZEI_KUBUN.Visible = false;
            // 
            // JIGYO_KUBUN
            // 
            this.JIGYO_KUBUN.HeaderText = "事業区分";
            this.JIGYO_KUBUN.Name = "JIGYO_KUBUN";
            this.JIGYO_KUBUN.Visible = false;
            // 
            // ZEI_RITSU
            // 
            this.ZEI_RITSU.HeaderText = "税率";
            this.ZEI_RITSU.Name = "ZEI_RITSU";
            this.ZEI_RITSU.Visible = false;
            // 
            // KUBUN_CD
            // 
            this.KUBUN_CD.HeaderText = "区ｺｰﾄﾞ";
            this.KUBUN_CD.Name = "KUBUN_CD";
            this.KUBUN_CD.Visible = false;
            // 
            // GEN_TANKA
            // 
            this.GEN_TANKA.HeaderText = "原単価";
            this.GEN_TANKA.Name = "GEN_TANKA";
            this.GEN_TANKA.Visible = false;
            // 
            // KAZEI_KUBUN
            // 
            this.KAZEI_KUBUN.HeaderText = "課税区分";
            this.KAZEI_KUBUN.Name = "KAZEI_KUBUN";
            this.KAZEI_KUBUN.Visible = false;
            // 
            // EDI1021
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 638);
            this.Controls.Add(this.lblBeforeDenpyoNo);
            this.Controls.Add(this.txtGridEdit);
            this.Controls.Add(this.dgvInputList);
            this.Controls.Add(this.lblTorihikiKubunNm);
            this.Controls.Add(this.txtTorihikiKubunCd);
            this.Controls.Add(this.lblTorihikiKubunCd);
            this.Controls.Add(this.lblDay);
            this.Controls.Add(this.lblMonth);
            this.Controls.Add(this.txtDay);
            this.Controls.Add(this.lblYear);
            this.Controls.Add(this.txtMonth);
            this.Controls.Add(this.txtGengoYear);
            this.Controls.Add(this.lblGengo);
            this.Controls.Add(this.txtDenpyoNo);
            this.Controls.Add(this.lblDenpyoNo);
            this.Controls.Add(this.lblFunanushiNm);
            this.Controls.Add(this.txtFunanushiCd);
            this.Controls.Add(this.lblFunanushiCd);
            this.Controls.Add(this.lblTantoshaNm);
            this.Controls.Add(this.txtTantoshaCd);
            this.Controls.Add(this.lblTantoshaCd);
            this.Controls.Add(this.lblMode);
            this.Controls.Add(this.lblDenpyoDate);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "EDI1021";
            this.Text = "発注入力";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.EDI1021_FormClosing);
            this.Controls.SetChildIndex(this.lblDenpyoDate, 0);
            this.Controls.SetChildIndex(this.lblMode, 0);
            this.Controls.SetChildIndex(this.lblTantoshaCd, 0);
            this.Controls.SetChildIndex(this.txtTantoshaCd, 0);
            this.Controls.SetChildIndex(this.lblTantoshaNm, 0);
            this.Controls.SetChildIndex(this.lblFunanushiCd, 0);
            this.Controls.SetChildIndex(this.txtFunanushiCd, 0);
            this.Controls.SetChildIndex(this.lblFunanushiNm, 0);
            this.Controls.SetChildIndex(this.lblDenpyoNo, 0);
            this.Controls.SetChildIndex(this.txtDenpyoNo, 0);
            this.Controls.SetChildIndex(this.lblGengo, 0);
            this.Controls.SetChildIndex(this.txtGengoYear, 0);
            this.Controls.SetChildIndex(this.txtMonth, 0);
            this.Controls.SetChildIndex(this.lblYear, 0);
            this.Controls.SetChildIndex(this.txtDay, 0);
            this.Controls.SetChildIndex(this.lblMonth, 0);
            this.Controls.SetChildIndex(this.lblDay, 0);
            this.Controls.SetChildIndex(this.lblTorihikiKubunCd, 0);
            this.Controls.SetChildIndex(this.txtTorihikiKubunCd, 0);
            this.Controls.SetChildIndex(this.lblTorihikiKubunNm, 0);
            this.Controls.SetChildIndex(this.dgvInputList, 0);
            this.Controls.SetChildIndex(this.txtGridEdit, 0);
            this.Controls.SetChildIndex(this.lblBeforeDenpyoNo, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.pnlDebug.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvInputList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblMode;
        private System.Windows.Forms.Label lblTantoshaNm;
        private common.controls.FsiTextBox txtTantoshaCd;
        private System.Windows.Forms.Label lblTantoshaCd;
        private System.Windows.Forms.Label lblFunanushiNm;
        private common.controls.FsiTextBox txtFunanushiCd;
        private System.Windows.Forms.Label lblFunanushiCd;
        private common.controls.FsiTextBox txtDenpyoNo;
        private System.Windows.Forms.Label lblDenpyoNo;
        private System.Windows.Forms.Label lblDay;
        private System.Windows.Forms.Label lblMonth;
        private common.controls.FsiTextBox txtDay;
        private System.Windows.Forms.Label lblYear;
        private common.controls.FsiTextBox txtMonth;
        private common.controls.FsiTextBox txtGengoYear;
        private System.Windows.Forms.Label lblGengo;
        private System.Windows.Forms.Label lblDenpyoDate;
        private System.Windows.Forms.Label lblTorihikiKubunNm;
        private common.controls.FsiTextBox txtTorihikiKubunCd;
        private System.Windows.Forms.Label lblTorihikiKubunCd;
        private System.Windows.Forms.DataGridView dgvInputList;
        private common.controls.FsiTextBox txtGridEdit;
        private System.Windows.Forms.Label lblBeforeDenpyoNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn GYO_BANGO;
        private System.Windows.Forms.DataGridViewTextBoxColumn KUBUN;
        private System.Windows.Forms.DataGridViewTextBoxColumn SHOHIN_CD;
        private System.Windows.Forms.DataGridViewTextBoxColumn SHOHIN_NM;
        private System.Windows.Forms.DataGridViewTextBoxColumn IRISU;
        private System.Windows.Forms.DataGridViewTextBoxColumn TANI;
        private System.Windows.Forms.DataGridViewTextBoxColumn KESUSU;
        private System.Windows.Forms.DataGridViewTextBoxColumn BARA_SOSU;
        private System.Windows.Forms.DataGridViewTextBoxColumn URI_TANKA;
        private System.Windows.Forms.DataGridViewTextBoxColumn BAIKA_KINGAKU;
        private System.Windows.Forms.DataGridViewTextBoxColumn SHOHIZEI;
        private System.Windows.Forms.DataGridViewTextBoxColumn SHIWAKE_CD;
        private System.Windows.Forms.DataGridViewTextBoxColumn BUMON_CD;
        private System.Windows.Forms.DataGridViewTextBoxColumn ZEI_KUBUN;
        private System.Windows.Forms.DataGridViewTextBoxColumn JIGYO_KUBUN;
        private System.Windows.Forms.DataGridViewTextBoxColumn ZEI_RITSU;
        private System.Windows.Forms.DataGridViewTextBoxColumn KUBUN_CD;
        private System.Windows.Forms.DataGridViewTextBoxColumn GEN_TANKA;
        private System.Windows.Forms.DataGridViewTextBoxColumn KAZEI_KUBUN;
    }
}