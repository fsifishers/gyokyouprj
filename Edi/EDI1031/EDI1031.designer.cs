﻿namespace jp.co.fsi.edi.edi1031
{
    partial class EDI1031
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.lblDayFr = new System.Windows.Forms.Label();
            this.lblMonthFr = new System.Windows.Forms.Label();
            this.txtDayFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblYearFr = new System.Windows.Forms.Label();
            this.txtMonthFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtGengoYearFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblGengoFr = new System.Windows.Forms.Label();
            this.lblDenpyoDate = new System.Windows.Forms.Label();
            this.dgvList = new System.Windows.Forms.DataGridView();
            this.lblDateBet1 = new System.Windows.Forms.Label();
            this.lblDayTo = new System.Windows.Forms.Label();
            this.lblMonthTo = new System.Windows.Forms.Label();
            this.txtDayTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblYearTo = new System.Windows.Forms.Label();
            this.txtMonthTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtGengoYearTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblGengoTo = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblTantoshaCd = new System.Windows.Forms.Label();
            this.txtTantoshaCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblTantoshaNmFr = new System.Windows.Forms.Label();
            this.lblDateBet3 = new System.Windows.Forms.Label();
            this.txtTantoshaCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblTantoshaNmTo = new System.Windows.Forms.Label();
            this.lblShohinCd = new System.Windows.Forms.Label();
            this.txtShohinCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShohinNmFr = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtShohinCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShohinNmTo = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvList)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.TabIndex = 999;
            this.lblTitle.Text = "発注一覧";
            // 
            // btnEsc
            // 
            this.btnEsc.Location = new System.Drawing.Point(3, 49);
            // 
            // pnlDebug
            // 
            this.pnlDebug.Size = new System.Drawing.Size(847, 100);
            // 
            // lblDayFr
            // 
            this.lblDayFr.AutoSize = true;
            this.lblDayFr.BackColor = System.Drawing.Color.Silver;
            this.lblDayFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblDayFr.Location = new System.Drawing.Point(303, 53);
            this.lblDayFr.Name = "lblDayFr";
            this.lblDayFr.Size = new System.Drawing.Size(21, 13);
            this.lblDayFr.TabIndex = 7;
            this.lblDayFr.Text = "日";
            // 
            // lblMonthFr
            // 
            this.lblMonthFr.AutoSize = true;
            this.lblMonthFr.BackColor = System.Drawing.Color.Silver;
            this.lblMonthFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblMonthFr.Location = new System.Drawing.Point(236, 53);
            this.lblMonthFr.Name = "lblMonthFr";
            this.lblMonthFr.Size = new System.Drawing.Size(21, 13);
            this.lblMonthFr.TabIndex = 5;
            this.lblMonthFr.Text = "月";
            // 
            // txtDayFr
            // 
            this.txtDayFr.AutoSizeFromLength = false;
            this.txtDayFr.DisplayLength = null;
            this.txtDayFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtDayFr.Location = new System.Drawing.Point(259, 49);
            this.txtDayFr.MaxLength = 2;
            this.txtDayFr.Name = "txtDayFr";
            this.txtDayFr.Size = new System.Drawing.Size(40, 20);
            this.txtDayFr.TabIndex = 6;
            this.txtDayFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDayFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDayFr_Validating);
            // 
            // lblYearFr
            // 
            this.lblYearFr.AutoSize = true;
            this.lblYearFr.BackColor = System.Drawing.Color.Silver;
            this.lblYearFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblYearFr.Location = new System.Drawing.Point(169, 53);
            this.lblYearFr.Name = "lblYearFr";
            this.lblYearFr.Size = new System.Drawing.Size(21, 13);
            this.lblYearFr.TabIndex = 3;
            this.lblYearFr.Text = "年";
            // 
            // txtMonthFr
            // 
            this.txtMonthFr.AutoSizeFromLength = false;
            this.txtMonthFr.DisplayLength = null;
            this.txtMonthFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtMonthFr.Location = new System.Drawing.Point(192, 49);
            this.txtMonthFr.MaxLength = 2;
            this.txtMonthFr.Name = "txtMonthFr";
            this.txtMonthFr.Size = new System.Drawing.Size(40, 20);
            this.txtMonthFr.TabIndex = 4;
            this.txtMonthFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonthFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthFr_Validating);
            // 
            // txtGengoYearFr
            // 
            this.txtGengoYearFr.AutoSizeFromLength = false;
            this.txtGengoYearFr.DisplayLength = null;
            this.txtGengoYearFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtGengoYearFr.Location = new System.Drawing.Point(125, 49);
            this.txtGengoYearFr.MaxLength = 2;
            this.txtGengoYearFr.Name = "txtGengoYearFr";
            this.txtGengoYearFr.Size = new System.Drawing.Size(40, 20);
            this.txtGengoYearFr.TabIndex = 2;
            this.txtGengoYearFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGengoYearFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtGengoYearFr_Validating);
            // 
            // lblGengoFr
            // 
            this.lblGengoFr.BackColor = System.Drawing.Color.Silver;
            this.lblGengoFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGengoFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblGengoFr.Location = new System.Drawing.Point(82, 49);
            this.lblGengoFr.Name = "lblGengoFr";
            this.lblGengoFr.Size = new System.Drawing.Size(40, 20);
            this.lblGengoFr.TabIndex = 1;
            this.lblGengoFr.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblDenpyoDate
            // 
            this.lblDenpyoDate.BackColor = System.Drawing.Color.Silver;
            this.lblDenpyoDate.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDenpyoDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblDenpyoDate.Location = new System.Drawing.Point(13, 47);
            this.lblDenpyoDate.Name = "lblDenpyoDate";
            this.lblDenpyoDate.Size = new System.Drawing.Size(315, 25);
            this.lblDenpyoDate.TabIndex = 0;
            this.lblDenpyoDate.Text = "伝票日付";
            this.lblDenpyoDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dgvList
            // 
            this.dgvList.AllowUserToAddRows = false;
            this.dgvList.AllowUserToDeleteRows = false;
            this.dgvList.AllowUserToResizeColumns = false;
            this.dgvList.AllowUserToResizeRows = false;
            this.dgvList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvList.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgvList.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvList.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.dgvList.Location = new System.Drawing.Point(13, 143);
            this.dgvList.MultiSelect = false;
            this.dgvList.Name = "dgvList";
            this.dgvList.RowHeadersVisible = false;
            this.dgvList.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvList.RowTemplate.Height = 21;
            this.dgvList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvList.Size = new System.Drawing.Size(749, 385);
            this.dgvList.TabIndex = 29;
            this.dgvList.Enter += new System.EventHandler(this.dgvList_Enter);
            // 
            // lblDateBet1
            // 
            this.lblDateBet1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblDateBet1.Location = new System.Drawing.Point(331, 47);
            this.lblDateBet1.Name = "lblDateBet1";
            this.lblDateBet1.Size = new System.Drawing.Size(18, 20);
            this.lblDateBet1.TabIndex = 8;
            this.lblDateBet1.Text = "～";
            this.lblDateBet1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDayTo
            // 
            this.lblDayTo.AutoSize = true;
            this.lblDayTo.BackColor = System.Drawing.Color.Silver;
            this.lblDayTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblDayTo.Location = new System.Drawing.Point(577, 52);
            this.lblDayTo.Name = "lblDayTo";
            this.lblDayTo.Size = new System.Drawing.Size(21, 13);
            this.lblDayTo.TabIndex = 16;
            this.lblDayTo.Text = "日";
            // 
            // lblMonthTo
            // 
            this.lblMonthTo.AutoSize = true;
            this.lblMonthTo.BackColor = System.Drawing.Color.Silver;
            this.lblMonthTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblMonthTo.Location = new System.Drawing.Point(510, 52);
            this.lblMonthTo.Name = "lblMonthTo";
            this.lblMonthTo.Size = new System.Drawing.Size(21, 13);
            this.lblMonthTo.TabIndex = 14;
            this.lblMonthTo.Text = "月";
            // 
            // txtDayTo
            // 
            this.txtDayTo.AutoSizeFromLength = false;
            this.txtDayTo.DisplayLength = null;
            this.txtDayTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtDayTo.Location = new System.Drawing.Point(533, 48);
            this.txtDayTo.MaxLength = 2;
            this.txtDayTo.Name = "txtDayTo";
            this.txtDayTo.Size = new System.Drawing.Size(40, 20);
            this.txtDayTo.TabIndex = 15;
            this.txtDayTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDayTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDayTo_Validating);
            // 
            // lblYearTo
            // 
            this.lblYearTo.AutoSize = true;
            this.lblYearTo.BackColor = System.Drawing.Color.Silver;
            this.lblYearTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblYearTo.Location = new System.Drawing.Point(443, 52);
            this.lblYearTo.Name = "lblYearTo";
            this.lblYearTo.Size = new System.Drawing.Size(21, 13);
            this.lblYearTo.TabIndex = 12;
            this.lblYearTo.Text = "年";
            // 
            // txtMonthTo
            // 
            this.txtMonthTo.AutoSizeFromLength = false;
            this.txtMonthTo.DisplayLength = null;
            this.txtMonthTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtMonthTo.Location = new System.Drawing.Point(466, 48);
            this.txtMonthTo.MaxLength = 2;
            this.txtMonthTo.Name = "txtMonthTo";
            this.txtMonthTo.Size = new System.Drawing.Size(40, 20);
            this.txtMonthTo.TabIndex = 13;
            this.txtMonthTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonthTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthTo_Validating);
            // 
            // txtGengoYearTo
            // 
            this.txtGengoYearTo.AutoSizeFromLength = false;
            this.txtGengoYearTo.DisplayLength = null;
            this.txtGengoYearTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtGengoYearTo.Location = new System.Drawing.Point(399, 48);
            this.txtGengoYearTo.MaxLength = 2;
            this.txtGengoYearTo.Name = "txtGengoYearTo";
            this.txtGengoYearTo.Size = new System.Drawing.Size(40, 20);
            this.txtGengoYearTo.TabIndex = 11;
            this.txtGengoYearTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGengoYearTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtGengoYearTo_Validating);
            // 
            // lblGengoTo
            // 
            this.lblGengoTo.BackColor = System.Drawing.Color.Silver;
            this.lblGengoTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGengoTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblGengoTo.Location = new System.Drawing.Point(356, 48);
            this.lblGengoTo.Name = "lblGengoTo";
            this.lblGengoTo.Size = new System.Drawing.Size(40, 20);
            this.lblGengoTo.TabIndex = 10;
            this.lblGengoTo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.Silver;
            this.label6.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.label6.Location = new System.Drawing.Point(353, 46);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(250, 25);
            this.label6.TabIndex = 9;
            this.label6.Text = " ";
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Silver;
            this.label4.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.label4.Location = new System.Drawing.Point(353, 77);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(250, 25);
            this.label4.TabIndex = 28;
            this.label4.Text = " ";
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Silver;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.label1.Location = new System.Drawing.Point(432, 107);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(330, 25);
            this.label1.TabIndex = 37;
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTantoshaCd
            // 
            this.lblTantoshaCd.BackColor = System.Drawing.Color.Silver;
            this.lblTantoshaCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTantoshaCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblTantoshaCd.Location = new System.Drawing.Point(13, 77);
            this.lblTantoshaCd.Name = "lblTantoshaCd";
            this.lblTantoshaCd.Size = new System.Drawing.Size(315, 25);
            this.lblTantoshaCd.TabIndex = 17;
            this.lblTantoshaCd.Text = "担 当 者";
            this.lblTantoshaCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTantoshaCdFr
            // 
            this.txtTantoshaCdFr.AutoSizeFromLength = true;
            this.txtTantoshaCdFr.DisplayLength = null;
            this.txtTantoshaCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtTantoshaCdFr.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtTantoshaCdFr.Location = new System.Drawing.Point(80, 79);
            this.txtTantoshaCdFr.MaxLength = 4;
            this.txtTantoshaCdFr.Name = "txtTantoshaCdFr";
            this.txtTantoshaCdFr.Size = new System.Drawing.Size(34, 20);
            this.txtTantoshaCdFr.TabIndex = 18;
            this.txtTantoshaCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTantoshaCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtTantoshaCdFr_Validating);
            // 
            // lblTantoshaNmFr
            // 
            this.lblTantoshaNmFr.BackColor = System.Drawing.Color.Silver;
            this.lblTantoshaNmFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTantoshaNmFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblTantoshaNmFr.Location = new System.Drawing.Point(115, 79);
            this.lblTantoshaNmFr.Name = "lblTantoshaNmFr";
            this.lblTantoshaNmFr.Size = new System.Drawing.Size(210, 20);
            this.lblTantoshaNmFr.TabIndex = 19;
            this.lblTantoshaNmFr.Text = "先　頭";
            this.lblTantoshaNmFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateBet3
            // 
            this.lblDateBet3.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblDateBet3.Location = new System.Drawing.Point(331, 77);
            this.lblDateBet3.Name = "lblDateBet3";
            this.lblDateBet3.Size = new System.Drawing.Size(18, 20);
            this.lblDateBet3.TabIndex = 20;
            this.lblDateBet3.Text = "～";
            this.lblDateBet3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTantoshaCdTo
            // 
            this.txtTantoshaCdTo.AutoSizeFromLength = true;
            this.txtTantoshaCdTo.DisplayLength = null;
            this.txtTantoshaCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtTantoshaCdTo.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtTantoshaCdTo.Location = new System.Drawing.Point(355, 79);
            this.txtTantoshaCdTo.MaxLength = 4;
            this.txtTantoshaCdTo.Name = "txtTantoshaCdTo";
            this.txtTantoshaCdTo.Size = new System.Drawing.Size(34, 20);
            this.txtTantoshaCdTo.TabIndex = 21;
            this.txtTantoshaCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTantoshaCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtTantoshaCdTo_Validating);
            // 
            // lblTantoshaNmTo
            // 
            this.lblTantoshaNmTo.BackColor = System.Drawing.Color.Silver;
            this.lblTantoshaNmTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTantoshaNmTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblTantoshaNmTo.Location = new System.Drawing.Point(390, 79);
            this.lblTantoshaNmTo.Name = "lblTantoshaNmTo";
            this.lblTantoshaNmTo.Size = new System.Drawing.Size(211, 20);
            this.lblTantoshaNmTo.TabIndex = 22;
            this.lblTantoshaNmTo.Text = "最　後";
            this.lblTantoshaNmTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShohinCd
            // 
            this.lblShohinCd.BackColor = System.Drawing.Color.Silver;
            this.lblShohinCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShohinCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblShohinCd.Location = new System.Drawing.Point(13, 107);
            this.lblShohinCd.Name = "lblShohinCd";
            this.lblShohinCd.Size = new System.Drawing.Size(394, 25);
            this.lblShohinCd.TabIndex = 23;
            this.lblShohinCd.Text = "商品CD";
            this.lblShohinCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShohinCdFr
            // 
            this.txtShohinCdFr.AutoSizeFromLength = true;
            this.txtShohinCdFr.DisplayLength = null;
            this.txtShohinCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtShohinCdFr.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtShohinCdFr.Location = new System.Drawing.Point(80, 109);
            this.txtShohinCdFr.MaxLength = 15;
            this.txtShohinCdFr.Name = "txtShohinCdFr";
            this.txtShohinCdFr.Size = new System.Drawing.Size(87, 20);
            this.txtShohinCdFr.TabIndex = 24;
            this.txtShohinCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShohinCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohinCdFr_Validating);
            // 
            // lblShohinNmFr
            // 
            this.lblShohinNmFr.BackColor = System.Drawing.Color.Silver;
            this.lblShohinNmFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShohinNmFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblShohinNmFr.Location = new System.Drawing.Point(168, 109);
            this.lblShohinNmFr.Name = "lblShohinNmFr";
            this.lblShohinNmFr.Size = new System.Drawing.Size(236, 20);
            this.lblShohinNmFr.TabIndex = 25;
            this.lblShohinNmFr.Text = "先　頭";
            this.lblShohinNmFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.label2.Location = new System.Drawing.Point(410, 107);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(18, 20);
            this.label2.TabIndex = 26;
            this.label2.Text = "～";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShohinCdTo
            // 
            this.txtShohinCdTo.AutoSizeFromLength = true;
            this.txtShohinCdTo.DisplayLength = null;
            this.txtShohinCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtShohinCdTo.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtShohinCdTo.Location = new System.Drawing.Point(434, 109);
            this.txtShohinCdTo.MaxLength = 15;
            this.txtShohinCdTo.Name = "txtShohinCdTo";
            this.txtShohinCdTo.Size = new System.Drawing.Size(87, 20);
            this.txtShohinCdTo.TabIndex = 27;
            this.txtShohinCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShohinCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohinCdTo_Validating);
            // 
            // lblShohinNmTo
            // 
            this.lblShohinNmTo.BackColor = System.Drawing.Color.Silver;
            this.lblShohinNmTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShohinNmTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblShohinNmTo.Location = new System.Drawing.Point(523, 109);
            this.lblShohinNmTo.Name = "lblShohinNmTo";
            this.lblShohinNmTo.Size = new System.Drawing.Size(236, 20);
            this.lblShohinNmTo.TabIndex = 28;
            this.lblShohinNmTo.Text = "最　後";
            this.lblShohinNmTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // EDI1031
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 638);
            this.Controls.Add(this.lblShohinNmTo);
            this.Controls.Add(this.txtShohinCdTo);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblShohinNmFr);
            this.Controls.Add(this.txtShohinCdFr);
            this.Controls.Add(this.lblShohinCd);
            this.Controls.Add(this.lblTantoshaNmTo);
            this.Controls.Add(this.txtTantoshaCdTo);
            this.Controls.Add(this.lblDateBet3);
            this.Controls.Add(this.lblDayTo);
            this.Controls.Add(this.lblMonthTo);
            this.Controls.Add(this.txtDayTo);
            this.Controls.Add(this.lblYearTo);
            this.Controls.Add(this.txtMonthTo);
            this.Controls.Add(this.txtGengoYearTo);
            this.Controls.Add(this.lblGengoTo);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lblDateBet1);
            this.Controls.Add(this.lblDayFr);
            this.Controls.Add(this.lblMonthFr);
            this.Controls.Add(this.txtDayFr);
            this.Controls.Add(this.lblYearFr);
            this.Controls.Add(this.txtMonthFr);
            this.Controls.Add(this.txtGengoYearFr);
            this.Controls.Add(this.lblGengoFr);
            this.Controls.Add(this.lblTantoshaNmFr);
            this.Controls.Add(this.txtTantoshaCdFr);
            this.Controls.Add(this.lblTantoshaCd);
            this.Controls.Add(this.lblDenpyoDate);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.dgvList);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "EDI1031";
            this.ShowFButton = true;
            this.Text = "発注一覧";
            this.Controls.SetChildIndex(this.dgvList, 0);
            this.Controls.SetChildIndex(this.label4, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.lblDenpyoDate, 0);
            this.Controls.SetChildIndex(this.lblTantoshaCd, 0);
            this.Controls.SetChildIndex(this.txtTantoshaCdFr, 0);
            this.Controls.SetChildIndex(this.lblTantoshaNmFr, 0);
            this.Controls.SetChildIndex(this.lblGengoFr, 0);
            this.Controls.SetChildIndex(this.txtGengoYearFr, 0);
            this.Controls.SetChildIndex(this.txtMonthFr, 0);
            this.Controls.SetChildIndex(this.lblYearFr, 0);
            this.Controls.SetChildIndex(this.txtDayFr, 0);
            this.Controls.SetChildIndex(this.lblMonthFr, 0);
            this.Controls.SetChildIndex(this.lblDayFr, 0);
            this.Controls.SetChildIndex(this.lblDateBet1, 0);
            this.Controls.SetChildIndex(this.label6, 0);
            this.Controls.SetChildIndex(this.lblGengoTo, 0);
            this.Controls.SetChildIndex(this.txtGengoYearTo, 0);
            this.Controls.SetChildIndex(this.txtMonthTo, 0);
            this.Controls.SetChildIndex(this.lblYearTo, 0);
            this.Controls.SetChildIndex(this.txtDayTo, 0);
            this.Controls.SetChildIndex(this.lblMonthTo, 0);
            this.Controls.SetChildIndex(this.lblDayTo, 0);
            this.Controls.SetChildIndex(this.lblDateBet3, 0);
            this.Controls.SetChildIndex(this.txtTantoshaCdTo, 0);
            this.Controls.SetChildIndex(this.lblTantoshaNmTo, 0);
            this.Controls.SetChildIndex(this.lblShohinCd, 0);
            this.Controls.SetChildIndex(this.txtShohinCdFr, 0);
            this.Controls.SetChildIndex(this.lblShohinNmFr, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.txtShohinCdTo, 0);
            this.Controls.SetChildIndex(this.lblShohinNmTo, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.pnlDebug.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblDayFr;
        private System.Windows.Forms.Label lblMonthFr;
        private common.controls.FsiTextBox txtDayFr;
        private System.Windows.Forms.Label lblYearFr;
        private common.controls.FsiTextBox txtMonthFr;
        private common.controls.FsiTextBox txtGengoYearFr;
        private System.Windows.Forms.Label lblGengoFr;
        private System.Windows.Forms.Label lblDenpyoDate;
        private System.Windows.Forms.DataGridView dgvList;
        private System.Windows.Forms.Label lblDateBet1;
        private System.Windows.Forms.Label lblDayTo;
        private System.Windows.Forms.Label lblMonthTo;
        private common.controls.FsiTextBox txtDayTo;
        private System.Windows.Forms.Label lblYearTo;
        private common.controls.FsiTextBox txtMonthTo;
        private common.controls.FsiTextBox txtGengoYearTo;
        private System.Windows.Forms.Label lblGengoTo;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblTantoshaCd;
        private common.controls.FsiTextBox txtTantoshaCdFr;
        private System.Windows.Forms.Label lblTantoshaNmFr;
        private System.Windows.Forms.Label lblDateBet3;
        private common.controls.FsiTextBox txtTantoshaCdTo;
        private System.Windows.Forms.Label lblTantoshaNmTo;
        private System.Windows.Forms.Label lblShohinCd;
        private common.controls.FsiTextBox txtShohinCdFr;
        private System.Windows.Forms.Label lblShohinNmFr;
        private System.Windows.Forms.Label label2;
        private common.controls.FsiTextBox txtShohinCdTo;
        private System.Windows.Forms.Label lblShohinNmTo;

    }
}