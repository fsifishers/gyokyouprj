﻿using System.Data;

using jp.co.fsi.common.report;

namespace jp.co.fsi.edi.edi1031
{
    /// <summary>
    /// EDI1031R の概要の説明です。
    /// </summary>
    public partial class EDI1031R : BaseReport
    {

        public EDI1031R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }
    }
}
