﻿using System;
using System.ComponentModel;
using System.Data;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Collections.Generic;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;
using jp.co.fsi.common.constants;
using System.Diagnostics;
using System.Security.Policy;
using System.Net;

namespace jp.co.fsi.edi.edi2011
{
    /// <summary>
    /// 受領取込(EDI2011)
    /// </summary>
    public partial class EDI2011 : BasePgForm
    {
        #region 定数
        #endregion

        #region プロパティ
        /// <summary>
        /// 画面上最後となるフォーカスのEnterボタン押下時処理用変数
        /// </summary>
        private bool _dtFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._dtFlg;
            }
        }
        private System.Net.NetworkCredential ftpCredential;
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public EDI2011()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF4();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF4()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("更新", "実行しますか？") == DialogResult.Yes)
            {
                // 更新処理を実行
            }
        }

        public override void PressF12()
        {
            //存在しないのでコメントアウト 2019/10/25
            //EDI2012 frm = new EDI2012();
            //frm.Show();
        }

        #endregion

        #region イベント
        #endregion

        #region privateメソッド
        /// <summary>
        /// 会計年度の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool CheckKaikeiNendo()
        {
            // 会計年度の凍結処理チェック 凍結されていた場合はアラートを表示し処理を行なわない
            if (Util.GetKaikeiNendoFixedFlg(this.UInfo.KaikeiNendo, this.Dba))
            {
                Msg.Error("この会計年度は凍結されています。");
                return false;
            }

            // 選択会計年度が今年度かチェック
            DateTime today = DateTime.Today;
            if (Util.GetKaikeiNendo(today, this.Dba) != this.UInfo.KaikeiNendo)
            {
                if (Msg.ConfNmYesNo("更新", "選択会計年度が今年度ではありませんが、宜しいですか？") == DialogResult.No)
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 会計年度のチェック
            if (!CheckKaikeiNendo())
            {
                return false;
            }

            return true;
        }
        #endregion
    }
}
