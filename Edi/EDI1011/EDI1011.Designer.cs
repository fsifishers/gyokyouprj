﻿namespace jp.co.fsi.edi.edi1011
{
    partial class EDI1011
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dgvInputList = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.GYO_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FILE_NM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FILE_PATH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.chkFlg = new System.Windows.Forms.CheckBox();
            this.pnlDebug.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInputList)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Text = "";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Size = new System.Drawing.Size(847, 100);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.dgvInputList);
            this.groupBox1.Font = new System.Drawing.Font("ＭＳ ゴシック", 14.25F);
            this.groupBox1.Location = new System.Drawing.Point(17, 64);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(519, 329);
            this.groupBox1.TabIndex = 909;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "取込ファイル";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("ＭＳ ゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(16, 292);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(389, 19);
            this.label1.TabIndex = 908;
            this.label1.Text = "上記、選択されたマスタを取り込みます。";
            // 
            // dgvInputList
            // 
            this.dgvInputList.AllowUserToAddRows = false;
            this.dgvInputList.AllowUserToDeleteRows = false;
            this.dgvInputList.AllowUserToResizeColumns = false;
            this.dgvInputList.AllowUserToResizeRows = false;
            this.dgvInputList.ColumnHeadersHeight = 27;
            this.dgvInputList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvInputList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.GYO_NO,
            this.FILE_NM,
            this.FILE_PATH});
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("ＭＳ ゴシック", 14.25F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvInputList.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgvInputList.Location = new System.Drawing.Point(20, 37);
            this.dgvInputList.MultiSelect = false;
            this.dgvInputList.Name = "dgvInputList";
            this.dgvInputList.RowHeadersVisible = false;
            this.dgvInputList.RowTemplate.Height = 21;
            this.dgvInputList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvInputList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvInputList.Size = new System.Drawing.Size(478, 252);
            this.dgvInputList.TabIndex = 907;
            // 
            // Column1
            // 
            this.Column1.FalseValue = "0";
            this.Column1.HeaderText = "Colmun1";
            this.Column1.Name = "Column1";
            this.Column1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Column1.TrueValue = "1";
            this.Column1.Width = 50;
            // 
            // GYO_NO
            // 
            this.GYO_NO.HeaderText = "GYO_NO";
            this.GYO_NO.Name = "GYO_NO";
            this.GYO_NO.Visible = false;
            // 
            // FILE_NM
            // 
            this.FILE_NM.HeaderText = "FILE_NM";
            this.FILE_NM.Name = "FILE_NM";
            this.FILE_NM.ReadOnly = true;
            this.FILE_NM.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.FILE_NM.Width = 400;
            // 
            // FILE_PATH
            // 
            this.FILE_PATH.HeaderText = "FILE_PATH";
            this.FILE_PATH.Name = "FILE_PATH";
            this.FILE_PATH.Visible = false;
            // 
            // chkFlg
            // 
            this.chkFlg.AutoSize = true;
            this.chkFlg.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.chkFlg.Location = new System.Drawing.Point(26, 399);
            this.chkFlg.Name = "chkFlg";
            this.chkFlg.Size = new System.Drawing.Size(152, 17);
            this.chkFlg.TabIndex = 908;
            this.chkFlg.Text = "実行済みも表示する";
            this.chkFlg.UseVisualStyleBackColor = true;
            // 
            // EDI1011
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 638);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.chkFlg);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "EDI1011";
            this.Text = "";
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.chkFlg, 0);
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.pnlDebug.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInputList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgvInputList;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn GYO_NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn FILE_NM;
        private System.Windows.Forms.DataGridViewTextBoxColumn FILE_PATH;
        private System.Windows.Forms.CheckBox chkFlg;
    }
}