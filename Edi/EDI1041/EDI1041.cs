﻿using System;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.edi.edi1041
{
    /// <summary>
    /// 発注送信(EDI1041)
    /// </summary>
    public partial class EDI1041 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// 文字コード(SJIS)
        /// </summary>
        private const int CHR_CD_SJIS = 932; // "Shift_JIS"と同じはず
        #endregion

        #region メンバ変数（フィールド）
        // WebClientオブジェクト
        System.Net.WebClient wc;
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public EDI1041()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            string[] jpDate = Util.ConvJpDate(DateTime.Now, this.Dba);
            lblDateGengoFr.Text = jpDate[0];
            txtDateYearFr.Text = jpDate[2];
            txtDateMonthFr.Text = jpDate[3];
            txtDateDayFr.Text = jpDate[4];

            lblDateGengoTo.Text = jpDate[0];
            txtDateYearTo.Text = jpDate[2];
            txtDateMonthTo.Text = jpDate[3];
            txtDateDayTo.Text = jpDate[4];

            // 初期フォーカス
            txtDateYearFr.Focus();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 日付にフォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtDateYearFr":
                case "txtDateYearTo":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;

            switch (this.ActiveCtlNm)
            {
                case "txtDateYearFr":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom("CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblDateGengoFr.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.lblDateGengoFr.Text = result[1];

                                // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
                                DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                                    this.txtDateMonthFr.Text, "1", this.Dba);
                                int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);
                                if (Util.ToInt(this.txtDateDayFr.Text) > lastDayInMonth)
                                {
                                    this.txtDateDayFr.Text = Util.ToString(lastDayInMonth);
                                }

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblDateGengoFr.Text,
                                        this.txtDateYearFr.Text,
                                        this.txtDateMonthFr.Text,
                                        this.txtDateDayFr.Text,
                                        this.Dba);
                                this.lblDateGengoFr.Text = arrJpDate[0];
                                this.txtDateYearFr.Text = arrJpDate[2];
                                this.txtDateMonthFr.Text = arrJpDate[3];
                                this.txtDateDayFr.Text = arrJpDate[4];
                            }
                        }
                    }
                    break;

                case "txtDateYearTo":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom("CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblDateGengoTo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.lblDateGengoTo.Text = result[1];

                                // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
                                DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                                    this.txtDateMonthTo.Text, "1", this.Dba);
                                int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);
                                if (Util.ToInt(this.txtDateDayTo.Text) > lastDayInMonth)
                                {
                                    this.txtDateDayTo.Text = Util.ToString(lastDayInMonth);
                                }

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblDateGengoTo.Text,
                                        this.txtDateYearTo.Text,
                                        this.txtDateMonthTo.Text,
                                        this.txtDateDayTo.Text,
                                        this.Dba);
                                this.lblDateGengoTo.Text = arrJpDate[0];
                                this.txtDateYearTo.Text = arrJpDate[2];
                                this.txtDateMonthTo.Text = arrJpDate[3];
                                this.txtDateDayTo.Text = arrJpDate[4];
                            }
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF6();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF4()
        {
            DateTime today = DateTime.Today;

            // 入力日付に該当する会計年度が今年度と一致するかチェック
            DateTime DENPYO_DATE_FR = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                    this.txtDateMonthFr.Text, this.txtDateDayFr.Text, this.Dba);
            if (Util.GetKaikeiNendo(DENPYO_DATE_FR, this.Dba) != Util.GetKaikeiNendo(today, this.Dba))
            {
                if (Msg.ConfNmYesNo("発注送信データ作成", "入力日付(自)が今年度の会計年度の範囲外ですが、宜しいですか？") == DialogResult.No)
                {
                    return;
                }
            }
            DateTime DENPYO_DATE_TO = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                    this.txtDateMonthTo.Text, this.txtDateDayTo.Text, this.Dba);
            if (Util.GetKaikeiNendo(DENPYO_DATE_TO, this.Dba) != Util.GetKaikeiNendo(today, this.Dba))
            {
                if (Msg.ConfNmYesNo("発注送信データ作成", "入力日付(至)が今年度の会計年度の範囲外ですが、宜しいですか？") == DialogResult.No)
                {
                    return;
                }
            }

            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            // 送信データ作成＆出力
            //OutputSendFiles("new");

            this.txtDateYearFr.Focus();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 和暦(年)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateYearFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidDateYear(this.txtDateYearFr.Text))
            {
                e.Cancel = true;
                this.txtDateYearFr.SelectAll();
            }
            else
            {
                // 空の場合、0年として処理
                if (ValChk.IsEmpty(this.txtDateYearFr.Text))
                {
                    this.txtDateYearFr.Text = "0";
                }

                // 年月日(自)の月末入力チェック
                this.txtDateDayFr.Text = CheckDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text, this.txtDateMonthFr.Text, this.txtDateDayFr.Text);
                SetDateFr();
            }
        }

        /// <summary>
        /// 和暦(月)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateMonth_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidDateMonth(this.txtDateMonthFr.Text))
            {
                e.Cancel = true;
                this.txtDateMonthFr.SelectAll();
            }
            else
            {
                // 入力月が範囲外時の場合に1又は12月を返す
                this.txtDateMonthFr.Text = ValueOfTheCaseOutsideTheMonthRange(this.txtDateMonthFr.Text);

                // 年月日(自)の月末入力チェック
                this.txtDateDayFr.Text = CheckDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text, this.txtDateMonthFr.Text, this.txtDateDayFr.Text);
                SetDateFr();
            }
        }

        /// <summary>
        /// 和暦(日)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateDay_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidDateDay(this.txtDateDayFr.Text))
            {
                e.Cancel = true;
                this.txtDateDayFr.SelectAll();
            }
            else
            {
                // 入力日が範囲外時の場合に1日を返す
                this.txtDateDayFr.Text = this.ValueOfTheCaseOutsideTheDateRange(this.txtDateDayFr.Text);

                // 年月日(自)の月末入力チェック
                this.txtDateDayFr.Text = CheckDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text, this.txtDateMonthFr.Text, this.txtDateDayFr.Text);
                SetDateFr();
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 年(自)の入力チェック
        /// </summary>
        /// <param name="dateYear">入力年</param>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidDateYear(string dateYear)
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(dateYear))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 月(自)の入力チェック
        /// </summary>
        /// <param name="dateMonth">入力月</param>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidDateMonth(string dateMonth)
        {
            // 数字以外が入力されたらエラーメッセージ
            //if (!ValChk.IsNumber(this.txtDateMonthFr.Text))
            if (!ValChk.IsNumber(dateMonth))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 日(自)の入力チェック
        /// </summary>
        /// <param name="dateDay">入力日</param>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidDateDay(string dateDay)
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(dateDay))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 年月日(自)の月末入力チェック
        /// </summary>
        /// <param name="dateGengo">元号</param>
        /// <param name="dateYear">入力年</param>
        /// <param name="dateMonth">入力月</param>
        /// <param name="dateDay">入力日</param>
        /// <returns>string 月末日</returns>
        /// 
        private string CheckDate(string dateGengo, string dateYear, string dateMonth, string dateDay)
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(dateGengo, dateYear, dateMonth, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            //if (Util.ToInt(this.txtDateDayFr.Text) > lastDayInMonth)
            if (Util.ToInt(dateDay) > lastDayInMonth)
            {
                //this.txtDateDayFr.Text = Util.ToString(lastDayInMonth);
                dateDay = Util.ToString(lastDayInMonth);
            }

            return dateDay;
        }

        /// <summary>
        /// 年月日(自)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetDateFr()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDateFr(Util.FixJpDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                this.txtDateMonthFr.Text, this.txtDateDayFr.Text, this.Dba));
        }

        /// <summary>
        /// 年月日(至)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetDateTo()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDateTo(Util.FixJpDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                this.txtDateMonthTo.Text, this.txtDateDayTo.Text, this.Dba));
        }

        /// <summary>
        /// 和暦(年)(至)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateYearTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidDateYear(this.txtDateYearTo.Text))
            {
                e.Cancel = true;
                this.txtDateYearTo.SelectAll();
            }
            else
            {
                // 空の場合、0年として処理
                if (ValChk.IsEmpty(this.txtDateYearTo.Text))
                {
                    this.txtDateYearTo.Text = "0";
                }

                // 年月日(自)の月末入力チェック
                this.txtDateDayTo.Text = CheckDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text, this.txtDateMonthTo.Text, this.txtDateDayTo.Text);
                SetDateTo();
            }
        }

        /// <summary>
        /// 和暦(月)(至)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateMonthTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidDateMonth(this.txtDateMonthTo.Text))
            {
                e.Cancel = true;
                this.txtDateMonthTo.SelectAll();
            }
            else
            {
                // 入力月が範囲外時の場合に1又は12月を返す
                this.txtDateMonthTo.Text = ValueOfTheCaseOutsideTheMonthRange(this.txtDateMonthTo.Text);

                // 年月日(自)の月末入力チェック
                this.txtDateDayTo.Text = CheckDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text, this.txtDateMonthTo.Text, this.txtDateDayTo.Text);
                SetDateTo();
            }
        }

        /// <summary>
        /// 和暦(日)(至)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateDayTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidDateDay(this.txtDateDayTo.Text))
            {
                e.Cancel = true;
                this.txtDateDayTo.SelectAll();
            }
            else
            {
                // 入力日が範囲外時の場合に1日を返す
                this.txtDateDayTo.Text = this.ValueOfTheCaseOutsideTheDateRange(this.txtDateDayTo.Text);

                // 年月日(自)の月末入力チェック
                this.txtDateDayTo.Text = CheckDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text, this.txtDateMonthTo.Text, this.txtDateDayTo.Text);
                SetDateTo();
            }
        }

        /// <summary>
        /// 入力月が範囲外時の場合に1又は12月を返す（入力チェック後に呼び出すこと）
        /// </summary>
        /// <param name="dateMonth">入力された月</param>
        /// <returns>月の範囲内に収めた月</returns>
        private string ValueOfTheCaseOutsideTheMonthRange(string dateMonth)
        {
            // 空の場合、1月として処理
            if (ValChk.IsEmpty(dateMonth))
            {
                dateMonth = "1";
            }

            int inputMonth = Util.ToInt(dateMonth);

            // 12を超える月が入力された場合、12月として処理
            if (inputMonth > 12)
            {
                dateMonth = "12";
            }
            // 1より小さい月が入力された場合、1月として処理
            else if (inputMonth < 1)
            {
                dateMonth = "1";
            }

            return dateMonth;
        }

        /// <summary>
        /// 入力日が範囲外時の日を返す
        /// </summary>
        /// <returns>string 年月日の日</returns>
        private string ValueOfTheCaseOutsideTheDateRange(string dateDay)
        {
            if (ValChk.IsEmpty(dateDay))
            {
                // 空の場合、1日として処理
                dateDay = "1";
            }
            else
            {
                // 1より小さい日が入力された場合、1日として処理
                if (Util.ToInt(dateDay) < 1)
                {
                    dateDay = "1";
                }
            }

            return dateDay;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 年(自)のチェック
            if (!IsValidDateYear(this.txtDateYearFr.Text))
            {
                this.txtDateYearFr.Focus();
                this.txtDateYearFr.SelectAll();
                return false;
            }
            // 月(自)のチェック
            if (!IsValidDateMonth(this.txtDateMonthFr.Text))
            {
                this.txtDateMonthFr.Focus();
                this.txtDateMonthFr.SelectAll();
                return false;
            }
            else
            {
                // 入力月が範囲外時の場合に1又は12月を返す
                this.txtDateMonthFr.Text = ValueOfTheCaseOutsideTheMonthRange(this.txtDateMonthFr.Text);
            }
            // 日(自)のチェック
            if (!IsValidDateDay(this.txtDateDayTo.Text))
            {
                this.txtDateDayFr.Focus();
                this.txtDateDayFr.SelectAll();
                return false;
            }
            else
            {
                // 入力日が範囲外時の場合に1日を返す
                this.txtDateDayFr.Text = this.ValueOfTheCaseOutsideTheDateRange(this.txtDateDayFr.Text);
            }
            // 年月日(自)の月末入力チェック処理
            this.txtDateDayFr.Text = CheckDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text, this.txtDateMonthFr.Text, this.txtDateDayFr.Text);
            // 年月日(自)の正しい和暦への変換処理
            SetDateFr();

            // 年(至)のチェック
            if (!IsValidDateYear(this.txtDateYearTo.Text))
            {
                this.txtDateYearTo.Focus();
                this.txtDateYearTo.SelectAll();
                return false;
            }
            // 月(至)のチェック
            if (!IsValidDateMonth(this.txtDateMonthTo.Text))
            {
                this.txtDateMonthTo.Focus();
                this.txtDateMonthTo.SelectAll();
                return false;
            }
            else
            {
                // 入力月が範囲外時の場合に1又は12月を返す
                this.txtDateMonthTo.Text = ValueOfTheCaseOutsideTheMonthRange(this.txtDateMonthTo.Text);
            }
            // 日(至)のチェック
            if (!IsValidDateDay(this.txtDateDayTo.Text))
            {
                this.txtDateDayTo.Focus();
                this.txtDateDayTo.SelectAll();
                return false;
            }
            else
            {
                // 入力日が範囲外時の場合に1日を返す
                this.txtDateDayTo.Text = this.ValueOfTheCaseOutsideTheDateRange(this.txtDateDayTo.Text);
            }
            // 年月日(自)の月末入力チェック処理
            this.txtDateDayTo.Text = CheckDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text, this.txtDateMonthTo.Text, this.txtDateDayTo.Text);
            // 年月日(自)の正しい和暦への変換処理
            SetDateTo();

            return true;
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。(自)
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpDateFr(string[] arrJpDate)
        {
            this.lblDateGengoFr.Text = arrJpDate[0];
            this.txtDateYearFr.Text = arrJpDate[2];
            this.txtDateMonthFr.Text = arrJpDate[3];
            this.txtDateDayFr.Text = arrJpDate[4];
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。(至)
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpDateTo(string[] arrJpDate)
        {
            this.lblDateGengoTo.Text = arrJpDate[0];
            this.txtDateYearTo.Text = arrJpDate[2];
            this.txtDateMonthTo.Text = arrJpDate[3];
            this.txtDateDayTo.Text = arrJpDate[4];
        }
        #endregion
    }
}
