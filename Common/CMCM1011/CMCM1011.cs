﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.cm.cmcm1011
{
    /// <summary>
    /// 会計年度選択(CMCM1011)
    /// </summary>
    public partial class CMCM1011 : BasePgForm
    {
        #region プロパティ
        /// <summary>
        /// キーダウン処理フラグ
        /// </summary>
        private bool _flg = false;
        public bool Flg()
        {
            return _flg;
        }
        /// <summary>
        /// 選択されている行のインデックスを保持
        /// </summary>
        private DataGridViewRow _SelectRowIndex = new DataGridViewRow();
        public DataGridViewRow SelectRowIndex()
        {
            return _SelectRowIndex;
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public CMCM1011()
        {
            InitializeComponent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // ログイン担当者情報を取得する
            Decimal cd = Util.ToDecimal(this.UInfo.UserCd);
            DataTable dt = Util.GetTantoshaDt(cd, this.Dba);
            // 担当者が存在しない場合
            if(dt.Rows.Count == 0)
            {
                Msg.Error("担当者コード：" + this.UInfo.UserCd + " は、不正なログインです。");
                this.Close();
                return;
            }
            // ログイン担当者の権限を取得する
            Decimal kbn = Util.ToDecimal(dt.Rows[0]["USER_KUBUN"]);

            // データを取得して表示用に加工
            DataTable dtDispData = GetDispList();

            this.dgvList.DataSource = dtDispData;

            // ユーザーによるソートを禁止させる
            foreach (DataGridViewColumn c in this.dgvList.Columns)
                c.SortMode = DataGridViewColumnSortMode.NotSortable;

            // フォントを設定する
            this.dgvList.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F);
            this.dgvList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.dgvList.DefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F);

            // 列幅を設定する
            this.dgvList.Columns[0].Width = 45;
            this.dgvList.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.dgvList.Columns[1].Width = 250;
            this.dgvList.Columns[2].Width = 60;
            this.dgvList.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.dgvList.Columns[3].Width = 175;
            this.dgvList.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.dgvList.Columns[4].Width = 50;
            this.dgvList.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.dgvList.Columns[5].Width = 75;
            this.dgvList.Columns[5].Visible = false;
            // 管理者の場合
            if(kbn == 1)
            {
                // 画面サイズを変更する
                this.Size = new Size(650, 395);
                this.dgvList.Size = new Size(600, 267);
            }
            else
            // 管理者でない場合
            {

                // 凍結フラグを非表示にする
                this.dgvList.Columns[4].Visible = false;
            }

            // 引数で渡された元号と一致する行を初期選択する
            this.dgvList.Rows[0].Selected = true;
            if (!ValChk.IsEmpty(this.InData))
            {
                for (int i = 0; i < this.dgvList.Rows.Count; i++)
                {
                    if (Util.ToString(this.InData).Equals(
                        Util.ToString(this.dgvList.Rows[i].Cells["決算期"].Value)))
                    {
                        this.dgvList.Rows[i].Selected = true;
                        break;
                    }
                }
            }

            // Gridに初期フォーカス
            this.dgvList.Focus();
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }
        #endregion

        #region イベント
        /// <summary>
        /// グリッドでのキーダウン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                // 凍結済みかの確認
                if (!CheckFlg())
                {
                    _SelectRowIndex = this.dgvList.SelectedRows[0];

                    // キーダウン処理フラグを立てる
                    _flg = true;

                    return;
                }

                ReturnVal();
            }
        }

        /// <summary>
        /// グリッドのセルエンター押下時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (_flg)
            {
                _SelectRowIndex.Selected = true;

                // キーダウン処理フラグを戻す
                _flg = false;
            }
        }

        /// <summary>
        /// グリッドのセルクリック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            // チェック列以外は処理しない
            if (e.RowIndex == -1 || this.dgvList.Columns[e.ColumnIndex].ValueType != typeof(bool))
            {
                return;
            }

            // 編集モードを終了する
            this.dgvList.EndEdit();
            
            try
            {
                // トランザクションの開始
                this.Dba.BeginTransaction();

                // ON ⇒ OFF の場合
                if (this.dgvList[e.ColumnIndex, e.RowIndex].Value.Equals(true))
                {
                    if (Msg.ConfNmYesNo("プレビュー", this.dgvList[5, e.RowIndex].Value + "年度の凍結を解除してよろしいですか？") == DialogResult.Yes)
                    {
                        updateFixFlg(0, Util.ToDecimal(this.dgvList[5, e.RowIndex].Value));
                        Msg.Info("凍結解除処理が完了しました。");
                        // チェックを外す
                        this.dgvList[e.ColumnIndex, e.RowIndex].Value = false;
                    }
                }
                // OFF ⇒ ON の場合
                else
                {
                    if (Msg.ConfNmYesNo("プレビュー", this.dgvList[5, e.RowIndex].Value + "年度を凍結してよろしいですか？") == DialogResult.Yes)
                    {
                        updateFixFlg(1, Util.ToDecimal(this.dgvList[5, e.RowIndex].Value));
                        Msg.Info("凍結処理が完了しました。");
                        // チェックを付ける
                        this.dgvList[e.ColumnIndex, e.RowIndex].Value = true;
                    }
                }

                // トランザクションをコミット
                this.Dba.Commit();
            }
            catch
            {
                Msg.Info("凍結処理に失敗しました。");
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }
        }

        /// <summary>
        /// グリッドのセルダブルクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            // 凍結済みかを確認
            if (!CheckFlg())
            {
                return;
            }

            ReturnVal();
        }

        /// <summary>
        /// Enterボタンクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEnter_Click(object sender, EventArgs e)
        {
            // 凍結済みかを確認
            if (!CheckFlg())
            {
                return;

            }

            ReturnVal();
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 表示するデータのリストを取得する
        /// </summary>
        /// <returns>表示するデータ</returns>
        private DataTable GetDispList()
        {
            DataTable dtDBData =
                this.Dba.GetDataTableByCondition("KAISHA_CD, KAISHA_NM, KESSANKI, KAIKEI_NENDO, KAIKEI_KIKAN_KAISHIBI, KAIKEI_KIKAN_SHURYOBI, FIX_FLG",
                    "VI_ZM_KAISHA_JOHO", null, "KAISHA_CD ASC, KESSANKI DESC");

            DataTable dtResult = new DataTable();
            DataRow drResult;
            dtResult.Columns.Add("ｺｰﾄﾞ", typeof(int));
            dtResult.Columns.Add("会　　社　　名", typeof(string));
            dtResult.Columns.Add("決算期", typeof(int));
            dtResult.Columns.Add("会計期間", typeof(string));
            dtResult.Columns.Add("凍結", typeof(bool));
            dtResult.Columns.Add("会計年度", typeof(int));

            string kaikeiKikan;
            string[] arrJpDate;

            for (int i = 0; i < dtDBData.Rows.Count; i++)
            {
                drResult = dtResult.NewRow();
                drResult["ｺｰﾄﾞ"] = dtDBData.Rows[i]["KAISHA_CD"];
                drResult["会　　社　　名"] = dtDBData.Rows[i]["KAISHA_NM"];
                drResult["決算期"] = dtDBData.Rows[i]["KESSANKI"];
                arrJpDate = Util.ConvJpDate(Util.ToDate(dtDBData.Rows[i]["KAIKEI_KIKAN_KAISHIBI"]), this.Dba);
                kaikeiKikan = arrJpDate[6];
                arrJpDate = Util.ConvJpDate(Util.ToDate(dtDBData.Rows[i]["KAIKEI_KIKAN_SHURYOBI"]), this.Dba);
                kaikeiKikan += "～" + arrJpDate[6];
                drResult["会計期間"] = kaikeiKikan;
                drResult["凍結"] = dtDBData.Rows[i]["FIX_FLG"];
                drResult["会計年度"] = dtDBData.Rows[i]["KAIKEI_NENDO"];
                dtResult.Rows.Add(drResult);
            }

            return dtResult;
        }

        /// <summary>
        /// 凍結フラグ更新処理
        /// </summary>
        private void updateFixFlg(Decimal flg, Decimal kaikeiNendo)
        {
            // 処理フラグのアップデート
            DbParamCollection updParam = new DbParamCollection();
            updParam.SetParam("@FIX_FLG", SqlDbType.Decimal, 4, flg);
            DbParamCollection whereParam = new DbParamCollection();
            whereParam.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, kaikeiNendo);

            this.Dba.Update("TB_ZM_KAISHA_JOHO", updParam, "KAIKEI_NENDO = @KAIKEI_NENDO", whereParam);
        }

        /// <summary>
        /// 凍結フラグが立っているかのチェック
        /// </summary>
        private bool CheckFlg()
        {
            if (this.dgvList.SelectedRows[0].Cells[4].Value.Equals(true))
            {
                // 選択自体は許可する為、処理をコメントアウト。 2016/06/20 kisemori
                return true;
                //Msg.Notice("この決算期は凍結されているため選択することが出来ません。");
                //return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// 呼び出し元に戻り値を返す
        /// </summary>
        private void ReturnVal()
        {
            this.OutData = new string[4] { 
                Util.ToString(this.dgvList.SelectedRows[0].Cells[0].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells[1].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells[2].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells[3].Value)
            };
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        #endregion
    }
}
