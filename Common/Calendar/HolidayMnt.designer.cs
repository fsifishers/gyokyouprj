﻿namespace jp.co.fsi.com.calendar
{
    partial class HolidayMnt
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvList = new System.Windows.Forms.DataGridView();
            this.btnPrevYear = new System.Windows.Forms.Button();
            this.btnNextYear = new System.Windows.Forms.Button();
            this.btnMoveMonth = new System.Windows.Forms.Button();
            this.lblYear = new System.Windows.Forms.Label();
            this.txtYear = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtGengo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblYearAddInfo = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvList)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Size = new System.Drawing.Size(519, 23);
            this.lblTitle.Text = "祝日メンテナンス";
            // 
            // btnEsc
            // 
            this.btnEsc.Location = new System.Drawing.Point(3, 49);
            // 
            // btnF1
            // 
            this.btnF1.Visible = false;
            // 
            // btnF2
            // 
            this.btnF2.Visible = false;
            // 
            // btnF3
            // 
            this.btnF3.Text = "F3\r\n\r\n行削除";
            this.btnF3.Visible = false;
            // 
            // btnF4
            // 
            this.btnF4.Text = "F4\r\n\r\n行追加";
            this.btnF4.Visible = false;
            // 
            // btnF5
            // 
            this.btnF5.Text = "F5\r\n\r\n前年複写";
            this.btnF5.Visible = false;
            // 
            // btnF7
            // 
            this.btnF7.Text = "F7\r\n\r\n前年";
            // 
            // btnF6
            // 
            this.btnF6.Text = "F6\r\n\r\n登録";
            // 
            // btnF8
            // 
            this.btnF8.Text = "F8\r\n\r\n翌年";
            this.btnF8.Visible = false;
            // 
            // btnF9
            // 
            this.btnF9.Visible = false;
            // 
            // btnF12
            // 
            this.btnF12.Visible = false;
            // 
            // btnF11
            // 
            this.btnF11.Visible = false;
            // 
            // btnF10
            // 
            this.btnF10.Visible = false;
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(12, 495);
            this.pnlDebug.Size = new System.Drawing.Size(1096, 100);
            // 
            // dgvList
            // 
            this.dgvList.AllowUserToAddRows = false;
            this.dgvList.AllowUserToDeleteRows = false;
            this.dgvList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvList.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dgvList.ImeMode = System.Windows.Forms.ImeMode.On;
            this.dgvList.Location = new System.Drawing.Point(12, 91);
            this.dgvList.MultiSelect = false;
            this.dgvList.Name = "dgvList";
            this.dgvList.RowHeadersVisible = false;
            this.dgvList.RowTemplate.Height = 21;
            this.dgvList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvList.Size = new System.Drawing.Size(520, 402);
            this.dgvList.TabIndex = 2;
            this.dgvList.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvList_CellEnter);
            this.dgvList.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvList_DataError);
            // 
            // btnPrevYear
            // 
            this.btnPrevYear.Location = new System.Drawing.Point(12, 42);
            this.btnPrevYear.Name = "btnPrevYear";
            this.btnPrevYear.Size = new System.Drawing.Size(75, 23);
            this.btnPrevYear.TabIndex = 902;
            this.btnPrevYear.Text = "<< 前年";
            this.btnPrevYear.UseVisualStyleBackColor = true;
            this.btnPrevYear.Click += new System.EventHandler(this.btnPrevYear_Click);
            // 
            // btnNextYear
            // 
            this.btnNextYear.Location = new System.Drawing.Point(457, 42);
            this.btnNextYear.Name = "btnNextYear";
            this.btnNextYear.Size = new System.Drawing.Size(75, 23);
            this.btnNextYear.TabIndex = 903;
            this.btnNextYear.Text = "翌年 >>";
            this.btnNextYear.UseVisualStyleBackColor = true;
            this.btnNextYear.Click += new System.EventHandler(this.btnNextYear_Click);
            // 
            // btnMoveMonth
            // 
            this.btnMoveMonth.Location = new System.Drawing.Point(292, 42);
            this.btnMoveMonth.Name = "btnMoveMonth";
            this.btnMoveMonth.Size = new System.Drawing.Size(51, 23);
            this.btnMoveMonth.TabIndex = 907;
            this.btnMoveMonth.Text = "移動";
            this.btnMoveMonth.UseVisualStyleBackColor = true;
            this.btnMoveMonth.Click += new System.EventHandler(this.btnMoveMonth_Click);
            // 
            // lblYear
            // 
            this.lblYear.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblYear.Location = new System.Drawing.Point(273, 43);
            this.lblYear.Name = "lblYear";
            this.lblYear.Size = new System.Drawing.Size(20, 20);
            this.lblYear.TabIndex = 906;
            this.lblYear.Text = "年";
            this.lblYear.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtYear
            // 
            this.txtYear.AutoSizeFromLength = true;
            this.txtYear.DisplayLength = null;
            this.txtYear.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtYear.Location = new System.Drawing.Point(251, 43);
            this.txtYear.MaxLength = 2;
            this.txtYear.Name = "txtYear";
            this.txtYear.Size = new System.Drawing.Size(20, 20);
            this.txtYear.TabIndex = 905;
            this.txtYear.Text = "26";
            this.txtYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtGengo
            // 
            this.txtGengo.AutoSizeFromLength = true;
            this.txtGengo.BackColor = System.Drawing.Color.Silver;
            this.txtGengo.DisplayLength = null;
            this.txtGengo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtGengo.Location = new System.Drawing.Point(215, 43);
            this.txtGengo.MaxLength = 4;
            this.txtGengo.Name = "txtGengo";
            this.txtGengo.ReadOnly = true;
            this.txtGengo.Size = new System.Drawing.Size(34, 20);
            this.txtGengo.TabIndex = 904;
            this.txtGengo.TabStop = false;
            this.txtGengo.Text = "平成";
            // 
            // lblYearAddInfo
            // 
            this.lblYearAddInfo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblYearAddInfo.Location = new System.Drawing.Point(215, 66);
            this.lblYearAddInfo.Name = "lblYearAddInfo";
            this.lblYearAddInfo.Size = new System.Drawing.Size(128, 20);
            this.lblYearAddInfo.TabIndex = 908;
            this.lblYearAddInfo.Text = "(2014年)";
            this.lblYearAddInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // HolidayMnt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(544, 598);
            this.Controls.Add(this.lblYearAddInfo);
            this.Controls.Add(this.btnMoveMonth);
            this.Controls.Add(this.lblYear);
            this.Controls.Add(this.txtYear);
            this.Controls.Add(this.txtGengo);
            this.Controls.Add(this.btnNextYear);
            this.Controls.Add(this.btnPrevYear);
            this.Controls.Add(this.dgvList);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.MinimizeBox = false;
            this.Name = "HolidayMnt";
            this.ShowFButton = true;
            this.Text = "祝日メンテナンス";
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.dgvList, 0);
            this.Controls.SetChildIndex(this.btnPrevYear, 0);
            this.Controls.SetChildIndex(this.btnNextYear, 0);
            this.Controls.SetChildIndex(this.txtGengo, 0);
            this.Controls.SetChildIndex(this.txtYear, 0);
            this.Controls.SetChildIndex(this.lblYear, 0);
            this.Controls.SetChildIndex(this.btnMoveMonth, 0);
            this.Controls.SetChildIndex(this.lblYearAddInfo, 0);
            this.pnlDebug.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvList;
        private System.Windows.Forms.Button btnPrevYear;
        private System.Windows.Forms.Button btnNextYear;
        private System.Windows.Forms.Button btnMoveMonth;
        private System.Windows.Forms.Label lblYear;
        private common.controls.FsiTextBox txtYear;
        private common.controls.FsiTextBox txtGengo;
        private System.Windows.Forms.Label lblYearAddInfo;

    }
}