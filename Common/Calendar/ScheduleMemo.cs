﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.com.calendar
{
    /// <summary>
    /// スケジュールメモ画面(ScheduleMemo)
    /// </summary>
    public partial class ScheduleMemo : BasePgForm
    {
        #region private変数
        /// <summary>
        /// 更新対象のSEQ
        /// </summary>
        private int _targetSeq;
        #endregion

        #region プロパティ
        private DateTime _targetDate;
        /// <summary>
        /// 対象日付
        /// </summary>
        public DateTime TargetDate
        {
            get
            {
                return this._targetDate;
            }
            set
            {
                this._targetDate = value;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ScheduleMemo()
        {
            InitializeComponent();
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // EscapeとF6、F7のみ表示
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF6.Location = this.btnF2.Location;
            this.btnF7.Location = this.btnF3.Location;
            this.btnF1.Visible = false;
            this.btnF2.Visible = false;
            this.btnF3.Visible = false;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF6.Visible = true;
            this.btnF7.Visible = true;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;

            // 対象日付を画面に表示
            string[] aryJpDate = Util.ConvJpDate(this._targetDate, this.Dba);
            this.lblDate.Text = aryJpDate[5];

            // 対象日付のスケジュールを取得して画面に表示
            DispData();
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        public override void PressF6()
        {
            // 登録or更新処理
            // 全項目チェック
            if (!ValidateAll())
            {
                return;
            }


            // 確認メッセージを表示
            if (this._targetSeq == 0)
            {
                if (Msg.ConfOKCancel("新規にメモを登録します。よろしいですか？") == DialogResult.Cancel)
                {
                    return;
                }
            }
            else
            {
                if (Msg.ConfOKCancel("選択された行のメモを編集します。よろしいですか？") == DialogResult.Cancel)
                {
                    return;
                }
            }

            try
            {
                this.Dba.BeginTransaction();

                // 対象行のIDが0なら登録処理、それ以外なら更新処理として扱う
                if (this._targetSeq == 0)
                {
                    // 登録処理
                    InsertData();
                }
                else
                {
                    // 更新処理
                    UpdateData();
                }

                this.Dba.Commit();

                // 再表示
                DispData();

                // メッセージ表示
                Msg.Info("登録が完了しました。");
            }
            catch (Exception)
            {
                Msg.Info("登録に失敗しました。");
            }
            finally
            {
                this.Dba.Rollback();
            }
        }

        /// <summary>
        /// F7キー押下時処理
        /// </summary>
        public override void PressF7()
        {
            // 削除処理
            // 対象行のIDが0(新規行)ならスルー
            if (this._targetSeq == 0)
            {
                return;
            }

            // 確認メッセージを表示
            if (Msg.ConfOKCancel("選択したメモを削除します。よろしいですか？") == DialogResult.Cancel)
            {
                return;
            }

            try
            {
                this.Dba.BeginTransaction();

                // 削除を実行
                DeleteData();

                this.Dba.Commit();

                // 再表示
                DispData();

                // メッセージ表示
                Msg.Info("削除が完了しました。");
            }
            catch (Exception)
            {
                Msg.Info("削除に失敗しました。");
            }
            finally
            {
                this.Dba.Rollback();
            }
        }
        #endregion

        #region イベント
        /// <summary>
        /// GridのCurrentCellChangedイベントハンドラ
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_CurrentCellChanged(object sender, EventArgs e)
        {
            try
            {
                this._targetSeq = Util.ToInt(this.dgvList.SelectedRows[0].Cells["SEQ"].Value);
                this.txtMemo.Text = Util.ToString(this.dgvList.SelectedRows[0].Cells["メモ"].Value);
                this.txtSort.Text = Util.ToString(this.dgvList.SelectedRows[0].Cells["表示順"].Value);
                if (ValChk.IsEmpty(this.dgvList.SelectedRows[0].Cells["OPEN_KUBUN"].Value) || Util.ToInt(this.dgvList.SelectedRows[0].Cells["OPEN_KUBUN"].Value) == 1)
                {
                    this.rdbPublic.Checked = true;
                }
                else
                {
                    this.rdbPersonal.Checked = true;
                }
            }
            catch (Exception)
            {
                // 無理矢理1行目の情報を設定
                this._targetSeq = Util.ToInt(this.dgvList.Rows[0].Cells["SEQ"].Value);
                this.txtMemo.Text = Util.ToString(this.dgvList.Rows[0].Cells["メモ"].Value);
                this.txtSort.Text = Util.ToString(this.dgvList.Rows[0].Cells["表示順"].Value);
                if (ValChk.IsEmpty(this.dgvList.Rows[0].Cells["OPEN_KUBUN"].Value) || Util.ToInt(this.dgvList.Rows[0].Cells["OPEN_KUBUN"].Value) == 1)
                {
                    this.rdbPublic.Checked = true;
                }
                else
                {
                    this.rdbPersonal.Checked = true;
                }
            }

            if (this._targetSeq == 0)
            {
                this.txtMemo.Text = string.Empty;
            }
        }

        /// <summary>
        /// メモの検証時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMemo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidMemo())
            {
                e.Cancel = true;
                this.txtMemo.SelectAll();
            }
        }

        /// <summary>
        /// 表示順の検証時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSort_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidSort())
            {
                e.Cancel = true;
                this.txtSort.SelectAll();
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// データを表示する
        /// </summary>
        private void DispData()
        {
            // 対象データをDBから取得
            DataTable dtData = SearchData();

            // 一覧に反映
            this.dgvList.DataSource = dtData;

            // ユーザーによるソートを禁止させる
            foreach (DataGridViewColumn c in this.dgvList.Columns)
                c.SortMode = DataGridViewColumnSortMode.NotSortable;

            // フォントを設定する
            this.dgvList.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F, FontStyle.Bold);
            this.dgvList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.dgvList.DefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F);

            // 列幅を設定する
            this.dgvList.Columns[0].Width = 98;
            this.dgvList.Columns[1].Width = 325;
            this.dgvList.Columns[2].Width = 60;
            this.dgvList.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvList.Columns[3].Visible = false;
            this.dgvList.Columns[4].Visible = false;

            // 1行目を選択状態にする
            this.dgvList.Rows[0].Selected = true;
            this.dgvList.Focus();
        }

        /// <summary>
        /// データを検索する
        /// </summary>
        private DataTable SearchData()
        {
            // TB_CM_MEMOからその日のメモを取得する
            StringBuilder cols = new StringBuilder();
            cols.Append("CASE OPEN_KUBUN WHEN 1 THEN '全社' WHEN 9 THEN '個人' ELSE '' END AS 公開範囲");
            cols.Append(", MEMO_CONTENT AS メモ");
            cols.Append(", SORT AS 表示順");
            cols.Append(", SEQ");
            cols.Append(", OPEN_KUBUN");

            StringBuilder where = new StringBuilder();
            where.Append("DATE = @DATE");
            where.Append(" AND (OPEN_KUBUN = 1 OR (OPEN_KUBUN = 9 AND TARGET_USER = @TARGET_USER))");
            where.Append(" AND DEL_FLG = 0");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@DATE", SqlDbType.DateTime, this._targetDate);
            dpc.SetParam("@TARGET_USER", SqlDbType.VarChar, 30, this.UInfo.UserId);

            DataTable dtData = this.Dba.GetDataTableByConditionWithParams(cols.ToString(), "TB_CM_MEMO",
                where.ToString(), "OPEN_KUBUN, SORT", dpc);

            // 新規データ用の行を用意する
            DataRow newRow = dtData.NewRow();
            newRow["公開範囲"] = DBNull.Value;
            newRow["メモ"] = "(新規行)";
            newRow["表示順"] = DBNull.Value;
            newRow["SEQ"] = 0;
            newRow["OPEN_KUBUN"] = DBNull.Value;
            dtData.Rows.InsertAt(newRow, 0);

            return dtData;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // メモ
            if (!IsValidMemo())
            {
                this.txtMemo.Focus();
                this.txtMemo.SelectAll();
                return false;
            }

            // 表示順
            if (!IsValidSort())
            {
                this.txtSort.Focus();
                this.txtSort.SelectAll();
                return false;
            }

            return true;
        }

        /// <summary>
        /// メモの入力チェック
        /// </summary>
        /// <returns>true:OK/false:NG</returns>
        private bool IsValidMemo()
        {
            // 4000バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtMemo.Text, 4000))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 表示順の入力チェック
        /// </summary>
        /// <returns>true:OK/false:NG</returns>
        private bool IsValidSort()
        {
            // 未入力はOK
            if (ValChk.IsEmpty(this.txtSort.Text))
            {
                return true;
            }

            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtSort.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// データを登録する
        /// </summary>
        private void InsertData()
        {
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@DATE", SqlDbType.DateTime, this._targetDate);
            dpc.SetParam("@SEQ", SqlDbType.Decimal, 3, GetNextSeq());
            dpc.SetParam("@MEMO_CONTENT", SqlDbType.VarChar, 4000, txtMemo.Text);
            dpc.SetParam("@OPEN_KUBUN", SqlDbType.Decimal, 1, this.rdbPublic.Checked ? 1 : 9);
            dpc.SetParam("@TARGET_USER", SqlDbType.VarChar, 30, this.rdbPublic.Checked ? string.Empty : this.UInfo.UserId);
            dpc.SetParam("@SORT", SqlDbType.Decimal, 3, ValChk.IsEmpty(this.txtSort.Text) ? GetNextSort(this.rdbPublic.Checked ? 1 : 9) : Util.ToInt(this.txtSort.Text));
            dpc.SetParam("@DEL_FLG", SqlDbType.Decimal, 1, 0);

            this.Dba.Insert("TB_CM_MEMO", dpc);
        }

        /// <summary>
        /// データを更新する
        /// </summary>
        private void UpdateData()
        {
            DbParamCollection setDpc = new DbParamCollection();
            setDpc.SetParam("@MEMO_CONTENT", SqlDbType.VarChar, 4000, txtMemo.Text);
            setDpc.SetParam("@OPEN_KUBUN", SqlDbType.Decimal, 1, this.rdbPublic.Checked ? 1 : 9);
            setDpc.SetParam("@TARGET_USER", SqlDbType.VarChar, 30, this.rdbPublic.Checked ? string.Empty : this.UInfo.UserId);
            setDpc.SetParam("@SORT", SqlDbType.Decimal, 3, ValChk.IsEmpty(this.txtSort.Text) ? GetNextSort(this.rdbPublic.Checked ? 1 : 9) : Util.ToInt(this.txtSort.Text));

            StringBuilder where = new StringBuilder();
            where.Append("DATE = @DATE AND SEQ = @SEQ");
            DbParamCollection whereDpc = new DbParamCollection();
            whereDpc.SetParam("@DATE", SqlDbType.DateTime, this._targetDate);
            whereDpc.SetParam("@SEQ", SqlDbType.Decimal, 3, this._targetSeq);

            this.Dba.Update("TB_CM_MEMO", setDpc, where.ToString(), whereDpc);
        }

        /// <summary>
        /// データを削除する
        /// </summary>
        private void DeleteData()
        {
            DbParamCollection setDpc = new DbParamCollection();
            setDpc.SetParam("@DEL_FLG", SqlDbType.Decimal, 1, 1);

            StringBuilder where = new StringBuilder();
            where.Append("DATE = @DATE AND SEQ = @SEQ");
            DbParamCollection whereDpc = new DbParamCollection();
            whereDpc.SetParam("@DATE", SqlDbType.DateTime, this._targetDate);
            whereDpc.SetParam("@SEQ", SqlDbType.Decimal, 3, this._targetSeq);

            this.Dba.Update("TB_CM_MEMO", setDpc, where.ToString(), whereDpc);
        }

        /// <summary>
        /// SEQの最大+1を取得
        /// </summary>
        /// <returns>同一日付のSEQの最大値+1</returns>
        private int GetNextSeq()
        {
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@DATE", SqlDbType.DateTime, this._targetDate);

            DataTable dtData = this.Dba.GetDataTableByConditionWithParams("MAX(SEQ) AS MAX_SEQ", "TB_CM_MEMO",
                "DATE = @DATE", dpc);

            if (dtData.Rows.Count == 0 || ValChk.IsEmpty(dtData.Rows[0]["MAX_SEQ"]))
            {
                return 1;
            }
            else
            {
                return Util.ToInt(dtData.Rows[0]["MAX_SEQ"]) + 1;
            }
        }

        /// <summary>
        /// 並び順の最大+1を取得
        /// </summary>
        /// <param name="openKubun">公開範囲(1:全体、9:個人)</param>
        /// <returns>同一日付・同一の公開範囲の並び順の最大値+1</returns>
        private int GetNextSort(int openKubun)
        {
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@DATE", SqlDbType.DateTime, this._targetDate);
            dpc.SetParam("@OPEN_KUBUN", SqlDbType.Decimal, 1, openKubun);

            DataTable dtData = this.Dba.GetDataTableByConditionWithParams("MAX(SORT) AS MAX_SORT", "TB_CM_MEMO",
                "DATE = @DATE AND OPEN_KUBUN = @OPEN_KUBUN AND DEL_FLG = 0", dpc);

            if (dtData.Rows.Count == 0 || ValChk.IsEmpty(dtData.Rows[0]["MAX_SORT"]))
            {
                return 1;
            }
            else
            {
                return Util.ToInt(dtData.Rows[0]["MAX_SORT"]) + 1;
            }
        }
        #endregion
    }
}
