﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jp.co.fsi.com.calendar
{
    /// <summary>
    /// 旧暦情報オブジェクト格納クラス
    /// </summary>
    public class KyurekiBean
    {
        #region プロパティ
        private int _year;
        /// <summary>
        /// 旧暦年
        /// </summary>
        public int Year
        {
            get
            {
                return this._year;
            }
            set
            {
                this._year = value;
            }
        }

        private bool _uruu;
        /// <summary>
        /// 平月／閏月フラグ(平月:false、閏月:true)
        /// </summary>
        public bool Uruu
        {
            get
            {
                return this._uruu;
            }
            set
            {
                this._uruu = value;
            }
        }

        private int _month;
        /// <summary>
        /// 旧暦月
        /// </summary>
        public int Month
        {
            get
            {
                return this._month;
            }
            set
            {
                this._month = value;
            }
        }

        private int _day;
        /// <summary>
        /// 旧暦日
        /// </summary>
        public int Day
        {
            get
            {
                return this._day;
            }
            set
            {
                this._day = value;
            }
        }

        private string _rokuyo;
        /// <summary>
        /// 六曜名
        /// </summary>
        public string Rokuyo
        {
            get
            {
                return this._rokuyo;
            }
            set
            {
                this._rokuyo = value;
            }
        }

        private string _eto;
        /// <summary>
        /// 干支
        /// </summary>
        public string Eto
        {
            get
            {
                return this._eto;
            }
            set
            {
                this._eto = value;
            }
        }

        private decimal _mage;
        /// <summary>
        /// リアルタイム月齢
        /// </summary>
        public decimal Mage
        {
            get
            {
                return this._mage;
            }
            set
            {
                this._mage = value;
            }
        }

        private decimal _mageNoon;
        /// <summary>
        /// 正午月齢
        /// </summary>
        public decimal MageNoon
        {
            get
            {
                return this._mageNoon;
            }
            set
            {
                this._mageNoon = value;
            }
        }

        private decimal _illumi;
        /// <summary>
        /// 輝面比(%)
        /// </summary>
        public decimal Illumi
        {
            get
            {
                return this._illumi;
            }
            set
            {
                this._illumi = value;
            }
        }

        private int _mphase;
        /// <summary>
        /// 月相
        /// </summary>
        public int Mphase
        {
            get
            {
                return this._mphase;
            }
            set
            {
                this._mphase = value;
            }
        }

        private decimal _jd;
        /// <summary>
        /// ユリウス日
        /// </summary>
        public decimal Jd
        {
            get
            {
                return this._jd;
            }
            set
            {
                this._jd = value;
            }
        }
        #endregion
    }
}
