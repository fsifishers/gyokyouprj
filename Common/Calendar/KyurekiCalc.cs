﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using jp.co.fsi.common.util;

namespace jp.co.fsi.com.calendar
{
    /// <summary>
    /// 旧暦算出クラス
    /// </summary>
    public class KyurekiCalc
    {
        #region 定数
        private const decimal DAY_OF_TICKS = 864000000000m; // 一日は 864000000000 Ticks
        private const decimal FIRST_DAY = 1721425.5m;       // 西暦1年1月1日0時0分0秒はユリウス日で 1721425.5
        #endregion

        #region private変数
        /// <summary>
        /// ラジアン角度
        /// </summary>
        private decimal _k;

        /// <summary>
        /// タイムゾーンオフセット
        /// </summary>
        private decimal _tz;

        /// <summary>
        /// 太陽黄経
        /// </summary>
        private decimal _rm_sun0;
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KyurekiCalc()
        {
            // private変数の初期化
            this._k = Util.ToDecimal(Math.PI) / 180m;
            TimeZoneInfo tzi = TimeZoneInfo.Local;
            this._tz = (Util.ToDecimal(tzi.BaseUtcOffset.TotalMinutes) / 1440m) * (-1);
        }
        #endregion

        #region publicメソッド
        /// <summary>
        /// 西暦を元に旧暦を求めます。
        /// </summary>
        /// <param name="dt">日付(西暦)</param>
        /// <returns>旧暦情報(KyurekiBean)</returns>
        public KyurekiBean GetKyureki(DateTime dt)
        {
            KyurekiBean objKrk = new KyurekiBean();

            int i = 0;
            bool lap = false;
            int state = 0;

            decimal[] chu = new decimal[4];
            decimal[] saku = new decimal[5];
            KyurekiBean[] m = new KyurekiBean[5];
            for (i = 0; i < 5; i++)
            {
                m[i] = new KyurekiBean();
            }

            // 引数の日付をJSTユリウス日に変換
            decimal tm = DateTimeToJulianDay(dt);

            // 計算対象の直前にあたる二分二至の時刻を求める
            chu[0] = CalcChu(tm, 90);

            // 上で求めた二分二至の時の太陽黄経をもとに朔日行列の先頭に月名をセット
            m[0].Month = Util.ToInt(Math.Floor(this._rm_sun0 / 30m) + 2m);

            // 中気の時刻を計算（４回計算する）
            // chu[i]:中気の時刻
            for (i = 1; i < 4; i++)
            {
                chu[i] = CalcChu(chu[i - 1] + 32m, 30);
            }

            // 計算対象の直前にあたる二分二至の直前の朔の時刻を求める
            saku[0] = CalcSaku(chu[0]);

            // 朔の時刻を求める
            for (i = 1; i < 5; i++)
            {
                saku[i] = CalcSaku(saku[i - 1] + 30);
                // 前と同じ時刻を計算した場合（両者の差が26日以内）には、初期値を
                // +33日にして再実行させる。
                if (Math.Abs(Math.Floor(saku[i - 1]) - Math.Floor(saku[i])) <= 26)
                {
                    saku[i] = CalcSaku(saku[i - 1] + 35);
                }
            }

            if (Math.Floor(saku[1]) <= Math.Floor(chu[0]))
            {
                // saku[1]が二分二至の時刻以前になってしまった場合には、朔をさかのぼり過ぎ
                // たと考えて、朔の時刻を繰り下げて修正する。
                // その際、計算もれ（saku[4]）になっている部分を補うため、朔の時刻を計算
                // する。（近日点通過の近辺で朔があると起こる事があるようだ...？）
                for (i = 0; i < 4; i++)
                {
                    saku[i] = saku[i + 1];
                }
                saku[4] = CalcSaku(saku[3] + 35);
            }
            else if (Math.Floor(saku[0]) > Math.Floor(chu[0]))
            {
                // saku[0]が二分二至の時刻以後になってしまった場合には、朔をさかのぼり足
                // りないと見て、朔の時刻を繰り上げて修正する。
                // その際、計算もれ（saku[0]）になっている部分を補うため、朔の時刻を計算
                // する。（春分点の近辺で朔があると起こる事があるようだ...？）
                for (i = 4; i > 0; i--) saku[i] = saku[i - 1];
                saku[0] = CalcSaku(saku[0] - 27);
            }

            // 閏月検索Ｆｌａｇセット
            // （節月で４ヶ月の間に朔が５回あると、閏月がある可能性がある。）
            // lap=false:平月  lap=true:閏月
            lap = (Math.Floor(saku[4]) <= Math.Floor(chu[3]));

            // 朔日行列の作成
            // m[i].month ... 月名（1:正月 2:２月 3:３月 ....）
            // m[i].uruu .... 閏フラグ（false:平月 true:閏月）
            // m[i].jd ...... 朔日のjd
            m[0].Uruu = false;
            m[0].Jd = Util.ToInt(Math.Floor(saku[0]));
            for (i = 1; i < 5; i++)
            {
                if (lap && i > 1)
                {
                    if (chu[i - 1] <= Math.Floor(saku[i - 1])
                            || chu[i - 1] >= Math.Floor(saku[i]))
                    {
                        m[i - 1].Month = m[i - 2].Month;
                        m[i - 1].Uruu = true;
                        m[i - 1].Jd = Util.ToInt(Math.Floor(saku[i - 1]));
                        lap = false;
                    }
                }
                m[i].Month = m[i - 1].Month + 1;
                if (m[i].Month > 12) m[i].Month -= 12;
                m[i].Jd = Util.ToInt(Math.Floor(saku[i]));
                m[i].Uruu = false;
            }

            // 朔日行列から旧暦を求める。
            state = 0;
            for (i = 0; i < 5; i++)
            {
                if (Math.Floor(tm) < Math.Floor(m[i].Jd))
                {
                    state = 1;
                    break;
                }
                else if (Math.Floor(tm) == Math.Floor(m[i].Jd))
                {
                    state = 2;
                    break;
                }
            }
            if (state == 0 || state == 1) i--;

            objKrk.Uruu = m[i].Uruu;
            objKrk.Month = m[i].Month;
            objKrk.Day = Util.ToInt(Math.Floor(tm) - Math.Floor(m[i].Jd) + 1);

            // 旧暦年の計算
            // （旧暦月が10以上でかつ新暦月より大きい場合には、
            //   まだ年を越していないはず...）
            DateTime tmpDate = JulianDayToDateTime(tm);
            objKrk.Year = tmpDate.Year;
            if (objKrk.Month > 9 && objKrk.Month > tmpDate.Month) objKrk.Year--;

            // 六曜を求める
            string[] rokuyo = new string[6] { "先勝", "友引", "先負", "仏滅", "大安", "赤口" };
            objKrk.Rokuyo = rokuyo[(objKrk.Month + objKrk.Day - 2) % 6];

            // 干支を求める
            string[] eto = new string[60] {
                "甲子", "乙丑", "丙寅", "丁卯", "戊辰", "己巳", "庚午", "辛未", "壬申", "癸酉",
                "甲戌", "乙亥", "丙子", "丁丑", "戊寅", "己卯", "庚辰", "辛巳", "壬午", "癸未",
                "甲申", "乙酉", "丙戌", "丁亥", "戊子", "己丑", "庚寅", "辛卯", "壬辰", "癸巳",
                "甲午", "乙未", "丙申", "丁酉", "戊戌", "己亥", "庚子", "辛丑", "壬寅", "癸卯",
                "甲辰", "乙巳", "丙午", "丁未", "戊申", "己酉", "庚戌", "辛亥", "壬子", "癸丑",
                "甲寅", "乙卯", "丙辰", "丁巳", "戊午", "己未", "庚申", "辛酉", "壬戌", "癸亥"
            };
            long tmpIdx = Util.ToLong(Math.Floor(tm + 50)) % 60;
            objKrk.Eto = eto[tmpIdx];

            // 月齢を求める
            objKrk.Mage = tm - saku[i];
            if (objKrk.Mage < 0) objKrk.Mage = tm - saku[i - 1];
            objKrk.MageNoon = Math.Floor(tm) + .5m - saku[i];
            if (objKrk.MageNoon < 0) objKrk.MageNoon = Math.Floor(tm) + .5m - saku[i - 1];

            // 輝面比を求める
            decimal tm1 = Math.Floor(tm);
            decimal tm2 = tm - tm1 + this._tz;
            decimal t = (tm2 + .5m) / 36525m + (tm1 - 2451545m) / 36525m;
            objKrk.Illumi = (1 - Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(LongitudeMoon(t) - LongitudeSun(t)))))) * 50;

            // 月相を求める（輝面比の計算で求めた変数 t を使用）
            objKrk.Mphase = Util.ToInt(Math.Floor(NormalizationAngle(LongitudeMoon(t) - LongitudeSun(t)) / 360m * 28m + .5m));
            if (objKrk.Mphase == 28) objKrk.Mphase = 0;

            return objKrk;
        }
        #endregion

        #region private変数
        /// <summary>
        /// DateTimeの日付をユリウス日に変換
        /// </summary>
        /// <param name="dateTime">日付DateTime型</param>
        /// <returns>ユリウス日</returns>
        private decimal DateTimeToJulianDay(DateTime dateTime)
        {
            // JSTとしてるけど怪しいかもしんない(9時間を補正)
            return ((dateTime.Ticks + (FIRST_DAY * DAY_OF_TICKS)) / DAY_OF_TICKS) - this._tz;
        }

        /// <summary>
        /// ユリウス日をDateTimeの日付に変換
        /// </summary>
        /// <param name="julianDay">ユリウス日</param>
        /// <returns>日付DateTime型</returns>
        private DateTime JulianDayToDateTime(decimal julianDay)
        {
            // JSTとしてるけど怪しいかもしんない(9時間を補正)
            return (new DateTime((long)((julianDay - this._tz - FIRST_DAY) * DAY_OF_TICKS)));
        }

        /// <summary>
        /// 直前の二分二至／中気の時刻を求める
        /// </summary>
        /// <param name="tm">計算基準の時刻（JSTユリウス日）</param>
        /// <param name="longitude">求める対象（90:二分二至,30:中気））</param>
        /// <returns>求めた時刻（JSTユリウス日）</returns>
        /// <remarks>
        /// グローバル変数 rm_sun0 に、その時の太陽黄経をセットする。
        /// 
        /// 引数、戻り値ともJSTユリウス日で表し、時分秒は日の小数で表す。
        /// 力学時とユリウス日との補正時刻=0.0secと仮定
        /// </remarks>
        private decimal CalcChu(decimal tm, int longitude)
        {
            decimal t;
            decimal tm1;
            decimal tm2;
            decimal rm_sun;
            decimal delta_rm;

            // 時刻引数を小数部と整数部とに分解する（精度を上げるため）
            tm1 = Math.Floor(tm);
            tm2 = tm - tm1 + this._tz;  // JST -> UTC

            // 直前の二分二至の黄経 λsun0 を求める
            t = (tm2 + .5m) / 36525m + (tm1 - 2451545m) / 36525m;
            rm_sun = LongitudeSun(t);
            this._rm_sun0 = longitude * Math.Floor(rm_sun / longitude);

            // 繰り返し計算によって朔の時刻を計算する
            // （誤差が±1.0 sec以内になったら打ち切る。）
            decimal delta_t1 = 0;
            decimal delta_t2 = 1;

            while (Math.Abs(delta_t1 + delta_t2) > (1m / 86400m))
            {
                // λsun(t) を計算
                //   t = (tm + .5 - 2451545) / 36525;
                t = (tm2 + .5m) / 36525m + (tm1 - 2451545m) / 36525m;
                rm_sun = LongitudeSun(t);

                // 黄経差 Δλ＝λsun －λsun0
                delta_rm = rm_sun - this._rm_sun0;

                // Δλの引き込み範囲（±180°）を逸脱した場合には、補正を行う
                if (delta_rm > 180)
                {
                    delta_rm -= 360m;
                }
                else if (delta_rm < -180)
                {
                    delta_rm += 360m;
                }

                // 時刻引数の補正値 Δt
                // delta_t = delta_rm * 365.2 / 360;
                delta_t1 = Math.Floor(delta_rm * 365.2m / 360m);
                delta_t2 = delta_rm * 365.2m / 360m - delta_t1;

                // 時刻引数の補正
                // tm -= delta_t;
                tm1 = tm1 - delta_t1;
                tm2 = tm2 - delta_t2;
                if (tm2 < 0)
                {
                    tm1 -= 1;
                    tm2 += 1;
                }
            }

            // 戻り値の作成
            //   時刻引数を合成し、戻り値（JSTユリウス日）とする
            return tm2 + tm1 - this._tz;
        }

        /// <summary>
        /// 直前の朔の時刻を求める
        /// </summary>
        /// <param name="tm">計算基準の時刻（JSTユリウス日）</param>
        /// <returns>朔の時刻</returns>
        /// <remarks>
        /// 引数、戻り値ともJSTユリウス日で表し、時分秒は日の小数で表す。
        /// 力学時とユリウス日との補正時刻=0.0secと仮定
        /// </remarks>
        private decimal CalcSaku(decimal tm)
        {
            int lc;
            decimal t;
            decimal tm1;
            decimal tm2;
            decimal rm_sun;
            decimal rm_moon;
            decimal delta_rm;

            // 時刻引数を小数部と整数部とに分解する（精度を上げるため）
            tm1 = Math.Floor(tm);
            tm2 = tm - tm1 + this._tz;  // JST -> UTC

            // 繰り返し計算によって朔の時刻を計算する
            // （誤差が±1.0 sec以内になったら打ち切る。）
            decimal delta_t1 = 0;
            decimal delta_t2 = 1;

            for (lc = 1; Math.Abs(delta_t1 + delta_t2) > (1m / 86400m); lc++)
            {
                // 太陽の黄経λsun(t) ,月の黄経λmoon(t) を計算
                //   t = (tm + .5 - 2451545) / 36525;
                t = (tm2 + .5m) / 36525m + (tm1 - 2451545m) / 36525m;
                rm_sun = LongitudeSun(t);
                rm_moon = LongitudeMoon(t);

                // 月と太陽の黄経差Δλ
                // Δλ＝λmoon－λsun
                delta_rm = rm_moon - rm_sun;

                if (lc == 1 && delta_rm < 0)
                {
                    // ループの１回目（lc=1）で delta_rm < 0 の場合には引き込み範囲に
                    // 入るように補正する
                    delta_rm = NormalizationAngle(delta_rm);
                }
                else if (rm_sun >= 0 && rm_sun <= 20 && rm_moon >= 300)
                {
                    // 春分の近くで朔がある場合（0 ≦λsun≦ 20）で、
                    // 月の黄経λmoon≧300 の場合には、
                    // Δλ＝ 360 － Δλ と計算して補正する
                    delta_rm = NormalizationAngle(delta_rm);
                    delta_rm = 360m - delta_rm;
                }
                else if (Math.Abs(delta_rm) > 40)
                {
                    // Δλの引き込み範囲（±40°）を逸脱した場合には、補正を行う
                    delta_rm = NormalizationAngle(delta_rm);
                }

                // 時刻引数の補正値 Δt
                // delta_t = delta_rm * 29.530589 / 360;
                delta_t1 = Math.Floor(delta_rm * 29.530589m / 360m);
                delta_t2 = delta_rm * 29.530589m / 360m - delta_t1;

                // 時刻引数の補正
                // tm -= delta_t;
                tm1 = tm1 - delta_t1;
                tm2 = tm2 - delta_t2;
                if (tm2 < 0)
                {
                    tm1 -= 1;
                    tm2 += 1;
                }

                if (lc == 15 && Math.Abs(delta_t1 + delta_t2) > (1m / 86400m))
                {
                    // ループ回数が15回になったら、初期値 tm を tm-26 とする。
                    tm1 = Math.Floor(tm - 26);
                    tm2 = 0;
                }
                else if (lc > 30 && Math.Abs(delta_t1 + delta_t2) > (1m / 86400m))
                {
                    // 初期値を補正したにも関わらず、振動を続ける場合には初期値を答えとし
                    // て返して強制的にループを抜け出して異常終了させる。
                    tm1 = tm;
                    tm2 = 0;
                    break;
                }
            }

            // 戻り値の作成
            //   時刻引数を合成し、戻り値（ユリウス日）とする
            return tm2 + tm1 - this._tz;
        }

        /// <summary>
        /// 角度の正規化を行う
        /// (引数の範囲を 0≦θ＜360 にする。)
        /// </summary>
        /// <param name="angle">角度</param>
        /// <returns>正規化された角度</returns>
        private decimal NormalizationAngle(decimal angle)
        {
            return angle - 360m * Math.Floor(angle / 360m);
        }

        /// <summary>
        /// 太陽の黄経 λsun(t) を計算する（t は力学時）
        /// </summary>
        /// <param name="t">力学時</param>
        /// <returns>太陽黄経</returns>
        private decimal LongitudeSun(decimal t)
        {
            decimal ang;
            decimal th;

            // 摂動項の計算
            th = .0004m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(31557m * t + 161m))));
            th += .0004m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(29930m * t + 48m))));
            th += .0005m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(2281m * t + 221m))));
            th += .0005m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(155m * t + 118m))));
            th += .0006m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(33718m * t + 316m))));
            th += .0007m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(9038m * t + 64m))));
            th += .0007m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(3035m * t + 110m))));
            th += .0007m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(65929m * t + 45m))));
            th += .0013m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(22519m * t + 352m))));
            th += .0015m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(45038m * t + 254m))));
            th += .0018m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(445267m * t + 208m))));
            th += .0018m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(19m * t + 159m))));
            th += .0020m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(32964m * t + 158m))));
            th += .0200m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(71998.1m * t + 265.1m))));

            ang = NormalizationAngle(35999.05m * t + 267.52m);
            th -= .0048m * t * Util.ToDecimal(Math.Cos(ToDouble(this._k * ang)));
            th += 1.9147m * t * Util.ToDecimal(Math.Cos(ToDouble(this._k * ang)));

            // 比例項の計算
            ang = NormalizationAngle(36000.7695m * t);
            ang = NormalizationAngle(ang + 280.4659m);
            th = NormalizationAngle(th + ang);

            return th;
        }

        /// <summary>
        /// 月の黄経 λmoon(t) を計算する（t は力学時）
        /// </summary>
        /// <param name="t">力学時</param>
        /// <returns>月黄経</returns>
        private decimal LongitudeMoon(decimal t)
        {
            decimal ang;
            decimal th;

            // 摂動項の計算
            th = .0003m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(2322131m * t + 191m))));
            th += .0003m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(4067m * t + 70m))));
            th += .0003m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(549197m * t + 220m))));
            th += .0003m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(1808933m * t + 58m))));
            th += .0003m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(349472m * t + 337m))));
            th += .0003m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(381404m * t + 354m))));
            th += .0003m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(958465m * t + 340m))));
            th += .0004m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(12006m * t + 187m))));
            th += .0004m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(39871m * t + 223m))));
            th += .0005m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(509131m * t + 242m))));
            th += .0005m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(1745069m * t + 24m))));
            th += .0005m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(1908795m * t + 90m))));
            th += .0006m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(2258267m * t + 156m))));
            th += .0006m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(111869m * t + 38m))));
            th += .0007m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(27864m * t + 127m))));
            th += .0007m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(485333m * t + 186m))));
            th += .0007m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(405201m * t + 50m))));
            th += .0007m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(790672m * t + 114m))));
            th += .0008m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(1403732m * t + 98m))));
            th += .0009m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(858602m * t + 129m))));
            th += .0011m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(1920802m * t + 186m))));
            th += .0012m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(1267871m * t + 249m))));
            th += .0016m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(1856938m * t + 152m))));
            th += .0018m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(401329m * t + 274m))));
            th += .0021m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(341337m * t + 16m))));
            th += .0021m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(71998m * t + 85m))));
            th += .0021m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(990397m * t + 357m))));
            th += .0022m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(818536m * t + 151m))));
            th += .0023m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(922466m * t + 163m))));
            th += .0024m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(99863m * t + 122m))));
            th += .0026m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(1379739m * t + 17m))));
            th += .0027m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(918399m * t + 182m))));
            th += .0028m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(1934m * t + 145m))));
            th += .0037m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(541062m * t + 259m))));
            th += .0038m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(1781068m * t + 21m))));
            th += .0040m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(133m * t + 29m))));
            th += .0040m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(1844932m * t + 56m))));
            th += .0040m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(1331734m * t + 283m))));
            th += .0050m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(481266m * t + 205m))));
            th += .0052m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(31932m * t + 107m))));
            th += .0068m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(926533m * t + 323m))));
            th += .0079m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(449334m * t + 188m))));
            th += .0085m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(826671m * t + 111m))));
            th += .0100m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(1431597m * t + 315m))));
            th += .0107m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(1303870m * t + 246m))));
            th += .0110m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(489205m * t + 142m))));
            th += .0125m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(1443603m * t + 52m))));
            th += .0154m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(75870m * t + 41m))));
            th += .0304m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(513197.9m * t + 222.5m))));
            th += .0347m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(445267.1m * t + 27.9m))));
            th += .0409m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(441199.8m * t + 47.4m))));
            th += .0458m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(854535.2m * t + 148.2m))));
            th += .0533m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(1367733.1m * t + 280.7m))));
            th += .0571m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(377336.3m * t + 13.2m))));
            th += .0588m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(63863.5m * t + 124.2m))));
            th += .1144m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(966404m * t + 276.5m))));
            th += .1851m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(35999.05m * t + 87.53m))));
            th += .2136m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(954397.74m * t + 179.93m))));
            th += .6583m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(890534.22m * t + 145.7m))));
            th += 1.2740m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(413335.35m * t + 10.74m))));
            th += 6.2888m * Util.ToDecimal(Math.Cos(ToDouble(this._k * NormalizationAngle(477198.868m * t + 44.963m))));

            // 比例項の計算
            ang = NormalizationAngle(481267.8809m * t);
            ang = NormalizationAngle(ang + 218.3162m);
            th = NormalizationAngle(th + ang);

            return th;
        }

        /// <summary>
        /// 引数に指定した値をdouble型に変換する
        /// </summary>
        /// <param name="obj">変換前の値</param>
        /// <returns>変換度の値</returns>
        private double ToDouble(object val)
        {
            double result;
            if (!Double.TryParse(val.ToString(), out result))
            {
                result = Double.Parse("0");
            }
            return result;
        }
        #endregion
    }
}
