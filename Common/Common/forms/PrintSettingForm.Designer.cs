﻿namespace jp.co.fsi.common.forms
{
    partial class PrintSettingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTitle = new System.Windows.Forms.Label();
            this.btnEsc = new System.Windows.Forms.Button();
            this.btnF6 = new System.Windows.Forms.Button();
            this.lblRepId = new System.Windows.Forms.Label();
            this.lblDocName = new System.Windows.Forms.Label();
            this.lblOutPrinter = new System.Windows.Forms.Label();
            this.cmbRepId = new System.Windows.Forms.ComboBox();
            this.lblManual = new System.Windows.Forms.Label();
            this.lblColor = new System.Windows.Forms.Label();
            this.lblDuplex = new System.Windows.Forms.Label();
            this.cmbPrinter = new System.Windows.Forms.ComboBox();
            this.cmbManual = new System.Windows.Forms.ComboBox();
            this.cmbColor = new System.Windows.Forms.ComboBox();
            this.cmbDuplex = new System.Windows.Forms.ComboBox();
            this.txtDocumentName = new System.Windows.Forms.TextBox();
            this.lblDocumentNmExp = new System.Windows.Forms.Label();
            this.lblManualExp = new System.Windows.Forms.Label();
            this.lblColorExp = new System.Windows.Forms.Label();
            this.lblDuplexExp = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTitle.BackColor = System.Drawing.Color.Navy;
            this.lblTitle.Font = new System.Drawing.Font("ＭＳ ゴシック", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTitle.ForeColor = System.Drawing.Color.White;
            this.lblTitle.Location = new System.Drawing.Point(12, 9);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(456, 23);
            this.lblTitle.TabIndex = 0;
            this.lblTitle.Text = "プリンタ設定";
            this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnEsc
            // 
            this.btnEsc.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnEsc.Location = new System.Drawing.Point(12, 304);
            this.btnEsc.Name = "btnEsc";
            this.btnEsc.Size = new System.Drawing.Size(65, 45);
            this.btnEsc.TabIndex = 13;
            this.btnEsc.TabStop = false;
            this.btnEsc.Text = "Esc\r\n\r\n終了";
            this.btnEsc.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnEsc.UseVisualStyleBackColor = true;
            this.btnEsc.Click += new System.EventHandler(this.btnEsc_Click);
            // 
            // btnF6
            // 
            this.btnF6.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnF6.Location = new System.Drawing.Point(83, 304);
            this.btnF6.Name = "btnF6";
            this.btnF6.Size = new System.Drawing.Size(65, 45);
            this.btnF6.TabIndex = 14;
            this.btnF6.TabStop = false;
            this.btnF6.Text = "F6\r\n\r\n保存";
            this.btnF6.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnF6.UseVisualStyleBackColor = true;
            this.btnF6.Click += new System.EventHandler(this.btnF6_Click);
            // 
            // lblRepId
            // 
            this.lblRepId.AutoSize = true;
            this.lblRepId.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblRepId.Location = new System.Drawing.Point(51, 50);
            this.lblRepId.Name = "lblRepId";
            this.lblRepId.Size = new System.Drawing.Size(77, 13);
            this.lblRepId.TabIndex = 1;
            this.lblRepId.Text = "帳票ＩＤ：";
            // 
            // lblDocName
            // 
            this.lblDocName.AutoSize = true;
            this.lblDocName.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDocName.Location = new System.Drawing.Point(9, 80);
            this.lblDocName.Name = "lblDocName";
            this.lblDocName.Size = new System.Drawing.Size(119, 13);
            this.lblDocName.TabIndex = 3;
            this.lblDocName.Text = "ドキュメント名：";
            // 
            // lblOutPrinter
            // 
            this.lblOutPrinter.AutoSize = true;
            this.lblOutPrinter.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblOutPrinter.Location = new System.Drawing.Point(23, 110);
            this.lblOutPrinter.Name = "lblOutPrinter";
            this.lblOutPrinter.Size = new System.Drawing.Size(105, 13);
            this.lblOutPrinter.TabIndex = 5;
            this.lblOutPrinter.Text = "出力プリンタ：";
            // 
            // cmbRepId
            // 
            this.cmbRepId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbRepId.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmbRepId.FormattingEnabled = true;
            this.cmbRepId.Location = new System.Drawing.Point(134, 47);
            this.cmbRepId.Name = "cmbRepId";
            this.cmbRepId.Size = new System.Drawing.Size(121, 21);
            this.cmbRepId.TabIndex = 2;
            this.cmbRepId.SelectedIndexChanged += new System.EventHandler(this.cmbRepId_SelectedIndexChanged);
            // 
            // lblManual
            // 
            this.lblManual.AutoSize = true;
            this.lblManual.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblManual.Location = new System.Drawing.Point(65, 140);
            this.lblManual.Name = "lblManual";
            this.lblManual.Size = new System.Drawing.Size(63, 13);
            this.lblManual.TabIndex = 7;
            this.lblManual.Text = "手差し：";
            // 
            // lblColor
            // 
            this.lblColor.AutoSize = true;
            this.lblColor.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblColor.Location = new System.Drawing.Point(37, 170);
            this.lblColor.Name = "lblColor";
            this.lblColor.Size = new System.Drawing.Size(91, 13);
            this.lblColor.TabIndex = 9;
            this.lblColor.Text = "カラー印刷：";
            // 
            // lblDuplex
            // 
            this.lblDuplex.AutoSize = true;
            this.lblDuplex.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDuplex.Location = new System.Drawing.Point(51, 200);
            this.lblDuplex.Name = "lblDuplex";
            this.lblDuplex.Size = new System.Drawing.Size(77, 13);
            this.lblDuplex.TabIndex = 11;
            this.lblDuplex.Text = "両面印刷：";
            // 
            // cmbPrinter
            // 
            this.cmbPrinter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPrinter.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmbPrinter.FormattingEnabled = true;
            this.cmbPrinter.Location = new System.Drawing.Point(134, 106);
            this.cmbPrinter.Name = "cmbPrinter";
            this.cmbPrinter.Size = new System.Drawing.Size(334, 21);
            this.cmbPrinter.TabIndex = 6;
            // 
            // cmbManual
            // 
            this.cmbManual.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbManual.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmbManual.FormattingEnabled = true;
            this.cmbManual.Location = new System.Drawing.Point(134, 137);
            this.cmbManual.Name = "cmbManual";
            this.cmbManual.Size = new System.Drawing.Size(77, 21);
            this.cmbManual.TabIndex = 8;
            // 
            // cmbColor
            // 
            this.cmbColor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbColor.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmbColor.FormattingEnabled = true;
            this.cmbColor.Location = new System.Drawing.Point(134, 167);
            this.cmbColor.Name = "cmbColor";
            this.cmbColor.Size = new System.Drawing.Size(77, 21);
            this.cmbColor.TabIndex = 10;
            // 
            // cmbDuplex
            // 
            this.cmbDuplex.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDuplex.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmbDuplex.FormattingEnabled = true;
            this.cmbDuplex.Location = new System.Drawing.Point(134, 197);
            this.cmbDuplex.Name = "cmbDuplex";
            this.cmbDuplex.Size = new System.Drawing.Size(77, 21);
            this.cmbDuplex.TabIndex = 12;
            // 
            // txtDocumentName
            // 
            this.txtDocumentName.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDocumentName.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtDocumentName.Location = new System.Drawing.Point(134, 76);
            this.txtDocumentName.Name = "txtDocumentName";
            this.txtDocumentName.Size = new System.Drawing.Size(211, 20);
            this.txtDocumentName.TabIndex = 4;
            // 
            // lblDocumentNmExp
            // 
            this.lblDocumentNmExp.AutoSize = true;
            this.lblDocumentNmExp.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDocumentNmExp.ForeColor = System.Drawing.Color.Red;
            this.lblDocumentNmExp.Location = new System.Drawing.Point(351, 80);
            this.lblDocumentNmExp.Name = "lblDocumentNmExp";
            this.lblDocumentNmExp.Size = new System.Drawing.Size(49, 13);
            this.lblDocumentNmExp.TabIndex = 15;
            this.lblDocumentNmExp.Text = "(必須)";
            // 
            // lblManualExp
            // 
            this.lblManualExp.AutoSize = true;
            this.lblManualExp.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblManualExp.Location = new System.Drawing.Point(217, 140);
            this.lblManualExp.Name = "lblManualExp";
            this.lblManualExp.Size = new System.Drawing.Size(91, 13);
            this.lblManualExp.TabIndex = 16;
            this.lblManualExp.Text = "(○：手差し)";
            // 
            // lblColorExp
            // 
            this.lblColorExp.AutoSize = true;
            this.lblColorExp.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblColorExp.Location = new System.Drawing.Point(217, 170);
            this.lblColorExp.Name = "lblColorExp";
            this.lblColorExp.Size = new System.Drawing.Size(161, 13);
            this.lblColorExp.TabIndex = 17;
            this.lblColorExp.Text = "(○：カラー、×：白黒)";
            // 
            // lblDuplexExp
            // 
            this.lblDuplexExp.AutoSize = true;
            this.lblDuplexExp.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDuplexExp.Location = new System.Drawing.Point(217, 200);
            this.lblDuplexExp.Name = "lblDuplexExp";
            this.lblDuplexExp.Size = new System.Drawing.Size(105, 13);
            this.lblDuplexExp.TabIndex = 18;
            this.lblDuplexExp.Text = "(○：両面印刷)";
            // 
            // PrintSettingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(480, 361);
            this.Controls.Add(this.lblDuplexExp);
            this.Controls.Add(this.lblColorExp);
            this.Controls.Add(this.lblManualExp);
            this.Controls.Add(this.lblDocumentNmExp);
            this.Controls.Add(this.txtDocumentName);
            this.Controls.Add(this.cmbDuplex);
            this.Controls.Add(this.cmbColor);
            this.Controls.Add(this.cmbManual);
            this.Controls.Add(this.cmbPrinter);
            this.Controls.Add(this.lblDuplex);
            this.Controls.Add(this.lblColor);
            this.Controls.Add(this.lblManual);
            this.Controls.Add(this.cmbRepId);
            this.Controls.Add(this.lblOutPrinter);
            this.Controls.Add(this.lblDocName);
            this.Controls.Add(this.lblRepId);
            this.Controls.Add(this.btnF6);
            this.Controls.Add(this.btnEsc);
            this.Controls.Add(this.lblTitle);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "PrintSettingForm";
            this.Text = "プリンタ設定";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PrintSettingForm_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        protected System.Windows.Forms.Label lblTitle;
        public System.Windows.Forms.Button btnEsc;
        public System.Windows.Forms.Button btnF6;
        private System.Windows.Forms.Label lblRepId;
        private System.Windows.Forms.Label lblDocName;
        private System.Windows.Forms.Label lblOutPrinter;
        private System.Windows.Forms.ComboBox cmbRepId;
        private System.Windows.Forms.Label lblManual;
        private System.Windows.Forms.Label lblColor;
        private System.Windows.Forms.Label lblDuplex;
        private System.Windows.Forms.ComboBox cmbPrinter;
        private System.Windows.Forms.ComboBox cmbManual;
        private System.Windows.Forms.ComboBox cmbColor;
        private System.Windows.Forms.ComboBox cmbDuplex;
        private System.Windows.Forms.TextBox txtDocumentName;
        private System.Windows.Forms.Label lblDocumentNmExp;
        private System.Windows.Forms.Label lblManualExp;
        private System.Windows.Forms.Label lblColorExp;
        private System.Windows.Forms.Label lblDuplexExp;
    }
}