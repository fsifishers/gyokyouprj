﻿namespace jp.co.fsi.common.forms
{
    partial class PreviewForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.vwMain = new GrapeCity.ActiveReports.Viewer.Win.Viewer();
            this.SuspendLayout();
            // 
            // vwMain
            // 
            this.vwMain.CurrentPage = 0;
            this.vwMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.vwMain.Location = new System.Drawing.Point(0, 0);
            this.vwMain.Name = "vwMain";
            this.vwMain.PreviewPages = 0;
            // 
            // 
            // 
            // 
            // 
            // 
            this.vwMain.Sidebar.ParametersPanel.ContextMenu = null;
            this.vwMain.Sidebar.ParametersPanel.Text = "パラメータ";
            this.vwMain.Sidebar.ParametersPanel.Width = 200;
            // 
            // 
            // 
            this.vwMain.Sidebar.SearchPanel.ContextMenu = null;
            this.vwMain.Sidebar.SearchPanel.Text = "検索";
            this.vwMain.Sidebar.SearchPanel.Width = 200;
            // 
            // 
            // 
            this.vwMain.Sidebar.ThumbnailsPanel.ContextMenu = null;
            this.vwMain.Sidebar.ThumbnailsPanel.Text = "サムネイル";
            this.vwMain.Sidebar.ThumbnailsPanel.Width = 200;
            // 
            // 
            // 
            this.vwMain.Sidebar.TocPanel.ContextMenu = null;
            this.vwMain.Sidebar.TocPanel.Text = "見出しマップラベル";
            this.vwMain.Sidebar.TocPanel.Width = 200;
            this.vwMain.Sidebar.Width = 200;
            this.vwMain.Size = new System.Drawing.Size(761, 462);
            this.vwMain.TabIndex = 0;
            this.vwMain.KeyDown += new System.Windows.Forms.KeyEventHandler(this.PreviewForm_KeyDown);
            // 
            // PreviewForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(761, 462);
            this.Controls.Add(this.vwMain);
            this.Name = "PreviewForm";
            this.Text = "帳票プレビュー";
            this.Load += new System.EventHandler(this.PreviewForm_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.PreviewForm_KeyDown);
            this.ResumeLayout(false);

        }

        #endregion

        private GrapeCity.ActiveReports.Viewer.Win.Viewer vwMain;
    }
}