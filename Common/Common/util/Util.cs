﻿using System;
using System.Data;
using System.IO;
using System.Text;
using System.Collections;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.constants;

namespace jp.co.fsi.common.util
{
    /// <summary>
    /// ユーティリティクラス
    /// </summary>
    public static class Util
    {
        #region publicメソッド
        /// <summary>
        /// 文字列への変換
        /// </summary>
        /// <param name="val">変換前の値</param>
        /// <returns>変換後の文字列</returns>
        public static string ToString(object val)
        {
            if (val == null) return string.Empty;

            return val.ToString();
        }

        /// <summary>
        /// 数値(int)への変換
        /// </summary>
        /// <param name="val">変換前の値</param>
        /// <returns>変換後の数値(int)</returns>
        public static int ToInt(object val)
        {
            int result;
            if (!Int32.TryParse(val.ToString(), out result))
            {
                result = 0;
            }
            return result;
        }

        /// <summary>
        /// 数値(long)への変換
        /// </summary>
        /// <param name="val">変換前の値</param>
        /// <returns>変換後の数値(long)</returns>
        public static long ToLong(object val)
        {
            long result;
            if (!Int64.TryParse(val.ToString(), out result))
            {
                result = 0;
            }
            return result;
        }

        /// <summary>
        /// 数値(decimal)への変換
        /// </summary>
        /// <param name="val">変換前の値</param>
        /// <returns>変換後の数値(decimal)</returns>
        public static decimal ToDecimal(object val)
        {
            decimal result;
            if (!Decimal.TryParse(val.ToString(), out result))
            {
                result = Decimal.Parse("0");
            }
            return result;
        }

        /// <summary>
        /// 引数で指定したデータを日付に変換します。
        /// </summary>
        /// <param name="val">変換する値</param>
        /// <returns>日付オブジェクト</returns>
        public static DateTime ToDate(object val)
        {
            try
            {
                //2019-11-12 吉村 キャストミス修正
                //return (DateTime)val;
                return DateTime.Parse(val.ToString());
            }
            catch (Exception e)
            {
                DateTime tmpDat;
                if (!DateTime.TryParse(Util.ToString(val), out tmpDat))
                {
                    return new DateTime();
                }

                return tmpDat;
            }
        }

        /// <summary>
        /// 引数で指定した日付データをyyyy/MM/dd形式の文字列に変換します。
        /// </summary>
        /// <param name="val">変換する値</param>
        /// <returns>文字列(yyyy/MM/dd形式)</returns>
        public static string ToDateStr(object val)
        {
            return Util.ToDate(val).ToString("yyyy/MM/dd");
        }

        /// <summary>
        /// 引数で指定した日付データを引数で指定したフォーマットの文字列に変換します。
        /// </summary>
        /// <param name="val">変換する値</param>
        /// <param name="format">書式文字列</param>
        /// <returns>文字列(yyyy/MM/dd形式)</returns>
        /// <remarks>
        /// 引数formatに指定する文字列はMicrosoftの.Netフレームワークが
        /// サポートする書式なら何でもOKです。
        /// 詳細はググr…MicrosoftのWebを参照して下さい。
        /// </remarks>
        public static string ToDateStr(object val, string format)
        {
            return Util.ToDate(val).ToString(format);
        }

        /// <summary>
        /// ゼロ埋めをする(引数が整数Ver.)
        /// </summary>
        /// <param name="num">フォーマットする数値</param>
        /// <param name="length">全体の文字長</param>
        /// <returns>フォーマットされた文字列</returns>
        public static string PadZero(int num, int length)
        {
            string result = string.Empty;
            result = String.Format("{0:D" + Util.ToString(length) + "}", num);

            return result;
        }

        /// <summary>
        /// ゼロ埋めをする(引数が文字Ver.)
        /// </summary>
        /// <param name="num">フォーマットする数値</param>
        /// <param name="length">全体の文字長</param>
        /// <returns>フォーマットされた文字列</returns>
        public static string PadZero(string num, int length)
        {
            if (ValChk.IsEmpty(num)) return string.Empty;

            return Util.PadZero(Util.ToInt(num), length);
        }

        /// <summary>
        /// 数値をカンマ区切りでフォーマットする(整数のみ)
        /// </summary>
        /// <param name="val">フォーマットする値(数値変換できる値)</param>
        /// <returns>フォーマットされた文字列</returns>
        public static string FormatNum(object val)
        {
            return Util.FormatNum(val, 0);
        }

        /// <summary>
        /// 数値をカンマ区切りでフォーマットする(小数部含む)
        /// </summary>
        /// <param name="val">フォーマットする値(数値変換できる値)</param>
        /// <param name="decLen">小数以下の桁数</param>
        /// <returns>フォーマットされた文字列</returns>
        public static string FormatNum(object val, int decLen)
        {
            decimal decVal = 0;
            if (!Decimal.TryParse(val.ToString(), out decVal))
            {
                return null;
            }

            string formatStr = "#,0";
            if (decLen > 0)
            {
                formatStr += "." + new String('0', decLen);
            }

            return decVal.ToString(formatStr);
        }

        /// <summary>
        /// 西暦の日付を和暦に変換します。
        /// </summary>
        /// <param name="date">日付オブジェクト(DateTime)</param>
        /// <param name="dba">データアクセスオブジェクト</param>
        /// <returns>
        /// 和暦にフォーマットされた日付
        /// 0:元号
        /// 1:元号を示す記号
        /// 2:年
        /// 3:月
        /// 4:日
        /// 5:全部一纏めにした日付
        /// 6:全部一纏めにした日付(省略表記)
        /// </returns>
        public static string[] ConvJpDate(DateTime date, DbAccess dba)
        {
            string[] array = new string[7];

            // 引数で指定した日付が含まれる元号のレコードを取得
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@DATE", SqlDbType.DateTime, date);

            DataTable dt = dba.GetDataTableByConditionWithParams("NAME1, NAME3, BEGIN_DATE, FINISH_DATE",
                "TB_CM_GENGO", "BEGIN_DATE <= @DATE AND FINISH_DATE >= @DATE", dpc);

            if (dt != null && dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                DateTime beginDate = (DateTime)dr["BEGIN_DATE"];

                array[0] = Util.ToString(dr["NAME1"]);
                array[1] = Util.ToString(dr["NAME3"]);
                array[2] = Util.ToString(date.Year - beginDate.Year + 1);
            }
            else
            {
                // 元号を取得できなければ値が設定されていない配列を返す
                return new string[6];
            }

            array[3] = Util.ToString(date.Month);
            array[4] = Util.ToString(date.Day);
            string concatYear = array[2];
            if (concatYear.Equals("1"))
            {
                concatYear = "元";
            }
            else
            {
                concatYear = concatYear.PadLeft(2);
            }
            array[5] = array[0] + concatYear + "年" + array[3].PadLeft(2)
                + "月" + array[4].PadLeft(2) + "日";
            array[6] = array[1] + concatYear + "/" + array[3].PadLeft(2) + "/" + array[4].PadLeft(2);

            return array;
        }

        /// <summary>
        /// 西暦の日付を和暦に変換します。
        /// </summary>
        /// <param name="dateStr">文字列(日付形式)</param>
        /// <param name="dba">データアクセスオブジェクト</param>
        /// <returns>
        /// 和暦にフォーマットされた日付
        /// 0:元号
        /// 1:元号を示す記号
        /// 2:年
        /// 3:月
        /// 4:日
        /// 5:全部一纏めにした日付
        /// </returns>
        public static string[] ConvJpDate(string dateStr, DbAccess dba)
        {
            DateTime tmpDate = new DateTime();

            if (!DateTime.TryParse(dateStr, out tmpDate))
            {
                // 変換できなければ値が設定されていない配列を返す
                return new string[6];
            }

            return Util.ConvJpDate(tmpDate, dba);
        }

        /// <summary>
        /// 和暦日付から西暦日付に変換します。
        /// (年月日が文字列ver)
        /// </summary>
        /// <param name="gengo">元号(漢字名称。「平成」「昭和」など)</param>
        /// <param name="jpYear">年(和暦)</param>
        /// <param name="month">月</param>
        /// <param name="day">日</param>
        /// <param name="dba">データアクセスオブジェクト</param>
        /// <returns>西暦日付</returns>
        public static DateTime ConvAdDate(string gengo, string jpYear, string month, string day, DbAccess dba)
        {
            // 引数の年には空白が入ってても、「元年」の「元」が入っていても正しく変換されるよう処理
            string tmpYear = jpYear.Trim();
            int intYear = 0;
            if (tmpYear.Equals("元"))
            {
                intYear = 1;
            }
            else
            {
                intYear = Util.ToInt(tmpYear);
            }

            // 月・日を数値変換
            int intMonth = Util.ToInt(month.Trim());
            int intDay = Util.ToInt(day.Trim());
            
            // 引数の年月日が数字verのメソッドをオーバーロードする
            return Util.ConvAdDate(gengo, intYear, intMonth, intDay, dba);
        }

        /// <summary>
        /// 和暦日付から西暦日付に変換します。
        /// (年月日が数字ver)
        /// </summary>
        /// <param name="gengo">元号(漢字名称。「平成」「昭和」など)</param>
        /// <param name="jpYear">年(和暦)</param>
        /// <param name="month">月</param>
        /// <param name="day">日</param>
        /// <param name="dba">データアクセスオブジェクト</param>
        /// <returns>西暦日付</returns>
        public static DateTime ConvAdDate(string gengo, int jpYear, int month, int day, DbAccess dba)
        {
            // 元号を元に開始日付を取得
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@GENGO", SqlDbType.VarChar, gengo);

            DataTable dt = dba.GetDataTableByConditionWithParams("BEGIN_DATE",
                "TB_CM_GENGO", "NAME1 = @GENGO", dpc);

            // 開始日付を元に西暦を計算
            DateTime datBgnDt = (DateTime)dt.Rows[0]["BEGIN_DATE"];
            int adYear = datBgnDt.Year + jpYear - 1;

            // 西暦と引数の月・日を合わせ、DateTimeとして返す
            return new DateTime(adYear, month, day);
        }

        /// <summary>
        /// 和暦日付を正しい日付に補正します。
        /// </summary>
        /// <param name="gengo">元号(漢字名称。「平成」「昭和」など)</param>
        /// <param name="jpYear">年(和暦)</param>
        /// <param name="month">月</param>
        /// <param name="day">日</param>
        /// <param name="dba">データアクセスオブジェクト</param>
        /// <returns>
        /// 和暦にフォーマットされた日付
        /// 0:元号
        /// 1:元号を示す記号
        /// 2:年
        /// 3:月
        /// 4:日
        /// 5:全部一纏めにした日付
        /// </returns>
        public static string[] FixJpDate(string gengo, string jpYear, string month, string day, DbAccess dba)
        {
            // まず西暦に変換
            DateTime datAdDate = Util.ConvAdDate(gengo, jpYear, month, day, dba);

            // それを和暦に変換して返却
            return Util.ConvJpDate(datAdDate, dba);
        }

        /// <summary>
        /// 文字列のバイト長を取得します。
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        public static int GetByteLength(string val)
        {
            Encoding sjisEnc = Encoding.GetEncoding("Shift_JIS");
            return sjisEnc.GetByteCount(val);
        }

        /// <summary>
        /// null値をDBNullに変換します。
        /// </summary>
        /// <param name="val">変換前の値</param>
        /// <returns>変換後の値</returns>
        public static object ConvDBNull(object val)
        {
            if (val == null)
            {
                return DBNull.Value;
            }
            else
            {
                return val;
            }
        }

        /// <summary>
        /// 10進値を、指定した小数部の桁数に丸めます。 
        /// </summary>
        /// <param name="d">丸め対象の10進数。</param>
        /// <param name="decimals">戻り値の小数部の桁数。</param>
        /// <returns></returns>
        public static decimal Round(decimal d, int decimals)
        {
            return Math.Round(d, decimals, MidpointRounding.AwayFromZero);
        }

        /// <summary>
        /// 指定した日付範囲の会計年度を取得します。
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        public static ArrayList GetKaikeiNendo(DateTime tmpDateFr, DateTime tmpDateTo, DbAccess dba)
        {
            // 日付範囲に該当する会計年度を取得
            string where = "KAIKEI_KIKAN_KAISHIBI <= @KAIKEI_KIKAN_SHURYOBI";
            where += " AND KAIKEI_KIKAN_SHURYOBI >= @KAIKEI_KIKAN_KAISHIBI";
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAIKEI_KIKAN_KAISHIBI", SqlDbType.DateTime, 10, tmpDateFr);
            dpc.SetParam("@KAIKEI_KIKAN_SHURYOBI", SqlDbType.DateTime, 10, tmpDateTo);
            DataTable dtDBData =
                dba.GetDataTableByConditionWithParams("KAIKEI_NENDO", "VI_ZM_KAISHA_JOHO", where, "KAIKEI_NENDO", dpc);
            DataTable dtResult = new DataTable();
            ArrayList kaikeiNendo = new ArrayList();
            for (int i = 0; i < dtDBData.Rows.Count; i++)
            {
                kaikeiNendo.Add(dtDBData.Rows[i]["KAIKEI_NENDO"]);
            }

            return kaikeiNendo;
        }

        /// <summary>
        /// 指定した日付に該当する会計年度を取得します。
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        public static Decimal GetKaikeiNendo(DateTime tmpDate, DbAccess dba)
        {
            // 日付範囲に該当する会計年度を取得
            string where = "KAIKEI_KIKAN_KAISHIBI <= @KAIKEI_KIKAN_SHURYOBI";
            where += " AND KAIKEI_KIKAN_SHURYOBI >= @KAIKEI_KIKAN_KAISHIBI";
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAIKEI_KIKAN_KAISHIBI", SqlDbType.DateTime, 10, tmpDate);
            dpc.SetParam("@KAIKEI_KIKAN_SHURYOBI", SqlDbType.DateTime, 10, tmpDate);
            DataTable dtDBData =
                dba.GetDataTableByConditionWithParams("KAIKEI_NENDO", "TB_ZM_KAISHA_JOHO", where, "KAIKEI_NENDO", dpc);

            Decimal kaikeiNendo = 0;
            if (dtDBData.Rows.Count > 0)
            {
                kaikeiNendo = Util.ToDecimal(dtDBData.Rows[0]["KAIKEI_NENDO"]);
            }

            return kaikeiNendo;
        }

        /// <summary>
        /// 指定した会計年度の会計期間開始と終了の日付を取得します。
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        public static DataTable GetKaikeiKikan(Decimal kaikeiNendo, DbAccess dba)
        {
            // 日付範囲に該当する会計年度を取得
            string where = "KAIKEI_NENDO = @KAIKEI_NENDO";
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, kaikeiNendo);
            DataTable dtResult =
                dba.GetDataTableByConditionWithParams("KAIKEI_KIKAN_KAISHIBI, KAIKEI_KIKAN_SHURYOBI", "TB_ZM_KAISHA_JOHO", where, dpc);
            
            return dtResult;
        }

        /// <summary>
        /// 指定した会計年度の凍結情報を取得します。
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        public static Boolean GetKaikeiNendoFixedFlg(Decimal kaikeiNendo, DbAccess dba)
        {
            // 会計年度に該当する凍結フラグを取得
            string where = "KAIKEI_NENDO = @KAIKEI_NENDO";
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, kaikeiNendo);
            DataTable dtDBData =
                dba.GetDataTableByConditionWithParams("FIX_FLG", "TB_ZM_KAISHA_JOHO", where, dpc);
            Boolean fixedFlg = false;
            if (dtDBData.Rows.Count > 0 && Util.ToInt(dtDBData.Rows[0]["FIX_FLG"]) == 1)
            {
                fixedFlg = true;
            }

            return fixedFlg;
        }

        /// <summary>
        /// 指定した担当者の情報を取得します。
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        public static DataTable GetTantoshaDt(Decimal tantosha_cd, DbAccess dba)
        {
            // 指定した担当者の情報を取得
            string where = "TANTOSHA_CD = @TANTOSHA_CD";
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@TANTOSHA_CD", SqlDbType.Decimal, 4, tantosha_cd);
            DataTable dtResult =
                dba.GetDataTableByConditionWithParams("*", "TB_CM_TANTOSHA", where, dpc);

            return dtResult;
        }

        /// <summary>
        /// exeが配置されているPathを取得します。
        /// </summary>
        /// <returns>exeが配置されているPath</returns>
        public static string GetPath()
        {
            return Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
        }

        /// <summary>
        /// 印刷用使用列名文字配列の作成
        /// </summary>
        /// <param name="colsCnt">列数</param>
        /// <param name="Prefix">接頭詞</param>
        /// <returns>印刷テーブル使用列名文字列</returns>
        public static StringBuilder ColsArray(int colsCnt, string Prefix)
        {
            StringBuilder cols = new StringBuilder();
            string itemNm = "ITEM";
            string wk;
            for (int i = 1; i < colsCnt + 1; i++)
            {
                if (i == 1)
                {
                    wk = "  " + Prefix + itemNm + Util.PadZero(i, 2);
                }
                else
                {
                    wk = " ," + Prefix + itemNm + Util.PadZero(i, 2);
                }
                cols.Append(wk);
            }
            return cols;
        }

        /// <summary>
        /// PDF.EXCELの出力設定の取得
        /// </summary>
        /// <param name="fName">ドキュメント名称</param>
        /// <param name="filetype">出力タイプ 1:PDF、2:EXCEL、3:テキスト</param>
        /// <returns>保存ダイアログで指定されてたファイルパスを返す。ダイアログでキャンセルされた場合は空文字を返す</returns>
        public static string GetSavePath(Constants.SubSys sys, string fName, decimal filetype)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            ConfigLoader conf = new ConfigLoader();

            sfd.FileName = fName;
            string fType = "";
            sfd.Filter = "全てのファイル(*.*)|*.*";
            switch (filetype)
            {
                case 1:
                    fType = "PDF";
                    sfd.Filter += "|ＰＤＦファイル(*.pdf)|*.pdf";
                    break;
                case 2:
                    fType = "EXCEL";
                    sfd.Filter += "|Excel 97-2003 ブック(*.xls)|*.xls";
                    break;
                case 3:
                    fType = "TEXT";
                    sfd.Filter += "|Textファイル(*.csv)|*.csv";
                    break;
            }
            sfd.Title = fType + "ファイルの保存";
            string folderName = conf.LoadPgConfig(sys, "OUTPUT", fType, "PATH");
            if (ValChk.IsEmpty(folderName))
            {
                folderName = @"C:\";
            }
            sfd.InitialDirectory = folderName;

            sfd.AddExtension = true;
            sfd.FilterIndex = 2;
            DialogResult ret = sfd.ShowDialog();
            if (ret == DialogResult.OK)
            {
                folderName = System.IO.Path.GetDirectoryName(sfd.FileName);
                conf.SetPgConfig(sys, "OUTPUT", fType, "PATH", folderName);
                conf.SaveConfig();
                return sfd.FileName;
            }
            else
            {
                return "";
            }
        }

        /// <summary>
        /// 指定したファイルパスをエクスプローラで開きます。
        /// </summary>
        /// <param name="path"></param>
        public static void OpenFolder(string path)
        {
            System.Diagnostics.Process.Start("EXPLORER.EXE", @"/select," + path);
        }

        #endregion
    }
}
