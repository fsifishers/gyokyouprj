﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace jp.co.fsi.common.util
{
    /// <summary>
    /// メッセージ表示を扱う
    /// </summary>
    public static class Msg
    {
        /// <summary>
        /// 情報メッセージを表示します。
        /// </summary>
        /// <param name="msg">メッセージ</param>
        /// <returns>DialogResult</returns>
        public static DialogResult Info(string msg)
        {
            return MessageBox.Show(msg, "情報", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        /// <summary>
        /// メッセージを表示します。
        /// </summary>
        /// <param name="nm">画面名</param>
        /// <param name="msg">メッセージ</param>
        /// <returns>DialogResult</returns>
        public static DialogResult InfoNm(string nm, string msg)
        {
            return MessageBox.Show(msg, nm, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        /// <summary>
        /// 確認メッセージを表示します。(Yes/No形式)
        /// </summary>
        /// <param name="msg">メッセージ</param>
        /// <returns>DialogResult</returns>
        public static DialogResult ConfYesNo(string msg)
        {
            return MessageBox.Show(msg, "確認", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        }

        /// <summary>
        /// メッセージを表示します。(Yes/No形式)
        /// </summary>
        /// <param name="nm">画面名</param>
        /// <param name="msg">メッセージ</param>
        /// <returns>DialogResult</returns>
        public static DialogResult ConfNmYesNo(string nm, string msg)
        {
            return MessageBox.Show(msg, nm, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        }

        /// <summary>
        /// 確認メッセージを表示します。(OK/Cancel形式)
        /// </summary>
        /// <param name="msg">メッセージ</param>
        /// <returns>DialogResult</returns>
        public static DialogResult ConfOKCancel(string msg)
        {
            return MessageBox.Show(msg, "確認", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
        }

        /// <summary>
        /// メッセージを表示します。(OK/Cancel形式)
        /// </summary>
        /// <param name="nm">画面名</param>
        /// <param name="msg">メッセージ</param>
        /// <returns>DialogResult</returns>
        public static DialogResult ConfNmOKCancel(string nm, string msg)
        {
            return MessageBox.Show(msg, nm, MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
        }

        /// <summary>
        /// 警告メッセージを表示します。
        /// </summary>
        /// <param name="msg">メッセージ</param>
        /// <returns>DialogResult</returns>
        public static DialogResult Notice(string msg)
        {
            return MessageBox.Show(msg, "エラー", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        /// <summary>
        /// エラーメッセージ(深刻な問題)を表示します。
        /// </summary>
        /// <param name="msg">メッセージ</param>
        /// <returns>DialogResult</returns>
        public static DialogResult Error(string msg)
        {
            return MessageBox.Show(msg, "エラー", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }
}
