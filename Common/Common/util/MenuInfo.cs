﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text.RegularExpressions;
using System.Text;

namespace jp.co.fsi.common.util
{
    /// <summary>
    /// メニューの設定を保持するクラスです。
    /// </summary>
    public class MenuInfo
    {
        #region 定数
        /// <summary>
        /// 設定ファイルのファイル名
        /// </summary>
        private const string CONFIG_FILE_NM = "Menu.csv";
        #endregion

        #region private変数
        /// <summary>
        /// CSVから取得したメニューの設定内容を保持するDataTable
        /// </summary>
        private DataTable _dtMenuInfo;
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public MenuInfo()
        {
            LoadData();
        }
        #endregion

        #region publicメソッド
        /// <summary>
        /// サブシステム単位の設定を取得します。
        /// </summary>
        /// <param name="subSysId">サブシステムID</param>
        /// <returns>取得した行</returns>
        public DataRow LoadSubSysConf(string subSysId)
        {
            DataRow[] drc = this._dtMenuInfo.Select(
                "Kind = 1 AND SubSysId = '" + subSysId + "'");

            if (drc.Length > 0)
            {
                return drc[0];
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// サブシステム内の業務一覧を取得します。
        /// </summary>
        /// <param name="subSysId">サブシステムID</param>
        /// <returns>取得した行(コレクション)</returns>
        public DataTable GetGyomuList(string subSysId)
        {
            DataRow[] drc = this._dtMenuInfo.Select(
                "Kind = 2 AND SubSysId = '" + subSysId + "'", "Pos");

            DataTable dtResult = this._dtMenuInfo.Clone();

            for (int i = 0; i < drc.Length; i++)
            {
                dtResult.ImportRow(drc[i]);
            }

            return dtResult;
        }

        /// <summary>
        /// 業務内のメニュー一覧を取得します。
        /// </summary>
        /// <param name="subSysId">サブシステムID</param>
        /// <param name="gyomuId">業務ID</param>
        /// <returns>取得した行(コレクション)</returns>
        public DataTable GetMenuList(string subSysId, string gyomuId)
        {
            DataRow[] drc = this._dtMenuInfo.Select(
                "Kind = 3 AND SubSysId = '" + subSysId
                + "' AND GyomuId = '" + gyomuId + "'", "Pos");

            DataTable dtResult = this._dtMenuInfo.Clone();

            for (int i = 0; i < drc.Length; i++)
            {
                dtResult.ImportRow(drc[i]);
            }

            return dtResult;
        }

        /// <summary>
        /// プログラム単位の設定を取得します。
        /// </summary>
        /// <param name="prgId">プログラムID</param>
        /// <param name="par1">引数1</param>
        /// <param name="par2">引数2</param>
        /// <param name="par3">引数3</param>
        /// <returns>取得した行</returns>
        public DataRow LoadPgConf(string prgId, string par1, string par2, string par3)
        {
            DataRow[] drc = this._dtMenuInfo.Select(
                "PrgId = '" + prgId + "'"
                + " AND Par1 = '" + par1 + "'"
                + " AND Par2 = '" + par2 + "'"
                + " AND Par3 = '" + par3 + "'");

            if (drc.Length > 0)
            {
                return drc[0];
            }
            else
            {
                return null;
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// メニュー設定のCSVファイルから設定を読み込む
        /// </summary>
        /// <returns></returns>
        private void LoadData()
        {
            DataTable dtSave = new DataTable();
            dtSave.Columns.Add("Kind", typeof(int));
            dtSave.Columns.Add("SubSysId", typeof(string));
            dtSave.Columns.Add("GyomuId", typeof(string));
            dtSave.Columns.Add("PrgId", typeof(string));
            dtSave.Columns.Add("Namespace", typeof(string));
            dtSave.Columns.Add("PrgNm", typeof(string));
            dtSave.Columns.Add("Par1", typeof(string));
            dtSave.Columns.Add("Par2", typeof(string));
            dtSave.Columns.Add("Par3", typeof(string));
            dtSave.Columns.Add("Pos", typeof(int));
            dtSave.Columns.Add("CapEsc", typeof(string));
            dtSave.Columns.Add("CapF1", typeof(string));
            dtSave.Columns.Add("CapF2", typeof(string));
            dtSave.Columns.Add("CapF3", typeof(string));
            dtSave.Columns.Add("CapF4", typeof(string));
            dtSave.Columns.Add("CapF5", typeof(string));
            dtSave.Columns.Add("CapF6", typeof(string));
            dtSave.Columns.Add("CapF7", typeof(string));
            dtSave.Columns.Add("CapF8", typeof(string));
            dtSave.Columns.Add("CapF9", typeof(string));
            dtSave.Columns.Add("CapF10", typeof(string));
            dtSave.Columns.Add("CapF11", typeof(string));
            dtSave.Columns.Add("CapF12", typeof(string));
            DataRow drSave = null;

            try
            {
                // 現在のアセンブリが存在するディレクトリを取得
                string path = Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
                path = Path.Combine(path, CONFIG_FILE_NM);

                string[] strData;//分解後の文字用変数
                string strLine;//１行分のデータ
                Boolean fileExists = File.Exists(path);
                Boolean readTitle = false;
                if (fileExists)
                {
                    StreamReader sr = new StreamReader(path, Encoding.Default);
                    while (sr.Peek() >= 0)
                    {
                        strLine = sr.ReadLine();
                        // 1行目はすっ飛ばし
                        if (!readTitle)
                        {
                            readTitle = true;
                            continue;
                        }

                        strData = CsvToList(strLine)[0];
                        // 行を追加
                        drSave = dtSave.NewRow();
                        drSave["Kind"] = Util.ToInt(strData[0]);
                        drSave["SubSysId"] = strData[1];
                        drSave["GyomuId"] = strData[2];
                        drSave["PrgId"] = strData[3];
                        drSave["Namespace"] = strData[4];
                        drSave["PrgNm"] = strData[5];
                        drSave["Par1"] = strData[6];
                        drSave["Par2"] = strData[7];
                        drSave["Par3"] = strData[8];
                        drSave["Pos"] = Util.ToInt(strData[9]);
                        drSave["CapEsc"] = strData[22];
                        drSave["CapF1"] = strData[10];
                        drSave["CapF2"] = strData[11];
                        drSave["CapF3"] = strData[12];
                        drSave["CapF4"] = strData[13];
                        drSave["CapF5"] = strData[14];
                        drSave["CapF6"] = strData[15];
                        drSave["CapF7"] = strData[16];
                        drSave["CapF8"] = strData[17];
                        drSave["CapF9"] = strData[18];
                        drSave["CapF10"] = strData[19];
                        drSave["CapF11"] = strData[20];
                        drSave["CapF12"] = strData[21];
                        dtSave.Rows.Add(drSave);
                    }

                    sr.Close();
                }
            }
            catch (Exception)
            {
                //何もしない(デザイナへの影響を考慮)
            }

            this._dtMenuInfo = dtSave;
        }

        /// <summary>
        /// CSVをListに変換する
        /// </summary>
        /// <param name="csvText">CSVデータの1行分</param>
        /// <returns>Listオブジェクト</returns>
        private List<string[]> CsvToList(string csvText)
        {
            List<string[]> csvRecords = new List<string[]>();

            //前後の改行を削除しておく
            csvText = csvText.Trim(new char[] { '\r', '\n' });

            //一行取り出すための正規表現
            Regex regLine = new Regex("^.*(?:\\n|$)", RegexOptions.Multiline);

            //1行のCSVから各フィールドを取得するための正規表現
            Regex regCsv =
                new Regex("\\s*(\"(?:[^\"]|\"\")*\"|[^,]*)\\s*,", RegexOptions.None);

            System.Text.RegularExpressions.Match mLine = regLine.Match(csvText);
            while (mLine.Success)
            {
                //一行取り出す
                string line = mLine.Value;
                //改行記号が"で囲まれているか調べる
                while ((CountString(line, "\"") % 2) == 1)
                {
                    mLine = mLine.NextMatch();
                    if (!mLine.Success)
                    {
                        throw new ApplicationException("不正なCSV");
                    }
                    line += mLine.Value;
                }
                //行の最後の改行記号を削除
                line = line.TrimEnd(new char[] { '\r', '\n' });
                //最後に「,」をつける
                line += ",";

                //1つの行からフィールドを取り出す
                List<string> csvFields = new List<string>();
                Match m = regCsv.Match(line);
                while (m.Success)
                {
                    string field = m.Groups[1].Value;
                    //前後の空白を削除
                    field = field.Trim();
                    //"で囲まれている時
                    if (field.StartsWith("\"") && field.EndsWith("\""))
                    {
                        //前後の"を取る
                        field = field.Substring(1, field.Length - 2);
                        //「""」を「"」にする
                        field = field.Replace("\"\"", "\"");
                    }
                    csvFields.Add(field);
                    m = m.NextMatch();
                }

                csvFields.TrimExcess();
                csvRecords.Add(csvFields.ToArray());

                mLine = mLine.NextMatch();
            }

            csvRecords.TrimExcess();
            return csvRecords;
        }

        /// <summary>
        /// 指定された文字列内にある文字列が幾つあるか数える
        /// </summary>
        /// <param name="strInput">strFindが幾つあるか数える文字列</param>
        /// <param name="strFind">数える文字列</param>
        /// <returns>strInput内にstrFindが幾つあったか</returns>
        private int CountString(string strInput, string strFind) {
            int foundCount = 0;
            int sPos = strInput.IndexOf(strFind);
            while(sPos > -1) {
                foundCount++;
                sPos = strInput.IndexOf(strFind, sPos + 1);
            }

            return foundCount;
        }
        #endregion
    }
}
