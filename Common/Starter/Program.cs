﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

using jp.co.fsi.common.util;

namespace jp.co.fsi.comon.starter
{
    static class Program
    {
        /// <summary>
        /// アプリケーションのメイン エントリ ポイントです。
        /// </summary>
        [STAThread]
        static void Main()
        {
            ConfigLoader cLdr = new ConfigLoader();

            if ("1".Equals(cLdr.LoadCommonConfig("UserInfo", "dologin")))
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new Starter());
            }
            else
            {
                Process.Start("Menu.exe");
            }
        }
    }
}
