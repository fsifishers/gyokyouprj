﻿using System;
using System.Diagnostics;
using System.Windows.Forms;

using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;
using System.Data;
using jp.co.fsi.common.dataaccess;
using System.Text;

namespace jp.co.fsi.comon.starter
{
    /// <summary>
    /// スターター画面です。
    /// (ログインが必要であればログインし、不要であれば、そのままメニューを起動)
    /// </summary>
    public partial class Starter : BaseForm
    {
        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public Starter()
        {
            // デフォルトは画面を隠す
            this.Visible = false;
            InitializeComponent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// プログラム個別の初期処理
        /// </summary>
        /// <remarks>親クラスからの継承</remarks>
        protected override void InitForm()
        {
            // NOTICE:20140331改良・設定ファイルからユーザー名を初期表示&入力不可にしておく
            string userid = this.Config.LoadCommonConfig("UserInfo", "userid");
            this.txtUserId.Text = userid;
        }
        #endregion

        #region イベント
        /// <summary>
        /// ログインボタンクリック時の処理です
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnLogin_Click(object sender, System.EventArgs e)
        {
            string tantoshaCd;// ユーザID
            string userId;    // ユーザID
            string passWord;  // パスワード
            string seriFlg;   // セリ販売の画面を1：表示する 0：表示しない
            string kobaiFlg;  // 購買の画面を1：表示する 0：表示しない
            string zaimuFlg;  // 財務の画面を1：表示する 0：表示しない
            string kyuyoFlg;  // 給与の画面を1：表示する 0：表示しない

            DbParamCollection dpc = new DbParamCollection();
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT");
            sql.Append(" * ");
            sql.Append("FROM");
            sql.Append(" TB_CM_TANTOSHA ");
            sql.Append("WHERE");
            sql.Append(" KAISHA_CD = @KAISHA_CD AND");
            sql.Append(" USER_ID = @USER_ID AND");
            sql.Append(" PASSWORD = @PASSWORD");
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@USER_ID", SqlDbType.VarChar, 15, this.txtUserId.Text);
            dpc.SetParam("@PASSWORD", SqlDbType.VarChar, 15, this.txtPassWord.Text);
            DataTable dtUser = this.Dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            if (dtUser.Rows.Count > 0)
            {
                tantoshaCd = Util.ToString(dtUser.Rows[0]["TANTOSHA_CD"]);
                userId = Util.ToString(dtUser.Rows[0]["USER_ID"]);
                passWord = Util.ToString(dtUser.Rows[0]["PASSWORD"]);
                seriFlg = Util.ToString(dtUser.Rows[0]["SERI_FLG"]);
                kobaiFlg = Util.ToString(dtUser.Rows[0]["KOBAI_FLG"]);
                zaimuFlg = Util.ToString(dtUser.Rows[0]["ZAIMU_FLG"]);
                kyuyoFlg = Util.ToString(dtUser.Rows[0]["KYUYO_FLG"]);

                //configを書き換える
                this.Config.SetCommonConfig("UserInfo", "usercd", tantoshaCd);
                this.Config.SetCommonConfig("UserInfo", "userid", userId);
                this.Config.SetCommonConfig("UserInfo", "passwd", passWord);
                this.Config.SetCommonConfig("UserInfo", "usehn", seriFlg);
                this.Config.SetCommonConfig("UserInfo", "usekb", kobaiFlg);
                this.Config.SetCommonConfig("UserInfo", "usezm", zaimuFlg);
                this.Config.SetCommonConfig("UserInfo", "useky", kyuyoFlg);
                this.Config.SaveConfig();
                // メニュー画面を起動
                Process.Start("Menu.exe");
                this.Close();
            }
            else
            {
                Msg.Notice("ユーザーIDもしくはパスワードが不正です。" + Environment.NewLine
                    + "再度入力してください。");
                this.txtUserId.Focus();
            }
        }

        /// <summary>
        /// キャンセルボタンクリック時の処理です
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancel_Click(object sender, System.EventArgs e)
        {
            if (Msg.ConfOKCancel(
                "ログインを取りやめます。" + Environment.NewLine +
                "よろしいですか？")
                == DialogResult.OK)
            {
                this.Close();
            }
        }
        #endregion
    }
}
