﻿using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Windows.Forms;

using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.common.menu
{
    /// <summary>
    /// メニュー画面です。
    /// </summary>
    public partial class Menu : BaseForm
    {
        #region 定数
        /// <summary>
        /// 画像が配置されているフォルダ
        /// </summary>
        private const string IMG_DIR = "images";

        /// <summary>
        /// セリ販売のデフォルト画像のファイル名
        /// </summary>
        private const string IMG_SERI_DEF_NM = "bt_seri_def.png";

        /// <summary>
        /// セリ販売の有効時の画像のファイル名
        /// </summary>
        private const string IMG_SERI_ON_NM = "bt_seri_on.png";

        /// <summary>
        /// 購買のデフォルト画像のファイル名
        /// </summary>
        private const string IMG_KOBAI_DEF_NM = "bt_kobai_def.png";

        /// <summary>
        /// 購買の有効時の画像のファイル名
        /// </summary>
        private const string IMG_KOBAI_ON_NM = "bt_kobai_on.png";

        /// <summary>
        /// 財務会計のデフォルト画像のファイル名
        /// </summary>
        private const string IMG_ZAIMU_DEF_NM = "bt_zaimu_def.png";

        /// <summary>
        /// 財務会計の有効時の画像のファイル名
        /// </summary>
        private const string IMG_ZAIMU_ON_NM = "bt_zaimu_on.png";

        /// <summary>
        /// 給与のデフォルト画像のファイル名
        /// </summary>
        private const string IMG_KYUYO_DEF_NM = "bt_kyuyo_def.png";

        /// <summary>
        /// 給与の有効時の画像のファイル名
        /// </summary>
        private const string IMG_KYUYO_ON_NM = "bt_kyuyo_on.png";

        /// <summary>
        /// 食堂販売のデフォルト画像のファイル名
        /// </summary>
        private const string IMG_SHOKUDO_DEF_NM = "bt_shokudo_def.png";

        /// <summary>
        /// 食堂販売の有効時の画像のファイル名
        /// </summary>
        private const string IMG_SHOKUDO_ON_NM = "bt_shokudo_on.png";

        /// <summary>
        /// 実行中の業務の色
        /// </summary>
        private const string IMG_ACTIVE_GYOMU = "bt_header_on.png";

        /// <summary>
        /// 実行中でない業務の色
        /// </summary>
        private const string IMG_INACTIVE_GYOMU = "bt_header_off.png";

        /// <summary>
        /// 実行中のメニューの色
        /// </summary>
        private const string IMG_ACTIVE_MENU = "bt_side_on.png";

        /// <summary>
        /// 実行中でないメニューの色
        /// </summary>
        private const string IMG_INACTIVE_MENU = "bt_side_off.png";

        /// <summary>
        /// 実行中のアイテムのフォント色
        /// </summary>
        private Color ACTIVE_FONT_COLOR = Color.FromArgb(255, 255, 255);

        /// <summary>
        /// 実行中でないアイテムのフォント色
        /// </summary>
        private Color INACTIVE_FONT_COLOR = Color.FromArgb(0, 0, 0);
        #endregion

        #region プロパティ
        private BasePgForm _runningPg;
        /// <summary>
        /// 現在起動中のプログラム
        /// </summary>
        public BasePgForm RunningPg
        {
            get
            {
                return this._runningPg;
            }
            set
            {
                this._runningPg = value;
                if (this._runningPg == null)
                {
                    // サブシステム選択のファンクションを表示
                    DispFunctionKey();
                }
            }
        }
        #endregion

        #region private変数
        /// <summary>
        /// 現在実行中のサブシステムのインデックス
        /// </summary>
        private int _runningSubSysIdx;

        /// <summary>
        /// 表示している業務一覧の情報
        /// </summary>
        private DataTable _dtGyomu;

        /// <summary>
        /// 現在実行中の業務のインデックス
        /// </summary>
        private int _runningGyomuIdx;

        /// <summary>
        /// 表示しているメニュー一覧の情報
        /// </summary>
        private DataTable _dtMenu;

        /// <summary>
        /// 現在実行中のメニューのインデックス
        /// </summary>
        private int _runningMenuIdx;
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public Menu()
        {
            InitializeComponent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理です。
        /// </summary>
        protected override void InitForm()
        {
            // タイトルの表示
            this.Text = this.UInfo.KaishaNm + " " + UInfo.UserNm + " 様";

            // 現在日付の表示
            string[] arrJpDate = Util.ConvJpDate(DateTime.Now, this.Dba);
            this.lblNowDate.Text = arrJpDate[5];

            // 会計期の表示
            this.lblAccYear.Text = this.UInfo.KessanKiDsp;

            // 使用可否区分によってボタンの表示を制御する
            // セリ販売
            this.btnSubSys1.Visible = this.Config.LoadCommonConfig("UserInfo", "usehn").Equals("1");
            // 購買
            this.btnSubSys2.Visible = this.Config.LoadCommonConfig("UserInfo", "usekb").Equals("1");
            // 財務会計
            this.btnSubSys3.Visible = this.Config.LoadCommonConfig("UserInfo", "usezm").Equals("1");
            // 給与
            this.btnSubSys4.Visible = this.Config.LoadCommonConfig("UserInfo", "useky").Equals("1");
            // 食堂販売
            this.btnSubSys5.Visible = this.Config.LoadCommonConfig("UserInfo", "usesk").Equals("1");

            // ボタンの初期化
            ClearForm();
        }

        /// <summary>
        /// F1クリック時処理
        /// </summary>
        public override void PressF1()
        {
            if (this._runningPg == null)
            {
                // 業務ボタンのエミュレート
                if (this.btnGyomu1.Enabled) this.btnGyomu1.PerformClick();
            }
        }

        /// <summary>
        /// F2クリック時処理
        /// </summary>
        public override void PressF2()
        {
            if (this._runningPg == null)
            {
                // 業務ボタンのエミュレート
                if (this.btnGyomu2.Enabled) this.btnGyomu2.PerformClick();
            }
        }

        /// <summary>
        /// F3クリック時処理
        /// </summary>
        public override void PressF3()
        {
            if (this._runningPg == null)
            {
                // 業務ボタンのエミュレート
                if (this.btnGyomu3.Enabled) this.btnGyomu3.PerformClick();
            }
        }

        /// <summary>
        /// F4クリック時処理
        /// </summary>
        public override void PressF4()
        {
            if (this._runningPg == null)
            {
                // 業務ボタンのエミュレート
                if (this.btnGyomu4.Enabled) this.btnGyomu4.PerformClick();
            }
        }

        /// <summary>
        /// F5クリック時処理
        /// </summary>
        public override void PressF5()
        {
            if (this._runningPg == null)
            {
                // 業務ボタンのエミュレート
                if (this.btnGyomu5.Enabled) this.btnGyomu5.PerformClick();
            }
        }

        /// <summary>
        /// F6クリック時処理
        /// </summary>
        public override void PressF6()
        {
            if (this._runningPg == null)
            {
                // 業務ボタンのエミュレート
                if (this.btnGyomu6.Enabled) this.btnGyomu6.PerformClick();
            }
        }

        /// <summary>
        /// F7クリック時処理
        /// </summary>
        public override void PressF7()
        {
            if (this._runningPg == null)
            {
                // 業務ボタンのエミュレート
                if (this.btnGyomu7.Enabled) this.btnGyomu7.PerformClick();
            }
        }

        /// <summary>
        /// F8クリック時処理
        /// </summary>
        public override void PressF8()
        {
            if (this._runningPg == null)
            {
                // 業務ボタンのエミュレート
                if (this.btnGyomu8.Enabled) this.btnGyomu8.PerformClick();
            }
        }

        /// <summary>
        /// F9クリック時処理
        /// </summary>
        public override void PressF9()
        {
            if (this._runningPg == null)
            {
                // 業務ボタンのエミュレート
                if (this.btnGyomu9.Enabled) this.btnGyomu9.PerformClick();
            }
        }

        /// <summary>
        /// F10クリック時処理
        /// </summary>
        public override void PressF10()
        {
            if (this._runningPg == null)
            {
                // 業務ボタンのエミュレート
                if (this.btnGyomu10.Enabled) this.btnGyomu10.PerformClick();
            }
        }

        /// <summary>
        /// Escクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // メニューボタンパネルを表示する
            this.pnlSubBtn.Visible = true;
        }
        #endregion

        #region イベント
        /// <summary>
        /// 会計期間クリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lblAccYear_Click(object sender, EventArgs e)
        {
            // アセンブリのロード
            Assembly asm = Assembly.LoadFrom("CMCM1011.exe");
            // フォーム作成
            Type t = asm.GetType("jp.co.fsi.cm.cmcm1011.CMCM1011");
            if (t != null)
            {
                Object obj = System.Activator.CreateInstance(t);
                if (obj != null)
                {
                    BasePgForm frm = (BasePgForm)obj;
                    frm.InData = this.UInfo.KessanKi;
                    frm.ShowDialog(this);

                    if (frm.DialogResult == DialogResult.OK)
                    {
                        string[] outData = (string[])frm.OutData;
                        this.UInfo.KaishaCd = outData[0];
                        this.UInfo.KaishaNm = outData[1];
                        this.UInfo.KessanKi = Util.ToInt(outData[2]);
                        this.Config.SetCommonConfig("UserInfo", "kaishacd", outData[0]);
                        this.Config.SetCommonConfig("UserInfo", "kaishanm", outData[1]);
                        this.Config.SetCommonConfig("UserInfo", "kaikeinendo", Util.ToString(this.UInfo.KaikeiNendo));
                        this.Config.SetCommonConfig("UserInfo", "kessanki", Util.ToString(this.UInfo.KessanKi));
                        this.Config.SetCommonConfig("UserInfo", "kessankiDsp", Util.ToString(this.UInfo.KessanKiDsp));
                        this.Config.SaveConfig();
                        this.Text = this.UInfo.KaishaNm + " " + UInfo.UserNm + " 様";
                        this.lblAccYear.Text = this.UInfo.KessanKiDsp;
                    }

                    frm.Dispose();
                }
            }
        }

        /// <summary>
        /// カレンダーアイコンクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pbxCalendar_Click(object sender, EventArgs e)
        {
            // アセンブリのロード
            Assembly asm = Assembly.LoadFrom("Calendar.exe");
            // フォーム作成
            Type t = asm.GetType("jp.co.fsi.com.calendar.Calendar");
            if (t != null)
            {
                Object obj = System.Activator.CreateInstance(t);
                if (obj != null)
                {
                    BasePgForm frm = (BasePgForm)obj;
                    frm.InData = DateTime.Now;
                    frm.ShowDialog(this);

                    frm.Dispose();
                }
            }
        }

        /// <summary>
        /// ログアウトボタンクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnLogout_Click(object sender, EventArgs e)
        {
            if (Msg.ConfOKCancel("終了します。よろしいですか？") == System.Windows.Forms.DialogResult.OK)
            {
                // 画面を閉じる
                this.Close();
            }

            this.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange;
        }

        /// <summary>
        /// サブシステムクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSubSys_Click(object sender, EventArgs e)
        {
            Button btnTmp;

            // 実行中のプログラムを閉じて無効にする
            if (this._runningPg != null)
            {
                this._runningPg.Close();
                this._runningPg = null;
            }

            // クリックされたボタンの色を変える
            this._runningSubSysIdx = Util.ToInt(((Button)sender).Name.Replace("btnSubSys", ""));

            switch (this._runningSubSysIdx)
            {
                case 1:
                    btnSubSys1.Image = System.Drawing.Image.FromFile(Path.Combine(IMG_DIR, IMG_SERI_ON_NM));
                    btnSubSys2.Image = System.Drawing.Image.FromFile(Path.Combine(IMG_DIR, IMG_KOBAI_DEF_NM));
                    btnSubSys3.Image = System.Drawing.Image.FromFile(Path.Combine(IMG_DIR, IMG_ZAIMU_DEF_NM));
                    btnSubSys4.Image = System.Drawing.Image.FromFile(Path.Combine(IMG_DIR, IMG_KYUYO_DEF_NM));
                    btnSubSys5.Image = System.Drawing.Image.FromFile(Path.Combine(IMG_DIR, IMG_SHOKUDO_DEF_NM));
                    break;

                case 2:
                    btnSubSys1.Image = System.Drawing.Image.FromFile(Path.Combine(IMG_DIR, IMG_SERI_DEF_NM));
                    btnSubSys2.Image = System.Drawing.Image.FromFile(Path.Combine(IMG_DIR, IMG_KOBAI_ON_NM));
                    btnSubSys3.Image = System.Drawing.Image.FromFile(Path.Combine(IMG_DIR, IMG_ZAIMU_DEF_NM));
                    btnSubSys4.Image = System.Drawing.Image.FromFile(Path.Combine(IMG_DIR, IMG_KYUYO_DEF_NM));
                    btnSubSys5.Image = System.Drawing.Image.FromFile(Path.Combine(IMG_DIR, IMG_SHOKUDO_DEF_NM));
                    break;

                case 3:
                    btnSubSys1.Image = System.Drawing.Image.FromFile(Path.Combine(IMG_DIR, IMG_SERI_DEF_NM));
                    btnSubSys2.Image = System.Drawing.Image.FromFile(Path.Combine(IMG_DIR, IMG_KOBAI_DEF_NM));
                    btnSubSys3.Image = System.Drawing.Image.FromFile(Path.Combine(IMG_DIR, IMG_ZAIMU_ON_NM));
                    btnSubSys4.Image = System.Drawing.Image.FromFile(Path.Combine(IMG_DIR, IMG_KYUYO_DEF_NM));
                    btnSubSys5.Image = System.Drawing.Image.FromFile(Path.Combine(IMG_DIR, IMG_SHOKUDO_DEF_NM));
                    break;

                case 4:
                    btnSubSys1.Image = System.Drawing.Image.FromFile(Path.Combine(IMG_DIR, IMG_SERI_DEF_NM));
                    btnSubSys2.Image = System.Drawing.Image.FromFile(Path.Combine(IMG_DIR, IMG_KOBAI_DEF_NM));
                    btnSubSys3.Image = System.Drawing.Image.FromFile(Path.Combine(IMG_DIR, IMG_ZAIMU_DEF_NM));
                    btnSubSys4.Image = System.Drawing.Image.FromFile(Path.Combine(IMG_DIR, IMG_KYUYO_ON_NM));
                    btnSubSys5.Image = System.Drawing.Image.FromFile(Path.Combine(IMG_DIR, IMG_SHOKUDO_DEF_NM));
                    break;

                case 5:
                    btnSubSys1.Image = System.Drawing.Image.FromFile(Path.Combine(IMG_DIR, IMG_SERI_DEF_NM));
                    btnSubSys2.Image = System.Drawing.Image.FromFile(Path.Combine(IMG_DIR, IMG_KOBAI_DEF_NM));
                    btnSubSys3.Image = System.Drawing.Image.FromFile(Path.Combine(IMG_DIR, IMG_ZAIMU_DEF_NM));
                    btnSubSys4.Image = System.Drawing.Image.FromFile(Path.Combine(IMG_DIR, IMG_KYUYO_DEF_NM));
                    btnSubSys5.Image = System.Drawing.Image.FromFile(Path.Combine(IMG_DIR, IMG_SHOKUDO_ON_NM));
                    break;

                default:
                    break;
            }

            // 実行中の業務のインデックス・色をクリア
            // また、設定ファイルから表示をする
            
            // 設定ファイルからファンクションキーの情報を取得
            DispFunctionKey();

            // 設定ファイルから業務一覧を取得
            string subSysId = GetSubSysId();
            this._dtGyomu = this.MInfo.GetGyomuList(subSysId);
            DataRow[] arrGyomuSetting;

            // 初期PG表示フラグ
            int dispFlg = 0;

            this._runningGyomuIdx = 0;
            for (int i = 1; i <= 10; i++)
            {
                btnTmp = (Button)this.Controls.Find("btnGyomu" + Util.ToString(i), true)[0];
                // 現在実行中でない業務の色設定
                btnTmp.BackgroundImage = System.Drawing.Image.FromFile(Path.Combine(IMG_DIR, IMG_INACTIVE_GYOMU));

                arrGyomuSetting = this._dtGyomu.Select("Pos = " + Util.ToString(i));
                if (arrGyomuSetting.Length > 0)
                {
                    // 指定した位置に表示するメニューがあればテキスト表示
                    btnTmp.Text = Util.ToString(arrGyomuSetting[0]["PrgNm"]);
                    btnTmp.Enabled = true;

                    // 20150414 oomine-add:名護版カスタマイズ↓
                    if (Util.ToString(arrGyomuSetting[0]["PrgNm"]) == "購買(日次)" && this._runningSubSysIdx == 2)
                    {
                        // 購買初期表示フラグをたてる
                        dispFlg = 2;

                        // 現在実行中の業務の色設定
                        btnTmp.BackgroundImage = System.Drawing.Image.FromFile(Path.Combine(IMG_DIR, IMG_ACTIVE_GYOMU));
                    }
                    else if (Util.ToString(arrGyomuSetting[0]["PrgNm"]) == "日常処理" && this._runningSubSysIdx == 3)
                    {
                        // 財務初期表示フラグをたてる
                        dispFlg = 3;

                        // 現在実行中の業務の色設定
                        btnTmp.BackgroundImage = System.Drawing.Image.FromFile(Path.Combine(IMG_DIR, IMG_ACTIVE_GYOMU));
                    }
                    // 20150414 oomine-add:名護版カスタマイズ↑
                }
                else
                {
                    // 指定した位置に表示するメニューがなければボタンは無効
                    btnTmp.Text = string.Empty;
                    btnTmp.Enabled = false;
                }
            }

            // 20150414 oomine-add:名護版カスタマイズ↓
            // 購買・財務の場合、プログラムの初期表示設定をする
            if (dispFlg != 0)
            {
                string gyoumuId;
                string prgNm;

                switch(dispFlg)
                {
                    case 2:
                        gyoumuId = "KOB1";
                        prgNm = "売上伝票入力";
                        break;
                    case 3:
                        gyoumuId = "ZAM1";
                        prgNm = "振替伝票入力";
                        break;
                    default:
                        gyoumuId = "";
                        prgNm = "";
                        break;
                }
                // 設定ファイルから業務にぶら下がるメニュー一覧を取得
                this._dtMenu = this.MInfo.GetMenuList(subSysId, gyoumuId);
                DataRow[] arrMenuSetting;

                // 実行中のメニューのインデックス・色をクリア
                for (int i = 1; i <= 16; i++)
                {
                    btnTmp = (Button)this.Controls.Find("btnMenu" + Util.ToString(i), true)[0];
                    // 現在実行中でないメニューの色設定
                    btnTmp.ForeColor = INACTIVE_FONT_COLOR;
                    btnTmp.BackgroundImage = System.Drawing.Image.FromFile(Path.Combine(IMG_DIR, IMG_INACTIVE_MENU));

                    arrMenuSetting = this._dtMenu.Select("Pos = " + Util.ToString(i));
                    if (arrMenuSetting.Length > 0)
                    {
                        // 指定した位置に表示するメニューがあればテキスト表示
                        btnTmp.Text = Util.ToString(arrMenuSetting[0]["PrgNm"]);
                        btnTmp.Enabled = true;

                        if (Util.ToString(arrMenuSetting[0]["PrgNm"]) == prgNm)
                        {
                            // 現在実行中のメニューの色設定
                            btnTmp.ForeColor = ACTIVE_FONT_COLOR;
                            btnTmp.BackgroundImage = System.Drawing.Image.FromFile(Path.Combine(IMG_DIR, IMG_ACTIVE_MENU));

                            // クリックされたPGの情報を取得
                            DataRow[] arrPgStg = this._dtMenu.Select("Pos = " + Util.ToString(i));
                            if (arrPgStg.Length > 0)
                            {
                                DataRow drPgStg = arrPgStg[0];

                                // クリックされたPGを起動
                                try
                                {
                                    // アセンブリのロード
                                    Assembly asm = Assembly.LoadFrom(Util.ToString(drPgStg["PrgId"]) + ".exe");
                                    // フォーム作成
                                    Type t = asm.GetType(Util.ToString(drPgStg["Namespace"]) + "." + Util.ToString(drPgStg["PrgId"]));

                                    if (t != null)
                                    {
                                        Object obj = System.Activator.CreateInstance(t);
                                        if (obj != null)
                                        {
                                            // タブの一部として埋め込む
                                            BasePgForm frm = (BasePgForm)obj;
                                            frm.TopLevel = false;
                                            frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                                            frm.MenuFrm = this;
                                            frm.Par1 = Util.ToString(drPgStg["Par1"]);
                                            frm.Par2 = Util.ToString(drPgStg["Par2"]);
                                            frm.Par3 = Util.ToString(drPgStg["Par3"]);
                                            pnlMain.Controls.Add(frm);
                                            frm.Show();
                                            frm.BringToFront();

                                            // PGのインスタンスを保存
                                            this._runningPg = frm;
                                        }
                                    }
                                }
                                catch (Exception)
                                {
                                    Msg.Error("起動するプログラムが見つかりません。"
                                        + Environment.NewLine + "実行環境をご確認ください。");
                                }
                            }
                            else
                            {
                                Msg.Error("プログラムの設定が見つかりません。"
                                    + Environment.NewLine + "Menu.csvの設定をご確認ください。");
                            }
                        }
                    }
                    else
                    {
                        // 指定した位置に表示するメニューがなければボタンは無効
                        btnTmp.Text = string.Empty;
                        btnTmp.Enabled = false;
                    }
                }
            }
            // 20150414 oomine-add:名護版カスタマイズ↑
            else
            {
                // 実行中のメニューのインデックス・色をクリア
                this._runningMenuIdx = 0;
                this._dtMenu = null;
                for (int i = 1; i <= 16; i++)
                {
                    btnTmp = (Button)this.Controls.Find("btnMenu" + Util.ToString(i), true)[0];
                    // 現在実行中でないメニューの色設定
                    btnTmp.ForeColor = INACTIVE_FONT_COLOR;
                    btnTmp.BackgroundImage = System.Drawing.Image.FromFile(Path.Combine(IMG_DIR, IMG_INACTIVE_MENU));
                    btnTmp.Text = string.Empty;
                    btnTmp.Enabled = false;
                }
            }

            // メニューボタンパネルを表示する
            this.pnlSubBtn.Visible = true;
        }

        /// <summary>
        /// 業務クリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnGyomu_Click(object sender, EventArgs e)
        {
            Button btnTmp;

            // 実行中のプログラムを閉じて無効にする
            if (this._runningPg != null)
            {
                this._runningPg.Close();
                this._runningPg = null;
            }

            // クリックされたボタンの色を変える
            this._runningGyomuIdx = Util.ToInt(((Button)sender).Name.Replace("btnGyomu", ""));

            for (int i = 1; i <= 10; i++)
            {
                btnTmp = (Button)this.Controls.Find("btnGyomu" + Util.ToString(i), true)[0];
                if (i == this._runningGyomuIdx)
                {
                    // 現在実行中の業務の色設定
                    btnTmp.BackgroundImage = System.Drawing.Image.FromFile(Path.Combine(IMG_DIR, IMG_ACTIVE_GYOMU));
                }
                else
                {
                    // 現在実行中でない業務の色設定
                    btnTmp.BackgroundImage = System.Drawing.Image.FromFile(Path.Combine(IMG_DIR, IMG_INACTIVE_GYOMU));
                }
            }

            // 実行中のサブシステムのIDを割り当て
            string subSysId = GetSubSysId();

            // クリックされたボタンの業務IDを取得
            string gyomuId = Util.ToString(this._dtGyomu.Select("Pos = " + Util.ToString(this._runningGyomuIdx))[0]["GyomuId"]);

            // 設定ファイルから業務にぶら下がるメニュー一覧を取得
            this._dtMenu = this.MInfo.GetMenuList(subSysId, gyomuId);
            DataRow[] arrMenuSetting;

            // 実行中のメニューのインデックス・色をクリア
            this._runningMenuIdx = 0;
            for (int i = 1; i <= 16; i++)
            {
                btnTmp = (Button)this.Controls.Find("btnMenu" + Util.ToString(i), true)[0];
                // 現在実行中でないメニューの色設定
                btnTmp.ForeColor = INACTIVE_FONT_COLOR;
                btnTmp.BackgroundImage = System.Drawing.Image.FromFile(Path.Combine(IMG_DIR, IMG_INACTIVE_MENU));

                arrMenuSetting = this._dtMenu.Select("Pos = " + Util.ToString(i));
                if (arrMenuSetting.Length > 0)
                {
                    // 指定した位置に表示するメニューがあればテキスト表示
                    btnTmp.Text = Util.ToString(arrMenuSetting[0]["PrgNm"]);
                    btnTmp.Enabled = true;
                }
                else
                {
                    // 指定した位置に表示するメニューがなければボタンは無効
                    btnTmp.Text = string.Empty;
                    btnTmp.Enabled = false;
                }
            }

            // メニューボタンパネルを表示する
            this.pnlSubBtn.Visible = true;
        }

        /// <summary>
        /// メニュークリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnMenu_Click(object sender, EventArgs e)
        {
            Button btnTmp;

            // 実行中のプログラムを閉じて無効にする
            if (this._runningPg != null)
            {
                this._runningPg.Close();
                this._runningPg = null;
            }

            // クリックされたボタンの色を変える
            this._runningMenuIdx = Util.ToInt(((Button)sender).Name.Replace("btnMenu", ""));

            for (int i = 1; i <= 16; i++)
            {
                btnTmp = (Button)this.Controls.Find("btnMenu" + Util.ToString(i), true)[0];
                if (i == this._runningMenuIdx)
                {
                    // 現在実行中のメニューの色設定
                    btnTmp.ForeColor = ACTIVE_FONT_COLOR;
                    btnTmp.BackgroundImage = System.Drawing.Image.FromFile(Path.Combine(IMG_DIR, IMG_ACTIVE_MENU));
                }
                else
                {
                    // 現在実行中でないメニューの色設定
                    btnTmp.ForeColor = INACTIVE_FONT_COLOR;
                    btnTmp.BackgroundImage = System.Drawing.Image.FromFile(Path.Combine(IMG_DIR, IMG_INACTIVE_MENU));
                }
            }

            // クリックされたPGの情報を取得
            DataRow[] arrPgStg = this._dtMenu.Select("Pos = " + Util.ToString(this._runningMenuIdx));
            if (arrPgStg.Length > 0)
            {
                DataRow drPgStg = arrPgStg[0];

                // クリックされたPGを起動
                try
                {
                    // アセンブリのロード
                    Assembly asm = Assembly.LoadFrom(Util.ToString(drPgStg["PrgId"]) + ".exe");

                    // フォーム作成
                    Type t = asm.GetType(Util.ToString(drPgStg["Namespace"]) + "." + Util.ToString(drPgStg["PrgId"]));

                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.TopLevel = false;
                            frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                            frm.MenuFrm = this;
                            frm.Par1 = Util.ToString(drPgStg["Par1"]);
                            frm.Par2 = Util.ToString(drPgStg["Par2"]);
                            frm.Par3 = Util.ToString(drPgStg["Par3"]);
                            pnlMain.Controls.Add(frm);
                            frm.Show();
                            frm.BringToFront();

                            // ファンクションキーの状態を引き継ぎ
                            this.btnEsc.Text = frm.btnEsc.Text;
                            this.btnEsc.Enabled = frm.btnEsc.Enabled;
                            this.btnF1.Text = frm.btnF1.Text;
                            this.btnF1.Enabled = frm.btnF1.Enabled;
                            this.btnF2.Text = frm.btnF2.Text;
                            this.btnF2.Enabled = frm.btnF2.Enabled;
                            this.btnF3.Text = frm.btnF3.Text;
                            this.btnF3.Enabled = frm.btnF3.Enabled;
                            this.btnF4.Text = frm.btnF4.Text;
                            this.btnF4.Enabled = frm.btnF4.Enabled;
                            this.btnF5.Text = frm.btnF5.Text;
                            this.btnF5.Enabled = frm.btnF5.Enabled;
                            this.btnF6.Text = frm.btnF6.Text;
                            this.btnF6.Enabled = frm.btnF6.Enabled;
                            this.btnF7.Text = frm.btnF7.Text;
                            this.btnF7.Enabled = frm.btnF7.Enabled;
                            this.btnF8.Text = frm.btnF8.Text;
                            this.btnF8.Enabled = frm.btnF8.Enabled;
                            this.btnF9.Text = frm.btnF9.Text;
                            this.btnF9.Enabled = frm.btnF9.Enabled;
                            this.btnF10.Text = frm.btnF10.Text;
                            this.btnF10.Enabled = frm.btnF10.Enabled;
                            this.btnF11.Text = frm.btnF11.Text;
                            this.btnF11.Enabled = frm.btnF11.Enabled;
                            this.btnF12.Text = frm.btnF12.Text;
                            this.btnF12.Enabled = frm.btnF12.Enabled;

                            // PGのインスタンスを保存
                            this._runningPg = frm;
                        }
                    }

                    // メニューボタンパネルを非表示にする
                    this.pnlSubBtn.Visible = false;
                }
                catch (Exception)
                {
                    Msg.Error("起動するプログラムが見つかりません。"
                        + Environment.NewLine + "実行環境をご確認ください。");
                }
            }
            else
            {
                Msg.Error("プログラムの設定が見つかりません。"
                    + Environment.NewLine + "Menu.csvの設定をご確認ください。");
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEsc_Click(object sender, EventArgs e)
        {
            if (this._runningPg == null)
            {
                this.PressEsc();
            }
            else
            {
                this._runningPg.PressEsc();
            }
        }

        /// <summary>
        /// F1ボタンクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnF1_Click(object sender, EventArgs e)
        {
            if (this._runningPg == null)
            {
                this.PressF1();
            }
            else
            {
                this._runningPg.PressF1();
            }
        }

        /// <summary>
        /// F2ボタンクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnF2_Click(object sender, EventArgs e)
        {
            if (this._runningPg == null)
            {
                this.PressF2();
            }
            else
            {
                this._runningPg.PressF2();
            }
        }

        /// <summary>
        /// F3ボタンクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnF3_Click(object sender, EventArgs e)
        {
            if (this._runningPg == null)
            {
                this.PressF3();
            }
            else
            {
                this._runningPg.PressF3();
            }
        }

        /// <summary>
        /// F4ボタンクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnF4_Click(object sender, EventArgs e)
        {
            if (this._runningPg == null)
            {
                this.PressF4();
            }
            else
            {
                this._runningPg.PressF4();
            }
        }

        /// <summary>
        /// F5ボタンクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnF5_Click(object sender, EventArgs e)
        {
            if (this._runningPg == null)
            {
                this.PressF5();
            }
            else
            {
                this._runningPg.PressF5();
            }
        }

        /// <summary>
        /// F6ボタンクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnF6_Click(object sender, EventArgs e)
        {
            if (this._runningPg == null)
            {
                this.PressF6();
            }
            else
            {
                this._runningPg.PressF6();
            }
        }

        /// <summary>
        /// F7ボタンクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnF7_Click(object sender, EventArgs e)
        {
            if (this._runningPg == null)
            {
                this.PressF7();
            }
            else
            {
                this._runningPg.PressF7();
            }
        }

        /// <summary>
        /// F8ボタンクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnF8_Click(object sender, EventArgs e)
        {
            if (this._runningPg == null)
            {
                this.PressF8();
            }
            else
            {
                this._runningPg.PressF8();
            }
        }

        /// <summary>
        /// F9ボタンクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnF9_Click(object sender, EventArgs e)
        {
            if (this._runningPg == null)
            {
                this.PressF9();
            }
            else
            {
                this._runningPg.PressF9();
            }
        }

        /// <summary>
        /// F10ボタンクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnF10_Click(object sender, EventArgs e)
        {
            if (this._runningPg == null)
            {
                this.PressF10();
            }
            else
            {
                this._runningPg.PressF10();
            }
        }

        /// <summary>
        /// F11ボタンクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnF11_Click(object sender, EventArgs e)
        {
            if (this._runningPg == null)
            {
                this.PressF11();
            }
            else
            {
                this._runningPg.PressF11();
            }
        }

        /// <summary>
        /// F12ボタンクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnF12_Click(object sender, EventArgs e)
        {
            if (this._runningPg == null)
            {
                this.PressF12();
            }
            else
            {
                this._runningPg.PressF12();
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 画面の選択状態をクリアする
        /// </summary>
        private void ClearForm()
        {
            Button btnTmp;

            // 実行中のサブシステムのインデックス・色をクリア
            this._runningSubSysIdx = 0;

            this.btnSubSys1.Image = System.Drawing.Image.FromFile(Path.Combine(IMG_DIR, IMG_SERI_DEF_NM));
            this.btnSubSys2.Image = System.Drawing.Image.FromFile(Path.Combine(IMG_DIR, IMG_KOBAI_DEF_NM));
            this.btnSubSys3.Image = System.Drawing.Image.FromFile(Path.Combine(IMG_DIR, IMG_ZAIMU_DEF_NM));
            this.btnSubSys4.Image = System.Drawing.Image.FromFile(Path.Combine(IMG_DIR, IMG_KYUYO_DEF_NM));
            this.btnSubSys5.Image = System.Drawing.Image.FromFile(Path.Combine(IMG_DIR, IMG_KYUYO_DEF_NM));

            // ファンクションキーのクリア
            for (int i = 1; i <= 12; i++)
            {
                btnTmp = (Button)this.Controls.Find("btnF" + Util.ToString(i), true)[0];
                btnTmp.Text = "F" + Util.ToString(i) + Environment.NewLine + Environment.NewLine;
                btnTmp.Enabled = false;
            }

            // 実行中の業務のインデックス・色をクリア
            this._runningGyomuIdx = 0;
            for (int i = 1; i <= 10; i++)
            {
                btnTmp = (Button)this.Controls.Find("btnGyomu" + Util.ToString(i), true)[0];
                // 現在実行中でない業務の色設定
                btnTmp.BackgroundImage = System.Drawing.Image.FromFile(Path.Combine(IMG_DIR, IMG_INACTIVE_GYOMU));
                btnTmp.Text = string.Empty;
                btnTmp.Enabled = false;
            }

            // 実行中のメニューのインデックス・色をクリア
            this._runningMenuIdx = 0;
            for (int i = 1; i <= 16; i++)
            {
                btnTmp = (Button)this.Controls.Find("btnMenu" + Util.ToString(i), true)[0];
                // 現在実行中でないメニューの色設定
                btnTmp.ForeColor = INACTIVE_FONT_COLOR;
                btnTmp.BackgroundImage = System.Drawing.Image.FromFile(Path.Combine(IMG_DIR, IMG_INACTIVE_MENU));
                btnTmp.Text = string.Empty;
                btnTmp.Enabled = false;
            }
        }

        /// <summary>
        /// サブシステムIDを取得する
        /// </summary>
        /// <returns>サブシステムID</returns>
        private string GetSubSysId()
        {
            string subSysId = string.Empty;
            switch (this._runningSubSysIdx)
            {
                case 1:
                    subSysId = "HAN";
                    break;

                case 2:
                    subSysId = "KOB";
                    break;

                case 3:
                    subSysId = "ZAM";
                    break;

                case 4:
                    subSysId = "KYU";
                    break;

                case 5:
                    subSysId = "SKD";
                    break;
            }

            return subSysId;
        }

        /// <summary>
        /// ファンクションキーを表示する
        /// </summary>
        private void DispFunctionKey()
        {
            Button btnTmp;

            string subSysId = GetSubSysId();
            DataRow drSubSysSetting = this.MInfo.LoadSubSysConf(subSysId);

            // ファンクションキーの表示
            if (drSubSysSetting != null)
            {
                for (int i = 1; i <= 12; i++)
                {
                    btnTmp = (Button)this.Controls.Find("btnF" + Util.ToString(i), true)[0];
                    btnTmp.Text = "F" + Util.ToString(i) + Environment.NewLine + Environment.NewLine + Util.ToString(drSubSysSetting["CapF" + Util.ToString(i)]);

                    // キャプションが空ならDisable
                    btnTmp.Enabled = !ValChk.IsEmpty(drSubSysSetting["CapF" + Util.ToString(i)]);
                }
            }
            else
            {
                for (int i = 1; i <= 12; i++)
                {
                    btnTmp = (Button)this.Controls.Find("btnF" + Util.ToString(i), true)[0];
                    btnTmp.Text = "F" + Util.ToString(i) + Environment.NewLine + Environment.NewLine;

                    // キャプションが空ならDisable
                    btnTmp.Enabled = false;
                }
            }
        }
        #endregion
    }
}
