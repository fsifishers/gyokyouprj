﻿namespace jp.co.fsi.common.menu
{
    partial class Menu
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Menu));
            this.pbxCompanyLogo = new System.Windows.Forms.PictureBox();
            this.btnEsc = new System.Windows.Forms.Button();
            this.btnF1 = new System.Windows.Forms.Button();
            this.btnF2 = new System.Windows.Forms.Button();
            this.btnF3 = new System.Windows.Forms.Button();
            this.btnF4 = new System.Windows.Forms.Button();
            this.btnF5 = new System.Windows.Forms.Button();
            this.btnF6 = new System.Windows.Forms.Button();
            this.btnF7 = new System.Windows.Forms.Button();
            this.btnF8 = new System.Windows.Forms.Button();
            this.btnF9 = new System.Windows.Forms.Button();
            this.btnF10 = new System.Windows.Forms.Button();
            this.btnF11 = new System.Windows.Forms.Button();
            this.btnF12 = new System.Windows.Forms.Button();
            this.lblNowDate = new System.Windows.Forms.Label();
            this.pbxCalendar = new System.Windows.Forms.PictureBox();
            this.btnLogout = new System.Windows.Forms.Button();
            this.lblAccYear = new System.Windows.Forms.Label();
            this.btnSubSys1 = new System.Windows.Forms.Button();
            this.btnSubSys2 = new System.Windows.Forms.Button();
            this.btnSubSys3 = new System.Windows.Forms.Button();
            this.btnSubSys4 = new System.Windows.Forms.Button();
            this.btnGyomu1 = new System.Windows.Forms.Button();
            this.btnGyomu2 = new System.Windows.Forms.Button();
            this.btnGyomu4 = new System.Windows.Forms.Button();
            this.btnGyomu3 = new System.Windows.Forms.Button();
            this.btnGyomu5 = new System.Windows.Forms.Button();
            this.btnGyomu10 = new System.Windows.Forms.Button();
            this.btnGyomu9 = new System.Windows.Forms.Button();
            this.btnGyomu8 = new System.Windows.Forms.Button();
            this.btnGyomu7 = new System.Windows.Forms.Button();
            this.btnGyomu6 = new System.Windows.Forms.Button();
            this.btnMenu1 = new System.Windows.Forms.Button();
            this.btnMenu2 = new System.Windows.Forms.Button();
            this.btnMenu4 = new System.Windows.Forms.Button();
            this.btnMenu3 = new System.Windows.Forms.Button();
            this.btnMenu8 = new System.Windows.Forms.Button();
            this.btnMenu7 = new System.Windows.Forms.Button();
            this.btnMenu6 = new System.Windows.Forms.Button();
            this.btnMenu5 = new System.Windows.Forms.Button();
            this.btnMenu16 = new System.Windows.Forms.Button();
            this.btnMenu15 = new System.Windows.Forms.Button();
            this.btnMenu14 = new System.Windows.Forms.Button();
            this.btnMenu13 = new System.Windows.Forms.Button();
            this.btnMenu12 = new System.Windows.Forms.Button();
            this.btnMenu11 = new System.Windows.Forms.Button();
            this.btnMenu10 = new System.Windows.Forms.Button();
            this.btnMenu9 = new System.Windows.Forms.Button();
            this.pnlMain = new jp.co.fsi.common.FsiPanel();
            this.btnSubSys5 = new System.Windows.Forms.Button();
            this.pnlSubBtn = new jp.co.fsi.common.FsiPanel();
            ((System.ComponentModel.ISupportInitialize)(this.pbxCompanyLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxCalendar)).BeginInit();
            this.pnlSubBtn.SuspendLayout();
            this.SuspendLayout();
            // 
            // pbxCompanyLogo
            // 
            this.pbxCompanyLogo.Image = ((System.Drawing.Image)(resources.GetObject("pbxCompanyLogo.Image")));
            this.pbxCompanyLogo.Location = new System.Drawing.Point(0, 0);
            this.pbxCompanyLogo.Name = "pbxCompanyLogo";
            this.pbxCompanyLogo.Size = new System.Drawing.Size(90, 60);
            this.pbxCompanyLogo.TabIndex = 1;
            this.pbxCompanyLogo.TabStop = false;
            // 
            // btnEsc
            // 
            this.btnEsc.Location = new System.Drawing.Point(5, 647);
            this.btnEsc.Name = "btnEsc";
            this.btnEsc.Size = new System.Drawing.Size(65, 44);
            this.btnEsc.TabIndex = 6;
            this.btnEsc.TabStop = false;
            this.btnEsc.Text = "Esc\r\n\r\n終了";
            this.btnEsc.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnEsc.UseVisualStyleBackColor = true;
            this.btnEsc.Click += new System.EventHandler(this.btnEsc_Click);
            // 
            // btnF1
            // 
            this.btnF1.Location = new System.Drawing.Point(95, 647);
            this.btnF1.Name = "btnF1";
            this.btnF1.Size = new System.Drawing.Size(65, 44);
            this.btnF1.TabIndex = 7;
            this.btnF1.TabStop = false;
            this.btnF1.Text = "F1\r\n\r\n検索";
            this.btnF1.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnF1.UseVisualStyleBackColor = true;
            this.btnF1.Click += new System.EventHandler(this.btnF1_Click);
            // 
            // btnF2
            // 
            this.btnF2.Location = new System.Drawing.Point(166, 647);
            this.btnF2.Name = "btnF2";
            this.btnF2.Size = new System.Drawing.Size(65, 44);
            this.btnF2.TabIndex = 8;
            this.btnF2.TabStop = false;
            this.btnF2.Text = "F2\r\n\r\n削除";
            this.btnF2.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnF2.UseVisualStyleBackColor = true;
            this.btnF2.Click += new System.EventHandler(this.btnF2_Click);
            // 
            // btnF3
            // 
            this.btnF3.Location = new System.Drawing.Point(237, 647);
            this.btnF3.Name = "btnF3";
            this.btnF3.Size = new System.Drawing.Size(65, 44);
            this.btnF3.TabIndex = 9;
            this.btnF3.TabStop = false;
            this.btnF3.Text = "F3\r\n\r\n登録";
            this.btnF3.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnF3.UseVisualStyleBackColor = true;
            this.btnF3.Click += new System.EventHandler(this.btnF3_Click);
            // 
            // btnF4
            // 
            this.btnF4.Location = new System.Drawing.Point(308, 647);
            this.btnF4.Name = "btnF4";
            this.btnF4.Size = new System.Drawing.Size(65, 44);
            this.btnF4.TabIndex = 10;
            this.btnF4.TabStop = false;
            this.btnF4.Text = "F4\r\n\r\n更新";
            this.btnF4.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnF4.UseVisualStyleBackColor = true;
            this.btnF4.Click += new System.EventHandler(this.btnF4_Click);
            // 
            // btnF5
            // 
            this.btnF5.Location = new System.Drawing.Point(400, 647);
            this.btnF5.Name = "btnF5";
            this.btnF5.Size = new System.Drawing.Size(65, 44);
            this.btnF5.TabIndex = 11;
            this.btnF5.TabStop = false;
            this.btnF5.Text = "F5\r\n\r\n印刷";
            this.btnF5.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnF5.UseVisualStyleBackColor = true;
            this.btnF5.Click += new System.EventHandler(this.btnF5_Click);
            // 
            // btnF6
            // 
            this.btnF6.Location = new System.Drawing.Point(471, 647);
            this.btnF6.Name = "btnF6";
            this.btnF6.Size = new System.Drawing.Size(65, 44);
            this.btnF6.TabIndex = 12;
            this.btnF6.TabStop = false;
            this.btnF6.Text = "F6\r\n\r\nプレビュー";
            this.btnF6.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnF6.UseVisualStyleBackColor = true;
            this.btnF6.Click += new System.EventHandler(this.btnF6_Click);
            // 
            // btnF7
            // 
            this.btnF7.Location = new System.Drawing.Point(542, 647);
            this.btnF7.Name = "btnF7";
            this.btnF7.Size = new System.Drawing.Size(65, 44);
            this.btnF7.TabIndex = 13;
            this.btnF7.TabStop = false;
            this.btnF7.Text = "F7";
            this.btnF7.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnF7.UseVisualStyleBackColor = true;
            this.btnF7.Click += new System.EventHandler(this.btnF7_Click);
            // 
            // btnF8
            // 
            this.btnF8.Location = new System.Drawing.Point(613, 647);
            this.btnF8.Name = "btnF8";
            this.btnF8.Size = new System.Drawing.Size(65, 44);
            this.btnF8.TabIndex = 14;
            this.btnF8.TabStop = false;
            this.btnF8.Text = "F8";
            this.btnF8.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnF8.UseVisualStyleBackColor = true;
            this.btnF8.Click += new System.EventHandler(this.btnF8_Click);
            // 
            // btnF9
            // 
            this.btnF9.Location = new System.Drawing.Point(708, 647);
            this.btnF9.Name = "btnF9";
            this.btnF9.Size = new System.Drawing.Size(65, 44);
            this.btnF9.TabIndex = 15;
            this.btnF9.TabStop = false;
            this.btnF9.Text = "F9";
            this.btnF9.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnF9.UseVisualStyleBackColor = true;
            this.btnF9.Click += new System.EventHandler(this.btnF9_Click);
            // 
            // btnF10
            // 
            this.btnF10.Location = new System.Drawing.Point(779, 647);
            this.btnF10.Name = "btnF10";
            this.btnF10.Size = new System.Drawing.Size(65, 44);
            this.btnF10.TabIndex = 16;
            this.btnF10.TabStop = false;
            this.btnF10.Text = "F10";
            this.btnF10.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnF10.UseVisualStyleBackColor = true;
            this.btnF10.Click += new System.EventHandler(this.btnF10_Click);
            // 
            // btnF11
            // 
            this.btnF11.Location = new System.Drawing.Point(850, 647);
            this.btnF11.Name = "btnF11";
            this.btnF11.Size = new System.Drawing.Size(65, 44);
            this.btnF11.TabIndex = 17;
            this.btnF11.TabStop = false;
            this.btnF11.Text = "F11";
            this.btnF11.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnF11.UseVisualStyleBackColor = true;
            this.btnF11.Click += new System.EventHandler(this.btnF11_Click);
            // 
            // btnF12
            // 
            this.btnF12.Location = new System.Drawing.Point(921, 647);
            this.btnF12.Name = "btnF12";
            this.btnF12.Size = new System.Drawing.Size(65, 44);
            this.btnF12.TabIndex = 18;
            this.btnF12.TabStop = false;
            this.btnF12.Text = "F12\r\n\r\nプリンタ";
            this.btnF12.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnF12.UseVisualStyleBackColor = true;
            this.btnF12.Click += new System.EventHandler(this.btnF12_Click);
            // 
            // lblNowDate
            // 
            this.lblNowDate.AutoSize = true;
            this.lblNowDate.BackColor = System.Drawing.Color.Transparent;
            this.lblNowDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblNowDate.ForeColor = System.Drawing.Color.Black;
            this.lblNowDate.Location = new System.Drawing.Point(720, 11);
            this.lblNowDate.Name = "lblNowDate";
            this.lblNowDate.Size = new System.Drawing.Size(130, 13);
            this.lblNowDate.TabIndex = 2;
            this.lblNowDate.Text = "平成25年11月28日";
            this.lblNowDate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pbxCalendar
            // 
            this.pbxCalendar.BackColor = System.Drawing.Color.Transparent;
            this.pbxCalendar.Image = ((System.Drawing.Image)(resources.GetObject("pbxCalendar.Image")));
            this.pbxCalendar.Location = new System.Drawing.Point(856, 1);
            this.pbxCalendar.Name = "pbxCalendar";
            this.pbxCalendar.Size = new System.Drawing.Size(28, 32);
            this.pbxCalendar.TabIndex = 21;
            this.pbxCalendar.TabStop = false;
            this.pbxCalendar.Click += new System.EventHandler(this.pbxCalendar_Click);
            // 
            // btnLogout
            // 
            this.btnLogout.BackColor = System.Drawing.Color.Red;
            this.btnLogout.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnLogout.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnLogout.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnLogout.Location = new System.Drawing.Point(891, 4);
            this.btnLogout.Margin = new System.Windows.Forms.Padding(0);
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.Size = new System.Drawing.Size(100, 25);
            this.btnLogout.TabIndex = 3;
            this.btnLogout.TabStop = false;
            this.btnLogout.Text = "ログアウト";
            this.btnLogout.UseVisualStyleBackColor = false;
            this.btnLogout.Click += new System.EventHandler(this.btnLogout_Click);
            // 
            // lblAccYear
            // 
            this.lblAccYear.AutoSize = true;
            this.lblAccYear.BackColor = System.Drawing.Color.Transparent;
            this.lblAccYear.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblAccYear.ForeColor = System.Drawing.Color.Blue;
            this.lblAccYear.Location = new System.Drawing.Point(730, 37);
            this.lblAccYear.Name = "lblAccYear";
            this.lblAccYear.Size = new System.Drawing.Size(252, 13);
            this.lblAccYear.TabIndex = 1;
            this.lblAccYear.Text = "第25期 (H25/ 4/ 1 ～ H26/ 3/31)";
            this.lblAccYear.Click += new System.EventHandler(this.lblAccYear_Click);
            // 
            // btnSubSys1
            // 
            this.btnSubSys1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btnSubSys1.Font = new System.Drawing.Font("ＭＳ ゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnSubSys1.ForeColor = System.Drawing.Color.Black;
            this.btnSubSys1.Image = ((System.Drawing.Image)(resources.GetObject("btnSubSys1.Image")));
            this.btnSubSys1.Location = new System.Drawing.Point(110, 3);
            this.btnSubSys1.Name = "btnSubSys1";
            this.btnSubSys1.Size = new System.Drawing.Size(110, 52);
            this.btnSubSys1.TabIndex = 4;
            this.btnSubSys1.TabStop = false;
            this.btnSubSys1.UseVisualStyleBackColor = false;
            this.btnSubSys1.Click += new System.EventHandler(this.btnSubSys_Click);
            // 
            // btnSubSys2
            // 
            this.btnSubSys2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btnSubSys2.Font = new System.Drawing.Font("ＭＳ ゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnSubSys2.Image = ((System.Drawing.Image)(resources.GetObject("btnSubSys2.Image")));
            this.btnSubSys2.Location = new System.Drawing.Point(230, 3);
            this.btnSubSys2.Name = "btnSubSys2";
            this.btnSubSys2.Size = new System.Drawing.Size(110, 52);
            this.btnSubSys2.TabIndex = 5;
            this.btnSubSys2.TabStop = false;
            this.btnSubSys2.UseVisualStyleBackColor = false;
            this.btnSubSys2.Click += new System.EventHandler(this.btnSubSys_Click);
            // 
            // btnSubSys3
            // 
            this.btnSubSys3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btnSubSys3.Font = new System.Drawing.Font("ＭＳ ゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnSubSys3.Image = ((System.Drawing.Image)(resources.GetObject("btnSubSys3.Image")));
            this.btnSubSys3.Location = new System.Drawing.Point(470, 3);
            this.btnSubSys3.Name = "btnSubSys3";
            this.btnSubSys3.Size = new System.Drawing.Size(110, 52);
            this.btnSubSys3.TabIndex = 7;
            this.btnSubSys3.TabStop = false;
            this.btnSubSys3.UseVisualStyleBackColor = false;
            this.btnSubSys3.Click += new System.EventHandler(this.btnSubSys_Click);
            // 
            // btnSubSys4
            // 
            this.btnSubSys4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btnSubSys4.Font = new System.Drawing.Font("ＭＳ ゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnSubSys4.Image = ((System.Drawing.Image)(resources.GetObject("btnSubSys4.Image")));
            this.btnSubSys4.Location = new System.Drawing.Point(590, 3);
            this.btnSubSys4.Name = "btnSubSys4";
            this.btnSubSys4.Size = new System.Drawing.Size(110, 52);
            this.btnSubSys4.TabIndex = 8;
            this.btnSubSys4.TabStop = false;
            this.btnSubSys4.UseVisualStyleBackColor = false;
            this.btnSubSys4.Click += new System.EventHandler(this.btnSubSys_Click);
            // 
            // btnGyomu1
            // 
            this.btnGyomu1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnGyomu1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnGyomu1.BackgroundImage")));
            this.btnGyomu1.Enabled = false;
            this.btnGyomu1.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnGyomu1.ForeColor = System.Drawing.Color.Black;
            this.btnGyomu1.Location = new System.Drawing.Point(0, 58);
            this.btnGyomu1.Name = "btnGyomu1";
            this.btnGyomu1.Size = new System.Drawing.Size(100, 28);
            this.btnGyomu1.TabIndex = 10;
            this.btnGyomu1.TabStop = false;
            this.btnGyomu1.UseVisualStyleBackColor = false;
            this.btnGyomu1.Click += new System.EventHandler(this.btnGyomu_Click);
            // 
            // btnGyomu2
            // 
            this.btnGyomu2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnGyomu2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnGyomu2.BackgroundImage")));
            this.btnGyomu2.Enabled = false;
            this.btnGyomu2.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnGyomu2.Location = new System.Drawing.Point(99, 58);
            this.btnGyomu2.Name = "btnGyomu2";
            this.btnGyomu2.Size = new System.Drawing.Size(100, 28);
            this.btnGyomu2.TabIndex = 11;
            this.btnGyomu2.TabStop = false;
            this.btnGyomu2.UseVisualStyleBackColor = false;
            this.btnGyomu2.Click += new System.EventHandler(this.btnGyomu_Click);
            // 
            // btnGyomu4
            // 
            this.btnGyomu4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnGyomu4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnGyomu4.BackgroundImage")));
            this.btnGyomu4.Enabled = false;
            this.btnGyomu4.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnGyomu4.Location = new System.Drawing.Point(297, 58);
            this.btnGyomu4.Name = "btnGyomu4";
            this.btnGyomu4.Size = new System.Drawing.Size(100, 28);
            this.btnGyomu4.TabIndex = 13;
            this.btnGyomu4.TabStop = false;
            this.btnGyomu4.UseVisualStyleBackColor = false;
            this.btnGyomu4.Click += new System.EventHandler(this.btnGyomu_Click);
            // 
            // btnGyomu3
            // 
            this.btnGyomu3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnGyomu3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnGyomu3.BackgroundImage")));
            this.btnGyomu3.Enabled = false;
            this.btnGyomu3.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnGyomu3.Location = new System.Drawing.Point(198, 58);
            this.btnGyomu3.Name = "btnGyomu3";
            this.btnGyomu3.Size = new System.Drawing.Size(100, 28);
            this.btnGyomu3.TabIndex = 12;
            this.btnGyomu3.TabStop = false;
            this.btnGyomu3.UseVisualStyleBackColor = false;
            this.btnGyomu3.Click += new System.EventHandler(this.btnGyomu_Click);
            // 
            // btnGyomu5
            // 
            this.btnGyomu5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnGyomu5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnGyomu5.BackgroundImage")));
            this.btnGyomu5.Enabled = false;
            this.btnGyomu5.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnGyomu5.Location = new System.Drawing.Point(396, 58);
            this.btnGyomu5.Name = "btnGyomu5";
            this.btnGyomu5.Size = new System.Drawing.Size(100, 28);
            this.btnGyomu5.TabIndex = 14;
            this.btnGyomu5.TabStop = false;
            this.btnGyomu5.UseVisualStyleBackColor = false;
            this.btnGyomu5.Click += new System.EventHandler(this.btnGyomu_Click);
            // 
            // btnGyomu10
            // 
            this.btnGyomu10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnGyomu10.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnGyomu10.BackgroundImage")));
            this.btnGyomu10.Enabled = false;
            this.btnGyomu10.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnGyomu10.Location = new System.Drawing.Point(891, 58);
            this.btnGyomu10.Name = "btnGyomu10";
            this.btnGyomu10.Size = new System.Drawing.Size(100, 28);
            this.btnGyomu10.TabIndex = 19;
            this.btnGyomu10.TabStop = false;
            this.btnGyomu10.UseVisualStyleBackColor = false;
            this.btnGyomu10.Click += new System.EventHandler(this.btnGyomu_Click);
            // 
            // btnGyomu9
            // 
            this.btnGyomu9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnGyomu9.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnGyomu9.BackgroundImage")));
            this.btnGyomu9.Enabled = false;
            this.btnGyomu9.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnGyomu9.Location = new System.Drawing.Point(792, 58);
            this.btnGyomu9.Name = "btnGyomu9";
            this.btnGyomu9.Size = new System.Drawing.Size(100, 28);
            this.btnGyomu9.TabIndex = 18;
            this.btnGyomu9.TabStop = false;
            this.btnGyomu9.UseVisualStyleBackColor = false;
            this.btnGyomu9.Click += new System.EventHandler(this.btnGyomu_Click);
            // 
            // btnGyomu8
            // 
            this.btnGyomu8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnGyomu8.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnGyomu8.BackgroundImage")));
            this.btnGyomu8.Enabled = false;
            this.btnGyomu8.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnGyomu8.Location = new System.Drawing.Point(693, 58);
            this.btnGyomu8.Name = "btnGyomu8";
            this.btnGyomu8.Size = new System.Drawing.Size(100, 28);
            this.btnGyomu8.TabIndex = 17;
            this.btnGyomu8.TabStop = false;
            this.btnGyomu8.UseVisualStyleBackColor = false;
            this.btnGyomu8.Click += new System.EventHandler(this.btnGyomu_Click);
            // 
            // btnGyomu7
            // 
            this.btnGyomu7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnGyomu7.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnGyomu7.BackgroundImage")));
            this.btnGyomu7.Enabled = false;
            this.btnGyomu7.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnGyomu7.Location = new System.Drawing.Point(594, 58);
            this.btnGyomu7.Name = "btnGyomu7";
            this.btnGyomu7.Size = new System.Drawing.Size(100, 28);
            this.btnGyomu7.TabIndex = 16;
            this.btnGyomu7.TabStop = false;
            this.btnGyomu7.UseVisualStyleBackColor = false;
            this.btnGyomu7.Click += new System.EventHandler(this.btnGyomu_Click);
            // 
            // btnGyomu6
            // 
            this.btnGyomu6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnGyomu6.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnGyomu6.BackgroundImage")));
            this.btnGyomu6.Enabled = false;
            this.btnGyomu6.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnGyomu6.Location = new System.Drawing.Point(495, 58);
            this.btnGyomu6.Name = "btnGyomu6";
            this.btnGyomu6.Size = new System.Drawing.Size(100, 28);
            this.btnGyomu6.TabIndex = 15;
            this.btnGyomu6.TabStop = false;
            this.btnGyomu6.UseVisualStyleBackColor = false;
            this.btnGyomu6.Click += new System.EventHandler(this.btnGyomu_Click);
            // 
            // btnMenu1
            // 
            this.btnMenu1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnMenu1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnMenu1.BackgroundImage")));
            this.btnMenu1.Enabled = false;
            this.btnMenu1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnMenu1.ForeColor = System.Drawing.Color.Black;
            this.btnMenu1.Location = new System.Drawing.Point(0, 0);
            this.btnMenu1.Name = "btnMenu1";
            this.btnMenu1.Size = new System.Drawing.Size(150, 36);
            this.btnMenu1.TabIndex = 20;
            this.btnMenu1.TabStop = false;
            this.btnMenu1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMenu1.UseVisualStyleBackColor = false;
            this.btnMenu1.Click += new System.EventHandler(this.btnMenu_Click);
            // 
            // btnMenu2
            // 
            this.btnMenu2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnMenu2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnMenu2.BackgroundImage")));
            this.btnMenu2.Enabled = false;
            this.btnMenu2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnMenu2.Location = new System.Drawing.Point(0, 35);
            this.btnMenu2.Name = "btnMenu2";
            this.btnMenu2.Size = new System.Drawing.Size(150, 36);
            this.btnMenu2.TabIndex = 21;
            this.btnMenu2.TabStop = false;
            this.btnMenu2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMenu2.UseVisualStyleBackColor = false;
            this.btnMenu2.Click += new System.EventHandler(this.btnMenu_Click);
            // 
            // btnMenu4
            // 
            this.btnMenu4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnMenu4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnMenu4.BackgroundImage")));
            this.btnMenu4.Enabled = false;
            this.btnMenu4.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnMenu4.Location = new System.Drawing.Point(0, 105);
            this.btnMenu4.Name = "btnMenu4";
            this.btnMenu4.Size = new System.Drawing.Size(150, 36);
            this.btnMenu4.TabIndex = 23;
            this.btnMenu4.TabStop = false;
            this.btnMenu4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMenu4.UseVisualStyleBackColor = false;
            this.btnMenu4.Click += new System.EventHandler(this.btnMenu_Click);
            // 
            // btnMenu3
            // 
            this.btnMenu3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnMenu3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnMenu3.BackgroundImage")));
            this.btnMenu3.Enabled = false;
            this.btnMenu3.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnMenu3.Location = new System.Drawing.Point(0, 70);
            this.btnMenu3.Name = "btnMenu3";
            this.btnMenu3.Size = new System.Drawing.Size(150, 36);
            this.btnMenu3.TabIndex = 22;
            this.btnMenu3.TabStop = false;
            this.btnMenu3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMenu3.UseVisualStyleBackColor = false;
            this.btnMenu3.Click += new System.EventHandler(this.btnMenu_Click);
            // 
            // btnMenu8
            // 
            this.btnMenu8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnMenu8.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnMenu8.BackgroundImage")));
            this.btnMenu8.Enabled = false;
            this.btnMenu8.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnMenu8.Location = new System.Drawing.Point(0, 245);
            this.btnMenu8.Name = "btnMenu8";
            this.btnMenu8.Size = new System.Drawing.Size(150, 36);
            this.btnMenu8.TabIndex = 27;
            this.btnMenu8.TabStop = false;
            this.btnMenu8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMenu8.UseVisualStyleBackColor = false;
            this.btnMenu8.Click += new System.EventHandler(this.btnMenu_Click);
            // 
            // btnMenu7
            // 
            this.btnMenu7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnMenu7.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnMenu7.BackgroundImage")));
            this.btnMenu7.Enabled = false;
            this.btnMenu7.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnMenu7.Location = new System.Drawing.Point(0, 210);
            this.btnMenu7.Name = "btnMenu7";
            this.btnMenu7.Size = new System.Drawing.Size(150, 36);
            this.btnMenu7.TabIndex = 26;
            this.btnMenu7.TabStop = false;
            this.btnMenu7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMenu7.UseVisualStyleBackColor = false;
            this.btnMenu7.Click += new System.EventHandler(this.btnMenu_Click);
            // 
            // btnMenu6
            // 
            this.btnMenu6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnMenu6.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnMenu6.BackgroundImage")));
            this.btnMenu6.Enabled = false;
            this.btnMenu6.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnMenu6.Location = new System.Drawing.Point(0, 175);
            this.btnMenu6.Name = "btnMenu6";
            this.btnMenu6.Size = new System.Drawing.Size(150, 36);
            this.btnMenu6.TabIndex = 25;
            this.btnMenu6.TabStop = false;
            this.btnMenu6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMenu6.UseVisualStyleBackColor = false;
            this.btnMenu6.Click += new System.EventHandler(this.btnMenu_Click);
            // 
            // btnMenu5
            // 
            this.btnMenu5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnMenu5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnMenu5.BackgroundImage")));
            this.btnMenu5.Enabled = false;
            this.btnMenu5.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnMenu5.Location = new System.Drawing.Point(0, 140);
            this.btnMenu5.Name = "btnMenu5";
            this.btnMenu5.Size = new System.Drawing.Size(150, 36);
            this.btnMenu5.TabIndex = 24;
            this.btnMenu5.TabStop = false;
            this.btnMenu5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMenu5.UseVisualStyleBackColor = false;
            this.btnMenu5.Click += new System.EventHandler(this.btnMenu_Click);
            // 
            // btnMenu16
            // 
            this.btnMenu16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnMenu16.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnMenu16.BackgroundImage")));
            this.btnMenu16.Enabled = false;
            this.btnMenu16.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnMenu16.Location = new System.Drawing.Point(0, 525);
            this.btnMenu16.Name = "btnMenu16";
            this.btnMenu16.Size = new System.Drawing.Size(150, 36);
            this.btnMenu16.TabIndex = 35;
            this.btnMenu16.TabStop = false;
            this.btnMenu16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMenu16.UseVisualStyleBackColor = false;
            this.btnMenu16.Click += new System.EventHandler(this.btnMenu_Click);
            // 
            // btnMenu15
            // 
            this.btnMenu15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnMenu15.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnMenu15.BackgroundImage")));
            this.btnMenu15.Enabled = false;
            this.btnMenu15.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnMenu15.Location = new System.Drawing.Point(0, 490);
            this.btnMenu15.Name = "btnMenu15";
            this.btnMenu15.Size = new System.Drawing.Size(150, 36);
            this.btnMenu15.TabIndex = 34;
            this.btnMenu15.TabStop = false;
            this.btnMenu15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMenu15.UseVisualStyleBackColor = false;
            this.btnMenu15.Click += new System.EventHandler(this.btnMenu_Click);
            // 
            // btnMenu14
            // 
            this.btnMenu14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnMenu14.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnMenu14.BackgroundImage")));
            this.btnMenu14.Enabled = false;
            this.btnMenu14.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnMenu14.Location = new System.Drawing.Point(0, 455);
            this.btnMenu14.Name = "btnMenu14";
            this.btnMenu14.Size = new System.Drawing.Size(150, 36);
            this.btnMenu14.TabIndex = 33;
            this.btnMenu14.TabStop = false;
            this.btnMenu14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMenu14.UseVisualStyleBackColor = false;
            this.btnMenu14.Click += new System.EventHandler(this.btnMenu_Click);
            // 
            // btnMenu13
            // 
            this.btnMenu13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnMenu13.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnMenu13.BackgroundImage")));
            this.btnMenu13.Enabled = false;
            this.btnMenu13.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnMenu13.Location = new System.Drawing.Point(0, 420);
            this.btnMenu13.Name = "btnMenu13";
            this.btnMenu13.Size = new System.Drawing.Size(150, 36);
            this.btnMenu13.TabIndex = 32;
            this.btnMenu13.TabStop = false;
            this.btnMenu13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMenu13.UseVisualStyleBackColor = false;
            this.btnMenu13.Click += new System.EventHandler(this.btnMenu_Click);
            // 
            // btnMenu12
            // 
            this.btnMenu12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnMenu12.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnMenu12.BackgroundImage")));
            this.btnMenu12.Enabled = false;
            this.btnMenu12.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnMenu12.Location = new System.Drawing.Point(0, 385);
            this.btnMenu12.Name = "btnMenu12";
            this.btnMenu12.Size = new System.Drawing.Size(150, 36);
            this.btnMenu12.TabIndex = 31;
            this.btnMenu12.TabStop = false;
            this.btnMenu12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMenu12.UseVisualStyleBackColor = false;
            this.btnMenu12.Click += new System.EventHandler(this.btnMenu_Click);
            // 
            // btnMenu11
            // 
            this.btnMenu11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnMenu11.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnMenu11.BackgroundImage")));
            this.btnMenu11.Enabled = false;
            this.btnMenu11.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnMenu11.Location = new System.Drawing.Point(0, 350);
            this.btnMenu11.Name = "btnMenu11";
            this.btnMenu11.Size = new System.Drawing.Size(150, 36);
            this.btnMenu11.TabIndex = 30;
            this.btnMenu11.TabStop = false;
            this.btnMenu11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMenu11.UseVisualStyleBackColor = false;
            this.btnMenu11.Click += new System.EventHandler(this.btnMenu_Click);
            // 
            // btnMenu10
            // 
            this.btnMenu10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnMenu10.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnMenu10.BackgroundImage")));
            this.btnMenu10.Enabled = false;
            this.btnMenu10.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnMenu10.Location = new System.Drawing.Point(0, 315);
            this.btnMenu10.Name = "btnMenu10";
            this.btnMenu10.Size = new System.Drawing.Size(150, 36);
            this.btnMenu10.TabIndex = 29;
            this.btnMenu10.TabStop = false;
            this.btnMenu10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMenu10.UseVisualStyleBackColor = false;
            this.btnMenu10.Click += new System.EventHandler(this.btnMenu_Click);
            // 
            // btnMenu9
            // 
            this.btnMenu9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnMenu9.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnMenu9.BackgroundImage")));
            this.btnMenu9.Enabled = false;
            this.btnMenu9.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnMenu9.Location = new System.Drawing.Point(0, 280);
            this.btnMenu9.Name = "btnMenu9";
            this.btnMenu9.Size = new System.Drawing.Size(150, 36);
            this.btnMenu9.TabIndex = 28;
            this.btnMenu9.TabStop = false;
            this.btnMenu9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMenu9.UseVisualStyleBackColor = false;
            this.btnMenu9.Click += new System.EventHandler(this.btnMenu_Click);
            // 
            // pnlMain
            // 
            this.pnlMain.BackColor = System.Drawing.Color.Transparent;
            this.pnlMain.Location = new System.Drawing.Point(0, 85);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(990, 560);
            this.pnlMain.TabIndex = 36;
            // 
            // btnSubSys5
            // 
            this.btnSubSys5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btnSubSys5.Font = new System.Drawing.Font("ＭＳ ゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnSubSys5.Image = ((System.Drawing.Image)(resources.GetObject("btnSubSys5.Image")));
            this.btnSubSys5.Location = new System.Drawing.Point(350, 3);
            this.btnSubSys5.Name = "btnSubSys5";
            this.btnSubSys5.Size = new System.Drawing.Size(110, 52);
            this.btnSubSys5.TabIndex = 6;
            this.btnSubSys5.TabStop = false;
            this.btnSubSys5.UseVisualStyleBackColor = false;
            this.btnSubSys5.Click += new System.EventHandler(this.btnSubSys_Click);
            // 
            // pnlSubBtn
            // 
            this.pnlSubBtn.Controls.Add(this.btnMenu1);
            this.pnlSubBtn.Controls.Add(this.btnMenu16);
            this.pnlSubBtn.Controls.Add(this.btnMenu2);
            this.pnlSubBtn.Controls.Add(this.btnMenu15);
            this.pnlSubBtn.Controls.Add(this.btnMenu3);
            this.pnlSubBtn.Controls.Add(this.btnMenu14);
            this.pnlSubBtn.Controls.Add(this.btnMenu4);
            this.pnlSubBtn.Controls.Add(this.btnMenu13);
            this.pnlSubBtn.Controls.Add(this.btnMenu5);
            this.pnlSubBtn.Controls.Add(this.btnMenu12);
            this.pnlSubBtn.Controls.Add(this.btnMenu6);
            this.pnlSubBtn.Controls.Add(this.btnMenu11);
            this.pnlSubBtn.Controls.Add(this.btnMenu7);
            this.pnlSubBtn.Controls.Add(this.btnMenu10);
            this.pnlSubBtn.Controls.Add(this.btnMenu8);
            this.pnlSubBtn.Controls.Add(this.btnMenu9);
            this.pnlSubBtn.Location = new System.Drawing.Point(0, 83);
            this.pnlSubBtn.Name = "pnlSubBtn";
            this.pnlSubBtn.Size = new System.Drawing.Size(149, 561);
            this.pnlSubBtn.TabIndex = 0;
            // 
            // Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SkyBlue;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(994, 693);
            this.Controls.Add(this.pnlSubBtn);
            this.Controls.Add(this.btnSubSys5);
            this.Controls.Add(this.btnGyomu10);
            this.Controls.Add(this.btnGyomu9);
            this.Controls.Add(this.btnGyomu8);
            this.Controls.Add(this.btnGyomu7);
            this.Controls.Add(this.btnGyomu6);
            this.Controls.Add(this.btnGyomu5);
            this.Controls.Add(this.btnGyomu4);
            this.Controls.Add(this.btnGyomu3);
            this.Controls.Add(this.btnGyomu2);
            this.Controls.Add(this.btnGyomu1);
            this.Controls.Add(this.btnSubSys4);
            this.Controls.Add(this.btnSubSys3);
            this.Controls.Add(this.btnSubSys2);
            this.Controls.Add(this.btnSubSys1);
            this.Controls.Add(this.pnlMain);
            this.Controls.Add(this.lblAccYear);
            this.Controls.Add(this.btnLogout);
            this.Controls.Add(this.pbxCalendar);
            this.Controls.Add(this.lblNowDate);
            this.Controls.Add(this.btnF12);
            this.Controls.Add(this.btnF11);
            this.Controls.Add(this.btnF10);
            this.Controls.Add(this.btnF9);
            this.Controls.Add(this.btnF8);
            this.Controls.Add(this.btnF7);
            this.Controls.Add(this.btnF6);
            this.Controls.Add(this.btnF5);
            this.Controls.Add(this.btnF4);
            this.Controls.Add(this.btnF3);
            this.Controls.Add(this.btnF2);
            this.Controls.Add(this.btnF1);
            this.Controls.Add(this.btnEsc);
            this.Controls.Add(this.pbxCompanyLogo);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Menu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ERPシステム";
            ((System.ComponentModel.ISupportInitialize)(this.pbxCompanyLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxCalendar)).EndInit();
            this.pnlSubBtn.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbxCompanyLogo;
        private System.Windows.Forms.Button btnEsc;
        private System.Windows.Forms.Button btnF1;
        private System.Windows.Forms.Button btnF2;
        private System.Windows.Forms.Button btnF3;
        private System.Windows.Forms.Button btnF4;
        private System.Windows.Forms.Button btnF5;
        private System.Windows.Forms.Button btnF6;
        private System.Windows.Forms.Button btnF7;
        private System.Windows.Forms.Button btnF8;
        private System.Windows.Forms.Button btnF9;
        private System.Windows.Forms.Button btnF10;
        private System.Windows.Forms.Button btnF11;
        private System.Windows.Forms.Button btnF12;
        private System.Windows.Forms.Label lblNowDate;
        private System.Windows.Forms.PictureBox pbxCalendar;
        private System.Windows.Forms.Button btnLogout;
        private System.Windows.Forms.Label lblAccYear;
        private System.Windows.Forms.Button btnSubSys1;
        private System.Windows.Forms.Button btnSubSys2;
        private System.Windows.Forms.Button btnSubSys3;
        private System.Windows.Forms.Button btnSubSys4;
        private System.Windows.Forms.Button btnGyomu1;
        private System.Windows.Forms.Button btnGyomu2;
        private System.Windows.Forms.Button btnGyomu4;
        private System.Windows.Forms.Button btnGyomu3;
        private System.Windows.Forms.Button btnGyomu5;
        private System.Windows.Forms.Button btnGyomu10;
        private System.Windows.Forms.Button btnGyomu9;
        private System.Windows.Forms.Button btnGyomu8;
        private System.Windows.Forms.Button btnGyomu7;
        private System.Windows.Forms.Button btnGyomu6;
        private System.Windows.Forms.Button btnMenu1;
        private System.Windows.Forms.Button btnMenu2;
        private System.Windows.Forms.Button btnMenu4;
        private System.Windows.Forms.Button btnMenu3;
        private System.Windows.Forms.Button btnMenu8;
        private System.Windows.Forms.Button btnMenu7;
        private System.Windows.Forms.Button btnMenu6;
        private System.Windows.Forms.Button btnMenu5;
        private System.Windows.Forms.Button btnMenu16;
        private System.Windows.Forms.Button btnMenu15;
        private System.Windows.Forms.Button btnMenu14;
        private System.Windows.Forms.Button btnMenu13;
        private System.Windows.Forms.Button btnMenu12;
        private System.Windows.Forms.Button btnMenu11;
        private System.Windows.Forms.Button btnMenu10;
        private System.Windows.Forms.Button btnMenu9;
        private jp.co.fsi.common.FsiPanel pnlMain;
        private System.Windows.Forms.Button btnSubSys5;
        private jp.co.fsi.common.FsiPanel pnlSubBtn;
    }
}

