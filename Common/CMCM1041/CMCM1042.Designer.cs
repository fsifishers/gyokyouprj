﻿namespace jp.co.fsi.cm.cmcm1041
{
    partial class CMCM1042
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnEnter = new System.Windows.Forms.Button();
            this.txtTitle01 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblTitle01 = new System.Windows.Forms.Label();
            this.txtTitle02 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblTitle02 = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Size = new System.Drawing.Size(363, 23);
            this.lblTitle.TabIndex = 999;
            // 
            // btnEsc
            // 
            this.btnEsc.Location = new System.Drawing.Point(5, 3);
            // 
            // btnF1
            // 
            this.btnF1.Location = new System.Drawing.Point(5, 3);
            this.btnF1.Visible = false;
            // 
            // btnF2
            // 
            this.btnF2.Location = new System.Drawing.Point(69, 3);
            this.btnF2.Visible = false;
            // 
            // btnF3
            // 
            this.btnF3.Location = new System.Drawing.Point(133, 3);
            this.btnF3.Visible = false;
            // 
            // btnF4
            // 
            this.btnF4.Location = new System.Drawing.Point(197, 3);
            this.btnF4.Visible = false;
            // 
            // btnF5
            // 
            this.btnF5.Location = new System.Drawing.Point(261, 3);
            this.btnF5.Visible = false;
            // 
            // btnF7
            // 
            this.btnF7.Location = new System.Drawing.Point(389, 3);
            this.btnF7.Visible = false;
            // 
            // btnF6
            // 
            this.btnF6.Location = new System.Drawing.Point(325, 3);
            this.btnF6.Visible = false;
            // 
            // btnF8
            // 
            this.btnF8.Location = new System.Drawing.Point(453, 3);
            this.btnF8.Visible = false;
            // 
            // btnF9
            // 
            this.btnF9.Location = new System.Drawing.Point(517, 3);
            this.btnF9.Visible = false;
            // 
            // btnF12
            // 
            this.btnF12.Location = new System.Drawing.Point(709, 3);
            this.btnF12.Visible = false;
            // 
            // btnF11
            // 
            this.btnF11.Location = new System.Drawing.Point(645, 3);
            this.btnF11.Visible = false;
            // 
            // btnF10
            // 
            this.btnF10.Location = new System.Drawing.Point(581, 3);
            this.btnF10.Visible = false;
            // 
            // pnlDebug
            // 
            this.pnlDebug.Controls.Add(this.btnEnter);
            this.pnlDebug.Location = new System.Drawing.Point(12, 82);
            this.pnlDebug.Size = new System.Drawing.Size(364, 55);
            this.pnlDebug.Controls.SetChildIndex(this.btnEnter, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF6, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF7, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF5, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF8, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF4, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF9, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF3, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF10, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF2, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF11, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF1, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF12, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnEsc, 0);
            // 
            // btnEnter
            // 
            this.btnEnter.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnEnter.Location = new System.Drawing.Point(69, 3);
            this.btnEnter.Name = "btnEnter";
            this.btnEnter.Size = new System.Drawing.Size(65, 45);
            this.btnEnter.TabIndex = 903;
            this.btnEnter.TabStop = false;
            this.btnEnter.Text = "Enter\r\n\r\n決定";
            this.btnEnter.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnEnter.UseVisualStyleBackColor = true;
            // 
            // txtTitle01
            // 
            this.txtTitle01.AllowDrop = true;
            this.txtTitle01.AutoSizeFromLength = false;
            this.txtTitle01.DisplayLength = null;
            this.txtTitle01.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtTitle01.Location = new System.Drawing.Point(116, 13);
            this.txtTitle01.MaxLength = 4;
            this.txtTitle01.Name = "txtTitle01";
            this.txtTitle01.Size = new System.Drawing.Size(39, 20);
            this.txtTitle01.TabIndex = 1;
            this.txtTitle01.Text = "0";
            this.txtTitle01.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTitle01.Validating += new System.ComponentModel.CancelEventHandler(this.txtTitle01_Validating);
            // 
            // lblTitle01
            // 
            this.lblTitle01.BackColor = System.Drawing.Color.Silver;
            this.lblTitle01.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTitle01.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblTitle01.Location = new System.Drawing.Point(12, 11);
            this.lblTitle01.Name = "lblTitle01";
            this.lblTitle01.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.lblTitle01.Size = new System.Drawing.Size(147, 25);
            this.lblTitle01.TabIndex = 0;
            this.lblTitle01.Text = "商品区分";
            this.lblTitle01.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTitle02
            // 
            this.txtTitle02.AllowDrop = true;
            this.txtTitle02.AutoSizeFromLength = false;
            this.txtTitle02.DisplayLength = null;
            this.txtTitle02.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtTitle02.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtTitle02.Location = new System.Drawing.Point(116, 47);
            this.txtTitle02.MaxLength = 15;
            this.txtTitle02.Name = "txtTitle02";
            this.txtTitle02.Size = new System.Drawing.Size(232, 20);
            this.txtTitle02.TabIndex = 3;
            // 
            // lblTitle02
            // 
            this.lblTitle02.BackColor = System.Drawing.Color.Silver;
            this.lblTitle02.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTitle02.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblTitle02.Location = new System.Drawing.Point(12, 45);
            this.lblTitle02.Name = "lblTitle02";
            this.lblTitle02.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.lblTitle02.Size = new System.Drawing.Size(341, 25);
            this.lblTitle02.TabIndex = 2;
            this.lblTitle02.Text = "商品区分4名称";
            this.lblTitle02.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // CMCM1042
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(388, 149);
            this.Controls.Add(this.txtTitle01);
            this.Controls.Add(this.lblTitle01);
            this.Controls.Add(this.txtTitle02);
            this.Controls.Add(this.lblTitle02);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.MinimizeBox = false;
            this.Name = "CMCM1042";
            this.ShowFButton = true;
            this.ShowTitle = false;
            this.Text = "商品区分";
            this.Load += new System.EventHandler(this.CMCM1042_Load);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle02, 0);
            this.Controls.SetChildIndex(this.txtTitle02, 0);
            this.Controls.SetChildIndex(this.lblTitle01, 0);
            this.Controls.SetChildIndex(this.txtTitle01, 0);
            this.pnlDebug.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        protected System.Windows.Forms.Button btnEnter;
        private common.controls.FsiTextBox txtTitle01;
        private System.Windows.Forms.Label lblTitle01;
        private common.controls.FsiTextBox txtTitle02;
        private System.Windows.Forms.Label lblTitle02;
    }
}
