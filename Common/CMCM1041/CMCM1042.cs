﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.cm.cmcm1041
{
    /// <summary>
    /// コード登録削除画面(CMCM1042)
    /// </summary>
    public partial class CMCM1042 : BasePgForm
    {
        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public CMCM1042()
        {
            InitializeComponent();
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="par1">引数1</param>
        public CMCM1042(string par1, string par2)
            : base(par1, par2)
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.InitForm();は呼び出さなくて構いません。
        /// また、このメソッド内の処理を外出しでこのクラス内にメソッド化するのは構いませんが、
        /// 原則、独自で起動時のイベント処理を実装することは禁じます。
        /// </remarks>
        protected override void InitForm()
        {
            // タイトルは非表示
            this.lblTitle.Visible = false;
            // Fキーボタンを制御
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF11.Location = this.btnF2.Location;
            this.btnF3.Location = this.btnF3.Location;
            this.btnF6.Location = this.btnF4.Location;
            this.btnEsc.Visible = true;
            this.btnF1.Visible = false;
            this.btnF2.Visible = false;
            this.btnF3.Visible = true;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF6.Visible = true;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = true;
            this.btnF12.Visible = false;
            this.btnF11.Enabled = false; // 現状常に非活性
            this.btnF11.Text = "F11\r\n\r\n検索";
            this.btnF3.Text = "F3\r\n\r\n削除";
            this.btnF6.Text = "F6\r\n\r\n登録";
            // 画面名を設定
            this.Text = this.Par1;
            // 項目名を設定
            this.lblTitle01.Text = this.Par1;
            this.lblTitle02.Text = this.Par1 + "名称";
            // F3ボタンを制御
            DataTable dtCheck = this.IsValidtxtTitle01UmuCheck();
            if (dtCheck.Rows.Count == 0)
            {
                this.btnF3.Enabled = false;
            }
            else
            {
                this.btnF3.Enabled = true;
            }

            // フォーカスを設定
            this.txtTitle01.Focus();
        }

        /// <summary>
        /// F3キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF3();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF3()
        {
            // フォーカスをF3キーに当てる
            this.btnF3.Focus();

            // F3ボタンが非活性の場合は処理を行わない
            if (!this.btnF3.Enabled)
            {
                return;
            }

            // 入力チェック
            if (!this.IsValidtxtTitle01DeleteCheck())
            {
                return;
            }

            if (Msg.ConfYesNo("削除しますか？") == DialogResult.No)
            {
                return;
            }

            try
            {
                this.DeleteData();
                Msg.Info("削除しました。");
            }
            catch (Exception e)
            {
                Msg.Info("削除処理に失敗しました。");
                // ロールバック
                this.Dba.Rollback();
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF6();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF6()
        {
            // 入力チェック
            if (!this.IsValidtxtTitle01Check())
            {
                return;
            }
            
            // 新規登録か更新かをチェック
            String str;
            bool flg;
            if (this.IsValidtxtTitle01InsertCheck())
            {
                str = "更新";
                flg = false;
            }
            else
            {
                str = "登録";
                flg = true;
            }

            if (Msg.ConfYesNo(str + "しますか？") == DialogResult.No)
            {
                return;
            }

            try
            {
                if (flg)
                {
                    // 登録処理
                    this.InsertData();
                }
                else
                {
                    // 更新処理
                    this.UpdateData();
                }
                Msg.Info(str + "しました。");
            }
            catch (Exception e)
            {
                Msg.Info(str + "処理に失敗しました。");
                // ロールバック
                this.Dba.Rollback();
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 区分の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTitle01_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidtxtTitle01())
            {
                e.Cancel = true;
                this.txtTitle01.SelectAll();
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 区分の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidtxtTitle01()
        {
            // 入力チェック
            if (!this.IsValidtxtTitle01Check())
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            DataTable dtCheck = this.IsValidtxtTitle01UmuCheck();

            if (dtCheck.Rows.Count == 0)
            {
                this.btnF3.Enabled = false;
                this.txtTitle02.Text = "";
            }
            else
            {
                this.btnF3.Enabled = true;
                this.txtTitle02.Text = dtCheck.Rows[0]["SHOHIN_KUBUN_NM"].ToString();
            }

            return true;
        }

        /// <summary>
        /// 区分の削除時チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidtxtTitle01DeleteCheck()
        {
            // 入力チェック
            if (!this.IsValidtxtTitle01Check())
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            DataTable dtCheck = this.IsValidtxtTitle01UmuCheck();

            if (dtCheck.Rows.Count > 0)
            {
                this.txtTitle02.Text = dtCheck.Rows[0]["SHOHIN_KUBUN_NM"].ToString();
            }

            return true;
        }

        /// <summary>
        /// 区分の登録時チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidtxtTitle01InsertCheck()
        {
            DataTable dtCheck = this.IsValidtxtTitle01UmuCheck();

            if (dtCheck.Rows.Count == 0)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// 区分のチェック(共通)
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidtxtTitle01Check()
        {
            // 未入力はエラーとする
            if (ValChk.IsEmpty(this.txtTitle01.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtTitle01.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }
        private DataTable IsValidtxtTitle01UmuCheck(bool titleDisp = false)
        {
            int kubunShubetsu = 0;
            int shohinKubun = int.Parse(this.txtTitle01.Text);
            if (int.Parse(this.txtTitle01.Text) > 0)
            {
                kubunShubetsu = int.Parse(this.Par2);
                shohinKubun = int.Parse(this.txtTitle01.Text);
            }

            if (titleDisp)
            {
                if (int.Parse(this.txtTitle01.Text) == 0)
                {
                    kubunShubetsu = int.Parse(this.Par2);
                    shohinKubun = int.Parse(this.txtTitle01.Text);
                }
            }

            // 既に存在するコードを入力した場合は、名称を表示する
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KUBUN_SHUBETSU", SqlDbType.Decimal, 2, kubunShubetsu);
            dpc.SetParam("@SHOHIN_KUBUN", SqlDbType.VarChar, 4, shohinKubun);
            StringBuilder where = new StringBuilder("KUBUN_SHUBETSU = @KUBUN_SHUBETSU");
            where.Append(" AND SHOHIN_KUBUN = @SHOHIN_KUBUN");
            DataTable dtCheck =
                this.Dba.GetDataTableByConditionWithParams("SHOHIN_KUBUN_NM",
                    "TB_HN_F_SHOHIN_KUBUN", Util.ToString(where), dpc);

            return dtCheck;
        }

        /// <summary>
        /// 削除処理
        /// </summary>
        private void DeleteData()
        {
            int kubunShubetsu = 0;
            int shohinKubun = int.Parse(this.txtTitle01.Text);
            if (int.Parse(this.txtTitle01.Text) > 0)
            {
                kubunShubetsu = int.Parse(this.Par2);
                shohinKubun = int.Parse(this.txtTitle01.Text);
            }

            // トランザクションの開始
            this.Dba.BeginTransaction();

            DbParamCollection whereParam;
            whereParam = new DbParamCollection();

            whereParam.SetParam("@KUBUN_SHUBETSU", SqlDbType.Decimal, 2, kubunShubetsu);
            whereParam.SetParam("@SHOHIN_KUBUN", SqlDbType.Decimal, 4, shohinKubun);
            this.Dba.Delete("TB_HN_F_SHOHIN_KUBUN",
                "KUBUN_SHUBETSU = @KUBUN_SHUBETSU AND SHOHIN_KUBUN = @SHOHIN_KUBUN",
                whereParam);

            // トランザクションをコミット
            this.Dba.Commit();
        }

        /// <summary>
        /// 登録処理
        /// </summary>
        private void InsertData()
        {
            int kubunShubetsu = 0;
            int shohinKubun = int.Parse(this.txtTitle01.Text);
            if (int.Parse(this.txtTitle01.Text) > 0)
            {
                kubunShubetsu = int.Parse(this.Par2);
                shohinKubun = int.Parse(this.txtTitle01.Text);
            }

            // トランザクション開始
            this.Dba.BeginTransaction();

            DbParamCollection updParam = new DbParamCollection();

            updParam.SetParam("@KUBUN_SHUBETSU", SqlDbType.Decimal, 2, kubunShubetsu);
            updParam.SetParam("@SHOHIN_KUBUN", SqlDbType.Decimal, 4, shohinKubun);
            updParam.SetParam("@SHOHIN_KUBUN_NM", SqlDbType.VarChar, 30, this.txtTitle02.Text);

            this.Dba.Insert("TB_HN_F_SHOHIN_KUBUN", updParam);

            // トランザクションをコミット
            this.Dba.Commit();
        }

        /// <summary>
        /// 更新処理
        /// </summary>
        private void UpdateData()
        {
            int kubunShubetsu = 0;
            int shohinKubun = int.Parse(this.txtTitle01.Text);
            if (int.Parse(this.txtTitle01.Text) > 0)
            {
                kubunShubetsu = int.Parse(this.Par2);
                shohinKubun = int.Parse(this.txtTitle01.Text);
            }

            // トランザクション開始
            this.Dba.BeginTransaction();

            DbParamCollection updParam = new DbParamCollection();
            DbParamCollection whereParam = new DbParamCollection();

            updParam.SetParam("@SHOHIN_KUBUN_NM", SqlDbType.VarChar, 30, this.txtTitle02.Text);

            whereParam.SetParam("@KUBUN_SHUBETSU", SqlDbType.Decimal, 2, kubunShubetsu);
            whereParam.SetParam("@SHOHIN_KUBUN", SqlDbType.Int, 4, shohinKubun);

            this.Dba.Update("TB_HN_F_SHOHIN_KUBUN", updParam,
                            "KUBUN_SHUBETSU = @KUBUN_SHUBETSU AND SHOHIN_KUBUN = @SHOHIN_KUBUN", whereParam);

            // トランザクションをコミット
            this.Dba.Commit();
        }
        #endregion

        private void CMCM1042_Load(object sender, EventArgs e)
        {

        }
    }
}
