﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;
using System.Windows.Forms;

using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.common.menu
{
    /// <summary>
    /// メニュー画面です。
    /// </summary>
    public partial class Menu : BaseForm
    {
        #region private変数
        /// <summary>
        /// プログラムの設定情報
        /// </summary>
        private DataTable _dtPgConf;

        /// <summary>
        /// 現在開いている子フォームのパス
        /// </summary>
        private string _curChildForm;
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public Menu()
        {
            InitializeComponent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理です。
        /// </summary>
        protected override void InitForm()
        {
            // タイトルの表示
            this.Text = this.Config.LoadCommonConfig("UserInfo", "kaishanm")
                + " " + this.Config.LoadCommonConfig("UserInfo", "usernm") + " 様";

            // TODO:仮実装


            //TODO:設定ファイルに従ってタブの動きを設定

            //TabControlをオーナードローする
            this.tctMain1.DrawMode = TabDrawMode.OwnerDrawFixed;
            this.tctMain3.DrawMode = TabDrawMode.OwnerDrawFixed;
            //DrawItemイベントハンドラを追加
            this.tctMain1.DrawItem += new DrawItemEventHandler(tctMain_DrawItem);
            this.tctMain3.DrawItem += new DrawItemEventHandler(tctMain_DrawItem);
        }
        #endregion

        #region イベント
        /// <summary>
        /// サブシステム-業務種別の中のメインタブ描画イベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tctMain_DrawItem(object sender, DrawItemEventArgs e)
        {
            TabControl tab = (TabControl)sender;
            TabPage page = tab.TabPages[e.Index];
            string txt = page.Text;

            StringFormat sf = new StringFormat();
            sf.LineAlignment = StringAlignment.Center;
            sf.FormatFlags |= StringFormatFlags.LineLimit;

            Brush foreBrush = new SolidBrush(page.ForeColor);
            e.Graphics.DrawString(txt, page.Font, foreBrush, e.Bounds, sf);
            foreBrush.Dispose();
        }

        /// <summary>
        /// カレンダーアイコンクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pbxCalendar_Click(object sender, EventArgs e)
        {
            Msg.Info("TODO:カレンダー画面を起動");
        }

        /// <summary>
        /// ログアウトボタンクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnLogout_Click(object sender, EventArgs e)
        {
            // 画面を閉じる
            this.Close();
        }

        /// <summary>
        /// サブシステムのタブが変更された後の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tctSubSystem_SelectedIndexChanged(object sender, EventArgs e)
        {
            // TODO:仮実装
            Msg.Info("サブシステムが変更されました。" + Environment.NewLine
                + "SelectedIndex：" + Util.ToString(this.tctSubSystem.SelectedIndex)
                + "TODO:業務タブ・メインタブを再描画する実装をして下さい。");
        }

        /// <summary>
        /// 会計期間クリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lblAccYear_Click(object sender, EventArgs e)
        {
            Msg.Info("TODO:期間選択画面を表示");
        }
        #endregion
    }
}
