﻿namespace jp.co.fsi.common.menu
{
    partial class Menu
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Menu));
            this.pbxCompanyLogo = new System.Windows.Forms.PictureBox();
            this.lblSystemName = new System.Windows.Forms.Label();
            this.tctSubSystem = new System.Windows.Forms.TabControl();
            this.tpgSubSystem1 = new System.Windows.Forms.TabPage();
            this.tpgSubSystem2 = new System.Windows.Forms.TabPage();
            this.tpgSubSystem3 = new System.Windows.Forms.TabPage();
            this.panel2 = new jp.co.fsi.common.FsiPanel();
            this.monthCalendar = new System.Windows.Forms.MonthCalendar();
            this.label5 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.button26 = new System.Windows.Forms.Button();
            this.button27 = new System.Windows.Forms.Button();
            this.button28 = new System.Windows.Forms.Button();
            this.button29 = new System.Windows.Forms.Button();
            this.button30 = new System.Windows.Forms.Button();
            this.button31 = new System.Windows.Forms.Button();
            this.button32 = new System.Windows.Forms.Button();
            this.button33 = new System.Windows.Forms.Button();
            this.button34 = new System.Windows.Forms.Button();
            this.button35 = new System.Windows.Forms.Button();
            this.button36 = new System.Windows.Forms.Button();
            this.button37 = new System.Windows.Forms.Button();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.tabPage9 = new System.Windows.Forms.TabPage();
            this.tabPage10 = new System.Windows.Forms.TabPage();
            this.tabPage11 = new System.Windows.Forms.TabPage();
            this.tabPage12 = new System.Windows.Forms.TabPage();
            this.tpgSubSystem4 = new System.Windows.Forms.TabPage();
            this.tpgSubSystem5 = new System.Windows.Forms.TabPage();
            this.tpgSubSystem6 = new System.Windows.Forms.TabPage();
            this.tctGyomu = new System.Windows.Forms.TabControl();
            this.tpgGyomu1 = new System.Windows.Forms.TabPage();
            this.tctMain1 = new System.Windows.Forms.TabControl();
            this.tpgMain1_01 = new System.Windows.Forms.TabPage();
            this.tpgMain1_02 = new System.Windows.Forms.TabPage();
            this.tpgMain1_03 = new System.Windows.Forms.TabPage();
            this.tpgMain1_04 = new System.Windows.Forms.TabPage();
            this.tpgMain1_05 = new System.Windows.Forms.TabPage();
            this.tpgMain1_06 = new System.Windows.Forms.TabPage();
            this.tpgMain1_07 = new System.Windows.Forms.TabPage();
            this.tpgMain1_08 = new System.Windows.Forms.TabPage();
            this.tpgMain1_09 = new System.Windows.Forms.TabPage();
            this.tpgMain1_10 = new System.Windows.Forms.TabPage();
            this.tpgMain1_11 = new System.Windows.Forms.TabPage();
            this.tpgMain1_12 = new System.Windows.Forms.TabPage();
            this.tpgMain1_13 = new System.Windows.Forms.TabPage();
            this.tpgMain1_14 = new System.Windows.Forms.TabPage();
            this.tpgMain1_15 = new System.Windows.Forms.TabPage();
            this.tpgMain1_16 = new System.Windows.Forms.TabPage();
            this.tpgGyomu2 = new System.Windows.Forms.TabPage();
            this.tctMain2 = new System.Windows.Forms.TabControl();
            this.tpgMain2_01 = new System.Windows.Forms.TabPage();
            this.tpgMain2_02 = new System.Windows.Forms.TabPage();
            this.tpgMain2_03 = new System.Windows.Forms.TabPage();
            this.tpgMain2_04 = new System.Windows.Forms.TabPage();
            this.tpgMain2_05 = new System.Windows.Forms.TabPage();
            this.tpgMain2_06 = new System.Windows.Forms.TabPage();
            this.tpgMain2_07 = new System.Windows.Forms.TabPage();
            this.tpgMain2_08 = new System.Windows.Forms.TabPage();
            this.tpgMain2_09 = new System.Windows.Forms.TabPage();
            this.tpgMain2_10 = new System.Windows.Forms.TabPage();
            this.tpgMain2_11 = new System.Windows.Forms.TabPage();
            this.tpgMain2_12 = new System.Windows.Forms.TabPage();
            this.tpgMain2_13 = new System.Windows.Forms.TabPage();
            this.tpgMain2_14 = new System.Windows.Forms.TabPage();
            this.tpgMain2_15 = new System.Windows.Forms.TabPage();
            this.tpgMain2_16 = new System.Windows.Forms.TabPage();
            this.tpgGyomu3 = new System.Windows.Forms.TabPage();
            this.tctMain3 = new System.Windows.Forms.TabControl();
            this.tpgMain3_01 = new System.Windows.Forms.TabPage();
            this.tpgMain3_02 = new System.Windows.Forms.TabPage();
            this.tpgMain3_03 = new System.Windows.Forms.TabPage();
            this.tpgMain3_04 = new System.Windows.Forms.TabPage();
            this.tpgMain3_05 = new System.Windows.Forms.TabPage();
            this.tpgMain3_06 = new System.Windows.Forms.TabPage();
            this.tpgMain3_07 = new System.Windows.Forms.TabPage();
            this.tpgMain3_08 = new System.Windows.Forms.TabPage();
            this.tpgMain3_09 = new System.Windows.Forms.TabPage();
            this.tpgMain3_10 = new System.Windows.Forms.TabPage();
            this.tpgMain3_11 = new System.Windows.Forms.TabPage();
            this.tpgMain3_12 = new System.Windows.Forms.TabPage();
            this.tpgMain3_13 = new System.Windows.Forms.TabPage();
            this.tpgMain3_14 = new System.Windows.Forms.TabPage();
            this.tpgMain3_15 = new System.Windows.Forms.TabPage();
            this.ItemTabPage16 = new System.Windows.Forms.TabPage();
            this.tpgGyomu4 = new System.Windows.Forms.TabPage();
            this.tpgGyomu5 = new System.Windows.Forms.TabPage();
            this.tpgGyomu6 = new System.Windows.Forms.TabPage();
            this.tpgGyomu7 = new System.Windows.Forms.TabPage();
            this.tpgGyomu8 = new System.Windows.Forms.TabPage();
            this.tpgGyomu9 = new System.Windows.Forms.TabPage();
            this.tpgGyomu10 = new System.Windows.Forms.TabPage();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.button13 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.button19 = new System.Windows.Forms.Button();
            this.button20 = new System.Windows.Forms.Button();
            this.button21 = new System.Windows.Forms.Button();
            this.button22 = new System.Windows.Forms.Button();
            this.button23 = new System.Windows.Forms.Button();
            this.button24 = new System.Windows.Forms.Button();
            this.button25 = new System.Windows.Forms.Button();
            this.lblNowDate = new System.Windows.Forms.Label();
            this.pbxCalendar = new System.Windows.Forms.PictureBox();
            this.btnLogout = new System.Windows.Forms.Button();
            this.lblAccYear = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pbxCompanyLogo)).BeginInit();
            this.tctSubSystem.SuspendLayout();
            this.tpgSubSystem3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.tctGyomu.SuspendLayout();
            this.tpgGyomu1.SuspendLayout();
            this.tctMain1.SuspendLayout();
            this.tpgGyomu2.SuspendLayout();
            this.tctMain2.SuspendLayout();
            this.tpgGyomu3.SuspendLayout();
            this.tctMain3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxCalendar)).BeginInit();
            this.SuspendLayout();
            // 
            // pbxCompanyLogo
            // 
            this.pbxCompanyLogo.Image = ((System.Drawing.Image)(resources.GetObject("pbxCompanyLogo.Image")));
            this.pbxCompanyLogo.Location = new System.Drawing.Point(0, -2);
            this.pbxCompanyLogo.Name = "pbxCompanyLogo";
            this.pbxCompanyLogo.Size = new System.Drawing.Size(90, 60);
            this.pbxCompanyLogo.TabIndex = 1;
            this.pbxCompanyLogo.TabStop = false;
            // 
            // lblSystemName
            // 
            this.lblSystemName.AutoSize = true;
            this.lblSystemName.BackColor = System.Drawing.Color.Transparent;
            this.lblSystemName.Font = new System.Drawing.Font("MS UI Gothic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblSystemName.Image = ((System.Drawing.Image)(resources.GetObject("lblSystemName.Image")));
            this.lblSystemName.Location = new System.Drawing.Point(85, 2);
            this.lblSystemName.Name = "lblSystemName";
            this.lblSystemName.Size = new System.Drawing.Size(160, 21);
            this.lblSystemName.TabIndex = 0;
            this.lblSystemName.Text = "　　　　　　　　　　";
            // 
            // tctSubSystem
            // 
            this.tctSubSystem.Controls.Add(this.tpgSubSystem1);
            this.tctSubSystem.Controls.Add(this.tpgSubSystem2);
            this.tctSubSystem.Controls.Add(this.tpgSubSystem3);
            this.tctSubSystem.Controls.Add(this.tpgSubSystem4);
            this.tctSubSystem.Controls.Add(this.tpgSubSystem5);
            this.tctSubSystem.Controls.Add(this.tpgSubSystem6);
            this.tctSubSystem.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tctSubSystem.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.tctSubSystem.ItemSize = new System.Drawing.Size(130, 30);
            this.tctSubSystem.Location = new System.Drawing.Point(207, 25);
            this.tctSubSystem.Name = "tctSubSystem";
            this.tctSubSystem.SelectedIndex = 0;
            this.tctSubSystem.Size = new System.Drawing.Size(785, 32);
            this.tctSubSystem.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tctSubSystem.TabIndex = 4;
            this.tctSubSystem.SelectedIndexChanged += new System.EventHandler(this.tctSubSystem_SelectedIndexChanged);
            // 
            // tpgSubSystem1
            // 
            this.tpgSubSystem1.BackColor = System.Drawing.Color.White;
            this.tpgSubSystem1.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tpgSubSystem1.Location = new System.Drawing.Point(4, 34);
            this.tpgSubSystem1.Name = "tpgSubSystem1";
            this.tpgSubSystem1.Padding = new System.Windows.Forms.Padding(3);
            this.tpgSubSystem1.Size = new System.Drawing.Size(777, 0);
            this.tpgSubSystem1.TabIndex = 0;
            this.tpgSubSystem1.Text = "セ リ 販 売";
            // 
            // tpgSubSystem2
            // 
            this.tpgSubSystem2.Font = new System.Drawing.Font("ＭＳ ゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tpgSubSystem2.Location = new System.Drawing.Point(4, 34);
            this.tpgSubSystem2.Name = "tpgSubSystem2";
            this.tpgSubSystem2.Padding = new System.Windows.Forms.Padding(3);
            this.tpgSubSystem2.Size = new System.Drawing.Size(777, 0);
            this.tpgSubSystem2.TabIndex = 1;
            this.tpgSubSystem2.Text = "購　買";
            this.tpgSubSystem2.UseVisualStyleBackColor = true;
            // 
            // tpgSubSystem3
            // 
            this.tpgSubSystem3.Controls.Add(this.panel2);
            this.tpgSubSystem3.Controls.Add(this.tabControl1);
            this.tpgSubSystem3.Location = new System.Drawing.Point(4, 34);
            this.tpgSubSystem3.Name = "tpgSubSystem3";
            this.tpgSubSystem3.Padding = new System.Windows.Forms.Padding(3);
            this.tpgSubSystem3.Size = new System.Drawing.Size(777, 0);
            this.tpgSubSystem3.TabIndex = 2;
            this.tpgSubSystem3.Text = "会　計";
            this.tpgSubSystem3.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Controls.Add(this.monthCalendar);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Location = new System.Drawing.Point(207, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(782, 530);
            this.panel2.TabIndex = 3;
            // 
            // monthCalendar
            // 
            this.monthCalendar.CalendarDimensions = new System.Drawing.Size(2, 1);
            this.monthCalendar.Font = new System.Drawing.Font("ＭＳ ゴシック", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.monthCalendar.Location = new System.Drawing.Point(119, 88);
            this.monthCalendar.Name = "monthCalendar";
            this.monthCalendar.ShowToday = false;
            this.monthCalendar.TabIndex = 1;
            this.monthCalendar.TitleBackColor = System.Drawing.SystemColors.MenuHighlight;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.label5.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label5.Location = new System.Drawing.Point(17, 17);
            this.label5.Name = "label5";
            this.label5.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.label5.Size = new System.Drawing.Size(757, 27);
            this.label5.TabIndex = 0;
            this.label5.Text = "鮮魚代金明細書兼請求書出力";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Controls.Add(this.tabPage7);
            this.tabControl1.Controls.Add(this.tabPage8);
            this.tabControl1.Controls.Add(this.tabPage9);
            this.tabControl1.Controls.Add(this.tabPage10);
            this.tabControl1.Controls.Add(this.tabPage11);
            this.tabControl1.Controls.Add(this.tabPage12);
            this.tabControl1.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tabControl1.ItemSize = new System.Drawing.Size(96, 30);
            this.tabControl1.Location = new System.Drawing.Point(1, 1);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.Padding = new System.Drawing.Point(0, 0);
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(200, 529);
            this.tabControl1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.button26);
            this.tabPage5.Controls.Add(this.button27);
            this.tabPage5.Controls.Add(this.button28);
            this.tabPage5.Controls.Add(this.button29);
            this.tabPage5.Controls.Add(this.button30);
            this.tabPage5.Controls.Add(this.button31);
            this.tabPage5.Controls.Add(this.button32);
            this.tabPage5.Controls.Add(this.button33);
            this.tabPage5.Controls.Add(this.button34);
            this.tabPage5.Controls.Add(this.button35);
            this.tabPage5.Controls.Add(this.button36);
            this.tabPage5.Controls.Add(this.button37);
            this.tabPage5.Font = new System.Drawing.Font("MS UI Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tabPage5.Location = new System.Drawing.Point(4, 124);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(192, 401);
            this.tabPage5.TabIndex = 0;
            this.tabPage5.Text = "通常業務";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // button26
            // 
            this.button26.AllowDrop = true;
            this.button26.BackColor = System.Drawing.Color.White;
            this.button26.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button26.BackgroundImage")));
            this.button26.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.button26.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.button26.Location = new System.Drawing.Point(99, 322);
            this.button26.Name = "button26";
            this.button26.Size = new System.Drawing.Size(90, 60);
            this.button26.TabIndex = 13;
            this.button26.Text = "振替及出勤チェックリスト";
            this.button26.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button26.UseVisualStyleBackColor = false;
            // 
            // button27
            // 
            this.button27.AllowDrop = true;
            this.button27.BackColor = System.Drawing.Color.White;
            this.button27.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button27.BackgroundImage")));
            this.button27.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.button27.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.button27.Location = new System.Drawing.Point(3, 322);
            this.button27.Name = "button27";
            this.button27.Size = new System.Drawing.Size(90, 60);
            this.button27.TabIndex = 12;
            this.button27.Text = "支払い明細書出力";
            this.button27.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button27.UseVisualStyleBackColor = false;
            // 
            // button28
            // 
            this.button28.AllowDrop = true;
            this.button28.BackColor = System.Drawing.Color.White;
            this.button28.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button28.BackgroundImage")));
            this.button28.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.button28.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.button28.Location = new System.Drawing.Point(99, 258);
            this.button28.Name = "button28";
            this.button28.Size = new System.Drawing.Size(90, 60);
            this.button28.TabIndex = 11;
            this.button28.Text = "入金チェックリスト";
            this.button28.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button28.UseVisualStyleBackColor = false;
            // 
            // button29
            // 
            this.button29.AllowDrop = true;
            this.button29.BackColor = System.Drawing.Color.White;
            this.button29.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button29.BackgroundImage")));
            this.button29.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.button29.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.button29.Location = new System.Drawing.Point(3, 258);
            this.button29.Name = "button29";
            this.button29.Size = new System.Drawing.Size(90, 60);
            this.button29.TabIndex = 10;
            this.button29.Text = "振替及出勤チェックリスト";
            this.button29.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button29.UseVisualStyleBackColor = false;
            // 
            // button30
            // 
            this.button30.AllowDrop = true;
            this.button30.BackColor = System.Drawing.Color.White;
            this.button30.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button30.BackgroundImage")));
            this.button30.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.button30.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.button30.Location = new System.Drawing.Point(99, 194);
            this.button30.Name = "button30";
            this.button30.Size = new System.Drawing.Size(90, 60);
            this.button30.TabIndex = 9;
            this.button30.Text = "支払い明細書出力";
            this.button30.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button30.UseVisualStyleBackColor = false;
            // 
            // button31
            // 
            this.button31.AllowDrop = true;
            this.button31.BackColor = System.Drawing.Color.White;
            this.button31.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button31.BackgroundImage")));
            this.button31.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.button31.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.button31.Location = new System.Drawing.Point(3, 194);
            this.button31.Name = "button31";
            this.button31.Size = new System.Drawing.Size(90, 60);
            this.button31.TabIndex = 8;
            this.button31.Text = "入金チェックリスト";
            this.button31.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button31.UseVisualStyleBackColor = false;
            // 
            // button32
            // 
            this.button32.AllowDrop = true;
            this.button32.BackColor = System.Drawing.Color.White;
            this.button32.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button32.BackgroundImage")));
            this.button32.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.button32.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.button32.Location = new System.Drawing.Point(99, 131);
            this.button32.Name = "button32";
            this.button32.Size = new System.Drawing.Size(90, 60);
            this.button32.TabIndex = 7;
            this.button32.Text = "振替及出勤チェックリスト";
            this.button32.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button32.UseVisualStyleBackColor = false;
            // 
            // button33
            // 
            this.button33.AllowDrop = true;
            this.button33.BackColor = System.Drawing.Color.White;
            this.button33.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button33.BackgroundImage")));
            this.button33.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.button33.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.button33.Location = new System.Drawing.Point(3, 131);
            this.button33.Name = "button33";
            this.button33.Size = new System.Drawing.Size(90, 60);
            this.button33.TabIndex = 6;
            this.button33.Text = "支払い明細書出力";
            this.button33.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button33.UseVisualStyleBackColor = false;
            // 
            // button34
            // 
            this.button34.AllowDrop = true;
            this.button34.BackColor = System.Drawing.Color.White;
            this.button34.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button34.BackgroundImage")));
            this.button34.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.button34.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.button34.Location = new System.Drawing.Point(99, 67);
            this.button34.Name = "button34";
            this.button34.Size = new System.Drawing.Size(90, 60);
            this.button34.TabIndex = 5;
            this.button34.Text = "入金チェックリスト";
            this.button34.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button34.UseVisualStyleBackColor = false;
            // 
            // button35
            // 
            this.button35.AllowDrop = true;
            this.button35.BackColor = System.Drawing.Color.White;
            this.button35.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button35.BackgroundImage")));
            this.button35.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.button35.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.button35.Location = new System.Drawing.Point(3, 67);
            this.button35.Name = "button35";
            this.button35.Size = new System.Drawing.Size(90, 60);
            this.button35.TabIndex = 4;
            this.button35.Text = "振替及出勤チェックリスト";
            this.button35.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button35.UseVisualStyleBackColor = false;
            // 
            // button36
            // 
            this.button36.AllowDrop = true;
            this.button36.BackColor = System.Drawing.Color.White;
            this.button36.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button36.BackgroundImage")));
            this.button36.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.button36.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.button36.Location = new System.Drawing.Point(99, 4);
            this.button36.Name = "button36";
            this.button36.Size = new System.Drawing.Size(90, 60);
            this.button36.TabIndex = 3;
            this.button36.Text = "支払い明細書出力";
            this.button36.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button36.UseVisualStyleBackColor = false;
            // 
            // button37
            // 
            this.button37.AllowDrop = true;
            this.button37.BackColor = System.Drawing.Color.White;
            this.button37.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button37.BackgroundImage")));
            this.button37.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.button37.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.button37.Location = new System.Drawing.Point(3, 4);
            this.button37.Name = "button37";
            this.button37.Size = new System.Drawing.Size(90, 60);
            this.button37.TabIndex = 2;
            this.button37.Text = "入金チェックリスト";
            this.button37.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button37.UseVisualStyleBackColor = false;
            // 
            // tabPage6
            // 
            this.tabPage6.Font = new System.Drawing.Font("MS UI Gothic", 10F);
            this.tabPage6.Location = new System.Drawing.Point(4, 124);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(192, 401);
            this.tabPage6.TabIndex = 1;
            this.tabPage6.Text = "確認業務";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // tabPage7
            // 
            this.tabPage7.Font = new System.Drawing.Font("MS UI Gothic", 10F);
            this.tabPage7.Location = new System.Drawing.Point(4, 124);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage7.Size = new System.Drawing.Size(192, 401);
            this.tabPage7.TabIndex = 2;
            this.tabPage7.Text = "月次処理";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // tabPage8
            // 
            this.tabPage8.Font = new System.Drawing.Font("MS UI Gothic", 10F);
            this.tabPage8.Location = new System.Drawing.Point(4, 124);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage8.Size = new System.Drawing.Size(192, 401);
            this.tabPage8.TabIndex = 3;
            this.tabPage8.Text = "マスタメンテ";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // tabPage9
            // 
            this.tabPage9.Font = new System.Drawing.Font("MS UI Gothic", 10F);
            this.tabPage9.Location = new System.Drawing.Point(4, 124);
            this.tabPage9.Name = "tabPage9";
            this.tabPage9.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage9.Size = new System.Drawing.Size(192, 401);
            this.tabPage9.TabIndex = 6;
            this.tabPage9.UseVisualStyleBackColor = true;
            // 
            // tabPage10
            // 
            this.tabPage10.Font = new System.Drawing.Font("MS UI Gothic", 10F);
            this.tabPage10.Location = new System.Drawing.Point(4, 124);
            this.tabPage10.Name = "tabPage10";
            this.tabPage10.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage10.Size = new System.Drawing.Size(192, 401);
            this.tabPage10.TabIndex = 5;
            this.tabPage10.UseVisualStyleBackColor = true;
            // 
            // tabPage11
            // 
            this.tabPage11.Font = new System.Drawing.Font("MS UI Gothic", 10F);
            this.tabPage11.Location = new System.Drawing.Point(4, 124);
            this.tabPage11.Name = "tabPage11";
            this.tabPage11.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage11.Size = new System.Drawing.Size(192, 401);
            this.tabPage11.TabIndex = 4;
            this.tabPage11.UseVisualStyleBackColor = true;
            // 
            // tabPage12
            // 
            this.tabPage12.Font = new System.Drawing.Font("MS UI Gothic", 10F);
            this.tabPage12.Location = new System.Drawing.Point(4, 124);
            this.tabPage12.Name = "tabPage12";
            this.tabPage12.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage12.Size = new System.Drawing.Size(192, 401);
            this.tabPage12.TabIndex = 7;
            this.tabPage12.UseVisualStyleBackColor = true;
            // 
            // tpgSubSystem4
            // 
            this.tpgSubSystem4.BackColor = System.Drawing.Color.White;
            this.tpgSubSystem4.Location = new System.Drawing.Point(4, 34);
            this.tpgSubSystem4.Name = "tpgSubSystem4";
            this.tpgSubSystem4.Padding = new System.Windows.Forms.Padding(3);
            this.tpgSubSystem4.Size = new System.Drawing.Size(777, 0);
            this.tpgSubSystem4.TabIndex = 3;
            this.tpgSubSystem4.Text = "給　与";
            // 
            // tpgSubSystem5
            // 
            this.tpgSubSystem5.Location = new System.Drawing.Point(4, 34);
            this.tpgSubSystem5.Name = "tpgSubSystem5";
            this.tpgSubSystem5.Size = new System.Drawing.Size(777, 0);
            this.tpgSubSystem5.TabIndex = 4;
            this.tpgSubSystem5.UseVisualStyleBackColor = true;
            // 
            // tpgSubSystem6
            // 
            this.tpgSubSystem6.Location = new System.Drawing.Point(4, 34);
            this.tpgSubSystem6.Name = "tpgSubSystem6";
            this.tpgSubSystem6.Size = new System.Drawing.Size(777, 0);
            this.tpgSubSystem6.TabIndex = 5;
            this.tpgSubSystem6.UseVisualStyleBackColor = true;
            // 
            // tctGyomu
            // 
            this.tctGyomu.Controls.Add(this.tpgGyomu1);
            this.tctGyomu.Controls.Add(this.tpgGyomu2);
            this.tctGyomu.Controls.Add(this.tpgGyomu3);
            this.tctGyomu.Controls.Add(this.tpgGyomu4);
            this.tctGyomu.Controls.Add(this.tpgGyomu5);
            this.tctGyomu.Controls.Add(this.tpgGyomu6);
            this.tctGyomu.Controls.Add(this.tpgGyomu7);
            this.tctGyomu.Controls.Add(this.tpgGyomu8);
            this.tctGyomu.Controls.Add(this.tpgGyomu9);
            this.tctGyomu.Controls.Add(this.tpgGyomu10);
            this.tctGyomu.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tctGyomu.ItemSize = new System.Drawing.Size(99, 30);
            this.tctGyomu.Location = new System.Drawing.Point(-3, 55);
            this.tctGyomu.Multiline = true;
            this.tctGyomu.Name = "tctGyomu";
            this.tctGyomu.Padding = new System.Drawing.Point(0, 0);
            this.tctGyomu.SelectedIndex = 0;
            this.tctGyomu.Size = new System.Drawing.Size(1000, 588);
            this.tctGyomu.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tctGyomu.TabIndex = 5;
            // 
            // tpgGyomu1
            // 
            this.tpgGyomu1.BackColor = System.Drawing.Color.AliceBlue;
            this.tpgGyomu1.Controls.Add(this.tctMain1);
            this.tpgGyomu1.Font = new System.Drawing.Font("MS UI Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tpgGyomu1.Location = new System.Drawing.Point(4, 34);
            this.tpgGyomu1.Name = "tpgGyomu1";
            this.tpgGyomu1.Padding = new System.Windows.Forms.Padding(3);
            this.tpgGyomu1.Size = new System.Drawing.Size(992, 550);
            this.tpgGyomu1.TabIndex = 0;
            this.tpgGyomu1.Text = "通常業務";
            // 
            // tctMain1
            // 
            this.tctMain1.Alignment = System.Windows.Forms.TabAlignment.Left;
            this.tctMain1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tctMain1.Controls.Add(this.tpgMain1_01);
            this.tctMain1.Controls.Add(this.tpgMain1_02);
            this.tctMain1.Controls.Add(this.tpgMain1_03);
            this.tctMain1.Controls.Add(this.tpgMain1_04);
            this.tctMain1.Controls.Add(this.tpgMain1_05);
            this.tctMain1.Controls.Add(this.tpgMain1_06);
            this.tctMain1.Controls.Add(this.tpgMain1_07);
            this.tctMain1.Controls.Add(this.tpgMain1_08);
            this.tctMain1.Controls.Add(this.tpgMain1_09);
            this.tctMain1.Controls.Add(this.tpgMain1_10);
            this.tctMain1.Controls.Add(this.tpgMain1_11);
            this.tctMain1.Controls.Add(this.tpgMain1_12);
            this.tctMain1.Controls.Add(this.tpgMain1_13);
            this.tctMain1.Controls.Add(this.tpgMain1_14);
            this.tctMain1.Controls.Add(this.tpgMain1_15);
            this.tctMain1.Controls.Add(this.tpgMain1_16);
            this.tctMain1.ItemSize = new System.Drawing.Size(34, 150);
            this.tctMain1.Location = new System.Drawing.Point(0, 0);
            this.tctMain1.Multiline = true;
            this.tctMain1.Name = "tctMain1";
            this.tctMain1.SelectedIndex = 0;
            this.tctMain1.Size = new System.Drawing.Size(997, 684);
            this.tctMain1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tctMain1.TabIndex = 0;
            // 
            // tpgMain1_01
            // 
            this.tpgMain1_01.Location = new System.Drawing.Point(154, 4);
            this.tpgMain1_01.Name = "tpgMain1_01";
            this.tpgMain1_01.Padding = new System.Windows.Forms.Padding(3);
            this.tpgMain1_01.Size = new System.Drawing.Size(839, 676);
            this.tpgMain1_01.TabIndex = 0;
            this.tpgMain1_01.Text = "　　　月初処理";
            this.tpgMain1_01.UseVisualStyleBackColor = true;
            // 
            // tpgMain1_02
            // 
            this.tpgMain1_02.Location = new System.Drawing.Point(154, 4);
            this.tpgMain1_02.Name = "tpgMain1_02";
            this.tpgMain1_02.Padding = new System.Windows.Forms.Padding(3);
            this.tpgMain1_02.Size = new System.Drawing.Size(839, 676);
            this.tpgMain1_02.TabIndex = 1;
            this.tpgMain1_02.Text = "　　　入金チェックリスト";
            this.tpgMain1_02.UseVisualStyleBackColor = true;
            // 
            // tpgMain1_03
            // 
            this.tpgMain1_03.Location = new System.Drawing.Point(154, 4);
            this.tpgMain1_03.Name = "tpgMain1_03";
            this.tpgMain1_03.Padding = new System.Windows.Forms.Padding(3);
            this.tpgMain1_03.Size = new System.Drawing.Size(839, 676);
            this.tpgMain1_03.TabIndex = 2;
            this.tpgMain1_03.Text = "　　　支払い明細書出力";
            this.tpgMain1_03.UseVisualStyleBackColor = true;
            // 
            // tpgMain1_04
            // 
            this.tpgMain1_04.Location = new System.Drawing.Point(154, 4);
            this.tpgMain1_04.Name = "tpgMain1_04";
            this.tpgMain1_04.Padding = new System.Windows.Forms.Padding(3);
            this.tpgMain1_04.Size = new System.Drawing.Size(839, 676);
            this.tpgMain1_04.TabIndex = 3;
            this.tpgMain1_04.Text = "　　　振替及出勤チェックリスト";
            this.tpgMain1_04.UseVisualStyleBackColor = true;
            // 
            // tpgMain1_05
            // 
            this.tpgMain1_05.Location = new System.Drawing.Point(154, 4);
            this.tpgMain1_05.Name = "tpgMain1_05";
            this.tpgMain1_05.Padding = new System.Windows.Forms.Padding(3);
            this.tpgMain1_05.Size = new System.Drawing.Size(839, 676);
            this.tpgMain1_05.TabIndex = 4;
            this.tpgMain1_05.Text = "05";
            this.tpgMain1_05.UseVisualStyleBackColor = true;
            // 
            // tpgMain1_06
            // 
            this.tpgMain1_06.Location = new System.Drawing.Point(154, 4);
            this.tpgMain1_06.Name = "tpgMain1_06";
            this.tpgMain1_06.Padding = new System.Windows.Forms.Padding(3);
            this.tpgMain1_06.Size = new System.Drawing.Size(839, 676);
            this.tpgMain1_06.TabIndex = 5;
            this.tpgMain1_06.Text = "06";
            this.tpgMain1_06.UseVisualStyleBackColor = true;
            // 
            // tpgMain1_07
            // 
            this.tpgMain1_07.Location = new System.Drawing.Point(154, 4);
            this.tpgMain1_07.Name = "tpgMain1_07";
            this.tpgMain1_07.Padding = new System.Windows.Forms.Padding(3);
            this.tpgMain1_07.Size = new System.Drawing.Size(839, 676);
            this.tpgMain1_07.TabIndex = 6;
            this.tpgMain1_07.Text = "07";
            this.tpgMain1_07.UseVisualStyleBackColor = true;
            // 
            // tpgMain1_08
            // 
            this.tpgMain1_08.Location = new System.Drawing.Point(154, 4);
            this.tpgMain1_08.Name = "tpgMain1_08";
            this.tpgMain1_08.Padding = new System.Windows.Forms.Padding(3);
            this.tpgMain1_08.Size = new System.Drawing.Size(839, 676);
            this.tpgMain1_08.TabIndex = 7;
            this.tpgMain1_08.Text = "08";
            this.tpgMain1_08.UseVisualStyleBackColor = true;
            // 
            // tpgMain1_09
            // 
            this.tpgMain1_09.Location = new System.Drawing.Point(154, 4);
            this.tpgMain1_09.Name = "tpgMain1_09";
            this.tpgMain1_09.Padding = new System.Windows.Forms.Padding(3);
            this.tpgMain1_09.Size = new System.Drawing.Size(839, 676);
            this.tpgMain1_09.TabIndex = 8;
            this.tpgMain1_09.Text = "09";
            this.tpgMain1_09.UseVisualStyleBackColor = true;
            // 
            // tpgMain1_10
            // 
            this.tpgMain1_10.Location = new System.Drawing.Point(154, 4);
            this.tpgMain1_10.Name = "tpgMain1_10";
            this.tpgMain1_10.Padding = new System.Windows.Forms.Padding(3);
            this.tpgMain1_10.Size = new System.Drawing.Size(839, 676);
            this.tpgMain1_10.TabIndex = 9;
            this.tpgMain1_10.Text = "10";
            this.tpgMain1_10.UseVisualStyleBackColor = true;
            // 
            // tpgMain1_11
            // 
            this.tpgMain1_11.Location = new System.Drawing.Point(154, 4);
            this.tpgMain1_11.Name = "tpgMain1_11";
            this.tpgMain1_11.Size = new System.Drawing.Size(839, 676);
            this.tpgMain1_11.TabIndex = 10;
            this.tpgMain1_11.Text = "11";
            this.tpgMain1_11.UseVisualStyleBackColor = true;
            // 
            // tpgMain1_12
            // 
            this.tpgMain1_12.Location = new System.Drawing.Point(154, 4);
            this.tpgMain1_12.Name = "tpgMain1_12";
            this.tpgMain1_12.Size = new System.Drawing.Size(839, 676);
            this.tpgMain1_12.TabIndex = 11;
            this.tpgMain1_12.Text = "12";
            this.tpgMain1_12.UseVisualStyleBackColor = true;
            // 
            // tpgMain1_13
            // 
            this.tpgMain1_13.Location = new System.Drawing.Point(154, 4);
            this.tpgMain1_13.Name = "tpgMain1_13";
            this.tpgMain1_13.Size = new System.Drawing.Size(839, 676);
            this.tpgMain1_13.TabIndex = 12;
            this.tpgMain1_13.Text = "13";
            this.tpgMain1_13.UseVisualStyleBackColor = true;
            // 
            // tpgMain1_14
            // 
            this.tpgMain1_14.Location = new System.Drawing.Point(154, 4);
            this.tpgMain1_14.Name = "tpgMain1_14";
            this.tpgMain1_14.Size = new System.Drawing.Size(839, 676);
            this.tpgMain1_14.TabIndex = 13;
            this.tpgMain1_14.Text = "14";
            this.tpgMain1_14.UseVisualStyleBackColor = true;
            // 
            // tpgMain1_15
            // 
            this.tpgMain1_15.Location = new System.Drawing.Point(154, 4);
            this.tpgMain1_15.Name = "tpgMain1_15";
            this.tpgMain1_15.Size = new System.Drawing.Size(839, 676);
            this.tpgMain1_15.TabIndex = 14;
            this.tpgMain1_15.Text = "15";
            this.tpgMain1_15.UseVisualStyleBackColor = true;
            // 
            // tpgMain1_16
            // 
            this.tpgMain1_16.Location = new System.Drawing.Point(154, 4);
            this.tpgMain1_16.Name = "tpgMain1_16";
            this.tpgMain1_16.Size = new System.Drawing.Size(839, 676);
            this.tpgMain1_16.TabIndex = 15;
            this.tpgMain1_16.Text = "16";
            this.tpgMain1_16.UseVisualStyleBackColor = true;
            // 
            // tpgGyomu2
            // 
            this.tpgGyomu2.BackColor = System.Drawing.Color.Linen;
            this.tpgGyomu2.Controls.Add(this.tctMain2);
            this.tpgGyomu2.Font = new System.Drawing.Font("MS UI Gothic", 10F);
            this.tpgGyomu2.Location = new System.Drawing.Point(4, 34);
            this.tpgGyomu2.Name = "tpgGyomu2";
            this.tpgGyomu2.Padding = new System.Windows.Forms.Padding(3);
            this.tpgGyomu2.Size = new System.Drawing.Size(992, 550);
            this.tpgGyomu2.TabIndex = 1;
            this.tpgGyomu2.Text = "確認業務";
            // 
            // tctMain2
            // 
            this.tctMain2.Alignment = System.Windows.Forms.TabAlignment.Left;
            this.tctMain2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tctMain2.Controls.Add(this.tpgMain2_01);
            this.tctMain2.Controls.Add(this.tpgMain2_02);
            this.tctMain2.Controls.Add(this.tpgMain2_03);
            this.tctMain2.Controls.Add(this.tpgMain2_04);
            this.tctMain2.Controls.Add(this.tpgMain2_05);
            this.tctMain2.Controls.Add(this.tpgMain2_06);
            this.tctMain2.Controls.Add(this.tpgMain2_07);
            this.tctMain2.Controls.Add(this.tpgMain2_08);
            this.tctMain2.Controls.Add(this.tpgMain2_09);
            this.tctMain2.Controls.Add(this.tpgMain2_10);
            this.tctMain2.Controls.Add(this.tpgMain2_11);
            this.tctMain2.Controls.Add(this.tpgMain2_12);
            this.tctMain2.Controls.Add(this.tpgMain2_13);
            this.tctMain2.Controls.Add(this.tpgMain2_14);
            this.tctMain2.Controls.Add(this.tpgMain2_15);
            this.tctMain2.Controls.Add(this.tpgMain2_16);
            this.tctMain2.ItemSize = new System.Drawing.Size(34, 150);
            this.tctMain2.Location = new System.Drawing.Point(0, 0);
            this.tctMain2.Multiline = true;
            this.tctMain2.Name = "tctMain2";
            this.tctMain2.SelectedIndex = 0;
            this.tctMain2.Size = new System.Drawing.Size(997, 684);
            this.tctMain2.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tctMain2.TabIndex = 0;
            // 
            // tpgMain2_01
            // 
            this.tpgMain2_01.Location = new System.Drawing.Point(154, 4);
            this.tpgMain2_01.Name = "tpgMain2_01";
            this.tpgMain2_01.Padding = new System.Windows.Forms.Padding(3);
            this.tpgMain2_01.Size = new System.Drawing.Size(839, 676);
            this.tpgMain2_01.TabIndex = 0;
            this.tpgMain2_01.Text = "　　　月初処理";
            this.tpgMain2_01.UseVisualStyleBackColor = true;
            // 
            // tpgMain2_02
            // 
            this.tpgMain2_02.Location = new System.Drawing.Point(154, 4);
            this.tpgMain2_02.Name = "tpgMain2_02";
            this.tpgMain2_02.Padding = new System.Windows.Forms.Padding(3);
            this.tpgMain2_02.Size = new System.Drawing.Size(839, 676);
            this.tpgMain2_02.TabIndex = 1;
            this.tpgMain2_02.Text = "　　　入金チェックリスト";
            this.tpgMain2_02.UseVisualStyleBackColor = true;
            // 
            // tpgMain2_03
            // 
            this.tpgMain2_03.Location = new System.Drawing.Point(154, 4);
            this.tpgMain2_03.Name = "tpgMain2_03";
            this.tpgMain2_03.Padding = new System.Windows.Forms.Padding(3);
            this.tpgMain2_03.Size = new System.Drawing.Size(839, 676);
            this.tpgMain2_03.TabIndex = 2;
            this.tpgMain2_03.Text = "　　　支払い明細書出力";
            this.tpgMain2_03.UseVisualStyleBackColor = true;
            // 
            // tpgMain2_04
            // 
            this.tpgMain2_04.Location = new System.Drawing.Point(154, 4);
            this.tpgMain2_04.Name = "tpgMain2_04";
            this.tpgMain2_04.Padding = new System.Windows.Forms.Padding(3);
            this.tpgMain2_04.Size = new System.Drawing.Size(839, 676);
            this.tpgMain2_04.TabIndex = 3;
            this.tpgMain2_04.Text = "　　　振替及出勤チェックリスト";
            this.tpgMain2_04.UseVisualStyleBackColor = true;
            // 
            // tpgMain2_05
            // 
            this.tpgMain2_05.Location = new System.Drawing.Point(154, 4);
            this.tpgMain2_05.Name = "tpgMain2_05";
            this.tpgMain2_05.Padding = new System.Windows.Forms.Padding(3);
            this.tpgMain2_05.Size = new System.Drawing.Size(839, 676);
            this.tpgMain2_05.TabIndex = 4;
            this.tpgMain2_05.Text = "05";
            this.tpgMain2_05.UseVisualStyleBackColor = true;
            // 
            // tpgMain2_06
            // 
            this.tpgMain2_06.Location = new System.Drawing.Point(154, 4);
            this.tpgMain2_06.Name = "tpgMain2_06";
            this.tpgMain2_06.Padding = new System.Windows.Forms.Padding(3);
            this.tpgMain2_06.Size = new System.Drawing.Size(839, 676);
            this.tpgMain2_06.TabIndex = 5;
            this.tpgMain2_06.Text = "06";
            this.tpgMain2_06.UseVisualStyleBackColor = true;
            // 
            // tpgMain2_07
            // 
            this.tpgMain2_07.Location = new System.Drawing.Point(154, 4);
            this.tpgMain2_07.Name = "tpgMain2_07";
            this.tpgMain2_07.Padding = new System.Windows.Forms.Padding(3);
            this.tpgMain2_07.Size = new System.Drawing.Size(839, 676);
            this.tpgMain2_07.TabIndex = 6;
            this.tpgMain2_07.Text = "07";
            this.tpgMain2_07.UseVisualStyleBackColor = true;
            // 
            // tpgMain2_08
            // 
            this.tpgMain2_08.Location = new System.Drawing.Point(154, 4);
            this.tpgMain2_08.Name = "tpgMain2_08";
            this.tpgMain2_08.Padding = new System.Windows.Forms.Padding(3);
            this.tpgMain2_08.Size = new System.Drawing.Size(839, 676);
            this.tpgMain2_08.TabIndex = 7;
            this.tpgMain2_08.Text = "08";
            this.tpgMain2_08.UseVisualStyleBackColor = true;
            // 
            // tpgMain2_09
            // 
            this.tpgMain2_09.Location = new System.Drawing.Point(154, 4);
            this.tpgMain2_09.Name = "tpgMain2_09";
            this.tpgMain2_09.Padding = new System.Windows.Forms.Padding(3);
            this.tpgMain2_09.Size = new System.Drawing.Size(839, 676);
            this.tpgMain2_09.TabIndex = 8;
            this.tpgMain2_09.Text = "09";
            this.tpgMain2_09.UseVisualStyleBackColor = true;
            // 
            // tpgMain2_10
            // 
            this.tpgMain2_10.Location = new System.Drawing.Point(154, 4);
            this.tpgMain2_10.Name = "tpgMain2_10";
            this.tpgMain2_10.Padding = new System.Windows.Forms.Padding(3);
            this.tpgMain2_10.Size = new System.Drawing.Size(839, 676);
            this.tpgMain2_10.TabIndex = 9;
            this.tpgMain2_10.Text = "10";
            this.tpgMain2_10.UseVisualStyleBackColor = true;
            // 
            // tpgMain2_11
            // 
            this.tpgMain2_11.Location = new System.Drawing.Point(154, 4);
            this.tpgMain2_11.Name = "tpgMain2_11";
            this.tpgMain2_11.Size = new System.Drawing.Size(839, 676);
            this.tpgMain2_11.TabIndex = 10;
            this.tpgMain2_11.Text = "11";
            this.tpgMain2_11.UseVisualStyleBackColor = true;
            // 
            // tpgMain2_12
            // 
            this.tpgMain2_12.Location = new System.Drawing.Point(154, 4);
            this.tpgMain2_12.Name = "tpgMain2_12";
            this.tpgMain2_12.Size = new System.Drawing.Size(839, 676);
            this.tpgMain2_12.TabIndex = 11;
            this.tpgMain2_12.Text = "12";
            this.tpgMain2_12.UseVisualStyleBackColor = true;
            // 
            // tpgMain2_13
            // 
            this.tpgMain2_13.Location = new System.Drawing.Point(154, 4);
            this.tpgMain2_13.Name = "tpgMain2_13";
            this.tpgMain2_13.Size = new System.Drawing.Size(839, 676);
            this.tpgMain2_13.TabIndex = 12;
            this.tpgMain2_13.Text = "13";
            this.tpgMain2_13.UseVisualStyleBackColor = true;
            // 
            // tpgMain2_14
            // 
            this.tpgMain2_14.Location = new System.Drawing.Point(154, 4);
            this.tpgMain2_14.Name = "tpgMain2_14";
            this.tpgMain2_14.Size = new System.Drawing.Size(839, 676);
            this.tpgMain2_14.TabIndex = 13;
            this.tpgMain2_14.Text = "14";
            this.tpgMain2_14.UseVisualStyleBackColor = true;
            // 
            // tpgMain2_15
            // 
            this.tpgMain2_15.Location = new System.Drawing.Point(154, 4);
            this.tpgMain2_15.Name = "tpgMain2_15";
            this.tpgMain2_15.Size = new System.Drawing.Size(839, 676);
            this.tpgMain2_15.TabIndex = 14;
            this.tpgMain2_15.Text = "15";
            this.tpgMain2_15.UseVisualStyleBackColor = true;
            // 
            // tpgMain2_16
            // 
            this.tpgMain2_16.Location = new System.Drawing.Point(154, 4);
            this.tpgMain2_16.Name = "tpgMain2_16";
            this.tpgMain2_16.Size = new System.Drawing.Size(839, 676);
            this.tpgMain2_16.TabIndex = 15;
            this.tpgMain2_16.Text = "16";
            this.tpgMain2_16.UseVisualStyleBackColor = true;
            // 
            // tpgGyomu3
            // 
            this.tpgGyomu3.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.tpgGyomu3.Controls.Add(this.tctMain3);
            this.tpgGyomu3.Font = new System.Drawing.Font("MS UI Gothic", 10F);
            this.tpgGyomu3.Location = new System.Drawing.Point(4, 34);
            this.tpgGyomu3.Name = "tpgGyomu3";
            this.tpgGyomu3.Padding = new System.Windows.Forms.Padding(3);
            this.tpgGyomu3.Size = new System.Drawing.Size(992, 550);
            this.tpgGyomu3.TabIndex = 2;
            this.tpgGyomu3.Text = "月次処理";
            // 
            // tctMain3
            // 
            this.tctMain3.Alignment = System.Windows.Forms.TabAlignment.Left;
            this.tctMain3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tctMain3.Controls.Add(this.tpgMain3_01);
            this.tctMain3.Controls.Add(this.tpgMain3_02);
            this.tctMain3.Controls.Add(this.tpgMain3_03);
            this.tctMain3.Controls.Add(this.tpgMain3_04);
            this.tctMain3.Controls.Add(this.tpgMain3_05);
            this.tctMain3.Controls.Add(this.tpgMain3_06);
            this.tctMain3.Controls.Add(this.tpgMain3_07);
            this.tctMain3.Controls.Add(this.tpgMain3_08);
            this.tctMain3.Controls.Add(this.tpgMain3_09);
            this.tctMain3.Controls.Add(this.tpgMain3_10);
            this.tctMain3.Controls.Add(this.tpgMain3_11);
            this.tctMain3.Controls.Add(this.tpgMain3_12);
            this.tctMain3.Controls.Add(this.tpgMain3_13);
            this.tctMain3.Controls.Add(this.tpgMain3_14);
            this.tctMain3.Controls.Add(this.tpgMain3_15);
            this.tctMain3.Controls.Add(this.ItemTabPage16);
            this.tctMain3.ItemSize = new System.Drawing.Size(34, 150);
            this.tctMain3.Location = new System.Drawing.Point(0, 0);
            this.tctMain3.Multiline = true;
            this.tctMain3.Name = "tctMain3";
            this.tctMain3.SelectedIndex = 0;
            this.tctMain3.Size = new System.Drawing.Size(997, 684);
            this.tctMain3.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tctMain3.TabIndex = 0;
            // 
            // tpgMain3_01
            // 
            this.tpgMain3_01.Location = new System.Drawing.Point(154, 4);
            this.tpgMain3_01.Name = "tpgMain3_01";
            this.tpgMain3_01.Padding = new System.Windows.Forms.Padding(3);
            this.tpgMain3_01.Size = new System.Drawing.Size(839, 676);
            this.tpgMain3_01.TabIndex = 0;
            this.tpgMain3_01.Text = "　　　月初処理";
            this.tpgMain3_01.UseVisualStyleBackColor = true;
            // 
            // tpgMain3_02
            // 
            this.tpgMain3_02.Location = new System.Drawing.Point(154, 4);
            this.tpgMain3_02.Name = "tpgMain3_02";
            this.tpgMain3_02.Padding = new System.Windows.Forms.Padding(3);
            this.tpgMain3_02.Size = new System.Drawing.Size(839, 676);
            this.tpgMain3_02.TabIndex = 1;
            this.tpgMain3_02.Text = "　　　入金チェックリスト";
            this.tpgMain3_02.UseVisualStyleBackColor = true;
            // 
            // tpgMain3_03
            // 
            this.tpgMain3_03.Location = new System.Drawing.Point(154, 4);
            this.tpgMain3_03.Name = "tpgMain3_03";
            this.tpgMain3_03.Padding = new System.Windows.Forms.Padding(3);
            this.tpgMain3_03.Size = new System.Drawing.Size(839, 676);
            this.tpgMain3_03.TabIndex = 2;
            this.tpgMain3_03.Text = "　　　支払い明細書出力";
            this.tpgMain3_03.UseVisualStyleBackColor = true;
            // 
            // tpgMain3_04
            // 
            this.tpgMain3_04.Location = new System.Drawing.Point(154, 4);
            this.tpgMain3_04.Name = "tpgMain3_04";
            this.tpgMain3_04.Padding = new System.Windows.Forms.Padding(3);
            this.tpgMain3_04.Size = new System.Drawing.Size(839, 676);
            this.tpgMain3_04.TabIndex = 3;
            this.tpgMain3_04.Text = "　　　振替及出勤チェックリスト";
            this.tpgMain3_04.UseVisualStyleBackColor = true;
            // 
            // tpgMain3_05
            // 
            this.tpgMain3_05.Location = new System.Drawing.Point(154, 4);
            this.tpgMain3_05.Name = "tpgMain3_05";
            this.tpgMain3_05.Padding = new System.Windows.Forms.Padding(3);
            this.tpgMain3_05.Size = new System.Drawing.Size(839, 676);
            this.tpgMain3_05.TabIndex = 4;
            this.tpgMain3_05.Text = "05";
            this.tpgMain3_05.UseVisualStyleBackColor = true;
            // 
            // tpgMain3_06
            // 
            this.tpgMain3_06.Location = new System.Drawing.Point(154, 4);
            this.tpgMain3_06.Name = "tpgMain3_06";
            this.tpgMain3_06.Padding = new System.Windows.Forms.Padding(3);
            this.tpgMain3_06.Size = new System.Drawing.Size(839, 676);
            this.tpgMain3_06.TabIndex = 5;
            this.tpgMain3_06.Text = "06";
            this.tpgMain3_06.UseVisualStyleBackColor = true;
            // 
            // tpgMain3_07
            // 
            this.tpgMain3_07.Location = new System.Drawing.Point(154, 4);
            this.tpgMain3_07.Name = "tpgMain3_07";
            this.tpgMain3_07.Padding = new System.Windows.Forms.Padding(3);
            this.tpgMain3_07.Size = new System.Drawing.Size(839, 676);
            this.tpgMain3_07.TabIndex = 6;
            this.tpgMain3_07.Text = "07";
            this.tpgMain3_07.UseVisualStyleBackColor = true;
            // 
            // tpgMain3_08
            // 
            this.tpgMain3_08.Location = new System.Drawing.Point(154, 4);
            this.tpgMain3_08.Name = "tpgMain3_08";
            this.tpgMain3_08.Padding = new System.Windows.Forms.Padding(3);
            this.tpgMain3_08.Size = new System.Drawing.Size(839, 676);
            this.tpgMain3_08.TabIndex = 7;
            this.tpgMain3_08.Text = "08";
            this.tpgMain3_08.UseVisualStyleBackColor = true;
            // 
            // tpgMain3_09
            // 
            this.tpgMain3_09.Location = new System.Drawing.Point(154, 4);
            this.tpgMain3_09.Name = "tpgMain3_09";
            this.tpgMain3_09.Padding = new System.Windows.Forms.Padding(3);
            this.tpgMain3_09.Size = new System.Drawing.Size(839, 676);
            this.tpgMain3_09.TabIndex = 8;
            this.tpgMain3_09.Text = "09";
            this.tpgMain3_09.UseVisualStyleBackColor = true;
            // 
            // tpgMain3_10
            // 
            this.tpgMain3_10.Location = new System.Drawing.Point(154, 4);
            this.tpgMain3_10.Name = "tpgMain3_10";
            this.tpgMain3_10.Padding = new System.Windows.Forms.Padding(3);
            this.tpgMain3_10.Size = new System.Drawing.Size(839, 676);
            this.tpgMain3_10.TabIndex = 9;
            this.tpgMain3_10.Text = "10";
            this.tpgMain3_10.UseVisualStyleBackColor = true;
            // 
            // tpgMain3_11
            // 
            this.tpgMain3_11.Location = new System.Drawing.Point(154, 4);
            this.tpgMain3_11.Name = "tpgMain3_11";
            this.tpgMain3_11.Size = new System.Drawing.Size(839, 676);
            this.tpgMain3_11.TabIndex = 10;
            this.tpgMain3_11.Text = "11";
            this.tpgMain3_11.UseVisualStyleBackColor = true;
            // 
            // tpgMain3_12
            // 
            this.tpgMain3_12.Location = new System.Drawing.Point(154, 4);
            this.tpgMain3_12.Name = "tpgMain3_12";
            this.tpgMain3_12.Size = new System.Drawing.Size(839, 676);
            this.tpgMain3_12.TabIndex = 11;
            this.tpgMain3_12.Text = "12";
            this.tpgMain3_12.UseVisualStyleBackColor = true;
            // 
            // tpgMain3_13
            // 
            this.tpgMain3_13.Location = new System.Drawing.Point(154, 4);
            this.tpgMain3_13.Name = "tpgMain3_13";
            this.tpgMain3_13.Size = new System.Drawing.Size(839, 676);
            this.tpgMain3_13.TabIndex = 12;
            this.tpgMain3_13.Text = "13";
            this.tpgMain3_13.UseVisualStyleBackColor = true;
            // 
            // tpgMain3_14
            // 
            this.tpgMain3_14.Location = new System.Drawing.Point(154, 4);
            this.tpgMain3_14.Name = "tpgMain3_14";
            this.tpgMain3_14.Size = new System.Drawing.Size(839, 676);
            this.tpgMain3_14.TabIndex = 13;
            this.tpgMain3_14.Text = "14";
            this.tpgMain3_14.UseVisualStyleBackColor = true;
            // 
            // tpgMain3_15
            // 
            this.tpgMain3_15.Location = new System.Drawing.Point(154, 4);
            this.tpgMain3_15.Name = "tpgMain3_15";
            this.tpgMain3_15.Size = new System.Drawing.Size(839, 676);
            this.tpgMain3_15.TabIndex = 14;
            this.tpgMain3_15.Text = "15";
            this.tpgMain3_15.UseVisualStyleBackColor = true;
            // 
            // ItemTabPage16
            // 
            this.ItemTabPage16.Location = new System.Drawing.Point(154, 4);
            this.ItemTabPage16.Name = "ItemTabPage16";
            this.ItemTabPage16.Size = new System.Drawing.Size(839, 676);
            this.ItemTabPage16.TabIndex = 15;
            this.ItemTabPage16.Text = "16";
            this.ItemTabPage16.UseVisualStyleBackColor = true;
            // 
            // tpgGyomu4
            // 
            this.tpgGyomu4.BackColor = System.Drawing.Color.Azure;
            this.tpgGyomu4.Font = new System.Drawing.Font("MS UI Gothic", 10F);
            this.tpgGyomu4.Location = new System.Drawing.Point(4, 34);
            this.tpgGyomu4.Name = "tpgGyomu4";
            this.tpgGyomu4.Padding = new System.Windows.Forms.Padding(3);
            this.tpgGyomu4.Size = new System.Drawing.Size(992, 550);
            this.tpgGyomu4.TabIndex = 3;
            this.tpgGyomu4.Text = "マスタメンテ";
            // 
            // tpgGyomu5
            // 
            this.tpgGyomu5.BackColor = System.Drawing.Color.LavenderBlush;
            this.tpgGyomu5.Font = new System.Drawing.Font("MS UI Gothic", 10F);
            this.tpgGyomu5.Location = new System.Drawing.Point(4, 34);
            this.tpgGyomu5.Name = "tpgGyomu5";
            this.tpgGyomu5.Padding = new System.Windows.Forms.Padding(3);
            this.tpgGyomu5.Size = new System.Drawing.Size(992, 550);
            this.tpgGyomu5.TabIndex = 6;
            // 
            // tpgGyomu6
            // 
            this.tpgGyomu6.Font = new System.Drawing.Font("MS UI Gothic", 10F);
            this.tpgGyomu6.Location = new System.Drawing.Point(4, 34);
            this.tpgGyomu6.Name = "tpgGyomu6";
            this.tpgGyomu6.Padding = new System.Windows.Forms.Padding(3);
            this.tpgGyomu6.Size = new System.Drawing.Size(992, 550);
            this.tpgGyomu6.TabIndex = 5;
            this.tpgGyomu6.UseVisualStyleBackColor = true;
            // 
            // tpgGyomu7
            // 
            this.tpgGyomu7.Font = new System.Drawing.Font("MS UI Gothic", 10F);
            this.tpgGyomu7.Location = new System.Drawing.Point(4, 34);
            this.tpgGyomu7.Name = "tpgGyomu7";
            this.tpgGyomu7.Padding = new System.Windows.Forms.Padding(3);
            this.tpgGyomu7.Size = new System.Drawing.Size(992, 550);
            this.tpgGyomu7.TabIndex = 4;
            this.tpgGyomu7.UseVisualStyleBackColor = true;
            // 
            // tpgGyomu8
            // 
            this.tpgGyomu8.Font = new System.Drawing.Font("MS UI Gothic", 10F);
            this.tpgGyomu8.Location = new System.Drawing.Point(4, 34);
            this.tpgGyomu8.Name = "tpgGyomu8";
            this.tpgGyomu8.Padding = new System.Windows.Forms.Padding(3);
            this.tpgGyomu8.Size = new System.Drawing.Size(992, 550);
            this.tpgGyomu8.TabIndex = 7;
            this.tpgGyomu8.UseVisualStyleBackColor = true;
            // 
            // tpgGyomu9
            // 
            this.tpgGyomu9.Location = new System.Drawing.Point(4, 34);
            this.tpgGyomu9.Name = "tpgGyomu9";
            this.tpgGyomu9.Padding = new System.Windows.Forms.Padding(3);
            this.tpgGyomu9.Size = new System.Drawing.Size(992, 550);
            this.tpgGyomu9.TabIndex = 8;
            this.tpgGyomu9.UseVisualStyleBackColor = true;
            // 
            // tpgGyomu10
            // 
            this.tpgGyomu10.Location = new System.Drawing.Point(4, 34);
            this.tpgGyomu10.Name = "tpgGyomu10";
            this.tpgGyomu10.Padding = new System.Windows.Forms.Padding(3);
            this.tpgGyomu10.Size = new System.Drawing.Size(992, 550);
            this.tpgGyomu10.TabIndex = 9;
            this.tpgGyomu10.UseVisualStyleBackColor = true;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox2.BackgroundImage")));
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox2.Location = new System.Drawing.Point(420, 32);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(30, 20);
            this.pictureBox2.TabIndex = 0;
            this.pictureBox2.TabStop = false;
            // 
            // button13
            // 
            this.button13.Location = new System.Drawing.Point(5, 649);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(65, 45);
            this.button13.TabIndex = 6;
            this.button13.Text = "Esc\r\n\r\n終了";
            this.button13.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button13.UseVisualStyleBackColor = true;
            // 
            // button14
            // 
            this.button14.Location = new System.Drawing.Point(95, 649);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(65, 45);
            this.button14.TabIndex = 7;
            this.button14.Text = "F1\r\n\r\n検索";
            this.button14.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button14.UseVisualStyleBackColor = true;
            // 
            // button15
            // 
            this.button15.Location = new System.Drawing.Point(166, 649);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(65, 45);
            this.button15.TabIndex = 8;
            this.button15.Text = "F2\r\n\r\n削除";
            this.button15.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button15.UseVisualStyleBackColor = true;
            // 
            // button16
            // 
            this.button16.Location = new System.Drawing.Point(237, 649);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(65, 45);
            this.button16.TabIndex = 9;
            this.button16.Text = "F3\r\n\r\n登録";
            this.button16.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button16.UseVisualStyleBackColor = true;
            // 
            // button17
            // 
            this.button17.Location = new System.Drawing.Point(308, 649);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(65, 45);
            this.button17.TabIndex = 10;
            this.button17.Text = "F4\r\n\r\n更新";
            this.button17.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button17.UseVisualStyleBackColor = true;
            // 
            // button18
            // 
            this.button18.Location = new System.Drawing.Point(400, 649);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(65, 45);
            this.button18.TabIndex = 11;
            this.button18.Text = "F5\r\n\r\n印刷";
            this.button18.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button18.UseVisualStyleBackColor = true;
            // 
            // button19
            // 
            this.button19.Location = new System.Drawing.Point(471, 649);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(65, 45);
            this.button19.TabIndex = 12;
            this.button19.Text = "F6\r\n\r\nプレビュー";
            this.button19.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button19.UseVisualStyleBackColor = true;
            // 
            // button20
            // 
            this.button20.Location = new System.Drawing.Point(542, 649);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(65, 45);
            this.button20.TabIndex = 13;
            this.button20.Text = "F7";
            this.button20.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button20.UseVisualStyleBackColor = true;
            // 
            // button21
            // 
            this.button21.Location = new System.Drawing.Point(613, 649);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(65, 45);
            this.button21.TabIndex = 14;
            this.button21.Text = "F8";
            this.button21.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button21.UseVisualStyleBackColor = true;
            // 
            // button22
            // 
            this.button22.Location = new System.Drawing.Point(708, 649);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(65, 45);
            this.button22.TabIndex = 15;
            this.button22.Text = "F9";
            this.button22.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button22.UseVisualStyleBackColor = true;
            // 
            // button23
            // 
            this.button23.Location = new System.Drawing.Point(779, 649);
            this.button23.Name = "button23";
            this.button23.Size = new System.Drawing.Size(65, 45);
            this.button23.TabIndex = 16;
            this.button23.Text = "F10";
            this.button23.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button23.UseVisualStyleBackColor = true;
            // 
            // button24
            // 
            this.button24.Location = new System.Drawing.Point(850, 649);
            this.button24.Name = "button24";
            this.button24.Size = new System.Drawing.Size(65, 45);
            this.button24.TabIndex = 17;
            this.button24.Text = "F11";
            this.button24.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button24.UseVisualStyleBackColor = true;
            // 
            // button25
            // 
            this.button25.Location = new System.Drawing.Point(921, 649);
            this.button25.Name = "button25";
            this.button25.Size = new System.Drawing.Size(65, 45);
            this.button25.TabIndex = 18;
            this.button25.Text = "F12\r\n\r\nプリンタ";
            this.button25.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button25.UseVisualStyleBackColor = true;
            // 
            // lblNowDate
            // 
            this.lblNowDate.AutoSize = true;
            this.lblNowDate.BackColor = System.Drawing.Color.Transparent;
            this.lblNowDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblNowDate.ForeColor = System.Drawing.Color.White;
            this.lblNowDate.Location = new System.Drawing.Point(728, 9);
            this.lblNowDate.Name = "lblNowDate";
            this.lblNowDate.Size = new System.Drawing.Size(130, 13);
            this.lblNowDate.TabIndex = 2;
            this.lblNowDate.Text = "平成25年11月28日";
            this.lblNowDate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pbxCalendar
            // 
            this.pbxCalendar.BackColor = System.Drawing.Color.Transparent;
            this.pbxCalendar.Image = ((System.Drawing.Image)(resources.GetObject("pbxCalendar.Image")));
            this.pbxCalendar.Location = new System.Drawing.Point(856, -1);
            this.pbxCalendar.Name = "pbxCalendar";
            this.pbxCalendar.Size = new System.Drawing.Size(28, 32);
            this.pbxCalendar.TabIndex = 21;
            this.pbxCalendar.TabStop = false;
            this.pbxCalendar.Click += new System.EventHandler(this.pbxCalendar_Click);
            // 
            // btnLogout
            // 
            this.btnLogout.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnLogout.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnLogout.BackgroundImage")));
            this.btnLogout.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnLogout.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnLogout.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnLogout.Location = new System.Drawing.Point(891, 2);
            this.btnLogout.Margin = new System.Windows.Forms.Padding(0);
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.Size = new System.Drawing.Size(100, 25);
            this.btnLogout.TabIndex = 3;
            this.btnLogout.Text = "ログアウト";
            this.btnLogout.UseVisualStyleBackColor = false;
            this.btnLogout.Click += new System.EventHandler(this.btnLogout_Click);
            // 
            // lblAccYear
            // 
            this.lblAccYear.AutoSize = true;
            this.lblAccYear.BackColor = System.Drawing.Color.Transparent;
            this.lblAccYear.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblAccYear.ForeColor = System.Drawing.Color.Black;
            this.lblAccYear.Location = new System.Drawing.Point(289, 7);
            this.lblAccYear.Name = "lblAccYear";
            this.lblAccYear.Size = new System.Drawing.Size(411, 16);
            this.lblAccYear.TabIndex = 1;
            this.lblAccYear.Text = "第 25 期 (自 平成25年 4月 1日 至 平成26年 3月31日)";
            this.lblAccYear.Click += new System.EventHandler(this.lblAccYear_Click);
            // 
            // Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SkyBlue;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(994, 698);
            this.Controls.Add(this.lblAccYear);
            this.Controls.Add(this.tctSubSystem);
            this.Controls.Add(this.tctGyomu);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.btnLogout);
            this.Controls.Add(this.pbxCalendar);
            this.Controls.Add(this.lblNowDate);
            this.Controls.Add(this.button25);
            this.Controls.Add(this.button24);
            this.Controls.Add(this.button23);
            this.Controls.Add(this.button22);
            this.Controls.Add(this.button21);
            this.Controls.Add(this.button20);
            this.Controls.Add(this.button19);
            this.Controls.Add(this.button18);
            this.Controls.Add(this.button17);
            this.Controls.Add(this.button16);
            this.Controls.Add(this.button15);
            this.Controls.Add(this.button14);
            this.Controls.Add(this.button13);
            this.Controls.Add(this.pbxCompanyLogo);
            this.Controls.Add(this.lblSystemName);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Menu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ERPシステム";
            ((System.ComponentModel.ISupportInitialize)(this.pbxCompanyLogo)).EndInit();
            this.tctSubSystem.ResumeLayout(false);
            this.tpgSubSystem3.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage5.ResumeLayout(false);
            this.tctGyomu.ResumeLayout(false);
            this.tpgGyomu1.ResumeLayout(false);
            this.tctMain1.ResumeLayout(false);
            this.tpgGyomu2.ResumeLayout(false);
            this.tctMain2.ResumeLayout(false);
            this.tpgGyomu3.ResumeLayout(false);
            this.tctMain3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxCalendar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbxCompanyLogo;
        private System.Windows.Forms.Label lblSystemName;
        private System.Windows.Forms.TabControl tctSubSystem;
        private System.Windows.Forms.TabPage tpgSubSystem2;
        private System.Windows.Forms.TabPage tpgSubSystem3;
        private System.Windows.Forms.TabPage tpgSubSystem4;
        private System.Windows.Forms.TabPage tpgGyomu1;
        private System.Windows.Forms.TabPage tpgGyomu2;
        private System.Windows.Forms.TabPage tpgGyomu3;
        private System.Windows.Forms.TabControl tctGyomu;
        private System.Windows.Forms.TabPage tpgGyomu4;
        private System.Windows.Forms.TabPage tpgGyomu7;
        private System.Windows.Forms.TabPage tpgGyomu6;
        private System.Windows.Forms.TabPage tpgGyomu5;
        private System.Windows.Forms.TabPage tpgGyomu8;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.Button button21;
        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.Button button23;
        private System.Windows.Forms.Button button24;
        private System.Windows.Forms.Button button25;
        private System.Windows.Forms.Label lblNowDate;
        private System.Windows.Forms.PictureBox pbxCalendar;
        private jp.co.fsi.common.FsiPanel panel2;
        private System.Windows.Forms.MonthCalendar monthCalendar;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Button button26;
        private System.Windows.Forms.Button button27;
        private System.Windows.Forms.Button button28;
        private System.Windows.Forms.Button button29;
        private System.Windows.Forms.Button button30;
        private System.Windows.Forms.Button button31;
        private System.Windows.Forms.Button button32;
        private System.Windows.Forms.Button button33;
        private System.Windows.Forms.Button button34;
        private System.Windows.Forms.Button button35;
        private System.Windows.Forms.Button button36;
        private System.Windows.Forms.Button button37;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.TabPage tabPage9;
        private System.Windows.Forms.TabPage tabPage10;
        private System.Windows.Forms.TabPage tabPage11;
        private System.Windows.Forms.TabPage tabPage12;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button btnLogout;
        private System.Windows.Forms.TabPage tpgSubSystem1;
        private System.Windows.Forms.TabControl tctMain3;
        private System.Windows.Forms.TabPage tpgMain3_01;
        private System.Windows.Forms.TabPage tpgMain3_02;
        private System.Windows.Forms.TabPage tpgMain3_03;
        private System.Windows.Forms.TabPage tpgMain3_04;
        private System.Windows.Forms.TabPage tpgMain3_05;
        private System.Windows.Forms.TabPage tpgMain3_06;
        private System.Windows.Forms.TabPage tpgMain3_07;
        private System.Windows.Forms.TabPage tpgMain3_08;
        private System.Windows.Forms.TabPage tpgMain3_09;
        private System.Windows.Forms.TabPage tpgMain3_10;
        private System.Windows.Forms.TabPage tpgMain3_11;
        private System.Windows.Forms.TabPage tpgMain3_12;
        private System.Windows.Forms.TabPage tpgMain3_13;
        private System.Windows.Forms.TabPage tpgMain3_14;
        private System.Windows.Forms.TabPage tpgMain3_15;
        private System.Windows.Forms.TabPage ItemTabPage16;
        private System.Windows.Forms.TabPage tpgGyomu9;
        private System.Windows.Forms.TabPage tpgGyomu10;
        private System.Windows.Forms.Label lblAccYear;
        private System.Windows.Forms.TabPage tpgSubSystem5;
        private System.Windows.Forms.TabPage tpgSubSystem6;
        private System.Windows.Forms.TabControl tctMain1;
        private System.Windows.Forms.TabPage tpgMain1_01;
        private System.Windows.Forms.TabPage tpgMain1_02;
        private System.Windows.Forms.TabPage tpgMain1_03;
        private System.Windows.Forms.TabPage tpgMain1_04;
        private System.Windows.Forms.TabPage tpgMain1_05;
        private System.Windows.Forms.TabPage tpgMain1_06;
        private System.Windows.Forms.TabPage tpgMain1_07;
        private System.Windows.Forms.TabPage tpgMain1_08;
        private System.Windows.Forms.TabPage tpgMain1_09;
        private System.Windows.Forms.TabPage tpgMain1_10;
        private System.Windows.Forms.TabPage tpgMain1_11;
        private System.Windows.Forms.TabPage tpgMain1_12;
        private System.Windows.Forms.TabPage tpgMain1_13;
        private System.Windows.Forms.TabPage tpgMain1_14;
        private System.Windows.Forms.TabPage tpgMain1_15;
        private System.Windows.Forms.TabPage tpgMain1_16;
        private System.Windows.Forms.TabControl tctMain2;
        private System.Windows.Forms.TabPage tpgMain2_01;
        private System.Windows.Forms.TabPage tpgMain2_02;
        private System.Windows.Forms.TabPage tpgMain2_03;
        private System.Windows.Forms.TabPage tpgMain2_04;
        private System.Windows.Forms.TabPage tpgMain2_05;
        private System.Windows.Forms.TabPage tpgMain2_06;
        private System.Windows.Forms.TabPage tpgMain2_07;
        private System.Windows.Forms.TabPage tpgMain2_08;
        private System.Windows.Forms.TabPage tpgMain2_09;
        private System.Windows.Forms.TabPage tpgMain2_10;
        private System.Windows.Forms.TabPage tpgMain2_11;
        private System.Windows.Forms.TabPage tpgMain2_12;
        private System.Windows.Forms.TabPage tpgMain2_13;
        private System.Windows.Forms.TabPage tpgMain2_14;
        private System.Windows.Forms.TabPage tpgMain2_15;
        private System.Windows.Forms.TabPage tpgMain2_16;
    }
}

