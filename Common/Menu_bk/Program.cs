﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace jp.co.fsi.common.menu
{
    static class Program
    {
        /// <summary>
        /// アプリケーションのメイン エントリ ポイントです。
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            //Application.DoEvents();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Menu());
        }
    }
}
