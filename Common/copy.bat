set excltxt="C:\work\SealaCube_ExCopy.txt"
set sourcedir="C:\SealaCUBE"
set destdir="(IPADDR)\SealaCUBE"

net use %destdir% (pass) /user:(uid)

xcopy /e /y /EXCLUDE:%excltxt% %sourcedir% %destdir%
