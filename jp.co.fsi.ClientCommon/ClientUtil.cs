﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace jp.co.fsi.ClientCommon
{
    public static class ClientUtil
    {


        public static void CircleForm(Form fm,int radi)
        {

            // フォームの境界線、タイトルバーを無しに設定
            fm.FormBorderStyle = FormBorderStyle.None;

            // フォームの不透明度を60%に設定（半透明化）
            fm.Opacity = 1;

            int radius = radi;
            int diameter = radius * 2;
            System.Drawing.Drawing2D.GraphicsPath gp = new System.Drawing.Drawing2D.GraphicsPath();

            // 左上
            gp.AddPie(0, 0, diameter, diameter, 180, 90);
            // 右上
            gp.AddPie(fm.Width - diameter, 0, diameter, diameter, 270, 90);
            // 左下
            gp.AddPie(0, fm.Height - diameter, diameter, diameter, 90, 90);
            // 右下
            gp.AddPie(fm.Width - diameter, fm.Height - diameter, diameter, diameter, 0, 90);
            // 中央
            gp.AddRectangle(new Rectangle(radius, 0, fm.Width - diameter, fm.Height));
            // 左
            gp.AddRectangle(new Rectangle(0, radius, radius, fm.Height - diameter));
            // 右
            gp.AddRectangle(new Rectangle(fm.Width - radius, radius, radius, fm.Height - diameter));

            fm.Region = new Region(gp);

        }

        public static void SetListThumbnail(string strPath,ListView ls,ImageList limg)
        {

            int ww = 72;
            int hh = 72;

            limg.Images.Clear();

            limg.ImageSize = new Size(ww, hh);

            ls.LargeImageList = limg;
            //ls.StateImageList = limg;

            Image orginInp = null;
            Image thumbInp = null;
            Image orginRep = null;
            Image thumbRep = null;

            Image orginInpc = null;
            Image thumbInpc = null;
            Image orginRepc = null;
            Image thumbRepc = null;

            try
            {
                orginInp = Bitmap.FromFile(@".\imgs\Keyborad.png");
                orginRep = Bitmap.FromFile(@".\imgs\printer.png");
                orginInpc = Bitmap.FromFile(@".\imgs\Keyboradc.png");
                orginRepc = Bitmap.FromFile(@".\imgs\printerc.png");

                thumbInp = CreateThumbnail(orginInp,ww,hh);
                thumbRep = CreateThumbnail(orginRep, ww, hh);
                thumbInpc = CreateThumbnail(orginInpc, ww, hh);
                thumbRepc = CreateThumbnail(orginRepc, ww, hh);

                limg.Images.Add(thumbInp);
                limg.Images.Add(thumbRep);
                limg.Images.Add(thumbInpc);
                limg.Images.Add(thumbRepc);

            }
            catch (Exception ex)
            {

                throw new Exception("画像処理でエラーが発生しました。", ex);

            }

        }


        private static Image CreateThumbnail(Image img,int w,int h)
        {

            Bitmap canvas = new Bitmap(w, h);

            Graphics g = Graphics.FromImage(canvas);

            try
            {

                g.FillRectangle(new SolidBrush(Color.Transparent), 0, 0, w, h);

                decimal fw = decimal.Parse(w.ToString()) / decimal.Parse(h.ToString());

                decimal fh = decimal.Parse(w.ToString()) / decimal.Parse(h.ToString());

                decimal scale = Math.Min(fw, fh);

                fw = img.Width * scale;
                fh = img.Height * scale;

                g.DrawImage(img, float.Parse(((w - fw) / 2).ToString()), float.Parse(((h - fh) / 2).ToString()), (float)fw, (float)fh);


            }
            catch(Exception ex)
            {

                throw new Exception("画像処理でエラーが発生しました。", ex);

            }
            finally
            {
                g.Dispose();
            }

            return canvas;

        }

    }


}
