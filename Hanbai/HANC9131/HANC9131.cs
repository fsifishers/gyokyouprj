﻿using System;
using System.ComponentModel;
using System.Data;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

using GrapeCity.ActiveReports;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.han.hanc9131
{
    /// <summary>
    /// 魚種マスタ一覧(HANC9131)
    /// </summary>
    public partial class HANC9131 : BasePgForm
    {
        #region プロパティ
        /// <summary>
        /// 画面上最後となるフォーカスのEnterボタン押下時処理用変数
        /// </summary>
        private bool _dtFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._dtFlg;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HANC9131()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region メソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // ボタンの位置調整
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF1.Location = this.btnF2.Location;
            this.btnF5.Location = this.btnF4.Location;
            this.btnF4.Location = this.btnF3.Location;
            this.btnF12.Location = this.btnF4.Location;
            // フォーカス設定
            this.txtGyoshuCdFr.Focus();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            //魚種CDに、
            // フォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtGyoshuCdFr":
                case "txtGyoshuCdTo":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;

            switch (this.ActiveCtlNm)
            {
                case "txtGyoshuCdFr":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom("HANC9061.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.han.hanc9061.HANC9061");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtGyoshuCdFr.Text = outData[0];
                            }
                        }
                    }
                    break;

                case "txtGyoshuCdTo":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom("HANC9061.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.han.hanc9061.HANC9061");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtGyoshuCdTo.Text = outData[0];
                            }
                        }
                    }
                    break;

                default:
                    break;
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
            {
                // プレビュー処理
                DoPrint(true);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("印刷", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }

        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            // 設定画面の起動
            // MEMO:原則としてここで渡す帳票IDの設定はReport.csvに保持していることが前提ですが、
            // 保持していない場合は、設定画面での保存(F6)時に新規に設定が保持されます。
            PrintSettingForm psForm = new PrintSettingForm(new string[1] { "HANC9131R" });
            psForm.ShowDialog();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 魚種コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGyoshuCdFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidGyoshuCdFr())
            {
                e.Cancel = true;
                this.txtGyoshuCdFr.SelectAll();
            }
        }

        /// <summary>
        /// 魚種コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGyoshuCdTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidGyoshuCdTo())
            {
                e.Cancel = true;
                this.txtGyoshuCdTo.SelectAll();

                // Enter処理を無効化
                this._dtFlg = false;
            }
            else
            {
                // Enter処理を有効化
                this._dtFlg = true;
            }
        }

        /// <summary>
        /// 魚種コード(至)のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGyoshuCdTo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.Flg)
            {
                // Enter処理を無効化
                this._dtFlg = false;

                // 全項目を再度入力値チェック
                if (!ValidateAll())
                {
                    // エラーありの場合ここで処理終了
                    return;
                }

                if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
                {
                    // ﾌﾟﾚﾋﾞｭｰ処理
                    DoPrint(true);
                }
                else
                {
                    this.txtGyoshuCdTo.Focus();
                }
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 魚種コードの入力チェック(先頭）
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidGyoshuCdFr()
        {
            // 未入力の場合「先頭」を表示
            if (ValChk.IsEmpty(this.txtGyoshuCdFr.Text))
            {
                this.lblGyoshuCdFr.Text = "先　頭";
                return true;
            }
            // 数字のみの入力を許可
            else if (!ValChk.IsNumber(this.txtGyoshuCdFr.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 存在するコードの場合はラベルに名称を表示する
            else
            {
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                dpc.SetParam("@SHOHIN_CD", SqlDbType.Decimal, 4, this.txtGyoshuCdFr.Text);
                dpc.SetParam("@BARCODE1", SqlDbType.VarChar, 13, "999");
                StringBuilder where = new StringBuilder("KAISHA_CD = @KAISHA_CD");
                where.Append(" AND SHOHIN_CD = @SHOHIN_CD");
                where.Append(" AND BARCODE1 = @BARCODE1");
                DataTable dtGyoshuDate =
                    this.Dba.GetDataTableByConditionWithParams("SHOHIN_NM",
                        "VI_HN_SHOHIN", Util.ToString(where), dpc);
                if (dtGyoshuDate.Rows.Count > 0)
                {
                    this.lblGyoshuCdFr.Text = dtGyoshuDate.Rows[0]["SHOHIN_NM"].ToString();
                }
                else
                {
                    this.lblGyoshuCdFr.Text = "";
                }
            }

            return true;
        }

        /// <summary>
        /// 魚種コードの入力チェック(最後）
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidGyoshuCdTo()
        {
            // 未入力の場合、「最後」を表示
            if (ValChk.IsEmpty(this.txtGyoshuCdTo.Text))
            {
                this.lblGyoshuCdTo.Text = "最　後";
                return true;
            }

            // 数字のみの入力を許可
            else if (!ValChk.IsNumber(this.txtGyoshuCdTo.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 存在するコードの場合はラベルに名称を表示する
            else
            {
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                dpc.SetParam("@SHOHIN_CD", SqlDbType.Decimal, 4, this.txtGyoshuCdTo.Text);
                dpc.SetParam("@BARCODE1", SqlDbType.VarChar, 13, "999");
                StringBuilder where = new StringBuilder("KAISHA_CD = @KAISHA_CD");
                where.Append(" AND SHOHIN_CD = @SHOHIN_CD");
                where.Append(" AND BARCODE1 = @BARCODE1");
                DataTable dtGyoshuDate =
                    this.Dba.GetDataTableByConditionWithParams("SHOHIN_NM",
                        "VI_HN_SHOHIN", Util.ToString(where), dpc);
                if (dtGyoshuDate.Rows.Count > 0)
                {
                    this.lblGyoshuCdTo.Text = dtGyoshuDate.Rows[0]["SHOHIN_NM"].ToString();
                }
                else
                {
                    this.lblGyoshuCdTo.Text = "";
                }
            }

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 魚種コードのチェック
            if (!IsValidGyoshuCdFr())
            {
                this.txtGyoshuCdFr.Focus();
                this.txtGyoshuCdFr.SelectAll();
                return false;
            }

            // 魚種コードのチェック
            if (!IsValidGyoshuCdTo())
            {
                this.txtGyoshuCdTo.Focus();
                this.txtGyoshuCdTo.SelectAll();
                return false;
            }

            return true;
        }

        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview)
        {
            try
            {
                this.Dba.BeginTransaction();

                // 帳票出力用にワークテーブルにデータを作成
                bool dataFlag = MakeWkData();

                // 帳票出力
                if (dataFlag)
                {
                    // 取得列の定義
                    StringBuilder cols = new StringBuilder();
                    cols.Append("  ITEM01");
                    cols.Append(" ,ITEM02");
                    cols.Append(" ,ITEM03");
                    cols.Append(" ,ITEM04");
                    cols.Append(" ,ITEM05");
                    cols.Append(" ,ITEM06");
                    cols.Append(" ,ITEM07");

                    // バインドパラメータの設定
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);

                    // データの取得
                    DataTable dtOutput = this.Dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_HN_TBL", "GUID = @GUID", "SORT ASC", dpc);

                    HANC9131R rpt = new HANC9131R(dtOutput);
                    if (isPreview)
                    {
                        // 帳票オブジェクトをインスタンス化
                        PreviewForm pFrm = new PreviewForm(rpt, this.UnqId);
                        // プレビュー画面表示
                        pFrm.WindowState = FormWindowState.Maximized;
                        pFrm.Show();
                    }
                    else
                    {
                        // 帳票オブジェクトをインスタンス化
                        // 直接印刷
                        rpt.Run(false);
                        rpt.Document.Print(true, true, false);
                    }
                }
            }
            finally
            {
                this.Dba.Rollback();
            }
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData()
        {
            #region データ取得の準備
            // 魚種コード設定
            string gyoshuCdFr;
            string gyoshuCdTo;
            string gyoshuNmFr;
            string gyoshuNmTo;
            if (Util.ToDecimal(txtGyoshuCdFr.Text) > 0)
            {
                gyoshuCdFr = txtGyoshuCdFr.Text;
            }
            else
            {
                gyoshuCdFr = "0";
            }
            if (Util.ToDecimal(txtGyoshuCdTo.Text) > 0)
            {
                gyoshuCdTo = txtGyoshuCdTo.Text;
            }
            else
            {
                gyoshuCdTo = "9999";
            }
            gyoshuNmFr = lblGyoshuCdFr.Text;
            gyoshuNmTo = lblGyoshuCdTo.Text;
            int i = 0; // ループ用カウント変数
            #endregion

            #region メインデータ取得
            // 入力された情報を元にワークテーブルに更新をする
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder sql = new StringBuilder();
            //TB_HN_SHOHINからデータ取得
            sql.Append("SELECT");
            sql.Append(" SHOHIN_CD,");
            sql.Append(" SHOHIN_NM,");
            sql.Append(" SHOHIN_KANA_NM ");
            sql.Append("FROM");
            sql.Append(" TB_HN_SHOHIN ");
            sql.Append("WHERE");
            sql.Append(" KAISHA_CD = @KAISHA_CD AND");
            sql.Append(" SHOHIN_CD BETWEEN @GYOSHU_CD_FR AND @GYOSHU_CD_TO AND");
            sql.Append(" BARCODE1 = 999 ");
            sql.Append("ORDER BY");
            sql.Append(" KAISHA_CD,");
            sql.Append(" SHOHIN_CD");
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@GYOSHU_CD_FR", SqlDbType.Decimal, 6, gyoshuCdFr);
            dpc.SetParam("@GYOSHU_CD_TO", SqlDbType.Decimal, 6, gyoshuCdTo);
            DataTable dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);
            #endregion

            if (dtMainLoop.Rows.Count == 0)
            {
                Msg.Info("該当データがありません。");
                return false;
            }
            else
            {
                while (dtMainLoop.Rows.Count > i)
                {
                    #region インサートテーブル
                    sql = new StringBuilder();
                    dpc = new DbParamCollection();
                    sql.Append("INSERT INTO PR_HN_TBL(");
                    sql.Append("  GUID");
                    sql.Append(" ,SORT");
                    sql.Append(" ,ITEM01");
                    sql.Append(" ,ITEM02");
                    sql.Append(" ,ITEM03");
                    sql.Append(" ,ITEM04");
                    sql.Append(" ,ITEM05");
                    sql.Append(" ,ITEM06");
                    sql.Append(" ,ITEM07");
                    sql.Append(") ");
                    sql.Append("VALUES(");
                    sql.Append("  @GUID");
                    sql.Append(" ,@SORT");
                    sql.Append(" ,@ITEM01");
                    sql.Append(" ,@ITEM02");
                    sql.Append(" ,@ITEM03");
                    sql.Append(" ,@ITEM04");
                    sql.Append(" ,@ITEM05");
                    sql.Append(" ,@ITEM06");
                    sql.Append(" ,@ITEM07");
                    sql.Append(") ");
                    #endregion

                    #region データ登録
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, i);
                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, gyoshuCdFr + "：" + gyoshuNmFr); // 魚種(自)
                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, gyoshuCdTo + "：" + gyoshuNmTo); // 魚種(至)
                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SHOHIN_CD"]); // コード
                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SHOHIN_NM"]);// 魚種名
                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SHOHIN_KANA_NM"]); // 魚種カナ名
                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, dtMainLoop.Rows.Count); // 総件数
                    this.Dba.ModifyBySql(Util.ToString(sql), dpc);
                    i++;
                    #endregion
                }
            }

            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
                "SORT",
                "PR_HN_TBL",
                "GUID = @GUID",
                dpc);

            bool dataFlag;
            if (tmpdtPR_HN_TBL.Rows.Count > 0)
            {
                dataFlag = true;
            }
            else
            {
                dataFlag = false;
            }

            return dataFlag;
        }
        #endregion
    }
}

