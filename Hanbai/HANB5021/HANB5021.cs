﻿using System;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.ComponentModel;
using System.Reflection;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;
using jp.co.fsi.common.constants;
using System.IO;
using System.Diagnostics;

namespace jp.co.fsi.han.hanb5021
{
    /// <summary>
    /// セリデータ転送(HANB5021)
    /// </summary>
    public partial class HANB5021 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// 文字コード(SJIS)
        /// </summary>
        private const int CHR_CD_SJIS = 932; // "Shift_JIS"と同じはず
        #endregion

        #region プロパティ
        /// <summary>
        /// 画面上最後となるフォーカスのEnterボタン押下時処理用変数
        /// </summary>
        private bool _dtFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._dtFlg;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HANB5021()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region メソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.InitForm();は呼び出さなくて構いません。
        /// また、このメソッド内の処理を外出しでこのクラス内にメソッド化するのは構いませんが、
        /// 原則、独自で起動時のイベント処理を実装することは禁じます。
        /// </remarks>
        protected override void InitForm()
        {
            // 水揚支所
            this.txtMizuageShishoCd.Text = "1";
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_HN_COMBO_DATA_MIZUAGE", this.txtMizuageShishoCd.Text);

            // 日付範囲の和暦設定
            string[] jpDate = Util.ConvJpDate(DateTime.Now, this.Dba);
            // 伝票日付
            this.lblGengo.Text = jpDate[0];
            this.txtYear.Text = jpDate[2];
            this.txtMonth.Text = jpDate[3];
            this.txtDay.Text = jpDate[4];

            // フォーカス設定
            this.txtYear.Focus();
            this.txtYear.Select();

            // Enter処理を無効化
            this._dtFlg = false;
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 水揚支所,日付(年)にフォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd":
                case "txtYear":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;
            String[] result;

            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd": // 水揚支所
                    result = this.openSearchWindow("COMC8011", "TB_HN_COMBO_DATA_MIZUAGE", this.txtMizuageShishoCd.Text);
                    if (!ValChk.IsEmpty(result[0]))
                    {
                        this.txtMizuageShishoCd.Text = result[0];
                        this.lblMizuageShishoNm.Text = result[1];
                    }
                    break;

                case "txtYear":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom("COMC9011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.com.comc9011.COMC9011");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblGengo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.lblGengo.Text = result[1];

                                // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
                                DateTime tmpDate = Util.ConvAdDate(this.lblGengo.Text, this.txtYear.Text,
                                    this.txtMonth.Text, "1", this.Dba);
                                int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);
                                if (Util.ToInt(this.txtDay.Text) > lastDayInMonth)
                                {
                                    this.txtDay.Text = Util.ToString(lastDayInMonth);
                                }

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblGengo.Text,
                                        this.txtYear.Text,
                                        this.txtMonth.Text,
                                        this.txtDay.Text,
                                        this.Dba);
                                this.lblGengo.Text = arrJpDate[0];
                                this.txtYear.Text = arrJpDate[2];
                                this.txtMonth.Text = arrJpDate[3];
                                this.txtDay.Text = arrJpDate[4];
                            }
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 会計年度の凍結処理チェック 凍結されていた場合はアラートを表示し処理を行なわない
            if (Util.GetKaikeiNendoFixedFlg(this.UInfo.KaikeiNendo, this.Dba))
            {
                Msg.Error("この会計年度は凍結されています。");
                return;
            }

            // 入力日付に該当する会計年度が選択会計年度と一致するかチェック
            DateTime DENPYO_DATE = Util.ConvAdDate(this.lblGengo.Text, this.txtYear.Text,
                    this.txtMonth.Text, this.txtDay.Text, this.Dba);
            if (Util.GetKaikeiNendo(DENPYO_DATE, this.Dba) != this.UInfo.KaikeiNendo)
            {
                Msg.Error("入力日付が選択会計年度の範囲外です。");
                return;
            }

            // 選択会計年度が今年度かチェック
            DateTime today = DateTime.Today;
            if (Util.GetKaikeiNendo(today, this.Dba) != this.UInfo.KaikeiNendo)
            {
                if (Msg.ConfNmYesNo("データ連携（更新）", "選択会計年度が今年度ではありませんが、宜しいですか？") == DialogResult.No)
                {
                    return;
                }
            }

            // 全入力項目チェック
            if (!this.ValidateAll())
            {
                return;
            }

            //ネットワークドライブ割り当て
            bool flg = NetUseStart();

            // セリ情報転送処理を実行
            if (flg)
            {
                getMasterData();
            }
        }
        #endregion

        #region イベント
        /// <summary>
        /// 水揚支所入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMizuageShishoCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                e.Cancel = true;
                this.txtMizuageShishoCd.SelectAll();
                this.txtMizuageShishoCd.Focus();
            }
        }

        /// <summary>
        /// (年)の入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtYear_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtYear.Text, this.txtYear.MaxLength))
            {
                e.Cancel = true;
                this.txtYear.SelectAll();
            }
            else
            {
                this.txtYear.Text = Util.ToString(IsValid.SetYear(this.txtYear.Text));
                CheckJp();
                SetJp();
            }
        }

        /// <summary>
        /// (月)の入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMonth_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtMonth.Text, this.txtMonth.MaxLength))
            {
                e.Cancel = true;
                this.txtMonth.SelectAll();
            }
            else
            {
                this.txtMonth.Text = Util.ToString(IsValid.SetMonth(this.txtMonth.Text));
                CheckJp();
                SetJp();
            }
        }

        /// <summary>
        /// (日)の入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDay_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsDay(this.txtDay.Text, this.txtDay.MaxLength))
            {
                e.Cancel = true;
                this.txtDay.SelectAll();

                // Enter処理を無効化
                this._dtFlg = false;
            }
            else
            {
                this.txtDay.Text = Util.ToString(IsValid.SetDay(this.txtDay.Text));
                CheckJp();
                SetJp();

                // Enter処理を有効化
                this._dtFlg = true;
            }
        }

        /// <summary>
        /// 日のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDay_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.Flg)
            {
                // Enter処理を無効化
                this._dtFlg = false;

                PressF4();
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 水揚支所の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool IsValidMizuageShishoCd()
        {
            // 空 又は 0入力の場合
            if (ValChk.IsEmpty(this.txtMizuageShishoCd.Text) || Equals(this.txtMizuageShishoCd.Text, "0"))
            {
                // 水揚支所名称を表示する
                this.txtMizuageShishoCd.Text = "0";
                this.lblMizuageShishoNm.Text = "全て";
                return true;
            }

            // 水揚支所名称を表示する
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_HN_COMBO_DATA_MIZUAGE", this.txtMizuageShishoCd.Text);

            if (ValChk.IsEmpty(this.lblMizuageShishoNm.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 年月日(自)の月末入力チェック
        /// </summary>
        /// 
        private void CheckJp()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblGengo.Text, this.txtYear.Text,
                this.txtMonth.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDay.Text) > lastDayInMonth)
            {
                this.txtDay.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(自)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetJp()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJp(Util.FixJpDate(this.lblGengo.Text, this.txtYear.Text,
                this.txtMonth.Text, this.txtDay.Text, this.Dba));
        }

        /// 全入力項目チェック
        /// </summary>
        /// <param name="modeFlg">追加モード=MODE_NEW , 更新モード=MODE_EDIT</param>
        /// <returns>true=OK, false=NG</returns>
        private bool ValidateAll()
        {
            // 水揚支所の入力チェック
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength))
            {
                this.txtMizuageShishoCd.Focus();
                this.txtMizuageShishoCd.SelectAll();
                return false;
            }

            // (年)の入力チェック
            if (!IsValid.IsYear(this.txtYear.Text, this.txtYear.MaxLength))
            {
                this.txtYear.Focus();
                this.txtYear.SelectAll();
                return false;
            }

            // (月)の入力チェック
            if (!IsValid.IsMonth(this.txtMonth.Text, this.txtMonth.MaxLength))
            {
                this.txtMonth.Focus();
                this.txtMonth.SelectAll();
                return false;
            }

            // (日)の入力チェック
            if (!IsValid.IsDay(this.txtDay.Text, this.txtDay.MaxLength))
            {
                this.txtDay.Focus();
                this.txtDay.SelectAll();
                return false;
            }

            this.txtYear.Text = Util.ToString(IsValid.SetYear(this.txtYear.Text));
            this.txtMonth.Text = Util.ToString(IsValid.SetMonth(this.txtMonth.Text));
            this.txtDay.Text = Util.ToString(IsValid.SetDay(this.txtDay.Text));
            // 年月日の月末入力チェック処理
            CheckJp();
            // 年月日の正しい和暦への変換処理
            SetJp();

            return true;
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJp(string[] arrJpDate)
        {
            this.lblGengo.Text = arrJpDate[0];
            this.txtYear.Text = arrJpDate[2];
            this.txtMonth.Text = arrJpDate[3];
            this.txtDay.Text = arrJpDate[4];
        }

        /// <summary>
        /// 検索サブウィンドウオープン
        /// </summary>
        /// <param name="moduleName">例："COMC8111"</param>
        /// <param name="para1">例："TB_HN_COMBO_DATA_MIZUAGE"</param>
        /// <param name="textBoxOfCode">例：this.txtGyogyoushuCd.Text</param>
        /// <param name="indata">例： this.txtGyogyoushuCd.Text</param>
        /// <returns>String[0]：検索結果から選択したコード , String[1]：検索結果から選択した名称</returns>
        private String[] openSearchWindow(String moduleName, String para1, String indata)
        {
            string[] result = { "", "" };

            // ネームスペースに使うモジュール名の小文字
            string lowerModuleName = moduleName.ToLower();

            // ネームスペースの末尾
            string nameSpace = lowerModuleName.Substring(0, 3);

            // アセンブリのロード
            Assembly asm = Assembly.LoadFrom(moduleName + ".exe");

            // フォーム作成
            string moduleNameSpace = "jp.co.fsi." + nameSpace + "." + lowerModuleName + "." + moduleName;
            Type t = asm.GetType(moduleNameSpace);

            if (t != null)
            {
                Object obj = Activator.CreateInstance(t);
                if (obj != null)
                {
                    BasePgForm frm = (BasePgForm)obj;
                    frm.Par1 = para1;
                    frm.InData = indata;
                    frm.ShowDialog(this);

                    if (frm.DialogResult == DialogResult.OK)
                    {
                        string[] ret = (string[])frm.OutData;
                        result[0] = ret[0];
                        result[1] = ret[1];
                        return result;
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// ネットワークドライブ接続
        /// <summary>
        /// <param name="seribi">セリ日</param>
        /// <returns>webセリ情報テーブル</returns>
        private bool NetUseStart()
        {
            // 変数宣言
            string Url;
            Uri dirPath;

            Url = this.Config.LoadPgConfig(Constants.SubSys.Han, "HANB1081", "Setting", "Start");
            dirPath = new Uri(Url); // 基準となるパス

            // 保存先の存在チェック
            if (!File.Exists(dirPath.LocalPath))
            {
                Msg.Info("ネットワークドライブ割り当てに失敗しました");
                return false;
            }
            else
            {
                ProcessStartInfo psInfo = new ProcessStartInfo();
                psInfo.FileName = Util.ToString(dirPath.LocalPath); // 実行するファイル
                psInfo.CreateNoWindow = true; // コンソール・ウィンドウを開かない
                psInfo.UseShellExecute = false; // シェル機能を使用しない

                Process.Start(psInfo);
                return true;
            }
        }

        /// <summary>
        /// ネットワークドライブ切断
        /// <summary>
        /// <param name="seribi">セリ日</param>
        /// <returns>webセリ情報テーブル</returns>
        private void NetUseDelete()
        {
            // 変数宣言
            string Url;
            Uri dirPath;

            Url = this.Config.LoadPgConfig(Constants.SubSys.Han, "HANB1081", "Setting", "Delete");
            dirPath = new Uri(Url); // 基準となるパス

            // 保存先の存在チェック
            if (!File.Exists(dirPath.LocalPath))
            {
                Msg.Info("ネットワークドライブ切断に失敗しました");
            }
            else
            {
                ProcessStartInfo psInfo = new ProcessStartInfo();
                psInfo.FileName = Util.ToString(dirPath.LocalPath); // 実行するファイル
                psInfo.CreateNoWindow = true; // コンソール・ウィンドウを開かない
                psInfo.UseShellExecute = false; // シェル機能を使用しない

                Process.Start(psInfo);
            }
        }

        /// <summary>
        /// セリデータ転送処理を実装
        /// </summary>
        private void getMasterData()
        {
            // ディレクトリは、送信パステーブル（TB_HN_SOSHIN_PATH）を参照する
            string dirPath = this.getSendFilePath("9999");
            if (ValChk.IsEmpty(dirPath))
            {
                Msg.Error("送信データの保存先を取得できませんでした。");
                return;
            }
            if (!System.IO.Directory.Exists(dirPath))
            {
                Msg.Error("送信データの保存先フォルダが見つかりません。");
                return;
            }

            try
            {
                // セリデータのファイル出力
                this.getSeriDt(dirPath);
            }
            catch (Exception e)
            {
                Msg.Notice(e.Message);
                return;
            }
            finally
            {
                // ネットワークドライブ切断
                NetUseDelete();
            }
        }
        
        /// <summary>
        /// セリデータ取得
        /// </summary>
        private void getSeriDt(string dirPath)
        {
            DateTime date = Util.ConvAdDate(this.lblGengo.Text, this.txtYear.Text, this.txtMonth.Text, this.txtDay.Text, this.Dba);
            String seribi = date.ToString("yyyy-MM-dd");
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder Sql = new StringBuilder();

            // セリデータを取得
            Sql.Append("SELECT");
            Sql.Append(" KAISHA_CD AS KAISHA_CD,"); // 会社CD
            Sql.Append(" YAMA_NO AS YAMA_NO,"); // 山NO
            Sql.Append(" SENSHU_CD AS SENSHU_CD,"); // 船主CD
            Sql.Append(" GYOHO_CD AS GYOHO_CD,"); // 漁法CD
            Sql.Append(" GYOSHU_CD AS GYOSHU_CD,"); // 魚種CD
            Sql.Append(" SURYO AS SURYO,"); // 数量
            Sql.Append(" TANKA AS TANKA,"); // 単価
            Sql.Append(" NAKAGAININ_CD AS NAKAGAININ_CD "); // 仲買人CD
            Sql.Append("FROM");
            Sql.Append(" VI_HN_SHIKIRI_MEISAI ");
            Sql.Append("WHERE");
            Sql.Append(" KAISHA_CD = @KAISHA_CD");
            Sql.Append(" AND KAIKEI_NENDO = @KAIKEI_NENDO");
            Sql.Append(" AND SERIBI = @SERIBI ");
            Sql.Append("ORDER BY");
            Sql.Append(" YAMA_NO,");
            Sql.Append(" GYO_NO");

            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            dpc.SetParam("@SERIBI", SqlDbType.VarChar, 10, seribi);

            DataTable dt = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            // セリデータが存在する場合、txtファイルを作成する
            if (dt.Rows.Count > 0)
            {
                // テキストファイル出力処理
                setSeriDt(dirPath, dt, seribi, "SERI_MEISAI.txt");
            }
            else
            {
                Msg.Info("該当する日付のデータが存在しません。");
            }
        }

        /// <summary>
        /// 船主/仲買にマスタデータのtxtファイル作成処理
        /// </summary>
        private void setSeriDt(string dirPath, DataTable dt, string seribi, string txtNm)
        {
            // --- これ以降は、テキストファイル出力処理 ---
            //CSVファイルに書き込むときに使うEncoding
            System.Text.Encoding enc =
                System.Text.Encoding.GetEncoding(CHR_CD_SJIS);

            // ファイルのストリーム宣言
            System.IO.StreamWriter sr;
            try
            {
                sr = new System.IO.StreamWriter(dirPath + seribi + "_" + txtNm, false, enc);
            }
            catch (Exception e)
            {
                Msg.Notice(e.Message);
                return;
            }

            try
            {
                //レコードを書き込む
                int i = 1;
                String yamaNo = "";
                int yamaSortNo = 0;
                foreach (DataRow row in dt.Rows)
                {
                    if (yamaNo != Util.ToString(row["YAMA_NO"]))
                    {
                        yamaSortNo = 0;
                    }

                    sr.Write(Util.ToString(row["KAISHA_CD"]) + '\t'); // 会社CD
                    sr.Write(seribi + '\t'); // セリ日
                    sr.Write("1" + '\t'); // セリ担当者
                    sr.Write(Util.ToString(i) + '\t'); // セリ番号
                    sr.Write(Util.ToString(row["YAMA_NO"]) + '\t'); // 山NO
                    sr.Write(Util.ToString(yamaSortNo) + '\t'); // 山ソートNO
                    sr.Write("0" + '\t'); // 山NO枝番
                    sr.Write(Util.ToString(row["SENSHU_CD"]) + '\t'); // 船主CD
                    sr.Write(Util.ToString(row["GYOHO_CD"]) + '\t'); // 漁法CD
                    sr.Write(Util.ToString(row["GYOSHU_CD"]) + '\t'); // 魚種CD
                    sr.Write(Util.ToString(row["SURYO"]) + '\t'); // 数量
                    sr.Write(Util.ToString(row["TANKA"]) + '\t'); // 単価
                    sr.Write(Util.ToString(row["NAKAGAININ_CD"]) + '\t'); // 仲買人CD
                    sr.Write("0" + '\t'); // 箱FLG
                    sr.Write("0" + '\t'); // 尾数
                    sr.Write("0" + '\t'); // 本数
                    sr.Write("1" + '\t'); // 処理フラグ
                    sr.Write(Util.ToString(i) + '\t'); // 親セリ番号
                    sr.Write("0"); // 子数
                    //sr.Write(Util.ToString(row["CLIENT_FLG"])); // 処理日時

                    //改行する
                    sr.Write("\r\n");
                    i++;
                    yamaSortNo++;
                    yamaNo = Util.ToString(row["YAMA_NO"]);
                }

                // 完了のメッセージを表示
                Msg.Info("データの書き込み処理を終了しました。");
            }
            catch (Exception e)
            {
                // エラーメッセージを表示
                Msg.Notice(e.Message);
                return;
            }
            finally
            {
                // ファイルのストリームを閉じる
                sr.Close();
            }
        }

        /// <summary>
        /// 送信ファイルの保存先ディレクトリ取得
        /// </summary>
        /// <param name="chikuCd">地区コード</param>
        /// <returns>string 保存先のディレクトリ</returns>
        private string getSendFilePath(string chikuCd)
        {
            string dir = string.Empty;

            DbParamCollection dpc = new DbParamCollection();

            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);

            // 検索する地区コードをセット
            dpc.SetParam("@CHIKU_CD", SqlDbType.Decimal, 4, chikuCd);

            string cols = "PATH";

            string from = "TB_HN_SOSHIN_PATH AS A";

            string where = "A.KAISHA_CD = @KAISHA_CD AND";
            where += "  A.CHIKU_CD = @CHIKU_CD";

            DataTable dtPath;
            try
            {
                // 保存先取得
                dtPath = this.Dba.GetDataTableByConditionWithParams(cols, from, where, dpc);
            }
            catch (Exception e)
            {
                Msg.Notice(e.Message);
                return dir;
            }

            if (dtPath.Rows.Count == 0 || ValChk.IsEmpty(dtPath.Rows[0]["PATH"]))
            {
                Msg.Error("保存先を取得できませんでした。");
            }
            else
            {
                dir = dtPath.Rows[0]["PATH"].ToString();
            }

            return dir;
        }
        #endregion
    }
}
