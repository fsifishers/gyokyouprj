﻿namespace jp.co.fsi.han.hanb1091
{
    partial class HANB1091
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgvInputList = new System.Windows.Forms.DataGridView();
            this.行 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.セリ日 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.船主CD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.船主名 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.水揚数量 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.税抜金額 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.消費税 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.税込金額 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ﾁｪｯｸ = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.伝票番号 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.仕切伝票番号 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlDebug.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInputList)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Text = "";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Size = new System.Drawing.Size(847, 100);
            // 
            // dgvInputList
            // 
            this.dgvInputList.AllowUserToAddRows = false;
            this.dgvInputList.AllowUserToDeleteRows = false;
            this.dgvInputList.AllowUserToResizeColumns = false;
            this.dgvInputList.AllowUserToResizeRows = false;
            this.dgvInputList.ColumnHeadersHeight = 27;
            this.dgvInputList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvInputList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.行,
            this.セリ日,
            this.船主CD,
            this.船主名,
            this.水揚数量,
            this.税抜金額,
            this.消費税,
            this.税込金額,
            this.ﾁｪｯｸ,
            this.伝票番号,
            this.仕切伝票番号});
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvInputList.DefaultCellStyle = dataGridViewCellStyle10;
            this.dgvInputList.Location = new System.Drawing.Point(14, 49);
            this.dgvInputList.MultiSelect = false;
            this.dgvInputList.Name = "dgvInputList";
            this.dgvInputList.RowHeadersVisible = false;
            this.dgvInputList.RowTemplate.Height = 21;
            this.dgvInputList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvInputList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvInputList.Size = new System.Drawing.Size(813, 320);
            this.dgvInputList.TabIndex = 1;
            this.dgvInputList.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvInputList_CellContentClick);
            // 
            // 行
            // 
            this.行.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            this.行.DefaultCellStyle = dataGridViewCellStyle1;
            this.行.Frozen = true;
            this.行.HeaderText = "行";
            this.行.Name = "行";
            this.行.ReadOnly = true;
            this.行.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.行.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.行.Width = 30;
            // 
            // セリ日
            // 
            this.セリ日.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            this.セリ日.DefaultCellStyle = dataGridViewCellStyle2;
            this.セリ日.Frozen = true;
            this.セリ日.HeaderText = "セリ日";
            this.セリ日.Name = "セリ日";
            this.セリ日.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.セリ日.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.セリ日.Width = 105;
            // 
            // 船主CD
            // 
            this.船主CD.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            this.船主CD.DefaultCellStyle = dataGridViewCellStyle3;
            this.船主CD.Frozen = true;
            this.船主CD.HeaderText = "船主CD";
            this.船主CD.Name = "船主CD";
            this.船主CD.ReadOnly = true;
            this.船主CD.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.船主CD.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.船主CD.Width = 70;
            // 
            // 船主名
            // 
            this.船主名.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            this.船主名.DefaultCellStyle = dataGridViewCellStyle4;
            this.船主名.Frozen = true;
            this.船主名.HeaderText = "船主名";
            this.船主名.Name = "船主名";
            this.船主名.ReadOnly = true;
            this.船主名.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.船主名.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.船主名.Width = 180;
            // 
            // 水揚数量
            // 
            this.水揚数量.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            this.水揚数量.DefaultCellStyle = dataGridViewCellStyle5;
            this.水揚数量.Frozen = true;
            this.水揚数量.HeaderText = "水揚数量";
            this.水揚数量.Name = "水揚数量";
            this.水揚数量.ReadOnly = true;
            this.水揚数量.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.水揚数量.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // 税抜金額
            // 
            this.税抜金額.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            this.税抜金額.DefaultCellStyle = dataGridViewCellStyle6;
            this.税抜金額.Frozen = true;
            this.税抜金額.HeaderText = "税抜金額";
            this.税抜金額.Name = "税抜金額";
            this.税抜金額.ReadOnly = true;
            this.税抜金額.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.税抜金額.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // 消費税
            // 
            this.消費税.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Black;
            this.消費税.DefaultCellStyle = dataGridViewCellStyle7;
            this.消費税.Frozen = true;
            this.消費税.HeaderText = "消費税";
            this.消費税.Name = "消費税";
            this.消費税.ReadOnly = true;
            this.消費税.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.消費税.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // 税込金額
            // 
            this.税込金額.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.Black;
            this.税込金額.DefaultCellStyle = dataGridViewCellStyle8;
            this.税込金額.Frozen = true;
            this.税込金額.HeaderText = "税込金額";
            this.税込金額.Name = "税込金額";
            this.税込金額.ReadOnly = true;
            this.税込金額.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.税込金額.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ﾁｪｯｸ
            // 
            this.ﾁｪｯｸ.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle9.NullValue = false;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.Black;
            this.ﾁｪｯｸ.DefaultCellStyle = dataGridViewCellStyle9;
            this.ﾁｪｯｸ.FalseValue = "0";
            this.ﾁｪｯｸ.Frozen = true;
            this.ﾁｪｯｸ.HeaderText = "ﾁｪｯｸ";
            this.ﾁｪｯｸ.Name = "ﾁｪｯｸ";
            this.ﾁｪｯｸ.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.ﾁｪｯｸ.TrueValue = "1";
            this.ﾁｪｯｸ.Visible = false;
            this.ﾁｪｯｸ.Width = 50;
            // 
            // 伝票番号
            // 
            this.伝票番号.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.伝票番号.Frozen = true;
            this.伝票番号.HeaderText = "伝票番号";
            this.伝票番号.Name = "伝票番号";
            this.伝票番号.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.伝票番号.Visible = false;
            // 
            // 仕切伝票番号
            // 
            this.仕切伝票番号.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.仕切伝票番号.Frozen = true;
            this.仕切伝票番号.HeaderText = "仕切伝票番号";
            this.仕切伝票番号.Name = "仕切伝票番号";
            this.仕切伝票番号.ReadOnly = true;
            this.仕切伝票番号.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.仕切伝票番号.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.仕切伝票番号.Visible = false;
            this.仕切伝票番号.Width = 5;
            // 
            // HANB1091
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 638);
            this.Controls.Add(this.dgvInputList);
            this.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "HANB1091";
            this.Text = "";
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.dgvInputList, 0);
            this.pnlDebug.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvInputList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvInputList;
        private System.Windows.Forms.DataGridViewTextBoxColumn 行;
        private System.Windows.Forms.DataGridViewTextBoxColumn セリ日;
        private System.Windows.Forms.DataGridViewTextBoxColumn 船主CD;
        private System.Windows.Forms.DataGridViewTextBoxColumn 船主名;
        private System.Windows.Forms.DataGridViewTextBoxColumn 水揚数量;
        private System.Windows.Forms.DataGridViewTextBoxColumn 税抜金額;
        private System.Windows.Forms.DataGridViewTextBoxColumn 消費税;
        private System.Windows.Forms.DataGridViewTextBoxColumn 税込金額;
        private System.Windows.Forms.DataGridViewCheckBoxColumn ﾁｪｯｸ;
        private System.Windows.Forms.DataGridViewTextBoxColumn 伝票番号;
        private System.Windows.Forms.DataGridViewTextBoxColumn 仕切伝票番号;


    }
}