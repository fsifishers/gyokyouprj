﻿using System;
using System.ComponentModel;
using System.Data;
using System.Reflection;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;
using jp.co.fsi.common.constants;

namespace jp.co.fsi.han.hanb1091
{
    /// <summary>
    /// データ連携(更新)(HANB1091)
    /// </summary>
    public partial class HANB1091 : BasePgForm
    {
        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HANB1091()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region 定数
        // 列の番号
        const int COL_GYO_NO = 0;                // 行No.
        const int COL_SERIBI = 1;                // セリ日
        const int COL_FUNANUSHI_CD = 2;          // 船主CD
        const int COL_FUNANUSHI_NM = 3;          // 船主名
        const int COL_SURYO = 4;                 // 水揚数量
        const int COL_ZEINUKI_KINGAKU = 5;       // 税抜金額
        const int COL_SHOHIZEI = 6;              // 消費税
        const int COL_ZEIKOMI_KINGAKU = 7;       // 税込金額
        const int COL_CHECK = 8;                 // チェック
        const int COL_DENPYO_BANGO = 9;          // 伝票番号
        const int COL_SHIKIRI_DENPYO_BANGO = 10; // 仕切伝票番号
        #endregion

        #region イベント
        /// <summary>
        /// データが既に登録済みか確認する
        /// </summary>
        private void dgvInputList_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            // チェック列以外は処理しない
            if (e.RowIndex == -1 || e.ColumnIndex != COL_CHECK)
            {
                return;
            }

            // 編集モードを終了する
            this.dgvInputList.EndEdit();

            if (this.dgvInputList[e.ColumnIndex, e.RowIndex].Value.ToString() == "1")
            {
                if (this.dgvInputList[COL_SHIKIRI_DENPYO_BANGO, e.RowIndex].Value.ToString() != "0")
                {
                    if (Msg.ConfNmYesNo("取込チェック", e.RowIndex + 1 + "行目のデータは登録済みです。\r\n上書き更新しますか？") == DialogResult.Yes)
                    {

                    }
                    else
                    {
                        // チェックを外す
                        this.dgvInputList[e.ColumnIndex, e.RowIndex].Value = "0";
                    }
                }
            }
        }
        #endregion

        #region メソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 仕切データ表示
            DispTmpData();
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        public override void PressF6()
        {
            // 会計年度の凍結処理チェック 凍結されていた場合はアラートを表示し処理を行なわない
            if (Util.GetKaikeiNendoFixedFlg(this.UInfo.KaikeiNendo, this.Dba))
            {
                Msg.Error("この会計年度は凍結されています。");
                return;
            }

            // 選択会計年度が今年度かチェック
            DateTime today = DateTime.Today;
            if (Util.GetKaikeiNendo(today, this.Dba) != this.UInfo.KaikeiNendo)
            {
                if (Msg.ConfNmYesNo("データ連携（更新）", "選択会計年度が今年度ではありませんが、宜しいですか？") == DialogResult.No)
                {
                    return;
                }
            }

            if (Msg.ConfNmYesNo("データ連携(確定)", "実行しますか？") == DialogResult.Yes)
            {
                // 更新処理
                UpdateShikiriData();
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// データグリッドビューに仕切データの表示をする
        /// </summary>
        private void DispTmpData()
        {
            // TMP仕切テーブルに存在するデータを取得
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT");
            sql.Append(" A.*,");
            sql.Append(" ISNULL(B.DENPYO_BANGO,0) AS SHIKIRI_DENPYO_BANGO,");
            sql.Append(" C.TORIHIKISAKI_NM AS SENSHU_NM ");
            sql.Append("FROM");
            sql.Append(" TB_HN_TMP_SHIKIRI_DATA AS A ");
            sql.Append("LEFT OUTER JOIN");
            sql.Append(" TB_HN_SHIKIRI_DATA AS B ");
            sql.Append("ON");
            sql.Append(" A.KAISHA_CD = B.KAISHA_CD  AND");
            sql.Append(" A.KAIKEI_NENDO = B.KAIKEI_NENDO AND");
            sql.Append(" B.DENPYO_KUBUN = 3 AND");
            sql.Append(" A.SEISAN_KUBUN = B.SEISAN_KUBUN AND");
            sql.Append(" A.DENPYO_BANGO = B.DENPYO_BANGO ");
            sql.Append("LEFT OUTER JOIN");
            sql.Append(" VI_HN_TORIHIKISAKI_JOHO AS C ");
            sql.Append("ON");
            sql.Append(" A.KAISHA_CD = C.KAISHA_CD AND");
            sql.Append(" A.SENSHU_CD = C.TORIHIKISAKI_CD ");
            sql.Append("ORDER BY");
            sql.Append(" A.SERIBI,");
            sql.Append(" A.DENPYO_BANGO");

            DataTable dtMainLoop = this.Dba.GetDataTableFromSql(Util.ToString(sql));

            // データグリッドビュー初期化
            this.dgvInputList.Rows.Clear();

            for (int i = 0; i < dtMainLoop.Rows.Count; i++)
            {
                // 列の追加
                this.dgvInputList.Rows.Add();

                // 列の高さ設定
                this.dgvInputList.Rows[i].Height = 27;

                // 列の値設定
                this.dgvInputList[COL_GYO_NO, i].Value = i + 1;
                this.dgvInputList[COL_SERIBI, i].Value = String.Format("{0:yyyy/MM/dd}", Util.ToDate(dtMainLoop.Rows[i]["SERIBI"]));
                this.dgvInputList[COL_FUNANUSHI_CD, i].Value = dtMainLoop.Rows[i]["SENSHU_CD"];
                this.dgvInputList[COL_FUNANUSHI_NM, i].Value = dtMainLoop.Rows[i]["SENSHU_NM"];
                this.dgvInputList[COL_SURYO, i].Value = Util.FormatNum(dtMainLoop.Rows[i]["MIZUAGE_SURYO"],2);
                this.dgvInputList[COL_ZEINUKI_KINGAKU, i].Value = Util.FormatNum(dtMainLoop.Rows[i]["ZEINUKI_MIZUAGE_KINGAKU"]);
                this.dgvInputList[COL_SHOHIZEI, i].Value = Util.FormatNum(dtMainLoop.Rows[i]["SHOHIZEIGAKU"]);
                this.dgvInputList[COL_ZEIKOMI_KINGAKU, i].Value = Util.FormatNum(dtMainLoop.Rows[i]["ZEIKOMI_MIZUAGE_KINGAKU"]);
                if (dtMainLoop.Rows[i]["NAGO_DENPYO_BANGO"].ToString() == "1")
                {
                    this.dgvInputList[COL_CHECK, i].Value = 1;
                }
                else
                {
                    this.dgvInputList[COL_CHECK, i].Value = 0;
                }
                this.dgvInputList[COL_DENPYO_BANGO, i].Value = dtMainLoop.Rows[i]["DENPYO_BANGO"];
                this.dgvInputList[COL_SHIKIRI_DENPYO_BANGO, i].Value = dtMainLoop.Rows[i]["SHIKIRI_DENPYO_BANGO"];
            }

            // フォント設定
            this.dgvInputList.DefaultCellStyle.Font = new Font("ＭＳ ゴシック", 11F);
            this.dgvInputList.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 11F);
            this.dgvInputList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.dgvInputList.Columns[COL_GYO_NO].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvInputList.Columns[COL_SERIBI].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this.dgvInputList.Columns[COL_FUNANUSHI_CD].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvInputList.Columns[COL_FUNANUSHI_NM].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this.dgvInputList.Columns[COL_SURYO].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvInputList.Columns[COL_ZEINUKI_KINGAKU].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvInputList.Columns[COL_SHOHIZEI].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvInputList.Columns[COL_ZEIKOMI_KINGAKU].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        }

        /// <summary>
        /// Tmpテーブルから本番テーブルに登録する
        /// </summary>
        private void UpdateShikiriData()
        {
            StringBuilder sql = new StringBuilder();
            StringBuilder sql1 = new StringBuilder();
            DbParamCollection dpc = new DbParamCollection();
            DbParamCollection dpc1 = new DbParamCollection();

            // 不正データチェック用変数
            bool improperData = false;

            try
            {
                // トランザクションの開始
                this.Dba.BeginTransaction();

                foreach (DataGridViewRow dr in this.dgvInputList.Rows)
                {
                    // 処理フラグのアップデート
                    sql = new StringBuilder();
                    dpc = new DbParamCollection();
                    sql.Append("UPDATE");
                    sql.Append(" TB_HN_TMP_SHIKIRI_DATA ");
                    sql.Append("SET");
                    sql.Append(" NAGO_DENPYO_BANGO = @NAGO_DENPYO_BANGO ");
                    sql.Append("WHERE");
                    sql.Append(" KAIKEI_NENDO = @KAIKEI_NENDO AND");
                    sql.Append(" DENPYO_BANGO = @DENPYO_BANGO");

                    dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
                    dpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 12, dr.Cells[COL_DENPYO_BANGO].Value);
                    dpc.SetParam("@NAGO_DENPYO_BANGO", SqlDbType.Decimal, 12, dr.Cells[COL_CHECK].Value);

                    this.Dba.ModifyBySql(Util.ToString(sql), dpc);
                }

                // データの正当性チェック
                sql = new StringBuilder();
                sql.Append("SELECT");
                sql.Append(" COUNT(*) AS HIT_COUNT ");
                sql.Append("FROM");
                sql.Append(" TB_HN_TMP_SHIKIRI_DATA ");
                sql.Append("WHERE");
                sql.Append(" SENSHU_CD = 0");

                DataTable dtDataCheck1 = this.Dba.GetDataTableFromSql(Util.ToString(sql));

                if (Util.ToDecimal(dtDataCheck1.Rows[0]["HIT_COUNT"]) > 0)
                {
                    improperData = true;
                }

                sql = new StringBuilder();
                sql.Append("SELECT");
                sql.Append(" COUNT(*) AS HIT_COUNT ");
                sql.Append("FROM");
                sql.Append(" TB_HN_TMP_SHIKIRI_DATA AS A ");
                sql.Append("LEFT OUTER JOIN");
                sql.Append(" TB_HN_TMP_SHIKIRI_MEISAI AS B ");
                sql.Append("ON");
                sql.Append(" A.KAIKEI_NENDO = B.KAIKEI_NENDO AND");
                sql.Append(" A.DENPYO_BANGO = B.DENPYO_BANGO ");
                sql.Append("WHERE");
                sql.Append(" B.DENPYO_BANGO IS NULL ");
                sql.Append("GROUP BY");
                sql.Append(" A.DENPYO_BANGO");

                DataTable dtDataCheck2 = this.Dba.GetDataTableFromSql(Util.ToString(sql));

                if (dtDataCheck2.Rows.Count > 0)
                {
                    improperData = true;
                }

                // 不正なデータがある場合は処理しない
                if (improperData == true)
                {
                    Msg.Error("不正なデータがあります。");
                    return;
                }

                // TMP仕切テーブルに存在するデータを取得
                sql = new StringBuilder();
                sql.Append("SELECT");
                sql.Append(" A.*,");
                sql.Append(" ISNULL(B.DENPYO_BANGO,0) AS SHIKIRI_DENPYO_BANGO,");
                sql.Append(" C.TORIHIKISAKI_NM AS SENSHU_NM,");
                sql.Append(" D.GYOHO_NM AS GYOHO_NM ");
                sql.Append("FROM");
                sql.Append(" TB_HN_TMP_SHIKIRI_DATA AS A ");
                sql.Append("LEFT OUTER JOIN");
                sql.Append(" TB_HN_SHIKIRI_DATA AS B ");
                sql.Append("ON");
                sql.Append(" A.KAISHA_CD = B.KAISHA_CD AND");
                sql.Append(" A.KAIKEI_NENDO = B.KAIKEI_NENDO AND");
                sql.Append(" B.DENPYO_KUBUN = 3 AND");
                sql.Append(" A.SEISAN_KUBUN = B.SEISAN_KUBUN AND");
                sql.Append(" A.DENPYO_BANGO = B.DENPYO_BANGO ");
                sql.Append("LEFT OUTER JOIN");
                sql.Append(" VI_HN_TORIHIKISAKI_JOHO AS C ");
                sql.Append("ON");
                sql.Append(" A.KAISHA_CD = C.KAISHA_CD AND");
                sql.Append(" A.SENSHU_CD = C.TORIHIKISAKI_CD ");
                sql.Append("LEFT OUTER JOIN");
                sql.Append(" TM_HN_GYOHO_MST AS D ");
                sql.Append("ON");
                sql.Append(" A.KAISHA_CD = D.KAISHA_CD AND");
                sql.Append(" A.GYOHO_CD = D.GYOHO_CD ");
                sql.Append("ORDER BY");
                sql.Append(" A.SERIBI,");
                sql.Append(" A.SENSHU_CD");

                DataTable dtTmpShikiri = this.Dba.GetDataTableFromSql(Util.ToString(sql));
                
                foreach (DataRow dr2 in dtTmpShikiri.Rows)
                {
                    DbParamCollection insParam;
                    decimal consumptionTax = 1; // 消費税率初期値（バグがわかりやすいように1%を設定している
                    // 伝票番号更新
                    //this.Dba.UpdateHNDenpyoNo(this.UInfo, 3, 0, Util.ToInt(dr2["DENPYO_BANGO"]));

                    // 仕切データ登録
                    // MIZUAGE_SHISHO_NM,NIUKENIN_RITSUは、他漁港との処理ではないので固定
                    insParam = new DbParamCollection();
                    insParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, dr2["KAISHA_CD"]);
                    insParam.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 5, 3);
                    insParam.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 6, dr2["DENPYO_BANGO"]);
                    insParam.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, dr2["KAIKEI_NENDO"]);
                    insParam.SetParam("@TORIHIKI_KUBUN1", SqlDbType.Decimal, 1, 1);
                    insParam.SetParam("@TORIHIKI_KUBUN2", SqlDbType.Decimal, 1, 1);
                    insParam.SetParam("@SERIBI", SqlDbType.DateTime, dr2["SERIBI"]);
                    insParam.SetParam("@MIZUAGE_SHISHO_KUBUN", SqlDbType.Decimal, 1, dr2["MIZUAGE_SHISHO_KUBUN"]);
                    insParam.SetParam("@MIZUAGE_SHISHO_NM", SqlDbType.VarChar, 40, "本所");
                    insParam.SetParam("@SENSHU_CD", SqlDbType.Decimal, 5, dr2["SENSHU_CD"]);
                    insParam.SetParam("@SENSHU_NM", SqlDbType.VarChar, 40, dr2["SENSHU_NM"]);
                    insParam.SetParam("@GYOGYO_TESURYO", SqlDbType.Decimal, 7, dr2["GYOKYO_TESURYO"]);
                    insParam.SetParam("@GYOHO_CD", SqlDbType.Decimal, 5, dr2["GYOHO_CD"]);
                    insParam.SetParam("@GYOHO_NM", SqlDbType.VarChar, 40, dr2["GYOHO_NM"]);
                    insParam.SetParam("@NIUKENIN_CD", SqlDbType.Decimal, 5, 0);// 0固定
                    insParam.SetParam("@NIUKENIN_NM", SqlDbType.VarChar, 40, "");// 空白固定
                    insParam.SetParam("@SEISAN_KUBUN", SqlDbType.Decimal, 1, dr2["SEISAN_KUBUN"]);
                    insParam.SetParam("@SEISAN_NO", SqlDbType.Decimal, 8, dr2["SEISAN_BANGO"]);
                    insParam.SetParam("@NIUKENIN_RITSU", SqlDbType.Decimal, 7, 0);
                    insParam.SetParam("@MIZUAGE_GOKEI_SURYO", SqlDbType.Decimal, 11, dr2["MIZUAGE_SURYO"]);
                    insParam.SetParam("@MIZUAGE_ZEINUKI_KINGAKU", SqlDbType.Decimal, 9, dr2["ZEINUKI_MIZUAGE_KINGAKU"]);
                    insParam.SetParam("@MIZUAGE_SHOHIZEIGAKU", SqlDbType.Decimal, 9, dr2["SHOHIZEIGAKU"]);
                    insParam.SetParam("@MIZUAGE_ZEIKOMI_KINGAKU", SqlDbType.Decimal, 9, dr2["ZEIKOMI_MIZUAGE_KINGAKU"]);
                    insParam.SetParam("@SHIWAKE_DENPYO_BANGO", SqlDbType.Decimal, 6, 0);
                    insParam.SetParam("@IKKATSU_DENPYO_BANGO", SqlDbType.Decimal, 6, 0);
                    insParam.SetParam("@KEIJO_NENGAPPI", SqlDbType.DateTime, dr2["KEIJO_NENBI"]);
                    insParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, DateTime.Now.Date);
                    insParam.SetParam("@SHORI_FLG", SqlDbType.Decimal, 1, 0);
                    insParam.SetParam("@HAKOSU", SqlDbType.Decimal, 2, dr2["HAKOSU"]);

                    // 処理実行
                    this.Dba.Insert("TB_HN_SHIKIRI_DATA", insParam);

                    // 仕切明細取得
                    sql = new StringBuilder();
                    dpc = new DbParamCollection();
                    sql.Append("SELECT");
                    sql.Append(" A.*,");
                    sql.Append(" B.SHOHIN_NM AS GYOSHU_NM,");
                    sql.Append(" B.SERI_JIGYO_KUBUN AS JIGYO_KUBUN,");
                    sql.Append(" B.SERI_ZEI_KUBUN AS ZEI_KUBUN,");
                    sql.Append(" B.SERI_SHIWAKE_CD AS SHIWAKE_CD,");
                    sql.Append(" C.TORIHIKISAKI_NM AS NAKAGAININ_NM,");
                    sql.Append(" D.ZEI_RITSU AS ZEI_RITSU ");
                    sql.Append("FROM");
                    sql.Append(" TB_HN_TMP_SHIKIRI_MEISAI AS A ");
                    sql.Append("LEFT OUTER JOIN");
                    sql.Append(" VI_HN_SHOHIN AS B ");
                    sql.Append("ON");
                    sql.Append(" A.KAISHA_CD = B.KAISHA_CD AND");
                    sql.Append(" A.GYOSHU_CD = B.SHOHIN_CD AND");
                    sql.Append(" B.BARCODE1 = 999 ");
                    sql.Append("LEFT OUTER JOIN");
                    sql.Append(" VI_HN_TORIHIKISAKI_JOHO AS C ");
                    sql.Append("ON");
                    sql.Append(" A.KAISHA_CD = C.KAISHA_CD AND");
                    sql.Append(" A.NAKAGAININ_CD = C.TORIHIKISAKI_CD ");
                    sql.Append("LEFT OUTER JOIN");
                    sql.Append("  TB_ZM_F_ZEI_KUBUN AS D ");
                    sql.Append("ON");
                    sql.Append(" B.SERI_ZEI_KUBUN = D.ZEI_KUBUN ");
                    sql.Append("WHERE");
                    sql.Append(" A.KAIKEI_NENDO = @KAIKEI_NENDO AND");
                    sql.Append(" A.DENPYO_BANGO = @DENPYO_BANGO");

                    dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, dr2["KAIKEI_NENDO"]);
                    dpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 6, dr2["DENPYO_BANGO"]);

                    DataTable dtTmpMeisai = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);

                    foreach (DataRow dr3 in dtTmpMeisai.Rows)
                    {
                        // 仕切明細登録
                        insParam = new DbParamCollection();
                        insParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, dr3["KAISHA_CD"]);
                        insParam.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 5, 3);
                        insParam.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 6, dr3["DENPYO_BANGO"]);
                        insParam.SetParam("@GYO_NO", SqlDbType.Decimal, 4, dr3["DENPYO_MEISAI_BANGO"]);
                        insParam.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, dr3["KAIKEI_NENDO"]);
                        insParam.SetParam("@YAMA_NO", SqlDbType.Decimal, 4, dr3["YAMA_NO"]);
                        insParam.SetParam("@GYOSHU_CD", SqlDbType.Decimal, 5, dr3["GYOSHU_CD"]);
                        insParam.SetParam("@GYOSHU_NM", SqlDbType.VarChar, 40, dr3["GYOSHU_NM"]);
                        insParam.SetParam("@SURYO", SqlDbType.Decimal, 11, dr3["SURYO"]);
                        insParam.SetParam("@TANKA", SqlDbType.VarChar, 9, dr3["TANKA"]);
                        insParam.SetParam("@KINGAKU", SqlDbType.Decimal, 9, dr3["KINGAKU"]);
                        insParam.SetParam("@SHOHIZEI", SqlDbType.Decimal, 9, dr3["SHOHIZEIGAKU"]);
                        insParam.SetParam("@SHOHINDAI", SqlDbType.Decimal, 9, 0);
                        insParam.SetParam("@SHOHINDAI_SHOHIZEI", SqlDbType.Decimal, 9, 0);
                        insParam.SetParam("@NAKAGAININ_CD", SqlDbType.Decimal, 5, dr3["NAKAGAININ_CD"]);
                        insParam.SetParam("@NAKAGAININ_NM", SqlDbType.VarChar, 40, dr3["NAKAGAININ_NM"]);
                        insParam.SetParam("@PAYAO_NO", SqlDbType.Decimal, 5, dr3["PAYAO_NO"]);
                        insParam.SetParam("@PAYAO_RITSU", SqlDbType.Decimal, 7, 0);
                        insParam.SetParam("@BUMON_CD", SqlDbType.Decimal, 5, 0);
                        insParam.SetParam("@JIGYO_KUBUN", SqlDbType.Decimal, 5, dr3["JIGYO_KUBUN"]);
                        insParam.SetParam("@ZEI_KUBUN", SqlDbType.Decimal, 5, this.getZeikubun());
                        // 消費税設定
                        consumptionTax = this.GetZeiritsu(Util.ToDate(dr2["SERIBI"]));
                        insParam.SetParam("@ZEI_RITSU", SqlDbType.Decimal, 5, consumptionTax);
                        insParam.SetParam("@SHIWAKE_CD", SqlDbType.Decimal, 5, dr3["SHIWAKE_CD"]);
                        insParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, DateTime.Now.Date);
                        insParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, null);
                        insParam.SetParam("@SHORI_FLG", SqlDbType.Decimal, 1, 0);
                        insParam.SetParam("@HONSU", SqlDbType.Decimal, 11, null);

                        // 処理実行
                        this.Dba.Insert("TB_HN_SHIKIRI_MEISAI", insParam);
                    }
                }

                // 仕切データ削除
                sql = new StringBuilder();
                sql.Append("TRUNCATE TABLE TB_HN_TMP_SHIKIRI_DATA");
                this.Dba.ModifyBySql(Util.ToString(sql), dpc);

                // 仕切明細削除
                sql = new StringBuilder();
                sql.Append("TRUNCATE TABLE TB_HN_TMP_SHIKIRI_MEISAI");
                this.Dba.ModifyBySql(Util.ToString(sql), dpc);

                //伝票番号更新 max値取得
                sql1 = new StringBuilder();
                sql1.Append("SELECT");
                sql1.Append(" MAX(DENPYO_BANGO) AS DENPYO_BANGO ");
                sql1.Append("FROM");
                sql1.Append(" TB_HN_SHIKIRI_DATA ");
                sql1.Append("WHERE");
                sql1.Append(" KAIKEI_NENDO = @KAIKEI_NENDO");
                dpc1.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
                DataTable dtMaxDenpyo = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql1), dpc1);

                if (dtMaxDenpyo.Rows.Count > 0)
                {
                    this.Dba.UpdateHNDenpyoNo(this.UInfo, 3, 0, Util.ToInt(dtMaxDenpyo.Rows[0]["DENPYO_BANGO"]));
                }
                else
                {
                    Msg.Info("伝票番号の更新に失敗しました");
                }

                // トランザクションをコミット
                this.Dba.Commit();
                Msg.Info("更新処理が正常に終了しました。");
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // 仕切データ再表示
            DispTmpData();
        }

        /// <summary>
        /// 税区分取得
        /// </summary>
        /// <returns></returns>
        private int getZeikubun()
        {
            // 税区分の初期値は、税率８％の区分にしておく
            int result = 50;

            string zeikubun = this.Config.LoadPgConfig(Constants.SubSys.Han, "HANE1011", "Setting", "ZeiKbn");

            if (!ValChk.IsEmpty(zeikubun))
            {
                result = Util.ToInt(zeikubun);
            }

            return result;
        }
        #endregion
    }
}
