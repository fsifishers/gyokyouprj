﻿using System.Data;
using jp.co.fsi.common.util;
using jp.co.fsi.common.report;

namespace jp.co.fsi.han.hanr2011
{
    /// <summary>
    /// HANR2011R の概要の説明です。
    /// </summary>
    public partial class HANR2011R : BaseReport
    {
        public HANR2011R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }
    }
}
