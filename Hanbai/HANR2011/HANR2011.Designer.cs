﻿namespace jp.co.fsi.han.hanr2011
{
    partial class HANR2011
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbxSeikyushoDate = new System.Windows.Forms.GroupBox();
            this.lblSeikyushoDateDay = new System.Windows.Forms.Label();
            this.lblSeikyushoDateMonth = new System.Windows.Forms.Label();
            this.lblSeikyushoDateYear = new System.Windows.Forms.Label();
            this.txtSeikyushoDateDay = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtSeikyushoDateYear = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtSeikyushoDateMonth = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblSeikyushoDateGengo = new System.Windows.Forms.Label();
            this.lblSeikyushoDate = new System.Windows.Forms.Label();
            this.gbxNakagaininCd = new System.Windows.Forms.GroupBox();
            this.lblNakagaininCdTo = new System.Windows.Forms.Label();
            this.lblCodeBet = new System.Windows.Forms.Label();
            this.txtNakagaininCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblNakagaininCdFr = new System.Windows.Forms.Label();
            this.txtNakagaininCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.gbxMizuageShisho = new System.Windows.Forms.GroupBox();
            this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMizuageShishoNm = new System.Windows.Forms.Label();
            this.lblMizuageShisho = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.gbxSeikyushoDate.SuspendLayout();
            this.gbxNakagaininCd.SuspendLayout();
            this.gbxMizuageShisho.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.TabIndex = 999;
            this.lblTitle.Text = "";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Size = new System.Drawing.Size(847, 100);
            // 
            // gbxSeikyushoDate
            // 
            this.gbxSeikyushoDate.Controls.Add(this.lblSeikyushoDateDay);
            this.gbxSeikyushoDate.Controls.Add(this.lblSeikyushoDateMonth);
            this.gbxSeikyushoDate.Controls.Add(this.lblSeikyushoDateYear);
            this.gbxSeikyushoDate.Controls.Add(this.txtSeikyushoDateDay);
            this.gbxSeikyushoDate.Controls.Add(this.txtSeikyushoDateYear);
            this.gbxSeikyushoDate.Controls.Add(this.txtSeikyushoDateMonth);
            this.gbxSeikyushoDate.Controls.Add(this.lblSeikyushoDateGengo);
            this.gbxSeikyushoDate.Controls.Add(this.lblSeikyushoDate);
            this.gbxSeikyushoDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.gbxSeikyushoDate.ForeColor = System.Drawing.Color.Black;
            this.gbxSeikyushoDate.Location = new System.Drawing.Point(12, 141);
            this.gbxSeikyushoDate.Name = "gbxSeikyushoDate";
            this.gbxSeikyushoDate.Size = new System.Drawing.Size(260, 77);
            this.gbxSeikyushoDate.TabIndex = 1;
            this.gbxSeikyushoDate.TabStop = false;
            this.gbxSeikyushoDate.Text = "請求書発効日";
            // 
            // lblSeikyushoDateDay
            // 
            this.lblSeikyushoDateDay.BackColor = System.Drawing.Color.Silver;
            this.lblSeikyushoDateDay.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblSeikyushoDateDay.ForeColor = System.Drawing.Color.Black;
            this.lblSeikyushoDateDay.Location = new System.Drawing.Point(208, 32);
            this.lblSeikyushoDateDay.Name = "lblSeikyushoDateDay";
            this.lblSeikyushoDateDay.Size = new System.Drawing.Size(20, 18);
            this.lblSeikyushoDateDay.TabIndex = 7;
            this.lblSeikyushoDateDay.Text = "日";
            this.lblSeikyushoDateDay.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSeikyushoDateMonth
            // 
            this.lblSeikyushoDateMonth.BackColor = System.Drawing.Color.Silver;
            this.lblSeikyushoDateMonth.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblSeikyushoDateMonth.ForeColor = System.Drawing.Color.Black;
            this.lblSeikyushoDateMonth.Location = new System.Drawing.Point(153, 32);
            this.lblSeikyushoDateMonth.Name = "lblSeikyushoDateMonth";
            this.lblSeikyushoDateMonth.Size = new System.Drawing.Size(15, 19);
            this.lblSeikyushoDateMonth.TabIndex = 5;
            this.lblSeikyushoDateMonth.Text = "月";
            this.lblSeikyushoDateMonth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSeikyushoDateYear
            // 
            this.lblSeikyushoDateYear.BackColor = System.Drawing.Color.Silver;
            this.lblSeikyushoDateYear.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblSeikyushoDateYear.ForeColor = System.Drawing.Color.Black;
            this.lblSeikyushoDateYear.Location = new System.Drawing.Point(100, 31);
            this.lblSeikyushoDateYear.Name = "lblSeikyushoDateYear";
            this.lblSeikyushoDateYear.Size = new System.Drawing.Size(17, 21);
            this.lblSeikyushoDateYear.TabIndex = 3;
            this.lblSeikyushoDateYear.Text = "年";
            this.lblSeikyushoDateYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtSeikyushoDateDay
            // 
            this.txtSeikyushoDateDay.AutoSizeFromLength = false;
            this.txtSeikyushoDateDay.DisplayLength = null;
            this.txtSeikyushoDateDay.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtSeikyushoDateDay.ForeColor = System.Drawing.Color.Black;
            this.txtSeikyushoDateDay.Location = new System.Drawing.Point(175, 31);
            this.txtSeikyushoDateDay.MaxLength = 2;
            this.txtSeikyushoDateDay.Name = "txtSeikyushoDateDay";
            this.txtSeikyushoDateDay.Size = new System.Drawing.Size(30, 20);
            this.txtSeikyushoDateDay.TabIndex = 6;
            this.txtSeikyushoDateDay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSeikyushoDateDay.Validating += new System.ComponentModel.CancelEventHandler(this.txtSeikyushoDateDay_Validating);
            // 
            // txtSeikyushoDateYear
            // 
            this.txtSeikyushoDateYear.AutoSizeFromLength = false;
            this.txtSeikyushoDateYear.DisplayLength = null;
            this.txtSeikyushoDateYear.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtSeikyushoDateYear.ForeColor = System.Drawing.Color.Black;
            this.txtSeikyushoDateYear.Location = new System.Drawing.Point(68, 31);
            this.txtSeikyushoDateYear.MaxLength = 2;
            this.txtSeikyushoDateYear.Name = "txtSeikyushoDateYear";
            this.txtSeikyushoDateYear.Size = new System.Drawing.Size(30, 20);
            this.txtSeikyushoDateYear.TabIndex = 2;
            this.txtSeikyushoDateYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSeikyushoDateYear.Validating += new System.ComponentModel.CancelEventHandler(this.txtSeikyushoDateYear_Validating);
            // 
            // txtSeikyushoDateMonth
            // 
            this.txtSeikyushoDateMonth.AutoSizeFromLength = false;
            this.txtSeikyushoDateMonth.DisplayLength = null;
            this.txtSeikyushoDateMonth.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtSeikyushoDateMonth.ForeColor = System.Drawing.Color.Black;
            this.txtSeikyushoDateMonth.Location = new System.Drawing.Point(121, 31);
            this.txtSeikyushoDateMonth.MaxLength = 2;
            this.txtSeikyushoDateMonth.Name = "txtSeikyushoDateMonth";
            this.txtSeikyushoDateMonth.Size = new System.Drawing.Size(30, 20);
            this.txtSeikyushoDateMonth.TabIndex = 4;
            this.txtSeikyushoDateMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSeikyushoDateMonth.Validating += new System.ComponentModel.CancelEventHandler(this.txtSeikyushoDateMonth_Validating);
            // 
            // lblSeikyushoDateGengo
            // 
            this.lblSeikyushoDateGengo.BackColor = System.Drawing.Color.Silver;
            this.lblSeikyushoDateGengo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSeikyushoDateGengo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblSeikyushoDateGengo.ForeColor = System.Drawing.Color.Black;
            this.lblSeikyushoDateGengo.Location = new System.Drawing.Point(23, 31);
            this.lblSeikyushoDateGengo.Name = "lblSeikyushoDateGengo";
            this.lblSeikyushoDateGengo.Size = new System.Drawing.Size(41, 21);
            this.lblSeikyushoDateGengo.TabIndex = 1;
            this.lblSeikyushoDateGengo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSeikyushoDate
            // 
            this.lblSeikyushoDate.BackColor = System.Drawing.Color.Silver;
            this.lblSeikyushoDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblSeikyushoDate.ForeColor = System.Drawing.Color.Black;
            this.lblSeikyushoDate.Location = new System.Drawing.Point(20, 28);
            this.lblSeikyushoDate.Name = "lblSeikyushoDate";
            this.lblSeikyushoDate.Size = new System.Drawing.Size(214, 27);
            this.lblSeikyushoDate.TabIndex = 1;
            this.lblSeikyushoDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // gbxNakagaininCd
            // 
            this.gbxNakagaininCd.Controls.Add(this.lblNakagaininCdTo);
            this.gbxNakagaininCd.Controls.Add(this.lblCodeBet);
            this.gbxNakagaininCd.Controls.Add(this.txtNakagaininCdFr);
            this.gbxNakagaininCd.Controls.Add(this.lblNakagaininCdFr);
            this.gbxNakagaininCd.Controls.Add(this.txtNakagaininCdTo);
            this.gbxNakagaininCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.gbxNakagaininCd.ForeColor = System.Drawing.Color.Black;
            this.gbxNakagaininCd.Location = new System.Drawing.Point(12, 235);
            this.gbxNakagaininCd.Name = "gbxNakagaininCd";
            this.gbxNakagaininCd.Size = new System.Drawing.Size(592, 84);
            this.gbxNakagaininCd.TabIndex = 2;
            this.gbxNakagaininCd.TabStop = false;
            this.gbxNakagaininCd.Text = "仲買人CD範囲";
            // 
            // lblNakagaininCdTo
            // 
            this.lblNakagaininCdTo.BackColor = System.Drawing.Color.Silver;
            this.lblNakagaininCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblNakagaininCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblNakagaininCdTo.ForeColor = System.Drawing.Color.Black;
            this.lblNakagaininCdTo.Location = new System.Drawing.Point(350, 36);
            this.lblNakagaininCdTo.Name = "lblNakagaininCdTo";
            this.lblNakagaininCdTo.Size = new System.Drawing.Size(202, 20);
            this.lblNakagaininCdTo.TabIndex = 4;
            this.lblNakagaininCdTo.Text = "最　後";
            this.lblNakagaininCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCodeBet
            // 
            this.lblCodeBet.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblCodeBet.ForeColor = System.Drawing.Color.Black;
            this.lblCodeBet.Location = new System.Drawing.Point(278, 34);
            this.lblCodeBet.Name = "lblCodeBet";
            this.lblCodeBet.Size = new System.Drawing.Size(18, 20);
            this.lblCodeBet.TabIndex = 2;
            this.lblCodeBet.Text = "～";
            this.lblCodeBet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtNakagaininCdFr
            // 
            this.txtNakagaininCdFr.AutoSizeFromLength = false;
            this.txtNakagaininCdFr.DisplayLength = null;
            this.txtNakagaininCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtNakagaininCdFr.ForeColor = System.Drawing.Color.Black;
            this.txtNakagaininCdFr.Location = new System.Drawing.Point(25, 34);
            this.txtNakagaininCdFr.MaxLength = 4;
            this.txtNakagaininCdFr.Name = "txtNakagaininCdFr";
            this.txtNakagaininCdFr.Size = new System.Drawing.Size(40, 20);
            this.txtNakagaininCdFr.TabIndex = 0;
            this.txtNakagaininCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtNakagaininCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtCodeFr_Validating);
            // 
            // lblNakagaininCdFr
            // 
            this.lblNakagaininCdFr.BackColor = System.Drawing.Color.Silver;
            this.lblNakagaininCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblNakagaininCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblNakagaininCdFr.ForeColor = System.Drawing.Color.Black;
            this.lblNakagaininCdFr.Location = new System.Drawing.Point(68, 35);
            this.lblNakagaininCdFr.Name = "lblNakagaininCdFr";
            this.lblNakagaininCdFr.Size = new System.Drawing.Size(202, 20);
            this.lblNakagaininCdFr.TabIndex = 1;
            this.lblNakagaininCdFr.Text = "先　頭";
            this.lblNakagaininCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtNakagaininCdTo
            // 
            this.txtNakagaininCdTo.AutoSizeFromLength = false;
            this.txtNakagaininCdTo.DisplayLength = null;
            this.txtNakagaininCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtNakagaininCdTo.ForeColor = System.Drawing.Color.Black;
            this.txtNakagaininCdTo.Location = new System.Drawing.Point(307, 35);
            this.txtNakagaininCdTo.MaxLength = 4;
            this.txtNakagaininCdTo.Name = "txtNakagaininCdTo";
            this.txtNakagaininCdTo.Size = new System.Drawing.Size(40, 20);
            this.txtNakagaininCdTo.TabIndex = 3;
            this.txtNakagaininCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtNakagaininCdTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNakagaininCdTo_KeyDown);
            this.txtNakagaininCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtCodeTo_Validating);
            // 
            // gbxMizuageShisho
            // 
            this.gbxMizuageShisho.Controls.Add(this.txtMizuageShishoCd);
            this.gbxMizuageShisho.Controls.Add(this.lblMizuageShishoNm);
            this.gbxMizuageShisho.Controls.Add(this.lblMizuageShisho);
            this.gbxMizuageShisho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.gbxMizuageShisho.ForeColor = System.Drawing.Color.Black;
            this.gbxMizuageShisho.Location = new System.Drawing.Point(12, 50);
            this.gbxMizuageShisho.Name = "gbxMizuageShisho";
            this.gbxMizuageShisho.Size = new System.Drawing.Size(381, 77);
            this.gbxMizuageShisho.TabIndex = 0;
            this.gbxMizuageShisho.TabStop = false;
            this.gbxMizuageShisho.Text = "水揚支所";
            // 
            // txtMizuageShishoCd
            // 
            this.txtMizuageShishoCd.AutoSizeFromLength = true;
            this.txtMizuageShishoCd.DisplayLength = null;
            this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtMizuageShishoCd.Location = new System.Drawing.Point(93, 33);
            this.txtMizuageShishoCd.MaxLength = 5;
            this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
            this.txtMizuageShishoCd.Size = new System.Drawing.Size(51, 20);
            this.txtMizuageShishoCd.TabIndex = 1;
            this.txtMizuageShishoCd.TabStop = false;
            this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
            // 
            // lblMizuageShishoNm
            // 
            this.lblMizuageShishoNm.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMizuageShishoNm.Location = new System.Drawing.Point(149, 33);
            this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
            this.lblMizuageShishoNm.Size = new System.Drawing.Size(212, 20);
            this.lblMizuageShishoNm.TabIndex = 2;
            this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMizuageShisho
            // 
            this.lblMizuageShisho.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShisho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShisho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblMizuageShisho.Location = new System.Drawing.Point(10, 31);
            this.lblMizuageShisho.Name = "lblMizuageShisho";
            this.lblMizuageShisho.Size = new System.Drawing.Size(354, 25);
            this.lblMizuageShisho.TabIndex = 0;
            this.lblMizuageShisho.Text = "水揚支所";
            this.lblMizuageShisho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // HANR2011
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 638);
            this.Controls.Add(this.gbxMizuageShisho);
            this.Controls.Add(this.gbxNakagaininCd);
            this.Controls.Add(this.gbxSeikyushoDate);
            this.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "HANR2011";
            this.Text = "";
            this.Controls.SetChildIndex(this.gbxSeikyushoDate, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.gbxNakagaininCd, 0);
            this.Controls.SetChildIndex(this.gbxMizuageShisho, 0);
            this.pnlDebug.ResumeLayout(false);
            this.gbxSeikyushoDate.ResumeLayout(false);
            this.gbxSeikyushoDate.PerformLayout();
            this.gbxNakagaininCd.ResumeLayout(false);
            this.gbxNakagaininCd.PerformLayout();
            this.gbxMizuageShisho.ResumeLayout(false);
            this.gbxMizuageShisho.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbxSeikyushoDate;
        private jp.co.fsi.common.controls.FsiTextBox txtSeikyushoDateYear;
        private System.Windows.Forms.Label lblSeikyushoDateGengo;
        private System.Windows.Forms.Label lblSeikyushoDate;
        private System.Windows.Forms.Label lblSeikyushoDateDay;
        private System.Windows.Forms.Label lblSeikyushoDateMonth;
        private System.Windows.Forms.Label lblSeikyushoDateYear;
        private jp.co.fsi.common.controls.FsiTextBox txtSeikyushoDateDay;
        private jp.co.fsi.common.controls.FsiTextBox txtSeikyushoDateMonth;
        private System.Windows.Forms.GroupBox gbxNakagaininCd;
        private System.Windows.Forms.Label lblNakagaininCdTo;
        private System.Windows.Forms.Label lblCodeBet;
        private common.controls.FsiTextBox txtNakagaininCdFr;
        private System.Windows.Forms.Label lblNakagaininCdFr;
        private common.controls.FsiTextBox txtNakagaininCdTo;
        private System.Windows.Forms.GroupBox gbxMizuageShisho;
        private common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private System.Windows.Forms.Label lblMizuageShisho;

    }
}