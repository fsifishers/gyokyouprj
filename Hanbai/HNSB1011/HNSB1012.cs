﻿using System.Data;
using System.Drawing;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.hn.hnsb1011
{
    /// <summary>
    /// 精算未処理一覧(HNSB1012)
    /// </summary>
    public partial class HNSB1012 : BasePgForm
    {
        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HNSB1012()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // タイトルは非表示
            this.lblTitle.Visible = false;
            // サイズを縮める
            this.Size = new Size(719, 364);
            // Escapeのみ表示
            this.btnEsc.Location = this.btnF1.Location;
            this.btnEnter.Visible = true;
            this.btnEnter.Location = this.btnF2.Location;
            this.btnF1.Visible = false;
            this.btnF2.Visible = false;
            this.btnF3.Visible = false;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF6.Visible = false;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;

            SearchData();
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }
        #endregion

        #region イベント
        /// <summary>
        /// グリッドでのキーダウン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                    ReturnVal();
            }
        }

        /// <summary>
        /// グリッドのセルダブルクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
                ReturnVal();
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// データを検索する
        /// </summary>
        private void SearchData()
        {
            string shishoCd = Util.ToString(this.InData);
            // データを取得して表示
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);

            string cols = "A.SEISAN_KUBUN AS 精算区分,";
            cols += " B.NM AS 精算区分名,";
            cols += " A.SERIBI AS セリ日,";
            cols += " A.SENSHU_CD AS 船主コード,";
            cols += " A.SENSHU_NM AS 船主名,";
            cols += " A.MIZUAGE_ZEINUKI_KINGAKU AS 税抜金額 ";
            string from = "TB_HN_SHIKIRI_DATA AS A ";
            from += "INNER JOIN";
            from += " TB_HN_COMBO_DATA AS B ";
            from += " ON B.KUBUN = 24 AND";
            from += "  A.SEISAN_KUBUN = B.CD";
            string where = "A.KAISHA_CD = @KAISHA_CD AND";
            where += " B.CD <> 0 AND";
            where += " ISNULL(A.SEISAN_NO,0) = 0";
            if (shishoCd != "0")
            {
                //支所コードが指定されていたら支所コードで抽出
                where += " AND A.SHISHO_CD = @SHISHO_CD";
            }
            string orderby = "A.SEISAN_KUBUN, A.SERIBI, A.SENSHU_CD";

            DataTable dtTantosha =
                this.Dba.GetDataTableByConditionWithParams(cols, from, where, orderby, dpc);

            // 該当データがなければエラーメッセージを表示
            if (dtTantosha.Rows.Count == 0)
            {
                    Msg.Info("該当データがありません。");
                dtTantosha.Rows.Add(dtTantosha.NewRow());
            }

            this.dgvList.DataSource = dtTantosha;

            // ユーザーによるソートを禁止させる
            foreach (DataGridViewColumn c in this.dgvList.Columns)
                c.SortMode = DataGridViewColumnSortMode.NotSortable;

            // フォントを設定する
            //this.dgvList.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F);
            this.dgvList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //this.dgvList.DefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F);

            // 列幅を設定する
            this.dgvList.Columns[0].Width = 50;
            this.dgvList.Columns[1].Width = 140;
            this.dgvList.Columns[2].Width = 110;
            this.dgvList.Columns[3].Width = 100;
            this.dgvList.Columns[4].Width = 130;
            this.dgvList.Columns[5].Width = 120;

            // 書式を設定する
            this.dgvList.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.dgvList.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.dgvList.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.dgvList.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvList.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvList.Columns[5].DefaultCellStyle.Format = "c";
        }

        /// <summary>
        /// 呼び出し元に戻り値を返す
        /// </summary>
        private void ReturnVal()
        {
            if (Util.ToString(this.dgvList.SelectedRows[0].Cells["船主コード"].Value) == "")
            {
                this.DialogResult = DialogResult.No;
                this.Close();
            }
            else
            {
                this.OutData = new string[4] { 
                Util.ToString(this.dgvList.SelectedRows[0].Cells["船主コード"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["船主名"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["セリ日"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["精算区分"].Value),
            };
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }
        #endregion
    }
}
