﻿namespace jp.co.fsi.hn.hnsb1011
{
    partial class HNSB1011
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblDay = new System.Windows.Forms.Label();
            this.lblMonth = new System.Windows.Forms.Label();
            this.lblYear = new System.Windows.Forms.Label();
            this.txtDay = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtYear = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtMonth = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblGengo = new System.Windows.Forms.Label();
            this.lblJp = new System.Windows.Forms.Label();
            this.lblFunanushiCdFr = new System.Windows.Forms.Label();
            this.lblFunanushiCdTo = new System.Windows.Forms.Label();
            this.txtFunanushiCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtFunanushiCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblFunanushiFr = new System.Windows.Forms.Label();
            this.lblSetsumei = new System.Windows.Forms.Label();
            this.lblSetsumei2 = new System.Windows.Forms.Label();
            this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMizuageShishoNm = new System.Windows.Forms.Label();
            this.lblMizuageShisho = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.txtSeisanKbn = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblSeisanKbnNm = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.panel4 = new jp.co.fsi.common.FsiPanel();
            this.panel3 = new jp.co.fsi.common.FsiPanel();
            this.lblCodeBetDate = new System.Windows.Forms.Label();
            this.panel2 = new jp.co.fsi.common.FsiPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new jp.co.fsi.common.FsiPanel();
            this.pnlDebug.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(7, 540);
            this.pnlDebug.Margin = new System.Windows.Forms.Padding(5);
            this.pnlDebug.Size = new System.Drawing.Size(1350, 133);
            // 
            // lblTitle
            // 
            this.lblTitle.ForeColor = System.Drawing.Color.Black;
            this.lblTitle.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(1340, 41);
            this.lblTitle.TabIndex = 999;
            this.lblTitle.Text = "";
            // 
            // lblDay
            // 
            this.lblDay.BackColor = System.Drawing.Color.Silver;
            this.lblDay.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDay.Location = new System.Drawing.Point(338, 6);
            this.lblDay.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDay.Name = "lblDay";
            this.lblDay.Size = new System.Drawing.Size(27, 24);
            this.lblDay.TabIndex = 7;
            this.lblDay.Tag = "CHANGE";
            this.lblDay.Text = "日";
            this.lblDay.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMonth
            // 
            this.lblMonth.BackColor = System.Drawing.Color.Silver;
            this.lblMonth.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMonth.Location = new System.Drawing.Point(271, 6);
            this.lblMonth.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMonth.Name = "lblMonth";
            this.lblMonth.Size = new System.Drawing.Size(20, 25);
            this.lblMonth.TabIndex = 5;
            this.lblMonth.Tag = "CHANGE";
            this.lblMonth.Text = "月";
            this.lblMonth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblYear
            // 
            this.lblYear.BackColor = System.Drawing.Color.Silver;
            this.lblYear.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblYear.Location = new System.Drawing.Point(205, 6);
            this.lblYear.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblYear.Name = "lblYear";
            this.lblYear.Size = new System.Drawing.Size(23, 28);
            this.lblYear.TabIndex = 3;
            this.lblYear.Tag = "CHANGE";
            this.lblYear.Text = "年";
            this.lblYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDay
            // 
            this.txtDay.AutoSizeFromLength = false;
            this.txtDay.DisplayLength = null;
            this.txtDay.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDay.Location = new System.Drawing.Point(295, 6);
            this.txtDay.Margin = new System.Windows.Forms.Padding(4);
            this.txtDay.MaxLength = 2;
            this.txtDay.Name = "txtDay";
            this.txtDay.Size = new System.Drawing.Size(39, 23);
            this.txtDay.TabIndex = 5;
            this.txtDay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDay.Validating += new System.ComponentModel.CancelEventHandler(this.txtDay_Validating);
            // 
            // txtYear
            // 
            this.txtYear.AutoSizeFromLength = false;
            this.txtYear.DisplayLength = null;
            this.txtYear.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtYear.Location = new System.Drawing.Point(158, 7);
            this.txtYear.Margin = new System.Windows.Forms.Padding(4);
            this.txtYear.MaxLength = 2;
            this.txtYear.Name = "txtYear";
            this.txtYear.Size = new System.Drawing.Size(39, 23);
            this.txtYear.TabIndex = 2;
            this.txtYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtYear.Validating += new System.ComponentModel.CancelEventHandler(this.txtYear_Validating);
            // 
            // txtMonth
            // 
            this.txtMonth.AutoSizeFromLength = false;
            this.txtMonth.DisplayLength = null;
            this.txtMonth.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMonth.Location = new System.Drawing.Point(229, 7);
            this.txtMonth.Margin = new System.Windows.Forms.Padding(4);
            this.txtMonth.MaxLength = 2;
            this.txtMonth.Name = "txtMonth";
            this.txtMonth.Size = new System.Drawing.Size(39, 23);
            this.txtMonth.TabIndex = 4;
            this.txtMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonth.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonth_Validating);
            // 
            // lblGengo
            // 
            this.lblGengo.BackColor = System.Drawing.Color.LightCyan;
            this.lblGengo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGengo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGengo.Location = new System.Drawing.Point(100, 7);
            this.lblGengo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblGengo.Name = "lblGengo";
            this.lblGengo.Size = new System.Drawing.Size(55, 24);
            this.lblGengo.TabIndex = 1;
            this.lblGengo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblJp
            // 
            this.lblJp.BackColor = System.Drawing.Color.Silver;
            this.lblJp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblJp.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblJp.Location = new System.Drawing.Point(0, 0);
            this.lblJp.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblJp.Name = "lblJp";
            this.lblJp.Size = new System.Drawing.Size(774, 39);
            this.lblJp.TabIndex = 0;
            this.lblJp.Tag = "CHANGE";
            this.lblJp.Text = "精 算 日";
            this.lblJp.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblFunanushiCdFr
            // 
            this.lblFunanushiCdFr.BackColor = System.Drawing.Color.LightCyan;
            this.lblFunanushiCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFunanushiCdFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFunanushiCdFr.Location = new System.Drawing.Point(158, 6);
            this.lblFunanushiCdFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblFunanushiCdFr.Name = "lblFunanushiCdFr";
            this.lblFunanushiCdFr.Size = new System.Drawing.Size(250, 24);
            this.lblFunanushiCdFr.TabIndex = 2;
            this.lblFunanushiCdFr.Text = "先  頭";
            this.lblFunanushiCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblFunanushiCdTo
            // 
            this.lblFunanushiCdTo.BackColor = System.Drawing.Color.LightCyan;
            this.lblFunanushiCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFunanushiCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFunanushiCdTo.Location = new System.Drawing.Point(505, 6);
            this.lblFunanushiCdTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblFunanushiCdTo.Name = "lblFunanushiCdTo";
            this.lblFunanushiCdTo.Size = new System.Drawing.Size(250, 24);
            this.lblFunanushiCdTo.TabIndex = 6;
            this.lblFunanushiCdTo.Text = "最  後";
            this.lblFunanushiCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFunanushiCdTo
            // 
            this.txtFunanushiCdTo.AutoSizeFromLength = false;
            this.txtFunanushiCdTo.DisplayLength = null;
            this.txtFunanushiCdTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFunanushiCdTo.Location = new System.Drawing.Point(449, 7);
            this.txtFunanushiCdTo.Margin = new System.Windows.Forms.Padding(4);
            this.txtFunanushiCdTo.MaxLength = 4;
            this.txtFunanushiCdTo.Name = "txtFunanushiCdTo";
            this.txtFunanushiCdTo.Size = new System.Drawing.Size(53, 23);
            this.txtFunanushiCdTo.TabIndex = 8;
            this.txtFunanushiCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFunanushiCdTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtFunanushiCdTo_KeyDown);
            this.txtFunanushiCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtFunanushiCdTo_Validating);
            // 
            // txtFunanushiCdFr
            // 
            this.txtFunanushiCdFr.AutoSizeFromLength = false;
            this.txtFunanushiCdFr.DisplayLength = null;
            this.txtFunanushiCdFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFunanushiCdFr.Location = new System.Drawing.Point(102, 7);
            this.txtFunanushiCdFr.Margin = new System.Windows.Forms.Padding(4);
            this.txtFunanushiCdFr.MaxLength = 4;
            this.txtFunanushiCdFr.Name = "txtFunanushiCdFr";
            this.txtFunanushiCdFr.Size = new System.Drawing.Size(53, 23);
            this.txtFunanushiCdFr.TabIndex = 7;
            this.txtFunanushiCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFunanushiCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtFunanushiCdFr_Validating);
            // 
            // lblFunanushiFr
            // 
            this.lblFunanushiFr.BackColor = System.Drawing.Color.Silver;
            this.lblFunanushiFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFunanushiFr.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblFunanushiFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFunanushiFr.Location = new System.Drawing.Point(0, 0);
            this.lblFunanushiFr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblFunanushiFr.Name = "lblFunanushiFr";
            this.lblFunanushiFr.Size = new System.Drawing.Size(774, 39);
            this.lblFunanushiFr.TabIndex = 0;
            this.lblFunanushiFr.Tag = "CHANGE";
            this.lblFunanushiFr.Text = "船 主 C D";
            this.lblFunanushiFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSetsumei
            // 
            this.lblSetsumei.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblSetsumei.ForeColor = System.Drawing.Color.Red;
            this.lblSetsumei.Location = new System.Drawing.Point(17, 63);
            this.lblSetsumei.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSetsumei.Name = "lblSetsumei";
            this.lblSetsumei.Size = new System.Drawing.Size(584, 36);
            this.lblSetsumei.TabIndex = 0;
            this.lblSetsumei.Text = "船主CDのデータ訂正をした場合は、セリ日全体で精算処理します。";
            this.lblSetsumei.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSetsumei2
            // 
            this.lblSetsumei2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblSetsumei2.ForeColor = System.Drawing.Color.Red;
            this.lblSetsumei2.Location = new System.Drawing.Point(285, 99);
            this.lblSetsumei2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSetsumei2.Name = "lblSetsumei2";
            this.lblSetsumei2.Size = new System.Drawing.Size(317, 36);
            this.lblSetsumei2.TabIndex = 1;
            this.lblSetsumei2.Text = "※控除入力を再度行って下さい。";
            this.lblSetsumei2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtMizuageShishoCd
            // 
            this.txtMizuageShishoCd.AutoSizeFromLength = true;
            this.txtMizuageShishoCd.DisplayLength = null;
            this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtMizuageShishoCd.Location = new System.Drawing.Point(102, 7);
            this.txtMizuageShishoCd.Margin = new System.Windows.Forms.Padding(4);
            this.txtMizuageShishoCd.MaxLength = 4;
            this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
            this.txtMizuageShishoCd.Size = new System.Drawing.Size(53, 23);
            this.txtMizuageShishoCd.TabIndex = 1;
            this.txtMizuageShishoCd.TabStop = false;
            this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
            // 
            // lblMizuageShishoNm
            // 
            this.lblMizuageShishoNm.BackColor = System.Drawing.Color.LightCyan;
            this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMizuageShishoNm.Location = new System.Drawing.Point(158, 6);
            this.lblMizuageShishoNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
            this.lblMizuageShishoNm.Size = new System.Drawing.Size(250, 24);
            this.lblMizuageShishoNm.TabIndex = 2;
            this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMizuageShisho
            // 
            this.lblMizuageShisho.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShisho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShisho.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblMizuageShisho.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMizuageShisho.Location = new System.Drawing.Point(0, 0);
            this.lblMizuageShisho.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMizuageShisho.Name = "lblMizuageShisho";
            this.lblMizuageShisho.Size = new System.Drawing.Size(774, 39);
            this.lblMizuageShisho.TabIndex = 0;
            this.lblMizuageShisho.Tag = "CHANGE";
            this.lblMizuageShisho.Text = "水揚支所";
            this.lblMizuageShisho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // progressBar1
            // 
            this.progressBar1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.progressBar1.Location = new System.Drawing.Point(0, 0);
            this.progressBar1.Margin = new System.Windows.Forms.Padding(4);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(774, 42);
            this.progressBar1.TabIndex = 1001;
            this.progressBar1.Visible = false;
            // 
            // txtSeisanKbn
            // 
            this.txtSeisanKbn.AutoSizeFromLength = true;
            this.txtSeisanKbn.DisplayLength = null;
            this.txtSeisanKbn.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtSeisanKbn.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtSeisanKbn.Location = new System.Drawing.Point(449, 8);
            this.txtSeisanKbn.Margin = new System.Windows.Forms.Padding(4);
            this.txtSeisanKbn.MaxLength = 5;
            this.txtSeisanKbn.Name = "txtSeisanKbn";
            this.txtSeisanKbn.Size = new System.Drawing.Size(48, 23);
            this.txtSeisanKbn.TabIndex = 6;
            this.txtSeisanKbn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSeisanKbn.Validating += new System.ComponentModel.CancelEventHandler(this.txtSeisanKbn_Validating);
            // 
            // lblSeisanKbnNm
            // 
            this.lblSeisanKbnNm.BackColor = System.Drawing.Color.LightCyan;
            this.lblSeisanKbnNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSeisanKbnNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblSeisanKbnNm.Location = new System.Drawing.Point(503, 6);
            this.lblSeisanKbnNm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSeisanKbnNm.Name = "lblSeisanKbnNm";
            this.lblSeisanKbnNm.Size = new System.Drawing.Size(250, 24);
            this.lblSeisanKbnNm.TabIndex = 6;
            this.lblSeisanKbnNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.panel4, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.panel3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(20, 138);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(782, 188);
            this.tableLayoutPanel1.TabIndex = 1002;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.progressBar1);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(4, 142);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(774, 42);
            this.panel4.TabIndex = 3;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.lblCodeBetDate);
            this.panel3.Controls.Add(this.lblFunanushiCdFr);
            this.panel3.Controls.Add(this.txtFunanushiCdFr);
            this.panel3.Controls.Add(this.lblFunanushiCdTo);
            this.panel3.Controls.Add(this.txtFunanushiCdTo);
            this.panel3.Controls.Add(this.lblFunanushiFr);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(4, 96);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(774, 39);
            this.panel3.TabIndex = 2;
            // 
            // lblCodeBetDate
            // 
            this.lblCodeBetDate.BackColor = System.Drawing.Color.White;
            this.lblCodeBetDate.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblCodeBetDate.ForeColor = System.Drawing.Color.Black;
            this.lblCodeBetDate.Location = new System.Drawing.Point(413, 6);
            this.lblCodeBetDate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCodeBetDate.Name = "lblCodeBetDate";
            this.lblCodeBetDate.Size = new System.Drawing.Size(34, 28);
            this.lblCodeBetDate.TabIndex = 3;
            this.lblCodeBetDate.Tag = "CHANGE";
            this.lblCodeBetDate.Text = "～";
            this.lblCodeBetDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.txtSeisanKbn);
            this.panel2.Controls.Add(this.lblSeisanKbnNm);
            this.panel2.Controls.Add(this.lblDay);
            this.panel2.Controls.Add(this.lblMonth);
            this.panel2.Controls.Add(this.lblGengo);
            this.panel2.Controls.Add(this.lblYear);
            this.panel2.Controls.Add(this.txtMonth);
            this.panel2.Controls.Add(this.txtDay);
            this.panel2.Controls.Add(this.txtYear);
            this.panel2.Controls.Add(this.lblJp);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(4, 50);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(774, 39);
            this.panel2.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Silver;
            this.label1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(365, 8);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 25);
            this.label1.TabIndex = 8;
            this.label1.Tag = "CHANGE";
            this.label1.Text = "精算区分";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txtMizuageShishoCd);
            this.panel1.Controls.Add(this.lblMizuageShishoNm);
            this.panel1.Controls.Add(this.lblMizuageShisho);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(4, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(774, 39);
            this.panel1.TabIndex = 0;
            // 
            // HNSB1011
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1340, 678);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.lblSetsumei2);
            this.Controls.Add(this.lblSetsumei);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Margin = new System.Windows.Forms.Padding(7);
            this.Name = "HNSB1011";
            this.Text = "";
            this.Controls.SetChildIndex(this.lblSetsumei, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.lblSetsumei2, 0);
            this.Controls.SetChildIndex(this.tableLayoutPanel1, 0);
            this.pnlDebug.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }
        #endregion
        private jp.co.fsi.common.controls.FsiTextBox txtYear;
        private System.Windows.Forms.Label lblGengo;
        private System.Windows.Forms.Label lblJp;
        private System.Windows.Forms.Label lblDay;
        private System.Windows.Forms.Label lblMonth;
        private System.Windows.Forms.Label lblYear;
        private jp.co.fsi.common.controls.FsiTextBox txtDay;
        private jp.co.fsi.common.controls.FsiTextBox txtMonth;
        private System.Windows.Forms.Label lblFunanushiCdFr;
        private jp.co.fsi.common.controls.FsiTextBox txtFunanushiCdFr;
        private System.Windows.Forms.Label lblFunanushiCdTo;
        private jp.co.fsi.common.controls.FsiTextBox txtFunanushiCdTo;
        private System.Windows.Forms.Label lblFunanushiFr;
        private System.Windows.Forms.Label lblSetsumei;
        private System.Windows.Forms.Label lblSetsumei2;
        private common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private System.Windows.Forms.Label lblMizuageShisho;
        private System.Windows.Forms.ProgressBar progressBar1;
        private common.controls.FsiTextBox txtSeisanKbn;
        private System.Windows.Forms.Label lblSeisanKbnNm;
        private jp.co.fsi.common.FsiTableLayoutPanel tableLayoutPanel1;
        private jp.co.fsi.common.FsiPanel panel4;
        private jp.co.fsi.common.FsiPanel panel3;
        private jp.co.fsi.common.FsiPanel panel2;
        private jp.co.fsi.common.FsiPanel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblCodeBetDate;
    }
}