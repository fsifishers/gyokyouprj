﻿using System.Data;

using jp.co.fsi.common.report;
using jp.co.fsi.common.util;
using GrapeCity.ActiveReports.SectionReportModel;
using System.Globalization;
using System;

namespace jp.co.fsi.hn.hnyr1081
{
    /// <summary>
    /// HNYR1081R の概要。
    /// </summary>
    public partial class HNYR1081R : BaseReport
    {
        public HNYR1081R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }

        private int count = 0;
        private int senshuCd = 0;
        private int senshuGokei = 0;//船主回数合計
        private int seijunGokei = 0;//正準回数合計
        private int ChikuGokei = 0;//地区回数合計
        private int AllGokei = 0;//総回数合計
        private void pageHeader_Format(object sender, System.EventArgs e)
        {
            ////西暦から和暦に変換
            //CultureInfo culture = new CultureInfo("ja-JP", true);
            //culture.DateTimeFormat.Calendar = new JapaneseCalendar();
            //txtToday.Text = DateTime.Now.ToString("ggyy年 M月 d日", culture);
        }

        private void detail_Format(object sender, EventArgs e)
        {
            if(count > 0)
            {
                this.txtCd.Visible = false;
                this.txtKumiaiin.Visible = false;
                this.txtkaisu.Value = 0;
            }
            else
            {
                this.txtCd.Visible = true;
                this.txtKumiaiin.Visible = true;
                
                senshuGokei += Util.ToInt(this.txtkaisu.Value);//船主回数合計
                seijunGokei += Util.ToInt(this.txtkaisu.Value);//正準回数合計
                ChikuGokei += Util.ToInt(this.txtkaisu.Value);//地区回数合計
                AllGokei += Util.ToInt(this.txtkaisu.Value);//総回数合計
            }
            count++;
            if (senshuCd == Util.ToInt(this.txtgyotaicd.Value))
            {
                this.txtgyotaicd.Visible = false;
            }
            else
            {
                this.txtgyotaicd.Visible = true;
            }
            //前回の船主CDを保存
            senshuCd = Util.ToInt(this.txtgyotaicd.Value);
        }

        private void grpHeadersenshu_Format(object sender, EventArgs e)
        {

            count = 0;
            senshuCd = 0;

            this.txtSenshuKaisu.Value = senshuGokei;
            this.txtChikuKaisu.Value = ChikuGokei;
            this.txtSeijunKaisu.Value = seijunGokei;
            senshuGokei = 0;
        }
        private void grpHeaderChiku_Format(object sender, EventArgs e)
        {
            this.txtSenshuKaisu.Value = senshuGokei;
            this.txtChikuKaisu.Value = ChikuGokei;
            this.txtSeijunKaisu.Value = seijunGokei;
            ChikuGokei = 0;

        }
        private void grpHeaderseijun_Format(object sender, EventArgs e)
        {
            this.txtSenshuKaisu.Value = senshuGokei;
            this.txtChikuKaisu.Value = ChikuGokei;
            this.txtSeijunKaisu.Value = seijunGokei;
            seijunGokei = 0;
        }
        private void reportFooter1_Format(object sender, EventArgs e)
        {

            this.txtAllKaisu.Value = AllGokei;
            //this.reportFooter1.NewPage = NewPage.Before;
        }


    }
}
