﻿namespace jp.co.fsi.han.hanc9061
{
    partial class HANC9062
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblGyosyuCd = new System.Windows.Forms.Label();
            this.txtGyosyuCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtGyosyuNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblGyosyuNm = new System.Windows.Forms.Label();
            this.txtGyosyuKanaNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblGyosyuKanaNm = new System.Windows.Forms.Label();
            this.txtShiwakeKbn = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShiwakeKbn = new System.Windows.Forms.Label();
            this.lblShiwakeKbnNm = new System.Windows.Forms.Label();
            this.lblJimotoShiwakeNm = new System.Windows.Forms.Label();
            this.txtJimotoShiwakeCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblJimotoShiwakeCd = new System.Windows.Forms.Label();
            this.lblGyosyuBunruiNm = new System.Windows.Forms.Label();
            this.txtGyosyuBunrui = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblGyosyuBunrui = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Size = new System.Drawing.Size(383, 23);
            this.lblTitle.TabIndex = 999;
            this.lblTitle.Text = "";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(5, 167);
            this.pnlDebug.Size = new System.Drawing.Size(416, 100);
            // 
            // lblGyosyuCd
            // 
            this.lblGyosyuCd.BackColor = System.Drawing.Color.Silver;
            this.lblGyosyuCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblGyosyuCd.Location = new System.Drawing.Point(12, 11);
            this.lblGyosyuCd.Name = "lblGyosyuCd";
            this.lblGyosyuCd.Size = new System.Drawing.Size(165, 25);
            this.lblGyosyuCd.TabIndex = 0;
            this.lblGyosyuCd.Text = "魚　種　C　D";
            this.lblGyosyuCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtGyosyuCd
            // 
            this.txtGyosyuCd.AutoSizeFromLength = false;
            this.txtGyosyuCd.DisplayLength = null;
            this.txtGyosyuCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtGyosyuCd.Location = new System.Drawing.Point(121, 14);
            this.txtGyosyuCd.MaxLength = 15;
            this.txtGyosyuCd.Name = "txtGyosyuCd";
            this.txtGyosyuCd.Size = new System.Drawing.Size(53, 20);
            this.txtGyosyuCd.TabIndex = 1;
            this.txtGyosyuCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGyosyuCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtGyosyuCd_Validating);
            // 
            // txtGyosyuNm
            // 
            this.txtGyosyuNm.AutoSizeFromLength = false;
            this.txtGyosyuNm.DisplayLength = null;
            this.txtGyosyuNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtGyosyuNm.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtGyosyuNm.Location = new System.Drawing.Point(121, 48);
            this.txtGyosyuNm.MaxLength = 40;
            this.txtGyosyuNm.Name = "txtGyosyuNm";
            this.txtGyosyuNm.Size = new System.Drawing.Size(225, 20);
            this.txtGyosyuNm.TabIndex = 3;
            this.txtGyosyuNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtGyosyuNm_Validating);
            // 
            // lblGyosyuNm
            // 
            this.lblGyosyuNm.BackColor = System.Drawing.Color.Silver;
            this.lblGyosyuNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblGyosyuNm.Location = new System.Drawing.Point(12, 45);
            this.lblGyosyuNm.Name = "lblGyosyuNm";
            this.lblGyosyuNm.Size = new System.Drawing.Size(337, 25);
            this.lblGyosyuNm.TabIndex = 2;
            this.lblGyosyuNm.Text = "魚　種　名";
            this.lblGyosyuNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtGyosyuKanaNm
            // 
            this.txtGyosyuKanaNm.AutoSizeFromLength = false;
            this.txtGyosyuKanaNm.DisplayLength = null;
            this.txtGyosyuKanaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtGyosyuKanaNm.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtGyosyuKanaNm.Location = new System.Drawing.Point(121, 81);
            this.txtGyosyuKanaNm.MaxLength = 15;
            this.txtGyosyuKanaNm.Name = "txtGyosyuKanaNm";
            this.txtGyosyuKanaNm.Size = new System.Drawing.Size(225, 20);
            this.txtGyosyuKanaNm.TabIndex = 5;
            this.txtGyosyuKanaNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtGyosyuKanaNm_Validating);
            // 
            // lblGyosyuKanaNm
            // 
            this.lblGyosyuKanaNm.BackColor = System.Drawing.Color.Silver;
            this.lblGyosyuKanaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblGyosyuKanaNm.Location = new System.Drawing.Point(12, 79);
            this.lblGyosyuKanaNm.Name = "lblGyosyuKanaNm";
            this.lblGyosyuKanaNm.Size = new System.Drawing.Size(337, 25);
            this.lblGyosyuKanaNm.TabIndex = 4;
            this.lblGyosyuKanaNm.Text = "魚 種 カ ナ 名";
            this.lblGyosyuKanaNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShiwakeKbn
            // 
            this.txtShiwakeKbn.AutoSizeFromLength = false;
            this.txtShiwakeKbn.DisplayLength = null;
            this.txtShiwakeKbn.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtShiwakeKbn.Location = new System.Drawing.Point(126, 115);
            this.txtShiwakeKbn.MaxLength = 6;
            this.txtShiwakeKbn.Name = "txtShiwakeKbn";
            this.txtShiwakeKbn.Size = new System.Drawing.Size(53, 20);
            this.txtShiwakeKbn.TabIndex = 7;
            this.txtShiwakeKbn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShiwakeKbn.Validating += new System.ComponentModel.CancelEventHandler(this.txtShiwakeKbn_Validating);
            // 
            // lblShiwakeKbn
            // 
            this.lblShiwakeKbn.BackColor = System.Drawing.Color.Silver;
            this.lblShiwakeKbn.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblShiwakeKbn.Location = new System.Drawing.Point(12, 113);
            this.lblShiwakeKbn.Name = "lblShiwakeKbn";
            this.lblShiwakeKbn.Size = new System.Drawing.Size(170, 25);
            this.lblShiwakeKbn.TabIndex = 6;
            this.lblShiwakeKbn.Text = "仕　訳　区　分";
            this.lblShiwakeKbn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShiwakeKbnNm
            // 
            this.lblShiwakeKbnNm.BackColor = System.Drawing.Color.Silver;
            this.lblShiwakeKbnNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblShiwakeKbnNm.Location = new System.Drawing.Point(185, 113);
            this.lblShiwakeKbnNm.Name = "lblShiwakeKbnNm";
            this.lblShiwakeKbnNm.Size = new System.Drawing.Size(211, 25);
            this.lblShiwakeKbnNm.TabIndex = 8;
            this.lblShiwakeKbnNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblJimotoShiwakeNm
            // 
            this.lblJimotoShiwakeNm.BackColor = System.Drawing.Color.Silver;
            this.lblJimotoShiwakeNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblJimotoShiwakeNm.Location = new System.Drawing.Point(185, 147);
            this.lblJimotoShiwakeNm.Name = "lblJimotoShiwakeNm";
            this.lblJimotoShiwakeNm.Size = new System.Drawing.Size(211, 25);
            this.lblJimotoShiwakeNm.TabIndex = 11;
            this.lblJimotoShiwakeNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtJimotoShiwakeCd
            // 
            this.txtJimotoShiwakeCd.AutoSizeFromLength = false;
            this.txtJimotoShiwakeCd.DisplayLength = null;
            this.txtJimotoShiwakeCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtJimotoShiwakeCd.Location = new System.Drawing.Point(126, 149);
            this.txtJimotoShiwakeCd.MaxLength = 6;
            this.txtJimotoShiwakeCd.Name = "txtJimotoShiwakeCd";
            this.txtJimotoShiwakeCd.Size = new System.Drawing.Size(53, 20);
            this.txtJimotoShiwakeCd.TabIndex = 10;
            this.txtJimotoShiwakeCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtJimotoShiwakeCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtJimotoShiwakeCd_Validating);
            // 
            // lblJimotoShiwakeCd
            // 
            this.lblJimotoShiwakeCd.BackColor = System.Drawing.Color.Silver;
            this.lblJimotoShiwakeCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblJimotoShiwakeCd.Location = new System.Drawing.Point(12, 147);
            this.lblJimotoShiwakeCd.Name = "lblJimotoShiwakeCd";
            this.lblJimotoShiwakeCd.Size = new System.Drawing.Size(170, 25);
            this.lblJimotoShiwakeCd.TabIndex = 9;
            this.lblJimotoShiwakeCd.Text = "地元仕訳コード";
            this.lblJimotoShiwakeCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblGyosyuBunruiNm
            // 
            this.lblGyosyuBunruiNm.BackColor = System.Drawing.Color.Silver;
            this.lblGyosyuBunruiNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblGyosyuBunruiNm.Location = new System.Drawing.Point(185, 182);
            this.lblGyosyuBunruiNm.Name = "lblGyosyuBunruiNm";
            this.lblGyosyuBunruiNm.Size = new System.Drawing.Size(211, 25);
            this.lblGyosyuBunruiNm.TabIndex = 14;
            this.lblGyosyuBunruiNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtGyosyuBunrui
            // 
            this.txtGyosyuBunrui.AutoSizeFromLength = false;
            this.txtGyosyuBunrui.DisplayLength = null;
            this.txtGyosyuBunrui.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtGyosyuBunrui.Location = new System.Drawing.Point(126, 184);
            this.txtGyosyuBunrui.MaxLength = 5;
            this.txtGyosyuBunrui.Name = "txtGyosyuBunrui";
            this.txtGyosyuBunrui.Size = new System.Drawing.Size(53, 20);
            this.txtGyosyuBunrui.TabIndex = 13;
            this.txtGyosyuBunrui.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGyosyuBunrui.Validating += new System.ComponentModel.CancelEventHandler(this.txtGyosyuBunrui_Validating);
            // 
            // lblGyosyuBunrui
            // 
            this.lblGyosyuBunrui.BackColor = System.Drawing.Color.Silver;
            this.lblGyosyuBunrui.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblGyosyuBunrui.Location = new System.Drawing.Point(12, 182);
            this.lblGyosyuBunrui.Name = "lblGyosyuBunrui";
            this.lblGyosyuBunrui.Size = new System.Drawing.Size(170, 25);
            this.lblGyosyuBunrui.TabIndex = 12;
            this.lblGyosyuBunrui.Text = "魚　種　分　類";
            this.lblGyosyuBunrui.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // HANC9062
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(408, 270);
            this.Controls.Add(this.lblGyosyuBunruiNm);
            this.Controls.Add(this.txtGyosyuBunrui);
            this.Controls.Add(this.lblGyosyuBunrui);
            this.Controls.Add(this.lblJimotoShiwakeNm);
            this.Controls.Add(this.txtJimotoShiwakeCd);
            this.Controls.Add(this.lblJimotoShiwakeCd);
            this.Controls.Add(this.lblShiwakeKbnNm);
            this.Controls.Add(this.txtShiwakeKbn);
            this.Controls.Add(this.lblShiwakeKbn);
            this.Controls.Add(this.txtGyosyuKanaNm);
            this.Controls.Add(this.lblGyosyuKanaNm);
            this.Controls.Add(this.txtGyosyuNm);
            this.Controls.Add(this.lblGyosyuNm);
            this.Controls.Add(this.txtGyosyuCd);
            this.Controls.Add(this.lblGyosyuCd);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "HANC9062";
            this.ShowFButton = true;
            this.Text = "";
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblGyosyuCd, 0);
            this.Controls.SetChildIndex(this.txtGyosyuCd, 0);
            this.Controls.SetChildIndex(this.lblGyosyuNm, 0);
            this.Controls.SetChildIndex(this.txtGyosyuNm, 0);
            this.Controls.SetChildIndex(this.lblGyosyuKanaNm, 0);
            this.Controls.SetChildIndex(this.txtGyosyuKanaNm, 0);
            this.Controls.SetChildIndex(this.lblShiwakeKbn, 0);
            this.Controls.SetChildIndex(this.txtShiwakeKbn, 0);
            this.Controls.SetChildIndex(this.lblShiwakeKbnNm, 0);
            this.Controls.SetChildIndex(this.lblJimotoShiwakeCd, 0);
            this.Controls.SetChildIndex(this.txtJimotoShiwakeCd, 0);
            this.Controls.SetChildIndex(this.lblJimotoShiwakeNm, 0);
            this.Controls.SetChildIndex(this.lblGyosyuBunrui, 0);
            this.Controls.SetChildIndex(this.txtGyosyuBunrui, 0);
            this.Controls.SetChildIndex(this.lblGyosyuBunruiNm, 0);
            this.pnlDebug.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblGyosyuCd;
        private jp.co.fsi.common.controls.FsiTextBox txtGyosyuCd;
        private jp.co.fsi.common.controls.FsiTextBox txtGyosyuNm;
        private System.Windows.Forms.Label lblGyosyuNm;
        private jp.co.fsi.common.controls.FsiTextBox txtGyosyuKanaNm;
        private System.Windows.Forms.Label lblGyosyuKanaNm;
        private jp.co.fsi.common.controls.FsiTextBox txtShiwakeKbn;
        private System.Windows.Forms.Label lblShiwakeKbn;
        private System.Windows.Forms.Label lblShiwakeKbnNm;
        private System.Windows.Forms.Label lblJimotoShiwakeNm;
        private common.controls.FsiTextBox txtJimotoShiwakeCd;
        private System.Windows.Forms.Label lblJimotoShiwakeCd;
        private System.Windows.Forms.Label lblGyosyuBunruiNm;
        private common.controls.FsiTextBox txtGyosyuBunrui;
        private System.Windows.Forms.Label lblGyosyuBunrui;


    }
}