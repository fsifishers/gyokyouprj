﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.han.hanc9061
{
    /// <summary>
    /// 魚種マスタメンテ(変更・追加)(HANC9062)
    /// </summary>
    public partial class HANC9062 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// モード(新規)
        /// </summary>
        private const string MODE_NEW = "1";

        /// <summary>
        /// モード(編集)
        /// </summary>
        private const string MODE_EDIT = "2";
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HANC9062()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="par1">引数1</param>
        public HANC9062(string par1)
            : base(par1)
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 引数：Par1／モード(1:新規、2:変更)、InData：担当者コード
            if (MODE_NEW.Equals(this.Par1))
            {
                // 新規モードの初期表示
                InitDispOnNew();
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // 編集モードの初期表示
                InitDispOnEdit();
            }
            else
            {
                // 不正な起動として閉じる
                Msg.Error("不正な起動です。終了します。");
                this.Close();
            }

            // タイトルは非表示
            this.lblTitle.Visible = false;
            // EscapeとF1のみ表示
            this.ShowFButton = true;
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF1.Location = this.btnF2.Location;
            this.btnF6.Location = this.btnF4.Location;
            this.btnF2.Visible = false;
            this.btnF3.Visible = true;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF6.Visible = true;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // フォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtShiwakeKbn":
                case "txtJimotoShiwakeCd":
                case "txtGyosyuBunrui":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            System.Reflection.Assembly asm;
            Type t;

            switch (this.ActiveCtlNm)
            {
                case "txtShiwakeKbn":
                case "txtJimotoShiwakeCd":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom("KOBC9051.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.kob.kobc9051.KOBC9053");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            if (this.ActiveCtlNm == "txtShiwakeKbn")
                            {
                                frm.InData = this.txtShiwakeKbn.Text;
                            }
                            else if (this.ActiveCtlNm == "txtJimotoShiwakeCd")
                            {
                                frm.InData = this.txtJimotoShiwakeCd.Text;
                            }
                            frm.Par1 = "3";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                if (this.ActiveCtlNm == "txtShiwakeKbn")
                                {
                                    this.txtShiwakeKbn.Text = outData[0];
                                    this.lblShiwakeKbnNm.Text = outData[1];
                                }
                                else if (this.ActiveCtlNm == "txtJimotoShiwakeCd")
                                {
                                    this.txtJimotoShiwakeCd.Text = outData[0];
                                    this.lblJimotoShiwakeNm.Text = outData[1];
                                }

                            }
                        }
                    }
                    break;
                
                case "txtGyosyuBunrui":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom("HANC9051.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.han.hanc9051.HANC9051");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.txtGyosyuBunrui.Text;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtGyosyuBunrui.Text = outData[0];
                                this.lblGyosyuBunruiNm.Text = outData[1];

                            }
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// F3キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF3();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF3()
        {
            if (MODE_NEW.Equals(Par1))
            {
                return;
            }

            if (Msg.ConfYesNo("削除しますか？") == DialogResult.No)
            {
                return;
            }

            try
            {
                // トランザクションの開始
                this.Dba.BeginTransaction();

                DbParamCollection whereParam;
                whereParam = new DbParamCollection();

                whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
                whereParam.SetParam("@SHOHIN_CD", SqlDbType.Decimal, 15, this.txtGyosyuCd.Text);
                whereParam.SetParam("@BARCODE1", SqlDbType.VarChar, 13, "999");
                this.Dba.Delete("TB_HN_SHOHIN",
                    "KAISHA_CD = @KAISHA_CD AND SHOHIN_CD = @SHOHIN_CD AND BARCODE1 = @BARCODE1",
                    whereParam);

                // トランザクションをコミット
                this.Dba.Commit();

                Msg.Info("削除しました。");
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF6();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF6()
        {
            // 確認メッセージを表示
            string msg = (MODE_NEW.Equals(this.Par1) ? "登録" : "更新") + "しますか？";
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            // 入力値をバインドパラメータとしてセットする
            ArrayList alParamsCmTanto = SetCmTantoParams();

            try
            {
                // トランザクションの開始
                this.Dba.BeginTransaction();

                if (MODE_NEW.Equals(this.Par1))
                {
                    // データ登録
                    // 共通.商品マスタ
                    this.Dba.Insert("TB_HN_SHOHIN", (DbParamCollection)alParamsCmTanto[0]);
                }
                else if (MODE_EDIT.Equals(this.Par1))
                {
                    // データ更新
                    // 共通.商品マスタ
                    this.Dba.Update("TB_HN_SHOHIN",
                        (DbParamCollection)alParamsCmTanto[1],
                        "KAISHA_CD = @KAISHA_CD AND SHOHIN_CD = @SHOHIN_CD AND BARCODE1 = @BARCODE1",
                        (DbParamCollection)alParamsCmTanto[0]);

                }

                // トランザクションをコミット
                this.Dba.Commit();

            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 魚種CDの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGyosyuCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidGyosyuCd())
            {
                e.Cancel = true;
                this.txtGyosyuCd.SelectAll();
            }
        }

        /// <summary>
        /// 魚種名の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGyosyuNm_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidGyosyuNm())
            {
                e.Cancel = true;
                this.txtGyosyuNm.SelectAll();
            }
        }

        /// <summary>
        /// 魚種カナ名の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGyosyuKanaNm_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidGyosyuKanaNm())
            {
                e.Cancel = true;
                this.txtGyosyuKanaNm.SelectAll();
            }
        }

        /// <summary>
        /// 仕訳区分の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShiwakeKbn_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShiwakeKbn())
            {
                e.Cancel = true;
                this.txtShiwakeKbn.SelectAll();
            }
        }

        /// <summary>
        /// 地元仕訳コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtJimotoShiwakeCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidJimotoShiwake())
            {
                e.Cancel = true;
                this.txtJimotoShiwakeCd.SelectAll();
            }
        }

        /// <summary>
        /// 魚種分類コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGyosyuBunrui_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidGyosyuBunrui())
            {
                e.Cancel = true;
                this.txtGyosyuBunrui.SelectAll();
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 新規モードの初期表示
        /// </summary>
        private void InitDispOnNew()
        {
            // 初期値、入力制御を実装

            // 魚種CDの初期値を取得
            // 魚種の中でのMAX+1を初期表示する
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@BARCODE1", SqlDbType.VarChar, 13, "999");
            StringBuilder where = new StringBuilder("KAISHA_CD = @KAISHA_CD");
            where.Append(" AND BARCODE1 = @BARCODE1");
            DataTable dtMaxTanto =
                this.Dba.GetDataTableByConditionWithParams("MAX(SHOHIN_CD) AS MAX_CD",
                    "TB_HN_SHOHIN", Util.ToString(where), dpc);
            if (dtMaxTanto.Rows.Count > 0 && !ValChk.IsEmpty(dtMaxTanto.Rows[0]["MAX_CD"]))
            {
                this.txtGyosyuCd.Text = Util.ToString(Util.ToInt(dtMaxTanto.Rows[0]["MAX_CD"]) + 1);
            }
            else
            {
                this.txtGyosyuCd.Text = "1";
            }

            // 仕訳区分の初期値を設定
            this.txtShiwakeKbn.Text = "0";
            // 地元仕訳コードの初期値を設定
            this.txtJimotoShiwakeCd.Text = "0";
            // 魚種分類の初期値を設定
            this.txtGyosyuBunrui.Text = "0";

            // 魚種CDに初期フォーカス
            this.ActiveControl = this.txtGyosyuCd;
            this.txtGyosyuCd.Focus();

            // 削除ボタン非表示
            this.btnF3.Enabled = false;
        }

        /// <summary>
        /// 編集モードの初期表示
        /// </summary>
        private void InitDispOnEdit()
        {
            // 現在DBに登録されている値、入力制御を実装
            StringBuilder cols = new StringBuilder();
            cols.Append("KAISHA_CD");
            cols.Append("    ,SHOHIN_CD");
            cols.Append("    ,SHOHIN_NM");
            cols.Append("    ,SHOHIN_KANA_NM");
            cols.Append("    ,SERI_SHIWAKE_CD");
            cols.Append("    ,JIMOTO_SHIWAKE_CD");
            cols.Append("    ,SHOHIN_KUBUN1");

            StringBuilder from = new StringBuilder();
            from.Append("TB_HN_SHOHIN");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHOHIN_CD", SqlDbType.Decimal, 4, Util.ToString(this.InData));
            dpc.SetParam("@BARCODE1", SqlDbType.VarChar, 13, "999");

            DataTable dtDispData =
                this.Dba.GetDataTableByConditionWithParams(
                    Util.ToString(cols), Util.ToString(from),
                    "KAISHA_CD = @KAISHA_CD AND SHOHIN_CD = @SHOHIN_CD AND BARCODE1 = @BARCODE1",
                    dpc);

            if (dtDispData.Rows.Count == 0)
            {
                Msg.Error("不正な起動です。終了します。");
                this.Close();
            }

            // 取得した内容を表示
            DataRow drDispData = dtDispData.Rows[0];
            this.txtGyosyuCd.Text = Util.ToString(drDispData["SHOHIN_CD"]);
            this.txtGyosyuNm.Text = Util.ToString(drDispData["SHOHIN_NM"]);
            this.txtGyosyuKanaNm.Text = Util.ToString(drDispData["SHOHIN_KANA_NM"]);
            this.txtShiwakeKbn.Text = Util.ToString(drDispData["SERI_SHIWAKE_CD"]);
            this.lblShiwakeKbnNm.Text = this.GetZidShiwakeSetteiBNm(this.txtShiwakeKbn.Text);
            this.txtJimotoShiwakeCd.Text = Util.ToString(drDispData["JIMOTO_SHIWAKE_CD"]);
            this.lblJimotoShiwakeNm.Text = this.GetZidShiwakeSetteiBNm(this.txtJimotoShiwakeCd.Text);
            this.txtGyosyuBunrui.Text = Util.ToString(drDispData["SHOHIN_KUBUN1"]);
            this.lblGyosyuBunruiNm.Text = this.GetGyoshuBunruiMstNm(this.txtGyosyuBunrui.Text);

            // 魚種CDは入力不可
            this.txtGyosyuCd.Focus();
            this.txtGyosyuCd.Enabled = false;
            this.ActiveControl = this.txtGyosyuCd;
            this.txtGyosyuNm.Focus();
        }

        /// <summary>
        /// 魚種CDの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidGyosyuCd()
        {
            // 未入力はエラーとする
            if (ValChk.IsEmpty(this.txtGyosyuCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtGyosyuCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            if (MODE_NEW.Equals(this.Par1))
            {
                // 既に存在するコードを入力した場合はエラーとする
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                dpc.SetParam("@SHOHIN_CD", SqlDbType.Decimal, 15, this.txtGyosyuCd.Text);
                dpc.SetParam("@BARCODE1", SqlDbType.VarChar, 13, "999");
                StringBuilder where = new StringBuilder("KAISHA_CD = @KAISHA_CD");
                where.Append(" AND SHOHIN_CD = @SHOHIN_CD");
                where.Append(" AND BARCODE1 = @BARCODE1");
                DataTable dtCheck =
                    this.Dba.GetDataTableByConditionWithParams("SHOHIN_CD",
                        "TB_HN_SHOHIN", Util.ToString(where), dpc);
                if (dtCheck.Rows.Count > 0)
                {
                    Msg.Error("既に存在する魚種CDと重複しています。");
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// 魚種名の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidGyosyuNm()
        {
            // 40バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtGyosyuNm.Text, this.txtGyosyuNm.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 魚種カナ名の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidGyosyuKanaNm()
        {
            // 15バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtGyosyuKanaNm.Text, this.txtGyosyuKanaNm.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 仕訳区分の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShiwakeKbn()
        {
            // 未入力は0とする
            if (ValChk.IsEmpty(this.txtShiwakeKbn.Text))
            {
                this.txtShiwakeKbn.Text = "0";
            }

            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtShiwakeKbn.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 存在しないコードを入力されたらエラー
            // 但し0は許可
            if ("0".Equals(this.txtShiwakeKbn.Text))
            {
                this.lblShiwakeKbnNm.Text = string.Empty;
            }
            else
            {
                string ShiwakeKbn = this.GetZidShiwakeSetteiBNm(this.txtShiwakeKbn.Text);
                if (ValChk.IsEmpty(ShiwakeKbn))
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }

                this.lblShiwakeKbnNm.Text = ShiwakeKbn;
            }

            return true;
        }

        /// <summary>
        /// 地元仕訳コードの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidJimotoShiwake()
        {
            // 未入力は0とする
            if (ValChk.IsEmpty(this.txtJimotoShiwakeCd.Text))
            {
                this.txtJimotoShiwakeCd.Text = "0";
            }

            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtJimotoShiwakeCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 存在しないコードを入力されたらエラー
            // 但し0は許可
            if ("0".Equals(this.txtJimotoShiwakeCd.Text))
            {
                this.lblJimotoShiwakeNm.Text = string.Empty;
            }
            else
            {
                string JimotoShiwake = this.GetZidShiwakeSetteiBNm(this.txtJimotoShiwakeCd.Text);
                if (ValChk.IsEmpty(JimotoShiwake))
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }

                this.lblJimotoShiwakeNm.Text = JimotoShiwake;
            }

            return true;
        }

        /// <summary>
        /// 魚種分類の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidGyosyuBunrui()
        {
            // 未入力は0とする
            if (ValChk.IsEmpty(this.txtGyosyuBunrui.Text))
            {
                this.txtGyosyuBunrui.Text = "0";
            }

            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtGyosyuBunrui.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 存在しないコードを入力されたらエラー
            // 但し0は許可
            if ("0".Equals(this.txtGyosyuBunrui.Text))
            {
                this.lblGyosyuBunruiNm.Text = string.Empty;
            }
            else
            {
                string GyosyuBunrui = this.GetGyoshuBunruiMstNm(this.txtGyosyuBunrui.Text);
                if (ValChk.IsEmpty(GyosyuBunrui))
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }

                this.lblGyosyuBunruiNm.Text = GyosyuBunrui;
            }

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            if (MODE_NEW.Equals(this.Par1))
            {
                // 魚種CDのチェック
                if (!IsValidGyosyuCd())
                {
                    this.txtGyosyuCd.Focus();
                    return false;
                }
            }

            // 魚種名のチェック
            if (!IsValidGyosyuNm())
            {
                this.txtGyosyuNm.Focus();
                return false;
            }

            // 魚種カナ名のチェック
            if (!IsValidGyosyuKanaNm())
            {
                this.txtGyosyuKanaNm.Focus();
                return false;
            }

            // 仕訳区分のチェック
            if (!IsValidShiwakeKbn())
            {
                this.txtShiwakeKbn.Focus();
                return false;
            }

            // 地元仕訳コードのチェック
            if (!IsValidJimotoShiwake())
            {
                this.txtJimotoShiwakeCd.Focus();
                return false;
            }

            // 仕訳区分のチェック
            if (!IsValidGyosyuBunrui())
            {
                this.txtGyosyuBunrui.Focus();
                return false;
            }

            return true;
        }

        /// <summary>
        /// TB_HN_SHOHINに更新するためのパラメータ設定をします。
        /// </summary>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// 更新処理：DbParamCollection*2(Where句,Set句)
        /// </returns>
        private ArrayList SetCmTantoParams()
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            if (MODE_NEW.Equals(this.Par1))
            {
                // 会社コードと魚種コードを更新パラメータに設定
                updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                updParam.SetParam("@SHOHIN_CD", SqlDbType.Decimal, 15, this.txtGyosyuCd.Text);
                updParam.SetParam("@BARCODE1", SqlDbType.VarChar, 13, "999");
                // 登録日
                updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWDATE");
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // 会社コードと担当者コードをWhere句のパラメータに設定
                DbParamCollection whereParam = new DbParamCollection();
                whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                whereParam.SetParam("@SHOHIN_CD", SqlDbType.Decimal, 15, this.txtGyosyuCd.Text);
                whereParam.SetParam("@BARCODE1", SqlDbType.VarChar, 13, "999");
                alParams.Add(whereParam);
            }

            // 魚種名
            updParam.SetParam("@SHOHIN_NM", SqlDbType.VarChar, 40, this.txtGyosyuNm.Text);
            // 魚種カナ名
            updParam.SetParam("@SHOHIN_KANA_NM", SqlDbType.VarChar, 30, this.txtGyosyuKanaNm.Text);
            // 仕訳区分
            updParam.SetParam("@SERI_SHIWAKE_CD", SqlDbType.Decimal, 6, this.txtShiwakeKbn.Text);
            // 地元仕訳コード
            updParam.SetParam("@JIMOTO_SHIWAKE_CD", SqlDbType.Decimal, 6, this.txtJimotoShiwakeCd.Text);
            // 地元仕訳コード
            updParam.SetParam("@SHOHIN_KUBUN1", SqlDbType.Decimal, 6, this.txtGyosyuBunrui.Text);
            // 更新日
            updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWDATE");

            alParams.Add(updParam);

            return alParams;
        }

        /// <summary>
        /// 自動仕訳設定Ｂテーブルから仕訳名を取得
        /// </summary>
        /// <param name="code">仕訳コード</param>
        /// <returns>
        /// 仕訳名
        /// </returns>
        private string GetZidShiwakeSetteiBNm(string code)
        {
            string ret = "";
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 4, 3);
            dpc.SetParam("@SHIWAKE_CD", SqlDbType.Decimal, 6, code);
            StringBuilder where = new StringBuilder("KAISHA_CD = @KAISHA_CD");
            where.Append(" AND DENPYO_KUBUN = @DENPYO_KUBUN");
            where.Append(" AND SHIWAKE_CD = @SHIWAKE_CD");
            DataTable dtNm =
                this.Dba.GetDataTableByConditionWithParams("SHIWAKE_NM AS NM",
                    "VI_HN_ZIDO_SHIWAKE_SETTEI_B", Util.ToString(where), dpc);
            if (dtNm.Rows.Count > 0 && !ValChk.IsEmpty(dtNm.Rows[0]["NM"]))
            {
                ret = Util.ToString(dtNm.Rows[0]["NM"]);
            }
            
            return ret;
        }

        /// <summary>
        /// 魚種分類マスタテーブルから魚種分類名を取得
        /// </summary>
        /// <param name="code">魚種分類CD</param>
        /// <returns>
        /// 魚種分類名
        /// </returns>
        private string GetGyoshuBunruiMstNm(string code)
        {
            string ret = "";
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@GYOSHU_BUNRUI_CD", SqlDbType.Decimal, 7, code);
            StringBuilder where = new StringBuilder("KAISHA_CD = @KAISHA_CD");
            where.Append(" AND GYOSHU_BUNRUI_CD = @GYOSHU_BUNRUI_CD");
            DataTable dtNm =
                this.Dba.GetDataTableByConditionWithParams("GYOSHU_BUNRUI_NM AS NM",
                    "TM_HN_GYOSHU_BUNRUI_MST", Util.ToString(where), dpc);
            if (dtNm.Rows.Count > 0 && !ValChk.IsEmpty(dtNm.Rows[0]["NM"]))
            {
                ret = Util.ToString(dtNm.Rows[0]["NM"]);
            }

            return ret;
        }
        #endregion
    }
}
