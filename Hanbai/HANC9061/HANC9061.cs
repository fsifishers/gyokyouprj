﻿using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.han.hanc9061
{
    /// <summary>
    /// 魚種マスタメンテ(HANC9061)
    /// </summary>
    public partial class HANC9061 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// モード(コード検索)
        /// </summary>
        private const string MODE_CD_SRC = "1";
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HANC9061()
        {
            InitializeComponent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                // Par1が"1"の場合、コード検索画面としての挙動をする
                // タイトルは非表示
                this.lblTitle.Visible = false;
                // サイズを縮める
                this.Size = new Size(600, 500);
                // フォームの配置を上へ移動する
                this.lblKanaNm.Location = new System.Drawing.Point(13, 11);
                this.txtKanaNm.Location = new System.Drawing.Point(90, 13);
                this.dgvList.Location = new System.Drawing.Point(13, 40);
                // EscapeとF1のみ表示
                this.btnEsc.Location = this.btnF1.Location;
                this.btnF1.Location = this.btnF2.Location;
                this.btnF2.Visible = false;
                this.btnF3.Visible = false;
                this.btnF4.Visible = false;
                this.btnF5.Visible = false;
                this.btnF6.Visible = false;
                this.btnF7.Visible = false;
                this.btnF8.Visible = false;
                this.btnF9.Visible = false;
                this.btnF10.Visible = false;
                this.btnF11.Visible = false;
                this.btnF12.Visible = false;
                this.ShowFButton = true;
            }
            else
            {
                // メニューから起動の場合はファンクションボタンを表示しない
                this.ShowFButton = false;
            }

            // まずデータが存在し得ない検索条件で検索をし、結果をバインドすることで、
            // 初期状態を作り出す
            SearchData(true);

            // カナ名にフォーカス
            this.txtKanaNm.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtKanaNm.Focus();
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                // Par1が"1"の場合、ダイアログとしての処理結果を返却する
                this.DialogResult = DialogResult.Cancel;
            }
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            // カナ名にフォーカスを戻す
            this.txtKanaNm.Focus();
            this.txtKanaNm.SelectAll();
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // メンテ機能で立ち上げている場合のみ担当者登録画面を立ち上げる
            if (ValChk.IsEmpty(this.Par1))
            {
                // 魚種マスタ登録画面の起動
                EditTantosha(string.Empty);
            }
        }
        
        /**//// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            // メンテ機能で立ち上げている場合のみ魚種マスタ一覧を立ち上げる
            if (ValChk.IsEmpty(this.Par1))
            {
                // 魚種マスタ一覧の起動
                ItiranGyoshu();
            }
        }
        #endregion

        #region イベント
        /// <summary>
        /// カナ名検証時の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKanaNm_Validating(object sender, CancelEventArgs e)
        {
            //TODO:何かチェックが必要なのかもしれない

            // 入力された情報を元に検索する
            SearchData(false);
        }

        /// <summary>
        /// グリッドでのキーダウン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (MODE_CD_SRC.Equals(this.Par1))
                {
                    ReturnVal();
                }
                else
                {
                    EditTantosha(Util.ToString(this.dgvList.SelectedRows[0].Cells["魚種CD"].Value));
                    e.Handled = true;
                }
            }
        }

        /// <summary>
        /// グリッドのセルダブルクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                ReturnVal();
            }
            else
            {
                EditTantosha(Util.ToString(this.dgvList.SelectedRows[0].Cells["魚種CD"].Value));
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// データを検索する
        /// </summary>
        /// <param name="isInitial">初期処理であるかどうか</param>
        private void SearchData(bool isInitial)
        {
            // 商品マスタからデータを取得して表示
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@BARCODE1", SqlDbType.VarChar, 13, "999");
            StringBuilder where = new StringBuilder("KAISHA_CD = @KAISHA_CD");
            where.Append(" AND BARCODE1 = @BARCODE1");
            if (isInitial)
            {
                // 初期処理の場合、検索結果がヒットしないようにあり得ない検索条件を設定する
                //where.Append(" AND 1 = 0");
            }
            else
            {
                // 初期処理でない場合、入力されたカナ名から検索する
                if (!ValChk.IsEmpty(this.txtKanaNm.Text))
                {
                    where.Append(" AND SHOHIN_KANA_NM LIKE @SHOHIN_KANA_NM");
                    // NOTICE!:部分一致検索をする場合、"%"の分桁数に2バイトを足すこと
                    dpc.SetParam("@SHOHIN_KANA_NM", SqlDbType.VarChar, 42, "%" + this.txtKanaNm.Text + "%");
                }
            }
            string cols = "SHOHIN_CD AS 魚種CD";
            cols += ", SHOHIN_NM AS 魚種名";
            cols += ", SHOHIN_KANA_NM AS 魚種カナ名";
            string from = "VI_HN_SHOHIN";

            DataTable dtGyosyu =
                this.Dba.GetDataTableByConditionWithParams(cols, from,
                    Util.ToString(where), "SHOHIN_CD", dpc);

            // 初期処理以外の場合、該当データがなければエラーメッセージを表示
            if (dtGyosyu.Rows.Count == 0)
            {
                if (!isInitial)
                {
                    Msg.Info("該当データがありません。");
                }

                dtGyosyu.Rows.Add(dtGyosyu.NewRow());
            }

            this.dgvList.DataSource = dtGyosyu;

            // ユーザーによるソートを禁止させる
            foreach (DataGridViewColumn c in this.dgvList.Columns)
                c.SortMode = DataGridViewColumnSortMode.NotSortable;

            // フォントを設定する
            this.dgvList.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F, FontStyle.Bold);
            this.dgvList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.dgvList.DefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F);

            // 列幅を設定する
            this.dgvList.Columns[0].Width = 110;
            this.dgvList.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvList.Columns[1].Width = 200;
            this.dgvList.Columns[2].Width = 200;
        }

        /// <summary>
        /// 魚種を追加編集する
        /// </summary>
        /// <param name="code">担当者コード(空：新規登録、以外：編集)</param>
        private void EditTantosha(string code)
        {
            HANC9062 frmHANC9062;

            if (ValChk.IsEmpty(code))
            {
                // 新規登録モードで登録画面を起動
                frmHANC9062 = new HANC9062("1");
            }
            else
            {
                // 編集モードで登録画面を起動
                frmHANC9062 = new HANC9062("2");
                frmHANC9062.InData = code;
            }

            DialogResult result = frmHANC9062.ShowDialog(this);

            if (result == DialogResult.OK)
            {
                // データを再検索する
                SearchData(false);
                // 元々選択していたデータを選択
                for (int i = 0; i < this.dgvList.Rows.Count; i++)
                {
                    if (code.Equals(Util.ToString(this.dgvList.Rows[i].Cells["魚種CD"].Value)))
                    {
                        this.dgvList.Rows[i].Selected = true;
                        break;
                    }
                }

                // Gridに再度フォーカスをセット
                this.ActiveControl = this.dgvList;
                this.dgvList.Focus();
            }
        }

        /// <summary>
        /// 魚種マスタ一覧画面を起動する
        /// </summary>
        /// <param name="code"></param>
        private void ItiranGyoshu()
        {
            HANC9063 frmHANC9063;
            // 魚種マスタ一覧画面を起動
            frmHANC9063 = new HANC9063();

            DialogResult result = frmHANC9063.ShowDialog(this);

            if (result == DialogResult.OK)
            {
                // Gridに再度フォーカスをセット
                this.ActiveControl = this.dgvList;
                this.dgvList.Focus();
            }
        }

        /// <summary>
        /// 呼び出し元に戻り値を返す
        /// </summary>
        private void ReturnVal()
        {
            this.OutData = new string[3] { 
                Util.ToString(this.dgvList.SelectedRows[0].Cells["魚種CD"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["魚種名"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["魚種カナ名"].Value)
            };
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        #endregion
    }
}
