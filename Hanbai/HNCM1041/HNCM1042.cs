﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.hn.hncm1041
{
    /// <summary>
    /// 魚種分類マスタの登録(HNCM1042)
    /// </summary>
    public partial class HNCM1042 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// モード(新規)
        /// </summary>
        private const string MODE_NEW = "1";

        /// <summary>
        /// モード(編集)
        /// </summary>
        private const string MODE_EDIT = "2";
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HNCM1042()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="par1">引数1</param>
        public HNCM1042(string par1)
            : base(par1)
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 引数：Par1／モード(1:新規、2:変更)、InData：魚種分類コード
            if (MODE_NEW.Equals(this.Par1))
            {
                // 新規モードの初期表示
                InitDispOnNew();
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // 編集モードの初期表示
                InitDispOnEdit();
            }
            else
            {
                // 不正な起動として閉じる
                Msg.Error("不正な起動です。終了します。");
                this.Close();
            }

            // タイトルは非表示
            this.lblTitle.Visible = false;
            // EscapeとF1のみ表示
            this.ShowFButton = true;
            this.btnF6.Location = this.btnF3.Location;
            this.btnF3.Location = this.btnF2.Location;
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF2.Visible = false;
            this.btnF3.Visible = true;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF6.Visible = true;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F3キー押下時処理
        /// </summary>
        public override void PressF3()
        {
            // 新規モード時
            if (MODE_NEW.Equals(this.Par1))
            {
                return;
            }

            if (Msg.ConfYesNo("削除しますか？") == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 入力値をバインドパラメータとしてセットする
            ArrayList delParams = SetDelParams();

            try
            {
                this.Dba.BeginTransaction();

                // データ削除
                // 魚種分類マスタ
                this.Dba.Delete("TB_HN_GYOSHU_BUNRUI_MST",
                    "KAISHA_CD = @KAISHA_CD AND GYOSHU_BUNRUI_CD = @GYOSHU_BUNRUI_CD",
                    (DbParamCollection)delParams[0]);

                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF6();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF6()
        {
            // 確認メッセージを表示
            string msg = (MODE_NEW.Equals(this.Par1) ? "登録" : "更新") + "しますか？";
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            // 入力値をバインドパラメータとしてセットする
            ArrayList alParamshnGyoshubn = SetHnGyoshubnParams();

            try
            {
                if (MODE_NEW.Equals(this.Par1))
                {
                    // データ登録
                    // 共通.魚種分類マスタ
                    this.Dba.Insert("TB_HN_GYOSHU_BUNRUI_MST", (DbParamCollection)alParamshnGyoshubn[0]);
                }
                else if (MODE_EDIT.Equals(this.Par1))
                {
                    // データ更新
                    // 共通.魚種分類マスタ
                    this.Dba.Update("TB_HN_GYOSHU_BUNRUI_MST",
                        (DbParamCollection)alParamshnGyoshubn[1],
                        "KAISHA_CD = @KAISHA_CD AND GYOSHU_BUNRUI_CD = @GYOSHU_BUNRUI_CD",
                        (DbParamCollection)alParamshnGyoshubn[0]);
                }

                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 魚種分類コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGyoshubnCd_Validating
            (object sender, CancelEventArgs e)
        {
            if (!IsValidGyoshubnCd())
            {
                e.Cancel = true;
                this.txtGyoshubnCd.SelectAll();
            }
        }

        /// <summary>
        /// 魚種分類名の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGyoshubnNm_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidGyoshubnNm())
            {
                e.Cancel = true;
                this.txtGyoshubnNm.SelectAll();
            }
        }

        /// <summary>
        /// 魚種分類名の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGyoshubnKana_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidGyoshubnKana())
            {
                e.Cancel = true;
                this.txtGyoshubnKana.SelectAll();
            }
        }

        /// <summary>
        /// 画面の最後のコントールでのキーダウン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LastControl_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.btnF6.Enabled)
            {
                this.PressF6();
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 新規モードの初期表示
        /// </summary>
        private void InitDispOnNew()
        {
            // 初期値、入力制御を実装

            // 魚種分類コードの初期値を取得
            // 魚種分類コードの中でのMAX+1を初期表示する
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            StringBuilder where = new StringBuilder();
            DataTable dtMaxGyoshubn =
                this.Dba.GetDataTableByConditionWithParams("MAX(GYOSHU_BUNRUI_CD) AS MAX_CD",
                    "TB_HN_GYOSHU_BUNRUI_MST", Util.ToString(where), dpc);
            if (dtMaxGyoshubn.Rows.Count > 0 && !ValChk.IsEmpty(dtMaxGyoshubn.Rows[0]["MAX_CD"]))
            {
                this.txtGyoshubnCd.Text = Util.ToString(Util.ToInt(dtMaxGyoshubn.Rows[0]["MAX_CD"]) + 1);
            }
            else
            {
                this.txtGyoshubnCd.Text = "100";
            }

            // 漁法名に初期フォーカス
            this.ActiveControl = this.txtGyoshubnNm;
            this.txtGyoshubnNm.Focus();

            // 削除ボタン非表示
            this.btnF3.Enabled = false;
        }

        /// <summary>
        /// 編集モードの初期表示
        /// </summary>
        private void InitDispOnEdit()
        {
            // 現在DBに登録されている値、入力制御を実装
            StringBuilder cols = new StringBuilder();
            cols.Append("A.KAISHA_CD");
            cols.Append(" ,A.GYOSHU_BUNRUI_CD");
            cols.Append(" ,A.GYOSHU_BUNRUI_NM");
            cols.Append(" ,A.GYOSHU_BUNRUI_KANA");
            cols.Append(" ,A.GYOSHU_CD");
            cols.Append(" ,A.REGIST_DATE");
            cols.Append(" ,A.UPDATE_DATE");
            cols.Append(" ,A.SHORI_FLG");

            StringBuilder from = new StringBuilder();
            from.Append("TB_HN_GYOSHU_BUNRUI_MST AS A");


            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@GYOSHU_BUNRUI_CD", SqlDbType.Decimal, 4, Util.ToString(this.InData));

            DataTable dtDispData =
                this.Dba.GetDataTableByConditionWithParams(
                    Util.ToString(cols), Util.ToString(from),
                    "A.KAISHA_CD = @KAISHA_CD AND A.GYOSHU_BUNRUI_CD = @GYOSHU_BUNRUI_CD",
                    dpc);

            if (dtDispData.Rows.Count == 0)
            {
                Msg.Error("不正な起動です。終了します。");
                this.Close();
            }

            // 取得した内容を表示
            DataRow drDispData = dtDispData.Rows[0];
            this.txtGyoshubnCd.Text = Util.ToString(drDispData["GYOSHU_BUNRUI_CD"]);
            this.txtGyoshubnNm.Text = Util.ToString(drDispData["GYOSHU_BUNRUI_NM"]);
            this.txtGyoshubnKana.Text = Util.ToString(drDispData["GYOSHU_BUNRUI_KANA"]);

            // 漁法コードは入力不可
            this.lbGyoshubnCd.Enabled = false;
            this.txtGyoshubnCd.Enabled = false;
        }

        /// <summary>
        /// 漁法コードの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidGyoshubnCd()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtGyoshubnCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 既に存在するコードを入力した場合はエラーとする
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@GYOSHU_BUNRUI_CD", SqlDbType.Decimal, 7, this.txtGyoshubnCd.Text);
            StringBuilder where = new StringBuilder("KAISHA_CD = @KAISHA_CD");
            where.Append(" AND GYOSHU_BUNRUI_CD = @GYOSHU_BUNRUI_CD");
            DataTable dtGyoshubn =
                this.Dba.GetDataTableByConditionWithParams("GYOSHU_BUNRUI_CD",
                    "TB_HN_GYOSHU_BUNRUI_MST", Util.ToString(where), dpc);
            if (dtGyoshubn.Rows.Count > 0)
            {
                Msg.Error("既に存在する魚種分類コードと重複しています。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 魚種分類名の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidGyoshubnNm()
        {
            // 20バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtGyoshubnNm.Text, this.txtGyoshubnNm.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 魚種分類カナ名の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidGyoshubnKana()
        {
            // 20バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtGyoshubnKana.Text, this.txtGyoshubnKana.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            if (MODE_NEW.Equals(this.Par1))
            {
                // 魚種分類コードのチェック
                if (!IsValidGyoshubnCd())
                {
                    this.txtGyoshubnCd.Focus();
                    return false;
                }
            }

            // 魚種分類名のチェック
            if (!IsValidGyoshubnNm())
            {
                this.txtGyoshubnNm.Focus();
                return false;
            }
            
            // 魚種分類カナ名のチェック
            if (!IsValidGyoshubnKana())
            {
                this.txtGyoshubnKana.Focus();
                return false;
            }

            return true;
        }

        /// <summary>
        /// TB_HN_GYOSHU_BUNRUI_MSTに更新するためのパラメータ設定をします。
        /// </summary>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// 更新処理：DbParamCollection*2(Where句,Set句)
        /// </returns>
        private ArrayList SetHnGyoshubnParams()
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            if (MODE_NEW.Equals(this.Par1))
            {
                // 会社コードと漁法コードを更新パラメータに設定
                updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                updParam.SetParam("@GYOSHU_BUNRUI_CD", SqlDbType.Decimal, 4, this.txtGyoshubnCd.Text);
                // 登録日
                updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWDATE");
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // 会社コードと魚種分類コードをWhere句のパラメータに設定
                DbParamCollection whereParam = new DbParamCollection();
                whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                whereParam.SetParam("@GYOSHU_BUNRUI_CD", SqlDbType.Decimal, 4, this.txtGyoshubnCd.Text);
                alParams.Add(whereParam);
            }

            // 魚種分類名
            updParam.SetParam("@GYOSHU_BUNRUI_NM", SqlDbType.VarChar, 20, this.txtGyoshubnNm.Text);
            // 魚種分類カナ名
            updParam.SetParam("@GYOSHU_BUNRUI_KANA", SqlDbType.VarChar, 20, this.txtGyoshubnKana.Text);
            // 更新日
            updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWDATE");
            // 処理フラグ
            updParam.SetParam("@SHORI_FLG", SqlDbType.VarChar, 3, "1");

            alParams.Add(updParam);

            return alParams;
        }

        /// <summary>
        /// TB_HN_GYOSHU_BUNRUI_MSTからデータを削除
        /// </summary>
        /// <returns>
        /// </returns>
        private ArrayList SetDelParams()
        {
            // 会社コードと魚種分類コードを削除パラメータに設定
            ArrayList alParams = new ArrayList();
            DbParamCollection dpcDenpyo = new DbParamCollection();
            dpcDenpyo.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpcDenpyo.SetParam("@GYOSHU_BUNRUI_CD", SqlDbType.VarChar, 6, this.txtGyoshubnCd.Text);

            alParams.Add(dpcDenpyo);

            return alParams;
        }
        #endregion
    }
}
