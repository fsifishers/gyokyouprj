﻿namespace jp.co.fsi.han.hanr2031
{
    partial class HANR2031
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbxSeriDate = new System.Windows.Forms.GroupBox();
            this.lblCodeBetS = new System.Windows.Forms.Label();
            this.lblSeriDateDayTo = new System.Windows.Forms.Label();
            this.lblSeriDateMonthTo = new System.Windows.Forms.Label();
            this.lblSeriDateYearTo = new System.Windows.Forms.Label();
            this.txtSeriDateDayTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtSeriDateYearTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtSeriDateMonthTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblSeriDateGengoTo = new System.Windows.Forms.Label();
            this.lblSeriDateTo = new System.Windows.Forms.Label();
            this.lblSeriDateDayFr = new System.Windows.Forms.Label();
            this.lblSeriDateMonthFr = new System.Windows.Forms.Label();
            this.lblSeriDateYearFr = new System.Windows.Forms.Label();
            this.txtSeriDateDayFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtSeriDateYearFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtSeriDateMonthFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblSeriDateGengoFr = new System.Windows.Forms.Label();
            this.lblSeriDateFｒ = new System.Windows.Forms.Label();
            this.gbxSeisanKbn = new System.Windows.Forms.GroupBox();
            this.rdoChikugai = new System.Windows.Forms.RadioButton();
            this.rdoHamauri = new System.Windows.Forms.RadioButton();
            this.rdoChikunai = new System.Windows.Forms.RadioButton();
            this.gbxFunanushiCd = new System.Windows.Forms.GroupBox();
            this.lblFunanushiCdTo = new System.Windows.Forms.Label();
            this.lblCodeBetF = new System.Windows.Forms.Label();
            this.txtFunanushiCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblFunanushiCdFr = new System.Windows.Forms.Label();
            this.txtFunanushiCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.pnlDebug.SuspendLayout();
            this.gbxSeriDate.SuspendLayout();
            this.gbxSeisanKbn.SuspendLayout();
            this.gbxFunanushiCd.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.TabIndex = 999;
            this.lblTitle.Text = "";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Size = new System.Drawing.Size(847, 100);
            // 
            // gbxSeriDate
            // 
            this.gbxSeriDate.Controls.Add(this.lblCodeBetS);
            this.gbxSeriDate.Controls.Add(this.lblSeriDateDayTo);
            this.gbxSeriDate.Controls.Add(this.lblSeriDateMonthTo);
            this.gbxSeriDate.Controls.Add(this.lblSeriDateYearTo);
            this.gbxSeriDate.Controls.Add(this.txtSeriDateDayTo);
            this.gbxSeriDate.Controls.Add(this.txtSeriDateYearTo);
            this.gbxSeriDate.Controls.Add(this.txtSeriDateMonthTo);
            this.gbxSeriDate.Controls.Add(this.lblSeriDateGengoTo);
            this.gbxSeriDate.Controls.Add(this.lblSeriDateTo);
            this.gbxSeriDate.Controls.Add(this.lblSeriDateDayFr);
            this.gbxSeriDate.Controls.Add(this.lblSeriDateMonthFr);
            this.gbxSeriDate.Controls.Add(this.lblSeriDateYearFr);
            this.gbxSeriDate.Controls.Add(this.txtSeriDateDayFr);
            this.gbxSeriDate.Controls.Add(this.txtSeriDateYearFr);
            this.gbxSeriDate.Controls.Add(this.txtSeriDateMonthFr);
            this.gbxSeriDate.Controls.Add(this.lblSeriDateGengoFr);
            this.gbxSeriDate.Controls.Add(this.lblSeriDateFｒ);
            this.gbxSeriDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.gbxSeriDate.ForeColor = System.Drawing.Color.Black;
            this.gbxSeriDate.Location = new System.Drawing.Point(12, 50);
            this.gbxSeriDate.Name = "gbxSeriDate";
            this.gbxSeriDate.Size = new System.Drawing.Size(526, 63);
            this.gbxSeriDate.TabIndex = 0;
            this.gbxSeriDate.TabStop = false;
            this.gbxSeriDate.Text = "セリ日付範囲";
            // 
            // lblCodeBetS
            // 
            this.lblCodeBetS.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblCodeBetS.ForeColor = System.Drawing.Color.Black;
            this.lblCodeBetS.Location = new System.Drawing.Point(253, 24);
            this.lblCodeBetS.Name = "lblCodeBetS";
            this.lblCodeBetS.Size = new System.Drawing.Size(18, 20);
            this.lblCodeBetS.TabIndex = 8;
            this.lblCodeBetS.Text = "～";
            this.lblCodeBetS.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSeriDateDayTo
            // 
            this.lblSeriDateDayTo.BackColor = System.Drawing.Color.Silver;
            this.lblSeriDateDayTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblSeriDateDayTo.ForeColor = System.Drawing.Color.Black;
            this.lblSeriDateDayTo.Location = new System.Drawing.Point(489, 25);
            this.lblSeriDateDayTo.Name = "lblSeriDateDayTo";
            this.lblSeriDateDayTo.Size = new System.Drawing.Size(20, 18);
            this.lblSeriDateDayTo.TabIndex = 16;
            this.lblSeriDateDayTo.Text = "日";
            this.lblSeriDateDayTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSeriDateMonthTo
            // 
            this.lblSeriDateMonthTo.BackColor = System.Drawing.Color.Silver;
            this.lblSeriDateMonthTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblSeriDateMonthTo.ForeColor = System.Drawing.Color.Black;
            this.lblSeriDateMonthTo.Location = new System.Drawing.Point(434, 25);
            this.lblSeriDateMonthTo.Name = "lblSeriDateMonthTo";
            this.lblSeriDateMonthTo.Size = new System.Drawing.Size(15, 19);
            this.lblSeriDateMonthTo.TabIndex = 14;
            this.lblSeriDateMonthTo.Text = "月";
            this.lblSeriDateMonthTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSeriDateYearTo
            // 
            this.lblSeriDateYearTo.BackColor = System.Drawing.Color.Silver;
            this.lblSeriDateYearTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblSeriDateYearTo.ForeColor = System.Drawing.Color.Black;
            this.lblSeriDateYearTo.Location = new System.Drawing.Point(381, 24);
            this.lblSeriDateYearTo.Name = "lblSeriDateYearTo";
            this.lblSeriDateYearTo.Size = new System.Drawing.Size(17, 21);
            this.lblSeriDateYearTo.TabIndex = 12;
            this.lblSeriDateYearTo.Text = "年";
            this.lblSeriDateYearTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtSeriDateDayTo
            // 
            this.txtSeriDateDayTo.AutoSizeFromLength = false;
            this.txtSeriDateDayTo.DisplayLength = null;
            this.txtSeriDateDayTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtSeriDateDayTo.ForeColor = System.Drawing.Color.Black;
            this.txtSeriDateDayTo.Location = new System.Drawing.Point(456, 24);
            this.txtSeriDateDayTo.MaxLength = 2;
            this.txtSeriDateDayTo.Name = "txtSeriDateDayTo";
            this.txtSeriDateDayTo.Size = new System.Drawing.Size(30, 20);
            this.txtSeriDateDayTo.TabIndex = 15;
            this.txtSeriDateDayTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSeriDateDayTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtSeriDateDayTo_Validating);
            // 
            // txtSeriDateYearTo
            // 
            this.txtSeriDateYearTo.AutoSizeFromLength = false;
            this.txtSeriDateYearTo.DisplayLength = null;
            this.txtSeriDateYearTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtSeriDateYearTo.ForeColor = System.Drawing.Color.Black;
            this.txtSeriDateYearTo.Location = new System.Drawing.Point(349, 24);
            this.txtSeriDateYearTo.MaxLength = 2;
            this.txtSeriDateYearTo.Name = "txtSeriDateYearTo";
            this.txtSeriDateYearTo.Size = new System.Drawing.Size(30, 20);
            this.txtSeriDateYearTo.TabIndex = 11;
            this.txtSeriDateYearTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSeriDateYearTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtSeriDateYearTo_Validating);
            // 
            // txtSeriDateMonthTo
            // 
            this.txtSeriDateMonthTo.AutoSizeFromLength = false;
            this.txtSeriDateMonthTo.DisplayLength = null;
            this.txtSeriDateMonthTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtSeriDateMonthTo.ForeColor = System.Drawing.Color.Black;
            this.txtSeriDateMonthTo.Location = new System.Drawing.Point(402, 24);
            this.txtSeriDateMonthTo.MaxLength = 2;
            this.txtSeriDateMonthTo.Name = "txtSeriDateMonthTo";
            this.txtSeriDateMonthTo.Size = new System.Drawing.Size(30, 20);
            this.txtSeriDateMonthTo.TabIndex = 13;
            this.txtSeriDateMonthTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSeriDateMonthTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtSeriDateMonthTo_Validating);
            // 
            // lblSeriDateGengoTo
            // 
            this.lblSeriDateGengoTo.BackColor = System.Drawing.Color.Silver;
            this.lblSeriDateGengoTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSeriDateGengoTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblSeriDateGengoTo.ForeColor = System.Drawing.Color.Black;
            this.lblSeriDateGengoTo.Location = new System.Drawing.Point(304, 24);
            this.lblSeriDateGengoTo.Name = "lblSeriDateGengoTo";
            this.lblSeriDateGengoTo.Size = new System.Drawing.Size(41, 21);
            this.lblSeriDateGengoTo.TabIndex = 10;
            this.lblSeriDateGengoTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSeriDateTo
            // 
            this.lblSeriDateTo.BackColor = System.Drawing.Color.Silver;
            this.lblSeriDateTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblSeriDateTo.ForeColor = System.Drawing.Color.Black;
            this.lblSeriDateTo.Location = new System.Drawing.Point(301, 21);
            this.lblSeriDateTo.Name = "lblSeriDateTo";
            this.lblSeriDateTo.Size = new System.Drawing.Size(214, 27);
            this.lblSeriDateTo.TabIndex = 9;
            this.lblSeriDateTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSeriDateDayFr
            // 
            this.lblSeriDateDayFr.BackColor = System.Drawing.Color.Silver;
            this.lblSeriDateDayFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblSeriDateDayFr.ForeColor = System.Drawing.Color.Black;
            this.lblSeriDateDayFr.Location = new System.Drawing.Point(197, 25);
            this.lblSeriDateDayFr.Name = "lblSeriDateDayFr";
            this.lblSeriDateDayFr.Size = new System.Drawing.Size(20, 18);
            this.lblSeriDateDayFr.TabIndex = 7;
            this.lblSeriDateDayFr.Text = "日";
            this.lblSeriDateDayFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSeriDateMonthFr
            // 
            this.lblSeriDateMonthFr.BackColor = System.Drawing.Color.Silver;
            this.lblSeriDateMonthFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblSeriDateMonthFr.ForeColor = System.Drawing.Color.Black;
            this.lblSeriDateMonthFr.Location = new System.Drawing.Point(142, 25);
            this.lblSeriDateMonthFr.Name = "lblSeriDateMonthFr";
            this.lblSeriDateMonthFr.Size = new System.Drawing.Size(15, 19);
            this.lblSeriDateMonthFr.TabIndex = 5;
            this.lblSeriDateMonthFr.Text = "月";
            this.lblSeriDateMonthFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSeriDateYearFr
            // 
            this.lblSeriDateYearFr.BackColor = System.Drawing.Color.Silver;
            this.lblSeriDateYearFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblSeriDateYearFr.ForeColor = System.Drawing.Color.Black;
            this.lblSeriDateYearFr.Location = new System.Drawing.Point(89, 24);
            this.lblSeriDateYearFr.Name = "lblSeriDateYearFr";
            this.lblSeriDateYearFr.Size = new System.Drawing.Size(17, 21);
            this.lblSeriDateYearFr.TabIndex = 3;
            this.lblSeriDateYearFr.Text = "年";
            this.lblSeriDateYearFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtSeriDateDayFr
            // 
            this.txtSeriDateDayFr.AutoSizeFromLength = false;
            this.txtSeriDateDayFr.DisplayLength = null;
            this.txtSeriDateDayFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtSeriDateDayFr.ForeColor = System.Drawing.Color.Black;
            this.txtSeriDateDayFr.Location = new System.Drawing.Point(164, 24);
            this.txtSeriDateDayFr.MaxLength = 2;
            this.txtSeriDateDayFr.Name = "txtSeriDateDayFr";
            this.txtSeriDateDayFr.Size = new System.Drawing.Size(30, 20);
            this.txtSeriDateDayFr.TabIndex = 6;
            this.txtSeriDateDayFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSeriDateDayFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtSeriDateDayFr_Validating);
            // 
            // txtSeriDateYearFr
            // 
            this.txtSeriDateYearFr.AutoSizeFromLength = false;
            this.txtSeriDateYearFr.DisplayLength = null;
            this.txtSeriDateYearFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtSeriDateYearFr.ForeColor = System.Drawing.Color.Black;
            this.txtSeriDateYearFr.Location = new System.Drawing.Point(57, 24);
            this.txtSeriDateYearFr.MaxLength = 2;
            this.txtSeriDateYearFr.Name = "txtSeriDateYearFr";
            this.txtSeriDateYearFr.Size = new System.Drawing.Size(30, 20);
            this.txtSeriDateYearFr.TabIndex = 2;
            this.txtSeriDateYearFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSeriDateYearFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtSeriDateYearFr_Validating);
            // 
            // txtSeriDateMonthFr
            // 
            this.txtSeriDateMonthFr.AutoSizeFromLength = false;
            this.txtSeriDateMonthFr.DisplayLength = null;
            this.txtSeriDateMonthFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtSeriDateMonthFr.ForeColor = System.Drawing.Color.Black;
            this.txtSeriDateMonthFr.Location = new System.Drawing.Point(110, 24);
            this.txtSeriDateMonthFr.MaxLength = 2;
            this.txtSeriDateMonthFr.Name = "txtSeriDateMonthFr";
            this.txtSeriDateMonthFr.Size = new System.Drawing.Size(30, 20);
            this.txtSeriDateMonthFr.TabIndex = 4;
            this.txtSeriDateMonthFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSeriDateMonthFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtSeriDateMonthFr_Validating);
            // 
            // lblSeriDateGengoFr
            // 
            this.lblSeriDateGengoFr.BackColor = System.Drawing.Color.Silver;
            this.lblSeriDateGengoFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSeriDateGengoFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblSeriDateGengoFr.ForeColor = System.Drawing.Color.Black;
            this.lblSeriDateGengoFr.Location = new System.Drawing.Point(12, 24);
            this.lblSeriDateGengoFr.Name = "lblSeriDateGengoFr";
            this.lblSeriDateGengoFr.Size = new System.Drawing.Size(41, 21);
            this.lblSeriDateGengoFr.TabIndex = 1;
            this.lblSeriDateGengoFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSeriDateFｒ
            // 
            this.lblSeriDateFｒ.BackColor = System.Drawing.Color.Silver;
            this.lblSeriDateFｒ.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblSeriDateFｒ.ForeColor = System.Drawing.Color.Black;
            this.lblSeriDateFｒ.Location = new System.Drawing.Point(9, 21);
            this.lblSeriDateFｒ.Name = "lblSeriDateFｒ";
            this.lblSeriDateFｒ.Size = new System.Drawing.Size(214, 27);
            this.lblSeriDateFｒ.TabIndex = 0;
            this.lblSeriDateFｒ.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // gbxSeisanKbn
            // 
            this.gbxSeisanKbn.Controls.Add(this.rdoChikugai);
            this.gbxSeisanKbn.Controls.Add(this.rdoHamauri);
            this.gbxSeisanKbn.Controls.Add(this.rdoChikunai);
            this.gbxSeisanKbn.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.gbxSeisanKbn.ForeColor = System.Drawing.Color.Black;
            this.gbxSeisanKbn.Location = new System.Drawing.Point(12, 133);
            this.gbxSeisanKbn.Name = "gbxSeisanKbn";
            this.gbxSeisanKbn.Size = new System.Drawing.Size(364, 69);
            this.gbxSeisanKbn.TabIndex = 1;
            this.gbxSeisanKbn.TabStop = false;
            this.gbxSeisanKbn.Text = "精算区分";
            // 
            // rdoChikugai
            // 
            this.rdoChikugai.AutoSize = true;
            this.rdoChikugai.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.rdoChikugai.ForeColor = System.Drawing.Color.Black;
            this.rdoChikugai.Location = new System.Drawing.Point(269, 30);
            this.rdoChikugai.Name = "rdoChikugai";
            this.rdoChikugai.Size = new System.Drawing.Size(81, 17);
            this.rdoChikugai.TabIndex = 2;
            this.rdoChikugai.TabStop = true;
            this.rdoChikugai.Text = "地区外ｾﾘ";
            this.rdoChikugai.UseVisualStyleBackColor = true;
            // 
            // rdoHamauri
            // 
            this.rdoHamauri.AutoSize = true;
            this.rdoHamauri.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.rdoHamauri.ForeColor = System.Drawing.Color.Black;
            this.rdoHamauri.Location = new System.Drawing.Point(145, 30);
            this.rdoHamauri.Name = "rdoHamauri";
            this.rdoHamauri.Size = new System.Drawing.Size(67, 17);
            this.rdoHamauri.TabIndex = 1;
            this.rdoHamauri.TabStop = true;
            this.rdoHamauri.Text = "浜売り";
            this.rdoHamauri.UseVisualStyleBackColor = true;
            // 
            // rdoChikunai
            // 
            this.rdoChikunai.AutoSize = true;
            this.rdoChikunai.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.rdoChikunai.ForeColor = System.Drawing.Color.Black;
            this.rdoChikunai.Location = new System.Drawing.Point(20, 30);
            this.rdoChikunai.Name = "rdoChikunai";
            this.rdoChikunai.Size = new System.Drawing.Size(81, 17);
            this.rdoChikunai.TabIndex = 0;
            this.rdoChikunai.TabStop = true;
            this.rdoChikunai.Text = "地区内ｾﾘ";
            this.rdoChikunai.UseVisualStyleBackColor = true;
            // 
            // gbxFunanushiCd
            // 
            this.gbxFunanushiCd.Controls.Add(this.lblFunanushiCdTo);
            this.gbxFunanushiCd.Controls.Add(this.lblCodeBetF);
            this.gbxFunanushiCd.Controls.Add(this.txtFunanushiCdFr);
            this.gbxFunanushiCd.Controls.Add(this.lblFunanushiCdFr);
            this.gbxFunanushiCd.Controls.Add(this.txtFunanushiCdTo);
            this.gbxFunanushiCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.gbxFunanushiCd.ForeColor = System.Drawing.Color.Black;
            this.gbxFunanushiCd.Location = new System.Drawing.Point(12, 223);
            this.gbxFunanushiCd.Name = "gbxFunanushiCd";
            this.gbxFunanushiCd.Size = new System.Drawing.Size(573, 76);
            this.gbxFunanushiCd.TabIndex = 2;
            this.gbxFunanushiCd.TabStop = false;
            this.gbxFunanushiCd.Text = "船主CD範囲";
            // 
            // lblFunanushiCdTo
            // 
            this.lblFunanushiCdTo.BackColor = System.Drawing.Color.Silver;
            this.lblFunanushiCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFunanushiCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblFunanushiCdTo.ForeColor = System.Drawing.Color.Black;
            this.lblFunanushiCdTo.Location = new System.Drawing.Point(344, 32);
            this.lblFunanushiCdTo.Name = "lblFunanushiCdTo";
            this.lblFunanushiCdTo.Size = new System.Drawing.Size(221, 20);
            this.lblFunanushiCdTo.TabIndex = 4;
            this.lblFunanushiCdTo.Text = "最　後";
            this.lblFunanushiCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCodeBetF
            // 
            this.lblCodeBetF.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblCodeBetF.ForeColor = System.Drawing.Color.Black;
            this.lblCodeBetF.Location = new System.Drawing.Point(276, 31);
            this.lblCodeBetF.Name = "lblCodeBetF";
            this.lblCodeBetF.Size = new System.Drawing.Size(18, 20);
            this.lblCodeBetF.TabIndex = 2;
            this.lblCodeBetF.Text = "～";
            this.lblCodeBetF.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFunanushiCdFr
            // 
            this.txtFunanushiCdFr.AutoSizeFromLength = false;
            this.txtFunanushiCdFr.DisplayLength = null;
            this.txtFunanushiCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtFunanushiCdFr.ForeColor = System.Drawing.Color.Black;
            this.txtFunanushiCdFr.Location = new System.Drawing.Point(8, 30);
            this.txtFunanushiCdFr.MaxLength = 4;
            this.txtFunanushiCdFr.Name = "txtFunanushiCdFr";
            this.txtFunanushiCdFr.Size = new System.Drawing.Size(40, 20);
            this.txtFunanushiCdFr.TabIndex = 0;
            this.txtFunanushiCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtCodeFr_Validating);
            // 
            // lblFunanushiCdFr
            // 
            this.lblFunanushiCdFr.BackColor = System.Drawing.Color.Silver;
            this.lblFunanushiCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFunanushiCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblFunanushiCdFr.ForeColor = System.Drawing.Color.Black;
            this.lblFunanushiCdFr.Location = new System.Drawing.Point(51, 31);
            this.lblFunanushiCdFr.Name = "lblFunanushiCdFr";
            this.lblFunanushiCdFr.Size = new System.Drawing.Size(221, 20);
            this.lblFunanushiCdFr.TabIndex = 1;
            this.lblFunanushiCdFr.Text = "先　頭";
            this.lblFunanushiCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFunanushiCdTo
            // 
            this.txtFunanushiCdTo.AutoSizeFromLength = false;
            this.txtFunanushiCdTo.DisplayLength = null;
            this.txtFunanushiCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtFunanushiCdTo.ForeColor = System.Drawing.Color.Black;
            this.txtFunanushiCdTo.Location = new System.Drawing.Point(301, 31);
            this.txtFunanushiCdTo.MaxLength = 4;
            this.txtFunanushiCdTo.Name = "txtFunanushiCdTo";
            this.txtFunanushiCdTo.Size = new System.Drawing.Size(40, 20);
            this.txtFunanushiCdTo.TabIndex = 3;
            this.txtFunanushiCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtCodeTo_Validating);
            // 
            // HANR2031
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 638);
            this.Controls.Add(this.gbxFunanushiCd);
            this.Controls.Add(this.gbxSeriDate);
            this.Controls.Add(this.gbxSeisanKbn);
            this.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "HANR2031";
            this.Text = "";
            this.Controls.SetChildIndex(this.gbxSeisanKbn, 0);
            this.Controls.SetChildIndex(this.gbxSeriDate, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.gbxFunanushiCd, 0);
            this.pnlDebug.ResumeLayout(false);
            this.gbxSeriDate.ResumeLayout(false);
            this.gbxSeriDate.PerformLayout();
            this.gbxSeisanKbn.ResumeLayout(false);
            this.gbxSeisanKbn.PerformLayout();
            this.gbxFunanushiCd.ResumeLayout(false);
            this.gbxFunanushiCd.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbxSeriDate;
        private jp.co.fsi.common.controls.FsiTextBox txtSeriDateYearFr;
        private System.Windows.Forms.Label lblSeriDateGengoFr;
        private System.Windows.Forms.Label lblSeriDateFｒ;
        private System.Windows.Forms.Label lblSeriDateDayFr;
        private System.Windows.Forms.Label lblSeriDateMonthFr;
        private System.Windows.Forms.Label lblSeriDateYearFr;
        private jp.co.fsi.common.controls.FsiTextBox txtSeriDateDayFr;
        private jp.co.fsi.common.controls.FsiTextBox txtSeriDateMonthFr;
        private System.Windows.Forms.GroupBox gbxSeisanKbn;
        private System.Windows.Forms.RadioButton rdoChikugai;
        private System.Windows.Forms.RadioButton rdoHamauri;
        private System.Windows.Forms.RadioButton rdoChikunai;
        private System.Windows.Forms.GroupBox gbxFunanushiCd;
        private System.Windows.Forms.Label lblFunanushiCdTo;
        private System.Windows.Forms.Label lblCodeBetF;
        private common.controls.FsiTextBox txtFunanushiCdFr;
        private System.Windows.Forms.Label lblFunanushiCdFr;
        private common.controls.FsiTextBox txtFunanushiCdTo;
        private System.Windows.Forms.Label lblSeriDateDayTo;
        private System.Windows.Forms.Label lblSeriDateMonthTo;
        private System.Windows.Forms.Label lblSeriDateYearTo;
        private common.controls.FsiTextBox txtSeriDateDayTo;
        private common.controls.FsiTextBox txtSeriDateYearTo;
        private common.controls.FsiTextBox txtSeriDateMonthTo;
        private System.Windows.Forms.Label lblSeriDateGengoTo;
        private System.Windows.Forms.Label lblSeriDateTo;
        private System.Windows.Forms.Label lblCodeBetS;

    }
}