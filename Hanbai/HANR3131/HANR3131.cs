﻿using System;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Windows.Forms;

using GrapeCity.ActiveReports;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.han.hanr3131
{
    /// <summary>
    /// メイン 地区別個人別業態別漁獲高月報(HANR3131)   ※実は累計出力！
    /// </summary>
    public partial class HANR3131 : BasePgForm
    {
        #region プロパティ
        /// <summary>
        /// 画面上最後となるフォーカスのEnterボタン押下時処理用変数
        /// </summary>
        private bool _dtFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._dtFlg;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HANR3131()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region メソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            string[] jpDate = Util.ConvJpDate(DateTime.Now, this.Dba);
            // 日付範囲前
            lblDateGengoFr.Text = jpDate[0];
            txtDateYearFr.Text = jpDate[2];
            txtDateMonthFr.Text = jpDate[3];
            // 日付範囲後
            lblDateGengoTo.Text = jpDate[0];
            txtDateYearTo.Text = jpDate[2];
            txtDateMonthTo.Text = jpDate[3];

            // 初期フォーカス
            txtDateYearFr.Focus();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 日付(年)にフォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtDateYearFr":
                case "txtDateYearTo":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            switch (this.ActiveCtlNm)
            {
                case "txtDateYearFr":
                    // アセンブリのロード
                    System.Reflection.Assembly asmFr = System.Reflection.Assembly.LoadFrom("COMC9011.exe");
                    // フォーム作成
                    Type tFr = asmFr.GetType("jp.co.fsi.com.comc9011.COMC9011");
                    if (tFr != null)
                    {
                        Object obj = System.Activator.CreateInstance(tFr);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblDateGengoFr.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.lblDateGengoFr.Text = result[1];

                                // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
                                DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                                    this.txtDateMonthFr.Text, "1", this.Dba);
                                int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblDateGengoFr.Text,
                                        this.txtDateYearFr.Text,
                                        this.txtDateMonthFr.Text,
                                        "1",
                                        this.Dba);
                                this.lblDateGengoFr.Text = arrJpDate[0];
                                this.txtDateYearFr.Text = arrJpDate[2];
                                this.txtDateMonthFr.Text = arrJpDate[3];
                            }
                        }
                    }
                    break;

                case "txtDateYearTo":
                    // アセンブリのロード
                    System.Reflection.Assembly asmTo = System.Reflection.Assembly.LoadFrom("COMC9011.exe");
                    // フォーム作成
                    Type tTo = asmTo.GetType("jp.co.fsi.com.comc9011.COMC9011");
                    if (tTo != null)
                    {
                        Object obj = System.Activator.CreateInstance(tTo);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblDateGengoTo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.lblDateGengoTo.Text = result[1];

                                // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
                                DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                                    this.txtDateMonthTo.Text, "1", this.Dba);
                                int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblDateGengoTo.Text,
                                        this.txtDateYearTo.Text,
                                        this.txtDateMonthTo.Text,
                                        "1",
                                        this.Dba);
                                this.lblDateGengoTo.Text = arrJpDate[0];
                                this.txtDateYearTo.Text = arrJpDate[2];
                                this.txtDateMonthTo.Text = arrJpDate[3];
                            }
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
            {
                // ﾌﾟﾚﾋﾞｭｰ処理
                DoPrint(true);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("印刷", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }

        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            // 設定画面の起動
            // MEMO:原則としてここで渡す帳票IDの設定はReport.csvに保持していることが前提ですが、
            // 保持していない場合は、設定画面での保存(F6)時に新規に設定が保持されます。
            PrintSettingForm psForm = new PrintSettingForm(new string[1] { "HANR3131R" });
            psForm.ShowDialog();
        }

        #endregion

        #region イベント
        /// <summary>
        /// 年(自)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateYearFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidDateYearFr())
            {
                e.Cancel = true;
                this.txtDateYearFr.SelectAll();
            }
            else
            {
                // 存在しない日付の場合、補正して存在する日付に戻す
                string[] arrJpDate =
                    Util.FixJpDate(this.lblDateGengoFr.Text,
                        this.txtDateYearFr.Text,
                        this.txtDateMonthFr.Text,
                        "1",
                        this.Dba);
                this.lblDateGengoFr.Text = arrJpDate[0];
                this.txtDateYearFr.Text = arrJpDate[2];
                this.txtDateMonthFr.Text = arrJpDate[3];
            }
        }

        /// <summary>
        /// 月(自)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateMonthFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidDateMonthFr())
            {
                e.Cancel = true;
                this.txtDateMonthFr.SelectAll();
            }
        }

        /// <summary>
        /// 年(至)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateYearTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidDateYearTo())
            {
                e.Cancel = true;
                this.txtDateYearTo.SelectAll();
            }
            else
            {
                // 存在しない日付の場合、補正して存在する日付に戻す
                string[] arrJpDate =
                    Util.FixJpDate(this.lblDateGengoTo.Text,
                        this.txtDateYearTo.Text,
                        this.txtDateMonthTo.Text,
                        "1",
                        this.Dba);
                this.lblDateGengoTo.Text = arrJpDate[0];
                this.txtDateYearTo.Text = arrJpDate[2];
                this.txtDateMonthTo.Text = arrJpDate[3];
            }
        }

        /// <summary>
        /// 月(至)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateMonthTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidDateMonthTo())
            {
                e.Cancel = true;
                this.txtDateMonthTo.SelectAll();

                // Enter処理を無効化
                this._dtFlg = false;
            }
            else
            {
                // Enter処理を有効化
                this._dtFlg = true;
            }
        }

        /// <summary>
        /// 月(至)のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateMonthTo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.Flg)
            {
                // Enter処理を無効化
                this._dtFlg = false;

                // 全項目を再度入力値チェック
                if (!ValidateAll())
                {
                    // エラーありの場合ここで処理終了
                    return;
                }

                if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
                {
                    // ﾌﾟﾚﾋﾞｭｰ処理
                    DoPrint(true);
                }
                else
                {
                    this.txtDateMonthTo.Focus();
                }
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 年(自)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidDateYearFr()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtDateYearFr.Text))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }
            // 空の場合、0年として処理
            if (ValChk.IsEmpty(this.txtDateYearFr.Text))
            {
                this.txtDateYearFr.Text = "0";
            }

            return true;
        }

        /// <summary>
        /// 月(自)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidDateMonthFr()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtDateMonthFr.Text))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }
            // 空の場合、1月として処理
            if (ValChk.IsEmpty(this.txtDateMonthFr.Text))
            {
                this.txtDateMonthFr.Text = "1";
            }
            else
            {
                // 12を超える月が入力された場合、12月として処理
                if (Util.ToInt(this.txtDateMonthFr.Text) > 12)
                {
                    this.txtDateMonthFr.Text = "12";
                }
                // 1より小さい月が入力された場合、1月として処理
                else if (Util.ToInt(this.txtDateMonthFr.Text) < 1)
                {
                    this.txtDateMonthFr.Text = "1";
                }
            }

            return true;
        }

        /// <summary>
        /// 年(至)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidDateYearTo()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtDateYearTo.Text))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }
            // 空の場合、0年として処理
            if (ValChk.IsEmpty(this.txtDateYearTo.Text))
            {
                this.txtDateYearTo.Text = "0";
            }

            return true;
        }

        /// <summary>
        /// 月(至)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidDateMonthTo()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtDateMonthTo.Text))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }
            // 空の場合、1月として処理
            if (ValChk.IsEmpty(this.txtDateMonthTo.Text))
            {
                this.txtDateMonthTo.Text = "1";
            }
            else
            {
                // 12を超える月が入力された場合、12月として処理
                if (Util.ToInt(this.txtDateMonthTo.Text) > 12)
                {
                    this.txtDateMonthTo.Text = "12";
                }
                // 1より小さい月が入力された場合、1月として処理
                else if (Util.ToInt(this.txtDateMonthTo.Text) < 1)
                {
                    this.txtDateMonthTo.Text = "1";
                }
            }

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 年(自)のチェック
            if (!IsValidDateYearFr())
            {
                this.txtDateYearFr.Focus();
                this.txtDateYearFr.SelectAll();
                return false;
            }
            // 月(自)のチェック
            if (!IsValidDateMonthFr())
            {
                this.txtDateMonthFr.Focus();
                this.txtDateMonthFr.SelectAll();
                return false;
            }

            // 年(至)のチェック
            if (!IsValidDateYearTo())
            {
                this.txtDateYearTo.Focus();
                this.txtDateYearTo.SelectAll();
                return false;
            }
            // 月(至)のチェック
            if (!IsValidDateMonthTo())
            {
                this.txtDateMonthTo.Focus();
                this.txtDateMonthTo.SelectAll();
                return false;
            }

            // 日付範囲を西暦にして取得
            DateTime tmpDateFr = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                    this.txtDateMonthFr.Text, "1", this.Dba);
            DateTime tmpDateTo = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                    this.txtDateMonthTo.Text, "1", this.Dba);

            // 終了年月 ＜ 開始年月ならエラーにする（※SQL高速化の副作用で、入力チェックでエラーにしなければならない）
            if (tmpDateTo < tmpDateFr)
            {
                this.txtDateYearTo.Focus();
                this.txtDateYearTo.SelectAll();
                Msg.Error("開始年月より終了年月が古いです。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpDateFr(string[] arrJpDate)
        {
            this.lblDateGengoFr.Text = arrJpDate[0];
            this.txtDateYearFr.Text = arrJpDate[2];
            this.txtDateMonthFr.Text = arrJpDate[3];
        }
        private void SetJpDateTo(string[] arrJpDate)
        {
            this.lblDateGengoTo.Text = arrJpDate[0];
            this.txtDateYearTo.Text = arrJpDate[2];
            this.txtDateMonthTo.Text = arrJpDate[3];
        }

        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview)
        {
            bool dataFlag;
            try
            {
                this.Dba.BeginTransaction();

                // 帳票出力用にワークテーブルにデータを作成
                dataFlag = MakeWkData();

                // 帳票出力
                if (dataFlag)
                {
                    // 印字日付設定
                    string printDateFr = string.Format("{0}{1}年{2}月", this.lblDateGengoFr.Text, this.txtDateYearFr.Text, this.txtDateMonthFr.Text);
                    string printDateTo = string.Format("{0}{1}年{2}月", this.lblDateGengoTo.Text, this.txtDateYearTo.Text, this.txtDateMonthTo.Text);
                    string[] todayDate = Util.ConvJpDate(DateTime.Now, this.Dba);
                    string today = string.Format("{0}{1}年{2}月{3}日", todayDate[0], todayDate[2], todayDate[3], todayDate[4]);
                    
                    if (btnYoshiB4.Checked)
                    {
                        // 取得列の定義
                        StringBuilder cols = new StringBuilder();
                        cols.Append("  ITEM01"); // 船主CD
                        cols.Append(" ,ITEM02"); // 地区CD
                        cols.Append(" ,ITEM03"); // 船主名称
                        cols.Append(" ,ITEM04"); // 地区名
                        cols.Append(" ,CONVERT(decimal(9, 0), ITEM05) AS ITEM05"); // 日数
                        cols.Append(" ,CONVERT(decimal(9, 2), ITEM06) AS ITEM06"); // 浅海一本釣数量
                        cols.Append(" ,CONVERT(decimal(9, 0), ITEM07) AS ITEM07"); // 浅海一本釣金額
                        cols.Append(" ,CONVERT(decimal(9, 2), ITEM08) AS ITEM08"); // 曳縄数量
                        cols.Append(" ,CONVERT(decimal(9, 0), ITEM09) AS ITEM09"); // 曳縄金額
                        cols.Append(" ,CONVERT(decimal(9, 2), ITEM10) AS ITEM10"); // 浅海刺網数量
                        cols.Append(" ,CONVERT(decimal(9, 0), ITEM11) AS ITEM11"); // 浅海刺網金額
                        cols.Append(" ,CONVERT(decimal(9, 2), ITEM12) AS ITEM12"); // 追込網数量
                        cols.Append(" ,CONVERT(decimal(9, 0), ITEM13) AS ITEM13"); // 追込網金額
                        cols.Append(" ,CONVERT(decimal(9, 2), ITEM14) AS ITEM14"); // 潜水器数量
                        cols.Append(" ,CONVERT(decimal(9, 0), ITEM15) AS ITEM15"); // 潜水器金額
                        cols.Append(" ,CONVERT(decimal(9, 2), ITEM16) AS ITEM16"); // 採貝数量
                        cols.Append(" ,CONVERT(decimal(9, 0), ITEM17) AS ITEM17"); // 採貝金額
                        cols.Append(" ,CONVERT(decimal(9, 2), ITEM18) AS ITEM18"); // 深海一本釣数量
                        cols.Append(" ,CONVERT(decimal(9, 0), ITEM19) AS ITEM19"); // 深海一本釣金額
                        cols.Append(" ,CONVERT(decimal(9, 2), ITEM20) AS ITEM20"); // いか曳数量
                        cols.Append(" ,CONVERT(decimal(9, 0), ITEM21) AS ITEM21"); // いか曳金額
                        cols.Append(" ,'浅海一本釣' AS GYOHO_01"); // マグロ類見出し
                        cols.Append(" ,'曳縄' AS GYOHO_02"); // カジキ類見出し
                        cols.Append(" ,'浅海刺網' AS GYOHO_03"); // 浅海刺網見出し
                        cols.Append(" ,'追込網' AS GYOHO_04"); // 追込網見出し
                        cols.Append(" ,'潜水器' AS GYOHO_05"); // 潜水器見出し
                        cols.Append(" ,'採貝' AS GYOHO_06"); // 採貝見出し
                        cols.Append(" ,'深海一本釣' AS GYOHO_07"); // 深海一本釣見出し
                        cols.Append(" ,'いか曳' AS GYOHO_08"); // いか曳見出し
                        cols.Append(" ,'" + printDateFr + "' AS printDateFr"); // 印刷対象日（開始）
                        cols.Append(" ,'" + printDateTo + "' AS printDateTo"); // 印刷対象日（終了）
                        cols.Append(" ,'" + today + "' AS today"); // 印刷実行日

                        // バインドパラメータの設定
                        DbParamCollection dpc = new DbParamCollection();
                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);

                        // データの取得
                        DataTable dtOutput = this.Dba.GetDataTableByConditionWithParams(
                            Util.ToString(cols), "PR_HN_TBL", "GUID = @GUID", "SORT ASC", dpc);

                        // 帳票オブジェクトをインスタンス化
                        HANR3131R rpt = new HANR3131R(dtOutput);

                        if (isPreview)
                        {
                            // プレビュー画面表示
                            PreviewForm pFrm = new PreviewForm(rpt, this.UnqId);
                            pFrm.WindowState = FormWindowState.Maximized;
                            pFrm.Show();
                        }
                        else
                        {
                            // 直接印刷
                            rpt.Run(false);
                            rpt.Document.Print(true, true, false);
                        }
                    }
                    else
                    {
                        // ２つ目
                        // 取得列の定義
                        StringBuilder cols = new StringBuilder();
                        cols.Clear();
                        cols.Append("  ITEM01"); // 船主CD
                        cols.Append(" ,ITEM02"); // 地区CD
                        cols.Append(" ,ITEM03"); // 船主名称
                        cols.Append(" ,ITEM04"); // 地区名
                        cols.Append(" ,CONVERT(decimal(9, 0), ITEM05) AS ITEM05"); // 日数
                        cols.Append(" ,CONVERT(decimal(9, 2), ITEM22) AS ITEM06"); // 素潜り数量
                        cols.Append(" ,CONVERT(decimal(9, 0), ITEM23) AS ITEM07"); // 素潜り金額
                        cols.Append(" ,CONVERT(decimal(9, 2), ITEM24) AS ITEM08"); // 近海鮪延縄数量
                        cols.Append(" ,CONVERT(decimal(9, 0), ITEM25) AS ITEM09"); // 近海鮪延縄金額
                        cols.Append(" ,CONVERT(decimal(9, 2), ITEM26) AS ITEM10"); // 曳網（パヤオ）数量
                        cols.Append(" ,CONVERT(decimal(9, 0), ITEM27) AS ITEM11"); // 曳網（パヤオ）金額
                        cols.Append(" ,CONVERT(decimal(9, 2), ITEM28) AS ITEM12"); // 延縄数量
                        cols.Append(" ,CONVERT(decimal(9, 0), ITEM29) AS ITEM13"); // 延縄金額
                        cols.Append(" ,CONVERT(decimal(9, 2), ITEM30) AS ITEM14"); // その他漁法数量
                        cols.Append(" ,CONVERT(decimal(9, 0), ITEM31) AS ITEM15"); // その他漁法金額
                        cols.Append(" ,CONVERT(decimal(9, 2), ITEM32) AS ITEM16"); // 数量合計
                        cols.Append(" ,CONVERT(decimal(9, 0), ITEM33) AS ITEM17"); // 金額合計
                        cols.Append(" ,'素潜り' AS GYOHO_01"); // 素潜り見出し
                        cols.Append(" ,'近海鮪延縄' AS GYOHO_02"); // 近海鮪延縄見出し
                        cols.Append(" ,'曳網（パヤオ）' AS GYOHO_03"); // 曳網（パヤオ）見出し
                        cols.Append(" ,'延縄' AS GYOHO_04"); // エビ類見出し
                        cols.Append(" ,'その他漁法' AS GYOHO_05"); // その他漁法見出し
                        cols.Append(" ,'合計' AS GYOHO_06"); // 合計見出し
                        cols.Append(" ,'" + printDateFr + "' AS printDateFr"); // 印刷対象日（開始）
                        cols.Append(" ,'" + printDateTo + "' AS printDateTo"); // 印刷対象日（終了）
                        cols.Append(" ,'" + today + "' AS today"); // 印刷実行日

                        // バインドパラメータの設定は、１回目と同じ
                        DbParamCollection dpc = new DbParamCollection();
                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);

                        // データの取得
                        DataTable dtOutput = this.Dba.GetDataTableByConditionWithParams(
                            Util.ToString(cols), "PR_HN_TBL", "GUID = @GUID", "SORT ASC", dpc);

                        // ２つ目の帳票オブジェクトをインスタンス化
                        HANR3132R rpt2 = new HANR3132R(dtOutput);

                        
                        if (isPreview)
                        {
                            // プレビュー画面表示
                            PreviewForm pFrm = new PreviewForm(rpt2, this.UnqId);
                            pFrm.WindowState = FormWindowState.Maximized;
                            pFrm.Show();
                        }
                        else
                        {
                            // 直接印刷
                            rpt2.Run(false);
                            rpt2.Document.Print(true, true, false);
                        }
                    }
                }
            }
            finally
            {
                // 印刷後なので、帳票出力用ワークテーブルをロールバックで元に戻す
                this.Dba.Rollback();
            }

        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData()
        {
            // 日付範囲を西暦にして取得
            DateTime tmpDateFr = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                    this.txtDateMonthFr.Text, "1", this.Dba);
            DateTime tmpDateTo = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                    this.txtDateMonthTo.Text, "1", this.Dba);
            DateTime tmpDateTo2 = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                    this.txtDateMonthTo.Text, Util.ToString(DateTime.DaysInMonth(tmpDateTo.Year, Util.ToInt(this.txtDateMonthTo.Text))), this.Dba);

            // 入力された情報を元にワークテーブルに更新をする
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder Sql = new StringBuilder();

            // han.VI_地区別個人別魚類別漁獲高月報(VI_HN_GYORUIBETSU_GKKDK_GKKI)の日付範囲に該当するデータを取得
            Sql.Append(" SELECT");
            Sql.Append("     MAX(CHIKU_CD) AS CHIKU_CD,");
            Sql.Append("     MAX(SENSHU_CD) AS SENSHU_CD,");
            Sql.Append("     MAX(CHIKU_NM) AS CHIKU_NM,");
            Sql.Append("     MAX(SENSHU_NM) AS SENSHU_NM,");
            Sql.Append("     SUM(NISSU) AS NISSU,");
            Sql.Append("     SUM(SENKAI_IPPONZURI_SURYO) AS SENKAI_IPPONZURI_SURYO,");
            Sql.Append("     SUM(SENKAI_IPPONZURI_KINGAKU) AS SENKAI_IPPONZURI_KINGAKU,");
            Sql.Append("     SUM(HIKINAWA_SURYO) AS HIKINAWA_SURYO,");
            Sql.Append("     SUM(HIKINAWA_KINGAKU) AS HIKINAWA_KINGAKU,");
            Sql.Append("     SUM(SENKAI_SASHIAMI_SURYO) AS SENKAI_SASHIAMI_SURYO,");
            Sql.Append("     SUM(SENKAI_SASHIAMI_KINGAKU) AS SENKAI_SASHIAMI_KINGAKU,");
            Sql.Append("     SUM(OIKOMIAMI_SURYO) AS OIKOMIAMI_SURYO,");
            Sql.Append("     SUM(OIKOMIAMI_KINGAKU) AS OIKOMIAMI_KINGAKU,");
            Sql.Append("     SUM(SENSUIKI_SURYO) AS SENSUIKI_SURYO,");
            Sql.Append("     SUM(SENSUIKI_KINGAKU) AS SENSUIKI_KINGAKU,");
            Sql.Append("     SUM(SAIKAI_SURYO) AS SAIKAI_SURYO,");
            Sql.Append("     SUM(SAIKAI_KINGAKU) AS SAIKAI_KINGAKU,");
            Sql.Append("     SUM(SHINKAI_IPPONZURI_SURYO) AS SHINKAI_IPPONZURI_SURYO,");
            Sql.Append("     SUM(SHINKAI_IPPONZURI_KINGAKU) AS SHINKAI_IPPONZURI_KINGAKU,");
            Sql.Append("     SUM(IKAHIKI_SURYO) AS IKAHIKI_SURYO,");
            Sql.Append("     SUM(IKAHIKI_KINGAKU) AS IKAHIKI_KINGAKU,");
            Sql.Append("     SUM(SUMOGURI_SURYO) AS SUMOGURI_SURYO,");
            Sql.Append("     SUM(SUMOGURI_KINGAKU) AS SUMOGURI_KINGAKU,");
            Sql.Append("     SUM(KINKAI_MAGURO_HAENAWA_SURYO) AS KINKAI_MAGURO_HAENAWA_SURYO,");
            Sql.Append("     SUM(KINKAI_MAGURO_HAENAWA_KINGAKU) AS KINKAI_MAGURO_HAENAWA_KINGAKU,");
            Sql.Append("     SUM(SHIMENAWA_PAYAO_SURYO) AS SHIMENAWA_PAYAO_SURYO,");
            Sql.Append("     SUM(SHIMENAWA_PAYAO_KINGAKU) AS SHIMENAWA_PAYAO_KINGAKU,");
            Sql.Append("     SUM(HAENAWA_SURYO) AS HAENAWA_SURYO,");
            Sql.Append("     SUM(HAENAWA_KINGAKU) AS HAENAWA_KINGAKU,");
            Sql.Append("     SUM(SONOTA_GYOHO_SURYO) AS SONOTA_GYOHO_SURYO,");
            Sql.Append("     SUM(SONOTA_GYOHO_KINGAKU) AS SONOTA_GYOHO_KINGAKU,");
            Sql.Append("     SUM(SURYO_GOKEI) AS SURYO_GOKEI,");
            Sql.Append("     SUM(KINGAKU_GOKEI) AS KINGAKU_GOKEI ");
            Sql.Append(" FROM");
            Sql.Append("     VI_HN_GYOTAIBETSU_GKKDK_GKI"); // VI_業態別漁獲高月計 ビュー
            Sql.Append(" WHERE");
            // 高速化のため、年をまたぐ場合とそうでない場合で検索条件を変える
            // （月報であれば、SHUKEI_NEN = @DATE_FR_NEN AND SHUKEI_TSUKI = @DATE_FR_TSUKI で済む）
            if (tmpDateFr.Year == tmpDateTo2.Year)
            {
                // 開始年と終了年が同じ場合
                Sql.Append("     SHUKEI_NEN = @DATE_FR_NEN");
                Sql.Append("   AND (SHUKEI_TSUKI BETWEEN @DATE_FR_TSUKI AND @DATE_TO_TSUKI) ");
            }
            else
            {
                // DateTime型で検索するより、WHEREが1.5倍ぐらい高速化した
                // 終了年月 ＜ 開始年月 でもHITするので、入力チェックでエラーにするように対処した。
                Sql.Append("     (SHUKEI_NEN = @DATE_FR_NEN AND (SHUKEI_TSUKI BETWEEN @DATE_FR_TSUKI AND 12)) ");
                Sql.Append("  OR (SHUKEI_NEN = @DATE_TO_NEN AND (SHUKEI_TSUKI BETWEEN 1 AND @DATE_TO_TSUKI)) ");

                // 開始・終了年の間の年は、全件取得対象にする
                if (2 <= (tmpDateFr.Year - tmpDateTo2.Year))
                {
                    Sql.Append("  OR (SHUKEI_NEN BETWEEN @DATE_FR_NEN + 1 AND @DATE_TO_NEN - 1) ");
                }
            }
            Sql.Append("   AND KAISHA_CD = @KAISHA_CD "); // WHERE ここまで
            Sql.Append(" GROUP BY"); // 重要なのでもう一度言います。「月報ではなくて、累計です。」
            Sql.Append("     CHIKU_CD,");
            Sql.Append("     SENSHU_CD ");
            Sql.Append(" ORDER BY"); // MS-SQLは、GROUP BY で並び替えられない
            Sql.Append("     CHIKU_CD DESC,");
            Sql.Append("     SENSHU_CD ASC ");

            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@DATE_FR_NEN", SqlDbType.Int, 10, tmpDateFr.Year);
            dpc.SetParam("@DATE_FR_TSUKI", SqlDbType.Int, 10, tmpDateFr.Month);
            dpc.SetParam("@DATE_TO_NEN", SqlDbType.Int, 10, tmpDateTo2.Year);
            dpc.SetParam("@DATE_TO_TSUKI", SqlDbType.Int, 10, tmpDateTo2.Month);

            DataTable dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            if (dtMainLoop.Rows.Count == 0)
            {
                Msg.Info("該当データがありません。");
                return false;
            }

            int i = 0; // SORT順

            foreach (DataRow dr in dtMainLoop.Rows)
            {
                Sql = new StringBuilder();
                dpc = new DbParamCollection();
                Sql.Append("INSERT INTO PR_HN_TBL(");
                Sql.Append("  GUID");
                Sql.Append(" ,SORT");
                Sql.Append(" ,ITEM01");
                Sql.Append(" ,ITEM02");
                Sql.Append(" ,ITEM03");
                Sql.Append(" ,ITEM04");
                Sql.Append(" ,ITEM05");
                Sql.Append(" ,ITEM06");
                Sql.Append(" ,ITEM07");
                Sql.Append(" ,ITEM08");
                Sql.Append(" ,ITEM09");
                Sql.Append(" ,ITEM10");
                Sql.Append(" ,ITEM11");
                Sql.Append(" ,ITEM12");
                Sql.Append(" ,ITEM13");
                Sql.Append(" ,ITEM14");
                Sql.Append(" ,ITEM15");
                Sql.Append(" ,ITEM16");
                Sql.Append(" ,ITEM17");
                Sql.Append(" ,ITEM18");
                Sql.Append(" ,ITEM19");
                Sql.Append(" ,ITEM20");
                Sql.Append(" ,ITEM21");
                Sql.Append(" ,ITEM22");
                Sql.Append(" ,ITEM23");
                Sql.Append(" ,ITEM24");
                Sql.Append(" ,ITEM25");
                Sql.Append(" ,ITEM26");
                Sql.Append(" ,ITEM27");
                Sql.Append(" ,ITEM28");
                Sql.Append(" ,ITEM29");
                Sql.Append(" ,ITEM30");
                Sql.Append(" ,ITEM31");
                Sql.Append(" ,ITEM32");
                Sql.Append(" ,ITEM33");
                Sql.Append(") ");
                Sql.Append("VALUES(");
                Sql.Append("  @GUID");
                Sql.Append(" ,@SORT");
                Sql.Append(" ,@ITEM01");
                Sql.Append(" ,@ITEM02");
                Sql.Append(" ,@ITEM03");
                Sql.Append(" ,@ITEM04");
                Sql.Append(" ,@ITEM05");
                Sql.Append(" ,@ITEM06");
                Sql.Append(" ,@ITEM07");
                Sql.Append(" ,@ITEM08");
                Sql.Append(" ,@ITEM09");
                Sql.Append(" ,@ITEM10");
                Sql.Append(" ,@ITEM11");
                Sql.Append(" ,@ITEM12");
                Sql.Append(" ,@ITEM13");
                Sql.Append(" ,@ITEM14");
                Sql.Append(" ,@ITEM15");
                Sql.Append(" ,@ITEM16");
                Sql.Append(" ,@ITEM17");
                Sql.Append(" ,@ITEM18");
                Sql.Append(" ,@ITEM19");
                Sql.Append(" ,@ITEM20");
                Sql.Append(" ,@ITEM21");
                Sql.Append(" ,@ITEM22");
                Sql.Append(" ,@ITEM23");
                Sql.Append(" ,@ITEM24");
                Sql.Append(" ,@ITEM25");
                Sql.Append(" ,@ITEM26");
                Sql.Append(" ,@ITEM27");
                Sql.Append(" ,@ITEM28");
                Sql.Append(" ,@ITEM29");
                Sql.Append(" ,@ITEM30");
                Sql.Append(" ,@ITEM31");
                Sql.Append(" ,@ITEM32");
                Sql.Append(" ,@ITEM33");
                Sql.Append(") ");
                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                dpc.SetParam("@SORT", SqlDbType.VarChar, 4, i);
                dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dr["CHIKU_CD"]);
                dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dr["SENSHU_CD"]);
                dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dr["CHIKU_NM"]);
                dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dr["SENSHU_NM"]);
                dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, dr["NISSU"]);
                dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, dr["SENKAI_IPPONZURI_SURYO"]);
                dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, dr["SENKAI_IPPONZURI_KINGAKU"]);
                dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, dr["HIKINAWA_SURYO"]);
                dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, dr["HIKINAWA_KINGAKU"]);
                dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, dr["SENKAI_SASHIAMI_SURYO"]);
                dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, dr["SENKAI_SASHIAMI_KINGAKU"]);
                dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, dr["OIKOMIAMI_SURYO"]);
                dpc.SetParam("@ITEM13", SqlDbType.VarChar, 200, dr["OIKOMIAMI_KINGAKU"]);
                dpc.SetParam("@ITEM14", SqlDbType.VarChar, 200, dr["SENSUIKI_SURYO"]);
                dpc.SetParam("@ITEM15", SqlDbType.VarChar, 200, dr["SENSUIKI_KINGAKU"]);
                dpc.SetParam("@ITEM16", SqlDbType.VarChar, 200, dr["SAIKAI_SURYO"]);
                dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, dr["SAIKAI_KINGAKU"]);
                dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, dr["SHINKAI_IPPONZURI_SURYO"]);
                dpc.SetParam("@ITEM19", SqlDbType.VarChar, 200, dr["SHINKAI_IPPONZURI_KINGAKU"]);
                dpc.SetParam("@ITEM20", SqlDbType.VarChar, 200, dr["IKAHIKI_SURYO"]);
                dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, dr["IKAHIKI_KINGAKU"]);
                dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, dr["SUMOGURI_SURYO"]);
                dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, dr["SUMOGURI_KINGAKU"]);
                dpc.SetParam("@ITEM24", SqlDbType.VarChar, 200, dr["KINKAI_MAGURO_HAENAWA_SURYO"]);
                dpc.SetParam("@ITEM25", SqlDbType.VarChar, 200, dr["KINKAI_MAGURO_HAENAWA_KINGAKU"]);
                dpc.SetParam("@ITEM26", SqlDbType.VarChar, 200, dr["SHIMENAWA_PAYAO_SURYO"]);
                dpc.SetParam("@ITEM27", SqlDbType.VarChar, 200, dr["SHIMENAWA_PAYAO_KINGAKU"]);
                dpc.SetParam("@ITEM28", SqlDbType.VarChar, 200, dr["HAENAWA_SURYO"]);
                dpc.SetParam("@ITEM29", SqlDbType.VarChar, 200, dr["HAENAWA_KINGAKU"]);
                dpc.SetParam("@ITEM30", SqlDbType.VarChar, 200, dr["SONOTA_GYOHO_SURYO"]);
                dpc.SetParam("@ITEM31", SqlDbType.VarChar, 200, dr["SONOTA_GYOHO_KINGAKU"]);
                dpc.SetParam("@ITEM32", SqlDbType.VarChar, 200, dr["SURYO_GOKEI"]);
                dpc.SetParam("@ITEM33", SqlDbType.VarChar, 200, dr["KINGAKU_GOKEI"]);

                this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                i++;
            }

            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
                "count(*) AS rows",
                "PR_HN_TBL",
                "GUID = @GUID",
                dpc);

            bool dataFlag;
            if (Util.ToInt(tmpdtPR_HN_TBL.Rows[0].ItemArray[0]) > 0)
            {
                dataFlag = true;
            }
            else
            {
                dataFlag = false;
            }

            return dataFlag;

        }

        #endregion

    }
}
