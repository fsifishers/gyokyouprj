﻿using System;
using System.Data;
using System.Drawing;

using jp.co.fsi.common.report;
using jp.co.fsi.common.util;

namespace jp.co.fsi.han.hanr3131
{
    /// <summary>
    /// HANR3131R 地区別個人別業態別漁獲高月報  ※実は累計出力！
    /// </summary>
    public partial class HANR3131R : BaseReport
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="tgtData">出力対象データ</param>
        public HANR3131R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }

    }
}
