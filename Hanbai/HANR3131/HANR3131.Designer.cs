﻿namespace jp.co.fsi.han.hanr3131
{
    partial class HANR3131
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbxDate = new System.Windows.Forms.GroupBox();
            this.lblCodeBetDate = new System.Windows.Forms.Label();
            this.lblDateMonthTo = new System.Windows.Forms.Label();
            this.lblDateYearTo = new System.Windows.Forms.Label();
            this.txtDateYearTo = new jp.co.fsi.common.controls.SosTextBox();
            this.txtDateMonthTo = new jp.co.fsi.common.controls.SosTextBox();
            this.lblDateGengoTo = new System.Windows.Forms.Label();
            this.lblDateTo = new System.Windows.Forms.Label();
            this.lblDateMonthFr = new System.Windows.Forms.Label();
            this.lblDateYearFr = new System.Windows.Forms.Label();
            this.txtDateYearFr = new jp.co.fsi.common.controls.SosTextBox();
            this.txtDateMonthFr = new jp.co.fsi.common.controls.SosTextBox();
            this.lblDateGengoFr = new System.Windows.Forms.Label();
            this.lblDateFr = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnYoshiB4 = new System.Windows.Forms.RadioButton();
            this.btnYoshiA4 = new System.Windows.Forms.RadioButton();
            this.pnlDebug.SuspendLayout();
            this.gbxDate.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Text = "";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(5, 531);
            this.pnlDebug.Size = new System.Drawing.Size(843, 100);
            // 
            // gbxDate
            // 
            this.gbxDate.Controls.Add(this.lblCodeBetDate);
            this.gbxDate.Controls.Add(this.lblDateMonthTo);
            this.gbxDate.Controls.Add(this.lblDateYearTo);
            this.gbxDate.Controls.Add(this.txtDateYearTo);
            this.gbxDate.Controls.Add(this.txtDateMonthTo);
            this.gbxDate.Controls.Add(this.lblDateGengoTo);
            this.gbxDate.Controls.Add(this.lblDateTo);
            this.gbxDate.Controls.Add(this.lblDateMonthFr);
            this.gbxDate.Controls.Add(this.lblDateYearFr);
            this.gbxDate.Controls.Add(this.txtDateYearFr);
            this.gbxDate.Controls.Add(this.txtDateMonthFr);
            this.gbxDate.Controls.Add(this.lblDateGengoFr);
            this.gbxDate.Controls.Add(this.lblDateFr);
            this.gbxDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxDate.ForeColor = System.Drawing.Color.Black;
            this.gbxDate.Location = new System.Drawing.Point(12, 50);
            this.gbxDate.Name = "gbxDate";
            this.gbxDate.Size = new System.Drawing.Size(411, 75);
            this.gbxDate.TabIndex = 1;
            this.gbxDate.TabStop = false;
            this.gbxDate.Text = "日付範囲";
            // 
            // lblCodeBetDate
            // 
            this.lblCodeBetDate.BackColor = System.Drawing.Color.White;
            this.lblCodeBetDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblCodeBetDate.ForeColor = System.Drawing.Color.Black;
            this.lblCodeBetDate.Location = new System.Drawing.Point(183, 31);
            this.lblCodeBetDate.Name = "lblCodeBetDate";
            this.lblCodeBetDate.Size = new System.Drawing.Size(43, 21);
            this.lblCodeBetDate.TabIndex = 902;
            this.lblCodeBetDate.Text = "～";
            this.lblCodeBetDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblDateMonthTo
            // 
            this.lblDateMonthTo.BackColor = System.Drawing.Color.Silver;
            this.lblDateMonthTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateMonthTo.ForeColor = System.Drawing.Color.Black;
            this.lblDateMonthTo.Location = new System.Drawing.Point(363, 32);
            this.lblDateMonthTo.Name = "lblDateMonthTo";
            this.lblDateMonthTo.Size = new System.Drawing.Size(15, 19);
            this.lblDateMonthTo.TabIndex = 13;
            this.lblDateMonthTo.Text = "月";
            this.lblDateMonthTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateYearTo
            // 
            this.lblDateYearTo.BackColor = System.Drawing.Color.Silver;
            this.lblDateYearTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateYearTo.ForeColor = System.Drawing.Color.Black;
            this.lblDateYearTo.Location = new System.Drawing.Point(310, 31);
            this.lblDateYearTo.Name = "lblDateYearTo";
            this.lblDateYearTo.Size = new System.Drawing.Size(17, 21);
            this.lblDateYearTo.TabIndex = 11;
            this.lblDateYearTo.Text = "年";
            this.lblDateYearTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDateYearTo
            // 
            this.txtDateYearTo.AutoSizeFromLength = false;
            this.txtDateYearTo.DisplayLength = null;
            this.txtDateYearTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateYearTo.ForeColor = System.Drawing.Color.Black;
            this.txtDateYearTo.Location = new System.Drawing.Point(278, 31);
            this.txtDateYearTo.MaxLength = 2;
            this.txtDateYearTo.Name = "txtDateYearTo";
            this.txtDateYearTo.Size = new System.Drawing.Size(30, 20);
            this.txtDateYearTo.TabIndex = 10;
            this.txtDateYearTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateYearTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateYearTo_Validating);
            // 
            // txtDateMonthTo
            // 
            this.txtDateMonthTo.AutoSizeFromLength = false;
            this.txtDateMonthTo.DisplayLength = null;
            this.txtDateMonthTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateMonthTo.ForeColor = System.Drawing.Color.Black;
            this.txtDateMonthTo.Location = new System.Drawing.Point(331, 31);
            this.txtDateMonthTo.MaxLength = 2;
            this.txtDateMonthTo.Name = "txtDateMonthTo";
            this.txtDateMonthTo.Size = new System.Drawing.Size(30, 20);
            this.txtDateMonthTo.TabIndex = 12;
            this.txtDateMonthTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateMonthTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtDateMonthTo_KeyDown);
            this.txtDateMonthTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateMonthTo_Validating);
            // 
            // lblDateGengoTo
            // 
            this.lblDateGengoTo.BackColor = System.Drawing.Color.Silver;
            this.lblDateGengoTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDateGengoTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateGengoTo.ForeColor = System.Drawing.Color.Black;
            this.lblDateGengoTo.Location = new System.Drawing.Point(233, 31);
            this.lblDateGengoTo.Name = "lblDateGengoTo";
            this.lblDateGengoTo.Size = new System.Drawing.Size(41, 21);
            this.lblDateGengoTo.TabIndex = 8;
            this.lblDateGengoTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateTo
            // 
            this.lblDateTo.BackColor = System.Drawing.Color.Silver;
            this.lblDateTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateTo.ForeColor = System.Drawing.Color.Black;
            this.lblDateTo.Location = new System.Drawing.Point(230, 28);
            this.lblDateTo.Name = "lblDateTo";
            this.lblDateTo.Size = new System.Drawing.Size(159, 27);
            this.lblDateTo.TabIndex = 9;
            this.lblDateTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateMonthFr
            // 
            this.lblDateMonthFr.BackColor = System.Drawing.Color.Silver;
            this.lblDateMonthFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateMonthFr.ForeColor = System.Drawing.Color.Black;
            this.lblDateMonthFr.Location = new System.Drawing.Point(152, 32);
            this.lblDateMonthFr.Name = "lblDateMonthFr";
            this.lblDateMonthFr.Size = new System.Drawing.Size(15, 19);
            this.lblDateMonthFr.TabIndex = 5;
            this.lblDateMonthFr.Text = "月";
            this.lblDateMonthFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateYearFr
            // 
            this.lblDateYearFr.BackColor = System.Drawing.Color.Silver;
            this.lblDateYearFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateYearFr.ForeColor = System.Drawing.Color.Black;
            this.lblDateYearFr.Location = new System.Drawing.Point(99, 31);
            this.lblDateYearFr.Name = "lblDateYearFr";
            this.lblDateYearFr.Size = new System.Drawing.Size(17, 21);
            this.lblDateYearFr.TabIndex = 3;
            this.lblDateYearFr.Text = "年";
            this.lblDateYearFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDateYearFr
            // 
            this.txtDateYearFr.AutoSizeFromLength = false;
            this.txtDateYearFr.DisplayLength = null;
            this.txtDateYearFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateYearFr.ForeColor = System.Drawing.Color.Black;
            this.txtDateYearFr.Location = new System.Drawing.Point(67, 31);
            this.txtDateYearFr.MaxLength = 2;
            this.txtDateYearFr.Name = "txtDateYearFr";
            this.txtDateYearFr.Size = new System.Drawing.Size(30, 20);
            this.txtDateYearFr.TabIndex = 2;
            this.txtDateYearFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateYearFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateYearFr_Validating);
            // 
            // txtDateMonthFr
            // 
            this.txtDateMonthFr.AutoSizeFromLength = false;
            this.txtDateMonthFr.DisplayLength = null;
            this.txtDateMonthFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateMonthFr.ForeColor = System.Drawing.Color.Black;
            this.txtDateMonthFr.Location = new System.Drawing.Point(120, 31);
            this.txtDateMonthFr.MaxLength = 2;
            this.txtDateMonthFr.Name = "txtDateMonthFr";
            this.txtDateMonthFr.Size = new System.Drawing.Size(30, 20);
            this.txtDateMonthFr.TabIndex = 4;
            this.txtDateMonthFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateMonthFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateMonthFr_Validating);
            // 
            // lblDateGengoFr
            // 
            this.lblDateGengoFr.BackColor = System.Drawing.Color.Silver;
            this.lblDateGengoFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDateGengoFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateGengoFr.ForeColor = System.Drawing.Color.Black;
            this.lblDateGengoFr.Location = new System.Drawing.Point(22, 31);
            this.lblDateGengoFr.Name = "lblDateGengoFr";
            this.lblDateGengoFr.Size = new System.Drawing.Size(41, 21);
            this.lblDateGengoFr.TabIndex = 1;
            this.lblDateGengoFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateFr
            // 
            this.lblDateFr.BackColor = System.Drawing.Color.Silver;
            this.lblDateFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateFr.ForeColor = System.Drawing.Color.Black;
            this.lblDateFr.Location = new System.Drawing.Point(19, 28);
            this.lblDateFr.Name = "lblDateFr";
            this.lblDateFr.Size = new System.Drawing.Size(159, 27);
            this.lblDateFr.TabIndex = 1;
            this.lblDateFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnYoshiA4);
            this.groupBox1.Controls.Add(this.btnYoshiB4);
            this.groupBox1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBox1.ForeColor = System.Drawing.Color.Black;
            this.groupBox1.Location = new System.Drawing.Point(447, 50);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(180, 75);
            this.groupBox1.TabIndex = 902;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "用紙";
            // 
            // btnYoshiB4
            // 
            this.btnYoshiB4.AutoSize = true;
            this.btnYoshiB4.Font = new System.Drawing.Font("ＭＳ ゴシック", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnYoshiB4.Location = new System.Drawing.Point(26, 31);
            this.btnYoshiB4.Name = "btnYoshiB4";
            this.btnYoshiB4.Size = new System.Drawing.Size(57, 19);
            this.btnYoshiB4.TabIndex = 0;
            this.btnYoshiB4.TabStop = true;
            this.btnYoshiB4.Text = "Ｂ４";
            this.btnYoshiB4.UseVisualStyleBackColor = true;
            // 
            // btnYoshiA4
            // 
            this.btnYoshiA4.AutoSize = true;
            this.btnYoshiA4.Font = new System.Drawing.Font("ＭＳ ゴシック", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnYoshiA4.Location = new System.Drawing.Point(105, 31);
            this.btnYoshiA4.Name = "btnYoshiA4";
            this.btnYoshiA4.Size = new System.Drawing.Size(57, 19);
            this.btnYoshiA4.TabIndex = 1;
            this.btnYoshiA4.TabStop = true;
            this.btnYoshiA4.Text = "Ａ４";
            this.btnYoshiA4.UseVisualStyleBackColor = true;
            // 
            // HANR3131
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(835, 634);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.gbxDate);
            this.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "HANR3131";
            this.Text = "";
            this.Controls.SetChildIndex(this.gbxDate, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.pnlDebug.ResumeLayout(false);
            this.gbxDate.ResumeLayout(false);
            this.gbxDate.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbxDate;
        private jp.co.fsi.common.controls.SosTextBox txtDateYearFr;
        private System.Windows.Forms.Label lblDateGengoFr;
        private System.Windows.Forms.Label lblDateFr;
        private System.Windows.Forms.Label lblDateMonthFr;
        private System.Windows.Forms.Label lblDateYearFr;
        private jp.co.fsi.common.controls.SosTextBox txtDateMonthFr;
        private System.Windows.Forms.Label lblCodeBetDate;
        private System.Windows.Forms.Label lblDateMonthTo;
        private System.Windows.Forms.Label lblDateYearTo;
        private common.controls.SosTextBox txtDateYearTo;
        private common.controls.SosTextBox txtDateMonthTo;
        private System.Windows.Forms.Label lblDateGengoTo;
        private System.Windows.Forms.Label lblDateTo;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton btnYoshiA4;
        private System.Windows.Forms.RadioButton btnYoshiB4;

    }
}