﻿using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.han.hanc9121
{
    /// <summary>
    /// 控除項目の登録(HANC9121)
    /// </summary>
    public partial class HANC9121 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// モード(コード検索)
        /// </summary>
        private const string MODE_CD_SRC = "1";
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HANC9121()
        {
            InitializeComponent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.InitForm();は呼び出さなくて構いません。
        /// また、このメソッド内の処理を外出しでこのクラス内にメソッド化するのは構いませんが、
        /// 原則、独自で起動時のイベント処理を実装することは禁じます。
        /// </remarks>
        protected override void InitForm()
        {
            // Par1が"1"の場合、コード検索画面としての挙動をする
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                // Par1が"1"の場合、コード検索画面としての挙動をする
                // タイトルは非表示
                this.lblTitle.Visible = false;
                // サイズを縮める
                this.Size = new Size(698, 500);
                // フォームの配置を上へ移動する
                this.lblMeisho.Location = new System.Drawing.Point(12, 13);
                this.txtMeisho.Location = new System.Drawing.Point(96, 13);
                this.dgvList.Location = new System.Drawing.Point(12, 42);
                // EscapeとF1のみ表示
                this.ShowFButton = true;
                this.btnEsc.Location = this.btnF1.Location;
                this.btnEnter.Location = this.btnF2.Location;
                this.btnF1.Location = this.btnF3.Location;
                this.btnEsc.Visible = true;
                this.btnEnter.Visible = true;
                this.btnF1.Visible = true;
                this.btnF2.Visible = false;
                this.btnF3.Visible = false;
                this.btnF4.Visible = true;
                this.btnF5.Visible = false;
                this.btnF6.Visible = false;
                this.btnF7.Visible = false;
                this.btnF8.Visible = false;
                this.btnF9.Visible = false;
                this.btnF10.Visible = false;
                this.btnF11.Visible = false;
                this.btnF12.Visible = false;
            }

            // まずデータが存在し得ない検索条件で検索をし、結果をバインドすることで、
            // 初期状態を作り出す
            SearchData(true);

            // カナ名にフォーカス
            this.txtMeisho.Focus();
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                // Par1が"1"の場合、ダイアログとしての処理結果を返却する
                this.DialogResult = DialogResult.Cancel;
            }
            base.PressEsc();
        }

        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            // 名称にフォーカスを戻す
            this.txtMeisho.Focus();
            this.txtMeisho.SelectAll();
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF4();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF4()
        {
            // 控除項目の登録画面の起動
            EditKojoKmok(string.Empty);
        }
        #endregion

        #region イベント
        /// <summary>
        /// カナ名検証時の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKanaName_Validating(object sender, CancelEventArgs e)
        {
            // 入力された情報を元に検索する
            SearchData(false);
        }

        /// <summary>
        /// グリッドでのキーダウン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                EditKojoKmok(Util.ToString(this.dgvList.SelectedRows[0].Cells["設定コード"].Value));
                e.Handled = true;
            }
        }

        /// <summary>
        /// グリッドのセルダブルクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            EditKojoKmok(Util.ToString(this.dgvList.SelectedRows[0].Cells["設定コード"].Value));
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// データを検索する
        /// </summary>
        /// <param name="isInitial">初期処理であるかどうか</param>
        private void SearchData(bool isInitial)
        {
            // 控除科目ビューからデータを取得して表示
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 6, this.UInfo.KaikeiNendo);
            StringBuilder where = new StringBuilder("HN.KAISHA_CD = @KAISHA_CD");
            where.Append(" AND HN.KAIKEI_NENDO = @KAIKEI_NENDO");
            if (isInitial)
            {
                //where.Append(" AND HN.SETTEI_CD = -1");
            }
            else
            {
                // 初期処理でない場合、入力されたカナ名から検索する
                if (!ValChk.IsEmpty(this.txtMeisho.Text))
                {
                    where.Append(" AND HN.KOJO_KOMOKU_NM LIKE @KOJO_KOMOKU_NM");
                    
                    // NOTICE!:部分一致検索をする場合、"%"の分桁数に2バイトを足すこと
                    dpc.SetParam("@KOJO_KOMOKU_NM", SqlDbType.VarChar, 42, "%" + this.txtMeisho.Text + "%");
                }
            }
            string cols = "HN.SETTEI_CD AS 設定コード";
            cols += ", HN.KOJO_KOMOKU_SETTEI AS 控除項目設定";
            cols += ", HN.KOJO_KOMOKU_NM AS 控除項目名称";

            string from = "VI_HN_KOJO_KAMOKU_SETTEI AS HN";

            DataTable dtkanjo =
                this.Dba.GetDataTableByConditionWithParams(cols, from,
                    Util.ToString(where), "HN.SETTEI_CD", dpc);

            
                
            // 初期処理以外の場合、該当データがなければエラーメッセージを表示
            if (dtkanjo.Rows.Count == 0)
            {
                if (!isInitial)
                {
                    Msg.Info("該当データがありません。");
                }

                dtkanjo.Rows.Add(dtkanjo.NewRow());
            }

            this.dgvList.DataSource = dtkanjo;

            
            // ユーザーによるソートを禁止させる
            foreach (DataGridViewColumn c in this.dgvList.Columns)
                c.SortMode = DataGridViewColumnSortMode.NotSortable;

            // フォントを設定する
            this.dgvList.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F, FontStyle.Bold);
            this.dgvList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.dgvList.DefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F);

            // 列幅を設定する
            this.dgvList.Columns[0].Width = 140;
            this.dgvList.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvList.Columns[1].Width = 250;
            this.dgvList.Columns[2].Width = 250;
        }

        /// <summary>
        /// 控除項目を追加編集する
        /// </summary>
        /// <param name="code">設定コード(空：新規登録、以外：編集)</param>
        private void EditKojoKmok(string code)
        {
            HANC9122 frmZAMC9012;

            if (ValChk.IsEmpty(code))
            {
                // 新規登録モードで登録画面を起動
                frmZAMC9012 = new HANC9122("1");
            }
            else
            {
                // 編集モードで登録画面を起動
                frmZAMC9012 = new HANC9122("2");
                frmZAMC9012.InData = code;
            }

            DialogResult result = frmZAMC9012.ShowDialog(this);

            if (result == DialogResult.OK)
            {
                // データを再検索する
                SearchData(false);
                // 元々選択していたデータを選択
                for (int i = 0; i < this.dgvList.Rows.Count; i++)
                {
                    if (code.Equals(Util.ToString(this.dgvList.Rows[i].Cells["設定コード"].Value)))
                    {
                        this.dgvList.Rows[i].Selected = true;
                        break;
                    }
                }
                // Gridに再度フォーカスをセット
                this.ActiveControl = this.dgvList;
                this.dgvList.Focus();
            }
        }
        #endregion
    }
}
