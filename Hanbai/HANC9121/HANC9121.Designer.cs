﻿namespace jp.co.fsi.han.hanc9121
{
    partial class HANC9121
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblMeisho = new System.Windows.Forms.Label();
            this.txtMeisho = new System.Windows.Forms.TextBox();
            this.dgvList = new System.Windows.Forms.DataGridView();
            this.btnEnter = new System.Windows.Forms.Button();
            this.pnlDebug.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvList)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.TabIndex = 999;
            this.lblTitle.Text = "控除項目の登録";
            // 
            // btnF1
            // 
            this.btnF1.TabIndex = 2;
            // 
            // btnF2
            // 
            this.btnF2.TabIndex = 3;
            // 
            // btnF3
            // 
            this.btnF3.TabIndex = 4;
            // 
            // btnF4
            // 
            this.btnF4.TabIndex = 5;
            // 
            // btnF5
            // 
            this.btnF5.TabIndex = 6;
            // 
            // btnF7
            // 
            this.btnF7.TabIndex = 8;
            // 
            // btnF6
            // 
            this.btnF6.TabIndex = 7;
            // 
            // btnF8
            // 
            this.btnF8.TabIndex = 9;
            // 
            // btnF9
            // 
            this.btnF9.TabIndex = 10;
            // 
            // btnF12
            // 
            this.btnF12.TabIndex = 13;
            // 
            // btnF11
            // 
            this.btnF11.TabIndex = 12;
            // 
            // btnF10
            // 
            this.btnF10.TabIndex = 11;
            // 
            // pnlDebug
            // 
            this.pnlDebug.Controls.Add(this.btnEnter);
            this.pnlDebug.Size = new System.Drawing.Size(847, 100);
            this.pnlDebug.Controls.SetChildIndex(this.btnF6, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF7, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF5, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF8, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF4, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF9, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF3, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF10, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF2, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF11, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF1, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF12, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnEsc, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnEnter, 0);
            // 
            // lblMeisho
            // 
            this.lblMeisho.BackColor = System.Drawing.Color.Silver;
            this.lblMeisho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMeisho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMeisho.Location = new System.Drawing.Point(12, 44);
            this.lblMeisho.Name = "lblMeisho";
            this.lblMeisho.Size = new System.Drawing.Size(267, 25);
            this.lblMeisho.TabIndex = 0;
            this.lblMeisho.Text = "名　　　称";
            this.lblMeisho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtMeisho
            // 
            this.txtMeisho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMeisho.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtMeisho.Location = new System.Drawing.Point(96, 46);
            this.txtMeisho.MaxLength = 30;
            this.txtMeisho.Name = "txtMeisho";
            this.txtMeisho.Size = new System.Drawing.Size(180, 20);
            this.txtMeisho.TabIndex = 1;
            this.txtMeisho.Validating += new System.ComponentModel.CancelEventHandler(this.txtKanaName_Validating);
            // 
            // dgvList
            // 
            this.dgvList.AllowUserToAddRows = false;
            this.dgvList.AllowUserToDeleteRows = false;
            this.dgvList.AllowUserToResizeColumns = false;
            this.dgvList.AllowUserToResizeRows = false;
            this.dgvList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvList.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.dgvList.Location = new System.Drawing.Point(12, 75);
            this.dgvList.MultiSelect = false;
            this.dgvList.Name = "dgvList";
            this.dgvList.ReadOnly = true;
            this.dgvList.RowHeadersVisible = false;
            this.dgvList.RowTemplate.Height = 21;
            this.dgvList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvList.Size = new System.Drawing.Size(654, 359);
            this.dgvList.TabIndex = 2;
            this.dgvList.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvList_CellDoubleClick);
            this.dgvList.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvList_KeyDown);
            // 
            // btnEnter
            // 
            this.btnEnter.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnEnter.Location = new System.Drawing.Point(67, 3);
            this.btnEnter.Name = "btnEnter";
            this.btnEnter.Size = new System.Drawing.Size(65, 45);
            this.btnEnter.TabIndex = 1;
            this.btnEnter.TabStop = false;
            this.btnEnter.Text = "Enter\r\n\r\n決定";
            this.btnEnter.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnEnter.UseVisualStyleBackColor = true;
            // 
            // HANC9121
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 638);
            this.Controls.Add(this.dgvList);
            this.Controls.Add(this.txtMeisho);
            this.Controls.Add(this.lblMeisho);
            this.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.Name = "HANC9121";
            this.Text = "控除科目の登録";
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.lblMeisho, 0);
            this.Controls.SetChildIndex(this.txtMeisho, 0);
            this.Controls.SetChildIndex(this.dgvList, 0);
            this.pnlDebug.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblMeisho;
        private System.Windows.Forms.TextBox txtMeisho;
        private System.Windows.Forms.DataGridView dgvList;
        protected System.Windows.Forms.Button btnEnter;

    }
}