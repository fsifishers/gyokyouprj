﻿namespace jp.co.fsi.han.hanc9121
{
    partial class HANC9122
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtSetteiCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblSetteiCd = new System.Windows.Forms.Label();
            this.pnlMain = new jp.co.fsi.common.FsiPanel();
            this.lblBumonnCd = new System.Windows.Forms.Label();
            this.lblHojoKamokuCd = new System.Windows.Forms.Label();
            this.lblKanjoKamokuCd = new System.Windows.Forms.Label();
            this.lblKamokbn = new System.Windows.Forms.Label();
            this.lblTaishakKbnn = new System.Windows.Forms.Label();
            this.lblTaishakKbn = new System.Windows.Forms.Label();
            this.txtTaishakKbn = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblJigyoKubn = new System.Windows.Forms.Label();
            this.txtJigyoKbn = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblJigyoKbn = new System.Windows.Forms.Label();
            this.lblZeiKbnn = new System.Windows.Forms.Label();
            this.txtZeiKbn = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblZeiKbn = new System.Windows.Forms.Label();
            this.txtBumonCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblBumonCd = new System.Windows.Forms.Label();
            this.txtHojoKamokCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblHojoKamokCd = new System.Windows.Forms.Label();
            this.txtKanjoKamokCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKanjoKamokCd = new System.Windows.Forms.Label();
            this.txtKeisanFugo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKeisanhugo = new System.Windows.Forms.Label();
            this.txtKojoTaishoShikriKomok = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKojoTaishoShikriKomok = new System.Windows.Forms.Label();
            this.lblKojoKomokNm = new System.Windows.Forms.Label();
            this.txtKojoKomokSettei = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKojoKomokSettei = new System.Windows.Forms.Label();
            this.txtKojoKomokNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.pnlDebug.SuspendLayout();
            this.pnlMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Size = new System.Drawing.Size(412, 23);
            this.lblTitle.TabIndex = 999;
            this.lblTitle.Text = "";
            this.lblTitle.Visible = false;
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(5, 299);
            this.pnlDebug.Size = new System.Drawing.Size(445, 100);
            // 
            // txtSetteiCd
            // 
            this.txtSetteiCd.AutoSizeFromLength = true;
            this.txtSetteiCd.DisplayLength = null;
            this.txtSetteiCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtSetteiCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtSetteiCd.Location = new System.Drawing.Point(148, 15);
            this.txtSetteiCd.MaxLength = 6;
            this.txtSetteiCd.Name = "txtSetteiCd";
            this.txtSetteiCd.Size = new System.Drawing.Size(48, 20);
            this.txtSetteiCd.TabIndex = 1;
            this.txtSetteiCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSetteiCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtSetteiCd_Validating);
            // 
            // lblSetteiCd
            // 
            this.lblSetteiCd.BackColor = System.Drawing.Color.Silver;
            this.lblSetteiCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSetteiCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblSetteiCd.Location = new System.Drawing.Point(12, 13);
            this.lblSetteiCd.Name = "lblSetteiCd";
            this.lblSetteiCd.Size = new System.Drawing.Size(187, 25);
            this.lblSetteiCd.TabIndex = 0;
            this.lblSetteiCd.Text = "設定コード";
            this.lblSetteiCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnlMain
            // 
            this.pnlMain.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlMain.Controls.Add(this.lblBumonnCd);
            this.pnlMain.Controls.Add(this.lblHojoKamokuCd);
            this.pnlMain.Controls.Add(this.lblKanjoKamokuCd);
            this.pnlMain.Controls.Add(this.lblKamokbn);
            this.pnlMain.Controls.Add(this.lblTaishakKbnn);
            this.pnlMain.Controls.Add(this.txtTaishakKbn);
            this.pnlMain.Controls.Add(this.lblJigyoKubn);
            this.pnlMain.Controls.Add(this.txtJigyoKbn);
            this.pnlMain.Controls.Add(this.lblJigyoKbn);
            this.pnlMain.Controls.Add(this.lblZeiKbnn);
            this.pnlMain.Controls.Add(this.txtZeiKbn);
            this.pnlMain.Controls.Add(this.lblZeiKbn);
            this.pnlMain.Controls.Add(this.txtBumonCd);
            this.pnlMain.Controls.Add(this.lblBumonCd);
            this.pnlMain.Controls.Add(this.txtHojoKamokCd);
            this.pnlMain.Controls.Add(this.lblHojoKamokCd);
            this.pnlMain.Controls.Add(this.txtKanjoKamokCd);
            this.pnlMain.Controls.Add(this.txtKeisanFugo);
            this.pnlMain.Controls.Add(this.txtKojoTaishoShikriKomok);
            this.pnlMain.Controls.Add(this.lblKojoTaishoShikriKomok);
            this.pnlMain.Controls.Add(this.lblKeisanhugo);
            this.pnlMain.Controls.Add(this.lblKanjoKamokCd);
            this.pnlMain.Controls.Add(this.lblTaishakKbn);
            this.pnlMain.Location = new System.Drawing.Point(9, 99);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(421, 232);
            this.pnlMain.TabIndex = 6;
            // 
            // lblBumonnCd
            // 
            this.lblBumonnCd.BackColor = System.Drawing.Color.Silver;
            this.lblBumonnCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBumonnCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblBumonnCd.Location = new System.Drawing.Point(200, 118);
            this.lblBumonnCd.Name = "lblBumonnCd";
            this.lblBumonnCd.Size = new System.Drawing.Size(210, 20);
            this.lblBumonnCd.TabIndex = 13;
            this.lblBumonnCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblHojoKamokuCd
            // 
            this.lblHojoKamokuCd.BackColor = System.Drawing.Color.Silver;
            this.lblHojoKamokuCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblHojoKamokuCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblHojoKamokuCd.Location = new System.Drawing.Point(200, 90);
            this.lblHojoKamokuCd.Name = "lblHojoKamokuCd";
            this.lblHojoKamokuCd.Size = new System.Drawing.Size(210, 20);
            this.lblHojoKamokuCd.TabIndex = 10;
            this.lblHojoKamokuCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKanjoKamokuCd
            // 
            this.lblKanjoKamokuCd.BackColor = System.Drawing.Color.Silver;
            this.lblKanjoKamokuCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKanjoKamokuCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblKanjoKamokuCd.Location = new System.Drawing.Point(200, 62);
            this.lblKanjoKamokuCd.Name = "lblKanjoKamokuCd";
            this.lblKanjoKamokuCd.Size = new System.Drawing.Size(210, 20);
            this.lblKanjoKamokuCd.TabIndex = 7;
            this.lblKanjoKamokuCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKamokbn
            // 
            this.lblKamokbn.BackColor = System.Drawing.Color.Silver;
            this.lblKamokbn.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblKamokbn.Location = new System.Drawing.Point(200, 34);
            this.lblKamokbn.Name = "lblKamokbn";
            this.lblKamokbn.Size = new System.Drawing.Size(210, 20);
            this.lblKamokbn.TabIndex = 4;
            this.lblKamokbn.Text = "1:正　2:負";
            this.lblKamokbn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTaishakKbnn
            // 
            this.lblTaishakKbnn.BackColor = System.Drawing.Color.Silver;
            this.lblTaishakKbnn.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTaishakKbnn.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblTaishakKbnn.Location = new System.Drawing.Point(200, 202);
            this.lblTaishakKbnn.Name = "lblTaishakKbnn";
            this.lblTaishakKbnn.Size = new System.Drawing.Size(210, 20);
            this.lblTaishakKbnn.TabIndex = 22;
            this.lblTaishakKbnn.Text = "1:借方　2:貸方";
            this.lblTaishakKbnn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTaishakKbn
            // 
            this.lblTaishakKbn.BackColor = System.Drawing.Color.Silver;
            this.lblTaishakKbn.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTaishakKbn.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblTaishakKbn.Location = new System.Drawing.Point(3, 200);
            this.lblTaishakKbn.Name = "lblTaishakKbn";
            this.lblTaishakKbn.Size = new System.Drawing.Size(411, 25);
            this.lblTaishakKbn.TabIndex = 20;
            this.lblTaishakKbn.Text = "貸　借　区　分";
            this.lblTaishakKbn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTaishakKbn
            // 
            this.txtTaishakKbn.AutoSizeFromLength = true;
            this.txtTaishakKbn.DisplayLength = 2;
            this.txtTaishakKbn.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtTaishakKbn.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtTaishakKbn.Location = new System.Drawing.Point(138, 202);
            this.txtTaishakKbn.MaxLength = 3;
            this.txtTaishakKbn.Name = "txtTaishakKbn";
            this.txtTaishakKbn.Size = new System.Drawing.Size(55, 20);
            this.txtTaishakKbn.TabIndex = 21;
            this.txtTaishakKbn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTaishakKbn.Validating += new System.ComponentModel.CancelEventHandler(this.txtTaishakKbn_Validating);
            // 
            // lblJigyoKubn
            // 
            this.lblJigyoKubn.BackColor = System.Drawing.Color.Silver;
            this.lblJigyoKubn.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblJigyoKubn.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblJigyoKubn.Location = new System.Drawing.Point(200, 174);
            this.lblJigyoKubn.Name = "lblJigyoKubn";
            this.lblJigyoKubn.Size = new System.Drawing.Size(210, 20);
            this.lblJigyoKubn.TabIndex = 19;
            this.lblJigyoKubn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtJigyoKbn
            // 
            this.txtJigyoKbn.AutoSizeFromLength = true;
            this.txtJigyoKbn.DisplayLength = null;
            this.txtJigyoKbn.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtJigyoKbn.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtJigyoKbn.Location = new System.Drawing.Point(138, 174);
            this.txtJigyoKbn.MaxLength = 3;
            this.txtJigyoKbn.Name = "txtJigyoKbn";
            this.txtJigyoKbn.Size = new System.Drawing.Size(55, 20);
            this.txtJigyoKbn.TabIndex = 18;
            this.txtJigyoKbn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtJigyoKbn.Validating += new System.ComponentModel.CancelEventHandler(this.txtZigyoKbn_Validating);
            // 
            // lblJigyoKbn
            // 
            this.lblJigyoKbn.BackColor = System.Drawing.Color.Silver;
            this.lblJigyoKbn.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblJigyoKbn.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblJigyoKbn.Location = new System.Drawing.Point(3, 172);
            this.lblJigyoKbn.Name = "lblJigyoKbn";
            this.lblJigyoKbn.Size = new System.Drawing.Size(411, 25);
            this.lblJigyoKbn.TabIndex = 17;
            this.lblJigyoKbn.Text = "事　業　区　分";
            this.lblJigyoKbn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblZeiKbnn
            // 
            this.lblZeiKbnn.BackColor = System.Drawing.Color.Silver;
            this.lblZeiKbnn.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblZeiKbnn.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblZeiKbnn.Location = new System.Drawing.Point(200, 147);
            this.lblZeiKbnn.Name = "lblZeiKbnn";
            this.lblZeiKbnn.Size = new System.Drawing.Size(210, 20);
            this.lblZeiKbnn.TabIndex = 16;
            this.lblZeiKbnn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtZeiKbn
            // 
            this.txtZeiKbn.AutoSizeFromLength = true;
            this.txtZeiKbn.DisplayLength = null;
            this.txtZeiKbn.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtZeiKbn.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtZeiKbn.Location = new System.Drawing.Point(138, 146);
            this.txtZeiKbn.MaxLength = 4;
            this.txtZeiKbn.Name = "txtZeiKbn";
            this.txtZeiKbn.Size = new System.Drawing.Size(55, 20);
            this.txtZeiKbn.TabIndex = 15;
            this.txtZeiKbn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtZeiKbn.Validating += new System.ComponentModel.CancelEventHandler(this.txtZeiKbn_Validating);
            // 
            // lblZeiKbn
            // 
            this.lblZeiKbn.BackColor = System.Drawing.Color.Silver;
            this.lblZeiKbn.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblZeiKbn.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblZeiKbn.Location = new System.Drawing.Point(3, 144);
            this.lblZeiKbn.Name = "lblZeiKbn";
            this.lblZeiKbn.Size = new System.Drawing.Size(411, 25);
            this.lblZeiKbn.TabIndex = 14;
            this.lblZeiKbn.Text = "税　　区　　分";
            this.lblZeiKbn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtBumonCd
            // 
            this.txtBumonCd.AutoSizeFromLength = true;
            this.txtBumonCd.DisplayLength = null;
            this.txtBumonCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtBumonCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtBumonCd.Location = new System.Drawing.Point(138, 118);
            this.txtBumonCd.MaxLength = 7;
            this.txtBumonCd.Name = "txtBumonCd";
            this.txtBumonCd.Size = new System.Drawing.Size(55, 20);
            this.txtBumonCd.TabIndex = 12;
            this.txtBumonCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBumonCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtBumonCd_Validating);
            // 
            // lblBumonCd
            // 
            this.lblBumonCd.BackColor = System.Drawing.Color.Silver;
            this.lblBumonCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBumonCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblBumonCd.Location = new System.Drawing.Point(3, 116);
            this.lblBumonCd.Name = "lblBumonCd";
            this.lblBumonCd.Size = new System.Drawing.Size(411, 25);
            this.lblBumonCd.TabIndex = 11;
            this.lblBumonCd.Text = "部門コード";
            this.lblBumonCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtHojoKamokCd
            // 
            this.txtHojoKamokCd.AutoSizeFromLength = true;
            this.txtHojoKamokCd.DisplayLength = null;
            this.txtHojoKamokCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtHojoKamokCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtHojoKamokCd.Location = new System.Drawing.Point(138, 90);
            this.txtHojoKamokCd.MaxLength = 7;
            this.txtHojoKamokCd.Name = "txtHojoKamokCd";
            this.txtHojoKamokCd.Size = new System.Drawing.Size(55, 20);
            this.txtHojoKamokCd.TabIndex = 9;
            this.txtHojoKamokCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtHojoKamokCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtHojoKamokCd_Validating);
            // 
            // lblHojoKamokCd
            // 
            this.lblHojoKamokCd.BackColor = System.Drawing.Color.Silver;
            this.lblHojoKamokCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblHojoKamokCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblHojoKamokCd.Location = new System.Drawing.Point(3, 88);
            this.lblHojoKamokCd.Name = "lblHojoKamokCd";
            this.lblHojoKamokCd.Size = new System.Drawing.Size(411, 25);
            this.lblHojoKamokCd.TabIndex = 8;
            this.lblHojoKamokCd.Text = "補助科目コード";
            this.lblHojoKamokCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKanjoKamokCd
            // 
            this.txtKanjoKamokCd.AutoSizeFromLength = false;
            this.txtKanjoKamokCd.DisplayLength = null;
            this.txtKanjoKamokCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtKanjoKamokCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtKanjoKamokCd.Location = new System.Drawing.Point(138, 62);
            this.txtKanjoKamokCd.MaxLength = 7;
            this.txtKanjoKamokCd.Name = "txtKanjoKamokCd";
            this.txtKanjoKamokCd.Size = new System.Drawing.Size(55, 20);
            this.txtKanjoKamokCd.TabIndex = 6;
            this.txtKanjoKamokCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKanjoKamokCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtKanjoKamokCd_Validating);
            // 
            // lblKanjoKamokCd
            // 
            this.lblKanjoKamokCd.BackColor = System.Drawing.Color.Silver;
            this.lblKanjoKamokCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKanjoKamokCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblKanjoKamokCd.Location = new System.Drawing.Point(3, 60);
            this.lblKanjoKamokCd.Name = "lblKanjoKamokCd";
            this.lblKanjoKamokCd.Size = new System.Drawing.Size(411, 25);
            this.lblKanjoKamokCd.TabIndex = 5;
            this.lblKanjoKamokCd.Text = "勘定科目コード";
            this.lblKanjoKamokCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKeisanFugo
            // 
            this.txtKeisanFugo.AutoSizeFromLength = false;
            this.txtKeisanFugo.DisplayLength = null;
            this.txtKeisanFugo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtKeisanFugo.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtKeisanFugo.Location = new System.Drawing.Point(138, 34);
            this.txtKeisanFugo.MaxLength = 1;
            this.txtKeisanFugo.Name = "txtKeisanFugo";
            this.txtKeisanFugo.Size = new System.Drawing.Size(55, 20);
            this.txtKeisanFugo.TabIndex = 3;
            this.txtKeisanFugo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKeisanFugo.Validating += new System.ComponentModel.CancelEventHandler(this.txtKeisanhugo_Validating);
            // 
            // lblKeisanhugo
            // 
            this.lblKeisanhugo.BackColor = System.Drawing.Color.Silver;
            this.lblKeisanhugo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKeisanhugo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblKeisanhugo.Location = new System.Drawing.Point(3, 32);
            this.lblKeisanhugo.Name = "lblKeisanhugo";
            this.lblKeisanhugo.Size = new System.Drawing.Size(411, 25);
            this.lblKeisanhugo.TabIndex = 2;
            this.lblKeisanhugo.Text = "計　算　符　号";
            this.lblKeisanhugo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKojoTaishoShikriKomok
            // 
            this.txtKojoTaishoShikriKomok.AutoSizeFromLength = true;
            this.txtKojoTaishoShikriKomok.DisplayLength = null;
            this.txtKojoTaishoShikriKomok.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtKojoTaishoShikriKomok.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtKojoTaishoShikriKomok.Location = new System.Drawing.Point(138, 6);
            this.txtKojoTaishoShikriKomok.MaxLength = 15;
            this.txtKojoTaishoShikriKomok.Name = "txtKojoTaishoShikriKomok";
            this.txtKojoTaishoShikriKomok.Size = new System.Drawing.Size(111, 20);
            this.txtKojoTaishoShikriKomok.TabIndex = 1;
            this.txtKojoTaishoShikriKomok.Validating += new System.ComponentModel.CancelEventHandler(this.txtKojoTaishoShikriKomok_Validating);
            // 
            // lblKojoTaishoShikriKomok
            // 
            this.lblKojoTaishoShikriKomok.BackColor = System.Drawing.Color.Silver;
            this.lblKojoTaishoShikriKomok.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKojoTaishoShikriKomok.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblKojoTaishoShikriKomok.Location = new System.Drawing.Point(3, 4);
            this.lblKojoTaishoShikriKomok.Name = "lblKojoTaishoShikriKomok";
            this.lblKojoTaishoShikriKomok.Size = new System.Drawing.Size(249, 25);
            this.lblKojoTaishoShikriKomok.TabIndex = 0;
            this.lblKojoTaishoShikriKomok.Text = "控除対象仕切項目";
            this.lblKojoTaishoShikriKomok.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKojoKomokNm
            // 
            this.lblKojoKomokNm.BackColor = System.Drawing.Color.Silver;
            this.lblKojoKomokNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKojoKomokNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblKojoKomokNm.Location = new System.Drawing.Point(12, 69);
            this.lblKojoKomokNm.Name = "lblKojoKomokNm";
            this.lblKojoKomokNm.Size = new System.Drawing.Size(409, 25);
            this.lblKojoKomokNm.TabIndex = 4;
            this.lblKojoKomokNm.Text = "控除項目名称";
            this.lblKojoKomokNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKojoKomokSettei
            // 
            this.txtKojoKomokSettei.AutoSizeFromLength = false;
            this.txtKojoKomokSettei.DisplayLength = null;
            this.txtKojoKomokSettei.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtKojoKomokSettei.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtKojoKomokSettei.Location = new System.Drawing.Point(148, 43);
            this.txtKojoKomokSettei.MaxLength = 15;
            this.txtKojoKomokSettei.Name = "txtKojoKomokSettei";
            this.txtKojoKomokSettei.Size = new System.Drawing.Size(270, 20);
            this.txtKojoKomokSettei.TabIndex = 3;
            this.txtKojoKomokSettei.Validating += new System.ComponentModel.CancelEventHandler(this.txtKojoKomokSettei_Validating);
            // 
            // lblKojoKomokSettei
            // 
            this.lblKojoKomokSettei.BackColor = System.Drawing.Color.Silver;
            this.lblKojoKomokSettei.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKojoKomokSettei.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblKojoKomokSettei.Location = new System.Drawing.Point(12, 41);
            this.lblKojoKomokSettei.Name = "lblKojoKomokSettei";
            this.lblKojoKomokSettei.Size = new System.Drawing.Size(409, 25);
            this.lblKojoKomokSettei.TabIndex = 2;
            this.lblKojoKomokSettei.Text = "控除項目設定";
            this.lblKojoKomokSettei.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKojoKomokNm
            // 
            this.txtKojoKomokNm.AutoSizeFromLength = false;
            this.txtKojoKomokNm.DisplayLength = null;
            this.txtKojoKomokNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtKojoKomokNm.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtKojoKomokNm.Location = new System.Drawing.Point(148, 71);
            this.txtKojoKomokNm.MaxLength = 15;
            this.txtKojoKomokNm.Name = "txtKojoKomokNm";
            this.txtKojoKomokNm.Size = new System.Drawing.Size(270, 20);
            this.txtKojoKomokNm.TabIndex = 5;
            this.txtKojoKomokNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtKojoKomokNm_Validating);
            // 
            // HANC9122
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(437, 402);
            this.Controls.Add(this.txtKojoKomokNm);
            this.Controls.Add(this.pnlMain);
            this.Controls.Add(this.txtSetteiCd);
            this.Controls.Add(this.lblSetteiCd);
            this.Controls.Add(this.txtKojoKomokSettei);
            this.Controls.Add(this.lblKojoKomokNm);
            this.Controls.Add(this.lblKojoKomokSettei);
            this.Name = "HANC9122";
            this.ShowFButton = true;
            this.Text = "控除項目の設定";
            this.Controls.SetChildIndex(this.lblKojoKomokSettei, 0);
            this.Controls.SetChildIndex(this.lblKojoKomokNm, 0);
            this.Controls.SetChildIndex(this.txtKojoKomokSettei, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblSetteiCd, 0);
            this.Controls.SetChildIndex(this.txtSetteiCd, 0);
            this.Controls.SetChildIndex(this.pnlMain, 0);
            this.Controls.SetChildIndex(this.txtKojoKomokNm, 0);
            this.pnlDebug.ResumeLayout(false);
            this.pnlMain.ResumeLayout(false);
            this.pnlMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private jp.co.fsi.common.controls.FsiTextBox txtSetteiCd;
        private System.Windows.Forms.Label lblSetteiCd;
        private jp.co.fsi.common.FsiPanel pnlMain;
        private jp.co.fsi.common.controls.FsiTextBox txtKojoKomokSettei;
        private System.Windows.Forms.Label lblKojoKomokSettei;
        private System.Windows.Forms.Label lblKojoKomokNm;
        private System.Windows.Forms.Label lblKojoTaishoShikriKomok;
        private jp.co.fsi.common.controls.FsiTextBox txtKojoTaishoShikriKomok;
        private jp.co.fsi.common.controls.FsiTextBox txtKeisanFugo;
        private System.Windows.Forms.Label lblKeisanhugo;
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoKamokCd;
        private System.Windows.Forms.Label lblKanjoKamokCd;
        private System.Windows.Forms.Label lblHojoKamokCd;
        private jp.co.fsi.common.controls.FsiTextBox txtHojoKamokCd;
        private jp.co.fsi.common.controls.FsiTextBox txtBumonCd;
        private System.Windows.Forms.Label lblBumonCd;
        private System.Windows.Forms.Label lblZeiKbnn;
        private jp.co.fsi.common.controls.FsiTextBox txtZeiKbn;
        private System.Windows.Forms.Label lblZeiKbn;
        private System.Windows.Forms.Label lblJigyoKbn;
        private jp.co.fsi.common.controls.FsiTextBox txtJigyoKbn;
        private System.Windows.Forms.Label lblJigyoKubn;
        private jp.co.fsi.common.controls.FsiTextBox txtTaishakKbn;
        private System.Windows.Forms.Label lblTaishakKbnn;
        private System.Windows.Forms.Label lblTaishakKbn;
        private System.Windows.Forms.Label lblBumonnCd;
        private System.Windows.Forms.Label lblHojoKamokuCd;
        private System.Windows.Forms.Label lblKanjoKamokuCd;
        private System.Windows.Forms.Label lblKamokbn;
        private common.controls.FsiTextBox txtKojoKomokNm;
    }
}