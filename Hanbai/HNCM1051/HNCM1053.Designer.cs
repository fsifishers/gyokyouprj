﻿namespace jp.co.fsi.hn.hncm1051
{
    partial class HNCM1053
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbxShiresakiCd = new System.Windows.Forms.GroupBox();
            this.lblGyoshuCdTo = new System.Windows.Forms.Label();
            this.lblCodeBet = new System.Windows.Forms.Label();
            this.txtGyoshuCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblGyoshuCdFr = new System.Windows.Forms.Label();
            this.txtGyoshuCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.pnlDebug.SuspendLayout();
            this.gbxShiresakiCd.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Size = new System.Drawing.Size(573, 23);
            this.lblTitle.TabIndex = 999;
            this.lblTitle.Text = "";
            // 
            // btnF2
            // 
            this.btnF2.Visible = false;
            // 
            // btnF3
            // 
            this.btnF3.Visible = false;
            // 
            // btnF7
            // 
            this.btnF7.Visible = false;
            // 
            // btnF6
            // 
            this.btnF6.Visible = false;
            // 
            // btnF8
            // 
            this.btnF8.Visible = false;
            // 
            // btnF9
            // 
            this.btnF9.Visible = false;
            // 
            // btnF10
            // 
            this.btnF10.Visible = false;
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(5, 47);
            this.pnlDebug.Size = new System.Drawing.Size(606, 100);
            // 
            // gbxShiresakiCd
            // 
            this.gbxShiresakiCd.Controls.Add(this.lblGyoshuCdTo);
            this.gbxShiresakiCd.Controls.Add(this.lblCodeBet);
            this.gbxShiresakiCd.Controls.Add(this.txtGyoshuCdFr);
            this.gbxShiresakiCd.Controls.Add(this.lblGyoshuCdFr);
            this.gbxShiresakiCd.Controls.Add(this.txtGyoshuCdTo);
            this.gbxShiresakiCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.gbxShiresakiCd.ForeColor = System.Drawing.Color.Black;
            this.gbxShiresakiCd.Location = new System.Drawing.Point(8, 13);
            this.gbxShiresakiCd.Name = "gbxShiresakiCd";
            this.gbxShiresakiCd.Size = new System.Drawing.Size(579, 75);
            this.gbxShiresakiCd.TabIndex = 0;
            this.gbxShiresakiCd.TabStop = false;
            this.gbxShiresakiCd.Text = "魚種CD範囲";
            // 
            // lblGyoshuCdTo
            // 
            this.lblGyoshuCdTo.BackColor = System.Drawing.Color.Silver;
            this.lblGyoshuCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGyoshuCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblGyoshuCdTo.Location = new System.Drawing.Point(349, 32);
            this.lblGyoshuCdTo.Name = "lblGyoshuCdTo";
            this.lblGyoshuCdTo.Size = new System.Drawing.Size(217, 20);
            this.lblGyoshuCdTo.TabIndex = 4;
            this.lblGyoshuCdTo.Text = "最　後";
            this.lblGyoshuCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCodeBet
            // 
            this.lblCodeBet.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblCodeBet.Location = new System.Drawing.Point(280, 31);
            this.lblCodeBet.Name = "lblCodeBet";
            this.lblCodeBet.Size = new System.Drawing.Size(18, 20);
            this.lblCodeBet.TabIndex = 2;
            this.lblCodeBet.Text = "～";
            this.lblCodeBet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtGyoshuCdFr
            // 
            this.txtGyoshuCdFr.AutoSizeFromLength = false;
            this.txtGyoshuCdFr.DisplayLength = null;
            this.txtGyoshuCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtGyoshuCdFr.Location = new System.Drawing.Point(13, 32);
            this.txtGyoshuCdFr.MaxLength = 4;
            this.txtGyoshuCdFr.Name = "txtGyoshuCdFr";
            this.txtGyoshuCdFr.Size = new System.Drawing.Size(40, 20);
            this.txtGyoshuCdFr.TabIndex = 0;
            this.txtGyoshuCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGyoshuCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtGyoshuCdFr_Validating);
            // 
            // lblGyoshuCdFr
            // 
            this.lblGyoshuCdFr.BackColor = System.Drawing.Color.Silver;
            this.lblGyoshuCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGyoshuCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblGyoshuCdFr.Location = new System.Drawing.Point(56, 32);
            this.lblGyoshuCdFr.Name = "lblGyoshuCdFr";
            this.lblGyoshuCdFr.Size = new System.Drawing.Size(217, 20);
            this.lblGyoshuCdFr.TabIndex = 1;
            this.lblGyoshuCdFr.Text = "先　頭";
            this.lblGyoshuCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtGyoshuCdTo
            // 
            this.txtGyoshuCdTo.AutoSizeFromLength = false;
            this.txtGyoshuCdTo.DisplayLength = null;
            this.txtGyoshuCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtGyoshuCdTo.Location = new System.Drawing.Point(306, 32);
            this.txtGyoshuCdTo.MaxLength = 4;
            this.txtGyoshuCdTo.Name = "txtGyoshuCdTo";
            this.txtGyoshuCdTo.Size = new System.Drawing.Size(40, 20);
            this.txtGyoshuCdTo.TabIndex = 3;
            this.txtGyoshuCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGyoshuCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtGyoshuCdTo_Validating);
            // 
            // HNCM1053
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(598, 150);
            this.Controls.Add(this.gbxShiresakiCd);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "HNCM1053";
            this.ShowFButton = true;
            this.Text = "";
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.gbxShiresakiCd, 0);
            this.pnlDebug.ResumeLayout(false);
            this.gbxShiresakiCd.ResumeLayout(false);
            this.gbxShiresakiCd.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbxShiresakiCd;
        private System.Windows.Forms.Label lblGyoshuCdTo;
        private System.Windows.Forms.Label lblCodeBet;
        private common.controls.FsiTextBox txtGyoshuCdFr;
        private System.Windows.Forms.Label lblGyoshuCdFr;
        private common.controls.FsiTextBox txtGyoshuCdTo;



    }
}