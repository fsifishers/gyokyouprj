﻿namespace jp.co.fsi.hn.hncm1051
{
    partial class HNCM1054
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblJimotoShiwakeNm = new System.Windows.Forms.Label();
            this.txtJimotoShiwakeCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblJimotoShiwakeCd = new System.Windows.Forms.Label();
            this.lblShiwakeKbnNm = new System.Windows.Forms.Label();
            this.txtShiwakeKbn = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShiwakeKbn = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Size = new System.Drawing.Size(426, 23);
            this.lblTitle.TabIndex = 999;
            this.lblTitle.Text = "";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(5, 116);
            this.pnlDebug.Size = new System.Drawing.Size(459, 100);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblJimotoShiwakeNm);
            this.groupBox1.Controls.Add(this.txtJimotoShiwakeCd);
            this.groupBox1.Controls.Add(this.lblJimotoShiwakeCd);
            this.groupBox1.Controls.Add(this.lblShiwakeKbnNm);
            this.groupBox1.Controls.Add(this.txtShiwakeKbn);
            this.groupBox1.Controls.Add(this.lblShiwakeKbn);
            this.groupBox1.Font = new System.Drawing.Font("MS UI Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBox1.Location = new System.Drawing.Point(12, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(427, 138);
            this.groupBox1.TabIndex = 1000;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "初期値設定";
            // 
            // lblJimotoShiwakeNm
            // 
            this.lblJimotoShiwakeNm.BackColor = System.Drawing.Color.Silver;
            this.lblJimotoShiwakeNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblJimotoShiwakeNm.Location = new System.Drawing.Point(189, 59);
            this.lblJimotoShiwakeNm.Name = "lblJimotoShiwakeNm";
            this.lblJimotoShiwakeNm.Size = new System.Drawing.Size(211, 25);
            this.lblJimotoShiwakeNm.TabIndex = 17;
            this.lblJimotoShiwakeNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtJimotoShiwakeCd
            // 
            this.txtJimotoShiwakeCd.AutoSizeFromLength = false;
            this.txtJimotoShiwakeCd.DisplayLength = null;
            this.txtJimotoShiwakeCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtJimotoShiwakeCd.Location = new System.Drawing.Point(130, 61);
            this.txtJimotoShiwakeCd.MaxLength = 6;
            this.txtJimotoShiwakeCd.Name = "txtJimotoShiwakeCd";
            this.txtJimotoShiwakeCd.Size = new System.Drawing.Size(53, 20);
            this.txtJimotoShiwakeCd.TabIndex = 16;
            this.txtJimotoShiwakeCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtJimotoShiwakeCd.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtJimotoShiwakeCd_KeyDown);
            this.txtJimotoShiwakeCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtJimotoShiwakeCd_Validating);
            // 
            // lblJimotoShiwakeCd
            // 
            this.lblJimotoShiwakeCd.BackColor = System.Drawing.Color.Silver;
            this.lblJimotoShiwakeCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblJimotoShiwakeCd.Location = new System.Drawing.Point(16, 59);
            this.lblJimotoShiwakeCd.Name = "lblJimotoShiwakeCd";
            this.lblJimotoShiwakeCd.Size = new System.Drawing.Size(170, 25);
            this.lblJimotoShiwakeCd.TabIndex = 15;
            this.lblJimotoShiwakeCd.Text = "地元仕訳コード";
            this.lblJimotoShiwakeCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShiwakeKbnNm
            // 
            this.lblShiwakeKbnNm.BackColor = System.Drawing.Color.Silver;
            this.lblShiwakeKbnNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblShiwakeKbnNm.Location = new System.Drawing.Point(189, 25);
            this.lblShiwakeKbnNm.Name = "lblShiwakeKbnNm";
            this.lblShiwakeKbnNm.Size = new System.Drawing.Size(211, 25);
            this.lblShiwakeKbnNm.TabIndex = 14;
            this.lblShiwakeKbnNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShiwakeKbn
            // 
            this.txtShiwakeKbn.AutoSizeFromLength = false;
            this.txtShiwakeKbn.DisplayLength = null;
            this.txtShiwakeKbn.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtShiwakeKbn.Location = new System.Drawing.Point(130, 27);
            this.txtShiwakeKbn.MaxLength = 6;
            this.txtShiwakeKbn.Name = "txtShiwakeKbn";
            this.txtShiwakeKbn.Size = new System.Drawing.Size(53, 20);
            this.txtShiwakeKbn.TabIndex = 13;
            this.txtShiwakeKbn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShiwakeKbn.Validating += new System.ComponentModel.CancelEventHandler(this.txtShiwakeKbn_Validating);
            // 
            // lblShiwakeKbn
            // 
            this.lblShiwakeKbn.BackColor = System.Drawing.Color.Silver;
            this.lblShiwakeKbn.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblShiwakeKbn.Location = new System.Drawing.Point(16, 25);
            this.lblShiwakeKbn.Name = "lblShiwakeKbn";
            this.lblShiwakeKbn.Size = new System.Drawing.Size(170, 25);
            this.lblShiwakeKbn.TabIndex = 12;
            this.lblShiwakeKbn.Text = "仕　訳　区　分";
            this.lblShiwakeKbn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // HNCM1054
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(451, 219);
            this.Controls.Add(this.groupBox1);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "HNCM1054";
            this.ShowFButton = true;
            this.Text = "魚種マスタ初期設定";
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.pnlDebug.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblJimotoShiwakeNm;
        private common.controls.FsiTextBox txtJimotoShiwakeCd;
        private System.Windows.Forms.Label lblJimotoShiwakeCd;
        private System.Windows.Forms.Label lblShiwakeKbnNm;
        private common.controls.FsiTextBox txtShiwakeKbn;
        private System.Windows.Forms.Label lblShiwakeKbn;
    }
}