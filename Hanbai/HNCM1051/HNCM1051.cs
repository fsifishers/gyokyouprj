﻿using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.Reflection;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.hn.hncm1051
{
    /// <summary>
    /// 魚種マスタメンテ(hncm1051)
    /// </summary>
    public partial class HNCM1051 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// モード(コード検索)
        /// </summary>
        private const string MODE_CD_SRC = "1";

        /// <summary>
        /// 検索画面用画面タイトル
        /// </summary>
        private const string SEARCH_TITLE = "魚種の検索";
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HNCM1051()
        {
            InitializeComponent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            //支所コード設定　他画面からの呼び出し前に設定しておく、名称は他画面からの起動処理後に設定
#if DEBUG
            this.txtMizuageShishoCd.Text = "1";
#else
            this..txtMizuageShishoCd.Text = this.UInfo.ShishoCd;
#endif

            if (MODE_CD_SRC.Equals(this.Par1))
            {
                // Par1が"1"の場合、コード検索画面としての挙動をする
                // フォームのキャプションにラベルタイトルのtextを設定する
                this.Text = SEARCH_TITLE;
                // タイトルは非表示
                this.lblTitle.Visible = false;
                // サイズを縮める
                this.Size = new Size(600, 500);
                // フォームの配置を上へ移動する
                this.txtMizuageShishoCd.Location = new System.Drawing.Point(50, 15);
                this.lblMizuageShisho.Location = new System.Drawing.Point(14, 13);
                this.lblMizuageShishoNm.Location = new System.Drawing.Point(86, 15);
                this.lblKanaNm.Location = new System.Drawing.Point(13, 47);
                this.txtKanaNm.Location = new System.Drawing.Point(90, 49);
                this.dgvList.Location = new System.Drawing.Point(13, 79);
                this.dgvList.Size = new Size(540, 250);
                // EscapeとF1のみ表示
                this.btnEsc.Location = this.btnF1.Location;
                this.btnF1.Location = this.btnF2.Location;
                this.btnF2.Visible = false;
                this.btnF3.Visible = false;
                this.btnF4.Visible = false;
                this.btnF5.Visible = false;
                this.btnF6.Visible = false;
                this.btnF7.Visible = false;
                this.btnF8.Visible = false;
                this.btnF9.Visible = false;
                this.btnF10.Visible = false;
                this.btnF11.Visible = false;
                this.btnF12.Visible = false;
                this.ShowFButton = true;

                //検索表示の場合は支所コートを使用不可にする。
                this.txtMizuageShishoCd.Text = this.Par2;
                this.txtMizuageShishoCd.Enabled = false;
            }
            else
            {
                // メニューから起動の場合はファンクションボタンを表示しない
                this.ShowFButton = false;
            }
            //本所以外は変更不可に
            this.txtMizuageShishoCd.Enabled = (this.txtMizuageShishoCd.Text == "1") ? true : false;
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);

            // まずデータが存在し得ない検索条件で検索をし、結果をバインドすることで、
            // 初期状態を作り出す
            SearchData(true);

            // カナ名にフォーカス
            this.txtKanaNm.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtKanaNm.Focus();
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                // Par1が"1"の場合、ダイアログとしての処理結果を返却する
                this.DialogResult = DialogResult.Cancel;
            }
            base.PressEsc();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // フォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd":
                case "dgvList":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {

            string stCurrentDir = System.IO.Directory.GetCurrentDirectory();
            // フォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd":
                    System.Reflection.Assembly asm;
                    Type t;
                    string[] result;
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM2031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2031.CMCM2031");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.InData = this.txtMizuageShishoCd.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.txtMizuageShishoCd.Text = result[0];
                                this.lblMizuageShishoNm.Text = result[1];
                            }
                        }
                    }

                    break;

                default:
                    // カナ名にフォーカスを戻す
                    this.txtKanaNm.Focus();
                    this.txtKanaNm.SelectAll();
                    break;
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // メンテ機能で立ち上げている場合のみ担当者登録画面を立ち上げる
            if (ValChk.IsEmpty(this.Par1))
            {
                // 魚種マスタ登録画面の起動
                EditGyoshu(string.Empty);
            }
        }
        
        /**//// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            // メンテ機能で立ち上げている場合のみ魚種マスタ一覧を立ち上げる
            if (ValChk.IsEmpty(this.Par1))
            {
                // 魚種マスタ一覧の起動
                ItiranGyoshu();
            }
        }

        public override void PressF9()
        {
            ShowConfig();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 水揚支所入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMizuageShishoCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                e.Cancel = true;
                this.txtMizuageShishoCd.SelectAll();
                this.txtMizuageShishoCd.Focus();
            }
        }

        /// <summary>
        /// カナ名検証時の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKanaNm_Validating(object sender, CancelEventArgs e)
        {
            //TODO:何かチェックが必要なのかもしれない
            if (txtKanaNm.Text != "")
            {
                // 入力された情報を元に検索する
                SearchData(false);
            }
        }
        /// <summary>
        /// カナ名でキー入力時の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKanaNm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                SearchData(false);
            }
        }

        /// <summary>
        /// グリッドでのキーダウン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (MODE_CD_SRC.Equals(this.Par1))
                {
                    ReturnVal();
                }
                else
                {
                    EditGyoshu(Util.ToString(this.dgvList.SelectedRows[0].Cells["魚種CD"].Value));
                    e.Handled = true;
                }
            }
        }

        /// <summary>
        /// グリッドのセルダブルクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                ReturnVal();
            }
            else
            {
                EditGyoshu(Util.ToString(this.dgvList.SelectedRows[0].Cells["魚種CD"].Value));
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 水揚支所の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool IsValidMizuageShishoCd()
        {
            // 空 又は 0入力の場合
            if (ValChk.IsEmpty(this.txtMizuageShishoCd.Text) || Equals(this.txtMizuageShishoCd.Text, "0"))
            {
                // 水揚支所名称を表示する
                Msg.Notice("入力に誤りがあります。");
                return false;
            }
            // 水揚支所名称を表示する
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
            if (ValChk.IsEmpty(this.lblMizuageShishoNm.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// データを検索する
        /// </summary>
        /// <param name="isInitial">初期処理であるかどうか</param>
        private void SearchData(bool isInitial)
        {
            // 商品マスタからデータを取得して表示
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.txtMizuageShishoCd.Text);
            StringBuilder where = new StringBuilder("KAISHA_CD = @KAISHA_CD");
            where.Append(" AND SHISHO_CD = @SHISHO_CD");
            if (isInitial)
            {
                // 初期処理の場合、検索結果がヒットしないようにあり得ない検索条件を設定する
                //where.Append(" AND 1 = 0");
            }
            else
            {
                // 初期処理でない場合、入力されたカナ名から検索する
                if (!ValChk.IsEmpty(this.txtKanaNm.Text))
                {
                    where.Append(" AND GYOSHU_KANA_NM LIKE @GYOSHU_KANA_NM");
                    // NOTICE!:部分一致検索をする場合、"%"の分桁数に2バイトを足すこと
                    dpc.SetParam("@GYOSHU_KANA_NM", SqlDbType.VarChar, 42, "%" + this.txtKanaNm.Text + "%");
                }
            }
            string cols = "GYOSHU_CD AS 魚種CD";
            cols += ", GYOSHU_NM AS 魚種名";
            cols += ", GYOSHU_KANA_NM AS 魚種カナ名";
            string from = "VI_HN_GYOSHU";

            DataTable dtGyosyu =
                this.Dba.GetDataTableByConditionWithParams(cols, from,
                    Util.ToString(where), "GYOSHU_CD", dpc);

            // 初期処理以外の場合、該当データがなければエラーメッセージを表示
            if (dtGyosyu.Rows.Count == 0)
            {
                if (!isInitial)
                {
                    Msg.Info("該当データがありません。");
                }

                dtGyosyu.Rows.Add(dtGyosyu.NewRow());
            }

            this.dgvList.DataSource = dtGyosyu;

            // ユーザーによるソートを禁止させる
            foreach (DataGridViewColumn c in this.dgvList.Columns)
                c.SortMode = DataGridViewColumnSortMode.NotSortable;

            // フォントを設定する
            this.dgvList.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F, FontStyle.Regular);
            this.dgvList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.dgvList.DefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F);

            // 列幅を設定する
            this.dgvList.Columns[0].Width = 110;
            this.dgvList.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvList.Columns[1].Width = 200;
            this.dgvList.Columns[2].Width = 200;
        }

        /// <summary>
        /// 魚種を追加編集する
        /// </summary>
        /// <param name="code">魚種コード(空：新規登録、以外：編集)</param>
        private void EditGyoshu(string code)
        {
            HNCM1052 frmHNCM1052;

            if (ValChk.IsEmpty(code))
            {
                // 新規登録モードで登録画面を起動
                frmHNCM1052 = new HNCM1052("1");
            }
            else
            {
                // 編集モードで登録画面を起動
                frmHNCM1052 = new HNCM1052("2");
                frmHNCM1052.InData = code;
            }
            frmHNCM1052.Par2 = this.txtMizuageShishoCd.Text;

            DialogResult result = frmHNCM1052.ShowDialog(this);

            if (result == DialogResult.OK)
            {
                // データを再検索する
                SearchData(false);
                // 元々選択していたデータを選択
                for (int i = 0; i < this.dgvList.Rows.Count; i++)
                {
                    if (code.Equals(Util.ToString(this.dgvList.Rows[i].Cells["魚種CD"].Value)))
                    {
                        this.dgvList.Rows[i].Selected = true;
                        this.dgvList.CurrentCell = this.dgvList[0, i];
                        break;
                    }
                }

                // Gridに再度フォーカスをセット
                this.ActiveControl = this.dgvList;
                this.dgvList.Focus();
            }
        }

        /// <summary>
        /// 魚種マスタ一覧画面を起動する
        /// </summary>
        /// <param name="code"></param>
        private void ItiranGyoshu()
        {
            HNCM1053 frmHNCM1053;
            // 魚種マスタ一覧画面を起動
            frmHNCM1053 = new HNCM1053();

            DialogResult result = frmHNCM1053.ShowDialog(this);

            if (result == DialogResult.OK)
            {
                // Gridに再度フォーカスをセット
                this.ActiveControl = this.dgvList;
                this.dgvList.Focus();
            }
        }

        /// <summary>
        /// 魚種マスタ初期値設定画面を起動する
        /// </summary>
        /// <param name="code"></param>
        private void ShowConfig()
        {
            HNCM1054 frmHNCM1054;
            // 魚種マスタ一覧画面を起動
            frmHNCM1054 = new HNCM1054();
            frmHNCM1054.Par1 = this.txtMizuageShishoCd.Text;

            DialogResult result = frmHNCM1054.ShowDialog(this);

            if (result == DialogResult.OK)
            {
                // Gridに再度フォーカスをセット
                this.ActiveControl = this.dgvList;
                this.dgvList.Focus();
            }
        }

        /// <summary>
        /// 呼び出し元に戻り値を返す
        /// </summary>
        private void ReturnVal()
        {
            this.OutData = new string[3] { 
                Util.ToString(this.dgvList.SelectedRows[0].Cells["魚種CD"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["魚種名"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["魚種カナ名"].Value)
            };
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        #endregion

    }
}
