﻿namespace jp.co.fsi.hn.hncm1051
{
    partial class HNCM1051
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblKanaNm = new System.Windows.Forms.Label();
            this.txtKanaNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.dgvList = new System.Windows.Forms.DataGridView();
            this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMizuageShishoNm = new System.Windows.Forms.Label();
            this.lblMizuageShisho = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvList)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.TabIndex = 999;
            this.lblTitle.Text = "魚種マスタの登録";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Size = new System.Drawing.Size(847, 100);
            // 
            // lblKanaNm
            // 
            this.lblKanaNm.BackColor = System.Drawing.Color.Silver;
            this.lblKanaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F);
            this.lblKanaNm.Location = new System.Drawing.Point(12, 79);
            this.lblKanaNm.Name = "lblKanaNm";
            this.lblKanaNm.Size = new System.Drawing.Size(295, 25);
            this.lblKanaNm.TabIndex = 1;
            this.lblKanaNm.Text = "カ  ナ  名";
            this.lblKanaNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKanaNm
            // 
            this.txtKanaNm.AutoSizeFromLength = false;
            this.txtKanaNm.DisplayLength = null;
            this.txtKanaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F);
            this.txtKanaNm.Location = new System.Drawing.Point(89, 83);
            this.txtKanaNm.Name = "txtKanaNm";
            this.txtKanaNm.Size = new System.Drawing.Size(215, 19);
            this.txtKanaNm.TabIndex = 4;
            this.txtKanaNm.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtKanaNm_KeyDown);
            this.txtKanaNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtKanaNm_Validating);
            // 
            // dgvList
            // 
            this.dgvList.AllowUserToAddRows = false;
            this.dgvList.AllowUserToDeleteRows = false;
            this.dgvList.AllowUserToResizeColumns = false;
            this.dgvList.AllowUserToResizeRows = false;
            this.dgvList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvList.Location = new System.Drawing.Point(12, 112);
            this.dgvList.MultiSelect = false;
            this.dgvList.Name = "dgvList";
            this.dgvList.ReadOnly = true;
            this.dgvList.RowHeadersVisible = false;
            this.dgvList.RowTemplate.Height = 21;
            this.dgvList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvList.Size = new System.Drawing.Size(540, 296);
            this.dgvList.TabIndex = 5;
            this.dgvList.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvList_CellDoubleClick);
            this.dgvList.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvList_KeyDown);
            // 
            // txtMizuageShishoCd
            // 
            this.txtMizuageShishoCd.AutoSizeFromLength = true;
            this.txtMizuageShishoCd.DisplayLength = null;
            this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtMizuageShishoCd.Location = new System.Drawing.Point(50, 49);
            this.txtMizuageShishoCd.MaxLength = 4;
            this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
            this.txtMizuageShishoCd.Size = new System.Drawing.Size(34, 20);
            this.txtMizuageShishoCd.TabIndex = 0;
            this.txtMizuageShishoCd.TabStop = false;
            this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
            // 
            // lblMizuageShishoNm
            // 
            this.lblMizuageShishoNm.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMizuageShishoNm.Location = new System.Drawing.Point(86, 50);
            this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
            this.lblMizuageShishoNm.Size = new System.Drawing.Size(212, 20);
            this.lblMizuageShishoNm.TabIndex = 3;
            this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMizuageShisho
            // 
            this.lblMizuageShisho.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShisho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShisho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblMizuageShisho.Location = new System.Drawing.Point(14, 47);
            this.lblMizuageShisho.Name = "lblMizuageShisho";
            this.lblMizuageShisho.Size = new System.Drawing.Size(290, 25);
            this.lblMizuageShisho.TabIndex = 1;
            this.lblMizuageShisho.Text = "支所";
            this.lblMizuageShisho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // HNCM1051
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 638);
            this.Controls.Add(this.txtMizuageShishoCd);
            this.Controls.Add(this.lblMizuageShishoNm);
            this.Controls.Add(this.lblMizuageShisho);
            this.Controls.Add(this.txtKanaNm);
            this.Controls.Add(this.lblKanaNm);
            this.Controls.Add(this.dgvList);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "HNCM1051";
            this.Par1 = "1";
            this.Par2 = "1";
            this.ShowFButton = true;
            this.Text = "魚種マスタの登録";
            this.Controls.SetChildIndex(this.dgvList, 0);
            this.Controls.SetChildIndex(this.lblKanaNm, 0);
            this.Controls.SetChildIndex(this.txtKanaNm, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblMizuageShisho, 0);
            this.Controls.SetChildIndex(this.lblMizuageShishoNm, 0);
            this.Controls.SetChildIndex(this.txtMizuageShishoCd, 0);
            this.pnlDebug.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblKanaNm;
        private jp.co.fsi.common.controls.FsiTextBox txtKanaNm;
        private System.Windows.Forms.DataGridView dgvList;
        private common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private System.Windows.Forms.Label lblMizuageShisho;
    }
}