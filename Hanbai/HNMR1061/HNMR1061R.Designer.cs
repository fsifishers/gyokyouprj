﻿namespace jp.co.fsi.hn.hnmr1061
{
    /// <summary>
    /// HNMR1061R の概要の説明です。
    /// </summary>
    partial class HNMR1061R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(HNMR1061R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.lblTitle01 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTitle02 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblTitle04 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTitle05 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.page = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblTitle03 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtToday = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblGyoshuCd = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblGyoshuNm = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblHonsu = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblNyukaryo = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSoriage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTakane = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblNakane = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblYasune = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.textBox3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ghShishoCd = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtValue02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue04 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue05 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue06 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue07 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.gfShishoCd = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.reportHeader1 = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.reportFooter1 = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.lblTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtTotal06 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotal01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotal02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotal03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotal05 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotal04 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle04)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle05)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.page)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtToday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGyoshuCd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGyoshuNm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHonsu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNyukaryo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSoriage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTakane)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNakane)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblYasune)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue04)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue05)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue06)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue07)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal06)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal05)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal04)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblTitle01,
            this.lblTitle02,
            this.textBox1,
            this.textBox2,
            this.lblTitle04,
            this.lblTitle05,
            this.page,
            this.lblTitle03,
            this.txtToday,
            this.lblGyoshuCd,
            this.lblGyoshuNm,
            this.lblHonsu,
            this.lblNyukaryo,
            this.lblSoriage,
            this.lblTakane,
            this.lblNakane,
            this.lblYasune,
            this.line2,
            this.line1,
            this.textBox3,
            this.textBox4});
            this.pageHeader.Height = 1.291667F;
            this.pageHeader.Name = "pageHeader";
            this.pageHeader.Format += new System.EventHandler(this.pageHeader_Format);
            // 
            // lblTitle01
            // 
            this.lblTitle01.Height = 0.2625984F;
            this.lblTitle01.HyperLink = null;
            this.lblTitle01.Left = 4.046063F;
            this.lblTitle01.Name = "lblTitle01";
            this.lblTitle01.Style = "font-family: ＭＳ 明朝; font-size: 15pt; font-weight: bold";
            this.lblTitle01.Text = "市況情報   (月計)";
            this.lblTitle01.Top = 0.1165354F;
            this.lblTitle01.Width = 1.854331F;
            // 
            // lblTitle02
            // 
            this.lblTitle02.Height = 0.2F;
            this.lblTitle02.HyperLink = null;
            this.lblTitle02.Left = 1.149607F;
            this.lblTitle02.Name = "lblTitle02";
            this.lblTitle02.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: bold";
            this.lblTitle02.Text = "～";
            this.lblTitle02.Top = 0.2811024F;
            this.lblTitle02.Width = 0.1874015F;
            // 
            // textBox1
            // 
            this.textBox1.DataField = "ITEM01";
            this.textBox1.Height = 0.2F;
            this.textBox1.Left = 0.2326772F;
            this.textBox1.MultiLine = false;
            this.textBox1.Name = "textBox1";
            this.textBox1.Style = "font-family: ＭＳ 明朝; font-size: 10pt; font-weight: normal; ddo-char-set: 1";
            this.textBox1.Text = null;
            this.textBox1.Top = 0.2811024F;
            this.textBox1.Width = 0.8440944F;
            // 
            // textBox2
            // 
            this.textBox2.DataField = "ITEM02";
            this.textBox2.Height = 0.2F;
            this.textBox2.Left = 1.396457F;
            this.textBox2.MultiLine = false;
            this.textBox2.Name = "textBox2";
            this.textBox2.Style = "font-family: ＭＳ 明朝; font-size: 10pt; font-weight: normal; text-align: right; ddo-" +
    "char-set: 1";
            this.textBox2.Text = null;
            this.textBox2.Top = 0.2811024F;
            this.textBox2.Width = 0.8437006F;
            // 
            // lblTitle04
            // 
            this.lblTitle04.Height = 0.2F;
            this.lblTitle04.HyperLink = null;
            this.lblTitle04.Left = 8.362206F;
            this.lblTitle04.Name = "lblTitle04";
            this.lblTitle04.Style = "font-family: ＭＳ 明朝; font-size: 10pt; font-weight: normal; ddo-char-set: 1";
            this.lblTitle04.Text = "作成";
            this.lblTitle04.Top = 0F;
            this.lblTitle04.Width = 0.3515748F;
            // 
            // lblTitle05
            // 
            this.lblTitle05.Height = 0.2F;
            this.lblTitle05.HyperLink = null;
            this.lblTitle05.Left = 9.130326F;
            this.lblTitle05.Name = "lblTitle05";
            this.lblTitle05.Style = "font-family: ＭＳ 明朝; font-size: 10pt; font-weight: normal; ddo-char-set: 1";
            this.lblTitle05.Text = "頁";
            this.lblTitle05.Top = 0F;
            this.lblTitle05.Width = 0.1976394F;
            // 
            // page
            // 
            this.page.Height = 0.2F;
            this.page.Left = 8.713785F;
            this.page.Name = "page";
            this.page.Style = "font-family: ＭＳ 明朝; font-size: 10pt; font-weight: normal; text-align: right; ddo-" +
    "char-set: 1";
            this.page.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.page.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.page.Text = null;
            this.page.Top = 0F;
            this.page.Width = 0.4165363F;
            // 
            // lblTitle03
            // 
            this.lblTitle03.Height = 0.2F;
            this.lblTitle03.HyperLink = null;
            this.lblTitle03.Left = 2.240156F;
            this.lblTitle03.Name = "lblTitle03";
            this.lblTitle03.Style = "font-family: ＭＳ 明朝; font-size: 10pt; font-weight: normal; ddo-char-set: 1";
            this.lblTitle03.Text = "分";
            this.lblTitle03.Top = 0.2811024F;
            this.lblTitle03.Width = 0.1874015F;
            // 
            // txtToday
            // 
            this.txtToday.Height = 0.2F;
            this.txtToday.Left = 7.153545F;
            this.txtToday.MultiLine = false;
            this.txtToday.Name = "txtToday";
            this.txtToday.Style = "font-family: ＭＳ 明朝; font-size: 10pt; font-weight: normal; text-align: right; ddo-" +
    "char-set: 1";
            this.txtToday.Text = "yyyy/MM/dd";
            this.txtToday.Top = 0F;
            this.txtToday.Width = 1.208662F;
            // 
            // lblGyoshuCd
            // 
            this.lblGyoshuCd.Height = 0.1968504F;
            this.lblGyoshuCd.HyperLink = null;
            this.lblGyoshuCd.Left = 0.09724413F;
            this.lblGyoshuCd.Name = "lblGyoshuCd";
            this.lblGyoshuCd.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: right; vert" +
    "ical-align: middle; ddo-char-set: 1";
            this.lblGyoshuCd.Text = "魚種CD";
            this.lblGyoshuCd.Top = 1.06378F;
            this.lblGyoshuCd.Width = 0.825984F;
            // 
            // lblGyoshuNm
            // 
            this.lblGyoshuNm.Height = 0.1968504F;
            this.lblGyoshuNm.HyperLink = null;
            this.lblGyoshuNm.Left = 1.014173F;
            this.lblGyoshuNm.Name = "lblGyoshuNm";
            this.lblGyoshuNm.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: left; verti" +
    "cal-align: middle; ddo-char-set: 1";
            this.lblGyoshuNm.Text = "魚種名称";
            this.lblGyoshuNm.Top = 1.06378F;
            this.lblGyoshuNm.Width = 0.9614174F;
            // 
            // lblHonsu
            // 
            this.lblHonsu.Height = 0.1968504F;
            this.lblHonsu.HyperLink = null;
            this.lblHonsu.Left = 2.87874F;
            this.lblHonsu.Name = "lblHonsu";
            this.lblHonsu.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: right; vert" +
    "ical-align: middle; ddo-char-set: 1";
            this.lblHonsu.Text = "本 数";
            this.lblHonsu.Top = 1.06378F;
            this.lblHonsu.Width = 0.9614174F;
            // 
            // lblNyukaryo
            // 
            this.lblNyukaryo.Height = 0.1968504F;
            this.lblNyukaryo.HyperLink = null;
            this.lblNyukaryo.Left = 3.840158F;
            this.lblNyukaryo.Name = "lblNyukaryo";
            this.lblNyukaryo.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: right; vert" +
    "ical-align: middle; ddo-char-set: 1";
            this.lblNyukaryo.Text = "入 荷 量";
            this.lblNyukaryo.Top = 1.06378F;
            this.lblNyukaryo.Width = 1.148818F;
            // 
            // lblSoriage
            // 
            this.lblSoriage.Height = 0.1968504F;
            this.lblSoriage.HyperLink = null;
            this.lblSoriage.Left = 5.269685F;
            this.lblSoriage.Name = "lblSoriage";
            this.lblSoriage.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: right; vert" +
    "ical-align: middle; ddo-char-set: 1";
            this.lblSoriage.Text = "総 売 上";
            this.lblSoriage.Top = 1.06378F;
            this.lblSoriage.Width = 0.9614172F;
            // 
            // lblTakane
            // 
            this.lblTakane.Height = 0.1968504F;
            this.lblTakane.HyperLink = null;
            this.lblTakane.Left = 6.447638F;
            this.lblTakane.Name = "lblTakane";
            this.lblTakane.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: right; vert" +
    "ical-align: middle; ddo-char-set: 1";
            this.lblTakane.Text = "高 値";
            this.lblTakane.Top = 1.06378F;
            this.lblTakane.Width = 0.8051186F;
            // 
            // lblNakane
            // 
            this.lblNakane.Height = 0.1968504F;
            this.lblNakane.HyperLink = null;
            this.lblNakane.Left = 7.458662F;
            this.lblNakane.Name = "lblNakane";
            this.lblNakane.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: right; vert" +
    "ical-align: middle; ddo-char-set: 1";
            this.lblNakane.Text = "中 値";
            this.lblNakane.Top = 1.06378F;
            this.lblNakane.Width = 0.9614172F;
            // 
            // lblYasune
            // 
            this.lblYasune.Height = 0.1968504F;
            this.lblYasune.HyperLink = null;
            this.lblYasune.Left = 8.507874F;
            this.lblYasune.Name = "lblYasune";
            this.lblYasune.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: right; vert" +
    "ical-align: middle; ddo-char-set: 1";
            this.lblYasune.Text = "安 値";
            this.lblYasune.Top = 1.06378F;
            this.lblYasune.Width = 0.9614172F;
            // 
            // line2
            // 
            this.line2.Height = 0F;
            this.line2.Left = 0F;
            this.line2.LineWeight = 2F;
            this.line2.Name = "line2";
            this.line2.Top = 1.26063F;
            this.line2.Width = 9.469292F;
            this.line2.X1 = 0F;
            this.line2.X2 = 9.469292F;
            this.line2.Y1 = 1.26063F;
            this.line2.Y2 = 1.26063F;
            // 
            // line1
            // 
            this.line1.Height = 0F;
            this.line1.Left = 0F;
            this.line1.LineWeight = 2F;
            this.line1.Name = "line1";
            this.line1.Top = 1.06378F;
            this.line1.Width = 9.469292F;
            this.line1.X1 = 0F;
            this.line1.X2 = 9.469292F;
            this.line1.Y1 = 1.06378F;
            this.line1.Y2 = 1.06378F;
            // 
            // textBox3
            // 
            this.textBox3.Height = 0.2F;
            this.textBox3.Left = 0.232677F;
            this.textBox3.MultiLine = false;
            this.textBox3.Name = "textBox3";
            this.textBox3.Style = "font-family: ＭＳ 明朝; font-size: 10pt; font-weight: normal; ddo-char-set: 1";
            this.textBox3.Text = null;
            this.textBox3.Top = 0F;
            this.textBox3.Width = 2.583859F;
            // 
            // textBox4
            // 
            this.textBox4.Height = 0.2F;
            this.textBox4.Left = 2.87874F;
            this.textBox4.MultiLine = false;
            this.textBox4.Name = "textBox4";
            this.textBox4.Style = "font-family: ＭＳ 明朝; font-size: 10pt; font-weight: normal; ddo-char-set: 1";
            this.textBox4.Text = null;
            this.textBox4.Top = 0F;
            this.textBox4.Width = 1.167129F;
            // 
            // ghShishoCd
            // 
            this.ghShishoCd.CanGrow = false;
            this.ghShishoCd.DataField = "ITEM12";
            this.ghShishoCd.Height = 0F;
            this.ghShishoCd.Name = "ghShishoCd";
            this.ghShishoCd.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
            this.ghShishoCd.UnderlayNext = true;
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtValue02,
            this.txtValue03,
            this.txtValue04,
            this.txtValue05,
            this.txtValue06,
            this.txtValue8,
            this.txtValue01,
            this.txtValue07});
            this.detail.Height = 0.21875F;
            this.detail.Name = "detail";
            // 
            // txtValue02
            // 
            this.txtValue02.DataField = "ITEM04";
            this.txtValue02.Height = 0.1968504F;
            this.txtValue02.Left = 1.014173F;
            this.txtValue02.MultiLine = false;
            this.txtValue02.Name = "txtValue02";
            this.txtValue02.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 1";
            this.txtValue02.Text = null;
            this.txtValue02.Top = 0F;
            this.txtValue02.Width = 2.023622F;
            // 
            // txtValue03
            // 
            this.txtValue03.DataField = "ITEM05";
            this.txtValue03.Height = 0.1968504F;
            this.txtValue03.Left = 2.659843F;
            this.txtValue03.Name = "txtValue03";
            this.txtValue03.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
            this.txtValue03.Text = null;
            this.txtValue03.Top = 0F;
            this.txtValue03.Width = 1.180315F;
            // 
            // txtValue04
            // 
            this.txtValue04.DataField = "ITEM06";
            this.txtValue04.Height = 0.1968504F;
            this.txtValue04.Left = 3.704725F;
            this.txtValue04.Name = "txtValue04";
            this.txtValue04.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; text-justify: auto; ver" +
    "tical-align: middle; ddo-char-set: 1";
            this.txtValue04.Text = null;
            this.txtValue04.Top = 0F;
            this.txtValue04.Width = 1.284252F;
            // 
            // txtValue05
            // 
            this.txtValue05.DataField = "ITEM07";
            this.txtValue05.Height = 0.1968504F;
            this.txtValue05.Left = 4.988976F;
            this.txtValue05.Name = "txtValue05";
            this.txtValue05.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
            this.txtValue05.Text = null;
            this.txtValue05.Top = 0F;
            this.txtValue05.Width = 1.242126F;
            // 
            // txtValue06
            // 
            this.txtValue06.DataField = "ITEM08";
            this.txtValue06.Height = 0.1968504F;
            this.txtValue06.Left = 6.231103F;
            this.txtValue06.Name = "txtValue06";
            this.txtValue06.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
            this.txtValue06.Text = null;
            this.txtValue06.Top = 0F;
            this.txtValue06.Width = 1.021654F;
            // 
            // txtValue8
            // 
            this.txtValue8.DataField = "ITEM10";
            this.txtValue8.Height = 0.1968504F;
            this.txtValue8.Left = 8.362206F;
            this.txtValue8.Name = "txtValue8";
            this.txtValue8.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
            this.txtValue8.Text = null;
            this.txtValue8.Top = 0F;
            this.txtValue8.Width = 1.107085F;
            // 
            // txtValue01
            // 
            this.txtValue01.DataField = "ITEM03";
            this.txtValue01.Height = 0.1968504F;
            this.txtValue01.Left = 0.09724413F;
            this.txtValue01.Name = "txtValue01";
            this.txtValue01.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
            this.txtValue01.Text = null;
            this.txtValue01.Top = 0F;
            this.txtValue01.Width = 0.8259841F;
            // 
            // txtValue07
            // 
            this.txtValue07.DataField = "ITEM09";
            this.txtValue07.Height = 0.1968504F;
            this.txtValue07.Left = 7.252756F;
            this.txtValue07.Name = "txtValue07";
            this.txtValue07.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
            this.txtValue07.Text = null;
            this.txtValue07.Top = 0F;
            this.txtValue07.Width = 1.167323F;
            // 
            // gfShishoCd
            // 
            this.gfShishoCd.CanGrow = false;
            this.gfShishoCd.Height = 0F;
            this.gfShishoCd.Name = "gfShishoCd";
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            // 
            // reportHeader1
            // 
            this.reportHeader1.Height = 0F;
            this.reportHeader1.Name = "reportHeader1";
            // 
            // reportFooter1
            // 
            this.reportFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblTotal,
            this.txtTotal06,
            this.txtTotal01,
            this.txtTotal02,
            this.txtTotal03,
            this.txtTotal05,
            this.txtTotal04});
            this.reportFooter1.Height = 0.2291667F;
            this.reportFooter1.Name = "reportFooter1";
            this.reportFooter1.Format += new System.EventHandler(this.reportFooter1_Format);
            // 
            // lblTotal
            // 
            this.lblTotal.Height = 0.1968504F;
            this.lblTotal.HyperLink = null;
            this.lblTotal.Left = 1.607874F;
            this.lblTotal.MultiLine = false;
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; ddo" +
    "-char-set: 1";
            this.lblTotal.Text = "[合計]";
            this.lblTotal.Top = 0F;
            this.lblTotal.Width = 0.8125986F;
            // 
            // txtTotal06
            // 
            this.txtTotal06.DataField = "ITEM10";
            this.txtTotal06.Height = 0.1968504F;
            this.txtTotal06.Left = 8.362206F;
            this.txtTotal06.Name = "txtTotal06";
            this.txtTotal06.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
            this.txtTotal06.SummaryFunc = GrapeCity.ActiveReports.SectionReportModel.SummaryFunc.Min;
            this.txtTotal06.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtTotal06.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtTotal06.Text = null;
            this.txtTotal06.Top = 0F;
            this.txtTotal06.Width = 1.107085F;
            // 
            // txtTotal01
            // 
            this.txtTotal01.DataField = "ITEM05";
            this.txtTotal01.Height = 0.1968504F;
            this.txtTotal01.Left = 2.659843F;
            this.txtTotal01.Name = "txtTotal01";
            this.txtTotal01.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; text-justify: auto; ver" +
    "tical-align: middle; ddo-char-set: 1";
            this.txtTotal01.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtTotal01.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtTotal01.Text = null;
            this.txtTotal01.Top = 0F;
            this.txtTotal01.Width = 1.180315F;
            // 
            // txtTotal02
            // 
            this.txtTotal02.DataField = "ITEM06";
            this.txtTotal02.Height = 0.1968504F;
            this.txtTotal02.Left = 3.704725F;
            this.txtTotal02.Name = "txtTotal02";
            this.txtTotal02.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
            this.txtTotal02.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtTotal02.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtTotal02.Text = null;
            this.txtTotal02.Top = 0F;
            this.txtTotal02.Width = 1.284252F;
            // 
            // txtTotal03
            // 
            this.txtTotal03.DataField = "ITEM07";
            this.txtTotal03.Height = 0.1968504F;
            this.txtTotal03.Left = 4.988976F;
            this.txtTotal03.Name = "txtTotal03";
            this.txtTotal03.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
            this.txtTotal03.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtTotal03.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtTotal03.Text = null;
            this.txtTotal03.Top = 0F;
            this.txtTotal03.Width = 1.242127F;
            // 
            // txtTotal05
            // 
            this.txtTotal05.DataField = "ITEM09";
            this.txtTotal05.Height = 0.1968504F;
            this.txtTotal05.Left = 7.252756F;
            this.txtTotal05.Name = "txtTotal05";
            this.txtTotal05.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
            this.txtTotal05.SummaryFunc = GrapeCity.ActiveReports.SectionReportModel.SummaryFunc.Avg;
            this.txtTotal05.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtTotal05.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtTotal05.Text = null;
            this.txtTotal05.Top = 0F;
            this.txtTotal05.Width = 1.167323F;
            // 
            // txtTotal04
            // 
            this.txtTotal04.DataField = "ITEM08";
            this.txtTotal04.Height = 0.1968504F;
            this.txtTotal04.Left = 6.231103F;
            this.txtTotal04.Name = "txtTotal04";
            this.txtTotal04.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
            this.txtTotal04.SummaryFunc = GrapeCity.ActiveReports.SectionReportModel.SummaryFunc.Max;
            this.txtTotal04.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtTotal04.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtTotal04.Text = null;
            this.txtTotal04.Top = 0F;
            this.txtTotal04.Width = 1.021653F;
            // 
            // HNMR1061R
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.7086614F;
            this.PageSettings.Margins.Left = 0.3937008F;
            this.PageSettings.Margins.Right = 0.1968504F;
            this.PageSettings.Margins.Top = 0.5905512F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Portrait;
            this.PageSettings.PaperHeight = 13.89764F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.B4;
            this.PageSettings.PaperWidth = 9.84252F;
            this.PrintWidth = 9.518287F;
            this.Sections.Add(this.reportHeader1);
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.ghShishoCd);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.gfShishoCd);
            this.Sections.Add(this.pageFooter);
            this.Sections.Add(this.reportFooter1);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle04)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle05)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.page)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtToday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGyoshuCd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGyoshuNm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHonsu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNyukaryo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSoriage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTakane)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNakane)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblYasune)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue04)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue05)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue06)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue07)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal06)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal05)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal04)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.ReportHeader reportHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.ReportFooter reportFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader ghShishoCd;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter gfShishoCd;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle01;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle02;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox2;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle04;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle05;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox page;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle03;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtToday;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblGyoshuCd;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblGyoshuNm;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblHonsu;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblNyukaryo;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSoriage;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTakane;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblNakane;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblYasune;
        private GrapeCity.ActiveReports.SectionReportModel.Line line2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue02;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue03;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue04;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue05;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue06;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue07;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTotal;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal06;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal02;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal03;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal05;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal04;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox4;
    }
}
