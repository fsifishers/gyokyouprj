﻿namespace jp.co.fsi.hn.hncm1081
{
    partial class HNCM1082
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblPayaoCd = new System.Windows.Forms.Label();
            this.txtPayaoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtPayaoNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblPayaoNm = new System.Windows.Forms.Label();
            this.txtPayaoKanaNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblPayaoKanaNm = new System.Windows.Forms.Label();
            this.lblTesuryoRitsu = new System.Windows.Forms.Label();
            this.lblRitsuNm = new System.Windows.Forms.Label();
            this.txtTesuryoRitsu = new jp.co.fsi.common.controls.FsiTextBox();
            this.pnlDebug.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Size = new System.Drawing.Size(312, 23);
            this.lblTitle.Text = "";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(5, 99);
            this.pnlDebug.Size = new System.Drawing.Size(345, 100);
            // 
            // lblPayaoCd
            // 
            this.lblPayaoCd.BackColor = System.Drawing.Color.Silver;
            this.lblPayaoCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblPayaoCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblPayaoCd.Location = new System.Drawing.Point(12, 11);
            this.lblPayaoCd.Name = "lblPayaoCd";
            this.lblPayaoCd.Size = new System.Drawing.Size(145, 26);
            this.lblPayaoCd.TabIndex = 1;
            this.lblPayaoCd.Text = "パヤオコード";
            this.lblPayaoCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtPayaoCd
            // 
            this.txtPayaoCd.AutoSizeFromLength = false;
            this.txtPayaoCd.DisplayLength = null;
            this.txtPayaoCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtPayaoCd.Location = new System.Drawing.Point(101, 14);
            this.txtPayaoCd.MaxLength = 6;
            this.txtPayaoCd.Name = "txtPayaoCd";
            this.txtPayaoCd.Size = new System.Drawing.Size(53, 20);
            this.txtPayaoCd.TabIndex = 2;
            this.txtPayaoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPayaoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtPayaoCd_Validating);
            // 
            // txtPayaoNm
            // 
            this.txtPayaoNm.AutoSizeFromLength = false;
            this.txtPayaoNm.DisplayLength = null;
            this.txtPayaoNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtPayaoNm.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtPayaoNm.Location = new System.Drawing.Point(101, 48);
            this.txtPayaoNm.MaxLength = 40;
            this.txtPayaoNm.Name = "txtPayaoNm";
            this.txtPayaoNm.Size = new System.Drawing.Size(225, 19);
            this.txtPayaoNm.TabIndex = 4;
            this.txtPayaoNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtPayaoNm_Validating);
            // 
            // lblPayaoNm
            // 
            this.lblPayaoNm.BackColor = System.Drawing.Color.Silver;
            this.lblPayaoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblPayaoNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblPayaoNm.Location = new System.Drawing.Point(12, 45);
            this.lblPayaoNm.Name = "lblPayaoNm";
            this.lblPayaoNm.Size = new System.Drawing.Size(317, 26);
            this.lblPayaoNm.TabIndex = 3;
            this.lblPayaoNm.Text = "パ ヤ オ 名";
            this.lblPayaoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtPayaoKanaNm
            // 
            this.txtPayaoKanaNm.AutoSizeFromLength = false;
            this.txtPayaoKanaNm.DisplayLength = null;
            this.txtPayaoKanaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtPayaoKanaNm.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtPayaoKanaNm.Location = new System.Drawing.Point(101, 82);
            this.txtPayaoKanaNm.MaxLength = 40;
            this.txtPayaoKanaNm.Name = "txtPayaoKanaNm";
            this.txtPayaoKanaNm.Size = new System.Drawing.Size(225, 20);
            this.txtPayaoKanaNm.TabIndex = 6;
            this.txtPayaoKanaNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtPayaoKanaNm_Validating);
            // 
            // lblPayaoKanaNm
            // 
            this.lblPayaoKanaNm.BackColor = System.Drawing.Color.Silver;
            this.lblPayaoKanaNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblPayaoKanaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblPayaoKanaNm.Location = new System.Drawing.Point(12, 79);
            this.lblPayaoKanaNm.Name = "lblPayaoKanaNm";
            this.lblPayaoKanaNm.Size = new System.Drawing.Size(317, 26);
            this.lblPayaoKanaNm.TabIndex = 5;
            this.lblPayaoKanaNm.Text = "パヤオカナ名";
            this.lblPayaoKanaNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTesuryoRitsu
            // 
            this.lblTesuryoRitsu.BackColor = System.Drawing.Color.Silver;
            this.lblTesuryoRitsu.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTesuryoRitsu.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTesuryoRitsu.Location = new System.Drawing.Point(12, 113);
            this.lblTesuryoRitsu.Name = "lblTesuryoRitsu";
            this.lblTesuryoRitsu.Size = new System.Drawing.Size(145, 26);
            this.lblTesuryoRitsu.TabIndex = 7;
            this.lblTesuryoRitsu.Text = "手　数　料　率";
            this.lblTesuryoRitsu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblRitsuNm
            // 
            this.lblRitsuNm.BackColor = System.Drawing.Color.Silver;
            this.lblRitsuNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblRitsuNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblRitsuNm.Location = new System.Drawing.Point(156, 113);
            this.lblRitsuNm.Name = "lblRitsuNm";
            this.lblRitsuNm.Size = new System.Drawing.Size(20, 26);
            this.lblRitsuNm.TabIndex = 9;
            this.lblRitsuNm.Text = "％";
            this.lblRitsuNm.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtTesuryoRitsu
            // 
            this.txtTesuryoRitsu.AutoSizeFromLength = false;
            this.txtTesuryoRitsu.DisplayLength = null;
            this.txtTesuryoRitsu.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTesuryoRitsu.Location = new System.Drawing.Point(101, 116);
            this.txtTesuryoRitsu.MaxLength = 5;
            this.txtTesuryoRitsu.Name = "txtTesuryoRitsu";
            this.txtTesuryoRitsu.Size = new System.Drawing.Size(53, 20);
            this.txtTesuryoRitsu.TabIndex = 8;
            this.txtTesuryoRitsu.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTesuryoRitsu.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LastControl_KeyDown);
            this.txtTesuryoRitsu.Validating += new System.ComponentModel.CancelEventHandler(this.txtTesuryoRitsu_Validating);
            // 
            // HNCM1082
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(337, 202);
            this.Controls.Add(this.txtTesuryoRitsu);
            this.Controls.Add(this.lblRitsuNm);
            this.Controls.Add(this.lblTesuryoRitsu);
            this.Controls.Add(this.txtPayaoKanaNm);
            this.Controls.Add(this.lblPayaoKanaNm);
            this.Controls.Add(this.txtPayaoNm);
            this.Controls.Add(this.lblPayaoNm);
            this.Controls.Add(this.txtPayaoCd);
            this.Controls.Add(this.lblPayaoCd);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "HNCM1082";
            this.ShowFButton = true;
            this.Text = "";
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.lblPayaoCd, 0);
            this.Controls.SetChildIndex(this.txtPayaoCd, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblPayaoNm, 0);
            this.Controls.SetChildIndex(this.txtPayaoNm, 0);
            this.Controls.SetChildIndex(this.lblPayaoKanaNm, 0);
            this.Controls.SetChildIndex(this.txtPayaoKanaNm, 0);
            this.Controls.SetChildIndex(this.lblTesuryoRitsu, 0);
            this.Controls.SetChildIndex(this.lblRitsuNm, 0);
            this.Controls.SetChildIndex(this.txtTesuryoRitsu, 0);
            this.pnlDebug.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblPayaoCd;
        private jp.co.fsi.common.controls.FsiTextBox txtPayaoCd;
        private jp.co.fsi.common.controls.FsiTextBox txtPayaoNm;
        private System.Windows.Forms.Label lblPayaoNm;
        private jp.co.fsi.common.controls.FsiTextBox txtPayaoKanaNm;
        private System.Windows.Forms.Label lblPayaoKanaNm;
        private System.Windows.Forms.Label lblTesuryoRitsu;
        private System.Windows.Forms.Label lblRitsuNm;
        private common.controls.FsiTextBox txtTesuryoRitsu;


    }
}