﻿namespace jp.co.fsi.hn.hnmr1141
{
    partial class HNMR1141
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbxOutput = new System.Windows.Forms.GroupBox();
            this.lblShukeihyoNm = new System.Windows.Forms.Label();
            this.txtShukeihyo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShukeihyo = new System.Windows.Forms.Label();
            this.txtChikuCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtChikuCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblCodeBet3 = new System.Windows.Forms.Label();
            this.gbxOutputRank = new System.Windows.Forms.GroupBox();
            this.lblJuni = new System.Windows.Forms.Label();
            this.txtJuni = new jp.co.fsi.common.controls.FsiTextBox();
            this.gbxCondition = new System.Windows.Forms.GroupBox();
            this.lblGyoshuCdTo = new System.Windows.Forms.Label();
            this.lblGyoshuCdFr = new System.Windows.Forms.Label();
            this.lblGyoshuCd = new System.Windows.Forms.Label();
            this.txtGyoshuCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtGyoshuCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblCodeBet6 = new System.Windows.Forms.Label();
            this.lblGyoshuBunruiCdTo = new System.Windows.Forms.Label();
            this.lblGyoshuBunruiCdFr = new System.Windows.Forms.Label();
            this.lblGyoshuBunruiCd = new System.Windows.Forms.Label();
            this.txtGyoshuBunruiCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtGyoshuBunruiCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblCodeBet5 = new System.Windows.Forms.Label();
            this.lblChikuCdTo = new System.Windows.Forms.Label();
            this.lblChikuCdFr = new System.Windows.Forms.Label();
            this.lblChikuCd = new System.Windows.Forms.Label();
            this.lblGyohoCdTo = new System.Windows.Forms.Label();
            this.txtGyohoCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblCodeBet2 = new System.Windows.Forms.Label();
            this.lblGyohoCdFr = new System.Windows.Forms.Label();
            this.txtGyohoCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblGyohoCd = new System.Windows.Forms.Label();
            this.lblSenshuCdTo = new System.Windows.Forms.Label();
            this.txtSenshuCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblCodeBet1 = new System.Windows.Forms.Label();
            this.lblSenshuCdFr = new System.Windows.Forms.Label();
            this.txtSenshuCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblSenshuCd = new System.Windows.Forms.Label();
            this.gbxSearchRangeDate = new System.Windows.Forms.GroupBox();
            this.lblDayTo = new System.Windows.Forms.Label();
            this.lblCodeBetDate = new System.Windows.Forms.Label();
            this.lblDayFr = new System.Windows.Forms.Label();
            this.lblMonthTo = new System.Windows.Forms.Label();
            this.lblMonthFr = new System.Windows.Forms.Label();
            this.lblYearTo = new System.Windows.Forms.Label();
            this.txtDayTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtDayFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblYearFr = new System.Windows.Forms.Label();
            this.txtMonthTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtMonthFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtYearTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtYearFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblGengoTo = new System.Windows.Forms.Label();
            this.lblGengoFr = new System.Windows.Forms.Label();
            this.lblTo = new System.Windows.Forms.Label();
            this.lblFr = new System.Windows.Forms.Label();
            this.gbxMizuageShisho = new System.Windows.Forms.GroupBox();
            this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMizuageShishoNm = new System.Windows.Forms.Label();
            this.lblMizuageShisho = new System.Windows.Forms.Label();
            this.gbxSeisanKubun = new System.Windows.Forms.GroupBox();
            this.lblSeisanKubun = new System.Windows.Forms.Label();
            this.txtSeisanKubun = new jp.co.fsi.common.controls.FsiTextBox();
            this.pnlDebug.SuspendLayout();
            this.gbxOutput.SuspendLayout();
            this.gbxOutputRank.SuspendLayout();
            this.gbxCondition.SuspendLayout();
            this.gbxSearchRangeDate.SuspendLayout();
            this.gbxMizuageShisho.SuspendLayout();
            this.gbxSeisanKubun.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Size = new System.Drawing.Size(818, 23);
            this.lblTitle.TabIndex = 1;
            this.lblTitle.Text = "セリ推移表";
            // 
            // btnF2
            // 
            this.btnF2.Text = "F2";
            // 
            // btnF3
            // 
            this.btnF3.Text = "F3";
            // 
            // btnF4
            // 
            this.btnF4.Text = "F4\r\n\r\nプレビュー";
            // 
            // btnF6
            // 
            this.btnF6.Text = "F6\r\n\r\nEXCEL";
            // 
            // btnF8
            // 
            this.btnF8.Text = "F8\r\n\r\n";
            // 
            // btnF12
            // 
            this.btnF12.Text = "F12\r\n\r\n項目設定";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.pnlDebug.Size = new System.Drawing.Size(847, 100);
            // 
            // gbxOutput
            // 
            this.gbxOutput.Controls.Add(this.lblShukeihyoNm);
            this.gbxOutput.Controls.Add(this.txtShukeihyo);
            this.gbxOutput.Controls.Add(this.lblShukeihyo);
            this.gbxOutput.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxOutput.Location = new System.Drawing.Point(12, 117);
            this.gbxOutput.Name = "gbxOutput";
            this.gbxOutput.Size = new System.Drawing.Size(365, 60);
            this.gbxOutput.TabIndex = 2;
            this.gbxOutput.TabStop = false;
            this.gbxOutput.Text = "推移表種別";
            // 
            // lblShukeihyoNm
            // 
            this.lblShukeihyoNm.BackColor = System.Drawing.Color.Silver;
            this.lblShukeihyoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShukeihyoNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShukeihyoNm.Location = new System.Drawing.Point(142, 26);
            this.lblShukeihyoNm.Name = "lblShukeihyoNm";
            this.lblShukeihyoNm.Size = new System.Drawing.Size(218, 20);
            this.lblShukeihyoNm.TabIndex = 2;
            this.lblShukeihyoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShukeihyo
            // 
            this.txtShukeihyo.AutoSizeFromLength = true;
            this.txtShukeihyo.DisplayLength = null;
            this.txtShukeihyo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShukeihyo.Location = new System.Drawing.Point(91, 26);
            this.txtShukeihyo.MaxLength = 4;
            this.txtShukeihyo.Name = "txtShukeihyo";
            this.txtShukeihyo.Size = new System.Drawing.Size(48, 20);
            this.txtShukeihyo.TabIndex = 1;
            this.txtShukeihyo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShukeihyo.Validating += new System.ComponentModel.CancelEventHandler(this.txtShukeihyo_Validating);
            // 
            // lblShukeihyo
            // 
            this.lblShukeihyo.BackColor = System.Drawing.Color.Silver;
            this.lblShukeihyo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShukeihyo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShukeihyo.Location = new System.Drawing.Point(10, 26);
            this.lblShukeihyo.Name = "lblShukeihyo";
            this.lblShukeihyo.Size = new System.Drawing.Size(80, 20);
            this.lblShukeihyo.TabIndex = 0;
            this.lblShukeihyo.Text = "推 移 表";
            this.lblShukeihyo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtChikuCdTo
            // 
            this.txtChikuCdTo.AutoSizeFromLength = true;
            this.txtChikuCdTo.DisplayLength = null;
            this.txtChikuCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtChikuCdTo.Location = new System.Drawing.Point(348, 73);
            this.txtChikuCdTo.MaxLength = 4;
            this.txtChikuCdTo.Name = "txtChikuCdTo";
            this.txtChikuCdTo.Size = new System.Drawing.Size(48, 20);
            this.txtChikuCdTo.TabIndex = 16;
            this.txtChikuCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtChikuCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtChikuCdTo_Validating);
            // 
            // txtChikuCdFr
            // 
            this.txtChikuCdFr.AutoSizeFromLength = true;
            this.txtChikuCdFr.DisplayLength = null;
            this.txtChikuCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtChikuCdFr.Location = new System.Drawing.Point(91, 73);
            this.txtChikuCdFr.MaxLength = 4;
            this.txtChikuCdFr.Name = "txtChikuCdFr";
            this.txtChikuCdFr.Size = new System.Drawing.Size(48, 20);
            this.txtChikuCdFr.TabIndex = 13;
            this.txtChikuCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtChikuCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtChikuCdFr_Validating);
            // 
            // lblCodeBet3
            // 
            this.lblCodeBet3.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblCodeBet3.Location = new System.Drawing.Point(324, 73);
            this.lblCodeBet3.Name = "lblCodeBet3";
            this.lblCodeBet3.Size = new System.Drawing.Size(18, 20);
            this.lblCodeBet3.TabIndex = 15;
            this.lblCodeBet3.Text = "～";
            this.lblCodeBet3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // gbxOutputRank
            // 
            this.gbxOutputRank.Controls.Add(this.lblJuni);
            this.gbxOutputRank.Controls.Add(this.txtJuni);
            this.gbxOutputRank.Enabled = false;
            this.gbxOutputRank.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxOutputRank.Location = new System.Drawing.Point(496, 191);
            this.gbxOutputRank.Name = "gbxOutputRank";
            this.gbxOutputRank.Size = new System.Drawing.Size(107, 63);
            this.gbxOutputRank.TabIndex = 4;
            this.gbxOutputRank.TabStop = false;
            this.gbxOutputRank.Text = "順位";
            this.gbxOutputRank.Visible = false;
            // 
            // lblJuni
            // 
            this.lblJuni.BackColor = System.Drawing.Color.White;
            this.lblJuni.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblJuni.Location = new System.Drawing.Point(51, 25);
            this.lblJuni.Name = "lblJuni";
            this.lblJuni.Size = new System.Drawing.Size(52, 20);
            this.lblJuni.TabIndex = 1;
            this.lblJuni.Text = "位まで";
            this.lblJuni.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtJuni
            // 
            this.txtJuni.AutoSizeFromLength = true;
            this.txtJuni.DisplayLength = null;
            this.txtJuni.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtJuni.Location = new System.Drawing.Point(10, 25);
            this.txtJuni.MaxLength = 4;
            this.txtJuni.Name = "txtJuni";
            this.txtJuni.Size = new System.Drawing.Size(34, 20);
            this.txtJuni.TabIndex = 0;
            this.txtJuni.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // gbxCondition
            // 
            this.gbxCondition.Controls.Add(this.lblGyoshuCdTo);
            this.gbxCondition.Controls.Add(this.lblGyoshuCdFr);
            this.gbxCondition.Controls.Add(this.lblGyoshuCd);
            this.gbxCondition.Controls.Add(this.txtGyoshuCdTo);
            this.gbxCondition.Controls.Add(this.txtGyoshuCdFr);
            this.gbxCondition.Controls.Add(this.lblCodeBet6);
            this.gbxCondition.Controls.Add(this.lblGyoshuBunruiCdTo);
            this.gbxCondition.Controls.Add(this.lblGyoshuBunruiCdFr);
            this.gbxCondition.Controls.Add(this.lblGyoshuBunruiCd);
            this.gbxCondition.Controls.Add(this.txtGyoshuBunruiCdTo);
            this.gbxCondition.Controls.Add(this.txtGyoshuBunruiCdFr);
            this.gbxCondition.Controls.Add(this.lblCodeBet5);
            this.gbxCondition.Controls.Add(this.lblChikuCdTo);
            this.gbxCondition.Controls.Add(this.lblChikuCdFr);
            this.gbxCondition.Controls.Add(this.lblChikuCd);
            this.gbxCondition.Controls.Add(this.lblGyohoCdTo);
            this.gbxCondition.Controls.Add(this.txtGyohoCdTo);
            this.gbxCondition.Controls.Add(this.lblCodeBet2);
            this.gbxCondition.Controls.Add(this.lblGyohoCdFr);
            this.gbxCondition.Controls.Add(this.txtGyohoCdFr);
            this.gbxCondition.Controls.Add(this.lblGyohoCd);
            this.gbxCondition.Controls.Add(this.lblSenshuCdTo);
            this.gbxCondition.Controls.Add(this.txtSenshuCdTo);
            this.gbxCondition.Controls.Add(this.lblCodeBet1);
            this.gbxCondition.Controls.Add(this.lblSenshuCdFr);
            this.gbxCondition.Controls.Add(this.txtSenshuCdFr);
            this.gbxCondition.Controls.Add(this.lblSenshuCd);
            this.gbxCondition.Controls.Add(this.txtChikuCdTo);
            this.gbxCondition.Controls.Add(this.txtChikuCdFr);
            this.gbxCondition.Controls.Add(this.lblCodeBet3);
            this.gbxCondition.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxCondition.Location = new System.Drawing.Point(12, 353);
            this.gbxCondition.Name = "gbxCondition";
            this.gbxCondition.Size = new System.Drawing.Size(591, 156);
            this.gbxCondition.TabIndex = 6;
            this.gbxCondition.TabStop = false;
            this.gbxCondition.Text = "コード範囲";
            // 
            // lblGyoshuCdTo
            // 
            this.lblGyoshuCdTo.BackColor = System.Drawing.Color.Silver;
            this.lblGyoshuCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGyoshuCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGyoshuCdTo.Location = new System.Drawing.Point(399, 123);
            this.lblGyoshuCdTo.Name = "lblGyoshuCdTo";
            this.lblGyoshuCdTo.Size = new System.Drawing.Size(180, 20);
            this.lblGyoshuCdTo.TabIndex = 29;
            this.lblGyoshuCdTo.Text = "最　後";
            this.lblGyoshuCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblGyoshuCdFr
            // 
            this.lblGyoshuCdFr.BackColor = System.Drawing.Color.Silver;
            this.lblGyoshuCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGyoshuCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGyoshuCdFr.Location = new System.Drawing.Point(142, 123);
            this.lblGyoshuCdFr.Name = "lblGyoshuCdFr";
            this.lblGyoshuCdFr.Size = new System.Drawing.Size(180, 20);
            this.lblGyoshuCdFr.TabIndex = 26;
            this.lblGyoshuCdFr.Text = "先　頭";
            this.lblGyoshuCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblGyoshuCd
            // 
            this.lblGyoshuCd.BackColor = System.Drawing.Color.Silver;
            this.lblGyoshuCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGyoshuCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGyoshuCd.Location = new System.Drawing.Point(10, 123);
            this.lblGyoshuCd.Name = "lblGyoshuCd";
            this.lblGyoshuCd.Size = new System.Drawing.Size(80, 20);
            this.lblGyoshuCd.TabIndex = 24;
            this.lblGyoshuCd.Text = "魚      種";
            this.lblGyoshuCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtGyoshuCdTo
            // 
            this.txtGyoshuCdTo.AutoSizeFromLength = true;
            this.txtGyoshuCdTo.DisplayLength = null;
            this.txtGyoshuCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGyoshuCdTo.Location = new System.Drawing.Point(348, 123);
            this.txtGyoshuCdTo.MaxLength = 4;
            this.txtGyoshuCdTo.Name = "txtGyoshuCdTo";
            this.txtGyoshuCdTo.Size = new System.Drawing.Size(48, 20);
            this.txtGyoshuCdTo.TabIndex = 28;
            this.txtGyoshuCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGyoshuCdTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtGyoshuCdTo_KeyDown);
            this.txtGyoshuCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtGyoshuCdTo_Validating);
            // 
            // txtGyoshuCdFr
            // 
            this.txtGyoshuCdFr.AutoSizeFromLength = true;
            this.txtGyoshuCdFr.DisplayLength = null;
            this.txtGyoshuCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGyoshuCdFr.Location = new System.Drawing.Point(91, 123);
            this.txtGyoshuCdFr.MaxLength = 4;
            this.txtGyoshuCdFr.Name = "txtGyoshuCdFr";
            this.txtGyoshuCdFr.Size = new System.Drawing.Size(48, 20);
            this.txtGyoshuCdFr.TabIndex = 25;
            this.txtGyoshuCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGyoshuCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtGyoshuCdFr_Validating);
            // 
            // lblCodeBet6
            // 
            this.lblCodeBet6.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblCodeBet6.Location = new System.Drawing.Point(324, 123);
            this.lblCodeBet6.Name = "lblCodeBet6";
            this.lblCodeBet6.Size = new System.Drawing.Size(18, 20);
            this.lblCodeBet6.TabIndex = 27;
            this.lblCodeBet6.Text = "～";
            this.lblCodeBet6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblGyoshuBunruiCdTo
            // 
            this.lblGyoshuBunruiCdTo.BackColor = System.Drawing.Color.Silver;
            this.lblGyoshuBunruiCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGyoshuBunruiCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGyoshuBunruiCdTo.Location = new System.Drawing.Point(399, 98);
            this.lblGyoshuBunruiCdTo.Name = "lblGyoshuBunruiCdTo";
            this.lblGyoshuBunruiCdTo.Size = new System.Drawing.Size(180, 20);
            this.lblGyoshuBunruiCdTo.TabIndex = 23;
            this.lblGyoshuBunruiCdTo.Text = "最　後";
            this.lblGyoshuBunruiCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblGyoshuBunruiCdFr
            // 
            this.lblGyoshuBunruiCdFr.BackColor = System.Drawing.Color.Silver;
            this.lblGyoshuBunruiCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGyoshuBunruiCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGyoshuBunruiCdFr.Location = new System.Drawing.Point(142, 98);
            this.lblGyoshuBunruiCdFr.Name = "lblGyoshuBunruiCdFr";
            this.lblGyoshuBunruiCdFr.Size = new System.Drawing.Size(180, 20);
            this.lblGyoshuBunruiCdFr.TabIndex = 20;
            this.lblGyoshuBunruiCdFr.Text = "先　頭";
            this.lblGyoshuBunruiCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblGyoshuBunruiCd
            // 
            this.lblGyoshuBunruiCd.BackColor = System.Drawing.Color.Silver;
            this.lblGyoshuBunruiCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGyoshuBunruiCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGyoshuBunruiCd.Location = new System.Drawing.Point(10, 98);
            this.lblGyoshuBunruiCd.Name = "lblGyoshuBunruiCd";
            this.lblGyoshuBunruiCd.Size = new System.Drawing.Size(80, 20);
            this.lblGyoshuBunruiCd.TabIndex = 18;
            this.lblGyoshuBunruiCd.Text = "魚 種分 類";
            this.lblGyoshuBunruiCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtGyoshuBunruiCdTo
            // 
            this.txtGyoshuBunruiCdTo.AutoSizeFromLength = true;
            this.txtGyoshuBunruiCdTo.DisplayLength = null;
            this.txtGyoshuBunruiCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGyoshuBunruiCdTo.Location = new System.Drawing.Point(348, 98);
            this.txtGyoshuBunruiCdTo.MaxLength = 4;
            this.txtGyoshuBunruiCdTo.Name = "txtGyoshuBunruiCdTo";
            this.txtGyoshuBunruiCdTo.Size = new System.Drawing.Size(48, 20);
            this.txtGyoshuBunruiCdTo.TabIndex = 22;
            this.txtGyoshuBunruiCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGyoshuBunruiCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtGyoshuBunruiCdTo_Validating);
            // 
            // txtGyoshuBunruiCdFr
            // 
            this.txtGyoshuBunruiCdFr.AutoSizeFromLength = true;
            this.txtGyoshuBunruiCdFr.DisplayLength = null;
            this.txtGyoshuBunruiCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGyoshuBunruiCdFr.Location = new System.Drawing.Point(91, 98);
            this.txtGyoshuBunruiCdFr.MaxLength = 4;
            this.txtGyoshuBunruiCdFr.Name = "txtGyoshuBunruiCdFr";
            this.txtGyoshuBunruiCdFr.Size = new System.Drawing.Size(48, 20);
            this.txtGyoshuBunruiCdFr.TabIndex = 19;
            this.txtGyoshuBunruiCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGyoshuBunruiCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtGyoshuBunruiCdFr_Validating);
            // 
            // lblCodeBet5
            // 
            this.lblCodeBet5.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblCodeBet5.Location = new System.Drawing.Point(324, 98);
            this.lblCodeBet5.Name = "lblCodeBet5";
            this.lblCodeBet5.Size = new System.Drawing.Size(18, 20);
            this.lblCodeBet5.TabIndex = 21;
            this.lblCodeBet5.Text = "～";
            this.lblCodeBet5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblChikuCdTo
            // 
            this.lblChikuCdTo.BackColor = System.Drawing.Color.Silver;
            this.lblChikuCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblChikuCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblChikuCdTo.Location = new System.Drawing.Point(399, 73);
            this.lblChikuCdTo.Name = "lblChikuCdTo";
            this.lblChikuCdTo.Size = new System.Drawing.Size(180, 20);
            this.lblChikuCdTo.TabIndex = 17;
            this.lblChikuCdTo.Text = "最　後";
            this.lblChikuCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblChikuCdFr
            // 
            this.lblChikuCdFr.BackColor = System.Drawing.Color.Silver;
            this.lblChikuCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblChikuCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblChikuCdFr.Location = new System.Drawing.Point(142, 73);
            this.lblChikuCdFr.Name = "lblChikuCdFr";
            this.lblChikuCdFr.Size = new System.Drawing.Size(180, 20);
            this.lblChikuCdFr.TabIndex = 14;
            this.lblChikuCdFr.Text = "先　頭";
            this.lblChikuCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblChikuCd
            // 
            this.lblChikuCd.BackColor = System.Drawing.Color.Silver;
            this.lblChikuCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblChikuCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblChikuCd.Location = new System.Drawing.Point(10, 73);
            this.lblChikuCd.Name = "lblChikuCd";
            this.lblChikuCd.Size = new System.Drawing.Size(80, 20);
            this.lblChikuCd.TabIndex = 12;
            this.lblChikuCd.Text = "地      区";
            this.lblChikuCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblGyohoCdTo
            // 
            this.lblGyohoCdTo.BackColor = System.Drawing.Color.Silver;
            this.lblGyohoCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGyohoCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGyohoCdTo.Location = new System.Drawing.Point(399, 48);
            this.lblGyohoCdTo.Name = "lblGyohoCdTo";
            this.lblGyohoCdTo.Size = new System.Drawing.Size(180, 20);
            this.lblGyohoCdTo.TabIndex = 11;
            this.lblGyohoCdTo.Text = "最　後";
            this.lblGyohoCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtGyohoCdTo
            // 
            this.txtGyohoCdTo.AutoSizeFromLength = true;
            this.txtGyohoCdTo.DisplayLength = null;
            this.txtGyohoCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGyohoCdTo.Location = new System.Drawing.Point(348, 48);
            this.txtGyohoCdTo.MaxLength = 4;
            this.txtGyohoCdTo.Name = "txtGyohoCdTo";
            this.txtGyohoCdTo.Size = new System.Drawing.Size(48, 20);
            this.txtGyohoCdTo.TabIndex = 10;
            this.txtGyohoCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGyohoCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtGyohoCdTo_Validating);
            // 
            // lblCodeBet2
            // 
            this.lblCodeBet2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblCodeBet2.Location = new System.Drawing.Point(324, 47);
            this.lblCodeBet2.Name = "lblCodeBet2";
            this.lblCodeBet2.Size = new System.Drawing.Size(18, 20);
            this.lblCodeBet2.TabIndex = 9;
            this.lblCodeBet2.Text = "～";
            this.lblCodeBet2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblGyohoCdFr
            // 
            this.lblGyohoCdFr.BackColor = System.Drawing.Color.Silver;
            this.lblGyohoCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGyohoCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGyohoCdFr.Location = new System.Drawing.Point(142, 48);
            this.lblGyohoCdFr.Name = "lblGyohoCdFr";
            this.lblGyohoCdFr.Size = new System.Drawing.Size(180, 20);
            this.lblGyohoCdFr.TabIndex = 8;
            this.lblGyohoCdFr.Text = "先　頭";
            this.lblGyohoCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtGyohoCdFr
            // 
            this.txtGyohoCdFr.AutoSizeFromLength = true;
            this.txtGyohoCdFr.DisplayLength = null;
            this.txtGyohoCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGyohoCdFr.Location = new System.Drawing.Point(91, 48);
            this.txtGyohoCdFr.MaxLength = 4;
            this.txtGyohoCdFr.Name = "txtGyohoCdFr";
            this.txtGyohoCdFr.Size = new System.Drawing.Size(48, 20);
            this.txtGyohoCdFr.TabIndex = 7;
            this.txtGyohoCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGyohoCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtGyohoCdFr_Validating);
            // 
            // lblGyohoCd
            // 
            this.lblGyohoCd.BackColor = System.Drawing.Color.Silver;
            this.lblGyohoCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGyohoCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGyohoCd.Location = new System.Drawing.Point(10, 48);
            this.lblGyohoCd.Name = "lblGyohoCd";
            this.lblGyohoCd.Size = new System.Drawing.Size(80, 20);
            this.lblGyohoCd.TabIndex = 6;
            this.lblGyohoCd.Text = "魚      法";
            this.lblGyohoCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSenshuCdTo
            // 
            this.lblSenshuCdTo.BackColor = System.Drawing.Color.Silver;
            this.lblSenshuCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSenshuCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblSenshuCdTo.Location = new System.Drawing.Point(399, 23);
            this.lblSenshuCdTo.Name = "lblSenshuCdTo";
            this.lblSenshuCdTo.Size = new System.Drawing.Size(180, 20);
            this.lblSenshuCdTo.TabIndex = 5;
            this.lblSenshuCdTo.Text = "最　後";
            this.lblSenshuCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtSenshuCdTo
            // 
            this.txtSenshuCdTo.AutoSizeFromLength = true;
            this.txtSenshuCdTo.DisplayLength = null;
            this.txtSenshuCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtSenshuCdTo.Location = new System.Drawing.Point(348, 23);
            this.txtSenshuCdTo.MaxLength = 4;
            this.txtSenshuCdTo.Name = "txtSenshuCdTo";
            this.txtSenshuCdTo.Size = new System.Drawing.Size(48, 20);
            this.txtSenshuCdTo.TabIndex = 4;
            this.txtSenshuCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSenshuCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtSenshuCdTo_Validating);
            // 
            // lblCodeBet1
            // 
            this.lblCodeBet1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblCodeBet1.Location = new System.Drawing.Point(324, 23);
            this.lblCodeBet1.Name = "lblCodeBet1";
            this.lblCodeBet1.Size = new System.Drawing.Size(18, 20);
            this.lblCodeBet1.TabIndex = 3;
            this.lblCodeBet1.Text = "～";
            this.lblCodeBet1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSenshuCdFr
            // 
            this.lblSenshuCdFr.BackColor = System.Drawing.Color.Silver;
            this.lblSenshuCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSenshuCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblSenshuCdFr.Location = new System.Drawing.Point(142, 23);
            this.lblSenshuCdFr.Name = "lblSenshuCdFr";
            this.lblSenshuCdFr.Size = new System.Drawing.Size(180, 20);
            this.lblSenshuCdFr.TabIndex = 2;
            this.lblSenshuCdFr.Text = "先　頭";
            this.lblSenshuCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtSenshuCdFr
            // 
            this.txtSenshuCdFr.AutoSizeFromLength = true;
            this.txtSenshuCdFr.DisplayLength = null;
            this.txtSenshuCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtSenshuCdFr.Location = new System.Drawing.Point(91, 23);
            this.txtSenshuCdFr.MaxLength = 4;
            this.txtSenshuCdFr.Name = "txtSenshuCdFr";
            this.txtSenshuCdFr.Size = new System.Drawing.Size(48, 20);
            this.txtSenshuCdFr.TabIndex = 1;
            this.txtSenshuCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSenshuCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtSenshuCdFr_Validating);
            // 
            // lblSenshuCd
            // 
            this.lblSenshuCd.BackColor = System.Drawing.Color.Silver;
            this.lblSenshuCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSenshuCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblSenshuCd.Location = new System.Drawing.Point(10, 23);
            this.lblSenshuCd.Name = "lblSenshuCd";
            this.lblSenshuCd.Size = new System.Drawing.Size(80, 20);
            this.lblSenshuCd.TabIndex = 0;
            this.lblSenshuCd.Text = "船      主";
            this.lblSenshuCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // gbxSearchRangeDate
            // 
            this.gbxSearchRangeDate.Controls.Add(this.lblDayTo);
            this.gbxSearchRangeDate.Controls.Add(this.lblCodeBetDate);
            this.gbxSearchRangeDate.Controls.Add(this.lblDayFr);
            this.gbxSearchRangeDate.Controls.Add(this.lblMonthTo);
            this.gbxSearchRangeDate.Controls.Add(this.lblMonthFr);
            this.gbxSearchRangeDate.Controls.Add(this.lblYearTo);
            this.gbxSearchRangeDate.Controls.Add(this.txtDayTo);
            this.gbxSearchRangeDate.Controls.Add(this.txtDayFr);
            this.gbxSearchRangeDate.Controls.Add(this.lblYearFr);
            this.gbxSearchRangeDate.Controls.Add(this.txtMonthTo);
            this.gbxSearchRangeDate.Controls.Add(this.txtMonthFr);
            this.gbxSearchRangeDate.Controls.Add(this.txtYearTo);
            this.gbxSearchRangeDate.Controls.Add(this.txtYearFr);
            this.gbxSearchRangeDate.Controls.Add(this.lblGengoTo);
            this.gbxSearchRangeDate.Controls.Add(this.lblGengoFr);
            this.gbxSearchRangeDate.Controls.Add(this.lblTo);
            this.gbxSearchRangeDate.Controls.Add(this.lblFr);
            this.gbxSearchRangeDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxSearchRangeDate.ForeColor = System.Drawing.Color.Black;
            this.gbxSearchRangeDate.Location = new System.Drawing.Point(12, 191);
            this.gbxSearchRangeDate.Name = "gbxSearchRangeDate";
            this.gbxSearchRangeDate.Size = new System.Drawing.Size(468, 63);
            this.gbxSearchRangeDate.TabIndex = 3;
            this.gbxSearchRangeDate.TabStop = false;
            this.gbxSearchRangeDate.Text = "日付範囲";
            // 
            // lblDayTo
            // 
            this.lblDayTo.AutoSize = true;
            this.lblDayTo.BackColor = System.Drawing.Color.Silver;
            this.lblDayTo.Enabled = false;
            this.lblDayTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDayTo.Location = new System.Drawing.Point(433, 29);
            this.lblDayTo.Name = "lblDayTo";
            this.lblDayTo.Size = new System.Drawing.Size(21, 13);
            this.lblDayTo.TabIndex = 16;
            this.lblDayTo.Text = "日";
            this.lblDayTo.Visible = false;
            // 
            // lblCodeBetDate
            // 
            this.lblCodeBetDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblCodeBetDate.Location = new System.Drawing.Point(222, 24);
            this.lblCodeBetDate.Name = "lblCodeBetDate";
            this.lblCodeBetDate.Size = new System.Drawing.Size(18, 20);
            this.lblCodeBetDate.TabIndex = 8;
            this.lblCodeBetDate.Text = "～";
            this.lblCodeBetDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDayFr
            // 
            this.lblDayFr.AutoSize = true;
            this.lblDayFr.BackColor = System.Drawing.Color.Silver;
            this.lblDayFr.Enabled = false;
            this.lblDayFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDayFr.Location = new System.Drawing.Point(201, 29);
            this.lblDayFr.Name = "lblDayFr";
            this.lblDayFr.Size = new System.Drawing.Size(21, 13);
            this.lblDayFr.TabIndex = 7;
            this.lblDayFr.Text = "日";
            this.lblDayFr.Visible = false;
            // 
            // lblMonthTo
            // 
            this.lblMonthTo.AutoSize = true;
            this.lblMonthTo.BackColor = System.Drawing.Color.Silver;
            this.lblMonthTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMonthTo.Location = new System.Drawing.Point(378, 29);
            this.lblMonthTo.Name = "lblMonthTo";
            this.lblMonthTo.Size = new System.Drawing.Size(21, 13);
            this.lblMonthTo.TabIndex = 14;
            this.lblMonthTo.Text = "月";
            // 
            // lblMonthFr
            // 
            this.lblMonthFr.AutoSize = true;
            this.lblMonthFr.BackColor = System.Drawing.Color.Silver;
            this.lblMonthFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMonthFr.Location = new System.Drawing.Point(147, 29);
            this.lblMonthFr.Name = "lblMonthFr";
            this.lblMonthFr.Size = new System.Drawing.Size(21, 13);
            this.lblMonthFr.TabIndex = 5;
            this.lblMonthFr.Text = "月";
            // 
            // lblYearTo
            // 
            this.lblYearTo.AutoSize = true;
            this.lblYearTo.BackColor = System.Drawing.Color.Silver;
            this.lblYearTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblYearTo.Location = new System.Drawing.Point(323, 29);
            this.lblYearTo.Name = "lblYearTo";
            this.lblYearTo.Size = new System.Drawing.Size(21, 13);
            this.lblYearTo.TabIndex = 12;
            this.lblYearTo.Text = "年";
            // 
            // txtDayTo
            // 
            this.txtDayTo.AutoSizeFromLength = false;
            this.txtDayTo.DisplayLength = null;
            this.txtDayTo.Enabled = false;
            this.txtDayTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDayTo.Location = new System.Drawing.Point(399, 25);
            this.txtDayTo.MaxLength = 2;
            this.txtDayTo.Name = "txtDayTo";
            this.txtDayTo.Size = new System.Drawing.Size(33, 20);
            this.txtDayTo.TabIndex = 15;
            this.txtDayTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDayTo.Visible = false;
            this.txtDayTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDayTo_Validating);
            // 
            // txtDayFr
            // 
            this.txtDayFr.AutoSizeFromLength = false;
            this.txtDayFr.DisplayLength = null;
            this.txtDayFr.Enabled = false;
            this.txtDayFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDayFr.Location = new System.Drawing.Point(168, 25);
            this.txtDayFr.MaxLength = 2;
            this.txtDayFr.Name = "txtDayFr";
            this.txtDayFr.Size = new System.Drawing.Size(33, 20);
            this.txtDayFr.TabIndex = 6;
            this.txtDayFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDayFr.Visible = false;
            this.txtDayFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDayFr_Validating);
            // 
            // lblYearFr
            // 
            this.lblYearFr.AutoSize = true;
            this.lblYearFr.BackColor = System.Drawing.Color.Silver;
            this.lblYearFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblYearFr.Location = new System.Drawing.Point(93, 29);
            this.lblYearFr.Name = "lblYearFr";
            this.lblYearFr.Size = new System.Drawing.Size(21, 13);
            this.lblYearFr.TabIndex = 3;
            this.lblYearFr.Text = "年";
            // 
            // txtMonthTo
            // 
            this.txtMonthTo.AutoSizeFromLength = false;
            this.txtMonthTo.DisplayLength = null;
            this.txtMonthTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMonthTo.Location = new System.Drawing.Point(344, 25);
            this.txtMonthTo.MaxLength = 2;
            this.txtMonthTo.Name = "txtMonthTo";
            this.txtMonthTo.Size = new System.Drawing.Size(33, 20);
            this.txtMonthTo.TabIndex = 13;
            this.txtMonthTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonthTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthTo_Validating);
            // 
            // txtMonthFr
            // 
            this.txtMonthFr.AutoSizeFromLength = false;
            this.txtMonthFr.DisplayLength = null;
            this.txtMonthFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMonthFr.Location = new System.Drawing.Point(114, 25);
            this.txtMonthFr.MaxLength = 2;
            this.txtMonthFr.Name = "txtMonthFr";
            this.txtMonthFr.Size = new System.Drawing.Size(33, 20);
            this.txtMonthFr.TabIndex = 4;
            this.txtMonthFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonthFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthFr_Validating);
            // 
            // txtYearTo
            // 
            this.txtYearTo.AutoSizeFromLength = false;
            this.txtYearTo.DisplayLength = null;
            this.txtYearTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtYearTo.Location = new System.Drawing.Point(290, 25);
            this.txtYearTo.MaxLength = 2;
            this.txtYearTo.Name = "txtYearTo";
            this.txtYearTo.Size = new System.Drawing.Size(33, 20);
            this.txtYearTo.TabIndex = 11;
            this.txtYearTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtYearTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtYearTo_Validating);
            // 
            // txtYearFr
            // 
            this.txtYearFr.AutoSizeFromLength = false;
            this.txtYearFr.DisplayLength = null;
            this.txtYearFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtYearFr.Location = new System.Drawing.Point(60, 25);
            this.txtYearFr.MaxLength = 2;
            this.txtYearFr.Name = "txtYearFr";
            this.txtYearFr.Size = new System.Drawing.Size(33, 20);
            this.txtYearFr.TabIndex = 2;
            this.txtYearFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtYearFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtYearFr_Validating);
            // 
            // lblGengoTo
            // 
            this.lblGengoTo.BackColor = System.Drawing.Color.Silver;
            this.lblGengoTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGengoTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGengoTo.Location = new System.Drawing.Point(244, 25);
            this.lblGengoTo.Name = "lblGengoTo";
            this.lblGengoTo.Size = new System.Drawing.Size(40, 20);
            this.lblGengoTo.TabIndex = 10;
            this.lblGengoTo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblGengoFr
            // 
            this.lblGengoFr.BackColor = System.Drawing.Color.Silver;
            this.lblGengoFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGengoFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGengoFr.Location = new System.Drawing.Point(14, 25);
            this.lblGengoFr.Name = "lblGengoFr";
            this.lblGengoFr.Size = new System.Drawing.Size(40, 20);
            this.lblGengoFr.TabIndex = 1;
            this.lblGengoFr.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblTo
            // 
            this.lblTo.BackColor = System.Drawing.Color.Silver;
            this.lblTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTo.Location = new System.Drawing.Point(241, 23);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(162, 24);
            this.lblTo.TabIndex = 9;
            this.lblTo.Text = " ";
            // 
            // lblFr
            // 
            this.lblFr.BackColor = System.Drawing.Color.Silver;
            this.lblFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFr.Location = new System.Drawing.Point(11, 23);
            this.lblFr.Name = "lblFr";
            this.lblFr.Size = new System.Drawing.Size(162, 24);
            this.lblFr.TabIndex = 0;
            this.lblFr.Text = " ";
            // 
            // gbxMizuageShisho
            // 
            this.gbxMizuageShisho.Controls.Add(this.txtMizuageShishoCd);
            this.gbxMizuageShisho.Controls.Add(this.lblMizuageShishoNm);
            this.gbxMizuageShisho.Controls.Add(this.lblMizuageShisho);
            this.gbxMizuageShisho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.gbxMizuageShisho.ForeColor = System.Drawing.Color.Black;
            this.gbxMizuageShisho.Location = new System.Drawing.Point(12, 49);
            this.gbxMizuageShisho.Name = "gbxMizuageShisho";
            this.gbxMizuageShisho.Size = new System.Drawing.Size(404, 59);
            this.gbxMizuageShisho.TabIndex = 902;
            this.gbxMizuageShisho.TabStop = false;
            this.gbxMizuageShisho.Text = "水揚支所";
            // 
            // txtMizuageShishoCd
            // 
            this.txtMizuageShishoCd.AutoSizeFromLength = true;
            this.txtMizuageShishoCd.DisplayLength = null;
            this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtMizuageShishoCd.Location = new System.Drawing.Point(110, 22);
            this.txtMizuageShishoCd.MaxLength = 4;
            this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
            this.txtMizuageShishoCd.Size = new System.Drawing.Size(51, 20);
            this.txtMizuageShishoCd.TabIndex = 0;
            this.txtMizuageShishoCd.TabStop = false;
            this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
            // 
            // lblMizuageShishoNm
            // 
            this.lblMizuageShishoNm.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMizuageShishoNm.Location = new System.Drawing.Point(166, 22);
            this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
            this.lblMizuageShishoNm.Size = new System.Drawing.Size(212, 20);
            this.lblMizuageShishoNm.TabIndex = 1;
            this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMizuageShisho
            // 
            this.lblMizuageShisho.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShisho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShisho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblMizuageShisho.Location = new System.Drawing.Point(27, 20);
            this.lblMizuageShisho.Name = "lblMizuageShisho";
            this.lblMizuageShisho.Size = new System.Drawing.Size(354, 25);
            this.lblMizuageShisho.TabIndex = 0;
            this.lblMizuageShisho.Text = "水揚支所";
            this.lblMizuageShisho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // gbxSeisanKubun
            // 
            this.gbxSeisanKubun.Controls.Add(this.lblSeisanKubun);
            this.gbxSeisanKubun.Controls.Add(this.txtSeisanKubun);
            this.gbxSeisanKubun.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.gbxSeisanKubun.ForeColor = System.Drawing.SystemColors.ControlText;
            this.gbxSeisanKubun.Location = new System.Drawing.Point(12, 271);
            this.gbxSeisanKubun.Name = "gbxSeisanKubun";
            this.gbxSeisanKubun.Size = new System.Drawing.Size(297, 67);
            this.gbxSeisanKubun.TabIndex = 5;
            this.gbxSeisanKubun.TabStop = false;
            this.gbxSeisanKubun.Text = "精算区分";
            // 
            // lblSeisanKubun
            // 
            this.lblSeisanKubun.BackColor = System.Drawing.Color.Silver;
            this.lblSeisanKubun.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSeisanKubun.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblSeisanKubun.Location = new System.Drawing.Point(60, 29);
            this.lblSeisanKubun.Name = "lblSeisanKubun";
            this.lblSeisanKubun.Size = new System.Drawing.Size(223, 20);
            this.lblSeisanKubun.TabIndex = 1;
            this.lblSeisanKubun.Text = "1:地区外 2:浜売り 3:地区内";
            this.lblSeisanKubun.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtSeisanKubun
            // 
            this.txtSeisanKubun.AutoSizeFromLength = false;
            this.txtSeisanKubun.DisplayLength = null;
            this.txtSeisanKubun.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtSeisanKubun.Location = new System.Drawing.Point(10, 29);
            this.txtSeisanKubun.MaxLength = 1;
            this.txtSeisanKubun.Name = "txtSeisanKubun";
            this.txtSeisanKubun.Size = new System.Drawing.Size(49, 20);
            this.txtSeisanKubun.TabIndex = 0;
            this.txtSeisanKubun.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSeisanKubun.Validating += new System.ComponentModel.CancelEventHandler(this.txtSeisanKubun_Validating);
            // 
            // HNMR1141
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 638);
            this.Controls.Add(this.gbxSeisanKubun);
            this.Controls.Add(this.gbxMizuageShisho);
            this.Controls.Add(this.gbxSearchRangeDate);
            this.Controls.Add(this.gbxCondition);
            this.Controls.Add(this.gbxOutputRank);
            this.Controls.Add(this.gbxOutput);
            this.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "HNMR1141";
            this.Text = "セリ推移表";
            this.Controls.SetChildIndex(this.gbxOutput, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.gbxOutputRank, 0);
            this.Controls.SetChildIndex(this.gbxCondition, 0);
            this.Controls.SetChildIndex(this.gbxSearchRangeDate, 0);
            this.Controls.SetChildIndex(this.gbxMizuageShisho, 0);
            this.Controls.SetChildIndex(this.gbxSeisanKubun, 0);
            this.pnlDebug.ResumeLayout(false);
            this.gbxOutput.ResumeLayout(false);
            this.gbxOutput.PerformLayout();
            this.gbxOutputRank.ResumeLayout(false);
            this.gbxOutputRank.PerformLayout();
            this.gbxCondition.ResumeLayout(false);
            this.gbxCondition.PerformLayout();
            this.gbxSearchRangeDate.ResumeLayout(false);
            this.gbxSearchRangeDate.PerformLayout();
            this.gbxMizuageShisho.ResumeLayout(false);
            this.gbxMizuageShisho.PerformLayout();
            this.gbxSeisanKubun.ResumeLayout(false);
            this.gbxSeisanKubun.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbxOutput;
        private jp.co.fsi.common.controls.FsiTextBox txtChikuCdTo;
        private jp.co.fsi.common.controls.FsiTextBox txtChikuCdFr;
        private System.Windows.Forms.Label lblCodeBet3;
        private System.Windows.Forms.GroupBox gbxOutputRank;
        private System.Windows.Forms.GroupBox gbxCondition;
        private jp.co.fsi.common.controls.FsiTextBox txtSenshuCdFr;
        private System.Windows.Forms.Label lblSenshuCd;
        private System.Windows.Forms.Label lblCodeBet1;
        private System.Windows.Forms.Label lblSenshuCdFr;
        private System.Windows.Forms.Label lblSenshuCdTo;
        private jp.co.fsi.common.controls.FsiTextBox txtSenshuCdTo;
        private System.Windows.Forms.Label lblCodeBet2;
        private System.Windows.Forms.Label lblGyohoCdFr;
        private jp.co.fsi.common.controls.FsiTextBox txtGyohoCdFr;
        private System.Windows.Forms.Label lblGyohoCd;
        private System.Windows.Forms.Label lblChikuCdTo;
        private System.Windows.Forms.Label lblChikuCdFr;
        private System.Windows.Forms.Label lblChikuCd;
        private System.Windows.Forms.Label lblGyohoCdTo;
        private jp.co.fsi.common.controls.FsiTextBox txtGyohoCdTo;
        private System.Windows.Forms.Label lblShukeihyoNm;
        private jp.co.fsi.common.controls.FsiTextBox txtShukeihyo;
        private System.Windows.Forms.Label lblShukeihyo;
        private System.Windows.Forms.Label lblJuni;
        private jp.co.fsi.common.controls.FsiTextBox txtJuni;
        private System.Windows.Forms.GroupBox gbxSearchRangeDate;
        private System.Windows.Forms.Label lblDayTo;
        private System.Windows.Forms.Label lblCodeBetDate;
        private System.Windows.Forms.Label lblDayFr;
        private System.Windows.Forms.Label lblMonthTo;
        private System.Windows.Forms.Label lblMonthFr;
        private System.Windows.Forms.Label lblYearTo;
        private jp.co.fsi.common.controls.FsiTextBox txtDayTo;
        private jp.co.fsi.common.controls.FsiTextBox txtDayFr;
        private System.Windows.Forms.Label lblYearFr;
        private jp.co.fsi.common.controls.FsiTextBox txtMonthTo;
        private jp.co.fsi.common.controls.FsiTextBox txtMonthFr;
        private jp.co.fsi.common.controls.FsiTextBox txtYearTo;
        private jp.co.fsi.common.controls.FsiTextBox txtYearFr;
        private System.Windows.Forms.Label lblGengoTo;
        private System.Windows.Forms.Label lblGengoFr;
        private System.Windows.Forms.Label lblTo;
        private System.Windows.Forms.Label lblFr;
        private System.Windows.Forms.Label lblGyoshuCdTo;
        private System.Windows.Forms.Label lblGyoshuCdFr;
        private System.Windows.Forms.Label lblGyoshuCd;
        private jp.co.fsi.common.controls.FsiTextBox txtGyoshuCdTo;
        private jp.co.fsi.common.controls.FsiTextBox txtGyoshuCdFr;
        private System.Windows.Forms.Label lblCodeBet6;
        private System.Windows.Forms.Label lblGyoshuBunruiCdTo;
        private System.Windows.Forms.Label lblGyoshuBunruiCdFr;
        private System.Windows.Forms.Label lblGyoshuBunruiCd;
        private jp.co.fsi.common.controls.FsiTextBox txtGyoshuBunruiCdTo;
        private jp.co.fsi.common.controls.FsiTextBox txtGyoshuBunruiCdFr;
        private System.Windows.Forms.Label lblCodeBet5;
        private System.Windows.Forms.GroupBox gbxMizuageShisho;
        private common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private System.Windows.Forms.Label lblMizuageShisho;
        private System.Windows.Forms.GroupBox gbxSeisanKubun;
        private System.Windows.Forms.Label lblSeisanKubun;
        private common.controls.FsiTextBox txtSeisanKubun;
    }
}