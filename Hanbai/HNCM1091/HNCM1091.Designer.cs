﻿namespace jp.co.fsi.hn.hncm1091
{
    partial class HNCM1091
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblKanaNm = new System.Windows.Forms.Label();
            this.txtKanaNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.dgvList = new System.Windows.Forms.DataGridView();
            this.btnEnter = new System.Windows.Forms.Button();
            this.pnlDebug.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvList)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Size = new System.Drawing.Size(788, 23);
            this.lblTitle.TabIndex = 999;
            this.lblTitle.Text = "手数料率の登録";
            // 
            // btnF1
            // 
            this.btnF1.Location = new System.Drawing.Point(69, 49);
            // 
            // btnF2
            // 
            this.btnF2.Location = new System.Drawing.Point(135, 49);
            // 
            // btnF3
            // 
            this.btnF3.Location = new System.Drawing.Point(201, 49);
            // 
            // btnF4
            // 
            this.btnF4.Location = new System.Drawing.Point(267, 49);
            this.btnF4.Text = "F4\r\n\r\n追加";
            // 
            // btnF5
            // 
            this.btnF5.Location = new System.Drawing.Point(333, 49);
            // 
            // btnF7
            // 
            this.btnF7.Location = new System.Drawing.Point(465, 49);
            // 
            // btnF6
            // 
            this.btnF6.Location = new System.Drawing.Point(399, 49);
            // 
            // btnF8
            // 
            this.btnF8.Location = new System.Drawing.Point(531, 49);
            // 
            // btnF9
            // 
            this.btnF9.Location = new System.Drawing.Point(597, 49);
            // 
            // btnF12
            // 
            this.btnF12.Location = new System.Drawing.Point(797, 49);
            this.btnF12.Visible = false;
            // 
            // btnF11
            // 
            this.btnF11.Location = new System.Drawing.Point(731, 49);
            // 
            // btnF10
            // 
            this.btnF10.Location = new System.Drawing.Point(664, 49);
            // 
            // pnlDebug
            // 
            this.pnlDebug.Controls.Add(this.btnEnter);
            this.pnlDebug.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.pnlDebug.Location = new System.Drawing.Point(5, 480);
            this.pnlDebug.Size = new System.Drawing.Size(880, 100);
            this.pnlDebug.Controls.SetChildIndex(this.btnF6, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF7, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF5, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF8, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF4, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF9, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF3, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF10, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF2, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF11, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF1, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF12, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnEsc, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnEnter, 0);
            // 
            // lblKanaNm
            // 
            this.lblKanaNm.BackColor = System.Drawing.Color.Silver;
            this.lblKanaNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKanaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKanaNm.Location = new System.Drawing.Point(12, 48);
            this.lblKanaNm.Name = "lblKanaNm";
            this.lblKanaNm.Size = new System.Drawing.Size(228, 25);
            this.lblKanaNm.TabIndex = 0;
            this.lblKanaNm.Text = "カ　ナ　名";
            this.lblKanaNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKanaNm
            // 
            this.txtKanaNm.AllowDrop = true;
            this.txtKanaNm.AutoSizeFromLength = false;
            this.txtKanaNm.DisplayLength = null;
            this.txtKanaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKanaNm.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtKanaNm.Location = new System.Drawing.Point(93, 50);
            this.txtKanaNm.MaxLength = 30;
            this.txtKanaNm.Name = "txtKanaNm";
            this.txtKanaNm.Size = new System.Drawing.Size(144, 20);
            this.txtKanaNm.TabIndex = 1;
            this.txtKanaNm.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtKanaName_KeyDown);
            // 
            // dgvList
            // 
            this.dgvList.AllowUserToAddRows = false;
            this.dgvList.AllowUserToDeleteRows = false;
            this.dgvList.AllowUserToResizeColumns = false;
            this.dgvList.AllowUserToResizeRows = false;
            this.dgvList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvList.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.dgvList.Location = new System.Drawing.Point(12, 90);
            this.dgvList.MultiSelect = false;
            this.dgvList.Name = "dgvList";
            this.dgvList.ReadOnly = true;
            this.dgvList.RowHeadersVisible = false;
            this.dgvList.RowTemplate.Height = 21;
            this.dgvList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvList.Size = new System.Drawing.Size(588, 296);
            this.dgvList.TabIndex = 2;
            this.dgvList.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvList_CellDoubleClick);
            this.dgvList.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvList_KeyDown);
            // 
            // btnEnter
            // 
            this.btnEnter.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnEnter.Location = new System.Drawing.Point(3, 49);
            this.btnEnter.Name = "btnEnter";
            this.btnEnter.Size = new System.Drawing.Size(65, 45);
            this.btnEnter.TabIndex = 907;
            this.btnEnter.TabStop = false;
            this.btnEnter.Text = "Enter\r\n\r\n変更";
            this.btnEnter.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnEnter.UseVisualStyleBackColor = true;
            this.btnEnter.Click += new System.EventHandler(this.btnEnter_Click);
            // 
            // HNCM1091
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(872, 583);
            this.Controls.Add(this.dgvList);
            this.Controls.Add(this.txtKanaNm);
            this.Controls.Add(this.lblKanaNm);
            this.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "HNCM1091";
            this.Par1 = "";
            this.Par2 = "";
            this.ShowFButton = true;
            this.Text = "ReportSample";
            this.Controls.SetChildIndex(this.lblKanaNm, 0);
            this.Controls.SetChildIndex(this.txtKanaNm, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.dgvList, 0);
            this.pnlDebug.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblKanaNm;
        private jp.co.fsi.common.controls.FsiTextBox txtKanaNm;
        private System.Windows.Forms.DataGridView dgvList;
        protected System.Windows.Forms.Button btnEnter;

    }
}