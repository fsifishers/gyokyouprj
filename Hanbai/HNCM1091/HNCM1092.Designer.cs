﻿namespace jp.co.fsi.hn.hncm1091
{
    partial class HNCM1092
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblSelectCd = new System.Windows.Forms.Label();
            this.txtCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKubun = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtTesuryoRitsu = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKanaNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblCd = new System.Windows.Forms.Label();
            this.lblKubun = new System.Windows.Forms.Label();
            this.lblTesuryoRitsu = new System.Windows.Forms.Label();
            this.lblKanaNm = new System.Windows.Forms.Label();
            this.lblNm = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Size = new System.Drawing.Size(376, 23);
            this.lblTitle.TabIndex = 999;
            this.lblTitle.Text = "";
            // 
            // btnF3
            // 
            this.btnF3.Text = "F3\r\n\r\nさくじょ";
            // 
            // btnF6
            // 
            this.btnF6.Text = "F6\r\n\r\nとうろく";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.pnlDebug.Location = new System.Drawing.Point(5, 147);
            this.pnlDebug.Size = new System.Drawing.Size(409, 100);
            // 
            // lblSelectCd
            // 
            this.lblSelectCd.BackColor = System.Drawing.Color.Silver;
            this.lblSelectCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSelectCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblSelectCd.Location = new System.Drawing.Point(176, 11);
            this.lblSelectCd.Name = "lblSelectCd";
            this.lblSelectCd.Size = new System.Drawing.Size(217, 25);
            this.lblSelectCd.TabIndex = 2;
            this.lblSelectCd.Text = "1:県内 2:県外 3:地元 4:パヤオ";
            this.lblSelectCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtCd
            // 
            this.txtCd.AllowDrop = true;
            this.txtCd.AutoSizeFromLength = false;
            this.txtCd.BackColor = System.Drawing.Color.White;
            this.txtCd.DisplayLength = null;
            this.txtCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtCd.Location = new System.Drawing.Point(115, 49);
            this.txtCd.MaxLength = 3;
            this.txtCd.Name = "txtCd";
            this.txtCd.Size = new System.Drawing.Size(58, 20);
            this.txtCd.TabIndex = 4;
            this.txtCd.Text = "0";
            this.txtCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtCd_Validating);
            // 
            // txtKubun
            // 
            this.txtKubun.AutoSizeFromLength = true;
            this.txtKubun.BackColor = System.Drawing.SystemColors.Window;
            this.txtKubun.DisplayLength = null;
            this.txtKubun.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKubun.Location = new System.Drawing.Point(115, 13);
            this.txtKubun.MaxLength = 1;
            this.txtKubun.Name = "txtKubun";
            this.txtKubun.Size = new System.Drawing.Size(58, 20);
            this.txtKubun.TabIndex = 1;
            this.txtKubun.Text = "1";
            this.txtKubun.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKubun.Validating += new System.ComponentModel.CancelEventHandler(this.txtKubun_Validating);
            // 
            // txtTesuryoRitsu
            // 
            this.txtTesuryoRitsu.AllowDrop = true;
            this.txtTesuryoRitsu.AutoSizeFromLength = false;
            this.txtTesuryoRitsu.BackColor = System.Drawing.Color.White;
            this.txtTesuryoRitsu.DisplayLength = null;
            this.txtTesuryoRitsu.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTesuryoRitsu.Location = new System.Drawing.Point(115, 164);
            this.txtTesuryoRitsu.MaxLength = 5;
            this.txtTesuryoRitsu.Name = "txtTesuryoRitsu";
            this.txtTesuryoRitsu.Size = new System.Drawing.Size(96, 20);
            this.txtTesuryoRitsu.TabIndex = 10;
            this.txtTesuryoRitsu.Text = "0.00";
            this.txtTesuryoRitsu.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTesuryoRitsu.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LastControl_KeyDown);
            this.txtTesuryoRitsu.Validating += new System.ComponentModel.CancelEventHandler(this.txtTesuryoRitsu_Validating);
            // 
            // txtKanaNm
            // 
            this.txtKanaNm.AllowDrop = true;
            this.txtKanaNm.AutoSizeFromLength = false;
            this.txtKanaNm.BackColor = System.Drawing.Color.White;
            this.txtKanaNm.DisplayLength = null;
            this.txtKanaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKanaNm.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtKanaNm.Location = new System.Drawing.Point(115, 128);
            this.txtKanaNm.MaxLength = 20;
            this.txtKanaNm.Name = "txtKanaNm";
            this.txtKanaNm.Size = new System.Drawing.Size(240, 20);
            this.txtKanaNm.TabIndex = 8;
            this.txtKanaNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtKanaNm_Validating);
            // 
            // txtNm
            // 
            this.txtNm.AllowDrop = true;
            this.txtNm.AutoSizeFromLength = false;
            this.txtNm.BackColor = System.Drawing.Color.White;
            this.txtNm.DisplayLength = null;
            this.txtNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtNm.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtNm.Location = new System.Drawing.Point(115, 90);
            this.txtNm.MaxLength = 20;
            this.txtNm.Name = "txtNm";
            this.txtNm.Size = new System.Drawing.Size(240, 20);
            this.txtNm.TabIndex = 6;
            this.txtNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtNm_Validating);
            // 
            // lblCd
            // 
            this.lblCd.BackColor = System.Drawing.Color.Silver;
            this.lblCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblCd.Location = new System.Drawing.Point(12, 47);
            this.lblCd.Name = "lblCd";
            this.lblCd.Size = new System.Drawing.Size(164, 25);
            this.lblCd.TabIndex = 3;
            this.lblCd.Text = "コ　　ー　　ド";
            this.lblCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKubun
            // 
            this.lblKubun.BackColor = System.Drawing.Color.Silver;
            this.lblKubun.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKubun.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKubun.Location = new System.Drawing.Point(12, 11);
            this.lblKubun.Name = "lblKubun";
            this.lblKubun.Size = new System.Drawing.Size(165, 25);
            this.lblKubun.TabIndex = 0;
            this.lblKubun.Text = "区　　　　　分";
            this.lblKubun.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTesuryoRitsu
            // 
            this.lblTesuryoRitsu.BackColor = System.Drawing.Color.Silver;
            this.lblTesuryoRitsu.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTesuryoRitsu.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTesuryoRitsu.Location = new System.Drawing.Point(12, 163);
            this.lblTesuryoRitsu.Name = "lblTesuryoRitsu";
            this.lblTesuryoRitsu.Size = new System.Drawing.Size(201, 25);
            this.lblTesuryoRitsu.TabIndex = 9;
            this.lblTesuryoRitsu.Text = "手　数　料　率";
            this.lblTesuryoRitsu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKanaNm
            // 
            this.lblKanaNm.BackColor = System.Drawing.Color.Silver;
            this.lblKanaNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKanaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKanaNm.Location = new System.Drawing.Point(12, 126);
            this.lblKanaNm.Name = "lblKanaNm";
            this.lblKanaNm.Size = new System.Drawing.Size(346, 25);
            this.lblKanaNm.TabIndex = 7;
            this.lblKanaNm.Text = "カ　　ナ　　名";
            this.lblKanaNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblNm
            // 
            this.lblNm.BackColor = System.Drawing.Color.Silver;
            this.lblNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblNm.Location = new System.Drawing.Point(12, 89);
            this.lblNm.Name = "lblNm";
            this.lblNm.Size = new System.Drawing.Size(346, 25);
            this.lblNm.TabIndex = 5;
            this.lblNm.Text = "名　　　　　称";
            this.lblNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // HNCM1092
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(401, 251);
            this.Controls.Add(this.txtNm);
            this.Controls.Add(this.lblSelectCd);
            this.Controls.Add(this.txtCd);
            this.Controls.Add(this.txtKubun);
            this.Controls.Add(this.lblKubun);
            this.Controls.Add(this.txtTesuryoRitsu);
            this.Controls.Add(this.lblNm);
            this.Controls.Add(this.txtKanaNm);
            this.Controls.Add(this.lblKanaNm);
            this.Controls.Add(this.lblTesuryoRitsu);
            this.Controls.Add(this.lblCd);
            this.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "HNCM1092";
            this.ShowFButton = true;
            this.Text = "手数料率の登録";
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.lblCd, 0);
            this.Controls.SetChildIndex(this.lblTesuryoRitsu, 0);
            this.Controls.SetChildIndex(this.lblKanaNm, 0);
            this.Controls.SetChildIndex(this.txtKanaNm, 0);
            this.Controls.SetChildIndex(this.lblNm, 0);
            this.Controls.SetChildIndex(this.txtTesuryoRitsu, 0);
            this.Controls.SetChildIndex(this.lblKubun, 0);
            this.Controls.SetChildIndex(this.txtKubun, 0);
            this.Controls.SetChildIndex(this.txtCd, 0);
            this.Controls.SetChildIndex(this.lblSelectCd, 0);
            this.Controls.SetChildIndex(this.txtNm, 0);
            this.pnlDebug.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblKanaNm;
        private System.Windows.Forms.Label lblNm;
        private System.Windows.Forms.Label lblCd;
        private System.Windows.Forms.Label lblKubun;
        private System.Windows.Forms.Label lblTesuryoRitsu;
        private jp.co.fsi.common.controls.FsiTextBox txtNm;
        private jp.co.fsi.common.controls.FsiTextBox txtTesuryoRitsu;
        private jp.co.fsi.common.controls.FsiTextBox txtKanaNm;
        private jp.co.fsi.common.controls.FsiTextBox txtCd;
        private jp.co.fsi.common.controls.FsiTextBox txtKubun;
        private System.Windows.Forms.Label lblSelectCd;

    }
}