﻿using System;
using System.ComponentModel;
using System.Data;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

using GrapeCity.ActiveReports;
using System.Collections;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.han.hanr3071
{
    /// <summary>
    /// 販売未払金元帳(HANR3071)
    /// </summary>
    public partial class HANR3071 : BasePgForm
    {
        #region プロパティ
        /// <summary>
        /// 画面上最後となるフォーカスのEnterボタン押下時処理用変数
        /// </summary>
        private bool _dtFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._dtFlg;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HANR3071()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region メソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 水揚支所
            this.txtMizuageShishoCd.Text = "1";
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_HN_COMBO_DATA_MIZUAGE", this.txtMizuageShishoCd.Text);

            string[] jpDate = Util.ConvJpDate(DateTime.Now, this.Dba);
            // 日付範囲
            lblDateGengoFr.Text = jpDate[0];
            txtDateYearFr.Text = jpDate[2];
            txtDateMonthFr.Text = jpDate[3];
            txtDateDayFr.Text = jpDate[4];
            lblDateGengoTo.Text = jpDate[0];
            txtDateYearTo.Text = jpDate[2];
            txtDateMonthTo.Text = jpDate[3];
            txtDateDayTo.Text = jpDate[4];

            rdoZeiKomi.Checked = true;

            // 初期フォーカス
            txtDateYearFr.Focus();

            // Enter処理を無効化
            this._dtFlg = false;
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 水揚支所,日付(年),船主CDにフォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd":
                case "txtDateYearFr":
                case "txtDateYearTo":
                case "txtFunanushiCdFr":
                case "txtFunanushiCdTo":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;
            String[] result;

            switch (this.ActiveCtlNm)
            {
                #region 水揚支所
                case "txtMizuageShishoCd": // 水揚支所
                    result = this.openSearchWindow("COMC8011", "TB_HN_COMBO_DATA_MIZUAGE", this.txtMizuageShishoCd.Text);
                    if (!ValChk.IsEmpty(result[0]))
                    {
                        this.txtMizuageShishoCd.Text = result[0];
                        this.lblMizuageShishoNm.Text = result[1];
                    }
                    break;
                #endregion

                case "txtDateYearFr":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom("COMC9011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.com.comc9011.COMC9011");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblDateGengoFr.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.lblDateGengoFr.Text = result[1];

                                // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
                                DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                                    this.txtDateMonthFr.Text, "1", this.Dba);
                                int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);
                                if (Util.ToInt(this.txtDateDayFr.Text) > lastDayInMonth)
                                {
                                    this.txtDateDayFr.Text = Util.ToString(lastDayInMonth);
                                }

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblDateGengoFr.Text,
                                        this.txtDateYearFr.Text,
                                        this.txtDateMonthFr.Text,
                                        this.txtDateDayFr.Text,
                                        this.Dba);
                                this.lblDateGengoFr.Text = arrJpDate[0];
                                this.txtDateYearFr.Text = arrJpDate[2];
                                this.txtDateMonthFr.Text = arrJpDate[3];
                                this.txtDateDayFr.Text = arrJpDate[4];
                            }
                        }
                    }
                    break;

                case "txtDateYearTo":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom("COMC9011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.com.comc9011.COMC9011");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblDateGengoTo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.lblDateGengoTo.Text = result[1];

                                // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
                                DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                                    this.txtDateMonthTo.Text, "1", this.Dba);
                                int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);
                                if (Util.ToInt(this.txtDateDayTo.Text) > lastDayInMonth)
                                {
                                    this.txtDateDayTo.Text = Util.ToString(lastDayInMonth);
                                }

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblDateGengoTo.Text,
                                        this.txtDateYearTo.Text,
                                        this.txtDateMonthTo.Text,
                                        this.txtDateDayTo.Text,
                                        this.Dba);
                                this.lblDateGengoTo.Text = arrJpDate[0];
                                this.txtDateYearTo.Text = arrJpDate[2];
                                this.txtDateMonthTo.Text = arrJpDate[3];
                                this.txtDateDayTo.Text = arrJpDate[4];
                            }
                        }
                    }
                    break;

                case "txtFunanushiCdFr":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom("KOBC9021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.kob.kobc9021.KOBC9021");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtFunanushiCdFr.Text = outData[0];
                                this.lblFunanushiCdFr.Text = outData[1];
                            }
                        }
                    }
                    break;

                case "txtFunanushiCdTo":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom("KOBC9021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.kob.kobc9021.KOBC9021");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtFunanushiCdTo.Text = outData[0];
                                this.lblFunanushiCdTo.Text = outData[1];
                            }
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
            {
                // ﾌﾟﾚﾋﾞｭｰ処理
                DoPrint(true);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("印刷", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }

        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            // 設定画面の起動
            // MEMO:原則としてここで渡す帳票IDの設定はReport.csvに保持していることが前提ですが、
            // 保持していない場合は、設定画面での保存(F6)時に新規に設定が保持されます。
            PrintSettingForm psForm = new PrintSettingForm(new string[1] { "HANR3071R" });
            psForm.ShowDialog();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 水揚支所入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMizuageShishoCd_Validating(object sender, CancelEventArgs e)
        {
            if (!this.IsValidMizuageShishoCd())
            {
                e.Cancel = true;

                this.lblMizuageShishoNm.Text = "";
                this.txtMizuageShishoCd.SelectAll();
                this.txtMizuageShishoCd.Focus();
            }
        }

        /// <summary>
        /// 和暦(年)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateYearFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidDateYearFr())
            {
                e.Cancel = true;
                this.txtDateYearFr.SelectAll();
            }
            else
            {
                CheckDateFr();
                SetDateFr();
            }
        }

        /// <summary>
        /// 和暦(月)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateMonthFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidDateMonthFr())
            {
                e.Cancel = true;
                this.txtDateMonthFr.SelectAll();
            }
            else
            {
                CheckDateFr();
                SetDateFr();
            }
        }

        /// <summary>
        /// 和暦(日)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateDayFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidDateDayFr())
            {
                e.Cancel = true;
                this.txtDateDayFr.SelectAll();
            }
            else
            {
                CheckDateFr();
                SetDateFr();
            }
        }

        /// <summary>
        /// 和暦(年)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateYearTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidDateYearTo())
            {
                e.Cancel = true;
                this.txtDateYearTo.SelectAll();
            }
            else
            {
                CheckDateTo();
                SetDateTo();
            }
        }

        /// <summary>
        /// 和暦(月)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateMonthTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidDateMonthTo())
            {
                e.Cancel = true;
                this.txtDateMonthTo.SelectAll();
            }
            else
            {
                CheckDateTo();
                SetDateTo();
            }
        }

        /// <summary>
        /// 和暦(日)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateDayTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidDateDayTo())
            {
                e.Cancel = true;
                this.txtDateDayTo.SelectAll();
            }
            else
            {
                CheckDateTo();
                SetDateTo();
            }
        }

        /// <summary>
        /// 共済加入絞込み入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKyosai_Validating(object sender, CancelEventArgs e)
        {
            if (!this.IsValidKyosai())
            {
                e.Cancel = true;

                this.txtKyosai.SelectAll();
                this.txtKyosai.Focus();
            }
        }

        /// <summary>
        /// 船主コード(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFunanushiCdFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidFunanushiCdFr())
            {
                e.Cancel = true;
                this.txtFunanushiCdFr.SelectAll();
            }
        }

        /// <summary>
        /// 船主コード(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFunanushiCdTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidFunanushiCdTo())
            {
                e.Cancel = true;
                this.txtFunanushiCdTo.SelectAll();

                // Enter処理を無効化
                this._dtFlg = false;
            }
            else
            {
                // Enter処理を有効化
                this._dtFlg = true;
            }
        }

        /// <summary>
        /// 船主コード(至)のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFunanushiCdTo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.Flg)
            {
                // Enter処理を無効化
                this._dtFlg = false;

                // 全項目を再度入力値チェック
                if (!ValidateAll())
                {
                    // エラーありの場合ここで処理終了
                    return;
                }

                if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
                {
                    // ﾌﾟﾚﾋﾞｭｰ処理
                    DoPrint(true);
                }
                else
                {
                    this.txtFunanushiCdTo.Focus();
                }
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 水揚支所の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool IsValidMizuageShishoCd()
        {
            // 空入力の場合
            if (ValChk.IsEmpty(this.txtMizuageShishoCd.Text))
            {
                // 水揚支所名称を表示する
                this.lblMizuageShishoNm.Text = "全て";
                return true;
            }

            // 最大桁数チェック
            if (!ValChk.IsWithinLength(this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.MaxLength))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            // 数値チェック
            if (!ValChk.IsNumber(this.txtMizuageShishoCd.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                return false;
            }

            // 0入力の場合
            if (Equals(this.txtMizuageShishoCd.Text, "0"))
            {
                // 水揚支所名称を表示する
                this.lblMizuageShishoNm.Text = "全て";
                return true;
            }

            // 水揚支所名称を表示する
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_HN_COMBO_DATA_MIZUAGE", this.txtMizuageShishoCd.Text);

            if (ValChk.IsEmpty(this.lblMizuageShishoNm.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 年(自)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidDateYearFr()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtDateYearFr.Text))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }
            // 空の場合、0年として処理
            if (ValChk.IsEmpty(this.txtDateYearFr.Text))
            {
                this.txtDateYearFr.Text = "0";
            }

            return true;
        }

        /// <summary>
        /// 月(自)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidDateMonthFr()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtDateMonthFr.Text))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }
            // 空の場合、1月として処理
            if (ValChk.IsEmpty(this.txtDateMonthFr.Text))
            {
                this.txtDateMonthFr.Text = "1";
            }
            else
            {
                // 12を超える月が入力された場合、12月として処理
                if (Util.ToInt(this.txtDateMonthFr.Text) > 12)
                {
                    this.txtDateMonthFr.Text = "12";
                }
                // 1より小さい月が入力された場合、1月として処理
                else if (Util.ToInt(this.txtDateMonthFr.Text) < 1)
                {
                    this.txtDateMonthFr.Text = "1";
                }
            }

            return true;
        }

        /// <summary>
        /// 日(自)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidDateDayFr()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtDateDayFr.Text))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }

            if (ValChk.IsEmpty(this.txtDateDayFr.Text))
            {
                // 空の場合、1日として処理
                this.txtDateDayFr.Text = "1";
            }
            else
            {
                // 1より小さい日が入力された場合、1日として処理
                if (Util.ToInt(this.txtDateDayFr.Text) < 1)
                {
                    this.txtDateDayFr.Text = "1";
                }
            }

            return true;
        }

        /// <summary>
        /// 年月日(自)の月末入力チェック
        /// </summary>
        /// 
        private void CheckDateFr()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                this.txtDateMonthFr.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDateDayFr.Text) > lastDayInMonth)
            {
                this.txtDateDayFr.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(自)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetDateFr()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDateFr(Util.FixJpDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                this.txtDateMonthFr.Text, this.txtDateDayFr.Text, this.Dba));
        }

        /// <summary>
        /// 年(至)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidDateYearTo()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtDateYearTo.Text))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }
            // 空の場合、0年として処理
            if (ValChk.IsEmpty(this.txtDateYearTo.Text))
            {
                this.txtDateYearTo.Text = "0";
            }

            return true;
        }

        /// <summary>
        /// 月(至)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidDateMonthTo()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtDateMonthTo.Text))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }
            // 空の場合、1月として処理
            if (ValChk.IsEmpty(this.txtDateMonthTo.Text))
            {
                this.txtDateMonthTo.Text = "1";
            }
            else
            {
                // 12を超える月が入力された場合、12月として処理
                if (Util.ToInt(this.txtDateMonthTo.Text) > 12)
                {
                    this.txtDateMonthTo.Text = "12";
                }
                // 1より小さい月が入力された場合、1月として処理
                else if (Util.ToInt(this.txtDateMonthTo.Text) < 1)
                {
                    this.txtDateMonthTo.Text = "1";
                }
            }

            return true;
        }

        /// <summary>
        /// 日(至)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidDateDayTo()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtDateDayTo.Text))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }

            if (ValChk.IsEmpty(this.txtDateDayTo.Text))
            {
                // 空の場合、1日として処理
                this.txtDateDayTo.Text = "1";
            }
            else
            {
                // 1より小さい日が入力された場合、1日として処理
                if (Util.ToInt(this.txtDateDayTo.Text) < 1)
                {
                    this.txtDateDayTo.Text = "1";
                }
            }

            return true;
        }

        /// <summary>
        /// 年月日(至)の月末入力チェック
        /// </summary>
        /// 
        private void CheckDateTo()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                this.txtDateMonthTo.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDateDayTo.Text) > lastDayInMonth)
            {
                this.txtDateDayTo.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(至)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetDateTo()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDateTo(Util.FixJpDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                this.txtDateMonthTo.Text, this.txtDateDayTo.Text, this.Dba));
        }

        /// <summary>
        /// 共済加入絞込みの値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool IsValidKyosai()
        {
            // 空入力の場合
            if (ValChk.IsEmpty(this.txtKyosai.Text))
            {
                this.txtKyosai.Text = "0";
                return true;
            }

            // 最大桁数チェック
            if (!ValChk.IsWithinLength(this.txtKyosai.Text, this.txtKyosai.MaxLength))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            // 数値チェック
            if (!ValChk.IsNumber(this.txtKyosai.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                return false;
            }

            // 0 又は 1以外が入力された場合
            if (!(Util.ToInt(this.txtKyosai.Text) >= 0 && Util.ToInt(this.txtKyosai.Text) <= 1))
            {
                Msg.Notice("0か1のみで入力してください。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 船主コード(自)の入力チェック
        /// </summary>
        private bool IsValidFunanushiCdFr()
        {
            if (ValChk.IsEmpty(this.txtFunanushiCdFr.Text))
            {
                this.lblFunanushiCdFr.Text = "先　頭";
            }
            else
                // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
                if (!ValChk.IsNumber(this.txtFunanushiCdFr.Text))
                {
                    Msg.Error("船主コードは数値のみで入力してください。");
                    return false;
                }
                else
                {
                    // コードを元に名称を取得する
                    this.lblFunanushiCdFr.Text = this.Dba.GetName(this.UInfo, "VI_HN_FUNANUSHI", this.txtFunanushiCdFr.Text);
                }

            return true;
        }

        /// <summary>
        /// 船主コード(至)の入力チェック
        /// </summary>
        private bool IsValidFunanushiCdTo()
        {
            if (ValChk.IsEmpty(this.txtFunanushiCdTo.Text))
            {
                this.lblFunanushiCdTo.Text = "最　後";
            }
            else
                // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
                if (!ValChk.IsNumber(this.txtFunanushiCdTo.Text))
                {
                    Msg.Error("船主コードは数値のみで入力してください。");
                    return false;
                }
                else
                {
                    // コードを元に名称を取得する
                    this.lblFunanushiCdTo.Text = this.Dba.GetName(this.UInfo, "VI_HN_FUNANUSHI", this.txtFunanushiCdTo.Text);
                }

            return true;
        }

        /// <summary>
        /// 日付範囲の入力チェック(会計年度)
        /// </summary>
        private bool CheckDate()
        {
            // 日付範囲を西暦にして取得
            DateTime tmpDateFr = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                    this.txtDateMonthFr.Text, this.txtDateDayFr.Text, this.Dba);
            DateTime tmpDateTo = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                    this.txtDateMonthTo.Text, this.txtDateDayTo.Text, this.Dba);
            // 日付範囲に該当する会計年度を取得
            ArrayList kaikeiNendoList = Util.GetKaikeiNendo(tmpDateFr, tmpDateTo, this.Dba);
            // 複数年度をまたいでいた場合、エラーとする
            if (kaikeiNendoList.Count > 1)
            {
                Msg.Error("会計年度をまたぐ日付範囲は設定できません。");
                return false;
            }
            else if (kaikeiNendoList.Count == 0)
            {
                Msg.Error("該当する会計年度がありません。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 水揚支所の入力チェック
            if (!IsValidMizuageShishoCd())
            {
                this.txtMizuageShishoCd.Focus();
                this.txtMizuageShishoCd.SelectAll();
                return false;
            }

            // 年(自)のチェック
            if (!IsValidDateYearFr())
            {
                this.txtDateYearFr.Focus();
                this.txtDateYearFr.SelectAll();
                return false;
            }
            // 月(自)のチェック
            if (!IsValidDateMonthFr())
            {
                this.txtDateMonthFr.Focus();
                this.txtDateMonthFr.SelectAll();
                return false;
            }
            // 日(自)のチェック
            if (!IsValidDateDayFr())
            {
                this.txtDateDayFr.Focus();
                this.txtDateDayFr.SelectAll();
                return false;
            }
            // 年月日(自)の月末入力チェック処理
            CheckDateFr();
            // 年月日(自)の正しい和暦への変換処理
            SetDateFr();

            // 年(至)のチェック
            if (!IsValidDateYearTo())
            {
                this.txtDateYearTo.Focus();
                this.txtDateYearTo.SelectAll();
                return false;
            }
            // 月(至)のチェック
            if (!IsValidDateMonthTo())
            {
                this.txtDateMonthTo.Focus();
                this.txtDateMonthTo.SelectAll();
                return false;
            }
            // 日(至)のチェック
            if (!IsValidDateDayTo())
            {
                this.txtDateDayTo.Focus();
                this.txtDateDayTo.SelectAll();
                return false;
            }
            // 年月日(至)の月末入力チェック処理
            CheckDateTo();
            // 年月日(至)の正しい和暦への変換処理
            SetDateTo();

            // 年度またぎチェック
            if (!CheckDate())
            {
                this.txtDateYearFr.Focus();
                this.txtDateYearFr.SelectAll();
                return false;
            }

            // 船主コード(自)の入力チェック
            if (!IsValidFunanushiCdFr())
            {
                this.txtFunanushiCdFr.Focus();
                this.txtFunanushiCdFr.SelectAll();
                return false;
            }
            // 船主コード(至)の入力チェック
            if (!IsValidFunanushiCdTo())
            {
                this.txtFunanushiCdTo.Focus();
                this.txtFunanushiCdTo.SelectAll();
                return false;
            }

            return true;
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpDateFr(string[] arrJpDate)
        {
            this.lblDateGengoFr.Text = arrJpDate[0];
            this.txtDateYearFr.Text = arrJpDate[2];
            this.txtDateMonthFr.Text = arrJpDate[3];
            this.txtDateDayFr.Text = arrJpDate[4];
        }
        private void SetJpDateTo(string[] arrJpDate)
        {
            this.lblDateGengoTo.Text = arrJpDate[0];
            this.txtDateYearTo.Text = arrJpDate[2];
            this.txtDateMonthTo.Text = arrJpDate[3];
            this.txtDateDayTo.Text = arrJpDate[4];
        }

        /// <summary>
        /// 検索サブウィンドウオープン
        /// </summary>
        /// <param name="moduleName">例："COMC8111"</param>
        /// <param name="para1">例："TB_HN_COMBO_DATA_MIZUAGE"</param>
        /// <param name="textBoxOfCode">例：this.txtGyogyoushuCd.Text</param>
        /// <param name="indata">例： this.txtGyogyoushuCd.Text</param>
        /// <returns>String[0]：検索結果から選択したコード , String[1]：検索結果から選択した名称</returns>
        private String[] openSearchWindow(String moduleName, String para1, String indata)
        {
            string[] result = { "", "" };

            // ネームスペースに使うモジュール名の小文字
            string lowerModuleName = moduleName.ToLower();

            // ネームスペースの末尾
            string nameSpace = lowerModuleName.Substring(0, 3);

            // アセンブリのロード
            Assembly asm = Assembly.LoadFrom(moduleName + ".exe");

            // フォーム作成
            string moduleNameSpace = "jp.co.fsi." + nameSpace + "." + lowerModuleName + "." + moduleName;
            Type t = asm.GetType(moduleNameSpace);

            if (t != null)
            {
                Object obj = Activator.CreateInstance(t);
                if (obj != null)
                {
                    BasePgForm frm = (BasePgForm)obj;
                    frm.Par1 = para1;
                    frm.InData = indata;
                    frm.ShowDialog(this);

                    if (frm.DialogResult == DialogResult.OK)
                    {
                        string[] ret = (string[])frm.OutData;
                        result[0] = ret[0];
                        result[1] = ret[1];
                        return result;
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview)
        {
            try
            {
                this.Dba.BeginTransaction();

                // 帳票出力用にワークテーブルにデータを作成
                bool dataFlag = MakeWkData();

                // 帳票出力
                if (dataFlag)
                {
                    // 取得列の定義
                    StringBuilder cols = new StringBuilder();
                    cols.Append("  ITEM01");
                    cols.Append(" ,ITEM02");
                    cols.Append(" ,ITEM03");
                    cols.Append(" ,ITEM04");
                    cols.Append(" ,ITEM05");
                    cols.Append(" ,ITEM06");
                    cols.Append(" ,ITEM07");
                    cols.Append(" ,ITEM08");
                    cols.Append(" ,ITEM09");
                    cols.Append(" ,ITEM10");
                    cols.Append(" ,ITEM11");
                    cols.Append(" ,ITEM12");
                    cols.Append(" ,ITEM13");
                    cols.Append(" ,ITEM14");
                    cols.Append(" ,ITEM15");
                    cols.Append(" ,ITEM16");
                    cols.Append(" ,ITEM17");
                    cols.Append(" ,ITEM18");

                    // バインドパラメータの設定
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);

                    // データの取得
                    DataTable dtOutput = this.Dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_HN_TBL", "GUID = @GUID", "SORT ASC", dpc);

                    // 帳票オブジェクトをインスタンス化
                    HANR3071R rpt = new HANR3071R(dtOutput);

                    if (isPreview)
                    {
                        // プレビュー画面表示
                        PreviewForm pFrm = new PreviewForm(rpt, this.UnqId);
                        pFrm.WindowState = FormWindowState.Maximized;
                        pFrm.Show();
                    }
                    else
                    {
                        // 直接印刷
                        rpt.Run(false);
                        rpt.Document.Print(true, true, false);
                    }
                }
            }
            finally
            {
                this.Dba.Rollback();
            }
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData()
        {
            // 日付範囲を西暦にして取得
            DateTime tmpDateFr = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                    this.txtDateMonthFr.Text, this.txtDateDayFr.Text, this.Dba);
            DateTime tmpDateTo = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                    this.txtDateMonthTo.Text, this.txtDateDayTo.Text, this.Dba);
            DateTime tmpDateTo2 = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                    this.txtDateMonthTo.Text, Util.ToString(DateTime.DaysInMonth(tmpDateTo.Year, Util.ToInt(this.txtDateMonthTo.Text))), this.Dba);

            // 日付範囲を和暦で保持
            string[] tmpjpDateFr = Util.ConvJpDate(tmpDateFr, this.Dba);
            string[] tmpjpDateTo = Util.ConvJpDate(tmpDateTo, this.Dba);
            string[] tmpjpDateTo2 = Util.ConvJpDate(tmpDateTo2, this.Dba);

            // 日付範囲に該当する会計年度を取得 ※入力チェックにて確認しているため、該当会計年度は1つの想定。
            ArrayList kaikeiNendoList = Util.GetKaikeiNendo(tmpDateFr, tmpDateTo, this.Dba);
            int kaikeiNendo = int.Parse(kaikeiNendoList[0].ToString());

            // 表示する日付を設定する
            string hyojiDate; // 表示用日付
            string hyojiDate2; // 表示用日付
            string dateFrChk = string.Format("{0}{1}{2}{3}", tmpjpDateFr[0], tmpjpDateFr[2], tmpjpDateFr[3], tmpjpDateFr[4]);
            string dateToChk = string.Format("{0}{1}{2}{3}", tmpjpDateTo[0], tmpjpDateTo[2], tmpjpDateTo[3], tmpjpDateTo[4]);
            string dateFrChk2 = string.Format("{0}{1}{2}{3}", tmpjpDateFr[0], tmpjpDateFr[2], tmpjpDateFr[3], "1");
            string dateToChk2 = string.Format("{0}{1}{2}{3}", tmpjpDateTo[0], tmpjpDateTo[2], tmpjpDateTo[3], tmpjpDateTo2[4]);
            if (dateFrChk == dateFrChk2 && dateToChk == dateToChk2)
            {
                hyojiDate = "(" + string.Format("{0}{1}年{2}月", tmpjpDateFr[0], tmpjpDateFr[2], tmpjpDateFr[3]) + "度"+")";
                hyojiDate2 = string.Format("{0}{1}年{2}月", tmpjpDateFr[0], tmpjpDateFr[2], tmpjpDateFr[3]) + "分";
            }
            else
            {
                hyojiDate = "(" + string.Format("{0}{1}年{2}月{3}日", tmpjpDateFr[0], tmpjpDateFr[2], tmpjpDateFr[3], tmpjpDateFr[4])
                          + "～"
                          + string.Format("{0}{1}年{2}月{3}日" + ")", tmpjpDateTo[0], tmpjpDateTo[2], tmpjpDateTo[3], tmpjpDateTo[4]);

                hyojiDate2 = string.Format("{0}{1}年{2}月", tmpjpDateFr[0], tmpjpDateFr[2], tmpjpDateFr[3], tmpjpDateFr[4])
                          + "～"
                          + string.Format("{0}{1}年{2}月", tmpjpDateTo[0], tmpjpDateTo[2], tmpjpDateTo[3], tmpjpDateTo[4])
                          + "分";
            }

            int i = 1; // ループ用カウント変数

            // 船主コード設定
            string funanushiCdFr;
            string funanushiCdTo;
            if (Util.ToString(txtFunanushiCdFr.Text) != "")
            {
                funanushiCdFr = txtFunanushiCdFr.Text;
            }
            else
            {
                funanushiCdFr = "0";
            }
            if (Util.ToString(txtFunanushiCdTo.Text) != "")
            {
                funanushiCdTo = txtFunanushiCdTo.Text;
            }
            else
            {
                funanushiCdTo = "9999";
            }

            #region データ取得
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder Sql = new StringBuilder();
            // 累計残高
            Sql.Append("SELECT");
            Sql.Append(" A.HOJO_KAMOKU_CD,");
            Sql.Append(" SUM(CASE WHEN A.TAISHAKU_KUBUN=1 THEN (ISNULL(A.ZEIKOMI_KINGAKU,0) * -1)");
            Sql.Append(" ELSE ISNULL(A.ZEIKOMI_KINGAKU,0) END) AS RUIKEI_ZANDAKA ");
            Sql.Append("FROM");
            Sql.Append(" TB_ZM_SHIWAKE_MEISAI AS A ");
            Sql.Append("LEFT OUTER JOIN");
            Sql.Append(" TB_HN_NYUKIN_KAMOKU AS B ");
            Sql.Append("ON");
            Sql.Append(" (A.KANJO_KAMOKU_CD = B.KANJO_KAMOKU_CD)");
            Sql.Append(" AND (A.KAISHA_CD = B.KAISHA_CD) ");
            Sql.Append("WHERE");
            Sql.Append(" A.KAIKEI_NENDO = @KAIKEI_NENDO");
            Sql.Append(" AND A.DENPYO_DATE < @DATE_FR");
            Sql.Append(" AND B.MOTOCHO_KUBUN = 2 ");
            Sql.Append("GROUP BY");
            Sql.Append(" A.HOJO_KAMOKU_CD,");
            Sql.Append(" B.MOTOCHO_KUBUN ");
            Sql.Append("HAVING");
            Sql.Append(" (((A.HOJO_KAMOKU_CD) BETWEEN @HOJO_KAMOKU_CD_FR AND @HOJO_KAMOKU_CD_TO))");

            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, kaikeiNendo);
            dpc.SetParam("@DATE_FR", SqlDbType.VarChar, 10, tmpDateFr.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@HOJO_KAMOKU_CD_FR", SqlDbType.VarChar, 4, funanushiCdFr);
            dpc.SetParam("@HOJO_KAMOKU_CD_TO", SqlDbType.VarChar, 4, funanushiCdTo);

            DataTable dtRuikeiZan = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
            #endregion

            #region 取得したデータをワークテーブルに更新をする
            foreach (DataRow dr in dtRuikeiZan.Rows)
            {
                Sql = new StringBuilder();
                dpc = new DbParamCollection();
                Sql.Append("INSERT INTO TW_HN_ZENGETSU_ZANDAKA(");
                Sql.Append("  GUID");
                Sql.Append(" ,SORT");
                Sql.Append(" ,HOJO_KAMOKU_CD");
                Sql.Append(" ,RUIKEI_ZANDAKA");
                Sql.Append(") ");
                Sql.Append("VALUES(");
                Sql.Append("  @GUID");
                Sql.Append(" ,@SORT");
                Sql.Append(" ,@HOJO_KAMOKU_CD");
                Sql.Append(" ,@RUIKEI_ZANDAKA");
                Sql.Append(") ");

                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                dpc.SetParam("@SORT", SqlDbType.VarChar, 4, i);
                dpc.SetParam("@HOJO_KAMOKU_CD", SqlDbType.Decimal, 4, dr["HOJO_KAMOKU_CD"]);
                dpc.SetParam("@RUIKEI_ZANDAKA", SqlDbType.Decimal, 38, dr["RUIKEI_ZANDAKA"]);

                this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                i++;
            }
            #endregion

            #region データ取得(貸方発生)
            i = 1;
            Sql = new StringBuilder();
            dpc = new DbParamCollection();
            Sql.Append("SELECT");
            Sql.Append(" SENSHU_CD,");
            Sql.Append(" SERIBI AS HASSEIBI,");
            Sql.Append(" ISNULL(MIZUAGE_GOKEI_SURYO,0) AS HASSEI_SURYO,");
            Sql.Append(" ISNULL(MIZUAGE_ZEIKOMI_KINGAKU,0) AS TOGETSU_KASHIKATA_HASSEI,");
            Sql.Append(" ISNULL(MIZUAGE_SHOHIZEIGAKU,0) AS UCHI_SHOHIZEIGAKU ");
            Sql.Append("FROM");
            Sql.Append(" TB_HN_SHIKIRI_DATA ");
            Sql.Append("WHERE");
            Sql.Append(" KAIKEI_NENDO = @KAIKEI_NENDO AND");
            Sql.Append(" SENSHU_CD  BETWEEN @SENSHU_CD_FR AND @SENSHU_CD_TO AND");
            Sql.Append(" SERIBI  BETWEEN @DATE_FR AND @DATE_TO AND");
            Sql.Append(" SEISAN_KUBUN IN (1,2,3)");

            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, kaikeiNendo);
            dpc.SetParam("@DATE_FR", SqlDbType.VarChar, 10, tmpDateFr.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@DATE_TO", SqlDbType.VarChar, 10, tmpDateTo.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@SENSHU_CD_FR", SqlDbType.VarChar, 4, funanushiCdFr);
            dpc.SetParam("@SENSHU_CD_TO", SqlDbType.VarChar, 4, funanushiCdTo);

            DataTable dtKashikata = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
            #endregion

            #region 取得したデータをワークテーブルに更新をする
            foreach (DataRow dr in dtKashikata.Rows)
            {
                Sql = new StringBuilder();
                dpc = new DbParamCollection();
                Sql.Append("INSERT INTO TW_HN_KASHIKATA_HASSEI(");
                Sql.Append("  GUID");
                Sql.Append(" ,SORT");
                Sql.Append(" ,SENSHU_CD");
                Sql.Append(" ,HASSEIBI");
                Sql.Append(" ,HASSEI_SURYO");
                Sql.Append(" ,TOGETSU_KASHIKATA_HASSEI");
                Sql.Append(" ,UCHI_SHOHIZEIGAKU");
                Sql.Append(") ");
                Sql.Append("VALUES(");
                Sql.Append("  @GUID");
                Sql.Append(" ,@SORT");
                Sql.Append(" ,@SENSHU_CD");
                Sql.Append(" ,@HASSEIBI");
                Sql.Append(" ,@HASSEI_SURYO");
                Sql.Append(" ,@TOGETSU_KASHIKATA_HASSEI");
                Sql.Append(" ,@UCHI_SHOHIZEIGAKU");
                Sql.Append(") ");

                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                dpc.SetParam("@SORT", SqlDbType.VarChar, 4, i);
                dpc.SetParam("@SENSHU_CD", SqlDbType.Decimal, 5, dr["SENSHU_CD"]);
                dpc.SetParam("@HASSEIBI", SqlDbType.DateTime, dr["HASSEIBI"]);
                dpc.SetParam("@HASSEI_SURYO", SqlDbType.Decimal, 9,2, dr["HASSEI_SURYO"]);
                dpc.SetParam("@TOGETSU_KASHIKATA_HASSEI", SqlDbType.Decimal, 9, dr["TOGETSU_KASHIKATA_HASSEI"]);
                dpc.SetParam("@UCHI_SHOHIZEIGAKU", SqlDbType.Decimal, 9, dr["UCHI_SHOHIZEIGAKU"]);

                this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                i++;
            }
            #endregion

            #region データ取得(借方発生)
            i = 1;
            Sql = new StringBuilder();
            dpc = new DbParamCollection();
            Sql.Append("SELECT");
            Sql.Append(" A.HOJO_KAMOKU_CD AS SENSHU_CD,");
            Sql.Append(" A.DENPYO_DATE AS KARIKATA_HASSEIBI,");
            Sql.Append(" ISNULL(A.ZEIKOMI_KINGAKU,0) AS TOGETSU_KARIKATA_HASSEI,");
            Sql.Append(" ISNULL(A.SHOHIZEI_KINGAKU,0) AS TOGETSU_KARIKATA_SHOHIZEI_KNGK ");
            Sql.Append("FROM");
            Sql.Append(" TB_ZM_SHIWAKE_MEISAI AS A ");
            Sql.Append("LEFT OUTER JOIN");
            Sql.Append(" TB_HN_NYUKIN_KAMOKU AS B ");
            Sql.Append("ON");
            Sql.Append(" (A.KANJO_KAMOKU_CD = B.KANJO_KAMOKU_CD)");
            Sql.Append(" AND (A.KAISHA_CD = B.KAISHA_CD) ");
            Sql.Append("LEFT OUTER JOIN");
            Sql.Append(" TB_HN_TORIHIKISAKI_JOHO AS C ");
            Sql.Append("ON");
            Sql.Append(" (A.HOJO_KAMOKU_CD = C.TORIHIKISAKI_CD)");
            Sql.Append(" AND (A.KAISHA_CD = C.KAISHA_CD) ");
            Sql.Append("WHERE");
            Sql.Append(" A.KAIKEI_NENDO = @KAIKEI_NENDO");
            Sql.Append(" AND A.TAISHAKU_KUBUN = 1");
            Sql.Append(" AND A.DENPYO_KUBUN = 1");
            Sql.Append(" AND B.MOTOCHO_KUBUN = 2");
            Sql.Append(" AND A.HOJO_KAMOKU_CD  BETWEEN @HOJO_KAMOKU_CD_FR AND @HOJO_KAMOKU_CD_TO");
            Sql.Append(" AND A.DENPYO_DATE  BETWEEN @DATE_FR AND @DATE_TO");
            Sql.Append(" AND C.TORIHIKISAKI_KUBUN1 = 1");

            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, kaikeiNendo);
            dpc.SetParam("@DATE_FR", SqlDbType.VarChar, 10, tmpDateFr.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@DATE_TO", SqlDbType.VarChar, 10, tmpDateTo.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@HOJO_KAMOKU_CD_FR", SqlDbType.VarChar, 4, funanushiCdFr);
            dpc.SetParam("@HOJO_KAMOKU_CD_TO", SqlDbType.VarChar, 4, funanushiCdTo);

            DataTable dtKarikata = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
            #endregion

            #region 取得したデータをワークテーブルに更新をする
            foreach (DataRow dr in dtKarikata.Rows)
            {
                Sql = new StringBuilder();
                dpc = new DbParamCollection();
                Sql.Append("INSERT INTO TW_HN_KARIKATA_HASSEI(");
                Sql.Append("  GUID");
                Sql.Append(" ,SORT");
                Sql.Append(" ,SENSHU_CD");
                Sql.Append(" ,KARIKATA_HASSEIBI");
                Sql.Append(" ,TOGETSU_KARIKATA_HASSEI");
                Sql.Append(" ,TOGETSU_KARIKATA_SHOHIZEI_KNGK");
                Sql.Append(") ");
                Sql.Append("VALUES(");
                Sql.Append("  @GUID");
                Sql.Append(" ,@SORT");
                Sql.Append(" ,@SENSHU_CD");
                Sql.Append(" ,@KARIKATA_HASSEIBI");
                Sql.Append(" ,@TOGETSU_KARIKATA_HASSEI");
                Sql.Append(" ,@TOGETSU_KARIKATA_SHOHIZEI_KNGK");
                Sql.Append(") ");

                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                dpc.SetParam("@SORT", SqlDbType.VarChar, 4, i);
                dpc.SetParam("@SENSHU_CD", SqlDbType.Decimal, 4, dr["SENSHU_CD"]);
                dpc.SetParam("@KARIKATA_HASSEIBI", SqlDbType.DateTime, dr["KARIKATA_HASSEIBI"]);
                dpc.SetParam("@TOGETSU_KARIKATA_HASSEI", SqlDbType.Decimal, 15, dr["TOGETSU_KARIKATA_HASSEI"]);
                dpc.SetParam("@TOGETSU_KARIKATA_SHOHIZEI_KNGK", SqlDbType.Decimal, 15, dr["TOGETSU_KARIKATA_SHOHIZEI_KNGK"]);

                this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                i++;
            }
            #endregion

            #region データ取得(販売未払金)
            i = 1;
            Sql = new StringBuilder();
            dpc = new DbParamCollection();
            Sql.Append("SELECT");
            Sql.Append(" SENSHU_CD AS SENSHU_CD,");
            Sql.Append(" KARIKATA_HASSEIBI AS KARIKATA_HASSEIBI,");
            Sql.Append(" 0 AS HASSEI_SURYO,");
            Sql.Append(" TOGETSU_KARIKATA_HASSEI AS TOGETSU_KARIKATA_HASSEI,");
            Sql.Append(" 0 AS TOGETSU_KASHIKATA_HASSEI,");
            Sql.Append(" TOGETSU_KARIKATA_SHOHIZEI_KNGK AS UCHI_SHOHIZEIGAKU ");
            Sql.Append("FROM");
            Sql.Append(" TW_HN_KARIKATA_HASSEI ");
            Sql.Append("WHERE");
            Sql.Append(" GUID = @GUID ");
            Sql.Append("UNION ALL ");
            Sql.Append("SELECT");
            Sql.Append(" SENSHU_CD,");
            Sql.Append(" HASSEIBI,");
            Sql.Append(" HASSEI_SURYO,");
            Sql.Append(" 0 AS TOGETSU_KARIKATA_HASSEI,");
            Sql.Append(" TOGETSU_KASHIKATA_HASSEI AS TOGETSU_KASHIKATA_HASSEI,");
            Sql.Append(" UCHI_SHOHIZEIGAKU ");
            Sql.Append("FROM");
            Sql.Append(" TW_HN_KASHIKATA_HASSEI ");
            Sql.Append("WHERE");
            Sql.Append(" GUID = @GUID");

            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);

            DataTable dtHanbaiMibaraikin = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
            #endregion

            #region 取得したデータをワークテーブルに更新をする
            foreach (DataRow dr in dtHanbaiMibaraikin.Rows)
            {
                Sql = new StringBuilder();
                dpc = new DbParamCollection();
                Sql.Append("INSERT INTO TW_HN_HANBAI_MIBARAIKIN(");
                Sql.Append("  GUID");
                Sql.Append(" ,SORT");
                Sql.Append(" ,SENSHU_CD");
                Sql.Append(" ,KARIKATA_HASSEIBI");
                Sql.Append(" ,SURYO");
                Sql.Append(" ,TOGETSU_KARIKATA_HASSEI");
                Sql.Append(" ,TOGETSU_KASHIKATA_HASSEI");
                Sql.Append(" ,UCHI_SHOHIZEIGAKU");
                Sql.Append(") ");
                Sql.Append("VALUES(");
                Sql.Append("  @GUID");
                Sql.Append(" ,@SORT");
                Sql.Append(" ,@SENSHU_CD");
                Sql.Append(" ,@KARIKATA_HASSEIBI");
                Sql.Append(" ,@SURYO");
                Sql.Append(" ,@TOGETSU_KARIKATA_HASSEI");
                Sql.Append(" ,@TOGETSU_KASHIKATA_HASSEI");
                Sql.Append(" ,@UCHI_SHOHIZEIGAKU");
                Sql.Append(") ");

                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                dpc.SetParam("@SORT", SqlDbType.VarChar, 4, i);
                dpc.SetParam("@SENSHU_CD", SqlDbType.Decimal, 5, dr["SENSHU_CD"]);
                dpc.SetParam("@KARIKATA_HASSEIBI", SqlDbType.DateTime, dr["KARIKATA_HASSEIBI"]);
                dpc.SetParam("@SURYO", SqlDbType.Decimal, 9,2, dr["HASSEI_SURYO"]);
                dpc.SetParam("@TOGETSU_KARIKATA_HASSEI", SqlDbType.Decimal, 15, dr["TOGETSU_KARIKATA_HASSEI"]);
                dpc.SetParam("@TOGETSU_KASHIKATA_HASSEI", SqlDbType.Decimal, 9, dr["TOGETSU_KASHIKATA_HASSEI"]);
                dpc.SetParam("@UCHI_SHOHIZEIGAKU", SqlDbType.Decimal, 15, dr["UCHI_SHOHIZEIGAKU"]);

                this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                i++;
            }
            #endregion

            #region 最終的なデータを取得
            i = 1;
            Sql = new StringBuilder();
            dpc = new DbParamCollection();
            Sql.Append("SELECT");
            Sql.Append(" year(B.KARIKATA_HASSEIBI) AS HASSEI_NEN,");
            Sql.Append(" month(B.KARIKATA_HASSEIBI) AS HASSEI_GETSU,");
            Sql.Append(" (year(B.KARIKATA_HASSEIBI) * 100 + month(B.KARIKATA_HASSEIBI))  AS HASSEI_NENGETSU,");
            Sql.Append(" B.SENSHU_CD AS SENSHU_CD,");
            Sql.Append(" MAX(C.TORIHIKISAKI_NM) AS SENSHU_NM,");
            Sql.Append(" B.KARIKATA_HASSEIBI AS HASSEIBI,");
            Sql.Append(" SUM(B.SURYO) AS HASSEI_SURYO,");
            Sql.Append(" MIN(ISNULL(A.RUIKEI_ZANDAKA,0)) AS ZENGETSU_ZAN,");
            Sql.Append(" SUM(B.TOGETSU_KARIKATA_HASSEI) AS TOGETSU_KARIKATA_HASSEI,");
            Sql.Append(" SUM(B.TOGETSU_KASHIKATA_HASSEI) AS TOGETSU_KASHIKATA_HASSEI,");
            Sql.Append(" SUM(B.UCHI_SHOHIZEIGAKU) AS UCHI_SHOHIZEIGAKU ");
            Sql.Append("FROM");
            Sql.Append(" TW_HN_ZENGETSU_ZANDAKA AS A ");
            Sql.Append("RIGHT OUTER JOIN");
            Sql.Append(" TW_HN_HANBAI_MIBARAIKIN AS B ");
            Sql.Append("ON");
            Sql.Append(" A.HOJO_KAMOKU_CD = B.SENSHU_CD ");
            Sql.Append("LEFT OUTER JOIN");
            Sql.Append(" TB_CM_TORIHIKISAKI AS C ");
            Sql.Append("ON");
            Sql.Append(" B.SENSHU_CD = C.TORIHIKISAKI_CD ");
            Sql.Append("GROUP BY");
            Sql.Append(" year(B.KARIKATA_HASSEIBI),");
            Sql.Append(" month(B.KARIKATA_HASSEIBI),");
            Sql.Append(" (year(B.KARIKATA_HASSEIBI) * 100 + month(B.KARIKATA_HASSEIBI)),");
            Sql.Append(" B.SENSHU_CD,");
            Sql.Append(" B.KARIKATA_HASSEIBI ORDER BY B.SENSHU_CD,B.KARIKATA_HASSEIBI");

            DataTable dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
            #endregion

            if (dtMainLoop.Rows.Count == 0)
            {
                Msg.Info("該当データがありません。");
                return false;
            }
            else
            {

                Decimal zengetsuZan = 0;
                Decimal zandaka = 0;
                Decimal RkariKingaku = 0;
                Decimal RkashiKingaku = 0;
                Decimal Rshohizei = 0;
                Decimal Rsuryo = 0;
                string funanushiCd = "";

                foreach (DataRow dr in dtMainLoop.Rows)
                {
                    if (funanushiCd == "" || !funanushiCd.Equals(Util.ToString(dr["SENSHU_CD"])))
                    {
                        zengetsuZan = Util.ToDecimal(dr["ZENGETSU_ZAN"]);
                        RkariKingaku = 0;
                        RkashiKingaku = 0;
                        Rshohizei = 0;
                        Rsuryo = 0;

                        // 累計　当月借方発生
                        Sql = new StringBuilder();
                        dpc = new DbParamCollection();
                        Sql.Append("SELECT");
                        Sql.Append(" B.HOJO_KAMOKU_CD AS TORIHIKISAKI_CD,");
                        Sql.Append(" B.ZEIKOMI_KINGAKU AS ZEIKOMI_KINGAKU ");
                        Sql.Append("FROM");
                        Sql.Append(" TB_HN_NYUKIN_KAMOKU AS A ");
                        Sql.Append("LEFT OUTER JOIN");
                        Sql.Append(" TB_ZM_SHIWAKE_MEISAI AS B ");
                        Sql.Append("ON");
                        Sql.Append(" A.KAISHA_CD = B.KAISHA_CD AND");
                        Sql.Append(" 1 = B.DENPYO_KUBUN AND");
                        Sql.Append(" A.KANJO_KAMOKU_CD = B.KANJO_KAMOKU_CD ");
                        Sql.Append("LEFT OUTER JOIN");
                        Sql.Append(" TB_ZM_KANJO_KAMOKU AS C ");
                        Sql.Append("ON");
                        Sql.Append(" A.KAISHA_CD = C.KAISHA_CD AND");
                        Sql.Append(" A.KANJO_KAMOKU_CD = C.KANJO_KAMOKU_CD AND");
                        Sql.Append(" B.KAIKEI_NENDO = C.KAIKEI_NENDO ");
                        Sql.Append("WHERE");
                        Sql.Append(" B.KAIKEI_NENDO = @KAIKEI_NENDO AND");
                        Sql.Append(" A.KAISHA_CD = @KAISHA_CD AND");
                        Sql.Append(" B.HOJO_KAMOKU_CD = @HOJO_KAMOKU_CD AND");
                        Sql.Append(" B.DENPYO_DATE Between @DATE_FR AND @DATE_TO AND");
                        Sql.Append(" B.TAISHAKU_KUBUN <> C.TAISHAKU_KUBUN AND");
                        Sql.Append(" A.MOTOCHO_KUBUN = 2");

                        dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                        dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, kaikeiNendo);
                        dpc.SetParam("@DATE_FR", SqlDbType.VarChar, 10, tmpDateFr.Date.ToString("yyyy/04/01"));
                        dpc.SetParam("@DATE_TO", SqlDbType.VarChar, 10, tmpDateTo.Date.ToString("yyyy/MM/dd"));
                        dpc.SetParam("@HOJO_KAMOKU_CD", SqlDbType.Decimal, 5, dr["SENSHU_CD"]);

                        DataTable dtRKarikata = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

                        // 累計　借方金額の計算
                        foreach (DataRow dr2 in dtRKarikata.Rows)
                        {
                            RkariKingaku += Util.ToDecimal(dr2["ZEIKOMI_KINGAKU"]);
                        }

                        // 累計　数量、当月貸方、消費税
                        Sql = new StringBuilder();
                        dpc = new DbParamCollection();
                        Sql.Append("SELECT");
                        Sql.Append(" A.DENPYO_BANGO AS DENPYO_BANGO,");
                        Sql.Append(" A.SEISAN_BI AS SEISAN_BI,");
                        Sql.Append(" A.SENSHU_CD AS TORIHIKISAKI_CD,");
                        Sql.Append(" A.ZEI_RITSU AS ZEI_RITSU,");
                        Sql.Append(" A.SHOHIZEI_NYURYOKU_HOHO AS SHOHIZEI_NYURYOKU_HOHO,");
                        Sql.Append(" SUM(A.SURYO) AS MIZUAGE_SURYO,");
                        Sql.Append(" SUM(A.URIAGE_KINGAKU) AS URIAGE_KINGAKU,");
                        Sql.Append(" SUM(A.TESURYO) AS TESURYO,");
                        Sql.Append(" SUM(A.SHOHINDAI) AS SHOHINDAI,");
                        Sql.Append(" MIN(MIZUAGE_SHOHIZEIGAKU) AS SHOHIZEI,");
                        Sql.Append(" SUM(A.URIAGE_KINGAKU) AS ZEINUKI_KINGAKU,");
                        Sql.Append(" SUM(A.SHOHIZEI) AS URIAGE_SHOHIZEI ");
                        Sql.Append("FROM");
                        Sql.Append(" VI_HN_SERI_MOTOCHO AS A ");
                        Sql.Append("WHERE");
                        Sql.Append(" A.KAISHA_CD = @KAISHA_CD AND");
                        Sql.Append(" A.TORIHIKI_KUBUN1 = 1 AND");
                        Sql.Append(" A.SEISAN_BI Between @DATE_FR AND @DATE_TO AND");
                        Sql.Append(" (A.SENSHU_CD = @SENSHU_CD) ");
                        Sql.Append("GROUP BY");
                        Sql.Append(" A.DENPYO_BANGO,");
                        Sql.Append(" A.SEISAN_BI,");
                        Sql.Append(" A.SENSHU_CD,");
                        Sql.Append(" A.ZEI_RITSU,");
                        Sql.Append(" A.SHOHIZEI_NYURYOKU_HOHO ");
                        Sql.Append("ORDER BY");
                        Sql.Append(" A.SENSHU_CD");

                        dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                        dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, kaikeiNendo);
                        dpc.SetParam("@DATE_FR", SqlDbType.VarChar, 10, tmpDateFr.Date.ToString("yyyy/04/01"));
                        dpc.SetParam("@DATE_TO", SqlDbType.VarChar, 10, tmpDateTo.Date.ToString("yyyy/MM/dd"));
                        dpc.SetParam("@SENSHU_CD", SqlDbType.Decimal, 5, dr["SENSHU_CD"]);

                        DataTable dtRKashikata = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

                        // 累計　数量、消費税、貸方金額の計算
                        foreach (DataRow dr2 in dtRKashikata.Rows)
                        {
                            Rsuryo += Util.ToDecimal(dr2["MIZUAGE_SURYO"]);
                            Rshohizei += Util.ToDecimal(dr2["SHOHIZEI"]);
                            RkashiKingaku += Util.ToDecimal(dr2["ZEINUKI_KINGAKU"]) + Util.ToDecimal(dr2["SHOHIZEI"]);
                        }
                    }

                    #region 取得したデータをワークテーブルに更新をする
                    Sql = new StringBuilder();
                    dpc = new DbParamCollection();
                    Sql.Append("INSERT INTO PR_HN_TBL(");
                    Sql.Append("  GUID");
                    Sql.Append(" ,SORT");
                    Sql.Append(" ,ITEM01");
                    Sql.Append(" ,ITEM02");
                    Sql.Append(" ,ITEM03");
                    Sql.Append(" ,ITEM04");
                    Sql.Append(" ,ITEM05");
                    Sql.Append(" ,ITEM06");
                    Sql.Append(" ,ITEM07");
                    Sql.Append(" ,ITEM08");
                    Sql.Append(" ,ITEM09");
                    Sql.Append(" ,ITEM10");
                    Sql.Append(" ,ITEM11");
                    Sql.Append(" ,ITEM12");
                    Sql.Append(" ,ITEM13");
                    Sql.Append(" ,ITEM14");
                    Sql.Append(" ,ITEM15");
                    Sql.Append(" ,ITEM16");
                    Sql.Append(" ,ITEM17");
                    Sql.Append(" ,ITEM18");
                    Sql.Append(") ");
                    Sql.Append("VALUES(");
                    Sql.Append("  @GUID");
                    Sql.Append(" ,@SORT");
                    Sql.Append(" ,@ITEM01");
                    Sql.Append(" ,@ITEM02");
                    Sql.Append(" ,@ITEM03");
                    Sql.Append(" ,@ITEM04");
                    Sql.Append(" ,@ITEM05");
                    Sql.Append(" ,@ITEM06");
                    Sql.Append(" ,@ITEM07");
                    Sql.Append(" ,@ITEM08");
                    Sql.Append(" ,@ITEM09");
                    Sql.Append(" ,@ITEM10");
                    Sql.Append(" ,@ITEM11");
                    Sql.Append(" ,@ITEM12");
                    Sql.Append(" ,@ITEM13");
                    Sql.Append(" ,@ITEM14");
                    Sql.Append(" ,@ITEM15");
                    Sql.Append(" ,@ITEM16");
                    Sql.Append(" ,@ITEM17");
                    Sql.Append(" ,@ITEM18");
                    Sql.Append(") ");

                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, i);
                    // ページヘッダーデータを設定
                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);
                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, hyojiDate2);
                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, hyojiDate);
                    // データを設定
                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dr["SENSHU_CD"]);
                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, dr["SENSHU_NM"]);
                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, zengetsuZan);
                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, dr["HASSEIBI"]);
                    dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, Util.FormatNum(dr["HASSEI_SURYO"], 1));
                    dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, Util.FormatNum(dr["TOGETSU_KARIKATA_HASSEI"]));
                    dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, Util.FormatNum(dr["TOGETSU_KASHIKATA_HASSEI"]));
                    // 残高の計算
                    if (!funanushiCd.Equals(Util.ToString(dr["SENSHU_CD"])))
                    {
                        zandaka = Util.ToDecimal(dr["TOGETSU_KASHIKATA_HASSEI"]);
                    }
                    else
                    {
                        zandaka = zandaka - Util.ToDecimal(dr["TOGETSU_KARIKATA_HASSEI"]) + Util.ToDecimal(dr["TOGETSU_KASHIKATA_HASSEI"]);
                    }
                    dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, Util.FormatNum(zandaka));
                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, Util.FormatNum(dr["UCHI_SHOHIZEIGAKU"]));
                    dpc.SetParam("@ITEM13", SqlDbType.VarChar, 200, Util.FormatNum(Rsuryo, 1));
                    dpc.SetParam("@ITEM14", SqlDbType.VarChar, 200, "");
                    dpc.SetParam("@ITEM15", SqlDbType.VarChar, 200, Util.FormatNum(RkariKingaku));
                    dpc.SetParam("@ITEM16", SqlDbType.VarChar, 200, Util.FormatNum(RkashiKingaku));
                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, "");
                    dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, Util.FormatNum(Rshohizei));

                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                    #endregion

                    // 船主コードの保持
                    funanushiCd = Util.ToString(dr["SENSHU_CD"]);
                    // 累計数量、前月残のリセット
                    Rsuryo = 0;
                    RkariKingaku = 0;
                    RkashiKingaku = 0;
                    Rshohizei = 0;
                    zengetsuZan = 0;

                    i++;
                }
            }

            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
                "SORT",
                "PR_HN_TBL",
                "GUID = @GUID",
                dpc);

            bool dataFlag;
            if (tmpdtPR_HN_TBL.Rows.Count > 0)
            {
                dataFlag = true;
            }
            else
            {
                dataFlag = false;
            }

            return dataFlag;
        }
        #endregion
    }
}
