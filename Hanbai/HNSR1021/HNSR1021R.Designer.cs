﻿namespace jp.co.fsi.hn.hnsr1021
{
    /// <summary>
    /// HNSR1021R の概要の説明です。
    /// </summary>
    partial class HNSR1021R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(HNSR1021R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.ラベル0 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.セリ日 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.直線55 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.date = new GrapeCity.ActiveReports.SectionReportModel.ReportInfo();
            this.page = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ghShishoCd = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.伝票番号 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.山NO = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.魚種CD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.魚種名称 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.水揚数量Kｇ = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.単価 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.仲買人CD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.仲買人名称 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.船主CD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.船主名称 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.漁法CD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.漁法名称 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.gfShishoCd = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.groupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.groupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.textBox8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.groupHeader2 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.groupFooter2 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.ラベル50 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.テキスト56 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト32 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル34 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.直線48 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.textBox6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.セリ日)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.date)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.page)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.伝票番号)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.山NO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.魚種CD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.魚種名称)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.水揚数量Kｇ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.単価)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.仲買人CD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.仲買人名称)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.船主CD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.船主名称)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.漁法CD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.漁法名称)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト56)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.ラベル0,
            this.セリ日,
            this.ラベル1,
            this.ラベル3,
            this.ラベル4,
            this.ラベル5,
            this.ラベル6,
            this.ラベル7,
            this.ラベル8,
            this.ラベル9,
            this.ラベル11,
            this.ラベル13,
            this.ラベル14,
            this.ラベル16,
            this.ラベル17,
            this.直線55,
            this.date,
            this.page,
            this.label2,
            this.label3,
            this.textBox4,
            this.textBox5,
            this.label4,
            this.label5,
            this.label6});
            this.pageHeader.Height = 0.8534285F;
            this.pageHeader.Name = "pageHeader";
            // 
            // ラベル0
            // 
            this.ラベル0.Height = 0.2291667F;
            this.ラベル0.HyperLink = null;
            this.ラベル0.Left = 4.562475F;
            this.ラベル0.Name = "ラベル0";
            this.ラベル0.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 15.75pt; font-weight: bold; text-ali" +
    "gn: left; ddo-char-set: 128";
            this.ラベル0.Tag = "";
            this.ラベル0.Text = "仕切チェックリスト";
            this.ラベル0.Top = 0F;
            this.ラベル0.Width = 2.1563F;
            // 
            // セリ日
            // 
            this.セリ日.DataField = "ITEM01";
            this.セリ日.Height = 0.157874F;
            this.セリ日.Left = 0.9078741F;
            this.セリ日.MultiLine = false;
            this.セリ日.Name = "セリ日";
            this.セリ日.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12.75pt; fo" +
    "nt-weight: bold; text-align: left; ddo-char-set: 128";
            this.セリ日.Tag = "";
            this.セリ日.Text = "ITEM01";
            this.セリ日.Top = 0.2811024F;
            this.セリ日.Width = 1.168329F;
            // 
            // ラベル1
            // 
            this.ラベル1.Height = 0.215748F;
            this.ラベル1.HyperLink = null;
            this.ラベル1.Left = 0F;
            this.ラベル1.Name = "ラベル1";
            this.ラベル1.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12.75pt; font-weight: bold; text-ali" +
    "gn: left; ddo-char-set: 128";
            this.ラベル1.Tag = "";
            this.ラベル1.Text = "セリ日";
            this.ラベル1.Top = 0.2905512F;
            this.ラベル1.Width = 0.8204725F;
            // 
            // ラベル3
            // 
            this.ラベル3.Height = 0.2291338F;
            this.ラベル3.HyperLink = null;
            this.ラベル3.Left = 0.2515748F;
            this.ラベル3.Name = "ラベル3";
            this.ラベル3.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12.75pt; font-weight: bold; text-ali" +
    "gn: left; ddo-char-set: 128";
            this.ラベル3.Tag = "";
            this.ラベル3.Text = "伝票番号";
            this.ラベル3.Top = 0.5062993F;
            this.ラベル3.Width = 0.8374017F;
            // 
            // ラベル4
            // 
            this.ラベル4.Height = 0.1999508F;
            this.ラベル4.HyperLink = null;
            this.ラベル4.Left = 1.088977F;
            this.ラベル4.Name = "ラベル4";
            this.ラベル4.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12.75pt; font-weight: bold; text-ali" +
    "gn: left; ddo-char-set: 128";
            this.ラベル4.Tag = "";
            this.ラベル4.Text = "山NO";
            this.ラベル4.Top = 0.5062993F;
            this.ラベル4.Width = 0.5637795F;
            // 
            // ラベル5
            // 
            this.ラベル5.Height = 0.2291338F;
            this.ラベル5.HyperLink = null;
            this.ラベル5.Left = 1.652756F;
            this.ラベル5.Name = "ラベル5";
            this.ラベル5.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12.75pt; font-weight: bold; text-ali" +
    "gn: left; ddo-char-set: 128";
            this.ラベル5.Tag = "";
            this.ラベル5.Text = "船主CD";
            this.ラベル5.Top = 0.5062993F;
            this.ラベル5.Width = 0.6263781F;
            // 
            // ラベル6
            // 
            this.ラベル6.Height = 0.2291338F;
            this.ラベル6.HyperLink = null;
            this.ラベル6.Left = 2.279134F;
            this.ラベル6.Name = "ラベル6";
            this.ラベル6.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12.75pt; font-weight: bold; text-ali" +
    "gn: left; ddo-char-set: 128";
            this.ラベル6.Tag = "";
            this.ラベル6.Text = "船主名称";
            this.ラベル6.Top = 0.5062993F;
            this.ラベル6.Width = 0.9897637F;
            // 
            // ラベル7
            // 
            this.ラベル7.Height = 0.2F;
            this.ラベル7.HyperLink = null;
            this.ラベル7.Left = 3.343701F;
            this.ラベル7.Name = "ラベル7";
            this.ラベル7.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12.75pt; font-weight: bold; text-ali" +
    "gn: left; ddo-char-set: 128";
            this.ラベル7.Tag = "";
            this.ラベル7.Text = "漁法CD";
            this.ラベル7.Top = 0.5062993F;
            this.ラベル7.Width = 0.6263778F;
            // 
            // ラベル8
            // 
            this.ラベル8.Height = 0.2811024F;
            this.ラベル8.HyperLink = null;
            this.ラベル8.Left = 3.970079F;
            this.ラベル8.Name = "ラベル8";
            this.ラベル8.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12.75pt; font-weight: bold; text-ali" +
    "gn: left; ddo-char-set: 128";
            this.ラベル8.Tag = "";
            this.ラベル8.Text = "漁法名称";
            this.ラベル8.Top = 0.5062993F;
            this.ラベル8.Width = 0.8748031F;
            // 
            // ラベル9
            // 
            this.ラベル9.Height = 0.2291338F;
            this.ラベル9.HyperLink = null;
            this.ラベル9.Left = 5.084646F;
            this.ラベル9.Name = "ラベル9";
            this.ラベル9.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12.75pt; font-weight: bold; text-ali" +
    "gn: left; ddo-char-set: 128";
            this.ラベル9.Tag = "";
            this.ラベル9.Text = "魚種CD";
            this.ラベル9.Top = 0.5062993F;
            this.ラベル9.Width = 0.7811027F;
            // 
            // ラベル11
            // 
            this.ラベル11.Height = 0.3019685F;
            this.ラベル11.HyperLink = null;
            this.ラベル11.Left = 5.81378F;
            this.ラベル11.Name = "ラベル11";
            this.ラベル11.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12.75pt; font-weight: bold; text-ali" +
    "gn: left; ddo-char-set: 128";
            this.ラベル11.Tag = "";
            this.ラベル11.Text = "魚種名称";
            this.ラベル11.Top = 0.5062993F;
            this.ラベル11.Width = 0.9051185F;
            // 
            // ラベル13
            // 
            this.ラベル13.Height = 0.2811024F;
            this.ラベル13.HyperLink = null;
            this.ラベル13.Left = 8.611812F;
            this.ラベル13.Name = "ラベル13";
            this.ラベル13.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12.75pt; font-weight: bold; text-ali" +
    "gn: center; ddo-char-set: 128";
            this.ラベル13.Tag = "";
            this.ラベル13.Text = "水揚数量(Kg)";
            this.ラベル13.Top = 0.5062993F;
            this.ラベル13.Width = 1.279528F;
            // 
            // ラベル14
            // 
            this.ラベル14.Height = 0.2291338F;
            this.ラベル14.HyperLink = null;
            this.ラベル14.Left = 10.01536F;
            this.ラベル14.Name = "ラベル14";
            this.ラベル14.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12.75pt; font-weight: bold; text-ali" +
    "gn: right; ddo-char-set: 128";
            this.ラベル14.Tag = "";
            this.ラベル14.Text = "単価";
            this.ラベル14.Top = 0.5062993F;
            this.ラベル14.Width = 0.6405516F;
            // 
            // ラベル16
            // 
            this.ラベル16.Height = 0.2291338F;
            this.ラベル16.HyperLink = null;
            this.ラベル16.Left = 10.71496F;
            this.ラベル16.Name = "ラベル16";
            this.ラベル16.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12.75pt; font-weight: bold; text-ali" +
    "gn: center; ddo-char-set: 128";
            this.ラベル16.Tag = "";
            this.ラベル16.Text = "仲買人CD";
            this.ラベル16.Top = 0.5062993F;
            this.ラベル16.Width = 1.069291F;
            // 
            // ラベル17
            // 
            this.ラベル17.Height = 0.2810531F;
            this.ラベル17.HyperLink = null;
            this.ラベル17.Left = 11.78425F;
            this.ラベル17.Name = "ラベル17";
            this.ラベル17.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12.75pt; font-weight: bold; text-ali" +
    "gn: left; ddo-char-set: 128";
            this.ラベル17.Tag = "";
            this.ラベル17.Text = "仲買人名称";
            this.ラベル17.Top = 0.5062993F;
            this.ラベル17.Width = 1.797703F;
            // 
            // 直線55
            // 
            this.直線55.Height = 5.960464E-08F;
            this.直線55.Left = 0.01023622F;
            this.直線55.LineWeight = 3F;
            this.直線55.Name = "直線55";
            this.直線55.Tag = "";
            this.直線55.Top = 0.7874016F;
            this.直線55.Width = 13.23819F;
            this.直線55.X1 = 0.01023622F;
            this.直線55.X2 = 13.24843F;
            this.直線55.Y1 = 0.7874016F;
            this.直線55.Y2 = 0.7874017F;
            // 
            // date
            // 
            this.date.FormatString = "{RunDateTime:yyyy/MM/dd}";
            this.date.Height = 0.1562664F;
            this.date.Left = 11.00984F;
            this.date.Name = "date";
            this.date.Style = "font-family: ＭＳ 明朝; font-size: 12pt";
            this.date.Top = 0.07283465F;
            this.date.Width = 1.230167F;
            // 
            // page
            // 
            this.page.Height = 0.1562664F;
            this.page.Left = 12.24016F;
            this.page.Name = "page";
            this.page.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: right";
            this.page.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.page.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.page.Text = "textBox1";
            this.page.Top = 0.07283465F;
            this.page.Width = 0.5102358F;
            // 
            // label2
            // 
            this.label2.Height = 0.2291338F;
            this.label2.HyperLink = null;
            this.label2.Left = 6.718898F;
            this.label2.Name = "label2";
            this.label2.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12.75pt; font-weight: bold; text-ali" +
    "gn: center; ddo-char-set: 128";
            this.label2.Tag = "";
            this.label2.Text = "率";
            this.label2.Top = 0.5062993F;
            this.label2.Width = 0.5405509F;
            // 
            // label3
            // 
            this.label3.Height = 0.2291338F;
            this.label3.HyperLink = null;
            this.label3.Left = 7.925985F;
            this.label3.Name = "label3";
            this.label3.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12.75pt; font-weight: bold; text-ali" +
    "gn: right; ddo-char-set: 128";
            this.label3.Tag = "";
            this.label3.Text = "箱代";
            this.label3.Top = 0.5062993F;
            this.label3.Width = 0.5992131F;
            // 
            // textBox4
            // 
            this.textBox4.DataField = "ITEM18";
            this.textBox4.Height = 0.157874F;
            this.textBox4.Left = 0.9078741F;
            this.textBox4.MultiLine = false;
            this.textBox4.Name = "textBox4";
            this.textBox4.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12.75pt; fo" +
    "nt-weight: bold; text-align: right; ddo-char-set: 128";
            this.textBox4.Tag = "";
            this.textBox4.Text = "ITEM18";
            this.textBox4.Top = 0.07125985F;
            this.textBox4.Width = 0.4476378F;
            // 
            // textBox5
            // 
            this.textBox5.DataField = "ITEM19";
            this.textBox5.Height = 0.157874F;
            this.textBox5.Left = 1.451574F;
            this.textBox5.MultiLine = false;
            this.textBox5.Name = "textBox5";
            this.textBox5.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12.75pt; fo" +
    "nt-weight: bold; text-align: left; ddo-char-set: 128";
            this.textBox5.Tag = "";
            this.textBox5.Text = "ITEM19";
            this.textBox5.Top = 0.07125985F;
            this.textBox5.Width = 2.510237F;
            // 
            // label4
            // 
            this.label4.Height = 0.2810777F;
            this.label4.HyperLink = null;
            this.label4.Left = 0F;
            this.label4.Name = "label4";
            this.label4.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12.75pt; font-weight: bold; text-ali" +
    "gn: left; ddo-char-set: 128";
            this.label4.Tag = "";
            this.label4.Text = "支　所";
            this.label4.Top = 0.07207187F;
            this.label4.Width = 0.8204725F;
            // 
            // label5
            // 
            this.label5.Height = 0.2F;
            this.label5.HyperLink = null;
            this.label5.Left = 7.259449F;
            this.label5.Name = "label5";
            this.label5.Style = "font-family: ＭＳ 明朝; font-size: 12.75pt; font-weight: bold; text-align: right";
            this.label5.Text = "パヤオ";
            this.label5.Top = 0.5062993F;
            this.label5.Width = 0.6665354F;
            // 
            // label6
            // 
            this.label6.Height = 0.2177166F;
            this.label6.HyperLink = null;
            this.label6.Left = 12.81417F;
            this.label6.Name = "label6";
            this.label6.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-weight: normal; text-alig" +
    "n: left; ddo-char-set: 128";
            this.label6.Tag = "";
            this.label6.Text = "頁";
            this.label6.Top = 0.07283465F;
            this.label6.Width = 0.3070859F;
            // 
            // ghShishoCd
            // 
            this.ghShishoCd.CanGrow = false;
            this.ghShishoCd.DataField = "ITEM18";
            this.ghShishoCd.Height = 0F;
            this.ghShishoCd.Name = "ghShishoCd";
            this.ghShishoCd.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
            this.ghShishoCd.UnderlayNext = true;
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.伝票番号,
            this.山NO,
            this.魚種CD,
            this.魚種名称,
            this.水揚数量Kｇ,
            this.単価,
            this.仲買人CD,
            this.仲買人名称,
            this.テキスト5,
            this.textBox2,
            this.textBox3,
            this.textBox7,
            this.textBox9});
            this.detail.Height = 0.2713419F;
            this.detail.Name = "detail";
            // 
            // 伝票番号
            // 
            this.伝票番号.DataField = "ITEM02";
            this.伝票番号.Height = 0.15625F;
            this.伝票番号.Left = 0.2515748F;
            this.伝票番号.Name = "伝票番号";
            this.伝票番号.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: center; ddo-char-set: 128";
            this.伝票番号.Tag = "";
            this.伝票番号.Text = "ITEM02";
            this.伝票番号.Top = 0.02125984F;
            this.伝票番号.Width = 0.8374017F;
            // 
            // 山NO
            // 
            this.山NO.DataField = "ITEM03";
            this.山NO.Height = 0.15625F;
            this.山NO.Left = 0.9927489F;
            this.山NO.Name = "山NO";
            this.山NO.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: center; ddo-char-set: 128";
            this.山NO.Tag = "";
            this.山NO.Text = "ITEM03";
            this.山NO.Top = 0.02124348F;
            this.山NO.Width = 0.5637795F;
            // 
            // 魚種CD
            // 
            this.魚種CD.DataField = "ITEM08";
            this.魚種CD.Height = 0.15625F;
            this.魚種CD.Left = 5.158662F;
            this.魚種CD.Name = "魚種CD";
            this.魚種CD.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 128";
            this.魚種CD.Tag = "";
            this.魚種CD.Text = "ITEM08";
            this.魚種CD.Top = 0.04212593F;
            this.魚種CD.Width = 0.4318895F;
            // 
            // 魚種名称
            // 
            this.魚種名称.DataField = "ITEM09";
            this.魚種名称.Height = 0.15625F;
            this.魚種名称.Left = 5.81378F;
            this.魚種名称.MultiLine = false;
            this.魚種名称.Name = "魚種名称";
            this.魚種名称.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: left; ddo-char-set: 128";
            this.魚種名称.Tag = "";
            this.魚種名称.Text = "ITEM09";
            this.魚種名称.Top = 0.04212593F;
            this.魚種名称.Width = 1.327953F;
            // 
            // 水揚数量Kｇ
            // 
            this.水揚数量Kｇ.DataField = "ITEM11";
            this.水揚数量Kｇ.Height = 0.15625F;
            this.水揚数量Kｇ.Left = 8.611812F;
            this.水揚数量Kｇ.Name = "水揚数量Kｇ";
            this.水揚数量Kｇ.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 128";
            this.水揚数量Kｇ.Tag = "";
            this.水揚数量Kｇ.Text = "ITEM11";
            this.水揚数量Kｇ.Top = 0.04212599F;
            this.水揚数量Kｇ.Width = 0.8803501F;
            // 
            // 単価
            // 
            this.単価.DataField = "ITEM12";
            this.単価.Height = 0.15625F;
            this.単価.Left = 9.57856F;
            this.単価.Name = "単価";
            this.単価.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 128";
            this.単価.Tag = "";
            this.単価.Text = "ITEM12";
            this.単価.Top = 0.04212593F;
            this.単価.Width = 1.077347F;
            // 
            // 仲買人CD
            // 
            this.仲買人CD.DataField = "ITEM14";
            this.仲買人CD.Height = 0.15625F;
            this.仲買人CD.Left = 10.71496F;
            this.仲買人CD.Name = "仲買人CD";
            this.仲買人CD.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: center; ddo-char-set: 128";
            this.仲買人CD.Tag = "";
            this.仲買人CD.Text = "ITEM14";
            this.仲買人CD.Top = 0.04212593F;
            this.仲買人CD.Width = 1.069291F;
            // 
            // 仲買人名称
            // 
            this.仲買人名称.DataField = "ITEM15";
            this.仲買人名称.Height = 0.15625F;
            this.仲買人名称.Left = 11.78425F;
            this.仲買人名称.MultiLine = false;
            this.仲買人名称.Name = "仲買人名称";
            this.仲買人名称.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: left; ddo-char-set: 128";
            this.仲買人名称.Tag = "";
            this.仲買人名称.Text = "ITEM15";
            this.仲買人名称.Top = 0.04212593F;
            this.仲買人名称.Width = 1.578739F;
            // 
            // テキスト5
            // 
            this.テキスト5.DataField = "ITEM17";
            this.テキスト5.Height = 0.15625F;
            this.テキスト5.Left = 12.24016F;
            this.テキスト5.Name = "テキスト5";
            this.テキスト5.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: left; ddo-char-set: 128";
            this.テキスト5.Tag = "";
            this.テキスト5.Text = "ITEM17";
            this.テキスト5.Top = 0.02125984F;
            this.テキスト5.Visible = false;
            this.テキスト5.Width = 0.5874999F;
            // 
            // textBox2
            // 
            this.textBox2.DataField = "ITEM16";
            this.textBox2.Height = 0.15625F;
            this.textBox2.Left = 6.718898F;
            this.textBox2.Name = "textBox2";
            this.textBox2.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: center; ddo-char-set: 128";
            this.textBox2.Tag = "";
            this.textBox2.Text = "ITEM16";
            this.textBox2.Top = 0.04212593F;
            this.textBox2.Width = 0.5405509F;
            // 
            // textBox3
            // 
            this.textBox3.DataField = "ITEM10";
            this.textBox3.Height = 0.15625F;
            this.textBox3.Left = 8.057482F;
            this.textBox3.Name = "textBox3";
            this.textBox3.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 128";
            this.textBox3.Tag = "";
            this.textBox3.Text = "ITEM10";
            this.textBox3.Top = 0.04212593F;
            this.textBox3.Width = 0.4677166F;
            // 
            // textBox7
            // 
            this.textBox7.DataField = "ITEM20";
            this.textBox7.Height = 0.15625F;
            this.textBox7.Left = 7.385434F;
            this.textBox7.Name = "textBox7";
            this.textBox7.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: center; ddo-char-set: 128";
            this.textBox7.Tag = "";
            this.textBox7.Text = null;
            this.textBox7.Top = 0.04212593F;
            this.textBox7.Width = 0.5405509F;
            // 
            // textBox9
            // 
            this.textBox9.DataField = "ITEM13";
            this.textBox9.Height = 0.15625F;
            this.textBox9.Left = 3.877559F;
            this.textBox9.Name = "textBox9";
            this.textBox9.OutputFormat = resources.GetString("textBox9.OutputFormat");
            this.textBox9.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 128";
            this.textBox9.Tag = "";
            this.textBox9.Text = "[ITEM13]";
            this.textBox9.Top = 0.04212599F;
            this.textBox9.Visible = false;
            this.textBox9.Width = 0.7645674F;
            // 
            // 船主CD
            // 
            this.船主CD.DataField = "ITEM04";
            this.船主CD.Height = 0.15625F;
            this.船主CD.Left = 1.652756F;
            this.船主CD.Name = "船主CD";
            this.船主CD.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: center; ddo-char-set: 128";
            this.船主CD.Tag = "";
            this.船主CD.Text = "ITEM04";
            this.船主CD.Top = 0F;
            this.船主CD.Width = 0.6263781F;
            // 
            // 船主名称
            // 
            this.船主名称.DataField = "ITEM05";
            this.船主名称.Height = 0.15625F;
            this.船主名称.Left = 2.353422F;
            this.船主名称.MultiLine = false;
            this.船主名称.Name = "船主名称";
            this.船主名称.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: left; ddo-char-set: 128";
            this.船主名称.Tag = "";
            this.船主名称.Text = "ITEM05";
            this.船主名称.Top = 1.636147E-05F;
            this.船主名称.Width = 1.066536F;
            // 
            // 漁法CD
            // 
            this.漁法CD.DataField = "ITEM06";
            this.漁法CD.Height = 0.15625F;
            this.漁法CD.Left = 3.343701F;
            this.漁法CD.Name = "漁法CD";
            this.漁法CD.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: center; ddo-char-set: 128";
            this.漁法CD.Tag = "";
            this.漁法CD.Text = "ITEM06";
            this.漁法CD.Top = 0F;
            this.漁法CD.Width = 0.6263778F;
            // 
            // 漁法名称
            // 
            this.漁法名称.DataField = "ITEM07";
            this.漁法名称.Height = 0.15625F;
            this.漁法名称.Left = 3.970079F;
            this.漁法名称.MultiLine = false;
            this.漁法名称.Name = "漁法名称";
            this.漁法名称.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: left; ddo-char-set: 128";
            this.漁法名称.Tag = "";
            this.漁法名称.Text = "ITEM07";
            this.漁法名称.Top = 1.636147E-05F;
            this.漁法名称.Width = 1.202241F;
            // 
            // gfShishoCd
            // 
            this.gfShishoCd.CanGrow = false;
            this.gfShishoCd.Height = 0F;
            this.gfShishoCd.Name = "gfShishoCd";
            this.gfShishoCd.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            // 
            // groupHeader1
            // 
            this.groupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.船主CD,
            this.船主名称,
            this.漁法CD,
            this.漁法名称});
            this.groupHeader1.DataField = "ITEM02";
            this.groupHeader1.Height = 0.1979331F;
            this.groupHeader1.Name = "groupHeader1";
            this.groupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
            this.groupHeader1.UnderlayNext = true;
            // 
            // groupFooter1
            // 
            this.groupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.label1,
            this.textBox1,
            this.line1,
            this.textBox8,
            this.textBox10});
            this.groupFooter1.Height = 0.3854331F;
            this.groupFooter1.Name = "groupFooter1";
            this.groupFooter1.Format += new System.EventHandler(this.groupFooter1_Format);
            // 
            // label1
            // 
            this.label1.Height = 0.2397145F;
            this.label1.HyperLink = null;
            this.label1.Left = 2.23937F;
            this.label1.Name = "label1";
            this.label1.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-weight: normal; text-alig" +
    "n: left; ddo-char-set: 128";
            this.label1.Tag = "";
            this.label1.Text = "*　*　*　小計　*　*　*";
            this.label1.Top = 0.07007875F;
            this.label1.Width = 2.093701F;
            // 
            // textBox1
            // 
            this.textBox1.DataField = "ITEM11";
            this.textBox1.Height = 0.15625F;
            this.textBox1.Left = 8.611812F;
            this.textBox1.Name = "textBox1";
            this.textBox1.OutputFormat = resources.GetString("textBox1.OutputFormat");
            this.textBox1.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 128";
            this.textBox1.SummaryGroup = "groupHeader1";
            this.textBox1.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox1.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox1.Tag = "";
            this.textBox1.Text = "[ITEM11]";
            this.textBox1.Top = 0.1535433F;
            this.textBox1.Width = 0.8803501F;
            // 
            // line1
            // 
            this.line1.Height = 0F;
            this.line1.Left = 0.01023622F;
            this.line1.LineWeight = 2F;
            this.line1.Name = "line1";
            this.line1.Tag = "";
            this.line1.Top = 0.3531496F;
            this.line1.Width = 13.23819F;
            this.line1.X1 = 0.01023622F;
            this.line1.X2 = 13.24843F;
            this.line1.Y1 = 0.3531496F;
            this.line1.Y2 = 0.3531496F;
            // 
            // textBox8
            // 
            this.textBox8.DataField = "ITEM13";
            this.textBox8.Height = 0.15625F;
            this.textBox8.Left = 9.891339F;
            this.textBox8.Name = "textBox8";
            this.textBox8.OutputFormat = resources.GetString("textBox8.OutputFormat");
            this.textBox8.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 128";
            this.textBox8.SummaryGroup = "groupHeader1";
            this.textBox8.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox8.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox8.Tag = "";
            this.textBox8.Text = "[ITEM13]";
            this.textBox8.Top = 0.1535433F;
            this.textBox8.Visible = false;
            this.textBox8.Width = 0.7645674F;
            // 
            // textBox10
            // 
            this.textBox10.Height = 0.15625F;
            this.textBox10.Left = 9.578741F;
            this.textBox10.Name = "textBox10";
            this.textBox10.OutputFormat = resources.GetString("textBox10.OutputFormat");
            this.textBox10.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 128";
            this.textBox10.SummaryGroup = "groupHeader1";
            this.textBox10.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox10.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox10.Tag = "";
            this.textBox10.Text = "[ITEM13]";
            this.textBox10.Top = 0.1535433F;
            this.textBox10.Width = 1.077166F;
            // 
            // groupHeader2
            // 
            this.groupHeader2.DataField = "ITEM01";
            this.groupHeader2.Height = 0F;
            this.groupHeader2.Name = "groupHeader2";
            // 
            // groupFooter2
            // 
            this.groupFooter2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.ラベル50,
            this.テキスト56,
            this.テキスト32,
            this.ラベル34,
            this.直線48,
            this.textBox6,
            this.textBox11});
            this.groupFooter2.Height = 0.2807087F;
            this.groupFooter2.Name = "groupFooter2";
            this.groupFooter2.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
            this.groupFooter2.Format += new System.EventHandler(this.groupFooter2_Format);
            // 
            // ラベル50
            // 
            this.ラベル50.Height = 0.15625F;
            this.ラベル50.HyperLink = null;
            this.ラベル50.Left = 2.239059F;
            this.ラベル50.Name = "ラベル50";
            this.ラベル50.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-weight: normal; text-alig" +
    "n: left; ddo-char-set: 128";
            this.ラベル50.Tag = "";
            this.ラベル50.Text = "*　*　*　合計　*　*　*";
            this.ラベル50.Top = 0.08072916F;
            this.ラベル50.Width = 2.094012F;
            // 
            // テキスト56
            // 
            this.テキスト56.DataField = "ITEM11";
            this.テキスト56.Height = 0.15625F;
            this.テキスト56.Left = 8.611812F;
            this.テキスト56.Name = "テキスト56";
            this.テキスト56.OutputFormat = resources.GetString("テキスト56.OutputFormat");
            this.テキスト56.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 128";
            this.テキスト56.SummaryGroup = "groupHeader2";
            this.テキスト56.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.テキスト56.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.テキスト56.Tag = "";
            this.テキスト56.Text = "[ITEM11]";
            this.テキスト56.Top = 0.08070867F;
            this.テキスト56.Width = 0.8803501F;
            // 
            // テキスト32
            // 
            this.テキスト32.DataField = "ITEM02";
            this.テキスト32.Height = 0.15625F;
            this.テキスト32.Left = 5.361418F;
            this.テキスト32.Name = "テキスト32";
            this.テキスト32.OutputFormat = resources.GetString("テキスト32.OutputFormat");
            this.テキスト32.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 128";
            this.テキスト32.SummaryFunc = GrapeCity.ActiveReports.SectionReportModel.SummaryFunc.Count;
            this.テキスト32.SummaryGroup = "groupHeader2";
            this.テキスト32.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.テキスト32.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.テキスト32.Tag = "";
            this.テキスト32.Text = "[ITEM02]";
            this.テキスト32.Top = 0.07795276F;
            this.テキスト32.Width = 1.452083F;
            // 
            // ラベル34
            // 
            this.ラベル34.Height = 0.1779528F;
            this.ラベル34.HyperLink = null;
            this.ラベル34.Left = 6.842126F;
            this.ラベル34.Name = "ラベル34";
            this.ラベル34.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-weight: normal; text-alig" +
    "n: left; ddo-char-set: 128";
            this.ラベル34.Tag = "";
            this.ラベル34.Text = "件";
            this.ラベル34.Top = 0.07795276F;
            this.ラベル34.Width = 0.1770833F;
            // 
            // 直線48
            // 
            this.直線48.Height = 0.0129921F;
            this.直線48.Left = 0.01023622F;
            this.直線48.LineWeight = 2F;
            this.直線48.Name = "直線48";
            this.直線48.Tag = "";
            this.直線48.Top = 2.980232E-08F;
            this.直線48.Width = 13.23819F;
            this.直線48.X1 = 0.01023622F;
            this.直線48.X2 = 13.24843F;
            this.直線48.Y1 = 0.01299213F;
            this.直線48.Y2 = 2.980232E-08F;
            // 
            // textBox6
            // 
            this.textBox6.DataField = "ITEM13";
            this.textBox6.Height = 0.2F;
            this.textBox6.Left = 9.555512F;
            this.textBox6.Name = "textBox6";
            this.textBox6.OutputFormat = resources.GetString("textBox6.OutputFormat");
            this.textBox6.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: right";
            this.textBox6.SummaryGroup = "groupHeader2";
            this.textBox6.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox6.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox6.Text = "[ITEM13]";
            this.textBox6.Top = 0.08070861F;
            this.textBox6.Visible = false;
            this.textBox6.Width = 1.100394F;
            // 
            // textBox11
            // 
            this.textBox11.DataField = "ITEM13";
            this.textBox11.Height = 0.2F;
            this.textBox11.Left = 9.555512F;
            this.textBox11.Name = "textBox11";
            this.textBox11.OutputFormat = resources.GetString("textBox11.OutputFormat");
            this.textBox11.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: right";
            this.textBox11.SummaryGroup = "groupHeader2";
            this.textBox11.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox11.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox11.Text = "[ITEM13]";
            this.textBox11.Top = 0.08070867F;
            this.textBox11.Width = 1.100394F;
            // 
            // HNSR1021R
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.1968504F;
            this.PageSettings.Margins.Left = 0.7874016F;
            this.PageSettings.Margins.Right = 0.1968504F;
            this.PageSettings.Margins.Top = 0.7874016F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
            this.PageSettings.PaperHeight = 13.89764F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.B4;
            this.PageSettings.PaperWidth = 9.84252F;
            this.PrintWidth = 13.35255F;
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.ghShishoCd);
            this.Sections.Add(this.groupHeader2);
            this.Sections.Add(this.groupHeader1);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.groupFooter1);
            this.Sections.Add(this.groupFooter2);
            this.Sections.Add(this.gfShishoCd);
            this.Sections.Add(this.pageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.ラベル0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.セリ日)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.date)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.page)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.伝票番号)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.山NO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.魚種CD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.魚種名称)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.水揚数量Kｇ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.単価)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.仲買人CD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.仲買人名称)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.船主CD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.船主名称)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.漁法CD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.漁法名称)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト56)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル0;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル1;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル3;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル4;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル5;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル6;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル7;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル8;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル9;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル11;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル13;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル14;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル16;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル17;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線55;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 伝票番号;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 山NO;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 船主CD;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 船主名称;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 漁法CD;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 漁法名称;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 魚種CD;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 魚種名称;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 水揚数量Kｇ;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 単価;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 仲買人CD;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 仲買人名称;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト5;
        private GrapeCity.ActiveReports.SectionReportModel.ReportInfo date;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox page;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader ghShishoCd;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter gfShishoCd;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader groupHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter groupFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox1;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader groupHeader2;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter groupFooter2;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル50;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト56;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト32;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル34;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線48;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox セリ日;
        private GrapeCity.ActiveReports.SectionReportModel.Label label2;
        private GrapeCity.ActiveReports.SectionReportModel.Label label3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox5;
        private GrapeCity.ActiveReports.SectionReportModel.Label label4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox6;
        private GrapeCity.ActiveReports.SectionReportModel.Label label5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox7;
        private GrapeCity.ActiveReports.SectionReportModel.Label label6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox11;
    }
}
