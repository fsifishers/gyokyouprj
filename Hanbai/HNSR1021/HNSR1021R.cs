﻿using System.Data;

using jp.co.fsi.common.report;
using jp.co.fsi.common.util;

namespace jp.co.fsi.hn.hnsr1021
{
    /// <summary>
    /// HNSR1021R の概要の説明です。
    /// </summary>
    public partial class HNSR1021R : BaseReport
    {

        public HNSR1021R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }

        private void groupFooter1_Format(object sender, System.EventArgs e)
        {

            textBox10.Value = Util.ToInt(textBox8.Value) / Util.ToDecimal(textBox1.Value);

        }

        private void groupFooter2_Format(object sender, System.EventArgs e)
        {
            textBox11.Value = Util.ToInt(textBox6.Value) / Util.ToDecimal(テキスト56.Value);

        }
    }
}
