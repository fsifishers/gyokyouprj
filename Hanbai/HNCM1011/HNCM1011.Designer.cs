﻿namespace jp.co.fsi.hn.hncm1011
{
    partial class HNCM1011
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblKanaNm = new System.Windows.Forms.Label();
            this.txtKanaName = new jp.co.fsi.common.controls.FsiTextBox();
            this.dgvList = new System.Windows.Forms.DataGridView();
            this.btnEnter = new System.Windows.Forms.Button();
            this.ckboxAllHyoji = new System.Windows.Forms.CheckBox();
            this.pnlDebug.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvList)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.TabIndex = 999;
            this.lblTitle.Text = "";
            // 
            // btnF1
            // 
            this.btnF1.Location = new System.Drawing.Point(69, 49);
            // 
            // btnF2
            // 
            this.btnF2.Location = new System.Drawing.Point(135, 49);
            // 
            // btnF3
            // 
            this.btnF3.Location = new System.Drawing.Point(201, 49);
            // 
            // btnF4
            // 
            this.btnF4.Location = new System.Drawing.Point(267, 49);
            this.btnF4.Text = "F4\r\n\r\nついか";
            // 
            // btnF5
            // 
            this.btnF5.Location = new System.Drawing.Point(333, 49);
            // 
            // btnF7
            // 
            this.btnF7.Location = new System.Drawing.Point(465, 49);
            // 
            // btnF6
            // 
            this.btnF6.Location = new System.Drawing.Point(399, 49);
            // 
            // btnF8
            // 
            this.btnF8.Location = new System.Drawing.Point(531, 49);
            // 
            // btnF9
            // 
            this.btnF9.Location = new System.Drawing.Point(597, 49);
            // 
            // btnF12
            // 
            this.btnF12.Location = new System.Drawing.Point(774, 52);
            this.btnF12.Visible = false;
            // 
            // btnF11
            // 
            this.btnF11.Location = new System.Drawing.Point(731, 49);
            // 
            // btnF10
            // 
            this.btnF10.Location = new System.Drawing.Point(664, 49);
            // 
            // pnlDebug
            // 
            this.pnlDebug.Controls.Add(this.btnEnter);
            this.pnlDebug.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.pnlDebug.Size = new System.Drawing.Size(847, 100);
            this.pnlDebug.Controls.SetChildIndex(this.btnF6, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF7, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF5, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF8, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF4, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF9, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF3, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF10, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF2, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF11, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF1, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF12, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnEsc, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnEnter, 0);
            // 
            // lblKanaNm
            // 
            this.lblKanaNm.BackColor = System.Drawing.Color.Silver;
            this.lblKanaNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKanaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblKanaNm.Location = new System.Drawing.Point(12, 48);
            this.lblKanaNm.Name = "lblKanaNm";
            this.lblKanaNm.Size = new System.Drawing.Size(228, 25);
            this.lblKanaNm.TabIndex = 0;
            this.lblKanaNm.Text = "カ　ナ　名";
            this.lblKanaNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKanaName
            // 
            this.txtKanaName.AllowDrop = true;
            this.txtKanaName.AutoSizeFromLength = false;
            this.txtKanaName.DisplayLength = null;
            this.txtKanaName.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtKanaName.Location = new System.Drawing.Point(93, 50);
            this.txtKanaName.MaxLength = 30;
            this.txtKanaName.Name = "txtKanaName";
            this.txtKanaName.Size = new System.Drawing.Size(144, 20);
            this.txtKanaName.TabIndex = 1;
            this.txtKanaName.Validating += new System.ComponentModel.CancelEventHandler(this.txtKanaName_Validating);
            // 
            // dgvList
            // 
            this.dgvList.AllowUserToAddRows = false;
            this.dgvList.AllowUserToDeleteRows = false;
            this.dgvList.AllowUserToResizeColumns = false;
            this.dgvList.AllowUserToResizeRows = false;
            this.dgvList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvList.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.dgvList.Location = new System.Drawing.Point(12, 82);
            this.dgvList.MultiSelect = false;
            this.dgvList.Name = "dgvList";
            this.dgvList.ReadOnly = true;
            this.dgvList.RowHeadersVisible = false;
            this.dgvList.RowTemplate.Height = 21;
            this.dgvList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvList.Size = new System.Drawing.Size(530, 296);
            this.dgvList.TabIndex = 3;
            this.dgvList.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvList_CellDoubleClick);
            this.dgvList.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvList_KeyDown);
            // 
            // btnEnter
            // 
            this.btnEnter.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnEnter.Location = new System.Drawing.Point(3, 49);
            this.btnEnter.Name = "btnEnter";
            this.btnEnter.Size = new System.Drawing.Size(65, 45);
            this.btnEnter.TabIndex = 907;
            this.btnEnter.TabStop = false;
            this.btnEnter.Text = "Enter\r\n\r\n変更";
            this.btnEnter.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnEnter.UseVisualStyleBackColor = true;
            this.btnEnter.Click += new System.EventHandler(this.btnEnter_Click);
            // 
            // ckboxAllHyoji
            // 
            this.ckboxAllHyoji.AutoSize = true;
            this.ckboxAllHyoji.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.ckboxAllHyoji.Location = new System.Drawing.Point(451, 48);
            this.ckboxAllHyoji.Name = "ckboxAllHyoji";
            this.ckboxAllHyoji.Size = new System.Drawing.Size(82, 17);
            this.ckboxAllHyoji.TabIndex = 2;
            this.ckboxAllHyoji.Text = "全て表示";
            this.ckboxAllHyoji.UseVisualStyleBackColor = true;
            this.ckboxAllHyoji.CheckedChanged += new System.EventHandler(this.ckboxAllHyoji_CheckedChanged);
            // 
            // HNCM1011
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 638);
            this.Controls.Add(this.ckboxAllHyoji);
            this.Controls.Add(this.dgvList);
            this.Controls.Add(this.txtKanaName);
            this.Controls.Add(this.lblKanaNm);
            this.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "HNCM1011";
            this.Par1 = "";
            this.Text = "仲買人コード検索";
            this.Controls.SetChildIndex(this.lblKanaNm, 0);
            this.Controls.SetChildIndex(this.txtKanaName, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.dgvList, 0);
            this.Controls.SetChildIndex(this.ckboxAllHyoji, 0);
            this.pnlDebug.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblKanaNm;
        private jp.co.fsi.common.controls.FsiTextBox txtKanaName;
        private System.Windows.Forms.DataGridView dgvList;
        protected System.Windows.Forms.Button btnEnter;
        private System.Windows.Forms.CheckBox ckboxAllHyoji;

    }
}