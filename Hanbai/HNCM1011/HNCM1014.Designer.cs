﻿namespace jp.co.fsi.hn.hncm1011
{
    partial class HNCM1014
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbxSenshuInfo = new System.Windows.Forms.GroupBox();
            this.lblShzTnkHohoMemo = new System.Windows.Forms.Label();
            this.lblShzHsSrMemo = new System.Windows.Forms.Label();
            this.txtShohizeiTenkaHoho = new jp.co.fsi.common.controls.FsiTextBox();
            this.textBox14 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtShohizeiHasuShori = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShzNrkHohoNm = new System.Windows.Forms.Label();
            this.lblKgkHsShrMemo = new System.Windows.Forms.Label();
            this.lblTnkStkHohoMemo = new System.Windows.Forms.Label();
            this.txtShohizeiNyuryokuHoho = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKingakuHasuShori = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtTankaShutokuHoho = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShohizeiTenkaHoho = new System.Windows.Forms.Label();
            this.lblShohizeiHasuShori = new System.Windows.Forms.Label();
            this.lblShohizeiNyuryokuHoho = new System.Windows.Forms.Label();
            this.lblKingakuHasuShori = new System.Windows.Forms.Label();
            this.lblTankaShutokuHoho = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.gbxSenshuInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Size = new System.Drawing.Size(420, 23);
            this.lblTitle.TabIndex = 999;
            this.lblTitle.Text = "";
            // 
            // btnF3
            // 
            this.btnF3.Text = "F3\r\n\r\n削除";
            // 
            // btnF6
            // 
            this.btnF6.Text = "F6\r\n\r\n登録";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.pnlDebug.Location = new System.Drawing.Point(5, 164);
            this.pnlDebug.Size = new System.Drawing.Size(437, 100);
            // 
            // gbxSenshuInfo
            // 
            this.gbxSenshuInfo.Controls.Add(this.lblShzTnkHohoMemo);
            this.gbxSenshuInfo.Controls.Add(this.lblShzHsSrMemo);
            this.gbxSenshuInfo.Controls.Add(this.txtShohizeiTenkaHoho);
            this.gbxSenshuInfo.Controls.Add(this.textBox14);
            this.gbxSenshuInfo.Controls.Add(this.txtShohizeiHasuShori);
            this.gbxSenshuInfo.Controls.Add(this.lblShzNrkHohoNm);
            this.gbxSenshuInfo.Controls.Add(this.lblKgkHsShrMemo);
            this.gbxSenshuInfo.Controls.Add(this.lblTnkStkHohoMemo);
            this.gbxSenshuInfo.Controls.Add(this.txtShohizeiNyuryokuHoho);
            this.gbxSenshuInfo.Controls.Add(this.txtKingakuHasuShori);
            this.gbxSenshuInfo.Controls.Add(this.txtTankaShutokuHoho);
            this.gbxSenshuInfo.Controls.Add(this.lblShohizeiTenkaHoho);
            this.gbxSenshuInfo.Controls.Add(this.lblShohizeiHasuShori);
            this.gbxSenshuInfo.Controls.Add(this.lblShohizeiNyuryokuHoho);
            this.gbxSenshuInfo.Controls.Add(this.lblKingakuHasuShori);
            this.gbxSenshuInfo.Controls.Add(this.lblTankaShutokuHoho);
            this.gbxSenshuInfo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxSenshuInfo.Location = new System.Drawing.Point(12, 12);
            this.gbxSenshuInfo.Name = "gbxSenshuInfo";
            this.gbxSenshuInfo.Size = new System.Drawing.Size(406, 190);
            this.gbxSenshuInfo.TabIndex = 2;
            this.gbxSenshuInfo.TabStop = false;
            this.gbxSenshuInfo.Text = "仲買人マスタ初期値設定";
            // 
            // lblShzTnkHohoMemo
            // 
            this.lblShzTnkHohoMemo.BackColor = System.Drawing.Color.Silver;
            this.lblShzTnkHohoMemo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShzTnkHohoMemo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblShzTnkHohoMemo.Location = new System.Drawing.Point(154, 140);
            this.lblShzTnkHohoMemo.Name = "lblShzTnkHohoMemo";
            this.lblShzTnkHohoMemo.Size = new System.Drawing.Size(236, 25);
            this.lblShzTnkHohoMemo.TabIndex = 47;
            this.lblShzTnkHohoMemo.Text = "1:明細転嫁 2:伝票転嫁 3:請求転嫁";
            this.lblShzTnkHohoMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShzHsSrMemo
            // 
            this.lblShzHsSrMemo.BackColor = System.Drawing.Color.Silver;
            this.lblShzHsSrMemo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShzHsSrMemo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblShzHsSrMemo.Location = new System.Drawing.Point(154, 112);
            this.lblShzHsSrMemo.Name = "lblShzHsSrMemo";
            this.lblShzHsSrMemo.Size = new System.Drawing.Size(236, 25);
            this.lblShzHsSrMemo.TabIndex = 44;
            this.lblShzHsSrMemo.Text = "1:切り捨て 2:四捨五入 3:切り上げ";
            this.lblShzHsSrMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShohizeiTenkaHoho
            // 
            this.txtShohizeiTenkaHoho.AllowDrop = true;
            this.txtShohizeiTenkaHoho.AutoSizeFromLength = false;
            this.txtShohizeiTenkaHoho.BackColor = System.Drawing.Color.White;
            this.txtShohizeiTenkaHoho.DisplayLength = null;
            this.txtShohizeiTenkaHoho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtShohizeiTenkaHoho.Location = new System.Drawing.Point(117, 142);
            this.txtShohizeiTenkaHoho.MaxLength = 1;
            this.txtShohizeiTenkaHoho.Name = "txtShohizeiTenkaHoho";
            this.txtShohizeiTenkaHoho.Size = new System.Drawing.Size(32, 20);
            this.txtShohizeiTenkaHoho.TabIndex = 46;
            this.txtShohizeiTenkaHoho.Text = "2";
            this.txtShohizeiTenkaHoho.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShohizeiTenkaHoho.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtShohizeiTenkaHoho_KeyDown);
            this.txtShohizeiTenkaHoho.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohizeiTenkaHoho_Validating);
            // 
            // textBox14
            // 
            this.textBox14.AllowDrop = true;
            this.textBox14.AutoSizeFromLength = false;
            this.textBox14.DisplayLength = null;
            this.textBox14.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBox14.Location = new System.Drawing.Point(919, -149);
            this.textBox14.MaxLength = 4;
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new System.Drawing.Size(50, 19);
            this.textBox14.TabIndex = 938;
            // 
            // txtShohizeiHasuShori
            // 
            this.txtShohizeiHasuShori.AllowDrop = true;
            this.txtShohizeiHasuShori.AutoSizeFromLength = false;
            this.txtShohizeiHasuShori.BackColor = System.Drawing.Color.White;
            this.txtShohizeiHasuShori.DisplayLength = null;
            this.txtShohizeiHasuShori.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtShohizeiHasuShori.Location = new System.Drawing.Point(117, 114);
            this.txtShohizeiHasuShori.MaxLength = 1;
            this.txtShohizeiHasuShori.Name = "txtShohizeiHasuShori";
            this.txtShohizeiHasuShori.Size = new System.Drawing.Size(32, 20);
            this.txtShohizeiHasuShori.TabIndex = 43;
            this.txtShohizeiHasuShori.Text = "2";
            this.txtShohizeiHasuShori.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShohizeiHasuShori.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohizeiHasuShori_Validating);
            // 
            // lblShzNrkHohoNm
            // 
            this.lblShzNrkHohoNm.BackColor = System.Drawing.Color.Silver;
            this.lblShzNrkHohoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShzNrkHohoNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShzNrkHohoNm.Location = new System.Drawing.Point(154, 84);
            this.lblShzNrkHohoNm.Name = "lblShzNrkHohoNm";
            this.lblShzNrkHohoNm.Size = new System.Drawing.Size(236, 25);
            this.lblShzNrkHohoNm.TabIndex = 29;
            this.lblShzNrkHohoNm.Text = "税抜き入力（自動計算あり）";
            this.lblShzNrkHohoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKgkHsShrMemo
            // 
            this.lblKgkHsShrMemo.BackColor = System.Drawing.Color.Silver;
            this.lblKgkHsShrMemo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKgkHsShrMemo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKgkHsShrMemo.Location = new System.Drawing.Point(154, 56);
            this.lblKgkHsShrMemo.Name = "lblKgkHsShrMemo";
            this.lblKgkHsShrMemo.Size = new System.Drawing.Size(236, 25);
            this.lblKgkHsShrMemo.TabIndex = 26;
            this.lblKgkHsShrMemo.Text = "1:切捨て 2:四捨五入 3:切り上げ";
            this.lblKgkHsShrMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTnkStkHohoMemo
            // 
            this.lblTnkStkHohoMemo.BackColor = System.Drawing.Color.Silver;
            this.lblTnkStkHohoMemo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTnkStkHohoMemo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTnkStkHohoMemo.Location = new System.Drawing.Point(154, 28);
            this.lblTnkStkHohoMemo.Name = "lblTnkStkHohoMemo";
            this.lblTnkStkHohoMemo.Size = new System.Drawing.Size(236, 25);
            this.lblTnkStkHohoMemo.TabIndex = 23;
            this.lblTnkStkHohoMemo.Text = "0:卸単価 1:小売単価 2:前回単価";
            this.lblTnkStkHohoMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShohizeiNyuryokuHoho
            // 
            this.txtShohizeiNyuryokuHoho.AllowDrop = true;
            this.txtShohizeiNyuryokuHoho.AutoSizeFromLength = false;
            this.txtShohizeiNyuryokuHoho.BackColor = System.Drawing.Color.White;
            this.txtShohizeiNyuryokuHoho.DisplayLength = null;
            this.txtShohizeiNyuryokuHoho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShohizeiNyuryokuHoho.Location = new System.Drawing.Point(117, 86);
            this.txtShohizeiNyuryokuHoho.MaxLength = 1;
            this.txtShohizeiNyuryokuHoho.Name = "txtShohizeiNyuryokuHoho";
            this.txtShohizeiNyuryokuHoho.Size = new System.Drawing.Size(31, 20);
            this.txtShohizeiNyuryokuHoho.TabIndex = 28;
            this.txtShohizeiNyuryokuHoho.Text = "2";
            this.txtShohizeiNyuryokuHoho.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShohizeiNyuryokuHoho.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohizeiNyuryokuHoho_Validating);
            // 
            // txtKingakuHasuShori
            // 
            this.txtKingakuHasuShori.AllowDrop = true;
            this.txtKingakuHasuShori.AutoSizeFromLength = false;
            this.txtKingakuHasuShori.BackColor = System.Drawing.Color.White;
            this.txtKingakuHasuShori.DisplayLength = null;
            this.txtKingakuHasuShori.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKingakuHasuShori.Location = new System.Drawing.Point(117, 57);
            this.txtKingakuHasuShori.MaxLength = 1;
            this.txtKingakuHasuShori.Name = "txtKingakuHasuShori";
            this.txtKingakuHasuShori.Size = new System.Drawing.Size(31, 20);
            this.txtKingakuHasuShori.TabIndex = 25;
            this.txtKingakuHasuShori.Text = "2";
            this.txtKingakuHasuShori.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKingakuHasuShori.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingakuHasuShori_Validating);
            // 
            // txtTankaShutokuHoho
            // 
            this.txtTankaShutokuHoho.AllowDrop = true;
            this.txtTankaShutokuHoho.AutoSizeFromLength = false;
            this.txtTankaShutokuHoho.BackColor = System.Drawing.Color.White;
            this.txtTankaShutokuHoho.DisplayLength = null;
            this.txtTankaShutokuHoho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTankaShutokuHoho.Location = new System.Drawing.Point(117, 30);
            this.txtTankaShutokuHoho.MaxLength = 1;
            this.txtTankaShutokuHoho.Name = "txtTankaShutokuHoho";
            this.txtTankaShutokuHoho.Size = new System.Drawing.Size(31, 20);
            this.txtTankaShutokuHoho.TabIndex = 22;
            this.txtTankaShutokuHoho.Text = "0";
            this.txtTankaShutokuHoho.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTankaShutokuHoho.Validating += new System.ComponentModel.CancelEventHandler(this.txtTankaShutokuHoho_Validating);
            // 
            // lblShohizeiTenkaHoho
            // 
            this.lblShohizeiTenkaHoho.BackColor = System.Drawing.Color.Silver;
            this.lblShohizeiTenkaHoho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShohizeiTenkaHoho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblShohizeiTenkaHoho.Location = new System.Drawing.Point(14, 140);
            this.lblShohizeiTenkaHoho.Name = "lblShohizeiTenkaHoho";
            this.lblShohizeiTenkaHoho.Size = new System.Drawing.Size(138, 25);
            this.lblShohizeiTenkaHoho.TabIndex = 45;
            this.lblShohizeiTenkaHoho.Text = "消費税転嫁方法";
            this.lblShohizeiTenkaHoho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShohizeiHasuShori
            // 
            this.lblShohizeiHasuShori.BackColor = System.Drawing.Color.Silver;
            this.lblShohizeiHasuShori.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShohizeiHasuShori.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblShohizeiHasuShori.Location = new System.Drawing.Point(14, 112);
            this.lblShohizeiHasuShori.Name = "lblShohizeiHasuShori";
            this.lblShohizeiHasuShori.Size = new System.Drawing.Size(138, 25);
            this.lblShohizeiHasuShori.TabIndex = 42;
            this.lblShohizeiHasuShori.Text = "消費税端数処理";
            this.lblShohizeiHasuShori.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShohizeiNyuryokuHoho
            // 
            this.lblShohizeiNyuryokuHoho.BackColor = System.Drawing.Color.Silver;
            this.lblShohizeiNyuryokuHoho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShohizeiNyuryokuHoho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShohizeiNyuryokuHoho.Location = new System.Drawing.Point(14, 84);
            this.lblShohizeiNyuryokuHoho.Name = "lblShohizeiNyuryokuHoho";
            this.lblShohizeiNyuryokuHoho.Size = new System.Drawing.Size(137, 25);
            this.lblShohizeiNyuryokuHoho.TabIndex = 27;
            this.lblShohizeiNyuryokuHoho.Text = "消費税入力方法";
            this.lblShohizeiNyuryokuHoho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKingakuHasuShori
            // 
            this.lblKingakuHasuShori.BackColor = System.Drawing.Color.Silver;
            this.lblKingakuHasuShori.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKingakuHasuShori.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKingakuHasuShori.Location = new System.Drawing.Point(14, 56);
            this.lblKingakuHasuShori.Name = "lblKingakuHasuShori";
            this.lblKingakuHasuShori.Size = new System.Drawing.Size(137, 25);
            this.lblKingakuHasuShori.TabIndex = 24;
            this.lblKingakuHasuShori.Text = "金額端数処理";
            this.lblKingakuHasuShori.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTankaShutokuHoho
            // 
            this.lblTankaShutokuHoho.BackColor = System.Drawing.Color.Silver;
            this.lblTankaShutokuHoho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTankaShutokuHoho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTankaShutokuHoho.Location = new System.Drawing.Point(14, 28);
            this.lblTankaShutokuHoho.Name = "lblTankaShutokuHoho";
            this.lblTankaShutokuHoho.Size = new System.Drawing.Size(137, 25);
            this.lblTankaShutokuHoho.TabIndex = 21;
            this.lblTankaShutokuHoho.Text = "単価取得方法";
            this.lblTankaShutokuHoho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // HNCM1014
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(429, 268);
            this.Controls.Add(this.gbxSenshuInfo);
            this.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "HNCM1014";
            this.ShowFButton = true;
            this.Text = "仲買人マスタ初期値設定";
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.gbxSenshuInfo, 0);
            this.pnlDebug.ResumeLayout(false);
            this.gbxSenshuInfo.ResumeLayout(false);
            this.gbxSenshuInfo.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox gbxSenshuInfo;
        private System.Windows.Forms.Label lblShohizeiTenkaHoho;
        private System.Windows.Forms.Label lblShohizeiHasuShori;
        private System.Windows.Forms.Label lblShohizeiNyuryokuHoho;
        private System.Windows.Forms.Label lblKingakuHasuShori;
        private System.Windows.Forms.Label lblTankaShutokuHoho;
        private jp.co.fsi.common.controls.FsiTextBox txtShohizeiNyuryokuHoho;
        private jp.co.fsi.common.controls.FsiTextBox txtKingakuHasuShori;
        private jp.co.fsi.common.controls.FsiTextBox txtTankaShutokuHoho;
        private System.Windows.Forms.Label lblShzNrkHohoNm;
        private System.Windows.Forms.Label lblKgkHsShrMemo;
        private System.Windows.Forms.Label lblTnkStkHohoMemo;
        private jp.co.fsi.common.controls.FsiTextBox txtShohizeiTenkaHoho;
        private jp.co.fsi.common.controls.FsiTextBox textBox14;
        private jp.co.fsi.common.controls.FsiTextBox txtShohizeiHasuShori;
        private System.Windows.Forms.Label lblShzTnkHohoMemo;
        private System.Windows.Forms.Label lblShzHsSrMemo;

    }
}