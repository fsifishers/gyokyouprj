﻿namespace jp.co.fsi.hn.hncm1011
{
    partial class HNCM1012
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblNakagaininCd = new System.Windows.Forms.Label();
            this.txtNakagaininCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.gbxSenshuInfo = new System.Windows.Forms.GroupBox();
            this.txthyoji = new jp.co.fsi.common.controls.FsiTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.FsiTextBox1 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtMyNumber = new jp.co.fsi.common.controls.FsiTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtKaishuBi = new jp.co.fsi.common.controls.FsiTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblShzTnkHohoMemo = new System.Windows.Forms.Label();
            this.lblShzHsSrMemo = new System.Windows.Forms.Label();
            this.lblSkshKskMemo = new System.Windows.Forms.Label();
            this.lblSkshHkMemo = new System.Windows.Forms.Label();
            this.lblShimebiMemo = new System.Windows.Forms.Label();
            this.lblSeikyusakiNm = new System.Windows.Forms.Label();
            this.txtKaishuTsuki = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtShohizeiTenkaHoho = new jp.co.fsi.common.controls.FsiTextBox();
            this.textBox14 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtShohizeiHasuShori = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtSeikyushoKeishiki = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtSeikyushoHakko = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtShimebi = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtSeikyusakiCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShzNrkHohoNm = new System.Windows.Forms.Label();
            this.lblKgkHsShrMemo = new System.Windows.Forms.Label();
            this.lblTnkStkHohoMemo = new System.Windows.Forms.Label();
            this.lblTantoshaNm = new System.Windows.Forms.Label();
            this.lblAddBar = new System.Windows.Forms.Label();
            this.txtYubinBango2 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtShohizeiNyuryokuHoho = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKingakuHasuShori = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtTankaShutokuHoho = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtTantoshaCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtFax = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtTel = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtJusho2 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtJusho1 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtYubinBango1 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtRyNakagaininNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtNakagaininKanaNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtNakagaininNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKaishuTsuki = new System.Windows.Forms.Label();
            this.lblShohizeiTenkaHoho = new System.Windows.Forms.Label();
            this.lblShohizeiHasuShori = new System.Windows.Forms.Label();
            this.lblSeikyushoKeishiki = new System.Windows.Forms.Label();
            this.lblSeikyushoHakko = new System.Windows.Forms.Label();
            this.lblShimebi = new System.Windows.Forms.Label();
            this.lblSeikyusakiCd = new System.Windows.Forms.Label();
            this.lblShohizeiNyuryokuHoho = new System.Windows.Forms.Label();
            this.lblKingakuHasuShori = new System.Windows.Forms.Label();
            this.lblTankaShutokuHoho = new System.Windows.Forms.Label();
            this.lblTantoshaCd = new System.Windows.Forms.Label();
            this.lblFax = new System.Windows.Forms.Label();
            this.lblTel = new System.Windows.Forms.Label();
            this.lblJusho2 = new System.Windows.Forms.Label();
            this.lblJusho1 = new System.Windows.Forms.Label();
            this.lblYubinBango = new System.Windows.Forms.Label();
            this.lblRyNakagaininNm = new System.Windows.Forms.Label();
            this.lblNakagaininKanaNm = new System.Windows.Forms.Label();
            this.lblNakagaininNm = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.gbxSenshuInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Size = new System.Drawing.Size(806, 23);
            this.lblTitle.TabIndex = 999;
            this.lblTitle.Text = "";
            // 
            // btnF3
            // 
            this.btnF3.Text = "F3\r\n\r\n削除";
            // 
            // btnF6
            // 
            this.btnF6.Text = "F6\r\n\r\n登録";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.pnlDebug.Location = new System.Drawing.Point(5, 337);
            this.pnlDebug.Size = new System.Drawing.Size(823, 100);
            // 
            // lblNakagaininCd
            // 
            this.lblNakagaininCd.BackColor = System.Drawing.Color.Silver;
            this.lblNakagaininCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblNakagaininCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblNakagaininCd.Location = new System.Drawing.Point(14, 14);
            this.lblNakagaininCd.Name = "lblNakagaininCd";
            this.lblNakagaininCd.Size = new System.Drawing.Size(139, 25);
            this.lblNakagaininCd.TabIndex = 0;
            this.lblNakagaininCd.Text = "仲 買 人 C D";
            this.lblNakagaininCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtNakagaininCd
            // 
            this.txtNakagaininCd.AllowDrop = true;
            this.txtNakagaininCd.AutoSizeFromLength = false;
            this.txtNakagaininCd.DisplayLength = null;
            this.txtNakagaininCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtNakagaininCd.Location = new System.Drawing.Point(116, 16);
            this.txtNakagaininCd.MaxLength = 4;
            this.txtNakagaininCd.Name = "txtNakagaininCd";
            this.txtNakagaininCd.Size = new System.Drawing.Size(34, 20);
            this.txtNakagaininCd.TabIndex = 1;
            this.txtNakagaininCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtNakagaininCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtNakagaininCd_Validating);
            // 
            // gbxSenshuInfo
            // 
            this.gbxSenshuInfo.Controls.Add(this.txthyoji);
            this.gbxSenshuInfo.Controls.Add(this.label4);
            this.gbxSenshuInfo.Controls.Add(this.label5);
            this.gbxSenshuInfo.Controls.Add(this.button1);
            this.gbxSenshuInfo.Controls.Add(this.FsiTextBox1);
            this.gbxSenshuInfo.Controls.Add(this.txtMyNumber);
            this.gbxSenshuInfo.Controls.Add(this.label3);
            this.gbxSenshuInfo.Controls.Add(this.txtKaishuBi);
            this.gbxSenshuInfo.Controls.Add(this.label2);
            this.gbxSenshuInfo.Controls.Add(this.label1);
            this.gbxSenshuInfo.Controls.Add(this.lblShzTnkHohoMemo);
            this.gbxSenshuInfo.Controls.Add(this.lblShzHsSrMemo);
            this.gbxSenshuInfo.Controls.Add(this.lblSkshKskMemo);
            this.gbxSenshuInfo.Controls.Add(this.lblSkshHkMemo);
            this.gbxSenshuInfo.Controls.Add(this.lblShimebiMemo);
            this.gbxSenshuInfo.Controls.Add(this.lblSeikyusakiNm);
            this.gbxSenshuInfo.Controls.Add(this.txtKaishuTsuki);
            this.gbxSenshuInfo.Controls.Add(this.txtShohizeiTenkaHoho);
            this.gbxSenshuInfo.Controls.Add(this.textBox14);
            this.gbxSenshuInfo.Controls.Add(this.txtShohizeiHasuShori);
            this.gbxSenshuInfo.Controls.Add(this.txtSeikyushoKeishiki);
            this.gbxSenshuInfo.Controls.Add(this.txtSeikyushoHakko);
            this.gbxSenshuInfo.Controls.Add(this.txtShimebi);
            this.gbxSenshuInfo.Controls.Add(this.txtSeikyusakiCd);
            this.gbxSenshuInfo.Controls.Add(this.lblShzNrkHohoNm);
            this.gbxSenshuInfo.Controls.Add(this.lblKgkHsShrMemo);
            this.gbxSenshuInfo.Controls.Add(this.lblTnkStkHohoMemo);
            this.gbxSenshuInfo.Controls.Add(this.lblTantoshaNm);
            this.gbxSenshuInfo.Controls.Add(this.lblAddBar);
            this.gbxSenshuInfo.Controls.Add(this.txtYubinBango2);
            this.gbxSenshuInfo.Controls.Add(this.txtShohizeiNyuryokuHoho);
            this.gbxSenshuInfo.Controls.Add(this.txtKingakuHasuShori);
            this.gbxSenshuInfo.Controls.Add(this.txtTankaShutokuHoho);
            this.gbxSenshuInfo.Controls.Add(this.txtTantoshaCd);
            this.gbxSenshuInfo.Controls.Add(this.txtFax);
            this.gbxSenshuInfo.Controls.Add(this.txtTel);
            this.gbxSenshuInfo.Controls.Add(this.txtJusho2);
            this.gbxSenshuInfo.Controls.Add(this.txtJusho1);
            this.gbxSenshuInfo.Controls.Add(this.txtYubinBango1);
            this.gbxSenshuInfo.Controls.Add(this.txtRyNakagaininNm);
            this.gbxSenshuInfo.Controls.Add(this.txtNakagaininKanaNm);
            this.gbxSenshuInfo.Controls.Add(this.txtNakagaininNm);
            this.gbxSenshuInfo.Controls.Add(this.lblKaishuTsuki);
            this.gbxSenshuInfo.Controls.Add(this.lblShohizeiTenkaHoho);
            this.gbxSenshuInfo.Controls.Add(this.lblShohizeiHasuShori);
            this.gbxSenshuInfo.Controls.Add(this.lblSeikyushoKeishiki);
            this.gbxSenshuInfo.Controls.Add(this.lblSeikyushoHakko);
            this.gbxSenshuInfo.Controls.Add(this.lblShimebi);
            this.gbxSenshuInfo.Controls.Add(this.lblSeikyusakiCd);
            this.gbxSenshuInfo.Controls.Add(this.lblShohizeiNyuryokuHoho);
            this.gbxSenshuInfo.Controls.Add(this.lblKingakuHasuShori);
            this.gbxSenshuInfo.Controls.Add(this.lblTankaShutokuHoho);
            this.gbxSenshuInfo.Controls.Add(this.lblTantoshaCd);
            this.gbxSenshuInfo.Controls.Add(this.lblFax);
            this.gbxSenshuInfo.Controls.Add(this.lblTel);
            this.gbxSenshuInfo.Controls.Add(this.lblJusho2);
            this.gbxSenshuInfo.Controls.Add(this.lblJusho1);
            this.gbxSenshuInfo.Controls.Add(this.lblYubinBango);
            this.gbxSenshuInfo.Controls.Add(this.lblRyNakagaininNm);
            this.gbxSenshuInfo.Controls.Add(this.lblNakagaininKanaNm);
            this.gbxSenshuInfo.Controls.Add(this.lblNakagaininNm);
            this.gbxSenshuInfo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxSenshuInfo.Location = new System.Drawing.Point(8, 43);
            this.gbxSenshuInfo.Name = "gbxSenshuInfo";
            this.gbxSenshuInfo.Size = new System.Drawing.Size(800, 337);
            this.gbxSenshuInfo.TabIndex = 2;
            this.gbxSenshuInfo.TabStop = false;
            // 
            // txthyoji
            // 
            this.txthyoji.AutoSizeFromLength = false;
            this.txthyoji.BackColor = System.Drawing.Color.White;
            this.txthyoji.DisplayLength = null;
            this.txthyoji.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txthyoji.Location = new System.Drawing.Point(462, 227);
            this.txthyoji.MaxLength = 1;
            this.txthyoji.Name = "txthyoji";
            this.txthyoji.Size = new System.Drawing.Size(32, 20);
            this.txthyoji.TabIndex = 58;
            this.txthyoji.Text = "0";
            this.txthyoji.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txthyoji.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txthyoji_KeyDown);
            this.txthyoji.Validating += new System.ComponentModel.CancelEventHandler(this.txthyoji_Validating);
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Silver;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label4.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.label4.Location = new System.Drawing.Point(499, 225);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(209, 25);
            this.label4.TabIndex = 59;
            this.label4.Text = "0:表示する 1:表示しない ";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Silver;
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label5.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.label5.Location = new System.Drawing.Point(359, 225);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(138, 25);
            this.label5.TabIndex = 57;
            this.label5.Text = "一覧表示";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.button1.Location = new System.Drawing.Point(583, 199);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 56;
            this.button1.Text = "公開";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // FsiTextBox1
            // 
            this.FsiTextBox1.AutoSizeFromLength = false;
            this.FsiTextBox1.DisplayLength = null;
            this.FsiTextBox1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.FsiTextBox1.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.FsiTextBox1.Location = new System.Drawing.Point(462, 200);
            this.FsiTextBox1.MaxLength = 15;
            this.FsiTextBox1.Name = "FsiTextBox1";
            this.FsiTextBox1.ReadOnly = true;
            this.FsiTextBox1.Size = new System.Drawing.Size(114, 20);
            this.FsiTextBox1.TabIndex = 55;
            this.FsiTextBox1.Text = "************";
            this.FsiTextBox1.Visible = false;
            // 
            // txtMyNumber
            // 
            this.txtMyNumber.AutoSizeFromLength = false;
            this.txtMyNumber.DisplayLength = null;
            this.txtMyNumber.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtMyNumber.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtMyNumber.Location = new System.Drawing.Point(462, 200);
            this.txtMyNumber.MaxLength = 12;
            this.txtMyNumber.Name = "txtMyNumber";
            this.txtMyNumber.ReadOnly = true;
            this.txtMyNumber.Size = new System.Drawing.Size(114, 20);
            this.txtMyNumber.TabIndex = 54;
            this.txtMyNumber.Validating += new System.ComponentModel.CancelEventHandler(this.txtMyNumber_Validating);
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Silver;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.label3.Location = new System.Drawing.Point(359, 199);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(219, 24);
            this.label3.TabIndex = 53;
            this.label3.Text = "マイナンバー";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKaishuBi
            // 
            this.txtKaishuBi.AllowDrop = true;
            this.txtKaishuBi.AutoSizeFromLength = false;
            this.txtKaishuBi.BackColor = System.Drawing.Color.White;
            this.txtKaishuBi.DisplayLength = null;
            this.txtKaishuBi.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtKaishuBi.Location = new System.Drawing.Point(739, 174);
            this.txtKaishuBi.MaxLength = 2;
            this.txtKaishuBi.Name = "txtKaishuBi";
            this.txtKaishuBi.Size = new System.Drawing.Size(33, 20);
            this.txtKaishuBi.TabIndex = 51;
            this.txtKaishuBi.Text = "99";
            this.txtKaishuBi.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKaishuBi.Validating += new System.ComponentModel.CancelEventHandler(this.txtKaishuBi_Validating);
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Silver;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.label2.Location = new System.Drawing.Point(737, 172);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 25);
            this.label2.TabIndex = 52;
            this.label2.Text = "日";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Silver;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.label1.Location = new System.Drawing.Point(499, 172);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(236, 25);
            this.label1.TabIndex = 50;
            this.label1.Text = "0:当月 1:翌月 2:翌々月…";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShzTnkHohoMemo
            // 
            this.lblShzTnkHohoMemo.BackColor = System.Drawing.Color.Silver;
            this.lblShzTnkHohoMemo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShzTnkHohoMemo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblShzTnkHohoMemo.Location = new System.Drawing.Point(499, 145);
            this.lblShzTnkHohoMemo.Name = "lblShzTnkHohoMemo";
            this.lblShzTnkHohoMemo.Size = new System.Drawing.Size(236, 25);
            this.lblShzTnkHohoMemo.TabIndex = 47;
            this.lblShzTnkHohoMemo.Text = "1:明細転嫁 2:伝票転嫁 3:請求転嫁";
            this.lblShzTnkHohoMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShzHsSrMemo
            // 
            this.lblShzHsSrMemo.BackColor = System.Drawing.Color.Silver;
            this.lblShzHsSrMemo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShzHsSrMemo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblShzHsSrMemo.Location = new System.Drawing.Point(499, 118);
            this.lblShzHsSrMemo.Name = "lblShzHsSrMemo";
            this.lblShzHsSrMemo.Size = new System.Drawing.Size(236, 25);
            this.lblShzHsSrMemo.TabIndex = 44;
            this.lblShzHsSrMemo.Text = "1:切り捨て 2:四捨五入 3:切り上げ";
            this.lblShzHsSrMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSkshKskMemo
            // 
            this.lblSkshKskMemo.BackColor = System.Drawing.Color.Silver;
            this.lblSkshKskMemo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSkshKskMemo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblSkshKskMemo.Location = new System.Drawing.Point(499, 92);
            this.lblSkshKskMemo.Name = "lblSkshKskMemo";
            this.lblSkshKskMemo.Size = new System.Drawing.Size(154, 25);
            this.lblSkshKskMemo.TabIndex = 41;
            this.lblSkshKskMemo.Text = "1:合計型 2:明細型";
            this.lblSkshKskMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSkshHkMemo
            // 
            this.lblSkshHkMemo.BackColor = System.Drawing.Color.Silver;
            this.lblSkshHkMemo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSkshHkMemo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblSkshHkMemo.Location = new System.Drawing.Point(499, 65);
            this.lblSkshHkMemo.Name = "lblSkshHkMemo";
            this.lblSkshHkMemo.Size = new System.Drawing.Size(154, 25);
            this.lblSkshHkMemo.TabIndex = 38;
            this.lblSkshHkMemo.Text = "1:する 2:しない";
            this.lblSkshHkMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShimebiMemo
            // 
            this.lblShimebiMemo.BackColor = System.Drawing.Color.Silver;
            this.lblShimebiMemo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShimebiMemo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblShimebiMemo.Location = new System.Drawing.Point(499, 38);
            this.lblShimebiMemo.Name = "lblShimebiMemo";
            this.lblShimebiMemo.Size = new System.Drawing.Size(154, 25);
            this.lblShimebiMemo.TabIndex = 35;
            this.lblShimebiMemo.Text = "1～28 ※末締めは99";
            this.lblShimebiMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSeikyusakiNm
            // 
            this.lblSeikyusakiNm.BackColor = System.Drawing.Color.Silver;
            this.lblSeikyusakiNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSeikyusakiNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblSeikyusakiNm.Location = new System.Drawing.Point(536, 11);
            this.lblSeikyusakiNm.Name = "lblSeikyusakiNm";
            this.lblSeikyusakiNm.Size = new System.Drawing.Size(258, 25);
            this.lblSeikyusakiNm.TabIndex = 32;
            this.lblSeikyusakiNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKaishuTsuki
            // 
            this.txtKaishuTsuki.AllowDrop = true;
            this.txtKaishuTsuki.AutoSizeFromLength = false;
            this.txtKaishuTsuki.BackColor = System.Drawing.Color.White;
            this.txtKaishuTsuki.DisplayLength = null;
            this.txtKaishuTsuki.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtKaishuTsuki.Location = new System.Drawing.Point(462, 175);
            this.txtKaishuTsuki.MaxLength = 1;
            this.txtKaishuTsuki.Name = "txtKaishuTsuki";
            this.txtKaishuTsuki.Size = new System.Drawing.Size(32, 20);
            this.txtKaishuTsuki.TabIndex = 49;
            this.txtKaishuTsuki.Text = "0";
            this.txtKaishuTsuki.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKaishuTsuki.Validating += new System.ComponentModel.CancelEventHandler(this.txtKaishuTsuki_Validating);
            // 
            // txtShohizeiTenkaHoho
            // 
            this.txtShohizeiTenkaHoho.AllowDrop = true;
            this.txtShohizeiTenkaHoho.AutoSizeFromLength = false;
            this.txtShohizeiTenkaHoho.BackColor = System.Drawing.Color.White;
            this.txtShohizeiTenkaHoho.DisplayLength = null;
            this.txtShohizeiTenkaHoho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtShohizeiTenkaHoho.Location = new System.Drawing.Point(462, 148);
            this.txtShohizeiTenkaHoho.MaxLength = 1;
            this.txtShohizeiTenkaHoho.Name = "txtShohizeiTenkaHoho";
            this.txtShohizeiTenkaHoho.Size = new System.Drawing.Size(32, 20);
            this.txtShohizeiTenkaHoho.TabIndex = 46;
            this.txtShohizeiTenkaHoho.Text = "2";
            this.txtShohizeiTenkaHoho.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShohizeiTenkaHoho.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohizeiTenkaHoho_Validating);
            // 
            // textBox14
            // 
            this.textBox14.AllowDrop = true;
            this.textBox14.AutoSizeFromLength = false;
            this.textBox14.DisplayLength = null;
            this.textBox14.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBox14.Location = new System.Drawing.Point(919, -149);
            this.textBox14.MaxLength = 4;
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new System.Drawing.Size(50, 19);
            this.textBox14.TabIndex = 938;
            // 
            // txtShohizeiHasuShori
            // 
            this.txtShohizeiHasuShori.AllowDrop = true;
            this.txtShohizeiHasuShori.AutoSizeFromLength = false;
            this.txtShohizeiHasuShori.BackColor = System.Drawing.Color.White;
            this.txtShohizeiHasuShori.DisplayLength = null;
            this.txtShohizeiHasuShori.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtShohizeiHasuShori.Location = new System.Drawing.Point(462, 121);
            this.txtShohizeiHasuShori.MaxLength = 1;
            this.txtShohizeiHasuShori.Name = "txtShohizeiHasuShori";
            this.txtShohizeiHasuShori.Size = new System.Drawing.Size(32, 20);
            this.txtShohizeiHasuShori.TabIndex = 43;
            this.txtShohizeiHasuShori.Text = "2";
            this.txtShohizeiHasuShori.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShohizeiHasuShori.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohizeiHasuShori_Validating);
            // 
            // txtSeikyushoKeishiki
            // 
            this.txtSeikyushoKeishiki.AllowDrop = true;
            this.txtSeikyushoKeishiki.AutoSizeFromLength = false;
            this.txtSeikyushoKeishiki.BackColor = System.Drawing.Color.White;
            this.txtSeikyushoKeishiki.DisplayLength = null;
            this.txtSeikyushoKeishiki.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtSeikyushoKeishiki.Location = new System.Drawing.Point(462, 94);
            this.txtSeikyushoKeishiki.MaxLength = 1;
            this.txtSeikyushoKeishiki.Name = "txtSeikyushoKeishiki";
            this.txtSeikyushoKeishiki.Size = new System.Drawing.Size(32, 20);
            this.txtSeikyushoKeishiki.TabIndex = 40;
            this.txtSeikyushoKeishiki.Text = "2";
            this.txtSeikyushoKeishiki.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSeikyushoKeishiki.Validating += new System.ComponentModel.CancelEventHandler(this.txtSeikyushoKeishiki_Validating);
            // 
            // txtSeikyushoHakko
            // 
            this.txtSeikyushoHakko.AllowDrop = true;
            this.txtSeikyushoHakko.AutoSizeFromLength = false;
            this.txtSeikyushoHakko.BackColor = System.Drawing.Color.White;
            this.txtSeikyushoHakko.DisplayLength = null;
            this.txtSeikyushoHakko.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtSeikyushoHakko.Location = new System.Drawing.Point(462, 67);
            this.txtSeikyushoHakko.MaxLength = 1;
            this.txtSeikyushoHakko.Name = "txtSeikyushoHakko";
            this.txtSeikyushoHakko.Size = new System.Drawing.Size(32, 20);
            this.txtSeikyushoHakko.TabIndex = 37;
            this.txtSeikyushoHakko.Text = "2";
            this.txtSeikyushoHakko.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSeikyushoHakko.Validating += new System.ComponentModel.CancelEventHandler(this.txtSeikyushoHakko_Validating);
            // 
            // txtShimebi
            // 
            this.txtShimebi.AllowDrop = true;
            this.txtShimebi.AutoSizeFromLength = false;
            this.txtShimebi.BackColor = System.Drawing.Color.White;
            this.txtShimebi.DisplayLength = null;
            this.txtShimebi.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtShimebi.Location = new System.Drawing.Point(462, 40);
            this.txtShimebi.MaxLength = 2;
            this.txtShimebi.Name = "txtShimebi";
            this.txtShimebi.Size = new System.Drawing.Size(32, 20);
            this.txtShimebi.TabIndex = 34;
            this.txtShimebi.Text = "99";
            this.txtShimebi.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShimebi.Validating += new System.ComponentModel.CancelEventHandler(this.txtShimebi_Validating);
            // 
            // txtSeikyusakiCd
            // 
            this.txtSeikyusakiCd.AutoSizeFromLength = true;
            this.txtSeikyusakiCd.BackColor = System.Drawing.SystemColors.Window;
            this.txtSeikyusakiCd.DisplayLength = null;
            this.txtSeikyusakiCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtSeikyusakiCd.Location = new System.Drawing.Point(462, 13);
            this.txtSeikyusakiCd.MaxLength = 4;
            this.txtSeikyusakiCd.Name = "txtSeikyusakiCd";
            this.txtSeikyusakiCd.Size = new System.Drawing.Size(68, 20);
            this.txtSeikyusakiCd.TabIndex = 31;
            this.txtSeikyusakiCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblShzNrkHohoNm
            // 
            this.lblShzNrkHohoNm.BackColor = System.Drawing.Color.Silver;
            this.lblShzNrkHohoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShzNrkHohoNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShzNrkHohoNm.Location = new System.Drawing.Point(129, 307);
            this.lblShzNrkHohoNm.Name = "lblShzNrkHohoNm";
            this.lblShzNrkHohoNm.Size = new System.Drawing.Size(223, 25);
            this.lblShzNrkHohoNm.TabIndex = 29;
            this.lblShzNrkHohoNm.Text = "税抜き入力（自動計算あり）";
            this.lblShzNrkHohoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKgkHsShrMemo
            // 
            this.lblKgkHsShrMemo.BackColor = System.Drawing.Color.Silver;
            this.lblKgkHsShrMemo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKgkHsShrMemo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKgkHsShrMemo.Location = new System.Drawing.Point(129, 281);
            this.lblKgkHsShrMemo.Name = "lblKgkHsShrMemo";
            this.lblKgkHsShrMemo.Size = new System.Drawing.Size(223, 25);
            this.lblKgkHsShrMemo.TabIndex = 26;
            this.lblKgkHsShrMemo.Text = "1:切捨て 2:四捨五入 3:切り上げ";
            this.lblKgkHsShrMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTnkStkHohoMemo
            // 
            this.lblTnkStkHohoMemo.BackColor = System.Drawing.Color.Silver;
            this.lblTnkStkHohoMemo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTnkStkHohoMemo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTnkStkHohoMemo.Location = new System.Drawing.Point(129, 254);
            this.lblTnkStkHohoMemo.Name = "lblTnkStkHohoMemo";
            this.lblTnkStkHohoMemo.Size = new System.Drawing.Size(223, 25);
            this.lblTnkStkHohoMemo.TabIndex = 23;
            this.lblTnkStkHohoMemo.Text = "0:卸単価 1:小売単価 2:前回単価";
            this.lblTnkStkHohoMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTantoshaNm
            // 
            this.lblTantoshaNm.BackColor = System.Drawing.Color.Silver;
            this.lblTantoshaNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTantoshaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTantoshaNm.Location = new System.Drawing.Point(173, 227);
            this.lblTantoshaNm.Name = "lblTantoshaNm";
            this.lblTantoshaNm.Size = new System.Drawing.Size(179, 25);
            this.lblTantoshaNm.TabIndex = 20;
            this.lblTantoshaNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblAddBar
            // 
            this.lblAddBar.AutoSize = true;
            this.lblAddBar.BackColor = System.Drawing.Color.Silver;
            this.lblAddBar.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblAddBar.Location = new System.Drawing.Point(154, 99);
            this.lblAddBar.Name = "lblAddBar";
            this.lblAddBar.Size = new System.Drawing.Size(14, 13);
            this.lblAddBar.TabIndex = 8;
            this.lblAddBar.Text = "-";
            // 
            // txtYubinBango2
            // 
            this.txtYubinBango2.AllowDrop = true;
            this.txtYubinBango2.AutoSizeFromLength = false;
            this.txtYubinBango2.BackColor = System.Drawing.Color.White;
            this.txtYubinBango2.DisplayLength = null;
            this.txtYubinBango2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtYubinBango2.Location = new System.Drawing.Point(172, 94);
            this.txtYubinBango2.MaxLength = 4;
            this.txtYubinBango2.Name = "txtYubinBango2";
            this.txtYubinBango2.Size = new System.Drawing.Size(46, 20);
            this.txtYubinBango2.TabIndex = 9;
            this.txtYubinBango2.Validating += new System.ComponentModel.CancelEventHandler(this.txtYubinBango2_Validating);
            // 
            // txtShohizeiNyuryokuHoho
            // 
            this.txtShohizeiNyuryokuHoho.AllowDrop = true;
            this.txtShohizeiNyuryokuHoho.AutoSizeFromLength = false;
            this.txtShohizeiNyuryokuHoho.BackColor = System.Drawing.Color.White;
            this.txtShohizeiNyuryokuHoho.DisplayLength = null;
            this.txtShohizeiNyuryokuHoho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShohizeiNyuryokuHoho.Location = new System.Drawing.Point(109, 310);
            this.txtShohizeiNyuryokuHoho.MaxLength = 1;
            this.txtShohizeiNyuryokuHoho.Name = "txtShohizeiNyuryokuHoho";
            this.txtShohizeiNyuryokuHoho.Size = new System.Drawing.Size(15, 20);
            this.txtShohizeiNyuryokuHoho.TabIndex = 28;
            this.txtShohizeiNyuryokuHoho.Text = "2";
            this.txtShohizeiNyuryokuHoho.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtKingakuHasuShori
            // 
            this.txtKingakuHasuShori.AllowDrop = true;
            this.txtKingakuHasuShori.AutoSizeFromLength = false;
            this.txtKingakuHasuShori.BackColor = System.Drawing.Color.White;
            this.txtKingakuHasuShori.DisplayLength = null;
            this.txtKingakuHasuShori.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKingakuHasuShori.Location = new System.Drawing.Point(109, 283);
            this.txtKingakuHasuShori.MaxLength = 1;
            this.txtKingakuHasuShori.Name = "txtKingakuHasuShori";
            this.txtKingakuHasuShori.Size = new System.Drawing.Size(15, 20);
            this.txtKingakuHasuShori.TabIndex = 25;
            this.txtKingakuHasuShori.Text = "2";
            this.txtKingakuHasuShori.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKingakuHasuShori.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingakuHasuShori_Validating);
            // 
            // txtTankaShutokuHoho
            // 
            this.txtTankaShutokuHoho.AllowDrop = true;
            this.txtTankaShutokuHoho.AutoSizeFromLength = false;
            this.txtTankaShutokuHoho.BackColor = System.Drawing.Color.White;
            this.txtTankaShutokuHoho.DisplayLength = null;
            this.txtTankaShutokuHoho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTankaShutokuHoho.Location = new System.Drawing.Point(109, 256);
            this.txtTankaShutokuHoho.MaxLength = 1;
            this.txtTankaShutokuHoho.Name = "txtTankaShutokuHoho";
            this.txtTankaShutokuHoho.Size = new System.Drawing.Size(15, 20);
            this.txtTankaShutokuHoho.TabIndex = 22;
            this.txtTankaShutokuHoho.Text = "0";
            this.txtTankaShutokuHoho.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTankaShutokuHoho.Validating += new System.ComponentModel.CancelEventHandler(this.txtTankaShutokuHoho_Validating);
            // 
            // txtTantoshaCd
            // 
            this.txtTantoshaCd.AllowDrop = true;
            this.txtTantoshaCd.AutoSizeFromLength = false;
            this.txtTantoshaCd.BackColor = System.Drawing.Color.White;
            this.txtTantoshaCd.DisplayLength = null;
            this.txtTantoshaCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTantoshaCd.Location = new System.Drawing.Point(109, 229);
            this.txtTantoshaCd.MaxLength = 4;
            this.txtTantoshaCd.Name = "txtTantoshaCd";
            this.txtTantoshaCd.Size = new System.Drawing.Size(58, 20);
            this.txtTantoshaCd.TabIndex = 19;
            this.txtTantoshaCd.Text = "0";
            this.txtTantoshaCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTantoshaCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtTantoshaCd_Validating);
            // 
            // txtFax
            // 
            this.txtFax.AllowDrop = true;
            this.txtFax.AutoSizeFromLength = false;
            this.txtFax.BackColor = System.Drawing.Color.White;
            this.txtFax.DisplayLength = null;
            this.txtFax.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFax.Location = new System.Drawing.Point(109, 202);
            this.txtFax.MaxLength = 15;
            this.txtFax.Name = "txtFax";
            this.txtFax.Size = new System.Drawing.Size(166, 20);
            this.txtFax.TabIndex = 17;
            this.txtFax.Validating += new System.ComponentModel.CancelEventHandler(this.txtFax_Validating);
            // 
            // txtTel
            // 
            this.txtTel.AllowDrop = true;
            this.txtTel.AutoSizeFromLength = false;
            this.txtTel.BackColor = System.Drawing.Color.White;
            this.txtTel.DisplayLength = null;
            this.txtTel.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTel.Location = new System.Drawing.Point(109, 175);
            this.txtTel.MaxLength = 15;
            this.txtTel.Name = "txtTel";
            this.txtTel.Size = new System.Drawing.Size(166, 20);
            this.txtTel.TabIndex = 15;
            this.txtTel.Validating += new System.ComponentModel.CancelEventHandler(this.txtTel_Validating);
            // 
            // txtJusho2
            // 
            this.txtJusho2.AllowDrop = true;
            this.txtJusho2.AutoSizeFromLength = false;
            this.txtJusho2.BackColor = System.Drawing.Color.White;
            this.txtJusho2.DisplayLength = null;
            this.txtJusho2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtJusho2.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtJusho2.Location = new System.Drawing.Point(109, 148);
            this.txtJusho2.MaxLength = 30;
            this.txtJusho2.Name = "txtJusho2";
            this.txtJusho2.Size = new System.Drawing.Size(240, 20);
            this.txtJusho2.TabIndex = 13;
            this.txtJusho2.Validating += new System.ComponentModel.CancelEventHandler(this.txtJusho2_Validating);
            // 
            // txtJusho1
            // 
            this.txtJusho1.AllowDrop = true;
            this.txtJusho1.AutoSizeFromLength = false;
            this.txtJusho1.BackColor = System.Drawing.Color.White;
            this.txtJusho1.DisplayLength = null;
            this.txtJusho1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtJusho1.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtJusho1.Location = new System.Drawing.Point(109, 121);
            this.txtJusho1.MaxLength = 30;
            this.txtJusho1.Name = "txtJusho1";
            this.txtJusho1.Size = new System.Drawing.Size(240, 20);
            this.txtJusho1.TabIndex = 11;
            this.txtJusho1.Validating += new System.ComponentModel.CancelEventHandler(this.txtJusho1_Validating);
            // 
            // txtYubinBango1
            // 
            this.txtYubinBango1.AllowDrop = true;
            this.txtYubinBango1.AutoSizeFromLength = false;
            this.txtYubinBango1.BackColor = System.Drawing.Color.White;
            this.txtYubinBango1.DisplayLength = null;
            this.txtYubinBango1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtYubinBango1.Location = new System.Drawing.Point(109, 94);
            this.txtYubinBango1.MaxLength = 3;
            this.txtYubinBango1.Name = "txtYubinBango1";
            this.txtYubinBango1.Size = new System.Drawing.Size(39, 20);
            this.txtYubinBango1.TabIndex = 7;
            this.txtYubinBango1.Validating += new System.ComponentModel.CancelEventHandler(this.txtYubinBango1_Validating);
            // 
            // txtRyNakagaininNm
            // 
            this.txtRyNakagaininNm.AllowDrop = true;
            this.txtRyNakagaininNm.AutoSizeFromLength = false;
            this.txtRyNakagaininNm.BackColor = System.Drawing.Color.White;
            this.txtRyNakagaininNm.DisplayLength = null;
            this.txtRyNakagaininNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtRyNakagaininNm.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtRyNakagaininNm.Location = new System.Drawing.Point(109, 67);
            this.txtRyNakagaininNm.MaxLength = 20;
            this.txtRyNakagaininNm.Name = "txtRyNakagaininNm";
            this.txtRyNakagaininNm.Size = new System.Drawing.Size(240, 20);
            this.txtRyNakagaininNm.TabIndex = 5;
            this.txtRyNakagaininNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtRyNakagaininNm_Validating);
            // 
            // txtNakagaininKanaNm
            // 
            this.txtNakagaininKanaNm.AllowDrop = true;
            this.txtNakagaininKanaNm.AutoSizeFromLength = false;
            this.txtNakagaininKanaNm.BackColor = System.Drawing.Color.White;
            this.txtNakagaininKanaNm.DisplayLength = null;
            this.txtNakagaininKanaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtNakagaininKanaNm.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtNakagaininKanaNm.Location = new System.Drawing.Point(109, 40);
            this.txtNakagaininKanaNm.MaxLength = 30;
            this.txtNakagaininKanaNm.Name = "txtNakagaininKanaNm";
            this.txtNakagaininKanaNm.Size = new System.Drawing.Size(240, 20);
            this.txtNakagaininKanaNm.TabIndex = 3;
            this.txtNakagaininKanaNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtNakagaininKanaNm_Validating);
            // 
            // txtNakagaininNm
            // 
            this.txtNakagaininNm.AllowDrop = true;
            this.txtNakagaininNm.AutoSizeFromLength = false;
            this.txtNakagaininNm.BackColor = System.Drawing.Color.White;
            this.txtNakagaininNm.DisplayLength = null;
            this.txtNakagaininNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtNakagaininNm.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtNakagaininNm.Location = new System.Drawing.Point(108, 13);
            this.txtNakagaininNm.MaxLength = 40;
            this.txtNakagaininNm.Name = "txtNakagaininNm";
            this.txtNakagaininNm.Size = new System.Drawing.Size(240, 20);
            this.txtNakagaininNm.TabIndex = 1;
            this.txtNakagaininNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtNakagaininNm_Validating);
            // 
            // lblKaishuTsuki
            // 
            this.lblKaishuTsuki.BackColor = System.Drawing.Color.Silver;
            this.lblKaishuTsuki.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKaishuTsuki.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblKaishuTsuki.Location = new System.Drawing.Point(359, 173);
            this.lblKaishuTsuki.Name = "lblKaishuTsuki";
            this.lblKaishuTsuki.Size = new System.Drawing.Size(138, 25);
            this.lblKaishuTsuki.TabIndex = 48;
            this.lblKaishuTsuki.Text = "回収日";
            this.lblKaishuTsuki.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShohizeiTenkaHoho
            // 
            this.lblShohizeiTenkaHoho.BackColor = System.Drawing.Color.Silver;
            this.lblShohizeiTenkaHoho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShohizeiTenkaHoho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblShohizeiTenkaHoho.Location = new System.Drawing.Point(359, 146);
            this.lblShohizeiTenkaHoho.Name = "lblShohizeiTenkaHoho";
            this.lblShohizeiTenkaHoho.Size = new System.Drawing.Size(138, 25);
            this.lblShohizeiTenkaHoho.TabIndex = 45;
            this.lblShohizeiTenkaHoho.Text = "消費税転嫁方法";
            this.lblShohizeiTenkaHoho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShohizeiHasuShori
            // 
            this.lblShohizeiHasuShori.BackColor = System.Drawing.Color.Silver;
            this.lblShohizeiHasuShori.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShohizeiHasuShori.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblShohizeiHasuShori.Location = new System.Drawing.Point(359, 119);
            this.lblShohizeiHasuShori.Name = "lblShohizeiHasuShori";
            this.lblShohizeiHasuShori.Size = new System.Drawing.Size(138, 25);
            this.lblShohizeiHasuShori.TabIndex = 42;
            this.lblShohizeiHasuShori.Text = "消費税端数処理";
            this.lblShohizeiHasuShori.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSeikyushoKeishiki
            // 
            this.lblSeikyushoKeishiki.BackColor = System.Drawing.Color.Silver;
            this.lblSeikyushoKeishiki.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSeikyushoKeishiki.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblSeikyushoKeishiki.Location = new System.Drawing.Point(359, 92);
            this.lblSeikyushoKeishiki.Name = "lblSeikyushoKeishiki";
            this.lblSeikyushoKeishiki.Size = new System.Drawing.Size(138, 25);
            this.lblSeikyushoKeishiki.TabIndex = 39;
            this.lblSeikyushoKeishiki.Text = "請求書形式";
            this.lblSeikyushoKeishiki.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSeikyushoHakko
            // 
            this.lblSeikyushoHakko.BackColor = System.Drawing.Color.Silver;
            this.lblSeikyushoHakko.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSeikyushoHakko.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblSeikyushoHakko.Location = new System.Drawing.Point(359, 65);
            this.lblSeikyushoHakko.Name = "lblSeikyushoHakko";
            this.lblSeikyushoHakko.Size = new System.Drawing.Size(138, 25);
            this.lblSeikyushoHakko.TabIndex = 36;
            this.lblSeikyushoHakko.Text = "請求書発行";
            this.lblSeikyushoHakko.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShimebi
            // 
            this.lblShimebi.BackColor = System.Drawing.Color.Silver;
            this.lblShimebi.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShimebi.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblShimebi.Location = new System.Drawing.Point(359, 38);
            this.lblShimebi.Name = "lblShimebi";
            this.lblShimebi.Size = new System.Drawing.Size(138, 25);
            this.lblShimebi.TabIndex = 33;
            this.lblShimebi.Text = "締日";
            this.lblShimebi.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSeikyusakiCd
            // 
            this.lblSeikyusakiCd.BackColor = System.Drawing.Color.Silver;
            this.lblSeikyusakiCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSeikyusakiCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblSeikyusakiCd.Location = new System.Drawing.Point(359, 11);
            this.lblSeikyusakiCd.Name = "lblSeikyusakiCd";
            this.lblSeikyusakiCd.Size = new System.Drawing.Size(175, 25);
            this.lblSeikyusakiCd.TabIndex = 30;
            this.lblSeikyusakiCd.Text = "請求先コード";
            this.lblSeikyusakiCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShohizeiNyuryokuHoho
            // 
            this.lblShohizeiNyuryokuHoho.BackColor = System.Drawing.Color.Silver;
            this.lblShohizeiNyuryokuHoho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShohizeiNyuryokuHoho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShohizeiNyuryokuHoho.Location = new System.Drawing.Point(6, 308);
            this.lblShohizeiNyuryokuHoho.Name = "lblShohizeiNyuryokuHoho";
            this.lblShohizeiNyuryokuHoho.Size = new System.Drawing.Size(121, 25);
            this.lblShohizeiNyuryokuHoho.TabIndex = 27;
            this.lblShohizeiNyuryokuHoho.Text = "消費税入力方法";
            this.lblShohizeiNyuryokuHoho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKingakuHasuShori
            // 
            this.lblKingakuHasuShori.BackColor = System.Drawing.Color.Silver;
            this.lblKingakuHasuShori.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKingakuHasuShori.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKingakuHasuShori.Location = new System.Drawing.Point(6, 281);
            this.lblKingakuHasuShori.Name = "lblKingakuHasuShori";
            this.lblKingakuHasuShori.Size = new System.Drawing.Size(121, 25);
            this.lblKingakuHasuShori.TabIndex = 24;
            this.lblKingakuHasuShori.Text = "金額端数処理";
            this.lblKingakuHasuShori.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTankaShutokuHoho
            // 
            this.lblTankaShutokuHoho.BackColor = System.Drawing.Color.Silver;
            this.lblTankaShutokuHoho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTankaShutokuHoho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTankaShutokuHoho.Location = new System.Drawing.Point(6, 254);
            this.lblTankaShutokuHoho.Name = "lblTankaShutokuHoho";
            this.lblTankaShutokuHoho.Size = new System.Drawing.Size(121, 25);
            this.lblTankaShutokuHoho.TabIndex = 21;
            this.lblTankaShutokuHoho.Text = "単価取得方法";
            this.lblTankaShutokuHoho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTantoshaCd
            // 
            this.lblTantoshaCd.BackColor = System.Drawing.Color.Silver;
            this.lblTantoshaCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTantoshaCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTantoshaCd.Location = new System.Drawing.Point(6, 227);
            this.lblTantoshaCd.Name = "lblTantoshaCd";
            this.lblTantoshaCd.Size = new System.Drawing.Size(165, 25);
            this.lblTantoshaCd.TabIndex = 18;
            this.lblTantoshaCd.Text = "担当者コード";
            this.lblTantoshaCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblFax
            // 
            this.lblFax.BackColor = System.Drawing.Color.Silver;
            this.lblFax.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFax.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFax.Location = new System.Drawing.Point(6, 200);
            this.lblFax.Name = "lblFax";
            this.lblFax.Size = new System.Drawing.Size(272, 25);
            this.lblFax.TabIndex = 16;
            this.lblFax.Text = "ＦＡＸ番号";
            this.lblFax.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTel
            // 
            this.lblTel.BackColor = System.Drawing.Color.Silver;
            this.lblTel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTel.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTel.Location = new System.Drawing.Point(6, 173);
            this.lblTel.Name = "lblTel";
            this.lblTel.Size = new System.Drawing.Size(272, 25);
            this.lblTel.TabIndex = 14;
            this.lblTel.Text = "電話番号";
            this.lblTel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblJusho2
            // 
            this.lblJusho2.BackColor = System.Drawing.Color.Silver;
            this.lblJusho2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblJusho2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblJusho2.Location = new System.Drawing.Point(6, 146);
            this.lblJusho2.Name = "lblJusho2";
            this.lblJusho2.Size = new System.Drawing.Size(346, 25);
            this.lblJusho2.TabIndex = 12;
            this.lblJusho2.Text = "住所２";
            this.lblJusho2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblJusho1
            // 
            this.lblJusho1.BackColor = System.Drawing.Color.Silver;
            this.lblJusho1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblJusho1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblJusho1.Location = new System.Drawing.Point(6, 119);
            this.lblJusho1.Name = "lblJusho1";
            this.lblJusho1.Size = new System.Drawing.Size(346, 25);
            this.lblJusho1.TabIndex = 10;
            this.lblJusho1.Text = "住所１";
            this.lblJusho1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblYubinBango
            // 
            this.lblYubinBango.BackColor = System.Drawing.Color.Silver;
            this.lblYubinBango.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblYubinBango.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblYubinBango.Location = new System.Drawing.Point(6, 92);
            this.lblYubinBango.Name = "lblYubinBango";
            this.lblYubinBango.Size = new System.Drawing.Size(215, 25);
            this.lblYubinBango.TabIndex = 6;
            this.lblYubinBango.Text = "郵便番号";
            this.lblYubinBango.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblRyNakagaininNm
            // 
            this.lblRyNakagaininNm.BackColor = System.Drawing.Color.Silver;
            this.lblRyNakagaininNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblRyNakagaininNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblRyNakagaininNm.Location = new System.Drawing.Point(6, 65);
            this.lblRyNakagaininNm.Name = "lblRyNakagaininNm";
            this.lblRyNakagaininNm.Size = new System.Drawing.Size(346, 25);
            this.lblRyNakagaininNm.TabIndex = 4;
            this.lblRyNakagaininNm.Text = "略称仲買人名";
            this.lblRyNakagaininNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblNakagaininKanaNm
            // 
            this.lblNakagaininKanaNm.BackColor = System.Drawing.Color.Silver;
            this.lblNakagaininKanaNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblNakagaininKanaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblNakagaininKanaNm.Location = new System.Drawing.Point(6, 38);
            this.lblNakagaininKanaNm.Name = "lblNakagaininKanaNm";
            this.lblNakagaininKanaNm.Size = new System.Drawing.Size(346, 25);
            this.lblNakagaininKanaNm.TabIndex = 2;
            this.lblNakagaininKanaNm.Text = "仲買人カナ名";
            this.lblNakagaininKanaNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblNakagaininNm
            // 
            this.lblNakagaininNm.BackColor = System.Drawing.Color.Silver;
            this.lblNakagaininNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblNakagaininNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblNakagaininNm.Location = new System.Drawing.Point(6, 11);
            this.lblNakagaininNm.Name = "lblNakagaininNm";
            this.lblNakagaininNm.Size = new System.Drawing.Size(346, 25);
            this.lblNakagaininNm.TabIndex = 0;
            this.lblNakagaininNm.Text = "正式仲買人名";
            this.lblNakagaininNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // HNCM1012
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(815, 441);
            this.Controls.Add(this.txtNakagaininCd);
            this.Controls.Add(this.lblNakagaininCd);
            this.Controls.Add(this.gbxSenshuInfo);
            this.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "HNCM1012";
            this.ShowFButton = true;
            this.Text = "ReportSample";
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.gbxSenshuInfo, 0);
            this.Controls.SetChildIndex(this.lblNakagaininCd, 0);
            this.Controls.SetChildIndex(this.txtNakagaininCd, 0);
            this.pnlDebug.ResumeLayout(false);
            this.gbxSenshuInfo.ResumeLayout(false);
            this.gbxSenshuInfo.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblNakagaininCd;
        private jp.co.fsi.common.controls.FsiTextBox txtNakagaininCd;
        private System.Windows.Forms.GroupBox gbxSenshuInfo;
        private System.Windows.Forms.Label lblFax;
        private System.Windows.Forms.Label lblTel;
        private System.Windows.Forms.Label lblJusho2;
        private System.Windows.Forms.Label lblJusho1;
        private System.Windows.Forms.Label lblYubinBango;
        private System.Windows.Forms.Label lblRyNakagaininNm;
        private System.Windows.Forms.Label lblNakagaininKanaNm;
        private System.Windows.Forms.Label lblNakagaininNm;
        private System.Windows.Forms.Label lblKaishuTsuki;
        private System.Windows.Forms.Label lblShohizeiTenkaHoho;
        private System.Windows.Forms.Label lblShohizeiHasuShori;
        private System.Windows.Forms.Label lblSeikyushoKeishiki;
        private System.Windows.Forms.Label lblSeikyushoHakko;
        private System.Windows.Forms.Label lblShimebi;
        private System.Windows.Forms.Label lblSeikyusakiCd;
        private System.Windows.Forms.Label lblShohizeiNyuryokuHoho;
        private System.Windows.Forms.Label lblKingakuHasuShori;
        private System.Windows.Forms.Label lblTankaShutokuHoho;
        private System.Windows.Forms.Label lblTantoshaCd;
        private jp.co.fsi.common.controls.FsiTextBox txtNakagaininNm;
        private jp.co.fsi.common.controls.FsiTextBox txtShohizeiNyuryokuHoho;
        private jp.co.fsi.common.controls.FsiTextBox txtKingakuHasuShori;
        private jp.co.fsi.common.controls.FsiTextBox txtTankaShutokuHoho;
        private jp.co.fsi.common.controls.FsiTextBox txtTantoshaCd;
        private jp.co.fsi.common.controls.FsiTextBox txtFax;
        private jp.co.fsi.common.controls.FsiTextBox txtTel;
        private jp.co.fsi.common.controls.FsiTextBox txtJusho2;
        private jp.co.fsi.common.controls.FsiTextBox txtJusho1;
        private jp.co.fsi.common.controls.FsiTextBox txtYubinBango1;
        private jp.co.fsi.common.controls.FsiTextBox txtRyNakagaininNm;
        private jp.co.fsi.common.controls.FsiTextBox txtNakagaininKanaNm;
        private System.Windows.Forms.Label lblAddBar;
        private jp.co.fsi.common.controls.FsiTextBox txtYubinBango2;
        private System.Windows.Forms.Label lblShzNrkHohoNm;
        private System.Windows.Forms.Label lblKgkHsShrMemo;
        private System.Windows.Forms.Label lblTnkStkHohoMemo;
        private System.Windows.Forms.Label lblTantoshaNm;
        private jp.co.fsi.common.controls.FsiTextBox txtKaishuTsuki;
        private jp.co.fsi.common.controls.FsiTextBox txtShohizeiTenkaHoho;
        private jp.co.fsi.common.controls.FsiTextBox textBox14;
        private jp.co.fsi.common.controls.FsiTextBox txtShohizeiHasuShori;
        private jp.co.fsi.common.controls.FsiTextBox txtSeikyushoKeishiki;
        private jp.co.fsi.common.controls.FsiTextBox txtSeikyushoHakko;
        private jp.co.fsi.common.controls.FsiTextBox txtShimebi;
        private jp.co.fsi.common.controls.FsiTextBox txtSeikyusakiCd;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblShzTnkHohoMemo;
        private System.Windows.Forms.Label lblShzHsSrMemo;
        private System.Windows.Forms.Label lblSkshKskMemo;
        private System.Windows.Forms.Label lblSkshHkMemo;
        private System.Windows.Forms.Label lblShimebiMemo;
        private System.Windows.Forms.Label lblSeikyusakiNm;
        private jp.co.fsi.common.controls.FsiTextBox txtKaishuBi;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button1;
        private common.controls.FsiTextBox FsiTextBox1;
        private common.controls.FsiTextBox txtMyNumber;
        private System.Windows.Forms.Label label3;
        private common.controls.FsiTextBox txthyoji;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;

    }
}