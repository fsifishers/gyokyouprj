﻿namespace jp.co.fsi.han.hanc9031
{
    partial class HANC9032
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtChikuCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lbChikuCd = new System.Windows.Forms.Label();
            this.txtChikuNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.lbChikuNm = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Size = new System.Drawing.Size(383, 23);
            this.lblTitle.TabIndex = 999;
            this.lblTitle.Text = "";
            // 
            // btnF3
            // 
            this.btnF3.Text = "F3\r\n\r\n削除";
            // 
            // btnF6
            // 
            this.btnF6.Text = "F6\r\n\r\n保存";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(5, 22);
            this.pnlDebug.Size = new System.Drawing.Size(416, 100);
            // 
            // txtChikuCd
            // 
            this.txtChikuCd.AutoSizeFromLength = true;
            this.txtChikuCd.DisplayLength = null;
            this.txtChikuCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtChikuCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtChikuCd.Location = new System.Drawing.Point(127, 13);
            this.txtChikuCd.MaxLength = 4;
            this.txtChikuCd.Name = "txtChikuCd";
            this.txtChikuCd.Size = new System.Drawing.Size(34, 20);
            this.txtChikuCd.TabIndex = 1;
            this.txtChikuCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtChikuCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtChikuCd_Validating);
            // 
            // lbChikuCd
            // 
            this.lbChikuCd.BackColor = System.Drawing.Color.Silver;
            this.lbChikuCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbChikuCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lbChikuCd.Location = new System.Drawing.Point(12, 13);
            this.lbChikuCd.Name = "lbChikuCd";
            this.lbChikuCd.Size = new System.Drawing.Size(115, 20);
            this.lbChikuCd.TabIndex = 0;
            this.lbChikuCd.Text = "地区コード";
            this.lbChikuCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtChikuNm
            // 
            this.txtChikuNm.AutoSizeFromLength = false;
            this.txtChikuNm.DisplayLength = null;
            this.txtChikuNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtChikuNm.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtChikuNm.Location = new System.Drawing.Point(127, 41);
            this.txtChikuNm.MaxLength = 40;
            this.txtChikuNm.Name = "txtChikuNm";
            this.txtChikuNm.Size = new System.Drawing.Size(272, 20);
            this.txtChikuNm.TabIndex = 3;
            this.txtChikuNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtChikuNm_Validating);
            // 
            // lbChikuNm
            // 
            this.lbChikuNm.BackColor = System.Drawing.Color.Silver;
            this.lbChikuNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbChikuNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lbChikuNm.Location = new System.Drawing.Point(12, 41);
            this.lbChikuNm.Name = "lbChikuNm";
            this.lbChikuNm.Size = new System.Drawing.Size(115, 20);
            this.lbChikuNm.TabIndex = 2;
            this.lbChikuNm.Text = "地区名";
            this.lbChikuNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // HANC9032
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(408, 125);
            this.Controls.Add(this.txtChikuNm);
            this.Controls.Add(this.lbChikuNm);
            this.Controls.Add(this.txtChikuCd);
            this.Controls.Add(this.lbChikuCd);
            this.Name = "HANC9032";
            this.ShowFButton = true;
            this.Text = "地区の登録";
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lbChikuCd, 0);
            this.Controls.SetChildIndex(this.txtChikuCd, 0);
            this.Controls.SetChildIndex(this.lbChikuNm, 0);
            this.Controls.SetChildIndex(this.txtChikuNm, 0);
            this.pnlDebug.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private jp.co.fsi.common.controls.FsiTextBox txtChikuCd;
        private System.Windows.Forms.Label lbChikuCd;
        private jp.co.fsi.common.controls.FsiTextBox txtChikuNm;
        private System.Windows.Forms.Label lbChikuNm;
    };
}