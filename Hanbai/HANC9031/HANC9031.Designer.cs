﻿namespace jp.co.fsi.han.hanc9031
{
    partial class HANC9031
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblChikuNm = new System.Windows.Forms.Label();
            this.txtChikuNm = new System.Windows.Forms.TextBox();
            this.dgvList = new System.Windows.Forms.DataGridView();
            this.txtChikuCd = new System.Windows.Forms.TextBox();
            this.lblChikuCd = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvList)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.TabIndex = 999;
            this.lblTitle.Text = "地区の登録";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Size = new System.Drawing.Size(847, 100);
            // 
            // lblChikuNm
            // 
            this.lblChikuNm.BackColor = System.Drawing.Color.Silver;
            this.lblChikuNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblChikuNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblChikuNm.Location = new System.Drawing.Point(13, 43);
            this.lblChikuNm.Name = "lblChikuNm";
            this.lblChikuNm.Size = new System.Drawing.Size(85, 20);
            this.lblChikuNm.TabIndex = 2;
            this.lblChikuNm.Text = "地　区　名";
            this.lblChikuNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtChikuNm
            // 
            this.txtChikuNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtChikuNm.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtChikuNm.Location = new System.Drawing.Point(100, 43);
            this.txtChikuNm.MaxLength = 20;
            this.txtChikuNm.Name = "txtChikuNm";
            this.txtChikuNm.Size = new System.Drawing.Size(180, 20);
            this.txtChikuNm.TabIndex = 3;
            this.txtChikuNm.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtChikuNm_KeyDown);
            // 
            // dgvList
            // 
            this.dgvList.AllowUserToAddRows = false;
            this.dgvList.AllowUserToDeleteRows = false;
            this.dgvList.AllowUserToResizeColumns = false;
            this.dgvList.AllowUserToResizeRows = false;
            this.dgvList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvList.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.dgvList.Location = new System.Drawing.Point(13, 70);
            this.dgvList.MultiSelect = false;
            this.dgvList.Name = "dgvList";
            this.dgvList.ReadOnly = true;
            this.dgvList.RowHeadersVisible = false;
            this.dgvList.RowTemplate.Height = 21;
            this.dgvList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvList.Size = new System.Drawing.Size(304, 296);
            this.dgvList.TabIndex = 4;
            this.dgvList.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvList_CellDoubleClick);
            this.dgvList.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvList_KeyDown);
            // 
            // txtChikuCd
            // 
            this.txtChikuCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtChikuCd.ImeMode = System.Windows.Forms.ImeMode.Alpha;
            this.txtChikuCd.Location = new System.Drawing.Point(100, 12);
            this.txtChikuCd.MaxLength = 7;
            this.txtChikuCd.Name = "txtChikuCd";
            this.txtChikuCd.Size = new System.Drawing.Size(180, 20);
            this.txtChikuCd.TabIndex = 1;
            this.txtChikuCd.Visible = false;
            this.txtChikuCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtChikuCd_Validating);
            // 
            // lblChikuCd
            // 
            this.lblChikuCd.BackColor = System.Drawing.Color.Silver;
            this.lblChikuCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblChikuCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblChikuCd.Location = new System.Drawing.Point(13, 12);
            this.lblChikuCd.Name = "lblChikuCd";
            this.lblChikuCd.Size = new System.Drawing.Size(85, 20);
            this.lblChikuCd.TabIndex = 0;
            this.lblChikuCd.Text = "地　区　CD";
            this.lblChikuCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblChikuCd.Visible = false;
            // 
            // HANC9031
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 638);
            this.Controls.Add(this.txtChikuCd);
            this.Controls.Add(this.lblChikuCd);
            this.Controls.Add(this.dgvList);
            this.Controls.Add(this.txtChikuNm);
            this.Controls.Add(this.lblChikuNm);
            this.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.Name = "HANC9031";
            this.Text = "地区の登録";
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.lblChikuNm, 0);
            this.Controls.SetChildIndex(this.txtChikuNm, 0);
            this.Controls.SetChildIndex(this.dgvList, 0);
            this.Controls.SetChildIndex(this.lblChikuCd, 0);
            this.Controls.SetChildIndex(this.txtChikuCd, 0);
            this.pnlDebug.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblChikuNm;
        private System.Windows.Forms.TextBox txtChikuNm;
        private System.Windows.Forms.DataGridView dgvList;
        private System.Windows.Forms.TextBox txtChikuCd;
        private System.Windows.Forms.Label lblChikuCd;

    }
}