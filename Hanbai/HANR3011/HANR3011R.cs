﻿using System;
using System.Data;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

using jp.co.fsi.common.report;
using jp.co.fsi.common.util;

namespace jp.co.fsi.han.hanr3011
{
    /// <summary>
    /// HANR3011R の概要の説明です。
    /// </summary>
    public partial class HANR3011R : BaseReport
    {

        public HANR3011R(DataTable tgtData): base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }
    }
}
