﻿namespace jp.co.fsi.han.hanr3011
{
    /// <summary>
    /// HANR3011R の概要の説明です。
    /// </summary>
    partial class HANR3011R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(HANR3011R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.textBox4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox33 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox36 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.groupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.groupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.textBox15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox21 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox22 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox23 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox24 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.textBox34 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox37 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.reportHeader1 = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.reportFooter1 = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox25 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox26 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox27 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox28 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox29 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox30 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox31 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox32 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox35 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox38 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox39 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox40 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.line1,
            this.label1,
            this.textBox1,
            this.label3,
            this.label4,
            this.label5,
            this.label6,
            this.label7,
            this.label8,
            this.label9,
            this.label10,
            this.label11,
            this.label12,
            this.label13,
            this.label18,
            this.label19,
            this.label20,
            this.textBox39,
            this.textBox40,
            this.txtPage,
            this.label21,
            this.label22});
            this.pageHeader.Height = 0.8047627F;
            this.pageHeader.Name = "pageHeader";
            // 
            // line1
            // 
            this.line1.Height = 0F;
            this.line1.Left = 0F;
            this.line1.LineWeight = 1F;
            this.line1.Name = "line1";
            this.line1.Top = 0.7874016F;
            this.line1.Width = 12.65354F;
            this.line1.X1 = 0F;
            this.line1.X2 = 12.65354F;
            this.line1.Y1 = 0.7874016F;
            this.line1.Y2 = 0.7874016F;
            // 
            // label1
            // 
            this.label1.Height = 0.1574803F;
            this.label1.HyperLink = null;
            this.label1.Left = 0.07874016F;
            this.label1.Name = "label1";
            this.label1.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold";
            this.label1.Text = "セ リ 年 月：\r\n\r\n";
            this.label1.Top = 0.2362205F;
            this.label1.Width = 1.032677F;
            // 
            // textBox1
            // 
            this.textBox1.DataField = "ITEM01";
            this.textBox1.Height = 0.1574803F;
            this.textBox1.Left = 1.181102F;
            this.textBox1.Name = "textBox1";
            this.textBox1.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt";
            this.textBox1.Text = "ITEM01";
            this.textBox1.Top = 0.2362205F;
            this.textBox1.Width = 2.872441F;
            // 
            // label3
            // 
            this.label3.Height = 0.1574803F;
            this.label3.HyperLink = null;
            this.label3.Left = 0.1181102F;
            this.label3.Name = "label3";
            this.label3.Style = "font-family: ＭＳ 明朝; font-size: 10pt; font-weight: bold; text-align: center; ddo-c" +
    "har-set: 1";
            this.label3.Text = "コード";
            this.label3.Top = 0.6299213F;
            this.label3.Width = 0.6299213F;
            // 
            // label4
            // 
            this.label4.Height = 0.1574803F;
            this.label4.HyperLink = null;
            this.label4.Left = 0.8035434F;
            this.label4.Name = "label4";
            this.label4.Style = "font-family: ＭＳ 明朝; font-size: 10pt; font-weight: bold; ddo-char-set: 1";
            this.label4.Text = "組合員\r\n";
            this.label4.Top = 0.6220473F;
            this.label4.Width = 0.6672987F;
            // 
            // label5
            // 
            this.label5.Height = 0.1574803F;
            this.label5.HyperLink = null;
            this.label5.Left = 2.730315F;
            this.label5.Name = "label5";
            this.label5.Style = "font-family: ＭＳ 明朝; font-size: 10pt; font-weight: bold; text-align: center; ddo-c" +
    "har-set: 1";
            this.label5.Text = "日数";
            this.label5.Top = 0.6220473F;
            this.label5.Width = 0.5905512F;
            // 
            // label6
            // 
            this.label6.Height = 0.1574803F;
            this.label6.HyperLink = null;
            this.label6.Left = 3.405118F;
            this.label6.Name = "label6";
            this.label6.Style = "font-family: ＭＳ 明朝; font-size: 10pt; font-weight: bold; text-align: right; ddo-ch" +
    "ar-set: 1";
            this.label6.Text = "数量\r\n";
            this.label6.Top = 0.6220473F;
            this.label6.Width = 0.9287159F;
            // 
            // label7
            // 
            this.label7.Height = 0.1574803F;
            this.label7.HyperLink = null;
            this.label7.Left = 4.389764F;
            this.label7.Name = "label7";
            this.label7.Style = "font-family: ＭＳ 明朝; font-size: 10pt; font-weight: bold; text-align: right; ddo-ch" +
    "ar-set: 1";
            this.label7.Text = "水揚金額";
            this.label7.Top = 0.6220473F;
            this.label7.Width = 1.036197F;
            // 
            // label8
            // 
            this.label8.Height = 0.1574803F;
            this.label8.HyperLink = null;
            this.label8.Left = 5.488977F;
            this.label8.Name = "label8";
            this.label8.Style = "font-family: ＭＳ 明朝; font-size: 10pt; font-weight: bold; text-align: right; ddo-ch" +
    "ar-set: 1";
            this.label8.Text = "消費税\r\n";
            this.label8.Top = 0.6220473F;
            this.label8.Width = 0.8972259F;
            // 
            // label9
            // 
            this.label9.Height = 0.1574803F;
            this.label9.HyperLink = null;
            this.label9.Left = 6.45F;
            this.label9.Name = "label9";
            this.label9.Style = "font-family: ＭＳ 明朝; font-size: 10pt; font-weight: bold; text-align: right; ddo-ch" +
    "ar-set: 1";
            this.label9.Text = "販売未払金";
            this.label9.Top = 0.6220473F;
            this.label9.Width = 1.014174F;
            // 
            // label10
            // 
            this.label10.Height = 0.1574803F;
            this.label10.HyperLink = null;
            this.label10.Left = 7.537796F;
            this.label10.Name = "label10";
            this.label10.Style = "font-family: ＭＳ 明朝; font-size: 10pt; font-weight: bold; text-align: right; ddo-ch" +
    "ar-set: 1";
            this.label10.Text = "漁協手数料";
            this.label10.Top = 0.6220473F;
            this.label10.Width = 0.9078636F;
            // 
            // label11
            // 
            this.label11.Height = 0.1574803F;
            this.label11.HyperLink = null;
            this.label11.Left = 8.520473F;
            this.label11.Name = "label11";
            this.label11.Style = "font-family: ＭＳ 明朝; font-size: 10pt; font-weight: bold; text-align: right; ddo-ch" +
    "ar-set: 1";
            this.label11.Text = "箱代";
            this.label11.Top = 0.6220473F;
            this.label11.Width = 0.4917316F;
            // 
            // label12
            // 
            this.label12.Height = 0.1574803F;
            this.label12.HyperLink = null;
            this.label12.Left = 9.048032F;
            this.label12.Name = "label12";
            this.label12.Style = "font-family: ＭＳ 明朝; font-size: 10pt; font-weight: bold; text-align: right; ddo-ch" +
    "ar-set: 1";
            this.label12.Text = "その他の控除";
            this.label12.Top = 0.6220473F;
            this.label12.Width = 0.9385834F;
            // 
            // label13
            // 
            this.label13.Height = 0.1574803F;
            this.label13.HyperLink = null;
            this.label13.Left = 10.0748F;
            this.label13.Name = "label13";
            this.label13.Style = "font-family: ＭＳ 明朝; font-size: 10pt; font-weight: bold; text-align: right; ddo-ch" +
    "ar-set: 1";
            this.label13.Text = "積立金";
            this.label13.Top = 0.6220473F;
            this.label13.Width = 0.8083086F;
            // 
            // label18
            // 
            this.label18.Height = 0.3149607F;
            this.label18.HyperLink = null;
            this.label18.Left = 4.556693F;
            this.label18.Name = "label18";
            this.label18.Style = "font-family: ＭＳ 明朝; font-size: 15pt; font-weight: bold; text-align: center; ddo-c" +
    "har-set: 128";
            this.label18.Text = "水揚仕切月計表";
            this.label18.Top = 0F;
            this.label18.Width = 3.333333F;
            // 
            // label19
            // 
            this.label19.Height = 0.1574803F;
            this.label19.HyperLink = null;
            this.label19.Left = 10.93661F;
            this.label19.Name = "label19";
            this.label19.Style = "font-family: ＭＳ 明朝; font-size: 10pt; font-weight: bold; text-align: right; ddo-ch" +
    "ar-set: 1";
            this.label19.Text = "預り金";
            this.label19.Top = 0.6220473F;
            this.label19.Width = 0.7874498F;
            // 
            // label20
            // 
            this.label20.Height = 0.1574803F;
            this.label20.HyperLink = null;
            this.label20.Left = 11.77677F;
            this.label20.Name = "label20";
            this.label20.Style = "font-family: ＭＳ 明朝; font-size: 10pt; font-weight: bold; text-align: right; ddo-ch" +
    "ar-set: 1";
            this.label20.Text = "差引金額";
            this.label20.Top = 0.6220473F;
            this.label20.Width = 0.8082676F;
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.textBox4,
            this.textBox5,
            this.textBox6,
            this.textBox7,
            this.textBox8,
            this.textBox9,
            this.textBox10,
            this.textBox11,
            this.textBox12,
            this.textBox13,
            this.textBox14,
            this.textBox33,
            this.textBox36});
            this.detail.Height = 0.2165354F;
            this.detail.Name = "detail";
            // 
            // textBox4
            // 
            this.textBox4.DataField = "ITEM04";
            this.textBox4.Height = 0.1574803F;
            this.textBox4.Left = 0.1181102F;
            this.textBox4.Name = "textBox4";
            this.textBox4.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: center; ddo-char-set: 128";
            this.textBox4.Text = "ITEM04\r\n";
            this.textBox4.Top = 0.007874017F;
            this.textBox4.Width = 0.6299214F;
            // 
            // textBox5
            // 
            this.textBox5.DataField = "ITEM05";
            this.textBox5.Height = 0.1574803F;
            this.textBox5.Left = 0.8035434F;
            this.textBox5.Name = "textBox5";
            this.textBox5.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; ddo-char-set: 128";
            this.textBox5.Text = "ITEM05";
            this.textBox5.Top = 0.007874016F;
            this.textBox5.Width = 1.840945F;
            // 
            // textBox6
            // 
            this.textBox6.DataField = "ITEM06";
            this.textBox6.Height = 0.1574803F;
            this.textBox6.Left = 2.730315F;
            this.textBox6.Name = "textBox6";
            this.textBox6.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: center; ddo-char-set: 128";
            this.textBox6.Text = "ITEM01";
            this.textBox6.Top = 0.007874016F;
            this.textBox6.Width = 0.5905512F;
            // 
            // textBox7
            // 
            this.textBox7.DataField = "ITEM07";
            this.textBox7.Height = 0.1574803F;
            this.textBox7.Left = 3.376378F;
            this.textBox7.Name = "textBox7";
            this.textBox7.OutputFormat = resources.GetString("textBox7.OutputFormat");
            this.textBox7.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 128";
            this.textBox7.Text = "ITEM07\r\n";
            this.textBox7.Top = 0.007874017F;
            this.textBox7.Width = 0.9287412F;
            // 
            // textBox8
            // 
            this.textBox8.DataField = "ITEM08";
            this.textBox8.Height = 0.1574803F;
            this.textBox8.Left = 4.389764F;
            this.textBox8.Name = "textBox8";
            this.textBox8.OutputFormat = resources.GetString("textBox8.OutputFormat");
            this.textBox8.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 128";
            this.textBox8.Text = "ITEM08";
            this.textBox8.Top = 0.007874017F;
            this.textBox8.Width = 1.00748F;
            // 
            // textBox9
            // 
            this.textBox9.DataField = "ITEM09";
            this.textBox9.Height = 0.1574803F;
            this.textBox9.Left = 5.460237F;
            this.textBox9.Name = "textBox9";
            this.textBox9.OutputFormat = resources.GetString("textBox9.OutputFormat");
            this.textBox9.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 128";
            this.textBox9.Text = "ITEM09";
            this.textBox9.Top = 0.007874017F;
            this.textBox9.Width = 0.897244F;
            // 
            // textBox10
            // 
            this.textBox10.DataField = "ITEM10";
            this.textBox10.Height = 0.1574803F;
            this.textBox10.Left = 6.42126F;
            this.textBox10.Name = "textBox10";
            this.textBox10.OutputFormat = resources.GetString("textBox10.OutputFormat");
            this.textBox10.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 128";
            this.textBox10.Text = "ITEM10";
            this.textBox10.Top = 0.007874017F;
            this.textBox10.Width = 1.014173F;
            // 
            // textBox11
            // 
            this.textBox11.DataField = "ITEM11";
            this.textBox11.Height = 0.1574803F;
            this.textBox11.Left = 7.509056F;
            this.textBox11.Name = "textBox11";
            this.textBox11.OutputFormat = resources.GetString("textBox11.OutputFormat");
            this.textBox11.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 128";
            this.textBox11.Text = "ITEM11";
            this.textBox11.Top = 0.007874017F;
            this.textBox11.Width = 0.9078684F;
            // 
            // textBox12
            // 
            this.textBox12.DataField = "ITEM12";
            this.textBox12.Height = 0.1574803F;
            this.textBox12.Left = 8.520473F;
            this.textBox12.Name = "textBox12";
            this.textBox12.OutputFormat = resources.GetString("textBox12.OutputFormat");
            this.textBox12.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 128";
            this.textBox12.Text = "ITEM12";
            this.textBox12.Top = 0.007874017F;
            this.textBox12.Width = 0.4917316F;
            // 
            // textBox13
            // 
            this.textBox13.DataField = "ITEM13";
            this.textBox13.Height = 0.1574803F;
            this.textBox13.Left = 9.048032F;
            this.textBox13.Name = "textBox13";
            this.textBox13.OutputFormat = resources.GetString("textBox13.OutputFormat");
            this.textBox13.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 128";
            this.textBox13.Text = "ITEM13";
            this.textBox13.Top = 0.007874017F;
            this.textBox13.Width = 0.9385834F;
            // 
            // textBox14
            // 
            this.textBox14.DataField = "ITEM14";
            this.textBox14.Height = 0.1574803F;
            this.textBox14.Left = 10.0748F;
            this.textBox14.Name = "textBox14";
            this.textBox14.OutputFormat = resources.GetString("textBox14.OutputFormat");
            this.textBox14.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 128";
            this.textBox14.Text = "ITEM14";
            this.textBox14.Top = 0.007874017F;
            this.textBox14.Width = 0.8082609F;
            // 
            // textBox33
            // 
            this.textBox33.DataField = "ITEM15";
            this.textBox33.Height = 0.1574803F;
            this.textBox33.Left = 10.93661F;
            this.textBox33.Name = "textBox33";
            this.textBox33.OutputFormat = resources.GetString("textBox33.OutputFormat");
            this.textBox33.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 128";
            this.textBox33.Text = "ITEM15";
            this.textBox33.Top = 0.007874017F;
            this.textBox33.Width = 0.7874021F;
            // 
            // textBox36
            // 
            this.textBox36.DataField = "ITEM16";
            this.textBox36.Height = 0.1574803F;
            this.textBox36.Left = 11.77677F;
            this.textBox36.Name = "textBox36";
            this.textBox36.OutputFormat = resources.GetString("textBox36.OutputFormat");
            this.textBox36.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 128";
            this.textBox36.Text = "ITEM16";
            this.textBox36.Top = 0.007874017F;
            this.textBox36.Width = 0.8082199F;
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            // 
            // groupHeader1
            // 
            this.groupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.label2,
            this.textBox2,
            this.textBox3});
            this.groupHeader1.DataField = "ITEM02";
            this.groupHeader1.Height = 0.2136647F;
            this.groupHeader1.Name = "groupHeader1";
            // 
            // label2
            // 
            this.label2.Height = 0.1979167F;
            this.label2.HyperLink = null;
            this.label2.Left = 0.0937008F;
            this.label2.Name = "label2";
            this.label2.Style = "vertical-align: middle";
            this.label2.Text = "＜＜　　  　　　　　　　　　　　　　　　　　　　　　＞＞";
            this.label2.Top = 0.01574803F;
            this.label2.Width = 2.843701F;
            // 
            // textBox2
            // 
            this.textBox2.DataField = "ITEM02";
            this.textBox2.Height = 0.1979167F;
            this.textBox2.Left = 0.6996064F;
            this.textBox2.Name = "textBox2";
            this.textBox2.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; vertical-align: middle";
            this.textBox2.Text = "ITEM02\r\n";
            this.textBox2.Top = 0.01574803F;
            this.textBox2.Width = 0.5511812F;
            // 
            // textBox3
            // 
            this.textBox3.DataField = "ITEM03";
            this.textBox3.Height = 0.1979167F;
            this.textBox3.Left = 1.250787F;
            this.textBox3.Name = "textBox3";
            this.textBox3.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; vertical-align: middle";
            this.textBox3.Text = "ITEM03\r\n";
            this.textBox3.Top = 0.01574803F;
            this.textBox3.Width = 1.268504F;
            // 
            // groupFooter1
            // 
            this.groupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.textBox15,
            this.label15,
            this.textBox17,
            this.textBox18,
            this.textBox19,
            this.textBox20,
            this.textBox21,
            this.textBox22,
            this.textBox23,
            this.textBox24,
            this.line2,
            this.textBox34,
            this.textBox37,
            this.label14});
            this.groupFooter1.Height = 0.2195428F;
            this.groupFooter1.Name = "groupFooter1";
            // 
            // textBox15
            // 
            this.textBox15.DataField = "ITEM05";
            this.textBox15.Height = 0.1665354F;
            this.textBox15.Left = 1.181102F;
            this.textBox15.Name = "textBox15";
            this.textBox15.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: bottom;" +
    " ddo-char-set: 128";
            this.textBox15.SummaryFunc = GrapeCity.ActiveReports.SectionReportModel.SummaryFunc.Count;
            this.textBox15.SummaryGroup = "groupHeader1";
            this.textBox15.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox15.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox15.Text = "ITEM05";
            this.textBox15.Top = 0.01181102F;
            this.textBox15.Width = 0.5511811F;
            // 
            // label15
            // 
            this.label15.Height = 0.1665354F;
            this.label15.HyperLink = null;
            this.label15.Left = 1.751969F;
            this.label15.Name = "label15";
            this.label15.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; vertical-align: top; ddo-char-set: 128";
            this.label15.Text = "人";
            this.label15.Top = 0.01181102F;
            this.label15.Width = 0.2082677F;
            // 
            // textBox17
            // 
            this.textBox17.DataField = "ITEM07";
            this.textBox17.Height = 0.1346457F;
            this.textBox17.Left = 3.376379F;
            this.textBox17.Name = "textBox17";
            this.textBox17.OutputFormat = resources.GetString("textBox17.OutputFormat");
            this.textBox17.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 128";
            this.textBox17.SummaryGroup = "groupHeader1";
            this.textBox17.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox17.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox17.Text = "ITEM07\r\n";
            this.textBox17.Top = 0.01181102F;
            this.textBox17.Width = 0.9287407F;
            // 
            // textBox18
            // 
            this.textBox18.DataField = "ITEM08";
            this.textBox18.Height = 0.1346457F;
            this.textBox18.Left = 4.389765F;
            this.textBox18.Name = "textBox18";
            this.textBox18.OutputFormat = resources.GetString("textBox18.OutputFormat");
            this.textBox18.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 128";
            this.textBox18.SummaryGroup = "groupHeader1";
            this.textBox18.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox18.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox18.Text = "ITEM08\r\n";
            this.textBox18.Top = 0.01181102F;
            this.textBox18.Width = 1.00748F;
            // 
            // textBox19
            // 
            this.textBox19.DataField = "ITEM09";
            this.textBox19.Height = 0.1346457F;
            this.textBox19.Left = 5.460237F;
            this.textBox19.Name = "textBox19";
            this.textBox19.OutputFormat = resources.GetString("textBox19.OutputFormat");
            this.textBox19.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 128";
            this.textBox19.SummaryGroup = "groupHeader1";
            this.textBox19.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox19.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox19.Text = "ITEM09";
            this.textBox19.Top = 0.01193407F;
            this.textBox19.Width = 0.897244F;
            // 
            // textBox20
            // 
            this.textBox20.DataField = "ITEM10";
            this.textBox20.Height = 0.1346457F;
            this.textBox20.Left = 6.42126F;
            this.textBox20.Name = "textBox20";
            this.textBox20.OutputFormat = resources.GetString("textBox20.OutputFormat");
            this.textBox20.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 128";
            this.textBox20.SummaryGroup = "groupHeader1";
            this.textBox20.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox20.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox20.Text = "ITEM10";
            this.textBox20.Top = 0.01181102F;
            this.textBox20.Width = 1.014173F;
            // 
            // textBox21
            // 
            this.textBox21.DataField = "ITEM11";
            this.textBox21.Height = 0.1346457F;
            this.textBox21.Left = 7.509056F;
            this.textBox21.Name = "textBox21";
            this.textBox21.OutputFormat = resources.GetString("textBox21.OutputFormat");
            this.textBox21.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 128";
            this.textBox21.SummaryGroup = "groupHeader1";
            this.textBox21.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox21.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox21.Text = "ITEM11";
            this.textBox21.Top = 0.01181102F;
            this.textBox21.Width = 0.9078684F;
            // 
            // textBox22
            // 
            this.textBox22.DataField = "ITEM12";
            this.textBox22.Height = 0.1346457F;
            this.textBox22.Left = 8.520473F;
            this.textBox22.Name = "textBox22";
            this.textBox22.OutputFormat = resources.GetString("textBox22.OutputFormat");
            this.textBox22.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 128";
            this.textBox22.SummaryGroup = "groupHeader1";
            this.textBox22.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox22.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox22.Text = "ITEM12";
            this.textBox22.Top = 0.01181102F;
            this.textBox22.Width = 0.4917316F;
            // 
            // textBox23
            // 
            this.textBox23.DataField = "ITEM13";
            this.textBox23.Height = 0.1346457F;
            this.textBox23.Left = 9.048032F;
            this.textBox23.Name = "textBox23";
            this.textBox23.OutputFormat = resources.GetString("textBox23.OutputFormat");
            this.textBox23.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 128";
            this.textBox23.SummaryGroup = "groupHeader1";
            this.textBox23.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox23.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox23.Text = "ITEM13\r\n";
            this.textBox23.Top = 0.01181102F;
            this.textBox23.Width = 0.9385834F;
            // 
            // textBox24
            // 
            this.textBox24.DataField = "ITEM14";
            this.textBox24.Height = 0.1346457F;
            this.textBox24.Left = 10.0748F;
            this.textBox24.Name = "textBox24";
            this.textBox24.OutputFormat = resources.GetString("textBox24.OutputFormat");
            this.textBox24.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 128";
            this.textBox24.SummaryGroup = "groupHeader1";
            this.textBox24.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox24.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox24.Text = "ITEM14\r\n";
            this.textBox24.Top = 0.01181102F;
            this.textBox24.Width = 0.80826F;
            // 
            // line2
            // 
            this.line2.Height = 0F;
            this.line2.Left = 0F;
            this.line2.LineWeight = 1F;
            this.line2.Name = "line2";
            this.line2.Top = 0.2125984F;
            this.line2.Width = 12.65354F;
            this.line2.X1 = 0F;
            this.line2.X2 = 12.65354F;
            this.line2.Y1 = 0.2125984F;
            this.line2.Y2 = 0.2125984F;
            // 
            // textBox34
            // 
            this.textBox34.DataField = "ITEM15";
            this.textBox34.Height = 0.1346457F;
            this.textBox34.Left = 10.93661F;
            this.textBox34.Name = "textBox34";
            this.textBox34.OutputFormat = resources.GetString("textBox34.OutputFormat");
            this.textBox34.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 128";
            this.textBox34.SummaryGroup = "groupHeader1";
            this.textBox34.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox34.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox34.Text = "ITEM15";
            this.textBox34.Top = 0.01181102F;
            this.textBox34.Width = 0.7874014F;
            // 
            // textBox37
            // 
            this.textBox37.DataField = "ITEM16";
            this.textBox37.Height = 0.1346457F;
            this.textBox37.Left = 11.77677F;
            this.textBox37.Name = "textBox37";
            this.textBox37.OutputFormat = resources.GetString("textBox37.OutputFormat");
            this.textBox37.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 128";
            this.textBox37.SummaryGroup = "groupHeader1";
            this.textBox37.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox37.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox37.Text = "ITEM16";
            this.textBox37.Top = 0.01181102F;
            this.textBox37.Width = 0.8082191F;
            // 
            // label14
            // 
            this.label14.Height = 0.1665354F;
            this.label14.HyperLink = null;
            this.label14.Left = 0.3716536F;
            this.label14.Name = "label14";
            this.label14.Style = "font-family: ＭＳ 明朝; font-size: 10pt; font-weight: bold; ddo-char-set: 1";
            this.label14.Text = "小計";
            this.label14.Top = 0.01181102F;
            this.label14.Width = 0.376378F;
            // 
            // reportHeader1
            // 
            this.reportHeader1.Height = 0F;
            this.reportHeader1.Name = "reportHeader1";
            // 
            // reportFooter1
            // 
            this.reportFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.label16,
            this.textBox16,
            this.label17,
            this.textBox25,
            this.textBox26,
            this.textBox27,
            this.textBox28,
            this.textBox29,
            this.textBox30,
            this.textBox31,
            this.textBox32,
            this.textBox35,
            this.textBox38});
            this.reportFooter1.Height = 0.2165354F;
            this.reportFooter1.Name = "reportFooter1";
            // 
            // label16
            // 
            this.label16.Height = 0.1666667F;
            this.label16.HyperLink = null;
            this.label16.Left = 0.3716536F;
            this.label16.Name = "label16";
            this.label16.Style = "font-family: ＭＳ 明朝; font-size: 10pt; font-weight: bold; ddo-char-set: 1";
            this.label16.Text = "総計\r\n";
            this.label16.Top = 0.02362205F;
            this.label16.Width = 0.376378F;
            // 
            // textBox16
            // 
            this.textBox16.DataField = "ITEM05";
            this.textBox16.Height = 0.1666667F;
            this.textBox16.Left = 1.181102F;
            this.textBox16.Name = "textBox16";
            this.textBox16.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: bottom";
            this.textBox16.SummaryFunc = GrapeCity.ActiveReports.SectionReportModel.SummaryFunc.Count;
            this.textBox16.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.textBox16.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.textBox16.Text = "ITEM05";
            this.textBox16.Top = 0.02362205F;
            this.textBox16.Width = 0.5511816F;
            // 
            // label17
            // 
            this.label17.Height = 0.1666667F;
            this.label17.HyperLink = null;
            this.label17.Left = 1.751969F;
            this.label17.Name = "label17";
            this.label17.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; vertical-align: top";
            this.label17.Text = "人";
            this.label17.Top = 0.02362205F;
            this.label17.Width = 0.2083333F;
            // 
            // textBox25
            // 
            this.textBox25.DataField = "ITEM07";
            this.textBox25.Height = 0.1666667F;
            this.textBox25.Left = 3.376378F;
            this.textBox25.Name = "textBox25";
            this.textBox25.OutputFormat = resources.GetString("textBox25.OutputFormat");
            this.textBox25.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right";
            this.textBox25.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.textBox25.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.textBox25.Text = "ITEM07\r\n";
            this.textBox25.Top = 0.02362205F;
            this.textBox25.Width = 0.9287407F;
            // 
            // textBox26
            // 
            this.textBox26.DataField = "ITEM08";
            this.textBox26.Height = 0.1666667F;
            this.textBox26.Left = 4.389764F;
            this.textBox26.Name = "textBox26";
            this.textBox26.OutputFormat = resources.GetString("textBox26.OutputFormat");
            this.textBox26.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right";
            this.textBox26.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.textBox26.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.textBox26.Text = "ITEM08\r\n";
            this.textBox26.Top = 0.02362205F;
            this.textBox26.Width = 1.00748F;
            // 
            // textBox27
            // 
            this.textBox27.DataField = "ITEM09";
            this.textBox27.Height = 0.1666667F;
            this.textBox27.Left = 5.460237F;
            this.textBox27.Name = "textBox27";
            this.textBox27.OutputFormat = resources.GetString("textBox27.OutputFormat");
            this.textBox27.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right";
            this.textBox27.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.textBox27.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.textBox27.Text = "ITEM09";
            this.textBox27.Top = 0.02362205F;
            this.textBox27.Width = 0.897244F;
            // 
            // textBox28
            // 
            this.textBox28.DataField = "ITEM10";
            this.textBox28.Height = 0.1666667F;
            this.textBox28.Left = 6.42126F;
            this.textBox28.Name = "textBox28";
            this.textBox28.OutputFormat = resources.GetString("textBox28.OutputFormat");
            this.textBox28.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right";
            this.textBox28.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.textBox28.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.textBox28.Text = "ITEM10";
            this.textBox28.Top = 0.02362205F;
            this.textBox28.Width = 1.014173F;
            // 
            // textBox29
            // 
            this.textBox29.DataField = "ITEM11";
            this.textBox29.Height = 0.1666667F;
            this.textBox29.Left = 7.509056F;
            this.textBox29.Name = "textBox29";
            this.textBox29.OutputFormat = resources.GetString("textBox29.OutputFormat");
            this.textBox29.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right";
            this.textBox29.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.textBox29.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.textBox29.Text = "ITEM11";
            this.textBox29.Top = 0.02362205F;
            this.textBox29.Width = 0.9078684F;
            // 
            // textBox30
            // 
            this.textBox30.DataField = "ITEM12";
            this.textBox30.Height = 0.1666667F;
            this.textBox30.Left = 8.520473F;
            this.textBox30.Name = "textBox30";
            this.textBox30.OutputFormat = resources.GetString("textBox30.OutputFormat");
            this.textBox30.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right";
            this.textBox30.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.textBox30.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.textBox30.Text = "ITEM12";
            this.textBox30.Top = 0.02362205F;
            this.textBox30.Width = 0.4917316F;
            // 
            // textBox31
            // 
            this.textBox31.DataField = "ITEM13";
            this.textBox31.Height = 0.1666667F;
            this.textBox31.Left = 9.048032F;
            this.textBox31.Name = "textBox31";
            this.textBox31.OutputFormat = resources.GetString("textBox31.OutputFormat");
            this.textBox31.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right";
            this.textBox31.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.textBox31.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.textBox31.Text = "ITEM13\r\n";
            this.textBox31.Top = 0.02362205F;
            this.textBox31.Width = 0.9385834F;
            // 
            // textBox32
            // 
            this.textBox32.DataField = "ITEM14";
            this.textBox32.Height = 0.1666667F;
            this.textBox32.Left = 10.0748F;
            this.textBox32.Name = "textBox32";
            this.textBox32.OutputFormat = resources.GetString("textBox32.OutputFormat");
            this.textBox32.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right";
            this.textBox32.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.textBox32.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.textBox32.Text = "ITEM14\r\n";
            this.textBox32.Top = 0.02362205F;
            this.textBox32.Width = 0.80826F;
            // 
            // textBox35
            // 
            this.textBox35.DataField = "ITEM15";
            this.textBox35.Height = 0.1666667F;
            this.textBox35.Left = 10.93661F;
            this.textBox35.Name = "textBox35";
            this.textBox35.OutputFormat = resources.GetString("textBox35.OutputFormat");
            this.textBox35.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right";
            this.textBox35.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.textBox35.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.textBox35.Text = "ITEM15";
            this.textBox35.Top = 0.02362205F;
            this.textBox35.Width = 0.7874014F;
            // 
            // textBox38
            // 
            this.textBox38.DataField = "ITEM16";
            this.textBox38.Height = 0.1666667F;
            this.textBox38.Left = 11.77677F;
            this.textBox38.Name = "textBox38";
            this.textBox38.OutputFormat = resources.GetString("textBox38.OutputFormat");
            this.textBox38.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right";
            this.textBox38.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.textBox38.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.textBox38.Text = "ITEM16";
            this.textBox38.Top = 0.02362205F;
            this.textBox38.Width = 0.8082191F;
            // 
            // textBox39
            // 
            this.textBox39.DataField = "ITEM17";
            this.textBox39.Height = 0.1574803F;
            this.textBox39.Left = 11.1248F;
            this.textBox39.Name = "textBox39";
            this.textBox39.OutputFormat = resources.GetString("textBox39.OutputFormat");
            this.textBox39.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: center; ddo-char-set: 128";
            this.textBox39.Text = "精算区分";
            this.textBox39.Top = 0F;
            this.textBox39.Width = 0.8082201F;
            // 
            // textBox40
            // 
            this.textBox40.DataField = "ITEM18";
            this.textBox40.Height = 0.1574803F;
            this.textBox40.Left = 11.51614F;
            this.textBox40.Name = "textBox40";
            this.textBox40.OutputFormat = resources.GetString("textBox40.OutputFormat");
            this.textBox40.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; ddo-char-set: 128";
            this.textBox40.Text = "日付";
            this.textBox40.Top = 0.1968504F;
            this.textBox40.Width = 0.8082201F;
            // 
            // txtPage
            // 
            this.txtPage.Height = 0.2F;
            this.txtPage.Left = 11.51614F;
            this.txtPage.MultiLine = false;
            this.txtPage.Name = "txtPage";
            this.txtPage.OutputFormat = resources.GetString("txtPage.OutputFormat");
            this.txtPage.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: left; vertical-align: middle; dd" +
    "o-char-set: 1";
            this.txtPage.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtPage.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.txtPage.Top = 0.3937008F;
            this.txtPage.Width = 0.4169283F;
            // 
            // label21
            // 
            this.label21.Height = 0.1574803F;
            this.label21.HyperLink = null;
            this.label21.Left = 10.4563F;
            this.label21.Name = "label21";
            this.label21.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right";
            this.label21.Text = "日　付：\r\n\r\n";
            this.label21.Top = 0.1968504F;
            this.label21.Width = 1.032677F;
            // 
            // label22
            // 
            this.label22.Height = 0.1574803F;
            this.label22.HyperLink = null;
            this.label22.Left = 10.4563F;
            this.label22.Name = "label22";
            this.label22.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right";
            this.label22.Text = "ページ：\r\n\r\n";
            this.label22.Top = 0.4362205F;
            this.label22.Width = 1.032677F;
            // 
            // HANR3011R
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.2362205F;
            this.PageSettings.Margins.Left = 1.133858F;
            this.PageSettings.Margins.Right = 0.5433071F;
            this.PageSettings.Margins.Top = 0.5905512F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
            this.PageSettings.PaperHeight = 13.89764F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.B4;
            this.PageSettings.PaperWidth = 9.84252F;
            this.PrintWidth = 12.6537F;
            this.Sections.Add(this.reportHeader1);
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.groupHeader1);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.groupFooter1);
            this.Sections.Add(this.pageFooter);
            this.Sections.Add(this.reportFooter1);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label3;
        private GrapeCity.ActiveReports.SectionReportModel.Label label4;
        private GrapeCity.ActiveReports.SectionReportModel.Label label5;
        private GrapeCity.ActiveReports.SectionReportModel.Label label6;
        private GrapeCity.ActiveReports.SectionReportModel.Label label7;
        private GrapeCity.ActiveReports.SectionReportModel.Label label8;
        private GrapeCity.ActiveReports.SectionReportModel.Label label9;
        private GrapeCity.ActiveReports.SectionReportModel.Label label10;
        private GrapeCity.ActiveReports.SectionReportModel.Label label11;
        private GrapeCity.ActiveReports.SectionReportModel.Label label12;
        private GrapeCity.ActiveReports.SectionReportModel.Label label13;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox13;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox14;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader groupHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox3;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter groupFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label14;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox15;
        private GrapeCity.ActiveReports.SectionReportModel.ReportHeader reportHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.ReportFooter reportFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label15;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox17;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox18;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox19;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox20;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox21;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox22;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox23;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox24;
        private GrapeCity.ActiveReports.SectionReportModel.Label label18;
        private GrapeCity.ActiveReports.SectionReportModel.Line line2;
        private GrapeCity.ActiveReports.SectionReportModel.Label label16;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox16;
        private GrapeCity.ActiveReports.SectionReportModel.Label label17;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox25;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox26;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox27;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox28;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox29;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox30;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox31;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox32;
        private GrapeCity.ActiveReports.SectionReportModel.Label label19;
        private GrapeCity.ActiveReports.SectionReportModel.Label label20;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox33;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox36;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox34;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox37;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox35;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox38;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox39;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox40;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
        private GrapeCity.ActiveReports.SectionReportModel.Label label21;
        private GrapeCity.ActiveReports.SectionReportModel.Label label22;
    }
}
