﻿namespace jp.co.fsi.han.hanb1101
{
    partial class HANB1101
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblDenpyoDate = new System.Windows.Forms.Label();
            this.lblDay = new System.Windows.Forms.Label();
            this.lblMonth = new System.Windows.Forms.Label();
            this.txtDay = new jp.co.fsi.common.controls.SosTextBox();
            this.lblYear = new System.Windows.Forms.Label();
            this.txtMonth = new jp.co.fsi.common.controls.SosTextBox();
            this.txtYear = new jp.co.fsi.common.controls.SosTextBox();
            this.lblGengo = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Text = "水揚情報転送(ホームページ)";
            // 
            // btnF1
            // 
            this.btnF1.Text = "F1\r\n\r\n";
            // 
            // btnF2
            // 
            this.btnF2.Text = "F2\r\n\r\n";
            // 
            // btnF3
            // 
            this.btnF3.Text = "F3\r\n\r\n";
            // 
            // btnF4
            // 
            this.btnF4.Text = "F4\r\n\r\n出力";
            // 
            // btnF5
            // 
            this.btnF5.Text = "F5\r\n\r\n";
            // 
            // btnF6
            // 
            this.btnF6.Text = "F6\r\n\r\n";
            // 
            // btnF12
            // 
            this.btnF12.Text = "F12\r\n\r\n";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Size = new System.Drawing.Size(847, 100);
            // 
            // lblDenpyoDate
            // 
            this.lblDenpyoDate.BackColor = System.Drawing.Color.Silver;
            this.lblDenpyoDate.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDenpyoDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDenpyoDate.Location = new System.Drawing.Point(12, 47);
            this.lblDenpyoDate.Name = "lblDenpyoDate";
            this.lblDenpyoDate.Size = new System.Drawing.Size(317, 26);
            this.lblDenpyoDate.TabIndex = 912;
            this.lblDenpyoDate.Text = "セリ日付";
            this.lblDenpyoDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDay
            // 
            this.lblDay.AutoSize = true;
            this.lblDay.BackColor = System.Drawing.Color.Silver;
            this.lblDay.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDay.Location = new System.Drawing.Point(306, 54);
            this.lblDay.Name = "lblDay";
            this.lblDay.Size = new System.Drawing.Size(21, 13);
            this.lblDay.TabIndex = 919;
            this.lblDay.Text = "日";
            // 
            // lblMonth
            // 
            this.lblMonth.AutoSize = true;
            this.lblMonth.BackColor = System.Drawing.Color.Silver;
            this.lblMonth.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMonth.Location = new System.Drawing.Point(241, 54);
            this.lblMonth.Name = "lblMonth";
            this.lblMonth.Size = new System.Drawing.Size(21, 13);
            this.lblMonth.TabIndex = 917;
            this.lblMonth.Text = "月";
            // 
            // txtDay
            // 
            this.txtDay.AutoSizeFromLength = false;
            this.txtDay.DisplayLength = null;
            this.txtDay.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDay.Location = new System.Drawing.Point(264, 50);
            this.txtDay.MaxLength = 2;
            this.txtDay.Name = "txtDay";
            this.txtDay.Size = new System.Drawing.Size(40, 20);
            this.txtDay.TabIndex = 918;
            this.txtDay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDay.Validating += new System.ComponentModel.CancelEventHandler(this.txtDay_Validating);
            // 
            // lblYear
            // 
            this.lblYear.AutoSize = true;
            this.lblYear.BackColor = System.Drawing.Color.Silver;
            this.lblYear.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblYear.Location = new System.Drawing.Point(174, 54);
            this.lblYear.Name = "lblYear";
            this.lblYear.Size = new System.Drawing.Size(21, 13);
            this.lblYear.TabIndex = 915;
            this.lblYear.Text = "年";
            // 
            // txtMonth
            // 
            this.txtMonth.AutoSizeFromLength = false;
            this.txtMonth.DisplayLength = null;
            this.txtMonth.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMonth.Location = new System.Drawing.Point(198, 50);
            this.txtMonth.MaxLength = 2;
            this.txtMonth.Name = "txtMonth";
            this.txtMonth.Size = new System.Drawing.Size(40, 20);
            this.txtMonth.TabIndex = 916;
            this.txtMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonth.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonth_Validating);
            // 
            // txtYear
            // 
            this.txtYear.AutoSizeFromLength = false;
            this.txtYear.DisplayLength = null;
            this.txtYear.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtYear.Location = new System.Drawing.Point(133, 50);
            this.txtYear.MaxLength = 2;
            this.txtYear.Name = "txtYear";
            this.txtYear.Size = new System.Drawing.Size(38, 20);
            this.txtYear.TabIndex = 914;
            this.txtYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtYear.Validating += new System.ComponentModel.CancelEventHandler(this.txtYear_Validating);
            // 
            // lblGengo
            // 
            this.lblGengo.BackColor = System.Drawing.Color.Silver;
            this.lblGengo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGengo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGengo.Location = new System.Drawing.Point(88, 50);
            this.lblGengo.Name = "lblGengo";
            this.lblGengo.Size = new System.Drawing.Size(40, 20);
            this.lblGengo.TabIndex = 913;
            this.lblGengo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // HANB1101
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 638);
            this.Controls.Add(this.lblDay);
            this.Controls.Add(this.lblMonth);
            this.Controls.Add(this.txtDay);
            this.Controls.Add(this.lblYear);
            this.Controls.Add(this.txtMonth);
            this.Controls.Add(this.txtYear);
            this.Controls.Add(this.lblGengo);
            this.Controls.Add(this.lblDenpyoDate);
            this.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "HANB1101";
            this.Text = "";
            this.Controls.SetChildIndex(this.lblDenpyoDate, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblGengo, 0);
            this.Controls.SetChildIndex(this.txtYear, 0);
            this.Controls.SetChildIndex(this.txtMonth, 0);
            this.Controls.SetChildIndex(this.lblYear, 0);
            this.Controls.SetChildIndex(this.txtDay, 0);
            this.Controls.SetChildIndex(this.lblMonth, 0);
            this.Controls.SetChildIndex(this.lblDay, 0);
            this.pnlDebug.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblDenpyoDate;
        private System.Windows.Forms.Label lblDay;
        private System.Windows.Forms.Label lblMonth;
        private common.controls.SosTextBox txtDay;
        private System.Windows.Forms.Label lblYear;
        private common.controls.SosTextBox txtMonth;
        private common.controls.SosTextBox txtYear;
        private System.Windows.Forms.Label lblGengo;


    }
}