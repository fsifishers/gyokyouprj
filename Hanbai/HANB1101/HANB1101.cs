﻿using System;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.ComponentModel;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;
using jp.co.fsi.common.constants;
using System.IO;
using System.Diagnostics;




namespace jp.co.fsi.han.hanb1101
{
    /// <summary>
    /// 水揚データ転送(ホームページ)(HANB5021)
    /// </summary>
    public partial class HANB1101 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// 文字コード(SJIS)
        /// </summary>
        private const int CHR_CD_SJIS = 932; // "Shift_JIS"と同じはず
        #endregion

        #region プロパティ
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HANB1101()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region メソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.InitForm();は呼び出さなくて構いません。
        /// また、このメソッド内の処理を外出しでこのクラス内にメソッド化するのは構いませんが、
        /// 原則、独自で起動時のイベント処理を実装することは禁じます。
        /// </remarks>
        protected override void InitForm()
        {
            // 日付範囲の和暦設定
            string[] jpDate = Util.ConvJpDate(DateTime.Now, this.Dba);
            // 伝票日付
            this.lblGengo.Text = jpDate[0];
            this.txtYear.Text = jpDate[2];
            this.txtMonth.Text = jpDate[3];
            this.txtDay.Text = jpDate[4];

            // フォーカス設定
            this.txtYear.Focus();
            this.txtYear.Select();
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 会計年度の凍結処理チェック 凍結されていた場合はアラートを表示し処理を行なわない
            if (Util.GetKaikeiNendoFixedFlg(this.UInfo.KaikeiNendo, this.Dba))
            {
                Msg.Error("この会計年度は凍結されています。");
                return;
            }

            // 入力日付に該当する会計年度が選択会計年度と一致するかチェック
            DateTime DENPYO_DATE = Util.ConvAdDate(this.lblGengo.Text, this.txtYear.Text,
                    this.txtMonth.Text, this.txtDay.Text, this.Dba);
            if (Util.GetKaikeiNendo(DENPYO_DATE, this.Dba) != this.UInfo.KaikeiNendo)
            {
                Msg.Error("入力日付が選択会計年度の範囲外です。");
                return;
            }

            // 選択会計年度が今年度かチェック
            DateTime today = DateTime.Today;
            if (Util.GetKaikeiNendo(today, this.Dba) != this.UInfo.KaikeiNendo)
            {
                if (Msg.ConfNmYesNo("データ連携（更新）", "選択会計年度が今年度ではありませんが、宜しいですか？") == DialogResult.No)
                {
                    return;
                }
            }

            // 全入力項目チェック
            if (!this.isValidDataAll())
            {
                return;
            }

            // セリ情報転送処理を実行
            getMasterData();
        }
        #endregion

        #region イベント
        /// <summary>
        /// "年"の入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtYear_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!this.isValidGengoYear())
            {
                this.txtYear.SelectAll();
                this.txtYear.Focus();
                e.Cancel = true;
                return;
            }

            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            this.SetJpDate(Util.FixJpDate(this.lblGengo.Text, this.txtYear.Text,
                this.txtMonth.Text, this.txtDay.Text, this.Dba));
        }

        /// <summary>
        /// "月"の入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMonth_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!this.isValidMonth())
            {
                this.txtMonth.SelectAll();
                this.txtMonth.Focus();
                e.Cancel = true;
                return;
            }

            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            this.SetJpDate(Util.FixJpDate(this.lblGengo.Text, this.txtYear.Text,
                this.txtMonth.Text, this.txtDay.Text, this.Dba));

        }

        /// <summary>
        /// "日"の入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDay_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!this.isValidDay())
            {
                this.txtDay.SelectAll();
                this.txtDay.Focus();
                e.Cancel = true;
                return;
            }

            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            this.SetJpDate(Util.FixJpDate(this.lblGengo.Text, this.txtYear.Text,
                this.txtMonth.Text, this.txtDay.Text, this.Dba));
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// "年"の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool isValidGengoYear()
        {
            // 空の場合、0年として処理
            if (ValChk.IsEmpty(this.txtYear.Text))
            {
                this.txtYear.Text = "0";
            }
            else
            {
                // 最大桁数チェック
                if (!ValChk.IsWithinLength(this.txtYear.Text, this.txtYear.MaxLength))
                {
                    Msg.Notice("入力に誤りがあります。");
                    return false;
                }

                // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
                if (!ValChk.IsNumber(this.txtYear.Text))
                {
                    Msg.Notice("数値のみで入力してください。");
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// "月"の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool isValidMonth()
        {
            if (ValChk.IsEmpty(this.txtMonth.Text))
            {
                // 空の場合は、1月として処理
                this.txtMonth.Text = "1";
                return true;
            }

            // 最大桁数チェック
            if (!ValChk.IsWithinLength(this.txtMonth.Text, this.txtMonth.MaxLength))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtMonth.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                return false;
            }

            int month = Util.ToInt(this.txtMonth.Text);
            // 12を超える月が入力された場合、12月として処理
            if (12 < month)
            {
                this.txtMonth.Text = "12";
            }
            else if (month < 1)
            {
                // 1未満場合は、1月として処理
                this.txtMonth.Text = "1";
            }

            int day = Util.ToInt(this.txtDay.Text);
            if (day < 1)
            {
                // 1未満の場合は、1日として処理
                this.txtDay.Text = "1";
            }
            else
            {
                // "月"を変更して、日が月末日を超えた場合、月末日として処理(年月が入力されていること前提)
                DateTime tmpDate = Util.ConvAdDate(this.lblGengo.Text, this.txtYear.Text,
                    this.txtMonth.Text, "1", this.Dba);
                int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

                if (lastDayInMonth < day)
                {
                    this.txtDay.Text = Util.ToString(lastDayInMonth);
                }
            }

            return true;
        }

        /// <summary>
        /// "日"の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool isValidDay()
        {
            if (ValChk.IsEmpty(this.txtDay.Text))
            {
                // 空の場合は、1日として処理
                this.txtDay.Text = "1";
                return true;
            }

            // 最大桁数チェック
            if (!ValChk.IsWithinLength(this.txtDay.Text, this.txtDay.MaxLength))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtDay.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                return false;
            }

            int day = Util.ToInt(this.txtDay.Text);
            if (day < 1)
            {
                // 1未満の場合は、1日として処理
                this.txtDay.Text = "1";
            }
            else
            {
                // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
                DateTime tmpDate = Util.ConvAdDate(this.lblGengo.Text, this.txtYear.Text,
                    this.txtMonth.Text, "1", this.Dba);
                int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

                if (lastDayInMonth < day)
                {
                    this.txtDay.Text = Util.ToString(lastDayInMonth);
                }
            }

            return true;

        }

        /// 全入力項目チェック
        /// </summary>
        /// <param name="modeFlg">追加モード=MODE_NEW , 更新モード=MODE_EDIT</param>
        /// <returns>true=OK, false=NG</returns>
        private bool isValidDataAll()
        {
            // "年"の入力チェック
            if (!this.isValidGengoYear())
            {
                this.txtYear.Focus();
                this.txtYear.SelectAll();
                return false;
            }

            // "月"の入力チェック
            if (!this.isValidMonth())
            {
                this.txtMonth.Focus();
                this.txtMonth.SelectAll();
                return false;
            }

            // "日"の入力チェック
            if (!this.isValidDay())
            {
                this.txtDay.Focus();
                this.txtDay.SelectAll();
                return false;
            }

            return true;
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpDate(string[] arrJpDate)
        {
            this.lblGengo.Text = arrJpDate[0];
            this.txtYear.Text = arrJpDate[2];
            this.txtMonth.Text = arrJpDate[3];
            this.txtDay.Text = arrJpDate[4];
        }

        /// <summary>
        /// セリデータ転送処理を実装
        /// </summary>
        private void getMasterData()
        {
            try
            {
                // セリデータのファイル出力
                this.getSeriDt();
            }
            catch (Exception e)
            {
                Msg.Notice(e.Message);
                return;
            }
        }
        
        /// <summary>
        /// セリデータ取得
        /// </summary>
        private void getSeriDt()
        {
            DateTime date = Util.ConvAdDate(this.lblGengo.Text, this.txtYear.Text, this.txtMonth.Text, this.txtDay.Text, this.Dba);
            String seribi = date.ToString("yyyy-MM-dd");
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder Sql = new StringBuilder();

            // セリデータを取得
            Sql.Append("SELECT");
            Sql.Append(" KAISHA_CD AS KAISHA_CD,"); // 会社CD
            Sql.Append(" YAMA_NO AS YAMA_NO,"); // 山NO
            Sql.Append(" SENSHU_CD AS SENSHU_CD,"); // 船主CD
            Sql.Append(" GYOHO_CD AS GYOHO_CD,"); // 漁法CD
            Sql.Append(" GYOSHU_CD AS GYOSHU_CD,"); // 魚種CD
            Sql.Append(" SURYO AS SURYO,"); // 数量
            Sql.Append(" TANKA AS TANKA,"); // 単価
            Sql.Append(" NAKAGAININ_CD AS NAKAGAININ_CD "); // 仲買人CD
            Sql.Append("FROM");
            Sql.Append(" VI_HN_SHIKIRI_MEISAI ");
            Sql.Append("WHERE");
            Sql.Append(" KAISHA_CD = @KAISHA_CD");
            Sql.Append(" AND KAIKEI_NENDO = @KAIKEI_NENDO");
            Sql.Append(" AND SERIBI = @SERIBI ");
            Sql.Append("ORDER BY");
            Sql.Append(" YAMA_NO,");
            Sql.Append(" GYO_NO");

            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            dpc.SetParam("@SERIBI", SqlDbType.VarChar, 10, seribi);

            DataTable dt = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            // セリデータが存在する場合、txtファイルを作成する
            if (dt.Rows.Count > 0)
            {
                // テキストファイル出力処理
                setSeriDt(dt, seribi, "SERI_MEISAI.txt");

                // FTPサーバーにアップロード
                System.Net.WebClient wc = new System.Net.WebClient();
                // ログインユーザー名とパスワードを指定
                wc.Credentials = new System.Net.NetworkCredential("jf-nago-jp", "c4EqXtWa");
                wc.UploadFile("ftp://wx21.wadax.ne.jp/data/homepage/" + seribi + "_" + "SERI_MEISAI.txt", seribi + "_" + "SERI_MEISAI.txt");

                System.IO.File.Delete(seribi + "_" + "SERI_MEISAI.txt");
            }
            else
            {
                Msg.Info("該当する日付のデータが存在しません。");
            }
        }

        /// <summary>
        /// 船主/仲買にマスタデータのtxtファイル作成処理
        /// </summary>
        private void setSeriDt(DataTable dt, string seribi, string txtNm)
        {
            // --- これ以降は、テキストファイル出力処理 ---
            //CSVファイルに書き込むときに使うEncoding
            System.Text.Encoding enc =
                System.Text.Encoding.GetEncoding(CHR_CD_SJIS);

            // ファイルのストリーム宣言
            System.IO.StreamWriter sr;
            try
            {
                sr = new System.IO.StreamWriter(seribi + "_" + txtNm, false, enc);
            }
            catch (Exception e)
            {
                Msg.Notice(e.Message);
                return;
            }

            try
            {
                //レコードを書き込む
                int i = 1;
                String yamaNo = "";
                int yamaSortNo = 0;
                foreach (DataRow row in dt.Rows)
                {
                    if (yamaNo != Util.ToString(row["YAMA_NO"]))
                    {
                        yamaSortNo = 0;
                    }

                    sr.Write(Util.ToString(row["KAISHA_CD"]) + '\t'); // 会社CD
                    sr.Write(seribi + '\t'); // セリ日
                    sr.Write("1" + '\t'); // セリ担当者
                    sr.Write(Util.ToString(i) + '\t'); // セリ番号
                    sr.Write(Util.ToString(row["YAMA_NO"]) + '\t'); // 山NO
                    sr.Write(Util.ToString(yamaSortNo) + '\t'); // 山ソートNO
                    sr.Write("0" + '\t'); // 山NO枝番
                    sr.Write(Util.ToString(row["SENSHU_CD"]) + '\t'); // 船主CD
                    sr.Write(Util.ToString(row["GYOHO_CD"]) + '\t'); // 漁法CD
                    sr.Write(Util.ToString(row["GYOSHU_CD"]) + '\t'); // 魚種CD
                    sr.Write(Util.ToString(row["SURYO"]) + '\t'); // 数量
                    sr.Write(Util.ToString(row["TANKA"]) + '\t'); // 単価
                    sr.Write(Util.ToString(row["NAKAGAININ_CD"]) + '\t'); // 仲買人CD
                    sr.Write("0" + '\t'); // 箱FLG
                    sr.Write("0" + '\t'); // 尾数
                    sr.Write("0" + '\t'); // 本数
                    sr.Write("1" + '\t'); // 処理フラグ
                    sr.Write(Util.ToString(i) + '\t'); // 親セリ番号
                    sr.Write("0"); // 子数
                    //sr.Write(Util.ToString(row["CLIENT_FLG"])); // 処理日時

                    //改行する
                    sr.Write("\r\n");
                    i++;
                    yamaSortNo++;
                    yamaNo = Util.ToString(row["YAMA_NO"]);
                }

                // FTPサーバーにアップロード
                /*
                System.Net.WebClient wc = new System.Net.WebClient();
                // ログインユーザー名とパスワードを指定
                wc.Credentials = new System.Net.NetworkCredential("jf-nago-jp", "c4EqXtWa");
                wc.UploadFile("ftp://wx21.wadax.ne.jp/data/homepage/" + seribi + "_" + txtNm, seribi + "_" + txtNm);

                System.IO.File.Delete(seribi + "_" + txtNm);*/

                // 完了のメッセージを表示
                Msg.Info("データの書き込み処理を終了しました。");
            }
            catch (Exception e)
            {
                // エラーメッセージを表示
                Msg.Notice(e.Message);
                return;
            }
            finally
            {
                // ファイルのストリームを閉じる
                sr.Close();
            }
        }
        #endregion
    }
}
