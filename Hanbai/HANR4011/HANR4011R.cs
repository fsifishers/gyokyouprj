﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using jp.co.fsi.common.report;
using System.Data;

namespace jp.co.fsi.han.hanr4011
{
    /// <summary>
    /// HANR4011R の概要の説明です。
    /// </summary>
    public partial class HANR4011R : BaseReport
    {

        public HANR4011R(DataTable tgtData): base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }

        private void pageHeader_Format(object sender, EventArgs e)
        {
            txtToday.Text = DateTime.Now.ToString("yyyy/MM/dd");
        }
    }
}
