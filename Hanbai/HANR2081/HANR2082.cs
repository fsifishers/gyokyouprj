﻿using System;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.han.hanr2081
{
    /// <summary>
    /// 魚種設定(HANR2082)
    /// </summary>
    public partial class HANR2082 : BasePgForm
    {
        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HANR2082()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // サイズを縮める
            this.Size = new Size(550, 590);
            // Escapeのみ表示
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF1.Visible = false;
            this.btnF2.Visible = false;
            this.btnF3.Visible = false;
            this.btnF4.Location = this.btnF2.Location;
            this.btnF5.Location = this.btnF3.Location;
            this.btnF6.Location = this.btnF6.Location;
            this.btnF7.Location = this.btnF7.Location;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;

            // リストボックスの初期表示
            SearchData();
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // 設定保存処理
            ShuturyokuData();

            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F4ボタンクリック時処理
        /// </summary>
        public override void PressF4()
        {
            // 魚種一覧の全選択
            for (int cnt = 0; cnt < this.lbxGyoshuIchiran.Items.Count; cnt++)
            {
                this.lbxGyoshuIchiran.SetSelected(cnt, true);
            }
        }

        /// <summary>
        /// F5ボタンクリック時処理
        /// </summary>
        public override void PressF5()
        {
            // 魚種一覧の全選択解除
            for (int cnt = 0; cnt < this.lbxGyoshuIchiran.Items.Count; cnt++)
            {
                this.lbxGyoshuIchiran.SetSelected(cnt, false);
            }
        }

        /// <summary>
        /// F6ボタンクリック時処理
        /// </summary>
        public override void PressF6()
        {
            // 出力魚種の全選択
            for (int cnt = 0; cnt < this.lbxShuturyokuCd.Items.Count; cnt++)
            {
                this.lbxShuturyokuCd.SetSelected(cnt, true);
            }
        }

        /// <summary>
        /// F7ボタンクリック時処理
        /// </summary>
        public override void PressF7()
        {
            // 出力魚種の全選択解除
            for (int cnt = 0; cnt < this.lbxShuturyokuCd.Items.Count; cnt++)
            {
                this.lbxShuturyokuCd.SetSelected(cnt, false);
            }
        }
        #endregion

        #region イベント
        /// <summary>
        /// 選択した項目の移動処理(左→右)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRMove_Click(object sender, System.EventArgs e)
        {
            int[] selectNum = new int[lbxGyoshuIchiran.SelectedIndices.Count];

            for (int i = 0; i < lbxGyoshuIchiran.SelectedIndices.Count; i++)
            {
                selectNum[i] = lbxGyoshuIchiran.SelectedIndices[i];
            }

            // 選択した項目の追加
            for (int i = 0; i < selectNum.Length; i++)
            {
                this.lbxShuturyokuCd.Items.Add(this.lbxGyoshuIchiran.Items[selectNum[i]]);
            }

            int l = 1;
            // 選択した項目の削除
            for (int i = 0; i < selectNum.Length; i++)
            {
                if (i == 0)
                {
                    this.lbxGyoshuIchiran.Items.Remove(this.lbxGyoshuIchiran.Items[selectNum[i]]);
                }
                else
                {
                    this.lbxGyoshuIchiran.Items.Remove(this.lbxGyoshuIchiran.Items[selectNum[i]-l]);
                    l++;
                }
            }
        }

        /// <summary>
        /// 選択した項目の移動処理(右→左)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnLMove_Click(object sender, System.EventArgs e)
        {
            {
                int[] selectNum = new int[lbxShuturyokuCd.SelectedIndices.Count];

                for (int i = 0; i < lbxShuturyokuCd.SelectedIndices.Count; i++)
                {
                    selectNum[i] = lbxShuturyokuCd.SelectedIndices[i];
                }

                // 選択した項目の追加
                for (int i = 0; i < selectNum.Length; i++)
                {
                    this.lbxGyoshuIchiran.Items.Add(this.lbxShuturyokuCd.Items[selectNum[i]]);
                }

                int l = 1;
                // 選択した項目の削除
                for (int i = 0; i < selectNum.Length; i++)
                {
                    if (i == 0)
                    {
                        this.lbxShuturyokuCd.Items.Remove(this.lbxShuturyokuCd.Items[selectNum[i]]);
                    }
                    else
                    {
                        this.lbxShuturyokuCd.Items.Remove(this.lbxShuturyokuCd.Items[selectNum[i] - l]);
                        l++;
                    }
                }
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// データを検索する
        /// </summary>
        private void SearchData()
        {
            // 魚種一覧に表示するデータを取得
            StringBuilder Sql = new StringBuilder();
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);

            Sql.Append(" SELECT");
            Sql.Append("     A.SHOHIN_CD AS SHOHIN_CD,");
            Sql.Append("     A.SHOHIN_NM     AS SHOHIN_NM ");
            Sql.Append(" FROM");
            Sql.Append("     TB_HN_SHOHIN AS A ");
            Sql.Append(" WHERE");
            Sql.Append("     A.KAISHA_CD = @KAISHA_CD AND");
            Sql.Append("     A.SHOHIN_CD ");
            Sql.Append(" NOT IN (SELECT ");
            Sql.Append("     B.GYOSHU_CD ");
            Sql.Append(" FROM");
            Sql.Append("     TB_HN_GYOSHU_SETTEI  AS B ");
            Sql.Append(" WHERE");
            Sql.Append("     B.KAISHA_CD = A.KAISHA_CD  AND ");
            Sql.Append("     1 = B.KUBUN ");
            Sql.Append(" ) AND");
            Sql.Append("     A.BARCODE1 = 999 ");
            Sql.Append(" ORDER BY");
            Sql.Append("     A.KAISHA_CD, ");
            Sql.Append("     A.SHOHIN_CD ");

            DataTable dtGyosyuIchiran = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            // 魚種一覧にセットする
            foreach (DataRow dr in dtGyosyuIchiran.Rows)
            {
                this.lbxGyoshuIchiran.Items.Add(Util.ToString(dr["SHOHIN_CD"]) + " " + Util.ToString(dr["SHOHIN_NM"]));
            }

            // 出力する魚種に表示するデータを取得
            Sql = new StringBuilder();
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);

            Sql.Append(" SELECT");
            Sql.Append("     A.GYOSHU_CD  AS SHOHIN_CD,");
            Sql.Append("     B.SHOHIN_NM      AS SHOHIN_NM");
            Sql.Append(" FROM");
            Sql.Append("     TB_HN_GYOSHU_SETTEI  AS A     ");
            Sql.Append(" LEFT OUTER JOIN TB_HN_SHOHIN AS B       ");
            Sql.Append(" ON A.KAISHA_CD     = B.KAISHA_CD AND ");
            Sql.Append("    A.GYOSHU_CD = B.SHOHIN_CD");
            Sql.Append(" WHERE");
            Sql.Append("     A.KAISHA_CD = @KAISHA_CD AND");
            Sql.Append("     A.KUBUN = 1 AND");
            Sql.Append("     B.BARCODE1 = 999");
            Sql.Append(" ORDER BY");
            Sql.Append("     A.GYOSHU_CD");

            DataTable dtShuturyoku = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            // 出力する魚種にセットする
            foreach (DataRow dr in dtShuturyoku.Rows)
            {
                this.lbxShuturyokuCd.Items.Add(Util.ToString(dr["SHOHIN_CD"]) + " " + Util.ToString(dr["SHOHIN_NM"]));
            }
        }

        /// <summary>
        /// 設定をテーブルに保存する
        /// </summary>
        private void ShuturyokuData()
        {
            try
            {
                this.Dba.BeginTransaction();

                // 魚種設定削除処理
                StringBuilder Sql = new StringBuilder();
                DbParamCollection dpc = new DbParamCollection();
                Sql.Append("DELETE ");
                Sql.Append("  FROM TB_HN_GYOSHU_SETTEI");
                Sql.Append("  WHERE");
                Sql.Append("   KAISHA_CD = @KAISHA_CD AND");
                Sql.Append("   KUBUN = 1 ");

                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);

                this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                for (int i = 0; i < this.lbxShuturyokuCd.Items.Count; i++)
                {
                    // 魚種設定登録処理
                    Sql = new StringBuilder();
                    dpc = new DbParamCollection();
                    Sql.Append(" INSERT INTO TB_HN_GYOSHU_SETTEI (");
                    Sql.Append(" KAISHA_CD,");
                    Sql.Append(" GYOSHU_CD,");
                    Sql.Append(" REGIST_DATE,");
                    Sql.Append(" KUBUN");
                    Sql.Append(" )");
                    Sql.Append(" VALUES (");
                    Sql.Append(" @KAISHA_CD,");
                    Sql.Append(" @GYOSHU_CD,");
                    Sql.Append(" @REGIST_DATE,");
                    Sql.Append(" @KUBUN");
                    Sql.Append(" )");

                    // 魚種CDの取得
                    string items = Util.ToString(this.lbxShuturyokuCd.Items[i]);
                    string[] gyoshuCd = items.Split(' ');

                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                    dpc.SetParam("@GYOSHU_CD", SqlDbType.Decimal, 4, gyoshuCd[0]);
                    dpc.SetParam("@REGIST_DATE", SqlDbType.DateTime, 20, DateTime.Today);
                    dpc.SetParam("@KUBUN", SqlDbType.Decimal, 4, 1);

                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                }

            // トランザクションをコミット
            this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

        }
        #endregion







    }
}
