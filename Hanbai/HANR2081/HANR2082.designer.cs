﻿namespace jp.co.fsi.han.hanr2081
{
    partial class HANR2082
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbxAll = new System.Windows.Forms.GroupBox();
            this.btnLMove = new System.Windows.Forms.Button();
            this.btnRMove = new System.Windows.Forms.Button();
            this.gbxShuturyokuCd = new System.Windows.Forms.GroupBox();
            this.lbxShuturyokuCd = new System.Windows.Forms.ListBox();
            this.gbxGyoshuIchiran = new System.Windows.Forms.GroupBox();
            this.lbxGyoshuIchiran = new System.Windows.Forms.ListBox();
            this.pnlDebug.SuspendLayout();
            this.gbxAll.SuspendLayout();
            this.gbxShuturyokuCd.SuspendLayout();
            this.gbxGyoshuIchiran.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Text = "";
            // 
            // gbxAll
            // 
            this.gbxAll.Controls.Add(this.btnLMove);
            this.gbxAll.Controls.Add(this.btnRMove);
            this.gbxAll.Controls.Add(this.gbxShuturyokuCd);
            this.gbxAll.Controls.Add(this.gbxGyoshuIchiran);
            this.gbxAll.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxAll.Location = new System.Drawing.Point(18, 65);
            this.gbxAll.Name = "gbxAll";
            this.gbxAll.Size = new System.Drawing.Size(502, 387);
            this.gbxAll.TabIndex = 902;
            this.gbxAll.TabStop = false;
            // 
            // btnLMove
            // 
            this.btnLMove.BackColor = System.Drawing.Color.Silver;
            this.btnLMove.Font = new System.Drawing.Font("ＭＳ ゴシック", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnLMove.Location = new System.Drawing.Point(227, 232);
            this.btnLMove.Name = "btnLMove";
            this.btnLMove.Size = new System.Drawing.Size(44, 39);
            this.btnLMove.TabIndex = 3;
            this.btnLMove.Text = "＜";
            this.btnLMove.UseVisualStyleBackColor = false;
            this.btnLMove.Click += new System.EventHandler(this.btnLMove_Click);
            // 
            // btnRMove
            // 
            this.btnRMove.BackColor = System.Drawing.Color.Silver;
            this.btnRMove.Font = new System.Drawing.Font("ＭＳ ゴシック", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnRMove.Location = new System.Drawing.Point(227, 178);
            this.btnRMove.Name = "btnRMove";
            this.btnRMove.Size = new System.Drawing.Size(44, 39);
            this.btnRMove.TabIndex = 2;
            this.btnRMove.Text = "＞";
            this.btnRMove.UseVisualStyleBackColor = false;
            this.btnRMove.Click += new System.EventHandler(this.btnRMove_Click);
            // 
            // gbxShuturyokuCd
            // 
            this.gbxShuturyokuCd.Controls.Add(this.lbxShuturyokuCd);
            this.gbxShuturyokuCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxShuturyokuCd.ForeColor = System.Drawing.SystemColors.ControlText;
            this.gbxShuturyokuCd.Location = new System.Drawing.Point(280, 50);
            this.gbxShuturyokuCd.Name = "gbxShuturyokuCd";
            this.gbxShuturyokuCd.Size = new System.Drawing.Size(204, 330);
            this.gbxShuturyokuCd.TabIndex = 1;
            this.gbxShuturyokuCd.TabStop = false;
            this.gbxShuturyokuCd.Text = "出力する魚種コード";
            // 
            // lbxShuturyokuCd
            // 
            this.lbxShuturyokuCd.FormattingEnabled = true;
            this.lbxShuturyokuCd.Location = new System.Drawing.Point(14, 27);
            this.lbxShuturyokuCd.Name = "lbxShuturyokuCd";
            this.lbxShuturyokuCd.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.lbxShuturyokuCd.Size = new System.Drawing.Size(179, 290);
            this.lbxShuturyokuCd.TabIndex = 1;
            // 
            // gbxGyoshuIchiran
            // 
            this.gbxGyoshuIchiran.Controls.Add(this.lbxGyoshuIchiran);
            this.gbxGyoshuIchiran.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxGyoshuIchiran.ForeColor = System.Drawing.SystemColors.ControlText;
            this.gbxGyoshuIchiran.Location = new System.Drawing.Point(17, 50);
            this.gbxGyoshuIchiran.Name = "gbxGyoshuIchiran";
            this.gbxGyoshuIchiran.Size = new System.Drawing.Size(204, 330);
            this.gbxGyoshuIchiran.TabIndex = 0;
            this.gbxGyoshuIchiran.TabStop = false;
            this.gbxGyoshuIchiran.Text = "魚種一覧";
            // 
            // lbxGyoshuIchiran
            // 
            this.lbxGyoshuIchiran.FormattingEnabled = true;
            this.lbxGyoshuIchiran.Location = new System.Drawing.Point(12, 27);
            this.lbxGyoshuIchiran.Name = "lbxGyoshuIchiran";
            this.lbxGyoshuIchiran.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.lbxGyoshuIchiran.Size = new System.Drawing.Size(179, 290);
            this.lbxGyoshuIchiran.TabIndex = 0;
            // 
            // HANR2082
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(823, 638);
            this.Controls.Add(this.gbxAll);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "HANR2082";
            this.ShowFButton = true;
            this.Text = "";
            this.Controls.SetChildIndex(this.gbxAll, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.pnlDebug.ResumeLayout(false);
            this.gbxAll.ResumeLayout(false);
            this.gbxShuturyokuCd.ResumeLayout(false);
            this.gbxGyoshuIchiran.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbxAll;
        private System.Windows.Forms.Button btnLMove;
        private System.Windows.Forms.Button btnRMove;
        private System.Windows.Forms.GroupBox gbxShuturyokuCd;
        private System.Windows.Forms.GroupBox gbxGyoshuIchiran;
        private System.Windows.Forms.ListBox lbxShuturyokuCd;
        private System.Windows.Forms.ListBox lbxGyoshuIchiran;




    }
}