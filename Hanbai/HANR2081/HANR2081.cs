﻿using System;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.access;
using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.han.hanr2081
{
    /// <summary>
    /// 魚態別魚種別水揚月報(HANR2081)
    /// </summary>
    public partial class HANR2081 : BasePgForm
    {
        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HANR2081()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            string[] jpDate = Util.ConvJpDate(DateTime.Now, this.Dba);
            lblDateGengoFr.Text = jpDate[0];
            txtDateYearFr.Text = jpDate[2];
            txtDateMonthFr.Text = jpDate[3];
            txtDateDayFr.Text = jpDate[4];

            lblDateGengoTo.Text = jpDate[0];
            txtDateYearTo.Text = jpDate[2];
            txtDateMonthTo.Text = jpDate[3];
            txtDateDayTo.Text = jpDate[4];

            lblGyotaiBetsuCdFr.Text = "先　頭";
            lblGyotaiBetsuCdTo.Text = "最　後";
            lblFunanushiCdFr.Text = "先　頭";
            lblFunanushiCdTo.Text = "最　後";

            // 初期フォーカス
            txtDateYearFr.Focus();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 日付,魚態別CD,船主CDにフォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtDateYearFr":
                case "txtDateYearTo":
                case "txtGyotaiBetsuCdFr":
                case "txtGyotaiBetsuCdTo":
                case "txtFunanushiCdFr":
                case "txtFunanushiCdTo":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;

            switch (this.ActiveCtlNm)
            {
                case "txtDateYearFr":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom("COMC9011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.com.comc9011.COMC9011");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblDateGengoFr.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.lblDateGengoFr.Text = result[1];

                                // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
                                DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                                    this.txtDateMonthFr.Text, "1", this.Dba);
                                int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);
                                if (Util.ToInt(this.txtDateDayFr.Text) > lastDayInMonth)
                                {
                                    this.txtDateDayFr.Text = Util.ToString(lastDayInMonth);
                                }

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblDateGengoFr.Text,
                                        this.txtDateYearFr.Text,
                                        this.txtDateMonthFr.Text,
                                        this.txtDateDayFr.Text,
                                        this.Dba);
                                this.lblDateGengoFr.Text = arrJpDate[0];
                                this.txtDateYearFr.Text = arrJpDate[2];
                                this.txtDateMonthFr.Text = arrJpDate[3];
                                this.txtDateDayFr.Text = arrJpDate[4];
                            }
                        }
                    }
                    break;

                case "txtDateYearTo":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom("COMC9011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.com.comc9011.COMC9011");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblDateGengoTo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.lblDateGengoTo.Text = result[1];

                                // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
                                DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                                    this.txtDateMonthTo.Text, "1", this.Dba);
                                int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);
                                if (Util.ToInt(this.txtDateDayTo.Text) > lastDayInMonth)
                                {
                                    this.txtDateDayTo.Text = Util.ToString(lastDayInMonth);
                                }

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblDateGengoTo.Text,
                                        this.txtDateYearTo.Text,
                                        this.txtDateMonthTo.Text,
                                        this.txtDateDayTo.Text,
                                        this.Dba);
                                this.lblDateGengoTo.Text = arrJpDate[0];
                                this.txtDateYearTo.Text = arrJpDate[2];
                                this.txtDateMonthTo.Text = arrJpDate[3];
                                this.txtDateDayTo.Text = arrJpDate[4];
                            }
                        }
                    }
                    break;

                case "txtGyotaiBetsuCdFr":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom("HANC9041.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.han.hanc9041.HANC9041");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtGyotaiBetsuCdFr.Text = outData[0];
                                this.lblGyotaiBetsuCdFr.Text = outData[1];
                            }
                        }
                    }
                    break;

                case "txtGyotaiBetsuCdTo":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom("HANC9041.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.han.hanc9041.HANC9041");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtGyotaiBetsuCdTo.Text = outData[0];
                                this.lblGyotaiBetsuCdTo.Text = outData[1];
                            }
                        }
                    }
                    break;

                case "txtFunanushiCdFr":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom("KOBC9021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.kob.kobc9021.KOBC9021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtFunanushiCdFr.Text = outData[0];
                                this.lblFunanushiCdFr.Text = outData[1];
                            }
                        }
                    }
                    break;

                case "txtFunanushiCdTo":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom("KOBC9021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.kob.kobc9021.KOBC9021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtFunanushiCdTo.Text = outData[0];
                                this.lblFunanushiCdTo.Text = outData[1];
                            }
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            // プレビュー処理
            DoPrint(true);
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfYesNo("実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }

        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            HANR2082 frmHANR2082;
            // 魚種設定画面を起動
            frmHANR2082 = new HANR2082();

            DialogResult result = frmHANR2082.ShowDialog(this);

        }
        #endregion

        #region イベント
        /// <summary>
        /// 魚態別CD(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGyotaiBetsuCdFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidGyotaiBetsuCdFr())
            {
                e.Cancel = true;
                this.txtGyotaiBetsuCdFr.SelectAll();
            }
        }

        /// <summary>
        /// 魚態別CD(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGyotaiBetsuCdTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidGyotaiBetsuCdTo())
            {
                e.Cancel = true;
                this.txtGyotaiBetsuCdTo.SelectAll();
            }
        }

        /// <summary>
        /// 船主CD(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFunanushiCdFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidFunanushiCdFr())
            {
                e.Cancel = true;
                this.txtFunanushiCdFr.SelectAll();
            }
        }

        /// <summary>
        /// 船主CD(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFunanushiCdTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidFunanushiCdTo())
            {
                e.Cancel = true;
                this.txtFunanushiCdTo.SelectAll();
            }
        }

        /// <summary>
        /// 和暦(年)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateYearFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidDateYearFr())
            {
                e.Cancel = true;
                this.txtDateYearFr.SelectAll();
            }
            else
            {
                CheckDateFr();
                SetDateFr();
            }
        }

        /// <summary>
        /// 和暦(月)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateMonthFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidDateMonthFr())
            {
                e.Cancel = true;
                this.txtDateMonthFr.SelectAll();
            }
            else
            {
                CheckDateFr();
                SetDateFr();
            }
        }

        /// <summary>
        /// 和暦(日)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateDayFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidDateDayFr())
            {
                e.Cancel = true;
                this.txtDateDayFr.SelectAll();
            }
            else
            {
                CheckDateFr();
                SetDateFr();
            }
        }

        /// <summary>
        /// 和暦(年)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateYearTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidDateYearTo())
            {
                e.Cancel = true;
                this.txtDateYearTo.SelectAll();
            }
            else
            {
                CheckDateTo();
                SetDateTo();
            }
        }

        /// <summary>
        /// 和暦(月)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateMonthTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidDateMonthTo())
            {
                e.Cancel = true;
                this.txtDateMonthTo.SelectAll();
            }
            else
            {
                CheckDateTo();
                SetDateTo();
            }
        }

        /// <summary>
        /// 和暦(日)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateDayTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidDateDayTo())
            {
                e.Cancel = true;
                this.txtDateDayTo.SelectAll();
            }
            else
            {
                CheckDateTo();
                SetDateTo();
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 年(自)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidDateYearFr()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtDateYearFr.Text))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }
            // 空の場合、0年として処理
            if (ValChk.IsEmpty(this.txtDateYearFr.Text))
            {
                this.txtDateYearFr.Text = "0";
            }

            return true;
        }

        /// <summary>
        /// 月(自)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidDateMonthFr()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtDateMonthFr.Text))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }
            // 空の場合、1月として処理
            if (ValChk.IsEmpty(this.txtDateMonthFr.Text))
            {
                this.txtDateMonthFr.Text = "1";
            }
            else
            {
                // 12を超える月が入力された場合、12月として処理
                if (Util.ToInt(this.txtDateMonthFr.Text) > 12)
                {
                    this.txtDateMonthFr.Text = "12";
                }
                // 1より小さい月が入力された場合、1月として処理
                else if (Util.ToInt(this.txtDateMonthFr.Text) < 1)
                {
                    this.txtDateMonthFr.Text = "1";
                }
            }

            return true;
        }

        /// <summary>
        /// 日(自)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidDateDayFr()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtDateDayFr.Text))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }

            if (ValChk.IsEmpty(this.txtDateDayFr.Text))
            {
                // 空の場合、1日として処理
                this.txtDateDayFr.Text = "1";
            }
            else
            {
                // 1より小さい日が入力された場合、1日として処理
                if (Util.ToInt(this.txtDateDayFr.Text) < 1)
                {
                    this.txtDateDayFr.Text = "1";
                }
            }

            return true;
        }

        /// <summary>
        /// 年月日(自)の月末入力チェック
        /// </summary>
        /// 
        private void CheckDateFr()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                this.txtDateMonthFr.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDateDayFr.Text) > lastDayInMonth)
            {
                this.txtDateDayFr.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(自)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetDateFr()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDateFr(Util.FixJpDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                this.txtDateMonthFr.Text, this.txtDateDayFr.Text, this.Dba));
        }

        /// <summary>
        /// 年(至)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidDateYearTo()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtDateYearTo.Text))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }
            // 空の場合、0年として処理
            if (ValChk.IsEmpty(this.txtDateYearTo.Text))
            {
                this.txtDateYearTo.Text = "0";
            }

            return true;
        }

        /// <summary>
        /// 月(至)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidDateMonthTo()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtDateMonthTo.Text))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }
            // 空の場合、1月として処理
            if (ValChk.IsEmpty(this.txtDateMonthTo.Text))
            {
                this.txtDateMonthTo.Text = "1";
            }
            else
            {
                // 12を超える月が入力された場合、12月として処理
                if (Util.ToInt(this.txtDateMonthTo.Text) > 12)
                {
                    this.txtDateMonthTo.Text = "12";
                }
                // 1より小さい月が入力された場合、1月として処理
                else if (Util.ToInt(this.txtDateMonthTo.Text) < 1)
                {
                    this.txtDateMonthTo.Text = "1";
                }
            }

            return true;
        }

        /// <summary>
        /// 日(至)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidDateDayTo()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtDateDayTo.Text))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }

            if (ValChk.IsEmpty(this.txtDateDayTo.Text))
            {
                // 空の場合、1日として処理
                this.txtDateDayTo.Text = "1";
            }
            else
            {
                // 1より小さい日が入力された場合、1日として処理
                if (Util.ToInt(this.txtDateDayTo.Text) < 1)
                {
                    this.txtDateDayTo.Text = "1";
                }
            }

            return true;
        }

        /// <summary>
        /// 年月日(至)の月末入力チェック
        /// </summary>
        /// 
        private void CheckDateTo()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                this.txtDateMonthTo.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDateDayTo.Text) > lastDayInMonth)
            {
                this.txtDateDayTo.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(至)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetDateTo()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDateTo(Util.FixJpDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                this.txtDateMonthTo.Text, this.txtDateDayTo.Text, this.Dba));
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpDateFr(string[] arrJpDate)
        {
            this.lblDateGengoFr.Text = arrJpDate[0];
            this.txtDateYearFr.Text = arrJpDate[2];
            this.txtDateMonthFr.Text = arrJpDate[3];
            this.txtDateDayFr.Text = arrJpDate[4];
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpDateTo(string[] arrJpDate)
        {
            this.lblDateGengoTo.Text = arrJpDate[0];
            this.txtDateYearTo.Text = arrJpDate[2];
            this.txtDateMonthTo.Text = arrJpDate[3];
            this.txtDateDayTo.Text = arrJpDate[4];
        }

        /// <summary>
        /// 魚態別CD(自)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidGyotaiBetsuCdFr()
        {
            if (ValChk.IsEmpty(this.txtGyotaiBetsuCdFr.Text))
            {
                this.lblGyotaiBetsuCdFr.Text = "先　頭";
            }
            else
                // 数字のみの入力を許可
                if (!ValChk.IsNumber(this.txtGyotaiBetsuCdFr.Text))
                {
                    Msg.Error("魚態別コードは数値のみで入力してください。");
                    return false;
                }
                else
                {
                    // コードを元に名称を取得する
                    this.lblGyotaiBetsuCdFr.Text = this.Dba.GetName(this.UInfo, "TM_HN_GYOHO_MST", this.txtGyotaiBetsuCdFr.Text);
                }

            return true;
        }


        /// <summary>
        /// 魚態別CD(至)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidGyotaiBetsuCdTo()
        {
            if (ValChk.IsEmpty(this.txtGyotaiBetsuCdTo.Text))
            {
                this.lblGyotaiBetsuCdTo.Text = "最　後";
            }
            else
                // 数字のみの入力を許可
                if (!ValChk.IsNumber(this.txtGyotaiBetsuCdTo.Text))
                {
                    Msg.Error("魚態別コードは数値のみで入力してください。");
                    return false;
                }
                else
                {
                    // コードを元に名称を取得する
                    this.lblGyotaiBetsuCdTo.Text = this.Dba.GetName(this.UInfo, "TM_HN_GYOHO_MST", this.txtGyotaiBetsuCdTo.Text);
                }

            return true;
        }

        /// <summary>
        /// 船主コード(自)の入力チェック
        /// </summary>
        private bool IsValidFunanushiCdFr()
        {
            if (ValChk.IsEmpty(this.txtFunanushiCdFr.Text))
            {
                this.lblFunanushiCdFr.Text = "先　頭";
            }
            else
                // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
                if (!ValChk.IsNumber(this.txtFunanushiCdFr.Text))
                {
                    Msg.Error("船主コードは数値のみで入力してください。");
                    return false;
                }
                else
                {
                    // コードを元に名称を取得する
                    this.lblFunanushiCdFr.Text = this.Dba.GetName(this.UInfo, "VI_HN_FUNANUSHI", this.txtFunanushiCdFr.Text);
                }

            return true;
        }

        /// <summary>
        /// 船主コード(至)の入力チェック
        /// </summary>
        private bool IsValidFunanushiCdTo()
        {
            if (ValChk.IsEmpty(this.txtFunanushiCdTo.Text))
            {
                this.lblFunanushiCdTo.Text = "最　後";
            }
            else
                // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
                if (!ValChk.IsNumber(this.txtFunanushiCdTo.Text))
                {
                    Msg.Error("船主コードは数値のみで入力してください。");
                    return false;
                }
                else
                {
                    // コードを元に名称を取得する
                    this.lblFunanushiCdTo.Text = this.Dba.GetName(this.UInfo, "VI_HN_FUNANUSHI", this.txtFunanushiCdTo.Text);
                }

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 年(自)のチェック
            if (!IsValidDateYearFr())
            {
                this.txtDateYearFr.Focus();
                this.txtDateYearFr.SelectAll();
                return false;
            }
            // 月(自)のチェック
            if (!IsValidDateMonthFr())
            {
                this.txtDateMonthFr.Focus();
                this.txtDateMonthFr.SelectAll();
                return false;
            }
            // 日(自)のチェック
            if (!IsValidDateDayFr())
            {
                this.txtDateDayFr.Focus();
                this.txtDateDayFr.SelectAll();
                return false;
            }
            // 年月日(自)の月末入力チェック処理
            CheckDateFr();
            // 年月日(自)の正しい和暦への変換処理
            SetDateFr();

            // 年(至)のチェック
            if (!IsValidDateYearTo())
            {
                this.txtDateYearTo.Focus();
                this.txtDateYearTo.SelectAll();
                return false;
            }
            // 月(至)のチェック
            if (!IsValidDateMonthTo())
            {
                this.txtDateMonthTo.Focus();
                this.txtDateMonthTo.SelectAll();
                return false;
            }
            // 日(至)のチェック
            if (!IsValidDateDayTo())
            {
                this.txtDateDayTo.Focus();
                this.txtDateDayTo.SelectAll();
                return false;
            }
            // 年月日(至)の月末入力チェック処理
            CheckDateTo();
            // 年月日(至)の正しい和暦への変換処理
            SetDateTo();

            // 魚態別コード(自)の入力チェック
            if (!IsValidGyotaiBetsuCdFr())
            {
                this.txtGyotaiBetsuCdFr.Focus();
                this.txtGyotaiBetsuCdFr.SelectAll();
                return false;
            }
            // 魚態別コード(至)の入力チェック
            if (!IsValidGyotaiBetsuCdTo())
            {
                this.txtGyotaiBetsuCdTo.Focus();
                this.txtGyotaiBetsuCdTo.SelectAll();
                return false;
            }

            // 船主コード(自)の入力チェック
            if (!IsValidFunanushiCdFr())
            {
                this.txtFunanushiCdFr.Focus();
                this.txtFunanushiCdFr.SelectAll();
                return false;
            }
            // 船主コード(至)の入力チェック
            if (!IsValidFunanushiCdTo())
            {
                this.txtFunanushiCdTo.Focus();
                this.txtFunanushiCdTo.SelectAll();
                return false;
            }

            return true;
        }

        /// <summary>
        /// 帳票を印刷する。
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview)
        {
            bool dataFlag;
            try
            {
                // 帳票出力用にワークテーブルにデータを作成
                dataFlag = MakeWkData();
                this.Dba.Commit();
            }
            finally
            {
                this.Dba.Rollback();
            }

            // 帳票出力
            if (dataFlag)
            {
                Report rpt = new Report();
                rpt.OutputReport(Path.Combine(Util.GetPath(), Constants.REP_DIR, "HANR2081.mdb"), "R_HANR2081", this.UnqId, isPreview);
            }

            // ワークテーブルに作成したデータを削除
            try
            {
                // 帳票出力用に作成したデータを削除
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                this.Dba.Delete("PR_HN_TBL", "GUID = @GUID", dpc);
                this.Dba.Commit();
            }
            finally
            {
                this.Dba.Rollback();
            }
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData()
        {
            DbParamCollection dpc = new DbParamCollection();
            // 日付を西暦にして取得
            DateTime tmpDateFr = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                    this.txtDateMonthFr.Text, this.txtDateDayFr.Text, this.Dba);
            DateTime tmpDateTo = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                    this.txtDateMonthTo.Text, this.txtDateDayTo.Text, this.Dba);

            // 日付を和暦で保持
            string[] tmpjpDateFr = Util.ConvJpDate(tmpDateFr, this.Dba);
            string[] tmpjpDateTo = Util.ConvJpDate(tmpDateTo, this.Dba);

            // 魚態別コード設定
            string gyotaiBetsuCdFr;
            string gyotaiBetsuCdTo;
            if (Util.ToDecimal(txtGyotaiBetsuCdFr.Text) > 0)
            {
                gyotaiBetsuCdFr = txtGyotaiBetsuCdFr.Text;
            }
            else
            {
                gyotaiBetsuCdFr = "0";
            }
            if (Util.ToDecimal(txtGyotaiBetsuCdTo.Text) > 0)
            {
                gyotaiBetsuCdTo = txtGyotaiBetsuCdTo.Text;
            }
            else
            {
                gyotaiBetsuCdTo = "9999";
            }

            // 船主コード設定
            string funanushiCdFr;
            string funanushiCdTo;
            if (Util.ToDecimal(txtFunanushiCdFr.Text) > 0)
            {
                funanushiCdFr = txtFunanushiCdFr.Text;
            }
            else
            {
                funanushiCdFr = "0";
            }
            if (Util.ToDecimal(txtFunanushiCdTo.Text) > 0)
            {
                funanushiCdTo = txtFunanushiCdTo.Text;
            }
            else
            {
                funanushiCdTo = "9999";
            }

            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            // 検索する日付をセット
            dpc.SetParam("@SERIBI_FR", SqlDbType.VarChar, 10, tmpDateFr.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@SERIBI_TO", SqlDbType.VarChar, 10, tmpDateTo.Date.ToString("yyyy/MM/dd"));
            // 検索する魚態コードをセット
            dpc.SetParam("@GYOTAI_CD_FR", SqlDbType.Decimal, 4, gyotaiBetsuCdFr);
            dpc.SetParam("@GYOTAI_CD_TO", SqlDbType.Decimal, 4, gyotaiBetsuCdTo);
            // 検索する船主コードをセット
            dpc.SetParam("@FUNANUSHI_CD_FR", SqlDbType.Decimal, 4, funanushiCdFr);
            dpc.SetParam("@FUNANUSHI_CD_TO", SqlDbType.Decimal, 4, funanushiCdTo);

            StringBuilder Sql = new StringBuilder();
            // 検索日付に発生しているデータを取得
            Sql.Append(" SELECT ");
            Sql.Append("     KUMIAIIN_CD,");
            Sql.Append("     KUMIAIIN_NM, ");
            Sql.Append("     GYOTAI_CD,");
            Sql.Append("     GYOTAI_NM, ");
            Sql.Append("     GYOSHU_CD,");
            Sql.Append("     GYOSHU_NM, ");
            Sql.Append("     MIZUAGE_SURYO,  ");
            Sql.Append("     ZEINUKI_MIZUAGE_KINGAKU + MEISAI_SHOHIZEI AS MIZUAGE_KINGAKU, ");
            Sql.Append("     MIZUAGE_KAISU,");
            Sql.Append("     ZEINUKI_MIZUAGE_KINGAKU/MIZUAGE_SURYO AS TANKA ");
            Sql.Append(" FROM ");
            Sql.Append("     VI_HN_GTIBT_GSBT_MZAG_GPP ");
            Sql.Append(" WHERE ");
            Sql.Append("     KAISHA_CD = @KAISHA_CD AND ");
            Sql.Append("     SERIBI BETWEEN @SERIBI_FR  AND @SERIBI_TO AND ");
            Sql.Append("     GYOTAI_CD BETWEEN @GYOTAI_CD_FR AND @GYOTAI_CD_TO AND ");
            Sql.Append("     KUMIAIIN_CD BETWEEN @FUNANUSHI_CD_FR AND @FUNANUSHI_CD_TO AND");
            Sql.Append("     GYOSHU_CD IN ");
            Sql.Append("  (SELECT ");
            Sql.Append("     GYOSHU_CD FROM  TB_HN_GYOSHU_SETTEI  ");
            Sql.Append("  WHERE ");
            Sql.Append("     KUBUN = 1 ");
            Sql.Append("  )");
            Sql.Append(" ORDER BY ");
            Sql.Append("     KUMIAIIN_CD, ");
            Sql.Append("     MIZUAGE_KAISU ");

            DataTable dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            if (dtMainLoop.Rows.Count == 0)
            {
                Msg.Info("該当データがありません。");
                return false;
            }
            else
            {
                dpc = new DbParamCollection();
                int i = 1; // ループ用カウント変数
                string kumiaiinCd = "";
                string mizuageKaisu = "";
                int mizuageKaisuCnt = 0;
                int mizuageKaisuGk = 0;

                string hyojiDate = string.Format("{0}{1}年{2}月{3}日", tmpjpDateFr[0], tmpjpDateFr[2], tmpjpDateFr[3], tmpjpDateFr[4])
                          + "～"
                          + string.Format("{0}{1}年{2}月{3}日", tmpjpDateTo[0], tmpjpDateTo[2], tmpjpDateTo[3], tmpjpDateTo[4])
                          + "分";

                #region 印刷ワークテーブルに登録
                foreach (DataRow dr in dtMainLoop.Rows)
                {
                    if (kumiaiinCd != Util.ToString(dr["KUMIAIIN_CD"]))
                    {
                        mizuageKaisuCnt = 1;
                        mizuageKaisuGk++;
                    }
                    else
                    {
                        if (mizuageKaisu != Util.ToString(dr["MIZUAGE_KAISU"]))
                        {
                            mizuageKaisuCnt++;
                            mizuageKaisuGk++;
                        }
                    }
                    Sql = new StringBuilder();
                    dpc = new DbParamCollection();
                    Sql.Append("INSERT INTO PR_HN_TBL(");
                    Sql.Append("  GUID");
                    Sql.Append(" ,SORT");
                    Sql.Append(" ,ITEM01");
                    Sql.Append(" ,ITEM02");
                    Sql.Append(" ,ITEM03");
                    Sql.Append(" ,ITEM04");
                    Sql.Append(" ,ITEM05");
                    Sql.Append(" ,ITEM06");
                    Sql.Append(" ,ITEM07");
                    Sql.Append(" ,ITEM08");
                    Sql.Append(" ,ITEM09");
                    Sql.Append(" ,ITEM10");
                    Sql.Append(" ,ITEM11");
                    Sql.Append(" ,ITEM12");
                    Sql.Append(") ");
                    Sql.Append("VALUES(");
                    Sql.Append("  @GUID");
                    Sql.Append(" ,@SORT");
                    Sql.Append(" ,@ITEM01");
                    Sql.Append(" ,@ITEM02");
                    Sql.Append(" ,@ITEM03");
                    Sql.Append(" ,@ITEM04");
                    Sql.Append(" ,@ITEM05");
                    Sql.Append(" ,@ITEM06");
                    Sql.Append(" ,@ITEM07");
                    Sql.Append(" ,@ITEM08");
                    Sql.Append(" ,@ITEM09");
                    Sql.Append(" ,@ITEM10");
                    Sql.Append(" ,@ITEM11");
                    Sql.Append(" ,@ITEM12");
                    Sql.Append(") ");

                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, i);

                    // ページヘッダーデータを設定
                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, hyojiDate);
                    // データを設定
                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dr["KUMIAIIN_CD"]);
                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dr["KUMIAIIN_NM"]);
                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dr["GYOTAI_CD"]);
                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, dr["GYOTAI_NM"]);
                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, dr["GYOSHU_CD"]);
                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, dr["GYOSHU_NM"]);
                    dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, dr["TANKA"]);
                    dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, dr["MIZUAGE_SURYO"]);
                    dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, dr["MIZUAGE_KINGAKU"]);
                    dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, mizuageKaisuCnt);
                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, mizuageKaisuGk);

                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                    i++;
                    // 組合員コードの保持
                    kumiaiinCd = Util.ToString(dr["KUMIAIIN_CD"]);
                    // 水揚回数の保持
                    mizuageKaisu = Util.ToString(dr["MIZUAGE_KAISU"]);
                }
                #endregion
            }
                // 印刷ワークテーブルのデータ件数を取得
                dpc = new DbParamCollection();
                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                DataTable tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
                    "SORT",
                    "PR_HN_TBL",
                    "GUID = @GUID",
                    dpc);

                bool dataFlag;
                if (tmpdtPR_HN_TBL.Rows.Count > 0)
                {
                    dataFlag = true;
                }
                else
                {
                    dataFlag = false;
                }

                return dataFlag;
            }
        #endregion


        
    }
}
