﻿namespace jp.co.fsi.han.hanr2081
{
    partial class HANR2081
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblDateFr = new System.Windows.Forms.Label();
            this.lblDateGengoFr = new System.Windows.Forms.Label();
            this.txtDateMonthFr = new jp.co.fsi.common.controls.SosTextBox();
            this.txtDateYearFr = new jp.co.fsi.common.controls.SosTextBox();
            this.txtDateDayFr = new jp.co.fsi.common.controls.SosTextBox();
            this.labelDateYearFr = new System.Windows.Forms.Label();
            this.lblDateMonthFr = new System.Windows.Forms.Label();
            this.lblDateDayFr = new System.Windows.Forms.Label();
            this.gbxDate = new System.Windows.Forms.GroupBox();
            this.lblDateDayTo = new System.Windows.Forms.Label();
            this.lblDateMonthTo = new System.Windows.Forms.Label();
            this.lblDateBet = new System.Windows.Forms.Label();
            this.labelDateYearTo = new System.Windows.Forms.Label();
            this.txtDateDayTo = new jp.co.fsi.common.controls.SosTextBox();
            this.txtDateYearTo = new jp.co.fsi.common.controls.SosTextBox();
            this.txtDateMonthTo = new jp.co.fsi.common.controls.SosTextBox();
            this.lblDateGengoTo = new System.Windows.Forms.Label();
            this.lblDateTo = new System.Windows.Forms.Label();
            this.gbxGyotaiBetsuCd = new System.Windows.Forms.GroupBox();
            this.lblGyotaiBetsuCdBet = new System.Windows.Forms.Label();
            this.txtGyotaiBetsuCdTo = new jp.co.fsi.common.controls.SosTextBox();
            this.lblGyotaiBetsuCdTo = new System.Windows.Forms.Label();
            this.txtGyotaiBetsuCdFr = new jp.co.fsi.common.controls.SosTextBox();
            this.lblGyotaiBetsuCdFr = new System.Windows.Forms.Label();
            this.gbxFunanushiCd = new System.Windows.Forms.GroupBox();
            this.lblFunanushiCdBet = new System.Windows.Forms.Label();
            this.txtFunanushiCdTo = new jp.co.fsi.common.controls.SosTextBox();
            this.lblFunanushiCdTo = new System.Windows.Forms.Label();
            this.txtFunanushiCdFr = new jp.co.fsi.common.controls.SosTextBox();
            this.lblFunanushiCdFr = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.gbxDate.SuspendLayout();
            this.gbxGyotaiBetsuCd.SuspendLayout();
            this.gbxFunanushiCd.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Text = "";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Size = new System.Drawing.Size(847, 100);
            // 
            // lblDateFr
            // 
            this.lblDateFr.BackColor = System.Drawing.Color.Silver;
            this.lblDateFr.Location = new System.Drawing.Point(9, 21);
            this.lblDateFr.Name = "lblDateFr";
            this.lblDateFr.Size = new System.Drawing.Size(197, 29);
            this.lblDateFr.TabIndex = 1;
            // 
            // lblDateGengoFr
            // 
            this.lblDateGengoFr.BackColor = System.Drawing.Color.Silver;
            this.lblDateGengoFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateGengoFr.Location = new System.Drawing.Point(14, 24);
            this.lblDateGengoFr.Name = "lblDateGengoFr";
            this.lblDateGengoFr.Size = new System.Drawing.Size(41, 25);
            this.lblDateGengoFr.TabIndex = 1;
            this.lblDateGengoFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDateMonthFr
            // 
            this.txtDateMonthFr.AutoSizeFromLength = false;
            this.txtDateMonthFr.DisplayLength = null;
            this.txtDateMonthFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateMonthFr.Location = new System.Drawing.Point(104, 25);
            this.txtDateMonthFr.MaxLength = 2;
            this.txtDateMonthFr.Name = "txtDateMonthFr";
            this.txtDateMonthFr.Size = new System.Drawing.Size(30, 20);
            this.txtDateMonthFr.TabIndex = 4;
            this.txtDateMonthFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateMonthFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateMonthFr_Validating);
            // 
            // txtDateYearFr
            // 
            this.txtDateYearFr.AutoSizeFromLength = false;
            this.txtDateYearFr.DisplayLength = null;
            this.txtDateYearFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateYearFr.Location = new System.Drawing.Point(54, 25);
            this.txtDateYearFr.MaxLength = 2;
            this.txtDateYearFr.Name = "txtDateYearFr";
            this.txtDateYearFr.Size = new System.Drawing.Size(30, 20);
            this.txtDateYearFr.TabIndex = 2;
            this.txtDateYearFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateYearFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateYearFr_Validating);
            // 
            // txtDateDayFr
            // 
            this.txtDateDayFr.AutoSizeFromLength = false;
            this.txtDateDayFr.DisplayLength = null;
            this.txtDateDayFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateDayFr.Location = new System.Drawing.Point(154, 25);
            this.txtDateDayFr.MaxLength = 2;
            this.txtDateDayFr.Name = "txtDateDayFr";
            this.txtDateDayFr.Size = new System.Drawing.Size(30, 20);
            this.txtDateDayFr.TabIndex = 6;
            this.txtDateDayFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateDayFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateDayFr_Validating);
            // 
            // labelDateYearFr
            // 
            this.labelDateYearFr.BackColor = System.Drawing.Color.Silver;
            this.labelDateYearFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.labelDateYearFr.Location = new System.Drawing.Point(86, 25);
            this.labelDateYearFr.Name = "labelDateYearFr";
            this.labelDateYearFr.Size = new System.Drawing.Size(17, 21);
            this.labelDateYearFr.TabIndex = 3;
            this.labelDateYearFr.Text = "年";
            this.labelDateYearFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateMonthFr
            // 
            this.lblDateMonthFr.BackColor = System.Drawing.Color.Silver;
            this.lblDateMonthFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateMonthFr.Location = new System.Drawing.Point(136, 25);
            this.lblDateMonthFr.Name = "lblDateMonthFr";
            this.lblDateMonthFr.Size = new System.Drawing.Size(15, 19);
            this.lblDateMonthFr.TabIndex = 5;
            this.lblDateMonthFr.Text = "月";
            this.lblDateMonthFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateDayFr
            // 
            this.lblDateDayFr.BackColor = System.Drawing.Color.Silver;
            this.lblDateDayFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateDayFr.Location = new System.Drawing.Point(186, 25);
            this.lblDateDayFr.Name = "lblDateDayFr";
            this.lblDateDayFr.Size = new System.Drawing.Size(20, 18);
            this.lblDateDayFr.TabIndex = 7;
            this.lblDateDayFr.Text = "日";
            this.lblDateDayFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // gbxDate
            // 
            this.gbxDate.Controls.Add(this.lblDateDayTo);
            this.gbxDate.Controls.Add(this.lblDateDayFr);
            this.gbxDate.Controls.Add(this.lblDateMonthTo);
            this.gbxDate.Controls.Add(this.lblDateBet);
            this.gbxDate.Controls.Add(this.lblDateMonthFr);
            this.gbxDate.Controls.Add(this.labelDateYearTo);
            this.gbxDate.Controls.Add(this.labelDateYearFr);
            this.gbxDate.Controls.Add(this.txtDateDayTo);
            this.gbxDate.Controls.Add(this.txtDateDayFr);
            this.gbxDate.Controls.Add(this.txtDateYearTo);
            this.gbxDate.Controls.Add(this.txtDateYearFr);
            this.gbxDate.Controls.Add(this.txtDateMonthTo);
            this.gbxDate.Controls.Add(this.txtDateMonthFr);
            this.gbxDate.Controls.Add(this.lblDateGengoTo);
            this.gbxDate.Controls.Add(this.lblDateTo);
            this.gbxDate.Controls.Add(this.lblDateGengoFr);
            this.gbxDate.Controls.Add(this.lblDateFr);
            this.gbxDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxDate.ForeColor = System.Drawing.SystemColors.ControlText;
            this.gbxDate.Location = new System.Drawing.Point(18, 52);
            this.gbxDate.Name = "gbxDate";
            this.gbxDate.Size = new System.Drawing.Size(457, 61);
            this.gbxDate.TabIndex = 1;
            this.gbxDate.TabStop = false;
            this.gbxDate.Text = "日付範囲";
            // 
            // lblDateDayTo
            // 
            this.lblDateDayTo.BackColor = System.Drawing.Color.Silver;
            this.lblDateDayTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateDayTo.Location = new System.Drawing.Point(415, 25);
            this.lblDateDayTo.Name = "lblDateDayTo";
            this.lblDateDayTo.Size = new System.Drawing.Size(20, 18);
            this.lblDateDayTo.TabIndex = 16;
            this.lblDateDayTo.Text = "日";
            this.lblDateDayTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateMonthTo
            // 
            this.lblDateMonthTo.BackColor = System.Drawing.Color.Silver;
            this.lblDateMonthTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateMonthTo.Location = new System.Drawing.Point(365, 25);
            this.lblDateMonthTo.Name = "lblDateMonthTo";
            this.lblDateMonthTo.Size = new System.Drawing.Size(15, 19);
            this.lblDateMonthTo.TabIndex = 14;
            this.lblDateMonthTo.Text = "月";
            this.lblDateMonthTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateBet
            // 
            this.lblDateBet.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateBet.Location = new System.Drawing.Point(212, 23);
            this.lblDateBet.Name = "lblDateBet";
            this.lblDateBet.Size = new System.Drawing.Size(20, 25);
            this.lblDateBet.TabIndex = 8;
            this.lblDateBet.Text = "～";
            this.lblDateBet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelDateYearTo
            // 
            this.labelDateYearTo.BackColor = System.Drawing.Color.Silver;
            this.labelDateYearTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.labelDateYearTo.Location = new System.Drawing.Point(315, 25);
            this.labelDateYearTo.Name = "labelDateYearTo";
            this.labelDateYearTo.Size = new System.Drawing.Size(17, 21);
            this.labelDateYearTo.TabIndex = 12;
            this.labelDateYearTo.Text = "年";
            this.labelDateYearTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDateDayTo
            // 
            this.txtDateDayTo.AutoSizeFromLength = false;
            this.txtDateDayTo.DisplayLength = null;
            this.txtDateDayTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateDayTo.Location = new System.Drawing.Point(383, 25);
            this.txtDateDayTo.MaxLength = 2;
            this.txtDateDayTo.Name = "txtDateDayTo";
            this.txtDateDayTo.Size = new System.Drawing.Size(30, 20);
            this.txtDateDayTo.TabIndex = 15;
            this.txtDateDayTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateDayTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateDayTo_Validating);
            // 
            // txtDateYearTo
            // 
            this.txtDateYearTo.AutoSizeFromLength = false;
            this.txtDateYearTo.DisplayLength = null;
            this.txtDateYearTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateYearTo.Location = new System.Drawing.Point(283, 25);
            this.txtDateYearTo.MaxLength = 2;
            this.txtDateYearTo.Name = "txtDateYearTo";
            this.txtDateYearTo.Size = new System.Drawing.Size(30, 20);
            this.txtDateYearTo.TabIndex = 11;
            this.txtDateYearTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateYearTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateYearTo_Validating);
            // 
            // txtDateMonthTo
            // 
            this.txtDateMonthTo.AutoSizeFromLength = false;
            this.txtDateMonthTo.DisplayLength = null;
            this.txtDateMonthTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateMonthTo.Location = new System.Drawing.Point(333, 25);
            this.txtDateMonthTo.MaxLength = 2;
            this.txtDateMonthTo.Name = "txtDateMonthTo";
            this.txtDateMonthTo.Size = new System.Drawing.Size(30, 20);
            this.txtDateMonthTo.TabIndex = 13;
            this.txtDateMonthTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateMonthTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateMonthTo_Validating);
            // 
            // lblDateGengoTo
            // 
            this.lblDateGengoTo.BackColor = System.Drawing.Color.Silver;
            this.lblDateGengoTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateGengoTo.Location = new System.Drawing.Point(243, 24);
            this.lblDateGengoTo.Name = "lblDateGengoTo";
            this.lblDateGengoTo.Size = new System.Drawing.Size(41, 22);
            this.lblDateGengoTo.TabIndex = 10;
            this.lblDateGengoTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateTo
            // 
            this.lblDateTo.BackColor = System.Drawing.Color.Silver;
            this.lblDateTo.Location = new System.Drawing.Point(238, 21);
            this.lblDateTo.Name = "lblDateTo";
            this.lblDateTo.Size = new System.Drawing.Size(197, 29);
            this.lblDateTo.TabIndex = 9;
            // 
            // gbxGyotaiBetsuCd
            // 
            this.gbxGyotaiBetsuCd.BackColor = System.Drawing.Color.White;
            this.gbxGyotaiBetsuCd.Controls.Add(this.lblGyotaiBetsuCdBet);
            this.gbxGyotaiBetsuCd.Controls.Add(this.txtGyotaiBetsuCdTo);
            this.gbxGyotaiBetsuCd.Controls.Add(this.lblGyotaiBetsuCdTo);
            this.gbxGyotaiBetsuCd.Controls.Add(this.txtGyotaiBetsuCdFr);
            this.gbxGyotaiBetsuCd.Controls.Add(this.lblGyotaiBetsuCdFr);
            this.gbxGyotaiBetsuCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxGyotaiBetsuCd.ForeColor = System.Drawing.SystemColors.ControlText;
            this.gbxGyotaiBetsuCd.Location = new System.Drawing.Point(18, 119);
            this.gbxGyotaiBetsuCd.Name = "gbxGyotaiBetsuCd";
            this.gbxGyotaiBetsuCd.Size = new System.Drawing.Size(559, 50);
            this.gbxGyotaiBetsuCd.TabIndex = 17;
            this.gbxGyotaiBetsuCd.TabStop = false;
            this.gbxGyotaiBetsuCd.Text = "魚態別CD範囲";
            // 
            // lblGyotaiBetsuCdBet
            // 
            this.lblGyotaiBetsuCdBet.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGyotaiBetsuCdBet.Location = new System.Drawing.Point(267, 18);
            this.lblGyotaiBetsuCdBet.Name = "lblGyotaiBetsuCdBet";
            this.lblGyotaiBetsuCdBet.Size = new System.Drawing.Size(20, 20);
            this.lblGyotaiBetsuCdBet.TabIndex = 20;
            this.lblGyotaiBetsuCdBet.Text = "～";
            this.lblGyotaiBetsuCdBet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtGyotaiBetsuCdTo
            // 
            this.txtGyotaiBetsuCdTo.AutoSizeFromLength = false;
            this.txtGyotaiBetsuCdTo.DisplayLength = null;
            this.txtGyotaiBetsuCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGyotaiBetsuCdTo.Location = new System.Drawing.Point(290, 18);
            this.txtGyotaiBetsuCdTo.MaxLength = 4;
            this.txtGyotaiBetsuCdTo.Name = "txtGyotaiBetsuCdTo";
            this.txtGyotaiBetsuCdTo.Size = new System.Drawing.Size(51, 20);
            this.txtGyotaiBetsuCdTo.TabIndex = 21;
            this.txtGyotaiBetsuCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGyotaiBetsuCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtGyotaiBetsuCdTo_Validating);
            // 
            // lblGyotaiBetsuCdTo
            // 
            this.lblGyotaiBetsuCdTo.BackColor = System.Drawing.Color.Silver;
            this.lblGyotaiBetsuCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGyotaiBetsuCdTo.Location = new System.Drawing.Point(347, 18);
            this.lblGyotaiBetsuCdTo.Name = "lblGyotaiBetsuCdTo";
            this.lblGyotaiBetsuCdTo.Size = new System.Drawing.Size(197, 20);
            this.lblGyotaiBetsuCdTo.TabIndex = 22;
            this.lblGyotaiBetsuCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtGyotaiBetsuCdFr
            // 
            this.txtGyotaiBetsuCdFr.AutoSizeFromLength = false;
            this.txtGyotaiBetsuCdFr.DisplayLength = null;
            this.txtGyotaiBetsuCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGyotaiBetsuCdFr.Location = new System.Drawing.Point(10, 18);
            this.txtGyotaiBetsuCdFr.MaxLength = 4;
            this.txtGyotaiBetsuCdFr.Name = "txtGyotaiBetsuCdFr";
            this.txtGyotaiBetsuCdFr.Size = new System.Drawing.Size(51, 20);
            this.txtGyotaiBetsuCdFr.TabIndex = 18;
            this.txtGyotaiBetsuCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGyotaiBetsuCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtGyotaiBetsuCdFr_Validating);
            // 
            // lblGyotaiBetsuCdFr
            // 
            this.lblGyotaiBetsuCdFr.BackColor = System.Drawing.Color.Silver;
            this.lblGyotaiBetsuCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGyotaiBetsuCdFr.Location = new System.Drawing.Point(67, 18);
            this.lblGyotaiBetsuCdFr.Name = "lblGyotaiBetsuCdFr";
            this.lblGyotaiBetsuCdFr.Size = new System.Drawing.Size(197, 20);
            this.lblGyotaiBetsuCdFr.TabIndex = 19;
            this.lblGyotaiBetsuCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // gbxFunanushiCd
            // 
            this.gbxFunanushiCd.BackColor = System.Drawing.Color.White;
            this.gbxFunanushiCd.Controls.Add(this.lblFunanushiCdBet);
            this.gbxFunanushiCd.Controls.Add(this.txtFunanushiCdTo);
            this.gbxFunanushiCd.Controls.Add(this.lblFunanushiCdTo);
            this.gbxFunanushiCd.Controls.Add(this.txtFunanushiCdFr);
            this.gbxFunanushiCd.Controls.Add(this.lblFunanushiCdFr);
            this.gbxFunanushiCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxFunanushiCd.ForeColor = System.Drawing.SystemColors.ControlText;
            this.gbxFunanushiCd.Location = new System.Drawing.Point(18, 175);
            this.gbxFunanushiCd.Name = "gbxFunanushiCd";
            this.gbxFunanushiCd.Size = new System.Drawing.Size(559, 50);
            this.gbxFunanushiCd.TabIndex = 23;
            this.gbxFunanushiCd.TabStop = false;
            this.gbxFunanushiCd.Text = "船主CD範囲";
            // 
            // lblFunanushiCdBet
            // 
            this.lblFunanushiCdBet.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFunanushiCdBet.Location = new System.Drawing.Point(267, 18);
            this.lblFunanushiCdBet.Name = "lblFunanushiCdBet";
            this.lblFunanushiCdBet.Size = new System.Drawing.Size(20, 20);
            this.lblFunanushiCdBet.TabIndex = 26;
            this.lblFunanushiCdBet.Text = "～";
            this.lblFunanushiCdBet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFunanushiCdTo
            // 
            this.txtFunanushiCdTo.AutoSizeFromLength = false;
            this.txtFunanushiCdTo.DisplayLength = null;
            this.txtFunanushiCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFunanushiCdTo.Location = new System.Drawing.Point(290, 18);
            this.txtFunanushiCdTo.MaxLength = 4;
            this.txtFunanushiCdTo.Name = "txtFunanushiCdTo";
            this.txtFunanushiCdTo.Size = new System.Drawing.Size(51, 20);
            this.txtFunanushiCdTo.TabIndex = 27;
            this.txtFunanushiCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFunanushiCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtFunanushiCdTo_Validating);
            // 
            // lblFunanushiCdTo
            // 
            this.lblFunanushiCdTo.BackColor = System.Drawing.Color.Silver;
            this.lblFunanushiCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFunanushiCdTo.Location = new System.Drawing.Point(347, 18);
            this.lblFunanushiCdTo.Name = "lblFunanushiCdTo";
            this.lblFunanushiCdTo.Size = new System.Drawing.Size(197, 20);
            this.lblFunanushiCdTo.TabIndex = 28;
            this.lblFunanushiCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFunanushiCdFr
            // 
            this.txtFunanushiCdFr.AutoSizeFromLength = false;
            this.txtFunanushiCdFr.DisplayLength = null;
            this.txtFunanushiCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFunanushiCdFr.Location = new System.Drawing.Point(10, 18);
            this.txtFunanushiCdFr.MaxLength = 4;
            this.txtFunanushiCdFr.Name = "txtFunanushiCdFr";
            this.txtFunanushiCdFr.Size = new System.Drawing.Size(51, 20);
            this.txtFunanushiCdFr.TabIndex = 24;
            this.txtFunanushiCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFunanushiCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtFunanushiCdFr_Validating);
            // 
            // lblFunanushiCdFr
            // 
            this.lblFunanushiCdFr.BackColor = System.Drawing.Color.Silver;
            this.lblFunanushiCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFunanushiCdFr.Location = new System.Drawing.Point(67, 18);
            this.lblFunanushiCdFr.Name = "lblFunanushiCdFr";
            this.lblFunanushiCdFr.Size = new System.Drawing.Size(197, 20);
            this.lblFunanushiCdFr.TabIndex = 25;
            this.lblFunanushiCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // HANR2081
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 638);
            this.Controls.Add(this.gbxFunanushiCd);
            this.Controls.Add(this.gbxGyotaiBetsuCd);
            this.Controls.Add(this.gbxDate);
            this.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "HANR2081";
            this.Text = "";
            this.Controls.SetChildIndex(this.gbxDate, 0);
            this.Controls.SetChildIndex(this.gbxGyotaiBetsuCd, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.gbxFunanushiCd, 0);
            this.pnlDebug.ResumeLayout(false);
            this.gbxDate.ResumeLayout(false);
            this.gbxDate.PerformLayout();
            this.gbxGyotaiBetsuCd.ResumeLayout(false);
            this.gbxGyotaiBetsuCd.PerformLayout();
            this.gbxFunanushiCd.ResumeLayout(false);
            this.gbxFunanushiCd.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblDateFr;
        private System.Windows.Forms.Label lblDateGengoFr;
        private jp.co.fsi.common.controls.SosTextBox txtDateMonthFr;
        private jp.co.fsi.common.controls.SosTextBox txtDateYearFr;
        private jp.co.fsi.common.controls.SosTextBox txtDateDayFr;
        private System.Windows.Forms.Label labelDateYearFr;
        private System.Windows.Forms.Label lblDateMonthFr;
        private System.Windows.Forms.Label lblDateDayFr;
        private System.Windows.Forms.GroupBox gbxDate;
        private System.Windows.Forms.Label lblDateDayTo;
        private System.Windows.Forms.Label lblDateMonthTo;
        private System.Windows.Forms.Label labelDateYearTo;
        private common.controls.SosTextBox txtDateDayTo;
        private common.controls.SosTextBox txtDateYearTo;
        private common.controls.SosTextBox txtDateMonthTo;
        private System.Windows.Forms.Label lblDateGengoTo;
        private System.Windows.Forms.Label lblDateTo;
        private System.Windows.Forms.Label lblDateBet;
        private System.Windows.Forms.GroupBox gbxGyotaiBetsuCd;
        private System.Windows.Forms.Label lblGyotaiBetsuCdBet;
        private common.controls.SosTextBox txtGyotaiBetsuCdTo;
        private System.Windows.Forms.Label lblGyotaiBetsuCdTo;
        private common.controls.SosTextBox txtGyotaiBetsuCdFr;
        private System.Windows.Forms.Label lblGyotaiBetsuCdFr;
        private System.Windows.Forms.GroupBox gbxFunanushiCd;
        private System.Windows.Forms.Label lblFunanushiCdBet;
        private common.controls.SosTextBox txtFunanushiCdTo;
        private System.Windows.Forms.Label lblFunanushiCdTo;
        private common.controls.SosTextBox txtFunanushiCdFr;
        private System.Windows.Forms.Label lblFunanushiCdFr;

    }
}