﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using jp.co.fsi.common.report;
using jp.co.fsi.common.util;
using System.Globalization;

namespace jp.co.fsi.hn.hnyr1051
{
    /// <summary>
    /// HNYR1051R の概要の説明です。
    /// </summary>
    public partial class HNYR1051R : BaseReport
    {

        public HNYR1051R(DataTable tgtData): base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }

        private void detail_Format(object sender, EventArgs e)
        {
            ////西暦から和暦に変換
            //CultureInfo culture = new CultureInfo("ja-JP", true);
            //culture.DateTimeFormat.Calendar = new JapaneseCalendar();
            //txtToday.Text = DateTime.Now.ToString("ggyy年M月d日", culture);


            this.水揚数量合計.Value = Util.ToDecimal(this.テキスト003.Value) + Util.ToDecimal(this.テキスト006.Value) + Util.ToDecimal(this.テキスト009.Value) + Util.ToDecimal(this.テキスト012.Value) + Util.ToDecimal(this.テキスト015.Value) + Util.ToDecimal(this.テキスト018.Value) 
                                    + Util.ToDecimal(this.テキスト021.Value) + Util.ToDecimal(this.テキスト024.Value) + Util.ToDecimal(this.テキスト027.Value) + Util.ToDecimal(this.テキスト030.Value) + Util.ToDecimal(this.テキスト033.Value) + Util.ToDecimal(this.テキスト036.Value);

            this.水揚金額合計.Value = Util.ToDecimal(this.テキスト004.Value) + Util.ToDecimal(this.テキスト007.Value) + Util.ToDecimal(this.テキスト010.Value) + Util.ToDecimal(this.テキスト013.Value) + Util.ToDecimal(this.テキスト016.Value) + Util.ToDecimal(this.テキスト019.Value)
                                    + Util.ToDecimal(this.テキスト022.Value) + Util.ToDecimal(this.テキスト025.Value) + Util.ToDecimal(this.テキスト028.Value) + Util.ToDecimal(this.テキスト031.Value) + Util.ToDecimal(this.テキスト034.Value) + Util.ToDecimal(this.テキスト037.Value);

            this.手数料合計.Value = Util.ToDecimal(this.テキスト005.Value) + Util.ToDecimal(this.テキスト008.Value) + Util.ToDecimal(this.テキスト011.Value) + Util.ToDecimal(this.テキスト014.Value) + Util.ToDecimal(this.テキスト017.Value) + Util.ToDecimal(this.テキスト020.Value)
                                    + Util.ToDecimal(this.テキスト023.Value) + Util.ToDecimal(this.テキスト026.Value) + Util.ToDecimal(this.テキスト029.Value) + Util.ToDecimal(this.テキスト032.Value) + Util.ToDecimal(this.テキスト035.Value) + Util.ToDecimal(this.テキスト038.Value);
        }

        private void pageFooter_Format(object sender, EventArgs e)
        {

        }
    }
}
