﻿namespace jp.co.fsi.hn.hncr1011
{
    partial class HNCR1011
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbxFunanushiCd = new System.Windows.Forms.GroupBox();
            this.lblFunanushiCdTo = new System.Windows.Forms.Label();
            this.lblCodeBet = new System.Windows.Forms.Label();
            this.txtFunanushiCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblFunanushiCdFr = new System.Windows.Forms.Label();
            this.txtFunanushiCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.gbxSeijunKubun = new System.Windows.Forms.GroupBox();
            this.txtSeijunKubunCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblSeijunKubunNm = new System.Windows.Forms.Label();
            this.gbxDate = new System.Windows.Forms.GroupBox();
            this.txtJpYear = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblJpYear = new System.Windows.Forms.Label();
            this.lblJpNengo = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.gbxFunanushiCd.SuspendLayout();
            this.gbxSeijunKubun.SuspendLayout();
            this.gbxDate.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.TabIndex = 999;
            this.lblTitle.Text = "取引先マスタ一覧";
            // 
            // btnF2
            // 
            this.btnF2.Visible = false;
            // 
            // btnF3
            // 
            this.btnF3.Visible = false;
            // 
            // btnF8
            // 
            this.btnF8.Visible = false;
            // 
            // btnF9
            // 
            this.btnF9.Visible = false;
            // 
            // btnF12
            // 
            this.btnF12.Visible = false;
            // 
            // btnF11
            // 
            this.btnF11.Visible = false;
            // 
            // btnF10
            // 
            this.btnF10.Visible = false;
            // 
            // pnlDebug
            // 
            this.pnlDebug.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.pnlDebug.Size = new System.Drawing.Size(866, 99);
            // 
            // gbxFunanushiCd
            // 
            this.gbxFunanushiCd.Controls.Add(this.lblFunanushiCdTo);
            this.gbxFunanushiCd.Controls.Add(this.lblCodeBet);
            this.gbxFunanushiCd.Controls.Add(this.txtFunanushiCdFr);
            this.gbxFunanushiCd.Controls.Add(this.lblFunanushiCdFr);
            this.gbxFunanushiCd.Controls.Add(this.txtFunanushiCdTo);
            this.gbxFunanushiCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.gbxFunanushiCd.ForeColor = System.Drawing.Color.Black;
            this.gbxFunanushiCd.Location = new System.Drawing.Point(12, 114);
            this.gbxFunanushiCd.Name = "gbxFunanushiCd";
            this.gbxFunanushiCd.Size = new System.Drawing.Size(583, 72);
            this.gbxFunanushiCd.TabIndex = 2;
            this.gbxFunanushiCd.TabStop = false;
            this.gbxFunanushiCd.Text = "船主CD範囲";
            // 
            // lblFunanushiCdTo
            // 
            this.lblFunanushiCdTo.BackColor = System.Drawing.Color.Silver;
            this.lblFunanushiCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFunanushiCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblFunanushiCdTo.Location = new System.Drawing.Point(351, 29);
            this.lblFunanushiCdTo.Name = "lblFunanushiCdTo";
            this.lblFunanushiCdTo.Size = new System.Drawing.Size(217, 20);
            this.lblFunanushiCdTo.TabIndex = 4;
            this.lblFunanushiCdTo.Text = "最　後";
            this.lblFunanushiCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCodeBet
            // 
            this.lblCodeBet.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblCodeBet.Location = new System.Drawing.Point(282, 29);
            this.lblCodeBet.Name = "lblCodeBet";
            this.lblCodeBet.Size = new System.Drawing.Size(18, 20);
            this.lblCodeBet.TabIndex = 2;
            this.lblCodeBet.Text = "～";
            this.lblCodeBet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFunanushiCdFr
            // 
            this.txtFunanushiCdFr.AutoSizeFromLength = false;
            this.txtFunanushiCdFr.DisplayLength = null;
            this.txtFunanushiCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtFunanushiCdFr.Location = new System.Drawing.Point(17, 29);
            this.txtFunanushiCdFr.MaxLength = 4;
            this.txtFunanushiCdFr.Name = "txtFunanushiCdFr";
            this.txtFunanushiCdFr.Size = new System.Drawing.Size(42, 20);
            this.txtFunanushiCdFr.TabIndex = 0;
            this.txtFunanushiCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFunanushiCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtFunanushiCdFr_Validating);
            // 
            // lblFunanushiCdFr
            // 
            this.lblFunanushiCdFr.BackColor = System.Drawing.Color.Silver;
            this.lblFunanushiCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFunanushiCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblFunanushiCdFr.Location = new System.Drawing.Point(61, 29);
            this.lblFunanushiCdFr.Name = "lblFunanushiCdFr";
            this.lblFunanushiCdFr.Size = new System.Drawing.Size(217, 20);
            this.lblFunanushiCdFr.TabIndex = 1;
            this.lblFunanushiCdFr.Text = "先　頭";
            this.lblFunanushiCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFunanushiCdTo
            // 
            this.txtFunanushiCdTo.AutoSizeFromLength = false;
            this.txtFunanushiCdTo.DisplayLength = null;
            this.txtFunanushiCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtFunanushiCdTo.Location = new System.Drawing.Point(306, 29);
            this.txtFunanushiCdTo.MaxLength = 4;
            this.txtFunanushiCdTo.Name = "txtFunanushiCdTo";
            this.txtFunanushiCdTo.Size = new System.Drawing.Size(42, 20);
            this.txtFunanushiCdTo.TabIndex = 3;
            this.txtFunanushiCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFunanushiCdTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtFunanushiCdTo_KeyDown);
            this.txtFunanushiCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtFunanushiCdTo_Validating);
            // 
            // gbxSeijunKubun
            // 
            this.gbxSeijunKubun.Controls.Add(this.txtSeijunKubunCd);
            this.gbxSeijunKubun.Controls.Add(this.lblSeijunKubunNm);
            this.gbxSeijunKubun.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.gbxSeijunKubun.ForeColor = System.Drawing.Color.Black;
            this.gbxSeijunKubun.Location = new System.Drawing.Point(12, 45);
            this.gbxSeijunKubun.Name = "gbxSeijunKubun";
            this.gbxSeijunKubun.Size = new System.Drawing.Size(385, 63);
            this.gbxSeijunKubun.TabIndex = 0;
            this.gbxSeijunKubun.TabStop = false;
            this.gbxSeijunKubun.Text = "正準区分";
            // 
            // txtSeijunKubunCd
            // 
            this.txtSeijunKubunCd.AutoSizeFromLength = false;
            this.txtSeijunKubunCd.DisplayLength = null;
            this.txtSeijunKubunCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtSeijunKubunCd.Location = new System.Drawing.Point(16, 25);
            this.txtSeijunKubunCd.MaxLength = 4;
            this.txtSeijunKubunCd.Name = "txtSeijunKubunCd";
            this.txtSeijunKubunCd.Size = new System.Drawing.Size(42, 20);
            this.txtSeijunKubunCd.TabIndex = 0;
            this.txtSeijunKubunCd.Text = "0";
            this.txtSeijunKubunCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSeijunKubunCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtSeijunKubunCd_Validating);
            // 
            // lblSeijunKubunNm
            // 
            this.lblSeijunKubunNm.BackColor = System.Drawing.Color.Silver;
            this.lblSeijunKubunNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSeijunKubunNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblSeijunKubunNm.Location = new System.Drawing.Point(60, 25);
            this.lblSeijunKubunNm.Name = "lblSeijunKubunNm";
            this.lblSeijunKubunNm.Size = new System.Drawing.Size(309, 20);
            this.lblSeijunKubunNm.TabIndex = 1;
            this.lblSeijunKubunNm.Text = "1:正組合員　  2:準組合員　  3:組合員以外";
            this.lblSeijunKubunNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // gbxDate
            // 
            this.gbxDate.Controls.Add(this.txtJpYear);
            this.gbxDate.Controls.Add(this.lblJpYear);
            this.gbxDate.Controls.Add(this.lblJpNengo);
            this.gbxDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.gbxDate.ForeColor = System.Drawing.Color.Black;
            this.gbxDate.Location = new System.Drawing.Point(408, 45);
            this.gbxDate.Name = "gbxDate";
            this.gbxDate.Size = new System.Drawing.Size(157, 63);
            this.gbxDate.TabIndex = 1;
            this.gbxDate.TabStop = false;
            this.gbxDate.Text = "年度";
            this.gbxDate.Visible = false;
            // 
            // txtJpYear
            // 
            this.txtJpYear.AutoSizeFromLength = false;
            this.txtJpYear.BackColor = System.Drawing.Color.White;
            this.txtJpYear.DisplayLength = null;
            this.txtJpYear.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtJpYear.Location = new System.Drawing.Point(66, 25);
            this.txtJpYear.MaxLength = 2;
            this.txtJpYear.Name = "txtJpYear";
            this.txtJpYear.Size = new System.Drawing.Size(32, 20);
            this.txtJpYear.TabIndex = 1;
            this.txtJpYear.Text = "0";
            this.txtJpYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtJpYear.Validating += new System.ComponentModel.CancelEventHandler(this.txtJpYear_Validating);
            // 
            // lblJpYear
            // 
            this.lblJpYear.BackColor = System.Drawing.Color.Silver;
            this.lblJpYear.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblJpYear.Location = new System.Drawing.Point(100, 25);
            this.lblJpYear.Name = "lblJpYear";
            this.lblJpYear.Size = new System.Drawing.Size(40, 20);
            this.lblJpYear.TabIndex = 2;
            this.lblJpYear.Text = "年度";
            this.lblJpYear.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblJpYear.UseWaitCursor = true;
            // 
            // lblJpNengo
            // 
            this.lblJpNengo.BackColor = System.Drawing.Color.Silver;
            this.lblJpNengo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblJpNengo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblJpNengo.Location = new System.Drawing.Point(20, 25);
            this.lblJpNengo.Name = "lblJpNengo";
            this.lblJpNengo.Size = new System.Drawing.Size(45, 20);
            this.lblJpNengo.TabIndex = 0;
            this.lblJpNengo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // HNCR1011
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 638);
            this.Controls.Add(this.gbxDate);
            this.Controls.Add(this.gbxSeijunKubun);
            this.Controls.Add(this.gbxFunanushiCd);
            this.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "HNCR1011";
            this.ShowFButton = true;
            this.Text = "ReportSample";
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.gbxFunanushiCd, 0);
            this.Controls.SetChildIndex(this.gbxSeijunKubun, 0);
            this.Controls.SetChildIndex(this.gbxDate, 0);
            this.pnlDebug.ResumeLayout(false);
            this.gbxFunanushiCd.ResumeLayout(false);
            this.gbxFunanushiCd.PerformLayout();
            this.gbxSeijunKubun.ResumeLayout(false);
            this.gbxSeijunKubun.PerformLayout();
            this.gbxDate.ResumeLayout(false);
            this.gbxDate.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbxFunanushiCd;
        private System.Windows.Forms.Label lblFunanushiCdTo;
        private System.Windows.Forms.Label lblCodeBet;
        private common.controls.FsiTextBox txtFunanushiCdFr;
        private System.Windows.Forms.Label lblFunanushiCdFr;
        private common.controls.FsiTextBox txtFunanushiCdTo;
        private System.Windows.Forms.GroupBox gbxSeijunKubun;
        private common.controls.FsiTextBox txtSeijunKubunCd;
        private System.Windows.Forms.Label lblSeijunKubunNm;
        private System.Windows.Forms.GroupBox gbxDate;
        private common.controls.FsiTextBox txtJpYear;
        private System.Windows.Forms.Label lblJpYear;
        private System.Windows.Forms.Label lblJpNengo;


    }
}