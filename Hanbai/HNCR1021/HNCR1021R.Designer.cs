﻿namespace jp.co.fsi.hn.hncr1021
{
    /// <summary>
    /// HNCR1021R の概要の説明です。
    /// </summary>
    partial class HNCR1021R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(HNCR1021R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.txtToday_tate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtNakagaininCdTo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitleName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtNakagaininCdFr = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblBet01 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtTitle02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle04 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle05 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle06 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle07 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle08 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle09 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtNakagaininNmTo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtNakagaininNmFr = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblBet02 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblBet03 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtValue01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue05 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue06 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue07 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue08 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtValue04 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox09 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            ((System.ComponentModel.ISupportInitialize)(this.txtToday_tate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNakagaininCdTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNakagaininCdFr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBet01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle04)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle05)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle06)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle07)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle08)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle09)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNakagaininNmTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNakagaininNmFr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBet02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBet03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue05)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue06)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue07)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue08)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue04)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox09)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtToday_tate,
            this.txtNakagaininCdTo,
            this.txtTitleName,
            this.txtNakagaininCdFr,
            this.lblBet01,
            this.txtTitle02,
            this.txtTitle01,
            this.txtTitle03,
            this.txtTitle04,
            this.txtTitle05,
            this.txtTitle06,
            this.txtTitle07,
            this.txtTitle08,
            this.txtTitle09,
            this.line1,
            this.txtNakagaininNmTo,
            this.txtNakagaininNmFr,
            this.lblBet02,
            this.lblBet03});
            this.pageHeader.Height = 1.001427F;
            this.pageHeader.Name = "pageHeader";
            this.pageHeader.Format += new System.EventHandler(this.pageHeader_Format);
            // 
            // txtToday_tate
            // 
            this.txtToday_tate.Height = 0.1968504F;
            this.txtToday_tate.Left = 10.02874F;
            this.txtToday_tate.MultiLine = false;
            this.txtToday_tate.Name = "txtToday_tate";
            this.txtToday_tate.OutputFormat = resources.GetString("txtToday_tate.OutputFormat");
            this.txtToday_tate.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
            this.txtToday_tate.Text = "yyyy/MM/dd";
            this.txtToday_tate.Top = 0.1559055F;
            this.txtToday_tate.Width = 1.181102F;
            // 
            // txtNakagaininCdTo
            // 
            this.txtNakagaininCdTo.DataField = "ITEM01";
            this.txtNakagaininCdTo.Height = 0.1968504F;
            this.txtNakagaininCdTo.Left = 0.1192913F;
            this.txtNakagaininCdTo.MultiLine = false;
            this.txtNakagaininCdTo.Name = "txtNakagaininCdTo";
            this.txtNakagaininCdTo.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle; ddo-char-set: 128";
            this.txtNakagaininCdTo.Text = null;
            this.txtNakagaininCdTo.Top = 0.218504F;
            this.txtNakagaininCdTo.Width = 0.422441F;
            // 
            // txtTitleName
            // 
            this.txtTitleName.Height = 0.2874016F;
            this.txtTitleName.Left = 4.06063F;
            this.txtTitleName.MultiLine = false;
            this.txtTitleName.Name = "txtTitleName";
            this.txtTitleName.Style = "font-family: ＭＳ 明朝; font-size: 15.75pt; font-weight: bold; text-align: center; te" +
    "xt-decoration: none; vertical-align: middle";
            this.txtTitleName.Text = "仲買人マスタ一覧";
            this.txtTitleName.Top = 0.06535433F;
            this.txtTitleName.Width = 2.952756F;
            // 
            // txtNakagaininCdFr
            // 
            this.txtNakagaininCdFr.DataField = "ITEM02";
            this.txtNakagaininCdFr.Height = 0.1968504F;
            this.txtNakagaininCdFr.Left = 2.130315F;
            this.txtNakagaininCdFr.MultiLine = false;
            this.txtNakagaininCdFr.Name = "txtNakagaininCdFr";
            this.txtNakagaininCdFr.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle; ddo-char-set: 128";
            this.txtNakagaininCdFr.Text = null;
            this.txtNakagaininCdFr.Top = 0.218504F;
            this.txtNakagaininCdFr.Width = 0.4062995F;
            // 
            // lblBet01
            // 
            this.lblBet01.Height = 0.1968504F;
            this.lblBet01.HyperLink = null;
            this.lblBet01.Left = 1.859055F;
            this.lblBet01.Name = "lblBet01";
            this.lblBet01.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: center; vertical-align: middle" +
    "; ddo-char-set: 128";
            this.lblBet01.Text = "～";
            this.lblBet01.Top = 0.218504F;
            this.lblBet01.Width = 0.2110236F;
            // 
            // txtTitle02
            // 
            this.txtTitle02.Height = 0.1968504F;
            this.txtTitle02.Left = 0.7228347F;
            this.txtTitle02.MultiLine = false;
            this.txtTitle02.Name = "txtTitle02";
            this.txtTitle02.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle; ddo-char-set: 1";
            this.txtTitle02.Text = "正式仲買人名";
            this.txtTitle02.Top = 0.7736221F;
            this.txtTitle02.Width = 1.378347F;
            // 
            // txtTitle01
            // 
            this.txtTitle01.Height = 0.1968504F;
            this.txtTitle01.Left = 0.03031496F;
            this.txtTitle01.MultiLine = false;
            this.txtTitle01.Name = "txtTitle01";
            this.txtTitle01.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; v" +
    "ertical-align: middle; ddo-char-set: 1";
            this.txtTitle01.Text = "仲買人CD";
            this.txtTitle01.Top = 0.7736221F;
            this.txtTitle01.Width = 0.6035433F;
            // 
            // txtTitle03
            // 
            this.txtTitle03.Height = 0.1968504F;
            this.txtTitle03.Left = 2.161417F;
            this.txtTitle03.MultiLine = false;
            this.txtTitle03.Name = "txtTitle03";
            this.txtTitle03.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle; ddo-char-set: 1";
            this.txtTitle03.Text = "仲買人ｶﾅ名";
            this.txtTitle03.Top = 0.7736221F;
            this.txtTitle03.Width = 1.280315F;
            // 
            // txtTitle04
            // 
            this.txtTitle04.Height = 0.1968504F;
            this.txtTitle04.Left = 3.472835F;
            this.txtTitle04.MultiLine = false;
            this.txtTitle04.Name = "txtTitle04";
            this.txtTitle04.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle; ddo-char-set: 1";
            this.txtTitle04.Text = "仲買人略称名";
            this.txtTitle04.Top = 0.7736221F;
            this.txtTitle04.Width = 0.9283464F;
            // 
            // txtTitle05
            // 
            this.txtTitle05.Height = 0.1968504F;
            this.txtTitle05.Left = 4.401181F;
            this.txtTitle05.MultiLine = false;
            this.txtTitle05.Name = "txtTitle05";
            this.txtTitle05.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; v" +
    "ertical-align: middle; ddo-char-set: 1";
            this.txtTitle05.Text = "郵便番号";
            this.txtTitle05.Top = 0.7736221F;
            this.txtTitle05.Width = 0.7704728F;
            // 
            // txtTitle06
            // 
            this.txtTitle06.Height = 0.1968504F;
            this.txtTitle06.Left = 5.223622F;
            this.txtTitle06.MultiLine = false;
            this.txtTitle06.Name = "txtTitle06";
            this.txtTitle06.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; v" +
    "ertical-align: middle; ddo-char-set: 1";
            this.txtTitle06.Text = "住所";
            this.txtTitle06.Top = 0.7736221F;
            this.txtTitle06.Width = 2.940552F;
            // 
            // txtTitle07
            // 
            this.txtTitle07.Height = 0.1968504F;
            this.txtTitle07.Left = 8.164174F;
            this.txtTitle07.MultiLine = false;
            this.txtTitle07.Name = "txtTitle07";
            this.txtTitle07.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; v" +
    "ertical-align: middle; ddo-char-set: 1";
            this.txtTitle07.Text = "電話番号";
            this.txtTitle07.Top = 0.7736221F;
            this.txtTitle07.Width = 1.008268F;
            // 
            // txtTitle08
            // 
            this.txtTitle08.Height = 0.1968504F;
            this.txtTitle08.Left = 9.172441F;
            this.txtTitle08.MultiLine = false;
            this.txtTitle08.Name = "txtTitle08";
            this.txtTitle08.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; v" +
    "ertical-align: middle; ddo-char-set: 1";
            this.txtTitle08.Text = "FAX番号";
            this.txtTitle08.Top = 0.7736221F;
            this.txtTitle08.Width = 0.9771652F;
            // 
            // txtTitle09
            // 
            this.txtTitle09.Height = 0.1968504F;
            this.txtTitle09.Left = 10.25394F;
            this.txtTitle09.MultiLine = false;
            this.txtTitle09.Name = "txtTitle09";
            this.txtTitle09.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; v" +
    "ertical-align: middle; ddo-char-set: 1";
            this.txtTitle09.Text = "金額計算区分";
            this.txtTitle09.Top = 0.7736221F;
            this.txtTitle09.Width = 0.9665356F;
            // 
            // line1
            // 
            this.line1.Height = 0F;
            this.line1.Left = 0F;
            this.line1.LineWeight = 2F;
            this.line1.Name = "line1";
            this.line1.Top = 0.9704725F;
            this.line1.Width = 11.29528F;
            this.line1.X1 = 0F;
            this.line1.X2 = 11.29528F;
            this.line1.Y1 = 0.9704725F;
            this.line1.Y2 = 0.9704725F;
            // 
            // txtNakagaininNmTo
            // 
            this.txtNakagaininNmTo.DataField = "ITEM03";
            this.txtNakagaininNmTo.Height = 0.1968504F;
            this.txtNakagaininNmTo.Left = 0.6338583F;
            this.txtNakagaininNmTo.MultiLine = false;
            this.txtNakagaininNmTo.Name = "txtNakagaininNmTo";
            this.txtNakagaininNmTo.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; vertical-align: middl" +
    "e; ddo-char-set: 128";
            this.txtNakagaininNmTo.Text = null;
            this.txtNakagaininNmTo.Top = 0.218504F;
            this.txtNakagaininNmTo.Width = 1.167717F;
            // 
            // txtNakagaininNmFr
            // 
            this.txtNakagaininNmFr.DataField = "ITEM04";
            this.txtNakagaininNmFr.Height = 0.1968504F;
            this.txtNakagaininNmFr.Left = 2.62874F;
            this.txtNakagaininNmFr.MultiLine = false;
            this.txtNakagaininNmFr.Name = "txtNakagaininNmFr";
            this.txtNakagaininNmFr.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; vertical-align: middl" +
    "e; ddo-char-set: 128";
            this.txtNakagaininNmFr.Text = null;
            this.txtNakagaininNmFr.Top = 0.218504F;
            this.txtNakagaininNmFr.Width = 1.237008F;
            // 
            // lblBet02
            // 
            this.lblBet02.Height = 0.1968504F;
            this.lblBet02.HyperLink = null;
            this.lblBet02.Left = 0.5417323F;
            this.lblBet02.Name = "lblBet02";
            this.lblBet02.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: center; vertical-align: middle" +
    "; ddo-char-set: 128";
            this.lblBet02.Text = ":";
            this.lblBet02.Top = 0.218504F;
            this.lblBet02.Width = 0.09212598F;
            // 
            // lblBet03
            // 
            this.lblBet03.Height = 0.1968504F;
            this.lblBet03.HyperLink = null;
            this.lblBet03.Left = 2.536614F;
            this.lblBet03.Name = "lblBet03";
            this.lblBet03.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: center; vertical-align: middle" +
    "; ddo-char-set: 128";
            this.lblBet03.Text = ":";
            this.lblBet03.Top = 0.218504F;
            this.lblBet03.Width = 0.09212604F;
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtValue01,
            this.txtValue02,
            this.txtValue03,
            this.txtValue05,
            this.txtValue06,
            this.txtValue07,
            this.txtValue08,
            this.label1,
            this.txtValue04,
            this.textBox09,
            this.txtValue10});
            this.detail.Height = 0.2734579F;
            this.detail.Name = "detail";
            // 
            // txtValue01
            // 
            this.txtValue01.DataField = "ITEM05";
            this.txtValue01.Height = 0.1574803F;
            this.txtValue01.Left = 0.08937009F;
            this.txtValue01.MultiLine = false;
            this.txtValue01.Name = "txtValue01";
            this.txtValue01.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle";
            this.txtValue01.Text = null;
            this.txtValue01.Top = 0.05196851F;
            this.txtValue01.Width = 0.4846457F;
            // 
            // txtValue02
            // 
            this.txtValue02.DataField = "ITEM06";
            this.txtValue02.Height = 0.1574803F;
            this.txtValue02.Left = 0.7228347F;
            this.txtValue02.MultiLine = false;
            this.txtValue02.Name = "txtValue02";
            this.txtValue02.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
            this.txtValue02.Text = null;
            this.txtValue02.Top = 0.05196851F;
            this.txtValue02.Width = 1.378346F;
            // 
            // txtValue03
            // 
            this.txtValue03.DataField = "ITEM07";
            this.txtValue03.Height = 0.1574803F;
            this.txtValue03.Left = 2.161417F;
            this.txtValue03.MultiLine = false;
            this.txtValue03.Name = "txtValue03";
            this.txtValue03.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
            this.txtValue03.Text = null;
            this.txtValue03.Top = 0.05196851F;
            this.txtValue03.Width = 1.280315F;
            // 
            // txtValue05
            // 
            this.txtValue05.DataField = "ITEM09";
            this.txtValue05.Height = 0.1574803F;
            this.txtValue05.Left = 4.401181F;
            this.txtValue05.MultiLine = false;
            this.txtValue05.Name = "txtValue05";
            this.txtValue05.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; vertical-align: middl" +
    "e; ddo-char-set: 128";
            this.txtValue05.Text = null;
            this.txtValue05.Top = 0.05196851F;
            this.txtValue05.Width = 0.2948819F;
            // 
            // txtValue06
            // 
            this.txtValue06.DataField = "ITEM10";
            this.txtValue06.Height = 0.1574803F;
            this.txtValue06.Left = 4.790552F;
            this.txtValue06.MultiLine = false;
            this.txtValue06.Name = "txtValue06";
            this.txtValue06.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; vertical-align: middl" +
    "e; ddo-char-set: 128";
            this.txtValue06.Text = null;
            this.txtValue06.Top = 0.05196851F;
            this.txtValue06.Width = 0.3811024F;
            // 
            // txtValue07
            // 
            this.txtValue07.DataField = "ITEM11";
            this.txtValue07.Height = 0.1574803F;
            this.txtValue07.Left = 5.171654F;
            this.txtValue07.MultiLine = false;
            this.txtValue07.Name = "txtValue07";
            this.txtValue07.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle; ddo-char-set: 128";
            this.txtValue07.Text = null;
            this.txtValue07.Top = 0.05196851F;
            this.txtValue07.Width = 2.99252F;
            // 
            // txtValue08
            // 
            this.txtValue08.DataField = "ITEM12";
            this.txtValue08.Height = 0.1574803F;
            this.txtValue08.Left = 8.164174F;
            this.txtValue08.MultiLine = false;
            this.txtValue08.Name = "txtValue08";
            this.txtValue08.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; vertical-align: middl" +
    "e; ddo-char-set: 128";
            this.txtValue08.Text = null;
            this.txtValue08.Top = 0.05196851F;
            this.txtValue08.Width = 1.008268F;
            // 
            // label1
            // 
            this.label1.Height = 0.1574803F;
            this.label1.HyperLink = null;
            this.label1.Left = 4.642126F;
            this.label1.Name = "label1";
            this.label1.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: center; vertical-align: middle" +
    "";
            this.label1.Text = "-";
            this.label1.Top = 0.05196851F;
            this.label1.Width = 0.1484252F;
            // 
            // txtValue04
            // 
            this.txtValue04.DataField = "ITEM08";
            this.txtValue04.Height = 0.1574803F;
            this.txtValue04.Left = 3.472835F;
            this.txtValue04.MultiLine = false;
            this.txtValue04.Name = "txtValue04";
            this.txtValue04.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: middle";
            this.txtValue04.Text = null;
            this.txtValue04.Top = 0.05196851F;
            this.txtValue04.Width = 0.8334644F;
            // 
            // textBox09
            // 
            this.textBox09.DataField = "ITEM13";
            this.textBox09.Height = 0.1574803F;
            this.textBox09.Left = 9.172441F;
            this.textBox09.MultiLine = false;
            this.textBox09.Name = "textBox09";
            this.textBox09.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; vertical-align: middl" +
    "e; ddo-char-set: 128";
            this.textBox09.Text = null;
            this.textBox09.Top = 0.05196851F;
            this.textBox09.Width = 0.9771654F;
            // 
            // txtValue10
            // 
            this.txtValue10.DataField = "ITEM14";
            this.txtValue10.Height = 0.1574803F;
            this.txtValue10.Left = 10.25394F;
            this.txtValue10.MultiLine = false;
            this.txtValue10.Name = "txtValue10";
            this.txtValue10.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; v" +
    "ertical-align: middle; ddo-char-set: 128";
            this.txtValue10.Text = null;
            this.txtValue10.Top = 0.05196851F;
            this.txtValue10.Width = 0.9559055F;
            // 
            // pageFooter
            // 
            this.pageFooter.Name = "pageFooter";
            // 
            // HNCR1021R
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0F;
            this.PageSettings.Margins.Left = 0.1968504F;
            this.PageSettings.Margins.Right = 0.1968504F;
            this.PageSettings.Margins.Top = 0.1968504F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
            this.PageSettings.PaperHeight = 11.69291F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.267716F;
            this.PrintWidth = 11.29528F;
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.pageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.txtToday_tate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNakagaininCdTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNakagaininCdFr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBet01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle04)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle05)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle06)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle07)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle08)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle09)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNakagaininNmTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNakagaininNmFr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBet02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBet03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue05)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue06)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue07)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue08)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue04)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox09)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtToday_tate;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNakagaininCdTo;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitleName;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNakagaininCdFr;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblBet01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNakagaininNmTo;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle02;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle03;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle04;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle05;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle06;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle07;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle08;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle09;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNakagaininNmFr;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue02;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue03;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue05;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue06;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue07;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue08;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue04;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox09;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue10;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblBet02;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblBet03;
    }
}
