﻿namespace jp.co.fsi.hn.hndr1061
{
    partial class HNDR1061
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblDateFr = new System.Windows.Forms.Label();
            this.lblDateGengo = new System.Windows.Forms.Label();
            this.txtDateMonth = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtDateYear = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtDateDay = new jp.co.fsi.common.controls.FsiTextBox();
            this.labelDateYear = new System.Windows.Forms.Label();
            this.lblDateMonth = new System.Windows.Forms.Label();
            this.lblDateDay = new System.Windows.Forms.Label();
            this.gbxDate = new System.Windows.Forms.GroupBox();
            this.gbxFunanushiCd = new System.Windows.Forms.GroupBox();
            this.lblFunanushiCdTo = new System.Windows.Forms.Label();
            this.lblFunanushiCdFr = new System.Windows.Forms.Label();
            this.lblFunanushiCdBet = new System.Windows.Forms.Label();
            this.txtFunanushiCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtFunanushiCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.gbxMizuageShisho = new System.Windows.Forms.GroupBox();
            this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMizuageShishoNm = new System.Windows.Forms.Label();
            this.lblMizuageShisho = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.gbxDate.SuspendLayout();
            this.gbxFunanushiCd.SuspendLayout();
            this.gbxMizuageShisho.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Text = "";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Size = new System.Drawing.Size(847, 100);
            // 
            // lblDateFr
            // 
            this.lblDateFr.BackColor = System.Drawing.Color.Silver;
            this.lblDateFr.Location = new System.Drawing.Point(30, 25);
            this.lblDateFr.Name = "lblDateFr";
            this.lblDateFr.Size = new System.Drawing.Size(197, 29);
            this.lblDateFr.TabIndex = 1;
            // 
            // lblDateGengo
            // 
            this.lblDateGengo.BackColor = System.Drawing.Color.Silver;
            this.lblDateGengo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDateGengo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateGengo.Location = new System.Drawing.Point(33, 29);
            this.lblDateGengo.Name = "lblDateGengo";
            this.lblDateGengo.Size = new System.Drawing.Size(41, 22);
            this.lblDateGengo.TabIndex = 1;
            this.lblDateGengo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDateMonth
            // 
            this.txtDateMonth.AutoSizeFromLength = false;
            this.txtDateMonth.DisplayLength = null;
            this.txtDateMonth.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateMonth.Location = new System.Drawing.Point(125, 29);
            this.txtDateMonth.MaxLength = 2;
            this.txtDateMonth.Name = "txtDateMonth";
            this.txtDateMonth.Size = new System.Drawing.Size(30, 20);
            this.txtDateMonth.TabIndex = 3;
            this.txtDateMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateMonth.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateMonth_Validating);
            // 
            // txtDateYear
            // 
            this.txtDateYear.AutoSizeFromLength = false;
            this.txtDateYear.DisplayLength = null;
            this.txtDateYear.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateYear.Location = new System.Drawing.Point(75, 29);
            this.txtDateYear.MaxLength = 2;
            this.txtDateYear.Name = "txtDateYear";
            this.txtDateYear.Size = new System.Drawing.Size(30, 20);
            this.txtDateYear.TabIndex = 2;
            this.txtDateYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateYear.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateYear_Validating);
            // 
            // txtDateDay
            // 
            this.txtDateDay.AutoSizeFromLength = false;
            this.txtDateDay.DisplayLength = null;
            this.txtDateDay.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateDay.Location = new System.Drawing.Point(175, 29);
            this.txtDateDay.MaxLength = 2;
            this.txtDateDay.Name = "txtDateDay";
            this.txtDateDay.Size = new System.Drawing.Size(30, 20);
            this.txtDateDay.TabIndex = 4;
            this.txtDateDay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateDay.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateDay_Validating);
            // 
            // labelDateYear
            // 
            this.labelDateYear.BackColor = System.Drawing.Color.Silver;
            this.labelDateYear.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.labelDateYear.Location = new System.Drawing.Point(107, 29);
            this.labelDateYear.Name = "labelDateYear";
            this.labelDateYear.Size = new System.Drawing.Size(17, 21);
            this.labelDateYear.TabIndex = 3;
            this.labelDateYear.Text = "年";
            this.labelDateYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateMonth
            // 
            this.lblDateMonth.BackColor = System.Drawing.Color.Silver;
            this.lblDateMonth.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateMonth.Location = new System.Drawing.Point(157, 30);
            this.lblDateMonth.Name = "lblDateMonth";
            this.lblDateMonth.Size = new System.Drawing.Size(15, 19);
            this.lblDateMonth.TabIndex = 5;
            this.lblDateMonth.Text = "月";
            this.lblDateMonth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateDay
            // 
            this.lblDateDay.BackColor = System.Drawing.Color.Silver;
            this.lblDateDay.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateDay.Location = new System.Drawing.Point(207, 30);
            this.lblDateDay.Name = "lblDateDay";
            this.lblDateDay.Size = new System.Drawing.Size(20, 18);
            this.lblDateDay.TabIndex = 7;
            this.lblDateDay.Text = "日";
            this.lblDateDay.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // gbxDate
            // 
            this.gbxDate.Controls.Add(this.lblDateDay);
            this.gbxDate.Controls.Add(this.lblDateMonth);
            this.gbxDate.Controls.Add(this.labelDateYear);
            this.gbxDate.Controls.Add(this.txtDateDay);
            this.gbxDate.Controls.Add(this.txtDateYear);
            this.gbxDate.Controls.Add(this.txtDateMonth);
            this.gbxDate.Controls.Add(this.lblDateGengo);
            this.gbxDate.Controls.Add(this.lblDateFr);
            this.gbxDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxDate.ForeColor = System.Drawing.SystemColors.ControlText;
            this.gbxDate.Location = new System.Drawing.Point(18, 135);
            this.gbxDate.Name = "gbxDate";
            this.gbxDate.Size = new System.Drawing.Size(264, 75);
            this.gbxDate.TabIndex = 1;
            this.gbxDate.TabStop = false;
            this.gbxDate.Text = "日付範囲";
            // 
            // gbxFunanushiCd
            // 
            this.gbxFunanushiCd.Controls.Add(this.lblFunanushiCdTo);
            this.gbxFunanushiCd.Controls.Add(this.lblFunanushiCdFr);
            this.gbxFunanushiCd.Controls.Add(this.lblFunanushiCdBet);
            this.gbxFunanushiCd.Controls.Add(this.txtFunanushiCdTo);
            this.gbxFunanushiCd.Controls.Add(this.txtFunanushiCdFr);
            this.gbxFunanushiCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.gbxFunanushiCd.ForeColor = System.Drawing.SystemColors.ControlText;
            this.gbxFunanushiCd.Location = new System.Drawing.Point(17, 222);
            this.gbxFunanushiCd.Name = "gbxFunanushiCd";
            this.gbxFunanushiCd.Size = new System.Drawing.Size(561, 80);
            this.gbxFunanushiCd.TabIndex = 2;
            this.gbxFunanushiCd.TabStop = false;
            this.gbxFunanushiCd.Text = "船主CD範囲";
            // 
            // lblFunanushiCdTo
            // 
            this.lblFunanushiCdTo.BackColor = System.Drawing.Color.Silver;
            this.lblFunanushiCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFunanushiCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFunanushiCdTo.Location = new System.Drawing.Point(342, 34);
            this.lblFunanushiCdTo.Name = "lblFunanushiCdTo";
            this.lblFunanushiCdTo.Size = new System.Drawing.Size(186, 20);
            this.lblFunanushiCdTo.TabIndex = 13;
            this.lblFunanushiCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblFunanushiCdFr
            // 
            this.lblFunanushiCdFr.BackColor = System.Drawing.Color.Silver;
            this.lblFunanushiCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFunanushiCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFunanushiCdFr.Location = new System.Drawing.Point(77, 33);
            this.lblFunanushiCdFr.Name = "lblFunanushiCdFr";
            this.lblFunanushiCdFr.Size = new System.Drawing.Size(186, 20);
            this.lblFunanushiCdFr.TabIndex = 10;
            this.lblFunanushiCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblFunanushiCdBet
            // 
            this.lblFunanushiCdBet.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFunanushiCdBet.Location = new System.Drawing.Point(269, 32);
            this.lblFunanushiCdBet.Name = "lblFunanushiCdBet";
            this.lblFunanushiCdBet.Size = new System.Drawing.Size(17, 22);
            this.lblFunanushiCdBet.TabIndex = 11;
            this.lblFunanushiCdBet.Text = "～";
            this.lblFunanushiCdBet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFunanushiCdTo
            // 
            this.txtFunanushiCdTo.AutoSizeFromLength = false;
            this.txtFunanushiCdTo.DisplayLength = null;
            this.txtFunanushiCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFunanushiCdTo.Location = new System.Drawing.Point(292, 34);
            this.txtFunanushiCdTo.MaxLength = 4;
            this.txtFunanushiCdTo.Name = "txtFunanushiCdTo";
            this.txtFunanushiCdTo.Size = new System.Drawing.Size(49, 20);
            this.txtFunanushiCdTo.TabIndex = 6;
            this.txtFunanushiCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFunanushiCdTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtFunanushiCdTo_KeyDown);
            this.txtFunanushiCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtFunanushiCdTo_Validating);
            // 
            // txtFunanushiCdFr
            // 
            this.txtFunanushiCdFr.AutoSizeFromLength = false;
            this.txtFunanushiCdFr.DisplayLength = null;
            this.txtFunanushiCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFunanushiCdFr.Location = new System.Drawing.Point(27, 33);
            this.txtFunanushiCdFr.MaxLength = 4;
            this.txtFunanushiCdFr.Name = "txtFunanushiCdFr";
            this.txtFunanushiCdFr.Size = new System.Drawing.Size(49, 20);
            this.txtFunanushiCdFr.TabIndex = 5;
            this.txtFunanushiCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFunanushiCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtFunanushiCdFr_Validating);
            // 
            // gbxMizuageShisho
            // 
            this.gbxMizuageShisho.Controls.Add(this.txtMizuageShishoCd);
            this.gbxMizuageShisho.Controls.Add(this.lblMizuageShishoNm);
            this.gbxMizuageShisho.Controls.Add(this.lblMizuageShisho);
            this.gbxMizuageShisho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.gbxMizuageShisho.ForeColor = System.Drawing.Color.Black;
            this.gbxMizuageShisho.Location = new System.Drawing.Point(18, 46);
            this.gbxMizuageShisho.Name = "gbxMizuageShisho";
            this.gbxMizuageShisho.Size = new System.Drawing.Size(381, 77);
            this.gbxMizuageShisho.TabIndex = 0;
            this.gbxMizuageShisho.TabStop = false;
            this.gbxMizuageShisho.Text = "水揚支所";
            // 
            // txtMizuageShishoCd
            // 
            this.txtMizuageShishoCd.AutoSizeFromLength = true;
            this.txtMizuageShishoCd.DisplayLength = null;
            this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtMizuageShishoCd.Location = new System.Drawing.Point(96, 33);
            this.txtMizuageShishoCd.MaxLength = 4;
            this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
            this.txtMizuageShishoCd.Size = new System.Drawing.Size(34, 20);
            this.txtMizuageShishoCd.TabIndex = 1;
            this.txtMizuageShishoCd.TabStop = false;
            this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
            // 
            // lblMizuageShishoNm
            // 
            this.lblMizuageShishoNm.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMizuageShishoNm.Location = new System.Drawing.Point(131, 33);
            this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
            this.lblMizuageShishoNm.Size = new System.Drawing.Size(212, 20);
            this.lblMizuageShishoNm.TabIndex = 2;
            this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMizuageShisho
            // 
            this.lblMizuageShisho.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShisho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShisho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblMizuageShisho.Location = new System.Drawing.Point(31, 31);
            this.lblMizuageShisho.Name = "lblMizuageShisho";
            this.lblMizuageShisho.Size = new System.Drawing.Size(317, 25);
            this.lblMizuageShisho.TabIndex = 0;
            this.lblMizuageShisho.Text = "水揚支所";
            this.lblMizuageShisho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // HNDR1061
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 638);
            this.Controls.Add(this.gbxMizuageShisho);
            this.Controls.Add(this.gbxDate);
            this.Controls.Add(this.gbxFunanushiCd);
            this.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "HNDR1061";
            this.Par1 = "12";
            this.Text = "";
            this.Controls.SetChildIndex(this.gbxFunanushiCd, 0);
            this.Controls.SetChildIndex(this.gbxDate, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.gbxMizuageShisho, 0);
            this.pnlDebug.ResumeLayout(false);
            this.gbxDate.ResumeLayout(false);
            this.gbxDate.PerformLayout();
            this.gbxFunanushiCd.ResumeLayout(false);
            this.gbxFunanushiCd.PerformLayout();
            this.gbxMizuageShisho.ResumeLayout(false);
            this.gbxMizuageShisho.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblDateFr;
        private System.Windows.Forms.Label lblDateGengo;
        private jp.co.fsi.common.controls.FsiTextBox txtDateMonth;
        private jp.co.fsi.common.controls.FsiTextBox txtDateYear;
        private jp.co.fsi.common.controls.FsiTextBox txtDateDay;
        private System.Windows.Forms.Label labelDateYear;
        private System.Windows.Forms.Label lblDateMonth;
        private System.Windows.Forms.Label lblDateDay;
        private System.Windows.Forms.GroupBox gbxDate;
        private System.Windows.Forms.GroupBox gbxFunanushiCd;
        private System.Windows.Forms.Label lblFunanushiCdFr;
        private common.controls.FsiTextBox txtFunanushiCdFr;
        private System.Windows.Forms.Label lblFunanushiCdTo;
        private System.Windows.Forms.Label lblFunanushiCdBet;
        private common.controls.FsiTextBox txtFunanushiCdTo;
        private System.Windows.Forms.GroupBox gbxMizuageShisho;
        private common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private System.Windows.Forms.Label lblMizuageShisho;
    }
}