﻿using System;
using System.ComponentModel;
using System.Data;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

using GrapeCity.ActiveReports;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;
using jp.co.fsi.common.constants;

namespace jp.co.fsi.hn.hndr1061
{
    /// <summary>
    /// 出金チェックリスト(HNDR1061)
    /// </summary>
    public partial class HNDR1061 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// 印刷用使用列数
        /// </summary>
        public const int prtCols = 13;
        #endregion

        #region プロパティ
        /// <summary>
        /// 画面上最後となるフォーカスのEnterボタン押下時処理用変数
        /// </summary>
        private bool _dtFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._dtFlg;
            }
        }

        private decimal MOTOCHO_KUBUN = 12;
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HNDR1061()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 水揚支所
#if DEBUG
            this.txtMizuageShishoCd.Text = "1";
#else
            this.txtMizuageShishoCd.Text = Uinfo.shishoCd;
#endif
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
            this.txtMizuageShishoCd.Enabled = (this.txtMizuageShishoCd.Text == "1") ? true : false;

            string[] jpDate = Util.ConvJpDate(DateTime.Now, this.Dba);
            lblDateGengo.Text = jpDate[0];
            txtDateYear.Text = jpDate[2];
            txtDateMonth.Text = jpDate[3];
            txtDateDay.Text = jpDate[4];

            lblFunanushiCdFr.Text = "先　頭";
            lblFunanushiCdTo.Text = "最　後";

            MOTOCHO_KUBUN = (Util.ToDecimal(this.Par1) == 0) ? MOTOCHO_KUBUN : Util.ToDecimal(this.Par1);

            txtDateYear.Focus();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 日付,船主コードにフォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd":
                case "txtDateYear":
                case "txtFunanushiCdFr":
                case "txtFunanushiCdTo":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;
            String[] result;
            string stCurrentDir = System.IO.Directory.GetCurrentDirectory();

            switch (this.ActiveCtlNm)
            {
                #region 水揚支所
                case "txtMizuageShishoCd": // 水揚支所
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM2031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2031.CMCM2031");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.InData = this.txtMizuageShishoCd.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.txtMizuageShishoCd.Text = result[0];
                                this.lblMizuageShishoNm.Text = result[1];
                            }
                        }
                    }
                    break;
                #endregion
                case "txtDateYear":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblDateGengo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (string[])frm.OutData;
                                this.lblDateGengo.Text = result[1];

                                // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
                                DateTime tmpDate = Util.ConvAdDate(this.lblDateGengo.Text, this.txtDateYear.Text,
                                    this.txtDateMonth.Text, "1", this.Dba);
                                int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);
                                if (Util.ToInt(this.txtDateDay.Text) > lastDayInMonth)
                                {
                                    this.txtDateDay.Text = Util.ToString(lastDayInMonth);
                                }

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblDateGengo.Text,
                                        this.txtDateYear.Text,
                                        this.txtDateMonth.Text,
                                        this.txtDateDay.Text,
                                        this.Dba);
                                this.lblDateGengo.Text = arrJpDate[0];
                                this.txtDateYear.Text = arrJpDate[2];
                                this.txtDateMonth.Text = arrJpDate[3];
                                this.txtDateDay.Text = arrJpDate[4];
                            }
                        }
                    }
                    break;

                case "txtFunanushiCdFr":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM2011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2011.CMCM2011");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtFunanushiCdFr.Text = outData[0];
                                this.lblFunanushiCdFr.Text = outData[1];
                            }
                        }
                    }
                    break;

                case "txtFunanushiCdTo":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM2011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2011.CMCM2011");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtFunanushiCdTo.Text = outData[0];
                                this.lblFunanushiCdTo.Text = outData[1];
                            }
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
            {
                // ﾌﾟﾚﾋﾞｭｰ処理
                DoPrint(true);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("印刷", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }

        /// <summary>
        /// F6キー押下時処理
        /// PDF出力
        /// </summary>
        public override void PressF6()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("PDF出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, true);
            }
        }

        /// <summary>
        /// F7キー押下時処理
        /// EXCEL出力
        /// </summary>
        public override void PressF7()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("EXCEL出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, false, true);
            }
        }

        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            // 設定画面の起動
            // MEMO:原則としてここで渡す帳票IDの設定はReport.csvに保持していることが前提ですが、
            // 保持していない場合は、設定画面での保存(F6)時に新規に設定が保持されます。
            PrintSettingForm psForm = new PrintSettingForm(new string[1] { "HNDR1061R" });
            psForm.ShowDialog();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 水揚支所入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMizuageShishoCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                e.Cancel = true;
                this.txtMizuageShishoCd.SelectAll();
                this.txtMizuageShishoCd.Focus();
            }
        }

        /// <summary>
        /// 船主CD(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFunanushiCdFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidFunanushiCdFr())
            {
                e.Cancel = true;
                this.txtFunanushiCdFr.SelectAll();
            }
        }

        /// <summary>
        /// 船主CD(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFunanushiCdTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidFunanushiCdTo())
            {
                e.Cancel = true;
                this.txtFunanushiCdTo.SelectAll();

                // Enter処理を無効化
                this._dtFlg = false;
            }
            else
            {
                // Enter処理を有効化
                this._dtFlg = true;
            }
        }

        /// <summary>
        /// 和暦(年)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateYear_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtDateYear.Text, this.txtDateYear.MaxLength))
            {
                e.Cancel = true;
                this.txtDateYear.SelectAll();
            }
            else
            {
                this.txtDateYear.Text = Util.ToString(IsValid.SetYear(this.txtDateYear.Text));
                CheckDate();
                SetDate();
            }
        }

        /// <summary>
        /// 和暦(月)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateMonth_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtDateMonth.Text, this.txtDateMonth.MaxLength))
            {
                e.Cancel = true;
                this.txtDateMonth.SelectAll();
            }
            else
            {
                this.txtDateMonth.Text = Util.ToString(IsValid.SetMonth(this.txtDateMonth.Text));
                CheckDate();
                SetDate();
            }
        }

        /// <summary>
        /// 和暦(日)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateDay_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsDay(this.txtDateDay.Text, this.txtDateDay.MaxLength))
            {
                e.Cancel = true;
                this.txtDateDay.SelectAll();
            }
            else
            {
                this.txtDateDay.Text = Util.ToString(IsValid.SetDay(this.txtDateDay.Text));
                CheckDate();
                SetDate();
            }
        }

        /// <summary>
        /// 船主コード(至)のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFunanushiCdTo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.Flg)
            {
                // Enter処理を無効化
                this._dtFlg = false;

                // 全項目を再度入力値チェック
                if (!ValidateAll())
                {
                    // エラーありの場合ここで処理終了
                    return;
                }

                if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
                {
                    // ﾌﾟﾚﾋﾞｭｰ処理
                    DoPrint(true);
                }
                else
                {
                    this.txtFunanushiCdTo.Focus();
                }
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 水揚支所の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool IsValidMizuageShishoCd()
        {
            // 空 又は 0入力の場合
            if (ValChk.IsEmpty(this.txtMizuageShishoCd.Text) || Equals(this.txtMizuageShishoCd.Text, "0"))
            {
                // 水揚支所名称を表示する
                this.txtMizuageShishoCd.Text = "0";
                this.lblMizuageShishoNm.Text = "全て";
                return true;
            }
            // 水揚支所名称を表示する
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
            if (ValChk.IsEmpty(this.lblMizuageShishoNm.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }
            return true;
        }

        /// <summary>
        /// 年月日(自)の月末入力チェック
        /// </summary>
        /// 
        private void CheckDate()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblDateGengo.Text, this.txtDateYear.Text,
                this.txtDateMonth.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDateDay.Text) > lastDayInMonth)
            {
                this.txtDateDay.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(自)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetDate()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDate(Util.FixJpDate(this.lblDateGengo.Text, this.txtDateYear.Text,
                this.txtDateMonth.Text, this.txtDateDay.Text, this.Dba));
        }

        /// <summary>
        /// 船主コード(自)の入力チェック
        /// </summary>
        private bool IsValidFunanushiCdFr()
        {
            if (ValChk.IsEmpty(this.txtFunanushiCdFr.Text))
            {
                this.lblFunanushiCdFr.Text = "先　頭";
            }
            else
                // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
                if (!ValChk.IsNumber(this.txtFunanushiCdFr.Text))
                {
                    Msg.Error("船主コードは数値のみで入力してください。");
                    return false;
                }
                else
                {
                    // コードを元に名称を取得する
                    this.lblFunanushiCdFr.Text = this.Dba.GetName(this.UInfo, "VI_HN_FUNANUSHI", this.txtMizuageShishoCd.Text, this.txtFunanushiCdFr.Text);
                }

            return true;
        }

        /// <summary>
        /// 船主コード(至)の入力チェック
        /// </summary>
        private bool IsValidFunanushiCdTo()
        {
            if (ValChk.IsEmpty(this.txtFunanushiCdTo.Text))
            {
                this.lblFunanushiCdTo.Text = "最　後";
            }
            else
                // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
                if (!ValChk.IsNumber(this.txtFunanushiCdTo.Text))
                {
                    Msg.Error("船主コードは数値のみで入力してください。");
                    return false;
                }
                else
                {
                    // コードを元に名称を取得する
                    this.lblFunanushiCdTo.Text = this.Dba.GetName(this.UInfo, "VI_HN_FUNANUSHI", this.txtMizuageShishoCd.Text, this.txtFunanushiCdTo.Text);
                }

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 水揚支所の入力チェック
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                this.txtMizuageShishoCd.Focus();
                this.txtMizuageShishoCd.SelectAll();
                return false;
            }
            // 年(自)のチェック
            if (!IsValid.IsYear(this.txtDateYear.Text, this.txtDateYear.MaxLength))
            {
                this.txtDateYear.Focus();
                this.txtDateYear.SelectAll();
                return false;
            }
            // 月(自)のチェック
            if (!IsValid.IsMonth(this.txtDateMonth.Text, this.txtDateMonth.MaxLength))
            {
                this.txtDateMonth.Focus();
                this.txtDateMonth.SelectAll();
                return false;
            }
            // 日(自)のチェック
            if (!IsValid.IsDay(this.txtDateDay.Text, this.txtDateDay.MaxLength))
            {
                this.txtDateDay.Focus();
                this.txtDateDay.SelectAll();
                return false;
            }
            // 年月日(自)の月末入力チェック処理
            CheckDate();
            // 年月日(自)の正しい和暦への変換処理
            SetDate();

            // 船主コード(自)の入力チェック
            if (!IsValidFunanushiCdFr())
            {
                this.txtFunanushiCdFr.Focus();
                this.txtFunanushiCdFr.SelectAll();
                return false;
            }
            // 船主コード(至)の入力チェック
            if (!IsValidFunanushiCdTo())
            {
                this.txtFunanushiCdTo.Focus();
                this.txtFunanushiCdTo.SelectAll();
                return false;
            }
            return true;
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpDate(string[] arrJpDate)
        {
            this.lblDateGengo.Text = arrJpDate[0];
            this.txtDateYear.Text = arrJpDate[2];
            this.txtDateMonth.Text = arrJpDate[3];
            this.txtDateDay.Text = arrJpDate[4];
        }

        /// <summary>
        /// 帳票を印刷する。
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview, bool isPdf = false, bool isExcel = false, bool isCsv = false)
        {
            try
            {
#if DEBUG
                Dba.DeleteWork("PR_HN_TBL", this.UnqId);
#endif

                this.Dba.BeginTransaction();

                // 帳票出力用にワークテーブルにデータを作成
                bool dataFlag = MakeWkData();

                // 帳票出力
                if (dataFlag)
                {
                    // 取得列の定義
                    StringBuilder cols = Util.ColsArray(prtCols, "");

                    // バインドパラメータの設定
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);

                    // データの取得
                    DataTable dtOutput = this.Dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_HN_TBL", "GUID = @GUID", "SORT ASC", dpc);

                    // 帳票オブジェクトをインスタンス化
                    HNDR1061R rpt = new HNDR1061R(dtOutput);
                    rpt.Document.Name = this.lblTitle.Text;
                    rpt.Document.Printer.DocumentName = this.lblTitle.Text;

                    if (isExcel)
                    {
                        GrapeCity.ActiveReports.Export.Excel.Section.XlsExport xlsExport1 = new GrapeCity.ActiveReports.Export.Excel.Section.XlsExport();
                        //SetExcelSetting(xlsExport1);
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Han, rpt.Document.Name, 2);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            xlsExport1.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("EXCEL出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPdf)
                    {
                        GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport p = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Han, rpt.Document.Name, 1);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            p.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("PDF出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPreview)
                    {
                        PreviewForm pFrm = new PreviewForm(rpt, this.UnqId);
                        // プレビュー画面表示
                        pFrm.WindowState = FormWindowState.Maximized;
                        pFrm.Show();
                    }
                    else
                    {
                        // 直接印刷
                        rpt.Run(false);
                        rpt.Document.Print(true, true, false);
                    }
                }
            }
            finally
            {
#if DEBUG
                this.Dba.Commit();
#else
                this.Dba.Rollback();
#endif
            }
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData()
        {
            DbParamCollection dpc = new DbParamCollection();
            // 日付を西暦にして取得
            DateTime tmpDate = Util.ConvAdDate(this.lblDateGengo.Text, this.txtDateYear.Text,
                    this.txtDateMonth.Text, this.txtDateDay.Text, this.Dba);

            int i; // ループ用カウント変数

            // 船主コード設定
            string funanushiCdFr;
            string funanushiCdTo;
            if (Util.ToDecimal(txtFunanushiCdFr.Text) > 0)
            {
                funanushiCdFr = txtFunanushiCdFr.Text;
            }
            else
            {
                funanushiCdFr = "0";
            }
            if (Util.ToDecimal(txtFunanushiCdTo.Text) > 0)
            {
                funanushiCdTo = txtFunanushiCdTo.Text;
            }
            else
            {
                funanushiCdTo = "9999";
            }
            //支所コードの退避
            string shishoCd = this.txtMizuageShishoCd.Text;

            StringBuilder Sql = new StringBuilder();
            // 検索日付に発生しているデータを取得
            Sql.Append(" SELECT ");
            Sql.Append("     SHISHO_CD,");
            Sql.Append("     SHISHO_NM,");
            Sql.Append("     DENPYO_BANGO,");
            Sql.Append("     DENPYO_DATE,");
            Sql.Append("     TORIHIKISAKI_CD,");
            Sql.Append("     TORIHIKISAKI_NM,");
            Sql.Append("     ZEIKOMI_KINGAKU,");
            Sql.Append("     TEKIYO,");
            Sql.Append("     AITE_KANJO_KAMOKU_NM");
            Sql.Append(" FROM");
            Sql.Append("     VI_HN_SENSHU_SHUKKIN_ZM ");
            Sql.Append(" WHERE");
            Sql.Append("     KAIKEI_NENDO = @KAIKEI_NENDO AND ");
            Sql.Append("     KAISHA_CD = @KAISHA_CD AND ");
            Sql.Append("     MOTOCHO_KUBUN = @MOTOCHO_KUBUN AND ");
            if (shishoCd != "0")
            {
                Sql.Append("     SHISHO_CD = @SHISHO_CD AND ");
            }
            Sql.Append("     DENPYO_DATE = @DENPYO_DATE AND ");
            Sql.Append("     TORIHIKISAKI_CD BETWEEN @FUNANUSHI_CD_FR AND @FUNANUSHI_CD_TO ");
            Sql.Append(" ORDER BY");
            Sql.Append("     KAISHA_CD,");
            Sql.Append("     SHISHO_CD,");
            Sql.Append("     DENPYO_BANGO");

            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            // 検索する日付をセット
            dpc.SetParam("@DENPYO_DATE", SqlDbType.VarChar, 10, tmpDate.Date.ToString("yyyy/MM/dd"));
            // 検索する船主をセット
            dpc.SetParam("@FUNANUSHI_CD_FR", SqlDbType.VarChar, 4, funanushiCdFr);
            dpc.SetParam("@FUNANUSHI_CD_TO", SqlDbType.VarChar, 4, funanushiCdTo);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            dpc.SetParam("@MOTOCHO_KUBUN", SqlDbType.Decimal, 4, MOTOCHO_KUBUN);

            DataTable dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            if (dtMainLoop.Rows.Count == 0)
            {
                Msg.Info("該当データがありません。");
                return false;
            }
            else
            {
                dpc = new DbParamCollection();
                i = 1;
                Decimal genkinGokei = 0; // 現金合計
                Decimal furikomiGokei = 0; // 振込合計
                string nyukinKubunNm; // 入金区分名称
                string aiteKanjoKamokuNm = "";

                #region 印刷ワークテーブルに登録
                foreach (DataRow dr in dtMainLoop.Rows)
                {
                    #region 相手勘定科目取得用SQL
                    Sql = new StringBuilder();
                    // 検索日付に発生しているデータを取得
                    Sql.Append(" SELECT");
                    Sql.Append("     B1.KANJO_KAMOKU_NM AS AITE_KANJO_KAMOKU_NM");
                    Sql.Append(" FROM");
                    Sql.Append("     TB_ZM_SHIWAKE_MEISAI AS A1");
                    Sql.Append(" LEFT OUTER JOIN");
                    Sql.Append("     TB_ZM_KANJO_KAMOKU AS B1");
                    Sql.Append("     ON A1.KAISHA_CD = B1.KAISHA_CD");
                    Sql.Append("     AND A1.KAIKEI_NENDO = B1.KAIKEI_NENDO");
                    Sql.Append("     AND A1.KANJO_KAMOKU_CD = B1.KANJO_KAMOKU_CD");
                    Sql.Append(" WHERE");
                    Sql.Append("     (A1.KAISHA_CD = @KAISHA_CD) AND");
                    Sql.Append("     (A1.SHISHO_CD = @SHISHO_CD) AND");
                    Sql.Append("     (A1.KAIKEI_NENDO = @KAIKEI_NENDO) AND");
                    Sql.Append("     (A1.DENPYO_BANGO = @DENPYO_BANGO) AND");
                    Sql.Append("     (A1.TAISHAKU_KUBUN <> 1) AND ");
                    Sql.Append("     (A1.ZEIKOMI_KINGAKU = @ZEIKOMI_KINGAKU) AND");
                    Sql.Append("     (A1.MEISAI_KUBUN = 0)");

                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                    dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
                    dpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 6, dr["DENPYO_BANGO"]);
                    dpc.SetParam("@ZEIKOMI_KINGAKU", SqlDbType.Decimal, 10, Util.FormatNum(dr["ZEIKOMI_KINGAKU"]));
                    dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);

                    DataTable dtAiteKanjoKamoku = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

                    // 相手勘定科目が0の場合、エラーにならないよう空白に。
                    /**/
                    if (dtAiteKanjoKamoku.Rows.Count != 0)
                    {
                        aiteKanjoKamokuNm = Util.ToString(dtAiteKanjoKamoku.Rows[0]["AITE_KANJO_KAMOKU_NM"]);
                    }
                    else if (dtAiteKanjoKamoku.Rows.Count == 0)
                    {
                        aiteKanjoKamokuNm = "";
                    }
                    #endregion

                    Sql = new StringBuilder();
                    dpc = new DbParamCollection();
                    Sql.Append("INSERT INTO PR_HN_TBL(");
                    Sql.Append("  GUID");
                    Sql.Append(" ,SORT");
                    Sql.Append(" ," + Util.ColsArray(prtCols, ""));
                    Sql.Append(") ");
                    Sql.Append("VALUES(");
                    Sql.Append("  @GUID");
                    Sql.Append(" ,@SORT");
                    Sql.Append(" ," + Util.ColsArray(prtCols, "@"));
                    Sql.Append(") ");

                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, i);
                    // ページヘッダーデータを設定
                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);
                    // データを設定 
                    if (aiteKanjoKamokuNm == "現金")//(Util.ToString(drAiteKanjoKamoku["AITE_KANJO_KAMOKU_NM"]) == "現金")
                    {
                        nyukinKubunNm = "現金";
                        genkinGokei = Util.ToDecimal(dr["ZEIKOMI_KINGAKU"]);
                        furikomiGokei = 0;
                    }
                    else
                    {
                        nyukinKubunNm = "振込";
                        genkinGokei = 0;
                        furikomiGokei = Util.ToDecimal(dr["ZEIKOMI_KINGAKU"]);
                    }

                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpDate.Date.ToString("yyyy/MM/dd"));
                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dr["DENPYO_BANGO"]);
                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dr["DENPYO_DATE"]);
                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, dr["TORIHIKISAKI_CD"]);
                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, dr["TORIHIKISAKI_NM"]);
                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, nyukinKubunNm);
                    dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, Util.FormatNum(dr["ZEIKOMI_KINGAKU"]));
                    dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, Util.FormatNum(genkinGokei));
                    dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, Util.FormatNum(furikomiGokei));
                    dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, dr["TEKIYO"]);
                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, Util.ToString(dr["SHISHO_CD"]));
                    dpc.SetParam("@ITEM13", SqlDbType.VarChar, 200, Util.ToString(dr["SHISHO_NM"]));

                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                    i++;
                }
                #endregion
            }

            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
                "SORT",
                "PR_HN_TBL",
                "GUID = @GUID",
                dpc);

            bool dataFlag;
            if (tmpdtPR_HN_TBL.Rows.Count > 0)
            {
                dataFlag = true;
            }
            else
            {
                dataFlag = false;
            }

            return dataFlag;
        }
        #endregion
    }
}
