﻿using System;
using System.Data;
//using System.Drawing;
//using System.Collections;
//using System.Collections.Generic;
//using System.ComponentModel;

using jp.co.fsi.common.report;
using jp.co.fsi.common.util;

namespace jp.co.fsi.hn.hnsr1011
{
    /// <summary>
    /// セリ日報(HNSR1011) の帳票
    /// </summary>
    public partial class HNSR1011R : BaseReport
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HNSR1011R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }

        decimal GokeiKingaku;
        decimal Tanka;

        private void gfSSenshu_Format(object sender, EventArgs e)
        {
            // 小計金額
            GokeiKingaku = Util.ToDecimal(this.txtSUriageKingaku.Value) + Util.ToDecimal(this.txtSShouhiZei.Value);
            this.txtSGhoukeiKingaku.Text = Util.ToString(Util.FormatNum(GokeiKingaku));

            // 単価
            Tanka = Util.ToDecimal(this.txtSUriageKingaku.Value) / Util.ToDecimal(this.txtSMizuageSuryou.Value);
            this.txtSTanka.Text = Util.ToString(Util.FormatNum(Tanka));
        }

        private void gfMSenshu_Format(object sender, EventArgs e)
        {
            // 合計金額
            GokeiKingaku = Util.ToDecimal(this.txtMUriageKingaku.Value) + Util.ToDecimal(this.txtMShouhiZei.Value);
            this.txtMGhoukeiKingaku.Text = Util.ToString(Util.FormatNum(GokeiKingaku));

            // 単価
            Tanka = Util.ToDecimal(this.txtMUriageKingaku.Value) / Util.ToDecimal(this.txtMMizuageSuryou.Value);
            this.txtMTanka.Text = Util.ToString(Util.FormatNum(Tanka));
        }
    }
}
