﻿using System;
using System.Data;

using jp.co.fsi.common.report;
using jp.co.fsi.common.util;

using System.Globalization;

namespace jp.co.fsi.hn.hnmr1121
{
    /// <summary>
    /// HNMR1121R の帳票
    /// </summary>
    public partial class HNMR1121R : BaseReport
    {

        public HNMR1121R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }

        /// <summary>
        /// ページフッターの設定(合計)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void reportFooter1_Format(object sender, EventArgs e)
        {
            // 合計金額をフォーマット
            //this.txtTotal01.Text = Util.FormatNum(this.txtTotal01.Text);
            //this.txtTotal02.Text = Util.FormatNum(this.txtTotal02.Text);
            //this.txtTotal03.Text = Util.FormatNum(this.txtTotal03.Text);
            //this.txtTotal04.Text = Util.FormatNum(this.txtTotal04.Text);
            //this.txtTotal05.Text = Util.FormatNum(this.txtTotal05.Text);
            //this.txtTotal06.Text = Util.FormatNum(this.txtTotal06.Text);
            //// 合計金額が0の場合、空表示とする
            //if (this.txtTotal01.Text == "0")
            //{
            //    this.txtTotal01.Text = "";
            //}
            //if (this.txtTotal02.Text == "0")
            //{
            //    this.txtTotal02.Text = "";
            //}
            //if (this.txtTotal03.Text == "0")
            //{
            //    this.txtTotal03.Text = "";
            //}
            //if (this.txtTotal04.Text == "0")
            //{
            //    this.txtTotal04.Text = "";
            //}
            //if (this.txtTotal05.Text == "0")
            //{
            //    this.txtTotal05.Text = "";
            //}
            //if (this.txtTotal06.Text == "0")
            //{
            //    this.txtTotal06.Text = "";
            //}

            this.txtTotal01.Text = Util.FormatNum(this.txtTotal01.Text,1);
            // 合計金額が0の場合、空表示とする
            if (this.txtTotal01.Text == "0.0")
            {
                this.txtTotal01.Text = "";
            }

        }
    }
}
