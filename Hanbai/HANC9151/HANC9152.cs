﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;
using System.Security.Cryptography;
using jp.co.fsi.common.mynumber;

namespace jp.co.fsi.han.hanc9151
{
    /// <summary>
    /// 仲買人アドレス登録(HANC9152)
    /// </summary>
    public partial class HANC9152 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// モード(新規)
        /// </summary>
        private const string MODE_NEW = "1";

        /// <summary>
        /// モード(編集)
        /// </summary>
        private const string MODE_EDIT = "2";
        #endregion

        #region プロパティ
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HANC9152()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="par1">引数1</param>
        /// <param name="par2">引数2</param>
        public HANC9152(string par1, string par2)
            : base(par1, par2)
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region メソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // タイトルは非表示
            this.lblTitle.Visible = false;

            //ボタン表示
            this.ShowFButton = true;

            // 引数：Par1／モード(1:新規、2:変更),InData(仲買人CD)
            if (MODE_NEW.Equals(this.Par1))
            {
                // 新規モードの初期表示
                InitDispOnNew();
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // 編集モードの初期表示
                InitDispOnEdit();
            }
            else
            {
                // 不正な起動として閉じる
                Msg.Error("不正な起動です。終了します。");
                this.Close();
            }

            // ボタンの配置を調整
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF1.Location = this.btnF2.Location;
            this.btnF6.Location = this.btnF4.Location;

            // ボタンを調整
            this.btnF1.Visible = true;
            this.btnF2.Visible = false;
            this.btnF3.Visible = true;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF6.Visible = true;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 仲買人コードにフォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtNakagaininCd":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;

            switch (this.ActiveCtlNm)
            {
                case "txtNakagaininCd":

                    // アセンブリのロード
                    asm = Assembly.LoadFrom("HANC9021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.han.hanc9021.HANC9021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtNakagaininCd.Text = outData[0];
                                this.lblNakagaininNm.Text = outData[1];
                            }
                        }
                    }
                    break;

                default:
                    break;
            }
        }

        /// <summary>
        /// F3キー押下時処理
        /// </summary>
        public override void PressF3()
        {
            // 新規モード時
            if (MODE_NEW.Equals(this.Par1))
            {
                return;
            }

            if (Msg.ConfYesNo("削除しますか？") == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 入力値をバインドパラメータとしてセットする
            ArrayList delParams = SetDelParams();

            try
            {
                this.Dba.BeginTransaction();
                this.Dba.Delete("TB_HN_SERI_NAKAGAININ_ADDRESS",
                    "ICHIBA_CD = @ICHIBA_CD AND NAKAGAININ_CD = @NAKAGAININ_CD",
                    (DbParamCollection)delParams[0]);

                // トランザクションをコミット
                this.Dba.Commit();
                Msg.Info("削除しました。");
            }
            catch
            {
                Msg.Info("削除に失敗しました。");
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF6();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF6()
        {
            // 確認メッセージを表示
            string msg = (MODE_NEW.Equals(this.Par1) ? "登録" : "更新") + "しますか？";
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            // 入力値をバインドパラメータとしてセットする
            ArrayList alParams = SetParams();

            try
            {
                this.Dba.BeginTransaction();

                if (MODE_NEW.Equals(this.Par1))
                {
                    // データ登録
                    this.Dba.Insert("TB_HN_SERI_NAKAGAININ_ADDRESS", (DbParamCollection)alParams[0]);
                }
                else if (MODE_EDIT.Equals(this.Par1))
                {
                    // データ更新
                    this.Dba.Update("TB_HN_SERI_NAKAGAININ_ADDRESS",
                        (DbParamCollection)alParams[1],
                        "ICHIBA_CD = @ICHIBA_CD AND NAKAGAININ_CD = @NAKAGAININ_CD",
                        (DbParamCollection)alParams[0]);
                }

                // トランザクションをコミット
                this.Dba.Commit();
                msg = (MODE_NEW.Equals(this.Par1) ? "登録" : "更新") + "しました。";
                Msg.Info(msg);
            }
            catch
            {
                msg = (MODE_NEW.Equals(this.Par1) ? "登録" : "更新") + "に失敗しました。";
                Msg.Info(msg);
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 仲買人コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtNakagaininCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidNakagaininCd())
            {
                e.Cancel = true;
                this.txtNakagaininCd.SelectAll();
            }
        }

        /// <summary>
        /// アドレス1の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMailAddress1_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidMailAddress1())
            {
                e.Cancel = true;
                this.txtMailAddress1.SelectAll();
            }
        }

        /// <summary>
        /// アドレス2の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMailAddress2_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidMailAddress2())
            {
                e.Cancel = true;
                this.txtMailAddress2.SelectAll();
            }
        }

        /// <summary>
        /// アドレス3の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMailAddress3_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidMailAddress3())
            {
                e.Cancel = true;
                this.txtMailAddress3.SelectAll();
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 新規モードの初期表示
        /// </summary>
        private void InitDispOnNew()
        {
            // 正式仲買人名に初期フォーカス
            this.ActiveControl = this.txtNakagaininCd;
            this.txtNakagaininCd.Focus();

            // 削除ボタン非活性
            this.btnF3.Enabled = false;
        }

        /// <summary>
        /// 編集モードの初期表示
        /// </summary>
        private void InitDispOnEdit()
        {
            // 仲買人コードを編集不可に設定する
            this.txtNakagaininCd.Enabled = false;

            // 仲買人アドレステーブルと仲買人マスタからデータを取得して表示
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder Sql = new StringBuilder();

            // 仲買人マスタデータを取得
            Sql.Append("SELECT");
            Sql.Append(" A.NAKAGAININ_CD AS NAKAGAININ_CD,");
            Sql.Append(" CM.TORIHIKISAKI_NM AS NAKAGAININ_NM,");
            Sql.Append(" A.MAIL_ADDRESS1 AS ADDRESS1,");
            Sql.Append(" A.ADDRESS1_FLG AS FLG1,");
            Sql.Append(" A.MAIL_ADDRESS2 AS ADDRESS2,");
            Sql.Append(" A.ADDRESS2_FLG AS FLG2,");
            Sql.Append(" A.MAIL_ADDRESS3 AS ADDRESS3,");
            Sql.Append(" A.ADDRESS3_FLG AS FLG3 ");
            Sql.Append("FROM");
            Sql.Append(" TB_HN_SERI_NAKAGAININ_ADDRESS AS A ");
            Sql.Append("LEFT OUTER JOIN");
            Sql.Append(" TB_CM_TORIHIKISAKI AS CM ");
            Sql.Append("ON");
            Sql.Append(" CM.KAISHA_CD = @KAISHA_CD AND CM.TORIHIKISAKI_CD = A.NAKAGAININ_CD AND CM.HYOJI_FLG = @HYOJI_FLG ");
            Sql.Append("WHERE");
            Sql.Append(" A.ICHIBA_CD = @ICHIBA_CD");
            Sql.Append(" AND A.NAKAGAININ_CD = @NAKAGAININ_CD");

            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@HYOJI_FLG", SqlDbType.Decimal, 1, 0);
            dpc.SetParam("@ICHIBA_CD", SqlDbType.Decimal, 3, 108);
            dpc.SetParam("@NAKAGAININ_CD", SqlDbType.Decimal, 4, Util.ToInt(InData));

            DataTable dt = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            if (dt.Rows.Count == 0)
            {
                Msg.Error("不正な起動です。終了します。");
                this.Close();
            }

            // 取得した内容を表示
            DataRow drDispData = dt.Rows[0];
            this.txtNakagaininCd.Text = Util.ToString(drDispData["NAKAGAININ_CD"]); // 仲買人コード
            this.lblNakagaininNm.Text = Util.ToString(drDispData["NAKAGAININ_NM"]); // 仲買人名
            this.txtMailAddress1.Text = Util.ToString(drDispData["ADDRESS1"]); // アドレス1
            this.txtMailAddress2.Text = Util.ToString(drDispData["ADDRESS2"]); // アドレス2
            this.txtMailAddress3.Text = Util.ToString(drDispData["ADDRESS3"]); // アドレス3
            // 有効フラグ
            if (Util.ToInt(drDispData["FLG1"]) == 0)
            {
                this.cbxFlg1.Checked = true;
            }
            if (Util.ToInt(drDispData["FLG2"]) == 0)
            {
                this.cbxFlg2.Checked = true;
            }
            if (Util.ToInt(drDispData["FLG3"]) == 0)
            {
                this.cbxFlg3.Checked = true;
            }

            this.btnF3.Enabled = true;
        }

        /// <summary>
        /// 仲買人コードの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidNakagaininCd()
        {
            // 未入力チェック
            if (ValChk.IsEmpty(this.txtNakagaininCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 数値チェック
            if (!ValChk.IsNumber(this.txtNakagaininCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 仲買人マスタに存在しないコードを入力した場合はエラーとする
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder Sql = new StringBuilder();
            Sql.Append("SELECT");
            Sql.Append(" CM.TORIHIKISAKI_NM AS NAKAGAININ_NM ");
            Sql.Append("FROM");
            Sql.Append(" TB_HN_TORIHIKISAKI_JOHO AS T ");
            Sql.Append("LEFT OUTER JOIN");
            Sql.Append(" TB_CM_TORIHIKISAKI AS CM ");
            Sql.Append("ON");
            Sql.Append(" CM.KAISHA_CD = @KAISHA_CD AND CM.TORIHIKISAKI_CD = T.TORIHIKISAKI_CD AND CM.HYOJI_FLG = @HYOJI_FLG ");
            Sql.Append("WHERE");
            Sql.Append(" T.TORIHIKISAKI_KUBUN2 = @TORIHIKISAKI_KUBUN2");
            Sql.Append(" AND T.TORIHIKISAKI_CD = @TORIHIKISAKI_CD");

            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@HYOJI_FLG", SqlDbType.Decimal, 1, 0);
            dpc.SetParam("@TORIHIKISAKI_KUBUN2", SqlDbType.Decimal, 1, 2);
            dpc.SetParam("@TORIHIKISAKI_CD", SqlDbType.Decimal, 4, this.txtNakagaininCd.Text);

            DataTable dtF = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
            if (dtF.Rows.Count == 0)
            {
                Msg.Error("仲買人マスタに登録されていないコードです。");
                return false;
            }
            else
            {
                this.lblNakagaininNm.Text = Util.ToString(dtF.Rows[0]["NAKAGAININ_NM"]); // 仲買人名

            }

            // 新規作成の場合、
            if (MODE_NEW.Equals(this.Par1))
            {
                // 仲買人アドレステーブルに既に存在するコードを入力した場合はエラーとする
                dpc = new DbParamCollection();
                dpc.SetParam("@ICHIBA_CD", SqlDbType.Decimal, 3, 108);
                dpc.SetParam("@NAKAGAININ_CD", SqlDbType.Decimal, 4, this.txtNakagaininCd.Text);
                StringBuilder where = new StringBuilder("ICHIBA_CD = @ICHIBA_CD");
                where.Append(" AND NAKAGAININ_CD = @NAKAGAININ_CD");
                DataTable dtA =
                    this.Dba.GetDataTableByConditionWithParams("NAKAGAININ_CD",
                        "TB_HN_SERI_NAKAGAININ_ADDRESS", Util.ToString(where), dpc);
                if (dtA.Rows.Count > 0)
                {
                    Msg.Error("既に存在する仲買人コードと重複しています。");
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// アドレス1の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidMailAddress1()
        {
            // 指定バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtMailAddress1.Text, this.txtMailAddress1.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// アドレス2の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidMailAddress2()
        {
            // 指定バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtMailAddress2.Text, this.txtMailAddress2.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// アドレス3の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidMailAddress3()
        {
            // 指定バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtMailAddress3.Text, this.txtMailAddress3.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            if (MODE_NEW.Equals(this.Par1))
            {
                // 仲買人コードのチェック
                if (!IsValidNakagaininCd())
                {
                    this.txtNakagaininCd.Focus();
                    return false;
                }
            }
            // アドレス1のチェック
            if (!IsValidMailAddress1())
            {
                this.txtMailAddress1.Focus();
                return false;
            }
            // アドレス2のチェック
            if (!IsValidMailAddress2())
            {
                this.txtMailAddress2.Focus();
                return false;
            }
            // アドレス3のチェック
            if (!IsValidMailAddress3())
            {
                this.txtMailAddress3.Focus();
                return false;
            }

            return true;
        }

        /// <summary>
        /// TB_HN_SERI_NAKAGAININ_ADDRESSに更新するためのパラメータ設定をします。
        /// </summary>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// 更新処理：DbParamCollection*2(Where句,Set句)
        /// </returns>
        private ArrayList SetParams()
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            if (MODE_NEW.Equals(this.Par1))
            {
                // 市場コードと仲買人コードを更新パラメータに設定
                updParam.SetParam("@ICHIBA_CD", SqlDbType.Decimal, 3, 108);
                updParam.SetParam("@NAKAGAININ_CD", SqlDbType.Decimal, 4, this.txtNakagaininCd.Text);
                // 登録日
                updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWDATE");
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // 市場コードと仲買人コードをWhere句のパラメータに設定
                DbParamCollection whereParam = new DbParamCollection();
                whereParam.SetParam("@ICHIBA_CD", SqlDbType.Decimal, 3, 108);
                whereParam.SetParam("@NAKAGAININ_CD", SqlDbType.Decimal, 4, this.txtNakagaininCd.Text);
                alParams.Add(whereParam);
            }

            // メールアドレス
            updParam.SetParam("@MAIL_ADDRESS1", SqlDbType.VarChar, 50, this.txtMailAddress1.Text);
            updParam.SetParam("@MAIL_ADDRESS2", SqlDbType.VarChar, 50, this.txtMailAddress2.Text);
            updParam.SetParam("@MAIL_ADDRESS3", SqlDbType.VarChar, 50, this.txtMailAddress3.Text);
            // アドレス有効フラグ
            int flg1 = 1;
            int flg2 = 1;
            int flg3 = 1;
            if (this.cbxFlg1.Checked)
            {
                flg1 = 0;
            }
            if (this.cbxFlg2.Checked)
            {
                flg2 = 0;
            }
            if (this.cbxFlg3.Checked)
            {
                flg3 = 0;
            }
            updParam.SetParam("@ADDRESS1_FLG", SqlDbType.Decimal, 1, flg1);
            updParam.SetParam("@ADDRESS2_FLG", SqlDbType.Decimal, 1, flg2);
            updParam.SetParam("@ADDRESS3_FLG", SqlDbType.Decimal, 1, flg3);

            alParams.Add(updParam);

            return alParams;
        }

        /// <summary>
        /// TB_HN_SERI_NAKAGAININ_ADDRESSからデータを削除する為のパラメータ
        /// </summary>
        /// <returns>
        /// </returns>
        private ArrayList SetDelParams()
        {
            // 市場コードと仲買人コードを削除パラメータに設定
            ArrayList alParams = new ArrayList();
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@ICHIBA_CD", SqlDbType.Decimal, 3, 108);
            dpc.SetParam("@NAKAGAININ_CD", SqlDbType.Decimal, 4, this.txtNakagaininCd.Text);

            alParams.Add(dpc);

            return alParams;
        }
        #endregion
    }
}
