﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;
using System.Security.Cryptography;
using jp.co.fsi.common.mynumber;

namespace jp.co.fsi.han.hanc9151
{
    /// <summary>
    /// 設定画面(HANC9153)
    /// </summary>
    public partial class HANC9153 : BasePgForm
    {
        #region 定数
        #endregion

        #region プロパティ
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HANC9153()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region メソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // タイトルは非表示
            this.lblTitle.Visible = false;

            //ボタン表示
            this.ShowFButton = true;

            // 初期表示
            InitDisp();

            // 送信用メールアドレスにフォーカス
            this.ActiveControl = this.txtMailAddress;
            this.txtMailAddress.Focus();

            // ボタンの配置を調整
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF6.Location = this.btnF2.Location;

            // ボタンを調整
            this.btnF1.Visible = false;
            this.btnF2.Visible = false;
            this.btnF3.Visible = false;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF6.Visible = true;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF6();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF6()
        {
            // 確認メッセージを表示
            string msg = "更新しますか？";
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            // 入力値をバインドパラメータとしてセットする
            ArrayList alParams = SetParams();

            try
            {
                this.Dba.BeginTransaction();

                // データ更新
                this.Dba.Update("TB_HN_SERI_MAIL_SETTING",
                    (DbParamCollection)alParams[1],
                    "ICHIBA_CD = @ICHIBA_CD",
                    (DbParamCollection)alParams[0]);

                // トランザクションをコミット
                this.Dba.Commit();

                Msg.Info("更新しました。");
            }
            catch (System.Exception e)
            {
                Msg.Info("更新に失敗しました。");
                // DialogResultに「Cancel」をセットし結果を返却
                this.DialogResult = DialogResult.Cancel;
                this.Close();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 送信用メールアドレスの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMailAddress_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidMailAddress())
            {
                e.Cancel = true;
                this.txtMailAddress.SelectAll();
            }
        }

        /// <summary>
        /// HOST名の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtHostNm_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidHostNm())
            {
                e.Cancel = true;
                this.txtHostNm.SelectAll();
            }
        }

        /// <summary>
        /// PORT番号の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtPortNo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidPortNo())
            {
                e.Cancel = true;
                this.txtPortNo.SelectAll();
            }
        }

        /// <summary>
        /// ユーザー名の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtUserNm_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidUserNm())
            {
                e.Cancel = true;
                this.txtUserNm.SelectAll();
            }
        }

        /// <summary>
        /// パスワードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtPass_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidPass())
            {
                e.Cancel = true;
                this.txtPass.SelectAll();
            }
        }

        /// <summary>
        /// 件名の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSubject_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidSubject())
            {
                e.Cancel = true;
                this.txtSubject.SelectAll();
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 新規モードの初期表示
        /// </summary>
        private void InitDisp()
        {
            // メール送信用設定テーブルからデータを取得して表示
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder Sql = new StringBuilder();

            // 仲買人マスタデータを取得
            Sql.Append("SELECT");
            Sql.Append(" A.TANTO_MAIL_ADDRESS AS MAIL_ADDRESS,");
            Sql.Append(" A.HOST_NM AS HOST_NM,");
            Sql.Append(" A.PORT_NO AS PORT_NO,");
            Sql.Append(" A.SUBJECT_NAKAGAININ AS SUBJECT, ");
            Sql.Append(" A.USER_NM AS USER_NM, ");
            Sql.Append(" A.PASSWORD AS PASSWORD ");
            Sql.Append("FROM");
            Sql.Append(" TB_HN_SERI_MAIL_SETTING AS A ");
            Sql.Append("WHERE");
            Sql.Append(" A.ICHIBA_CD = @ICHIBA_CD");

            dpc.SetParam("@ICHIBA_CD", SqlDbType.Decimal, 3, 108);

            DataTable dt = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            if (dt.Rows.Count == 0)
            {
                Msg.Error("不正な起動です。終了します。");
                this.Close();
            }

            // 取得した内容を表示
            DataRow drDispData = dt.Rows[0];
            this.txtMailAddress.Text = Util.ToString(drDispData["MAIL_ADDRESS"]); // 送信用メールアドレス
            this.txtHostNm.Text = Util.ToString(drDispData["HOST_NM"]); // HOST名
            this.txtPortNo.Text = Util.ToString(drDispData["PORT_NO"]); // PORT番号
            this.txtUserNm.Text = Util.ToString(drDispData["USER_NM"]); // ユーザー名
            this.txtPass.Text = Util.ToString(drDispData["PASSWORD"]); // パスワード
            this.txtSubject.Text = Util.ToString(drDispData["SUBJECT"]); // 件名
        }

        /// <summary>
        /// 送信用メールアドレスの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidMailAddress()
        {
            // 未入力チェック
            if (ValChk.IsEmpty(this.txtMailAddress.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 指定バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtMailAddress.Text, this.txtMailAddress.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 半角英数字チェック
            if (!ValChk.IsHalfChar(this.txtMailAddress.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// HOST名の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidHostNm()
        {
            // 未入力チェック
            if (ValChk.IsEmpty(this.txtHostNm.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 指定バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtHostNm.Text, this.txtHostNm.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 半角英数字チェック
            if (!ValChk.IsHalfChar(this.txtHostNm.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// PORT番号の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidPortNo()
        {
            // 未入力チェック
            if (ValChk.IsEmpty(this.txtPortNo.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 指定バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtPortNo.Text, this.txtPortNo.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 数値チェック
            if (!ValChk.IsNumber(this.txtPortNo.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// ユーザー名の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidUserNm()
        {
            // 未入力チェック
            if (ValChk.IsEmpty(this.txtUserNm.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 指定バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtUserNm.Text, this.txtUserNm.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// パスワードの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidPass()
        {
            // 未入力チェック
            if (ValChk.IsEmpty(this.txtPass.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 指定バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtPass.Text, this.txtPass.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 件名の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidSubject()
        {
            // 指定バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtSubject.Text, this.txtSubject.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 送信用メールアドレスのチェック
            if (!IsValidMailAddress())
            {
                this.txtMailAddress.Focus();
                return false;
            }
            // HOST名のチェック
            if (!IsValidHostNm())
            {
                this.txtHostNm.Focus();
                return false;
            }
            // PORT番号のチェック
            if (!IsValidPortNo())
            {
                this.txtPortNo.Focus();
                return false;
            }
            // ユーザー名のチェック
            if (!IsValidPortNo())
            {
                this.txtPortNo.Focus();
                return false;
            }
            // パスワードのチェック
            if (!IsValidPortNo())
            {
                this.txtPortNo.Focus();
                return false;
            }
            // 件名のチェック
            if (!IsValidSubject())
            {
                this.txtSubject.Focus();
                return false;
            }

            return true;
        }

        /// <summary>
        /// TB_HN_SERI_MAIL_SETTINGに更新するためのパラメータ設定をします。
        /// </summary>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// 更新処理：DbParamCollection*2(Where句,Set句)
        /// </returns>
        private ArrayList SetParams()
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            // 市場コードをWhere句のパラメータに設定
            DbParamCollection whereParam = new DbParamCollection();
            whereParam.SetParam("@ICHIBA_CD", SqlDbType.Decimal, 3, 108);
            alParams.Add(whereParam);

            // メールアドレス
            updParam.SetParam("@TANTO_MAIL_ADDRESS", SqlDbType.VarChar, 50, this.txtMailAddress.Text);
            updParam.SetParam("@HOST_NM", SqlDbType.VarChar, 50, this.txtHostNm.Text);
            updParam.SetParam("@PORT_NO", SqlDbType.Decimal, 10, this.txtPortNo.Text);
            updParam.SetParam("@SUBJECT_NAKAGAININ", SqlDbType.VarChar, 50, this.txtSubject.Text);
            updParam.SetParam("@USER_NM", SqlDbType.VarChar, 50, this.txtUserNm.Text);
            updParam.SetParam("@PASSWORD", SqlDbType.VarChar, 50, this.txtPass.Text);

            alParams.Add(updParam);

            return alParams;
        }
        #endregion
    }
}
