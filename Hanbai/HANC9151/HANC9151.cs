﻿using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Net.Mail;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.han.hanc9151
{
    /// <summary>
    /// メール送信(HANC9151)
    /// </summary>
    public partial class HANC9151 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// モード(コード検索)
        /// </summary>
        private const string MODE_CD_SRC = "1";
        #endregion

        #region プロパティ
        /// <summary>
        /// セリ日をセット
        /// </summary>
        private string[] _today = new string[5];
        public string[] Today
        {
            get
            {
                return this._today;
            }
        }

        /// <summary>
        /// セリ日の曜日をセット
        /// </summary>
        private string _todayWeek;
        public string TodayWeek
        {
            get
            {
                return this._todayWeek;
            }
        }

        /// <summary>
        /// 請求日をセット
        /// </summary>
        private string[] _seikyuDay = new string[5];
        public string[] SeikyuDay
        {
            get
            {
                return this._seikyuDay;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HANC9151()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region メソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.InitForm();は呼び出さなくて構いません。
        /// また、このメソッド内の処理を外出しでこのクラス内にメソッド化するのは構いませんが、
        /// 原則、独自で起動時のイベント処理を実装することは禁じます。
        /// </remarks>
        protected override void InitForm()
        {
            // まずデータが存在し得ない検索条件で検索をし、結果をバインドすることで、
            // 初期状態を作り出す
            SearchData();

            // フォーカス設定
            this.txtKanaName.Focus();
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            // カナ名にフォーカスを戻す
            this.txtKanaName.Focus();
            this.txtKanaName.SelectAll();
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 仲買人アドレス登録画面の起動
            EditNakagainin(string.Empty);
        }

        /// <summary>
        /// F8キー押下時処理
        /// </summary>
        public override void PressF8()
        {
            // 選択会計年度が今年度かチェック
            DateTime today = DateTime.Today;
            if (Util.GetKaikeiNendo(today, this.Dba) != this.UInfo.KaikeiNendo)
            {
                Msg.Error("選択会計年度が今年度ではありません。");
                return;
            }

            // 確認メッセージを表示
            string msg = "メールを送信しますか？";
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            /* メール送信処理 */
            bool flg = mailSet();

            if(flg)
            {
                Msg.Info("メール送信が完了しました。");
            }
            else
            {
                Msg.Info("メール送信に失敗しました。");
            }
        }

        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            HANC9153 frmHANC9153 = new HANC9153();

            DialogResult result = frmHANC9153.ShowDialog(this);

            if (result == DialogResult.OK)
            {
                // Gridに再度フォーカスをセット
                this.ActiveControl = this.dgvList;
                this.dgvList.Focus();
            }
        }
        #endregion

        #region イベント
        /// <summary>
        /// 表示ボタンの変更時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ckbAllHyoji_CheckedChanged(object sender, System.EventArgs e)
        {
            // 入力された情報を元に検索する
            SearchData();
        }

        /// <summary>
        /// チェックボタンの変更時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ckbAllCheck_CheckedChanged(object sender, System.EventArgs e)
        {
            // チェックボックスの切り替えを行う
            checkOnOff();
        }

        /// <summary>
        /// カナ名検証時の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKanaName_Validating(object sender, CancelEventArgs e)
        {
            //TODO:何かチェックが必要なのかもしれない
            // 入力された情報を元に検索する
            SearchData();
        }

        /// <summary>
        /// グリッドでのキーダウン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                EditNakagainin(Util.ToString(this.dgvList.SelectedRows[0].Cells["nakagaininCd"].Value));
                e.Handled = true;
            }
        }

        /// <summary>
        /// グリッドのセルダブルクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            EditNakagainin(Util.ToString(this.dgvList.SelectedRows[0].Cells["nakagaininCd"].Value));
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// データを検索する
        /// </summary>
        private void checkOnOff()
        {
            bool flg = false;
            // 「全てチェック」の場合
            if (this.ckbAllCheck.Checked)
            {
                flg = true;
            }

            // 送信フラグのON/OFF設定
            foreach (DataGridViewRow rowDt in this.dgvList.Rows)
            {
                rowDt.Cells[3].Value = flg;
            }
        }

        /// <summary>
        /// データを検索する
        /// </summary>
        private void SearchData()
        {
            // 仲買人アドレステーブルと仲買人マスタからデータを取得して表示
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder Sql = new StringBuilder();

            // 仲買人マスタデータを取得
            Sql.Append("SELECT");
            Sql.Append(" A.NAKAGAININ_CD AS NAKAGAININ_CD,");
            Sql.Append(" CM.TORIHIKISAKI_NM AS NAKAGAININ_NM,");
            Sql.Append(" CM.TORIHIKISAKI_KANA_NM AS NAKAGAININ_KANA ");
            Sql.Append("FROM");
            Sql.Append(" TB_HN_SERI_NAKAGAININ_ADDRESS AS A ");
            Sql.Append("LEFT OUTER JOIN");
            Sql.Append(" TB_CM_TORIHIKISAKI AS CM ");
            Sql.Append("ON");
            Sql.Append(" CM.KAISHA_CD = @KAISHA_CD AND CM.TORIHIKISAKI_CD = A.NAKAGAININ_CD AND CM.HYOJI_FLG = @HYOJI_FLG ");
            Sql.Append("WHERE");
            Sql.Append(" 1 = 1");

            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@HYOJI_FLG", SqlDbType.Decimal, 1, 0);

            // 入力されたカナ名から検索する
            if (!ValChk.IsEmpty(this.txtKanaName.Text))
            {
                Sql.Append(" AND CM.TORIHIKISAKI_KANA_NM LIKE @TORIHIKISAKI_KANA_NM");
                // NOTICE!:部分一致検索をする場合、"%"の分桁数に2バイトを足すこと
                dpc.SetParam("@TORIHIKISAKI_KANA_NM", SqlDbType.VarChar, 30, "%" + this.txtKanaName.Text + "%");
            }
            // 「全て表示」でない場合、有効なアドレスが存在しない仲買人は表示しない
            if (this.ckbAllHyoji.Checked == false)
            {
                Sql.Append(" AND ((A.MAIL_ADDRESS1 IS NOT NULL AND A.MAIL_ADDRESS1 != '' AND A.ADDRESS1_FLG = 0) OR");
                Sql.Append(" (A.MAIL_ADDRESS2 IS NOT NULL AND A.MAIL_ADDRESS2 != '' AND A.ADDRESS2_FLG = 0) OR");
                Sql.Append(" (A.MAIL_ADDRESS3 IS NOT NULL AND A.MAIL_ADDRESS3 != '' AND A.ADDRESS3_FLG = 0))");
            }

            DataTable dt = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            // 初期処理以外の場合、該当データがなければエラーメッセージを表示
            if (dt.Rows.Count == 0)
            {
                Msg.Info("該当データがありません。");

                dt.Rows.Add(dt.NewRow());
            }

            // 明細のクリア
            this.dgvList.Rows.Clear();
            // 明細へのデータ設定
            int rows = dt.Rows.Count;
            int row = 0;

            // データを明細にセットする
            foreach (DataRow rowDt in dt.Rows)
            {
                // １行分の列（セル）データを設定する
                this.dgvList.Rows.Add();
                this.dgvList.Rows[row].Cells[0].Value = rowDt["NAKAGAININ_CD"].ToString(); // 仲買人コード
                this.dgvList.Rows[row].Cells[1].Value = rowDt["NAKAGAININ_NM"].ToString(); // 仲買人名
                this.dgvList.Rows[row].Cells[2].Value = rowDt["NAKAGAININ_KANA"].ToString(); // 仲買人カナ名
                // メール送信フラグ
                if (this.ckbAllCheck.Checked)
                {
                    // 「全てチェック」の場合
                    this.dgvList.Rows[row].Cells[3].Value = true;
                }
                else
                {
                    // 「全てチェック」でない場合
                    this.dgvList.Rows[row].Cells[3].Value = false;
                }
                row++;
            }
        }

        /// <summary>
        /// 仲買人主を追加編集する
        /// </summary>
        /// <param name="code">仲買人コード(空：新規登録、以外：編集)</param>
        private void EditNakagainin(string code)
        {
            HANC9152 frmHANC9152;

            if (ValChk.IsEmpty(code))
            {
                // 新規登録モードで登録画面を起動
                frmHANC9152 = new HANC9152("1", this.Par2);
            }
            else
            {
                // 編集モードで登録画面を起動
                frmHANC9152 = new HANC9152("2", this.Par2);
                frmHANC9152.InData = code;
            }

            DialogResult result = frmHANC9152.ShowDialog(this);

            if (result == DialogResult.OK)
            {
                // データを再検索する
                SearchData();
                // Gridに再度フォーカスをセット
                this.ActiveControl = this.dgvList;
                this.dgvList.Focus();
            }
        }

        /// <summary>
        /// メール送信用のデータを作成する
        /// </summary>
        private bool mailSet()
        {
            try
            {
                // 送信用データを取得する
                DataTable getSendDt = getSendMailAddress();
                // 送信用情報をセットする
                string mailAddress = "";
                string subject = "";
                string hostNm = "";
                int portNo = 0;
                string userNm = "";
                string password = "";
                if (getSendDt.Rows.Count > 0)
                {
                    mailAddress = Util.ToString(getSendDt.Rows[0]["MAIL_ADDRESS"]);
                    subject = Util.ToString(getSendDt.Rows[0]["SUBJECT"]);
                    hostNm = Util.ToString(getSendDt.Rows[0]["HOST_NM"]);
                    portNo = Util.ToInt(getSendDt.Rows[0]["PORT_NO"]);
                    userNm = Util.ToString(getSendDt.Rows[0]["USER_NM"]);
                    password = Util.ToString(getSendDt.Rows[0]["PASSWORD"]);
                }
                // 今日の日付をセットする
                DateTime dt = DateTime.Now;
                this._today = Util.ConvJpDate(dt, this.Dba);
                this._todayWeek = dt.ToString("ddd");
                // 支払期日をセットする
                // 日付(請求書支払日)を取得
                DateTime seikyuDt = dt.AddDays(7);
                this._seikyuDay = Util.ConvJpDate(seikyuDt, this.Dba);

                string address1 = "";
                string address2 = "";
                string address3 = "";
                DataTable getAddressList;
                string nakagaininCd;
                DataTable seriDt;
                DataTable seriMeisaiDt;
                bool flgAdd1;
                bool flgAdd2;
                bool flgAdd3;

                #region データを明細にセットする
                foreach (DataGridViewRow rowDt in this.dgvList.Rows)
                {
                    // 送信フラグがONの場合、仲買人CDを格納
                    if (Util.ToString(rowDt.Cells[3].Value) == "True")
                    {
                        // 該当仲買人コードを取得
                        nakagaininCd = Util.ToString(rowDt.Cells[0].Value);

                        // 仲買人コードに紐づく本日の鮮魚代金情報を取得
                        seriDt = getSeriData(nakagaininCd);
                        // 該当データが存在しない場合、以降処理を行わない
                        if (seriDt.Rows.Count == 0)
                        {
                            continue;
                        }

                        // 仲買人コードに紐づく本日の鮮魚代金明細情報を取得
                        seriMeisaiDt = getSeriMeisaiData(nakagaininCd);

                        // 該当する仲買人のアドレスを取得
                        getAddressList = getNakagaininAddress(nakagaininCd);

                        // 有効なアドレスが1件以上存在する場合、有効なアドレスをセット
                        if (getAddressList.Rows.Count > 0)
                        {
                            // アドレス1のフラグを初期化
                            if (Util.ToString(getAddressList.Rows[0]["ADDRESS1_FLG"]) == "0")
                            {
                                flgAdd1 = true;
                            }
                            else
                            {
                                flgAdd1 = false;
                            }

                            // アドレス2のフラグを初期化
                            if (Util.ToString(getAddressList.Rows[0]["ADDRESS2_FLG"]) == "0")
                            {
                                flgAdd2 = true;
                            }
                            else
                            {
                                flgAdd2 = false;
                            }

                            // アドレス3のフラグを初期化
                            if (Util.ToString(getAddressList.Rows[0]["ADDRESS3_FLG"]) == "0")
                            {
                                flgAdd3 = true;
                            }
                            else
                            {
                                flgAdd3 = false;
                            }

                            // アドレス1
                            if (flgAdd1 && !ValChk.IsEmpty(getAddressList.Rows[0]["ADDRESS1"]))
                            {
                                address1 = Util.ToString(getAddressList.Rows[0]["ADDRESS1"]);
                            }
                            else if (flgAdd2 && !ValChk.IsEmpty(getAddressList.Rows[0]["ADDRESS2"]))
                            {
                                address1 = Util.ToString(getAddressList.Rows[0]["ADDRESS2"]);
                                flgAdd2 = false;
                            }
                            else if (flgAdd3 && !ValChk.IsEmpty(getAddressList.Rows[0]["ADDRESS3"]))
                            {
                                address1 = Util.ToString(getAddressList.Rows[0]["ADDRESS3"]);
                                flgAdd3 = false;
                            }

                            // アドレス2
                            if (flgAdd2 && !ValChk.IsEmpty(address1) && !ValChk.IsEmpty(getAddressList.Rows[0]["ADDRESS2"]))
                            {
                                address2 = Util.ToString(getAddressList.Rows[0]["ADDRESS2"]);
                            }

                            // アドレス3
                            if (flgAdd3 && !ValChk.IsEmpty(address1) && !ValChk.IsEmpty(address2) && !ValChk.IsEmpty(getAddressList.Rows[0]["ADDRESS3"]))
                            {
                                address3 = Util.ToString(getAddressList.Rows[0]["ADDRESS3"]);
                            }
                            else if(flgAdd3 && !ValChk.IsEmpty(address1) && ValChk.IsEmpty(address2) && !ValChk.IsEmpty(getAddressList.Rows[0]["ADDRESS3"]))
                            {
                                address2 = Util.ToString(getAddressList.Rows[0]["ADDRESS3"]);
                            }

                            // メール内容を生成し、送信する
                            sendMail(seriDt, seriMeisaiDt, mailAddress, subject, hostNm, portNo, userNm, password, address1, address2, address3);

                            // 初期化
                            address1 = "";
                            address2 = "";
                            address3 = "";
                        }
                    }
                }
                #endregion

                return true;
            }
            catch (System.Exception e)
            {
                return false;
            }
        }
        
        /// <summary>
        /// データを検索する
        /// </summary>
        private DataTable getNakagaininAddress(string code)
        {
            // 仲買人アドレステーブルと仲買人マスタからデータを取得して表示
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder Sql = new StringBuilder();

            // 仲買人マスタデータを取得
            Sql.Append("SELECT");
            Sql.Append(" A.MAIL_ADDRESS1 AS ADDRESS1,");
            Sql.Append(" A.ADDRESS1_FLG AS ADDRESS1_FLG,");
            Sql.Append(" A.MAIL_ADDRESS2 AS ADDRESS2,");
            Sql.Append(" A.ADDRESS2_FLG AS ADDRESS2_FLG,");
            Sql.Append(" A.MAIL_ADDRESS3 AS ADDRESS3,");
            Sql.Append(" A.ADDRESS3_FLG AS ADDRESS3_FLG ");
            Sql.Append("FROM");
            Sql.Append(" TB_HN_SERI_NAKAGAININ_ADDRESS AS A ");
            Sql.Append("WHERE");
            Sql.Append(" NAKAGAININ_CD = @NAKAGAININ_CD AND");
            Sql.Append(" ((A.MAIL_ADDRESS1 IS NOT NULL AND A.MAIL_ADDRESS1 != '' AND A.ADDRESS1_FLG = 0) OR");
            Sql.Append(" (A.MAIL_ADDRESS2 IS NOT NULL AND A.MAIL_ADDRESS2 != '' AND A.ADDRESS2_FLG = 0) OR");
            Sql.Append(" (A.MAIL_ADDRESS3 IS NOT NULL AND A.MAIL_ADDRESS3 != '' AND A.ADDRESS3_FLG = 0))");

            dpc.SetParam("@NAKAGAININ_CD", SqlDbType.VarChar, 4, code);

            DataTable dt = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            return dt;
        }

        /// <summary>
        /// 送信者のメールアドレスを取得する
        /// </summary>
        private DataTable getSendMailAddress()
        {
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder Sql = new StringBuilder();

            // メール設定マスタデータを取得
            Sql.Append("SELECT");
            Sql.Append(" A.TANTO_MAIL_ADDRESS AS MAIL_ADDRESS,");
            Sql.Append(" A.SUBJECT_NAKAGAININ AS SUBJECT,");
            Sql.Append(" A.HP_ADDRESS AS HP_ADDRESS,");
            Sql.Append(" A.HOST_NM AS HOST_NM,");
            Sql.Append(" A.PORT_NO AS PORT_NO, ");
            Sql.Append(" A.USER_NM AS USER_NM, ");
            Sql.Append(" A.PASSWORD AS PASSWORD ");
            Sql.Append("FROM");
            Sql.Append(" TB_HN_SERI_MAIL_SETTING AS A ");
            Sql.Append("WHERE");
            Sql.Append(" ICHIBA_CD = @ICHIBA_CD");

            dpc.SetParam("@ICHIBA_CD", SqlDbType.Decimal, 4, 108);

            DataTable dt = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            return dt;
        }

        /// <summary>
        /// メールを送信する
        /// </summary>
        private void sendMail(
            DataTable seriDt, DataTable seriMeisaiDt, string mailAddress, 
            string subject, string hostNm, int portNo, string userNm, string password, 
            string address1, string address2, string address3)
        {
            try
            {
                MailMessage msg = new MailMessage();

                // 送信者 ※担当ユーザーアドレス
                msg.From = new MailAddress(mailAddress);
                // 宛先(Bcc) ※担当ユーザーアドレス
                msg.Bcc.Add(new MailAddress(mailAddress));

                // 宛先(To) ※仲買人のアドレス
                msg.To.Add(new MailAddress(address1));
                // 宛先(Cc) ※仲買人のアドレス
                if (!ValChk.IsEmpty(address2))
                {
                    msg.CC.Add(new MailAddress(address2));
                }
                if (!ValChk.IsEmpty(address3))
                {
                    msg.CC.Add(new MailAddress(address3));
                }

                // 添付資料 ※今回は無しなので、コメントアウト
                //System.Net.Mail.Attachment attach1 =
                //    new System.Net.Mail.Attachment("C:\\work\\pdf\\XXX.pdf");
                //msg.Attachments.Add(attach1);

                // 件名
                msg.Subject = subject;
                // 本文
                msg.BodyEncoding = System.Text.Encoding.GetEncoding(50220);
                msg.Body = "\r\n鮮魚代金明細です。";
                msg.Body += "\r\n\r\nセリ日 " + this._today[5] + " [" + this._todayWeek + "]";
                msg.Body += "\r\n\r\n会員名 ： " + Util.ToString(seriDt.Rows[0]["NAKAGAININ_NM"]) + "  殿\r\n";

                msg.Body += "\r\n--------------------------------------------------------------------------";
                msg.Body += "\r\n・番号、 魚種、 数量、 単価、 金額、 摘要";
                msg.Body += "\r\n--------------------------------------------------------------------------";

                // セリ明細
                Encoding sjisEnc = Encoding.GetEncoding("Shift_JIS");
                int num; 
                foreach (DataRow row in seriMeisaiDt.Rows)
                {
                    num = sjisEnc.GetByteCount(Util.ToString(row["GYOSHU_NM"]));
                    num = (10 - num / 2);

                    msg.Body += "\r\n・" + Util.PadZero(Util.ToString(row["YAMA_NO"]), 4) + "、 ";
                    msg.Body += String.Format("{0, " + num + "}", Util.ToString(row["GYOSHU_NM"])) + "、 ";
                    msg.Body += Util.FormatNum(row["SURYO"], 1) + "Kg、";
                    msg.Body += Util.FormatNum(row["TANKA"]) + "円、 ";
                    msg.Body += Util.FormatNum(row["KINGAKU"]) + "円、";
                    msg.Body += "\r\n----------------------------";
                }

                msg.Body += "\r\n--------------------------------------------------------------------------";
                msg.Body += "\r\n税抜合計 ： " + Util.FormatNum(seriDt.Rows[0]["KINGAKU"]) + "円";
                msg.Body += "\r\n消費税 ： " + Util.FormatNum(seriDt.Rows[0]["SHOHIZEI"]) + "円";
                msg.Body += "\r\n合計 ： ";
                msg.Body += "数量 " + Util.FormatNum(seriDt.Rows[0]["SURYO"]) + "Kg、 ";
                msg.Body += "単価 " + Util.FormatNum(seriDt.Rows[0]["TANKA"]) + "円、 ";
                msg.Body += "金額 " + Util.FormatNum(seriDt.Rows[0]["GOKEI_KINGAKU"]) + "円";

                msg.Body += "\r\n--------------------------------------------------------------------------";
                msg.Body += "\r\n\r\n本日のお買い上げ代金は上記のとおりです。\r\n";
                msg.Body += "\r\n--------------------------------------------------------------------------";
                msg.Body += "\r\n\r\n前回請求残 ： " + Util.FormatNum(seriDt.Rows[0]["ZENKAI_SEIKYU"]) + "円";
                msg.Body += "\r\n今回買上高 ： " + Util.FormatNum(seriDt.Rows[0]["GOKEI_KINGAKU"]) + "円";
                msg.Body += "\r\n合計請求金額 ： " + Util.FormatNum(seriDt.Rows[0]["GOKEI_SEIKYU"]) + "円\r\n";
                msg.Body += "\r\n--------------------------------------------------------------------------";

                msg.Body += "\r\n\r\n上記のとおり請求しますので、確認のうえ";
                msg.Body += "\r\n"+ this._seikyuDay[5] + "（当日が休日の場合は翌営業日）までにお支払ください。";

                msg.Body += "\r\n\r\n漁 協 名 ： 名護漁業協同組合";
                msg.Body += "\r\n住　　 所 ： 沖縄県名護市城三丁目１番１号";
                msg.Body += "\r\nＴ　Ｅ　Ｌ  ： (0980)52-2812";
                msg.Body += "\r\nセリ市場 ： (0980)51-0727";

                System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();
                smtp.Host = hostNm;
                smtp.Port = portNo;
                smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                smtp.Credentials = new System.Net.NetworkCredential(userNm, password);
                smtp.Send(msg);

                msg.Dispose();
                smtp.Dispose();
            }
            catch (System.Exception e)
            {
                //Msg.Info("例外が発生しました。");
            }
        }

        /// <summary>
        /// 鮮魚代金情報を取得する
        /// </summary>
        private DataTable getSeriData(string nakagaininCd)
        {
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder Sql = new StringBuilder();

            // 鮮魚代金情報を取得
            Sql.Append("SELECT");
            Sql.Append(" A.NAKAGAININ_NM AS NAKAGAININ_NM,");
            Sql.Append(" SUM(A.SURYO) AS SURYO,");
            Sql.Append(" AVG(A.TANKA) AS TANKA,");
            Sql.Append(" SUM(A.KINGAKU) AS KINGAKU,");
            Sql.Append(" SUM(A.KINGAKU * (A.ZEI_RITSU/100)) AS SHOHIZEI,");
            Sql.Append(" SUM(A.KINGAKU * (A.ZEI_RITSU/100)) + SUM(A.KINGAKU) AS GOKEI_KINGAKU,");
            Sql.Append(" MAX(B.ZANDAKA) AS ZENKAI_SEIKYU,");
            Sql.Append(" SUM(A.KINGAKU * (A.ZEI_RITSU/100)) + SUM(A.KINGAKU) + MAX(B.ZANDAKA) AS GOKEI_SEIKYU ");
            Sql.Append("FROM");
            Sql.Append(" VI_HN_SNGDIKN_MISI_KEN_SIKS AS A ");
            Sql.Append("LEFT OUTER JOIN");
            Sql.Append(" (SELECT");
            Sql.Append("  A.TORIHIKISAKI_CD AS KAIIN_BANGO,");
            Sql.Append("  SUM(CASE WHEN C.TAISHAKU_KUBUN = D.TAISHAKU_KUBUN THEN C.ZEIKOMI_KINGAKU ELSE C.ZEIKOMI_KINGAKU * -1 END) AS ZANDAKA");
            Sql.Append(" FROM");
            Sql.Append("  (SELECT");
            Sql.Append("   A.KAISHA_CD AS KAISHA_CD,");
            Sql.Append("   A.TORIHIKISAKI_CD AS TORIHIKISAKI_CD");
            Sql.Append("  FROM");
            Sql.Append("   TB_CM_TORIHIKISAKI AS A");
            Sql.Append("  LEFT OUTER JOIN");
            Sql.Append("   TB_HN_TORIHIKISAKI_JOHO AS B");
            Sql.Append("  ON");
            Sql.Append("   A.KAISHA_CD = B.KAISHA_CD");
            Sql.Append("   AND A.TORIHIKISAKI_CD = B.TORIHIKISAKI_CD");
            Sql.Append("   AND B.TORIHIKISAKI_KUBUN2 = 2");
            Sql.Append("  WHERE");
            Sql.Append("   A.KAISHA_CD = @KAISHA_CD");
            Sql.Append("   AND A.TORIHIKISAKI_CD = @NAKAGAININ_CD");
            Sql.Append("  ) AS A");
            Sql.Append(" LEFT OUTER JOIN");
            Sql.Append("  TB_HN_KISHU_ZAN_KAMOKU AS B");
            Sql.Append(" ON");
            Sql.Append("  A.KAISHA_CD = B.KAISHA_CD");
            Sql.Append("  AND B.MOTOCHO_KUBUN = 2");
            Sql.Append(" LEFT OUTER JOIN");
            Sql.Append("  TB_ZM_SHIWAKE_MEISAI AS C");
            Sql.Append(" ON");
            Sql.Append("  A.KAISHA_CD = C.KAISHA_CD");
            Sql.Append("  AND A.TORIHIKISAKI_CD = C.HOJO_KAMOKU_CD");
            Sql.Append("  AND B.KANJO_KAMOKU_CD = C.KANJO_KAMOKU_CD");
            Sql.Append("  AND C.KAIKEI_NENDO = @KAIKEI_NENDO");
            Sql.Append("  AND C.DENPYO_DATE < @TODAY");
            Sql.Append(" LEFT OUTER JOIN");
            Sql.Append("  TB_ZM_KANJO_KAMOKU AS D");
            Sql.Append(" ON");
            Sql.Append("  B.KAISHA_CD = D.KAISHA_CD");
            Sql.Append("  AND B.KANJO_KAMOKU_CD = D.KANJO_KAMOKU_CD");
            Sql.Append("  AND D.KAIKEI_NENDO = C.KAIKEI_NENDO");
            Sql.Append(" GROUP BY");
            Sql.Append("  A.TORIHIKISAKI_CD");
            Sql.Append(" ) AS B ");
            Sql.Append("ON");
            Sql.Append(" A.NAKAGAININ_CD = B.KAIIN_BANGO ");
            Sql.Append("WHERE");
            Sql.Append(" A.KAISHA_CD = @KAISHA_CD");
            Sql.Append(" AND A.SERIBI = @TODAY");
            Sql.Append(" AND NAKAGAININ_CD = @NAKAGAININ_CD ");
            Sql.Append("GROUP BY");
            Sql.Append(" A.NAKAGAININ_NM");

            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            dpc.SetParam("@TODAY", SqlDbType.DateTime, 20, DateTime.Now.Date);
            dpc.SetParam("@NAKAGAININ_CD", SqlDbType.Decimal, 4, nakagaininCd);

            DataTable dt = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            return dt;
        }

        /// <summary>
        /// 鮮魚代金明細情報を取得する
        /// </summary>
        private DataTable getSeriMeisaiData(string nakagaininCd)
        {
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder Sql = new StringBuilder();

            // セリ明細データを取得
            Sql.Append("SELECT");
            Sql.Append(" A.YAMA_NO,");
            Sql.Append(" SUBSTRING(A.GYOSHU_NM, 1, 5) AS GYOSHU_NM,");
            Sql.Append(" A.SURYO,");
            Sql.Append(" A.TANKA,");
            Sql.Append(" A.KINGAKU ");
            Sql.Append("FROM");
            Sql.Append(" VI_HN_SNGDIKN_MISI_KEN_SIKS AS A ");
            Sql.Append("WHERE");
            Sql.Append(" A.KAISHA_CD = @KAISHA_CD");
            Sql.Append(" AND A.SERIBI = @TODAY");
            Sql.Append(" AND NAKAGAININ_CD = @NAKAGAININ_CD ");
            Sql.Append("ORDER BY");
            Sql.Append(" YAMA_NO ASC");

            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            dpc.SetParam("@TODAY", SqlDbType.DateTime, 20, DateTime.Now.Date);
            dpc.SetParam("@NAKAGAININ_CD", SqlDbType.Decimal, 4, nakagaininCd);

            DataTable dt = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            return dt;
        }
        #endregion
    }
}
