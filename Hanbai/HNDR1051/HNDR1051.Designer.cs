﻿namespace jp.co.fsi.hn.hndr1051
{
    partial class HNDR1051
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblDateFr = new System.Windows.Forms.Label();
            this.lblDateGengo = new System.Windows.Forms.Label();
            this.txtDateMonth = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtDateYear = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtDateDay = new jp.co.fsi.common.controls.FsiTextBox();
            this.labelDateYear = new System.Windows.Forms.Label();
            this.lblDateMonth = new System.Windows.Forms.Label();
            this.lblDateDay = new System.Windows.Forms.Label();
            this.gbxDate = new System.Windows.Forms.GroupBox();
            this.gbxNakagaininCd = new System.Windows.Forms.GroupBox();
            this.lblNakagaininCdTo = new System.Windows.Forms.Label();
            this.lblNakagaininCdFr = new System.Windows.Forms.Label();
            this.lblNakagaininCdBet = new System.Windows.Forms.Label();
            this.txtNakagaininCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtNakagaininCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.gbxMizuageShisho = new System.Windows.Forms.GroupBox();
            this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMizuageShishoNm = new System.Windows.Forms.Label();
            this.lblMizuageShisho = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.gbxDate.SuspendLayout();
            this.gbxNakagaininCd.SuspendLayout();
            this.gbxMizuageShisho.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.TabIndex = 999;
            this.lblTitle.Text = "";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Size = new System.Drawing.Size(847, 100);
            // 
            // lblDateFr
            // 
            this.lblDateFr.BackColor = System.Drawing.Color.Silver;
            this.lblDateFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblDateFr.Location = new System.Drawing.Point(7, 33);
            this.lblDateFr.Name = "lblDateFr";
            this.lblDateFr.Size = new System.Drawing.Size(199, 25);
            this.lblDateFr.TabIndex = 0;
            // 
            // lblDateGengo
            // 
            this.lblDateGengo.BackColor = System.Drawing.Color.Silver;
            this.lblDateGengo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDateGengo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblDateGengo.Location = new System.Drawing.Point(10, 35);
            this.lblDateGengo.Name = "lblDateGengo";
            this.lblDateGengo.Size = new System.Drawing.Size(41, 22);
            this.lblDateGengo.TabIndex = 1;
            this.lblDateGengo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDateMonth
            // 
            this.txtDateMonth.AutoSizeFromLength = false;
            this.txtDateMonth.DisplayLength = null;
            this.txtDateMonth.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtDateMonth.Location = new System.Drawing.Point(102, 35);
            this.txtDateMonth.MaxLength = 2;
            this.txtDateMonth.Name = "txtDateMonth";
            this.txtDateMonth.Size = new System.Drawing.Size(30, 20);
            this.txtDateMonth.TabIndex = 3;
            this.txtDateMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateMonth.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateMonth_Validating);
            // 
            // txtDateYear
            // 
            this.txtDateYear.AutoSizeFromLength = false;
            this.txtDateYear.DisplayLength = null;
            this.txtDateYear.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtDateYear.Location = new System.Drawing.Point(52, 35);
            this.txtDateYear.MaxLength = 2;
            this.txtDateYear.Name = "txtDateYear";
            this.txtDateYear.Size = new System.Drawing.Size(30, 20);
            this.txtDateYear.TabIndex = 2;
            this.txtDateYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateYear.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateYear_Validating);
            // 
            // txtDateDay
            // 
            this.txtDateDay.AutoSizeFromLength = false;
            this.txtDateDay.DisplayLength = null;
            this.txtDateDay.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtDateDay.Location = new System.Drawing.Point(152, 35);
            this.txtDateDay.MaxLength = 2;
            this.txtDateDay.Name = "txtDateDay";
            this.txtDateDay.Size = new System.Drawing.Size(30, 20);
            this.txtDateDay.TabIndex = 4;
            this.txtDateDay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateDay.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateDay_Validating);
            // 
            // labelDateYear
            // 
            this.labelDateYear.BackColor = System.Drawing.Color.Silver;
            this.labelDateYear.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.labelDateYear.Location = new System.Drawing.Point(84, 35);
            this.labelDateYear.Name = "labelDateYear";
            this.labelDateYear.Size = new System.Drawing.Size(17, 21);
            this.labelDateYear.TabIndex = 3;
            this.labelDateYear.Text = "年";
            this.labelDateYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateMonth
            // 
            this.lblDateMonth.BackColor = System.Drawing.Color.Silver;
            this.lblDateMonth.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblDateMonth.Location = new System.Drawing.Point(134, 35);
            this.lblDateMonth.Name = "lblDateMonth";
            this.lblDateMonth.Size = new System.Drawing.Size(15, 19);
            this.lblDateMonth.TabIndex = 5;
            this.lblDateMonth.Text = "月";
            this.lblDateMonth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateDay
            // 
            this.lblDateDay.BackColor = System.Drawing.Color.Silver;
            this.lblDateDay.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblDateDay.Location = new System.Drawing.Point(184, 35);
            this.lblDateDay.Name = "lblDateDay";
            this.lblDateDay.Size = new System.Drawing.Size(20, 18);
            this.lblDateDay.TabIndex = 7;
            this.lblDateDay.Text = "日";
            this.lblDateDay.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // gbxDate
            // 
            this.gbxDate.Controls.Add(this.lblDateDay);
            this.gbxDate.Controls.Add(this.lblDateMonth);
            this.gbxDate.Controls.Add(this.labelDateYear);
            this.gbxDate.Controls.Add(this.txtDateDay);
            this.gbxDate.Controls.Add(this.txtDateYear);
            this.gbxDate.Controls.Add(this.txtDateMonth);
            this.gbxDate.Controls.Add(this.lblDateGengo);
            this.gbxDate.Controls.Add(this.lblDateFr);
            this.gbxDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.gbxDate.ForeColor = System.Drawing.SystemColors.ControlText;
            this.gbxDate.Location = new System.Drawing.Point(12, 141);
            this.gbxDate.Name = "gbxDate";
            this.gbxDate.Size = new System.Drawing.Size(220, 75);
            this.gbxDate.TabIndex = 1;
            this.gbxDate.TabStop = false;
            this.gbxDate.Text = "日付範囲";
            // 
            // gbxNakagaininCd
            // 
            this.gbxNakagaininCd.Controls.Add(this.lblNakagaininCdTo);
            this.gbxNakagaininCd.Controls.Add(this.lblNakagaininCdFr);
            this.gbxNakagaininCd.Controls.Add(this.lblNakagaininCdBet);
            this.gbxNakagaininCd.Controls.Add(this.txtNakagaininCdTo);
            this.gbxNakagaininCd.Controls.Add(this.txtNakagaininCdFr);
            this.gbxNakagaininCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.gbxNakagaininCd.ForeColor = System.Drawing.SystemColors.ControlText;
            this.gbxNakagaininCd.Location = new System.Drawing.Point(12, 233);
            this.gbxNakagaininCd.Name = "gbxNakagaininCd";
            this.gbxNakagaininCd.Size = new System.Drawing.Size(525, 80);
            this.gbxNakagaininCd.TabIndex = 2;
            this.gbxNakagaininCd.TabStop = false;
            this.gbxNakagaininCd.Text = "仲買人CD範囲";
            // 
            // lblNakagaininCdTo
            // 
            this.lblNakagaininCdTo.BackColor = System.Drawing.Color.Silver;
            this.lblNakagaininCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblNakagaininCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblNakagaininCdTo.Location = new System.Drawing.Point(327, 36);
            this.lblNakagaininCdTo.Name = "lblNakagaininCdTo";
            this.lblNakagaininCdTo.Size = new System.Drawing.Size(186, 20);
            this.lblNakagaininCdTo.TabIndex = 4;
            this.lblNakagaininCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblNakagaininCdFr
            // 
            this.lblNakagaininCdFr.BackColor = System.Drawing.Color.Silver;
            this.lblNakagaininCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblNakagaininCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblNakagaininCdFr.Location = new System.Drawing.Point(62, 35);
            this.lblNakagaininCdFr.Name = "lblNakagaininCdFr";
            this.lblNakagaininCdFr.Size = new System.Drawing.Size(186, 20);
            this.lblNakagaininCdFr.TabIndex = 1;
            this.lblNakagaininCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblNakagaininCdBet
            // 
            this.lblNakagaininCdBet.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblNakagaininCdBet.Location = new System.Drawing.Point(254, 34);
            this.lblNakagaininCdBet.Name = "lblNakagaininCdBet";
            this.lblNakagaininCdBet.Size = new System.Drawing.Size(17, 22);
            this.lblNakagaininCdBet.TabIndex = 2;
            this.lblNakagaininCdBet.Text = "～";
            this.lblNakagaininCdBet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtNakagaininCdTo
            // 
            this.txtNakagaininCdTo.AutoSizeFromLength = false;
            this.txtNakagaininCdTo.DisplayLength = null;
            this.txtNakagaininCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtNakagaininCdTo.Location = new System.Drawing.Point(277, 36);
            this.txtNakagaininCdTo.MaxLength = 4;
            this.txtNakagaininCdTo.Name = "txtNakagaininCdTo";
            this.txtNakagaininCdTo.Size = new System.Drawing.Size(49, 20);
            this.txtNakagaininCdTo.TabIndex = 6;
            this.txtNakagaininCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtNakagaininCdTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNakagaininCdTo_KeyDown);
            this.txtNakagaininCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtNakagaininCdTo_Validating);
            // 
            // txtNakagaininCdFr
            // 
            this.txtNakagaininCdFr.AutoSizeFromLength = false;
            this.txtNakagaininCdFr.DisplayLength = null;
            this.txtNakagaininCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtNakagaininCdFr.Location = new System.Drawing.Point(12, 35);
            this.txtNakagaininCdFr.MaxLength = 4;
            this.txtNakagaininCdFr.Name = "txtNakagaininCdFr";
            this.txtNakagaininCdFr.Size = new System.Drawing.Size(49, 20);
            this.txtNakagaininCdFr.TabIndex = 5;
            this.txtNakagaininCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtNakagaininCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtNakagaininCdFr_Validating);
            // 
            // gbxMizuageShisho
            // 
            this.gbxMizuageShisho.Controls.Add(this.txtMizuageShishoCd);
            this.gbxMizuageShisho.Controls.Add(this.lblMizuageShishoNm);
            this.gbxMizuageShisho.Controls.Add(this.lblMizuageShisho);
            this.gbxMizuageShisho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.gbxMizuageShisho.ForeColor = System.Drawing.Color.Black;
            this.gbxMizuageShisho.Location = new System.Drawing.Point(12, 49);
            this.gbxMizuageShisho.Name = "gbxMizuageShisho";
            this.gbxMizuageShisho.Size = new System.Drawing.Size(381, 77);
            this.gbxMizuageShisho.TabIndex = 0;
            this.gbxMizuageShisho.TabStop = false;
            this.gbxMizuageShisho.Text = "水揚支所";
            // 
            // txtMizuageShishoCd
            // 
            this.txtMizuageShishoCd.AutoSizeFromLength = true;
            this.txtMizuageShishoCd.DisplayLength = null;
            this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtMizuageShishoCd.Location = new System.Drawing.Point(76, 33);
            this.txtMizuageShishoCd.MaxLength = 4;
            this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
            this.txtMizuageShishoCd.Size = new System.Drawing.Size(34, 20);
            this.txtMizuageShishoCd.TabIndex = 1;
            this.txtMizuageShishoCd.TabStop = false;
            this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
            // 
            // lblMizuageShishoNm
            // 
            this.lblMizuageShishoNm.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMizuageShishoNm.Location = new System.Drawing.Point(115, 33);
            this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
            this.lblMizuageShishoNm.Size = new System.Drawing.Size(212, 20);
            this.lblMizuageShishoNm.TabIndex = 2;
            this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMizuageShisho
            // 
            this.lblMizuageShisho.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShisho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShisho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblMizuageShisho.Location = new System.Drawing.Point(10, 31);
            this.lblMizuageShisho.Name = "lblMizuageShisho";
            this.lblMizuageShisho.Size = new System.Drawing.Size(322, 25);
            this.lblMizuageShisho.TabIndex = 0;
            this.lblMizuageShisho.Text = "水揚支所";
            this.lblMizuageShisho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // HNDR1051
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 638);
            this.Controls.Add(this.gbxMizuageShisho);
            this.Controls.Add(this.gbxDate);
            this.Controls.Add(this.gbxNakagaininCd);
            this.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "HNDR1051";
            this.Par1 = "11";
            this.Text = "";
            this.Controls.SetChildIndex(this.gbxNakagaininCd, 0);
            this.Controls.SetChildIndex(this.gbxDate, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.gbxMizuageShisho, 0);
            this.pnlDebug.ResumeLayout(false);
            this.gbxDate.ResumeLayout(false);
            this.gbxDate.PerformLayout();
            this.gbxNakagaininCd.ResumeLayout(false);
            this.gbxNakagaininCd.PerformLayout();
            this.gbxMizuageShisho.ResumeLayout(false);
            this.gbxMizuageShisho.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblDateFr;
        private System.Windows.Forms.Label lblDateGengo;
        private jp.co.fsi.common.controls.FsiTextBox txtDateMonth;
        private jp.co.fsi.common.controls.FsiTextBox txtDateYear;
        private jp.co.fsi.common.controls.FsiTextBox txtDateDay;
        private System.Windows.Forms.Label labelDateYear;
        private System.Windows.Forms.Label lblDateMonth;
        private System.Windows.Forms.Label lblDateDay;
        private System.Windows.Forms.GroupBox gbxDate;
        private System.Windows.Forms.GroupBox gbxNakagaininCd;
        private System.Windows.Forms.Label lblNakagaininCdFr;
        private common.controls.FsiTextBox txtNakagaininCdFr;
        private System.Windows.Forms.Label lblNakagaininCdTo;
        private System.Windows.Forms.Label lblNakagaininCdBet;
        private common.controls.FsiTextBox txtNakagaininCdTo;
        private System.Windows.Forms.GroupBox gbxMizuageShisho;
        private common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private System.Windows.Forms.Label lblMizuageShisho;

    }
}