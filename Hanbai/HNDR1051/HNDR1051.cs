﻿using System;
using System.ComponentModel;
using System.Data;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

using GrapeCity.ActiveReports;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;
using jp.co.fsi.common.constants;

namespace jp.co.fsi.hn.hndr1051
{
    /// <summary>
    /// 入金チェックリスト(HNDR1051)
    /// </summary>
    public partial class HNDR1051 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// 印刷テーブル使用列数
        /// </summary>
        public const int prtCols = 13;
        #endregion

        #region プロパティ
        /// <summary>
        /// 画面上最後となるフォーカスのEnterボタン押下時処理用変数
        /// </summary>
        private bool _dtFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._dtFlg;
            }
        }

        private decimal MOTOCHO_KUBUN = 11;
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HNDR1051()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 水揚支所
#if DEBUG
            this.txtMizuageShishoCd.Text = "1";
#else
            this.txtMizuageShishoCd.Text = this.Uinfo.shishoCd;
#endif
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
            this.txtMizuageShishoCd.Enabled = (this.txtMizuageShishoCd.Text == "1") ? true : false;

            string[] jpDate = Util.ConvJpDate(DateTime.Now, this.Dba);
            lblDateGengo.Text = jpDate[0];
            txtDateYear.Text = jpDate[2];
            txtDateMonth.Text = jpDate[3];
            txtDateDay.Text = jpDate[4];

            lblNakagaininCdFr.Text = "先　頭";
            lblNakagaininCdTo.Text = "最　後";

            MOTOCHO_KUBUN = (Util.ToDecimal(this.Par1) == 0) ? MOTOCHO_KUBUN : Util.ToDecimal(this.Par1);

            txtDateYear.Focus();

            // Enter処理を無効化
            this._dtFlg = false;
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 日付,仲買人コード,水揚支所にフォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd":
                case "txtDateYear":
                case "txtNakagaininCdFr":
                case "txtNakagaininCdTo":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;
            String[] result;
            string stCurrentDir = System.IO.Directory.GetCurrentDirectory();

            switch (this.ActiveCtlNm)
            {
                #region 水揚支所
                case "txtMizuageShishoCd": // 水揚支所
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM2031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2031.CMCM2031");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.InData = this.txtMizuageShishoCd.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.txtMizuageShishoCd.Text = result[0];
                                this.lblMizuageShishoNm.Text = result[1];
                            }
                        }
                    }
                    break;
                #endregion

                case "txtDateYear":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblDateGengo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.lblDateGengo.Text = result[1];

                                // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
                                DateTime tmpDate = Util.ConvAdDate(this.lblDateGengo.Text, this.txtDateYear.Text,
                                    this.txtDateMonth.Text, "1", this.Dba);
                                int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);
                                if (Util.ToInt(this.txtDateDay.Text) > lastDayInMonth)
                                {
                                    this.txtDateDay.Text = Util.ToString(lastDayInMonth);
                                }

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblDateGengo.Text,
                                        this.txtDateYear.Text,
                                        this.txtDateMonth.Text,
                                        this.txtDateDay.Text,
                                        this.Dba);
                                this.lblDateGengo.Text = arrJpDate[0];
                                this.txtDateYear.Text = arrJpDate[2];
                                this.txtDateMonth.Text = arrJpDate[3];
                                this.txtDateDay.Text = arrJpDate[4];
                            }
                        }
                    }
                    break;

                case "txtNakagaininCdFr":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom("HNCM1011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.hn.hncm1011.HNCM1011");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtNakagaininCdFr.Text = outData[0];
                                this.lblNakagaininCdFr.Text = outData[1];
                            }
                        }
                    }
                    break;

                case "txtNakagaininCdTo":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom("HNCM1011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.hn.hncm1011.HNCM1011");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtNakagaininCdTo.Text = outData[0];
                                this.lblNakagaininCdTo.Text = outData[1];
                            }
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
            {
                // ﾌﾟﾚﾋﾞｭｰ処理
                DoPrint(true);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("印刷", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }

        /// <summary>
        /// F6キー押下時処理
        /// PDF出力
        /// </summary>
        public override void PressF6()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("PDF出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, true);
            }
        }

        /// <summary>
        /// F7キー押下時処理
        /// EXCEL出力
        /// </summary>
        public override void PressF7()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("EXCEL出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false, false, true);
            }
        }

        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            // 設定画面の起動
            // MEMO:原則としてここで渡す帳票IDの設定はReport.csvに保持していることが前提ですが、
            // 保持していない場合は、設定画面での保存(F6)時に新規に設定が保持されます。
            PrintSettingForm psForm = new PrintSettingForm(new string[1] { "HNDR1051R" });
            psForm.ShowDialog();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 水揚支所入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMizuageShishoCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                e.Cancel = true;
                this.txtMizuageShishoCd.SelectAll();
                this.txtMizuageShishoCd.Focus();
            }
        }

        /// <summary>
        /// 和暦(年)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateYear_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidDateYear())
            {
                e.Cancel = true;
                this.txtDateYear.SelectAll();
            }
            else
            {
                CheckDate();
                SetDate();
            }
        }

        /// <summary>
        /// 和暦(月)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateMonth_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidDateMonth())
            {
                e.Cancel = true;
                this.txtDateMonth.SelectAll();
            }
            else
            {
                CheckDate();
                SetDate();
            }
        }

        /// <summary>
        /// 和暦(日)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateDay_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidDateDay())
            {
                e.Cancel = true;
                this.txtDateDay.SelectAll();
            }
            else
            {
                CheckDate();
                SetDate();
            }
        }

        /// <summary>
        /// 仲買人CD(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtNakagaininCdFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidNakagaininCdFr())
            {
                e.Cancel = true;
                this.txtNakagaininCdFr.SelectAll();
            }
        }

        /// <summary>
        /// 仲買人CD(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtNakagaininCdTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidNakagaininCdTo())
            {
                e.Cancel = true;
                this.txtNakagaininCdTo.SelectAll();

                // Enter処理を無効化
                this._dtFlg = false;
            }
            else
            {
                // Enter処理を有効化
                this._dtFlg = true;
            }
        }

        /// <summary>
        /// 仲買人CD(至)のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtNakagaininCdTo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.Flg)
            {
                // Enter処理を無効化
                this._dtFlg = false;

                // 全項目を再度入力値チェック
                if (!ValidateAll())
                {
                    // エラーありの場合ここで処理終了
                    return;
                }

                if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
                {
                    // ﾌﾟﾚﾋﾞｭｰ処理
                    DoPrint(true);
                }
                else
                {
                    this.txtNakagaininCdTo.Focus();
                }
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 水揚支所の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool IsValidMizuageShishoCd()
        {
            // 空 又は 0入力の場合
            if (ValChk.IsEmpty(this.txtMizuageShishoCd.Text) || Equals(this.txtMizuageShishoCd.Text, "0"))
            {
                // 水揚支所名称を表示する
                this.txtMizuageShishoCd.Text = "0";
                this.lblMizuageShishoNm.Text = "全て";
                return true;
            }
            // 水揚支所名称を表示する
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
            if (ValChk.IsEmpty(this.lblMizuageShishoNm.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }
            return true;
        }

        /// <summary>
        /// 年の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidDateYear()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtDateYear.Text))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }
            // 空の場合、0年として処理
            if (ValChk.IsEmpty(this.txtDateYear.Text))
            {
                this.txtDateYear.Text = "0";
            }

            return true;
        }

        /// <summary>
        /// 月の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidDateMonth()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtDateMonth.Text))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }
            // 空の場合、1月として処理
            if (ValChk.IsEmpty(this.txtDateMonth.Text))
            {
                this.txtDateMonth.Text = "1";
            }
            else
            {
                // 12を超える月が入力された場合、12月として処理
                if (Util.ToInt(this.txtDateMonth.Text) > 12)
                {
                    this.txtDateMonth.Text = "12";
                }
                // 1より小さい月が入力された場合、1月として処理
                else if (Util.ToInt(this.txtDateMonth.Text) < 1)
                {
                    this.txtDateMonth.Text = "1";
                }
            }

            return true;
        }

        /// <summary>
        /// 日(自)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidDateDay()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtDateDay.Text))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }

            if (ValChk.IsEmpty(this.txtDateDay.Text))
            {
                // 空の場合、1日として処理
                this.txtDateDay.Text = "1";
            }
            else
            {
                // 1より小さい日が入力された場合、1日として処理
                if (Util.ToInt(this.txtDateDay.Text) < 1)
                {
                    this.txtDateDay.Text = "1";
                }
            }

            return true;
        }

        /// <summary>
        /// 年月日(自)の月末入力チェック
        /// </summary>
        /// 
        private void CheckDate()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblDateGengo.Text, this.txtDateYear.Text,
                this.txtDateMonth.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDateDay.Text) > lastDayInMonth)
            {
                this.txtDateDay.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(自)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetDate()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDate(Util.FixJpDate(this.lblDateGengo.Text, this.txtDateYear.Text,
                this.txtDateMonth.Text, this.txtDateDay.Text, this.Dba));
        }

        /// <summary>
        /// 仲買人コード(自)の入力チェック
        /// </summary>
        private bool IsValidNakagaininCdFr()
        {
            if (ValChk.IsEmpty(this.txtNakagaininCdFr.Text))
            {
                this.lblNakagaininCdFr.Text = "先　頭";
            }
            else
                // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
                if (!ValChk.IsNumber(this.txtNakagaininCdFr.Text))
                {
                    Msg.Error("仲買人コードは数値のみで入力してください。");
                    return false;
                }
                else
                {
                    // コードを元に名称を取得する
                    this.lblNakagaininCdFr.Text = this.Dba.GetName(this.UInfo, "VI_HN_NAKAGAI", "", this.txtNakagaininCdFr.Text);
                }

            return true;
        }

        /// <summary>
        /// 仲買人コード(至)の入力チェック
        /// </summary>
        private bool IsValidNakagaininCdTo()
        {
            if (ValChk.IsEmpty(this.txtNakagaininCdTo.Text))
            {
                this.lblNakagaininCdTo.Text = "最　後";
            }
            else
                // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
                if (!ValChk.IsNumber(this.txtNakagaininCdTo.Text))
                {
                    Msg.Error("仲買人コードは数値のみで入力してください。");
                    return false;
                }
                else
                {
                    // コードを元に名称を取得する
                    this.lblNakagaininCdTo.Text = this.Dba.GetName(this.UInfo, "VI_HN_NAKAGAI", "", this.txtNakagaininCdTo.Text);
                }

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 水揚支所の入力チェック
            if (!IsValidMizuageShishoCd())
            {
                this.txtMizuageShishoCd.Focus();
                this.txtMizuageShishoCd.SelectAll();
                return false;
            }

            // 年(自)のチェック
            if (!IsValidDateYear())
            {
                this.txtDateYear.Focus();
                this.txtDateYear.SelectAll();
                return false;
            }
            // 月(自)のチェック
            if (!IsValidDateMonth())
            {
                this.txtDateMonth.Focus();
                this.txtDateMonth.SelectAll();
                return false;
            }
            // 日(自)のチェック
            if (!IsValidDateDay())
            {
                this.txtDateDay.Focus();
                this.txtDateDay.SelectAll();
                return false;
            }
            // 年月日(自)の月末入力チェック処理
            CheckDate();
            // 年月日(自)の正しい和暦への変換処理
            SetDate();

            // 仲買人コード(自)の入力チェック
            if (!IsValidNakagaininCdFr())
            {
                this.txtNakagaininCdFr.Focus();
                this.txtNakagaininCdFr.SelectAll();
                return false;
            }
            // 仲買人コード(至)の入力チェック
            if (!IsValidNakagaininCdTo())
            {
                this.txtNakagaininCdTo.Focus();
                this.txtNakagaininCdTo.SelectAll();
                return false;
            }

            return true;
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpDate(string[] arrJpDate)
        {
            this.lblDateGengo.Text = arrJpDate[0];
            this.txtDateYear.Text = arrJpDate[2];
            this.txtDateMonth.Text = arrJpDate[3];
            this.txtDateDay.Text = arrJpDate[4];
        }

        /// <summary>
        /// 検索サブウィンドウオープン
        /// </summary>
        /// <param name="moduleName">例："COMC8111"</param>
        /// <param name="para1">例："TB_HN_COMBO_DATA_MIZUAGE"</param>
        /// <param name="textBoxOfCode">例：this.txtGyogyoushuCd.Text</param>
        /// <param name="indata">例： this.txtGyogyoushuCd.Text</param>
        /// <returns>String[0]：検索結果から選択したコード , String[1]：検索結果から選択した名称</returns>
        private String[] openSearchWindow(String moduleName, String para1, String indata)
        {
            string[] result = { "", "" };

            // ネームスペースに使うモジュール名の小文字
            string lowerModuleName = moduleName.ToLower();

            // ネームスペースの末尾
            string nameSpace = lowerModuleName.Substring(0, 3);

            // アセンブリのロード
            Assembly asm = Assembly.LoadFrom(moduleName + ".exe");

            // フォーム作成
            string moduleNameSpace = "jp.co.fsi." + nameSpace + "." + lowerModuleName + "." + moduleName;
            Type t = asm.GetType(moduleNameSpace);

            if (t != null)
            {
                Object obj = Activator.CreateInstance(t);
                if (obj != null)
                {
                    BasePgForm frm = (BasePgForm)obj;
                    frm.Par1 = para1;
                    frm.InData = indata;
                    frm.ShowDialog(this);

                    if (frm.DialogResult == DialogResult.OK)
                    {
                        string[] ret = (string[])frm.OutData;
                        result[0] = ret[0];
                        result[1] = ret[1];
                        return result;
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// 帳票を印刷する。
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview, bool isPdf = false, bool isExcel = false, bool isCsv = false)
        {
            try
            {
#if DEBUG
                this.Dba.DeleteWork("PR_HN_TBL", this.UnqId);
#endif
                this.Dba.BeginTransaction();

                // 帳票出力用にワークテーブルにデータを作成
                bool dataFlag = MakeWkData();
                
                // 帳票出力
                if (dataFlag)
                {
                    // 取得列の定義
                    StringBuilder cols = Util.ColsArray(prtCols, "");

                    // バインドパラメータの設定
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);

                    // データの取得
                    DataTable dtOutput = this.Dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_HN_TBL", "GUID = @GUID", "SORT ASC", dpc);

                    // 帳票オブジェクトをインスタンス化
                    HNDR1051R rpt = new HNDR1051R(dtOutput);
                    rpt.Document.Name = this.lblTitle.Text;
                    rpt.Document.Printer.DocumentName = this.lblTitle.Text;

                    if (isExcel)
                    {
                        GrapeCity.ActiveReports.Export.Excel.Section.XlsExport xlsExport1 = new GrapeCity.ActiveReports.Export.Excel.Section.XlsExport();
                        //SetExcelSetting(xlsExport1);
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Han, rpt.Document.Name, 2);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            xlsExport1.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("EXCEL出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPdf)
                    {
                        GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport p = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Han, rpt.Document.Name, 1);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            p.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("PDF出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPreview)
                    {
                        PreviewForm pFrm = new PreviewForm(rpt, this.UnqId);
                        // プレビュー画面表示
                        pFrm.WindowState = FormWindowState.Maximized;
                        pFrm.Show();
                    }
                    else
                    {
                        // 直接印刷
                        rpt.Run(false);
                        rpt.Document.Print(true, true, false);
                    }
                }
            }
            finally
            {
#if DEBUG
                this.Dba.Commit();
#else
                this.Dba.Rollback();
#endif
            }
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData()
        {
            DbParamCollection dpc = new DbParamCollection();
            // 日付を西暦にして取得
            DateTime tmpDate = Util.ConvAdDate(this.lblDateGengo.Text, this.txtDateYear.Text,
                    this.txtDateMonth.Text, this.txtDateDay.Text, this.Dba);

            int i; // ループ用カウント変数

            // 仲買人コード設定
            string nakagaininCdFr;
            string nakagaininCdTo;
            if (Util.ToDecimal(txtNakagaininCdFr.Text) > 0)
            {
                nakagaininCdFr = txtNakagaininCdFr.Text;
            }
            else
            {
                nakagaininCdFr = "0";
            }
            if (Util.ToDecimal(txtNakagaininCdTo.Text) > 0)
            {
                nakagaininCdTo = txtNakagaininCdTo.Text;
            }
            else
            {
                nakagaininCdTo = "9999";
            }

            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            // 検索する日付をセット
            dpc.SetParam("@DENPYO_DATE", SqlDbType.VarChar, 10, tmpDate.Date.ToString("yyyy/MM/dd"));

            // 検索する精算区分をセット
            dpc.SetParam("@NAKAGAININ_CD_FR", SqlDbType.VarChar, 6, nakagaininCdFr);
            dpc.SetParam("@NAKAGAININ_CD_TO", SqlDbType.VarChar, 6, nakagaininCdTo);
            //支所コードの退避
            string shishoCd = this.txtMizuageShishoCd.Text;
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            dpc.SetParam("@MOTOCHO_KUBUN", SqlDbType.Decimal, 4, MOTOCHO_KUBUN);

            StringBuilder Sql = new StringBuilder();
            // 検索日付に発生しているデータを取得
            Sql.Append("SELECT");
            Sql.Append(" A.SHISHO_CD,");
            Sql.Append(" A.SHISHO_NM,");
            Sql.Append(" A.DENPYO_BANGO,");
            Sql.Append(" A.DENPYO_DATE,");
            Sql.Append(" A.TEKIYO,");
            Sql.Append(" A.TORIHIKISAKI_CD,");
            Sql.Append(" A.TORIHIKISAKI_NM,");
            Sql.Append(" A.AITE_KANJO_KAMOKU_NM,");
            Sql.Append(" A.ZEIKOMI_KINGAKU,");
            Sql.Append(" B.ZANDAKA ");
            Sql.Append("FROM");
            Sql.Append(" VI_HN_NAKAGAININ_NYUKIN_ZM AS A ");
            Sql.Append("LEFT OUTER JOIN ( ");
            Sql.Append("	SELECT ");
            Sql.Append("		A.HOJO_KAMOKU_CD AS TORIHIKISAKI_CD,");
            Sql.Append("		SUM(CASE WHEN A.TAISHAKU_KUBUN = C.TAISHAKU_KUBUN THEN A.ZEIKOMI_KINGAKU ELSE (A.ZEIKOMI_KINGAKU * - 1) END) AS ZANDAKA ");
            Sql.Append("	FROM TB_ZM_SHIWAKE_MEISAI AS A");
            Sql.Append("		INNER JOIN TB_HN_KISHU_ZAN_KAMOKU AS B ");
            Sql.Append("		ON	A.KAISHA_CD = B.KAISHA_CD ");
            Sql.Append("		AND A.SHISHO_CD = B.SHISHO_CD ");
            Sql.Append("		AND A.KANJO_KAMOKU_CD = B.KANJO_KAMOKU_CD ");
            Sql.Append("		AND @MOTOCHO_KUBUN = B.MOTOCHO_KUBUN ");
            Sql.Append("	LEFT OUTER JOIN TB_ZM_KANJO_KAMOKU AS C");
            Sql.Append("		ON	A.KAISHA_CD = C.KAISHA_CD ");
            Sql.Append("		AND A.KAIKEI_NENDO = C.KAIKEI_NENDO");
            Sql.Append("		AND A.KANJO_KAMOKU_CD = C.KANJO_KAMOKU_CD ");
            Sql.Append("	LEFT OUTER JOIN VI_HN_TORIHIKISAKI_JOHO D");
            Sql.Append("		ON	A.KAISHA_CD = D.KAISHA_CD");
            Sql.Append("		AND A.HOJO_KAMOKU_CD = D.TORIHIKISAKI_CD");
            Sql.Append("	WHERE ");
            Sql.Append("		A.KAISHA_CD = @KAISHA_CD AND ");
            Sql.Append("		A.SHISHO_CD = @SHISHO_CD AND ");
            Sql.Append("		A.KAIKEI_NENDO = @KAIKEI_NENDO AND ");
            Sql.Append("		A.HOJO_KAMOKU_CD BETWEEN @NAKAGAININ_CD_FR AND @NAKAGAININ_CD_TO AND ");
            Sql.Append("		D.TORIHIKISAKI_KUBUN2 = 2  AND ");
            Sql.Append("		A.DENPYO_DATE <= @DENPYO_DATE ");
            Sql.Append("	GROUP BY A.HOJO_KAMOKU_CD");
            Sql.Append(" ) AS B ");
            Sql.Append("ON");
            Sql.Append(" A.TORIHIKISAKI_CD = B.TORIHIKISAKI_CD ");
            Sql.Append("WHERE");
            Sql.Append(" A.KAIKEI_NENDO = @KAIKEI_NENDO AND");
            Sql.Append(" A.KAISHA_CD = @KAISHA_CD AND");
            Sql.Append(" A.MOTOCHO_KUBUN = @MOTOCHO_KUBUN AND");
            // 支所コードが指定されていなければ条件に含めない
            if (shishoCd != "0")
            {
                Sql.Append(" A.SHISHO_CD = @SHISHO_CD AND");
            }
            Sql.Append(" A.DENPYO_DATE = @DENPYO_DATE AND");
            Sql.Append(" A.TORIHIKISAKI_CD BETWEEN @NAKAGAININ_CD_FR AND @NAKAGAININ_CD_TO ");
            Sql.Append("ORDER BY");
            Sql.Append(" A.KAISHA_CD,");
            Sql.Append(" A.SHISHO_CD,");
            Sql.Append(" A.DENPYO_BANGO,");
            Sql.Append(" A.GYO_BANGO");

            DataTable dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            if (dtMainLoop.Rows.Count == 0)
            {
                Msg.Info("該当データがありません。");
                return false;
            }
            else
            {
                dpc = new DbParamCollection();
                i = 1;
                Decimal genkinGokei = 0; // 現金合計
                Decimal furikomiGokei = 0; // 振込合計
                string nyukinKubunNm; // 入金区分名称

                #region 印刷ワークテーブルに登録
                foreach (DataRow dr in dtMainLoop.Rows)
                {
                    Sql = new StringBuilder();
                    dpc = new DbParamCollection();
                    Sql.Append("INSERT INTO PR_HN_TBL(");
                    Sql.Append("  GUID");
                    Sql.Append(" ,SORT");
                    Sql.Append(" ," + Util.ColsArray(prtCols, ""));
                    Sql.Append(") ");
                    Sql.Append("VALUES(");
                    Sql.Append("  @GUID");
                    Sql.Append(" ,@SORT");
                    Sql.Append(" ," + Util.ColsArray(prtCols, "@"));
                    Sql.Append(") ");

                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, i);
                    // ページヘッダーデータを設定
                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);
                    // データを設定
                    if (Util.ToString(dr["AITE_KANJO_KAMOKU_NM"]) == "現金")
                    {
                        nyukinKubunNm = "現金";
                        genkinGokei = Util.ToDecimal(dr["ZEIKOMI_KINGAKU"]);
                        furikomiGokei = 0;
                    }
                    else
                    {
                        nyukinKubunNm = "振込";
                        genkinGokei = 0;
                        furikomiGokei = Util.ToDecimal(dr["ZEIKOMI_KINGAKU"]);
                    }

                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dr["DENPYO_BANGO"]);
                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dr["DENPYO_DATE"]);
                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dr["TORIHIKISAKI_CD"]);
                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, dr["TORIHIKISAKI_NM"]);
                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, nyukinKubunNm);
                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, Util.FormatNum(dr["ZEIKOMI_KINGAKU"]));
                    dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, Util.FormatNum(dr["ZANDAKA"]));
                    dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, dr["TEKIYO"]);
                    dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, genkinGokei);
                    dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, furikomiGokei);
                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, Util.ToString(dr["SHISHO_CD"]));
                    dpc.SetParam("@ITEM13", SqlDbType.VarChar, 200, Util.ToString(dr["SHISHO_NM"]));

                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                    i++;
                }
                #endregion
            }

            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
                "SORT",
                "PR_HN_TBL",
                "GUID = @GUID",
                dpc);

            bool dataFlag;
            if (tmpdtPR_HN_TBL.Rows.Count > 0)
            {
                dataFlag = true;
            }
            else
            {
                dataFlag = false;
            }

            return dataFlag;
        }
        #endregion

    }
}
