﻿using System;
using System.Drawing;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

using jp.co.fsi.common.report;
using jp.co.fsi.common.util;


namespace jp.co.fsi.han.hanr2101
{
    /// <summary>
    /// HANR2101R の概要の説明です。
    /// </summary>
    public partial class HANR2101R : BaseReport
    {

        public HANR2101R(DataTable tgtData)
            : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
            }

        private void groupFooter2_Format(object sender, EventArgs e)
        {
            this.textBox71.Text = Util.FormatNum(Util.ToDecimal(this.textBox72.Value) / Util.ToDecimal(this.textBox70.Value));
        }

        private void groupFooter1_Format(object sender, EventArgs e)
        {
            this.textBox34.Text = Util.FormatNum(Util.ToDecimal(this.textBox35.Value) / Util.ToDecimal(this.textBox20.Value));
        }

        private void pageHeader_Format(object sender, EventArgs e)
        {
            txtToday.Text = DateTime.Now.ToString("yyyy/MM/dd");
        }
    }
}