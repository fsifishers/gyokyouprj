﻿using System;
using System.Collections;
using System.Data;
using System.Text;

using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.userinfo;
using jp.co.fsi.common.util;

namespace jp.co.fsi.hn.hnsb1021
{
    /// <summary>
    /// モジュール全体で使用するデータアクセスクラスです。
    /// </summary>
    public class HNSB1021DA
    {
        #region 定数
        ///// <summary>
        ///// 勘定科目コード(販売雑費用)
        ///// </summary>
        //private const int KJ_KMK_CD_ZAT_HIY = 724;

        ///// <summary>
        ///// 勘定科目コード(販売雑収益)
        ///// </summary>
        //private const int KJ_KMK_CD_ZAT_SEK = 623;
        #endregion

        #region private変数
        /// <summary>
        /// ユーザー情報
        /// </summary>
        UserInfo _uInfo;

        /// <summary>
        /// データアクセスオブジェクト
        /// </summary>
        DbAccess _dba;

        /// <summary>
        /// 設定ファイルアクセスオブジェクト
        /// </summary>
        ConfigLoader _config;

        /// <summary>
        /// 支所コード
        /// </summary>
        int ShishoCode;
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="uInfo">操作中のユーザーの情報</param>
        /// <param name="dba">呼び出し元で保持するデータアクセスオブジェクト</param>
        /// <param name="config">呼び出し元で保持する設定ファイルアクセスオブジェクト</param>
        public HNSB1021DA(UserInfo uInfo, DbAccess dba, ConfigLoader config)
        {
            this._uInfo = uInfo;
            this._dba = dba;
            this._config = config;

            this.ShishoCode = Util.ToInt(this._uInfo.ShishoCd);
        }
        #endregion

        #region publicメソッド
        /// <summary>
        /// 仕訳対象データを取得(セリ)
        /// </summary>
        /// <param name="mode">処理モード(1:新規、2:更新)</param>
        /// <param name="condition">条件画面の入力値</param>
        /// <returns>仕訳対象データ</returns>
        public DataTable GetSeriSwkTgtData(int mode, Hashtable condition)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT");
            sql.Append(" A.DENPYO_BANGO AS 伝票番号,");
            sql.Append(" A.KEIJO_NENGAPPI AS 計上年月日,");
            sql.Append(" A.SENSHU_CD AS 船主CD,");
            sql.Append(" B.NAKAGAININ_CD AS 仲買人CD,");
            sql.Append(" A.TORIHIKI_KUBUN1 AS 取引区分１,");
            sql.Append(" A.TORIHIKI_KUBUN2 AS 取引区分２,");
            sql.Append(" MIN(A.SENSHU_NM) AS 船主名称,");
            sql.Append(" MIN(B.NAKAGAININ_NM) AS 仲買人名称,");
            sql.Append(" B.BUMON_CD AS 部門CD,");
            sql.Append(" B.SHIWAKE_CD AS 仕訳CD,");
            sql.Append(" 0 AS 税区分,");
            sql.Append(" B.JIGYO_KUBUN AS 事業区分,");
            sql.Append(" MIN(C.SHOHIZEI_NYURYOKU_HOHO) AS 消費税入力方法,");
            sql.Append(" MIN(C.SHOHIZEI_TENKA_HOHO) AS 消費税転嫁方法,");
            sql.Append(" MIN(C.SHOHIZEI_HASU_SHORI) AS 消費税端数処理,");

            //sql.Append(" SUM(CASE");
            //sql.Append("  WHEN C.SHOHIZEI_TENKA_HOHO = 3 THEN");
            //sql.Append("   CASE");
            //sql.Append("    WHEN C.SHOHIZEI_NYURYOKU_HOHO = 1 THEN (B.KINGAKU + round((B.KINGAKU * (B.ZEI_RITSU/100)), 0))");
            //sql.Append("    ELSE CASE WHEN A.TORIHIKI_KUBUN1 = 1 THEN (B.KINGAKU + round((B.KINGAKU * (B.ZEI_RITSU/100)), 0)) ELSE 0 END");
            //sql.Append("   END");
            //sql.Append("  ELSE");
            //sql.Append("   CASE WHEN C.SHOHIZEI_NYURYOKU_HOHO = 3 THEN (B.KINGAKU - B.SHOHIZEI)");
            //sql.Append("   ELSE B.KINGAKU");
            //sql.Append("   END");
            //sql.Append("  END) AS 売上金額,");
            // 消費税振分使用（消費税振分未対応の場合は以下を使用）
            //sql.Append(" (CASE WHEN MIN(C.SHOHIZEI_NYURYOKU_HOHO) = 3 THEN ");
            //sql.Append("      CASE WHEN MIN(B.GYO_NO) = 1 THEN MIN(A.MIZUAGE_ZEIKOMI_KINGAKU - A.MIZUAGE_SHOHIZEIGAKU) ELSE 0 END ");
            //sql.Append("   ELSE ");
            //sql.Append("      CASE WHEN MIN(B.GYO_NO) = 1 THEN MIN(A.MIZUAGE_ZEINUKI_KINGAKU) ELSE 0 END ");
            //sql.Append("   END) AS 売上金額,");
            sql.Append("  SUM(CASE WHEN C.SHOHIZEI_TENKA_HOHO = 3 THEN ");
            sql.Append("                CASE WHEN B.SHOHIZEI_NYURYOKU_HOHO = 1 THEN B.KINGAKU ");
            sql.Append("                     ELSE CASE WHEN A.TORIHIKI_KUBUN1 = 2 THEN B.KINGAKU ");
            sql.Append("                               ELSE 0 ");
            sql.Append("                          END ");
            sql.Append("                END ");
            sql.Append("           ELSE CASE WHEN B.SHOHIZEI_NYURYOKU_HOHO = 3 THEN (B.KINGAKU - B.SHOHIZEI_FURIWAKE) ");
            sql.Append("                     ELSE B.KINGAKU ");
            sql.Append("                END ");
            sql.Append("      END) AS 売上金額,");

            sql.Append(" SUM(CASE");
            sql.Append("  WHEN C.SHOHIZEI_TENKA_HOHO = 3 THEN");
            sql.Append("   CASE WHEN A.TORIHIKI_KUBUN1 = 1 THEN 0 ELSE 0 END");
            sql.Append("  ELSE 0");
            sql.Append("  END) AS 消費税,");
            sql.Append(" SUM(CASE");
            sql.Append("  WHEN C.SHOHIZEI_TENKA_HOHO = 3 THEN");
            sql.Append("   CASE WHEN C.SHOHIZEI_NYURYOKU_HOHO = 2 THEN");
            sql.Append("    CASE WHEN A.TORIHIKI_KUBUN1 = 1 THEN 0 ELSE (B.KINGAKU + B.SHOHIZEI) END");
            sql.Append("   ELSE 0 END");
            sql.Append("  ELSE 0");
            sql.Append("  END) AS 外税売上金額,");
            sql.Append(" SUM(CASE");
            sql.Append("  WHEN C.SHOHIZEI_TENKA_HOHO = 3 THEN");
            sql.Append("   CASE WHEN C.SHOHIZEI_NYURYOKU_HOHO = 3 THEN ");
            sql.Append("    CASE WHEN A.TORIHIKI_KUBUN1 = 1 THEN 0 ELSE (B.KINGAKU + B.SHOHIZEI) END");
            sql.Append("    ELSE 0 END ");
            sql.Append("  ELSE 0 ");
            sql.Append("  END) AS 内税売上金額,");
            sql.Append(" SUM(B.SHOHINDAI) AS 紙箱代 ");
            sql.Append(" ,SUM(B.SHOHINDAI_SHOHIZEI) AS 紙箱消費税 ");
            // 消費税振分使用（消費税振分未対応の場合は以下を使用）
            //sql.Append(" ,(CASE WHEN MIN(B.GYO_NO) = 1 THEN A.MIZUAGE_SHOHIZEIGAKU ELSE 0 END) AS 水揚消費税額 ");
            sql.Append(" ,SUM(CASE WHEN C.SHOHIZEI_TENKA_HOHO = 3 THEN ");
            sql.Append("                CASE WHEN A.TORIHIKI_KUBUN1 = 2 THEN B.SHOHIZEI_FURIWAKE ");
            sql.Append("                     ELSE 0 ");
            sql.Append("                END ");
            sql.Append("           ELSE B.SHOHIZEI_FURIWAKE ");
            sql.Append("      END) AS 水揚消費税額 ");
            sql.Append(" ,B.ZEI_RITSU AS 税率 ");
            sql.Append("FROM");
            sql.Append(" TB_HN_SHIKIRI_DATA AS A ");
            sql.Append("LEFT OUTER JOIN");
            sql.Append(" TB_HN_SHIKIRI_MEISAI AS B ");
            sql.Append("ON");
            sql.Append(" A.KAISHA_CD = B.KAISHA_CD AND");
            sql.Append(" A.SHISHO_CD = B.SHISHO_CD AND");
            sql.Append(" A.KAIKEI_NENDO = B.KAIKEI_NENDO AND");
            sql.Append(" A.DENPYO_KUBUN = B.DENPYO_KUBUN AND");
            sql.Append(" A.DENPYO_BANGO = B.DENPYO_BANGO ");
            sql.Append("LEFT OUTER JOIN");
            sql.Append(" VI_HN_TORIHIKISAKI_JOHO AS C ");
            sql.Append("ON");
            sql.Append(" A.KAISHA_CD = C.KAISHA_CD AND");
            sql.Append(" A.SENSHU_CD = C.TORIHIKISAKI_CD AND");
            sql.Append(" C.TORIHIKISAKI_KUBUN1 = 1 ");
            sql.Append("WHERE");
            sql.Append(" A.KAISHA_CD = @KAISHA_CD AND");
            sql.Append(" A.SHISHO_CD = @SHISHO_CD AND");
            sql.Append(" A.KAIKEI_NENDO = @KAIKEI_NENDO AND");
            sql.Append(" A.KEIJO_NENGAPPI BETWEEN @KEIJO_NENGAPPI_FR AND @KEIJO_NENGAPPI_TO AND");
            sql.Append(" A.SENSHU_CD BETWEEN @FUNANUSHI_CD_FR AND @FUNANUSHI_CD_TO AND");
            sql.Append(" A.SEISAN_KUBUN = 3 AND");
            if (mode == 1)
            {
                sql.Append(" ISNULL(A.IKKATSU_DENPYO_BANGO, 0) = 0 AND");
            }
            sql.Append(" A.DENPYO_KUBUN = 3 AND");
            sql.Append(" B.KINGAKU <> 0 AND");
            sql.Append(" A.SHIWAKE_DENPYO_BANGO = 0 AND");
            sql.Append(" B.SHIWAKE_CD <> 0 ");
            sql.Append("GROUP BY");
            sql.Append(" A.DENPYO_BANGO,");
            sql.Append(" A.KEIJO_NENGAPPI,");
            sql.Append(" A.SENSHU_CD,");
            sql.Append(" B.NAKAGAININ_CD,");
            sql.Append(" A.TORIHIKI_KUBUN1,");
            sql.Append(" A.TORIHIKI_KUBUN2,");
            sql.Append(" B.BUMON_CD,");
            sql.Append(" B.SHIWAKE_CD,");
            sql.Append(" B.ZEI_KUBUN,");
            sql.Append(" B.JIGYO_KUBUN,");
            // 消費税振分使用（消費税振分未対応の場合は以下を使用）
            //sql.Append(" A.MIZUAGE_SHOHIZEIGAKU,");
            sql.Append(" B.ZEI_RITSU ");
            sql.Append("ORDER BY");
            sql.Append(" A.SENSHU_CD,");
            sql.Append(" B.NAKAGAININ_CD,");
            sql.Append(" A.TORIHIKI_KUBUN1,");
            sql.Append(" A.TORIHIKI_KUBUN2,");
            sql.Append(" A.KEIJO_NENGAPPI");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
            dpc.SetParam("@KEIJO_NENGAPPI_FR", SqlDbType.DateTime, Util.ToDate(condition["SeisanDtFr"]));
            dpc.SetParam("@KEIJO_NENGAPPI_TO", SqlDbType.DateTime, Util.ToDate(condition["SeisanDtTo"]));
            dpc.SetParam("@FUNANUSHI_CD_FR", SqlDbType.VarChar, 4, Util.ToString(condition["FunanushiCdFr"]));
            dpc.SetParam("@FUNANUSHI_CD_TO", SqlDbType.VarChar, 4, Util.ToString(condition["FunanushiCdTo"]));

            DataTable dtResult = this._dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            return dtResult;
        }

        /// <summary>
        /// 仕訳データを作成(セリ)
        /// </summary>
        /// <param name="mode">モード(1:登録、2:更新、3:削除)</param>
        /// <param name="packDpyNo">一括伝票番号</param>
        /// <param name="condition">条件画面で入力された条件</param>
        /// <param name="dtZeiSetting">消費税設定情報</param>
        /// <returns>更新の成否(true:成功、false:失敗)</returns>
        /// <remarks>
        /// コミットやロールバックは呼び出し元で
        /// 例外発生時は親クラスで捕捉
        /// </remarks>
        public bool MakeSeriSwkData(int mode, int packDpyNo,
            Hashtable condition, ref bool dataExists)
        {
            try
            {
                DataTable dtDenpyo; // 各伝票のデータを格納するDataTable
                int swkDpyNo = 0;   // 仕訳伝票番号
                DataTable dtTemp;   // 汎用的にテンポラリとして使うDataTable

                int denpyoKbn = Util.ToInt(((HNSB1021.SKbn)condition["SakuseiKbn"]).ToString("D"));

                // 自動仕訳設定Aを取得
                DataTable dtJdSwkStgA = GetSeriTB_HN_ZIDO_SHIWAKE_SETTEI_A(Util.ToDate(condition["SwkDpyDt"]));

                // 自動仕訳設定Bを取得
                DataTable dtJdSwkStgB = GetSeriTB_HN_ZIDO_SHIWAKE_SETTEI_B(Util.ToDate(condition["SwkDpyDt"]));

                // 対象データを取得
                DataTable dtTgtData = GetSeriSwkTgtData(mode, condition);
                // データなしの場合終了
                if (dtTgtData.Rows.Count == 0)
                {
                    dataExists = false;
                    return false;
                }
                else
                {
                    dataExists = true;
                }

                // 貸借のデータを作成する
                DataSet dsSwkData = GetSeriTaishakuData(condition, dtJdSwkStgA, dtJdSwkStgB, dtTgtData);

                // 自動仕訳履歴から仕訳伝票番号を取得する
                DataTable dtDpyNo = GetChkDpyNo(packDpyNo, denpyoKbn);

                if (mode == 1)
                {
                    // (新規モードの場合のみ)仕訳伝票番号を取得する
                    packDpyNo = this._dba.GetHNDenpyoNo(this._uInfo, this.ShishoCode, 20, 1);

                    // (新規モードの場合のみ)自動仕訳履歴の存在チェックをする
                    if (Util.ToInt(GetJidoSwkRirekiExistChk(packDpyNo, denpyoKbn).Rows[0]["件数"]) > 0)
                    {
                        // 更新失敗
                        return false;
                    }

                    // (新規モードの場合のみ)伝票番号のUPDATE
                    this._dba.UpdateHNDenpyoNo(this._uInfo, this.ShishoCode, 20, 1, packDpyNo);
                }

                // 仕訳伝票のDELETE
                DeleteTB_ZM_SHIWAKE_DENPYO(packDpyNo, denpyoKbn);

                // 仕訳明細のDELETE
                DeleteTB_ZM_SHIWAKE_MEISAI(packDpyNo, denpyoKbn);

                // 自動仕訳履歴のDELETE
                DeleteTB_HN_ZIDO_SHIWAKE_RIREKI(packDpyNo, denpyoKbn);

                // 仕切データへの取消処理
                UpdateCancelTB_HN_SHIKIRI_DATA(packDpyNo);

                if (mode != 3)
                {
                    // [伝票の件数分]↓
                    for (int i = 0; i < dsSwkData.Tables.Count; i++)
                    {
                        dtDenpyo = dsSwkData.Tables[i];

                        // 新規に採番する必要があるときのみ伝票番号のMAX値を取得
                        if ((i + 1) > dtDpyNo.Rows.Count)
                        {
                            dtTemp = GetSaisyuDenpyoNo();
                            swkDpyNo = Util.ToInt(dtTemp.Rows[0]["最終伝票番号"]) + 1;
                        }
                        else
                        {
                            swkDpyNo = Util.ToInt(dtDpyNo.Rows[i]["SHIWAKE_DENPYO_BANGO"]);
                        }

                        // 仕訳伝票の存在チェック
                        if (GetChkShiwakeDenpyo(swkDpyNo).Rows.Count > 0)
                        {
                            // 更新失敗
                            return false;
                        }

                        // 仕訳伝票の登録
                        InsertTB_ZM_SHIWAKE_DENPYO(swkDpyNo, condition);

                        // [明細(貸借それぞれ、課税対象の仕訳は消費税も)の件数分]↓
                        for (int j = 0; j < dtDenpyo.Rows.Count; j++)
                        {
                            // 明細行の存在チェック
                            if (GetChkShiwakeMeisai(swkDpyNo, dtDenpyo.Rows[j]).Rows.Count > 0)
                            {
                                // 更新失敗
                                return false;
                            }

                            // 明細行の登録
                            InsertTB_ZM_SHIWAKE_MEISAI(swkDpyNo, condition, dtDenpyo.Rows[j]);
                        }
                        // [明細(貸借それぞれ、課税対象の仕訳は消費税も)の件数分]↑

                        // 自動仕訳履歴の存在チェック
                        if (GetChkJidoSwkRireki(packDpyNo, swkDpyNo, denpyoKbn).Rows.Count > 0)
                        {
                            // 更新失敗
                            return false;
                        }

                        // 自動仕訳履歴の登録
                        InsertTB_HN_ZIDO_SHIWAKE_RIREKI(condition, packDpyNo, swkDpyNo, denpyoKbn);
                    }
                    // [伝票の件数分]↑

                    // 財務の伝票番号テーブルへの更新
                    this._dba.UpdateZMDenpyoNo(this._uInfo, this.ShishoCode, 0, swkDpyNo);

                    // 仕切データに一括伝票番号を更新
                    UpdateShikiriData(packDpyNo, condition);
                }

                return true;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// 仕訳データを作成(控除)
        /// </summary>
        /// <param name="mode">モード(1:登録、2:更新、3:削除)</param>
        /// <param name="packDpyNo">一括伝票番号</param>
        /// <param name="condition">条件画面で入力された条件</param>
        /// <param name="dtZeiSetting">消費税設定情報</param>
        /// <returns>更新の成否(true:成功、false:失敗)</returns>
        /// <remarks>
        /// コミットやロールバックは呼び出し元で
        /// 例外発生時は親クラスで捕捉
        /// </remarks>
        public bool MakeKojoSwkData(int mode, int packDpyNo,
            Hashtable condition, DataTable zeiSetting, ref bool dataExists)
        {
            try
            {
                DataTable dtDenpyo; // 各伝票のデータを格納するDataTable
                int swkDpyNo = 0;   // 仕訳伝票番号
                DataTable dtTemp;   // 汎用的にテンポラリとして使うDataTable

                int denpyoKbn = Util.ToInt(((HNSB1021.SKbn)condition["SakuseiKbn"]).ToString("D"));

                // 自動仕訳設定Ａを取得
                DataTable dtJdSwkStgA = GetKojoTB_HN_ZIDO_SHIWAKE_SETTEI_A(Util.ToDate(condition["SwkDpyDt"]));

                // 控除項目設定&自動仕訳設定Ｂを取得
                DataTable dtJdSwkStgB = GetKojoTB_HN_ZIDO_SHIWAKE_SETTEI_B(Util.ToDate(condition["SwkDpyDt"]));

                // 対象データを取得
                DataTable dtTgtData = GetKojoSwkTgtData(mode, condition);
                // データなしの場合終了
                if (dtTgtData.Rows.Count == 0)
                {
                    dataExists = false;
                    return false;
                }
                else
                {
                    dataExists = true;
                }

                // 貸借のデータを作成する
                DataSet dsSwkData = GetKojoTaishakuData(condition, dtJdSwkStgA, dtJdSwkStgB, zeiSetting, dtTgtData);

                // 自動仕訳履歴から仕訳伝票番号を取得する
                DataTable dtDpyNo = GetChkDpyNo(packDpyNo, denpyoKbn);

                if (mode == 1)
                {
                    // (新規モードの場合のみ)仕訳伝票番号を取得する
                    packDpyNo = this._dba.GetHNDenpyoNo(this._uInfo, this.ShishoCode, 20, 1);

                    // (新規モードの場合のみ)自動仕訳履歴の存在チェックをする
                    if (Util.ToInt(GetJidoSwkRirekiExistChk(packDpyNo, denpyoKbn).Rows[0]["件数"]) > 0)
                    {
                        // 更新失敗
                        return false;
                    }

                    // (新規モードの場合のみ)伝票番号のUPDATE
                    this._dba.UpdateHNDenpyoNo(this._uInfo, this.ShishoCode, 20, 1, packDpyNo);
                }

                // 仕訳伝票のDELETE
                DeleteTB_ZM_SHIWAKE_DENPYO(packDpyNo, denpyoKbn);

                // 仕訳明細のDELETE
                DeleteTB_ZM_SHIWAKE_MEISAI(packDpyNo, denpyoKbn);

                // 自動仕訳履歴のDELETE
                DeleteTB_HN_ZIDO_SHIWAKE_RIREKI(packDpyNo, denpyoKbn);

                // 控除への取消処理
                UpdateCancelTB_HN_KOJO_DATA(packDpyNo);

                if (mode != 3)
                {
                    // [伝票の件数分]↓
                    for (int i = 0; i < dsSwkData.Tables.Count; i++)
                    {
                        dtDenpyo = dsSwkData.Tables[i];

                        // 新規に採番する必要があるときのみ伝票番号のMAX値を取得
                        if ((i + 1) > dtDpyNo.Rows.Count)
                        {
                            dtTemp = GetSaisyuDenpyoNo();
                            swkDpyNo = Util.ToInt(dtTemp.Rows[0]["最終伝票番号"]) + 1;
                        }
                        else
                        {
                            swkDpyNo = Util.ToInt(dtDpyNo.Rows[i]["SHIWAKE_DENPYO_BANGO"]);
                        }

                        // 仕訳伝票の存在チェック
                        if (GetChkShiwakeDenpyo(swkDpyNo).Rows.Count > 0)
                        {
                            // 更新失敗
                            return false;
                        }

                        // 仕訳伝票の登録
                        InsertTB_ZM_SHIWAKE_DENPYO(swkDpyNo, condition);

                        // [明細(貸借それぞれ、課税対象の仕訳は消費税も)の件数分]↓
                        for (int j = 0; j < dtDenpyo.Rows.Count; j++)
                        {
                            // 明細行の存在チェック
                            if (GetChkShiwakeMeisai(swkDpyNo, dtDenpyo.Rows[j]).Rows.Count > 0)
                            {
                                // 更新失敗
                                return false;
                            }

                            // 明細行の登録
                            InsertTB_ZM_SHIWAKE_MEISAI(swkDpyNo, condition, dtDenpyo.Rows[j]);
                        }
                        // [明細(貸借それぞれ、課税対象の仕訳は消費税も)の件数分]↑

                        // 自動仕訳履歴の存在チェック
                        if (GetChkJidoSwkRireki(packDpyNo, swkDpyNo, denpyoKbn).Rows.Count > 0)
                        {
                            // 更新失敗
                            return false;
                        }

                        // 自動仕訳履歴の登録
                        InsertTB_HN_ZIDO_SHIWAKE_RIREKI(condition, packDpyNo, swkDpyNo, denpyoKbn);
                    }
                    // [伝票の件数分]↑

                    // 財務の伝票番号テーブルへの更新
                    this._dba.UpdateZMDenpyoNo(this._uInfo, this.ShishoCode, 0, swkDpyNo);

                    // 控除データに一括伝票番号を更新
                    UpdateKojoData(packDpyNo, condition);
                }

                return true;
            }
            catch (Exception e)
            {
                throw e;
            }
        }


        /// <summary>
        /// 仕訳対象データを取得(支払)
        /// </summary>
        /// <param name="mode">処理モード(1:新規、2:更新)</param>
        /// <param name="condition">条件画面の入力値</param>
        /// <returns>仕訳対象データ</returns>
        public DataTable GetShiharaiSwkTgtData(int mode, Hashtable condition)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("  (SELECT");
            sql.Append("    0 AS SiHaNo,");
            sql.Append("    A.SENSHU_CD,");
            sql.Append("    MAX(B.TORIHIKISAKI_NM) AS FUNANUSHI_NM,");
            sql.Append("    SUM(A.SASHIHIKI_SEISANGAKU + A.KOJO_KOMOKU4) AS KINGAKU ");
            sql.Append("FROM");
            sql.Append("    TB_HN_KOJO_DATA AS A ");
            sql.Append("LEFT OUTER JOIN TB_CM_TORIHIKISAKI AS B   ON A.KAISHA_CD = B.KAISHA_CD   AND A.SENSHU_CD = B.TORIHIKISAKI_CD ");
            sql.Append("WHERE");
            sql.Append("    A.KAISHA_CD = @KAISHA_CD AND");
            sql.Append("    A.SHISHO_CD = @SHISHO_CD AND");
            sql.Append("    A.KAIKEI_NENDO = @KAIKEI_NENDO AND");
            sql.Append("    B.SHIHARAI_KUBUN = 1 AND");
            sql.Append("    A.SEISAN_BI BETWEEN @KEIJO_NENGAPPI_FR AND @KEIJO_NENGAPPI_TO AND");
            sql.Append("    A.SENSHU_CD BETWEEN @FUNANUSHI_CD_FR AND @FUNANUSHI_CD_TO AND");
            sql.Append("    A.SEISAN_KUBUN = 3 ");
            sql.Append("GROUP BY");
            sql.Append("    A.SENSHU_CD ");
            sql.Append("HAVING");
            sql.Append("    SUM(A.SASHIHIKI_SEISANGAKU + A.KOJO_KOMOKU4) > 0 ");
            sql.Append(") ");
        /*    sql.Append("UNION ALL ");
            sql.Append("SELECT ");
            sql.Append("    4 AS Pay, ");
            sql.Append("    A.SENSHU_CD, ");
            sql.Append("    MAX(B.TORIHIKISAKI_NM) AS FUNANUSHI_NM, ");
            sql.Append("    SUM(KOJO_KOMOKU4) AS KINGAKU ");
            sql.Append("FROM ");
            sql.Append("    TB_HN_KOJO_DATA AS A ");
            sql.Append("LEFT OUTER JOIN TB_CM_TORIHIKISAKI AS B  ON A.KAISHA_CD = B.KAISHA_CD  AND A.SENSHU_CD = B.TORIHIKISAKI_CD ");
            sql.Append("WHERE ");
            sql.Append("    A.KAISHA_CD = @KAISHA_CD AND");
            sql.Append("    A.KAIKEI_NENDO = @KAIKEI_NENDO AND");
            sql.Append("    A.SEISAN_BI BETWEEN @KEIJO_NENGAPPI_FR AND @KEIJO_NENGAPPI_TO AND ");
            sql.Append("    A.SENSHU_CD BETWEEN @FUNANUSHI_CD_FR AND @FUNANUSHI_CD_TO AND ");
            sql.Append("    A.SEISAN_KUBUN = 3 ");
            sql.Append("GROUP BY ");
            sql.Append("    A.SENSHU_CD ");
            sql.Append("HAVING ");
            sql.Append("    SUM(KOJO_KOMOKU4) > 0 ");
            sql.Append("UNION ALL   ");
            sql.Append("(SELECT");
            sql.Append("    1 AS SiHaNo,");
            sql.Append("    A.SENSHU_CD,");
            sql.Append("    MAX(B.TORIHIKISAKI_NM) AS FUNANUSHI_NM,");
            sql.Append("    SUM(A.SASHIHIKI_SEISANGAKU) AS KINGAKU ");
            sql.Append("FROM");
            sql.Append("    TB_HN_KOJO_DATA AS A   ");
            sql.Append("LEFT OUTER JOIN TB_CM_TORIHIKISAKI AS B    ON A.KAISHA_CD = B.KAISHA_CD    AND A.SENSHU_CD = B.TORIHIKISAKI_CD ");
            sql.Append("WHERE");
            sql.Append("    A.KAISHA_CD = @KAISHA_CD AND");
            sql.Append("    A.KAIKEI_NENDO = @KAIKEI_NENDO AND");
            sql.Append("    B.SHIHARAI_KUBUN = 1 AND");
            sql.Append("    A.SEISAN_BI BETWEEN @KEIJO_NENGAPPI_FR AND @KEIJO_NENGAPPI_TO AND");
            sql.Append("    A.SENSHU_CD BETWEEN @FUNANUSHI_CD_FR AND @FUNANUSHI_CD_TO AND");
            sql.Append("    A.SEISAN_KUBUN = 3 ");
            sql.Append("GROUP BY");
            sql.Append("    A.SENSHU_CD ");
            sql.Append("HAVING");
            sql.Append("    SUM(A.SASHIHIKI_SEISANGAKU) > 0 ");
         */
            sql.Append(" ORDER BY ");
            sql.Append("    A.SENSHU_CD ");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
            dpc.SetParam("@KEIJO_NENGAPPI_FR", SqlDbType.DateTime, Util.ToDate(condition["SeisanDtFr"]));
            dpc.SetParam("@KEIJO_NENGAPPI_TO", SqlDbType.DateTime, Util.ToDate(condition["SeisanDtTo"]));
            dpc.SetParam("@FUNANUSHI_CD_FR", SqlDbType.VarChar, 4, Util.ToString(condition["FunanushiCdFr"]));
            dpc.SetParam("@FUNANUSHI_CD_TO", SqlDbType.VarChar, 4, Util.ToString(condition["FunanushiCdTo"]));

            DataTable dtResult = this._dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            return dtResult;
        }

        /// <summary>
        /// 貸借を仕訳したデータを取得(セリ用)
        /// セリの参照画面表示より呼び出されてる
        /// </summary>
        /// <param name="condition">条件画面の入力値</param>
        /// <param name="jdSwkSettingA">自動仕訳設定Ａ</param>
        /// <param name="jdSwkSettingB">自動仕訳設定Ｂ</param>
        /// <param name="swkTgtData">仕訳対象データ</param>
        /// <returns>貸借を仕訳したデータ(1伝票あたり1DataTable)</returns>
        public DataSet GetTaishakuData(Hashtable condition, DataTable jdSwkSettingA,
            DataTable jdSwkSettingB, DataTable swkTgtData)
        {
            return this.GetSeriTaishakuData(condition, jdSwkSettingA, jdSwkSettingB, swkTgtData);
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 貸借を仕訳したデータを取得(セリ用)
        /// </summary>
        /// <param name="condition">条件画面の入力値</param>
        /// <param name="jdSwkSettingA">自動仕訳設定Ａ</param>
        /// <param name="jdSwkSettingB">自動仕訳設定Ｂ</param>
        /// <param name="swkTgtData">仕訳対象データ</param>
        /// <returns>貸借を仕訳したデータ(1伝票あたり1DataTable)</returns>
        private DataSet GetSeriTaishakuData(Hashtable condition, DataTable jdSwkSettingA,
        DataTable jdSwkSettingB, DataTable swkTgtData)
        {
            DataSet dsTaishakuData = new DataSet();
            DataTable dtTaishakuData;
            DataRow drTaishakuData;
            DataTable dtCommit;
            DataRow[] aryFormattedData;
            DataTable dtFormattedData;

            StringBuilder srcCond = new StringBuilder();
            DataRow[] aryTaishakuRow;
            DataRow[] arySortedRow;
            DataRow[] aryDupRow;
            int prevRowNo;

            DataRow drSwkStgA;
            DataRow drSwkStgB;

            decimal wZeiRt = 0;

            int rowNo = 0;
            string prevFunanushiCd = string.Empty;

            // 設定より取得
            // 勘定科目コード(販売雑費用)
            int KJ_KMK_CD_ZAT_HIY = 724;
            KJ_KMK_CD_ZAT_HIY = Util.ToInt(this._config.LoadPgConfig(Constants.SubSys.Han, "HNSB1021", "Setting", "KJ_KMK_CD_ZAT_HIY"));
            int KJ_HJK_CD_ZAT_HIY = 0;
            KJ_HJK_CD_ZAT_HIY = Util.ToInt(this._config.LoadPgConfig(Constants.SubSys.Han, "HNSB1021", "Setting", "KJ_HJK_CD_ZAT_HIY"));
            int BUMON_CD_ZAT_HIY = 0;
            BUMON_CD_ZAT_HIY = Util.ToInt(this._config.LoadPgConfig(Constants.SubSys.Han, "HNSB1021", "Setting", "BUMON_CD_ZAT_HIY")); ;
            // 勘定科目コード(販売雑収益)
            int KJ_KMK_CD_ZAT_SEK = 623;
            KJ_KMK_CD_ZAT_SEK = Util.ToInt(this._config.LoadPgConfig(Constants.SubSys.Han, "HNSB1021", "Setting", "KJ_KMK_CD_ZAT_SEK"));
            int KJ_HJK_CD_ZAT_SEK = 0;
            KJ_HJK_CD_ZAT_SEK = Util.ToInt(this._config.LoadPgConfig(Constants.SubSys.Han, "HNSB1021", "Setting", "KJ_HJK_CD_ZAT_SEK"));
            int BUMON_CD_ZAT_SEK = 0;
            BUMON_CD_ZAT_SEK = Util.ToInt(this._config.LoadPgConfig(Constants.SubSys.Han, "HNSB1021", "Setting", "BUMON_CD_ZAT_SEK"));

            // 伝票区分（仕訳作成区分に設定）
            int denpyoKbn = Util.ToInt(((HNSB1021.SKbn)condition["SakuseiKbn"]).ToString("D"));

            // 会社情報から消費税を取得する(初期値)
            Decimal taxRate = Util.ToDecimal(this._uInfo.KaikeiSettings["SHIN_SHOHIZEI_RITSU"]) / 100;
            Decimal kingakuInclTax; // 仲買人単位の税込金額

            // 貸借それぞれの合計を保持する
            Decimal kariSum = 0;    // 借方の合計
            Decimal kashiSum = 0;   // 貸方の合計

            // 消費税情報を取得する
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KESSANKI", SqlDbType.Decimal, 3, this._uInfo.KessanKi);
            DataTable dtShohizeiJoho = this._dba.GetDataTableByConditionWithParams("*", "TB_ZM_SHOHIZEI_JOHO", "KESSANKI = @KESSANKI", dpc);

            // 販売雑費用の情報を取得する
            dpc = new DbParamCollection();
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
            dpc.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 6, KJ_KMK_CD_ZAT_HIY);
            DataTable dtZatsuHiyo = this._dba.GetDataTableByConditionWithParams("*", "VI_ZM_KANJO_KAMOKU",
                "KAIKEI_NENDO = @KAIKEI_NENDO AND KANJO_KAMOKU_CD = @KANJO_KAMOKU_CD", dpc);

            // 販売雑収益の情報を取得する
            dpc = new DbParamCollection();
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
            dpc.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 6, KJ_KMK_CD_ZAT_SEK);
            DataTable dtZatsuShueki = this._dba.GetDataTableByConditionWithParams("*", "VI_ZM_KANJO_KAMOKU",
                "KAIKEI_NENDO = @KAIKEI_NENDO AND KANJO_KAMOKU_CD = @KANJO_KAMOKU_CD", dpc);

            // 仕訳伝票の作成方法によって処理を切り替える
            int swkDpMk = Util.ToInt(this._config.LoadPgConfig(Constants.SubSys.Han, "HNSB1021", "Setting", "SwkDpMk"));

            //int swkBmnCd = Util.ToInt(this._config.LoadPgConfig(Constants.SubSys.Kob, "HNSB1021", "Setting", "BumonCd"));
            DataRow BaseDataRow = this._dba.GetKaikeiSettingsByKessanKi(this._uInfo.KaishaCd, this._uInfo.KessanKi).Rows[0];
            int swkJgyKb = Util.ToInt(BaseDataRow["JIGYO_KUBUN"]);
            int swkZeiNh = Util.ToInt(BaseDataRow["SHOHIZEI_NYURYOKU_HOHO"]);

            // 更新する際のレイアウトをベースに定義を作成
            dtTaishakuData = new DataTable();

            dtTaishakuData.Columns.Add("GYO_BANGO", typeof(decimal));
            dtTaishakuData.Columns.Add("KEIJO_NENGAPPI", typeof(DateTime));
            dtTaishakuData.Columns.Add("TAISHAKU_KUBUN", typeof(decimal));
            dtTaishakuData.Columns.Add("MEISAI_KUBUN", typeof(decimal));
            dtTaishakuData.Columns.Add("DENPYO_KUBUN", typeof(decimal));
            dtTaishakuData.Columns.Add("DENPYO_DATE", typeof(DateTime));
            dtTaishakuData.Columns.Add("KANJO_KAMOKU_CD", typeof(decimal));
            dtTaishakuData.Columns.Add("KANJO_KAMOKU_NM", typeof(string));
            dtTaishakuData.Columns.Add("HOJO_KAMOKU_CD", typeof(decimal));
            dtTaishakuData.Columns.Add("HOJO_KAMOKU_NM", typeof(string));
            dtTaishakuData.Columns.Add("BUMON_CD", typeof(decimal));
            dtTaishakuData.Columns.Add("TEKIYO_CD", typeof(decimal));
            dtTaishakuData.Columns.Add("TEKIYO", typeof(string));
            dtTaishakuData.Columns.Add("ZEIKOMI_KINGAKU", typeof(decimal));
            dtTaishakuData.Columns.Add("ZEINUKI_KINGAKU", typeof(decimal));
            dtTaishakuData.Columns.Add("SHOHIZEI_KINGAKU", typeof(decimal));
            dtTaishakuData.Columns.Add("ZEI_KUBUN", typeof(decimal));
            dtTaishakuData.Columns.Add("KAZEI_KUBUN", typeof(decimal));
            dtTaishakuData.Columns.Add("TORIHIKI_KUBUN", typeof(decimal));
            dtTaishakuData.Columns.Add("ZEI_RITSU", typeof(decimal));
            dtTaishakuData.Columns.Add("JIGYO_KUBUN", typeof(decimal));
            dtTaishakuData.Columns.Add("SHOHIZEI_NYURYOKU_HOHO", typeof(decimal));
            dtTaishakuData.Columns.Add("SHOHIZEI_HENKO", typeof(decimal));
            dtTaishakuData.Columns.Add("KESSAN_KUBUN", typeof(decimal));
            dtTaishakuData.Columns.Add("SHIWAKE_SAKUSEI_KUBUN", typeof(decimal));
            dtTaishakuData.Columns.Add("CHK_ZUMI", typeof(decimal));
            dtTaishakuData.Columns.Add("DEL_FLG", typeof(decimal));

            // とりあえず1行ずつ貸借及び課税区分によって起票
            for (int i = 0; i < swkTgtData.Rows.Count; i++)
            {
                // 請求先別に伝票を起票する場合のブレイク処理
                if (swkDpMk == 3 && (!ValChk.IsEmpty(prevFunanushiCd) && !Util.ToString(swkTgtData.Rows[i]["船主CD"]).Equals(prevFunanushiCd)))
                {
                    // 貸借の科目単位でまとめ上げる
                    arySortedRow = dtTaishakuData.Select(null, "GYO_BANGO, TAISHAKU_KUBUN, MEISAI_KUBUN");

                    for (int j = 0; j < arySortedRow.Length; j++)
                    {
                        // 既に削除された行は評価しない
                        if (Util.ToInt(arySortedRow[j]["DEL_FLG"]) == 1)
                        {
                            continue;
                        }

                        // 同一の計上年月日・貸借区分・明細区分・勘定科目コード・補助科目コード・部門コード・税区分・事業区分で
                        // ※チェック済みのデータは参照しない
                        srcCond = new StringBuilder();
                        srcCond.Append("KEIJO_NENGAPPI = '" + Util.ToDate(arySortedRow[j]["KEIJO_NENGAPPI"]).ToString("yyyy/MM/dd") + "'");
                        srcCond.Append(" AND TAISHAKU_KUBUN = " + Util.ToString(arySortedRow[j]["TAISHAKU_KUBUN"]));
                        srcCond.Append(" AND MEISAI_KUBUN = " + Util.ToString(arySortedRow[j]["MEISAI_KUBUN"]));
                        srcCond.Append(" AND KANJO_KAMOKU_CD = " + Util.ToString(arySortedRow[j]["KANJO_KAMOKU_CD"]));
                        srcCond.Append(" AND HOJO_KAMOKU_CD = " + Util.ToString(arySortedRow[j]["HOJO_KAMOKU_CD"]));
                        srcCond.Append(" AND BUMON_CD = " + Util.ToString(arySortedRow[j]["BUMON_CD"]));
                        srcCond.Append(" AND ZEI_KUBUN = " + Util.ToString(arySortedRow[j]["ZEI_KUBUN"]));
                        srcCond.Append(" AND JIGYO_KUBUN = " + Util.ToString(arySortedRow[j]["JIGYO_KUBUN"]));

                        // 複数税率対応
                        srcCond.Append(" AND ZEI_RITSU = " + Util.ToString(arySortedRow[j]["ZEI_RITSU"]));
                        srcCond.Append(" AND CHK_ZUMI = 0");
                        srcCond.Append(" AND DEL_FLG = 0");

                        aryDupRow = dtTaishakuData.Select(srcCond.ToString(), "GYO_BANGO, TAISHAKU_KUBUN, MEISAI_KUBUN");

                        // 重複した1行目の情報に2,3行目の金額を加算していく
                        for (int k = 0; k < aryDupRow.Length; k++)
                        {
                            // 自身の行は読み飛ばす
                            if (k == 0) continue;

                            // 既に削除された行は評価しない
                            if (Util.ToInt(aryDupRow[k]["DEL_FLG"]) == 1) continue;

                            aryDupRow[0]["ZEIKOMI_KINGAKU"] = Util.ToDecimal(aryDupRow[0]["ZEIKOMI_KINGAKU"]) + Util.ToDecimal(aryDupRow[k]["ZEIKOMI_KINGAKU"]);
                            aryDupRow[0]["ZEINUKI_KINGAKU"] = Util.ToDecimal(aryDupRow[0]["ZEINUKI_KINGAKU"]) + Util.ToDecimal(aryDupRow[k]["ZEINUKI_KINGAKU"]);
                            aryDupRow[0]["SHOHIZEI_KINGAKU"] = Util.ToDecimal(aryDupRow[0]["SHOHIZEI_KINGAKU"]) + Util.ToDecimal(aryDupRow[k]["SHOHIZEI_KINGAKU"]);
                            aryDupRow[k]["DEL_FLG"] = 1;
                        }

                        // 借方(仲買人ごと)についてはこのタイミングで税計算をする
                        if (Util.ToInt(arySortedRow[j]["TAISHAKU_KUBUN"]) == 1)
                        {

                            // 複数税率対応
                            taxRate = Util.ToDecimal(arySortedRow[j]["ZEI_RITSU"]) / 100;

                            kingakuInclTax = Util.Round((Util.ToDecimal(aryDupRow[0]["ZEIKOMI_KINGAKU"]) * (1 + taxRate)), 0);
                            aryDupRow[0]["ZEIKOMI_KINGAKU"] = kingakuInclTax;
                            aryDupRow[0]["ZEINUKI_KINGAKU"] = kingakuInclTax;
                            kariSum += kingakuInclTax;
                        }
                    }

                    // 削除フラグが1の行を削除(一旦削除確定用テンポラリテーブルに詰める)
                    dtCommit = dtTaishakuData.Clone();
                    foreach (DataRow row in dtTaishakuData.Rows)
                    {
                        // 複数税率対応
                        //if (Util.ToInt(row["DEL_FLG"]) == 0) dtCommit.ImportRow(row);
                        if (Util.ToInt(row["DEL_FLG"]) == 0)
                        {
                            if (Util.ToInt(row["TAISHAKU_KUBUN"]) == 1 && Util.ToDecimal(row["ZEI_RITSU"]) != 0)
                            {
                                row["ZEI_RITSU"] = 0;
                            }
                            dtCommit.ImportRow(row);
                        }
                    }
                    dtTaishakuData.Clear();
                    dtTaishakuData = dtCommit.Copy();

                    // 消し込んだ結果を整理する(上の行に詰める)
                    // 借方
                    aryTaishakuRow = dtTaishakuData.Select("TAISHAKU_KUBUN = 1", "GYO_BANGO, MEISAI_KUBUN");
                    prevRowNo = 0;
                    for (int j = 0; j < aryTaishakuRow.Length; j++)
                    {
                        // 現在保持している行番号が前に振った行番号+1、あるいは変わっていない場合は特に
                        // 変更しない。飛んでる場合だけ変更する
                        if (Util.ToInt(aryTaishakuRow[j]["GYO_BANGO"]) > prevRowNo + 1)
                        {
                            aryTaishakuRow[j]["GYO_BANGO"] = prevRowNo + 1;
                        }

                        prevRowNo = Util.ToInt(aryTaishakuRow[j]["GYO_BANGO"]);
                    }

                    // 貸方
                    aryTaishakuRow = dtTaishakuData.Select("TAISHAKU_KUBUN = 2", "GYO_BANGO, MEISAI_KUBUN");
                    prevRowNo = 0;
                    for (int j = 0; j < aryTaishakuRow.Length; j++)
                    {
                        // 現在保持している行番号が前に振った行番号+1、あるいは変わっていない場合は特に
                        // 変更しない。飛んでる場合だけ変更する
                        if (Util.ToInt(aryTaishakuRow[j]["GYO_BANGO"]) > prevRowNo + 1)
                        {
                            aryTaishakuRow[j]["GYO_BANGO"] = prevRowNo + 1;
                        }

                        prevRowNo = Util.ToInt(aryTaishakuRow[j]["GYO_BANGO"]);
                    }

                    // 最後に整列して格納
                    aryFormattedData = dtTaishakuData.Select(null, "GYO_BANGO, TAISHAKU_KUBUN, MEISAI_KUBUN");
                    dtFormattedData = dtTaishakuData.Clone();
                    for (int j = 0; j < aryFormattedData.Length; j++)
                    {
                        dtFormattedData.ImportRow(aryFormattedData[j]);
                    }

                    // 貸借の誤差を雑費として計上
                    if (kariSum != kashiSum)
                    {
                        rowNo = Util.ToInt(dtFormattedData.Rows[dtFormattedData.Rows.Count - 1]["GYO_BANGO"]) + 1;
                        drTaishakuData = dtFormattedData.NewRow();
                        drTaishakuData["GYO_BANGO"] = rowNo;
                        drTaishakuData["TAISHAKU_KUBUN"] = (kariSum < kashiSum) ? 1 : 2;
                        drTaishakuData["MEISAI_KUBUN"] = 0;
                        drTaishakuData["DENPYO_KUBUN"] = 1; // TODO:1固定でいいかどうか
                        drTaishakuData["DENPYO_DATE"] = condition["SwkDpyDt"];
                        // MOD-20140403:借方計上の場合は販売雑費用、貸方計上の場合は販売雑収益として計上

                        // 設定ファイルより取得した値を使用
                        drTaishakuData["KANJO_KAMOKU_CD"] = (kariSum < kashiSum) ? KJ_KMK_CD_ZAT_HIY : KJ_KMK_CD_ZAT_SEK;
                        //drTaishakuData["HOJO_KAMOKU_CD"] = 0;       // TODO:べた書き
                        drTaishakuData["HOJO_KAMOKU_CD"] = (kariSum < kashiSum) ? KJ_HJK_CD_ZAT_HIY : KJ_HJK_CD_ZAT_SEK;
                        //drTaishakuData["BUMON_CD"] = 0;     // TODO:0固定でいいかどうか
                        drTaishakuData["BUMON_CD"] = (kariSum < kashiSum) ? BUMON_CD_ZAT_HIY : BUMON_CD_ZAT_SEK;

                        drTaishakuData["TEKIYO_CD"] = Util.ToDecimal(condition["TekiyoCd"]);
                        drTaishakuData["TEKIYO"] = condition["Tekiyo"];
                        drTaishakuData["ZEIKOMI_KINGAKU"] = Math.Abs(kariSum - kashiSum);
                        drTaishakuData["ZEINUKI_KINGAKU"] = Math.Abs(kariSum - kashiSum);
                        drTaishakuData["SHOHIZEI_KINGAKU"] = 0;
                        if (kariSum < kashiSum)
                        {
                            if (dtZatsuHiyo.Rows.Count > 0)
                            {
                                drTaishakuData["ZEI_KUBUN"] = dtZatsuHiyo.Rows[0]["KARIKATA_ZEI_KUBUN"];
                                drTaishakuData["KAZEI_KUBUN"] = dtZatsuHiyo.Rows[0]["KARI_KAZEI_KUBUN"];
                                drTaishakuData["TORIHIKI_KUBUN"] = dtZatsuHiyo.Rows[0]["KARI_TORIHIKI_KUBUN"];

                                //drTaishakuData["ZEI_RITSU"] = dtZatsuHiyo.Rows[0]["KARI_ZEI_RITSU"];
                                wZeiRt = TaxUtil.GetTaxRate(Util.ToDate(condition["SwkDpyDt"]) , Util.ToInt(dtZatsuHiyo.Rows[0]["KARIKATA_ZEI_KUBUN"]), this._dba); // 新税率から取得
                                drTaishakuData["ZEI_RITSU"] = wZeiRt;
                            }
                        }
                        else
                        {
                            if (dtZatsuShueki.Rows.Count > 0)
                            {
                                drTaishakuData["ZEI_KUBUN"] = dtZatsuShueki.Rows[0]["KASHIKATA_ZEI_KUBUN"];
                                drTaishakuData["KAZEI_KUBUN"] = dtZatsuShueki.Rows[0]["KASHI_KAZEI_KUBUN"];
                                drTaishakuData["TORIHIKI_KUBUN"] = dtZatsuShueki.Rows[0]["KASHI_TORIHIKI_KUBUN"];

                                //drTaishakuData["ZEI_RITSU"] = dtZatsuShueki.Rows[0]["KASHI_ZEI_RITSU"];
                                wZeiRt = TaxUtil.GetTaxRate(Util.ToDate(condition["SwkDpyDt"]), Util.ToInt(dtZatsuShueki.Rows[0]["KASHI_ZEI_RITSU"]), this._dba); // 新税率から取得
                                drTaishakuData["ZEI_RITSU"] = wZeiRt;
                            }
                        }
                        if (dtShohizeiJoho.Rows.Count > 0)
                        {
                            drTaishakuData["JIGYO_KUBUN"] = dtShohizeiJoho.Rows[0]["JIGYO_KUBUN"];
                            drTaishakuData["SHOHIZEI_NYURYOKU_HOHO"] = dtShohizeiJoho.Rows[0]["SHOHIZEI_NYURYOKU_HOHO"];
                        }
                        drTaishakuData["SHOHIZEI_HENKO"] = 0;   // TODO:固定?
                        drTaishakuData["KESSAN_KUBUN"] = 0;     // TODO:固定?
                        //drTaishakuData["SHIWAKE_SAKUSEI_KUBUN"] = 9;    // TODO:固定?
                        drTaishakuData["SHIWAKE_SAKUSEI_KUBUN"] = denpyoKbn;    // 伝票区分を設定
                        
                        // 初期化追加
                        drTaishakuData["CHK_ZUMI"] = 0;
                        drTaishakuData["DEL_FLG"] = 0;

                        dtFormattedData.Rows.Add(drTaishakuData);
                    }

                    // DataSetにDataTableを追加
                    dsTaishakuData.Tables.Add(dtFormattedData);

                    // 次の請求先の貸借情報格納用に新たにDataTableを作成
                    dtTaishakuData = new DataTable();

                    dtTaishakuData.Columns.Add("GYO_BANGO", typeof(decimal));
                    dtTaishakuData.Columns.Add("KEIJO_NENGAPPI", typeof(DateTime));
                    dtTaishakuData.Columns.Add("TAISHAKU_KUBUN", typeof(decimal));
                    dtTaishakuData.Columns.Add("MEISAI_KUBUN", typeof(decimal));
                    dtTaishakuData.Columns.Add("DENPYO_KUBUN", typeof(decimal));
                    dtTaishakuData.Columns.Add("DENPYO_DATE", typeof(DateTime));
                    dtTaishakuData.Columns.Add("KANJO_KAMOKU_CD", typeof(decimal));
                    dtTaishakuData.Columns.Add("KANJO_KAMOKU_NM", typeof(string));
                    dtTaishakuData.Columns.Add("HOJO_KAMOKU_CD", typeof(decimal));
                    dtTaishakuData.Columns.Add("HOJO_KAMOKU_NM", typeof(string));
                    dtTaishakuData.Columns.Add("BUMON_CD", typeof(decimal));
                    dtTaishakuData.Columns.Add("TEKIYO_CD", typeof(decimal));
                    dtTaishakuData.Columns.Add("TEKIYO", typeof(string));
                    dtTaishakuData.Columns.Add("ZEIKOMI_KINGAKU", typeof(decimal));
                    dtTaishakuData.Columns.Add("ZEINUKI_KINGAKU", typeof(decimal));
                    dtTaishakuData.Columns.Add("SHOHIZEI_KINGAKU", typeof(decimal));
                    dtTaishakuData.Columns.Add("ZEI_KUBUN", typeof(decimal));
                    dtTaishakuData.Columns.Add("KAZEI_KUBUN", typeof(decimal));
                    dtTaishakuData.Columns.Add("TORIHIKI_KUBUN", typeof(decimal));
                    dtTaishakuData.Columns.Add("ZEI_RITSU", typeof(decimal));
                    dtTaishakuData.Columns.Add("JIGYO_KUBUN", typeof(decimal));
                    dtTaishakuData.Columns.Add("SHOHIZEI_NYURYOKU_HOHO", typeof(decimal));
                    dtTaishakuData.Columns.Add("SHOHIZEI_HENKO", typeof(decimal));
                    dtTaishakuData.Columns.Add("KESSAN_KUBUN", typeof(decimal));
                    dtTaishakuData.Columns.Add("SHIWAKE_SAKUSEI_KUBUN", typeof(decimal));
                    dtTaishakuData.Columns.Add("CHK_ZUMI", typeof(decimal));
                    dtTaishakuData.Columns.Add("DEL_FLG", typeof(decimal));

                    // 行番号をリセット
                    rowNo = 0;

                    // 貸借の合計をリセット
                    kariSum = 0;
                    kashiSum = 0;
                }

                rowNo++;

                // 借方
                // 仕訳コードから自動仕訳設定Ｂ.仕訳コードに該当するレコードを引き当て
                drSwkStgB = GetSwkSettingBRec(jdSwkSettingB, Util.ToInt(swkTgtData.Rows[i]["仕訳CD"]));

                drTaishakuData = dtTaishakuData.NewRow();
                drTaishakuData["GYO_BANGO"] = rowNo;
                drTaishakuData["KEIJO_NENGAPPI"] = swkTgtData.Rows[i]["計上年月日"];
                drTaishakuData["TAISHAKU_KUBUN"] = 1;
                drTaishakuData["MEISAI_KUBUN"] = 0;
                drTaishakuData["DENPYO_KUBUN"] = 1; // TODO:1固定でいいかどうか
                drTaishakuData["DENPYO_DATE"] = condition["SwkDpyDt"];
                drTaishakuData["TEKIYO_CD"] = Util.ToDecimal(condition["TekiyoCd"]);
                drTaishakuData["TEKIYO"] = condition["Tekiyo"];
                // 結果としては両方とも税込金額にならなければならないが、税抜金額を一旦セットする。集約するタイミングで税計算をする
                drTaishakuData["ZEIKOMI_KINGAKU"] = Util.ToDecimal(swkTgtData.Rows[i]["売上金額"]);
                drTaishakuData["ZEINUKI_KINGAKU"] = Util.ToDecimal(swkTgtData.Rows[i]["売上金額"]);
                drTaishakuData["SHOHIZEI_KINGAKU"] = 0;
                drTaishakuData["ZEI_KUBUN"] = 0;
                drTaishakuData["KAZEI_KUBUN"] = 0;
                drTaishakuData["TORIHIKI_KUBUN"] = 0;

                // 複数税率対応（税率は後でクリア）
                //drTaishakuData["ZEI_RITSU"] = 0;
                drTaishakuData["ZEI_RITSU"] = Util.ToDecimal(swkTgtData.Rows[i]["税率"]);

                drTaishakuData["SHOHIZEI_NYURYOKU_HOHO"] = swkZeiNh;//swkTgtData.Rows[i]["消費税入力方法"];
                drTaishakuData["SHOHIZEI_HENKO"] = 0;   // TODO:固定?
                drTaishakuData["KESSAN_KUBUN"] = 0;     // TODO:固定?
                //drTaishakuData["SHIWAKE_SAKUSEI_KUBUN"] = 9;    // TODO:固定?
                drTaishakuData["SHIWAKE_SAKUSEI_KUBUN"] = denpyoKbn;    // 伝票区分を設定

                drTaishakuData["CHK_ZUMI"] = 0;
                drTaishakuData["DEL_FLG"] = 0;

                // GetSeiSwkPos(　こちらは税区分ゼロ設定で税情報は全てゼロ

                if (drSwkStgB != null)
                {
                    //drTaishakuData["BUMON_CD"] = drSwkStgB["部門コード"];
                    drTaishakuData["BUMON_CD"] = (Util.ToInt(Util.ToString(drSwkStgB["部門コード"])) == -1 ? swkTgtData.Rows[i]["部門CD"] : drSwkStgB["部門コード"]);
                    drTaishakuData["KANJO_KAMOKU_CD"] = drSwkStgB["勘定科目コード"];
                    drTaishakuData["KANJO_KAMOKU_NM"] = drSwkStgB["勘定科目名"];
                    if (Util.ToInt(drSwkStgB["補助使用区分"]) == 2)
                    {
                        drTaishakuData["HOJO_KAMOKU_CD"] = swkTgtData.Rows[i]["仲買人CD"];
                        drTaishakuData["HOJO_KAMOKU_NM"] = swkTgtData.Rows[i]["仲買人名称"];
                    }
                    else
                    {
                        drTaishakuData["HOJO_KAMOKU_CD"] = drSwkStgB["補助科目コード"];
                        //drTaishakuData["HOJO_KAMOKU_NM"] = drSwkStgB["補助科目名"]; // 未設定の為
                    }
                    drTaishakuData["JIGYO_KUBUN"] = Util.ToInt(Util.ToString(drSwkStgB["事業区分"])) == 0 ? swkJgyKb : drSwkStgB["事業区分"];
                }
                else
                {
                    drTaishakuData["BUMON_CD"] = DBNull.Value;
                    drTaishakuData["KANJO_KAMOKU_CD"] = DBNull.Value;
                    drTaishakuData["KANJO_KAMOKU_NM"] = DBNull.Value;
                    drTaishakuData["HOJO_KAMOKU_CD"] = DBNull.Value;
                    drTaishakuData["HOJO_KAMOKU_NM"] = DBNull.Value;
                    drTaishakuData["JIGYO_KUBUN"] = DBNull.Value;
                }
                dtTaishakuData.Rows.Add(drTaishakuData);

                // 貸方
                // 取引区分１、２から自動仕訳設定Ａ.仕訳コードに該当するレコードを引き当て
                drSwkStgA = GetSwkSettingARec(jdSwkSettingA,
                    Util.ToInt(swkTgtData.Rows[i]["取引区分１"]), Util.ToInt(swkTgtData.Rows[i]["取引区分２"]));

                drTaishakuData = dtTaishakuData.NewRow();
                drTaishakuData["GYO_BANGO"] = rowNo;
                drTaishakuData["KEIJO_NENGAPPI"] = swkTgtData.Rows[i]["計上年月日"];
                drTaishakuData["TAISHAKU_KUBUN"] = 2;
                drTaishakuData["MEISAI_KUBUN"] = 0;
                drTaishakuData["DENPYO_KUBUN"] = 1; // TODO:1固定でいいかどうか
                drTaishakuData["DENPYO_DATE"] = condition["SwkDpyDt"];
                drTaishakuData["TEKIYO_CD"] = Util.ToDecimal(condition["TekiyoCd"]);
                drTaishakuData["TEKIYO"] = condition["Tekiyo"];
                kingakuInclTax = Util.ToDecimal(swkTgtData.Rows[i]["売上金額"]) + Util.ToDecimal(swkTgtData.Rows[i]["水揚消費税額"]);
                drTaishakuData["ZEIKOMI_KINGAKU"] = kingakuInclTax;
                drTaishakuData["ZEINUKI_KINGAKU"] = kingakuInclTax;
                drTaishakuData["SHOHIZEI_KINGAKU"] = 0;

                kashiSum += kingakuInclTax;

                drTaishakuData["ZEI_KUBUN"] = 0;
                drTaishakuData["KAZEI_KUBUN"] = 0;
                drTaishakuData["TORIHIKI_KUBUN"] = 0;
                drTaishakuData["ZEI_RITSU"] = 0;
                drTaishakuData["SHOHIZEI_NYURYOKU_HOHO"] = swkZeiNh;// swkTgtData.Rows[i]["消費税入力方法"];
                drTaishakuData["SHOHIZEI_HENKO"] = 0;   // TODO:固定?
                drTaishakuData["KESSAN_KUBUN"] = 0;     // TODO:固定?
                //drTaishakuData["SHIWAKE_SAKUSEI_KUBUN"] = 9;    // TODO:固定?
                drTaishakuData["SHIWAKE_SAKUSEI_KUBUN"] = denpyoKbn;    // 伝票区分を設定

                drTaishakuData["CHK_ZUMI"] = 0;
                drTaishakuData["DEL_FLG"] = 0;

                // SetMeisaiの相手処理 こちらは仕訳設定を利用（税関係）

                if (drSwkStgA != null)
                {
                    //drTaishakuData["BUMON_CD"] = drSwkStgA["部門コード"];
                    drTaishakuData["BUMON_CD"] = (Util.ToInt(Util.ToString(drSwkStgA["部門コード"])) == -1 ? swkTgtData.Rows[i]["部門CD"] : drSwkStgA["部門コード"]);
                    drTaishakuData["KANJO_KAMOKU_CD"] = drSwkStgA["勘定科目コード"];
                    drTaishakuData["KANJO_KAMOKU_NM"] = drSwkStgA["勘定科目名"];
                    if (Util.ToInt(drSwkStgA["補助使用区分"]) == 2)
                    {
                        drTaishakuData["HOJO_KAMOKU_CD"] = swkTgtData.Rows[i]["船主CD"];
                        drTaishakuData["HOJO_KAMOKU_NM"] = swkTgtData.Rows[i]["船主名称"];
                    }
                    else
                    {
                        drTaishakuData["HOJO_KAMOKU_CD"] = drSwkStgA["補助科目コード"];
                        //drTaishakuData["HOJO_KAMOKU_NM"] = drSwkStgA["補助科目名"]; // 未設定の為
                    }
                    drTaishakuData["JIGYO_KUBUN"] = Util.ToInt(Util.ToString(drSwkStgA["事業区分"])) == 0 ? swkJgyKb : drSwkStgA["事業区分"];

                    dtTaishakuData.Rows.Add(drTaishakuData);
                }
                else
                {
                    drTaishakuData["BUMON_CD"] = DBNull.Value;
                    drTaishakuData["KANJO_KAMOKU_CD"] = DBNull.Value;
                    drTaishakuData["KANJO_KAMOKU_NM"] = DBNull.Value;
                    drTaishakuData["HOJO_KAMOKU_CD"] = DBNull.Value;
                    drTaishakuData["HOJO_KAMOKU_NM"] = DBNull.Value;
                    drTaishakuData["JIGYO_KUBUN"] = DBNull.Value;
                }

                // 会員番号の退避
                prevFunanushiCd = Util.ToString(swkTgtData.Rows[i]["船主CD"]);
            }

            // 貸借の科目単位でまとめ上げる
            arySortedRow = dtTaishakuData.Select(null, "GYO_BANGO, TAISHAKU_KUBUN, MEISAI_KUBUN");

            for (int j = 0; j < arySortedRow.Length; j++)
            {
                // 既に削除された行は評価しない
                if (Util.ToInt(arySortedRow[j]["DEL_FLG"]) == 1) continue;

                // 同一の計上年月日・貸借区分・明細区分・勘定科目コード・補助科目コード・部門コード・税区分・事業区分で
                // 金額をまとめ上げる
                // ※チェック済みのデータは参照しない
                srcCond = new StringBuilder();
                srcCond.Append("KEIJO_NENGAPPI = '" + Util.ToDate(arySortedRow[j]["KEIJO_NENGAPPI"]).ToString("yyyy/MM/dd") + "'");
                srcCond.Append(" AND TAISHAKU_KUBUN = " + Util.ToString(arySortedRow[j]["TAISHAKU_KUBUN"]));
                srcCond.Append(" AND MEISAI_KUBUN = " + Util.ToString(arySortedRow[j]["MEISAI_KUBUN"]));
                srcCond.Append(" AND KANJO_KAMOKU_CD = " + Util.ToString(arySortedRow[j]["KANJO_KAMOKU_CD"]));
                srcCond.Append(" AND HOJO_KAMOKU_CD = " + Util.ToString(arySortedRow[j]["HOJO_KAMOKU_CD"]));
                srcCond.Append(" AND BUMON_CD = " + Util.ToString(arySortedRow[j]["BUMON_CD"]));
                srcCond.Append(" AND ZEI_KUBUN = " + Util.ToString(arySortedRow[j]["ZEI_KUBUN"]));
                srcCond.Append(" AND JIGYO_KUBUN = " + Util.ToString(arySortedRow[j]["JIGYO_KUBUN"]));

                // 複数税率対応
                srcCond.Append(" AND ZEI_RITSU = " + Util.ToString(arySortedRow[j]["ZEI_RITSU"]));
                srcCond.Append(" AND CHK_ZUMI = 0");
                srcCond.Append(" AND DEL_FLG = 0");
                
                aryDupRow = dtTaishakuData.Select(srcCond.ToString(), "GYO_BANGO, TAISHAKU_KUBUN, MEISAI_KUBUN");

                // 重複した1行目の情報に2,3行目の金額を加算していく
                for (int k = 0; k < aryDupRow.Length; k++)
                {
                    // 自身の行は読み飛ばす
                    if (k == 0) continue;

                    // 既に削除された行は評価しない
                    if (Util.ToInt(aryDupRow[k]["DEL_FLG"]) == 1) continue;

                    aryDupRow[0]["ZEIKOMI_KINGAKU"] = Util.ToDecimal(aryDupRow[0]["ZEIKOMI_KINGAKU"]) + Util.ToDecimal(aryDupRow[k]["ZEIKOMI_KINGAKU"]);
                    aryDupRow[0]["ZEINUKI_KINGAKU"] = Util.ToDecimal(aryDupRow[0]["ZEINUKI_KINGAKU"]) + Util.ToDecimal(aryDupRow[k]["ZEINUKI_KINGAKU"]);
                    aryDupRow[0]["SHOHIZEI_KINGAKU"] = Util.ToDecimal(aryDupRow[0]["SHOHIZEI_KINGAKU"]) + Util.ToDecimal(aryDupRow[k]["SHOHIZEI_KINGAKU"]);
                    aryDupRow[k]["DEL_FLG"] = 1;
                }

                // 借方(仲買人ごと)についてはこのタイミングで税計算をする
                if (Util.ToInt(arySortedRow[j]["TAISHAKU_KUBUN"]) == 1)
                {

                    // 複数税率対応
                    taxRate = Util.ToDecimal(arySortedRow[j]["ZEI_RITSU"]) / 100;

                    kingakuInclTax = Util.Round((Util.ToDecimal(aryDupRow[0]["ZEIKOMI_KINGAKU"]) * (1 + taxRate)), 0);
                    aryDupRow[0]["ZEIKOMI_KINGAKU"] = kingakuInclTax;
                    aryDupRow[0]["ZEINUKI_KINGAKU"] = kingakuInclTax;
                    kariSum += kingakuInclTax;
                }
            }

            // 削除フラグが1の行を削除(一旦削除確定用テンポラリテーブルに詰める)
            dtCommit = dtTaishakuData.Clone();
            foreach (DataRow row in dtTaishakuData.Rows)
            {
                // 複数税率対応
                //if (Util.ToInt(row["DEL_FLG"]) == 0) dtCommit.ImportRow(row);
                if (Util.ToInt(row["DEL_FLG"]) == 0)
                {
                    if (Util.ToInt(row["TAISHAKU_KUBUN"]) == 1 && Util.ToDecimal(row["ZEI_RITSU"]) != 0)
                    {
                        row["ZEI_RITSU"] = 0;
                    }
                    dtCommit.ImportRow(row);
                }
            }
            dtTaishakuData.Clear();
            dtTaishakuData = dtCommit.Copy();

            // 貸借それぞれ上に詰める
            // 借方
            aryTaishakuRow = dtTaishakuData.Select("TAISHAKU_KUBUN = 1", "GYO_BANGO, MEISAI_KUBUN");
            prevRowNo = 0;
            for (int j = 0; j < aryTaishakuRow.Length; j++)
            {
                // 現在保持している行番号が前に振った行番号+1、あるいは変わっていない場合は特に
                // 変更しない。飛んでる場合だけ変更する
                if (Util.ToInt(aryTaishakuRow[j]["GYO_BANGO"]) > prevRowNo + 1)
                {
                    aryTaishakuRow[j]["GYO_BANGO"] = prevRowNo + 1;
                }

                prevRowNo = Util.ToInt(aryTaishakuRow[j]["GYO_BANGO"]);
            }

            // 貸方
            aryTaishakuRow = dtTaishakuData.Select("TAISHAKU_KUBUN = 2", "GYO_BANGO, MEISAI_KUBUN");
            prevRowNo = 0;
            for (int j = 0; j < aryTaishakuRow.Length; j++)
            {
                // 現在保持している行番号が前に振った行番号+1、あるいは変わっていない場合は特に
                // 変更しない。飛んでる場合だけ変更する
                if (Util.ToInt(aryTaishakuRow[j]["GYO_BANGO"]) > prevRowNo + 1)
                {
                    aryTaishakuRow[j]["GYO_BANGO"] = prevRowNo + 1;
                }

                prevRowNo = Util.ToInt(aryTaishakuRow[j]["GYO_BANGO"]);
            }

            // 最後に整列して格納
            aryFormattedData = dtTaishakuData.Select(null, "GYO_BANGO, TAISHAKU_KUBUN, MEISAI_KUBUN");
            dtFormattedData = dtTaishakuData.Clone();
            for (int j = 0; j < aryFormattedData.Length; j++)
            {
                dtFormattedData.ImportRow(aryFormattedData[j]);
            }

            // 貸借の誤差を計上
            if (kariSum != kashiSum)
            {
                rowNo = Util.ToInt(dtFormattedData.Rows[dtFormattedData.Rows.Count - 1]["GYO_BANGO"]) + 1;
                drTaishakuData = dtFormattedData.NewRow();
                drTaishakuData["GYO_BANGO"] = rowNo;
                drTaishakuData["TAISHAKU_KUBUN"] = (kariSum < kashiSum) ? 1 : 2;
                drTaishakuData["MEISAI_KUBUN"] = 0;
                drTaishakuData["DENPYO_KUBUN"] = 1; // TODO:1固定でいいかどうか
                drTaishakuData["DENPYO_DATE"] = condition["SwkDpyDt"];
                // MOD-20140403:借方計上の場合は販売雑費用、貸方計上の場合は販売雑収益として計上

                // 設定ファイルより取得した値を使用
                drTaishakuData["KANJO_KAMOKU_CD"] = (kariSum < kashiSum) ? KJ_KMK_CD_ZAT_HIY : KJ_KMK_CD_ZAT_SEK;
                //drTaishakuData["HOJO_KAMOKU_CD"] = 0;       // TODO:べた書き
                drTaishakuData["HOJO_KAMOKU_CD"] = (kariSum < kashiSum) ? KJ_HJK_CD_ZAT_HIY : KJ_HJK_CD_ZAT_SEK;
                //drTaishakuData["BUMON_CD"] = 0;     // TODO:0固定でいいかどうか
                drTaishakuData["BUMON_CD"] = (kariSum < kashiSum) ? BUMON_CD_ZAT_HIY : BUMON_CD_ZAT_SEK;

                drTaishakuData["TEKIYO_CD"] = Util.ToDecimal(condition["TekiyoCd"]);
                drTaishakuData["TEKIYO"] = condition["Tekiyo"];
                drTaishakuData["ZEIKOMI_KINGAKU"] = Math.Abs(kariSum - kashiSum);
                drTaishakuData["ZEINUKI_KINGAKU"] = Math.Abs(kariSum - kashiSum);
                drTaishakuData["SHOHIZEI_KINGAKU"] = 0;
                if (kariSum < kashiSum)
                {
                    if (dtZatsuHiyo.Rows.Count > 0)
                    {
                        drTaishakuData["ZEI_KUBUN"] = dtZatsuHiyo.Rows[0]["KARIKATA_ZEI_KUBUN"];
                        drTaishakuData["KAZEI_KUBUN"] = dtZatsuHiyo.Rows[0]["KARI_KAZEI_KUBUN"];
                        drTaishakuData["TORIHIKI_KUBUN"] = dtZatsuHiyo.Rows[0]["KARI_TORIHIKI_KUBUN"];

                        //drTaishakuData["ZEI_RITSU"] = dtZatsuHiyo.Rows[0]["KARI_ZEI_RITSU"];
                        wZeiRt = TaxUtil.GetTaxRate(Util.ToDate(condition["SwkDpyDt"]), Util.ToInt(dtZatsuHiyo.Rows[0]["KARIKATA_ZEI_KUBUN"]), this._dba); // 新税率から取得
                        drTaishakuData["ZEI_RITSU"] = wZeiRt;
                    }
                }
                else
                {
                    if (dtZatsuShueki.Rows.Count > 0)
                    {
                        drTaishakuData["ZEI_KUBUN"] = dtZatsuShueki.Rows[0]["KASHIKATA_ZEI_KUBUN"];
                        drTaishakuData["KAZEI_KUBUN"] = dtZatsuShueki.Rows[0]["KASHI_KAZEI_KUBUN"];
                        drTaishakuData["TORIHIKI_KUBUN"] = dtZatsuShueki.Rows[0]["KASHI_TORIHIKI_KUBUN"];

                        //drTaishakuData["ZEI_RITSU"] = dtZatsuShueki.Rows[0]["KASHI_ZEI_RITSU"];
                        wZeiRt = TaxUtil.GetTaxRate(Util.ToDate(condition["SwkDpyDt"]), Util.ToInt(dtZatsuShueki.Rows[0]["KASHIKATA_ZEI_KUBUN"]), this._dba); // 新税率から取得
                        drTaishakuData["ZEI_RITSU"] = wZeiRt;
                    }
                }
                if (dtShohizeiJoho.Rows.Count > 0)
                {
                    drTaishakuData["JIGYO_KUBUN"] = dtShohizeiJoho.Rows[0]["JIGYO_KUBUN"];
                    drTaishakuData["SHOHIZEI_NYURYOKU_HOHO"] = dtShohizeiJoho.Rows[0]["SHOHIZEI_NYURYOKU_HOHO"];
                }
                drTaishakuData["SHOHIZEI_HENKO"] = 0;   // TODO:固定?
                drTaishakuData["KESSAN_KUBUN"] = 0;     // TODO:固定?
                //drTaishakuData["SHIWAKE_SAKUSEI_KUBUN"] = 9;    // TODO:固定?
                drTaishakuData["SHIWAKE_SAKUSEI_KUBUN"] = denpyoKbn;    // 伝票区分を設定

                dtFormattedData.Rows.Add(drTaishakuData);
            }

            dsTaishakuData.Tables.Add(dtFormattedData);

            return dsTaishakuData;
        }

        /// <summary>
        /// 貸借を仕訳したデータを取得(控除用)
        /// </summary>
        /// <param name="condition">条件画面の入力値</param>
        /// <param name="jdSwkSettingA">自動仕訳設定Ａ</param>
        /// <param name="jdSwkSettingB">自動仕訳設定Ｂ</param>
        /// <param name="zeiSetting">消費税設定</param>
        /// <param name="swkTgtData">仕訳対象データ</param>
        /// <returns>貸借を仕訳したデータ(1伝票あたり1DataTable)</returns>
        private DataSet GetKojoTaishakuData(Hashtable condition, DataTable jdSwkSettingA,
            DataTable jdSwkSettingB, DataTable zeiSetting, DataTable swkTgtData)
        {
            DataSet dsTaishakuData = new DataSet();
            DataTable dtTaishakuData;
            DataRow drTaishakuData;
            DataTable dtCommit;
            DataRow[] aryFormattedData;
            DataTable dtFormattedData;

            StringBuilder srcCond = new StringBuilder();
            DataRow[] aryAllRow;
            DataRow[] aryTaishakuRow;
            DataRow[] arySortedRow;
            DataRow[] aryDupRow;
            int prevRowNo;

            // 勘定科目に対する消費税を保持するワーク
            DataRow[] aryZeiRow;
            DataRow drColSrcZeiRow; // 収集される元の消費税行
            bool zeiSrcFlg = false;

            DataRow drSwkStgA;
            DataRow drSwkStgB;
            DataRow[] arySwkStgB;

            // 自動仕訳設定Ａの保持(1行のみ)
            if (jdSwkSettingA.Rows.Count > 0)
            {
                drSwkStgA = jdSwkSettingA.Rows[0];
            }
            else
            {
                drSwkStgA = null;
            }

            int rowNo = 0;
            string prevFunanushiCd = string.Empty;
            int prevKjno = 0;

            bool kazeiFlg;
            decimal tax;

            // 仮受消費税の設定の保持
            DataRow drKariukeZei = zeiSetting.Select("KBN = 1")[0];

            // 伝票区分（仕訳作成区分に設定）
            int denpyoKbn = Util.ToInt(((HNSB1021.SKbn)condition["SakuseiKbn"]).ToString("D"));

            // 仕訳伝票の作成方法によって処理を切り替える
            int swkDpMk = Util.ToInt(this._config.LoadPgConfig(Constants.SubSys.Han, "HNSB1021", "Setting", "SwkDpMk"));

            // 更新する際のレイアウトをベースに定義を作成
            dtTaishakuData = new DataTable();

            dtTaishakuData.Columns.Add("GYO_BANGO", typeof(decimal));
            dtTaishakuData.Columns.Add("SEIKYUSAKI_CD", typeof(decimal));
            dtTaishakuData.Columns.Add("TAISHAKU_KUBUN", typeof(decimal));
            dtTaishakuData.Columns.Add("MEISAI_KUBUN", typeof(decimal));
            dtTaishakuData.Columns.Add("DENPYO_KUBUN", typeof(decimal));
            dtTaishakuData.Columns.Add("DENPYO_DATE", typeof(DateTime));
            dtTaishakuData.Columns.Add("KANJO_KAMOKU_CD", typeof(decimal));
            dtTaishakuData.Columns.Add("KANJO_KAMOKU_NM", typeof(string));
            dtTaishakuData.Columns.Add("HOJO_KAMOKU_CD", typeof(decimal));
            dtTaishakuData.Columns.Add("HOJO_KAMOKU_NM", typeof(string));
            dtTaishakuData.Columns.Add("BUMON_CD", typeof(decimal));
            dtTaishakuData.Columns.Add("TEKIYO_CD", typeof(decimal));
            dtTaishakuData.Columns.Add("TEKIYO", typeof(string));
            dtTaishakuData.Columns.Add("ZEIKOMI_KINGAKU", typeof(decimal));
            dtTaishakuData.Columns.Add("ZEINUKI_KINGAKU", typeof(decimal));
            dtTaishakuData.Columns.Add("SHOHIZEI_KINGAKU", typeof(decimal));
            dtTaishakuData.Columns.Add("ZEI_KUBUN", typeof(decimal));
            dtTaishakuData.Columns.Add("KAZEI_KUBUN", typeof(decimal));
            dtTaishakuData.Columns.Add("TORIHIKI_KUBUN", typeof(decimal));
            dtTaishakuData.Columns.Add("ZEI_RITSU", typeof(decimal));
            dtTaishakuData.Columns.Add("JIGYO_KUBUN", typeof(decimal));
            dtTaishakuData.Columns.Add("SHOHIZEI_NYURYOKU_HOHO", typeof(decimal));
            dtTaishakuData.Columns.Add("SHOHIZEI_HENKO", typeof(decimal));
            dtTaishakuData.Columns.Add("KESSAN_KUBUN", typeof(decimal));
            dtTaishakuData.Columns.Add("SHIWAKE_SAKUSEI_KUBUN", typeof(decimal));
            dtTaishakuData.Columns.Add("DEL_FLG", typeof(decimal));

            // とりあえず1行ずつ貸借及び課税区分によって起票
            for (int i = 0; i < swkTgtData.Rows.Count; i++)
            {
                // 請求先別に伝票を起票する場合のブレイク処理
                if (swkDpMk == 3 && (!ValChk.IsEmpty(prevFunanushiCd) && !Util.ToString(swkTgtData.Rows[i]["SENSHU_CD"]).Equals(prevFunanushiCd)))
                {
                    // DataSetにDataTableを追加
                    dsTaishakuData.Tables.Add(dtTaishakuData);

                    // 次の請求先の貸借情報格納用に新たにDataTableを作成
                    dtTaishakuData = new DataTable();

                    dtTaishakuData.Columns.Add("GYO_BANGO", typeof(decimal));
                    dtTaishakuData.Columns.Add("SEIKYUSAKI_CD", typeof(decimal));
                    dtTaishakuData.Columns.Add("TAISHAKU_KUBUN", typeof(decimal));
                    dtTaishakuData.Columns.Add("MEISAI_KUBUN", typeof(decimal));
                    dtTaishakuData.Columns.Add("DENPYO_KUBUN", typeof(decimal));
                    dtTaishakuData.Columns.Add("DENPYO_DATE", typeof(DateTime));
                    dtTaishakuData.Columns.Add("KANJO_KAMOKU_CD", typeof(decimal));
                    dtTaishakuData.Columns.Add("KANJO_KAMOKU_NM", typeof(string));
                    dtTaishakuData.Columns.Add("HOJO_KAMOKU_CD", typeof(decimal));
                    dtTaishakuData.Columns.Add("HOJO_KAMOKU_NM", typeof(string));
                    dtTaishakuData.Columns.Add("BUMON_CD", typeof(decimal));
                    dtTaishakuData.Columns.Add("TEKIYO_CD", typeof(decimal));
                    dtTaishakuData.Columns.Add("TEKIYO", typeof(string));
                    dtTaishakuData.Columns.Add("ZEIKOMI_KINGAKU", typeof(decimal));
                    dtTaishakuData.Columns.Add("ZEINUKI_KINGAKU", typeof(decimal));
                    dtTaishakuData.Columns.Add("SHOHIZEI_KINGAKU", typeof(decimal));
                    dtTaishakuData.Columns.Add("ZEI_KUBUN", typeof(decimal));
                    dtTaishakuData.Columns.Add("KAZEI_KUBUN", typeof(decimal));
                    dtTaishakuData.Columns.Add("TORIHIKI_KUBUN", typeof(decimal));
                    dtTaishakuData.Columns.Add("ZEI_RITSU", typeof(decimal));
                    dtTaishakuData.Columns.Add("JIGYO_KUBUN", typeof(decimal));
                    dtTaishakuData.Columns.Add("SHOHIZEI_NYURYOKU_HOHO", typeof(decimal));
                    dtTaishakuData.Columns.Add("SHOHIZEI_HENKO", typeof(decimal));
                    dtTaishakuData.Columns.Add("KESSAN_KUBUN", typeof(decimal));
                    dtTaishakuData.Columns.Add("SHIWAKE_SAKUSEI_KUBUN", typeof(decimal));
                    dtTaishakuData.Columns.Add("DEL_FLG", typeof(decimal));

                    // 行番号をリセット
                    rowNo = 0;
                }

                if (Util.ToInt(swkTgtData.Rows[i]["kjno"]) == 0)
                {
                    rowNo++;

                    // 借方
                    // TODO:非課税前提でいい?
                    drTaishakuData = dtTaishakuData.NewRow();
                    drTaishakuData["GYO_BANGO"] = rowNo;
                    drTaishakuData["SEIKYUSAKI_CD"] = swkTgtData.Rows[i]["SENSHU_CD"];
                    drTaishakuData["TAISHAKU_KUBUN"] = 1;
                    drTaishakuData["MEISAI_KUBUN"] = 0;
                    drTaishakuData["DENPYO_KUBUN"] = 1; // TODO:1固定でいいかどうか
                    drTaishakuData["DENPYO_DATE"] = condition["SwkDpyDt"];
                    drTaishakuData["TEKIYO_CD"] = Util.ToDecimal(condition["TekiyoCd"]);
                    drTaishakuData["TEKIYO"] = condition["Tekiyo"];
                    drTaishakuData["ZEIKOMI_KINGAKU"] = swkTgtData.Rows[i]["金額"];
                    drTaishakuData["ZEINUKI_KINGAKU"] = swkTgtData.Rows[i]["金額"];
                    drTaishakuData["SHOHIZEI_KINGAKU"] = 0;
                    drTaishakuData["JIGYO_KUBUN"] = this._uInfo.KaikeiSettings["JIGYO_KUBUN"];
                    drTaishakuData["SHOHIZEI_NYURYOKU_HOHO"] = this._uInfo.KaikeiSettings["SHOHIZEI_NYURYOKU_HOHO"];
                    drTaishakuData["SHOHIZEI_HENKO"] = 0;   // TODO:固定?
                    drTaishakuData["KESSAN_KUBUN"] = 0;     // TODO:固定?
                    //drTaishakuData["SHIWAKE_SAKUSEI_KUBUN"] = 9;    // TODO:固定?
                    drTaishakuData["SHIWAKE_SAKUSEI_KUBUN"] = denpyoKbn;    // 伝票区分を設定

                    drTaishakuData["DEL_FLG"] = 0;
                    if (drSwkStgA != null)
                    {
                        drTaishakuData["KANJO_KAMOKU_CD"] = drSwkStgA["勘定科目コード"];
                        // 設定で差し替え
                        //drTaishakuData["HOJO_KAMOKU_CD"] = swkTgtData.Rows[i]["SENSHU_CD"];
                        drTaishakuData["HOJO_KAMOKU_CD"] = (Util.ToInt(Util.ToString(drSwkStgA["補助科目コード"])) == -1 ? swkTgtData.Rows[i]["SENSHU_CD"] : drSwkStgA["補助科目コード"]);
                        drTaishakuData["BUMON_CD"] = drSwkStgA["部門コード"];
                        drTaishakuData["ZEI_KUBUN"] = drSwkStgA["税区分"];
                        drTaishakuData["KAZEI_KUBUN"] = drSwkStgA["課税区分"];
                        drTaishakuData["TORIHIKI_KUBUN"] = drSwkStgA["取引区分"];
                        drTaishakuData["ZEI_RITSU"] = drSwkStgA["税率"];
                    }
                    else
                    {
                        drTaishakuData["KANJO_KAMOKU_CD"] = DBNull.Value;
                        drTaishakuData["HOJO_KAMOKU_CD"] = DBNull.Value;
                        drTaishakuData["BUMON_CD"] = DBNull.Value;
                        drTaishakuData["ZEI_KUBUN"] = DBNull.Value;
                        drTaishakuData["KAZEI_KUBUN"] = DBNull.Value;
                        drTaishakuData["TORIHIKI_KUBUN"] = DBNull.Value;
                        drTaishakuData["ZEI_RITSU"] = DBNull.Value;
                    }
                    dtTaishakuData.Rows.Add(drTaishakuData);
                }
                else
                {
                    if (prevKjno != 0)
                    {
                        // 船主単位で貸方の先頭行の場合は、借方と同じ行番号にする
                        rowNo++;
                    }

                    // 貸方
                    // 設定コードから設定を取得
                    arySwkStgB = jdSwkSettingB.Select("設定コード = " + swkTgtData.Rows[i]["kjno"]);
                    if (arySwkStgB.Length > 0)
                    {
                        drSwkStgB = arySwkStgB[0];
                        if (Util.ToInt(drSwkStgB["課税区分"]) == 1)
                        {
                            kazeiFlg = true;
                        }
                        else
                        {
                            kazeiFlg = false;
                        }
                    }
                    else
                    {
                        drSwkStgB = null;
                        kazeiFlg = false;
                    }

                    // 課税の場合、金額から消費税を計算する
                    if (kazeiFlg)
                    {
                        tax = Util.Round(Util.ToDecimal(swkTgtData.Rows[i]["金額"]) *
                            (Util.ToDecimal(drSwkStgB["税率"]) / (100 + Util.ToDecimal(drSwkStgB["税率"]))), 0);
                    }
                    else
                    {
                        tax = 0;
                    }

                    drTaishakuData = dtTaishakuData.NewRow();
                    drTaishakuData["GYO_BANGO"] = rowNo;
                    drTaishakuData["SEIKYUSAKI_CD"] = swkTgtData.Rows[i]["SENSHU_CD"];
                    drTaishakuData["TAISHAKU_KUBUN"] = 2;
                    drTaishakuData["MEISAI_KUBUN"] = 0;
                    drTaishakuData["DENPYO_KUBUN"] = 1; // TODO:1固定でいいかどうか
                    drTaishakuData["DENPYO_DATE"] = condition["SwkDpyDt"];
                    drTaishakuData["TEKIYO_CD"] = Util.ToDecimal(condition["TekiyoCd"]);
                    drTaishakuData["TEKIYO"] = condition["Tekiyo"];
                    drTaishakuData["ZEIKOMI_KINGAKU"] = swkTgtData.Rows[i]["金額"];
                    drTaishakuData["ZEINUKI_KINGAKU"] = Util.ToDecimal(swkTgtData.Rows[i]["金額"]) - tax;
                    drTaishakuData["SHOHIZEI_KINGAKU"] = tax;
                    drTaishakuData["JIGYO_KUBUN"] = this._uInfo.KaikeiSettings["JIGYO_KUBUN"];
                    drTaishakuData["SHOHIZEI_NYURYOKU_HOHO"] = this._uInfo.KaikeiSettings["SHOHIZEI_NYURYOKU_HOHO"];
                    drTaishakuData["SHOHIZEI_HENKO"] = 0;   // TODO:固定?
                    drTaishakuData["KESSAN_KUBUN"] = 0;     // TODO:固定?
                    //drTaishakuData["SHIWAKE_SAKUSEI_KUBUN"] = 9;    // TODO:固定?
                    drTaishakuData["SHIWAKE_SAKUSEI_KUBUN"] = denpyoKbn;    // 伝票区分を設定

                    drTaishakuData["DEL_FLG"] = 0;
                    if (drSwkStgB != null)
                    {
                        drTaishakuData["KANJO_KAMOKU_CD"] = drSwkStgB["勘定科目コード"];
                        // 設定内容で切り替え
                        // if (Util.ToDecimal(drSwkStgB["補助科目コード"]) <= 0) //控除科目の設定で設定している補助科目が0以下の場合　船主コードを入れる
                        if (Util.ToDecimal(drSwkStgB["補助科目コード"]) == -1) //控除科目の設定で設定している補助科目が0以下の場合　船主コードを入れる
                        {
                            drTaishakuData["HOJO_KAMOKU_CD"] = swkTgtData.Rows[i]["SENSHU_CD"]; 
                        }
                        else
                        {
                            drTaishakuData["HOJO_KAMOKU_CD"] = drSwkStgB["補助科目コード"];
                        }
                        drTaishakuData["BUMON_CD"] = drSwkStgB["部門コード"];
                        drTaishakuData["ZEI_KUBUN"] = drSwkStgB["貸方税区分"];
                        drTaishakuData["KAZEI_KUBUN"] = drSwkStgB["課税区分"];
                        drTaishakuData["TORIHIKI_KUBUN"] = drSwkStgB["取引区分"];
                        drTaishakuData["ZEI_RITSU"] = drSwkStgB["税率"];
                    }
                    else
                    {
                        drTaishakuData["KANJO_KAMOKU_CD"] = DBNull.Value;
                        drTaishakuData["HOJO_KAMOKU_CD"] = DBNull.Value;
                        drTaishakuData["BUMON_CD"] = DBNull.Value;
                        drTaishakuData["ZEI_KUBUN"] = DBNull.Value;
                        drTaishakuData["KAZEI_KUBUN"] = DBNull.Value;
                        drTaishakuData["TORIHIKI_KUBUN"] = DBNull.Value;
                        drTaishakuData["ZEI_RITSU"] = DBNull.Value;
                    }
                    dtTaishakuData.Rows.Add(drTaishakuData);

                    // 付随する消費税を仮受消費税として計上
                    if (kazeiFlg)
                    {
                        drTaishakuData = dtTaishakuData.NewRow();
                        drTaishakuData["GYO_BANGO"] = rowNo;
                        drTaishakuData["SEIKYUSAKI_CD"] = swkTgtData.Rows[i]["SENSHU_CD"];
                        drTaishakuData["TAISHAKU_KUBUN"] = 2;
                        drTaishakuData["MEISAI_KUBUN"] = 1;
                        drTaishakuData["DENPYO_KUBUN"] = 1; // TODO:1固定でいいかどうか
                        drTaishakuData["DENPYO_DATE"] = condition["SwkDpyDt"];
                        drTaishakuData["BUMON_CD"] = 0;
                        drTaishakuData["TEKIYO_CD"] = Util.ToDecimal(condition["TekiyoCd"]);
                        drTaishakuData["TEKIYO"] = condition["Tekiyo"];
                        drTaishakuData["ZEIKOMI_KINGAKU"] = 0;
                        drTaishakuData["ZEINUKI_KINGAKU"] = tax;
                        drTaishakuData["SHOHIZEI_KINGAKU"] = 0;
                        drTaishakuData["JIGYO_KUBUN"] = this._uInfo.KaikeiSettings["JIGYO_KUBUN"];
                        drTaishakuData["SHOHIZEI_NYURYOKU_HOHO"] = this._uInfo.KaikeiSettings["SHOHIZEI_NYURYOKU_HOHO"];
                        drTaishakuData["SHOHIZEI_HENKO"] = 0;   // TODO:固定?
                        drTaishakuData["KESSAN_KUBUN"] = 0;     // TODO:固定?
                        //drTaishakuData["SHIWAKE_SAKUSEI_KUBUN"] = 9;    // TODO:固定?
                        drTaishakuData["SHIWAKE_SAKUSEI_KUBUN"] = denpyoKbn;    // 伝票区分を設定

                        drTaishakuData["DEL_FLG"] = 0;

                        drTaishakuData["KANJO_KAMOKU_CD"] = drKariukeZei["KANJO_KAMOKU_CD"];
                        drTaishakuData["HOJO_KAMOKU_CD"] = 0;
                        drTaishakuData["ZEI_KUBUN"] = drKariukeZei["KASHIKATA_ZEI_KUBUN"];
                        drTaishakuData["KAZEI_KUBUN"] = drKariukeZei["KASHI_KAZEI_KUBUN"];
                        drTaishakuData["TORIHIKI_KUBUN"] = drKariukeZei["KASHI_TORIHIKI_KUBUN"];
                        drTaishakuData["ZEI_RITSU"] = drKariukeZei["KASHI_ZEI_RITSU"];

                        dtTaishakuData.Rows.Add(drTaishakuData);
                    }
                }

                // 会員番号の退避
                prevFunanushiCd = Util.ToString(swkTgtData.Rows[i]["SENSHU_CD"]);
                // 控除項目NOの退避
                prevKjno = Util.ToInt(swkTgtData.Rows[i]["kjno"]);
            }

            if (swkDpMk == 1)
            {
                // 複合仕訳の場合、貸借の科目単位でまとめ上げる
                arySortedRow = dtTaishakuData.Select(null, "GYO_BANGO, TAISHAKU_KUBUN, MEISAI_KUBUN");

                for (int j = 0; j < arySortedRow.Length; j++)
                {
                    // 既に削除された行は評価しない
                    if (Util.ToInt(arySortedRow[j]["DEL_FLG"]) == 1) continue;

                    // 集約される元の消費税行の情報をクリア
                    drColSrcZeiRow = null;
                    zeiSrcFlg = false;

                    // 同一の貸借区分・勘定科目コード・補助科目コード・部門コード・税区分・事業区分でまとめ上げる
                    // 消費税行は上記に従属させる
                    // 本体行
                    srcCond = new StringBuilder();
                    srcCond.Append("TAISHAKU_KUBUN = " + Util.ToString(arySortedRow[j]["TAISHAKU_KUBUN"]));
                    srcCond.Append(" AND MEISAI_KUBUN = " + Util.ToString(arySortedRow[j]["MEISAI_KUBUN"]));
                    srcCond.Append(" AND KANJO_KAMOKU_CD = " + Util.ToString(arySortedRow[j]["KANJO_KAMOKU_CD"]));
                    srcCond.Append(" AND HOJO_KAMOKU_CD = " + Util.ToString(arySortedRow[j]["HOJO_KAMOKU_CD"]));
                    srcCond.Append(" AND BUMON_CD = " + Util.ToString(arySortedRow[j]["BUMON_CD"]));
                    srcCond.Append(" AND ZEI_KUBUN = " + Util.ToString(arySortedRow[j]["ZEI_KUBUN"]));
                    srcCond.Append(" AND JIGYO_KUBUN = " + Util.ToString(arySortedRow[j]["JIGYO_KUBUN"]));

                    aryDupRow = dtTaishakuData.Select(srcCond.ToString(), "GYO_BANGO, TAISHAKU_KUBUN, MEISAI_KUBUN");

                    // 重複した1行目の情報に2,3行目の金額を加算していく
                    for (int k = 0; k < aryDupRow.Length; k++)
                    {
                        zeiSrcFlg = false;

                        // 既に削除された行は評価しない
                        if (Util.ToInt(aryDupRow[k]["DEL_FLG"]) == 1) continue;

                        // 現在の行に従属する消費税行を取得
                        srcCond = new StringBuilder();
                        srcCond.Append("GYO_BANGO = " + Util.ToString(aryDupRow[k]["GYO_BANGO"]));
                        srcCond.Append(" AND TAISHAKU_KUBUN = " + Util.ToString(aryDupRow[k]["TAISHAKU_KUBUN"]));
                        srcCond.Append(" AND MEISAI_KUBUN = 1");
                        aryZeiRow = dtTaishakuData.Select(srcCond.ToString());

                        if (drColSrcZeiRow == null && aryZeiRow.Length > 0)
                        {
                            // 取得した行を集約される元の消費税行とする
                            drColSrcZeiRow = aryZeiRow[0];
                            zeiSrcFlg = true;
                        }

                        // 自身の行は読み飛ばす
                        if (k == 0)
                        {
                            // 行に持っている請求先コードをクリア
                            aryDupRow[k]["SEIKYUSAKI_CD"] = DBNull.Value;
                            continue;
                        }

                        aryDupRow[0]["ZEIKOMI_KINGAKU"] = Util.ToDecimal(aryDupRow[0]["ZEIKOMI_KINGAKU"]) + Util.ToDecimal(aryDupRow[k]["ZEIKOMI_KINGAKU"]);
                        aryDupRow[0]["ZEINUKI_KINGAKU"] = Util.ToDecimal(aryDupRow[0]["ZEINUKI_KINGAKU"]) + Util.ToDecimal(aryDupRow[k]["ZEINUKI_KINGAKU"]);
                        aryDupRow[0]["SHOHIZEI_KINGAKU"] = Util.ToDecimal(aryDupRow[0]["SHOHIZEI_KINGAKU"]) + Util.ToDecimal(aryDupRow[k]["SHOHIZEI_KINGAKU"]);
                        aryDupRow[k]["DEL_FLG"] = 1;

                        if (drColSrcZeiRow != null && aryZeiRow.Length > 0 && !zeiSrcFlg)
                        {
                            // 消費税行を積み上げる
                            drColSrcZeiRow["ZEIKOMI_KINGAKU"] = Util.ToDecimal(drColSrcZeiRow["ZEIKOMI_KINGAKU"]) + Util.ToDecimal(aryZeiRow[0]["ZEIKOMI_KINGAKU"]);
                            drColSrcZeiRow["ZEINUKI_KINGAKU"] = Util.ToDecimal(drColSrcZeiRow["ZEINUKI_KINGAKU"]) + Util.ToDecimal(aryZeiRow[0]["ZEINUKI_KINGAKU"]);
                            drColSrcZeiRow["SHOHIZEI_KINGAKU"] = Util.ToDecimal(drColSrcZeiRow["SHOHIZEI_KINGAKU"]) + Util.ToDecimal(aryZeiRow[0]["SHOHIZEI_KINGAKU"]);
                            aryZeiRow[0]["DEL_FLG"] = 1;
                        }
                    }
                }
            }

            // 削除フラグが1の行を削除(一旦削除確定用テンポラリテーブルに詰める)
            dtCommit = dtTaishakuData.Clone();
            foreach (DataRow row in dtTaishakuData.Rows)
            {
                if (Util.ToInt(row["DEL_FLG"]) == 0) dtCommit.ImportRow(row);
            }
            dtTaishakuData.Clear();
            dtTaishakuData = dtCommit.Copy();

            // 消し込んだ結果を整理する(上の行に詰める)
            if (swkDpMk == 1)
            {
                // 複合仕訳の場合、貸借それぞれ上に詰める。
                // 借方
                aryTaishakuRow = dtTaishakuData.Select("TAISHAKU_KUBUN = 1", "GYO_BANGO, MEISAI_KUBUN");
                prevRowNo = 0;
                for (int j = 0; j < aryTaishakuRow.Length; j++)
                {
                    // 現在保持している行番号が前に振った行番号+1、あるいは変わっていない場合は特に
                    // 変更しない。飛んでる場合だけ変更する
                    if (Util.ToInt(aryTaishakuRow[j]["GYO_BANGO"]) > prevRowNo + 1)
                    {
                        aryTaishakuRow[j]["GYO_BANGO"] = prevRowNo + 1;
                    }

                    prevRowNo = Util.ToInt(aryTaishakuRow[j]["GYO_BANGO"]);
                }

                // 貸方
                aryTaishakuRow = dtTaishakuData.Select("TAISHAKU_KUBUN = 2", "GYO_BANGO, MEISAI_KUBUN");
                prevRowNo = 0;
                for (int j = 0; j < aryTaishakuRow.Length; j++)
                {
                    // 現在保持している行番号が前に振った行番号+1、あるいは変わっていない場合は特に
                    // 変更しない。飛んでる場合だけ変更する
                    if (Util.ToInt(aryTaishakuRow[j]["GYO_BANGO"]) > prevRowNo + 1)
                    {
                        aryTaishakuRow[j]["GYO_BANGO"] = prevRowNo + 1;
                    }

                    prevRowNo = Util.ToInt(aryTaishakuRow[j]["GYO_BANGO"]);
                }
            }
            else
            {
                // 複合仕訳でない場合、請求先単位で貸借それぞれ上に詰める
                aryAllRow = dtTaishakuData.Select(null, "GYO_BANGO, MEISAI_KUBUN");
                prevFunanushiCd = string.Empty;
                for (int j = 0; j < aryAllRow.Length; j++)
                {
                    if (!prevFunanushiCd.Equals(Util.ToString(aryAllRow[j]["SEIKYUSAKI_CD"])))
                    {
                        // 借方
                        aryTaishakuRow = dtTaishakuData.Select(
                            "SEIKYUSAKI_CD = " + Util.ToString(aryAllRow[j]["SEIKYUSAKI_CD"]) + " AND TAISHAKU_KUBUN = 1",
                            "GYO_BANGO, MEISAI_KUBUN");
                        if (j == 0)
                        {
                            prevRowNo = 0;
                        }
                        else
                        {
                            prevRowNo = Util.ToInt(aryAllRow[j - 1]["GYO_BANGO"]);
                        }
                        for (int k = 0; k < aryTaishakuRow.Length; k++)
                        {
                            // 現在保持している行番号が前に振った行番号+1、あるいは変わっていない場合は特に
                            // 変更しない。飛んでる場合だけ変更する
                            if (Util.ToInt(aryTaishakuRow[k]["GYO_BANGO"]) > prevRowNo + 1)
                            {
                                aryTaishakuRow[k]["GYO_BANGO"] = prevRowNo + 1;
                            }

                            prevRowNo = Util.ToInt(aryTaishakuRow[k]["GYO_BANGO"]);
                        }

                        // 貸方
                        aryTaishakuRow = dtTaishakuData.Select(
                            "SEIKYUSAKI_CD = " + Util.ToString(aryAllRow[j]["SEIKYUSAKI_CD"]) + " AND TAISHAKU_KUBUN = 2",
                            "GYO_BANGO, MEISAI_KUBUN");
                        if (j == 0)
                        {
                            prevRowNo = 0;
                        }
                        else
                        {
                            prevRowNo = Util.ToInt(aryAllRow[j - 1]["GYO_BANGO"]);
                        }
                        for (int k = 0; k < aryTaishakuRow.Length; k++)
                        {
                            // 現在保持している行番号が前に振った行番号+1、あるいは変わっていない場合は特に
                            // 変更しない。飛んでる場合だけ変更する
                            if (Util.ToInt(aryTaishakuRow[k]["GYO_BANGO"]) > prevRowNo + 1)
                            {
                                aryTaishakuRow[k]["GYO_BANGO"] = prevRowNo + 1;
                            }

                            prevRowNo = Util.ToInt(aryTaishakuRow[k]["GYO_BANGO"]);
                        }
                    }

                    prevFunanushiCd = Util.ToString(aryAllRow[j]["SEIKYUSAKI_CD"]);
                }
            }

            // 最後に整列して格納
            aryFormattedData = dtTaishakuData.Select(null, "GYO_BANGO, TAISHAKU_KUBUN, MEISAI_KUBUN");
            dtFormattedData = dtTaishakuData.Clone();
            for (int j = 0; j < aryFormattedData.Length; j++)
            {
                dtFormattedData.ImportRow(aryFormattedData[j]);
            }

            dsTaishakuData.Tables.Add(dtFormattedData);

            return dsTaishakuData;
        }

        /// <summary>
        /// 貸借を仕訳したデータを取得(支払用)
        /// </summary>
        /// <param name="condition">条件画面の入力値</param>
        /// <param name="jdSwkSettingA">自動仕訳設定Ａ</param>
        /// <param name="jdSwkSettingB">自動仕訳設定Ｂ</param>
        /// <param name="swkTgtData">仕訳対象データ</param>
        /// <returns>貸借を仕訳したデータ(1伝票あたり1DataTable)</returns>
        private DataSet GetShiharaiTaishakuData(Hashtable condition, DataTable jdSwkSettingA,
            DataTable jdSwkSettingB, DataTable swkTgtData)
        {
            DataSet dsTaishakuData = new DataSet();
            DataTable dtTaishakuData;
            DataRow drTaishakuData;
            DataTable dtCommit;
            DataRow[] aryFormattedData;
            DataTable dtFormattedData;

            StringBuilder srcCond = new StringBuilder();
            DataRow[] aryTaishakuRow;
            DataRow[] arySortedRow;
            DataRow[] aryDupRow;
            int prevRowNo;

            DataRow drSwkStgA;
            DataRow drSwkStgB;

            int rowNo = 0;
            string prevFunanushiCd = string.Empty;

            int denpyoKbn = Util.ToInt(((HNSB1021.SKbn)condition["SakuseiKbn"]).ToString("D"));

            // 未使用
            //// 会社情報から消費税を取得する
            //Decimal taxRate = Util.ToDecimal(this._uInfo.KaikeiSettings["SHIN_SHOHIZEI_RITSU"]) / 100;

            // 未使用
            //// 消費税情報を取得する
            //DbParamCollection dpc = new DbParamCollection();
            //dpc.SetParam("@KESSANKI", SqlDbType.Decimal, 3, this._uInfo.KessanKi);
            //DataTable dtShohizeiJoho = this._dba.GetDataTableByConditionWithParams("*", "TB_ZM_SHOHIZEI_JOHO", "KESSANKI = @KESSANKI", dpc);

            // 未使用
            //// 販売雑費用の情報を取得する
            //dpc = new DbParamCollection();
            //dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
            //dpc.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 4, KJ_KMK_CD_ZAT_HIY);
            //DataTable dtZatsuHiyo = this._dba.GetDataTableByConditionWithParams("*", "VI_ZM_KANJO_KAMOKU",
            //    "KAIKEI_NENDO = @KAIKEI_NENDO AND KANJO_KAMOKU_CD = @KANJO_KAMOKU_CD", dpc);

            // 未使用
            //// 販売雑収益の情報を取得する
            //dpc = new DbParamCollection();
            //dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
            //dpc.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 4, KJ_KMK_CD_ZAT_SEK);
            //DataTable dtZatsuShueki = this._dba.GetDataTableByConditionWithParams("*", "VI_ZM_KANJO_KAMOKU",
            //    "KAIKEI_NENDO = @KAIKEI_NENDO AND KANJO_KAMOKU_CD = @KANJO_KAMOKU_CD", dpc);

            // 更新する際のレイアウトをベースに定義を作成
            dtTaishakuData = new DataTable();

            dtTaishakuData.Columns.Add("GYO_BANGO", typeof(decimal));
            dtTaishakuData.Columns.Add("KEIJO_NENGAPPI", typeof(DateTime));
            dtTaishakuData.Columns.Add("TAISHAKU_KUBUN", typeof(decimal));
            dtTaishakuData.Columns.Add("MEISAI_KUBUN", typeof(decimal));
            dtTaishakuData.Columns.Add("DENPYO_KUBUN", typeof(decimal));
            dtTaishakuData.Columns.Add("DENPYO_DATE", typeof(DateTime));
            dtTaishakuData.Columns.Add("KANJO_KAMOKU_CD", typeof(decimal));
            dtTaishakuData.Columns.Add("KANJO_KAMOKU_NM", typeof(string));
            dtTaishakuData.Columns.Add("HOJO_KAMOKU_CD", typeof(decimal));
            dtTaishakuData.Columns.Add("HOJO_KAMOKU_NM", typeof(string));
            dtTaishakuData.Columns.Add("BUMON_CD", typeof(decimal));
            dtTaishakuData.Columns.Add("TEKIYO_CD", typeof(decimal));
            dtTaishakuData.Columns.Add("TEKIYO", typeof(string));
            dtTaishakuData.Columns.Add("ZEIKOMI_KINGAKU", typeof(decimal));
            dtTaishakuData.Columns.Add("ZEINUKI_KINGAKU", typeof(decimal));
            dtTaishakuData.Columns.Add("SHOHIZEI_KINGAKU", typeof(decimal));
            dtTaishakuData.Columns.Add("ZEI_KUBUN", typeof(decimal));
            dtTaishakuData.Columns.Add("KAZEI_KUBUN", typeof(decimal));
            dtTaishakuData.Columns.Add("TORIHIKI_KUBUN", typeof(decimal));
            dtTaishakuData.Columns.Add("ZEI_RITSU", typeof(decimal));
            dtTaishakuData.Columns.Add("JIGYO_KUBUN", typeof(decimal));
            dtTaishakuData.Columns.Add("SHOHIZEI_NYURYOKU_HOHO", typeof(decimal));
            dtTaishakuData.Columns.Add("SHOHIZEI_HENKO", typeof(decimal));
            dtTaishakuData.Columns.Add("KESSAN_KUBUN", typeof(decimal));
            dtTaishakuData.Columns.Add("SHIWAKE_SAKUSEI_KUBUN", typeof(decimal));
            dtTaishakuData.Columns.Add("CHK_ZUMI", typeof(decimal));
            dtTaishakuData.Columns.Add("DEL_FLG", typeof(decimal));

            // とりあえず1行ずつ貸借及び課税区分によって起票
            for (int i = 0; i < swkTgtData.Rows.Count; i++)
            {
                rowNo++;

                // 借方
                // 仕訳コードから自動仕訳設定Ｂ.仕訳コードに該当するレコードを引き当て
                drSwkStgB = jdSwkSettingB.Select("1 = " + 1)[0];

                drTaishakuData = dtTaishakuData.NewRow();
                drTaishakuData["GYO_BANGO"] = rowNo;
                drTaishakuData["TAISHAKU_KUBUN"] = 1;
                drTaishakuData["MEISAI_KUBUN"] = 0;
                drTaishakuData["DENPYO_KUBUN"] = 1; // TODO:1固定でいいかどうか
                drTaishakuData["DENPYO_DATE"] = condition["SwkDpyDt"];
                drTaishakuData["TEKIYO_CD"] = Util.ToDecimal(condition["TekiyoCd"]);
                drTaishakuData["TEKIYO"] = condition["Tekiyo"];
                // 結果としては両方とも税込金額にならなければならないが、税抜金額を一旦セットする。集約するタイミングで税計算をする
                drTaishakuData["ZEIKOMI_KINGAKU"] = Util.ToDecimal(swkTgtData.Rows[i]["KINGAKU"]);
                drTaishakuData["ZEINUKI_KINGAKU"] = Util.ToDecimal(swkTgtData.Rows[i]["KINGAKU"]);
                drTaishakuData["SHOHIZEI_KINGAKU"] = 0;
                drTaishakuData["ZEI_KUBUN"] = 0;
                drTaishakuData["KAZEI_KUBUN"] = 0;
                drTaishakuData["TORIHIKI_KUBUN"] = 0;
                drTaishakuData["ZEI_RITSU"] = 0;
                //drTaishakuData["SHOHIZEI_NYURYOKU_HOHO"] = dtShohizeiJoho.Rows[0]["SHOHIZEI_NYURYOKU_HOHO"];
                drTaishakuData["SHOHIZEI_NYURYOKU_HOHO"] = this._uInfo.KaikeiSettings["SHOHIZEI_NYURYOKU_HOHO"];
                drTaishakuData["SHOHIZEI_HENKO"] = 0;   // TODO:固定?
                drTaishakuData["KESSAN_KUBUN"] = 0;     // TODO:固定?
                //drTaishakuData["SHIWAKE_SAKUSEI_KUBUN"] = 9;    // TODO:固定?
                drTaishakuData["SHIWAKE_SAKUSEI_KUBUN"] = denpyoKbn;    // 伝票区分を設定

                drTaishakuData["CHK_ZUMI"] = 0;
                drTaishakuData["DEL_FLG"] = 0;
                if (drSwkStgB != null)
                {
                    drTaishakuData["BUMON_CD"] = drSwkStgB["BUMON_CD"];
                    drTaishakuData["KANJO_KAMOKU_CD"] = drSwkStgB["KANJO_KAMOKU_CD"];

                    drTaishakuData["HOJO_KAMOKU_CD"] = swkTgtData.Rows[i]["SENSHU_CD"];
                    drTaishakuData["HOJO_KAMOKU_NM"] = swkTgtData.Rows[i]["FUNANUSHI_NM"];

                    drTaishakuData["JIGYO_KUBUN"] = drSwkStgB["JIGYO_KUBUN"];
                }
                else
                {
                    drTaishakuData["BUMON_CD"] = DBNull.Value;
                    drTaishakuData["KANJO_KAMOKU_CD"] = DBNull.Value;
                    drTaishakuData["KANJO_KAMOKU_NM"] = DBNull.Value;
                    drTaishakuData["HOJO_KAMOKU_CD"] = DBNull.Value;
                    drTaishakuData["HOJO_KAMOKU_NM"] = DBNull.Value;
                    drTaishakuData["JIGYO_KUBUN"] = DBNull.Value;
                }
                dtTaishakuData.Rows.Add(drTaishakuData);

                // 貸方
                // 取引区分１、２から自動仕訳設定Ａ.仕訳コードに該当するレコードを引き当て
                drSwkStgA = jdSwkSettingA.Select("1 = " + 1)[0];

                drTaishakuData = dtTaishakuData.NewRow();
                drTaishakuData["GYO_BANGO"] = rowNo;
                drTaishakuData["TAISHAKU_KUBUN"] = 2;
                drTaishakuData["MEISAI_KUBUN"] = 0;
                drTaishakuData["DENPYO_KUBUN"] = 1; // TODO:1固定でいいかどうか
                drTaishakuData["DENPYO_DATE"] = condition["SwkDpyDt"];
                drTaishakuData["TEKIYO_CD"] = Util.ToDecimal(condition["TekiyoCd"]);
                drTaishakuData["TEKIYO"] = condition["Tekiyo"];
                drTaishakuData["ZEIKOMI_KINGAKU"] = Util.ToDecimal(swkTgtData.Rows[i]["KINGAKU"]);
                drTaishakuData["ZEINUKI_KINGAKU"] = Util.ToDecimal(swkTgtData.Rows[i]["KINGAKU"]);
                drTaishakuData["SHOHIZEI_KINGAKU"] = 0;

                drTaishakuData["ZEI_KUBUN"] = 0;
                drTaishakuData["KAZEI_KUBUN"] = 0;
                drTaishakuData["TORIHIKI_KUBUN"] = 0;
                drTaishakuData["ZEI_RITSU"] = 0;

                //drTaishakuData["SHOHIZEI_NYURYOKU_HOHO"] = dtShohizeiJoho.Rows[0]["SHOHIZEI_NYURYOKU_HOHO"];
                drTaishakuData["SHOHIZEI_NYURYOKU_HOHO"] = this._uInfo.KaikeiSettings["SHOHIZEI_NYURYOKU_HOHO"];

                drTaishakuData["SHOHIZEI_HENKO"] = 0;   // TODO:固定?
                drTaishakuData["KESSAN_KUBUN"] = 0;     // TODO:固定?
                //drTaishakuData["SHIWAKE_SAKUSEI_KUBUN"] = 9;    // TODO:固定?
                drTaishakuData["SHIWAKE_SAKUSEI_KUBUN"] = denpyoKbn;    // 伝票区分を設定

                drTaishakuData["CHK_ZUMI"] = 0;
                drTaishakuData["DEL_FLG"] = 0;
                if (drSwkStgA != null)
                {
                    drTaishakuData["BUMON_CD"] = drSwkStgA["BUMON_CD"];
                    drTaishakuData["KANJO_KAMOKU_CD"] = drSwkStgA["KANJO_KAMOKU_CD"];

                    drTaishakuData["HOJO_KAMOKU_CD"] = drSwkStgA["HOJO_KAMOKU_CD"];

                    drTaishakuData["JIGYO_KUBUN"] = drSwkStgA["JIGYO_KUBUN"];

                    dtTaishakuData.Rows.Add(drTaishakuData);
                }
                else
                {
                    drTaishakuData["BUMON_CD"] = DBNull.Value;
                    drTaishakuData["KANJO_KAMOKU_CD"] = DBNull.Value;
                    drTaishakuData["KANJO_KAMOKU_NM"] = DBNull.Value;
                    drTaishakuData["HOJO_KAMOKU_CD"] = DBNull.Value;
                    drTaishakuData["HOJO_KAMOKU_NM"] = DBNull.Value;
                    drTaishakuData["JIGYO_KUBUN"] = DBNull.Value;
                }

                // 会員番号の退避
                prevFunanushiCd = Util.ToString(swkTgtData.Rows[i]["SENSHU_CD"]);
            }

            // 貸借の科目単位でまとめ上げる
            arySortedRow = dtTaishakuData.Select(null, "GYO_BANGO, TAISHAKU_KUBUN, MEISAI_KUBUN");

            for (int j = 0; j < arySortedRow.Length; j++)
            {
                // 既に削除された行は評価しない
                if (Util.ToInt(arySortedRow[j]["DEL_FLG"]) == 1) continue;

                // 同一の計上年月日・貸借区分・明細区分・勘定科目コード・補助科目コード・部門コード・税区分・事業区分で
                // 金額をまとめ上げる
                // ※チェック済みのデータは参照しない
                srcCond = new StringBuilder();
                srcCond.Append("KEIJO_NENGAPPI = '" + Util.ToDate(arySortedRow[j]["KEIJO_NENGAPPI"]).ToString("yyyy/MM/dd") + "'");
                srcCond.Append(" AND TAISHAKU_KUBUN = " + Util.ToString(arySortedRow[j]["TAISHAKU_KUBUN"]));
                srcCond.Append(" AND MEISAI_KUBUN = " + Util.ToString(arySortedRow[j]["MEISAI_KUBUN"]));
                srcCond.Append(" AND KANJO_KAMOKU_CD = " + Util.ToString(arySortedRow[j]["KANJO_KAMOKU_CD"]));
                srcCond.Append(" AND HOJO_KAMOKU_CD = " + Util.ToString(arySortedRow[j]["HOJO_KAMOKU_CD"]));
                srcCond.Append(" AND BUMON_CD = " + Util.ToString(arySortedRow[j]["BUMON_CD"]));
                srcCond.Append(" AND ZEI_KUBUN = " + Util.ToString(arySortedRow[j]["ZEI_KUBUN"]));
                srcCond.Append(" AND JIGYO_KUBUN = " + Util.ToString(arySortedRow[j]["JIGYO_KUBUN"]));

                // 削除済み読み飛ばし不具合調整
                srcCond.Append(" AND DEL_FLG = 0 ");
                
                aryDupRow = dtTaishakuData.Select(srcCond.ToString(), "GYO_BANGO, TAISHAKU_KUBUN, MEISAI_KUBUN");

                // 重複した1行目の情報に2,3行目の金額を加算していく
                for (int k = 0; k < aryDupRow.Length; k++)
                {
                    // 自身の行は読み飛ばす
                    if (k == 0) continue;

                    // 既に削除された行は評価しない
                    if (Util.ToInt(aryDupRow[k]["DEL_FLG"]) == 1) continue;

                    aryDupRow[0]["ZEIKOMI_KINGAKU"] = Util.ToDecimal(aryDupRow[0]["ZEIKOMI_KINGAKU"]) + Util.ToDecimal(aryDupRow[k]["ZEIKOMI_KINGAKU"]);
                    aryDupRow[0]["ZEINUKI_KINGAKU"] = Util.ToDecimal(aryDupRow[0]["ZEINUKI_KINGAKU"]) + Util.ToDecimal(aryDupRow[k]["ZEINUKI_KINGAKU"]);
                    aryDupRow[0]["SHOHIZEI_KINGAKU"] = Util.ToDecimal(aryDupRow[0]["SHOHIZEI_KINGAKU"]) + Util.ToDecimal(aryDupRow[k]["SHOHIZEI_KINGAKU"]);
                    aryDupRow[k]["DEL_FLG"] = 1;
                }
            }

            // 削除フラグが1の行を削除(一旦削除確定用テンポラリテーブルに詰める)
            dtCommit = dtTaishakuData.Clone();
            foreach (DataRow row in dtTaishakuData.Rows)
            {
                if (Util.ToInt(row["DEL_FLG"]) == 0) dtCommit.ImportRow(row);
            }
            dtTaishakuData.Clear();
            dtTaishakuData = dtCommit.Copy();

            // 貸借それぞれ上に詰める
            // 借方
            aryTaishakuRow = dtTaishakuData.Select("TAISHAKU_KUBUN = 1", "GYO_BANGO, MEISAI_KUBUN");
            prevRowNo = 0;
            for (int j = 0; j < aryTaishakuRow.Length; j++)
            {
                // 現在保持している行番号が前に振った行番号+1、あるいは変わっていない場合は特に
                // 変更しない。飛んでる場合だけ変更する
                if (Util.ToInt(aryTaishakuRow[j]["GYO_BANGO"]) > prevRowNo + 1)
                {
                    aryTaishakuRow[j]["GYO_BANGO"] = prevRowNo + 1;
                }

                prevRowNo = Util.ToInt(aryTaishakuRow[j]["GYO_BANGO"]);
            }

            // 貸方
            aryTaishakuRow = dtTaishakuData.Select("TAISHAKU_KUBUN = 2", "GYO_BANGO, MEISAI_KUBUN");
            prevRowNo = 0;
            for (int j = 0; j < aryTaishakuRow.Length; j++)
            {
                // 現在保持している行番号が前に振った行番号+1、あるいは変わっていない場合は特に
                // 変更しない。飛んでる場合だけ変更する
                if (Util.ToInt(aryTaishakuRow[j]["GYO_BANGO"]) > prevRowNo + 1)
                {
                    aryTaishakuRow[j]["GYO_BANGO"] = prevRowNo + 1;
                }

                prevRowNo = Util.ToInt(aryTaishakuRow[j]["GYO_BANGO"]);
            }

            // 最後に整列して格納
            aryFormattedData = dtTaishakuData.Select(null, "GYO_BANGO, TAISHAKU_KUBUN, MEISAI_KUBUN");
            dtFormattedData = dtTaishakuData.Clone();
            for (int j = 0; j < aryFormattedData.Length; j++)
            {
                dtFormattedData.ImportRow(aryFormattedData[j]);
            }

            dsTaishakuData.Tables.Add(dtFormattedData);

            return dsTaishakuData;
        }

        /// <summary>
        /// 仕訳対象データを取得(控除)
        /// </summary>
        /// <param name="mode">処理モード(1:新規、2:更新)</param>
        /// <param name="condition">条件画面の入力値</param>
        /// <returns>仕訳対象データ</returns>
        private DataTable GetKojoSwkTgtData(int mode, Hashtable condition)
        {

            // 控除の特殊パターン判定
            int KojoIrregularPtn = 1;
            try
            {
                KojoIrregularPtn = Util.ToInt(this._config.LoadPgConfig(Constants.SubSys.Han, "HNSB1021", "Setting", "KojoIrregularPtn"));
            }
            catch (Exception)
            {
                KojoIrregularPtn = 1;
            }

            StringBuilder sql = new StringBuilder();
            if (KojoIrregularPtn == 2)
            {
                // 名護（控除項目４を除外）
                sql.Append("SELECT " + Environment.NewLine);
                sql.Append("  SENSHU_CD " + Environment.NewLine);
                sql.Append(" ,0 AS kjno " + Environment.NewLine);
                sql.Append(" ,SUM(KOJO_KOMOKU1) + SUM(KOJO_KOMOKU2) + SUM(KOJO_KOMOKU3) + SUM(KOJO_KOMOKU5) " + Environment.NewLine);
                sql.Append("  + SUM(KOJO_KOMOKU6) + SUM(KOJO_KOMOKU7) + SUM(KOJO_KOMOKU8) + SUM(KOJO_KOMOKU9) + SUM(KOJO_KOMOKU10) " + Environment.NewLine);
                sql.Append("  + SUM(KOJO_KOMOKU11) + SUM(KOJO_KOMOKU12) + SUM(KOJO_KOMOKU13) + SUM(KOJO_KOMOKU14) + SUM(KOJO_KOMOKU15) " + Environment.NewLine);
                sql.Append("  + SUM(KOJO_KOMOKU16) + SUM(KOJO_KOMOKU17) + SUM(KOJO_KOMOKU18) + SUM(KOJO_KOMOKU19) + SUM(KOJO_KOMOKU20) " + Environment.NewLine);
                sql.Append("  + SUM(KOJO_KOMOKU21) + SUM(KOJO_KOMOKU22) + SUM(KOJO_KOMOKU23) + SUM(KOJO_KOMOKU24) + SUM(KOJO_KOMOKU25) " + Environment.NewLine);
                sql.Append("  + SUM(KOJO_KOMOKU26) + SUM(KOJO_KOMOKU27) + SUM(KOJO_KOMOKU28) + SUM(KOJO_KOMOKU29) + SUM(KOJO_KOMOKU30) " + Environment.NewLine);
                sql.Append("  + SUM(KOJO_KOMOKU31) + SUM(KOJO_KOMOKU32) + SUM(KOJO_KOMOKU33) + SUM(KOJO_KOMOKU34) + SUM(KOJO_KOMOKU35) " + Environment.NewLine);
                sql.Append("  AS 金額 " + Environment.NewLine);
                sql.Append("FROM " + Environment.NewLine);
                sql.Append("  TB_HN_KOJO_DATA " + Environment.NewLine);
                sql.Append("WHERE " + Environment.NewLine);
                sql.Append("    KAISHA_CD = @KAISHA_CD " + Environment.NewLine);
                sql.Append("AND SHISHO_CD = @SHISHO_CD " + Environment.NewLine);
                sql.Append("AND KAIKEI_NENDO = @KAIKEI_NENDO " + Environment.NewLine);
                sql.Append("AND SEISAN_BI BETWEEN @SEISAN_BI_FR AND @SEISAN_BI_TO " + Environment.NewLine);
                sql.Append("AND SENSHU_CD BETWEEN @FUNANUSHI_CD_FR AND @FUNANUSHI_CD_TO " + Environment.NewLine);
                sql.Append("AND SEISAN_KUBUN = 3 " + Environment.NewLine);
                if (mode == 1)
                {
                    sql.Append("AND ISNULL(IKKATSU_DENPYO_BANGO, 0) = 0 ");
                }
                sql.Append("GROUP BY " + Environment.NewLine);
                sql.Append("  SENSHU_CD " + Environment.NewLine);
                sql.Append("HAVING " + Environment.NewLine);
                sql.Append("  SUM(KOJO_KOMOKU1) + SUM(KOJO_KOMOKU2) + SUM(KOJO_KOMOKU3) + SUM(KOJO_KOMOKU5) " + Environment.NewLine);
                sql.Append("  + SUM(KOJO_KOMOKU6) + SUM(KOJO_KOMOKU7) + SUM(KOJO_KOMOKU8) + SUM(KOJO_KOMOKU9) + SUM(KOJO_KOMOKU10) " + Environment.NewLine);
                sql.Append("  + SUM(KOJO_KOMOKU11) + SUM(KOJO_KOMOKU12) + SUM(KOJO_KOMOKU13) + SUM(KOJO_KOMOKU14) + SUM(KOJO_KOMOKU15) " + Environment.NewLine);
                sql.Append("  + SUM(KOJO_KOMOKU16) + SUM(KOJO_KOMOKU17) + SUM(KOJO_KOMOKU18) + SUM(KOJO_KOMOKU19) + SUM(KOJO_KOMOKU20) " + Environment.NewLine);
                sql.Append("  + SUM(KOJO_KOMOKU21) + SUM(KOJO_KOMOKU22) + SUM(KOJO_KOMOKU23) + SUM(KOJO_KOMOKU24) + SUM(KOJO_KOMOKU25) " + Environment.NewLine);
                sql.Append("  + SUM(KOJO_KOMOKU26) + SUM(KOJO_KOMOKU27) + SUM(KOJO_KOMOKU28) + SUM(KOJO_KOMOKU29) + SUM(KOJO_KOMOKU30) " + Environment.NewLine);
                sql.Append("  + SUM(KOJO_KOMOKU31) + SUM(KOJO_KOMOKU32) + SUM(KOJO_KOMOKU33) + SUM(KOJO_KOMOKU34) + SUM(KOJO_KOMOKU35) " + Environment.NewLine);
                sql.Append("  > 0 " + Environment.NewLine);
                for (int i = 1; i <= 35; i++)
                {
                    if (i == 4)
                    {
                        continue;
                    }
                    sql.Append("UNION ALL " + Environment.NewLine);
                    sql.Append("SELECT " + Environment.NewLine);
                    sql.Append("  SENSHU_CD " + Environment.NewLine);
                    sql.Append(" ," + Util.ToString(i) + " AS kjno " + Environment.NewLine);
                    sql.Append(" ,SUM(KOJO_KOMOKU" + Util.ToString(i) + ") AS 金額 " + Environment.NewLine);
                    sql.Append("FROM " + Environment.NewLine);
                    sql.Append("  TB_HN_KOJO_DATA " + Environment.NewLine);
                    sql.Append("WHERE " + Environment.NewLine);
                    sql.Append("    KAISHA_CD = @KAISHA_CD " + Environment.NewLine);
                    sql.Append("AND SHISHO_CD = @SHISHO_CD " + Environment.NewLine);
                    sql.Append("AND KAIKEI_NENDO = @KAIKEI_NENDO " + Environment.NewLine);
                    sql.Append("AND SEISAN_BI BETWEEN @SEISAN_BI_FR AND @SEISAN_BI_TO " + Environment.NewLine);
                    sql.Append("AND SENSHU_CD BETWEEN @FUNANUSHI_CD_FR AND @FUNANUSHI_CD_TO " + Environment.NewLine);
                    sql.Append("AND SEISAN_KUBUN = 3 " + Environment.NewLine);
                    if (mode == 1)
                    {
                        sql.Append("AND ISNULL(IKKATSU_DENPYO_BANGO, 0) = 0 ");
                    }
                    sql.Append("GROUP BY " + Environment.NewLine);
                    sql.Append("  SENSHU_CD " + Environment.NewLine);
                    sql.Append("HAVING " + Environment.NewLine);
                    sql.Append("  SUM(KOJO_KOMOKU" + Util.ToString(i) + ") > 0 " + Environment.NewLine);
                }
            }
            else if (KojoIrregularPtn == 3)
            {
                // 沿岸（控除項目２、６を除外）
                sql.Append("SELECT " + Environment.NewLine);
                sql.Append("  SENSHU_CD " + Environment.NewLine);
                sql.Append(" ,0 AS kjno " + Environment.NewLine);
                sql.Append(" ,SUM(KOJO_KOMOKU1) + SUM(KOJO_KOMOKU3) + SUM(KOJO_KOMOKU4) + SUM(KOJO_KOMOKU5) " + Environment.NewLine);
                sql.Append("  + SUM(KOJO_KOMOKU7) + SUM(KOJO_KOMOKU8) + SUM(KOJO_KOMOKU9) + SUM(KOJO_KOMOKU10) " + Environment.NewLine);
                sql.Append("  + SUM(KOJO_KOMOKU11) + SUM(KOJO_KOMOKU12) + SUM(KOJO_KOMOKU13) + SUM(KOJO_KOMOKU14) + SUM(KOJO_KOMOKU15) " + Environment.NewLine);
                sql.Append("  + SUM(KOJO_KOMOKU16) + SUM(KOJO_KOMOKU17) + SUM(KOJO_KOMOKU18) + SUM(KOJO_KOMOKU19) + SUM(KOJO_KOMOKU20) " + Environment.NewLine);
                sql.Append("  + SUM(KOJO_KOMOKU21) + SUM(KOJO_KOMOKU22) + SUM(KOJO_KOMOKU23) + SUM(KOJO_KOMOKU24) + SUM(KOJO_KOMOKU25) " + Environment.NewLine);
                sql.Append("  + SUM(KOJO_KOMOKU26) + SUM(KOJO_KOMOKU27) + SUM(KOJO_KOMOKU28) + SUM(KOJO_KOMOKU29) + SUM(KOJO_KOMOKU30) " + Environment.NewLine);
                sql.Append("  + SUM(KOJO_KOMOKU31) + SUM(KOJO_KOMOKU32) + SUM(KOJO_KOMOKU33) + SUM(KOJO_KOMOKU34) + SUM(KOJO_KOMOKU35) " + Environment.NewLine);

                sql.Append("  AS 金額 " + Environment.NewLine);
                sql.Append("FROM " + Environment.NewLine);
                sql.Append("  TB_HN_KOJO_DATA " + Environment.NewLine);
                sql.Append("WHERE " + Environment.NewLine);
                sql.Append("    KAISHA_CD = @KAISHA_CD " + Environment.NewLine);
                sql.Append("AND SHISHO_CD = @SHISHO_CD " + Environment.NewLine);
                sql.Append("AND KAIKEI_NENDO = @KAIKEI_NENDO " + Environment.NewLine);
                sql.Append("AND SEISAN_BI BETWEEN @SEISAN_BI_FR AND @SEISAN_BI_TO " + Environment.NewLine);
                sql.Append("AND SENSHU_CD BETWEEN @FUNANUSHI_CD_FR AND @FUNANUSHI_CD_TO " + Environment.NewLine);
                sql.Append("AND SEISAN_KUBUN = 3 " + Environment.NewLine);
                if (mode == 1)
                {
                    sql.Append("AND ISNULL(IKKATSU_DENPYO_BANGO, 0) = 0 ");
                }
                sql.Append("GROUP BY " + Environment.NewLine);
                sql.Append("  SENSHU_CD " + Environment.NewLine);
                sql.Append("HAVING " + Environment.NewLine);
                sql.Append("  SUM(KOJO_KOMOKU1) + SUM(KOJO_KOMOKU3) + SUM(KOJO_KOMOKU4) + SUM(KOJO_KOMOKU5) " + Environment.NewLine);
                sql.Append("  + SUM(KOJO_KOMOKU7) + SUM(KOJO_KOMOKU8) + SUM(KOJO_KOMOKU9) + SUM(KOJO_KOMOKU10) " + Environment.NewLine);
                sql.Append("  + SUM(KOJO_KOMOKU11) + SUM(KOJO_KOMOKU12) + SUM(KOJO_KOMOKU13) + SUM(KOJO_KOMOKU14) + SUM(KOJO_KOMOKU15) " + Environment.NewLine);
                sql.Append("  + SUM(KOJO_KOMOKU16) + SUM(KOJO_KOMOKU17) + SUM(KOJO_KOMOKU18) + SUM(KOJO_KOMOKU19) + SUM(KOJO_KOMOKU20) " + Environment.NewLine);
                sql.Append("  + SUM(KOJO_KOMOKU21) + SUM(KOJO_KOMOKU22) + SUM(KOJO_KOMOKU23) + SUM(KOJO_KOMOKU24) + SUM(KOJO_KOMOKU25) " + Environment.NewLine);
                sql.Append("  + SUM(KOJO_KOMOKU26) + SUM(KOJO_KOMOKU27) + SUM(KOJO_KOMOKU28) + SUM(KOJO_KOMOKU29) + SUM(KOJO_KOMOKU30) " + Environment.NewLine);
                sql.Append("  + SUM(KOJO_KOMOKU31) + SUM(KOJO_KOMOKU32) + SUM(KOJO_KOMOKU33) + SUM(KOJO_KOMOKU34) + SUM(KOJO_KOMOKU35) " + Environment.NewLine);

                sql.Append("  > 0 ");
                for (int i = 1; i <= 35; i++)
                {
                    if (i == 2 || i == 6)
                    {
                        continue;
                    }
                    sql.Append("UNION ALL " + Environment.NewLine);
                    sql.Append("SELECT " + Environment.NewLine);
                    sql.Append("  SENSHU_CD " + Environment.NewLine);
                    sql.Append(" ," + Util.ToString(i) + " AS kjno " + Environment.NewLine);
                    sql.Append(" ,SUM(KOJO_KOMOKU" + Util.ToString(i) + ") AS 金額 " + Environment.NewLine);
                    sql.Append("FROM " + Environment.NewLine);
                    sql.Append("  TB_HN_KOJO_DATA " + Environment.NewLine);
                    sql.Append("WHERE " + Environment.NewLine);
                    sql.Append("    KAISHA_CD = @KAISHA_CD " + Environment.NewLine);
                    sql.Append("AND SHISHO_CD = @SHISHO_CD " + Environment.NewLine);
                    sql.Append("AND KAIKEI_NENDO = @KAIKEI_NENDO " + Environment.NewLine);
                    sql.Append("AND SEISAN_BI BETWEEN @SEISAN_BI_FR AND @SEISAN_BI_TO " + Environment.NewLine);
                    sql.Append("AND SENSHU_CD BETWEEN @FUNANUSHI_CD_FR AND @FUNANUSHI_CD_TO " + Environment.NewLine);
                    sql.Append("AND SEISAN_KUBUN = 3 " + Environment.NewLine);
                    if (mode == 1)
                    {
                        sql.Append("AND ISNULL(IKKATSU_DENPYO_BANGO, 0) = 0 ");
                    }
                    sql.Append("GROUP BY " + Environment.NewLine);
                    sql.Append("  SENSHU_CD " + Environment.NewLine);
                    sql.Append("HAVING " + Environment.NewLine);
                    sql.Append("  SUM(KOJO_KOMOKU" + Util.ToString(i) + ") > 0 " + Environment.NewLine);
                }
            }
            else
            {
                // 全て処理
                sql.Append("SELECT " + Environment.NewLine);
                sql.Append("  SENSHU_CD " + Environment.NewLine);
                sql.Append(" ,0 AS kjno " + Environment.NewLine);
                sql.Append(" ,SUM(KOJO_KOMOKU1) + SUM(KOJO_KOMOKU2) + SUM(KOJO_KOMOKU3) + SUM(KOJO_KOMOKU4) + SUM(KOJO_KOMOKU5) " + Environment.NewLine);
                sql.Append("  + SUM(KOJO_KOMOKU6) + SUM(KOJO_KOMOKU7) + SUM(KOJO_KOMOKU8) + SUM(KOJO_KOMOKU9) + SUM(KOJO_KOMOKU10) " + Environment.NewLine);
                sql.Append("  + SUM(KOJO_KOMOKU11) + SUM(KOJO_KOMOKU12) + SUM(KOJO_KOMOKU13) + SUM(KOJO_KOMOKU14) + SUM(KOJO_KOMOKU15) " + Environment.NewLine);
                sql.Append("  + SUM(KOJO_KOMOKU16) + SUM(KOJO_KOMOKU17) + SUM(KOJO_KOMOKU18) + SUM(KOJO_KOMOKU19) + SUM(KOJO_KOMOKU20) " + Environment.NewLine);
                sql.Append("  + SUM(KOJO_KOMOKU21) + SUM(KOJO_KOMOKU22) + SUM(KOJO_KOMOKU23) + SUM(KOJO_KOMOKU24) + SUM(KOJO_KOMOKU25) " + Environment.NewLine);
                sql.Append("  + SUM(KOJO_KOMOKU26) + SUM(KOJO_KOMOKU27) + SUM(KOJO_KOMOKU28) + SUM(KOJO_KOMOKU29) + SUM(KOJO_KOMOKU30) " + Environment.NewLine);
                sql.Append("  + SUM(KOJO_KOMOKU31) + SUM(KOJO_KOMOKU32) + SUM(KOJO_KOMOKU33) + SUM(KOJO_KOMOKU34) + SUM(KOJO_KOMOKU35) " + Environment.NewLine);
                sql.Append("  AS 金額 " + Environment.NewLine);
                sql.Append("FROM " + Environment.NewLine);
                sql.Append("  TB_HN_KOJO_DATA " + Environment.NewLine);
                sql.Append("WHERE " + Environment.NewLine);
                sql.Append("    KAISHA_CD = @KAISHA_CD " + Environment.NewLine);
                sql.Append("AND SHISHO_CD = @SHISHO_CD " + Environment.NewLine);
                sql.Append("AND KAIKEI_NENDO = @KAIKEI_NENDO " + Environment.NewLine);
                sql.Append("AND SEISAN_BI BETWEEN @SEISAN_BI_FR AND @SEISAN_BI_TO " + Environment.NewLine);
                sql.Append("AND SENSHU_CD BETWEEN @FUNANUSHI_CD_FR AND @FUNANUSHI_CD_TO " + Environment.NewLine);
                sql.Append("AND SEISAN_KUBUN = 3 " + Environment.NewLine);
                if (mode == 1)
                {
                    sql.Append("AND ISNULL(IKKATSU_DENPYO_BANGO, 0) = 0 " + Environment.NewLine);
                }
                sql.Append("GROUP BY " + Environment.NewLine);
                sql.Append("  SENSHU_CD " + Environment.NewLine);
                sql.Append("HAVING " + Environment.NewLine);
                sql.Append("  SUM(KOJO_KOMOKU1) + SUM(KOJO_KOMOKU2) + SUM(KOJO_KOMOKU3) + SUM(KOJO_KOMOKU4) + SUM(KOJO_KOMOKU5) " + Environment.NewLine);
                sql.Append("  + SUM(KOJO_KOMOKU6) + SUM(KOJO_KOMOKU7) + SUM(KOJO_KOMOKU8) + SUM(KOJO_KOMOKU9) + SUM(KOJO_KOMOKU10) " + Environment.NewLine);
                sql.Append("  + SUM(KOJO_KOMOKU11) + SUM(KOJO_KOMOKU12) + SUM(KOJO_KOMOKU13) + SUM(KOJO_KOMOKU14) + SUM(KOJO_KOMOKU15) " + Environment.NewLine);
                sql.Append("  + SUM(KOJO_KOMOKU16) + SUM(KOJO_KOMOKU17) + SUM(KOJO_KOMOKU18) + SUM(KOJO_KOMOKU19) + SUM(KOJO_KOMOKU20) " + Environment.NewLine);
                sql.Append("  + SUM(KOJO_KOMOKU21) + SUM(KOJO_KOMOKU22) + SUM(KOJO_KOMOKU23) + SUM(KOJO_KOMOKU24) + SUM(KOJO_KOMOKU25) " + Environment.NewLine);
                sql.Append("  + SUM(KOJO_KOMOKU26) + SUM(KOJO_KOMOKU27) + SUM(KOJO_KOMOKU28) + SUM(KOJO_KOMOKU29) + SUM(KOJO_KOMOKU30) " + Environment.NewLine);
                sql.Append("  + SUM(KOJO_KOMOKU31) + SUM(KOJO_KOMOKU32) + SUM(KOJO_KOMOKU33) + SUM(KOJO_KOMOKU34) + SUM(KOJO_KOMOKU35) " + Environment.NewLine);
                sql.Append("  > 0 ");
                for (int i = 1; i <= 35; i++)
                {
                    sql.Append("UNION ALL " + Environment.NewLine);
                    sql.Append("SELECT " + Environment.NewLine);
                    sql.Append("  SENSHU_CD " + Environment.NewLine);
                    sql.Append(" ," + Util.ToString(i) + " AS kjno " + Environment.NewLine);
                    sql.Append(" ,SUM(KOJO_KOMOKU" + Util.ToString(i) + ") AS 金額 " + Environment.NewLine);
                    sql.Append("FROM " + Environment.NewLine);
                    sql.Append("  TB_HN_KOJO_DATA " + Environment.NewLine);
                    sql.Append("WHERE " + Environment.NewLine);
                    sql.Append("    KAISHA_CD = @KAISHA_CD " + Environment.NewLine);
                    sql.Append("AND SHISHO_CD = @SHISHO_CD " + Environment.NewLine);
                    sql.Append("AND KAIKEI_NENDO = @KAIKEI_NENDO " + Environment.NewLine);
                    sql.Append("AND SEISAN_BI BETWEEN @SEISAN_BI_FR AND @SEISAN_BI_TO " + Environment.NewLine);
                    sql.Append("AND SENSHU_CD BETWEEN @FUNANUSHI_CD_FR AND @FUNANUSHI_CD_TO " + Environment.NewLine);
                    sql.Append("AND SEISAN_KUBUN = 3 " + Environment.NewLine);
                    if (mode == 1)
                    {
                        sql.Append("AND ISNULL(IKKATSU_DENPYO_BANGO, 0) = 0 ");
                    }
                    sql.Append("GROUP BY " + Environment.NewLine);
                    sql.Append("  SENSHU_CD " + Environment.NewLine);
                    sql.Append("HAVING " + Environment.NewLine);
                    sql.Append("  SUM(KOJO_KOMOKU" + Util.ToString(i) + ") > 0 " + Environment.NewLine);
                }
            }

            //sql.Append("SELECT ");
            //sql.Append("  SENSHU_CD ");
            //sql.Append(" ,0 AS kjno ");
            //sql.Append(" ,SUM(KOJO_KOMOKU1) + SUM(KOJO_KOMOKU2) + SUM(KOJO_KOMOKU3) + SUM(KOJO_KOMOKU5) ");
            //sql.Append("  + SUM(KOJO_KOMOKU6) + SUM(KOJO_KOMOKU7) + SUM(KOJO_KOMOKU8) + SUM(KOJO_KOMOKU9) + SUM(KOJO_KOMOKU10) ");
            //sql.Append("  + SUM(KOJO_KOMOKU11) + SUM(KOJO_KOMOKU12) + SUM(KOJO_KOMOKU13) + SUM(KOJO_KOMOKU14) + SUM(KOJO_KOMOKU15) ");
            //sql.Append("  + SUM(KOJO_KOMOKU16) + SUM(KOJO_KOMOKU17) + SUM(KOJO_KOMOKU18) + SUM(KOJO_KOMOKU19) + SUM(KOJO_KOMOKU20) ");
            //sql.Append("  + SUM(KOJO_KOMOKU21) + SUM(KOJO_KOMOKU22) + SUM(KOJO_KOMOKU23) + SUM(KOJO_KOMOKU24) + SUM(KOJO_KOMOKU25) ");
            //sql.Append("  + SUM(KOJO_KOMOKU26) + SUM(KOJO_KOMOKU27) + SUM(KOJO_KOMOKU28) + SUM(KOJO_KOMOKU29) + SUM(KOJO_KOMOKU30) ");
            //sql.Append("  + SUM(KOJO_KOMOKU31) + SUM(KOJO_KOMOKU32) + SUM(KOJO_KOMOKU33) + SUM(KOJO_KOMOKU34) + SUM(KOJO_KOMOKU35) ");
            //sql.Append("  AS 金額 ");
            //sql.Append("FROM ");
            //sql.Append("  TB_HN_KOJO_DATA ");
            //sql.Append("WHERE ");
            //sql.Append("    KAISHA_CD = @KAISHA_CD ");
            //sql.Append("AND SHISHO_CD = @SHISHO_CD ");
            //sql.Append("AND KAIKEI_NENDO = @KAIKEI_NENDO ");
            //sql.Append("AND SEISAN_BI BETWEEN @SEISAN_BI_FR AND @SEISAN_BI_TO ");
            //sql.Append("AND SENSHU_CD BETWEEN @FUNANUSHI_CD_FR AND @FUNANUSHI_CD_TO ");
            //sql.Append("AND SEISAN_KUBUN = 3 ");
            //if (mode == 1)
            //{
            //    sql.Append("AND ISNULL(IKKATSU_DENPYO_BANGO, 0) = 0 ");
            //}
            //sql.Append("GROUP BY ");
            //sql.Append("  SENSHU_CD ");
            //sql.Append("HAVING ");
            //sql.Append("  SUM(KOJO_KOMOKU1) + SUM(KOJO_KOMOKU2) + SUM(KOJO_KOMOKU3) + SUM(KOJO_KOMOKU5) ");
            //sql.Append("  + SUM(KOJO_KOMOKU6) + SUM(KOJO_KOMOKU7) + SUM(KOJO_KOMOKU8) + SUM(KOJO_KOMOKU9) + SUM(KOJO_KOMOKU10) ");
            //sql.Append("  + SUM(KOJO_KOMOKU11) + SUM(KOJO_KOMOKU12) + SUM(KOJO_KOMOKU13) + SUM(KOJO_KOMOKU14) + SUM(KOJO_KOMOKU15) ");
            //sql.Append("  + SUM(KOJO_KOMOKU16) + SUM(KOJO_KOMOKU17) + SUM(KOJO_KOMOKU18) + SUM(KOJO_KOMOKU19) + SUM(KOJO_KOMOKU20) ");
            //sql.Append("  + SUM(KOJO_KOMOKU21) + SUM(KOJO_KOMOKU22) + SUM(KOJO_KOMOKU23) + SUM(KOJO_KOMOKU24) + SUM(KOJO_KOMOKU25) ");
            //sql.Append("  + SUM(KOJO_KOMOKU26) + SUM(KOJO_KOMOKU27) + SUM(KOJO_KOMOKU28) + SUM(KOJO_KOMOKU29) + SUM(KOJO_KOMOKU30) ");
            //sql.Append("  + SUM(KOJO_KOMOKU31) + SUM(KOJO_KOMOKU32) + SUM(KOJO_KOMOKU33) + SUM(KOJO_KOMOKU34) + SUM(KOJO_KOMOKU35) ");
            //sql.Append("  > 0 ");
            //for (int i = 1; i <= 35; i++)
            //{
            //    if (i == 4)
            //    {
            //        continue;
            //    }
            //    sql.Append("UNION ALL ");
            //    sql.Append("SELECT ");
            //    sql.Append("  SENSHU_CD ");
            //    sql.Append(" ," + Util.ToString(i) + " AS kjno ");
            //    sql.Append(" ,SUM(KOJO_KOMOKU" + Util.ToString(i) + ") AS 金額 ");
            //    sql.Append("FROM ");
            //    sql.Append("  TB_HN_KOJO_DATA ");
            //    sql.Append("WHERE ");
            //    sql.Append("    KAISHA_CD = @KAISHA_CD ");
            //    sql.Append("AND SHISHO_CD = @SHISHO_CD ");
            //    sql.Append("AND KAIKEI_NENDO = @KAIKEI_NENDO ");
            //    sql.Append("AND SEISAN_BI BETWEEN @SEISAN_BI_FR AND @SEISAN_BI_TO ");
            //    sql.Append("AND SENSHU_CD BETWEEN @FUNANUSHI_CD_FR AND @FUNANUSHI_CD_TO ");
            //    sql.Append("AND SEISAN_KUBUN = 3 ");
            //    if (mode == 1)
            //    {
            //        sql.Append("AND ISNULL(IKKATSU_DENPYO_BANGO, 0) = 0 ");
            //    }
            //    sql.Append("GROUP BY ");
            //    sql.Append("  SENSHU_CD ");
            //    sql.Append("HAVING ");
            //    sql.Append("  SUM(KOJO_KOMOKU" + Util.ToString(i) + ") > 0 ");
            //}


            /*
            sql.Append("UNION ALL ");
            sql.Append("SELECT ");
            sql.Append("  SENSHU_CD ");
            sql.Append(" ,36 AS kjno ");
            sql.Append(" ,SUM(SASHIHIKI_SEISANGAKU) AS 金額 ");
            sql.Append("FROM ");
            sql.Append("  TB_HN_KOJO_DATA ");
            sql.Append("WHERE ");
            sql.Append("    KAISHA_CD = @KAISHA_CD ");
            sql.Append("AND KAIKEI_NENDO = @KAIKEI_NENDO ");
            sql.Append("AND SEISAN_BI BETWEEN @SEISAN_BI_FR AND @SEISAN_BI_TO ");
            sql.Append("AND SENSHU_CD BETWEEN @FUNANUSHI_CD_FR AND @FUNANUSHI_CD_TO ");
            if (mode == 1)
            {
                sql.Append("AND ISNULL(IKKATSU_DENPYO_BANGO, 0) = 0 ");
            }
            sql.Append("GROUP BY ");
            sql.Append("  SENSHU_CD ");
            sql.Append("HAVING ");
            sql.Append("  SUM(SASHIHIKI_SEISANGAKU) > 0 ");
            sql.Append("ORDER BY ");
            sql.Append("  SENSHU_CD, kjno ");
             * */

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
            dpc.SetParam("@SEISAN_BI_FR", SqlDbType.DateTime, Util.ToDate(condition["SeisanDtFr"]));
            dpc.SetParam("@SEISAN_BI_TO", SqlDbType.DateTime, Util.ToDate(condition["SeisanDtTo"]));
            dpc.SetParam("@FUNANUSHI_CD_FR", SqlDbType.VarChar, 4, Util.ToString(condition["FunanushiCdFr"]));
            dpc.SetParam("@FUNANUSHI_CD_TO", SqlDbType.VarChar, 4, Util.ToString(condition["FunanushiCdTo"]));

            DataTable dtResult = this._dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            return dtResult;
        }

        /// <summary>
        /// TB_自動仕訳設定Ａから設定を取得(セリ用)
        /// </summary>
        /// <returns>取得した自動仕訳設定Ａ</returns>
        private DataTable GetSeriTB_HN_ZIDO_SHIWAKE_SETTEI_A(DateTime DenpyoDay)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT ");
            sql.Append("  A.SHIWAKE_CD            AS 仕訳コード ");
            sql.Append(" ,A.KANJO_KAMOKU_CD       AS 勘定科目コード ");
            sql.Append(" ,B.KANJO_KAMOKU_NM       AS 勘定科目名 ");
            sql.Append(" ,(CASE WHEN B.HOJO_KAMOKU_UMU = 0 THEN 0 ");
            sql.Append("        WHEN A.HOJO_KAMOKU_CD = 0 THEN -1 ");
            sql.Append("        ELSE A.HOJO_KAMOKU_CD ");
            sql.Append("   END) AS 補助科目コード ");
            sql.Append(" ,(CASE WHEN B.BUMON_UMU = 0 THEN 0 ");
            sql.Append("        WHEN A.BUMON_CD = 0 THEN -1 ");
            sql.Append("        ELSE A.BUMON_CD ");
            sql.Append("   END) AS 部門コード ");
            sql.Append(" ,B.HOJO_SHIYO_KUBUN      AS 補助使用区分 ");
            sql.Append(" ,B.TAISHAKU_KUBUN        AS 貸借区分 ");
            sql.Append(" ,A.ZEI_KUBUN             AS 税区分 ");
            sql.Append(" ,C.KAZEI_KUBUN           AS 課税区分 ");
            sql.Append(" ,C.TORIHIKI_KUBUN        AS 取引区分 ");

            //sql.Append(" ,C.ZEI_RITSU             AS 税率 ");
            sql.Append(" ,dbo.FNC_GetTaxRate( A.ZEI_KUBUN, @DENPYO_DATE ) AS 税率 ");

            sql.Append(" ,A.JIGYO_KUBUN           AS 事業区分 ");
            sql.Append("FROM ");
            sql.Append("  TB_HN_ZIDO_SHIWAKE_SETTEI_A AS A ");
            sql.Append("LEFT OUTER JOIN TB_ZM_KANJO_KAMOKU AS B ");
            sql.Append("ON A.KAISHA_CD = B.KAISHA_CD ");
            sql.Append("AND A.KANJO_KAMOKU_CD = B.KANJO_KAMOKU_CD ");
            sql.Append("AND B.KAIKEI_NENDO = @KAIKEI_NENDO ");
            sql.Append("LEFT OUTER JOIN TB_ZM_F_ZEI_KUBUN AS C ");
            sql.Append("ON A.ZEI_KUBUN = C.ZEI_KUBUN ");
            sql.Append("WHERE ");
            sql.Append("    A.KAISHA_CD = @KAISHA_CD ");
            sql.Append("AND A.SHISHO_CD = @SHISHO_CD ");
            sql.Append("AND A.DENPYO_KUBUN = 3 ");
            sql.Append("AND ISNULL(B.KANJO_KAMOKU_CD, 0) <> 0 ");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
            dpc.SetParam("@DENPYO_DATE", SqlDbType.DateTime, DenpyoDay);

            DataTable dtResult = this._dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            return dtResult;
        }

        /// <summary>
        /// TB_自動仕訳設定Ｂから設定を取得(セリ用)
        /// </summary>
        /// <returns>取得した自動仕訳設定Ｂ</returns>
        private DataTable GetSeriTB_HN_ZIDO_SHIWAKE_SETTEI_B(DateTime DenpyoDay)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT ");
            sql.Append("  A.SHIWAKE_CD            AS 仕訳コード ");
            sql.Append(" ,A.KANJO_KAMOKU_CD       AS 勘定科目コード ");
            sql.Append(" ,B.KANJO_KAMOKU_NM       AS 勘定科目名 ");
            sql.Append(" ,(CASE WHEN B.HOJO_KAMOKU_UMU = 0 THEN 0 ");
            sql.Append("        ELSE CASE WHEN A.HOJO_KAMOKU_CD = 0 THEN -1 ");
            sql.Append("                  ELSE A.HOJO_KAMOKU_CD ");
            sql.Append("             END ");
            sql.Append("   END) AS 補助科目コード ");
            sql.Append(" ,(CASE WHEN B.BUMON_UMU = 0 THEN 0 ");
            sql.Append("        ELSE CASE WHEN A.BUMON_CD = 0 THEN -1 ");
            sql.Append("                  ELSE A.BUMON_CD ");
            sql.Append("             END ");
            sql.Append("   END) AS 部門コード ");
            sql.Append(" ,B.HOJO_SHIYO_KUBUN      AS 補助使用区分 ");
            sql.Append(" ,A.TAISHAKU_KUBUN        AS 貸借区分 ");
            sql.Append(" ,A.ZEI_KUBUN             AS 税区分 ");
            sql.Append(" ,C.KAZEI_KUBUN           AS 課税区分 ");
            sql.Append(" ,C.TORIHIKI_KUBUN        AS 取引区分 ");

            //sql.Append(" ,C.ZEI_RITSU             AS 税率 ");
            sql.Append(" ,dbo.FNC_GetTaxRate( A.ZEI_KUBUN, @DENPYO_DATE ) AS 税率 ");

            sql.Append(" ,A.JIGYO_KUBUN           AS 事業区分 ");
            sql.Append("FROM ");
            sql.Append("  TB_HN_ZIDO_SHIWAKE_SETTEI_B  AS A ");
            sql.Append("LEFT OUTER JOIN TB_ZM_KANJO_KAMOKU  AS B ");
            sql.Append("ON A.KAISHA_CD = B.KAISHA_CD ");
            sql.Append("AND A.KANJO_KAMOKU_CD = B.KANJO_KAMOKU_CD ");
            sql.Append("AND B.KAIKEI_NENDO = @KAIKEI_NENDO ");
            sql.Append("LEFT OUTER JOIN TB_ZM_F_ZEI_KUBUN AS C ");
            sql.Append("ON A.ZEI_KUBUN = C.ZEI_KUBUN ");
            sql.Append("WHERE ");
            sql.Append("    A.KAISHA_CD = @KAISHA_CD ");
            sql.Append("AND A.SHISHO_CD = @SHISHO_CD ");
            sql.Append("AND A.DENPYO_KUBUN = 3 ");
            sql.Append("AND ISNULL(B.KANJO_KAMOKU_CD, 0) <> 0 ");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
            dpc.SetParam("@DENPYO_DATE", SqlDbType.DateTime, DenpyoDay);

            DataTable dtResult = this._dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            return dtResult;
        }

        /// <summary>
        /// TB_自動仕訳設定Ａから設定を取得(控除用)
        /// </summary>
        /// <returns>取得した自動仕訳設定Ａ</returns>
        private DataTable GetKojoTB_HN_ZIDO_SHIWAKE_SETTEI_A(DateTime DenpyoDay)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT ");
            sql.Append("  A.KANJO_KAMOKU_CD       AS 勘定科目コード ");
            sql.Append(" ,A.HOJO_KAMOKU_CD        AS 補助科目コード ");
            sql.Append(" ,A.BUMON_CD              AS 部門コード ");
            sql.Append(" ,A.ZEI_KUBUN             AS 税区分 ");
            sql.Append(" ,A.JIGYO_KUBUN           AS 事業区分 ");
            sql.Append(" ,C.KAZEI_KUBUN           AS 課税区分 ");
            sql.Append(" ,C.TORIHIKI_KUBUN        AS 取引区分 ");

            //sql.Append(" ,C.ZEI_RITSU             AS 税率 ");
            sql.Append(" ,dbo.FNC_GetTaxRate( A.ZEI_KUBUN, @DENPYO_DATE ) AS 税率 ");

            sql.Append("FROM ");
            sql.Append("  TB_HN_ZIDO_SHIWAKE_SETTEI_A AS A ");
            sql.Append("LEFT OUTER JOIN TB_ZM_F_ZEI_KUBUN AS C ");
            sql.Append("ON A.ZEI_KUBUN = C.ZEI_KUBUN ");
            sql.Append("WHERE ");
            sql.Append("    A.KAISHA_CD = @KAISHA_CD ");
            sql.Append("AND A.SHISHO_CD = @SHISHO_CD ");
            sql.Append("AND A.DENPYO_KUBUN = 4 ");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
            dpc.SetParam("@DENPYO_DATE", SqlDbType.DateTime, DenpyoDay);

            DataTable dtResult = this._dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            return dtResult;
        }

        /// <summary>
        /// 控除科目設定およびTB_自動仕訳設定Ｂから設定を取得(控除用)
        /// </summary>
        /// <returns>取得したデータ</returns>
        private DataTable GetKojoTB_HN_ZIDO_SHIWAKE_SETTEI_B(DateTime DenpyoDay)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT ");
            sql.Append("  A.SETTEI_CD             AS 設定コード ");
            sql.Append(" ,A.KANJO_KAMOKU_CD       AS 勘定科目コード ");
            sql.Append(" ,A.HOJO_KAMOKU_CD        AS 補助科目コード ");
            sql.Append(" ,A.BUMON_CD              AS 部門コード ");
            sql.Append(" ,B.KASHIKATA_ZEI_KUBUN   AS 貸方税区分 ");
            sql.Append(" ,C.KAZEI_KUBUN           AS 課税区分 ");
            sql.Append(" ,C.TORIHIKI_KUBUN        AS 取引区分 ");

            //sql.Append(" ,C.ZEI_RITSU             AS 税率 ");
            sql.Append(" ,dbo.FNC_GetTaxRate( A.ZEI_KUBUN, @DENPYO_DATE ) AS 税率 ");

            sql.Append("FROM ");
            sql.Append("  TB_HN_KOJO_KAMOKU_SETTEI AS A ");
            sql.Append("LEFT OUTER JOIN TB_ZM_KANJO_KAMOKU AS B ");
            sql.Append("ON A.KAISHA_CD = B.KAISHA_CD ");
            sql.Append("AND B.KAIKEI_NENDO = @KAIKEI_NENDO ");
            sql.Append("AND A.KANJO_KAMOKU_CD = B.KANJO_KAMOKU_CD ");
            sql.Append("LEFT OUTER JOIN TB_ZM_F_ZEI_KUBUN AS C ");
            sql.Append("ON B.KASHIKATA_ZEI_KUBUN = C.ZEI_KUBUN ");
            sql.Append("WHERE ");
            sql.Append("     A.KAISHA_CD = @KAISHA_CD ");
            sql.Append(" AND A.SHISHO_CD = @SHISHO_CD ");
            /*
            sql.Append("UNION ALL ");
            sql.Append("SELECT ");
            sql.Append("  36                      AS 設定コード ");
            sql.Append(" ,A.KANJO_KAMOKU_CD       AS 勘定科目コード ");
            sql.Append(" ,A.HOJO_KAMOKU_CD        AS 補助科目コード ");
            sql.Append(" ,A.BUMON_CD              AS 部門コード ");
            sql.Append(" ,A.ZEI_KUBUN             AS 貸方税区分 ");
            sql.Append(" ,B.KAZEI_KUBUN           AS 課税区分 ");
            sql.Append(" ,B.TORIHIKI_KUBUN        AS 取引区分 ");
            sql.Append(" ,B.ZEI_RITSU             AS 税率 ");
            sql.Append("FROM ");
            sql.Append("  TB_HN_ZIDO_SHIWAKE_SETTEI_B AS A ");
            sql.Append("LEFT OUTER JOIN TB_ZM_F_ZEI_KUBUN AS B ");
            sql.Append("ON A.ZEI_KUBUN = B.ZEI_KUBUN ");
            sql.Append("WHERE ");
            sql.Append("    A.KAISHA_CD = @KAISHA_CD ");
            sql.Append("AND A.DENPYO_KUBUN = 5 ");
            sql.Append("ORDER BY A.SETTEI_CD ");
             */

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this._uInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
            dpc.SetParam("@DENPYO_DATE", SqlDbType.DateTime, DenpyoDay);

            DataTable dtResult = this._dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            return dtResult;
        }

        /// <summary>
        /// TB_自動仕訳設定Ａから設定を取得(支払用)
        /// </summary>
        /// <returns>取得した自動仕訳設定Ａ</returns>
        private DataTable GetShiharaiTB_HN_ZIDO_SHIWAKE_SETTEI_A(DateTime DenpyoDay)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT");
            sql.Append("    0 AS SETTEI_CD,");
            sql.Append("    A.KANJO_KAMOKU_CD,");
            sql.Append("    A.HOJO_KAMOKU_CD,");
            sql.Append("    A.BUMON_CD,");
            sql.Append("    A.ZEI_KUBUN,");
            sql.Append("    A.JIGYO_KUBUN,");
            sql.Append("    B.KAZEI_KUBUN,");
            sql.Append("    B.TORIHIKI_KUBUN,");

            //sql.Append("    B.ZEI_RITSU ");
            sql.Append("    dbo.FNC_GetTaxRate( A.ZEI_KUBUN, @DENPYO_DATE ) AS ZEI_RITSU ");

            sql.Append("FROM");
            sql.Append("    TB_HN_ZIDO_SHIWAKE_SETTEI_A AS A  LEFT JOIN TB_ZM_F_ZEI_KUBUN AS B   ON A.ZEI_KUBUN = B.ZEI_KUBUN ");
            sql.Append("WHERE");
            //sql.Append("    A.KAISHA_CD = 1 AND");
            sql.Append("    A.KAISHA_CD = @KAISHA_CD AND");
            sql.Append("    A.SHISHO_CD = @SHISHO_CD AND ");
            sql.Append("    A.DENPYO_KUBUN = 5 ");
            sql.Append("ORDER BY");
            sql.Append("    A.SHIWAKE_CD");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
            dpc.SetParam("@DENPYO_DATE", SqlDbType.DateTime, DenpyoDay);

            DataTable dtResult = this._dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            return dtResult;
        }

        /// <summary>
        /// TB_自動仕訳設定Ｂから設定を取得(支払用)
        /// </summary>
        /// <returns>取得した自動仕訳設定Ｂ</returns>
        private DataTable GetShiharaiTB_HN_ZIDO_SHIWAKE_SETTEI_B(DateTime DenpyoDay)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT");
            sql.Append("    0 AS SETTEI_CD,");
            sql.Append("    A.KANJO_KAMOKU_CD,");
            sql.Append("    A.HOJO_KAMOKU_CD,");
            sql.Append("    A.BUMON_CD,");
            sql.Append("    A.ZEI_KUBUN,");
            sql.Append("    A.JIGYO_KUBUN,");
            sql.Append("    B.KAZEI_KUBUN,");
            sql.Append("    B.TORIHIKI_KUBUN,");

            //sql.Append("    B.ZEI_RITSU ");
            sql.Append(" dbo.FNC_GetTaxRate( A.ZEI_KUBUN, @DENPYO_DATE ) AS ZEI_RITSU ");

            sql.Append("FROM");
            sql.Append("    TB_HN_ZIDO_SHIWAKE_SETTEI_B AS A  LEFT JOIN TB_ZM_F_ZEI_KUBUN AS B   ON A.ZEI_KUBUN = B.ZEI_KUBUN ");
            sql.Append("WHERE");
            //sql.Append("    A.KAISHA_CD = 1 AND");
            sql.Append("    A.KAISHA_CD = @KAISHA_CD AND");
            sql.Append("    A.SHISHO_CD = @SHISHO_CD AND ");
            sql.Append("    A.DENPYO_KUBUN = 5 ");
            sql.Append("ORDER BY");
            sql.Append("    A.SHIWAKE_CD");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
            dpc.SetParam("@DENPYO_DATE", SqlDbType.DateTime, DenpyoDay);

            DataTable dtResult = this._dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            return dtResult;
        }

        /// <summary>
        /// 自動仕訳設定Ａから情報を取得する
        /// </summary>
        /// <param name="dtSwkSettingA">予め取得した自動仕訳設定Ａ</param>
        /// <param name="toriKbn1">取引区分１</param>
        /// <param name="toriKbn2">取引区分２</param>
        /// <returns>設定レコード</returns>
        private DataRow GetSwkSettingARec(DataTable dtSwkSettingA, int toriKbn1, int toriKbn2)
        {
            int swkCd = toriKbn1 * 10 + toriKbn2;
            DataRow[] aryRow = dtSwkSettingA.Select("仕訳コード = " + Util.ToString(swkCd));

            if (aryRow.Length > 0)
            {
                return aryRow[0];
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 自動仕訳設定Ｂから情報を取得する
        /// </summary>
        /// <param name="dtSwkSettingB">予め取得した自動仕訳設定Ｂ</param>
        /// <param name="swkCd">仕訳コード</param>
        /// <returns>設定レコード</returns>
        private DataRow GetSwkSettingBRec(DataTable dtSwkSettingB, int swkCd)
        {
            DataRow[] aryRow = dtSwkSettingB.Select("仕訳コード = " + Util.ToString(swkCd));

            if (aryRow.Length > 0)
            {
                return aryRow[0];
            }
            else
            {
                return null;
            }
        }

        ///// <summary>
        ///// 消費税設定から情報を取得する
        ///// </summary>
        ///// <param name="dtZeiSetting">dtZeiSetting</param>
        ///// <param name="ukeBriKbn">仮受／仮払区分(1:仮受、2:仮払)</param>
        ///// <returns></returns>
        //private DataRow GetZeiSettingRow(DataTable dtZeiSetting, int ukeBriKbn)
        //{
        //    DataRow[] aryRow = dtZeiSetting.Select("KBN = " + Util.ToString(ukeBriKbn));

        //    if (aryRow.Length > 0)
        //    {
        //        return aryRow[0];
        //    }
        //    else
        //    {
        //        return null;
        //    }
        //}

        /// <summary>
        /// 伝票番号の存在チェック
        /// </summary>
        /// <param name="packDpyNo">一括伝票番号</param>
        /// <param name="denpyoKbn">伝票区分</param>
        /// <returns>取得したデータ</returns>
        private DataTable GetChkDpyNo(int packDpyNo, int denpyoKbn)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT ");
            sql.Append("  DENPYO_BANGO ");
            sql.Append(" ,SHIWAKE_DENPYO_BANGO ");
            sql.Append("FROM ");
            sql.Append("  TB_HN_ZIDO_SHIWAKE_RIREKI ");
            sql.Append("WHERE ");
            sql.Append("    KAISHA_CD    = @KAISHA_CD ");
            sql.Append("AND SHISHO_CD    = @SHISHO_CD ");
            sql.Append("AND KAIKEI_NENDO = @KAIKEI_NENDO ");
            sql.Append("AND DENPYO_KUBUN = @DENPYO_KUBUN ");
            sql.Append("AND DENPYO_BANGO = @DENPYO_BANGO ");
            sql.Append("ORDER BY ");
            sql.Append("  DENPYO_BANGO ");
            sql.Append(" ,SHIWAKE_DENPYO_BANGO ");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
            dpc.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 2, denpyoKbn);
            dpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 6, packDpyNo);

            DataTable dtResult = this._dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            return dtResult;
        }

        ///// <summary>
        ///// 伝票番号の存在チェック(入出庫伝票)
        ///// </summary>
        ///// <param name="packDpyNo">一括伝票番号</param>
        ///// <param name="denpyoKbn">伝票区分</param>
        ///// <returns>取得したデータ</returns>
        //private DataTable GetNyushukkoDpyNo(int packDpyNo, int denpyoKbn)
        //{
        //    StringBuilder sql = new StringBuilder();
        //    sql.Append("SELECT ");
        //    sql.Append("  COUNT(*) AS KENSU ");
        //    sql.Append("FROM ");
        //    sql.Append("  TB_HN_NYUSHUKKO_DENPYO ");
        //    sql.Append("WHERE ");
        //    sql.Append("    KAISHA_CD    = @KAISHA_CD ");
        //    sql.Append("AND KAIKEI_NENDO = @KAIKEI_NENDO ");
        //    sql.Append("AND DENPYO_KUBUN = @DENPYO_KUBUN ");
        //    sql.Append("AND DENPYO_BANGO = @DENPYO_BANGO ");

        //    DbParamCollection dpc = new DbParamCollection();
        //    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
        //    dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
        //    dpc.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 2, denpyoKbn);
        //    dpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 6, packDpyNo);

        //    DataTable dtResult = this._dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

        //    return dtResult;
        //}

        /// <summary>
        /// 自動仕訳履歴の存在チェック
        /// </summary>
        /// <param name="packDpyNo">一括伝票番号</param>
        /// <param name="denpyoKbn">伝票区分</param>
        /// <returns></returns>
        private DataTable GetJidoSwkRirekiExistChk(int packDpyNo, int denpyoKbn)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT ");
            sql.Append("  COUNT(*) AS 件数 ");
            sql.Append("FROM ");
            sql.Append("  TB_HN_ZIDO_SHIWAKE_RIREKI ");
            sql.Append("WHERE ");
            sql.Append("    KAISHA_CD    = @KAISHA_CD ");
            sql.Append("AND SHISHO_CD    = @SHISHO_CD ");
            sql.Append("AND KAIKEI_NENDO = @KAIKEI_NENDO ");
            sql.Append("AND DENPYO_KUBUN = @DENPYO_KUBUN ");
            sql.Append("AND DENPYO_BANGO = @DENPYO_BANGO ");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
            dpc.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 2, denpyoKbn);
            dpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 6, packDpyNo);

            DataTable dtResult = this._dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            return dtResult;
        }

        /// <summary>
        /// TB_ZM_SHIWAKE_DENPYOを削除する
        /// </summary>
        /// <param name="packDpyNo">一括伝票番号</param>
        /// <param name="denpyoKbn">伝票区分</param>
        /// <returns>更新件数</returns>
        private int DeleteTB_ZM_SHIWAKE_DENPYO(int packDpyNo, int denpyoKbn)
        {
            StringBuilder where = new StringBuilder();
            where.Append("    KAISHA_CD = @KAISHA_CD ");
            where.Append("AND SHISHO_CD = @SHISHO_CD ");
            where.Append("AND KAIKEI_NENDO = @KAIKEI_NENDO ");
            where.Append("AND DENPYO_BANGO IN (SELECT DISTINCT ");
            where.Append("                       X.SHIWAKE_DENPYO_BANGO ");
            where.Append("                     FROM ");
            where.Append("                       TB_HN_ZIDO_SHIWAKE_RIREKI AS X ");
            where.Append("                     WHERE ");
            where.Append("                         X.KAISHA_CD = @KAISHA_CD ");
            where.Append("                     AND X.SHISHO_CD = @SHISHO_CD ");
            where.Append("                     AND X.KAIKEI_NENDO = @KAIKEI_NENDO ");
            where.Append("                     AND X.DENPYO_KUBUN = @DENPYO_KUBUN ");
            where.Append("                     AND X.DENPYO_BANGO = @DENPYO_BANGO ");
            where.Append("                    ) ");

            DbParamCollection whereDpc = new DbParamCollection();
            whereDpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            whereDpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
            whereDpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
            whereDpc.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 2, denpyoKbn);
            whereDpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 6, packDpyNo);

            return this._dba.Delete("TB_ZM_SHIWAKE_DENPYO", where.ToString(), whereDpc);
        }

        /// <summary>
        /// TB_ZM_SHIWAKE_MEISAIを削除する
        /// </summary>
        /// <param name="packDpyNo">一括伝票番号</param>
        /// <param name="denpyoKbn">伝票区分</param>
        /// <returns>更新件数</returns>
        private int DeleteTB_ZM_SHIWAKE_MEISAI(int packDpyNo, int denpyoKbn)
        {
            StringBuilder where = new StringBuilder();
            where.Append("    KAISHA_CD = @KAISHA_CD ");
            where.Append("AND SHISHO_CD = @SHISHO_CD ");
            where.Append("AND KAIKEI_NENDO = @KAIKEI_NENDO ");
            where.Append("AND DENPYO_BANGO IN (SELECT DISTINCT ");
            where.Append("                       X.SHIWAKE_DENPYO_BANGO ");
            where.Append("                     FROM ");
            where.Append("                       TB_HN_ZIDO_SHIWAKE_RIREKI AS X ");
            where.Append("                     WHERE ");
            where.Append("                         X.KAISHA_CD = @KAISHA_CD ");
            where.Append("                     AND X.SHISHO_CD = @SHISHO_CD ");
            where.Append("                     AND X.KAIKEI_NENDO = @KAIKEI_NENDO ");
            where.Append("                     AND X.DENPYO_KUBUN = @DENPYO_KUBUN ");
            where.Append("                     AND X.DENPYO_BANGO = @DENPYO_BANGO ");
            where.Append("                    ) ");

            DbParamCollection whereDpc = new DbParamCollection();
            whereDpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            whereDpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
            whereDpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
            whereDpc.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 2, denpyoKbn);
            whereDpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 6, packDpyNo);

            return this._dba.Delete("TB_ZM_SHIWAKE_MEISAI", where.ToString(), whereDpc);
        }

        /// <summary>
        /// TB_HN_ZIDO_SHIWAKE_RIREKIを削除する
        /// </summary>
        /// <param name="packDpyNo">一括伝票番号</param>
        /// <param name="denpyoKbn">伝票区分</param>
        /// <returns>更新件数</returns>
        private int DeleteTB_HN_ZIDO_SHIWAKE_RIREKI(int packDpyNo, int denpyoKbn)
        {
            StringBuilder where = new StringBuilder();
            where.Append("    KAISHA_CD = @KAISHA_CD ");
            where.Append("AND SHISHO_CD = @SHISHO_CD ");
            where.Append("AND KAIKEI_NENDO = @KAIKEI_NENDO ");
            where.Append("AND DENPYO_KUBUN = @DENPYO_KUBUN ");
            where.Append("AND DENPYO_BANGO = @DENPYO_BANGO ");

            DbParamCollection whereDpc = new DbParamCollection();
            whereDpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            whereDpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
            whereDpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
            whereDpc.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 2, denpyoKbn);
            whereDpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 6, packDpyNo);

            return this._dba.Delete("TB_HN_ZIDO_SHIWAKE_RIREKI", where.ToString(), whereDpc);
        }

        /// <summary>
        /// 仕切データテーブルへの取消処理を実行
        /// </summary>
        /// <param name="packDpyNo">取消対象の一括伝票番号</param>
        /// <returns>更新件数</returns>
        private int UpdateCancelTB_HN_SHIKIRI_DATA(int packDpyNo)
        {
            StringBuilder where = new StringBuilder();
            where.Append("    KAISHA_CD = @KAISHA_CD ");
            where.Append("AND SHISHO_CD = @SHISHO_CD ");
            where.Append("AND KAIKEI_NENDO = @KAIKEI_NENDO ");
            where.Append("AND DENPYO_KUBUN = 3 ");
            where.Append("AND IKKATSU_DENPYO_BANGO = @CNCL_IKKATSU_DENPYO_BANGO ");

            DbParamCollection updDpc = new DbParamCollection();
            updDpc.SetParam("@IKKATSU_DENPYO_BANGO", SqlDbType.Decimal, 6, 0);
            updDpc.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWTIME");

            DbParamCollection whereDpc = new DbParamCollection();
            whereDpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            whereDpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
            whereDpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
            whereDpc.SetParam("@CNCL_IKKATSU_DENPYO_BANGO", SqlDbType.Decimal, 6, packDpyNo);

            return this._dba.Update("TB_HN_SHIKIRI_DATA", updDpc, where.ToString(), whereDpc);
        }

        /// <summary>
        /// 控除データテーブルへの取消処理を実行
        /// </summary>
        /// <param name="packDpyNo">取消対象の一括伝票番号</param>
        /// <returns>更新件数</returns>
        private int UpdateCancelTB_HN_KOJO_DATA(int packDpyNo)
        {
            StringBuilder where = new StringBuilder();
            where.Append("    KAISHA_CD = @KAISHA_CD ");
            where.Append("AND SHISHO_CD = @SHISHO_CD ");
            where.Append("AND KAIKEI_NENDO = @KAIKEI_NENDO ");
            where.Append("AND IKKATSU_DENPYO_BANGO = @CNCL_IKKATSU_DENPYO_BANGO ");

            DbParamCollection updDpc = new DbParamCollection();
            updDpc.SetParam("@IKKATSU_DENPYO_BANGO", SqlDbType.Decimal, 6, 0);
            updDpc.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWTIME");

            DbParamCollection whereDpc = new DbParamCollection();
            whereDpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            whereDpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
            whereDpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
            whereDpc.SetParam("@CNCL_IKKATSU_DENPYO_BANGO", SqlDbType.Decimal, 6, packDpyNo);

            return this._dba.Update("TB_HN_KOJO_DATA", updDpc, where.ToString(), whereDpc);
        }

        /// <summary>
        /// 最終伝票番号を取得する
        /// </summary>
        /// <returns>取得したデータ</returns>
        private DataTable GetSaisyuDenpyoNo()
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT ");
            sql.Append("  MAX(DENPYO_BANGO) AS 最終伝票番号 ");
            sql.Append("FROM ");
            sql.Append("  TB_ZM_SHIWAKE_DENPYO ");
            sql.Append("WHERE ");
            sql.Append("    KAISHA_CD = @KAISHA_CD ");
            sql.Append("AND SHISHO_CD = @SHISHO_CD ");
            sql.Append("AND KAIKEI_NENDO = @KAIKEI_NENDO ");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);

            DataTable dtResult = this._dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            return dtResult;
        }

        /// <summary>
        /// 仕訳伝票の存在チェックのためのデータ取得
        /// </summary>
        /// <param name="swkDpyNo">仕訳伝票番号</param>
        /// <returns>取得したデータ</returns>
        private DataTable GetChkShiwakeDenpyo(int swkDpyNo)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT ");
            sql.Append("  * ");
            sql.Append("FROM ");
            sql.Append("  TB_ZM_SHIWAKE_DENPYO ");
            sql.Append("WHERE ");
            sql.Append("    KAISHA_CD = @KAISHA_CD ");
            sql.Append("AND SHISHO_CD = @SHISHO_CD ");
            sql.Append("AND KAIKEI_NENDO = @KAIKEI_NENDO ");
            sql.Append("AND DENPYO_BANGO = @DENPYO_BANGO ");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
            dpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 6, swkDpyNo);

            DataTable dtResult = this._dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            return dtResult;
        }

        /// <summary>
        /// 仕訳伝票の登録
        /// </summary>
        /// <param name="swkDpyNo">仕訳伝票番号</param>
        /// <param name="condition">条件画面の入力内容</param>
        /// <returns>登録件数</returns>
        private int InsertTB_ZM_SHIWAKE_DENPYO(int swkDpyNo, Hashtable condition)
        {
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
            dpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 6, swkDpyNo);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
            dpc.SetParam("@DENPYO_DATE", SqlDbType.DateTime, condition["SwkDpyDt"]);
            dpc.SetParam("@TANTOSHA_CD", SqlDbType.Decimal, 4, condition["TantoCd"]);
            dpc.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWTIME");

            return this._dba.Insert("TB_ZM_SHIWAKE_DENPYO", dpc);
        }

        /// <summary>
        /// 仕訳明細の存在チェックのためのデータ取得
        /// </summary>
        /// <param name="swkDpyNo">仕訳伝票番号</param>
        /// <param name="meisaiInfo">明細情報</param>
        /// <returns>取得したデータ</returns>
        private DataTable GetChkShiwakeMeisai(int swkDpyNo, DataRow meisaiInfo)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT ");
            sql.Append("  * ");
            sql.Append("FROM ");
            sql.Append("  TB_ZM_SHIWAKE_MEISAI ");
            sql.Append("WHERE ");
            sql.Append("    KAISHA_CD = @KAISHA_CD ");
            sql.Append("AND SHISHO_CD = @SHISHO_CD ");
            sql.Append("AND KAIKEI_NENDO = @KAIKEI_NENDO ");
            sql.Append("AND DENPYO_BANGO = @DENPYO_BANGO ");
            sql.Append("AND GYO_BANGO = @GYO_BANGO ");
            sql.Append("AND TAISHAKU_KUBUN = @TAISHAKU_KUBUN ");
            sql.Append("AND MEISAI_KUBUN = @MEISAI_KUBUN ");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
            dpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 6, swkDpyNo);
            dpc.SetParam("@GYO_BANGO", SqlDbType.Decimal, 10, meisaiInfo["GYO_BANGO"]);
            dpc.SetParam("@TAISHAKU_KUBUN", SqlDbType.Decimal, 6, meisaiInfo["TAISHAKU_KUBUN"]);
            dpc.SetParam("@MEISAI_KUBUN", SqlDbType.Decimal, 6, meisaiInfo["MEISAI_KUBUN"]);

            DataTable dtResult = this._dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            return dtResult;
        }

        /// <summary>
        /// 仕訳明細の登録
        /// </summary>
        /// <param name="swkDpyNo">仕訳伝票番号</param>
        /// <param name="condition">条件画面の入力内容</param>
        /// <param name="meisaiInfo">明細情報</param>
        /// <returns>登録件数</returns>
        private int InsertTB_ZM_SHIWAKE_MEISAI(int swkDpyNo, Hashtable condition, DataRow meisaiInfo)
        {
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
            dpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 6, swkDpyNo);
            dpc.SetParam("@GYO_BANGO", SqlDbType.Decimal, 10, meisaiInfo["GYO_BANGO"]);
            dpc.SetParam("@TAISHAKU_KUBUN", SqlDbType.Decimal, 1, meisaiInfo["TAISHAKU_KUBUN"]);
            dpc.SetParam("@MEISAI_KUBUN", SqlDbType.Decimal, 1, meisaiInfo["MEISAI_KUBUN"]);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
            dpc.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 1, meisaiInfo["DENPYO_KUBUN"]);
            dpc.SetParam("@DENPYO_DATE", SqlDbType.DateTime, condition["SwkDpyDt"]);
            dpc.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 6, meisaiInfo["KANJO_KAMOKU_CD"]);
            dpc.SetParam("@HOJO_KAMOKU_CD", SqlDbType.Decimal, 10, meisaiInfo["HOJO_KAMOKU_CD"]);
            dpc.SetParam("@BUMON_CD", SqlDbType.Decimal, 4, meisaiInfo["BUMON_CD"]);

            //dpc.SetParam("@KOJI_CD", SqlDbType.Decimal, 4, 0);
            //dpc.SetParam("@KOSHU_CD", SqlDbType.Decimal, 4, 0);

            dpc.SetParam("@TEKIYO_CD", SqlDbType.Decimal, 4, meisaiInfo["TEKIYO_CD"]);
            dpc.SetParam("@TEKIYO", SqlDbType.VarChar, 40, meisaiInfo["TEKIYO"]);
            dpc.SetParam("@ZEIKOMI_KINGAKU", SqlDbType.Decimal, 15, meisaiInfo["ZEIKOMI_KINGAKU"]);
            dpc.SetParam("@ZEINUKI_KINGAKU", SqlDbType.Decimal, 15, meisaiInfo["ZEINUKI_KINGAKU"]);
            dpc.SetParam("@SHOHIZEI_KINGAKU", SqlDbType.Decimal, 15, meisaiInfo["SHOHIZEI_KINGAKU"]);
            dpc.SetParam("@ZEI_KUBUN", SqlDbType.Decimal, 2, meisaiInfo["ZEI_KUBUN"]);
            dpc.SetParam("@KAZEI_KUBUN", SqlDbType.Decimal, 1, meisaiInfo["KAZEI_KUBUN"]);
            dpc.SetParam("@TORIHIKI_KUBUN", SqlDbType.Decimal, 2, meisaiInfo["TORIHIKI_KUBUN"]);
            dpc.SetParam("@ZEI_RITSU", SqlDbType.Decimal, 4, 2, meisaiInfo["ZEI_RITSU"]);
            dpc.SetParam("@JIGYO_KUBUN", SqlDbType.Decimal, 1, meisaiInfo["JIGYO_KUBUN"]);
            dpc.SetParam("@SHOHIZEI_NYURYOKU_HOHO", SqlDbType.Decimal, 1, meisaiInfo["SHOHIZEI_NYURYOKU_HOHO"]);
            dpc.SetParam("@SHOHIZEI_HENKO", SqlDbType.Decimal, 1, meisaiInfo["SHOHIZEI_HENKO"]);
            dpc.SetParam("@KESSAN_KUBUN", SqlDbType.Decimal, 1, meisaiInfo["KESSAN_KUBUN"]);
            dpc.SetParam("@SHIWAKE_SAKUSEI_KUBUN", SqlDbType.Decimal, 1, meisaiInfo["SHIWAKE_SAKUSEI_KUBUN"]);
            dpc.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWTIME");

            return this._dba.Insert("TB_ZM_SHIWAKE_MEISAI", dpc);
        }

        /// <summary>
        /// 自動仕訳履歴の存在チェックのためのデータ取得
        /// </summary>
        /// <param name="packDpyNo">一括伝票番号</param>
        /// <param name="swkDpyNo">仕訳伝票番号</param>
        /// <param name="denpyoKbn">伝票区分</param>
        /// <returns>取得したデータ</returns>
        private DataTable GetChkJidoSwkRireki(int packDpyNo, int swkDpyNo, int denpyoKbn)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT ");
            sql.Append("  A.DENPYO_BANGO            AS 伝票番号 ");
            sql.Append(" ,A.SHIWAKE_DENPYO_BANGO    AS 仕訳伝票番号 ");
            sql.Append(" ,A.DENPYO_DATE             AS 伝票日付 ");
            sql.Append(" ,A.SHORI_KUBUN             AS 処理区分 ");
            sql.Append(" ,CASE A.SHORI_KUBUN WHEN 1 THEN 'セリ販売' ");
            sql.Append("                     WHEN 2 THEN '控除' ");
            sql.Append("                     WHEN 3 THEN '一括' ");
            sql.Append("                     WHEN 4 THEN '支払' ");
            sql.Append("  END AS 処理区分名称 ");
            sql.Append(" ,A.SHIMEBI                 AS 締日 ");
            sql.Append(" ,A.KAISHI_DENPYO_DATE      AS 開始伝票日付 ");
            sql.Append(" ,A.SHURYO_DENPYO_DATE      AS 終了伝票日付 ");
            sql.Append(" ,A.KAISHI_SEIKYUSAKI_CD    AS 開始請求先コード ");
            sql.Append(" ,CASE WHEN ISNULL(A.KAISHI_SEIKYUSAKI_CD, '') = '' THEN '先　頭' ELSE B.TORIHIKISAKI_NM END AS 開始請求先名 ");
            sql.Append(" ,A.SHURYO_SEIKYUSAKI_CD  AS 終了請求先コード ");
            sql.Append(" ,CASE WHEN ISNULL(A.SHURYO_SEIKYUSAKI_CD, '') = '' THEN '最　後' ELSE C.TORIHIKISAKI_NM END AS 終了請求先名 ");
            sql.Append(" ,A.TEKIYO_CD               AS 摘要コード ");
            sql.Append(" ,A.TEKIYO                  AS 摘要名 ");
            sql.Append("FROM ");
            sql.Append("  TB_HN_ZIDO_SHIWAKE_RIREKI AS A ");
            sql.Append("LEFT OUTER JOIN VI_HN_TORIHIKISAKI_JOHO AS B ");
            sql.Append("ON  A.KAISHA_CD            = B.KAISHA_CD ");
            sql.Append("AND A.KAISHI_SEIKYUSAKI_CD = B.TORIHIKISAKI_CD ");
            sql.Append("AND B.TORIHIKISAKI_KUBUN1  = '1' ");
            sql.Append("LEFT OUTER JOIN VI_HN_TORIHIKISAKI_JOHO AS C ");
            sql.Append("ON  A.KAISHA_CD            = C.KAISHA_CD ");
            sql.Append("AND A.SHURYO_SEIKYUSAKI_CD = C.TORIHIKISAKI_CD ");
            sql.Append("AND C.TORIHIKISAKI_KUBUN1  = '1' ");
            sql.Append("WHERE ");
            sql.Append("    A.KAISHA_CD            = @KAISHA_CD ");
            sql.Append("AND A.SHISHO_CD            = @SHISHO_CD ");
            sql.Append("AND A.KAIKEI_NENDO         = @KAIKEI_NENDO ");
            sql.Append("AND A.DENPYO_KUBUN         = @DENPYO_KUBUN ");
            sql.Append("AND A.DENPYO_BANGO         = @DENPYO_BANGO ");
            sql.Append("AND A.SHIWAKE_DENPYO_BANGO = @SHIWAKE_DENPYO_BANGO ");
            sql.Append("ORDER BY ");
            sql.Append("  A.DENPYO_BANGO ");
            sql.Append(" ,A.SHIWAKE_DENPYO_BANGO ");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
            dpc.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 2, denpyoKbn);
            dpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 6, packDpyNo);
            dpc.SetParam("@SHIWAKE_DENPYO_BANGO", SqlDbType.Decimal, 6, swkDpyNo);

            DataTable dtResult = this._dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            return dtResult;
        }

        /// <summary>
        /// 自動仕訳履歴の登録
        /// </summary>
        /// <param name="condition">条件画面の入力内容</param>
        /// <param name="packDpyNo">一括伝票番号</param>
        /// <param name="swkDpyNo">仕訳伝票番号</param>
        /// <param name="denpyoKbn">伝票区分</param>
        /// <returns>登録件数</returns>
        private int InsertTB_HN_ZIDO_SHIWAKE_RIREKI(Hashtable condition, int packDpyNo, int swkDpyNo, int denpyoKbn)
        {
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
            dpc.SetParam("@DENPYO_KUBUN", SqlDbType.Decimal, 1, denpyoKbn);
            dpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 6, packDpyNo);
            dpc.SetParam("@SHIWAKE_DENPYO_BANGO", SqlDbType.Decimal, 6, swkDpyNo);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
            dpc.SetParam("@DENPYO_DATE", SqlDbType.DateTime, condition["SwkDpyDt"]);
            dpc.SetParam("@TEKIYO_CD", SqlDbType.Decimal, 4, Util.ToInt(condition["TekiyoCd"]));
            dpc.SetParam("@TEKIYO", SqlDbType.VarChar, 40, ValChk.IsEmpty(condition["Tekiyo"]) ? DBNull.Value : condition["Tekiyo"]);
            dpc.SetParam("@SHORI_KUBUN", SqlDbType.Decimal, 2, Util.ToInt(((HNSB1021.SKbn)condition["SakuseiKbn"]).ToString("D")) - 2);
            dpc.SetParam("@KAISHI_DENPYO_DATE", SqlDbType.DateTime, condition["SeisanDtFr"]);
            dpc.SetParam("@SHURYO_DENPYO_DATE", SqlDbType.DateTime, condition["SeisanDtTo"]);
            dpc.SetParam("@KAISHI_SEIKYUSAKI_CD", SqlDbType.VarChar, 4, condition["FunanushiCdFr"]);
            dpc.SetParam("@SHURYO_SEIKYUSAKI_CD", SqlDbType.VarChar, 4, condition["FunanushiCdTo"]);
            dpc.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWTIME");

            return this._dba.Insert("TB_HN_ZIDO_SHIWAKE_RIREKI", dpc);
        }

        /// <summary>
        /// 仕切データの更新
        /// </summary>
        /// <param name="packDpyNo">一括伝票番号</param>
        /// <param name="condition">条件画面の入力内容</param>
        /// <returns>更新件数</returns>
        private int UpdateShikiriData(int packDpyNo, Hashtable condition)
        {
            StringBuilder where = new StringBuilder();
            where.Append("    KAISHA_CD = @KAISHA_CD ");
            where.Append("AND SHISHO_CD = @SHISHO_CD ");
            where.Append("AND KAIKEI_NENDO = @KAIKEI_NENDO ");
            where.Append("AND DENPYO_KUBUN = 3 ");
            where.Append("AND DENPYO_BANGO IN (SELECT DISTINCT ");
            where.Append("                       A.DENPYO_BANGO ");
            where.Append("                     FROM ");
            where.Append("                       TB_HN_SHIKIRI_MEISAI AS A ");
            where.Append("                     LEFT OUTER JOIN TB_HN_SHIKIRI_DATA AS B ");
            where.Append("                     ON  A.KAISHA_CD    = B.KAISHA_CD ");
            where.Append("                     AND A.SHISHO_CD    = B.SHISHO_CD ");
            where.Append("                     AND A.KAIKEI_NENDO = B.KAIKEI_NENDO ");
            where.Append("                     AND A.DENPYO_KUBUN = B.DENPYO_KUBUN ");
            where.Append("                     AND A.DENPYO_BANGO = B.DENPYO_BANGO ");
            where.Append("                     LEFT OUTER JOIN VI_HN_TORIHIKISAKI_JOHO AS C ");
            where.Append("                     ON  B.KAISHA_CD   = C.KAISHA_CD ");
            where.Append("                     AND B.SENSHU_CD   = C.TORIHIKISAKI_CD ");
            where.Append("                     LEFT OUTER JOIN TB_HN_ZIDO_SHIWAKE_SETTEI_B AS D ");
            where.Append("                     ON  A.KAISHA_CD    = D.KAISHA_CD ");
            where.Append("                     AND A.SHISHO_CD    = D.SHISHO_CD ");
            where.Append("                     AND A.DENPYO_KUBUN = D.DENPYO_KUBUN ");
            where.Append("                     AND A.SHIWAKE_CD   = D.SHIWAKE_CD ");
            where.Append("                     LEFT OUTER JOIN TB_HN_ZIDO_SHIWAKE_SETTEI_A AS H ");
            where.Append("                     ON  B.KAISHA_CD    = H.KAISHA_CD ");
            where.Append("                     AND B.SHISHO_CD    = H.SHISHO_CD ");
            where.Append("                     AND B.DENPYO_KUBUN = H.DENPYO_KUBUN ");
            where.Append("                     AND ((B.TORIHIKI_KUBUN1 * 10) + B.TORIHIKI_KUBUN2) = H.SHIWAKE_CD ");
            where.Append("                     WHERE ");
            where.Append("                         A.KAISHA_CD = @KAISHA_CD ");
            where.Append("                     AND A.SHISHO_CD = @SHISHO_CD ");
            where.Append("                     AND A.KAIKEI_NENDO = @KAIKEI_NENDO ");
            where.Append("                     AND A.DENPYO_KUBUN = 3 ");
            where.Append("                     AND B.KEIJO_NENGAPPI BETWEEN @KEIJO_NENGAPPI_FR AND @KEIJO_NENGAPPI_TO ");
            where.Append("                     AND B.SENSHU_CD BETWEEN @FUNANUSHI_CD_FR AND @FUNANUSHI_CD_TO ");
            where.Append("                     AND B.SHIWAKE_DENPYO_BANGO = 0 ");
            where.Append("                     AND A.KINGAKU <> 0 ");
            where.Append("                     AND ISNULL(D.KANJO_KAMOKU_CD, 0) <> 0 ");
            where.Append("                     AND ISNULL(H.KANJO_KAMOKU_CD, 0) <> 0 ");
            where.Append("                    ) ");

            DbParamCollection whereDpc = new DbParamCollection();
            whereDpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            whereDpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
            whereDpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
            whereDpc.SetParam("@KEIJO_NENGAPPI_FR", SqlDbType.DateTime, Util.ToDate(condition["SeisanDtFr"]));
            whereDpc.SetParam("@KEIJO_NENGAPPI_TO", SqlDbType.DateTime, Util.ToDate(condition["SeisanDtTo"]));
            whereDpc.SetParam("@FUNANUSHI_CD_FR", SqlDbType.VarChar, 4, Util.ToString(condition["FunanushiCdFr"]));
            whereDpc.SetParam("@FUNANUSHI_CD_TO", SqlDbType.VarChar, 4, Util.ToString(condition["FunanushiCdTo"]));

            DbParamCollection updDpc = new DbParamCollection();
            updDpc.SetParam("@IKKATSU_DENPYO_BANGO", SqlDbType.Decimal, 6, packDpyNo);
            updDpc.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWTIME");

            return this._dba.Update("TB_HN_SHIKIRI_DATA", updDpc, where.ToString(), whereDpc);
        }

        /// <summary>
        /// 控除データの更新
        /// </summary>
        /// <param name="packDpyNo">一括伝票番号</param>
        /// <param name="condition">条件画面の入力内容</param>
        /// <returns>更新件数</returns>
        private int UpdateKojoData(int packDpyNo, Hashtable condition)
        {
            StringBuilder where = new StringBuilder();
            where.Append("    KAISHA_CD = @KAISHA_CD ");
            where.Append("AND SHISHO_CD = @SHISHO_CD ");
            where.Append("AND KAIKEI_NENDO = @KAIKEI_NENDO ");
            where.Append("AND SEISAN_BI BETWEEN @SEISAN_BI_FR AND @SEISAN_BI_TO ");
            where.Append("AND SENSHU_CD BETWEEN @FUNANUSHI_CD_FR AND @FUNANUSHI_CD_TO ");
            where.Append("AND SEISAN_KUBUN = 3 ");

            DbParamCollection whereDpc = new DbParamCollection();
            whereDpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            whereDpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
            whereDpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
            whereDpc.SetParam("@SEISAN_BI_FR", SqlDbType.DateTime, Util.ToDate(condition["SeisanDtFr"]));
            whereDpc.SetParam("@SEISAN_BI_TO", SqlDbType.DateTime, Util.ToDate(condition["SeisanDtTo"]));
            whereDpc.SetParam("@FUNANUSHI_CD_FR", SqlDbType.VarChar, 4, Util.ToString(condition["FunanushiCdFr"]));
            whereDpc.SetParam("@FUNANUSHI_CD_TO", SqlDbType.VarChar, 4, Util.ToString(condition["FunanushiCdTo"]));

            DbParamCollection updDpc = new DbParamCollection();
            updDpc.SetParam("@IKKATSU_DENPYO_BANGO", SqlDbType.Decimal, 6, packDpyNo);
            updDpc.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWTIME");

            return this._dba.Update("TB_HN_KOJO_DATA", updDpc, where.ToString(), whereDpc);
        }
        #endregion
    }
}
