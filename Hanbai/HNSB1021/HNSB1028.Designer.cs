﻿namespace jp.co.fsi.hn.hnsb1021
{
    partial class HNSB1028
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblKashiZei = new System.Windows.Forms.Label();
            this.lblKashiAmount = new System.Windows.Forms.Label();
            this.lblKariZei = new System.Windows.Forms.Label();
            this.lblKariAmount = new System.Windows.Forms.Label();
            this.mtbList = new jp.co.fsi.common.controls.SjMultiTable();
            this.lblKashikataSum = new System.Windows.Forms.Label();
            this.lblKarikataSum = new System.Windows.Forms.Label();
            this.lblDpyDt = new System.Windows.Forms.Label();
            this.lblDpyDtDay = new System.Windows.Forms.Label();
            this.txtDpyDtDay = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblDpyDtMonth = new System.Windows.Forms.Label();
            this.txtDpyDtMonth = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblDpyDtJpYear = new System.Windows.Forms.Label();
            this.txtDpyDtJpYear = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblGengo = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Size = new System.Drawing.Size(685, 23);
            this.lblTitle.TabIndex = 999;
            this.lblTitle.Text = "仕訳データ参照";
            // 
            // btnEsc
            // 
            this.btnEsc.Location = new System.Drawing.Point(3, 49);
            // 
            // btnF1
            // 
            this.btnF1.Visible = false;
            // 
            // btnF2
            // 
            this.btnF2.Visible = false;
            // 
            // btnF3
            // 
            this.btnF3.Visible = false;
            // 
            // btnF4
            // 
            this.btnF4.Visible = false;
            // 
            // btnF5
            // 
            this.btnF5.Visible = false;
            // 
            // btnF7
            // 
            this.btnF7.Location = new System.Drawing.Point(67, 49);
            // 
            // btnF6
            // 
            this.btnF6.Visible = false;
            // 
            // btnF8
            // 
            this.btnF8.Location = new System.Drawing.Point(131, 49);
            // 
            // btnF9
            // 
            this.btnF9.Visible = false;
            // 
            // btnF11
            // 
            this.btnF11.Visible = false;
            // 
            // btnF10
            // 
            this.btnF10.Visible = false;
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(5, 363);
            this.pnlDebug.Size = new System.Drawing.Size(702, 100);
            // 
            // lblKashiZei
            // 
            this.lblKashiZei.BackColor = System.Drawing.Color.White;
            this.lblKashiZei.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblKashiZei.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblKashiZei.ForeColor = System.Drawing.Color.Black;
            this.lblKashiZei.Location = new System.Drawing.Point(565, 335);
            this.lblKashiZei.Name = "lblKashiZei";
            this.lblKashiZei.Size = new System.Drawing.Size(120, 20);
            this.lblKashiZei.TabIndex = 14;
            this.lblKashiZei.Text = "-99,999,999,999";
            this.lblKashiZei.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblKashiAmount
            // 
            this.lblKashiAmount.BackColor = System.Drawing.Color.White;
            this.lblKashiAmount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblKashiAmount.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblKashiAmount.ForeColor = System.Drawing.Color.Black;
            this.lblKashiAmount.Location = new System.Drawing.Point(565, 317);
            this.lblKashiAmount.Name = "lblKashiAmount";
            this.lblKashiAmount.Size = new System.Drawing.Size(120, 20);
            this.lblKashiAmount.TabIndex = 13;
            this.lblKashiAmount.Text = "-99,999,999,999";
            this.lblKashiAmount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblKariZei
            // 
            this.lblKariZei.BackColor = System.Drawing.Color.White;
            this.lblKariZei.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblKariZei.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblKariZei.ForeColor = System.Drawing.Color.Black;
            this.lblKariZei.Location = new System.Drawing.Point(229, 335);
            this.lblKariZei.Name = "lblKariZei";
            this.lblKariZei.Size = new System.Drawing.Size(120, 20);
            this.lblKariZei.TabIndex = 11;
            this.lblKariZei.Text = "-99,999,999,999";
            this.lblKariZei.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblKariAmount
            // 
            this.lblKariAmount.BackColor = System.Drawing.Color.White;
            this.lblKariAmount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblKariAmount.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblKariAmount.ForeColor = System.Drawing.Color.Black;
            this.lblKariAmount.Location = new System.Drawing.Point(229, 317);
            this.lblKariAmount.Name = "lblKariAmount";
            this.lblKariAmount.Size = new System.Drawing.Size(120, 20);
            this.lblKariAmount.TabIndex = 10;
            this.lblKariAmount.Text = "-99,999,999,999";
            this.lblKariAmount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // mtbList
            // 
            this.mtbList.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.mtbList.FixedCols = 0;
            this.mtbList.FocusField = null;
            this.mtbList.Location = new System.Drawing.Point(13, 36);
            this.mtbList.Name = "mtbList";
            this.mtbList.NotSelectableCols = 0;
            this.mtbList.SelectRange = null;
            this.mtbList.Size = new System.Drawing.Size(673, 281);
            this.mtbList.TabIndex = 8;
            this.mtbList.Text = "sjMultiTable1";
            this.mtbList.UndoBufferEnabled = false;
            // 
            // lblKashikataSum
            // 
            this.lblKashikataSum.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.lblKashikataSum.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblKashikataSum.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblKashikataSum.ForeColor = System.Drawing.Color.White;
            this.lblKashikataSum.Location = new System.Drawing.Point(348, 317);
            this.lblKashikataSum.Name = "lblKashikataSum";
            this.lblKashikataSum.Size = new System.Drawing.Size(218, 38);
            this.lblKashikataSum.TabIndex = 12;
            this.lblKashikataSum.Text = "[貸方合計] ";
            this.lblKashikataSum.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblKarikataSum
            // 
            this.lblKarikataSum.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.lblKarikataSum.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblKarikataSum.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblKarikataSum.ForeColor = System.Drawing.Color.White;
            this.lblKarikataSum.Location = new System.Drawing.Point(12, 317);
            this.lblKarikataSum.Name = "lblKarikataSum";
            this.lblKarikataSum.Size = new System.Drawing.Size(218, 38);
            this.lblKarikataSum.TabIndex = 9;
            this.lblKarikataSum.Text = "[借方合計] ";
            this.lblKarikataSum.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblDpyDt
            // 
            this.lblDpyDt.BackColor = System.Drawing.Color.Silver;
            this.lblDpyDt.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDpyDt.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblDpyDt.Location = new System.Drawing.Point(13, 9);
            this.lblDpyDt.Name = "lblDpyDt";
            this.lblDpyDt.Size = new System.Drawing.Size(264, 24);
            this.lblDpyDt.TabIndex = 0;
            this.lblDpyDt.Text = "伝 票 日 付";
            this.lblDpyDt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDpyDtDay
            // 
            this.lblDpyDtDay.BackColor = System.Drawing.Color.Silver;
            this.lblDpyDtDay.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDpyDtDay.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblDpyDtDay.Location = new System.Drawing.Point(256, 11);
            this.lblDpyDtDay.Name = "lblDpyDtDay";
            this.lblDpyDtDay.Size = new System.Drawing.Size(18, 20);
            this.lblDpyDtDay.TabIndex = 7;
            this.lblDpyDtDay.Text = "日";
            this.lblDpyDtDay.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDpyDtDay
            // 
            this.txtDpyDtDay.AutoSizeFromLength = true;
            this.txtDpyDtDay.DisplayLength = null;
            this.txtDpyDtDay.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDpyDtDay.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtDpyDtDay.Location = new System.Drawing.Point(232, 11);
            this.txtDpyDtDay.MaxLength = 2;
            this.txtDpyDtDay.Name = "txtDpyDtDay";
            this.txtDpyDtDay.ReadOnly = true;
            this.txtDpyDtDay.Size = new System.Drawing.Size(22, 20);
            this.txtDpyDtDay.TabIndex = 6;
            this.txtDpyDtDay.TabStop = false;
            this.txtDpyDtDay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblDpyDtMonth
            // 
            this.lblDpyDtMonth.BackColor = System.Drawing.Color.Silver;
            this.lblDpyDtMonth.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDpyDtMonth.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblDpyDtMonth.Location = new System.Drawing.Point(213, 11);
            this.lblDpyDtMonth.Name = "lblDpyDtMonth";
            this.lblDpyDtMonth.Size = new System.Drawing.Size(18, 20);
            this.lblDpyDtMonth.TabIndex = 5;
            this.lblDpyDtMonth.Text = "月";
            this.lblDpyDtMonth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDpyDtMonth
            // 
            this.txtDpyDtMonth.AutoSizeFromLength = true;
            this.txtDpyDtMonth.DisplayLength = null;
            this.txtDpyDtMonth.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDpyDtMonth.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtDpyDtMonth.Location = new System.Drawing.Point(189, 11);
            this.txtDpyDtMonth.MaxLength = 2;
            this.txtDpyDtMonth.Name = "txtDpyDtMonth";
            this.txtDpyDtMonth.ReadOnly = true;
            this.txtDpyDtMonth.Size = new System.Drawing.Size(22, 20);
            this.txtDpyDtMonth.TabIndex = 4;
            this.txtDpyDtMonth.TabStop = false;
            this.txtDpyDtMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblDpyDtJpYear
            // 
            this.lblDpyDtJpYear.BackColor = System.Drawing.Color.Silver;
            this.lblDpyDtJpYear.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDpyDtJpYear.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblDpyDtJpYear.Location = new System.Drawing.Point(170, 11);
            this.lblDpyDtJpYear.Name = "lblDpyDtJpYear";
            this.lblDpyDtJpYear.Size = new System.Drawing.Size(18, 20);
            this.lblDpyDtJpYear.TabIndex = 3;
            this.lblDpyDtJpYear.Text = "年";
            this.lblDpyDtJpYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDpyDtJpYear
            // 
            this.txtDpyDtJpYear.AutoSizeFromLength = true;
            this.txtDpyDtJpYear.DisplayLength = null;
            this.txtDpyDtJpYear.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDpyDtJpYear.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtDpyDtJpYear.Location = new System.Drawing.Point(146, 11);
            this.txtDpyDtJpYear.MaxLength = 2;
            this.txtDpyDtJpYear.Name = "txtDpyDtJpYear";
            this.txtDpyDtJpYear.ReadOnly = true;
            this.txtDpyDtJpYear.Size = new System.Drawing.Size(22, 20);
            this.txtDpyDtJpYear.TabIndex = 2;
            this.txtDpyDtJpYear.TabStop = false;
            this.txtDpyDtJpYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblGengo
            // 
            this.lblGengo.BackColor = System.Drawing.Color.Silver;
            this.lblGengo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGengo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGengo.Location = new System.Drawing.Point(99, 11);
            this.lblGengo.Name = "lblGengo";
            this.lblGengo.Size = new System.Drawing.Size(45, 20);
            this.lblGengo.TabIndex = 1;
            this.lblGengo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // HNSB1028
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(710, 466);
            this.Controls.Add(this.mtbList);
            this.Controls.Add(this.lblGengo);
            this.Controls.Add(this.lblDpyDtDay);
            this.Controls.Add(this.txtDpyDtDay);
            this.Controls.Add(this.lblDpyDtMonth);
            this.Controls.Add(this.txtDpyDtMonth);
            this.Controls.Add(this.lblDpyDtJpYear);
            this.Controls.Add(this.txtDpyDtJpYear);
            this.Controls.Add(this.lblKashiZei);
            this.Controls.Add(this.lblKashiAmount);
            this.Controls.Add(this.lblKariZei);
            this.Controls.Add(this.lblKariAmount);
            this.Controls.Add(this.lblKashikataSum);
            this.Controls.Add(this.lblKarikataSum);
            this.Controls.Add(this.lblDpyDt);
            this.Name = "HNSB1028";
            this.ShowFButton = true;
            this.ShowTitle = false;
            this.Text = "仕訳データ参照";
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.lblDpyDt, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblKarikataSum, 0);
            this.Controls.SetChildIndex(this.lblKashikataSum, 0);
            this.Controls.SetChildIndex(this.lblKariAmount, 0);
            this.Controls.SetChildIndex(this.lblKariZei, 0);
            this.Controls.SetChildIndex(this.lblKashiAmount, 0);
            this.Controls.SetChildIndex(this.lblKashiZei, 0);
            this.Controls.SetChildIndex(this.txtDpyDtJpYear, 0);
            this.Controls.SetChildIndex(this.lblDpyDtJpYear, 0);
            this.Controls.SetChildIndex(this.txtDpyDtMonth, 0);
            this.Controls.SetChildIndex(this.lblDpyDtMonth, 0);
            this.Controls.SetChildIndex(this.txtDpyDtDay, 0);
            this.Controls.SetChildIndex(this.lblDpyDtDay, 0);
            this.Controls.SetChildIndex(this.lblGengo, 0);
            this.Controls.SetChildIndex(this.mtbList, 0);
            this.pnlDebug.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblKashiZei;
        private System.Windows.Forms.Label lblKashiAmount;
        private System.Windows.Forms.Label lblKariZei;
        private System.Windows.Forms.Label lblKariAmount;
        private common.controls.SjMultiTable mtbList;
        private System.Windows.Forms.Label lblKashikataSum;
        private System.Windows.Forms.Label lblKarikataSum;
        private System.Windows.Forms.Label lblDpyDt;
        private System.Windows.Forms.Label lblDpyDtDay;
        private common.controls.FsiTextBox txtDpyDtDay;
        private System.Windows.Forms.Label lblDpyDtMonth;
        private common.controls.FsiTextBox txtDpyDtMonth;
        private System.Windows.Forms.Label lblDpyDtJpYear;
        private common.controls.FsiTextBox txtDpyDtJpYear;
        private System.Windows.Forms.Label lblGengo;
    }
}