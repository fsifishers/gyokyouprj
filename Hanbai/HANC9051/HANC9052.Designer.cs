﻿namespace jp.co.fsi.han.hanc9051
{
    partial class HANC9052
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtGyoshubnCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lbGyoshubnCd = new System.Windows.Forms.Label();
            this.txtGyoshubnNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.lbGyoshubnKana = new System.Windows.Forms.Label();
            this.txtGyoshubnKana = new jp.co.fsi.common.controls.FsiTextBox();
            this.lbGyoshubnNM = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Size = new System.Drawing.Size(335, 23);
            this.lblTitle.TabIndex = 999;
            this.lblTitle.Text = "魚種分類マスタ登録";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(5, 69);
            this.pnlDebug.Size = new System.Drawing.Size(368, 100);
            // 
            // txtGyoshubnCd
            // 
            this.txtGyoshubnCd.AutoSizeFromLength = true;
            this.txtGyoshubnCd.DisplayLength = null;
            this.txtGyoshubnCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtGyoshubnCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtGyoshubnCd.Location = new System.Drawing.Point(127, 13);
            this.txtGyoshubnCd.MaxLength = 7;
            this.txtGyoshubnCd.Name = "txtGyoshubnCd";
            this.txtGyoshubnCd.Size = new System.Drawing.Size(55, 20);
            this.txtGyoshubnCd.TabIndex = 1;
            this.txtGyoshubnCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGyoshubnCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtGyoshubnCd_Validating);
            // 
            // lbGyoshubnCd
            // 
            this.lbGyoshubnCd.BackColor = System.Drawing.Color.Silver;
            this.lbGyoshubnCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbGyoshubnCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lbGyoshubnCd.Location = new System.Drawing.Point(12, 13);
            this.lbGyoshubnCd.Name = "lbGyoshubnCd";
            this.lbGyoshubnCd.Size = new System.Drawing.Size(115, 20);
            this.lbGyoshubnCd.TabIndex = 0;
            this.lbGyoshubnCd.Text = "魚種分類コード";
            this.lbGyoshubnCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtGyoshubnNm
            // 
            this.txtGyoshubnNm.AutoSizeFromLength = false;
            this.txtGyoshubnNm.DisplayLength = null;
            this.txtGyoshubnNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtGyoshubnNm.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtGyoshubnNm.Location = new System.Drawing.Point(127, 50);
            this.txtGyoshubnNm.MaxLength = 20;
            this.txtGyoshubnNm.Name = "txtGyoshubnNm";
            this.txtGyoshubnNm.Size = new System.Drawing.Size(220, 20);
            this.txtGyoshubnNm.TabIndex = 3;
            this.txtGyoshubnNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtGyoshubnNm_Validating);
            // 
            // lbGyoshubnKana
            // 
            this.lbGyoshubnKana.BackColor = System.Drawing.Color.Silver;
            this.lbGyoshubnKana.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbGyoshubnKana.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lbGyoshubnKana.Location = new System.Drawing.Point(12, 88);
            this.lbGyoshubnKana.Name = "lbGyoshubnKana";
            this.lbGyoshubnKana.Size = new System.Drawing.Size(115, 20);
            this.lbGyoshubnKana.TabIndex = 4;
            this.lbGyoshubnKana.Text = "魚種分類カナ名";
            this.lbGyoshubnKana.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtGyoshubnKana
            // 
            this.txtGyoshubnKana.AutoSizeFromLength = false;
            this.txtGyoshubnKana.DisplayLength = null;
            this.txtGyoshubnKana.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtGyoshubnKana.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtGyoshubnKana.Location = new System.Drawing.Point(127, 88);
            this.txtGyoshubnKana.MaxLength = 20;
            this.txtGyoshubnKana.Name = "txtGyoshubnKana";
            this.txtGyoshubnKana.Size = new System.Drawing.Size(220, 20);
            this.txtGyoshubnKana.TabIndex = 5;
            // 
            // lbGyoshubnNM
            // 
            this.lbGyoshubnNM.BackColor = System.Drawing.Color.Silver;
            this.lbGyoshubnNM.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbGyoshubnNM.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lbGyoshubnNM.Location = new System.Drawing.Point(12, 50);
            this.lbGyoshubnNM.Name = "lbGyoshubnNM";
            this.lbGyoshubnNM.Size = new System.Drawing.Size(115, 20);
            this.lbGyoshubnNM.TabIndex = 2;
            this.lbGyoshubnNM.Text = "魚種分類名";
            this.lbGyoshubnNM.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // HANC9052
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(360, 172);
            this.Controls.Add(this.txtGyoshubnNm);
            this.Controls.Add(this.lbGyoshubnKana);
            this.Controls.Add(this.txtGyoshubnKana);
            this.Controls.Add(this.txtGyoshubnCd);
            this.Controls.Add(this.lbGyoshubnNM);
            this.Controls.Add(this.lbGyoshubnCd);
            this.Name = "HANC9052";
            this.ShowFButton = true;
            this.Text = "魚種分類マスタ登録";
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lbGyoshubnCd, 0);
            this.Controls.SetChildIndex(this.lbGyoshubnNM, 0);
            this.Controls.SetChildIndex(this.txtGyoshubnCd, 0);
            this.Controls.SetChildIndex(this.txtGyoshubnKana, 0);
            this.Controls.SetChildIndex(this.lbGyoshubnKana, 0);
            this.Controls.SetChildIndex(this.txtGyoshubnNm, 0);
            this.pnlDebug.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private jp.co.fsi.common.controls.FsiTextBox txtGyoshubnCd;
        private System.Windows.Forms.Label lbGyoshubnCd;
        private jp.co.fsi.common.controls.FsiTextBox txtGyoshubnNm;
        private System.Windows.Forms.Label lbGyoshubnKana;
        private jp.co.fsi.common.controls.FsiTextBox txtGyoshubnKana;
        private System.Windows.Forms.Label lbGyoshubnNM;
    };
}