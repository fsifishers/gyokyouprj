﻿namespace jp.co.fsi.han.hanc9051
{
    partial class HANC9051
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblGyoshubnKana = new System.Windows.Forms.Label();
            this.txtGyoshubnKana = new System.Windows.Forms.TextBox();
            this.dgvList = new System.Windows.Forms.DataGridView();
            this.pnlDebug.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvList)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.TabIndex = 999;
            this.lblTitle.Text = "魚種分類の登録";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Size = new System.Drawing.Size(847, 100);
            // 
            // lblGyoshubnKana
            // 
            this.lblGyoshubnKana.BackColor = System.Drawing.Color.Silver;
            this.lblGyoshubnKana.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGyoshubnKana.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGyoshubnKana.Location = new System.Drawing.Point(17, 50);
            this.lblGyoshubnKana.Name = "lblGyoshubnKana";
            this.lblGyoshubnKana.Size = new System.Drawing.Size(126, 20);
            this.lblGyoshubnKana.TabIndex = 0;
            this.lblGyoshubnKana.Text = "魚種分類カナ名";
            this.lblGyoshubnKana.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtGyoshubnKana
            // 
            this.txtGyoshubnKana.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGyoshubnKana.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtGyoshubnKana.Location = new System.Drawing.Point(144, 50);
            this.txtGyoshubnKana.MaxLength = 20;
            this.txtGyoshubnKana.Name = "txtGyoshubnKana";
            this.txtGyoshubnKana.Size = new System.Drawing.Size(180, 20);
            this.txtGyoshubnKana.TabIndex = 1;
            this.txtGyoshubnKana.Validating += new System.ComponentModel.CancelEventHandler(this.txtGyoshubnName_Validating);
            // 
            // dgvList
            // 
            this.dgvList.AllowUserToAddRows = false;
            this.dgvList.AllowUserToDeleteRows = false;
            this.dgvList.AllowUserToResizeColumns = false;
            this.dgvList.AllowUserToResizeRows = false;
            this.dgvList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvList.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.dgvList.Location = new System.Drawing.Point(17, 86);
            this.dgvList.MultiSelect = false;
            this.dgvList.Name = "dgvList";
            this.dgvList.ReadOnly = true;
            this.dgvList.RowHeadersVisible = false;
            this.dgvList.RowTemplate.Height = 21;
            this.dgvList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvList.Size = new System.Drawing.Size(530, 296);
            this.dgvList.TabIndex = 2;
            this.dgvList.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvList_CellDoubleClick);
            this.dgvList.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvList_KeyDown);
            // 
            // HANC9051
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 638);
            this.Controls.Add(this.dgvList);
            this.Controls.Add(this.txtGyoshubnKana);
            this.Controls.Add(this.lblGyoshubnKana);
            this.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.Name = "HANC9051";
            this.Text = "魚種分類の登録";
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.lblGyoshubnKana, 0);
            this.Controls.SetChildIndex(this.txtGyoshubnKana, 0);
            this.Controls.SetChildIndex(this.dgvList, 0);
            this.pnlDebug.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblGyoshubnKana;
        private System.Windows.Forms.TextBox txtGyoshubnKana;
        private System.Windows.Forms.DataGridView dgvList;

    }
}