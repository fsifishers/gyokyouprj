﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.hn.hncm1121
{
    /// <summary>
    /// 仲買人変換マスタメンテ(変更・追加)(HANC9162)
    /// </summary>
    public partial class HNCM1122 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// モード(新規)
        /// </summary>
        private const string MODE_NEW = "1";

        /// <summary>
        /// モード(編集)
        /// </summary>
        private const string MODE_EDIT = "2";
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HNCM1122()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="par1">引数1</param>
        public HNCM1122(string par1)
            : base(par1)
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 引数：Par1／モード(1:新規、2:変更)、InData：名護漁協コード
            if (MODE_NEW.Equals(this.Par1))
            {
                // 新規モードの初期表示
                InitDispOnNew();
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // 編集モードの初期表示
                InitDispOnEdit();
            }
            else
            {
                // 不正な起動として閉じる
                Msg.Error("不正な起動です。終了します。");
                this.Close();
            }

            // タイトルは非表示
            this.lblTitle.Visible = false;
            // EscapeとF1のみ表示
            this.ShowFButton = true;
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF1.Location = this.btnF2.Location;
            this.btnF6.Location = this.btnF4.Location;
            this.btnF2.Visible = false;
            this.btnF3.Visible = true;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF6.Visible = true;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;

            this.gbxGinozaGyo.Text = UInfo.KaishaNm;
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // フォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtNakagaiCd":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// Escボタン押下時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;
            string stCurrentDir = System.IO.Directory.GetCurrentDirectory();

            switch (this.ActiveCtlNm)
            {
                case "txtNakagaiCd":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"HNCM1011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.hn.hncm1011.HNCM1011");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            if (this.ActiveCtlNm == "txtNakagaiCd")
                            {
                                frm.InData = this.txtNakagaiCd.Text;
                            }
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                if (this.ActiveCtlNm == "txtNakagaiCd")
                                {
                                    // 仲買人CDが入力されたら名護仲買人CD、名を有効にする。
                                    // 仲買人名は変更出来てはいけないため無効。
                                    this.txtKenNakaCd.Enabled = true;
                                    this.txtKenNakaNm.Enabled = true;
                                    this.txtNakagaiNm.Enabled = false;

                                    this.txtNakagaiCd.Text = outData[0];
                                    this.txtNakagaiNm.Text = outData[1];
                                    this.txtKenNakaNm.Text = outData[1];

                                    this.txtKenNakaCd.Focus();

                                    // 仲買人CDを非活性にする
                                    this.txtNakagaiCd.Enabled = false;

                                    // 削除ボタンを表示する
                                    this.btnF3.Enabled = true;
                                }

                            }
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// F3キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF3();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF3()
        {
            if (!this.btnF3.Enabled)
            {
                return;
            }

            if (Msg.ConfYesNo("削除しますか？") == DialogResult.No)
            {
                return;
            }

            try
            {
                // トランザクションの開始
                this.Dba.BeginTransaction();

                DbParamCollection whereParam;
                whereParam = new DbParamCollection();

                whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
                whereParam.SetParam("@NIUKENIN_CD", SqlDbType.Decimal, 6, this.txtNakagaiCd.Text);
                this.Dba.Delete("TB_HN_NIUKENIN_CD_HENKAN_MST",
                    "KAISHA_CD = @KAISHA_CD AND NIUKENIN_CD = @NIUKENIN_CD",
                    whereParam);

                // トランザクションをコミット
                this.Dba.Commit();

                Msg.Info("削除しました。");
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF6();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF6()
        {
            DataTable a = GetNakagaiCd(Util.ToDecimal(this.txtNakagaiCd.Text));

            if (Util.ToDecimal(a.Rows[0]["KENSU"]) == 0)
            {
                this.Par1 = "1";
            }
            else
            {
                this.Par1 = "2";
            }
            // 確認メッセージを表示
            string msg = (MODE_NEW.Equals(this.Par1) ? "登録" : "更新") + "しますか？";
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            // 入力値をバインドパラメータとしてセットする
            ArrayList alParamsCmTanto = SetCmNakagaiParams();

            try
            {
                // トランザクションの開始
                this.Dba.BeginTransaction();

                if (MODE_NEW.Equals(this.Par1))
                {
                    // データ登録
                    // 仲買人CD変換マスタメンテ
                    this.Dba.Insert("TB_HN_NIUKENIN_CD_HENKAN_MST", (DbParamCollection)alParamsCmTanto[0]);
                }
                else if (MODE_EDIT.Equals(this.Par1))
                {
                    // データ更新
                    // 共通.商品マスタ // 仲買人CD変換マスタメンテ
                    this.Dba.Update("TB_HN_NIUKENIN_CD_HENKAN_MST",
                        (DbParamCollection)alParamsCmTanto[1],
                        // NIUKENIN_CDとNIUKENIN_CD_WHEREがある
                        // 更新だからWHEREの方から取ってくる。
                        "KAISHA_CD = @KAISHA_CD AND NIUKENIN_CD = @NIUKENIN_CD_WHERE",
                        (DbParamCollection)alParamsCmTanto[0]);
                }

                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            Msg.Info("登録しました。");
            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 名護仲買人CDの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKenNakaCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidKenGyoCd())
            {
                e.Cancel = true;
                this.txtKenNakaCd.SelectAll();
            }
        }

        /// <summary>
        /// 名護仲買人名の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKenNakaNm_Validating(object sender, CancelEventArgs e)
        {
            IsValidKenGyoNm();

            if (!IsValidKenGyoNm())
            {
                e.Cancel = true;
                this.txtKenNakaNm.SelectAll();
            }
        }

        /// <summary>
        /// 名護仲買人名Enter押下時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKenNakaNm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.Par1 == "1")
            {
                this.PressF6();
            }
        }

        private void txtNakagaiCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidNakagai())
            {
                e.Cancel = true;
                this.txtNakagaiCd.Focus();
                this.txtNakagaiCd.SelectAll();
            }
        }

        /// <summary>
        /// 仲買人名Enter押下時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtNakagaiCd_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (!IsValidNakagai())
                {
                    this.txtNakagaiCd.Focus();
                }
            }
        }

        /// <summary>
        /// 仲買人名の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtNakagaiNm_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidNakagaiNm())
            {
                e.Cancel = true;
                this.txtNakagaiNm.SelectAll();
            }
        }
        private void txtNakagaiNm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.PressF6();
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 新規モードの初期表示
        /// </summary>
        private void InitDispOnNew()
        {
            // 初期値、入力制御を実装
            // 初期値を設定
            this.txtKenNakaCd.Text = "0";
            this.txtNakagaiCd.Text = "0";
            // コントロール制御
            this.txtKenNakaCd.Enabled = false;
            this.txtKenNakaNm.Enabled = false;
            this.txtNakagaiNm.Enabled = false;

            // 名護仲買人CDに初期フォーカス
            this.ActiveControl = this.txtNakagaiCd;
            this.txtNakagaiCd.Focus();

            // 削除ボタン非表示
            this.btnF3.Enabled = false;
        }

        /// <summary>
        /// 編集モードの初期表示
        /// </summary>
        private void InitDispOnEdit()
        {
            Array indata = (Array)InData;
            // 現在DBに登録されている値、入力制御を実装
            StringBuilder cols = new StringBuilder();
            cols.Append("KAISHA_CD");
            cols.Append("    ,KEN_GYOREN_NIUKENIN_CD");
            cols.Append("    ,KEN_GYOREN_NIUKENIN_NM");
            cols.Append("    ,NIUKENIN_CD");
            cols.Append("    ,NIUKENIN_NM");

            StringBuilder from = new StringBuilder();
            from.Append("TB_HN_NIUKENIN_CD_HENKAN_MST");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@KEN_GYOREN_NIUKENIN_CD", SqlDbType.Decimal, 6, indata.GetValue(0));
            dpc.SetParam("@NIUKENIN_CD", SqlDbType.Decimal, 6, indata.GetValue(1));

            DataTable dtDispData =
                this.Dba.GetDataTableByConditionWithParams(
                    Util.ToString(cols), Util.ToString(from),
                    "KAISHA_CD = @KAISHA_CD AND KEN_GYOREN_NIUKENIN_CD = @KEN_GYOREN_NIUKENIN_CD AND NIUKENIN_CD = @NIUKENIN_CD ",
                    dpc);

            if (dtDispData.Rows.Count == 0)
            {
                Msg.Error("不正な起動です。終了します。");
                this.Close();
            }
            else
            {
                // 最初は名護仲買人CD、名、仲買人名を有効にする
                this.txtKenNakaCd.Enabled = true;
                this.txtKenNakaNm.Enabled = true;
                this.txtNakagaiNm.Enabled = true;

                // 取得した内容を表示
                DataRow drDispData = dtDispData.Rows[0];
                this.txtKenNakaCd.Text = Util.ToString(drDispData["KEN_GYOREN_NIUKENIN_CD"]);
                this.txtKenNakaNm.Text = Util.ToString(drDispData["KEN_GYOREN_NIUKENIN_NM"]);
                this.txtNakagaiCd.Text = Util.ToString(drDispData["NIUKENIN_CD"]);
                this.txtNakagaiNm.Text = Util.ToString(drDispData["NIUKENIN_NM"]);

                // 名護仲買人CDにフォーカス
                this.ActiveControl = this.txtKenNakaCd;
                this.txtKenNakaCd.Focus();
                this.txtKenNakaCd.SelectAll();

            }
        }

        /// <summary>
        /// 名護仲買人CDの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidKenGyoCd()
        {
            // 未入力はエラーとする
            if (ValChk.IsEmpty(this.txtKenNakaCd.Text))
            {
                Msg.Error("入力してください。");
                return false;
            }

            // 0はエラーとする
            if (this.txtKenNakaCd.Text == "0")
            {
                Msg.Error("0は登録できません。");
                return false;
            }

            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtKenNakaCd.Text))
            {
                Msg.Error("数字で入力してください。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 名護仲買人名の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidKenGyoNm()
        {
            // 未入力はエラーとする
            if (ValChk.IsEmpty(this.txtKenNakaNm.Text))
            {
                Msg.Error("入力してください。");
                return false;
            }
            // 0はエラーとする
            if (this.txtKenNakaNm.Text == "0")
            {
                Msg.Error("0は登録できません。");
                return false;
            }
            // 40バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtKenNakaNm.Text, this.txtKenNakaNm.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 仲買人名の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidNakagaiNm()
        {
            // 未入力はエラーとする
            if (ValChk.IsEmpty(this.txtNakagaiNm.Text))
            {
                Msg.Error("入力してください。");
                return false;
            }
            // 0はエラーとする
            if (this.txtNakagaiNm.Text == "0")
            {
                Msg.Error("0は登録できません。");
                return false;
            }
            // 40バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtNakagaiNm.Text, this.txtNakagaiNm.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 仲買人CDの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidNakagai()
        {
            // 未入力はエラー
            if (ValChk.IsEmpty(this.txtNakagaiCd.Text))
            {
                Msg.Error("入力してください。");
                return false;
            }
            // 0はエラーとする
            if (this.txtNakagaiCd.Text == "0")
            {
                Msg.Error("0は登録できません。");
                return false;
            }
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtNakagaiCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 新規の場合
            if (MODE_NEW == (this.Par1))
            {
                // 空でない場合、入力された仲買人CDから検索する
                if (!ValChk.IsEmpty(this.txtNakagaiCd.Text))
                {
                    // 現在DBに登録されている値をtextboxに表示
                    StringBuilder cols = new StringBuilder();
                    cols.Append("KAISHA_CD");
                    cols.Append("    ,TORIHIKISAKI_CD");
                    cols.Append("    ,TORIHIKISAKI_NM");
                    cols.Append("    ,TORIHIKISAKI_KUBUN2");

                    StringBuilder from = new StringBuilder();
                    from.Append("VI_HN_NAKAGAININ_JOHO");

                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
                    dpc.SetParam("@TORIHIKISAKI_CD", SqlDbType.Decimal, 6, Util.ToDecimal(this.txtNakagaiCd.Text));
                    dpc.SetParam("@TORIHIKISAKI_KUBUN2", SqlDbType.Decimal, 6, 2);
                    DataTable dtDispData =
                        this.Dba.GetDataTableByConditionWithParams(
                            Util.ToString(cols), Util.ToString(from),
                            "KAISHA_CD = @KAISHA_CD AND TORIHIKISAKI_CD = @TORIHIKISAKI_CD AND TORIHIKISAKI_KUBUN2 = @TORIHIKISAKI_KUBUN2",
                            dpc);

                    if (dtDispData.Rows.Count == 0)
                    {
                        Msg.Error("データがありません。");
                        // 仲買人CDにFocusを当てる
                        this.txtNakagaiCd.Focus();
                        this.txtNakagaiCd.SelectAll();
                        return false;
                    }
                    else
                    {
                        // 取得した内容を表示
                        DataRow drDispData = dtDispData.Rows[0];
                        this.txtNakagaiCd.Text = Util.ToString(drDispData["TORIHIKISAKI_CD"]);
                        this.txtNakagaiNm.Text = Util.ToString(drDispData["TORIHIKISAKI_NM"]);
                        if (this.txtKenNakaNm.Text == "")
                            this.txtKenNakaNm.Text = Util.ToString(drDispData["TORIHIKISAKI_NM"]);

                        // 名護仲買人CD、名を有効にする
                        this.txtKenNakaCd.Enabled = true;
                        this.txtKenNakaNm.Enabled = true;
                        // 名護仲買人CDにFocusを当てる
                        this.txtKenNakaCd.Focus();

                        // 仲買人CDを非活性にする
                        this.txtNakagaiCd.Enabled = false;
                        this.txtNakagaiNm.Enabled = false;

                        // 削除ボタンを表示する
                        this.btnF3.Enabled = true;
                    }
                }
            }
            // 編集の場合
            else if (MODE_EDIT.Equals(this.Par1))
            {
                
                // 空でない場合、入力された仲買人CDから検索する
                if (!ValChk.IsEmpty(this.txtNakagaiCd.Text))
                {
                    // 現在DBに登録されている値をtextboxに表示
                    StringBuilder cols = new StringBuilder();
                    cols.Append("KAISHA_CD");
                    cols.Append("    ,TORIHIKISAKI_CD");
                    cols.Append("    ,TORIHIKISAKI_NM");
                    cols.Append("    ,TORIHIKISAKI_KUBUN2");

                    StringBuilder from = new StringBuilder();
                    from.Append("VI_HN_NAKAGAININ_JOHO");

                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
                    dpc.SetParam("@TORIHIKISAKI_CD", SqlDbType.Decimal, 6, Util.ToDecimal(this.txtNakagaiCd.Text));
                    dpc.SetParam("@TORIHIKISAKI_KUBUN2", SqlDbType.Decimal, 6, 2);
                    DataTable dtDispData =
                        this.Dba.GetDataTableByConditionWithParams(
                            Util.ToString(cols), Util.ToString(from),
                            "KAISHA_CD = @KAISHA_CD AND TORIHIKISAKI_CD = @TORIHIKISAKI_CD ",
                            dpc);

                    if (dtDispData.Rows.Count == 0)
                    {
                        Msg.Error("データがありません。");
                        // 仲買人名を無効にする
                        this.txtNakagaiNm.Enabled = false;
                        // 名護仲買人CD、名を有効にする
                        this.txtKenNakaCd.Enabled = false;
                        this.txtKenNakaNm.Enabled = false;
                        // 全部一旦消してからデータ入れるため
                        this.txtNakagaiNm.Text = "";
                        this.txtKenNakaCd.Text = "";
                        this.txtKenNakaNm.Text = "";
                        // 仲買人CDにFocusを当てる
                        this.txtNakagaiCd.Focus();
                        this.txtNakagaiCd.SelectAll();
                        return false;
                    }
                    else
                    {
                        // 取得した内容を表示
                        DataRow drDispData = dtDispData.Rows[0];
                        this.txtNakagaiCd.Text = Util.ToString(drDispData["TORIHIKISAKI_CD"]);
                        if (this.txtNakagaiNm.Text == "")
                            this.txtNakagaiNm.Text = Util.ToString(drDispData["TORIHIKISAKI_NM"]);
                        if (this.txtKenNakaNm.Text == "")
                            this.txtKenNakaNm.Text = Util.ToString(drDispData["TORIHIKISAKI_NM"]);

                        // 名護仲買人CD、名を有効にする
                        this.txtKenNakaCd.Enabled = true;
                        this.txtKenNakaNm.Enabled = true;
                        //// 仲買人名を無効にする
                        //this.txtNakagaiNm.Enabled = false;
                        //// 名護仲買人CDにFocusを当てる
                        //this.txtKenNakaCd.Focus();
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 名護仲買人CDのチェック
            if (!IsValidKenGyoCd())
            {
                this.txtKenNakaCd.Focus();
                return false;
            }

            // 名護仲買人名のチェック
            if (!IsValidKenGyoNm())
            {
                this.txtKenNakaNm.Focus();
                return false;
            }

            // 仲買人名のチェック
            if (!IsValidNakagaiNm())
            {
                this.txtNakagaiNm.Focus();
                return false;
            }

            // 仲買人CDのチェック
            if (!IsValidNakagai())
            {
                this.txtNakagaiCd.Focus();
                return false;
            }

            return true;
        }

        /// <summary>
        /// TB_HN_NIUKENIN_CD_HENKAN_MSTに更新するためのパラメータ設定をします。
        /// </summary>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// 更新処理：DbParamCollection*2(Where句,Set句)
        /// </returns>
        private ArrayList SetCmNakagaiParams()
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();
            // 登録
            if (MODE_NEW.Equals(this.Par1))
            {
                // 会社CD,仲買人CDをパラメータに設定
                updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                updParam.SetParam("@NIUKENIN_CD", SqlDbType.Decimal, 6, this.txtNakagaiCd.Text);
                // 登録日
                updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWDATE");
                // 処理フラグ
                updParam.SetParam("@SHORI_FLG", SqlDbType.Decimal, 1, 0);
            }
            // 更新
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // 会社CDをWhere句のパラメータに設定
                DbParamCollection whereParam = new DbParamCollection();
                whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                whereParam.SetParam("@NIUKENIN_CD_WHERE", SqlDbType.Decimal, 6, Util.ToDecimal(this.txtNakagaiCd.Text));
                alParams.Add(whereParam);
                // 処理フラグ
                updParam.SetParam("@SHORI_FLG", SqlDbType.Decimal, 1, 1);
            }

            // 名護仲買人CD
            updParam.SetParam("@KEN_GYOREN_NIUKENIN_CD", SqlDbType.Decimal, 6, Util.ToDecimal(this.txtKenNakaCd.Text));
            // 名護仲買人名
            updParam.SetParam("@KEN_GYOREN_NIUKENIN_NM", SqlDbType.VarChar, 40, this.txtKenNakaNm.Text);
            // 仲買人名
            updParam.SetParam("@NIUKENIN_NM", SqlDbType.VarChar, 40, this.txtNakagaiNm.Text);
            // 更新日
            updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWDATE");

            alParams.Add(updParam);

            return alParams;
        }

        /// <summary>
        /// 仲買人コードの存在チェック(宜野座)
        /// </summary>
        /// <param name="nakagaiCd">仲買人コード</param>
        /// <returns>取得したデータ</returns>
        private DataTable GetNakagaiCd(decimal nakagaiCd)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT ");
            sql.Append("  COUNT(*) AS KENSU ");
            sql.Append("FROM ");
            sql.Append("  TB_HN_NIUKENIN_CD_HENKAN_MST ");
            sql.Append("WHERE ");
            sql.Append("    KAISHA_CD    = @KAISHA_CD ");
            sql.Append("AND NIUKENIN_CD = @NIUKENIN_CD ");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@NIUKENIN_CD", SqlDbType.Decimal, 6, nakagaiCd);

            DataTable dtResult = this.Dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            return dtResult;
        }
        #endregion

    }
}
