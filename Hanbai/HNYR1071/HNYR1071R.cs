﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using jp.co.fsi.common.report;
using System.Data;
using System.Globalization;
using jp.co.fsi.common.util;

namespace jp.co.fsi.hn.hnyr1071
{
    /// <summary>
    /// HNYR1071R の概要の説明です。
    /// </summary>
    public partial class HNYR1071R : BaseReport
    {

        public HNYR1071R(DataTable tgtData): base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }

        private void detail_Format(object sender, EventArgs e)
        {
            this.テキスト152.Value = Util.ToDecimal(this.テキスト036.Value) + Util.ToDecimal(this.テキスト037.Value) + Util.ToDecimal(this.テキスト038.Value) + Util.ToDecimal(this.テキスト039.Value) + Util.ToDecimal(this.テキスト040.Value) + Util.ToDecimal(this.テキスト041.Value)
                                    + Util.ToDecimal(this.テキスト042.Value) + Util.ToDecimal(this.テキスト043.Value) + Util.ToDecimal(this.テキスト044.Value) + Util.ToDecimal(this.テキスト045.Value) + Util.ToDecimal(this.テキスト046.Value) + Util.ToDecimal(this.テキスト047.Value);
        }

        private void groupFooter1_Format(object sender, EventArgs e)
        {
            this.textBox15.Value = Util.ToDecimal(this.textBox3.Value) + Util.ToDecimal(this.textBox4.Value) + Util.ToDecimal(this.textBox5.Value) + Util.ToDecimal(this.textBox6.Value) + Util.ToDecimal(this.textBox7.Value) + Util.ToDecimal(this.textBox8.Value)
                                   + Util.ToDecimal(this.textBox9.Value) + Util.ToDecimal(this.textBox10.Value) + Util.ToDecimal(this.textBox11.Value) + Util.ToDecimal(this.textBox12.Value) + Util.ToDecimal(this.textBox13.Value) + Util.ToDecimal(this.textBox14.Value);
        }

        private void reportFooter1_Format(object sender, EventArgs e)
        {
            this.テキスト166.Value = Util.ToDecimal(this.テキスト154.Value) + Util.ToDecimal(this.テキスト155.Value) + Util.ToDecimal(this.テキスト156.Value) + Util.ToDecimal(this.テキスト157.Value) + Util.ToDecimal(this.テキスト158.Value) + Util.ToDecimal(this.テキスト159.Value)
                                   + Util.ToDecimal(this.テキスト160.Value) + Util.ToDecimal(this.テキスト161.Value) + Util.ToDecimal(this.テキスト162.Value) + Util.ToDecimal(this.テキスト163.Value) + Util.ToDecimal(this.テキスト164.Value) + Util.ToDecimal(this.テキスト165.Value);
        }

        private void pageHeader_Format(object sender, EventArgs e)
        {
            txtToday.Text = DateTime.Now.ToString("yyyy/MM/dd");
        }

        private void HNYR1071R_ReportStart(object sender, EventArgs e)
        {

        }
    }
}
