﻿using System;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

using GrapeCity.ActiveReports;

using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.han.hanrt001
{
    /// <summary>
    /// ActiveReports帳票テスト(HANRT001)
    /// </summary>
    public partial class HANRT001 : BasePgForm
    {
        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HANRT001()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region メソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 特にないかな
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 特に無いかな
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // TODO:画面項目の入力値チェックしてね

            // プレビュー処理
            DoPrint(true);
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            // TODO:画面項目の入力値チェックしてね

            if (Msg.ConfYesNo("実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }

        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            // 設定画面の起動
            // MEMO:原則としてここで渡す帳票IDの設定はReport.csvに保持していることが前提ですが、
            // 保持していない場合は、設定画面での保存(F6)時に新規に設定が保持されます。
            PrintSettingForm psForm = new PrintSettingForm(new string[2] { "HANRT001R", "HANRT002R" });
            psForm.ShowDialog();
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview)
        {
            try
            {
                bool dataFlag;

                this.Dba.BeginTransaction();

                // 帳票出力用にワークテーブルにデータを作成
                dataFlag = MakeWkData();

                // 帳票出力
                if (dataFlag)
                {
                    // 取得列の定義
                    StringBuilder cols = new StringBuilder();
                    cols.Append("  ITEM01");
                    cols.Append(" ,ITEM02");
                    cols.Append(" ,ITEM03");
                    cols.Append(" ,ITEM04");
                    cols.Append(" ,ITEM05");
                    cols.Append(" ,ITEM06");
                    cols.Append(" ,ITEM07");
                    cols.Append(" ,ITEM08");
                    cols.Append(" ,ITEM09");
                    cols.Append(" ,ITEM10");
                    cols.Append(" ,CONVERT(numeric(12, 3), ITEM11) AS ITEM11");
                    cols.Append(" ,CONVERT(numeric(12, 3), ITEM12) AS ITEM12");

                    // バインドパラメータの設定
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);

                    // データの取得
                    DataTable dtOutput = this.Dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_HN_TBL", "GUID = @GUID", "SORT ASC", dpc);

                    // 帳票オブジェクトをインスタンス化
                    HANRT001R rpt = new HANRT001R(dtOutput);
                    
                    if (isPreview)
                    {
                        // プレビュー画面表示
                        PreviewForm pFrm = new PreviewForm(rpt, this.UnqId);
                        pFrm.Show();
                    }
                    else
                    {
                        // 直接印刷
                        rpt.Run(false);
                        rpt.Document.Print(true, true, false);
                    }
                }
            }
            finally
            {
                this.Dba.Rollback();
            }
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData()
        {
            // ワークテーブルに更新をする
            DbParamCollection dpc;
            StringBuilder sql = new StringBuilder();

            sql.Append("INSERT INTO PR_HN_TBL(");
            sql.Append("  GUID");
            sql.Append(" ,SORT");
            sql.Append(" ,ITEM01");
            sql.Append(" ,ITEM02");
            sql.Append(" ,ITEM03");
            sql.Append(" ,ITEM04");
            sql.Append(" ,ITEM05");
            sql.Append(" ,ITEM06");
            sql.Append(" ,ITEM07");
            sql.Append(" ,ITEM08");
            sql.Append(" ,ITEM09");
            sql.Append(" ,ITEM10");
            sql.Append(" ,ITEM11");
            sql.Append(" ,ITEM12");
            sql.Append(") ");
            sql.Append("VALUES(");
            sql.Append("  @GUID");
            sql.Append(" ,@SORT");
            sql.Append(" ,@ITEM01");
            sql.Append(" ,@ITEM02");
            sql.Append(" ,@ITEM03");
            sql.Append(" ,@ITEM04");
            sql.Append(" ,@ITEM05");
            sql.Append(" ,@ITEM06");
            sql.Append(" ,@ITEM07");
            sql.Append(" ,@ITEM08");
            sql.Append(" ,@ITEM09");
            sql.Append(" ,@ITEM10");
            sql.Append(" ,@ITEM11");
            sql.Append(" ,@ITEM12");
            sql.Append(") ");

            int idx = 0;

            for (int i = 1; i <= 3; i++)
            {
                for (int j = 1; j <= 3; j++)
                {
                    for (int k = 1; k <= 3; k++)
                    {
                        for (int l = 1; l <= 3; l++)
                        {
                            dpc = new DbParamCollection();

                            idx++;
                            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                            dpc.SetParam("@SORT", SqlDbType.VarChar, 4, idx);
                            dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, "帳票テスト会社"); // 会社名
                            dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, Util.ConvJpDate(DateTime.Now, this.Dba)[5]); // 出力日付
                            dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, Util.ToString(i)); // 大コード
                            dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, "名称" + Util.ToString(i)); // 大名称
                            dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, Util.ToString(j)); // 中コード
                            dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, "名称" + Util.ToString(j)); // 中名称
                            dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, Util.ToString(k)); // 小コード
                            dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, "名称" + Util.ToString(k)); // 小名称
                            dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, Util.ToString(l)); // 細コード
                            dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, "名称" + Util.ToString(l)); // 細名称
                            dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, idx); // 数量
                            dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, 98 + (10 * l)); // 単価

                            this.Dba.ModifyBySql(Util.ToString(sql), dpc);
                        }
                    }
                }
            }

            return true;
        }
        #endregion
    }
}
