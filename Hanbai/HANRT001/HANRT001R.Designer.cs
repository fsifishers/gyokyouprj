﻿namespace jp.co.fsi.han.hanrt001
{
    /// <summary>
    /// SectionReport1 の概要の説明です。
    /// </summary>
    partial class HANRT001R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(HANRT001R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.lblTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtCompanyName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPrintDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblCptLCode = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCptMCode = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCptSCode = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCptCode = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCptSuryo = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCptTanka = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCptKingaku = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lnHeader1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lnHeader2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtPageCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblPageDiv = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtPageTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtDCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSuryo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTanka = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtKingaku = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.ghLCode = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.txtLCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtLName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lnLHdr = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.gfLCode = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.lblLTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtLTotalKin = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lnLFtr = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.ghMCode = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.txtMCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lnMHdr = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.gfMCode = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.txtMTotalKin = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblMTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lnMFtr = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.ghSCode = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.txtSCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lnSHdr = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.gfSCode = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.lblSTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtSTotalKin = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lnSFtr = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.reportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.reportFooter1 = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.lblGTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtGTotalKin = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lnRFtr = new GrapeCity.ActiveReports.SectionReportModel.Line();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrintDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCptLCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCptMCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCptSCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCptCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCptSuryo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCptTanka)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCptKingaku)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPageDiv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSuryo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTanka)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKingaku)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLTotalKin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMTotalKin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSTotalKin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGTotalKin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.CanGrow = false;
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblTitle,
            this.txtCompanyName,
            this.txtPrintDate,
            this.lblCptLCode,
            this.lblCptMCode,
            this.lblCptSCode,
            this.lblCptCode,
            this.lblCptSuryo,
            this.lblCptTanka,
            this.lblCptKingaku,
            this.lnHeader1,
            this.lnHeader2,
            this.txtPageCount,
            this.lblPageDiv,
            this.txtPageTotal,
            this.label1});
            this.pageHeader.Height = 0.7944883F;
            this.pageHeader.Name = "pageHeader";
            // 
            // lblTitle
            // 
            this.lblTitle.Height = 0.1968504F;
            this.lblTitle.HyperLink = null;
            this.lblTitle.Left = 3.937008F;
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Style = "font-family: ＭＳ ゴシック; font-size: 14.25pt; font-weight: bold; text-align: center";
            this.lblTitle.Text = "サンプル帳票";
            this.lblTitle.Top = 0.1968504F;
            this.lblTitle.Width = 1.574803F;
            // 
            // txtCompanyName
            // 
            this.txtCompanyName.DataField = "ITEM01";
            this.txtCompanyName.Height = 0.1968504F;
            this.txtCompanyName.Left = 0F;
            this.txtCompanyName.MultiLine = false;
            this.txtCompanyName.Name = "txtCompanyName";
            this.txtCompanyName.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; vertical-align: middle";
            this.txtCompanyName.Text = "ＮＮＮＮＮＮＮＮＮＮＮＮＮＮＮ";
            this.txtCompanyName.Top = 0F;
            this.txtCompanyName.Width = 1.968504F;
            // 
            // txtPrintDate
            // 
            this.txtPrintDate.DataField = "ITEM02";
            this.txtPrintDate.Height = 0.1968504F;
            this.txtPrintDate.Left = 8.267716F;
            this.txtPrintDate.MultiLine = false;
            this.txtPrintDate.Name = "txtPrintDate";
            this.txtPrintDate.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; text-align: right; vertical-align: middl" +
    "e";
            this.txtPrintDate.Text = "ＧＧYY年MM月DD日";
            this.txtPrintDate.Top = 0F;
            this.txtPrintDate.Width = 1.181102F;
            // 
            // lblCptLCode
            // 
            this.lblCptLCode.Height = 0.1968504F;
            this.lblCptLCode.HyperLink = null;
            this.lblCptLCode.Left = 0F;
            this.lblCptLCode.Name = "lblCptLCode";
            this.lblCptLCode.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; vertical-align: middle";
            this.lblCptLCode.Text = "大分類";
            this.lblCptLCode.Top = 0.5905512F;
            this.lblCptLCode.Width = 0.7874016F;
            // 
            // lblCptMCode
            // 
            this.lblCptMCode.Height = 0.1968504F;
            this.lblCptMCode.HyperLink = null;
            this.lblCptMCode.Left = 1.181102F;
            this.lblCptMCode.Name = "lblCptMCode";
            this.lblCptMCode.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; vertical-align: middle";
            this.lblCptMCode.Text = "中分類";
            this.lblCptMCode.Top = 0.5905512F;
            this.lblCptMCode.Width = 0.7874014F;
            // 
            // lblCptSCode
            // 
            this.lblCptSCode.Height = 0.1968504F;
            this.lblCptSCode.HyperLink = null;
            this.lblCptSCode.Left = 2.362205F;
            this.lblCptSCode.Name = "lblCptSCode";
            this.lblCptSCode.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; vertical-align: middle";
            this.lblCptSCode.Text = "小分類";
            this.lblCptSCode.Top = 0.5905512F;
            this.lblCptSCode.Width = 0.7874014F;
            // 
            // lblCptCode
            // 
            this.lblCptCode.Height = 0.1968504F;
            this.lblCptCode.HyperLink = null;
            this.lblCptCode.Left = 3.543307F;
            this.lblCptCode.Name = "lblCptCode";
            this.lblCptCode.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; vertical-align: middle";
            this.lblCptCode.Text = "コード";
            this.lblCptCode.Top = 0.5905512F;
            this.lblCptCode.Width = 0.7874014F;
            // 
            // lblCptSuryo
            // 
            this.lblCptSuryo.Height = 0.1968504F;
            this.lblCptSuryo.HyperLink = null;
            this.lblCptSuryo.Left = 6.299212F;
            this.lblCptSuryo.Name = "lblCptSuryo";
            this.lblCptSuryo.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; text-align: right; vertical-align: middl" +
    "e";
            this.lblCptSuryo.Text = "数量";
            this.lblCptSuryo.Top = 0.5905512F;
            this.lblCptSuryo.Width = 0.7874014F;
            // 
            // lblCptTanka
            // 
            this.lblCptTanka.Height = 0.1968504F;
            this.lblCptTanka.HyperLink = null;
            this.lblCptTanka.Left = 7.480315F;
            this.lblCptTanka.Name = "lblCptTanka";
            this.lblCptTanka.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; text-align: right; vertical-align: middl" +
    "e";
            this.lblCptTanka.Text = "単価";
            this.lblCptTanka.Top = 0.5905512F;
            this.lblCptTanka.Width = 0.7874014F;
            // 
            // lblCptKingaku
            // 
            this.lblCptKingaku.Height = 0.1968504F;
            this.lblCptKingaku.HyperLink = null;
            this.lblCptKingaku.Left = 8.661417F;
            this.lblCptKingaku.Name = "lblCptKingaku";
            this.lblCptKingaku.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; text-align: right; vertical-align: middl" +
    "e";
            this.lblCptKingaku.Text = "金額";
            this.lblCptKingaku.Top = 0.5905512F;
            this.lblCptKingaku.Width = 0.7874014F;
            // 
            // lnHeader1
            // 
            this.lnHeader1.Height = 0F;
            this.lnHeader1.Left = 0F;
            this.lnHeader1.LineWeight = 1F;
            this.lnHeader1.Name = "lnHeader1";
            this.lnHeader1.Top = 0.5905512F;
            this.lnHeader1.Width = 9.448819F;
            this.lnHeader1.X1 = 0F;
            this.lnHeader1.X2 = 9.448819F;
            this.lnHeader1.Y1 = 0.5905512F;
            this.lnHeader1.Y2 = 0.5905512F;
            // 
            // lnHeader2
            // 
            this.lnHeader2.Height = 0F;
            this.lnHeader2.Left = 0F;
            this.lnHeader2.LineWeight = 1F;
            this.lnHeader2.Name = "lnHeader2";
            this.lnHeader2.Top = 0.7874016F;
            this.lnHeader2.Width = 9.448819F;
            this.lnHeader2.X1 = 0F;
            this.lnHeader2.X2 = 9.448819F;
            this.lnHeader2.Y1 = 0.7874016F;
            this.lnHeader2.Y2 = 0.7874016F;
            // 
            // txtPageCount
            // 
            this.txtPageCount.Height = 0.1968504F;
            this.txtPageCount.Left = 8.267716F;
            this.txtPageCount.MultiLine = false;
            this.txtPageCount.Name = "txtPageCount";
            this.txtPageCount.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; text-align: right; vertical-align: middl" +
    "e";
            this.txtPageCount.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtPageCount.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.txtPageCount.Text = "999";
            this.txtPageCount.Top = 0.1968504F;
            this.txtPageCount.Width = 0.2952756F;
            // 
            // lblPageDiv
            // 
            this.lblPageDiv.Height = 0.1968504F;
            this.lblPageDiv.HyperLink = null;
            this.lblPageDiv.Left = 8.562992F;
            this.lblPageDiv.Name = "lblPageDiv";
            this.lblPageDiv.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; text-align: left; vertical-align: middle" +
    "";
            this.lblPageDiv.Text = "/";
            this.lblPageDiv.Top = 0.1968504F;
            this.lblPageDiv.Width = 0.09842519F;
            // 
            // txtPageTotal
            // 
            this.txtPageTotal.DataField = "ITEM02";
            this.txtPageTotal.Height = 0.1968504F;
            this.txtPageTotal.Left = 8.661418F;
            this.txtPageTotal.MultiLine = false;
            this.txtPageTotal.Name = "txtPageTotal";
            this.txtPageTotal.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; text-align: right; vertical-align: middl" +
    "e";
            this.txtPageTotal.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.txtPageTotal.Text = "999";
            this.txtPageTotal.Top = 0.1968504F;
            this.txtPageTotal.Width = 0.2952757F;
            // 
            // label1
            // 
            this.label1.Height = 0.1968504F;
            this.label1.HyperLink = null;
            this.label1.Left = 8.956694F;
            this.label1.Name = "label1";
            this.label1.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; text-align: right; vertical-align: middl" +
    "e";
            this.label1.Text = "ページ";
            this.label1.Top = 0.1968504F;
            this.label1.Width = 0.492126F;
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtDCode,
            this.txtDName,
            this.txtSuryo,
            this.txtTanka,
            this.txtKingaku});
            this.detail.Height = 0.1968504F;
            this.detail.KeepTogether = true;
            this.detail.Name = "detail";
            // 
            // txtDCode
            // 
            this.txtDCode.CanGrow = false;
            this.txtDCode.DataField = "ITEM09";
            this.txtDCode.Height = 0.1968504F;
            this.txtDCode.Left = 3.543307F;
            this.txtDCode.MultiLine = false;
            this.txtDCode.Name = "txtDCode";
            this.txtDCode.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; vertical-align: middle";
            this.txtDCode.Text = "9";
            this.txtDCode.Top = 0F;
            this.txtDCode.Width = 0.3937007F;
            // 
            // txtDName
            // 
            this.txtDName.CanGrow = false;
            this.txtDName.DataField = "ITEM10";
            this.txtDName.Height = 0.1968504F;
            this.txtDName.Left = 3.937008F;
            this.txtDName.MultiLine = false;
            this.txtDName.Name = "txtDName";
            this.txtDName.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; vertical-align: middle";
            this.txtDName.Text = "ＮＮＮＮＮ";
            this.txtDName.Top = 0F;
            this.txtDName.Width = 0.7874014F;
            // 
            // txtSuryo
            // 
            this.txtSuryo.CanGrow = false;
            this.txtSuryo.DataField = "ITEM11";
            this.txtSuryo.Height = 0.1968504F;
            this.txtSuryo.Left = 6.299212F;
            this.txtSuryo.MultiLine = false;
            this.txtSuryo.Name = "txtSuryo";
            this.txtSuryo.OutputFormat = resources.GetString("txtSuryo.OutputFormat");
            this.txtSuryo.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; text-align: right; vertical-align: middl" +
    "e";
            this.txtSuryo.Text = "999,999";
            this.txtSuryo.Top = 0F;
            this.txtSuryo.Width = 0.7874014F;
            // 
            // txtTanka
            // 
            this.txtTanka.CanGrow = false;
            this.txtTanka.DataField = "ITEM12";
            this.txtTanka.Height = 0.1968504F;
            this.txtTanka.Left = 7.480315F;
            this.txtTanka.MultiLine = false;
            this.txtTanka.Name = "txtTanka";
            this.txtTanka.OutputFormat = resources.GetString("txtTanka.OutputFormat");
            this.txtTanka.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; text-align: right; vertical-align: middl" +
    "e";
            this.txtTanka.Text = "999,999";
            this.txtTanka.Top = 0F;
            this.txtTanka.Width = 0.7874014F;
            // 
            // txtKingaku
            // 
            this.txtKingaku.CanGrow = false;
            this.txtKingaku.DataField = "Amount";
            this.txtKingaku.Height = 0.1968504F;
            this.txtKingaku.Left = 8.661417F;
            this.txtKingaku.MultiLine = false;
            this.txtKingaku.Name = "txtKingaku";
            this.txtKingaku.OutputFormat = resources.GetString("txtKingaku.OutputFormat");
            this.txtKingaku.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; text-align: right; vertical-align: middl" +
    "e; ddo-shrink-to-fit: none";
            this.txtKingaku.Text = "999,999";
            this.txtKingaku.Top = 0F;
            this.txtKingaku.Width = 0.7874016F;
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            // 
            // ghLCode
            // 
            this.ghLCode.CanGrow = false;
            this.ghLCode.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtLCode,
            this.txtLName,
            this.lnLHdr});
            this.ghLCode.DataField = "ITEM03";
            this.ghLCode.Height = 0.1968504F;
            this.ghLCode.KeepTogether = true;
            this.ghLCode.Name = "ghLCode";
            this.ghLCode.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
            this.ghLCode.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPageIncludeNoDetail;
            // 
            // txtLCode
            // 
            this.txtLCode.CanGrow = false;
            this.txtLCode.DataField = "ITEM03";
            this.txtLCode.Height = 0.1968504F;
            this.txtLCode.Left = 0F;
            this.txtLCode.MultiLine = false;
            this.txtLCode.Name = "txtLCode";
            this.txtLCode.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; vertical-align: middle";
            this.txtLCode.Text = "9";
            this.txtLCode.Top = 0F;
            this.txtLCode.Width = 0.3937008F;
            // 
            // txtLName
            // 
            this.txtLName.CanGrow = false;
            this.txtLName.DataField = "ITEM04";
            this.txtLName.Height = 0.1968504F;
            this.txtLName.Left = 0.3937008F;
            this.txtLName.MultiLine = false;
            this.txtLName.Name = "txtLName";
            this.txtLName.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; vertical-align: middle";
            this.txtLName.Text = "ＮＮＮＮＮ";
            this.txtLName.Top = 0F;
            this.txtLName.Width = 0.7874016F;
            // 
            // lnLHdr
            // 
            this.lnLHdr.Height = 0F;
            this.lnLHdr.Left = 0F;
            this.lnLHdr.LineWeight = 1F;
            this.lnLHdr.Name = "lnLHdr";
            this.lnLHdr.Top = 0.1968504F;
            this.lnLHdr.Width = 9.448819F;
            this.lnLHdr.X1 = 0F;
            this.lnLHdr.X2 = 9.448819F;
            this.lnLHdr.Y1 = 0.1968504F;
            this.lnLHdr.Y2 = 0.1968504F;
            // 
            // gfLCode
            // 
            this.gfLCode.CanGrow = false;
            this.gfLCode.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblLTotal,
            this.txtLTotalKin,
            this.lnLFtr});
            this.gfLCode.Height = 0.1968504F;
            this.gfLCode.KeepTogether = true;
            this.gfLCode.Name = "gfLCode";
            // 
            // lblLTotal
            // 
            this.lblLTotal.Height = 0.1968504F;
            this.lblLTotal.HyperLink = null;
            this.lblLTotal.Left = 0F;
            this.lblLTotal.Name = "lblLTotal";
            this.lblLTotal.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; vertical-align: middle";
            this.lblLTotal.Text = "【大計】";
            this.lblLTotal.Top = 0F;
            this.lblLTotal.Width = 0.7874014F;
            // 
            // txtLTotalKin
            // 
            this.txtLTotalKin.CanGrow = false;
            this.txtLTotalKin.DataField = "Amount";
            this.txtLTotalKin.Height = 0.1968504F;
            this.txtLTotalKin.Left = 8.661417F;
            this.txtLTotalKin.MultiLine = false;
            this.txtLTotalKin.Name = "txtLTotalKin";
            this.txtLTotalKin.OutputFormat = resources.GetString("txtLTotalKin.OutputFormat");
            this.txtLTotalKin.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; text-align: right; vertical-align: middl" +
    "e";
            this.txtLTotalKin.SummaryGroup = "ghLCode";
            this.txtLTotalKin.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtLTotalKin.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtLTotalKin.Text = "999,999";
            this.txtLTotalKin.Top = 0F;
            this.txtLTotalKin.Width = 0.7874014F;
            // 
            // lnLFtr
            // 
            this.lnLFtr.Height = 0F;
            this.lnLFtr.Left = 0F;
            this.lnLFtr.LineWeight = 1F;
            this.lnLFtr.Name = "lnLFtr";
            this.lnLFtr.Top = 0F;
            this.lnLFtr.Width = 9.448819F;
            this.lnLFtr.X1 = 0F;
            this.lnLFtr.X2 = 9.448819F;
            this.lnLFtr.Y1 = 0F;
            this.lnLFtr.Y2 = 0F;
            // 
            // ghMCode
            // 
            this.ghMCode.CanGrow = false;
            this.ghMCode.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtMCode,
            this.txtMName,
            this.lnMHdr});
            this.ghMCode.DataField = "ITEM05";
            this.ghMCode.Height = 0.1968504F;
            this.ghMCode.KeepTogether = true;
            this.ghMCode.Name = "ghMCode";
            this.ghMCode.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPageIncludeNoDetail;
            // 
            // txtMCode
            // 
            this.txtMCode.CanGrow = false;
            this.txtMCode.DataField = "ITEM05";
            this.txtMCode.Height = 0.1968504F;
            this.txtMCode.Left = 1.181102F;
            this.txtMCode.MultiLine = false;
            this.txtMCode.Name = "txtMCode";
            this.txtMCode.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; vertical-align: middle";
            this.txtMCode.Text = "9";
            this.txtMCode.Top = 0F;
            this.txtMCode.Width = 0.3937007F;
            // 
            // txtMName
            // 
            this.txtMName.CanGrow = false;
            this.txtMName.DataField = "ITEM06";
            this.txtMName.Height = 0.1968504F;
            this.txtMName.Left = 1.574803F;
            this.txtMName.MultiLine = false;
            this.txtMName.Name = "txtMName";
            this.txtMName.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; vertical-align: middle";
            this.txtMName.Text = "ＮＮＮＮＮ";
            this.txtMName.Top = 0F;
            this.txtMName.Width = 0.7874014F;
            // 
            // lnMHdr
            // 
            this.lnMHdr.Height = 0F;
            this.lnMHdr.Left = 1.181102F;
            this.lnMHdr.LineWeight = 1F;
            this.lnMHdr.Name = "lnMHdr";
            this.lnMHdr.Top = 0.1968504F;
            this.lnMHdr.Width = 8.267717F;
            this.lnMHdr.X1 = 1.181102F;
            this.lnMHdr.X2 = 9.448819F;
            this.lnMHdr.Y1 = 0.1968504F;
            this.lnMHdr.Y2 = 0.1968504F;
            // 
            // gfMCode
            // 
            this.gfMCode.CanGrow = false;
            this.gfMCode.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtMTotalKin,
            this.lblMTotal,
            this.lnMFtr});
            this.gfMCode.Height = 0.1968504F;
            this.gfMCode.KeepTogether = true;
            this.gfMCode.Name = "gfMCode";
            // 
            // txtMTotalKin
            // 
            this.txtMTotalKin.CanGrow = false;
            this.txtMTotalKin.DataField = "Amount";
            this.txtMTotalKin.Height = 0.1968504F;
            this.txtMTotalKin.Left = 8.661417F;
            this.txtMTotalKin.MultiLine = false;
            this.txtMTotalKin.Name = "txtMTotalKin";
            this.txtMTotalKin.OutputFormat = resources.GetString("txtMTotalKin.OutputFormat");
            this.txtMTotalKin.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; text-align: right; vertical-align: middl" +
    "e";
            this.txtMTotalKin.SummaryGroup = "ghMCode";
            this.txtMTotalKin.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtMTotalKin.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtMTotalKin.Text = "999,999";
            this.txtMTotalKin.Top = 0F;
            this.txtMTotalKin.Width = 0.7874014F;
            // 
            // lblMTotal
            // 
            this.lblMTotal.Height = 0.1968504F;
            this.lblMTotal.HyperLink = null;
            this.lblMTotal.Left = 1.181102F;
            this.lblMTotal.Name = "lblMTotal";
            this.lblMTotal.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; vertical-align: middle";
            this.lblMTotal.Text = "【中計】";
            this.lblMTotal.Top = 0F;
            this.lblMTotal.Width = 0.7874014F;
            // 
            // lnMFtr
            // 
            this.lnMFtr.Height = 0F;
            this.lnMFtr.Left = 1.181102F;
            this.lnMFtr.LineWeight = 1F;
            this.lnMFtr.Name = "lnMFtr";
            this.lnMFtr.Top = 0F;
            this.lnMFtr.Width = 8.267717F;
            this.lnMFtr.X1 = 1.181102F;
            this.lnMFtr.X2 = 9.448819F;
            this.lnMFtr.Y1 = 0F;
            this.lnMFtr.Y2 = 0F;
            // 
            // ghSCode
            // 
            this.ghSCode.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtSCode,
            this.txtSName,
            this.lnSHdr});
            this.ghSCode.DataField = "ITEM07";
            this.ghSCode.Height = 0.1968504F;
            this.ghSCode.KeepTogether = true;
            this.ghSCode.Name = "ghSCode";
            this.ghSCode.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPageIncludeNoDetail;
            // 
            // txtSCode
            // 
            this.txtSCode.CanGrow = false;
            this.txtSCode.DataField = "ITEM07";
            this.txtSCode.Height = 0.1968504F;
            this.txtSCode.Left = 2.362205F;
            this.txtSCode.MultiLine = false;
            this.txtSCode.Name = "txtSCode";
            this.txtSCode.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; vertical-align: middle";
            this.txtSCode.Text = "9";
            this.txtSCode.Top = 0F;
            this.txtSCode.Width = 0.3937007F;
            // 
            // txtSName
            // 
            this.txtSName.CanGrow = false;
            this.txtSName.DataField = "ITEM08";
            this.txtSName.Height = 0.1968504F;
            this.txtSName.Left = 2.755906F;
            this.txtSName.MultiLine = false;
            this.txtSName.Name = "txtSName";
            this.txtSName.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; vertical-align: middle";
            this.txtSName.Text = "ＮＮＮＮＮ";
            this.txtSName.Top = 0F;
            this.txtSName.Width = 0.7874014F;
            // 
            // lnSHdr
            // 
            this.lnSHdr.Height = 0F;
            this.lnSHdr.Left = 2.362205F;
            this.lnSHdr.LineWeight = 1F;
            this.lnSHdr.Name = "lnSHdr";
            this.lnSHdr.Top = 0.1968504F;
            this.lnSHdr.Width = 7.086614F;
            this.lnSHdr.X1 = 2.362205F;
            this.lnSHdr.X2 = 9.448819F;
            this.lnSHdr.Y1 = 0.1968504F;
            this.lnSHdr.Y2 = 0.1968504F;
            // 
            // gfSCode
            // 
            this.gfSCode.CanGrow = false;
            this.gfSCode.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblSTotal,
            this.txtSTotalKin,
            this.lnSFtr});
            this.gfSCode.Height = 0.1968504F;
            this.gfSCode.KeepTogether = true;
            this.gfSCode.Name = "gfSCode";
            // 
            // lblSTotal
            // 
            this.lblSTotal.Height = 0.1968504F;
            this.lblSTotal.HyperLink = null;
            this.lblSTotal.Left = 2.362205F;
            this.lblSTotal.Name = "lblSTotal";
            this.lblSTotal.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; vertical-align: middle";
            this.lblSTotal.Text = "【小計】";
            this.lblSTotal.Top = 0F;
            this.lblSTotal.Width = 0.7874014F;
            // 
            // txtSTotalKin
            // 
            this.txtSTotalKin.CanGrow = false;
            this.txtSTotalKin.DataField = "Amount";
            this.txtSTotalKin.Height = 0.1968504F;
            this.txtSTotalKin.Left = 8.661417F;
            this.txtSTotalKin.MultiLine = false;
            this.txtSTotalKin.Name = "txtSTotalKin";
            this.txtSTotalKin.OutputFormat = resources.GetString("txtSTotalKin.OutputFormat");
            this.txtSTotalKin.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; text-align: right; vertical-align: middl" +
    "e";
            this.txtSTotalKin.SummaryGroup = "ghSCode";
            this.txtSTotalKin.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtSTotalKin.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtSTotalKin.Text = "999,999";
            this.txtSTotalKin.Top = 0F;
            this.txtSTotalKin.Width = 0.7874014F;
            // 
            // lnSFtr
            // 
            this.lnSFtr.Height = 0F;
            this.lnSFtr.Left = 2.362205F;
            this.lnSFtr.LineWeight = 1F;
            this.lnSFtr.Name = "lnSFtr";
            this.lnSFtr.Top = 0F;
            this.lnSFtr.Width = 7.086614F;
            this.lnSFtr.X1 = 2.362205F;
            this.lnSFtr.X2 = 9.448819F;
            this.lnSFtr.Y1 = 0F;
            this.lnSFtr.Y2 = 0F;
            // 
            // reportHeader
            // 
            this.reportHeader.Height = 0F;
            this.reportHeader.Name = "reportHeader";
            // 
            // reportFooter1
            // 
            this.reportFooter1.CanGrow = false;
            this.reportFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblGTotal,
            this.txtGTotalKin,
            this.lnRFtr});
            this.reportFooter1.Height = 0.1968504F;
            this.reportFooter1.KeepTogether = true;
            this.reportFooter1.Name = "reportFooter1";
            // 
            // lblGTotal
            // 
            this.lblGTotal.Height = 0.1968504F;
            this.lblGTotal.HyperLink = null;
            this.lblGTotal.Left = 0F;
            this.lblGTotal.Name = "lblGTotal";
            this.lblGTotal.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; vertical-align: middle";
            this.lblGTotal.Text = "【総合計】";
            this.lblGTotal.Top = 0F;
            this.lblGTotal.Width = 0.7874014F;
            // 
            // txtGTotalKin
            // 
            this.txtGTotalKin.CanGrow = false;
            this.txtGTotalKin.DataField = "Amount";
            this.txtGTotalKin.Height = 0.1968504F;
            this.txtGTotalKin.Left = 8.661417F;
            this.txtGTotalKin.MultiLine = false;
            this.txtGTotalKin.Name = "txtGTotalKin";
            this.txtGTotalKin.OutputFormat = resources.GetString("txtGTotalKin.OutputFormat");
            this.txtGTotalKin.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; text-align: right; vertical-align: middl" +
    "e";
            this.txtGTotalKin.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtGTotalKin.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtGTotalKin.Text = "999,999";
            this.txtGTotalKin.Top = 0F;
            this.txtGTotalKin.Width = 0.7874014F;
            // 
            // lnRFtr
            // 
            this.lnRFtr.Height = 0F;
            this.lnRFtr.Left = 0F;
            this.lnRFtr.LineWeight = 1F;
            this.lnRFtr.Name = "lnRFtr";
            this.lnRFtr.Top = 0F;
            this.lnRFtr.Width = 9.448819F;
            this.lnRFtr.X1 = 0F;
            this.lnRFtr.X2 = 9.448819F;
            this.lnRFtr.Y1 = 0F;
            this.lnRFtr.Y2 = 0F;
            // 
            // HANRT001R
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
            this.PageSettings.PaperHeight = 11.69291F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.267716F;
            this.PrintWidth = 9.44882F;
            this.Sections.Add(this.reportHeader);
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.ghLCode);
            this.Sections.Add(this.ghMCode);
            this.Sections.Add(this.ghSCode);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.gfSCode);
            this.Sections.Add(this.gfMCode);
            this.Sections.Add(this.gfLCode);
            this.Sections.Add(this.pageFooter);
            this.Sections.Add(this.reportFooter1);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.HANRT001R_FetchData);
            this.DataInitialize += new System.EventHandler(this.HANRT001R_DataInitialize);
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrintDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCptLCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCptMCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCptSCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCptCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCptSuryo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCptTanka)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCptKingaku)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPageDiv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSuryo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTanka)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKingaku)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLTotalKin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMTotalKin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSTotalKin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGTotalKin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader ghLCode;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter gfLCode;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle;
        private GrapeCity.ActiveReports.SectionReportModel.Line lnHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.Line lnHeader2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCompanyName;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrintDate;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblCptLCode;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblCptMCode;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblCptSCode;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblCptCode;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblCptSuryo;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblCptTanka;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblCptKingaku;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader ghMCode;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter gfMCode;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader ghSCode;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter gfSCode;
        private GrapeCity.ActiveReports.SectionReportModel.ReportHeader reportHeader;
        private GrapeCity.ActiveReports.SectionReportModel.ReportFooter reportFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLCode;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLName;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMCode;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMName;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSCode;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSName;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDCode;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDName;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSuryo;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTanka;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKingaku;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSTotal;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSTotalKin;
        private GrapeCity.ActiveReports.SectionReportModel.Line lnLHdr;
        private GrapeCity.ActiveReports.SectionReportModel.Line lnMHdr;
        private GrapeCity.ActiveReports.SectionReportModel.Line lnSHdr;
        private GrapeCity.ActiveReports.SectionReportModel.Line lnSFtr;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMTotalKin;
        private GrapeCity.ActiveReports.SectionReportModel.Line lnMFtr;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblMTotal;
        private GrapeCity.ActiveReports.SectionReportModel.Line lnLFtr;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblLTotal;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLTotalKin;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblGTotal;
        private GrapeCity.ActiveReports.SectionReportModel.Line lnRFtr;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGTotalKin;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPageCount;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPageDiv;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPageTotal;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
    }
}
