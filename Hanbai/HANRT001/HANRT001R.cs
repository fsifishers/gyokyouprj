﻿using System;
using System.Data;

using jp.co.fsi.common.report;
using jp.co.fsi.common.util;

namespace jp.co.fsi.han.hanrt001
{
    /// <summary>
    /// サンプル帳票です。
    /// </summary>
    public partial class HANRT001R : BaseReport
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="tgtData">出力対象データ</param>
        public HANRT001R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }

        /// <summary>
        /// DataInitializeイベント
        /// ワークに持っていない項目を出力する際に、非連結項目を定義して下さい。
        /// </summary>
        private void HANRT001R_DataInitialize(object sender, EventArgs e)
        {
            this.Fields.Add("Amount");
        }

        // <summary>
        // FetchDataイベント
        // </summary>
        private void HANRT001R_FetchData(object sender, FetchEventArgs eArgs)
        {
            // 数量×単価で金額を計算します。
            this.Fields["Amount"].Value = Util.ToDecimal(this.Fields["ITEM11"].Value) * Util.ToDecimal(this.Fields["ITEM12"].Value);
        }
    }
}
