﻿using System.Data;

using jp.co.fsi.common.util;
using jp.co.fsi.common.report;

namespace jp.co.fsi.han.hanr2151
{
    /// <summary>
    /// HANR2151R の概要の説明です。
    /// </summary>
    public partial class HANR2151R : BaseReport
    {

        public HANR2151R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }

        decimal tanka;          // 単価
        decimal zeikomiKingaku; // 税込金額
        decimal gokeiSeikyu;    // 合計請求高

        private void detail_Format(object sender, System.EventArgs e)
        {
            // 金額の計算
            tanka = Util.ToDecimal(this.テキスト07.Text) / Util.ToDecimal(this.テキスト05.Text);
            zeikomiKingaku = Util.ToDecimal(this.テキスト07.Text) + Util.ToDecimal(this.テキスト08.Text);
            gokeiSeikyu = Util.ToDecimal(this.テキスト07.Text) + Util.ToDecimal(this.テキスト08.Text) + Util.ToDecimal(this.テキスト10.Text);

            // 金額のセット
            this.テキスト06.Text = Util.FormatNum(tanka);
            this.テキスト09.Text = Util.FormatNum(zeikomiKingaku);
            this.テキスト11.Text = Util.FormatNum(gokeiSeikyu);
        }

        private void reportFooter1_Format(object sender, System.EventArgs e)
        {
            // 合計金額の計算
            tanka = Util.ToDecimal(this.テキスト15.Text) / Util.ToDecimal(this.テキスト13.Text);
            zeikomiKingaku = Util.ToDecimal(this.テキスト15.Text) + Util.ToDecimal(this.テキスト16.Text);
            gokeiSeikyu = Util.ToDecimal(this.テキスト15.Text) + Util.ToDecimal(this.テキスト16.Text) + Util.ToDecimal(this.テキスト18.Text);

            // 合計金額のセット
            this.テキスト14.Text = Util.FormatNum(tanka);
            this.テキスト17.Text = Util.FormatNum(zeikomiKingaku);
            this.テキスト19.Text = Util.FormatNum(gokeiSeikyu);
        }
    }
}
