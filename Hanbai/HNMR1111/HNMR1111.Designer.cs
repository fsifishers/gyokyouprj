﻿namespace jp.co.fsi.hn.hnmr1111
{
    partial class HNMR1111
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbxDate = new System.Windows.Forms.GroupBox();
            this.lblDateDayFr = new System.Windows.Forms.Label();
            this.lblDateDayTo = new System.Windows.Forms.Label();
            this.txtDateDayFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblCodeBetDate = new System.Windows.Forms.Label();
            this.txtDateDayTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblDateMonthTo = new System.Windows.Forms.Label();
            this.lblDateYearTo = new System.Windows.Forms.Label();
            this.txtDateYearTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtDateMonthTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblDateGengoTo = new System.Windows.Forms.Label();
            this.lblDateTo = new System.Windows.Forms.Label();
            this.lblDateMonthFr = new System.Windows.Forms.Label();
            this.lblDateYearFr = new System.Windows.Forms.Label();
            this.txtDateYearFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtDateMonthFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblDateGengoFr = new System.Windows.Forms.Label();
            this.lblDateFr = new System.Windows.Forms.Label();
            this.gbxNakagaininCd = new System.Windows.Forms.GroupBox();
            this.lblNakagaininCdBet = new System.Windows.Forms.Label();
            this.lblNakagaininCdTo = new System.Windows.Forms.Label();
            this.lblNakagaininCdFr = new System.Windows.Forms.Label();
            this.txtNakagaininCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtNakagaininCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.gbxMizuageShisho = new System.Windows.Forms.GroupBox();
            this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMizuageShishoNm = new System.Windows.Forms.Label();
            this.lblMizuageShisho = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.gbxDate.SuspendLayout();
            this.gbxNakagaininCd.SuspendLayout();
            this.gbxMizuageShisho.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.TabIndex = 999;
            this.lblTitle.Text = "";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Size = new System.Drawing.Size(847, 100);
            // 
            // gbxDate
            // 
            this.gbxDate.Controls.Add(this.lblDateDayFr);
            this.gbxDate.Controls.Add(this.lblDateDayTo);
            this.gbxDate.Controls.Add(this.txtDateDayFr);
            this.gbxDate.Controls.Add(this.lblCodeBetDate);
            this.gbxDate.Controls.Add(this.txtDateDayTo);
            this.gbxDate.Controls.Add(this.lblDateMonthTo);
            this.gbxDate.Controls.Add(this.lblDateYearTo);
            this.gbxDate.Controls.Add(this.txtDateYearTo);
            this.gbxDate.Controls.Add(this.txtDateMonthTo);
            this.gbxDate.Controls.Add(this.lblDateGengoTo);
            this.gbxDate.Controls.Add(this.lblDateTo);
            this.gbxDate.Controls.Add(this.lblDateMonthFr);
            this.gbxDate.Controls.Add(this.lblDateYearFr);
            this.gbxDate.Controls.Add(this.txtDateYearFr);
            this.gbxDate.Controls.Add(this.txtDateMonthFr);
            this.gbxDate.Controls.Add(this.lblDateGengoFr);
            this.gbxDate.Controls.Add(this.lblDateFr);
            this.gbxDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.gbxDate.ForeColor = System.Drawing.Color.Black;
            this.gbxDate.Location = new System.Drawing.Point(12, 145);
            this.gbxDate.Name = "gbxDate";
            this.gbxDate.Size = new System.Drawing.Size(529, 80);
            this.gbxDate.TabIndex = 1;
            this.gbxDate.TabStop = false;
            this.gbxDate.Text = "日付範囲";
            // 
            // lblDateDayFr
            // 
            this.lblDateDayFr.BackColor = System.Drawing.Color.Silver;
            this.lblDateDayFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblDateDayFr.ForeColor = System.Drawing.Color.Black;
            this.lblDateDayFr.Location = new System.Drawing.Point(210, 32);
            this.lblDateDayFr.Name = "lblDateDayFr";
            this.lblDateDayFr.Size = new System.Drawing.Size(15, 19);
            this.lblDateDayFr.TabIndex = 7;
            this.lblDateDayFr.Text = "日";
            this.lblDateDayFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateDayTo
            // 
            this.lblDateDayTo.BackColor = System.Drawing.Color.Silver;
            this.lblDateDayTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblDateDayTo.ForeColor = System.Drawing.Color.Black;
            this.lblDateDayTo.Location = new System.Drawing.Point(469, 32);
            this.lblDateDayTo.Name = "lblDateDayTo";
            this.lblDateDayTo.Size = new System.Drawing.Size(15, 19);
            this.lblDateDayTo.TabIndex = 16;
            this.lblDateDayTo.Text = "日";
            this.lblDateDayTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDateDayFr
            // 
            this.txtDateDayFr.AutoSizeFromLength = false;
            this.txtDateDayFr.DisplayLength = null;
            this.txtDateDayFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtDateDayFr.ForeColor = System.Drawing.Color.Black;
            this.txtDateDayFr.Location = new System.Drawing.Point(178, 31);
            this.txtDateDayFr.MaxLength = 2;
            this.txtDateDayFr.Name = "txtDateDayFr";
            this.txtDateDayFr.Size = new System.Drawing.Size(30, 20);
            this.txtDateDayFr.TabIndex = 6;
            this.txtDateDayFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateDayFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateDayFr_Validating);
            // 
            // lblCodeBetDate
            // 
            this.lblCodeBetDate.BackColor = System.Drawing.Color.White;
            this.lblCodeBetDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblCodeBetDate.ForeColor = System.Drawing.Color.Black;
            this.lblCodeBetDate.Location = new System.Drawing.Point(240, 30);
            this.lblCodeBetDate.Name = "lblCodeBetDate";
            this.lblCodeBetDate.Size = new System.Drawing.Size(43, 21);
            this.lblCodeBetDate.TabIndex = 8;
            this.lblCodeBetDate.Text = "～";
            this.lblCodeBetDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtDateDayTo
            // 
            this.txtDateDayTo.AutoSizeFromLength = false;
            this.txtDateDayTo.DisplayLength = null;
            this.txtDateDayTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtDateDayTo.ForeColor = System.Drawing.Color.Black;
            this.txtDateDayTo.Location = new System.Drawing.Point(437, 31);
            this.txtDateDayTo.MaxLength = 2;
            this.txtDateDayTo.Name = "txtDateDayTo";
            this.txtDateDayTo.Size = new System.Drawing.Size(30, 20);
            this.txtDateDayTo.TabIndex = 15;
            this.txtDateDayTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateDayTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateDayTo_Validating);
            // 
            // lblDateMonthTo
            // 
            this.lblDateMonthTo.BackColor = System.Drawing.Color.Silver;
            this.lblDateMonthTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblDateMonthTo.ForeColor = System.Drawing.Color.Black;
            this.lblDateMonthTo.Location = new System.Drawing.Point(416, 32);
            this.lblDateMonthTo.Name = "lblDateMonthTo";
            this.lblDateMonthTo.Size = new System.Drawing.Size(15, 19);
            this.lblDateMonthTo.TabIndex = 14;
            this.lblDateMonthTo.Text = "月";
            this.lblDateMonthTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateYearTo
            // 
            this.lblDateYearTo.BackColor = System.Drawing.Color.Silver;
            this.lblDateYearTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblDateYearTo.ForeColor = System.Drawing.Color.Black;
            this.lblDateYearTo.Location = new System.Drawing.Point(363, 31);
            this.lblDateYearTo.Name = "lblDateYearTo";
            this.lblDateYearTo.Size = new System.Drawing.Size(17, 21);
            this.lblDateYearTo.TabIndex = 12;
            this.lblDateYearTo.Text = "年";
            this.lblDateYearTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDateYearTo
            // 
            this.txtDateYearTo.AutoSizeFromLength = false;
            this.txtDateYearTo.DisplayLength = null;
            this.txtDateYearTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtDateYearTo.ForeColor = System.Drawing.Color.Black;
            this.txtDateYearTo.Location = new System.Drawing.Point(331, 31);
            this.txtDateYearTo.MaxLength = 2;
            this.txtDateYearTo.Name = "txtDateYearTo";
            this.txtDateYearTo.Size = new System.Drawing.Size(30, 20);
            this.txtDateYearTo.TabIndex = 11;
            this.txtDateYearTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateYearTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateYearTo_Validating);
            // 
            // txtDateMonthTo
            // 
            this.txtDateMonthTo.AutoSizeFromLength = false;
            this.txtDateMonthTo.DisplayLength = null;
            this.txtDateMonthTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtDateMonthTo.ForeColor = System.Drawing.Color.Black;
            this.txtDateMonthTo.Location = new System.Drawing.Point(384, 31);
            this.txtDateMonthTo.MaxLength = 2;
            this.txtDateMonthTo.Name = "txtDateMonthTo";
            this.txtDateMonthTo.Size = new System.Drawing.Size(30, 20);
            this.txtDateMonthTo.TabIndex = 13;
            this.txtDateMonthTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateMonthTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateMonthTo_Validating);
            // 
            // lblDateGengoTo
            // 
            this.lblDateGengoTo.BackColor = System.Drawing.Color.Silver;
            this.lblDateGengoTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDateGengoTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblDateGengoTo.ForeColor = System.Drawing.Color.Black;
            this.lblDateGengoTo.Location = new System.Drawing.Point(286, 31);
            this.lblDateGengoTo.Name = "lblDateGengoTo";
            this.lblDateGengoTo.Size = new System.Drawing.Size(41, 21);
            this.lblDateGengoTo.TabIndex = 10;
            this.lblDateGengoTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateTo
            // 
            this.lblDateTo.BackColor = System.Drawing.Color.Silver;
            this.lblDateTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDateTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblDateTo.ForeColor = System.Drawing.Color.Black;
            this.lblDateTo.Location = new System.Drawing.Point(283, 28);
            this.lblDateTo.Name = "lblDateTo";
            this.lblDateTo.Size = new System.Drawing.Size(210, 27);
            this.lblDateTo.TabIndex = 9;
            this.lblDateTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateMonthFr
            // 
            this.lblDateMonthFr.BackColor = System.Drawing.Color.Silver;
            this.lblDateMonthFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblDateMonthFr.ForeColor = System.Drawing.Color.Black;
            this.lblDateMonthFr.Location = new System.Drawing.Point(157, 32);
            this.lblDateMonthFr.Name = "lblDateMonthFr";
            this.lblDateMonthFr.Size = new System.Drawing.Size(15, 19);
            this.lblDateMonthFr.TabIndex = 5;
            this.lblDateMonthFr.Text = "月";
            this.lblDateMonthFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateYearFr
            // 
            this.lblDateYearFr.BackColor = System.Drawing.Color.Silver;
            this.lblDateYearFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblDateYearFr.ForeColor = System.Drawing.Color.Black;
            this.lblDateYearFr.Location = new System.Drawing.Point(104, 31);
            this.lblDateYearFr.Name = "lblDateYearFr";
            this.lblDateYearFr.Size = new System.Drawing.Size(17, 21);
            this.lblDateYearFr.TabIndex = 3;
            this.lblDateYearFr.Text = "年";
            this.lblDateYearFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDateYearFr
            // 
            this.txtDateYearFr.AutoSizeFromLength = false;
            this.txtDateYearFr.DisplayLength = null;
            this.txtDateYearFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtDateYearFr.ForeColor = System.Drawing.Color.Black;
            this.txtDateYearFr.Location = new System.Drawing.Point(72, 31);
            this.txtDateYearFr.MaxLength = 2;
            this.txtDateYearFr.Name = "txtDateYearFr";
            this.txtDateYearFr.Size = new System.Drawing.Size(30, 20);
            this.txtDateYearFr.TabIndex = 2;
            this.txtDateYearFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateYearFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateYearFr_Validating);
            // 
            // txtDateMonthFr
            // 
            this.txtDateMonthFr.AutoSizeFromLength = false;
            this.txtDateMonthFr.DisplayLength = null;
            this.txtDateMonthFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtDateMonthFr.ForeColor = System.Drawing.Color.Black;
            this.txtDateMonthFr.Location = new System.Drawing.Point(125, 31);
            this.txtDateMonthFr.MaxLength = 2;
            this.txtDateMonthFr.Name = "txtDateMonthFr";
            this.txtDateMonthFr.Size = new System.Drawing.Size(30, 20);
            this.txtDateMonthFr.TabIndex = 4;
            this.txtDateMonthFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateMonthFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateMonthFr_Validating);
            // 
            // lblDateGengoFr
            // 
            this.lblDateGengoFr.BackColor = System.Drawing.Color.Silver;
            this.lblDateGengoFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDateGengoFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblDateGengoFr.ForeColor = System.Drawing.Color.Black;
            this.lblDateGengoFr.Location = new System.Drawing.Point(27, 31);
            this.lblDateGengoFr.Name = "lblDateGengoFr";
            this.lblDateGengoFr.Size = new System.Drawing.Size(41, 21);
            this.lblDateGengoFr.TabIndex = 1;
            this.lblDateGengoFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateFr
            // 
            this.lblDateFr.BackColor = System.Drawing.Color.Silver;
            this.lblDateFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDateFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblDateFr.ForeColor = System.Drawing.Color.Black;
            this.lblDateFr.Location = new System.Drawing.Point(24, 28);
            this.lblDateFr.Name = "lblDateFr";
            this.lblDateFr.Size = new System.Drawing.Size(210, 27);
            this.lblDateFr.TabIndex = 0;
            this.lblDateFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // gbxNakagaininCd
            // 
            this.gbxNakagaininCd.Controls.Add(this.lblNakagaininCdBet);
            this.gbxNakagaininCd.Controls.Add(this.lblNakagaininCdTo);
            this.gbxNakagaininCd.Controls.Add(this.lblNakagaininCdFr);
            this.gbxNakagaininCd.Controls.Add(this.txtNakagaininCdTo);
            this.gbxNakagaininCd.Controls.Add(this.txtNakagaininCdFr);
            this.gbxNakagaininCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.gbxNakagaininCd.ForeColor = System.Drawing.Color.Black;
            this.gbxNakagaininCd.Location = new System.Drawing.Point(12, 240);
            this.gbxNakagaininCd.Name = "gbxNakagaininCd";
            this.gbxNakagaininCd.Size = new System.Drawing.Size(573, 79);
            this.gbxNakagaininCd.TabIndex = 2;
            this.gbxNakagaininCd.TabStop = false;
            this.gbxNakagaininCd.Text = "仲買人CD範囲";
            // 
            // lblNakagaininCdBet
            // 
            this.lblNakagaininCdBet.BackColor = System.Drawing.Color.White;
            this.lblNakagaininCdBet.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblNakagaininCdBet.ForeColor = System.Drawing.Color.Black;
            this.lblNakagaininCdBet.Location = new System.Drawing.Point(265, 33);
            this.lblNakagaininCdBet.Name = "lblNakagaininCdBet";
            this.lblNakagaininCdBet.Size = new System.Drawing.Size(43, 21);
            this.lblNakagaininCdBet.TabIndex = 2;
            this.lblNakagaininCdBet.Text = "～";
            this.lblNakagaininCdBet.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblNakagaininCdTo
            // 
            this.lblNakagaininCdTo.BackColor = System.Drawing.Color.Silver;
            this.lblNakagaininCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblNakagaininCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblNakagaininCdTo.Location = new System.Drawing.Point(364, 33);
            this.lblNakagaininCdTo.Name = "lblNakagaininCdTo";
            this.lblNakagaininCdTo.Size = new System.Drawing.Size(180, 20);
            this.lblNakagaininCdTo.TabIndex = 4;
            this.lblNakagaininCdTo.Text = "最　後";
            this.lblNakagaininCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblNakagaininCdFr
            // 
            this.lblNakagaininCdFr.BackColor = System.Drawing.Color.Silver;
            this.lblNakagaininCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblNakagaininCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblNakagaininCdFr.Location = new System.Drawing.Point(79, 33);
            this.lblNakagaininCdFr.Name = "lblNakagaininCdFr";
            this.lblNakagaininCdFr.Size = new System.Drawing.Size(180, 20);
            this.lblNakagaininCdFr.TabIndex = 1;
            this.lblNakagaininCdFr.Text = "先　頭";
            this.lblNakagaininCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtNakagaininCdTo
            // 
            this.txtNakagaininCdTo.AutoSizeFromLength = false;
            this.txtNakagaininCdTo.DisplayLength = null;
            this.txtNakagaininCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtNakagaininCdTo.ForeColor = System.Drawing.Color.Black;
            this.txtNakagaininCdTo.Location = new System.Drawing.Point(312, 33);
            this.txtNakagaininCdTo.MaxLength = 4;
            this.txtNakagaininCdTo.Name = "txtNakagaininCdTo";
            this.txtNakagaininCdTo.Size = new System.Drawing.Size(49, 20);
            this.txtNakagaininCdTo.TabIndex = 3;
            this.txtNakagaininCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtNakagaininCdTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNakagaininCdTo_KeyDown);
            this.txtNakagaininCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtNakagaininCdTo_Validating);
            // 
            // txtNakagaininCdFr
            // 
            this.txtNakagaininCdFr.AutoSizeFromLength = false;
            this.txtNakagaininCdFr.DisplayLength = null;
            this.txtNakagaininCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtNakagaininCdFr.ForeColor = System.Drawing.Color.Black;
            this.txtNakagaininCdFr.Location = new System.Drawing.Point(27, 33);
            this.txtNakagaininCdFr.MaxLength = 4;
            this.txtNakagaininCdFr.Name = "txtNakagaininCdFr";
            this.txtNakagaininCdFr.Size = new System.Drawing.Size(49, 20);
            this.txtNakagaininCdFr.TabIndex = 0;
            this.txtNakagaininCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtNakagaininCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtNakagaininCdFr_Validating);
            // 
            // gbxMizuageShisho
            // 
            this.gbxMizuageShisho.Controls.Add(this.txtMizuageShishoCd);
            this.gbxMizuageShisho.Controls.Add(this.lblMizuageShishoNm);
            this.gbxMizuageShisho.Controls.Add(this.lblMizuageShisho);
            this.gbxMizuageShisho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.gbxMizuageShisho.ForeColor = System.Drawing.Color.Black;
            this.gbxMizuageShisho.Location = new System.Drawing.Point(12, 54);
            this.gbxMizuageShisho.Name = "gbxMizuageShisho";
            this.gbxMizuageShisho.Size = new System.Drawing.Size(381, 77);
            this.gbxMizuageShisho.TabIndex = 0;
            this.gbxMizuageShisho.TabStop = false;
            this.gbxMizuageShisho.Text = "水揚支所";
            // 
            // txtMizuageShishoCd
            // 
            this.txtMizuageShishoCd.AutoSizeFromLength = true;
            this.txtMizuageShishoCd.DisplayLength = null;
            this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtMizuageShishoCd.Location = new System.Drawing.Point(93, 33);
            this.txtMizuageShishoCd.MaxLength = 4;
            this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
            this.txtMizuageShishoCd.Size = new System.Drawing.Size(34, 20);
            this.txtMizuageShishoCd.TabIndex = 1;
            this.txtMizuageShishoCd.TabStop = false;
            this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
            // 
            // lblMizuageShishoNm
            // 
            this.lblMizuageShishoNm.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMizuageShishoNm.Location = new System.Drawing.Point(128, 33);
            this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
            this.lblMizuageShishoNm.Size = new System.Drawing.Size(212, 20);
            this.lblMizuageShishoNm.TabIndex = 2;
            this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMizuageShisho
            // 
            this.lblMizuageShisho.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShisho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShisho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblMizuageShisho.Location = new System.Drawing.Point(26, 31);
            this.lblMizuageShisho.Name = "lblMizuageShisho";
            this.lblMizuageShisho.Size = new System.Drawing.Size(320, 25);
            this.lblMizuageShisho.TabIndex = 0;
            this.lblMizuageShisho.Text = "水揚支所";
            this.lblMizuageShisho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // HNMR1111
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 638);
            this.Controls.Add(this.gbxMizuageShisho);
            this.Controls.Add(this.gbxNakagaininCd);
            this.Controls.Add(this.gbxDate);
            this.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "HNMR1111";
            this.Par1 = "11";
            this.Text = "";
            this.Controls.SetChildIndex(this.gbxDate, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.gbxNakagaininCd, 0);
            this.Controls.SetChildIndex(this.gbxMizuageShisho, 0);
            this.pnlDebug.ResumeLayout(false);
            this.gbxDate.ResumeLayout(false);
            this.gbxDate.PerformLayout();
            this.gbxNakagaininCd.ResumeLayout(false);
            this.gbxNakagaininCd.PerformLayout();
            this.gbxMizuageShisho.ResumeLayout(false);
            this.gbxMizuageShisho.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbxDate;
        private jp.co.fsi.common.controls.FsiTextBox txtDateYearFr;
        private System.Windows.Forms.Label lblDateGengoFr;
        private System.Windows.Forms.Label lblDateFr;
        private System.Windows.Forms.Label lblDateMonthFr;
        private System.Windows.Forms.Label lblDateYearFr;
        private jp.co.fsi.common.controls.FsiTextBox txtDateMonthFr;
        private System.Windows.Forms.Label lblCodeBetDate;
        private System.Windows.Forms.Label lblDateMonthTo;
        private System.Windows.Forms.Label lblDateYearTo;
        private common.controls.FsiTextBox txtDateYearTo;
        private common.controls.FsiTextBox txtDateMonthTo;
        private System.Windows.Forms.Label lblDateGengoTo;
        private System.Windows.Forms.Label lblDateTo;
        private System.Windows.Forms.Label lblDateDayFr;
        private System.Windows.Forms.Label lblDateDayTo;
        private common.controls.FsiTextBox txtDateDayFr;
        private common.controls.FsiTextBox txtDateDayTo;
        private System.Windows.Forms.GroupBox gbxNakagaininCd;
        private System.Windows.Forms.Label lblNakagaininCdBet;
        private System.Windows.Forms.Label lblNakagaininCdTo;
        private System.Windows.Forms.Label lblNakagaininCdFr;
        private common.controls.FsiTextBox txtNakagaininCdTo;
        private common.controls.FsiTextBox txtNakagaininCdFr;
        private System.Windows.Forms.GroupBox gbxMizuageShisho;
        private common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private System.Windows.Forms.Label lblMizuageShisho;

    }
}