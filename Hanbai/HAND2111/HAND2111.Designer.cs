﻿namespace jp.co.fsi.han.hand2111
{
    partial class HAND2111
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtMonthFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtDayFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.labelDateYearFr = new System.Windows.Forms.Label();
            this.lblDateMonthFr = new System.Windows.Forms.Label();
            this.lblDateDayFr = new System.Windows.Forms.Label();
            this.gbxDate = new System.Windows.Forms.GroupBox();
            this.lblGengoTo = new System.Windows.Forms.Label();
            this.lblGengoFr = new System.Windows.Forms.Label();
            this.lblDateDayTo = new System.Windows.Forms.Label();
            this.lblDateMonthTo = new System.Windows.Forms.Label();
            this.lblDateBet = new System.Windows.Forms.Label();
            this.labelDateYearTo = new System.Windows.Forms.Label();
            this.txtDayTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtYearTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtYearFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtMonthTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblDateFr = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblGengo = new System.Windows.Forms.Label();
            this.lblMessage = new System.Windows.Forms.Label();
            this.lblFurikomibiNote = new System.Windows.Forms.Label();
            this.lblTekiyoNote = new System.Windows.Forms.Label();
            this.lblFurikomibiDay = new System.Windows.Forms.Label();
            this.lblFurikomibiMonth = new System.Windows.Forms.Label();
            this.lblFurikomibiYear = new System.Windows.Forms.Label();
            this.txtDay = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtYear = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtMonth = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblFurikomibiCap = new System.Windows.Forms.Label();
            this.txtTekiyo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblTekiyoCap = new System.Windows.Forms.Label();
            this.lblFloppy = new System.Windows.Forms.Label();
            this.grpSakuseiKubun = new System.Windows.Forms.GroupBox();
            this.rdoHonninTsumitate = new System.Windows.Forms.RadioButton();
            this.rdoFutsu = new System.Windows.Forms.RadioButton();
            this.rdoAzukarikin = new System.Windows.Forms.RadioButton();
            this.rdoTsumitate = new System.Windows.Forms.RadioButton();
            this.gbxMizuageShisho = new System.Windows.Forms.GroupBox();
            this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMizuageShishoNm = new System.Windows.Forms.Label();
            this.lblMizuageShisho = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.gbxDate.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.grpSakuseiKubun.SuspendLayout();
            this.gbxMizuageShisho.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.TabIndex = 999;
            this.lblTitle.Text = "信用連帯";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Size = new System.Drawing.Size(847, 100);
            // 
            // txtDateMonthFr
            // 
            this.txtMonthFr.AutoSizeFromLength = true;
            this.txtMonthFr.DisplayLength = null;
            this.txtMonthFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtMonthFr.Location = new System.Drawing.Point(95, 33);
            this.txtMonthFr.MaxLength = 2;
            this.txtMonthFr.Name = "txtDateMonthFr";
            this.txtMonthFr.Size = new System.Drawing.Size(20, 20);
            this.txtMonthFr.TabIndex = 4;
            this.txtMonthFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonthFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthFr_Validating);
            // 
            // txtDateDayFr
            // 
            this.txtDayFr.AutoSizeFromLength = true;
            this.txtDayFr.DisplayLength = null;
            this.txtDayFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtDayFr.Location = new System.Drawing.Point(135, 33);
            this.txtDayFr.MaxLength = 2;
            this.txtDayFr.Name = "txtDateDayFr";
            this.txtDayFr.Size = new System.Drawing.Size(20, 20);
            this.txtDayFr.TabIndex = 6;
            this.txtDayFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDayFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDayFr_Validating);
            // 
            // labelDateYearFr
            // 
            this.labelDateYearFr.BackColor = System.Drawing.Color.Silver;
            this.labelDateYearFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.labelDateYearFr.Location = new System.Drawing.Point(76, 33);
            this.labelDateYearFr.Name = "labelDateYearFr";
            this.labelDateYearFr.Size = new System.Drawing.Size(18, 20);
            this.labelDateYearFr.TabIndex = 3;
            this.labelDateYearFr.Text = "年";
            this.labelDateYearFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateMonthFr
            // 
            this.lblDateMonthFr.BackColor = System.Drawing.Color.Silver;
            this.lblDateMonthFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblDateMonthFr.Location = new System.Drawing.Point(116, 33);
            this.lblDateMonthFr.Name = "lblDateMonthFr";
            this.lblDateMonthFr.Size = new System.Drawing.Size(18, 20);
            this.lblDateMonthFr.TabIndex = 5;
            this.lblDateMonthFr.Text = "月";
            this.lblDateMonthFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateDayFr
            // 
            this.lblDateDayFr.BackColor = System.Drawing.Color.Silver;
            this.lblDateDayFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblDateDayFr.Location = new System.Drawing.Point(156, 33);
            this.lblDateDayFr.Name = "lblDateDayFr";
            this.lblDateDayFr.Size = new System.Drawing.Size(18, 20);
            this.lblDateDayFr.TabIndex = 7;
            this.lblDateDayFr.Text = "日";
            this.lblDateDayFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // gbxDate
            // 
            this.gbxDate.Controls.Add(this.lblGengoTo);
            this.gbxDate.Controls.Add(this.lblGengoFr);
            this.gbxDate.Controls.Add(this.lblDateDayTo);
            this.gbxDate.Controls.Add(this.lblDateDayFr);
            this.gbxDate.Controls.Add(this.lblDateMonthTo);
            this.gbxDate.Controls.Add(this.lblDateBet);
            this.gbxDate.Controls.Add(this.lblDateMonthFr);
            this.gbxDate.Controls.Add(this.labelDateYearTo);
            this.gbxDate.Controls.Add(this.labelDateYearFr);
            this.gbxDate.Controls.Add(this.txtDayTo);
            this.gbxDate.Controls.Add(this.txtDayFr);
            this.gbxDate.Controls.Add(this.txtYearTo);
            this.gbxDate.Controls.Add(this.txtYearFr);
            this.gbxDate.Controls.Add(this.txtMonthTo);
            this.gbxDate.Controls.Add(this.txtMonthFr);
            this.gbxDate.Controls.Add(this.lblDateFr);
            this.gbxDate.Controls.Add(this.label1);
            this.gbxDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.gbxDate.ForeColor = System.Drawing.SystemColors.ControlText;
            this.gbxDate.Location = new System.Drawing.Point(12, 140);
            this.gbxDate.Name = "gbxDate";
            this.gbxDate.Size = new System.Drawing.Size(381, 76);
            this.gbxDate.TabIndex = 1;
            this.gbxDate.TabStop = false;
            this.gbxDate.Text = "日付範囲";
            // 
            // lblDateEraTo
            // 
            this.lblGengoTo.BackColor = System.Drawing.Color.Silver;
            this.lblGengoTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGengoTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblGengoTo.Location = new System.Drawing.Point(203, 33);
            this.lblGengoTo.Name = "lblDateEraTo";
            this.lblGengoTo.Size = new System.Drawing.Size(41, 22);
            this.lblGengoTo.TabIndex = 10;
            this.lblGengoTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateGengoTo
            // 
            this.lblGengoFr.BackColor = System.Drawing.Color.Silver;
            this.lblGengoFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGengoFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblGengoFr.Location = new System.Drawing.Point(13, 33);
            this.lblGengoFr.Name = "lblDateGengoTo";
            this.lblGengoFr.Size = new System.Drawing.Size(41, 22);
            this.lblGengoFr.TabIndex = 1;
            this.lblGengoFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateDayTo
            // 
            this.lblDateDayTo.BackColor = System.Drawing.Color.Silver;
            this.lblDateDayTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblDateDayTo.Location = new System.Drawing.Point(347, 33);
            this.lblDateDayTo.Name = "lblDateDayTo";
            this.lblDateDayTo.Size = new System.Drawing.Size(18, 20);
            this.lblDateDayTo.TabIndex = 16;
            this.lblDateDayTo.Text = "日";
            this.lblDateDayTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateMonthTo
            // 
            this.lblDateMonthTo.BackColor = System.Drawing.Color.Silver;
            this.lblDateMonthTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblDateMonthTo.Location = new System.Drawing.Point(307, 33);
            this.lblDateMonthTo.Name = "lblDateMonthTo";
            this.lblDateMonthTo.Size = new System.Drawing.Size(18, 20);
            this.lblDateMonthTo.TabIndex = 14;
            this.lblDateMonthTo.Text = "月";
            this.lblDateMonthTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateBet
            // 
            this.lblDateBet.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblDateBet.Location = new System.Drawing.Point(177, 31);
            this.lblDateBet.Name = "lblDateBet";
            this.lblDateBet.Size = new System.Drawing.Size(20, 25);
            this.lblDateBet.TabIndex = 8;
            this.lblDateBet.Text = "～";
            this.lblDateBet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelDateYearTo
            // 
            this.labelDateYearTo.BackColor = System.Drawing.Color.Silver;
            this.labelDateYearTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.labelDateYearTo.Location = new System.Drawing.Point(267, 33);
            this.labelDateYearTo.Name = "labelDateYearTo";
            this.labelDateYearTo.Size = new System.Drawing.Size(18, 20);
            this.labelDateYearTo.TabIndex = 12;
            this.labelDateYearTo.Text = "年";
            this.labelDateYearTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDateDayTo
            // 
            this.txtDayTo.AutoSizeFromLength = true;
            this.txtDayTo.DisplayLength = null;
            this.txtDayTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtDayTo.Location = new System.Drawing.Point(326, 33);
            this.txtDayTo.MaxLength = 2;
            this.txtDayTo.Name = "txtDateDayTo";
            this.txtDayTo.Size = new System.Drawing.Size(20, 20);
            this.txtDayTo.TabIndex = 15;
            this.txtDayTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDayTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDayTo_Validating);
            // 
            // txtDateYearTo
            // 
            this.txtYearTo.AutoSizeFromLength = true;
            this.txtYearTo.DisplayLength = null;
            this.txtYearTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtYearTo.Location = new System.Drawing.Point(246, 33);
            this.txtYearTo.MaxLength = 2;
            this.txtYearTo.Name = "txtDateYearTo";
            this.txtYearTo.Size = new System.Drawing.Size(20, 20);
            this.txtYearTo.TabIndex = 11;
            this.txtYearTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtYearTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtYearTo_Validating);
            // 
            // txtDateYearFr
            // 
            this.txtYearFr.AutoSizeFromLength = true;
            this.txtYearFr.DisplayLength = null;
            this.txtYearFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtYearFr.Location = new System.Drawing.Point(55, 33);
            this.txtYearFr.MaxLength = 2;
            this.txtYearFr.Name = "txtDateYearFr";
            this.txtYearFr.Size = new System.Drawing.Size(20, 20);
            this.txtYearFr.TabIndex = 2;
            this.txtYearFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtYearFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtYearFr_Validating);
            // 
            // txtDateMonthTo
            // 
            this.txtMonthTo.AutoSizeFromLength = true;
            this.txtMonthTo.DisplayLength = null;
            this.txtMonthTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtMonthTo.Location = new System.Drawing.Point(286, 33);
            this.txtMonthTo.MaxLength = 2;
            this.txtMonthTo.Name = "txtDateMonthTo";
            this.txtMonthTo.Size = new System.Drawing.Size(20, 20);
            this.txtMonthTo.TabIndex = 13;
            this.txtMonthTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonthTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthTo_Validating);
            // 
            // lblDateFr
            // 
            this.lblDateFr.BackColor = System.Drawing.Color.Silver;
            this.lblDateFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblDateFr.Location = new System.Drawing.Point(10, 31);
            this.lblDateFr.Name = "lblDateFr";
            this.lblDateFr.Size = new System.Drawing.Size(166, 25);
            this.lblDateFr.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Silver;
            this.label1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.label1.Location = new System.Drawing.Point(199, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(166, 25);
            this.label1.TabIndex = 9;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblGengo);
            this.groupBox1.Controls.Add(this.lblMessage);
            this.groupBox1.Controls.Add(this.lblFurikomibiNote);
            this.groupBox1.Controls.Add(this.lblTekiyoNote);
            this.groupBox1.Controls.Add(this.lblFurikomibiDay);
            this.groupBox1.Controls.Add(this.lblFurikomibiMonth);
            this.groupBox1.Controls.Add(this.lblFurikomibiYear);
            this.groupBox1.Controls.Add(this.txtDay);
            this.groupBox1.Controls.Add(this.txtYear);
            this.groupBox1.Controls.Add(this.txtMonth);
            this.groupBox1.Controls.Add(this.lblFurikomibiCap);
            this.groupBox1.Controls.Add(this.txtTekiyo);
            this.groupBox1.Controls.Add(this.lblTekiyoCap);
            this.groupBox1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.groupBox1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox1.Location = new System.Drawing.Point(12, 245);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(381, 126);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "摘要・振込日";
            // 
            // lblFurikomibiEra
            // 
            this.lblGengo.BackColor = System.Drawing.Color.Silver;
            this.lblGengo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGengo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblGengo.Location = new System.Drawing.Point(98, 60);
            this.lblGengo.Name = "lblFurikomibiEra";
            this.lblGengo.Size = new System.Drawing.Size(41, 20);
            this.lblGengo.TabIndex = 4;
            this.lblGengo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMessage
            // 
            this.lblMessage.BackColor = System.Drawing.Color.Transparent;
            this.lblMessage.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblMessage.Location = new System.Drawing.Point(7, 95);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(366, 20);
            this.lblMessage.TabIndex = 12;
            this.lblMessage.Text = "通帳印字用の摘要メッセージと振込日を入力して下さい";
            this.lblMessage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblFurikomibiNote
            // 
            this.lblFurikomibiNote.BackColor = System.Drawing.Color.Transparent;
            this.lblFurikomibiNote.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblFurikomibiNote.Location = new System.Drawing.Point(273, 61);
            this.lblFurikomibiNote.Name = "lblFurikomibiNote";
            this.lblFurikomibiNote.Size = new System.Drawing.Size(111, 20);
            this.lblFurikomibiNote.TabIndex = 11;
            this.lblFurikomibiNote.Text = "（和暦入力）";
            this.lblFurikomibiNote.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTekiyoNote
            // 
            this.lblTekiyoNote.BackColor = System.Drawing.Color.Transparent;
            this.lblTekiyoNote.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblTekiyoNote.Location = new System.Drawing.Point(212, 27);
            this.lblTekiyoNote.Name = "lblTekiyoNote";
            this.lblTekiyoNote.Size = new System.Drawing.Size(126, 20);
            this.lblTekiyoNote.TabIndex = 2;
            this.lblTekiyoNote.Text = "（１２文字入力）";
            this.lblTekiyoNote.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblFurikomibiDay
            // 
            this.lblFurikomibiDay.BackColor = System.Drawing.Color.Silver;
            this.lblFurikomibiDay.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblFurikomibiDay.Location = new System.Drawing.Point(243, 60);
            this.lblFurikomibiDay.Name = "lblFurikomibiDay";
            this.lblFurikomibiDay.Size = new System.Drawing.Size(18, 20);
            this.lblFurikomibiDay.TabIndex = 10;
            this.lblFurikomibiDay.Text = "日";
            this.lblFurikomibiDay.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblFurikomibiMonth
            // 
            this.lblFurikomibiMonth.BackColor = System.Drawing.Color.Silver;
            this.lblFurikomibiMonth.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblFurikomibiMonth.Location = new System.Drawing.Point(203, 60);
            this.lblFurikomibiMonth.Name = "lblFurikomibiMonth";
            this.lblFurikomibiMonth.Size = new System.Drawing.Size(18, 20);
            this.lblFurikomibiMonth.TabIndex = 8;
            this.lblFurikomibiMonth.Text = "月";
            this.lblFurikomibiMonth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblFurikomibiYear
            // 
            this.lblFurikomibiYear.BackColor = System.Drawing.Color.Silver;
            this.lblFurikomibiYear.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblFurikomibiYear.Location = new System.Drawing.Point(163, 60);
            this.lblFurikomibiYear.Name = "lblFurikomibiYear";
            this.lblFurikomibiYear.Size = new System.Drawing.Size(18, 20);
            this.lblFurikomibiYear.TabIndex = 6;
            this.lblFurikomibiYear.Text = "年";
            this.lblFurikomibiYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFurikomibiDay
            // 
            this.txtDay.AutoSizeFromLength = true;
            this.txtDay.DisplayLength = null;
            this.txtDay.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtDay.Location = new System.Drawing.Point(222, 60);
            this.txtDay.MaxLength = 2;
            this.txtDay.Name = "txtFurikomibiDay";
            this.txtDay.Size = new System.Drawing.Size(20, 20);
            this.txtDay.TabIndex = 9;
            this.txtDay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDay.Validating += new System.ComponentModel.CancelEventHandler(this.txtDay_Validating);
            // 
            // txtFurikomibiYear
            // 
            this.txtYear.AutoSizeFromLength = true;
            this.txtYear.DisplayLength = null;
            this.txtYear.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtYear.Location = new System.Drawing.Point(142, 60);
            this.txtYear.MaxLength = 2;
            this.txtYear.Name = "txtFurikomibiYear";
            this.txtYear.Size = new System.Drawing.Size(20, 20);
            this.txtYear.TabIndex = 5;
            this.txtYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtYear.Validating += new System.ComponentModel.CancelEventHandler(this.txtYear_Validating);
            // 
            // txtFurikomibiMonth
            // 
            this.txtMonth.AutoSizeFromLength = true;
            this.txtMonth.DisplayLength = null;
            this.txtMonth.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtMonth.Location = new System.Drawing.Point(182, 60);
            this.txtMonth.MaxLength = 2;
            this.txtMonth.Name = "txtFurikomibiMonth";
            this.txtMonth.Size = new System.Drawing.Size(20, 20);
            this.txtMonth.TabIndex = 7;
            this.txtMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonth.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonth_Validating);
            // 
            // lblFurikomibiCap
            // 
            this.lblFurikomibiCap.BackColor = System.Drawing.Color.Silver;
            this.lblFurikomibiCap.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFurikomibiCap.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblFurikomibiCap.Location = new System.Drawing.Point(10, 58);
            this.lblFurikomibiCap.Name = "lblFurikomibiCap";
            this.lblFurikomibiCap.Size = new System.Drawing.Size(252, 25);
            this.lblFurikomibiCap.TabIndex = 3;
            this.lblFurikomibiCap.Text = "振　込　日";
            this.lblFurikomibiCap.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTekiyo
            // 
            this.txtTekiyo.AutoSizeFromLength = true;
            this.txtTekiyo.DisplayLength = null;
            this.txtTekiyo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtTekiyo.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtTekiyo.Location = new System.Drawing.Point(99, 27);
            this.txtTekiyo.MaxLength = 12;
            this.txtTekiyo.Name = "txtTekiyo";
            this.txtTekiyo.Size = new System.Drawing.Size(99, 20);
            this.txtTekiyo.TabIndex = 1;
            this.txtTekiyo.Validating += new System.ComponentModel.CancelEventHandler(this.txtTekiyo_Validating);
            // 
            // lblTekiyoCap
            // 
            this.lblTekiyoCap.BackColor = System.Drawing.Color.Silver;
            this.lblTekiyoCap.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTekiyoCap.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblTekiyoCap.Location = new System.Drawing.Point(10, 25);
            this.lblTekiyoCap.Name = "lblTekiyoCap";
            this.lblTekiyoCap.Size = new System.Drawing.Size(192, 25);
            this.lblTekiyoCap.TabIndex = 0;
            this.lblTekiyoCap.Text = "摘     要";
            this.lblTekiyoCap.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblFloppy
            // 
            this.lblFloppy.BackColor = System.Drawing.Color.Transparent;
            this.lblFloppy.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F);
            this.lblFloppy.ForeColor = System.Drawing.Color.Red;
            this.lblFloppy.Location = new System.Drawing.Point(14, 411);
            this.lblFloppy.Name = "lblFloppy";
            this.lblFloppy.Size = new System.Drawing.Size(443, 20);
            this.lblFloppy.TabIndex = 4;
            this.lblFloppy.Text = "信用移行用のフロッピーをセットして下さい！！！";
            this.lblFloppy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // grpSakuseiKubun
            // 
            this.grpSakuseiKubun.CausesValidation = false;
            this.grpSakuseiKubun.Controls.Add(this.rdoHonninTsumitate);
            this.grpSakuseiKubun.Controls.Add(this.rdoFutsu);
            this.grpSakuseiKubun.Controls.Add(this.rdoAzukarikin);
            this.grpSakuseiKubun.Controls.Add(this.rdoTsumitate);
            this.grpSakuseiKubun.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.grpSakuseiKubun.Location = new System.Drawing.Point(414, 245);
            this.grpSakuseiKubun.Name = "grpSakuseiKubun";
            this.grpSakuseiKubun.Size = new System.Drawing.Size(144, 126);
            this.grpSakuseiKubun.TabIndex = 3;
            this.grpSakuseiKubun.TabStop = false;
            this.grpSakuseiKubun.Text = "作成区分";
            // 
            // rdoHonninTsumitate
            // 
            this.rdoHonninTsumitate.AutoSize = true;
            this.rdoHonninTsumitate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.rdoHonninTsumitate.Location = new System.Drawing.Point(18, 94);
            this.rdoHonninTsumitate.Name = "rdoHonninTsumitate";
            this.rdoHonninTsumitate.Size = new System.Drawing.Size(109, 17);
            this.rdoHonninTsumitate.TabIndex = 3;
            this.rdoHonninTsumitate.Text = "本人積立振込";
            this.rdoHonninTsumitate.UseVisualStyleBackColor = true;
            this.rdoHonninTsumitate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rdoHonninTsumitate_KeyDown);
            // 
            // rdoFutsu
            // 
            this.rdoFutsu.AutoSize = true;
            this.rdoFutsu.Checked = true;
            this.rdoFutsu.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.rdoFutsu.Location = new System.Drawing.Point(18, 19);
            this.rdoFutsu.Name = "rdoFutsu";
            this.rdoFutsu.Size = new System.Drawing.Size(81, 17);
            this.rdoFutsu.TabIndex = 0;
            this.rdoFutsu.TabStop = true;
            this.rdoFutsu.Text = "普通振込";
            this.rdoFutsu.UseVisualStyleBackColor = true;
            this.rdoFutsu.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rdoFutsu_KeyDown);
            // 
            // rdoAzukarikin
            // 
            this.rdoAzukarikin.AutoSize = true;
            this.rdoAzukarikin.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.rdoAzukarikin.Location = new System.Drawing.Point(18, 69);
            this.rdoAzukarikin.Name = "rdoAzukarikin";
            this.rdoAzukarikin.Size = new System.Drawing.Size(95, 17);
            this.rdoAzukarikin.TabIndex = 2;
            this.rdoAzukarikin.Text = "預り金振込";
            this.rdoAzukarikin.UseVisualStyleBackColor = true;
            this.rdoAzukarikin.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rdoAzukarikin_KeyDown);
            // 
            // rdoTsumitate
            // 
            this.rdoTsumitate.AutoSize = true;
            this.rdoTsumitate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.rdoTsumitate.Location = new System.Drawing.Point(18, 44);
            this.rdoTsumitate.Name = "rdoTsumitate";
            this.rdoTsumitate.Size = new System.Drawing.Size(81, 17);
            this.rdoTsumitate.TabIndex = 1;
            this.rdoTsumitate.Text = "積立振込";
            this.rdoTsumitate.UseVisualStyleBackColor = true;
            this.rdoTsumitate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rdoTsumitate_KeyDown);
            // 
            // gbxMizuageShisho
            // 
            this.gbxMizuageShisho.Controls.Add(this.txtMizuageShishoCd);
            this.gbxMizuageShisho.Controls.Add(this.lblMizuageShishoNm);
            this.gbxMizuageShisho.Controls.Add(this.lblMizuageShisho);
            this.gbxMizuageShisho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.gbxMizuageShisho.ForeColor = System.Drawing.Color.Black;
            this.gbxMizuageShisho.Location = new System.Drawing.Point(12, 47);
            this.gbxMizuageShisho.Name = "gbxMizuageShisho";
            this.gbxMizuageShisho.Size = new System.Drawing.Size(381, 77);
            this.gbxMizuageShisho.TabIndex = 0;
            this.gbxMizuageShisho.TabStop = false;
            this.gbxMizuageShisho.Text = "支所";
            // 
            // txtMizuageShishoCd
            // 
            this.txtMizuageShishoCd.AutoSizeFromLength = true;
            this.txtMizuageShishoCd.DisplayLength = null;
            this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtMizuageShishoCd.Location = new System.Drawing.Point(93, 33);
            this.txtMizuageShishoCd.MaxLength = 5;
            this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
            this.txtMizuageShishoCd.Size = new System.Drawing.Size(51, 20);
            this.txtMizuageShishoCd.TabIndex = 1;
            this.txtMizuageShishoCd.TabStop = false;
            this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
            // 
            // lblMizuageShishoNm
            // 
            this.lblMizuageShishoNm.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMizuageShishoNm.Location = new System.Drawing.Point(149, 33);
            this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
            this.lblMizuageShishoNm.Size = new System.Drawing.Size(212, 20);
            this.lblMizuageShishoNm.TabIndex = 2;
            this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMizuageShisho
            // 
            this.lblMizuageShisho.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShisho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShisho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblMizuageShisho.Location = new System.Drawing.Point(10, 31);
            this.lblMizuageShisho.Name = "lblMizuageShisho";
            this.lblMizuageShisho.Size = new System.Drawing.Size(354, 25);
            this.lblMizuageShisho.TabIndex = 0;
            this.lblMizuageShisho.Text = "支所";
            this.lblMizuageShisho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // HAND2111
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 638);
            this.Controls.Add(this.gbxMizuageShisho);
            this.Controls.Add(this.grpSakuseiKubun);
            this.Controls.Add(this.lblFloppy);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.gbxDate);
            this.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "HAND2111";
            this.Text = "信用連帯";
            this.Controls.SetChildIndex(this.gbxDate, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.Controls.SetChildIndex(this.lblFloppy, 0);
            this.Controls.SetChildIndex(this.grpSakuseiKubun, 0);
            this.Controls.SetChildIndex(this.gbxMizuageShisho, 0);
            this.pnlDebug.ResumeLayout(false);
            this.gbxDate.ResumeLayout(false);
            this.gbxDate.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.grpSakuseiKubun.ResumeLayout(false);
            this.grpSakuseiKubun.PerformLayout();
            this.gbxMizuageShisho.ResumeLayout(false);
            this.gbxMizuageShisho.PerformLayout();
            this.ResumeLayout(false);

        }
        #endregion

        private jp.co.fsi.common.controls.FsiTextBox txtMonthFr;
        private jp.co.fsi.common.controls.FsiTextBox txtDayFr;
        private System.Windows.Forms.Label labelDateYearFr;
        private System.Windows.Forms.Label lblDateMonthFr;
        private System.Windows.Forms.Label lblDateDayFr;
        private System.Windows.Forms.GroupBox gbxDate;
        private System.Windows.Forms.Label lblDateDayTo;
        private System.Windows.Forms.Label lblDateMonthTo;
        private System.Windows.Forms.Label labelDateYearTo;
        private common.controls.FsiTextBox txtDayTo;
        private common.controls.FsiTextBox txtYearTo;
        private common.controls.FsiTextBox txtMonthTo;
        private System.Windows.Forms.Label lblDateBet;
        private System.Windows.Forms.GroupBox groupBox1;
        private common.controls.FsiTextBox txtYearFr;
        private common.controls.FsiTextBox txtTekiyo;
        private System.Windows.Forms.Label lblTekiyoCap;
        private System.Windows.Forms.Label lblFurikomibiCap;
        private System.Windows.Forms.Label lblTekiyoNote;
        private System.Windows.Forms.Label lblFurikomibiDay;
        private System.Windows.Forms.Label lblFurikomibiMonth;
        private System.Windows.Forms.Label lblFurikomibiYear;
        private common.controls.FsiTextBox txtDay;
        private common.controls.FsiTextBox txtYear;
        private common.controls.FsiTextBox txtMonth;
        private System.Windows.Forms.Label lblFurikomibiNote;
        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.Label lblFloppy;
        private System.Windows.Forms.GroupBox grpSakuseiKubun;
        private System.Windows.Forms.RadioButton rdoHonninTsumitate;
        private System.Windows.Forms.RadioButton rdoFutsu;
        private System.Windows.Forms.RadioButton rdoAzukarikin;
        private System.Windows.Forms.RadioButton rdoTsumitate;
        private System.Windows.Forms.Label lblGengoFr;
        private System.Windows.Forms.Label lblGengoTo;
        private System.Windows.Forms.Label lblGengo;
        private System.Windows.Forms.Label lblDateFr;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox gbxMizuageShisho;
        private common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private System.Windows.Forms.Label lblMizuageShisho;
    }
}
