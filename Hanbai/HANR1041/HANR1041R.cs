﻿using System.Data;

using jp.co.fsi.common.report;

namespace jp.co.fsi.han.hanr1041
{
    /// <summary>
    /// HANR1041R の概要の説明です。
    /// </summary>
    public partial class HANR1041R : BaseReport
    {

        public HANR1041R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }
    }
}
