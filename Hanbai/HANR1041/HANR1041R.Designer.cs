﻿namespace jp.co.fsi.han.hanr1041
{
    /// <summary>
    /// HANR1041R の概要の説明です。
    /// </summary>
    partial class HANR1041R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(HANR1041R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.ラベル0 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.セリ日 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.直線55 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.ラベル37 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.date = new GrapeCity.ActiveReports.SectionReportModel.ReportInfo();
            this.page = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.伝票番号 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.山NO = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.船主CD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.船主名称 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.漁法CD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.漁法名称 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.魚種CD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.魚種名称 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.水揚数量Kｇ = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.単価 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.仲買人CD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.仲買人名称 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.groupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.groupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.groupHeader2 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.groupFooter2 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.ラベル50 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.テキスト56 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト32 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル34 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.直線48 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.セリ日)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.date)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.page)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.伝票番号)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.山NO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.船主CD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.船主名称)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.漁法CD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.漁法名称)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.魚種CD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.魚種名称)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.水揚数量Kｇ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.単価)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.仲買人CD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.仲買人名称)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト56)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.ラベル0,
            this.セリ日,
            this.ラベル1,
            this.ラベル3,
            this.ラベル4,
            this.ラベル5,
            this.ラベル6,
            this.ラベル7,
            this.ラベル8,
            this.ラベル9,
            this.ラベル11,
            this.ラベル13,
            this.ラベル14,
            this.ラベル16,
            this.ラベル17,
            this.直線55,
            this.ラベル37,
            this.date,
            this.page,
            this.label2,
            this.label3});
            this.pageHeader.Height = 0.9055118F;
            this.pageHeader.Name = "pageHeader";
            // 
            // ラベル0
            // 
            this.ラベル0.Height = 0.2291667F;
            this.ラベル0.HyperLink = null;
            this.ラベル0.Left = 4.770866F;
            this.ラベル0.Name = "ラベル0";
            this.ラベル0.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 15.75pt; font-weight: bold; text-ali" +
    "gn: left; ddo-char-set: 128";
            this.ラベル0.Tag = "";
            this.ラベル0.Text = "仕切チェックリスト";
            this.ラベル0.Top = 0F;
            this.ラベル0.Width = 2.1563F;
            // 
            // セリ日
            // 
            this.セリ日.DataField = "ITEM01";
            this.セリ日.Height = 0.157874F;
            this.セリ日.Left = 0.5452756F;
            this.セリ日.MultiLine = false;
            this.セリ日.Name = "セリ日";
            this.セリ日.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: bold; text-align: left; ddo-char-set: 128";
            this.セリ日.Tag = "";
            this.セリ日.Text = "ITEM01";
            this.セリ日.Top = 0.2811024F;
            this.セリ日.Width = 1.168329F;
            // 
            // ラベル1
            // 
            this.ラベル1.Height = 0.15625F;
            this.ラベル1.HyperLink = null;
            this.ラベル1.Left = 0F;
            this.ラベル1.Name = "ラベル1";
            this.ラベル1.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-alig" +
    "n: left; ddo-char-set: 128";
            this.ラベル1.Tag = "";
            this.ラベル1.Text = "セリ日";
            this.ラベル1.Top = 0.2826772F;
            this.ラベル1.Width = 0.4350394F;
            // 
            // ラベル3
            // 
            this.ラベル3.Height = 0.15625F;
            this.ラベル3.HyperLink = null;
            this.ラベル3.Left = 0.4326772F;
            this.ラベル3.Name = "ラベル3";
            this.ラベル3.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-alig" +
    "n: left; ddo-char-set: 128";
            this.ラベル3.Tag = "";
            this.ラベル3.Text = "伝票番号";
            this.ラベル3.Top = 0.6311024F;
            this.ラベル3.Width = 0.6562993F;
            // 
            // ラベル4
            // 
            this.ラベル4.Height = 0.15625F;
            this.ラベル4.HyperLink = null;
            this.ラベル4.Left = 1.192476F;
            this.ラベル4.Name = "ラベル4";
            this.ラベル4.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-alig" +
    "n: left; ddo-char-set: 128";
            this.ラベル4.Tag = "";
            this.ラベル4.Text = "山NO";
            this.ラベル4.Top = 0.631086F;
            this.ラベル4.Width = 0.3125F;
            // 
            // ラベル5
            // 
            this.ラベル5.Height = 0.15625F;
            this.ラベル5.HyperLink = null;
            this.ラベル5.Left = 1.767279F;
            this.ラベル5.Name = "ラベル5";
            this.ラベル5.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-alig" +
    "n: left; ddo-char-set: 128";
            this.ラベル5.Tag = "";
            this.ラベル5.Text = "船主CD";
            this.ラベル5.Top = 0.631086F;
            this.ラベル5.Width = 0.4375F;
            // 
            // ラベル6
            // 
            this.ラベル6.Height = 0.15625F;
            this.ラベル6.HyperLink = null;
            this.ラベル6.Left = 2.27914F;
            this.ラベル6.Name = "ラベル6";
            this.ラベル6.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-alig" +
    "n: left; ddo-char-set: 128";
            this.ラベル6.Tag = "";
            this.ラベル6.Text = "船主名称";
            this.ラベル6.Top = 0.6311024F;
            this.ラベル6.Width = 0.4791279F;
            // 
            // ラベル7
            // 
            this.ラベル7.Height = 0.15625F;
            this.ラベル7.HyperLink = null;
            this.ラベル7.Left = 3.46063F;
            this.ラベル7.Name = "ラベル7";
            this.ラベル7.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-alig" +
    "n: left; ddo-char-set: 128";
            this.ラベル7.Tag = "";
            this.ラベル7.Text = "漁法CD";
            this.ラベル7.Top = 0.6311024F;
            this.ラベル7.Width = 0.4375F;
            // 
            // ラベル8
            // 
            this.ラベル8.Height = 0.15625F;
            this.ラベル8.HyperLink = null;
            this.ラベル8.Left = 3.979987F;
            this.ラベル8.Name = "ラベル8";
            this.ラベル8.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-alig" +
    "n: left; ddo-char-set: 128";
            this.ラベル8.Tag = "";
            this.ラベル8.Text = "漁法名称";
            this.ラベル8.Top = 0.631086F;
            this.ラベル8.Width = 0.4582024F;
            // 
            // ラベル9
            // 
            this.ラベル9.Height = 0.15625F;
            this.ラベル9.HyperLink = null;
            this.ラベル9.Left = 5.231372F;
            this.ラベル9.Name = "ラベル9";
            this.ラベル9.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-alig" +
    "n: left; ddo-char-set: 128";
            this.ラベル9.Tag = "";
            this.ラベル9.Text = "魚種CD";
            this.ラベル9.Top = 0.631086F;
            this.ラベル9.Width = 0.4375F;
            // 
            // ラベル11
            // 
            this.ラベル11.Height = 0.15625F;
            this.ラベル11.HyperLink = null;
            this.ラベル11.Left = 5.74665F;
            this.ラベル11.Name = "ラベル11";
            this.ラベル11.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-alig" +
    "n: left; ddo-char-set: 128";
            this.ラベル11.Tag = "";
            this.ラベル11.Text = "魚種名称";
            this.ラベル11.Top = 0.631086F;
            this.ラベル11.Width = 0.5625F;
            // 
            // ラベル13
            // 
            this.ラベル13.Height = 0.15625F;
            this.ラベル13.HyperLink = null;
            this.ラベル13.Left = 7.978347F;
            this.ラベル13.Name = "ラベル13";
            this.ラベル13.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-alig" +
    "n: center; ddo-char-set: 128";
            this.ラベル13.Tag = "";
            this.ラベル13.Text = "水揚数量Kg";
            this.ラベル13.Top = 0.631086F;
            this.ラベル13.Width = 0.6457243F;
            // 
            // ラベル14
            // 
            this.ラベル14.Height = 0.15625F;
            this.ラベル14.HyperLink = null;
            this.ラベル14.Left = 8.975452F;
            this.ラベル14.Name = "ラベル14";
            this.ラベル14.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-alig" +
    "n: left; ddo-char-set: 128";
            this.ラベル14.Tag = "";
            this.ラベル14.Text = "単価";
            this.ラベル14.Top = 0.631086F;
            this.ラベル14.Width = 0.3125F;
            // 
            // ラベル16
            // 
            this.ラベル16.Height = 0.15625F;
            this.ラベル16.HyperLink = null;
            this.ラベル16.Left = 9.370867F;
            this.ラベル16.Name = "ラベル16";
            this.ラベル16.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-alig" +
    "n: right; ddo-char-set: 128";
            this.ラベル16.Tag = "";
            this.ラベル16.Text = "仲買人CD";
            this.ラベル16.Top = 0.6311024F;
            this.ラベル16.Width = 0.603981F;
            // 
            // ラベル17
            // 
            this.ラベル17.Height = 0.15625F;
            this.ラベル17.HyperLink = null;
            this.ラベル17.Left = 10.06807F;
            this.ラベル17.Name = "ラベル17";
            this.ラベル17.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-alig" +
    "n: left; ddo-char-set: 128";
            this.ラベル17.Tag = "";
            this.ラベル17.Text = "仲買人名称";
            this.ラベル17.Top = 0.631086F;
            this.ラベル17.Width = 0.7500448F;
            // 
            // 直線55
            // 
            this.直線55.Height = 0F;
            this.直線55.Left = 0.01023622F;
            this.直線55.LineWeight = 3F;
            this.直線55.Name = "直線55";
            this.直線55.Tag = "";
            this.直線55.Top = 0.7874016F;
            this.直線55.Width = 11.84256F;
            this.直線55.X1 = 0.01023622F;
            this.直線55.X2 = 11.8528F;
            this.直線55.Y1 = 0.7874016F;
            this.直線55.Y2 = 0.7874016F;
            // 
            // ラベル37
            // 
            this.ラベル37.Height = 0.1576389F;
            this.ラベル37.HyperLink = null;
            this.ラベル37.Left = 10.47165F;
            this.ラベル37.Name = "ラベル37";
            this.ラベル37.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.ラベル37.Tag = "";
            this.ラベル37.Text = "作成";
            this.ラベル37.Top = 0.2811187F;
            this.ラベル37.Width = 0.4722222F;
            // 
            // date
            // 
            this.date.FormatString = "{RunDateTime:yyyy/MM/dd}";
            this.date.Height = 0.1562664F;
            this.date.Left = 9.24134F;
            this.date.Name = "date";
            this.date.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt";
            this.date.Top = 0.2826772F;
            this.date.Width = 1.230167F;
            // 
            // page
            // 
            this.page.Height = 0.1562664F;
            this.page.Left = 11.00984F;
            this.page.Name = "page";
            this.page.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt";
            this.page.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.page.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.page.Text = "textBox1";
            this.page.Top = 0.2826772F;
            this.page.Width = 0.2500038F;
            // 
            // label2
            // 
            this.label2.Height = 0.15625F;
            this.label2.HyperLink = null;
            this.label2.Left = 7.117279F;
            this.label2.Name = "label2";
            this.label2.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-alig" +
    "n: left; ddo-char-set: 128";
            this.label2.Tag = "";
            this.label2.Text = "率";
            this.label2.Top = 0.631086F;
            this.label2.Width = 0.1764226F;
            // 
            // label3
            // 
            this.label3.Height = 0.15625F;
            this.label3.HyperLink = null;
            this.label3.Left = 7.503894F;
            this.label3.Name = "label3";
            this.label3.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-alig" +
    "n: left; ddo-char-set: 128";
            this.label3.Tag = "";
            this.label3.Text = "箱代";
            this.label3.Top = 0.631086F;
            this.label3.Width = 0.3500437F;
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.伝票番号,
            this.山NO,
            this.船主CD,
            this.船主名称,
            this.漁法CD,
            this.漁法名称,
            this.魚種CD,
            this.魚種名称,
            this.水揚数量Kｇ,
            this.単価,
            this.仲買人CD,
            this.仲買人名称,
            this.テキスト5,
            this.textBox2,
            this.textBox3});
            this.detail.Height = 0.1984252F;
            this.detail.Name = "detail";
            // 
            // 伝票番号
            // 
            this.伝票番号.DataField = "ITEM02";
            this.伝票番号.Height = 0.15625F;
            this.伝票番号.Left = 0.4350333F;
            this.伝票番号.Name = "伝票番号";
            this.伝票番号.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: right; ddo-char-set: 128";
            this.伝票番号.Tag = "";
            this.伝票番号.Text = "ITEM02";
            this.伝票番号.Top = 0.02125984F;
            this.伝票番号.Width = 0.4729167F;
            // 
            // 山NO
            // 
            this.山NO.DataField = "ITEM03";
            this.山NO.Height = 0.15625F;
            this.山NO.Left = 0.9927489F;
            this.山NO.Name = "山NO";
            this.山NO.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: right; ddo-char-set: 128";
            this.山NO.Tag = "";
            this.山NO.Text = "ITEM03";
            this.山NO.Top = 0.02124348F;
            this.山NO.Width = 0.5145833F;
            // 
            // 船主CD
            // 
            this.船主CD.DataField = "ITEM04";
            this.船主CD.Height = 0.15625F;
            this.船主CD.Left = 1.640468F;
            this.船主CD.Name = "船主CD";
            this.船主CD.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: right; ddo-char-set: 128";
            this.船主CD.Tag = "";
            this.船主CD.Text = "ITEM04";
            this.船主CD.Top = 0.02124348F;
            this.船主CD.Width = 0.5666667F;
            // 
            // 船主名称
            // 
            this.船主名称.DataField = "ITEM05";
            this.船主名称.Height = 0.15625F;
            this.船主名称.Left = 2.281496F;
            this.船主名称.MultiLine = false;
            this.船主名称.Name = "船主名称";
            this.船主名称.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: left; ddo-char-set: 128";
            this.船主名称.Tag = "";
            this.船主名称.Text = "ITEM05";
            this.船主名称.Top = 0.02125984F;
            this.船主名称.Width = 1.066536F;
            // 
            // 漁法CD
            // 
            this.漁法CD.DataField = "ITEM06";
            this.漁法CD.Height = 0.15625F;
            this.漁法CD.Left = 3.383465F;
            this.漁法CD.Name = "漁法CD";
            this.漁法CD.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: right; ddo-char-set: 128";
            this.漁法CD.Tag = "";
            this.漁法CD.Text = "ITEM06";
            this.漁法CD.Top = 0.02125984F;
            this.漁法CD.Width = 0.5145833F;
            // 
            // 漁法名称
            // 
            this.漁法名称.DataField = "ITEM07";
            this.漁法名称.Height = 0.15625F;
            this.漁法名称.Left = 3.974531F;
            this.漁法名称.MultiLine = false;
            this.漁法名称.Name = "漁法名称";
            this.漁法名称.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: left; ddo-char-set: 128";
            this.漁法名称.Tag = "";
            this.漁法名称.Text = "ITEM07";
            this.漁法名称.Top = 0.02125984F;
            this.漁法名称.Width = 1.202241F;
            // 
            // 魚種CD
            // 
            this.魚種CD.DataField = "ITEM08";
            this.魚種CD.Height = 0.15625F;
            this.魚種CD.Left = 5.231496F;
            this.魚種CD.Name = "魚種CD";
            this.魚種CD.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: right; ddo-char-set: 128";
            this.魚種CD.Tag = "";
            this.魚種CD.Text = "ITEM08";
            this.魚種CD.Top = 0.02127621F;
            this.魚種CD.Width = 0.4318895F;
            // 
            // 魚種名称
            // 
            this.魚種名称.DataField = "ITEM09";
            this.魚種名称.Height = 0.15625F;
            this.魚種名称.Left = 5.741163F;
            this.魚種名称.MultiLine = false;
            this.魚種名称.Name = "魚種名称";
            this.魚種名称.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: left; ddo-char-set: 128";
            this.魚種名称.Tag = "";
            this.魚種名称.Text = "ITEM09";
            this.魚種名称.Top = 0.02127621F;
            this.魚種名称.Width = 1.280097F;
            // 
            // 水揚数量Kｇ
            // 
            this.水揚数量Kｇ.DataField = "ITEM11";
            this.水揚数量Kｇ.Height = 0.15625F;
            this.水揚数量Kｇ.Left = 7.916501F;
            this.水揚数量Kｇ.Name = "水揚数量Kｇ";
            this.水揚数量Kｇ.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: right; ddo-char-set: 128";
            this.水揚数量Kｇ.Tag = "";
            this.水揚数量Kｇ.Text = "ITEM11";
            this.水揚数量Kｇ.Top = 0.02127621F;
            this.水揚数量Kｇ.Width = 0.7075154F;
            // 
            // 単価
            // 
            this.単価.DataField = "ITEM12";
            this.単価.Height = 0.15625F;
            this.単価.Left = 8.694882F;
            this.単価.Name = "単価";
            this.単価.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: right; ddo-char-set: 128";
            this.単価.Tag = "";
            this.単価.Text = "ITEM12";
            this.単価.Top = 0.02127621F;
            this.単価.Width = 0.5875826F;
            // 
            // 仲買人CD
            // 
            this.仲買人CD.DataField = "ITEM14";
            this.仲買人CD.Height = 0.15625F;
            this.仲買人CD.Left = 9.543307F;
            this.仲買人CD.Name = "仲買人CD";
            this.仲買人CD.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: right; ddo-char-set: 128";
            this.仲買人CD.Tag = "";
            this.仲買人CD.Text = "ITEM14";
            this.仲買人CD.Top = 0.02125984F;
            this.仲買人CD.Width = 0.4313765F;
            // 
            // 仲買人名称
            // 
            this.仲買人名称.DataField = "ITEM15";
            this.仲買人名称.Height = 0.15625F;
            this.仲買人名称.Left = 10.0682F;
            this.仲買人名称.MultiLine = false;
            this.仲買人名称.Name = "仲買人名称";
            this.仲買人名称.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: left; ddo-char-set: 128";
            this.仲買人名称.Tag = "";
            this.仲買人名称.Text = "ITEM15";
            this.仲買人名称.Top = 0.02127621F;
            this.仲買人名称.Width = 0.9416667F;
            // 
            // テキスト5
            // 
            this.テキスト5.DataField = "ITEM17";
            this.テキスト5.Height = 0.15625F;
            this.テキスト5.Left = 11.32868F;
            this.テキスト5.Name = "テキスト5";
            this.テキスト5.Style = "background-color: White; color: Black; font-family: ＭＳ ゴシック; font-size: 9pt; font" +
    "-weight: normal; text-align: left; ddo-char-set: 128";
            this.テキスト5.Tag = "";
            this.テキスト5.Text = "ITEM17";
            this.テキスト5.Top = 0.02127621F;
            this.テキスト5.Visible = false;
            this.テキスト5.Width = 0.5874999F;
            // 
            // textBox2
            // 
            this.textBox2.DataField = "ITEM16";
            this.textBox2.Height = 0.15625F;
            this.textBox2.Left = 7.117323F;
            this.textBox2.Name = "textBox2";
            this.textBox2.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: right; ddo-char-set: 128";
            this.textBox2.Tag = "";
            this.textBox2.Text = "ITEM16";
            this.textBox2.Top = 0.02125984F;
            this.textBox2.Width = 0.1972449F;
            // 
            // textBox3
            // 
            this.textBox3.DataField = "ITEM10";
            this.textBox3.Height = 0.15625F;
            this.textBox3.Left = 7.440552F;
            this.textBox3.Name = "textBox3";
            this.textBox3.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: right; ddo-char-set: 128";
            this.textBox3.Tag = "";
            this.textBox3.Text = "ITEM10";
            this.textBox3.Top = 0.02125984F;
            this.textBox3.Width = 0.3948826F;
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            // 
            // groupHeader1
            // 
            this.groupHeader1.DataField = "ITEM02";
            this.groupHeader1.Height = 0F;
            this.groupHeader1.Name = "groupHeader1";
            // 
            // groupFooter1
            // 
            this.groupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.label1,
            this.textBox1,
            this.line1});
            this.groupFooter1.Height = 0.3541666F;
            this.groupFooter1.Name = "groupFooter1";
            // 
            // label1
            // 
            this.label1.Height = 0.15625F;
            this.label1.HyperLink = null;
            this.label1.Left = 2.23937F;
            this.label1.Name = "label1";
            this.label1.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.label1.Tag = "";
            this.label1.Text = "*　*　*　小計　*　*　*";
            this.label1.Top = 0.1535433F;
            this.label1.Width = 1.4375F;
            // 
            // textBox1
            // 
            this.textBox1.DataField = "ITEM11";
            this.textBox1.Height = 0.15625F;
            this.textBox1.Left = 7.91693F;
            this.textBox1.Name = "textBox1";
            this.textBox1.OutputFormat = resources.GetString("textBox1.OutputFormat");
            this.textBox1.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: right; ddo-char-set: 128";
            this.textBox1.SummaryGroup = "groupHeader1";
            this.textBox1.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox1.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox1.Tag = "";
            this.textBox1.Text = "[ITEM11]";
            this.textBox1.Top = 0.1535433F;
            this.textBox1.Width = 0.6874018F;
            // 
            // line1
            // 
            this.line1.Height = 0F;
            this.line1.Left = 0.01023622F;
            this.line1.LineWeight = 2F;
            this.line1.Name = "line1";
            this.line1.Tag = "";
            this.line1.Top = 0.3503937F;
            this.line1.Width = 12.59445F;
            this.line1.X1 = 0.01023622F;
            this.line1.X2 = 12.60469F;
            this.line1.Y1 = 0.3503937F;
            this.line1.Y2 = 0.3503937F;
            // 
            // groupHeader2
            // 
            this.groupHeader2.DataField = "ITEM01";
            this.groupHeader2.Height = 0F;
            this.groupHeader2.Name = "groupHeader2";
            // 
            // groupFooter2
            // 
            this.groupFooter2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.ラベル50,
            this.テキスト56,
            this.テキスト32,
            this.ラベル34,
            this.直線48});
            this.groupFooter2.Name = "groupFooter2";
            this.groupFooter2.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
            // 
            // ラベル50
            // 
            this.ラベル50.Height = 0.15625F;
            this.ラベル50.HyperLink = null;
            this.ラベル50.Left = 2.239059F;
            this.ラベル50.Name = "ラベル50";
            this.ラベル50.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.ラベル50.Tag = "";
            this.ラベル50.Text = "*　*　*　合計　*　*　*";
            this.ラベル50.Top = 0.08072916F;
            this.ラベル50.Width = 1.4375F;
            // 
            // テキスト56
            // 
            this.テキスト56.DataField = "ITEM11";
            this.テキスト56.Height = 0.15625F;
            this.テキスト56.Left = 7.91693F;
            this.テキスト56.Name = "テキスト56";
            this.テキスト56.OutputFormat = resources.GetString("テキスト56.OutputFormat");
            this.テキスト56.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: right; ddo-char-set: 128";
            this.テキスト56.SummaryGroup = "groupHeader2";
            this.テキスト56.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.テキスト56.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.テキスト56.Tag = "";
            this.テキスト56.Text = "[ITEM11]";
            this.テキスト56.Top = 0.07795276F;
            this.テキスト56.Width = 0.6874018F;
            // 
            // テキスト32
            // 
            this.テキスト32.DataField = "ITEM02";
            this.テキスト32.Height = 0.15625F;
            this.テキスト32.Left = 5.361418F;
            this.テキスト32.Name = "テキスト32";
            this.テキスト32.OutputFormat = resources.GetString("テキスト32.OutputFormat");
            this.テキスト32.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: right; ddo-char-set: 128";
            this.テキスト32.SummaryFunc = GrapeCity.ActiveReports.SectionReportModel.SummaryFunc.Count;
            this.テキスト32.SummaryGroup = "groupHeader2";
            this.テキスト32.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.テキスト32.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.テキスト32.Tag = "";
            this.テキスト32.Text = "[ITEM02]";
            this.テキスト32.Top = 0.07795276F;
            this.テキスト32.Width = 1.452083F;
            // 
            // ラベル34
            // 
            this.ラベル34.Height = 0.15625F;
            this.ラベル34.HyperLink = null;
            this.ラベル34.Left = 6.818504F;
            this.ラベル34.Name = "ラベル34";
            this.ラベル34.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: normal; text-a" +
    "lign: left; ddo-char-set: 128";
            this.ラベル34.Tag = "";
            this.ラベル34.Text = "件";
            this.ラベル34.Top = 0.06299213F;
            this.ラベル34.Width = 0.1770833F;
            // 
            // 直線48
            // 
            this.直線48.Height = 2.870988E-05F;
            this.直線48.Left = 0.01023622F;
            this.直線48.LineWeight = 2F;
            this.直線48.Name = "直線48";
            this.直線48.Tag = "";
            this.直線48.Top = 0.01299213F;
            this.直線48.Width = 11.86987F;
            this.直線48.X1 = 0.01023622F;
            this.直線48.X2 = 11.88011F;
            this.直線48.Y1 = 0.01299213F;
            this.直線48.Y2 = 0.01302084F;
            // 
            // HANR1041R
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.1968504F;
            this.PageSettings.Margins.Left = 1.181102F;
            this.PageSettings.Margins.Right = 1.181102F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
            this.PageSettings.PaperHeight = 13.89764F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.B4;
            this.PageSettings.PaperWidth = 9.84252F;
            this.PrintWidth = 11.28125F;
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.groupHeader2);
            this.Sections.Add(this.groupHeader1);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.groupFooter1);
            this.Sections.Add(this.groupFooter2);
            this.Sections.Add(this.pageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.ラベル0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.セリ日)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.date)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.page)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.伝票番号)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.山NO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.船主CD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.船主名称)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.漁法CD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.漁法名称)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.魚種CD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.魚種名称)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.水揚数量Kｇ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.単価)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.仲買人CD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.仲買人名称)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト56)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル0;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル1;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル3;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル4;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル5;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル6;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル7;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル8;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル9;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル11;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル13;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル14;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル16;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル17;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線55;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル37;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 伝票番号;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 山NO;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 船主CD;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 船主名称;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 漁法CD;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 漁法名称;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 魚種CD;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 魚種名称;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 水揚数量Kｇ;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 単価;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 仲買人CD;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 仲買人名称;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト5;
        private GrapeCity.ActiveReports.SectionReportModel.ReportInfo date;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox page;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader groupHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter groupFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox1;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader groupHeader2;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter groupFooter2;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル50;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト56;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト32;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル34;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線48;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox セリ日;
        private GrapeCity.ActiveReports.SectionReportModel.Label label2;
        private GrapeCity.ActiveReports.SectionReportModel.Label label3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox3;
    }
}
