﻿namespace jp.co.fsi.han.hanb5011
{
    partial class HANB5011
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtKanaName = new jp.co.fsi.common.controls.FsiTextBox();
            this.gbxMaster = new System.Windows.Forms.GroupBox();
            this.cbxGyoshu = new System.Windows.Forms.CheckBox();
            this.cbxGyoshuBunrui = new System.Windows.Forms.CheckBox();
            this.cbxFune = new System.Windows.Forms.CheckBox();
            this.cbxNakagainin = new System.Windows.Forms.CheckBox();
            this.cbxGyoho = new System.Windows.Forms.CheckBox();
            this.cbxFunanushi = new System.Windows.Forms.CheckBox();
            this.cbxChiku = new System.Windows.Forms.CheckBox();
            this.gbxMizuageShisho = new System.Windows.Forms.GroupBox();
            this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMizuageShishoNm = new System.Windows.Forms.Label();
            this.lblMizuageShisho = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.gbxMaster.SuspendLayout();
            this.gbxMizuageShisho.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Text = "マスタ転送";
            // 
            // btnF1
            // 
            this.btnF1.Text = "F1\r\n\r\n";
            // 
            // btnF2
            // 
            this.btnF2.Text = "F2\r\n\r\n";
            // 
            // btnF3
            // 
            this.btnF3.Text = "F3\r\n\r\n";
            // 
            // btnF4
            // 
            this.btnF4.Text = "F4\r\n\r\n出力";
            // 
            // btnF5
            // 
            this.btnF5.Text = "F5\r\n\r\n";
            // 
            // btnF6
            // 
            this.btnF6.Text = "F6\r\n\r\n";
            // 
            // btnF12
            // 
            this.btnF12.Text = "F12\r\n\r\n";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Size = new System.Drawing.Size(847, 100);
            // 
            // txtKanaName
            // 
            this.txtKanaName.AutoSizeFromLength = false;
            this.txtKanaName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtKanaName.DisplayLength = null;
            this.txtKanaName.Font = new System.Drawing.Font("ＭＳ ゴシック", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKanaName.Location = new System.Drawing.Point(12, 255);
            this.txtKanaName.MaxLength = 30;
            this.txtKanaName.Name = "txtKanaName";
            this.txtKanaName.Size = new System.Drawing.Size(360, 21);
            this.txtKanaName.TabIndex = 902;
            this.txtKanaName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // gbxMaster
            // 
            this.gbxMaster.Controls.Add(this.cbxGyoshu);
            this.gbxMaster.Controls.Add(this.cbxGyoshuBunrui);
            this.gbxMaster.Controls.Add(this.cbxFune);
            this.gbxMaster.Controls.Add(this.cbxNakagainin);
            this.gbxMaster.Controls.Add(this.cbxGyoho);
            this.gbxMaster.Controls.Add(this.cbxFunanushi);
            this.gbxMaster.Controls.Add(this.cbxChiku);
            this.gbxMaster.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.gbxMaster.Location = new System.Drawing.Point(12, 140);
            this.gbxMaster.Name = "gbxMaster";
            this.gbxMaster.Size = new System.Drawing.Size(381, 161);
            this.gbxMaster.TabIndex = 911;
            this.gbxMaster.TabStop = false;
            this.gbxMaster.Text = "マスタ選択";
            // 
            // cbxGyoshu
            // 
            this.cbxGyoshu.AutoSize = true;
            this.cbxGyoshu.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.cbxGyoshu.Location = new System.Drawing.Point(22, 120);
            this.cbxGyoshu.Name = "cbxGyoshu";
            this.cbxGyoshu.Size = new System.Drawing.Size(96, 17);
            this.cbxGyoshu.TabIndex = 912;
            this.cbxGyoshu.Text = "魚種マスタ";
            this.cbxGyoshu.UseVisualStyleBackColor = true;
            // 
            // cbxGyoshuBunrui
            // 
            this.cbxGyoshuBunrui.AutoSize = true;
            this.cbxGyoshuBunrui.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.cbxGyoshuBunrui.Location = new System.Drawing.Point(143, 120);
            this.cbxGyoshuBunrui.Name = "cbxGyoshuBunrui";
            this.cbxGyoshuBunrui.Size = new System.Drawing.Size(124, 17);
            this.cbxGyoshuBunrui.TabIndex = 911;
            this.cbxGyoshuBunrui.Text = "漁法分類マスタ";
            this.cbxGyoshuBunrui.UseVisualStyleBackColor = true;
            // 
            // cbxFune
            // 
            this.cbxFune.AutoSize = true;
            this.cbxFune.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.cbxFune.Location = new System.Drawing.Point(22, 70);
            this.cbxFune.Name = "cbxFune";
            this.cbxFune.Size = new System.Drawing.Size(96, 17);
            this.cbxFune.TabIndex = 910;
            this.cbxFune.Text = "船名マスタ";
            this.cbxFune.UseVisualStyleBackColor = true;
            // 
            // cbxNakagainin
            // 
            this.cbxNakagainin.AutoSize = true;
            this.cbxNakagainin.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.cbxNakagainin.Location = new System.Drawing.Point(143, 27);
            this.cbxNakagainin.Name = "cbxNakagainin";
            this.cbxNakagainin.Size = new System.Drawing.Size(110, 17);
            this.cbxNakagainin.TabIndex = 908;
            this.cbxNakagainin.Text = "仲買人マスタ";
            this.cbxNakagainin.UseVisualStyleBackColor = true;
            // 
            // cbxGyoho
            // 
            this.cbxGyoho.AutoSize = true;
            this.cbxGyoho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.cbxGyoho.Location = new System.Drawing.Point(143, 70);
            this.cbxGyoho.Name = "cbxGyoho";
            this.cbxGyoho.Size = new System.Drawing.Size(96, 17);
            this.cbxGyoho.TabIndex = 909;
            this.cbxGyoho.Text = "漁法マスタ";
            this.cbxGyoho.UseVisualStyleBackColor = true;
            // 
            // cbxFunanushi
            // 
            this.cbxFunanushi.AutoSize = true;
            this.cbxFunanushi.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.cbxFunanushi.Location = new System.Drawing.Point(22, 27);
            this.cbxFunanushi.Name = "cbxFunanushi";
            this.cbxFunanushi.Size = new System.Drawing.Size(96, 17);
            this.cbxFunanushi.TabIndex = 906;
            this.cbxFunanushi.Text = "船主マスタ";
            this.cbxFunanushi.UseVisualStyleBackColor = true;
            // 
            // cbxChiku
            // 
            this.cbxChiku.AutoSize = true;
            this.cbxChiku.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.cbxChiku.Location = new System.Drawing.Point(267, 27);
            this.cbxChiku.Name = "cbxChiku";
            this.cbxChiku.Size = new System.Drawing.Size(96, 17);
            this.cbxChiku.TabIndex = 907;
            this.cbxChiku.Text = "地区マスタ";
            this.cbxChiku.UseVisualStyleBackColor = true;
            // 
            // gbxMizuageShisho
            // 
            this.gbxMizuageShisho.Controls.Add(this.txtMizuageShishoCd);
            this.gbxMizuageShisho.Controls.Add(this.lblMizuageShishoNm);
            this.gbxMizuageShisho.Controls.Add(this.lblMizuageShisho);
            this.gbxMizuageShisho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.gbxMizuageShisho.ForeColor = System.Drawing.Color.Black;
            this.gbxMizuageShisho.Location = new System.Drawing.Point(12, 48);
            this.gbxMizuageShisho.Name = "gbxMizuageShisho";
            this.gbxMizuageShisho.Size = new System.Drawing.Size(381, 77);
            this.gbxMizuageShisho.TabIndex = 912;
            this.gbxMizuageShisho.TabStop = false;
            this.gbxMizuageShisho.Text = "水揚支所";
            // 
            // txtMizuageShishoCd
            // 
            this.txtMizuageShishoCd.AutoSizeFromLength = true;
            this.txtMizuageShishoCd.DisplayLength = null;
            this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtMizuageShishoCd.Location = new System.Drawing.Point(93, 33);
            this.txtMizuageShishoCd.MaxLength = 5;
            this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
            this.txtMizuageShishoCd.Size = new System.Drawing.Size(51, 20);
            this.txtMizuageShishoCd.TabIndex = 1;
            this.txtMizuageShishoCd.TabStop = false;
            this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
            // 
            // lblMizuageShishoNm
            // 
            this.lblMizuageShishoNm.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMizuageShishoNm.Location = new System.Drawing.Point(149, 33);
            this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
            this.lblMizuageShishoNm.Size = new System.Drawing.Size(212, 20);
            this.lblMizuageShishoNm.TabIndex = 2;
            this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMizuageShisho
            // 
            this.lblMizuageShisho.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShisho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShisho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblMizuageShisho.Location = new System.Drawing.Point(10, 31);
            this.lblMizuageShisho.Name = "lblMizuageShisho";
            this.lblMizuageShisho.Size = new System.Drawing.Size(354, 25);
            this.lblMizuageShisho.TabIndex = 0;
            this.lblMizuageShisho.Text = "水揚支所";
            this.lblMizuageShisho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // HANB5011
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 638);
            this.Controls.Add(this.gbxMizuageShisho);
            this.Controls.Add(this.gbxMaster);
            this.Controls.Add(this.txtKanaName);
            this.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "HANB5011";
            this.Text = "";
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.txtKanaName, 0);
            this.Controls.SetChildIndex(this.gbxMaster, 0);
            this.Controls.SetChildIndex(this.gbxMizuageShisho, 0);
            this.pnlDebug.ResumeLayout(false);
            this.gbxMaster.ResumeLayout(false);
            this.gbxMaster.PerformLayout();
            this.gbxMizuageShisho.ResumeLayout(false);
            this.gbxMizuageShisho.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private common.controls.FsiTextBox txtKanaName;
        private System.Windows.Forms.GroupBox gbxMaster;
        private System.Windows.Forms.CheckBox cbxNakagainin;
        private System.Windows.Forms.CheckBox cbxGyoho;
        private System.Windows.Forms.CheckBox cbxFunanushi;
        private System.Windows.Forms.CheckBox cbxChiku;
        private System.Windows.Forms.CheckBox cbxGyoshu;
        private System.Windows.Forms.CheckBox cbxGyoshuBunrui;
        private System.Windows.Forms.CheckBox cbxFune;
        private System.Windows.Forms.GroupBox gbxMizuageShisho;
        private common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private System.Windows.Forms.Label lblMizuageShisho;


    }
}