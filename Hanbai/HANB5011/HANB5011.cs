﻿using System;
using System.IO;
using System.Data;
using System.Text;
using System.Reflection;
using System.Diagnostics;
using System.Windows.Forms;
using System.ComponentModel;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;
using jp.co.fsi.common.constants;

namespace jp.co.fsi.han.hanb5011
{
    /// <summary>
    /// マスタ転送(HANB5011)
    /// </summary>
    public partial class HANB5011 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// 文字コード(SJIS)
        /// </summary>
        private const int CHR_CD_SJIS = 932; // "Shift_JIS"と同じはず
        #endregion

        #region プロパティ
        // WebClientオブジェクト
        System.Net.WebClient wc;
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HANB5011()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region メソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.InitForm();は呼び出さなくて構いません。
        /// また、このメソッド内の処理を外出しでこのクラス内にメソッド化するのは構いませんが、
        /// 原則、独自で起動時のイベント処理を実装することは禁じます。
        /// </remarks>
        protected override void InitForm()
        {
            // 水揚支所
            this.txtMizuageShishoCd.Text = "1";
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_HN_COMBO_DATA_MIZUAGE", "", this.txtMizuageShishoCd.Text);

            // フォーカス設定
            this.gbxMaster.Focus();
            this.gbxMaster.Select();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 水揚支所にフォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd": // 水揚支所
                    String[] result = this.openSearchWindow("COMC8011", "TB_HN_COMBO_DATA_MIZUAGE", this.txtMizuageShishoCd.Text);
                    if (!ValChk.IsEmpty(result[0]))
                    {
                        this.txtMizuageShishoCd.Text = result[0];
                        this.lblMizuageShishoNm.Text = result[1];
                    }
                    break;
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            //ネットワークドライブ割り当て
            bool flg = NetUseStart();

            // マスタ転送処理を実行
            if (flg)
            {
                getMasterData("old");
            }
            // DialogResultとしてCancelを返却する
            //this.DialogResult = DialogResult.Cancel;
            //base.PressEsc();
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            // マスタ転送処理を実行(FTP使用)
            getMasterData("new");
            // 開放する
            wc.Dispose();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 水揚支所入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMizuageShishoCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !this.IsValidMizuageShishoCd())
            {
                e.Cancel = true;
                this.txtMizuageShishoCd.SelectAll();
                this.txtMizuageShishoCd.Focus();
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 水揚支所の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool IsValidMizuageShishoCd()
        {
            // 空 又は 0入力の場合
            if (ValChk.IsEmpty(this.txtMizuageShishoCd.Text) || Equals(this.txtMizuageShishoCd.Text, "0"))
            {
                // 水揚支所名称を表示する
                this.txtMizuageShishoCd.Text = "0";
                this.lblMizuageShishoNm.Text = "全て";
                return true;
            }

            // 水揚支所名称を表示する
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_HN_COMBO_DATA_MIZUAGE", "", this.txtMizuageShishoCd.Text);

            if (ValChk.IsEmpty(this.lblMizuageShishoNm.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 検索サブウィンドウオープン
        /// </summary>
        /// <param name="moduleName">例："COMC8111"</param>
        /// <param name="para1">例："TB_HN_COMBO_DATA_MIZUAGE"</param>
        /// <param name="textBoxOfCode">例：this.txtGyogyoushuCd.Text</param>
        /// <param name="indata">例： this.txtGyogyoushuCd.Text</param>
        /// <returns>String[0]：検索結果から選択したコード , String[1]：検索結果から選択した名称</returns>
        private String[] openSearchWindow(String moduleName, String para1, String indata)
        {
            string[] result = { "", "" };

            // ネームスペースに使うモジュール名の小文字
            string lowerModuleName = moduleName.ToLower();

            // ネームスペースの末尾
            string nameSpace = lowerModuleName.Substring(0, 3);

            // アセンブリのロード
            Assembly asm = Assembly.LoadFrom(moduleName + ".exe");

            // フォーム作成
            string moduleNameSpace = "jp.co.fsi." + nameSpace + "." + lowerModuleName + "." + moduleName;
            Type t = asm.GetType(moduleNameSpace);

            if (t != null)
            {
                Object obj = Activator.CreateInstance(t);
                if (obj != null)
                {
                    BasePgForm frm = (BasePgForm)obj;
                    frm.Par1 = para1;
                    frm.InData = indata;
                    frm.ShowDialog(this);

                    if (frm.DialogResult == DialogResult.OK)
                    {
                        string[] ret = (string[])frm.OutData;
                        result[0] = ret[0];
                        result[1] = ret[1];
                        return result;
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// ネットワークドライブ接続
        /// <summary>
        /// <param name="seribi">セリ日</param>
        /// <returns>webセリ情報テーブル</returns>
        private bool NetUseStart()
        {
            // 変数宣言
            string Url;
            Uri dirPath;

            Url = this.Config.LoadPgConfig(Constants.SubSys.Han, "HANB1081", "Setting", "Start");
            dirPath = new Uri(Url); // 基準となるパス

            // 保存先の存在チェック
            if (!File.Exists(dirPath.LocalPath))
            {
                Msg.Info("ネットワークドライブ割り当てに失敗しました");
                return false;
            }
            else
            {
                ProcessStartInfo psInfo = new ProcessStartInfo();
                psInfo.FileName = Util.ToString(dirPath.LocalPath); // 実行するファイル
                psInfo.CreateNoWindow = true; // コンソール・ウィンドウを開かない
                psInfo.UseShellExecute = false; // シェル機能を使用しない

                Process.Start(psInfo);
                return true;
            }
        }

        /// <summary>
        /// ネットワークドライブ切断
        /// <summary>
        /// <param name="seribi">セリ日</param>
        /// <returns>webセリ情報テーブル</returns>
        private void NetUseDelete()
        {
            // 変数宣言
            string Url;
            Uri dirPath;

            Url = this.Config.LoadPgConfig(Constants.SubSys.Han, "HANB1081", "Setting", "Delete");
            dirPath = new Uri(Url); // 基準となるパス

            // 保存先の存在チェック
            if (!File.Exists(dirPath.LocalPath))
            {
                Msg.Info("ネットワークドライブ切断に失敗しました");
            }
            else
            {
                ProcessStartInfo psInfo = new ProcessStartInfo();
                psInfo.FileName = Util.ToString(dirPath.LocalPath); // 実行するファイル
                psInfo.CreateNoWindow = true; // コンソール・ウィンドウを開かない
                psInfo.UseShellExecute = false; // シェル機能を使用しない

                Process.Start(psInfo);
            }
        }

        /// <summary>
        /// マスタ転送処理を実装
        /// </summary>
        private void getMasterData(string type)
        {
            string dirPath = "";
            if (type == "old")
            {
                // ディレクトリは、送信パステーブル（TB_HN_SOSHIN_PATH）を参照する
                dirPath = this.getSendFilePath("9999");
                if (ValChk.IsEmpty(dirPath))
                {
                    Msg.Error("送信データの保存先を取得できませんでした。");
                    return;
                }
                if (!System.IO.Directory.Exists(dirPath))
                {
                    Msg.Error("送信データの保存先フォルダが見つかりません。");
                    return;
                }
            }
            else if(type == "new")
            {
                wc = new System.Net.WebClient();
                //ログインユーザー名とパスワードを指定
                wc.Credentials = new System.Net.NetworkCredential("jf-nago-jp", "c4EqXtWa");
            }

            try
            {
                bool flg = false;
                // 船主マスタデータのファイル出力
                if (this.cbxFunanushi.Checked)
                {
                    this.getMasterFunanushi(dirPath, type);
                    flg = true;
                }
                // 仲買人マスタデータのファイル出力
                if (this.cbxNakagainin.Checked)
                {
                    this.getMasterNakagainin(dirPath, type);
                    flg = true;
                }

                // 地区マスタデータのファイル出力
                if (this.cbxChiku.Checked)
                {
                    this.getMasterChiku(dirPath, type);
                    flg = true;
                }
                // 船名マスタデータのファイル出力
                if (this.cbxFune.Checked)
                {
                    this.getMasterFune(dirPath, type);
                    flg = true;
                }
                // 漁法マスタデータのファイル出力
                if (this.cbxGyoho.Checked)
                {
                    this.getMasterGyoho(dirPath, type);
                    flg = true;
                }
                // 魚種マスタデータのファイル出力
                if (this.cbxGyoshu.Checked)
                {
                    this.getMasterGyoshu(dirPath, type);
                    flg = true;
                }
                // 魚種分類マスタデータのファイル出力
                if (this.cbxGyoshuBunrui.Checked)
                {
                    this.getMasterGyoshuBunrui(dirPath, type);
                    flg = true;
                }

                // マスタ出力が1件以上の場合
                if (flg)
                {
                    Msg.Info("データの書き込み処理を終了しました。");
                }
            }
            catch (Exception e)
            {
                Msg.Notice(e.Message);
                return;
            }
            finally
            {
                // ネットワークドライブ切断
                NetUseDelete();
            }
        }
        
        /// <summary>
        /// 船主マスタデータ取得
        /// </summary>
        private void getMasterFunanushi(string dirPath, string type)
        {
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder Sql = new StringBuilder();

            // 船主マスタデータを取得
            Sql.Append("SELECT");
            Sql.Append(" A.KAISHA_CD AS KAISHA_CD,");
            Sql.Append(" @TORIHIKISAKI_KUBUN1 AS TORIHIKISAKI_KBN,");
            Sql.Append(" A.TORIHIKISAKI_CD AS TORIHIKISAKI_CD,");
            Sql.Append(" A.TORIHIKISAKI_NM AS TORIHIKISAKI_NM,");
            Sql.Append(" A.TORIHIKISAKI_KANA_NM AS TORIHIKISAKI_KANA_NM,");
            Sql.Append(" A.RYAKUSHO AS RYAKUSHO,");
            Sql.Append(" A.FUNE_NM_CD AS FUNE_NM_CD,");
            Sql.Append(" A.GYOHO_CD AS GYOHO_CD,");
            Sql.Append(" A.CHIKU_CD AS CHIKU_CD,");
            Sql.Append(" @CLIENT_FLG AS CLIENT_FLG ");
            Sql.Append("FROM");
            Sql.Append(" TB_CM_TORIHIKISAKI AS A ");
            Sql.Append("LEFT OUTER JOIN");
            Sql.Append(" TB_HN_TORIHIKISAKI_JOHO AS B ");
            Sql.Append("ON");
            Sql.Append(" A.KAISHA_CD = B.KAISHA_CD AND A.TORIHIKISAKI_CD = B.TORIHIKISAKI_CD ");
            Sql.Append("WHERE");
            Sql.Append(" B.TORIHIKISAKI_KUBUN1 = @TORIHIKISAKI_KUBUN1");

            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@TORIHIKISAKI_KUBUN1", SqlDbType.Decimal, 4, 1);
            dpc.SetParam("@CLIENT_FLG", SqlDbType.Decimal, 1, 0);

            DataTable dt_FUNANUSHI = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            // 船主マスタデータが存在する場合、txtファイルを作成する
            if (dt_FUNANUSHI.Rows.Count > 0)
            {
                // テキストファイル出力処理
                setMasterClient(dirPath, dt_FUNANUSHI, "FUNANUSHI.txt");

                if (type == "new")
                {
                    //FTPサーバーにアップロード
                    wc.UploadFile("ftp://wx21.wadax.ne.jp/data/master/FUNANUSHI.txt", "FUNANUSHI.txt");

                    System.IO.File.Delete("FUNANUSHI.txt");
                }
            }
        }

        /// <summary>
        /// 仲買人マスタデータ取得
        /// </summary>
        private void getMasterNakagainin(string dirPath, string type)
        {
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder Sql = new StringBuilder();

            // 仲買人マスタデータを取得
            Sql.Append("SELECT");
            Sql.Append(" A.KAISHA_CD AS KAISHA_CD,");
            Sql.Append(" @TORIHIKISAKI_KUBUN2 AS TORIHIKISAKI_KBN,");
            Sql.Append(" A.TORIHIKISAKI_CD AS TORIHIKISAKI_CD,");
            Sql.Append(" A.TORIHIKISAKI_NM AS TORIHIKISAKI_NM,");
            Sql.Append(" A.TORIHIKISAKI_KANA_NM AS TORIHIKISAKI_KANA_NM,");
            Sql.Append(" A.RYAKUSHO AS RYAKUSHO,");
            Sql.Append(" A.FUNE_NM_CD AS FUNE_NM_CD,");
            Sql.Append(" A.GYOHO_CD AS GYOHO_CD,");
            Sql.Append(" A.CHIKU_CD AS CHIKU_CD,");
            Sql.Append(" @CLIENT_FLG AS CLIENT_FLG ");
            Sql.Append("FROM");
            Sql.Append(" TB_CM_TORIHIKISAKI AS A ");
            Sql.Append("LEFT OUTER JOIN");
            Sql.Append(" TB_HN_TORIHIKISAKI_JOHO AS B ");
            Sql.Append("ON");
            Sql.Append(" A.KAISHA_CD = B.KAISHA_CD AND A.TORIHIKISAKI_CD = B.TORIHIKISAKI_CD ");
            Sql.Append("WHERE");
            Sql.Append(" B.TORIHIKISAKI_KUBUN2 = @TORIHIKISAKI_KUBUN2");

            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@TORIHIKISAKI_KUBUN2", SqlDbType.Decimal, 4, 2);
            dpc.SetParam("@CLIENT_FLG", SqlDbType.Decimal, 1, 0);

            DataTable dt_NAKAGAININ = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            // 仲買人マスタデータが存在する場合、txtファイルを作成する
            if (dt_NAKAGAININ.Rows.Count > 0)
            {
                // テキストファイル出力処理
                setMasterClient(dirPath, dt_NAKAGAININ, "NAKAGAININ.txt");

                if (type == "new")
                {
                    //FTPサーバーにアップロード
                    wc.UploadFile("ftp://wx21.wadax.ne.jp/data/master/NAKAGAININ.txt", "NAKAGAININ.txt");

                    System.IO.File.Delete("NAKAGAININ.txt");
                }
            }
        }

        /// <summary>
        /// 地区マスタデータ取得
        /// </summary>
        private void getMasterChiku(string dirPath, string type)
        {
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder Sql = new StringBuilder();

            // 地区マスタデータを取得
            Sql.Append("SELECT");
            Sql.Append(" KAISHA_CD AS KAISHA_CD,");
            Sql.Append(" @NM_KBN AS NM_KBN,");
            Sql.Append(" CHIKU_CD AS NM_CD,");
            Sql.Append(" CHIKU_NM AS CD_NM,");
            Sql.Append(" '' AS CD_KANA_NM,");
            Sql.Append(" '' AS CD_CNM,");
            Sql.Append(" '' AS CD_FIL_1,");
            Sql.Append(" '' AS CD_FIL_2,");
            Sql.Append(" @CD_FLG AS CD_FLG ");
            Sql.Append("FROM");
            Sql.Append(" TM_HN_CHIKU_MST ");
            Sql.Append("WHERE");
            Sql.Append(" KAISHA_CD = @KAISHA_CD");

            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@NM_KBN", SqlDbType.Decimal, 1, 2);
            dpc.SetParam("@CD_FLG", SqlDbType.Decimal, 1, 0);

            DataTable dt_CHIKU = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            // 地区マスタデータが存在する場合、txtファイルを作成する
            if (dt_CHIKU.Rows.Count > 0)
            {
                // テキストファイル出力処理
                setMasterNameCds(dirPath, dt_CHIKU, "CHIKU.txt");

                if (type == "new")
                {
                    //FTPサーバーにアップロード
                    wc.UploadFile("ftp://wx21.wadax.ne.jp/data/master/CHIKU.txt", "CHIKU.txt");

                    System.IO.File.Delete("CHIKU.txt");
                }
            }
        }

        /// <summary>
        /// 船名マスタデータ取得
        /// </summary>
        private void getMasterFune(string dirPath, string type)
        {
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder Sql = new StringBuilder();

            // 船名マスタデータを取得
            Sql.Append("SELECT");
            Sql.Append(" KAISHA_CD AS KAISHA_CD,");
            Sql.Append(" @NM_KBN AS NM_KBN,");
            Sql.Append(" FUNE_NM_CD AS NM_CD,");
            Sql.Append(" FUNE_NM AS CD_NM,");
            Sql.Append(" FUNE_KANA_NM AS CD_KANA_NM,");
            Sql.Append(" '' AS CD_CNM,");
            Sql.Append(" '' AS CD_FIL_1,");
            Sql.Append(" '' AS CD_FIL_2,");
            Sql.Append(" @CD_FLG AS CD_FLG ");
            Sql.Append("FROM");
            Sql.Append(" TM_HN_FUNE_NM_MST ");
            Sql.Append("WHERE");
            Sql.Append(" KAISHA_CD = @KAISHA_CD AND SHORI_FLG <> 2");

            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@NM_KBN", SqlDbType.Decimal, 1, 1);
            dpc.SetParam("@CD_FLG", SqlDbType.Decimal, 1, 0);

            DataTable dt_FUNE = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            // 船名マスタデータが存在する場合、txtファイルを作成する
            if (dt_FUNE.Rows.Count > 0)
            {
                // テキストファイル出力処理
                setMasterNameCds(dirPath, dt_FUNE, "FUNE.txt");

                if (type == "new")
                {
                    //FTPサーバーにアップロード
                    wc.UploadFile("ftp://wx21.wadax.ne.jp/data/master/FUNE.txt", "FUNE.txt");

                    System.IO.File.Delete("FUNE.txt");
                }
            }
        }

        /// <summary>
        /// 漁法マスタデータ取得
        /// </summary>
        private void getMasterGyoho(string dirPath, string type)
        {
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder Sql = new StringBuilder();

            // 漁法マスタデータを取得
            Sql.Append("SELECT");
            Sql.Append(" KAISHA_CD AS KAISHA_CD,");
            Sql.Append(" @NM_KBN AS NM_KBN,");
            Sql.Append(" GYOHO_CD AS NM_CD,");
            Sql.Append(" GYOHO_NM AS CD_NM,");
            Sql.Append(" GYOHO_KANA AS CD_KANA_NM,");
            Sql.Append(" '' AS CD_CNM,");
            Sql.Append(" '' AS CD_FIL_1,");
            Sql.Append(" '' AS CD_FIL_2,");
            Sql.Append(" @CD_FLG AS CD_FLG ");
            Sql.Append("FROM");
            Sql.Append(" TM_HN_GYOHO_MST ");
            Sql.Append("WHERE");
            Sql.Append(" KAISHA_CD = @KAISHA_CD AND SHORI_FLG <> 2");

            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@NM_KBN", SqlDbType.Decimal, 1, 3);
            dpc.SetParam("@CD_FLG", SqlDbType.Decimal, 1, 0);

            DataTable dt_GYOHO = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            // 漁法マスタデータが存在する場合、txtファイルを作成する
            if (dt_GYOHO.Rows.Count > 0)
            {
                // テキストファイル出力処理
                setMasterNameCds(dirPath, dt_GYOHO, "GYOHO.txt");

                if (type == "new")
                {
                    //FTPサーバーにアップロード
                    wc.UploadFile("ftp://wx21.wadax.ne.jp/data/master/GYOHO.txt", "GYOHO.txt");

                    System.IO.File.Delete("GYOHO.txt");
                }
            }
        }

        /// <summary>
        /// 魚種マスタデータ取得
        /// </summary>
        private void getMasterGyoshu(string dirPath, string type)
        {
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder Sql = new StringBuilder();

            // 魚種マスタデータを取得
            Sql.Append("SELECT");
            Sql.Append(" KAISHA_CD AS KAISHA_CD,");
            Sql.Append(" SHOHIN_CD AS GYOSHU_CD,");
            Sql.Append(" SHOHIN_NM AS GYOSHU_NM,");
            Sql.Append(" SHOHIN_KANA_NM AS GYOSHU_KANA_NM,");
            Sql.Append(" SHOHIN_KUBUN1 AS GYOSHU_BUNRUI_CD, ");
            Sql.Append(" 0 AS CD_FLG ");
            
            Sql.Append("FROM");
            Sql.Append(" TB_HN_SHOHIN ");
            Sql.Append("WHERE");
            Sql.Append(" KAISHA_CD = @KAISHA_CD AND");
            Sql.Append(" BARCODE1 = @BARCODE1");

            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@BARCODE1", SqlDbType.Decimal, 3, 999);

            DataTable dt_GYOSHU = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            // 魚種マスタデータが存在する場合、txtファイルを作成する
            if (dt_GYOSHU.Rows.Count > 0)
            {
                // テキストファイル出力処理
                setMasterGyoshu(dirPath, dt_GYOSHU, "GYOSHU.txt");

                if (type == "new")
                {
                    //FTPサーバーにアップロード
                    wc.UploadFile("ftp://wx21.wadax.ne.jp/data/master/GYOSHU.txt", "GYOSHU.txt");

                    System.IO.File.Delete("GYOSHU.txt");
                }
            }
        }

        /// <summary>
        /// 魚種分類マスタデータ取得
        /// </summary>
        private void getMasterGyoshuBunrui(string dirPath, string type)
        {
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder Sql = new StringBuilder();

            // 魚種分類マスタデータを取得
            Sql.Append("SELECT");
            Sql.Append(" KAISHA_CD AS KAISHA_CD,");
            Sql.Append(" @NM_KBN AS NM_KBN,");
            Sql.Append(" GYOSHU_BUNRUI_CD AS NM_CD,");
            Sql.Append(" GYOSHU_BUNRUI_NM AS CD_NM,");
            Sql.Append(" GYOSHU_BUNRUI_KANA AS CD_KANA_NM,");
            Sql.Append(" '' AS CD_CNM,");
            Sql.Append(" '' AS CD_FIL_1,");
            Sql.Append(" '' AS CD_FIL_2,");
            Sql.Append(" @CD_FLG AS CD_FLG ");
            Sql.Append("FROM");
            Sql.Append(" TM_HN_GYOSHU_BUNRUI_MST ");
            Sql.Append("WHERE");
            Sql.Append(" KAISHA_CD = @KAISHA_CD AND SHORI_FLG <> 2");

            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@NM_KBN", SqlDbType.Decimal, 1, 4);
            dpc.SetParam("@CD_FLG", SqlDbType.Decimal, 1, 0);

            DataTable dt_GYOSHUBUNRUI = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            // 魚種分類マスタデータが存在する場合、txtファイルを作成する
            if (dt_GYOSHUBUNRUI.Rows.Count > 0)
            {
                // テキストファイル出力処理
                setMasterNameCds(dirPath, dt_GYOSHUBUNRUI, "GYOSHUBUNRUI.txt");

                if (type == "new")
                {
                    //FTPサーバーにアップロード
                    wc.UploadFile("ftp://wx21.wadax.ne.jp/data/master/GYOSHUBUNRUI.txt", "GYOSHUBUNRUI.txt");

                    System.IO.File.Delete("GYOSHUBUNRUI.txt");
                }
            }
        }

        /// <summary>
        /// 船主/仲買にマスタデータのtxtファイル作成処理
        /// </summary>
        private void setMasterClient(string dirPath, DataTable dt_CLIENT, string txtNm)
        {
            // --- これ以降は、テキストファイル出力処理 ---
            //CSVファイルに書き込むときに使うEncoding
            System.Text.Encoding enc =
                System.Text.Encoding.GetEncoding(CHR_CD_SJIS);

            // ファイルのストリーム宣言
            System.IO.StreamWriter sr;
            try
            {
                sr = new System.IO.StreamWriter(dirPath + txtNm, false, enc);
            }
            catch (Exception e)
            {
                Msg.Notice(e.Message);
                return;
            }

            try
            {
                //レコードを書き込む
                foreach (DataRow row in dt_CLIENT.Rows)
                {
                    sr.Write(Util.ToString(row["KAISHA_CD"]) + '\t'); // 会社CD
                    sr.Write(Util.ToString(row["TORIHIKISAKI_KBN"]) + '\t'); // 取引先区分
                    sr.Write(Util.ToString(row["TORIHIKISAKI_CD"]) + '\t'); // 取引先CD
                    sr.Write(Util.ToString(row["TORIHIKISAKI_NM"]) + '\t'); // 取引先名
                    sr.Write(Util.ToString(row["TORIHIKISAKI_KANA_NM"]) + '\t'); // 取引先カナ名
                    sr.Write(Util.ToString(row["RYAKUSHO"]) + '\t'); // 略称
                    sr.Write(Util.ToString(row["FUNE_NM_CD"]) + '\t'); // 船名CD
                    sr.Write(Util.ToString(row["GYOHO_CD"]) + '\t'); // 漁法CD
                    sr.Write(Util.ToString(row["CHIKU_CD"]) + '\t'); // 地区CD
                    sr.Write(Util.ToString(row["CLIENT_FLG"])); // 処理フラグ

                    //改行する
                    sr.Write("\r\n");
                }
            }
            catch (Exception e)
            {
                Msg.Notice(e.Message);
                return;
            }
            finally
            {
                // ファイルのストリームを閉じる
                sr.Close();
            }
        }

        /// <summary>
        /// 地区/船名/漁法/魚種分類にマスタデータのtxtファイル作成処理
        /// </summary>
        private void setMasterNameCds(string dirPath, DataTable dt_NAME_CSD, string txtNm)
        {
            // --- これ以降は、テキストファイル出力処理 ---
            //CSVファイルに書き込むときに使うEncoding
            System.Text.Encoding enc =
                System.Text.Encoding.GetEncoding(CHR_CD_SJIS);

            // ファイルのストリーム宣言
            System.IO.StreamWriter sr;
            try
            {
                sr = new System.IO.StreamWriter(dirPath + txtNm, false, enc);
            }
            catch (Exception e)
            {
                Msg.Notice(e.Message);
                return;
            }

            try
            {
                //レコードを書き込む
                foreach (DataRow row in dt_NAME_CSD.Rows)
                {
                    sr.Write(Util.ToString(row["KAISHA_CD"]) + '\t'); // 会社CD
                    sr.Write(Util.ToString(row["NM_KBN"]) + '\t'); // 名称区分
                    sr.Write(Util.ToString(row["NM_CD"]) + '\t'); // 名称CD
                    sr.Write(Util.ToString(row["CD_NM"]) + '\t'); // 名称
                    sr.Write(Util.ToString(row["CD_KANA_NM"]) + '\t'); // 名称カナ
                    sr.Write(Util.ToString(row["CD_CNM"]) + '\t'); // 名称略称
                    sr.Write(Util.ToString(row["CD_FIL_1"]) + '\t'); // 付加項目1
                    sr.Write(Util.ToString(row["CD_FIL_2"]) + '\t'); //付加項目2
                    sr.Write(Util.ToString(row["CD_FLG"])); // 処理フラグ

                    //改行する
                    sr.Write("\r\n");
                }
            }
            catch (Exception e)
            {
                Msg.Notice(e.Message);
                return;
            }
            finally
            {
                // ファイルのストリームを閉じる
                sr.Close();
            }
        }

        /// <summary>
        /// 地区/船名/漁法/魚種/魚種分類にマスタデータのtxtファイル作成処理
        /// </summary>
        private void setMasterGyoshu(string dirPath, DataTable dt_GYOSHU, string txtNm)
        {
            // --- これ以降は、テキストファイル出力処理 ---
            //CSVファイルに書き込むときに使うEncoding
            System.Text.Encoding enc =
                System.Text.Encoding.GetEncoding(CHR_CD_SJIS);

            // ファイルのストリーム宣言
            System.IO.StreamWriter sr;
            try
            {
                sr = new System.IO.StreamWriter(dirPath + txtNm, false, enc);
            }
            catch (Exception e)
            {
                Msg.Notice(e.Message);
                return;
            }

            try
            {
                //レコードを書き込む
                foreach (DataRow row in dt_GYOSHU.Rows)
                {
                    sr.Write(Util.ToString(row["KAISHA_CD"]) + '\t'); // 会社CD
                    sr.Write(Util.ToString(row["GYOSHU_CD"]) + '\t'); // 魚種CD
                    sr.Write(Util.ToString(row["GYOSHU_NM"]) + '\t'); // 魚種名
                    sr.Write(Util.ToString(row["GYOSHU_KANA_NM"]) + '\t'); // 魚種カナ
                    sr.Write(Util.ToString(row["GYOSHU_BUNRUI_CD"]) + '\t'); // 魚種分類CD
                    sr.Write(Util.ToString(row["CD_FLG"])); // 処理フラグ

                    //改行する
                    sr.Write("\r\n");
                }
            }
            catch (Exception e)
            {
                Msg.Notice(e.Message);
                return;
            }
            finally
            {
                // ファイルのストリームを閉じる
                sr.Close();
            }
        }

        /// <summary>
        /// 送信ファイルの保存先ディレクトリ取得
        /// </summary>
        /// <param name="chikuCd">地区コード</param>
        /// <returns>string 保存先のディレクトリ</returns>
        private string getSendFilePath(string chikuCd)
        {
            string dir = string.Empty;

            DbParamCollection dpc = new DbParamCollection();

            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);

            // 検索する地区コードをセット
            dpc.SetParam("@CHIKU_CD", SqlDbType.Decimal, 4, chikuCd);

            string cols = "PATH";

            string from = "TB_HN_SOSHIN_PATH AS A";

            string where = "A.KAISHA_CD = @KAISHA_CD AND";
            where += "  A.CHIKU_CD = @CHIKU_CD";

            DataTable dtPath;
            try
            {
                // 保存先取得
                dtPath = this.Dba.GetDataTableByConditionWithParams(cols, from, where, dpc);
            }
            catch (Exception e)
            {
                Msg.Notice(e.Message);
                return dir;
            }

            if (dtPath.Rows.Count == 0 || ValChk.IsEmpty(dtPath.Rows[0]["PATH"]))
            {
                Msg.Error("保存先を取得できませんでした。");
            }
            else
            {
                dir = dtPath.Rows[0]["PATH"].ToString();
            }

            return dir;
        }
        #endregion
    }
}
