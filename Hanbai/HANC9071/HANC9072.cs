﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.han.hanc9071
{
    /// <summary>
    /// 船名マスタの登録(HANC9072)
    /// </summary>
    public partial class HANC9072 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// モード(新規)
        /// </summary>
        private const string MODE_NEW = "1";

        /// <summary>
        /// モード(編集)
        /// </summary>
        private const string MODE_EDIT = "2";
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HANC9072()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="par1">引数1</param>
        public HANC9072(string par1)
            : base(par1)
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // タイトルは非表示
            this.lblTitle.Visible = false;

            // 引数：Par1／モード(1:新規、2:変更)、InData：船名コード
            if (MODE_NEW.Equals(this.Par1))
            {
                // 新規モードの初期表示
                InitDispOnNew();
                this.btnF3.Enabled = false;
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // 編集モードの初期表示
                InitDispOnEdit();
            }
            else
            {
                // 不正な起動として閉じる
                Msg.Error("不正な起動です。終了します。");
                this.Close();
            }

            // ボタンの表示非表示を設定
            this.btnF6.Location = this.btnF3.Location;
            this.btnF3.Location = this.btnF2.Location;
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF2.Visible = false;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F3キー押下時処理
        /// </summary>
        public override void PressF3()
        {
            // 新規モード時
            if (MODE_NEW.Equals(this.Par1))
            {
                return;
            }

            if (Msg.ConfYesNo("削除しますか？") == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 入力値をバインドパラメータとしてセットする
            ArrayList delParams = SetDelParams();
            try
            {
                this.Dba.BeginTransaction();

                // データ削除
                // 船名マスタ
                this.Dba.Update("TM_HN_FUNE_NM_MST",
                    (DbParamCollection)delParams[1],
                    "KAISHA_CD = @KAISHA_CD AND FUNE_NM_CD = @FUNE_NM_CD",
                    (DbParamCollection)delParams[0]);


                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF6();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF6()
        {
            // 確認メッセージを表示
            string msg = (MODE_NEW.Equals(this.Par1) ? "登録" : "更新") + "しますか？";
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            // 入力値をバインドパラメータとしてセットする
            ArrayList alParamshnFuneNm = SetHnFuneNmParams();

            try
            {
                if (MODE_NEW.Equals(this.Par1))
                {
                    // データ登録
                    // 船名マスタ
                    this.Dba.Insert("TM_HN_FUNE_NM_MST", (DbParamCollection)alParamshnFuneNm[0]);
                }
                else if (MODE_EDIT.Equals(this.Par1))
                {
                    // データ更新
                    // 船名マスタ
                    this.Dba.Update("TM_HN_FUNE_NM_MST",
                        (DbParamCollection)alParamshnFuneNm[1],
                        "KAISHA_CD = @KAISHA_CD AND FUNE_NM_CD = @FUNE_NM_CD",
                        (DbParamCollection)alParamshnFuneNm[0]);
                }

                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 船名コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFuneNmCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidFuneNmCd())
            {
                e.Cancel = true;
                this.txtFuneNmCd.SelectAll();
            }
        }

        /// <summary>
        /// 船名の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFuneNm_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidFuneNm())
            {
                e.Cancel = true;
                this.txtFuneNm.SelectAll();
            }
        }

        /// <summary>
        /// 船カナ名の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFuneKanaNm_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidFuneKanaNm())
            {
                e.Cancel = true;
                this.txtFuneKanaNm.SelectAll();
            }
        }

        /// <summary>
        /// トン数の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTonSu_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidTonSu(this.txtTonSu.Text))
            {
                e.Cancel = true;
                this.txtTonSu.SelectAll();
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 新規モードの初期表示
        /// </summary>
        private void InitDispOnNew()
        {
            // 初期値、入力制御を実装

            // 船名コードの初期値を取得
            // 船名コードの中でのMAX+1を初期表示する
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            StringBuilder where = new StringBuilder();
            DataTable dtMaxFuneNm =
                this.Dba.GetDataTableByConditionWithParams("MAX(FUNE_NM_CD) AS MAX_CD",
                    "TM_HN_FUNE_NM_MST", Util.ToString(where), dpc);
            if (dtMaxFuneNm.Rows.Count > 0 && !ValChk.IsEmpty(dtMaxFuneNm.Rows[0]["MAX_CD"]))
            {
                this.txtFuneNmCd.Text = Util.ToString(Util.ToInt(dtMaxFuneNm.Rows[0]["MAX_CD"]) + 1);
            }
            else
            {
                this.txtFuneNmCd.Text = "1001";
            }

            // 初期表示 積立率(本人)0.00
            this.txtTonSu.Text = "0.00";
            // 船名に初期フォーカス
            this.ActiveControl = this.txtFuneNm;
            this.txtFuneNm.Focus();
        }

        /// <summary>
        /// 編集モードの初期表示
        /// </summary>
        private void InitDispOnEdit()
        {
            // 現在DBに登録されている値、入力制御を実装
            StringBuilder cols = new StringBuilder();
            cols.Append("A.KAISHA_CD");
            cols.Append(" ,A.FUNE_NM_CD");
            cols.Append(" ,A.FUNE_NM");
            cols.Append(" ,A.FUNE_KANA_NM");
            cols.Append(" ,A.TON_SU");
            cols.Append(" ,A.REGIST_DATE");
            cols.Append(" ,A.UPDATE_DATE");
            cols.Append(" ,A.SHORI_FLG");

            StringBuilder from = new StringBuilder();
            from.Append("TM_HN_FUNE_NM_MST AS A");
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@FUNE_NM_CD", SqlDbType.Decimal, 7, Util.ToString(this.InData));

            DataTable dtDispData =
                this.Dba.GetDataTableByConditionWithParams(
                    Util.ToString(cols), Util.ToString(from),
                    "A.KAISHA_CD = @KAISHA_CD AND A.FUNE_NM_CD = @FUNE_NM_CD",
                    dpc);

            if (dtDispData.Rows.Count == 0)
            {
                Msg.Error("不正な起動です。終了します。");
                this.Close();
            }

            // 取得した内容を表示
            DataRow drDispData = dtDispData.Rows[0];
            this.txtFuneNmCd.Text = Util.ToString(drDispData["FUNE_NM_CD"]);
            this.txtFuneNm.Text = Util.ToString(drDispData["FUNE_NM"]);
            this.txtFuneKanaNm.Text = Util.ToString(drDispData["FUNE_KANA_NM"]);
            this.txtTonSu.Text = Util.ToString(drDispData["TON_SU"]);

            // 船名コードは入力不可
            this.lblFuneNmCd.Enabled = false;
            this.txtFuneNmCd.Enabled = false;
        }

        /// <summary>
        /// 船名コードの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidFuneNmCd()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtFuneNmCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 既に存在するコードを入力した場合はエラーとする
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@FUNE_NM_CD", SqlDbType.Decimal, 7, this.txtFuneNmCd.Text);
            StringBuilder where = new StringBuilder("KAISHA_CD = @KAISHA_CD");
            where.Append(" AND FUNE_NM_CD = @FUNE_NM_CD");
            DataTable dtFuneNm =
                this.Dba.GetDataTableByConditionWithParams("FUNE_NM_CD",
                    "TM_HN_FUNE_NM_MST", Util.ToString(where), dpc);
            if (dtFuneNm.Rows.Count > 0)
            {
                Msg.Error("既に存在する船名コードと重複しています。");
                return false;
            }
            return true;
        }

        /// <summary>
        /// 船名の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidFuneNm()
        {
            // 20バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtFuneNm.Text, this.txtFuneNm.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 船カナ名の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidFuneKanaNm()
        {
            // 20バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtFuneKanaNm.Text, this.txtFuneKanaNm.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// トン数の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidTonSu(string tonsuStr)
        {
            // 未入力は0とする
            if (ValChk.IsEmpty(tonsuStr))
            {
                this.txtTonSu.Text = "0.00";
            }
            // 数字のみの入力を許可
            if (!ValChk.IsDecNumWithinLength(this.txtTonSu.Text, 2, 2))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 数値のフォーマット
            this.txtTonSu.Text = Util.FormatNum(this.txtTonSu.Text, 2);
            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            if (MODE_NEW.Equals(this.Par1))
            {
                // 船名コードのチェック
                if (!IsValidFuneNmCd())
                {
                    this.txtFuneNmCd.Focus();
                    return false;
                }
            }

            // 船名のチェック
            if (!IsValidFuneNm())
            {
                this.txtFuneNm.Focus();
                return false;
            }
            
            // 船カナ名のチェック
            if (!IsValidFuneKanaNm())
            {
                this.txtFuneKanaNm.Focus();
                return false;
            }

            // トン数のチェック
            if (!IsValidTonSu(this.txtTonSu.Text))
            {
                this.txtTonSu.Focus();
                return false;
            }

            return true;
        }

        /// <summary>
        /// TM_HN_FUNE_NM_MSTに更新するためのパラメータ設定をします。
        /// </summary>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// 更新処理：DbParamCollection*2(Where句,Set句)
        /// </returns>
        private ArrayList SetHnFuneNmParams()
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            if (MODE_NEW.Equals(this.Par1))
            {
                // 会社コードと船名コードを更新パラメータに設定
                updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
                updParam.SetParam("@FUNE_NM_CD", SqlDbType.Decimal, 7, this.txtFuneNmCd.Text);
                // 登録日
                updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWDATE");
                // 処理フラグ
                updParam.SetParam("@SHORI_FLG", SqlDbType.Decimal, 1, "0");
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // 会社コードと船名コードをWhere句のパラメータに設定
                DbParamCollection whereParam = new DbParamCollection();
                whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
                whereParam.SetParam("@FUNE_NM_CD", SqlDbType.Decimal, 7, this.txtFuneNmCd.Text);
                alParams.Add(whereParam);
                // 処理フラグ
                updParam.SetParam("@SHORI_FLG", SqlDbType.Decimal, 1, "1");
            }

            // 船名
            updParam.SetParam("@FUNE_NM", SqlDbType.VarChar, 20, this.txtFuneNm.Text);
            // 船カナ名
            updParam.SetParam("@FUNE_KANA_NM", SqlDbType.VarChar, 20, this.txtFuneKanaNm.Text);
            // トン数
            updParam.SetParam("@TON_SU", SqlDbType.Decimal, 5, this.txtTonSu.Text);
            // 更新日
            updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWDATE");

            alParams.Add(updParam);

            return alParams;
        }

        /// <summary>
        /// TM_HN_FUNE_NM_MSTからデータを削除
        /// </summary>
        /// <returns>
        /// </returns>
        private ArrayList SetDelParams()
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            // 会社コードと船名コードをWhere句のパラメータに設定
            DbParamCollection whereParam = new DbParamCollection();
            whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
            whereParam.SetParam("@FUNE_NM_CD", SqlDbType.Decimal, 7, this.txtFuneNmCd.Text);
            alParams.Add(whereParam);
            // 更新日
            updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWDATE");
            // 処理フラグ
            updParam.SetParam("@SHORI_FLG", SqlDbType.Decimal, 1, "2");

            alParams.Add(updParam);

            return alParams;
        }
        #endregion
    }
}
