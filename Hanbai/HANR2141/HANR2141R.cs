﻿using System.Data;

using jp.co.fsi.common.report;

namespace jp.co.fsi.han.hanr2141
{
    /// <summary>
    /// HANR2141R の概要の説明です。
    /// </summary>
    public partial class HANR2141R : BaseReport
    {

        public HANR2141R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }
    }
}
