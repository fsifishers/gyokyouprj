﻿using System;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Reflection;
using System.Windows.Forms;

using GrapeCity.ActiveReports;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;
using System.Globalization;

namespace jp.co.fsi.han.hanr2141
{
    /// <summary>
    /// 支払明細書(HANR2141)
    /// </summary>
    public partial class HANR2141 : BasePgForm
    {
        #region 構造体
        /// <summary>
        /// 印刷ワークテーブルのデータ構造体
        /// </summary>
        struct NumVals
        {
            public decimal SURYO;
            public decimal KINGAKU;

            public void Clear()
            {
                SURYO = 0;
                KINGAKU = 0;
            }
        }
        #endregion

        #region プロパティ
        /// <summary>
        /// 画面上最後となるフォーカスのEnterボタン押下時処理用変数
        /// </summary>
        private bool _dtFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._dtFlg;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HANR2141()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region メソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 水揚支所
            this.txtMizuageShishoCd.Text = "1";
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_HN_COMBO_DATA_MIZUAGE", this.txtMizuageShishoCd.Text);

            string[] jpDate = Util.ConvJpDate(DateTime.Now, this.Dba);
            // 日付(支払日)
            lblShiharaiDateGengo.Text = jpDate[0];
            txtShiharaiDateYear.Text = jpDate[2];
            txtShiharaiDateMonth.Text = jpDate[3];
            txtShiharaiDateDay.Text = jpDate[4];
            // 日付(セリ日期間)
            lblSeriDateGengoFr.Text = jpDate[0];
            txtSeriDateYearFr.Text = jpDate[2];
            txtSeriDateMonthFr.Text = jpDate[3];
            txtSeriDateDayFr.Text = jpDate[4];
            lblSeriDateGengoTo.Text = jpDate[0];
            txtSeriDateYearTo.Text = jpDate[2];
            txtSeriDateMonthTo.Text = jpDate[3];
            txtSeriDateDayTo.Text = jpDate[4];

            // フォーカス設定
            this.txtShiharaiDateYear.Focus();

            // Enter処理を無効化
            this._dtFlg = false;
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 水揚支所,日付(年),船主コードにフォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd":
                case "txtShiharaiDateYear":
                case "txtSeriDateYearFr":
                case "txtSeriDateYearTo":
                case "txtFunanushiCdFr":
                case "txtFunanushiCdTo":
                    this.btnF1.Enabled = true;
                    break;
                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;
            String[] result;

            switch (this.ActiveCtlNm)
            {
                #region 水揚支所
                case "txtMizuageShishoCd": // 水揚支所
                    result = this.openSearchWindow("COMC8011", "TB_HN_COMBO_DATA_MIZUAGE", this.txtMizuageShishoCd.Text);
                    if (!ValChk.IsEmpty(result[0]))
                    {
                        this.txtMizuageShishoCd.Text = result[0];
                        this.lblMizuageShishoNm.Text = result[1];
                    }
                    break;
                #endregion

                #region 支払日
                case "txtShiharaiDateYear":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom("COMC9011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.com.comc9011.COMC9011");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblShiharaiDateGengo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.lblShiharaiDateGengo.Text = result[1];

                                // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
                                DateTime tmpDate = Util.ConvAdDate(this.lblShiharaiDateGengo.Text, this.txtShiharaiDateYear.Text,
                                    this.txtShiharaiDateMonth.Text, "1", this.Dba);
                                int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);
                                if (Util.ToInt(this.txtShiharaiDateDay.Text) > lastDayInMonth)
                                {
                                    this.txtShiharaiDateDay.Text = Util.ToString(lastDayInMonth);
                                }

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblShiharaiDateGengo.Text,
                                        this.txtShiharaiDateYear.Text,
                                        this.txtShiharaiDateMonth.Text,
                                        this.txtShiharaiDateDay.Text,
                                        this.Dba);
                                this.lblShiharaiDateGengo.Text = arrJpDate[0];
                                this.txtShiharaiDateYear.Text = arrJpDate[2];
                                this.txtShiharaiDateMonth.Text = arrJpDate[3];
                                this.txtShiharaiDateDay.Text = arrJpDate[4];
                            }
                        }
                    }
                    break;
                #endregion

                #region セリ日期間
                case "txtSeriDateYearFr":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom("COMC9011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.com.comc9011.COMC9011");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblSeriDateGengoFr.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.lblSeriDateGengoFr.Text = result[1];

                                // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
                                DateTime tmpDate = Util.ConvAdDate(this.lblSeriDateGengoFr.Text, this.txtSeriDateYearFr.Text,
                                    this.txtSeriDateMonthFr.Text, "1", this.Dba);
                                int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);
                                if (Util.ToInt(this.txtSeriDateDayFr.Text) > lastDayInMonth)
                                {
                                    this.txtSeriDateDayFr.Text = Util.ToString(lastDayInMonth);
                                }

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblSeriDateGengoFr.Text,
                                        this.txtSeriDateYearFr.Text,
                                        this.txtSeriDateMonthFr.Text,
                                        this.txtSeriDateDayFr.Text,
                                        this.Dba);
                                this.lblSeriDateGengoFr.Text = arrJpDate[0];
                                this.txtSeriDateYearFr.Text = arrJpDate[2];
                                this.txtSeriDateMonthFr.Text = arrJpDate[3];
                                this.txtSeriDateDayFr.Text = arrJpDate[4];
                            }
                        }
                    }
                    break;

                case "txtSeriDateYearTo":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom("COMC9011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.com.comc9011.COMC9011");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblSeriDateGengoTo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.lblSeriDateGengoTo.Text = result[1];

                                // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
                                DateTime tmpDate = Util.ConvAdDate(this.lblSeriDateGengoTo.Text, this.txtSeriDateYearTo.Text,
                                    this.txtSeriDateMonthTo.Text, "1", this.Dba);
                                int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);
                                if (Util.ToInt(this.txtSeriDateDayTo.Text) > lastDayInMonth)
                                {
                                    this.txtSeriDateDayTo.Text = Util.ToString(lastDayInMonth);
                                }

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblSeriDateGengoTo.Text,
                                        this.txtSeriDateYearTo.Text,
                                        this.txtSeriDateMonthTo.Text,
                                        this.txtSeriDateDayTo.Text,
                                        this.Dba);
                                this.lblSeriDateGengoTo.Text = arrJpDate[0];
                                this.txtSeriDateYearTo.Text = arrJpDate[2];
                                this.txtSeriDateMonthTo.Text = arrJpDate[3];
                                this.txtSeriDateDayTo.Text = arrJpDate[4];
                            }
                        }
                    }
                    break;
                #endregion

                #region 船主CD
                case "txtFunanushiCdFr":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom("KOBC9021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.kob.kobc9021.KOBC9021");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtFunanushiCdFr.Text = outData[0];
                                this.lblFunanushiCdFr.Text = outData[1];
                            }
                        }
                    }
                    break;

                case "txtFunanushiCdTo":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom("KOBC9021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.kob.kobc9021.KOBC9021");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtFunanushiCdTo.Text = outData[0];
                                this.lblFunanushiCdTo.Text = outData[1];
                            }
                        }
                    }
                    break;
                #endregion
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
            {
                // ﾌﾟﾚﾋﾞｭｰ処理
                DoPrint(true);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("印刷", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }

        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            // 設定画面の起動
            // MEMO:原則としてここで渡す帳票IDの設定はReport.csvに保持していることが前提ですが、
            // 保持していない場合は、設定画面での保存(F6)時に新規に設定が保持されます。
            PrintSettingForm psForm = new PrintSettingForm(new string[1] { "HANR2141R" });
            psForm.ShowDialog();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 水揚支所入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMizuageShishoCd_Validating(object sender, CancelEventArgs e)
        {
            if (!this.IsValidMizuageShishoCd())
            {
                e.Cancel = true;

                this.lblMizuageShishoNm.Text = "";
                this.txtMizuageShishoCd.SelectAll();
                this.txtMizuageShishoCd.Focus();
            }
        }

        /// <summary>
        /// 年(支払日)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShiharaiDateYear_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShiharaiDateYear())
            {
                e.Cancel = true;
                this.txtShiharaiDateYear.SelectAll();
            }
            else
            {
                CheckShiharaiDate();
                SetShiharaiDate();
            }
        }

        /// <summary>
        /// 月(支払日)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShiharaiDateMonth_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShiharaiDateMonth())
            {
                e.Cancel = true;
                this.txtShiharaiDateMonth.SelectAll();
            }
            else
            {
                CheckShiharaiDate();
                SetShiharaiDate();
            }
        }

        /// <summary>
        /// 日(支払日)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShiharaiDateDay_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShiharaiDateDay())
            {
                e.Cancel = true;
                this.txtShiharaiDateDay.SelectAll();
            }
            else
            {
                CheckShiharaiDate();
                SetShiharaiDate();
            }
        }

        /// <summary>
        /// 年(セリ日付)(自)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSeriDateYearFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidSeriDateYearFr())
            {
                e.Cancel = true;
                this.txtSeriDateYearFr.SelectAll();
            }
            else
            {
                CheckSeriDateFr();
                SetSeriDateFr();
            }
        }

        /// <summary>
        /// 年(セリ日付)(至)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSeriDateYearTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidSeriDateYearTo())
            {
                e.Cancel = true;
                this.txtSeriDateYearTo.SelectAll();
            }
            else
            {
                CheckSeriDateTo();
                SetSeriDateTo();
            }
        }

        /// <summary>
        /// 月(セリ日付)(自)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSeriDateMonthFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidSeriDateMonthFr())
            {
                e.Cancel = true;
                this.txtSeriDateMonthFr.SelectAll();
            }
            else
            {
                CheckSeriDateFr();
                SetSeriDateFr();
            }
        }

        /// <summary>
        /// 月(セリ日付)(至)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSeriDateMonthTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidSeriDateMonthTo())
            {
                e.Cancel = true;
                this.txtSeriDateMonthTo.SelectAll();
            }
            else
            {
                CheckSeriDateTo();
                SetSeriDateTo();
            }
        }

        /// <summary>
        /// 日(セリ日付)(自)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSeriDateDayFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidSeriDateDayFr())
            {
                e.Cancel = true;
                this.txtSeriDateDayFr.SelectAll();
            }
            else
            {
                CheckSeriDateFr();
                SetSeriDateFr();
            }
        }
        /// <summary>
        /// 日(セリ日付)(至)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSeriDateDayTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidSeriDateDayTo())
            {
                e.Cancel = true;
                this.txtSeriDateDayTo.SelectAll();
            }
            else
            {
                CheckSeriDateTo();
                SetSeriDateTo();
            }
        }

        /// <summary>
        /// 船主コード(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCodeFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidFunanushiCdFr())
            {
                e.Cancel = true;
                this.txtFunanushiCdFr.SelectAll();
            }
        }

        /// <summary>
        /// 船主コード(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCodeTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidFunanushiCdTo())
            {
                e.Cancel = true;
                this.txtFunanushiCdTo.SelectAll();

                // Enter処理を無効化
                this._dtFlg = false;
            }
            else
            {
                // Enter処理を有効化
                this._dtFlg = true;
            }
        }

        /// <summary>
        /// 船主コード(至)のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFunanushiCdTo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.Flg)
            {
                // Enter処理を無効化
                this._dtFlg = false;

                // 全項目を再度入力値チェック
                if (!ValidateAll())
                {
                    // エラーありの場合ここで処理終了
                    return;
                }

                if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
                {
                    // ﾌﾟﾚﾋﾞｭｰ処理
                    DoPrint(true);
                }
                else
                {
                    this.txtFunanushiCdTo.Focus();
                }
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 水揚支所の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool IsValidMizuageShishoCd()
        {
            // 空入力の場合
            if (ValChk.IsEmpty(this.txtMizuageShishoCd.Text))
            {
                // 水揚支所名称を表示する
                this.lblMizuageShishoNm.Text = "全て";
                return true;
            }

            // 最大桁数チェック
            if (!ValChk.IsWithinLength(this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.MaxLength))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            // 数値チェック
            if (!ValChk.IsNumber(this.txtMizuageShishoCd.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                return false;
            }

            // 0入力の場合
            if (Equals(this.txtMizuageShishoCd.Text, "0"))
            {
                // 水揚支所名称を表示する
                this.lblMizuageShishoNm.Text = "全て";
                return true;
            }

            // 水揚支所名称を表示する
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_HN_COMBO_DATA_MIZUAGE", this.txtMizuageShishoCd.Text);

            if (ValChk.IsEmpty(this.lblMizuageShishoNm.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 年(支払日)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShiharaiDateYear()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtShiharaiDateYear.Text))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }
            // 空の場合、0年として処理
            if (ValChk.IsEmpty(this.txtShiharaiDateYear.Text))
            {
                this.txtShiharaiDateYear.Text = "0";
            }

            return true;
        }

        /// <summary>
        /// 月(支払日)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShiharaiDateMonth()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtShiharaiDateMonth.Text))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }
            // 空の場合、1月として処理
            if (ValChk.IsEmpty(this.txtShiharaiDateMonth.Text))
            {
                this.txtShiharaiDateMonth.Text = "1";
            }
            else
            {
                // 12を超える月が入力された場合、12月として処理
                if (Util.ToInt(this.txtShiharaiDateMonth.Text) > 12)
                {
                    this.txtShiharaiDateMonth.Text = "12";
                }
                // 1より小さい月が入力された場合、1月として処理
                else if (Util.ToInt(this.txtShiharaiDateMonth.Text) < 1)
                {
                    this.txtShiharaiDateMonth.Text = "1";
                }
            }

            return true;
        }

        /// <summary>
        /// 日(支払日)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShiharaiDateDay()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtShiharaiDateDay.Text))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }

            if (ValChk.IsEmpty(this.txtShiharaiDateDay.Text))
            {
                // 空の場合、1日として処理
                this.txtShiharaiDateDay.Text = "1";
            }
            else
            {
                // 1より小さい日が入力された場合、1日として処理
                if (Util.ToInt(this.txtShiharaiDateDay.Text) < 1)
                {
                    this.txtShiharaiDateDay.Text = "1";
                }
            }

            return true;
        }

        /// <summary>
        /// 年月日(支払日)の月末入力チェック
        /// </summary>
        /// 
        private void CheckShiharaiDate()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblShiharaiDateGengo.Text, this.txtShiharaiDateYear.Text,
                this.txtShiharaiDateMonth.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtShiharaiDateDay.Text) > lastDayInMonth)
            {
                this.txtShiharaiDateDay.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(支払日)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetShiharaiDate()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetShiharaiDate(Util.FixJpDate(this.lblShiharaiDateGengo.Text, this.txtShiharaiDateYear.Text,
                this.txtShiharaiDateMonth.Text, this.txtShiharaiDateDay.Text, this.Dba));
        }

        /// <summary>
        /// 年(セリ日付)(自)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidSeriDateYearFr()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtSeriDateYearFr.Text))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }
            // 空の場合、0年として処理
            if (ValChk.IsEmpty(this.txtSeriDateYearFr.Text))
            {
                this.txtSeriDateYearFr.Text = "0";
            }

            return true;
        }

        /// <summary>
        /// 月(セリ日付)(自)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidSeriDateMonthFr()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtSeriDateMonthFr.Text))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }
            // 空の場合、1月として処理
            if (ValChk.IsEmpty(this.txtSeriDateMonthFr.Text))
            {
                this.txtSeriDateMonthFr.Text = "1";
            }
            else
            {
                // 12を超える月が入力された場合、12月として処理
                if (Util.ToInt(this.txtSeriDateMonthFr.Text) > 12)
                {
                    this.txtSeriDateMonthFr.Text = "12";
                }
                // 1より小さい月が入力された場合、1月として処理
                else if (Util.ToInt(this.txtSeriDateMonthFr.Text) < 1)
                {
                    this.txtSeriDateMonthFr.Text = "1";
                }
            }

            return true;
        }

        /// <summary>
        /// 日(セリ日付)(自)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidSeriDateDayFr()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtSeriDateDayFr.Text))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }

            if (ValChk.IsEmpty(this.txtSeriDateDayFr.Text))
            {
                // 空の場合、1日として処理
                this.txtSeriDateDayFr.Text = "1";
            }
            else
            {
                // 1より小さい日が入力された場合、1日として処理
                if (Util.ToInt(this.txtSeriDateDayFr.Text) < 1)
                {
                    this.txtSeriDateDayFr.Text = "1";
                }
            }

            return true;
        }

        /// <summary>
        /// 年月日(セリ日付)(自)の月末入力チェック
        /// </summary>
        /// 
        private void CheckSeriDateFr()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblSeriDateGengoFr.Text, this.txtSeriDateYearFr.Text,
                this.txtSeriDateMonthFr.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtSeriDateDayFr.Text) > lastDayInMonth)
            {
                this.txtSeriDateDayFr.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(セリ日付)(自)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetSeriDateFr()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetSeriDateFr(Util.FixJpDate(this.lblSeriDateGengoFr.Text, this.txtSeriDateYearFr.Text,
                this.txtSeriDateMonthFr.Text, this.txtSeriDateDayFr.Text, this.Dba));
        }

        /// <summary>
        /// 年(セリ日付)(至)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidSeriDateYearTo()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtSeriDateYearTo.Text))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }
            // 空の場合、0年として処理
            if (ValChk.IsEmpty(this.txtSeriDateYearTo.Text))
            {
                this.txtSeriDateYearTo.Text = "0";
            }

            return true;
        }

        /// <summary>
        /// 月(セリ日付)(至)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidSeriDateMonthTo()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtSeriDateMonthTo.Text))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }
            // 空の場合、1月として処理
            if (ValChk.IsEmpty(this.txtSeriDateMonthTo.Text))
            {
                this.txtSeriDateMonthTo.Text = "1";
            }
            else
            {
                // 12を超える月が入力された場合、12月として処理
                if (Util.ToInt(this.txtSeriDateMonthTo.Text) > 12)
                {
                    this.txtSeriDateMonthTo.Text = "12";
                }
                // 1より小さい月が入力された場合、1月として処理
                else if (Util.ToInt(this.txtSeriDateMonthTo.Text) < 1)
                {
                    this.txtSeriDateMonthTo.Text = "1";
                }
            }

            return true;
        }

        /// <summary>
        /// 日(セリ日付)(至)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidSeriDateDayTo()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtSeriDateDayTo.Text))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }

            if (ValChk.IsEmpty(this.txtSeriDateDayTo.Text))
            {
                // 空の場合、1日として処理
                this.txtSeriDateDayTo.Text = "1";
            }
            else
            {
                // 1より小さい日が入力された場合、1日として処理
                if (Util.ToInt(this.txtSeriDateDayTo.Text) < 1)
                {
                    this.txtSeriDateDayTo.Text = "1";
                }
            }

            return true;
        }

        /// <summary>
        /// 年月日(セリ日付)(至)の月末入力チェック
        /// </summary>
        /// 
        private void CheckSeriDateTo()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblSeriDateGengoTo.Text, this.txtSeriDateYearTo.Text,
                this.txtSeriDateMonthTo.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtSeriDateDayTo.Text) > lastDayInMonth)
            {
                this.txtSeriDateDayTo.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(セリ日付)(至)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetSeriDateTo()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetSeriDateTo(Util.FixJpDate(this.lblSeriDateGengoTo.Text, this.txtSeriDateYearTo.Text,
                this.txtSeriDateMonthTo.Text, this.txtSeriDateDayTo.Text, this.Dba));
        }

        /// <summary>
        /// 船主コード(自)の入力チェック
        /// </summary>
        private bool IsValidFunanushiCdFr()
        {
            if (ValChk.IsEmpty(this.txtFunanushiCdFr.Text))
            {
                this.lblFunanushiCdFr.Text = "先　頭";
            }
            else
                // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
                if (!ValChk.IsNumber(this.txtFunanushiCdFr.Text))
                {
                    Msg.Notice("コード(自)は数値のみで入力してください。");
                    return false;
                }
                else
                {
                    // コードを元に名称を取得する
                    this.lblFunanushiCdFr.Text = this.Dba.GetName(this.UInfo, "VI_HN_FUNANUSHI", this.txtFunanushiCdFr.Text);
                }

            return true;
        }

        /// <summary>
        /// 船主コード(至)の入力チェック
        /// </summary>
        private bool IsValidFunanushiCdTo()
        {
            if (ValChk.IsEmpty(this.txtFunanushiCdTo.Text))
            {
                this.lblFunanushiCdTo.Text = "最　後";
            }
            else
                // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
                if (!ValChk.IsNumber(this.txtFunanushiCdTo.Text))
                {
                    Msg.Notice("コード(至)は数値のみで入力してください。");
                    return false;
                }
                else
                {
                    // コードを元に名称を取得する
                    this.lblFunanushiCdTo.Text = this.Dba.GetName(this.UInfo, "VI_HN_FUNANUSHI", this.txtFunanushiCdTo.Text);
                }

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 水揚支所の入力チェック
            if (!IsValidMizuageShishoCd())
            {
                this.txtMizuageShishoCd.Focus();
                this.txtMizuageShishoCd.SelectAll();
                return false;
            }

            // 年(支払日)のチェック
            if (!IsValidShiharaiDateYear())
            {
                this.txtShiharaiDateYear.Focus();
                this.txtShiharaiDateYear.SelectAll();
                return false;
            }
            // 月(支払日)のチェック
            if (!IsValidShiharaiDateMonth())
            {
                this.txtShiharaiDateMonth.Focus();
                this.txtShiharaiDateMonth.SelectAll();
                return false;
            }
            // 日(支払日)のチェック
            if (!IsValidShiharaiDateDay())
            {
                this.txtShiharaiDateDay.Focus();
                this.txtShiharaiDateDay.SelectAll();
                return false;
            }
            // 年月日(支払日)の月末入力チェック処理
            CheckShiharaiDate();
            // 年月日(支払日)の正しい和暦への変換処理
            SetShiharaiDate();

            // 年(セリ日付)(自)のチェック
            if (!IsValidSeriDateYearFr())
            {
                this.txtSeriDateYearFr.Focus();
                this.txtSeriDateYearFr.SelectAll();
                return false;
            }
            // 年(セリ日付)(至)のチェック
            if (!IsValidSeriDateYearTo())
            {
                this.txtSeriDateYearTo.Focus();
                this.txtSeriDateYearTo.SelectAll();
                return false;
            }
            // 月(セリ日付)(自)のチェック
            if (!IsValidSeriDateMonthFr())
            {
                this.txtSeriDateMonthFr.Focus();
                this.txtSeriDateMonthFr.SelectAll();
                return false;
            }
            // 月(セリ日付)(至)のチェック
            if (!IsValidSeriDateMonthTo())
            {
                this.txtSeriDateMonthTo.Focus();
                this.txtSeriDateMonthTo.SelectAll();
                return false;
            }
            // 日(セリ日付)(自)のチェック
            if (!IsValidSeriDateDayFr())
            {
                this.txtSeriDateDayFr.Focus();
                this.txtSeriDateDayFr.SelectAll();
                return false;
            }
            // 日(セリ日付)(至)のチェック
            if (!IsValidSeriDateDayTo())
            {
                this.txtSeriDateDayTo.Focus();
                this.txtSeriDateDayTo.SelectAll();
                return false;
            }
            // 年月日(セリ日付)の月末入力チェック処理
            CheckSeriDateFr();
            CheckSeriDateTo();
            // 年月日(セリ日付)の正しい和暦への変換処理
            SetSeriDateFr();
            SetSeriDateTo();

            // 船主コード(自)の入力チェック
            if (!IsValidFunanushiCdFr())
            {
                this.txtFunanushiCdFr.Focus();
                this.txtFunanushiCdFr.SelectAll();
                return false;
            }
            // 船主コード(至)の入力チェック
            if (!IsValidFunanushiCdTo())
            {
                this.txtFunanushiCdTo.Focus();
                this.txtFunanushiCdTo.SelectAll();
                return false;
            }

            return true;
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetSeriDateFr(string[] arrJpDate)
        {
            this.lblSeriDateGengoFr.Text = arrJpDate[0];
            this.txtSeriDateYearFr.Text = arrJpDate[2];
            this.txtSeriDateMonthFr.Text = arrJpDate[3];
            this.txtSeriDateDayFr.Text = arrJpDate[4];
        }
        private void SetSeriDateTo(string[] arrJpDate)
        {
            this.lblSeriDateGengoTo.Text = arrJpDate[0];
            this.txtSeriDateYearTo.Text = arrJpDate[2];
            this.txtSeriDateMonthTo.Text = arrJpDate[3];
            this.txtSeriDateDayTo.Text = arrJpDate[4];
        }
        private void SetShiharaiDate(string[] arrJpDate)
        {
            this.lblShiharaiDateGengo.Text = arrJpDate[0];
            this.txtShiharaiDateYear.Text = arrJpDate[2];
            this.txtShiharaiDateMonth.Text = arrJpDate[3];
            this.txtShiharaiDateDay.Text = arrJpDate[4];
        }

        /// <summary>
        /// 検索サブウィンドウオープン
        /// </summary>
        /// <param name="moduleName">例："COMC8111"</param>
        /// <param name="para1">例："TB_HN_COMBO_DATA_MIZUAGE"</param>
        /// <param name="textBoxOfCode">例：this.txtGyogyoushuCd.Text</param>
        /// <param name="indata">例： this.txtGyogyoushuCd.Text</param>
        /// <returns>String[0]：検索結果から選択したコード , String[1]：検索結果から選択した名称</returns>
        private String[] openSearchWindow(String moduleName, String para1, String indata)
        {
            string[] result = { "", "" };

            // ネームスペースに使うモジュール名の小文字
            string lowerModuleName = moduleName.ToLower();

            // ネームスペースの末尾
            string nameSpace = lowerModuleName.Substring(0, 3);

            // アセンブリのロード
            Assembly asm = Assembly.LoadFrom(moduleName + ".exe");

            // フォーム作成
            string moduleNameSpace = "jp.co.fsi." + nameSpace + "." + lowerModuleName + "." + moduleName;
            Type t = asm.GetType(moduleNameSpace);

            if (t != null)
            {
                Object obj = Activator.CreateInstance(t);
                if (obj != null)
                {
                    BasePgForm frm = (BasePgForm)obj;
                    frm.Par1 = para1;
                    frm.InData = indata;
                    frm.ShowDialog(this);

                    if (frm.DialogResult == DialogResult.OK)
                    {
                        string[] ret = (string[])frm.OutData;
                        result[0] = ret[0];
                        result[1] = ret[1];
                        return result;
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview)
        {
            try
            {
                bool dataFlag;

                this.Dba.BeginTransaction();

                // 帳票出力用にワークテーブルにデータを作成
                dataFlag = MakeWkData();

                // 帳票出力
                if (dataFlag)
                {
                    // 取得列の定義
                    StringBuilder cols = new StringBuilder();
                    cols.Append("  ITEM01");
                    cols.Append(" ,ITEM02");
                    cols.Append(" ,ITEM03");
                    cols.Append(" ,ITEM04");
                    cols.Append(" ,ITEM05");
                    cols.Append(" ,ITEM06");
                    cols.Append(" ,ITEM07");
                    cols.Append(" ,ITEM08");
                    cols.Append(" ,ITEM09");
                    cols.Append(" ,ITEM10");
                    cols.Append(" ,ITEM11");
                    cols.Append(" ,ITEM12");
                    cols.Append(" ,ITEM13");
                    cols.Append(" ,ITEM14");
                    cols.Append(" ,ITEM15");
                    cols.Append(" ,ITEM16");
                    cols.Append(" ,ITEM17");
                    cols.Append(" ,ITEM18");
                    cols.Append(" ,ITEM19");
                    cols.Append(" ,ITEM20");
                    cols.Append(" ,ITEM21");
                    cols.Append(" ,ITEM22");
                    cols.Append(" ,ITEM23");
                    cols.Append(" ,ITEM24");
                    cols.Append(" ,ITEM25");
                    cols.Append(" ,ITEM26");
                    cols.Append(" ,ITEM27");
                    cols.Append(" ,ITEM28");
                    cols.Append(" ,ITEM29");
                    cols.Append(" ,ITEM30");
                    cols.Append(" ,ITEM31");
                    cols.Append(" ,ITEM32");
                    cols.Append(" ,ITEM33");
                    cols.Append(" ,ITEM34");
                    cols.Append(" ,ITEM35");
                    cols.Append(" ,ITEM36");
                    cols.Append(" ,ITEM37");
                    cols.Append(" ,ITEM38");
                    cols.Append(" ,ITEM39");
                    cols.Append(" ,ITEM40");
                    cols.Append(" ,ITEM41");
                    cols.Append(" ,ITEM42");
                    cols.Append(" ,ITEM43");
                    cols.Append(" ,ITEM44");
                    cols.Append(" ,ITEM45");
                    cols.Append(" ,ITEM46");
                    cols.Append(" ,ITEM47");
                    cols.Append(" ,ITEM48");
                    cols.Append(" ,ITEM49");
                    cols.Append(" ,ITEM50");
                    cols.Append(" ,ITEM51");
                    cols.Append(" ,ITEM52");
                    cols.Append(" ,ITEM53");
                    cols.Append(" ,ITEM54");
                    cols.Append(" ,ITEM55");
                    cols.Append(" ,ITEM56");
                    cols.Append(" ,ITEM57");
                    cols.Append(" ,ITEM58");
                    cols.Append(" ,ITEM59");
                    cols.Append(" ,ITEM60");
                    cols.Append(" ,ITEM61");
                    cols.Append(" ,ITEM62");
                    cols.Append(" ,ITEM63");
                    cols.Append(" ,ITEM64");
                    cols.Append(" ,ITEM65");
                    cols.Append(" ,ITEM66");
                    cols.Append(" ,ITEM67");
                    cols.Append(" ,ITEM68");
                    cols.Append(" ,ITEM69");
                    cols.Append(" ,ITEM70");
                    cols.Append(" ,ITEM71");
                    cols.Append(" ,ITEM72");
                    cols.Append(" ,ITEM73");
                    cols.Append(" ,ITEM74");
                    cols.Append(" ,ITEM75");
                    cols.Append(" ,ITEM76");
                    cols.Append(" ,ITEM77");
                    cols.Append(" ,ITEM78");
                    cols.Append(" ,ITEM79");
                    cols.Append(" ,ITEM80");
                    cols.Append(" ,ITEM81");
                    cols.Append(" ,ITEM82");
                    cols.Append(" ,ITEM83");
                    cols.Append(" ,ITEM84");
                    cols.Append(" ,ITEM85");
                    cols.Append(" ,ITEM86");
                    cols.Append(" ,ITEM87");
                    cols.Append(" ,ITEM88");
                    cols.Append(" ,ITEM89");
                    cols.Append(" ,ITEM90");
                    cols.Append(" ,ITEM91");
                    cols.Append(" ,ITEM92");
                    cols.Append(" ,ITEM93");
                    cols.Append(" ,ITEM94");
                    cols.Append(" ,ITEM95");
                    cols.Append(" ,ITEM96");
                    cols.Append(" ,ITEM97");
                    cols.Append(" ,ITEM98");
                    cols.Append(" ,ITEM99");
                    cols.Append(" ,ITEM100");
                    cols.Append(" ,ITEM101");
                    cols.Append(" ,ITEM102");
                    cols.Append(" ,ITEM103");
                    cols.Append(" ,ITEM104");
                    cols.Append(" ,ITEM105");
                    cols.Append(" ,ITEM106");
                    cols.Append(" ,ITEM107");

                    // バインドパラメータの設定
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);

                    // データの取得
                    DataTable dtOutput = this.Dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_HN_TBL", "GUID = @GUID", "SORT ASC", dpc);

                    // 帳票オブジェクトをインスタンス化
                    HANR2141R rpt = new HANR2141R(dtOutput);

                    if (isPreview)
                    {
                        // プレビュー画面表示
                        PreviewForm pFrm = new PreviewForm(rpt, this.UnqId);
                        pFrm.WindowState = FormWindowState.Maximized;
                        pFrm.Show();
                    }
                    else
                    {
                        // 直接印刷
                        rpt.Run(false);
                        rpt.Document.Print(true, true, false);
                    }
                }
            }
            finally
            {
                this.Dba.Rollback();
            }
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData()
        {
            #region データ取得の準備
            // 日付(セリ日付)を西暦にして取得
            DateTime tmpSRDate = Util.ConvAdDate(this.lblSeriDateGengoTo.Text, this.txtSeriDateYearTo.Text,
                    this.txtSeriDateMonthTo.Text, this.txtSeriDateDayTo.Text, this.Dba);
            // 日付を和暦で保持
            string[] tmpSRWareki = Util.ConvJpDate(tmpSRDate, this.Dba);
            tmpSRWareki[3] = Util.PadZero(tmpSRWareki[3], 2);
            tmpSRWareki[4] = Util.PadZero(tmpSRWareki[4], 2);

            // 船主コード設定
            string funanushiCdFr;
            string funanushiCdTo;
            if (Util.ToDecimal(txtFunanushiCdFr.Text) > 0)
            {
                funanushiCdFr = txtFunanushiCdFr.Text;
            }
            else
            {
                funanushiCdFr = "0";
            }
            if (Util.ToDecimal(txtFunanushiCdTo.Text) > 0)
            {
                funanushiCdTo = txtFunanushiCdTo.Text;
            }
            else
            {
                funanushiCdTo = "99999";
            }

            DbParamCollection dpc = new DbParamCollection();
            StringBuilder sql = new StringBuilder();

            int dbSORT = 1;
            int i = 0; // メインループ用カウント変数
            int j = 7; // データ格納用変数
            int k = 7; // インサート項目数格納用変数
            int flg = 0; // インサートフラグ
            int flgLast = 0; // 合計表示用ページ表示フラグ
            int pageCount = 1; // 改ページ用カウント変数
            int tmpLastPageData = 0; // 船主毎最終頁表示データ残数
            decimal tmpMeisaiCount = 0; // 船主データ件数変数
            int tmpPage = 0; // 船主CD毎の件数によるﾍﾟｰｼﾞ設定変数
            decimal tmpGokeiKingaku = 0; // 船主CD毎の金額合計変数
            decimal tmpGokeiShohizei = 0; // 船主CD毎の消費税額合計変数
            decimal tmpGokei = 0; // 船主CD毎の金額合計+消費税額合計変数
            decimal kojoGokei = 0; // 船主CD毎の金額合計+消費税額合計変数
            int before = -1; // 改ページ比較用カウント変数
            int next = 1; // 改ページ比較用カウント変数

            #endregion

            #region メインデータ取得
            dpc = new DbParamCollection();
            sql = new StringBuilder();

            // han.VI_水揚仕切書(VI_HN_MIZUAGE_SHIKIRISHO)の日付から発生しているデータを取得
            sql.Append(" SELECT");
            sql.Append(" *");
            sql.Append(" FROM");
            sql.Append(" VI_HN_MIZUAGE_SHIKIRISHO");
            sql.Append(" WHERE");
            sql.Append(" KAISHA_CD = @KAISHA_CD AND");
            sql.Append(" SERIBI = @DATE_SR AND");
            sql.Append(" SENSHU_CD BETWEEN @FUNANUSHI_CD_FR AND @FUNANUSHI_CD_TO ");
            sql.Append(" ORDER BY");
            sql.Append(" SENSHU_CD,");
            sql.Append(" SERIBI,");
            sql.Append(" GYOGYO_TESURYO,");
            sql.Append(" DENPYO_BANGO,");
            sql.Append(" GYO_NO");
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@DATE_SR", SqlDbType.DateTime, tmpSRDate);
            dpc.SetParam("@FUNANUSHI_CD_FR", SqlDbType.VarChar, 6, funanushiCdFr);
            dpc.SetParam("@FUNANUSHI_CD_TO", SqlDbType.VarChar, 6, funanushiCdTo);

            DataTable dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);

            #endregion

            if (dtMainLoop.Rows.Count == 0)
            {
                Msg.Info("該当データがありません。");
                return false;
            }
            else
            {
                NumVals GOKEI = new NumVals(); // 伝票グループ

                while (dtMainLoop.Rows.Count > i)
                {
                    #region 船主別のデータ件数を取得
                    // 現在の船主コードと前の船主コードが一致しない場合
                    if (i == 0)
                    {
                        dpc = new DbParamCollection();
                        sql = new StringBuilder();
                        sql.Append(" SELECT");
                        sql.Append(" COUNT");
                        sql.Append(" (SENSHU_CD) AS MEISAI_COUNT");
                        sql.Append(" FROM");
                        sql.Append(" VI_HN_MIZUAGE_SHIKIRISHO");
                        sql.Append(" WHERE");
                        sql.Append(" KAISHA_CD = @KAISHA_CD AND");
                        sql.Append(" SERIBI = @DATE_SR AND");
                        sql.Append(" SENSHU_CD = @SENSHU_CD");
                        dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                        dpc.SetParam("@DATE_SR", SqlDbType.VarChar, 10, tmpSRDate.Date.ToString("yyyy/MM/dd"));
                        dpc.SetParam("@SENSHU_CD", SqlDbType.VarChar, 6, Util.ToInt(dtMainLoop.Rows[i]["SENSHU_CD"]));

                        DataTable dtFunanushiCount = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);
                        tmpMeisaiCount = (decimal)(Util.ToInt(dtFunanushiCount.Rows[0]["MEISAI_COUNT"]) - 1) / 17;
                        tmpPage = (int)Math.Truncate(tmpMeisaiCount) + 1;
                        tmpLastPageData = Util.ToInt(dtFunanushiCount.Rows[0]["MEISAI_COUNT"]) - (int)Math.Truncate(tmpMeisaiCount) * 17;
                        if (tmpLastPageData == 17 || tmpLastPageData == 16)
                        {
                            tmpPage = tmpPage + 1;
                            flgLast = 1;
                        }
                    }
                    else if (Util.ToInt(dtMainLoop.Rows[i]["SENSHU_CD"]) != Util.ToInt(dtMainLoop.Rows[before]["SENSHU_CD"]))
                    {
                        dpc = new DbParamCollection();
                        sql = new StringBuilder();
                        sql.Append(" SELECT");
                        sql.Append(" COUNT");
                        sql.Append(" (SENSHU_CD) AS MEISAI_COUNT");
                        sql.Append(" FROM");
                        sql.Append(" VI_HN_MIZUAGE_SHIKIRISHO");
                        sql.Append(" WHERE");
                        sql.Append(" KAISHA_CD = @KAISHA_CD AND");
                        sql.Append(" SERIBI = @DATE_SR AND");
                        sql.Append(" SENSHU_CD = @SENSHU_CD");
                        dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                        dpc.SetParam("@DATE_SR", SqlDbType.VarChar, 10, tmpSRDate.Date.ToString("yyyy/MM/dd"));
                        dpc.SetParam("@SENSHU_CD", SqlDbType.VarChar, 6, Util.ToInt(dtMainLoop.Rows[i]["SENSHU_CD"]));

                        DataTable dtFunanushiCount = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);
                        tmpMeisaiCount = (decimal)(Util.ToInt(dtFunanushiCount.Rows[0]["MEISAI_COUNT"]) - 1) / 17;
                        tmpPage = (int)Math.Truncate(tmpMeisaiCount) + 1;
                        tmpLastPageData = Util.ToInt(dtFunanushiCount.Rows[0]["MEISAI_COUNT"]) - (int)Math.Truncate(tmpMeisaiCount) * 17;
                        if (tmpLastPageData == 17 || tmpLastPageData == 16)
                        {
                            tmpPage = tmpPage + 1;
                            flgLast = 1;
                        }
                    }
                    #endregion

                    #region 船主別の税抜合計、消費税額を取得
                    // 現在の船主コードと前の船主コードが一致しない場合
                    if (i == 0)
                    {
                        dpc = new DbParamCollection();
                        sql = new StringBuilder();

                        sql.Append(" SELECT");
                        sql.Append(" SUM(MIZUAGE_ZEINUKI_KINGAKU) AS GOKEI_KINGAKU,");
                        sql.Append(" SUM(MIZUAGE_SHOHIZEIGAKU) AS GOKEI_SHOHIZEI");
                        sql.Append(" FROM");
                        sql.Append(" VI_HN_MIZUAGE_SHIKIRISHO");
                        sql.Append(" WHERE");
                        sql.Append(" KAISHA_CD = @KAISHA_CD AND");
                        sql.Append(" SERIBI = @DATE_SR AND");
                        sql.Append(" SENSHU_CD = @SENSHU_CD AND");
                        sql.Append(" GYO_NO = 1");
                        dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                        dpc.SetParam("@DATE_SR", SqlDbType.VarChar, 10, tmpSRDate.Date.ToString("yyyy/MM/dd"));
                        dpc.SetParam("@SENSHU_CD", SqlDbType.VarChar, 6, Util.ToInt(dtMainLoop.Rows[i]["SENSHU_CD"]));

                        DataTable dtGokeiKingaku = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);
                        tmpGokeiKingaku = Util.ToInt(dtGokeiKingaku.Rows[0]["GOKEI_KINGAKU"]);
                        tmpGokeiShohizei = Util.ToInt(dtGokeiKingaku.Rows[0]["GOKEI_SHOHIZEI"]);

                    }
                    else if (Util.ToInt(dtMainLoop.Rows[i]["SENSHU_CD"]) != Util.ToInt(dtMainLoop.Rows[before]["SENSHU_CD"]))
                    {
                        dpc = new DbParamCollection();
                        sql = new StringBuilder();

                        sql.Append(" SELECT");
                        sql.Append(" SUM(MIZUAGE_ZEINUKI_KINGAKU) AS GOKEI_KINGAKU,");
                        sql.Append(" SUM(MIZUAGE_SHOHIZEIGAKU) AS GOKEI_SHOHIZEI");
                        sql.Append(" FROM");
                        sql.Append(" VI_HN_MIZUAGE_SHIKIRISHO");
                        sql.Append(" WHERE");
                        sql.Append(" KAISHA_CD = @KAISHA_CD AND");
                        sql.Append(" SERIBI = @DATE_SR AND");
                        sql.Append(" SENSHU_CD = @SENSHU_CD AND");
                        sql.Append(" GYO_NO = 1");
                        dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                        dpc.SetParam("@DATE_SR", SqlDbType.VarChar, 10, tmpSRDate.Date.ToString("yyyy/MM/dd"));
                        dpc.SetParam("@SENSHU_CD", SqlDbType.VarChar, 6, Util.ToInt(dtMainLoop.Rows[i]["SENSHU_CD"]));

                        DataTable dtGokeiKingaku = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);
                        tmpGokeiKingaku = Util.ToInt(dtGokeiKingaku.Rows[0]["GOKEI_KINGAKU"]);
                        tmpGokeiShohizei = Util.ToInt(dtGokeiKingaku.Rows[0]["GOKEI_SHOHIZEI"]);

                    }
                    #endregion

                    #region データのセット
                    // データを1行毎にセット
                    if (j == 7)
                    {
                        dpc.SetParam("@ITEM0" + j, SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["YAMA_NO"]); // 山NO
                        j++;
                        dpc.SetParam("@ITEM0" + j, SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["GYOSHU_NM"]); // 魚種名称
                        j++;
                        dpc.SetParam("@ITEM0" + j, SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["SURYO"], 1)); // 数量
                        j++;
                        dpc.SetParam("@ITEM" + j, SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["TANKA"])); // 単価
                        j++;
                        dpc.SetParam("@ITEM" + j, SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["KINGAKU"])); // 金額
                        j++;
                    }
                    else
                    {
                        dpc.SetParam("@ITEM" + j, SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["YAMA_NO"]); // 山NO
                        j++;
                        dpc.SetParam("@ITEM" + j, SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["GYOSHU_NM"]); // 魚種名称
                        j++;
                        dpc.SetParam("@ITEM" + j, SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["SURYO"], 1)); // 数量
                        j++;
                        dpc.SetParam("@ITEM" + j, SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["TANKA"])); // 単価
                        j++;
                        dpc.SetParam("@ITEM" + j, SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["KINGAKU"])); // 金額
                        j++;
                    }

                    // 現在の仲買人コードと次の仲買人コードが一致しない場合、フラグをたてる
                    if (Util.ToInt(dtMainLoop.Rows.Count) != next && Util.ToInt(dtMainLoop.Rows[i]["SENSHU_CD"]) != Util.ToInt(dtMainLoop.Rows[next]["SENSHU_CD"]))
                    {
                        flg = 1;
                    }
                    // 仲買人毎の最終ページではない & ページの最大までデータがセットされた場合、フラグをたてる
                    else if (pageCount != tmpPage && j == 92)
                    {
                        flg = 1;
                    }

                    // 最後のデータの場合、フラグをたてる
                    if (Util.ToInt(dtMainLoop.Rows.Count) == next)
                    {
                        flg = 1;
                    }

                    // 合計金額の足しこみ
                    GOKEI.SURYO += Util.ToDecimal(dtMainLoop.Rows[i]["SURYO"]);
                    GOKEI.KINGAKU += Util.ToDecimal(dtMainLoop.Rows[i]["KINGAKU"]);
                    #endregion

                    #region データのインサート
                    // フラグが立っていればインサート
                    if (flg == 1)
                    {
                        sql = new StringBuilder();
                        sql.Append("INSERT INTO PR_HN_TBL(");
                        sql.Append("  GUID");
                        sql.Append(" ,SORT");
                        sql.Append(" ,ITEM01");
                        sql.Append(" ,ITEM02");
                        sql.Append(" ,ITEM03");
                        sql.Append(" ,ITEM04");
                        sql.Append(" ,ITEM05");
                        sql.Append(" ,ITEM06");
                        while (k < j)
                        {
                            if (k < 10)
                            {
                                sql.Append(" ,ITEM0" + k);
                            }
                            else
                            {
                                sql.Append(" ,ITEM" + k);
                            }

                            k++;
                        }
                        // 各船主データの最終頁
                        if (pageCount == tmpPage)
                        {
                            sql.Append(" ,ITEM82");
                            sql.Append(" ,ITEM83");
                            sql.Append(" ,ITEM84");
                            sql.Append(" ,ITEM85");
                            sql.Append(" ,ITEM86");
                            sql.Append(" ,ITEM87");
                            sql.Append(" ,ITEM88");
                            sql.Append(" ,ITEM89");
                            sql.Append(" ,ITEM90");
                            sql.Append(" ,ITEM91");
                        }
                        sql.Append(" ,ITEM92");
                        sql.Append(" ,ITEM93");
                        sql.Append(" ,ITEM94");
                        sql.Append(" ,ITEM95");
                        sql.Append(" ,ITEM96");
                        sql.Append(" ,ITEM97");
                        sql.Append(" ,ITEM98");
                        sql.Append(" ,ITEM99");
                        sql.Append(" ,ITEM100");
                        sql.Append(" ,ITEM101");
                        sql.Append(" ,ITEM102");
                        sql.Append(" ,ITEM103");
                        sql.Append(" ,ITEM104");
                        sql.Append(" ,ITEM105");
                        sql.Append(" ,ITEM106");
                        sql.Append(" ,ITEM107");

                        k = 7;

                        sql.Append(") ");
                        sql.Append("VALUES(");
                        sql.Append("  @GUID");
                        sql.Append(" ,@SORT");
                        sql.Append(" ,@ITEM01");
                        sql.Append(" ,@ITEM02");
                        sql.Append(" ,@ITEM03");
                        sql.Append(" ,@ITEM04");
                        sql.Append(" ,@ITEM05");
                        sql.Append(" ,@ITEM06");
                        while (k < j)
                        {
                            if (k < 10)
                            {
                                sql.Append(" ,@ITEM0" + k);
                            }
                            else
                            {
                                sql.Append(" ,@ITEM" + k);
                            }

                            k++;
                        }
                        // 各船主データの最終頁
                        if (pageCount == tmpPage)
                        {
                            sql.Append(" ,@ITEM82");
                            sql.Append(" ,@ITEM83");
                            sql.Append(" ,@ITEM84");
                            sql.Append(" ,@ITEM85");
                            sql.Append(" ,@ITEM86");
                            sql.Append(" ,@ITEM87");
                            sql.Append(" ,@ITEM88");
                            sql.Append(" ,@ITEM89");
                            sql.Append(" ,@ITEM90");
                            sql.Append(" ,@ITEM91");
                        }
                        sql.Append(" ,@ITEM92");
                        sql.Append(" ,@ITEM93");
                        sql.Append(" ,@ITEM94");
                        sql.Append(" ,@ITEM95");
                        sql.Append(" ,@ITEM96");
                        sql.Append(" ,@ITEM97");
                        sql.Append(" ,@ITEM98");
                        sql.Append(" ,@ITEM99");
                        sql.Append(" ,@ITEM100");
                        sql.Append(" ,@ITEM101");
                        sql.Append(" ,@ITEM102");
                        sql.Append(" ,@ITEM103");
                        sql.Append(" ,@ITEM104");
                        sql.Append(" ,@ITEM105");
                        sql.Append(" ,@ITEM106");
                        sql.Append(" ,@ITEM107");
                        sql.Append(") ");

                        // 各船主データの最終頁
                        if (pageCount == tmpPage)
                        {
                            #region データ登録
                            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                            dpc.SetParam("@SORT", SqlDbType.VarChar, 9, dbSORT);
                            dbSORT++;
                            dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, tmpSRWareki[5]); // セリ日
                            dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpSRWareki[3]); // セリ日(月)
                            dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpSRWareki[4]); // セリ日(日)
                            dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SENSHU_CD"]); // 船主コード
                            dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SENSHU_NM"]); // 船主名称
                            dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, Util.PadZero(pageCount, 4)); // 改ページ用カウント
                            dpc.SetParam("@ITEM82", SqlDbType.VarChar, 200, "");
                            dpc.SetParam("@ITEM83", SqlDbType.VarChar, 200, "税抜合計");
                            dpc.SetParam("@ITEM84", SqlDbType.VarChar, 200, "");
                            dpc.SetParam("@ITEM85", SqlDbType.VarChar, 200, "");
                            dpc.SetParam("@ITEM86", SqlDbType.VarChar, 200, Util.FormatNum(tmpGokeiKingaku)); // 税抜合計
                            dpc.SetParam("@ITEM87", SqlDbType.VarChar, 200, "");
                            dpc.SetParam("@ITEM88", SqlDbType.VarChar, 200, "消費税");
                            dpc.SetParam("@ITEM89", SqlDbType.VarChar, 200, "");
                            dpc.SetParam("@ITEM90", SqlDbType.VarChar, 200, "");
                            dpc.SetParam("@ITEM91", SqlDbType.VarChar, 200, Util.FormatNum(tmpGokeiShohizei)); // 消費税
                            dpc.SetParam("@ITEM92", SqlDbType.VarChar, 200, "");
                            dpc.SetParam("@ITEM93", SqlDbType.VarChar, 200, "合計");
                            dpc.SetParam("@ITEM94", SqlDbType.VarChar, 200, Util.FormatNum(GOKEI.SURYO, 1));
                            dpc.SetParam("@ITEM95", SqlDbType.VarChar, 200, Util.FormatNum(GOKEI.KINGAKU / GOKEI.SURYO));
                            tmpGokei = tmpGokeiKingaku + tmpGokeiShohizei;
                            dpc.SetParam("@ITEM96", SqlDbType.VarChar, 200, Util.FormatNum(tmpGokei)); // 合計
                            dpc.SetParam("@ITEM97", SqlDbType.VarChar, 200, Util.FormatNum(tmpGokei)); // 水揚金額
                            dpc.SetParam("@ITEM98", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["KOJO_KOMOKU1"])); // 税込販売手数料
                            dpc.SetParam("@ITEM99", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["KOJO_KOMOKU2"])); // 箱代
                            dpc.SetParam("@ITEM100", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["KOJO_KOMOKU3"])); // その他の控除
                            dpc.SetParam("@ITEM101", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["KOJO_KOMOKU4"])); // 未収・立替
                            dpc.SetParam("@ITEM102", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["KOJO_KOMOKU5"])); // 預り金積立
                            dpc.SetParam("@ITEM103", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["KOJO_KOMOKU6"])); // 本人積立金
                            kojoGokei = Util.ToDecimal(dtMainLoop.Rows[i]["KOJO_KOMOKU1"]) + Util.ToDecimal(dtMainLoop.Rows[i]["KOJO_KOMOKU2"])
                                              + Util.ToDecimal(dtMainLoop.Rows[i]["KOJO_KOMOKU3"]) + Util.ToDecimal(dtMainLoop.Rows[i]["KOJO_KOMOKU4"])
                                              + Util.ToDecimal(dtMainLoop.Rows[i]["KOJO_KOMOKU5"]) + Util.ToDecimal(dtMainLoop.Rows[i]["KOJO_KOMOKU6"]);
                            dpc.SetParam("@ITEM104", SqlDbType.VarChar, 200, Util.FormatNum(kojoGokei)); // 税込み控除額合計
                            dpc.SetParam("@ITEM105", SqlDbType.VarChar, 200, Util.FormatNum(tmpGokei - kojoGokei)); // 差引支払額

                            // 摘要欄への「口座無し」出力設定
                            if (dtMainLoop.Rows[i]["FUTSU_KOZA_BANGO"].ToString() == ""
                                || Util.ToDecimal(dtMainLoop.Rows[i]["FUTSU_KOZA_BANGO"]) < 1)
                            {
                                dpc.SetParam("@ITEM106", SqlDbType.VarChar, 200, "口座無し"); // 差引支払額
                            }
                            else
                            {
                                dpc.SetParam("@ITEM106", SqlDbType.VarChar, 200, ""); // 差引支払額
                            }
                            dpc.SetParam("@ITEM107", SqlDbType.VarChar, 200, SetJapaneseLunisolar(Util.ToDate(tmpSRWareki[5]))); // 旧暦
                            this.Dba.ModifyBySql(Util.ToString(sql), dpc);

                            // ページ数リセット
                            pageCount = 1;

                            // 合計金額リセット
                            GOKEI.Clear();

                            #endregion
                        }
                        else
                        {
                            #region データ登録
                            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                            dpc.SetParam("@SORT", SqlDbType.VarChar, 9, dbSORT);
                            dbSORT++;
                            dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, tmpSRWareki[5]); // セリ日(年)
                            dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpSRWareki[3]); // セリ日(月)
                            dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpSRWareki[4]); // セリ日(日)
                            dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SENSHU_CD"]); // 船主コード
                            dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SENSHU_NM"]); // 船主名称
                            dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, Util.PadZero(pageCount, 4)); // 改ページ用カウント
                            dpc.SetParam("@ITEM92", SqlDbType.VarChar, 200, "");
                            dpc.SetParam("@ITEM93", SqlDbType.VarChar, 200, "小計");
                            dpc.SetParam("@ITEM94", SqlDbType.VarChar, 200, Util.FormatNum(GOKEI.SURYO, 1));
                            dpc.SetParam("@ITEM95", SqlDbType.VarChar, 200, Util.FormatNum(GOKEI.KINGAKU / GOKEI.SURYO));
                            dpc.SetParam("@ITEM96", SqlDbType.VarChar, 200, Util.FormatNum(GOKEI.KINGAKU)); // 合計
                            dpc.SetParam("@ITEM97", SqlDbType.VarChar, 200, ""); // 水揚金額
                            dpc.SetParam("@ITEM98", SqlDbType.VarChar, 200, ""); // 税込販売手数料
                            dpc.SetParam("@ITEM99", SqlDbType.VarChar, 200, ""); // 箱代
                            dpc.SetParam("@ITEM100", SqlDbType.VarChar, 200, ""); // その他の控除
                            dpc.SetParam("@ITEM101", SqlDbType.VarChar, 200, ""); // 未収・立替
                            dpc.SetParam("@ITEM102", SqlDbType.VarChar, 200, ""); // 預り金積立
                            dpc.SetParam("@ITEM103", SqlDbType.VarChar, 200, ""); // 本人積立金
                            dpc.SetParam("@ITEM104", SqlDbType.VarChar, 200, ""); // 税込み控除額合計
                            dpc.SetParam("@ITEM105", SqlDbType.VarChar, 200, ""); // 差引支払額

                            // 摘要欄への「口座無し」出力設定
                            if (dtMainLoop.Rows[i]["FUTSU_KOZA_BANGO"].ToString() == ""
                                || Util.ToDecimal(dtMainLoop.Rows[i]["FUTSU_KOZA_BANGO"]) < 1)
                            {
                                dpc.SetParam("@ITEM106", SqlDbType.VarChar, 200, "口座無し"); // 差引支払額
                            }
                            else
                            {
                                dpc.SetParam("@ITEM106", SqlDbType.VarChar, 200, ""); // 差引支払額
                            }
                            dpc.SetParam("@ITEM107", SqlDbType.VarChar, 200, SetJapaneseLunisolar(Util.ToDate(tmpSRWareki[5]))); // 旧暦

                            this.Dba.ModifyBySql(Util.ToString(sql), dpc);

                            pageCount++;

                            #endregion
                        }

                        // 合計表示用ページのインサート
                        if (flgLast == 1 && pageCount == tmpPage)
                        {
                            #region データ登録
                            sql = new StringBuilder();
                            dpc = new DbParamCollection();
                            sql.Append("INSERT INTO PR_HN_TBL(");
                            sql.Append("  GUID");
                            sql.Append(" ,SORT");
                            sql.Append(" ,ITEM01");
                            sql.Append(" ,ITEM02");
                            sql.Append(" ,ITEM03");
                            sql.Append(" ,ITEM04");
                            sql.Append(" ,ITEM05");
                            sql.Append(" ,ITEM06");
                            sql.Append(" ,ITEM82");
                            sql.Append(" ,ITEM83");
                            sql.Append(" ,ITEM84");
                            sql.Append(" ,ITEM85");
                            sql.Append(" ,ITEM86");
                            sql.Append(" ,ITEM87");
                            sql.Append(" ,ITEM88");
                            sql.Append(" ,ITEM89");
                            sql.Append(" ,ITEM90");
                            sql.Append(" ,ITEM91");
                            sql.Append(" ,ITEM92");
                            sql.Append(" ,ITEM93");
                            sql.Append(" ,ITEM94");
                            sql.Append(" ,ITEM95");
                            sql.Append(" ,ITEM96");
                            sql.Append(" ,ITEM97");
                            sql.Append(" ,ITEM98");
                            sql.Append(" ,ITEM99");
                            sql.Append(" ,ITEM100");
                            sql.Append(" ,ITEM101");
                            sql.Append(" ,ITEM102");
                            sql.Append(" ,ITEM103");
                            sql.Append(" ,ITEM104");
                            sql.Append(" ,ITEM105");
                            sql.Append(" ,ITEM106");
                            sql.Append(" ,ITEM107");
                            sql.Append(") ");
                            sql.Append("VALUES(");
                            sql.Append("  @GUID");
                            sql.Append(" ,@SORT");
                            sql.Append(" ,@ITEM01");
                            sql.Append(" ,@ITEM02");
                            sql.Append(" ,@ITEM03");
                            sql.Append(" ,@ITEM04");
                            sql.Append(" ,@ITEM05");
                            sql.Append(" ,@ITEM06");
                            sql.Append(" ,@ITEM82");
                            sql.Append(" ,@ITEM83");
                            sql.Append(" ,@ITEM84");
                            sql.Append(" ,@ITEM85");
                            sql.Append(" ,@ITEM86");
                            sql.Append(" ,@ITEM87");
                            sql.Append(" ,@ITEM88");
                            sql.Append(" ,@ITEM89");
                            sql.Append(" ,@ITEM90");
                            sql.Append(" ,@ITEM91");
                            sql.Append(" ,@ITEM92");
                            sql.Append(" ,@ITEM93");
                            sql.Append(" ,@ITEM94");
                            sql.Append(" ,@ITEM95");
                            sql.Append(" ,@ITEM96");
                            sql.Append(" ,@ITEM97");
                            sql.Append(" ,@ITEM98");
                            sql.Append(" ,@ITEM99");
                            sql.Append(" ,@ITEM100");
                            sql.Append(" ,@ITEM101");
                            sql.Append(" ,@ITEM102");
                            sql.Append(" ,@ITEM103");
                            sql.Append(" ,@ITEM104");
                            sql.Append(" ,@ITEM105");
                            sql.Append(" ,@ITEM106");
                            sql.Append(" ,@ITEM107");
                            sql.Append(") ");

                            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                            dpc.SetParam("@SORT", SqlDbType.VarChar, 9, dbSORT);
                            dbSORT++;
                            dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, tmpSRWareki[5]); // セリ日(年)
                            dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpSRWareki[3]); // セリ日(月)
                            dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpSRWareki[4]); // セリ日(日)
                            dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SENSHU_CD"]); // 船主コード
                            dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SENSHU_NM"]); // 船主名称
                            dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, Util.PadZero(pageCount, 4)); // 改ページ用カウント
                            dpc.SetParam("@ITEM82", SqlDbType.VarChar, 200, "");
                            dpc.SetParam("@ITEM83", SqlDbType.VarChar, 200, "税抜合計");
                            dpc.SetParam("@ITEM84", SqlDbType.VarChar, 200, "");
                            dpc.SetParam("@ITEM85", SqlDbType.VarChar, 200, "");
                            dpc.SetParam("@ITEM86", SqlDbType.VarChar, 200, Util.FormatNum(tmpGokeiKingaku)); // 税抜合計
                            dpc.SetParam("@ITEM87", SqlDbType.VarChar, 200, "");
                            dpc.SetParam("@ITEM88", SqlDbType.VarChar, 200, "消費税");
                            dpc.SetParam("@ITEM89", SqlDbType.VarChar, 200, "");
                            dpc.SetParam("@ITEM90", SqlDbType.VarChar, 200, "");
                            dpc.SetParam("@ITEM91", SqlDbType.VarChar, 200, Util.FormatNum(tmpGokeiShohizei)); // 消費税
                            dpc.SetParam("@ITEM92", SqlDbType.VarChar, 200, "");
                            dpc.SetParam("@ITEM93", SqlDbType.VarChar, 200, "合計");
                            dpc.SetParam("@ITEM94", SqlDbType.VarChar, 200, Util.FormatNum(GOKEI.SURYO, 1));
                            dpc.SetParam("@ITEM95", SqlDbType.VarChar, 200, Util.FormatNum(GOKEI.KINGAKU / GOKEI.SURYO));
                            tmpGokei = tmpGokeiKingaku + tmpGokeiShohizei;
                            dpc.SetParam("@ITEM96", SqlDbType.VarChar, 200, Util.FormatNum(tmpGokei)); // 合計
                            dpc.SetParam("@ITEM97", SqlDbType.VarChar, 200, Util.FormatNum(tmpGokei)); // 水揚金額
                            dpc.SetParam("@ITEM98", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["KOJO_KOMOKU1"])); // 税込販売手数料
                            dpc.SetParam("@ITEM99", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["KOJO_KOMOKU2"])); // 箱代
                            dpc.SetParam("@ITEM100", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["KOJO_KOMOKU3"])); // その他の控除
                            dpc.SetParam("@ITEM101", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["KOJO_KOMOKU4"])); // 未収・立替
                            dpc.SetParam("@ITEM102", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["KOJO_KOMOKU5"])); // 預り金積立
                            dpc.SetParam("@ITEM103", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["KOJO_KOMOKU6"])); // 本人積立金
                            kojoGokei = Util.ToDecimal(dtMainLoop.Rows[i]["KOJO_KOMOKU1"]) + Util.ToDecimal(dtMainLoop.Rows[i]["KOJO_KOMOKU2"])
                                              + Util.ToDecimal(dtMainLoop.Rows[i]["KOJO_KOMOKU3"]) + Util.ToDecimal(dtMainLoop.Rows[i]["KOJO_KOMOKU4"])
                                              + Util.ToDecimal(dtMainLoop.Rows[i]["KOJO_KOMOKU5"]) + Util.ToDecimal(dtMainLoop.Rows[i]["KOJO_KOMOKU6"]);
                            dpc.SetParam("@ITEM104", SqlDbType.VarChar, 200, Util.FormatNum(kojoGokei)); // 税込み控除額合計
                            dpc.SetParam("@ITEM105", SqlDbType.VarChar, 200, Util.FormatNum(tmpGokei - kojoGokei)); // 差引支払額

                            // 摘要欄への「口座無し」出力設定
                            if (dtMainLoop.Rows[i]["FUTSU_KOZA_BANGO"].ToString() == ""
                                || Util.ToDecimal(dtMainLoop.Rows[i]["FUTSU_KOZA_BANGO"]) < 1)
                            {
                                dpc.SetParam("@ITEM106", SqlDbType.VarChar, 200, "口座無し"); // 差引支払額
                            }
                            else
                            {
                                dpc.SetParam("@ITEM106", SqlDbType.VarChar, 200, ""); // 差引支払額
                            }
                            dpc.SetParam("@ITEM107", SqlDbType.VarChar, 200, SetJapaneseLunisolar(Util.ToDate(tmpSRWareki[5]))); // 旧暦

                            this.Dba.ModifyBySql(Util.ToString(sql), dpc);

                            // ページ数リセット
                            pageCount = 1;

                            flgLast = 0;

                            // 合計金額リセット
                            GOKEI.Clear();
                            #endregion
                        }
                        flg = 0;
                        j = 7;
                        k = 7;

                        dpc = new DbParamCollection();

                    }
                    #endregion

                    i++;
                    before++;
                    next++;
                }
            }

            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
                "SORT",
                "PR_HN_TBL",
                "GUID = @GUID",
                dpc);

            bool dataFlag;
            if (tmpdtPR_HN_TBL.Rows.Count > 0)
            {
                dataFlag = true;
            }
            else
            {
                dataFlag = false;
            }

            return dataFlag;
        }

        //JapaneseLunisolarCalendar（日本の太陰太陽暦＝旧暦）
        private string SetJapaneseLunisolar(DateTime selectDay)
        {
             JapaneseLunisolarCalendar jpnOldDays = new JapaneseLunisolarCalendar();

            int oldMonth = jpnOldDays.GetMonth(selectDay); //旧暦の月を取得
            int oldDay = jpnOldDays.GetDayOfMonth(selectDay);//旧暦の日を取得　
            //閏月を取得
            int uruMonth = jpnOldDays.GetLeapMonth(
                    jpnOldDays.GetYear(selectDay),
                    jpnOldDays.GetEra(selectDay));
            //閏月がある場合の旧暦月の補正
            if ((uruMonth > 0) && (oldMonth - uruMonth >= 0))
            {
                oldMonth = oldMonth - 1;              //旧暦月の補正
            }
            return oldMonth.ToString() + "月" + " " + oldDay.ToString() + "日";
        }
        #endregion
    }
}
