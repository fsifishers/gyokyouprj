﻿namespace jp.co.fsi.han.hanr2141
{
    partial class HANR2141
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbxSeriDate = new System.Windows.Forms.GroupBox();
            this.lblSeriDateDayFr = new System.Windows.Forms.Label();
            this.lblSeriDateMonthFr = new System.Windows.Forms.Label();
            this.lblSeriDateYearFr = new System.Windows.Forms.Label();
            this.txtSeriDateDayFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtSeriDateYearFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtSeriDateMonthFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblSeriDateGengoFr = new System.Windows.Forms.Label();
            this.lblSeriDateFr = new System.Windows.Forms.Label();
            this.lblCodeBetSeri = new System.Windows.Forms.Label();
            this.lblSeriDateDayTo = new System.Windows.Forms.Label();
            this.lblSeriDateMonthTo = new System.Windows.Forms.Label();
            this.lblSeriDateYearTo = new System.Windows.Forms.Label();
            this.txtSeriDateDayTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtSeriDateYearTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtSeriDateMonthTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblSeriDateGengoTo = new System.Windows.Forms.Label();
            this.lblSeriDateTo = new System.Windows.Forms.Label();
            this.gbxFunanushiCd = new System.Windows.Forms.GroupBox();
            this.lblFunanushiCdTo = new System.Windows.Forms.Label();
            this.lblCodeBet = new System.Windows.Forms.Label();
            this.txtFunanushiCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblFunanushiCdFr = new System.Windows.Forms.Label();
            this.txtFunanushiCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.gbxShiharaiDate = new System.Windows.Forms.GroupBox();
            this.lblShiharaiDateDay = new System.Windows.Forms.Label();
            this.lblShiharaiDateMonth = new System.Windows.Forms.Label();
            this.lblShiharaiDateYear = new System.Windows.Forms.Label();
            this.txtShiharaiDateDay = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtShiharaiDateYear = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtShiharaiDateMonth = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShiharaiDateGengo = new System.Windows.Forms.Label();
            this.lblShiharaiDate = new System.Windows.Forms.Label();
            this.gbxMizuageShisho = new System.Windows.Forms.GroupBox();
            this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMizuageShishoNm = new System.Windows.Forms.Label();
            this.lblMizuageShisho = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.gbxSeriDate.SuspendLayout();
            this.gbxFunanushiCd.SuspendLayout();
            this.gbxShiharaiDate.SuspendLayout();
            this.gbxMizuageShisho.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.TabIndex = 999;
            this.lblTitle.Text = "";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Size = new System.Drawing.Size(847, 100);
            // 
            // gbxSeriDate
            // 
            this.gbxSeriDate.Controls.Add(this.lblSeriDateDayFr);
            this.gbxSeriDate.Controls.Add(this.lblSeriDateMonthFr);
            this.gbxSeriDate.Controls.Add(this.lblSeriDateYearFr);
            this.gbxSeriDate.Controls.Add(this.txtSeriDateDayFr);
            this.gbxSeriDate.Controls.Add(this.txtSeriDateYearFr);
            this.gbxSeriDate.Controls.Add(this.txtSeriDateMonthFr);
            this.gbxSeriDate.Controls.Add(this.lblSeriDateGengoFr);
            this.gbxSeriDate.Controls.Add(this.lblSeriDateFr);
            this.gbxSeriDate.Controls.Add(this.lblCodeBetSeri);
            this.gbxSeriDate.Controls.Add(this.lblSeriDateDayTo);
            this.gbxSeriDate.Controls.Add(this.lblSeriDateMonthTo);
            this.gbxSeriDate.Controls.Add(this.lblSeriDateYearTo);
            this.gbxSeriDate.Controls.Add(this.txtSeriDateDayTo);
            this.gbxSeriDate.Controls.Add(this.txtSeriDateYearTo);
            this.gbxSeriDate.Controls.Add(this.txtSeriDateMonthTo);
            this.gbxSeriDate.Controls.Add(this.lblSeriDateGengoTo);
            this.gbxSeriDate.Controls.Add(this.lblSeriDateTo);
            this.gbxSeriDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.gbxSeriDate.ForeColor = System.Drawing.Color.Black;
            this.gbxSeriDate.Location = new System.Drawing.Point(12, 223);
            this.gbxSeriDate.Name = "gbxSeriDate";
            this.gbxSeriDate.Size = new System.Drawing.Size(487, 72);
            this.gbxSeriDate.TabIndex = 2;
            this.gbxSeriDate.TabStop = false;
            this.gbxSeriDate.Text = "セリ日期間";
            // 
            // lblSeriDateDayFr
            // 
            this.lblSeriDateDayFr.BackColor = System.Drawing.Color.Silver;
            this.lblSeriDateDayFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblSeriDateDayFr.ForeColor = System.Drawing.Color.Black;
            this.lblSeriDateDayFr.Location = new System.Drawing.Point(198, 32);
            this.lblSeriDateDayFr.Name = "lblSeriDateDayFr";
            this.lblSeriDateDayFr.Size = new System.Drawing.Size(20, 18);
            this.lblSeriDateDayFr.TabIndex = 7;
            this.lblSeriDateDayFr.Text = "日";
            this.lblSeriDateDayFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSeriDateMonthFr
            // 
            this.lblSeriDateMonthFr.BackColor = System.Drawing.Color.Silver;
            this.lblSeriDateMonthFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblSeriDateMonthFr.ForeColor = System.Drawing.Color.Black;
            this.lblSeriDateMonthFr.Location = new System.Drawing.Point(143, 32);
            this.lblSeriDateMonthFr.Name = "lblSeriDateMonthFr";
            this.lblSeriDateMonthFr.Size = new System.Drawing.Size(15, 19);
            this.lblSeriDateMonthFr.TabIndex = 5;
            this.lblSeriDateMonthFr.Text = "月";
            this.lblSeriDateMonthFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSeriDateYearFr
            // 
            this.lblSeriDateYearFr.BackColor = System.Drawing.Color.Silver;
            this.lblSeriDateYearFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblSeriDateYearFr.ForeColor = System.Drawing.Color.Black;
            this.lblSeriDateYearFr.Location = new System.Drawing.Point(90, 31);
            this.lblSeriDateYearFr.Name = "lblSeriDateYearFr";
            this.lblSeriDateYearFr.Size = new System.Drawing.Size(17, 21);
            this.lblSeriDateYearFr.TabIndex = 3;
            this.lblSeriDateYearFr.Text = "年";
            this.lblSeriDateYearFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtSeriDateDayFr
            // 
            this.txtSeriDateDayFr.AutoSizeFromLength = false;
            this.txtSeriDateDayFr.DisplayLength = null;
            this.txtSeriDateDayFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtSeriDateDayFr.ForeColor = System.Drawing.Color.Black;
            this.txtSeriDateDayFr.Location = new System.Drawing.Point(165, 31);
            this.txtSeriDateDayFr.MaxLength = 2;
            this.txtSeriDateDayFr.Name = "txtSeriDateDayFr";
            this.txtSeriDateDayFr.Size = new System.Drawing.Size(30, 20);
            this.txtSeriDateDayFr.TabIndex = 6;
            this.txtSeriDateDayFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSeriDateDayFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtSeriDateDayFr_Validating);
            // 
            // txtSeriDateYearFr
            // 
            this.txtSeriDateYearFr.AutoSizeFromLength = false;
            this.txtSeriDateYearFr.DisplayLength = null;
            this.txtSeriDateYearFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtSeriDateYearFr.ForeColor = System.Drawing.Color.Black;
            this.txtSeriDateYearFr.Location = new System.Drawing.Point(58, 31);
            this.txtSeriDateYearFr.MaxLength = 2;
            this.txtSeriDateYearFr.Name = "txtSeriDateYearFr";
            this.txtSeriDateYearFr.Size = new System.Drawing.Size(30, 20);
            this.txtSeriDateYearFr.TabIndex = 2;
            this.txtSeriDateYearFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSeriDateYearFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtSeriDateYearFr_Validating);
            // 
            // txtSeriDateMonthFr
            // 
            this.txtSeriDateMonthFr.AutoSizeFromLength = false;
            this.txtSeriDateMonthFr.DisplayLength = null;
            this.txtSeriDateMonthFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtSeriDateMonthFr.ForeColor = System.Drawing.Color.Black;
            this.txtSeriDateMonthFr.Location = new System.Drawing.Point(111, 31);
            this.txtSeriDateMonthFr.MaxLength = 2;
            this.txtSeriDateMonthFr.Name = "txtSeriDateMonthFr";
            this.txtSeriDateMonthFr.Size = new System.Drawing.Size(30, 20);
            this.txtSeriDateMonthFr.TabIndex = 4;
            this.txtSeriDateMonthFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSeriDateMonthFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtSeriDateMonthFr_Validating);
            // 
            // lblSeriDateGengoFr
            // 
            this.lblSeriDateGengoFr.BackColor = System.Drawing.Color.Silver;
            this.lblSeriDateGengoFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSeriDateGengoFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblSeriDateGengoFr.ForeColor = System.Drawing.Color.Black;
            this.lblSeriDateGengoFr.Location = new System.Drawing.Point(13, 31);
            this.lblSeriDateGengoFr.Name = "lblSeriDateGengoFr";
            this.lblSeriDateGengoFr.Size = new System.Drawing.Size(41, 21);
            this.lblSeriDateGengoFr.TabIndex = 1;
            this.lblSeriDateGengoFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSeriDateFr
            // 
            this.lblSeriDateFr.BackColor = System.Drawing.Color.Silver;
            this.lblSeriDateFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblSeriDateFr.ForeColor = System.Drawing.Color.Black;
            this.lblSeriDateFr.Location = new System.Drawing.Point(10, 28);
            this.lblSeriDateFr.Name = "lblSeriDateFr";
            this.lblSeriDateFr.Size = new System.Drawing.Size(214, 27);
            this.lblSeriDateFr.TabIndex = 0;
            this.lblSeriDateFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCodeBetSeri
            // 
            this.lblCodeBetSeri.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblCodeBetSeri.ForeColor = System.Drawing.Color.Black;
            this.lblCodeBetSeri.Location = new System.Drawing.Point(233, 30);
            this.lblCodeBetSeri.Name = "lblCodeBetSeri";
            this.lblCodeBetSeri.Size = new System.Drawing.Size(18, 20);
            this.lblCodeBetSeri.TabIndex = 8;
            this.lblCodeBetSeri.Text = "～";
            this.lblCodeBetSeri.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSeriDateDayTo
            // 
            this.lblSeriDateDayTo.BackColor = System.Drawing.Color.Silver;
            this.lblSeriDateDayTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblSeriDateDayTo.ForeColor = System.Drawing.Color.Black;
            this.lblSeriDateDayTo.Location = new System.Drawing.Point(448, 32);
            this.lblSeriDateDayTo.Name = "lblSeriDateDayTo";
            this.lblSeriDateDayTo.Size = new System.Drawing.Size(20, 18);
            this.lblSeriDateDayTo.TabIndex = 16;
            this.lblSeriDateDayTo.Text = "日";
            this.lblSeriDateDayTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSeriDateMonthTo
            // 
            this.lblSeriDateMonthTo.BackColor = System.Drawing.Color.Silver;
            this.lblSeriDateMonthTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblSeriDateMonthTo.ForeColor = System.Drawing.Color.Black;
            this.lblSeriDateMonthTo.Location = new System.Drawing.Point(393, 32);
            this.lblSeriDateMonthTo.Name = "lblSeriDateMonthTo";
            this.lblSeriDateMonthTo.Size = new System.Drawing.Size(15, 19);
            this.lblSeriDateMonthTo.TabIndex = 14;
            this.lblSeriDateMonthTo.Text = "月";
            this.lblSeriDateMonthTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSeriDateYearTo
            // 
            this.lblSeriDateYearTo.BackColor = System.Drawing.Color.Silver;
            this.lblSeriDateYearTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblSeriDateYearTo.ForeColor = System.Drawing.Color.Black;
            this.lblSeriDateYearTo.Location = new System.Drawing.Point(340, 31);
            this.lblSeriDateYearTo.Name = "lblSeriDateYearTo";
            this.lblSeriDateYearTo.Size = new System.Drawing.Size(17, 21);
            this.lblSeriDateYearTo.TabIndex = 12;
            this.lblSeriDateYearTo.Text = "年";
            this.lblSeriDateYearTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtSeriDateDayTo
            // 
            this.txtSeriDateDayTo.AutoSizeFromLength = false;
            this.txtSeriDateDayTo.DisplayLength = null;
            this.txtSeriDateDayTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtSeriDateDayTo.ForeColor = System.Drawing.Color.Black;
            this.txtSeriDateDayTo.Location = new System.Drawing.Point(415, 31);
            this.txtSeriDateDayTo.MaxLength = 2;
            this.txtSeriDateDayTo.Name = "txtSeriDateDayTo";
            this.txtSeriDateDayTo.Size = new System.Drawing.Size(30, 20);
            this.txtSeriDateDayTo.TabIndex = 15;
            this.txtSeriDateDayTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSeriDateDayTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtSeriDateDayTo_Validating);
            // 
            // txtSeriDateYearTo
            // 
            this.txtSeriDateYearTo.AutoSizeFromLength = false;
            this.txtSeriDateYearTo.DisplayLength = null;
            this.txtSeriDateYearTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtSeriDateYearTo.ForeColor = System.Drawing.Color.Black;
            this.txtSeriDateYearTo.Location = new System.Drawing.Point(308, 31);
            this.txtSeriDateYearTo.MaxLength = 2;
            this.txtSeriDateYearTo.Name = "txtSeriDateYearTo";
            this.txtSeriDateYearTo.Size = new System.Drawing.Size(30, 20);
            this.txtSeriDateYearTo.TabIndex = 11;
            this.txtSeriDateYearTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSeriDateYearTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtSeriDateYearTo_Validating);
            // 
            // txtSeriDateMonthTo
            // 
            this.txtSeriDateMonthTo.AutoSizeFromLength = false;
            this.txtSeriDateMonthTo.DisplayLength = null;
            this.txtSeriDateMonthTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtSeriDateMonthTo.ForeColor = System.Drawing.Color.Black;
            this.txtSeriDateMonthTo.Location = new System.Drawing.Point(361, 31);
            this.txtSeriDateMonthTo.MaxLength = 2;
            this.txtSeriDateMonthTo.Name = "txtSeriDateMonthTo";
            this.txtSeriDateMonthTo.Size = new System.Drawing.Size(30, 20);
            this.txtSeriDateMonthTo.TabIndex = 13;
            this.txtSeriDateMonthTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSeriDateMonthTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtSeriDateMonthTo_Validating);
            // 
            // lblSeriDateGengoTo
            // 
            this.lblSeriDateGengoTo.BackColor = System.Drawing.Color.Silver;
            this.lblSeriDateGengoTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSeriDateGengoTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblSeriDateGengoTo.ForeColor = System.Drawing.Color.Black;
            this.lblSeriDateGengoTo.Location = new System.Drawing.Point(263, 31);
            this.lblSeriDateGengoTo.Name = "lblSeriDateGengoTo";
            this.lblSeriDateGengoTo.Size = new System.Drawing.Size(41, 21);
            this.lblSeriDateGengoTo.TabIndex = 10;
            this.lblSeriDateGengoTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSeriDateTo
            // 
            this.lblSeriDateTo.BackColor = System.Drawing.Color.Silver;
            this.lblSeriDateTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblSeriDateTo.ForeColor = System.Drawing.Color.Black;
            this.lblSeriDateTo.Location = new System.Drawing.Point(260, 28);
            this.lblSeriDateTo.Name = "lblSeriDateTo";
            this.lblSeriDateTo.Size = new System.Drawing.Size(214, 27);
            this.lblSeriDateTo.TabIndex = 9;
            this.lblSeriDateTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // gbxFunanushiCd
            // 
            this.gbxFunanushiCd.Controls.Add(this.lblFunanushiCdTo);
            this.gbxFunanushiCd.Controls.Add(this.lblCodeBet);
            this.gbxFunanushiCd.Controls.Add(this.txtFunanushiCdFr);
            this.gbxFunanushiCd.Controls.Add(this.lblFunanushiCdFr);
            this.gbxFunanushiCd.Controls.Add(this.txtFunanushiCdTo);
            this.gbxFunanushiCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.gbxFunanushiCd.ForeColor = System.Drawing.Color.Black;
            this.gbxFunanushiCd.Location = new System.Drawing.Point(12, 319);
            this.gbxFunanushiCd.Name = "gbxFunanushiCd";
            this.gbxFunanushiCd.Size = new System.Drawing.Size(573, 85);
            this.gbxFunanushiCd.TabIndex = 3;
            this.gbxFunanushiCd.TabStop = false;
            this.gbxFunanushiCd.Text = "船主CD範囲";
            // 
            // lblFunanushiCdTo
            // 
            this.lblFunanushiCdTo.BackColor = System.Drawing.Color.Silver;
            this.lblFunanushiCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFunanushiCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblFunanushiCdTo.ForeColor = System.Drawing.Color.Black;
            this.lblFunanushiCdTo.Location = new System.Drawing.Point(346, 36);
            this.lblFunanushiCdTo.Name = "lblFunanushiCdTo";
            this.lblFunanushiCdTo.Size = new System.Drawing.Size(212, 20);
            this.lblFunanushiCdTo.TabIndex = 4;
            this.lblFunanushiCdTo.Text = "最　後";
            this.lblFunanushiCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCodeBet
            // 
            this.lblCodeBet.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblCodeBet.ForeColor = System.Drawing.Color.Black;
            this.lblCodeBet.Location = new System.Drawing.Point(275, 35);
            this.lblCodeBet.Name = "lblCodeBet";
            this.lblCodeBet.Size = new System.Drawing.Size(18, 20);
            this.lblCodeBet.TabIndex = 2;
            this.lblCodeBet.Text = "～";
            this.lblCodeBet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFunanushiCdFr
            // 
            this.txtFunanushiCdFr.AutoSizeFromLength = false;
            this.txtFunanushiCdFr.DisplayLength = null;
            this.txtFunanushiCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtFunanushiCdFr.ForeColor = System.Drawing.Color.Black;
            this.txtFunanushiCdFr.Location = new System.Drawing.Point(14, 35);
            this.txtFunanushiCdFr.MaxLength = 4;
            this.txtFunanushiCdFr.Name = "txtFunanushiCdFr";
            this.txtFunanushiCdFr.Size = new System.Drawing.Size(40, 20);
            this.txtFunanushiCdFr.TabIndex = 0;
            this.txtFunanushiCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFunanushiCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtCodeFr_Validating);
            // 
            // lblFunanushiCdFr
            // 
            this.lblFunanushiCdFr.BackColor = System.Drawing.Color.Silver;
            this.lblFunanushiCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFunanushiCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblFunanushiCdFr.ForeColor = System.Drawing.Color.Black;
            this.lblFunanushiCdFr.Location = new System.Drawing.Point(57, 36);
            this.lblFunanushiCdFr.Name = "lblFunanushiCdFr";
            this.lblFunanushiCdFr.Size = new System.Drawing.Size(212, 20);
            this.lblFunanushiCdFr.TabIndex = 1;
            this.lblFunanushiCdFr.Text = "先　頭";
            this.lblFunanushiCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFunanushiCdTo
            // 
            this.txtFunanushiCdTo.AutoSizeFromLength = false;
            this.txtFunanushiCdTo.DisplayLength = null;
            this.txtFunanushiCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtFunanushiCdTo.ForeColor = System.Drawing.Color.Black;
            this.txtFunanushiCdTo.Location = new System.Drawing.Point(303, 35);
            this.txtFunanushiCdTo.MaxLength = 4;
            this.txtFunanushiCdTo.Name = "txtFunanushiCdTo";
            this.txtFunanushiCdTo.Size = new System.Drawing.Size(40, 20);
            this.txtFunanushiCdTo.TabIndex = 3;
            this.txtFunanushiCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFunanushiCdTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtFunanushiCdTo_KeyDown);
            this.txtFunanushiCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtCodeTo_Validating);
            // 
            // gbxShiharaiDate
            // 
            this.gbxShiharaiDate.Controls.Add(this.lblShiharaiDateDay);
            this.gbxShiharaiDate.Controls.Add(this.lblShiharaiDateMonth);
            this.gbxShiharaiDate.Controls.Add(this.lblShiharaiDateYear);
            this.gbxShiharaiDate.Controls.Add(this.txtShiharaiDateDay);
            this.gbxShiharaiDate.Controls.Add(this.txtShiharaiDateYear);
            this.gbxShiharaiDate.Controls.Add(this.txtShiharaiDateMonth);
            this.gbxShiharaiDate.Controls.Add(this.lblShiharaiDateGengo);
            this.gbxShiharaiDate.Controls.Add(this.lblShiharaiDate);
            this.gbxShiharaiDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.gbxShiharaiDate.ForeColor = System.Drawing.Color.Black;
            this.gbxShiharaiDate.Location = new System.Drawing.Point(12, 135);
            this.gbxShiharaiDate.Name = "gbxShiharaiDate";
            this.gbxShiharaiDate.Size = new System.Drawing.Size(288, 72);
            this.gbxShiharaiDate.TabIndex = 1;
            this.gbxShiharaiDate.TabStop = false;
            this.gbxShiharaiDate.Text = "支払日";
            // 
            // lblShiharaiDateDay
            // 
            this.lblShiharaiDateDay.BackColor = System.Drawing.Color.Silver;
            this.lblShiharaiDateDay.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblShiharaiDateDay.ForeColor = System.Drawing.Color.Black;
            this.lblShiharaiDateDay.Location = new System.Drawing.Point(198, 31);
            this.lblShiharaiDateDay.Name = "lblShiharaiDateDay";
            this.lblShiharaiDateDay.Size = new System.Drawing.Size(20, 18);
            this.lblShiharaiDateDay.TabIndex = 7;
            this.lblShiharaiDateDay.Text = "日";
            this.lblShiharaiDateDay.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShiharaiDateMonth
            // 
            this.lblShiharaiDateMonth.BackColor = System.Drawing.Color.Silver;
            this.lblShiharaiDateMonth.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblShiharaiDateMonth.ForeColor = System.Drawing.Color.Black;
            this.lblShiharaiDateMonth.Location = new System.Drawing.Point(143, 31);
            this.lblShiharaiDateMonth.Name = "lblShiharaiDateMonth";
            this.lblShiharaiDateMonth.Size = new System.Drawing.Size(15, 19);
            this.lblShiharaiDateMonth.TabIndex = 5;
            this.lblShiharaiDateMonth.Text = "月";
            this.lblShiharaiDateMonth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShiharaiDateYear
            // 
            this.lblShiharaiDateYear.BackColor = System.Drawing.Color.Silver;
            this.lblShiharaiDateYear.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblShiharaiDateYear.ForeColor = System.Drawing.Color.Black;
            this.lblShiharaiDateYear.Location = new System.Drawing.Point(90, 30);
            this.lblShiharaiDateYear.Name = "lblShiharaiDateYear";
            this.lblShiharaiDateYear.Size = new System.Drawing.Size(17, 21);
            this.lblShiharaiDateYear.TabIndex = 3;
            this.lblShiharaiDateYear.Text = "年";
            this.lblShiharaiDateYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShiharaiDateDay
            // 
            this.txtShiharaiDateDay.AutoSizeFromLength = false;
            this.txtShiharaiDateDay.DisplayLength = null;
            this.txtShiharaiDateDay.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtShiharaiDateDay.ForeColor = System.Drawing.Color.Black;
            this.txtShiharaiDateDay.Location = new System.Drawing.Point(165, 30);
            this.txtShiharaiDateDay.MaxLength = 2;
            this.txtShiharaiDateDay.Name = "txtShiharaiDateDay";
            this.txtShiharaiDateDay.Size = new System.Drawing.Size(30, 20);
            this.txtShiharaiDateDay.TabIndex = 6;
            this.txtShiharaiDateDay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShiharaiDateDay.Validating += new System.ComponentModel.CancelEventHandler(this.txtShiharaiDateDay_Validating);
            // 
            // txtShiharaiDateYear
            // 
            this.txtShiharaiDateYear.AutoSizeFromLength = false;
            this.txtShiharaiDateYear.DisplayLength = null;
            this.txtShiharaiDateYear.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtShiharaiDateYear.ForeColor = System.Drawing.Color.Black;
            this.txtShiharaiDateYear.Location = new System.Drawing.Point(58, 30);
            this.txtShiharaiDateYear.MaxLength = 2;
            this.txtShiharaiDateYear.Name = "txtShiharaiDateYear";
            this.txtShiharaiDateYear.Size = new System.Drawing.Size(30, 20);
            this.txtShiharaiDateYear.TabIndex = 2;
            this.txtShiharaiDateYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShiharaiDateYear.Validating += new System.ComponentModel.CancelEventHandler(this.txtShiharaiDateYear_Validating);
            // 
            // txtShiharaiDateMonth
            // 
            this.txtShiharaiDateMonth.AutoSizeFromLength = false;
            this.txtShiharaiDateMonth.DisplayLength = null;
            this.txtShiharaiDateMonth.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtShiharaiDateMonth.ForeColor = System.Drawing.Color.Black;
            this.txtShiharaiDateMonth.Location = new System.Drawing.Point(111, 30);
            this.txtShiharaiDateMonth.MaxLength = 2;
            this.txtShiharaiDateMonth.Name = "txtShiharaiDateMonth";
            this.txtShiharaiDateMonth.Size = new System.Drawing.Size(30, 20);
            this.txtShiharaiDateMonth.TabIndex = 4;
            this.txtShiharaiDateMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShiharaiDateMonth.Validating += new System.ComponentModel.CancelEventHandler(this.txtShiharaiDateMonth_Validating);
            // 
            // lblShiharaiDateGengo
            // 
            this.lblShiharaiDateGengo.BackColor = System.Drawing.Color.Silver;
            this.lblShiharaiDateGengo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShiharaiDateGengo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblShiharaiDateGengo.ForeColor = System.Drawing.Color.Black;
            this.lblShiharaiDateGengo.Location = new System.Drawing.Point(13, 30);
            this.lblShiharaiDateGengo.Name = "lblShiharaiDateGengo";
            this.lblShiharaiDateGengo.Size = new System.Drawing.Size(41, 21);
            this.lblShiharaiDateGengo.TabIndex = 1;
            this.lblShiharaiDateGengo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShiharaiDate
            // 
            this.lblShiharaiDate.BackColor = System.Drawing.Color.Silver;
            this.lblShiharaiDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblShiharaiDate.ForeColor = System.Drawing.Color.Black;
            this.lblShiharaiDate.Location = new System.Drawing.Point(10, 27);
            this.lblShiharaiDate.Name = "lblShiharaiDate";
            this.lblShiharaiDate.Size = new System.Drawing.Size(214, 27);
            this.lblShiharaiDate.TabIndex = 0;
            this.lblShiharaiDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // gbxMizuageShisho
            // 
            this.gbxMizuageShisho.Controls.Add(this.txtMizuageShishoCd);
            this.gbxMizuageShisho.Controls.Add(this.lblMizuageShishoNm);
            this.gbxMizuageShisho.Controls.Add(this.lblMizuageShisho);
            this.gbxMizuageShisho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.gbxMizuageShisho.ForeColor = System.Drawing.Color.Black;
            this.gbxMizuageShisho.Location = new System.Drawing.Point(12, 47);
            this.gbxMizuageShisho.Name = "gbxMizuageShisho";
            this.gbxMizuageShisho.Size = new System.Drawing.Size(381, 77);
            this.gbxMizuageShisho.TabIndex = 0;
            this.gbxMizuageShisho.TabStop = false;
            this.gbxMizuageShisho.Text = "水揚支所";
            // 
            // txtMizuageShishoCd
            // 
            this.txtMizuageShishoCd.AutoSizeFromLength = true;
            this.txtMizuageShishoCd.DisplayLength = null;
            this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtMizuageShishoCd.Location = new System.Drawing.Point(93, 33);
            this.txtMizuageShishoCd.MaxLength = 5;
            this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
            this.txtMizuageShishoCd.Size = new System.Drawing.Size(51, 20);
            this.txtMizuageShishoCd.TabIndex = 1;
            this.txtMizuageShishoCd.TabStop = false;
            this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
            // 
            // lblMizuageShishoNm
            // 
            this.lblMizuageShishoNm.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMizuageShishoNm.Location = new System.Drawing.Point(149, 33);
            this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
            this.lblMizuageShishoNm.Size = new System.Drawing.Size(212, 20);
            this.lblMizuageShishoNm.TabIndex = 2;
            this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMizuageShisho
            // 
            this.lblMizuageShisho.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShisho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShisho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblMizuageShisho.Location = new System.Drawing.Point(10, 31);
            this.lblMizuageShisho.Name = "lblMizuageShisho";
            this.lblMizuageShisho.Size = new System.Drawing.Size(354, 25);
            this.lblMizuageShisho.TabIndex = 0;
            this.lblMizuageShisho.Text = "水揚支所";
            this.lblMizuageShisho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // HANR2141
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 638);
            this.Controls.Add(this.gbxMizuageShisho);
            this.Controls.Add(this.gbxShiharaiDate);
            this.Controls.Add(this.gbxFunanushiCd);
            this.Controls.Add(this.gbxSeriDate);
            this.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "HANR2141";
            this.Text = "";
            this.Controls.SetChildIndex(this.gbxSeriDate, 0);
            this.Controls.SetChildIndex(this.gbxFunanushiCd, 0);
            this.Controls.SetChildIndex(this.gbxShiharaiDate, 0);
            this.Controls.SetChildIndex(this.gbxMizuageShisho, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.pnlDebug.ResumeLayout(false);
            this.gbxSeriDate.ResumeLayout(false);
            this.gbxSeriDate.PerformLayout();
            this.gbxFunanushiCd.ResumeLayout(false);
            this.gbxFunanushiCd.PerformLayout();
            this.gbxShiharaiDate.ResumeLayout(false);
            this.gbxShiharaiDate.PerformLayout();
            this.gbxMizuageShisho.ResumeLayout(false);
            this.gbxMizuageShisho.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbxSeriDate;
        private jp.co.fsi.common.controls.FsiTextBox txtSeriDateYearTo;
        private System.Windows.Forms.Label lblSeriDateGengoTo;
        private System.Windows.Forms.Label lblSeriDateTo;
        private System.Windows.Forms.Label lblSeriDateDayTo;
        private System.Windows.Forms.Label lblSeriDateMonthTo;
        private System.Windows.Forms.Label lblSeriDateYearTo;
        private jp.co.fsi.common.controls.FsiTextBox txtSeriDateDayTo;
        private jp.co.fsi.common.controls.FsiTextBox txtSeriDateMonthTo;
        private System.Windows.Forms.GroupBox gbxFunanushiCd;
        private System.Windows.Forms.Label lblFunanushiCdTo;
        private System.Windows.Forms.Label lblCodeBet;
        private common.controls.FsiTextBox txtFunanushiCdFr;
        private System.Windows.Forms.Label lblFunanushiCdFr;
        private common.controls.FsiTextBox txtFunanushiCdTo;
        private System.Windows.Forms.GroupBox gbxShiharaiDate;
        private System.Windows.Forms.Label lblShiharaiDateDay;
        private System.Windows.Forms.Label lblShiharaiDateMonth;
        private System.Windows.Forms.Label lblShiharaiDateYear;
        private common.controls.FsiTextBox txtShiharaiDateDay;
        private common.controls.FsiTextBox txtShiharaiDateYear;
        private common.controls.FsiTextBox txtShiharaiDateMonth;
        private System.Windows.Forms.Label lblShiharaiDateGengo;
        private System.Windows.Forms.Label lblShiharaiDate;
        private System.Windows.Forms.GroupBox gbxMizuageShisho;
        private common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private System.Windows.Forms.Label lblMizuageShisho;
        private System.Windows.Forms.Label lblSeriDateDayFr;
        private System.Windows.Forms.Label lblSeriDateMonthFr;
        private System.Windows.Forms.Label lblSeriDateYearFr;
        private common.controls.FsiTextBox txtSeriDateDayFr;
        private common.controls.FsiTextBox txtSeriDateYearFr;
        private common.controls.FsiTextBox txtSeriDateMonthFr;
        private System.Windows.Forms.Label lblSeriDateGengoFr;
        private System.Windows.Forms.Label lblSeriDateFr;
        private System.Windows.Forms.Label lblCodeBetSeri;

    }
}