﻿namespace jp.co.fsi.hn.hndr1101
{
    /// <summary>
    /// HNSR1011Rの概要の説明です。
    /// </summary>
    partial class HNDR11011R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(HNDR11011R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.lblSeribi = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtSeriDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtPrintDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblSakusei = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblYamaNo = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSenshuCD = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSenshuNM = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblGyoshuCD = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblMizuageSuryou = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTanka = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblUriageGingaku = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblShouhiZei = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblGoukeiKingaku = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblGyoshuNM = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtYamaNo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGyoshu = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtShouhiZei = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGoukeiKingaku = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSenshuCD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSenshuNM = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGyoshuCD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.reportHeader1 = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.reportFooter1 = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.ghMSenshu = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.gfMSenshu = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.lblShoukei = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtMMizuageSuryou = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMUriageKingaku = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMShouhiZei = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMGhoukeiKingaku = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMKensu = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblKensu = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtMTanka = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.ghSSenshu = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.gfSSenshu = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtSMizuageSuryou = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSUriageKingaku = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSShouhiZei = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSGhoukeiKingaku = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSTanka = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            ((System.ComponentModel.ISupportInitialize)(this.lblSeribi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSeriDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrintDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSakusei)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblYamaNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSenshuCD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSenshuNM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGyoshuCD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMizuageSuryou)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTanka)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblUriageGingaku)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblShouhiZei)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGoukeiKingaku)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGyoshuNM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYamaNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyoshu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShouhiZei)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGoukeiKingaku)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSenshuCD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSenshuNM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyoshuCD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblShoukei)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMMizuageSuryou)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMUriageKingaku)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMShouhiZei)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMGhoukeiKingaku)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMKensu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKensu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMTanka)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSMizuageSuryou)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSUriageKingaku)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSShouhiZei)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSGhoukeiKingaku)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSTanka)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.CanGrow = false;
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblSeribi,
            this.txtSeriDate,
            this.lblTitle,
            this.txtPrintDate,
            this.lblSakusei,
            this.txtPage,
            this.lblPage,
            this.line1,
            this.lblYamaNo,
            this.lblSenshuCD,
            this.lblSenshuNM,
            this.lblGyoshuCD,
            this.lblMizuageSuryou,
            this.lblTanka,
            this.lblUriageGingaku,
            this.lblShouhiZei,
            this.lblGoukeiKingaku,
            this.lblGyoshuNM});
            this.pageHeader.Height = 1.052083F;
            this.pageHeader.Name = "pageHeader";
            // 
            // lblSeribi
            // 
            this.lblSeribi.Height = 0.1688976F;
            this.lblSeribi.HyperLink = null;
            this.lblSeribi.Left = 0.07874016F;
            this.lblSeribi.MultiLine = false;
            this.lblSeribi.Name = "lblSeribi";
            this.lblSeribi.Style = "font-family: ＭＳ 明朝; font-size: 10pt; vertical-align: middle";
            this.lblSeribi.Text = "セリ日";
            this.lblSeribi.Top = 0.2362205F;
            this.lblSeribi.Width = 0.5102364F;
            // 
            // txtSeriDate
            // 
            this.txtSeriDate.DataField = "ITEM01";
            this.txtSeriDate.Height = 0.2F;
            this.txtSeriDate.Left = 0.5889765F;
            this.txtSeriDate.MultiLine = false;
            this.txtSeriDate.Name = "txtSeriDate";
            this.txtSeriDate.OutputFormat = resources.GetString("txtSeriDate.OutputFormat");
            this.txtSeriDate.Style = "font-family: ＭＳ 明朝; font-size: 10pt; vertical-align: middle";
            this.txtSeriDate.SummaryGroup = "ghMSenshu";
            this.txtSeriDate.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtSeriDate.Text = "YYYY/MM/DD";
            this.txtSeriDate.Top = 0.2362205F;
            this.txtSeriDate.Width = 0.7708662F;
            // 
            // lblTitle
            // 
            this.lblTitle.Height = 0.2106299F;
            this.lblTitle.HyperLink = null;
            this.lblTitle.Left = 4.188977F;
            this.lblTitle.MultiLine = false;
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Style = "font-family: ＭＳ 明朝; font-size: 15.75pt; font-weight: bold; text-align: center; ve" +
    "rtical-align: middle";
            this.lblTitle.Text = "仲買人別売上明細書";
            this.lblTitle.Top = 0F;
            this.lblTitle.Width = 2.415354F;
            // 
            // txtPrintDate
            // 
            this.txtPrintDate.DataField = "ITEM12";
            this.txtPrintDate.Height = 0.2F;
            this.txtPrintDate.Left = 8.501575F;
            this.txtPrintDate.MultiLine = false;
            this.txtPrintDate.Name = "txtPrintDate";
            this.txtPrintDate.OutputFormat = resources.GetString("txtPrintDate.OutputFormat");
            this.txtPrintDate.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle; d" +
    "do-char-set: 1";
            this.txtPrintDate.Text = "YYYY/MM/DD";
            this.txtPrintDate.Top = 0.2051181F;
            this.txtPrintDate.Width = 1.166142F;
            // 
            // lblSakusei
            // 
            this.lblSakusei.Height = 0.1688976F;
            this.lblSakusei.HyperLink = null;
            this.lblSakusei.Left = 9.761816F;
            this.lblSakusei.MultiLine = false;
            this.lblSakusei.Name = "lblSakusei";
            this.lblSakusei.Style = "font-family: ＭＳ 明朝; font-size: 10pt; vertical-align: middle; ddo-char-set: 1";
            this.lblSakusei.Text = "作成";
            this.lblSakusei.Top = 0.2051181F;
            this.lblSakusei.Width = 0.3125987F;
            // 
            // txtPage
            // 
            this.txtPage.Height = 0.2F;
            this.txtPage.Left = 10.16143F;
            this.txtPage.MultiLine = false;
            this.txtPage.Name = "txtPage";
            this.txtPage.OutputFormat = resources.GetString("txtPage.OutputFormat");
            this.txtPage.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle; d" +
    "do-char-set: 1";
            this.txtPage.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtPage.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.txtPage.Text = "ZZ9";
            this.txtPage.Top = 0.1740157F;
            this.txtPage.Width = 0.3334641F;
            // 
            // lblPage
            // 
            this.lblPage.Height = 0.1688976F;
            this.lblPage.HyperLink = null;
            this.lblPage.Left = 10.49489F;
            this.lblPage.MultiLine = false;
            this.lblPage.Name = "lblPage";
            this.lblPage.Style = "font-family: ＭＳ 明朝; font-size: 10pt; vertical-align: middle";
            this.lblPage.Text = "頁";
            this.lblPage.Top = 0.1740157F;
            this.lblPage.Width = 0.1562996F;
            // 
            // line1
            // 
            this.line1.Height = 0F;
            this.line1.Left = 0F;
            this.line1.LineWeight = 1F;
            this.line1.Name = "line1";
            this.line1.Top = 1.035039F;
            this.line1.Width = 12.55906F;
            this.line1.X1 = 0F;
            this.line1.X2 = 12.55906F;
            this.line1.Y1 = 1.035039F;
            this.line1.Y2 = 1.035039F;
            // 
            // lblYamaNo
            // 
            this.lblYamaNo.Height = 0.1688976F;
            this.lblYamaNo.HyperLink = null;
            this.lblYamaNo.Left = 2.543307F;
            this.lblYamaNo.Name = "lblYamaNo";
            this.lblYamaNo.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: right; vert" +
    "ical-align: middle";
            this.lblYamaNo.Text = "セリNo";
            this.lblYamaNo.Top = 0.8661418F;
            this.lblYamaNo.Width = 0.6354332F;
            // 
            // lblSenshuCD
            // 
            this.lblSenshuCD.Height = 0.1688976F;
            this.lblSenshuCD.HyperLink = null;
            this.lblSenshuCD.Left = 0.07874016F;
            this.lblSenshuCD.Name = "lblSenshuCD";
            this.lblSenshuCD.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: right; vert" +
    "ical-align: middle";
            this.lblSenshuCD.Text = "コード";
            this.lblSenshuCD.Top = 0.8661418F;
            this.lblSenshuCD.Width = 0.4480315F;
            // 
            // lblSenshuNM
            // 
            this.lblSenshuNM.Height = 0.1688976F;
            this.lblSenshuNM.HyperLink = null;
            this.lblSenshuNM.Left = 0.5889764F;
            this.lblSenshuNM.Name = "lblSenshuNM";
            this.lblSenshuNM.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; vertical-align: middle";
            this.lblSenshuNM.Text = "仲買人名称";
            this.lblSenshuNM.Top = 0.8661418F;
            this.lblSenshuNM.Width = 0.7708663F;
            // 
            // lblGyoshuCD
            // 
            this.lblGyoshuCD.Height = 0.1688976F;
            this.lblGyoshuCD.HyperLink = null;
            this.lblGyoshuCD.Left = 3.305512F;
            this.lblGyoshuCD.Name = "lblGyoshuCD";
            this.lblGyoshuCD.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: right; vert" +
    "ical-align: middle";
            this.lblGyoshuCD.Text = "漁種CD";
            this.lblGyoshuCD.Top = 0.8661418F;
            this.lblGyoshuCD.Width = 0.4480315F;
            // 
            // lblMizuageSuryou
            // 
            this.lblMizuageSuryou.Height = 0.1688976F;
            this.lblMizuageSuryou.HyperLink = null;
            this.lblMizuageSuryou.Left = 6.198426F;
            this.lblMizuageSuryou.Name = "lblMizuageSuryou";
            this.lblMizuageSuryou.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: right; vert" +
    "ical-align: middle";
            this.lblMizuageSuryou.Text = "水揚数量Kg";
            this.lblMizuageSuryou.Top = 0.8661418F;
            this.lblMizuageSuryou.Width = 0.7622048F;
            // 
            // lblTanka
            // 
            this.lblTanka.Height = 0.1688976F;
            this.lblTanka.HyperLink = null;
            this.lblTanka.Left = 7.089764F;
            this.lblTanka.Name = "lblTanka";
            this.lblTanka.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: right; vert" +
    "ical-align: middle";
            this.lblTanka.Text = "単価";
            this.lblTanka.Top = 0.8661418F;
            this.lblTanka.Width = 0.5314965F;
            // 
            // lblUriageGingaku
            // 
            this.lblUriageGingaku.Height = 0.1688976F;
            this.lblUriageGingaku.HyperLink = null;
            this.lblUriageGingaku.Left = 7.825985F;
            this.lblUriageGingaku.Name = "lblUriageGingaku";
            this.lblUriageGingaku.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: right; vert" +
    "ical-align: middle";
            this.lblUriageGingaku.Text = "売上金額";
            this.lblUriageGingaku.Top = 0.8661418F;
            this.lblUriageGingaku.Width = 0.802362F;
            // 
            // lblShouhiZei
            // 
            this.lblShouhiZei.Height = 0.1688976F;
            this.lblShouhiZei.HyperLink = null;
            this.lblShouhiZei.Left = 8.741339F;
            this.lblShouhiZei.Name = "lblShouhiZei";
            this.lblShouhiZei.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: right; vert" +
    "ical-align: middle";
            this.lblShouhiZei.Text = "消費税";
            this.lblShouhiZei.Top = 0.8661417F;
            this.lblShouhiZei.Width = 0.802362F;
            // 
            // lblGoukeiKingaku
            // 
            this.lblGoukeiKingaku.Height = 0.1688976F;
            this.lblGoukeiKingaku.HyperLink = null;
            this.lblGoukeiKingaku.Left = 9.69252F;
            this.lblGoukeiKingaku.Name = "lblGoukeiKingaku";
            this.lblGoukeiKingaku.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: right; vert" +
    "ical-align: middle";
            this.lblGoukeiKingaku.Text = "合計金額";
            this.lblGoukeiKingaku.Top = 0.8661418F;
            this.lblGoukeiKingaku.Width = 0.8023624F;
            // 
            // lblGyoshuNM
            // 
            this.lblGyoshuNM.Height = 0.1688976F;
            this.lblGyoshuNM.HyperLink = null;
            this.lblGyoshuNM.Left = 3.969685F;
            this.lblGyoshuNM.Name = "lblGyoshuNM";
            this.lblGyoshuNM.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; vertical-align: middle";
            this.lblGyoshuNM.Text = "魚種名称";
            this.lblGyoshuNM.Top = 0.8661418F;
            this.lblGyoshuNM.Width = 0.6255915F;
            // 
            // detail
            // 
            this.detail.CanGrow = false;
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtYamaNo,
            this.txtGyoshu,
            this.textBox7,
            this.textBox8,
            this.textBox9,
            this.txtShouhiZei,
            this.txtGoukeiKingaku,
            this.txtSenshuCD,
            this.txtSenshuNM,
            this.txtGyoshuCD,
            this.textBox2});
            this.detail.Height = 0.2188976F;
            this.detail.KeepTogether = true;
            this.detail.Name = "detail";
            this.detail.Format += new System.EventHandler(this.detail_Format);
            this.detail.BeforePrint += new System.EventHandler(this.detail_BeforePrint);
            // 
            // txtYamaNo
            // 
            this.txtYamaNo.DataField = "ITEM04";
            this.txtYamaNo.Height = 0.2F;
            this.txtYamaNo.Left = 2.866142F;
            this.txtYamaNo.MultiLine = false;
            this.txtYamaNo.Name = "txtYamaNo";
            this.txtYamaNo.OutputFormat = resources.GetString("txtYamaNo.OutputFormat");
            this.txtYamaNo.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtYamaNo.Text = "ZZ9";
            this.txtYamaNo.Top = 0F;
            this.txtYamaNo.Width = 0.2602361F;
            // 
            // txtGyoshu
            // 
            this.txtGyoshu.DataField = "ITEM06";
            this.txtGyoshu.Height = 0.2F;
            this.txtGyoshu.Left = 3.969685F;
            this.txtGyoshu.MultiLine = false;
            this.txtGyoshu.Name = "txtGyoshu";
            this.txtGyoshu.OutputFormat = resources.GetString("txtGyoshu.OutputFormat");
            this.txtGyoshu.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: left; vertical-align: middle";
            this.txtGyoshu.Text = "ＮＮＮＮＮＮＮＮＮ";
            this.txtGyoshu.Top = 0F;
            this.txtGyoshu.Width = 1.993307F;
            // 
            // textBox7
            // 
            this.textBox7.DataField = "ITEM07";
            this.textBox7.Height = 0.2F;
            this.textBox7.Left = 6.198426F;
            this.textBox7.MultiLine = false;
            this.textBox7.Name = "textBox7";
            this.textBox7.OutputFormat = resources.GetString("textBox7.OutputFormat");
            this.textBox7.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.textBox7.Text = "ZZZ,ZZ9.9";
            this.textBox7.Top = 0F;
            this.textBox7.Width = 0.7102361F;
            // 
            // textBox8
            // 
            this.textBox8.DataField = "ITEM08";
            this.textBox8.Height = 0.2F;
            this.textBox8.Left = 6.96063F;
            this.textBox8.MultiLine = false;
            this.textBox8.Name = "textBox8";
            this.textBox8.OutputFormat = resources.GetString("textBox8.OutputFormat");
            this.textBox8.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.textBox8.Text = "ZZZ,ZZ9.0";
            this.textBox8.Top = 0F;
            this.textBox8.Width = 0.6606302F;
            // 
            // textBox9
            // 
            this.textBox9.DataField = "ITEM09";
            this.textBox9.Height = 0.2F;
            this.textBox9.Left = 7.825985F;
            this.textBox9.MultiLine = false;
            this.textBox9.Name = "textBox9";
            this.textBox9.OutputFormat = resources.GetString("textBox9.OutputFormat");
            this.textBox9.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.textBox9.Text = "ZZZ,ZZZ,ZZ9";
            this.textBox9.Top = 0F;
            this.textBox9.Width = 0.8023624F;
            // 
            // txtShouhiZei
            // 
            this.txtShouhiZei.DataField = "ITEM10";
            this.txtShouhiZei.Height = 0.2F;
            this.txtShouhiZei.Left = 8.741339F;
            this.txtShouhiZei.MultiLine = false;
            this.txtShouhiZei.Name = "txtShouhiZei";
            this.txtShouhiZei.OutputFormat = resources.GetString("txtShouhiZei.OutputFormat");
            this.txtShouhiZei.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtShouhiZei.Text = "ZZZ,ZZZ,ZZ9";
            this.txtShouhiZei.Top = 0F;
            this.txtShouhiZei.Width = 0.8023625F;
            // 
            // txtGoukeiKingaku
            // 
            this.txtGoukeiKingaku.DataField = "ITEM11";
            this.txtGoukeiKingaku.Height = 0.2F;
            this.txtGoukeiKingaku.Left = 9.69252F;
            this.txtGoukeiKingaku.MultiLine = false;
            this.txtGoukeiKingaku.Name = "txtGoukeiKingaku";
            this.txtGoukeiKingaku.OutputFormat = resources.GetString("txtGoukeiKingaku.OutputFormat");
            this.txtGoukeiKingaku.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtGoukeiKingaku.Text = "ZZZ,ZZZ,ZZ9";
            this.txtGoukeiKingaku.Top = 0F;
            this.txtGoukeiKingaku.Width = 0.8023625F;
            // 
            // txtSenshuCD
            // 
            this.txtSenshuCD.DataField = "ITEM02";
            this.txtSenshuCD.Height = 0.2F;
            this.txtSenshuCD.Left = 0.07874016F;
            this.txtSenshuCD.MultiLine = false;
            this.txtSenshuCD.Name = "txtSenshuCD";
            this.txtSenshuCD.OutputFormat = resources.GetString("txtSenshuCD.OutputFormat");
            this.txtSenshuCD.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtSenshuCD.Text = "ZZZZZ9";
            this.txtSenshuCD.Top = 0F;
            this.txtSenshuCD.Width = 0.4480315F;
            // 
            // txtSenshuNM
            // 
            this.txtSenshuNM.DataField = "ITEM03";
            this.txtSenshuNM.Height = 0.2F;
            this.txtSenshuNM.Left = 0.5889764F;
            this.txtSenshuNM.MultiLine = false;
            this.txtSenshuNM.Name = "txtSenshuNM";
            this.txtSenshuNM.OutputFormat = resources.GetString("txtSenshuNM.OutputFormat");
            this.txtSenshuNM.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: left; vertical-align: middle";
            this.txtSenshuNM.Text = "ＮＮＮＮＮＮＮＮＮ";
            this.txtSenshuNM.Top = 0F;
            this.txtSenshuNM.Width = 2.144488F;
            // 
            // txtGyoshuCD
            // 
            this.txtGyoshuCD.DataField = "ITEM05";
            this.txtGyoshuCD.Height = 0.2F;
            this.txtGyoshuCD.Left = 3.305512F;
            this.txtGyoshuCD.MultiLine = false;
            this.txtGyoshuCD.Name = "txtGyoshuCD";
            this.txtGyoshuCD.OutputFormat = resources.GetString("txtGyoshuCD.OutputFormat");
            this.txtGyoshuCD.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtGyoshuCD.Text = "ZZZZ9";
            this.txtGyoshuCD.Top = 0F;
            this.txtGyoshuCD.Width = 0.375197F;
            // 
            // textBox2
            // 
            this.textBox2.DataField = "ITEM16";
            this.textBox2.Height = 0.2F;
            this.textBox2.Left = 5.962993F;
            this.textBox2.MultiLine = false;
            this.textBox2.Name = "textBox2";
            this.textBox2.OutputFormat = resources.GetString("textBox2.OutputFormat");
            this.textBox2.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.textBox2.Text = "16";
            this.textBox2.Top = 0F;
            this.textBox2.Visible = false;
            this.textBox2.Width = 0.2602361F;
            // 
            // pageFooter
            // 
            this.pageFooter.CanGrow = false;
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            // 
            // reportHeader1
            // 
            this.reportHeader1.CanGrow = false;
            this.reportHeader1.Height = 0F;
            this.reportHeader1.Name = "reportHeader1";
            // 
            // reportFooter1
            // 
            this.reportFooter1.CanGrow = false;
            this.reportFooter1.Height = 0F;
            this.reportFooter1.Name = "reportFooter1";
            // 
            // ghMSenshu
            // 
            this.ghMSenshu.CanGrow = false;
            this.ghMSenshu.DataField = "ITEM14";
            this.ghMSenshu.Height = 0F;
            this.ghMSenshu.Name = "ghMSenshu";
            this.ghMSenshu.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
            this.ghMSenshu.UnderlayNext = true;
            // 
            // gfMSenshu
            // 
            this.gfMSenshu.CanGrow = false;
            this.gfMSenshu.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblShoukei,
            this.txtMMizuageSuryou,
            this.txtMUriageKingaku,
            this.txtMShouhiZei,
            this.txtMGhoukeiKingaku,
            this.txtMKensu,
            this.lblKensu,
            this.txtMTanka});
            this.gfMSenshu.Height = 0.2673228F;
            this.gfMSenshu.Name = "gfMSenshu";
            this.gfMSenshu.Format += new System.EventHandler(this.gfMSenshu_Format);
            // 
            // lblShoukei
            // 
            this.lblShoukei.Height = 0.2F;
            this.lblShoukei.HyperLink = null;
            this.lblShoukei.Left = 1.909449F;
            this.lblShoukei.Name = "lblShoukei";
            this.lblShoukei.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; vertical-align: middle";
            this.lblShoukei.Text = "＊＊＊ 日付計 ＊＊＊";
            this.lblShoukei.Top = 0.03622048F;
            this.lblShoukei.Width = 1.843898F;
            // 
            // txtMMizuageSuryou
            // 
            this.txtMMizuageSuryou.DataField = "ITEM07";
            this.txtMMizuageSuryou.Height = 0.2F;
            this.txtMMizuageSuryou.Left = 6.198426F;
            this.txtMMizuageSuryou.MultiLine = false;
            this.txtMMizuageSuryou.Name = "txtMMizuageSuryou";
            this.txtMMizuageSuryou.OutputFormat = resources.GetString("txtMMizuageSuryou.OutputFormat");
            this.txtMMizuageSuryou.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtMMizuageSuryou.SummaryGroup = "ghMSenshu";
            this.txtMMizuageSuryou.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtMMizuageSuryou.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtMMizuageSuryou.Text = "ZZZ,ZZ9.9";
            this.txtMMizuageSuryou.Top = 0.03622048F;
            this.txtMMizuageSuryou.Width = 0.7102361F;
            // 
            // txtMUriageKingaku
            // 
            this.txtMUriageKingaku.DataField = "ITEM09";
            this.txtMUriageKingaku.Height = 0.2F;
            this.txtMUriageKingaku.Left = 7.825985F;
            this.txtMUriageKingaku.MultiLine = false;
            this.txtMUriageKingaku.Name = "txtMUriageKingaku";
            this.txtMUriageKingaku.OutputFormat = resources.GetString("txtMUriageKingaku.OutputFormat");
            this.txtMUriageKingaku.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtMUriageKingaku.SummaryGroup = "ghMSenshu";
            this.txtMUriageKingaku.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtMUriageKingaku.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtMUriageKingaku.Text = "ZZZ,ZZZ,ZZ9";
            this.txtMUriageKingaku.Top = 0.03622048F;
            this.txtMUriageKingaku.Width = 0.8023625F;
            // 
            // txtMShouhiZei
            // 
            this.txtMShouhiZei.DataField = "ITEM10";
            this.txtMShouhiZei.Height = 0.2F;
            this.txtMShouhiZei.Left = 8.741339F;
            this.txtMShouhiZei.MultiLine = false;
            this.txtMShouhiZei.Name = "txtMShouhiZei";
            this.txtMShouhiZei.OutputFormat = resources.GetString("txtMShouhiZei.OutputFormat");
            this.txtMShouhiZei.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtMShouhiZei.SummaryGroup = "ghMSenshu";
            this.txtMShouhiZei.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtMShouhiZei.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtMShouhiZei.Text = "ZZZ,ZZZ,ZZ9";
            this.txtMShouhiZei.Top = 0.03622047F;
            this.txtMShouhiZei.Width = 0.8023625F;
            // 
            // txtMGhoukeiKingaku
            // 
            this.txtMGhoukeiKingaku.DataField = "ITEM11";
            this.txtMGhoukeiKingaku.Height = 0.2F;
            this.txtMGhoukeiKingaku.Left = 9.69252F;
            this.txtMGhoukeiKingaku.MultiLine = false;
            this.txtMGhoukeiKingaku.Name = "txtMGhoukeiKingaku";
            this.txtMGhoukeiKingaku.OutputFormat = resources.GetString("txtMGhoukeiKingaku.OutputFormat");
            this.txtMGhoukeiKingaku.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtMGhoukeiKingaku.SummaryGroup = "ghMSenshu";
            this.txtMGhoukeiKingaku.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtMGhoukeiKingaku.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtMGhoukeiKingaku.Text = "ZZZ,ZZZ,ZZ9";
            this.txtMGhoukeiKingaku.Top = 0.03622048F;
            this.txtMGhoukeiKingaku.Width = 0.8023625F;
            // 
            // txtMKensu
            // 
            this.txtMKensu.Height = 0.2F;
            this.txtMKensu.Left = 3.88504F;
            this.txtMKensu.MultiLine = false;
            this.txtMKensu.Name = "txtMKensu";
            this.txtMKensu.OutputFormat = resources.GetString("txtMKensu.OutputFormat");
            this.txtMKensu.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtMKensu.SummaryFunc = GrapeCity.ActiveReports.SectionReportModel.SummaryFunc.Count;
            this.txtMKensu.SummaryGroup = "ghMSenshu";
            this.txtMKensu.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtMKensu.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtMKensu.Text = "ZZZ,ZZ9";
            this.txtMKensu.Top = 0.03622047F;
            this.txtMKensu.Width = 0.553937F;
            // 
            // lblKensu
            // 
            this.lblKensu.Height = 0.1688976F;
            this.lblKensu.HyperLink = null;
            this.lblKensu.Left = 4.438975F;
            this.lblKensu.MultiLine = false;
            this.lblKensu.Name = "lblKensu";
            this.lblKensu.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; vertical-align: middle";
            this.lblKensu.Text = "件";
            this.lblKensu.Top = 0.03622047F;
            this.lblKensu.Width = 0.1562996F;
            // 
            // txtMTanka
            // 
            this.txtMTanka.DataField = "ITEM08";
            this.txtMTanka.Height = 0.2F;
            this.txtMTanka.Left = 6.96063F;
            this.txtMTanka.MultiLine = false;
            this.txtMTanka.Name = "txtMTanka";
            this.txtMTanka.OutputFormat = resources.GetString("txtMTanka.OutputFormat");
            this.txtMTanka.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtMTanka.Text = "ZZZ,ZZ9.0";
            this.txtMTanka.Top = 0.03622048F;
            this.txtMTanka.Width = 0.6606299F;
            // 
            // line2
            // 
            this.line2.Height = 0F;
            this.line2.Left = 0F;
            this.line2.LineWeight = 1F;
            this.line2.Name = "line2";
            this.line2.Top = 0.3070866F;
            this.line2.Width = 12.55906F;
            this.line2.X1 = 0F;
            this.line2.X2 = 12.55906F;
            this.line2.Y1 = 0.3070866F;
            this.line2.Y2 = 0.3070866F;
            // 
            // ghSSenshu
            // 
            this.ghSSenshu.CanGrow = false;
            this.ghSSenshu.DataField = "ITEM13";
            this.ghSSenshu.Height = 0F;
            this.ghSSenshu.Name = "ghSSenshu";
            this.ghSSenshu.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
            this.ghSSenshu.UnderlayNext = true;
            // 
            // gfSSenshu
            // 
            this.gfSSenshu.CanGrow = false;
            this.gfSSenshu.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.label1,
            this.txtSMizuageSuryou,
            this.txtSUriageKingaku,
            this.txtSShouhiZei,
            this.txtSGhoukeiKingaku,
            this.line2,
            this.txtSTanka,
            this.textBox1,
            this.label2});
            this.gfSSenshu.Height = 0.3543307F;
            this.gfSSenshu.Name = "gfSSenshu";
            this.gfSSenshu.Format += new System.EventHandler(this.gfSSenshu_Format);
            // 
            // label1
            // 
            this.label1.Height = 0.2F;
            this.label1.HyperLink = null;
            this.label1.Left = 1.909449F;
            this.label1.Name = "label1";
            this.label1.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; vertical-align: middle";
            this.label1.Text = "＊＊＊ 小　計 ＊＊＊";
            this.label1.Top = 0.1102362F;
            this.label1.Width = 1.843898F;
            // 
            // txtSMizuageSuryou
            // 
            this.txtSMizuageSuryou.DataField = "ITEM07";
            this.txtSMizuageSuryou.Height = 0.2F;
            this.txtSMizuageSuryou.Left = 6.198423F;
            this.txtSMizuageSuryou.MultiLine = false;
            this.txtSMizuageSuryou.Name = "txtSMizuageSuryou";
            this.txtSMizuageSuryou.OutputFormat = resources.GetString("txtSMizuageSuryou.OutputFormat");
            this.txtSMizuageSuryou.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtSMizuageSuryou.SummaryGroup = "ghSSenshu";
            this.txtSMizuageSuryou.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtSMizuageSuryou.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtSMizuageSuryou.Text = "ZZZ,ZZ9.9";
            this.txtSMizuageSuryou.Top = 0.1102362F;
            this.txtSMizuageSuryou.Width = 0.7102361F;
            // 
            // txtSUriageKingaku
            // 
            this.txtSUriageKingaku.DataField = "ITEM09";
            this.txtSUriageKingaku.Height = 0.2F;
            this.txtSUriageKingaku.Left = 7.825985F;
            this.txtSUriageKingaku.MultiLine = false;
            this.txtSUriageKingaku.Name = "txtSUriageKingaku";
            this.txtSUriageKingaku.OutputFormat = resources.GetString("txtSUriageKingaku.OutputFormat");
            this.txtSUriageKingaku.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtSUriageKingaku.SummaryGroup = "ghSSenshu";
            this.txtSUriageKingaku.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtSUriageKingaku.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtSUriageKingaku.Text = "ZZZ,ZZZ,ZZ9";
            this.txtSUriageKingaku.Top = 0.1102362F;
            this.txtSUriageKingaku.Width = 0.8023625F;
            // 
            // txtSShouhiZei
            // 
            this.txtSShouhiZei.DataField = "ITEM10";
            this.txtSShouhiZei.Height = 0.2F;
            this.txtSShouhiZei.Left = 8.741339F;
            this.txtSShouhiZei.MultiLine = false;
            this.txtSShouhiZei.Name = "txtSShouhiZei";
            this.txtSShouhiZei.OutputFormat = resources.GetString("txtSShouhiZei.OutputFormat");
            this.txtSShouhiZei.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtSShouhiZei.SummaryGroup = "ghSSenshu";
            this.txtSShouhiZei.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtSShouhiZei.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtSShouhiZei.Text = "ZZZ,ZZZ,ZZ9";
            this.txtSShouhiZei.Top = 0.1102362F;
            this.txtSShouhiZei.Width = 0.8023625F;
            // 
            // txtSGhoukeiKingaku
            // 
            this.txtSGhoukeiKingaku.DataField = "ITEM11";
            this.txtSGhoukeiKingaku.Height = 0.2F;
            this.txtSGhoukeiKingaku.Left = 9.692524F;
            this.txtSGhoukeiKingaku.MultiLine = false;
            this.txtSGhoukeiKingaku.Name = "txtSGhoukeiKingaku";
            this.txtSGhoukeiKingaku.OutputFormat = resources.GetString("txtSGhoukeiKingaku.OutputFormat");
            this.txtSGhoukeiKingaku.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtSGhoukeiKingaku.SummaryGroup = "ghSSenshu";
            this.txtSGhoukeiKingaku.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtSGhoukeiKingaku.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtSGhoukeiKingaku.Text = "ZZZ,ZZZ,ZZ9";
            this.txtSGhoukeiKingaku.Top = 0.1102362F;
            this.txtSGhoukeiKingaku.Width = 0.8023625F;
            // 
            // txtSTanka
            // 
            this.txtSTanka.DataField = "ITEM08";
            this.txtSTanka.Height = 0.2F;
            this.txtSTanka.Left = 6.96063F;
            this.txtSTanka.MultiLine = false;
            this.txtSTanka.Name = "txtSTanka";
            this.txtSTanka.OutputFormat = resources.GetString("txtSTanka.OutputFormat");
            this.txtSTanka.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.txtSTanka.Text = "ZZZ,ZZ9.0";
            this.txtSTanka.Top = 0.1102362F;
            this.txtSTanka.Width = 0.6606299F;
            // 
            // textBox1
            // 
            this.textBox1.Height = 0.2F;
            this.textBox1.Left = 3.88504F;
            this.textBox1.MultiLine = false;
            this.textBox1.Name = "textBox1";
            this.textBox1.OutputFormat = resources.GetString("textBox1.OutputFormat");
            this.textBox1.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: middle";
            this.textBox1.SummaryFunc = GrapeCity.ActiveReports.SectionReportModel.SummaryFunc.Count;
            this.textBox1.SummaryGroup = "ghSSenshu";
            this.textBox1.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox1.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox1.Text = "ZZZ,ZZ9";
            this.textBox1.Top = 0.1102362F;
            this.textBox1.Width = 0.553937F;
            // 
            // label2
            // 
            this.label2.Height = 0.1688976F;
            this.label2.HyperLink = null;
            this.label2.Left = 4.438975F;
            this.label2.MultiLine = false;
            this.label2.Name = "label2";
            this.label2.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; vertical-align: middle";
            this.label2.Text = "件";
            this.label2.Top = 0.1102362F;
            this.label2.Width = 0.1562996F;
            // 
            // HNDR11011R
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.1905512F;
            this.PageSettings.Margins.Left = 0.3937008F;
            this.PageSettings.Margins.Right = 0.1937008F;
            this.PageSettings.Margins.Top = 0.3937008F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
            this.PageSettings.PaperHeight = 11.69291F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 10.78623F;
            this.Sections.Add(this.reportHeader1);
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.ghMSenshu);
            this.Sections.Add(this.ghSSenshu);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.gfSSenshu);
            this.Sections.Add(this.gfMSenshu);
            this.Sections.Add(this.pageFooter);
            this.Sections.Add(this.reportFooter1);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            this.PageEnd += new System.EventHandler(this.HNDR11011R_PageEnd);
            ((System.ComponentModel.ISupportInitialize)(this.lblSeribi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSeriDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrintDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSakusei)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblYamaNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSenshuCD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSenshuNM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGyoshuCD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMizuageSuryou)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTanka)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblUriageGingaku)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblShouhiZei)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGoukeiKingaku)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGyoshuNM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYamaNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyoshu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShouhiZei)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGoukeiKingaku)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSenshuCD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSenshuNM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyoshuCD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblShoukei)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMMizuageSuryou)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMUriageKingaku)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMShouhiZei)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMGhoukeiKingaku)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMKensu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKensu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMTanka)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSMizuageSuryou)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSUriageKingaku)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSShouhiZei)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSGhoukeiKingaku)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSTanka)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.Label lblSeribi;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSeriDate;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrintDate;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSakusei;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblYamaNo;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSenshuCD;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSenshuNM;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblGyoshuCD;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblMizuageSuryou;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTanka;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblUriageGingaku;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblShouhiZei;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblGoukeiKingaku;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblGyoshuNM;
        private GrapeCity.ActiveReports.SectionReportModel.ReportHeader reportHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.ReportFooter reportFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader ghMSenshu;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter gfMSenshu;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblShoukei;
        private GrapeCity.ActiveReports.SectionReportModel.Line line2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYamaNo;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSenshuCD;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSenshuNM;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGyoshu;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtShouhiZei;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGoukeiKingaku;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMMizuageSuryou;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMUriageKingaku;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMShouhiZei;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMGhoukeiKingaku;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader ghSSenshu;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter gfSSenshu;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMKensu;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblKensu;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSMizuageSuryou;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSUriageKingaku;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSShouhiZei;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSGhoukeiKingaku;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMTanka;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSTanka;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGyoshuCD;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox2;
    }
}
