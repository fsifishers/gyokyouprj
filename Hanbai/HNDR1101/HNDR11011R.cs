﻿using System;
using System.Data;

using jp.co.fsi.common.report;
using jp.co.fsi.common.util;

namespace jp.co.fsi.hn.hndr1101
{
    /// <summary>
    /// 仲買人別売上明細書(HNDR11011R) の帳票
    /// </summary>
    public partial class HNDR11011R : BaseReport
    {
        // コード、名称の重複表示回避
        string code = "";
        string name = "";

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HNDR11011R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }

        decimal GokeiKingaku;
        decimal Tanka;

        private void gfSSenshu_Format(object sender, EventArgs e)
        {
            // 小計金額
            GokeiKingaku = Util.ToDecimal(this.txtSUriageKingaku.Value) + Util.ToDecimal(this.txtSShouhiZei.Value);
            this.txtSGhoukeiKingaku.Text = Util.ToString(Util.FormatNum(GokeiKingaku));

            // 単価
            try
            {
                Tanka = Util.ToDecimal(this.txtSUriageKingaku.Value) / Util.ToDecimal(this.txtSMizuageSuryou.Value);
                this.txtSTanka.Text = Util.ToString(Util.FormatNum(Tanka, 1));
            }
            catch (Exception) { }
        }

        private void gfMSenshu_Format(object sender, EventArgs e)
        {
            // 合計金額
            GokeiKingaku = Util.ToDecimal(this.txtMUriageKingaku.Value) + Util.ToDecimal(this.txtMShouhiZei.Value);
            this.txtMGhoukeiKingaku.Text = Util.ToString(Util.FormatNum(GokeiKingaku));

            // 単価
            try
            {
                Tanka = Util.ToDecimal(this.txtMUriageKingaku.Value) / Util.ToDecimal(this.txtMMizuageSuryou.Value);
                this.txtMTanka.Text = Util.ToString(Util.FormatNum(Tanka, 1));
            }
            catch (Exception) { }
        }

        private void detail_BeforePrint(object sender, EventArgs e)
        {
            // コード、名称の重複表示回避
            if (this.txtSenshuCD.Text != code)
                this.txtSenshuCD.Visible = true;
            else
                this.txtSenshuCD.Visible = false;
            code = this.txtSenshuCD.Text;

            if (this.txtSenshuNM.Text != name)
                this.txtSenshuNM.Visible = true;
            else
                this.txtSenshuNM.Visible = false;
            name = this.txtSenshuNM.Text;
        }

        private void detail_Format(object sender, EventArgs e)
        {
            // 消費税明細転嫁
            if (this.textBox2.Text == "1")
            {
                this.txtShouhiZei.Visible = true;
            }
            else
            {
                this.txtShouhiZei.Visible = false;
            }

        }

        private void HNDR11011R_PageEnd(object sender, EventArgs e)
        {
            // コード、名称の重複表示回避
            code = "";
            name = "";
        }
    }
}
