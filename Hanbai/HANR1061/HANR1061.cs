﻿using System;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.access;
using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.han.hanr1061
{
    /// <summary>
    /// 控除一覧表(HANR1061)
    /// </summary>
    public partial class HANR1061 : BasePgForm
    {
        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HANR1061()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region メソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            string[] jpDate = Util.ConvJpDate(DateTime.Now, this.Dba);
            // 日付範囲前
            lblDateGengo.Text = jpDate[0];
            txtDateYear.Text = jpDate[2];
            txtDateMonth.Text = jpDate[3];
            txtDateDay.Text = jpDate[4];

            // フォーカス設定
            this.txtDateYear.Focus();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 日付(年)にフォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtDateYear":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;

            switch (this.ActiveCtlNm)
            {
                case "txtDateYear":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom("COMC9011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.com.comc9011.COMC9011");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblDateGengo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.lblDateGengo.Text = result[1];

                                // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
                                DateTime tmpDate = Util.ConvAdDate(this.lblDateGengo.Text, this.txtDateYear.Text,
                                    this.txtDateMonth.Text, "1", this.Dba);
                                int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);
                                if (Util.ToInt(this.txtDateDay.Text) > lastDayInMonth)
                                {
                                    this.txtDateDay.Text = Util.ToString(lastDayInMonth);
                                }

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblDateGengo.Text,
                                        this.txtDateYear.Text,
                                        this.txtDateMonth.Text,
                                        this.txtDateDay.Text,
                                        this.Dba);
                                this.lblDateGengo.Text = arrJpDate[0];
                                this.txtDateYear.Text = arrJpDate[2];
                                this.txtDateMonth.Text = arrJpDate[3];
                                this.txtDateDay.Text = arrJpDate[4];
                            }
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            // プレビュー処理
            DoPrint(true);
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfYesNo("実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }

        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            //PrintDocumentオブジェクトの作成
            System.Drawing.Printing.PrintDocument pd = new System.Drawing.Printing.PrintDocument();
            //PrintDialogクラスの作成
            PrintDialog pdlg = new PrintDialog();
            //PrintDocumentを指定
            pdlg.Document = pd;
            //印刷の選択ダイアログを表示する
            if (pdlg.ShowDialog() == DialogResult.OK)
            {
                //OKがクリックされた時は印刷する
                //pd.Print();
                Msg.Info("実装する必要がある");
            }
        }
        #endregion

        #region イベント
        /// <summary>
        /// 年(自)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateYear_Validating(object sender, CancelEventArgs e)
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtDateYear.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtDateYear.SelectAll();
                e.Cancel = true;
                return;
            }

            // 空の場合、0年として処理
            if (ValChk.IsEmpty(this.txtDateYear.Text))
            {
                this.txtDateYear.Text = "0";
            }
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblDateGengo.Text, this.txtDateYear.Text,
                this.txtDateMonth.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDateDay.Text) > lastDayInMonth)
            {
                this.txtDateDay.Text = Util.ToString(lastDayInMonth);
            }

            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDate(Util.FixJpDate(this.lblDateGengo.Text, this.txtDateYear.Text,
                this.txtDateMonth.Text, this.txtDateDay.Text, this.Dba));
        }

        /// <summary>
        /// 月(自)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateMonth_Validating(object sender, CancelEventArgs e)
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtDateMonth.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtDateMonth.SelectAll();
                e.Cancel = true;
                return;
            }

            if (ValChk.IsEmpty(this.txtDateMonth.Text))
            {
                // 空の場合、1月として処理
                this.txtDateMonth.Text = "1";
            }
            else
            {
                // 12を超える月が入力された場合、12月として処理
                if (Util.ToInt(this.txtDateMonth.Text) > 12)
                {
                    this.txtDateMonth.Text = "12";
                }
                // 1より小さい月が入力された場合、1月として処理
                else if (Util.ToInt(this.txtDateMonth.Text) < 1)
                {
                    this.txtDateMonth.Text = "1";
                }

                // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
                DateTime tmpDate = Util.ConvAdDate(this.lblDateGengo.Text, this.txtDateYear.Text,
                    this.txtDateMonth.Text, "1", this.Dba);
                int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);
                if (Util.ToInt(this.txtDateDay.Text) > lastDayInMonth)
                {
                    this.txtDateDay.Text = Util.ToString(lastDayInMonth);
                }
            }

            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDate(Util.FixJpDate(this.lblDateGengo.Text, this.txtDateYear.Text,
                this.txtDateMonth.Text, this.txtDateDay.Text, this.Dba));
        }

        /// <summary>
        /// 日(自)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateDay_Validating(object sender, CancelEventArgs e)
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtDateDay.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtDateDay.SelectAll();
                e.Cancel = true;
                return;
            }

            if (ValChk.IsEmpty(this.txtDateDay.Text))
            {
                // 空の場合、1日として処理
                this.txtDateDay.Text = "1";
            }
            else
            {
                // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
                DateTime tmpDate = Util.ConvAdDate(this.lblDateGengo.Text, this.txtDateYear.Text,
                    this.txtDateMonth.Text, "1", this.Dba);
                int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

                if (Util.ToInt(this.txtDateDay.Text) > lastDayInMonth)
                {
                    this.txtDateDay.Text = Util.ToString(lastDayInMonth);
                }
                // 1より小さい日が入力された場合、1日として処理
                else if (Util.ToInt(this.txtDateDay.Text) < 1)
                {
                    this.txtDateDay.Text = "1";
                }
            }

            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDate(Util.FixJpDate(this.lblDateGengo.Text, this.txtDateYear.Text,
                this.txtDateMonth.Text, this.txtDateDay.Text, this.Dba));
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 年(自)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidDateYear()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtDateYear.Text))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }
            // 空の場合、0年として処理
            if (ValChk.IsEmpty(this.txtDateYear.Text))
            {
                this.txtDateYear.Text = "0";
            }

            return true;
        }

        /// <summary>
        /// 月(自)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidDateMonth()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtDateMonth.Text))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }
            // 空の場合、1月として処理
            if (ValChk.IsEmpty(this.txtDateMonth.Text))
            {
                this.txtDateMonth.Text = "1";
            }
            else
            {
                // 12を超える月が入力された場合、12月として処理
                if (Util.ToInt(this.txtDateMonth.Text) > 12)
                {
                    this.txtDateMonth.Text = "12";
                }
                // 1より小さい月が入力された場合、1月として処理
                else if (Util.ToInt(this.txtDateMonth.Text) < 1)
                {
                    this.txtDateMonth.Text = "1";
                }
            }

            return true;
        }

        /// <summary>
        /// 日(自)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidDateDay()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtDateDay.Text))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }

            if (ValChk.IsEmpty(this.txtDateDay.Text))
            {
                // 空の場合、1日として処理
                this.txtDateDay.Text = "1";
            }
            else
            {
                // 1より小さい日が入力された場合、1日として処理
                if (Util.ToInt(this.txtDateDay.Text) < 1)
                {
                    this.txtDateDay.Text = "1";
                }
            }

            return true;
        }

        /// <summary>
        /// 年月日(自)の月末入力チェック
        /// </summary>
        /// 
        private void CheckJpDate()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblDateGengo.Text, this.txtDateYear.Text,
                this.txtDateMonth.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDateDay.Text) > lastDayInMonth)
            {
                this.txtDateDay.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(自)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetJpDate()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDate(Util.FixJpDate(this.lblDateGengo.Text, this.txtDateYear.Text,
                this.txtDateMonth.Text, this.txtDateDay.Text, this.Dba));
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 年のチェック
            if (!IsValidDateYear())
            {
                this.txtDateYear.Focus();
                this.txtDateYear.SelectAll();
                return false;
            }
            // 月のチェック
            if (!IsValidDateMonth())
            {
                this.txtDateMonth.Focus();
                this.txtDateMonth.SelectAll();
                return false;
            }
            // 日のチェック
            if (!IsValidDateDay())
            {
                this.txtDateDay.Focus();
                this.txtDateDay.SelectAll();
                return false;
            }
            // 年月日(自)の月末入力チェック処理
            CheckJpDate();
            // 年月日(自)の正しい和暦への変換処理
            SetJpDate();

            return true;
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpDate(string[] arrJpDate)
        {
            this.lblDateGengo.Text = arrJpDate[0];
            this.txtDateYear.Text = arrJpDate[2];
            this.txtDateMonth.Text = arrJpDate[3];
            this.txtDateDay.Text = arrJpDate[4];
        }

        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview)
        {
            bool dataFlag;
            try
            {
                this.Dba.BeginTransaction();

                // 帳票出力用にワークテーブルにデータを作成
                dataFlag = MakeWkData();
                this.Dba.Commit();
            }
            finally
            {
                this.Dba.Rollback();
            }

            // 帳票出力
            if (dataFlag)
            {
                Report rpt = new Report();
                rpt.OutputReport(Path.Combine(Util.GetPath(), Constants.REP_DIR, "HANR1061.mdb"), "R_HANR1061", this.UnqId, isPreview);
            }

            // ワークテーブルに作成したデータを削除
            try
            {
                this.Dba.BeginTransaction();

                // 帳票出力用に作成したデータを削除
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                this.Dba.Delete("PR_HN_TBL", "GUID = @GUID", dpc);
                this.Dba.Commit();
            }
            finally
            {
                this.Dba.Rollback();
            }
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData()
        {
            // 日付範囲を西暦にして取得
            DateTime tmpDateFr = Util.ConvAdDate(this.lblDateGengo.Text, this.txtDateYear.Text,
                    this.txtDateMonth.Text, "1", this.Dba);
            DateTime tmpDateTo = Util.ConvAdDate(this.lblDateGengo.Text, this.txtDateYear.Text,
                    this.txtDateMonth.Text, this.txtDateDay.Text, this.Dba);

            // 入力された情報を元にワークテーブルに更新をする
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder Sql = new StringBuilder();

            // han.TM_控除科目設定(TM_HN_KOJO_KAMOKU_SETTEI)からデータを取得
            Sql.Append(" SELECT");
            Sql.Append(" SETTEI_CD, KOJO_KOMOKU_NM");
            Sql.Append(" FROM");
            Sql.Append(" TM_HN_KOJO_KAMOKU_SETTEI");
            Sql.Append(" ORDER BY");
            Sql.Append(" SETTEI_CD");
            DataTable dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            #region データ取得の準備
            int i = 0; // メインループ用カウント変数
            int j = 0; // サブループ用カウント変数
            int dbSORT = 1;
            DateTime d;
            string[] seribi_date;
            string seribi;
            string[] tmpKK = new string[dtMainLoop.Rows.Count]; // 控除項目変数
            string[] tmpKN = new string[dtMainLoop.Rows.Count]; // 控除項目日計変数
            string[] tmpKG = new string[dtMainLoop.Rows.Count]; // 控除項目月計変数
            // カラム名を準備
            while (dtMainLoop.Rows.Count > j)
            {
                tmpKK[j] = "KOJO_KOMOKU" + Util.ToInt(dtMainLoop.Rows[j]["SETTEI_CD"]);
                tmpKN[j] = tmpKK[j] + "_NIKKEI";
                tmpKG[j] = tmpKK[j] + "_GEKKEI";
                j++;
            }
            j = 0; // ループ用カウント変数の初期化
            #endregion

            dpc = new DbParamCollection();
            Sql = new StringBuilder();

            // han.TB_控除データ(TB_HN_KOJO_DATA)、han.TB_仕切データ(TB_HN_SHIKIRI_DATA)の日付範囲から発生するデータを取得
            Sql.Append(" SELECT");
            Sql.Append(" A.SERIBI,");
            while (dtMainLoop.Rows.Count > j)
            {
                Sql.Append(" A." + tmpKN[j] + ",");
                j++;
            }
            j = 0;
            // ループ用カウント変数の初期化
            while (dtMainLoop.Rows.Count - 1 > j)
            {
                Sql.Append(" B." + tmpKG[j] + ",");
                j++;
            }
            Sql.Append(" B." + tmpKG[j]);
            j = 0; // ループ用カウント変数の初期化
            Sql.Append(" FROM (");
            Sql.Append(" SELECT");
            Sql.Append(" D.SERIBI,");
            while (dtMainLoop.Rows.Count - 1 > j)
            {
                Sql.Append(" SUM(" + tmpKK[j] + ") AS " + tmpKN[j] + ",");
                j++;
            }
            Sql.Append(" SUM(" + tmpKK[j] + ") AS " + tmpKN[j]);
            j = 0; // ループ用カウント変数の初期化
            Sql.Append(" FROM");
            Sql.Append(" TB_HN_KOJO_DATA AS C");
            Sql.Append(" LEFT JOIN");
            Sql.Append(" (select");
            Sql.Append(" KAISHA_CD,");
            Sql.Append(" SEISAN_NO,");
            Sql.Append(" SERIBI");
            Sql.Append(" FROM");
            Sql.Append(" TB_HN_SHIKIRI_DATA");
            Sql.Append(" GROUP BY");
            Sql.Append(" KAISHA_CD,");
            Sql.Append(" SEISAN_NO,");
            Sql.Append(" SERIBI)");
            Sql.Append(" AS D ON");
            Sql.Append(" C.KAISHA_CD = D.KAISHA_CD AND");
            Sql.Append(" C.SEISAN_BANGO = D.SEISAN_NO");
            Sql.Append(" WHERE");
            Sql.Append(" C.KAISHA_CD = @KAISHA_CD AND");
            Sql.Append(" D.SERIBI = @DATE_TO");
            Sql.Append(" GROUP BY");
            Sql.Append(" D.SERIBI");
            Sql.Append(" ) A,");
            Sql.Append(" (select");
            while (dtMainLoop.Rows.Count - 1 > j)
            {
                Sql.Append(" SUM(" + tmpKK[j] + ") AS " + tmpKG[j] + ",");
                j++;
            }
            Sql.Append(" SUM(" + tmpKK[j] + ") AS " + tmpKG[j]);
            j = 0; // ループ用カウント変数の初期化
            Sql.Append(" FROM");
            Sql.Append(" TB_HN_KOJO_DATA AS C");
            Sql.Append(" LEFT JOIN");
            Sql.Append(" (select");
            Sql.Append(" KAISHA_CD,");
            Sql.Append(" SEISAN_NO,");
            Sql.Append(" SERIBI");
            Sql.Append(" FROM");
            Sql.Append(" TB_HN_SHIKIRI_DATA");
            Sql.Append(" GROUP BY");
            Sql.Append(" KAISHA_CD,");
            Sql.Append(" SEISAN_NO,");
            Sql.Append(" SERIBI)");
            Sql.Append(" AS D ON");
            Sql.Append(" C.KAISHA_CD = D.KAISHA_CD AND");
            Sql.Append(" C.SEISAN_BANGO = D.SEISAN_NO");
            Sql.Append(" WHERE");
            Sql.Append(" C.KAISHA_CD = @KAISHA_CD AND");
            Sql.Append(" D.SERIBI BETWEEN @DATE_FR AND @DATE_TO");
            Sql.Append(" ) B");
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@DATE_FR", SqlDbType.VarChar, 10, tmpDateFr.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@DATE_TO", SqlDbType.VarChar, 10, tmpDateTo.Date.ToString("yyyy/MM/dd"));
            DataTable dtKojoIchiran = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            if (dtMainLoop.Rows.Count == 0)
            {
                Msg.Info("該当データがありません。");
                return false;
            }
            else
            {
                if (Msg.ConfYesNo("実行しますか？") == DialogResult.Yes)
                {
                    while (dtMainLoop.Rows.Count > i)
                    {
                        dpc = new DbParamCollection();
                        Sql = new StringBuilder();

                        #region インサートテーブル
                        Sql = new StringBuilder();
                        dpc = new DbParamCollection();
                        Sql.Append("INSERT INTO PR_HN_TBL(");
                        Sql.Append("  GUID");
                        Sql.Append(" ,SORT");
                        Sql.Append(" ,ITEM01");
                        Sql.Append(" ,ITEM02");
                        Sql.Append(" ,ITEM03");
                        Sql.Append(" ,ITEM04");
                        Sql.Append(") ");
                        Sql.Append("VALUES(");
                        Sql.Append("  @GUID");
                        Sql.Append(" ,@SORT");
                        Sql.Append(" ,@ITEM01");
                        Sql.Append(" ,@ITEM02");
                        Sql.Append(" ,@ITEM03");
                        Sql.Append(" ,@ITEM04");
                        Sql.Append(") ");
                        #endregion

                        #region データ登録
                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                        dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                        dbSORT++;

                        dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["KOJO_KOMOKU_NM"]); // 控除名

                        // セリ日を和暦に変換
                        d = Util.ToDate(dtKojoIchiran.Rows[0]["SERIBI"]);
                        seribi_date = Util.ConvJpDate(d, this.Dba);
                        if (seribi_date[3].Length == 1)
                        {
                            seribi_date[3] = "0" + seribi_date[3];
                        }
                        if (seribi_date[4].Length == 1)
                        {
                            seribi_date[4] = "0" + seribi_date[4];
                        }
                        seribi = seribi_date[0] + seribi_date[2] + "年" + seribi_date[3] + "月" + seribi_date[4] + "日";
                        dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, seribi); // セリ日
                        dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, Util.FormatNum(dtKojoIchiran.Rows[0][tmpKN[j]])); // 日計
                        dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, Util.FormatNum(dtKojoIchiran.Rows[0][tmpKG[j]])); // 月計

                        this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                        #endregion

                        i++;
                        j++;
                    }
                }
            }

            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
                "SORT",
                "PR_HN_TBL",
                "GUID = @GUID",
                dpc);

            bool dataFlag;
            if (tmpdtPR_HN_TBL.Rows.Count > 0)
            {
                dataFlag = true;
            }
            else
            {
                dataFlag = false;
            }

            return dataFlag;

        }
        #endregion

    }
}
