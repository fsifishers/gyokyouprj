﻿using System.Data;

using jp.co.fsi.common.report;
using jp.co.fsi.common.util;

namespace jp.co.fsi.han.hanr2131
{
    /// <summary>
    /// HANR2131R の概要の説明です。
    /// </summary>
    public partial class HANR2131R : BaseReport
    {
        public HANR2131R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }

        private void grpFooterChiku_Format(object sender, System.EventArgs e)
        {
            this.txtShokeiTanka.Text = Util.ToString(Util.FormatNum(Util.ToDecimal(this.txtShokeiMizuageKingaku.Text) / Util.ToDecimal(this.txtSyokeiSuryo.Text)));
        }

        private void grpFooterSeribi_Format(object sender, System.EventArgs e)
        {
            this.txtSokeiTanka.Text = Util.ToString(Util.FormatNum(Util.ToDecimal(this.txtSokeiMizuageKingaku.Text) / Util.ToDecimal(this.txtSokeiSuryo.Text)));
        }
    }
}
