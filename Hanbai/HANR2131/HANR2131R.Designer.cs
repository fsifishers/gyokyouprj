﻿namespace jp.co.fsi.han.hanr2131
{
    /// <summary>
    /// HANR2131R の概要の説明です。
    /// </summary>
    partial class HANR2131R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(HANR2131R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.lblTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtShiharaiKbn = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.rptDate = new GrapeCity.ActiveReports.SectionReportModel.ReportInfo();
            this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblCd = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblKumiaiin = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblKensu = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSuryo = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTanka = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblMizuageKingaku = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblShohizei = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblZeikomiKingaku = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTesuryo = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblHako = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSonota = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTsumitatekin = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblAzukarikin = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblHonnin = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSashihikiKingaku = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSeribi = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtSeribi = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtCd = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtKumiaiin = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtKensu = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSuryo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTanka = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMizuageKingaku = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtShohizei = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtZeikomiKingaku = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGyokyouTesuryo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtHako = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSonota = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTsumitatekin = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAzukarikin = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtHonnin = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSashihikiKingaku = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.grpHeaderChiku = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.txtChikuCd = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtChikuNm = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.grpFooterChiku = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.lblShokei = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtShokeiCt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtShokeiKensu = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSyokeiSuryo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtShokeiTanka = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtShokeiMizuageKingaku = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtShokeiShohizei = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtShokeiZeikomiKingaku = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtShokeiGyokyouTesuryo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtShokeiHako = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtShokeiSonota = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtShokeiTsumiatatekin = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtShokeiAzukarikin = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtShokeiHonnin = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtShokeiSashihikiKingaku = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.grpHeaderSeribi = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.grpFooterSeribi = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.lblSokei = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtSokeiCt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtSokeiKensu = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSokeiSuryo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSokeiTanka = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSokeiMizuageKingaku = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSokeiShohizei = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSokeiZeikomiKingaku = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSokeiGyokyouTesuryo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSokeiHako = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSokeiSonota = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSokeiTsumitatekin = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSokeiAzukarikin = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSokeiHonnin = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSokeiSashihikiKingaku = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShiharaiKbn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rptDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKumiaiin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKensu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSuryo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTanka)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMizuageKingaku)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblShohizei)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblZeikomiKingaku)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTesuryo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHako)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSonota)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTsumitatekin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAzukarikin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHonnin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSashihikiKingaku)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSeribi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSeribi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKumiaiin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKensu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSuryo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTanka)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMizuageKingaku)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShohizei)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZeikomiKingaku)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyokyouTesuryo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHako)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSonota)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTsumitatekin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAzukarikin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHonnin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSashihikiKingaku)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChikuCd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChikuNm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblShokei)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShokeiCt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShokeiKensu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSyokeiSuryo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShokeiTanka)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShokeiMizuageKingaku)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShokeiShohizei)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShokeiZeikomiKingaku)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShokeiGyokyouTesuryo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShokeiHako)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShokeiSonota)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShokeiTsumiatatekin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShokeiAzukarikin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShokeiHonnin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShokeiSashihikiKingaku)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSokei)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSokeiCt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSokeiKensu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSokeiSuryo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSokeiTanka)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSokeiMizuageKingaku)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSokeiShohizei)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSokeiZeikomiKingaku)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSokeiGyokyouTesuryo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSokeiHako)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSokeiSonota)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSokeiTsumitatekin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSokeiAzukarikin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSokeiHonnin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSokeiSashihikiKingaku)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblTitle,
            this.txtShiharaiKbn,
            this.lblDate,
            this.rptDate,
            this.lblPage,
            this.txtPage,
            this.lblCd,
            this.line1,
            this.lblKumiaiin,
            this.lblKensu,
            this.lblSuryo,
            this.lblTanka,
            this.lblMizuageKingaku,
            this.lblShohizei,
            this.lblZeikomiKingaku,
            this.lblTesuryo,
            this.lblHako,
            this.lblSonota,
            this.lblTsumitatekin,
            this.lblAzukarikin,
            this.lblHonnin,
            this.lblSashihikiKingaku,
            this.lblSeribi,
            this.txtSeribi});
            this.pageHeader.Height = 0.824065F;
            this.pageHeader.Name = "pageHeader";
            // 
            // lblTitle
            // 
            this.lblTitle.Height = 0.249836F;
            this.lblTitle.HyperLink = null;
            this.lblTitle.Left = 4.073622F;
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Style = "font-family: ＭＳ 明朝; font-size: 15.75pt; font-weight: bold; text-align: center";
            this.lblTitle.Text = "水揚仕切日計表";
            this.lblTitle.Top = 0F;
            this.lblTitle.Width = 1.698031F;
            // 
            // txtShiharaiKbn
            // 
            this.txtShiharaiKbn.DataField = "ITEM20";
            this.txtShiharaiKbn.Height = 0.1559876F;
            this.txtShiharaiKbn.Left = 5.823491F;
            this.txtShiharaiKbn.Name = "txtShiharaiKbn";
            this.txtShiharaiKbn.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; ddo-char-set: 1";
            this.txtShiharaiKbn.Text = "shiharaiKubun";
            this.txtShiharaiKbn.Top = 0.07424545F;
            this.txtShiharaiKbn.Width = 0.9791341F;
            // 
            // lblDate
            // 
            this.lblDate.Height = 0.1458333F;
            this.lblDate.HyperLink = null;
            this.lblDate.Left = 8.541733F;
            this.lblDate.Name = "lblDate";
            this.lblDate.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; ddo-char-set: 1";
            this.lblDate.Text = "日　付：";
            this.lblDate.Top = 0.1240158F;
            this.lblDate.Width = 0.6041349F;
            // 
            // rptDate
            // 
            this.rptDate.FormatString = "{RunDateTime:yyyy/MM/dd}";
            this.rptDate.Height = 0.144685F;
            this.rptDate.Left = 9.198031F;
            this.rptDate.Name = "rptDate";
            this.rptDate.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; ddo-char-set: 1";
            this.rptDate.Top = 0.1240158F;
            this.rptDate.Width = 0.8023634F;
            // 
            // lblPage
            // 
            this.lblPage.Height = 0.1458333F;
            this.lblPage.HyperLink = null;
            this.lblPage.Left = 8.541733F;
            this.lblPage.Name = "lblPage";
            this.lblPage.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; ddo-char-set: 1";
            this.lblPage.Text = "ページ：";
            this.lblPage.Top = 0.3051182F;
            this.lblPage.Width = 0.6041349F;
            // 
            // txtPage
            // 
            this.txtPage.Height = 0.1456693F;
            this.txtPage.Left = 9.21875F;
            this.txtPage.Name = "txtPage";
            this.txtPage.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; ddo-char-set: 1";
            this.txtPage.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtPage.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.txtPage.Text = "Page";
            this.txtPage.Top = 0.3051182F;
            this.txtPage.Width = 0.78125F;
            // 
            // lblCd
            // 
            this.lblCd.Height = 0.1874016F;
            this.lblCd.HyperLink = null;
            this.lblCd.Left = 0.08110237F;
            this.lblCd.Name = "lblCd";
            this.lblCd.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; ver" +
    "tical-align: bottom; ddo-char-set: 128";
            this.lblCd.Text = "コード";
            this.lblCd.Top = 0.5677166F;
            this.lblCd.Width = 0.4370081F;
            // 
            // line1
            // 
            this.line1.Height = 0F;
            this.line1.Left = 0.08110237F;
            this.line1.LineWeight = 2F;
            this.line1.Name = "line1";
            this.line1.Top = 0.7551182F;
            this.line1.Width = 10.51178F;
            this.line1.X1 = 0.08110237F;
            this.line1.X2 = 10.59288F;
            this.line1.Y1 = 0.7551182F;
            this.line1.Y2 = 0.7551182F;
            // 
            // lblKumiaiin
            // 
            this.lblKumiaiin.Height = 0.1874016F;
            this.lblKumiaiin.HyperLink = null;
            this.lblKumiaiin.Left = 0.5811025F;
            this.lblKumiaiin.Name = "lblKumiaiin";
            this.lblKumiaiin.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; ver" +
    "tical-align: bottom; ddo-char-set: 128";
            this.lblKumiaiin.Text = "組合員";
            this.lblKumiaiin.Top = 0.5677166F;
            this.lblKumiaiin.Width = 0.4787402F;
            // 
            // lblKensu
            // 
            this.lblKensu.Height = 0.1874016F;
            this.lblKensu.HyperLink = null;
            this.lblKensu.Left = 2.043701F;
            this.lblKensu.Name = "lblKensu";
            this.lblKensu.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; ver" +
    "tical-align: bottom; ddo-char-set: 128";
            this.lblKensu.Text = "件数";
            this.lblKensu.Top = 0.5677166F;
            this.lblKensu.Width = 0.353937F;
            // 
            // lblSuryo
            // 
            this.lblSuryo.Height = 0.1874016F;
            this.lblSuryo.HyperLink = null;
            this.lblSuryo.Left = 2.61811F;
            this.lblSuryo.Name = "lblSuryo";
            this.lblSuryo.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; ver" +
    "tical-align: bottom; ddo-char-set: 128";
            this.lblSuryo.Text = "数量";
            this.lblSuryo.Top = 0.5677166F;
            this.lblSuryo.Width = 0.3539371F;
            // 
            // lblTanka
            // 
            this.lblTanka.Height = 0.1874016F;
            this.lblTanka.HyperLink = null;
            this.lblTanka.Left = 3.180709F;
            this.lblTanka.Name = "lblTanka";
            this.lblTanka.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; ver" +
    "tical-align: bottom; ddo-char-set: 128";
            this.lblTanka.Text = "単価";
            this.lblTanka.Top = 0.5677166F;
            this.lblTanka.Width = 0.3539371F;
            // 
            // lblMizuageKingaku
            // 
            this.lblMizuageKingaku.Height = 0.1874016F;
            this.lblMizuageKingaku.HyperLink = null;
            this.lblMizuageKingaku.Left = 3.696063F;
            this.lblMizuageKingaku.Name = "lblMizuageKingaku";
            this.lblMizuageKingaku.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; ver" +
    "tical-align: bottom; ddo-char-set: 128";
            this.lblMizuageKingaku.Text = "水揚金額";
            this.lblMizuageKingaku.Top = 0.5677166F;
            this.lblMizuageKingaku.Width = 0.5728344F;
            // 
            // lblShohizei
            // 
            this.lblShohizei.Height = 0.1874016F;
            this.lblShohizei.HyperLink = null;
            this.lblShohizei.Left = 4.39919F;
            this.lblShohizei.Name = "lblShohizei";
            this.lblShohizei.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; ver" +
    "tical-align: bottom; ddo-char-set: 128";
            this.lblShohizei.Text = "消費税";
            this.lblShohizei.Top = 0.5677166F;
            this.lblShohizei.Width = 0.4688978F;
            // 
            // lblZeikomiKingaku
            // 
            this.lblZeikomiKingaku.Height = 0.1874016F;
            this.lblZeikomiKingaku.HyperLink = null;
            this.lblZeikomiKingaku.Left = 4.970044F;
            this.lblZeikomiKingaku.Name = "lblZeikomiKingaku";
            this.lblZeikomiKingaku.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; ver" +
    "tical-align: bottom; ddo-char-set: 128";
            this.lblZeikomiKingaku.Text = "税込金額";
            this.lblZeikomiKingaku.Top = 0.5677166F;
            this.lblZeikomiKingaku.Width = 0.5728344F;
            // 
            // lblTesuryo
            // 
            this.lblTesuryo.Height = 0.1874016F;
            this.lblTesuryo.HyperLink = null;
            this.lblTesuryo.Left = 5.634218F;
            this.lblTesuryo.Name = "lblTesuryo";
            this.lblTesuryo.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; ver" +
    "tical-align: bottom; ddo-char-set: 128";
            this.lblTesuryo.Text = "漁協手数料";
            this.lblTesuryo.Top = 0.5677166F;
            this.lblTesuryo.Width = 0.7397639F;
            // 
            // lblHako
            // 
            this.lblHako.Height = 0.1874016F;
            this.lblHako.HyperLink = null;
            this.lblHako.Left = 6.459415F;
            this.lblHako.Name = "lblHako";
            this.lblHako.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; ver" +
    "tical-align: bottom; ddo-char-set: 128";
            this.lblHako.Text = "箱代";
            this.lblHako.Top = 0.5677166F;
            this.lblHako.Width = 0.3437008F;
            // 
            // lblSonota
            // 
            this.lblSonota.Height = 0.1874016F;
            this.lblSonota.HyperLink = null;
            this.lblSonota.Left = 6.917684F;
            this.lblSonota.Name = "lblSonota";
            this.lblSonota.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; ver" +
    "tical-align: bottom; ddo-char-set: 128";
            this.lblSonota.Text = "その他の控除";
            this.lblSonota.Top = 0.5677166F;
            this.lblSonota.Width = 0.8543305F;
            // 
            // lblTsumitatekin
            // 
            this.lblTsumitatekin.Height = 0.1874016F;
            this.lblTsumitatekin.HyperLink = null;
            this.lblTsumitatekin.Left = 7.868073F;
            this.lblTsumitatekin.Name = "lblTsumitatekin";
            this.lblTsumitatekin.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; ver" +
    "tical-align: bottom; ddo-char-set: 128";
            this.lblTsumitatekin.Text = "積立金";
            this.lblTsumitatekin.Top = 0.5677166F;
            this.lblTsumitatekin.Width = 0.4688978F;
            // 
            // lblAzukarikin
            // 
            this.lblAzukarikin.Height = 0.1874016F;
            this.lblAzukarikin.HyperLink = null;
            this.lblAzukarikin.Left = 8.440912F;
            this.lblAzukarikin.Name = "lblAzukarikin";
            this.lblAzukarikin.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; ver" +
    "tical-align: bottom; ddo-char-set: 128";
            this.lblAzukarikin.Text = "預り金";
            this.lblAzukarikin.Top = 0.5677166F;
            this.lblAzukarikin.Width = 0.4582681F;
            // 
            // lblHonnin
            // 
            this.lblHonnin.Height = 0.1874016F;
            this.lblHonnin.HyperLink = null;
            this.lblHonnin.Left = 8.949176F;
            this.lblHonnin.Name = "lblHonnin";
            this.lblHonnin.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; ver" +
    "tical-align: bottom; ddo-char-set: 128";
            this.lblHonnin.Text = "本人積立金";
            this.lblHonnin.Top = 0.5677166F;
            this.lblHonnin.Width = 0.7397639F;
            // 
            // lblSashihikiKingaku
            // 
            this.lblSashihikiKingaku.Height = 0.1874016F;
            this.lblSashihikiKingaku.HyperLink = null;
            this.lblSashihikiKingaku.Left = 9.858231F;
            this.lblSashihikiKingaku.Name = "lblSashihikiKingaku";
            this.lblSashihikiKingaku.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; ver" +
    "tical-align: bottom; ddo-char-set: 128";
            this.lblSashihikiKingaku.Text = "差引金額";
            this.lblSashihikiKingaku.Top = 0.5677166F;
            this.lblSashihikiKingaku.Width = 0.640983F;
            // 
            // lblSeribi
            // 
            this.lblSeribi.Height = 0.1874016F;
            this.lblSeribi.HyperLink = null;
            this.lblSeribi.Left = 0.08110237F;
            this.lblSeribi.Name = "lblSeribi";
            this.lblSeribi.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; ddo-char-set: 1";
            this.lblSeribi.Text = "セリ日：";
            this.lblSeribi.Top = 0.3051181F;
            this.lblSeribi.Width = 0.5830709F;
            // 
            // txtSeribi
            // 
            this.txtSeribi.DataField = "ITEM02";
            this.txtSeribi.Height = 0.1875F;
            this.txtSeribi.Left = 0.7373686F;
            this.txtSeribi.Name = "txtSeribi";
            this.txtSeribi.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; ddo-char-set: 1";
            this.txtSeribi.Text = "seribi";
            this.txtSeribi.Top = 0.3051181F;
            this.txtSeribi.Width = 1.25F;
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtCd,
            this.txtKumiaiin,
            this.txtKensu,
            this.txtSuryo,
            this.txtTanka,
            this.txtMizuageKingaku,
            this.txtShohizei,
            this.txtZeikomiKingaku,
            this.txtGyokyouTesuryo,
            this.txtHako,
            this.txtSonota,
            this.txtTsumitatekin,
            this.txtAzukarikin,
            this.txtHonnin,
            this.txtSashihikiKingaku});
            this.detail.Height = 0.2916667F;
            this.detail.Name = "detail";
            // 
            // txtCd
            // 
            this.txtCd.DataField = "ITEM05";
            this.txtCd.Height = 0.1979167F;
            this.txtCd.Left = 0.08110237F;
            this.txtCd.MultiLine = false;
            this.txtCd.Name = "txtCd";
            this.txtCd.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: center; ddo-char-set: 1";
            this.txtCd.Text = "cd";
            this.txtCd.Top = 0F;
            this.txtCd.Width = 0.4370406F;
            // 
            // txtKumiaiin
            // 
            this.txtKumiaiin.DataField = "ITEM06";
            this.txtKumiaiin.Height = 0.1979167F;
            this.txtKumiaiin.Left = 0.5208662F;
            this.txtKumiaiin.MultiLine = false;
            this.txtKumiaiin.Name = "txtKumiaiin";
            this.txtKumiaiin.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; ddo-char-set: 1";
            this.txtKumiaiin.Text = "kumiaiin";
            this.txtKumiaiin.Top = 0F;
            this.txtKumiaiin.Width = 1.466536F;
            // 
            // txtKensu
            // 
            this.txtKensu.DataField = "ITEM07";
            this.txtKensu.Height = 0.1979167F;
            this.txtKensu.Left = 2.114961F;
            this.txtKensu.MultiLine = false;
            this.txtKensu.Name = "txtKensu";
            this.txtKensu.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtKensu.Text = "kensu";
            this.txtKensu.Top = 0F;
            this.txtKensu.Width = 0.2574803F;
            // 
            // txtSuryo
            // 
            this.txtSuryo.DataField = "ITEM08";
            this.txtSuryo.Height = 0.1979167F;
            this.txtSuryo.Left = 2.397638F;
            this.txtSuryo.MultiLine = false;
            this.txtSuryo.Name = "txtSuryo";
            this.txtSuryo.OutputFormat = resources.GetString("txtSuryo.OutputFormat");
            this.txtSuryo.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtSuryo.Text = "suryo";
            this.txtSuryo.Top = 0F;
            this.txtSuryo.Width = 0.5507874F;
            // 
            // txtTanka
            // 
            this.txtTanka.DataField = "ITEM19";
            this.txtTanka.Height = 0.1979167F;
            this.txtTanka.Left = 2.972047F;
            this.txtTanka.MultiLine = false;
            this.txtTanka.Name = "txtTanka";
            this.txtTanka.OutputFormat = resources.GetString("txtTanka.OutputFormat");
            this.txtTanka.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtTanka.Text = "tanka";
            this.txtTanka.Top = 0F;
            this.txtTanka.Width = 0.5625989F;
            // 
            // txtMizuageKingaku
            // 
            this.txtMizuageKingaku.DataField = "ITEM09";
            this.txtMizuageKingaku.Height = 0.1979167F;
            this.txtMizuageKingaku.Left = 3.581496F;
            this.txtMizuageKingaku.MultiLine = false;
            this.txtMizuageKingaku.Name = "txtMizuageKingaku";
            this.txtMizuageKingaku.OutputFormat = resources.GetString("txtMizuageKingaku.OutputFormat");
            this.txtMizuageKingaku.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtMizuageKingaku.Text = "mizuageKingaku";
            this.txtMizuageKingaku.Top = 0F;
            this.txtMizuageKingaku.Width = 0.6874018F;
            // 
            // txtShohizei
            // 
            this.txtShohizei.DataField = "ITEM10";
            this.txtShohizei.Height = 0.1979167F;
            this.txtShohizei.Left = 4.312599F;
            this.txtShohizei.MultiLine = false;
            this.txtShohizei.Name = "txtShohizei";
            this.txtShohizei.OutputFormat = resources.GetString("txtShohizei.OutputFormat");
            this.txtShohizei.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtShohizei.Text = "shohizei";
            this.txtShohizei.Top = 0F;
            this.txtShohizei.Width = 0.5161421F;
            // 
            // txtZeikomiKingaku
            // 
            this.txtZeikomiKingaku.DataField = "ITEM11";
            this.txtZeikomiKingaku.Height = 0.1979167F;
            this.txtZeikomiKingaku.Left = 4.868111F;
            this.txtZeikomiKingaku.MultiLine = false;
            this.txtZeikomiKingaku.Name = "txtZeikomiKingaku";
            this.txtZeikomiKingaku.OutputFormat = resources.GetString("txtZeikomiKingaku.OutputFormat");
            this.txtZeikomiKingaku.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtZeikomiKingaku.Text = "zeikomiKingaku";
            this.txtZeikomiKingaku.Top = 0F;
            this.txtZeikomiKingaku.Width = 0.7141733F;
            // 
            // txtGyokyouTesuryo
            // 
            this.txtGyokyouTesuryo.DataField = "ITEM12";
            this.txtGyokyouTesuryo.Height = 0.1979167F;
            this.txtGyokyouTesuryo.Left = 5.634253F;
            this.txtGyokyouTesuryo.MultiLine = false;
            this.txtGyokyouTesuryo.Name = "txtGyokyouTesuryo";
            this.txtGyokyouTesuryo.OutputFormat = resources.GetString("txtGyokyouTesuryo.OutputFormat");
            this.txtGyokyouTesuryo.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtGyokyouTesuryo.Text = "gyokyouTesuryo";
            this.txtGyokyouTesuryo.Top = 0F;
            this.txtGyokyouTesuryo.Width = 0.7397413F;
            // 
            // txtHako
            // 
            this.txtHako.DataField = "ITEM13";
            this.txtHako.Height = 0.1979167F;
            this.txtHako.Left = 6.412183F;
            this.txtHako.MultiLine = false;
            this.txtHako.Name = "txtHako";
            this.txtHako.OutputFormat = resources.GetString("txtHako.OutputFormat");
            this.txtHako.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtHako.Text = "hako";
            this.txtHako.Top = 0F;
            this.txtHako.Width = 0.3909448F;
            // 
            // txtSonota
            // 
            this.txtSonota.DataField = "ITEM14";
            this.txtSonota.Height = 0.1979167F;
            this.txtSonota.Left = 6.835402F;
            this.txtSonota.MultiLine = false;
            this.txtSonota.Name = "txtSonota";
            this.txtSonota.OutputFormat = resources.GetString("txtSonota.OutputFormat");
            this.txtSonota.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtSonota.Text = "sonota";
            this.txtSonota.Top = 0F;
            this.txtSonota.Width = 0.8578738F;
            // 
            // txtTsumitatekin
            // 
            this.txtTsumitatekin.DataField = "ITEM15";
            this.txtTsumitatekin.Height = 0.1979167F;
            this.txtTsumitatekin.Left = 7.742094F;
            this.txtTsumitatekin.MultiLine = false;
            this.txtTsumitatekin.Name = "txtTsumitatekin";
            this.txtTsumitatekin.OutputFormat = resources.GetString("txtTsumitatekin.OutputFormat");
            this.txtTsumitatekin.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtTsumitatekin.Text = "tsumitatekin";
            this.txtTsumitatekin.Top = 0F;
            this.txtTsumitatekin.Width = 0.5161419F;
            // 
            // txtAzukarikin
            // 
            this.txtAzukarikin.DataField = "ITEM16";
            this.txtAzukarikin.Height = 0.1979167F;
            this.txtAzukarikin.Left = 8.314928F;
            this.txtAzukarikin.MultiLine = false;
            this.txtAzukarikin.Name = "txtAzukarikin";
            this.txtAzukarikin.OutputFormat = resources.GetString("txtAzukarikin.OutputFormat");
            this.txtAzukarikin.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtAzukarikin.Text = "azukarikin";
            this.txtAzukarikin.Top = 0F;
            this.txtAzukarikin.Width = 0.5055118F;
            // 
            // txtHonnin
            // 
            this.txtHonnin.DataField = "ITEM17";
            this.txtHonnin.Height = 0.1979167F;
            this.txtHonnin.Left = 8.823198F;
            this.txtHonnin.MultiLine = false;
            this.txtHonnin.Name = "txtHonnin";
            this.txtHonnin.OutputFormat = resources.GetString("txtHonnin.OutputFormat");
            this.txtHonnin.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtHonnin.Text = "honnin";
            this.txtHonnin.Top = 0F;
            this.txtHonnin.Width = 0.7870069F;
            // 
            // txtSashihikiKingaku
            // 
            this.txtSashihikiKingaku.DataField = "ITEM18";
            this.txtSashihikiKingaku.Height = 0.1979167F;
            this.txtSashihikiKingaku.Left = 9.679497F;
            this.txtSashihikiKingaku.MultiLine = false;
            this.txtSashihikiKingaku.Name = "txtSashihikiKingaku";
            this.txtSashihikiKingaku.OutputFormat = resources.GetString("txtSashihikiKingaku.OutputFormat");
            this.txtSashihikiKingaku.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtSashihikiKingaku.Text = "sashihikiKingaku";
            this.txtSashihikiKingaku.Top = 0F;
            this.txtSashihikiKingaku.Width = 0.7299216F;
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            // 
            // grpHeaderChiku
            // 
            this.grpHeaderChiku.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtChikuCd,
            this.txtChikuNm,
            this.label1,
            this.label2});
            this.grpHeaderChiku.DataField = "ITEM03";
            this.grpHeaderChiku.Name = "grpHeaderChiku";
            // 
            // txtChikuCd
            // 
            this.txtChikuCd.DataField = "ITEM03";
            this.txtChikuCd.Height = 0.15625F;
            this.txtChikuCd.Left = 0.6161418F;
            this.txtChikuCd.Name = "txtChikuCd";
            this.txtChikuCd.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; ddo-char-set: 1";
            this.txtChikuCd.Text = "chikuCd";
            this.txtChikuCd.Top = 0F;
            this.txtChikuCd.Width = 0.2915683F;
            // 
            // txtChikuNm
            // 
            this.txtChikuNm.DataField = "ITEM04";
            this.txtChikuNm.Height = 0.15625F;
            this.txtChikuNm.Left = 0.9515747F;
            this.txtChikuNm.Name = "txtChikuNm";
            this.txtChikuNm.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; ddo-char-set: 1";
            this.txtChikuNm.Text = "chikuNm";
            this.txtChikuNm.Top = 0.0001312345F;
            this.txtChikuNm.Width = 0.8228346F;
            // 
            // label1
            // 
            this.label1.Height = 0.1562992F;
            this.label1.HyperLink = null;
            this.label1.Left = 0.1458333F;
            this.label1.Name = "label1";
            this.label1.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt";
            this.label1.Text = "＜＜";
            this.label1.Top = 0F;
            this.label1.Width = 0.375F;
            // 
            // label2
            // 
            this.label2.Height = 0.1562992F;
            this.label2.HyperLink = null;
            this.label2.Left = 1.864961F;
            this.label2.Name = "label2";
            this.label2.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt";
            this.label2.Text = "＞＞";
            this.label2.Top = 0F;
            this.label2.Width = 0.375F;
            // 
            // grpFooterChiku
            // 
            this.grpFooterChiku.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblShokei,
            this.line2,
            this.txtShokeiCt,
            this.label3,
            this.txtShokeiKensu,
            this.txtSyokeiSuryo,
            this.txtShokeiTanka,
            this.txtShokeiMizuageKingaku,
            this.txtShokeiShohizei,
            this.txtShokeiZeikomiKingaku,
            this.txtShokeiGyokyouTesuryo,
            this.txtShokeiHako,
            this.txtShokeiSonota,
            this.txtShokeiTsumiatatekin,
            this.txtShokeiAzukarikin,
            this.txtShokeiHonnin,
            this.txtShokeiSashihikiKingaku});
            this.grpFooterChiku.Height = 0.3020833F;
            this.grpFooterChiku.Name = "grpFooterChiku";
            this.grpFooterChiku.Format += new System.EventHandler(this.grpFooterChiku_Format);
            // 
            // lblShokei
            // 
            this.lblShokei.Height = 0.1770833F;
            this.lblShokei.HyperLink = null;
            this.lblShokei.Left = 0.2165354F;
            this.lblShokei.Name = "lblShokei";
            this.lblShokei.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; ddo-char-set: 128";
            this.lblShokei.Text = "小計";
            this.lblShokei.Top = 0.03937008F;
            this.lblShokei.Width = 0.3646162F;
            // 
            // line2
            // 
            this.line2.Height = 0F;
            this.line2.Left = 0.08110237F;
            this.line2.LineWeight = 2F;
            this.line2.Name = "line2";
            this.line2.Top = 0.2165354F;
            this.line2.Width = 10.51178F;
            this.line2.X1 = 0.08110237F;
            this.line2.X2 = 10.59288F;
            this.line2.Y1 = 0.2165354F;
            this.line2.Y2 = 0.2165354F;
            // 
            // txtShokeiCt
            // 
            this.txtShokeiCt.DataField = "ITEM06";
            this.txtShokeiCt.Height = 0.1771654F;
            this.txtShokeiCt.Left = 0.7374016F;
            this.txtShokeiCt.Name = "txtShokeiCt";
            this.txtShokeiCt.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtShokeiCt.SummaryFunc = GrapeCity.ActiveReports.SectionReportModel.SummaryFunc.Count;
            this.txtShokeiCt.SummaryGroup = "grpHeaderChiku";
            this.txtShokeiCt.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtShokeiCt.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtShokeiCt.Text = "cd";
            this.txtShokeiCt.Top = 0.04173228F;
            this.txtShokeiCt.Width = 0.45F;
            // 
            // label3
            // 
            this.label3.Height = 0.1793799F;
            this.label3.HyperLink = null;
            this.label3.Left = 1.187402F;
            this.label3.Name = "label3";
            this.label3.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; ddo-char-set: 128";
            this.label3.Text = "人";
            this.label3.Top = 0.03937008F;
            this.label3.Width = 0.1770506F;
            // 
            // txtShokeiKensu
            // 
            this.txtShokeiKensu.DataField = "ITEM07";
            this.txtShokeiKensu.Height = 0.1771653F;
            this.txtShokeiKensu.Left = 1.922441F;
            this.txtShokeiKensu.Name = "txtShokeiKensu";
            this.txtShokeiKensu.OutputFormat = resources.GetString("txtShokeiKensu.OutputFormat");
            this.txtShokeiKensu.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtShokeiKensu.SummaryGroup = "grpHeaderChiku";
            this.txtShokeiKensu.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtShokeiKensu.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtShokeiKensu.Text = "kensu";
            this.txtShokeiKensu.Top = 0.03937008F;
            this.txtShokeiKensu.Width = 0.4500001F;
            // 
            // txtSyokeiSuryo
            // 
            this.txtSyokeiSuryo.DataField = "ITEM08";
            this.txtSyokeiSuryo.Height = 0.1771653F;
            this.txtSyokeiSuryo.Left = 2.397638F;
            this.txtSyokeiSuryo.Name = "txtSyokeiSuryo";
            this.txtSyokeiSuryo.OutputFormat = resources.GetString("txtSyokeiSuryo.OutputFormat");
            this.txtSyokeiSuryo.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtSyokeiSuryo.SummaryGroup = "grpHeaderChiku";
            this.txtSyokeiSuryo.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtSyokeiSuryo.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtSyokeiSuryo.Text = "suryo";
            this.txtSyokeiSuryo.Top = 0.03937008F;
            this.txtSyokeiSuryo.Width = 0.5448821F;
            // 
            // txtShokeiTanka
            // 
            this.txtShokeiTanka.Height = 0.1771653F;
            this.txtShokeiTanka.Left = 2.972047F;
            this.txtShokeiTanka.Name = "txtShokeiTanka";
            this.txtShokeiTanka.OutputFormat = resources.GetString("txtShokeiTanka.OutputFormat");
            this.txtShokeiTanka.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtShokeiTanka.SummaryGroup = "grpHeaderChiku";
            this.txtShokeiTanka.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtShokeiTanka.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtShokeiTanka.Text = "tanka";
            this.txtShokeiTanka.Top = 0.03937008F;
            this.txtShokeiTanka.Width = 0.5625987F;
            // 
            // txtShokeiMizuageKingaku
            // 
            this.txtShokeiMizuageKingaku.DataField = "ITEM09";
            this.txtShokeiMizuageKingaku.Height = 0.1771653F;
            this.txtShokeiMizuageKingaku.Left = 3.581496F;
            this.txtShokeiMizuageKingaku.Name = "txtShokeiMizuageKingaku";
            this.txtShokeiMizuageKingaku.OutputFormat = resources.GetString("txtShokeiMizuageKingaku.OutputFormat");
            this.txtShokeiMizuageKingaku.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtShokeiMizuageKingaku.SummaryGroup = "grpHeaderChiku";
            this.txtShokeiMizuageKingaku.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtShokeiMizuageKingaku.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtShokeiMizuageKingaku.Text = "mizuageKingaku";
            this.txtShokeiMizuageKingaku.Top = 0.03937008F;
            this.txtShokeiMizuageKingaku.Width = 0.6877956F;
            // 
            // txtShokeiShohizei
            // 
            this.txtShokeiShohizei.DataField = "ITEM10";
            this.txtShokeiShohizei.Height = 0.1771653F;
            this.txtShokeiShohizei.Left = 4.312599F;
            this.txtShokeiShohizei.Name = "txtShokeiShohizei";
            this.txtShokeiShohizei.OutputFormat = resources.GetString("txtShokeiShohizei.OutputFormat");
            this.txtShokeiShohizei.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtShokeiShohizei.SummaryGroup = "grpHeaderChiku";
            this.txtShokeiShohizei.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtShokeiShohizei.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtShokeiShohizei.Text = "shohizei";
            this.txtShokeiShohizei.Top = 0.03937008F;
            this.txtShokeiShohizei.Width = 0.5161424F;
            // 
            // txtShokeiZeikomiKingaku
            // 
            this.txtShokeiZeikomiKingaku.DataField = "ITEM11";
            this.txtShokeiZeikomiKingaku.Height = 0.1771653F;
            this.txtShokeiZeikomiKingaku.Left = 4.868111F;
            this.txtShokeiZeikomiKingaku.Name = "txtShokeiZeikomiKingaku";
            this.txtShokeiZeikomiKingaku.OutputFormat = resources.GetString("txtShokeiZeikomiKingaku.OutputFormat");
            this.txtShokeiZeikomiKingaku.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtShokeiZeikomiKingaku.SummaryGroup = "grpHeaderChiku";
            this.txtShokeiZeikomiKingaku.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtShokeiZeikomiKingaku.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtShokeiZeikomiKingaku.Text = "zeikomiKingaku";
            this.txtShokeiZeikomiKingaku.Top = 0.03937008F;
            this.txtShokeiZeikomiKingaku.Width = 0.7141728F;
            // 
            // txtShokeiGyokyouTesuryo
            // 
            this.txtShokeiGyokyouTesuryo.DataField = "ITEM12";
            this.txtShokeiGyokyouTesuryo.Height = 0.1771653F;
            this.txtShokeiGyokyouTesuryo.Left = 5.634253F;
            this.txtShokeiGyokyouTesuryo.Name = "txtShokeiGyokyouTesuryo";
            this.txtShokeiGyokyouTesuryo.OutputFormat = resources.GetString("txtShokeiGyokyouTesuryo.OutputFormat");
            this.txtShokeiGyokyouTesuryo.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtShokeiGyokyouTesuryo.SummaryGroup = "grpHeaderChiku";
            this.txtShokeiGyokyouTesuryo.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtShokeiGyokyouTesuryo.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtShokeiGyokyouTesuryo.Text = "gyokyouTesuryo";
            this.txtShokeiGyokyouTesuryo.Top = 0.03937008F;
            this.txtShokeiGyokyouTesuryo.Width = 0.7397413F;
            // 
            // txtShokeiHako
            // 
            this.txtShokeiHako.DataField = "ITEM13";
            this.txtShokeiHako.Height = 0.1771653F;
            this.txtShokeiHako.Left = 6.373994F;
            this.txtShokeiHako.Name = "txtShokeiHako";
            this.txtShokeiHako.OutputFormat = resources.GetString("txtShokeiHako.OutputFormat");
            this.txtShokeiHako.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtShokeiHako.SummaryGroup = "grpHeaderChiku";
            this.txtShokeiHako.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtShokeiHako.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtShokeiHako.Text = "hako";
            this.txtShokeiHako.Top = 0.03937008F;
            this.txtShokeiHako.Width = 0.4291334F;
            // 
            // txtShokeiSonota
            // 
            this.txtShokeiSonota.DataField = "ITEM14";
            this.txtShokeiSonota.Height = 0.1771653F;
            this.txtShokeiSonota.Left = 6.835011F;
            this.txtShokeiSonota.Name = "txtShokeiSonota";
            this.txtShokeiSonota.OutputFormat = resources.GetString("txtShokeiSonota.OutputFormat");
            this.txtShokeiSonota.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtShokeiSonota.SummaryGroup = "grpHeaderChiku";
            this.txtShokeiSonota.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtShokeiSonota.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtShokeiSonota.Text = "sonota";
            this.txtShokeiSonota.Top = 0.03937008F;
            this.txtShokeiSonota.Width = 0.8543301F;
            // 
            // txtShokeiTsumiatatekin
            // 
            this.txtShokeiTsumiatatekin.DataField = "ITEM15";
            this.txtShokeiTsumiatatekin.Height = 0.1771653F;
            this.txtShokeiTsumiatatekin.Left = 7.689342F;
            this.txtShokeiTsumiatatekin.Name = "txtShokeiTsumiatatekin";
            this.txtShokeiTsumiatatekin.OutputFormat = resources.GetString("txtShokeiTsumiatatekin.OutputFormat");
            this.txtShokeiTsumiatatekin.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtShokeiTsumiatatekin.SummaryGroup = "grpHeaderChiku";
            this.txtShokeiTsumiatatekin.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtShokeiTsumiatatekin.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtShokeiTsumiatatekin.Text = "tsumitatekin";
            this.txtShokeiTsumiatatekin.Top = 0.03937008F;
            this.txtShokeiTsumiatatekin.Width = 0.5649595F;
            // 
            // txtShokeiAzukarikin
            // 
            this.txtShokeiAzukarikin.DataField = "ITEM16";
            this.txtShokeiAzukarikin.Height = 0.1771653F;
            this.txtShokeiAzukarikin.Left = 8.254302F;
            this.txtShokeiAzukarikin.Name = "txtShokeiAzukarikin";
            this.txtShokeiAzukarikin.OutputFormat = resources.GetString("txtShokeiAzukarikin.OutputFormat");
            this.txtShokeiAzukarikin.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtShokeiAzukarikin.SummaryGroup = "grpHeaderChiku";
            this.txtShokeiAzukarikin.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtShokeiAzukarikin.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtShokeiAzukarikin.Text = "azukarikin";
            this.txtShokeiAzukarikin.Top = 0.03937008F;
            this.txtShokeiAzukarikin.Width = 0.5622029F;
            // 
            // txtShokeiHonnin
            // 
            this.txtShokeiHonnin.DataField = "ITEM17";
            this.txtShokeiHonnin.Height = 0.1771653F;
            this.txtShokeiHonnin.Left = 8.816505F;
            this.txtShokeiHonnin.Name = "txtShokeiHonnin";
            this.txtShokeiHonnin.OutputFormat = resources.GetString("txtShokeiHonnin.OutputFormat");
            this.txtShokeiHonnin.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtShokeiHonnin.SummaryGroup = "grpHeaderChiku";
            this.txtShokeiHonnin.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtShokeiHonnin.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtShokeiHonnin.Text = "honnin";
            this.txtShokeiHonnin.Top = 0.03937008F;
            this.txtShokeiHonnin.Width = 0.7897625F;
            // 
            // txtShokeiSashihikiKingaku
            // 
            this.txtShokeiSashihikiKingaku.DataField = "ITEM18";
            this.txtShokeiSashihikiKingaku.Height = 0.1771653F;
            this.txtShokeiSashihikiKingaku.Left = 9.64997F;
            this.txtShokeiSashihikiKingaku.Name = "txtShokeiSashihikiKingaku";
            this.txtShokeiSashihikiKingaku.OutputFormat = resources.GetString("txtShokeiSashihikiKingaku.OutputFormat");
            this.txtShokeiSashihikiKingaku.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtShokeiSashihikiKingaku.SummaryGroup = "grpHeaderChiku";
            this.txtShokeiSashihikiKingaku.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtShokeiSashihikiKingaku.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtShokeiSashihikiKingaku.Text = "sashihikiKingaku";
            this.txtShokeiSashihikiKingaku.Top = 0.03937008F;
            this.txtShokeiSashihikiKingaku.Width = 0.7555103F;
            // 
            // grpHeaderSeribi
            // 
            this.grpHeaderSeribi.DataField = "ITEM21";
            this.grpHeaderSeribi.Height = 0F;
            this.grpHeaderSeribi.Name = "grpHeaderSeribi";
            this.grpHeaderSeribi.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
            // 
            // grpFooterSeribi
            // 
            this.grpFooterSeribi.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblSokei,
            this.txtSokeiCt,
            this.label5,
            this.txtSokeiKensu,
            this.txtSokeiSuryo,
            this.txtSokeiTanka,
            this.txtSokeiMizuageKingaku,
            this.txtSokeiShohizei,
            this.txtSokeiZeikomiKingaku,
            this.txtSokeiGyokyouTesuryo,
            this.txtSokeiHako,
            this.txtSokeiSonota,
            this.txtSokeiTsumitatekin,
            this.txtSokeiAzukarikin,
            this.txtSokeiHonnin,
            this.txtSokeiSashihikiKingaku});
            this.grpFooterSeribi.Height = 0.1875F;
            this.grpFooterSeribi.Name = "grpFooterSeribi";
            this.grpFooterSeribi.Format += new System.EventHandler(this.grpFooterSeribi_Format);
            // 
            // lblSokei
            // 
            this.lblSokei.Height = 0.1770833F;
            this.lblSokei.HyperLink = null;
            this.lblSokei.Left = 0.2161417F;
            this.lblSokei.Name = "lblSokei";
            this.lblSokei.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; ddo-char-set: 128";
            this.lblSokei.Text = "総計";
            this.lblSokei.Top = 0F;
            this.lblSokei.Width = 0.3646161F;
            // 
            // txtSokeiCt
            // 
            this.txtSokeiCt.DataField = "ITEM06";
            this.txtSokeiCt.Height = 0.1771653F;
            this.txtSokeiCt.Left = 0.7370079F;
            this.txtSokeiCt.Name = "txtSokeiCt";
            this.txtSokeiCt.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtSokeiCt.SummaryFunc = GrapeCity.ActiveReports.SectionReportModel.SummaryFunc.Count;
            this.txtSokeiCt.SummaryGroup = "grpHeaderSeribi";
            this.txtSokeiCt.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtSokeiCt.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtSokeiCt.Text = "cd";
            this.txtSokeiCt.Top = 0.002362205F;
            this.txtSokeiCt.Width = 0.4500001F;
            // 
            // label5
            // 
            this.label5.Height = 0.1793799F;
            this.label5.HyperLink = null;
            this.label5.Left = 1.187008F;
            this.label5.Name = "label5";
            this.label5.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; ddo-char-set: 128";
            this.label5.Text = "人";
            this.label5.Top = 0F;
            this.label5.Width = 0.1770506F;
            // 
            // txtSokeiKensu
            // 
            this.txtSokeiKensu.DataField = "ITEM07";
            this.txtSokeiKensu.Height = 0.1771653F;
            this.txtSokeiKensu.Left = 1.922047F;
            this.txtSokeiKensu.Name = "txtSokeiKensu";
            this.txtSokeiKensu.OutputFormat = resources.GetString("txtSokeiKensu.OutputFormat");
            this.txtSokeiKensu.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtSokeiKensu.SummaryGroup = "grpHeaderSeribi";
            this.txtSokeiKensu.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtSokeiKensu.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtSokeiKensu.Text = "kensu";
            this.txtSokeiKensu.Top = 0F;
            this.txtSokeiKensu.Width = 0.4500001F;
            // 
            // txtSokeiSuryo
            // 
            this.txtSokeiSuryo.DataField = "ITEM08";
            this.txtSokeiSuryo.Height = 0.1771653F;
            this.txtSokeiSuryo.Left = 2.397244F;
            this.txtSokeiSuryo.Name = "txtSokeiSuryo";
            this.txtSokeiSuryo.OutputFormat = resources.GetString("txtSokeiSuryo.OutputFormat");
            this.txtSokeiSuryo.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtSokeiSuryo.SummaryGroup = "grpHeaderSeribi";
            this.txtSokeiSuryo.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtSokeiSuryo.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtSokeiSuryo.Text = "suryo";
            this.txtSokeiSuryo.Top = 0F;
            this.txtSokeiSuryo.Width = 0.5448821F;
            // 
            // txtSokeiTanka
            // 
            this.txtSokeiTanka.Height = 0.1771653F;
            this.txtSokeiTanka.Left = 2.971654F;
            this.txtSokeiTanka.Name = "txtSokeiTanka";
            this.txtSokeiTanka.OutputFormat = resources.GetString("txtSokeiTanka.OutputFormat");
            this.txtSokeiTanka.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtSokeiTanka.SummaryGroup = "grpHeaderSeribi";
            this.txtSokeiTanka.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtSokeiTanka.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtSokeiTanka.Text = "tanka";
            this.txtSokeiTanka.Top = 0F;
            this.txtSokeiTanka.Width = 0.5625988F;
            // 
            // txtSokeiMizuageKingaku
            // 
            this.txtSokeiMizuageKingaku.DataField = "ITEM09";
            this.txtSokeiMizuageKingaku.Height = 0.1771653F;
            this.txtSokeiMizuageKingaku.Left = 3.581103F;
            this.txtSokeiMizuageKingaku.Name = "txtSokeiMizuageKingaku";
            this.txtSokeiMizuageKingaku.OutputFormat = resources.GetString("txtSokeiMizuageKingaku.OutputFormat");
            this.txtSokeiMizuageKingaku.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtSokeiMizuageKingaku.SummaryGroup = "grpHeaderSeribi";
            this.txtSokeiMizuageKingaku.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtSokeiMizuageKingaku.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtSokeiMizuageKingaku.Text = "mizuageKingaku";
            this.txtSokeiMizuageKingaku.Top = 0F;
            this.txtSokeiMizuageKingaku.Width = 0.6877956F;
            // 
            // txtSokeiShohizei
            // 
            this.txtSokeiShohizei.DataField = "ITEM10";
            this.txtSokeiShohizei.Height = 0.1771653F;
            this.txtSokeiShohizei.Left = 4.312205F;
            this.txtSokeiShohizei.Name = "txtSokeiShohizei";
            this.txtSokeiShohizei.OutputFormat = resources.GetString("txtSokeiShohizei.OutputFormat");
            this.txtSokeiShohizei.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtSokeiShohizei.SummaryGroup = "grpHeaderSeribi";
            this.txtSokeiShohizei.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtSokeiShohizei.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtSokeiShohizei.Text = "shohizei";
            this.txtSokeiShohizei.Top = 0F;
            this.txtSokeiShohizei.Width = 0.5161424F;
            // 
            // txtSokeiZeikomiKingaku
            // 
            this.txtSokeiZeikomiKingaku.DataField = "ITEM11";
            this.txtSokeiZeikomiKingaku.Height = 0.1771653F;
            this.txtSokeiZeikomiKingaku.Left = 4.867717F;
            this.txtSokeiZeikomiKingaku.Name = "txtSokeiZeikomiKingaku";
            this.txtSokeiZeikomiKingaku.OutputFormat = resources.GetString("txtSokeiZeikomiKingaku.OutputFormat");
            this.txtSokeiZeikomiKingaku.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtSokeiZeikomiKingaku.SummaryGroup = "grpHeaderSeribi";
            this.txtSokeiZeikomiKingaku.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtSokeiZeikomiKingaku.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtSokeiZeikomiKingaku.Text = "zeikomiKingaku";
            this.txtSokeiZeikomiKingaku.Top = 0F;
            this.txtSokeiZeikomiKingaku.Width = 0.714173F;
            // 
            // txtSokeiGyokyouTesuryo
            // 
            this.txtSokeiGyokyouTesuryo.DataField = "ITEM12";
            this.txtSokeiGyokyouTesuryo.Height = 0.1771653F;
            this.txtSokeiGyokyouTesuryo.Left = 5.633859F;
            this.txtSokeiGyokyouTesuryo.Name = "txtSokeiGyokyouTesuryo";
            this.txtSokeiGyokyouTesuryo.OutputFormat = resources.GetString("txtSokeiGyokyouTesuryo.OutputFormat");
            this.txtSokeiGyokyouTesuryo.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtSokeiGyokyouTesuryo.SummaryGroup = "grpHeaderSeribi";
            this.txtSokeiGyokyouTesuryo.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtSokeiGyokyouTesuryo.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtSokeiGyokyouTesuryo.Text = "gyokyouTesuryo";
            this.txtSokeiGyokyouTesuryo.Top = 0F;
            this.txtSokeiGyokyouTesuryo.Width = 0.7397417F;
            // 
            // txtSokeiHako
            // 
            this.txtSokeiHako.DataField = "ITEM13";
            this.txtSokeiHako.Height = 0.1771653F;
            this.txtSokeiHako.Left = 6.3736F;
            this.txtSokeiHako.Name = "txtSokeiHako";
            this.txtSokeiHako.OutputFormat = resources.GetString("txtSokeiHako.OutputFormat");
            this.txtSokeiHako.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtSokeiHako.SummaryGroup = "grpHeaderSeribi";
            this.txtSokeiHako.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtSokeiHako.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtSokeiHako.Text = "hako";
            this.txtSokeiHako.Top = 0F;
            this.txtSokeiHako.Width = 0.4291334F;
            // 
            // txtSokeiSonota
            // 
            this.txtSokeiSonota.DataField = "ITEM14";
            this.txtSokeiSonota.Height = 0.1771653F;
            this.txtSokeiSonota.Left = 6.834618F;
            this.txtSokeiSonota.Name = "txtSokeiSonota";
            this.txtSokeiSonota.OutputFormat = resources.GetString("txtSokeiSonota.OutputFormat");
            this.txtSokeiSonota.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtSokeiSonota.SummaryGroup = "grpHeaderSeribi";
            this.txtSokeiSonota.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtSokeiSonota.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtSokeiSonota.Text = "sonota";
            this.txtSokeiSonota.Top = 0F;
            this.txtSokeiSonota.Width = 0.8543298F;
            // 
            // txtSokeiTsumitatekin
            // 
            this.txtSokeiTsumitatekin.DataField = "ITEM15";
            this.txtSokeiTsumitatekin.Height = 0.1771653F;
            this.txtSokeiTsumitatekin.Left = 7.688947F;
            this.txtSokeiTsumitatekin.Name = "txtSokeiTsumitatekin";
            this.txtSokeiTsumitatekin.OutputFormat = resources.GetString("txtSokeiTsumitatekin.OutputFormat");
            this.txtSokeiTsumitatekin.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtSokeiTsumitatekin.SummaryGroup = "grpHeaderSeribi";
            this.txtSokeiTsumitatekin.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtSokeiTsumitatekin.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtSokeiTsumitatekin.Text = "tsumitatekin";
            this.txtSokeiTsumitatekin.Top = 0F;
            this.txtSokeiTsumitatekin.Width = 0.5649595F;
            // 
            // txtSokeiAzukarikin
            // 
            this.txtSokeiAzukarikin.DataField = "ITEM16";
            this.txtSokeiAzukarikin.Height = 0.1771653F;
            this.txtSokeiAzukarikin.Left = 8.253912F;
            this.txtSokeiAzukarikin.Name = "txtSokeiAzukarikin";
            this.txtSokeiAzukarikin.OutputFormat = resources.GetString("txtSokeiAzukarikin.OutputFormat");
            this.txtSokeiAzukarikin.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtSokeiAzukarikin.SummaryGroup = "grpHeaderSeribi";
            this.txtSokeiAzukarikin.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtSokeiAzukarikin.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtSokeiAzukarikin.Text = "azukarikin";
            this.txtSokeiAzukarikin.Top = 0F;
            this.txtSokeiAzukarikin.Width = 0.5622029F;
            // 
            // txtSokeiHonnin
            // 
            this.txtSokeiHonnin.DataField = "ITEM17";
            this.txtSokeiHonnin.Height = 0.1771653F;
            this.txtSokeiHonnin.Left = 8.816113F;
            this.txtSokeiHonnin.Name = "txtSokeiHonnin";
            this.txtSokeiHonnin.OutputFormat = resources.GetString("txtSokeiHonnin.OutputFormat");
            this.txtSokeiHonnin.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtSokeiHonnin.SummaryGroup = "grpHeaderSeribi";
            this.txtSokeiHonnin.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtSokeiHonnin.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtSokeiHonnin.Text = "honnin";
            this.txtSokeiHonnin.Top = 0F;
            this.txtSokeiHonnin.Width = 0.7897626F;
            // 
            // txtSokeiSashihikiKingaku
            // 
            this.txtSokeiSashihikiKingaku.DataField = "ITEM18";
            this.txtSokeiSashihikiKingaku.Height = 0.1771653F;
            this.txtSokeiSashihikiKingaku.Left = 9.649578F;
            this.txtSokeiSashihikiKingaku.Name = "txtSokeiSashihikiKingaku";
            this.txtSokeiSashihikiKingaku.OutputFormat = resources.GetString("txtSokeiSashihikiKingaku.OutputFormat");
            this.txtSokeiSashihikiKingaku.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtSokeiSashihikiKingaku.SummaryGroup = "grpHeaderSeribi";
            this.txtSokeiSashihikiKingaku.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtSokeiSashihikiKingaku.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtSokeiSashihikiKingaku.Text = "sashihikiKingaku";
            this.txtSokeiSashihikiKingaku.Top = 0F;
            this.txtSokeiSashihikiKingaku.Width = 0.7555104F;
            // 
            // HANR2131R
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.1968504F;
            this.PageSettings.Margins.Left = 0.3937007F;
            this.PageSettings.Margins.Right = 0.3937007F;
            this.PageSettings.Margins.Top = 0.7874016F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 10.82677F;
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.grpHeaderSeribi);
            this.Sections.Add(this.grpHeaderChiku);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.grpFooterChiku);
            this.Sections.Add(this.grpFooterSeribi);
            this.Sections.Add(this.pageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShiharaiKbn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rptDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKumiaiin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKensu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSuryo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTanka)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMizuageKingaku)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblShohizei)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblZeikomiKingaku)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTesuryo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHako)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSonota)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTsumitatekin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAzukarikin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHonnin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSashihikiKingaku)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSeribi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSeribi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKumiaiin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKensu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSuryo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTanka)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMizuageKingaku)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShohizei)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZeikomiKingaku)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyokyouTesuryo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHako)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSonota)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTsumitatekin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAzukarikin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHonnin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSashihikiKingaku)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChikuCd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChikuNm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblShokei)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShokeiCt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShokeiKensu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSyokeiSuryo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShokeiTanka)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShokeiMizuageKingaku)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShokeiShohizei)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShokeiZeikomiKingaku)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShokeiGyokyouTesuryo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShokeiHako)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShokeiSonota)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShokeiTsumiatatekin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShokeiAzukarikin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShokeiHonnin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShokeiSashihikiKingaku)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSokei)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSokeiCt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSokeiKensu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSokeiSuryo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSokeiTanka)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSokeiMizuageKingaku)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSokeiShohizei)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSokeiZeikomiKingaku)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSokeiGyokyouTesuryo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSokeiHako)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSokeiSonota)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSokeiTsumitatekin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSokeiAzukarikin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSokeiHonnin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSokeiSashihikiKingaku)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtShiharaiKbn;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader grpHeaderChiku;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter grpFooterChiku;
        private GrapeCity.ActiveReports.SectionReportModel.ReportInfo rptDate;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtChikuCd;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtChikuNm;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader grpHeaderSeribi;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter grpFooterSeribi;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblCd;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblKumiaiin;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblKensu;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSuryo;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTanka;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblMizuageKingaku;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblShohizei;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblZeikomiKingaku;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTesuryo;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblHako;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSonota;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTsumitatekin;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblAzukarikin;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblHonnin;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSashihikiKingaku;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSeribi;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSeribi;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCd;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKumiaiin;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKensu;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSuryo;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTanka;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMizuageKingaku;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtShohizei;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtZeikomiKingaku;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGyokyouTesuryo;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHako;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSonota;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTsumitatekin;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAzukarikin;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHonnin;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSashihikiKingaku;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblShokei;
        private GrapeCity.ActiveReports.SectionReportModel.Line line2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtShokeiCt;
        private GrapeCity.ActiveReports.SectionReportModel.Label label3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtShokeiKensu;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSyokeiSuryo;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtShokeiTanka;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtShokeiMizuageKingaku;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtShokeiShohizei;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtShokeiZeikomiKingaku;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtShokeiGyokyouTesuryo;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtShokeiHako;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtShokeiSonota;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtShokeiTsumiatatekin;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtShokeiAzukarikin;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtShokeiHonnin;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtShokeiSashihikiKingaku;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSokei;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSokeiCt;
        private GrapeCity.ActiveReports.SectionReportModel.Label label5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSokeiKensu;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSokeiSuryo;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSokeiTanka;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSokeiMizuageKingaku;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSokeiShohizei;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSokeiZeikomiKingaku;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSokeiGyokyouTesuryo;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSokeiHako;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSokeiSonota;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSokeiTsumitatekin;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSokeiAzukarikin;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSokeiHonnin;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSokeiSashihikiKingaku;
    }
}
