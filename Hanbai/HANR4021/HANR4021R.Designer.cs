﻿namespace jp.co.fsi.han.hanr4021
{
    /// <summary>
    /// HANR4021R の概要の説明です。
    /// </summary>
    partial class HANR4021R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(HANR4021R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.テキスト001 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキストData = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキストPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.直線31 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.ラベル33 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル34 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル004S = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル004K = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル41 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル42 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル45 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル46 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル49 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル50 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル53 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル54 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル61 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル62 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル64 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル65 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.テキスト2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル124 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.テキスト003S = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト003K = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト004S = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト004K = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト005S = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト005K = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト006S = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト006K = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト007S = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト007K = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト008S = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト008K = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト139 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト140 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル159 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.直線160 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.テキスト009 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト010 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト011 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト012 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト013 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト014 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト015 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト016 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト017 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト018 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト019 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト020 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト021 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト022 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.数量合計 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.金額合計 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.直線89 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.reportHeader1 = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.reportFooter1 = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.テキスト143 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト144 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト145 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト146 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト147 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト148 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト149 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト150 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト151 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト152 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト153 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト154 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.数量総合計 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.金額総合計 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.直線157 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.ラベル158 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト001)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキストData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキストPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル004S)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル004K)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル54)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル61)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル62)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル64)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル65)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル124)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト003S)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト003K)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト004S)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト004K)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト005S)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト005K)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト006S)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト006K)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト007S)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト007K)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト008S)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト008K)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト139)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト140)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル159)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト009)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト010)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト011)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト012)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト013)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト014)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト015)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト016)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト017)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト018)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト019)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト020)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト021)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト022)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.数量合計)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.金額合計)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト143)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト144)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト145)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト146)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト147)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト148)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト149)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト150)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト151)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト152)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト153)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト154)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.数量総合計)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.金額総合計)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル158)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.テキスト001,
            this.テキストData,
            this.テキストPage,
            this.ラベル1,
            this.ラベル3,
            this.直線31,
            this.ラベル33,
            this.ラベル34,
            this.ラベル004S,
            this.ラベル004K,
            this.ラベル41,
            this.ラベル42,
            this.ラベル45,
            this.ラベル46,
            this.ラベル49,
            this.ラベル50,
            this.ラベル53,
            this.ラベル54,
            this.ラベル61,
            this.ラベル62,
            this.ラベル64,
            this.ラベル65,
            this.テキスト2,
            this.ラベル124,
            this.テキスト003S,
            this.テキスト003K,
            this.テキスト004S,
            this.テキスト004K,
            this.テキスト005S,
            this.テキスト005K,
            this.テキスト006S,
            this.テキスト006K,
            this.テキスト007S,
            this.テキスト007K,
            this.テキスト008S,
            this.テキスト008K,
            this.テキスト139,
            this.テキスト140,
            this.ラベル159,
            this.直線160});
            this.pageHeader.Height = 0.7986111F;
            this.pageHeader.Name = "pageHeader";
            this.pageHeader.Format += new System.EventHandler(this.pageHeader_Format);
            // 
            // テキスト001
            // 
            this.テキスト001.DataField = "ITEM01";
            this.テキスト001.Height = 0.15625F;
            this.テキスト001.Left = 0.05826772F;
            this.テキスト001.Name = "テキスト001";
            this.テキスト001.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: left; ddo-char-set: 1";
            this.テキスト001.Tag = "";
            this.テキスト001.Text = "ITEM01";
            this.テキスト001.Top = 0.03897638F;
            this.テキスト001.Width = 0.9104167F;
            // 
            // テキストData
            // 
            this.テキストData.Height = 0.15625F;
            this.テキストData.Left = 10.4375F;
            this.テキストData.Name = "テキストData";
            this.テキストData.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: right; ddo-char-set: 128";
            this.テキストData.Tag = "";
            this.テキストData.Text = null;
            this.テキストData.Top = 0F;
            this.テキストData.Width = 1.16931F;
            // 
            // テキストPage
            // 
            this.テキストPage.Height = 0.15625F;
            this.テキストPage.Left = 12.58307F;
            this.テキストPage.Name = "テキストPage";
            this.テキストPage.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: right; ddo-char-set: 128";
            this.テキストPage.SummaryFunc = GrapeCity.ActiveReports.SectionReportModel.SummaryFunc.Count;
            this.テキストPage.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキストPage.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.テキストPage.Tag = "";
            this.テキストPage.Text = null;
            this.テキストPage.Top = 0F;
            this.テキストPage.Width = 0.754425F;
            // 
            // ラベル1
            // 
            this.ラベル1.Height = 0.15625F;
            this.ラベル1.HyperLink = null;
            this.ラベル1.Left = 0.876378F;
            this.ラベル1.Name = "ラベル1";
            this.ラベル1.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-alig" +
    "n: left; ddo-char-set: 128";
            this.ラベル1.Tag = "";
            this.ラベル1.Text = "船主名称";
            this.ラベル1.Top = 0.4374016F;
            this.ラベル1.Width = 0.7396373F;
            // 
            // ラベル3
            // 
            this.ラベル3.Height = 0.15625F;
            this.ラベル3.HyperLink = null;
            this.ラベル3.Left = 3.833859F;
            this.ラベル3.Name = "ラベル3";
            this.ラベル3.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "left; ddo-char-set: 1";
            this.ラベル3.Tag = "";
            this.ラベル3.Text = "月数量";
            this.ラベル3.Top = 0.4374016F;
            this.ラベル3.Width = 0.5F;
            // 
            // 直線31
            // 
            this.直線31.Height = 0F;
            this.直線31.Left = 4.768372E-07F;
            this.直線31.LineWeight = 1F;
            this.直線31.Name = "直線31";
            this.直線31.Tag = "";
            this.直線31.Top = 0.7916667F;
            this.直線31.Width = 13.38583F;
            this.直線31.X1 = 4.768372E-07F;
            this.直線31.X2 = 13.38583F;
            this.直線31.Y1 = 0.7916667F;
            this.直線31.Y2 = 0.7916667F;
            // 
            // ラベル33
            // 
            this.ラベル33.Height = 0.15625F;
            this.ラベル33.HyperLink = null;
            this.ラベル33.Left = 0.2F;
            this.ラベル33.Name = "ラベル33";
            this.ラベル33.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-alig" +
    "n: right; ddo-char-set: 128";
            this.ラベル33.Tag = "";
            this.ラベル33.Text = "船主CD";
            this.ラベル33.Top = 0.4374016F;
            this.ラベル33.Width = 0.4897638F;
            // 
            // ラベル34
            // 
            this.ラベル34.Height = 0.15625F;
            this.ラベル34.HyperLink = null;
            this.ラベル34.Left = 3.833859F;
            this.ラベル34.Name = "ラベル34";
            this.ラベル34.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "left; ddo-char-set: 1";
            this.ラベル34.Tag = "";
            this.ラベル34.Text = "月金額";
            this.ラベル34.Top = 0.6144849F;
            this.ラベル34.Width = 0.5F;
            // 
            // ラベル004S
            // 
            this.ラベル004S.Height = 0.15625F;
            this.ラベル004S.HyperLink = null;
            this.ラベル004S.Left = 5.302362F;
            this.ラベル004S.Name = "ラベル004S";
            this.ラベル004S.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "left; ddo-char-set: 1";
            this.ラベル004S.Tag = "";
            this.ラベル004S.Text = "月数量";
            this.ラベル004S.Top = 0.4374016F;
            this.ラベル004S.Width = 0.5F;
            // 
            // ラベル004K
            // 
            this.ラベル004K.Height = 0.15625F;
            this.ラベル004K.HyperLink = null;
            this.ラベル004K.Left = 5.302362F;
            this.ラベル004K.Name = "ラベル004K";
            this.ラベル004K.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "left; ddo-char-set: 1";
            this.ラベル004K.Tag = "";
            this.ラベル004K.Text = "月金額";
            this.ラベル004K.Top = 0.6144849F;
            this.ラベル004K.Width = 0.5F;
            // 
            // ラベル41
            // 
            this.ラベル41.Height = 0.15625F;
            this.ラベル41.HyperLink = null;
            this.ラベル41.Left = 6.885385F;
            this.ラベル41.Name = "ラベル41";
            this.ラベル41.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "left; ddo-char-set: 1";
            this.ラベル41.Tag = "";
            this.ラベル41.Text = "月数量";
            this.ラベル41.Top = 0.4375F;
            this.ラベル41.Width = 0.5F;
            // 
            // ラベル42
            // 
            this.ラベル42.Height = 0.15625F;
            this.ラベル42.HyperLink = null;
            this.ラベル42.Left = 6.885385F;
            this.ラベル42.Name = "ラベル42";
            this.ラベル42.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "left; ddo-char-set: 1";
            this.ラベル42.Tag = "";
            this.ラベル42.Text = "月金額";
            this.ラベル42.Top = 0.6145833F;
            this.ラベル42.Width = 0.5F;
            // 
            // ラベル45
            // 
            this.ラベル45.Height = 0.15625F;
            this.ラベル45.HyperLink = null;
            this.ラベル45.Left = 8.32535F;
            this.ラベル45.Name = "ラベル45";
            this.ラベル45.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "left; ddo-char-set: 1";
            this.ラベル45.Tag = "";
            this.ラベル45.Text = "月数量";
            this.ラベル45.Top = 0.4375F;
            this.ラベル45.Width = 0.5F;
            // 
            // ラベル46
            // 
            this.ラベル46.Height = 0.15625F;
            this.ラベル46.HyperLink = null;
            this.ラベル46.Left = 8.32535F;
            this.ラベル46.Name = "ラベル46";
            this.ラベル46.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "left; ddo-char-set: 1";
            this.ラベル46.Tag = "";
            this.ラベル46.Text = "月金額";
            this.ラベル46.Top = 0.6145833F;
            this.ラベル46.Width = 0.5F;
            // 
            // ラベル49
            // 
            this.ラベル49.Height = 0.15625F;
            this.ラベル49.HyperLink = null;
            this.ラベル49.Left = 9.800345F;
            this.ラベル49.Name = "ラベル49";
            this.ラベル49.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "left; ddo-char-set: 1";
            this.ラベル49.Tag = "";
            this.ラベル49.Text = "月数量";
            this.ラベル49.Top = 0.4375F;
            this.ラベル49.Width = 0.5F;
            // 
            // ラベル50
            // 
            this.ラベル50.Height = 0.15625F;
            this.ラベル50.HyperLink = null;
            this.ラベル50.Left = 9.800345F;
            this.ラベル50.Name = "ラベル50";
            this.ラベル50.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "left; ddo-char-set: 1";
            this.ラベル50.Tag = "";
            this.ラベル50.Text = "月金額";
            this.ラベル50.Top = 0.6145833F;
            this.ラベル50.Width = 0.5F;
            // 
            // ラベル53
            // 
            this.ラベル53.Height = 0.15625F;
            this.ラベル53.HyperLink = null;
            this.ラベル53.Left = 11.21688F;
            this.ラベル53.Name = "ラベル53";
            this.ラベル53.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "left; ddo-char-set: 1";
            this.ラベル53.Tag = "";
            this.ラベル53.Text = "月数量";
            this.ラベル53.Top = 0.4373688F;
            this.ラベル53.Width = 0.5F;
            // 
            // ラベル54
            // 
            this.ラベル54.Height = 0.15625F;
            this.ラベル54.HyperLink = null;
            this.ラベル54.Left = 11.21688F;
            this.ラベル54.Name = "ラベル54";
            this.ラベル54.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "left; ddo-char-set: 1";
            this.ラベル54.Tag = "";
            this.ラベル54.Text = "月金額";
            this.ラベル54.Top = 0.6144521F;
            this.ラベル54.Width = 0.5F;
            // 
            // ラベル61
            // 
            this.ラベル61.Height = 0.15625F;
            this.ラベル61.HyperLink = null;
            this.ラベル61.Left = 12.77352F;
            this.ラベル61.Name = "ラベル61";
            this.ラベル61.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "left; ddo-char-set: 1";
            this.ラベル61.Tag = "";
            this.ラベル61.Text = "数量";
            this.ラベル61.Top = 0.4374016F;
            this.ラベル61.Width = 0.3020833F;
            // 
            // ラベル62
            // 
            this.ラベル62.Height = 0.15625F;
            this.ラベル62.HyperLink = null;
            this.ラベル62.Left = 12.77352F;
            this.ラベル62.Name = "ラベル62";
            this.ラベル62.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "left; ddo-char-set: 1";
            this.ラベル62.Tag = "";
            this.ラベル62.Text = "金額";
            this.ラベル62.Top = 0.6144849F;
            this.ラベル62.Width = 0.3020833F;
            // 
            // ラベル64
            // 
            this.ラベル64.Height = 0.15625F;
            this.ラベル64.HyperLink = null;
            this.ラベル64.Left = 11.60669F;
            this.ラベル64.Name = "ラベル64";
            this.ラベル64.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128";
            this.ラベル64.Tag = "";
            this.ラベル64.Text = "作成";
            this.ラベル64.Top = 0F;
            this.ラベル64.Width = 0.3734763F;
            // 
            // ラベル65
            // 
            this.ラベル65.Height = 0.25F;
            this.ラベル65.HyperLink = null;
            this.ラベル65.Left = 4.885417F;
            this.ラベル65.Name = "ラベル65";
            this.ラベル65.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 16pt; font-weight: bold; text-align:" +
    " left; ddo-char-set: 1";
            this.ラベル65.Tag = "";
            this.ラベル65.Text = "操業制限損失保証一覧表";
            this.ラベル65.Top = 0F;
            this.ラベル65.Width = 2.5F;
            // 
            // テキスト2
            // 
            this.テキスト2.DataField = "ITEM02";
            this.テキスト2.Height = 0.25F;
            this.テキスト2.Left = 7.5625F;
            this.テキスト2.Name = "テキスト2";
            this.テキスト2.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 16pt; font-" +
    "weight: bold; text-align: left; ddo-char-set: 1";
            this.テキスト2.Tag = "";
            this.テキスト2.Text = "ITEM02";
            this.テキスト2.Top = 0F;
            this.テキスト2.Width = 0.6291667F;
            // 
            // ラベル124
            // 
            this.ラベル124.Height = 0.25F;
            this.ラベル124.HyperLink = null;
            this.ラベル124.Left = 7.44882F;
            this.ラベル124.Name = "ラベル124";
            this.ラベル124.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 16pt; font-weight: bold; text-align:" +
    " left; ddo-char-set: 1";
            this.ラベル124.Tag = "";
            this.ラベル124.Text = "(    )";
            this.ラベル124.Top = 0.003937008F;
            this.ラベル124.Width = 0.7874012F;
            // 
            // テキスト003S
            // 
            this.テキスト003S.DataField = "ITEM03";
            this.テキスト003S.Height = 0.15625F;
            this.テキスト003S.Left = 3.479692F;
            this.テキスト003S.Name = "テキスト003S";
            this.テキスト003S.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: right; ddo-char-set: 1";
            this.テキスト003S.Tag = "";
            this.テキスト003S.Text = "ITEM03";
            this.テキスト003S.Top = 0.4374016F;
            this.テキスト003S.Width = 0.3583333F;
            // 
            // テキスト003K
            // 
            this.テキスト003K.DataField = "ITEM03";
            this.テキスト003K.Height = 0.15625F;
            this.テキスト003K.Left = 3.479692F;
            this.テキスト003K.Name = "テキスト003K";
            this.テキスト003K.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: right; ddo-char-set: 1";
            this.テキスト003K.Tag = "";
            this.テキスト003K.Text = "ITEM03";
            this.テキスト003K.Top = 0.6144849F;
            this.テキスト003K.Width = 0.3583333F;
            // 
            // テキスト004S
            // 
            this.テキスト004S.DataField = "ITEM04";
            this.テキスト004S.Height = 0.15625F;
            this.テキスト004S.Left = 4.948195F;
            this.テキスト004S.Name = "テキスト004S";
            this.テキスト004S.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: right; ddo-char-set: 1";
            this.テキスト004S.Tag = "";
            this.テキスト004S.Text = "ITEM04";
            this.テキスト004S.Top = 0.4374016F;
            this.テキスト004S.Width = 0.3583333F;
            // 
            // テキスト004K
            // 
            this.テキスト004K.DataField = "ITEM04";
            this.テキスト004K.Height = 0.15625F;
            this.テキスト004K.Left = 4.948195F;
            this.テキスト004K.Name = "テキスト004K";
            this.テキスト004K.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: right; ddo-char-set: 1";
            this.テキスト004K.Tag = "";
            this.テキスト004K.Text = "ITEM04";
            this.テキスト004K.Top = 0.6144849F;
            this.テキスト004K.Width = 0.3583333F;
            // 
            // テキスト005S
            // 
            this.テキスト005S.DataField = "ITEM05";
            this.テキスト005S.Height = 0.15625F;
            this.テキスト005S.Left = 6.520801F;
            this.テキスト005S.Name = "テキスト005S";
            this.テキスト005S.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: right; ddo-char-set: 1";
            this.テキスト005S.Tag = "";
            this.テキスト005S.Text = "ITEM05";
            this.テキスト005S.Top = 0.4375F;
            this.テキスト005S.Width = 0.36875F;
            // 
            // テキスト005K
            // 
            this.テキスト005K.DataField = "ITEM05";
            this.テキスト005K.Height = 0.15625F;
            this.テキスト005K.Left = 6.520801F;
            this.テキスト005K.Name = "テキスト005K";
            this.テキスト005K.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: right; ddo-char-set: 1";
            this.テキスト005K.Tag = "";
            this.テキスト005K.Text = "ITEM05";
            this.テキスト005K.Top = 0.6145833F;
            this.テキスト005K.Width = 0.36875F;
            // 
            // テキスト006S
            // 
            this.テキスト006S.DataField = "ITEM06";
            this.テキスト006S.Height = 0.15625F;
            this.テキスト006S.Left = 7.971183F;
            this.テキスト006S.Name = "テキスト006S";
            this.テキスト006S.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: right; ddo-char-set: 1";
            this.テキスト006S.Tag = "";
            this.テキスト006S.Text = "ITEM06";
            this.テキスト006S.Top = 0.4375F;
            this.テキスト006S.Width = 0.3583333F;
            // 
            // テキスト006K
            // 
            this.テキスト006K.DataField = "ITEM06";
            this.テキスト006K.Height = 0.15625F;
            this.テキスト006K.Left = 7.971183F;
            this.テキスト006K.Name = "テキスト006K";
            this.テキスト006K.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: right; ddo-char-set: 1";
            this.テキスト006K.Tag = "";
            this.テキスト006K.Text = "ITEM06";
            this.テキスト006K.Top = 0.6145833F;
            this.テキスト006K.Width = 0.3583333F;
            // 
            // テキスト007S
            // 
            this.テキスト007S.DataField = "ITEM07";
            this.テキスト007S.Height = 0.15625F;
            this.テキスト007S.Left = 9.446177F;
            this.テキスト007S.Name = "テキスト007S";
            this.テキスト007S.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: right; ddo-char-set: 1";
            this.テキスト007S.Tag = "";
            this.テキスト007S.Text = "ITEM07";
            this.テキスト007S.Top = 0.4375F;
            this.テキスト007S.Width = 0.3583333F;
            // 
            // テキスト007K
            // 
            this.テキスト007K.DataField = "ITEM07";
            this.テキスト007K.Height = 0.15625F;
            this.テキスト007K.Left = 9.446177F;
            this.テキスト007K.Name = "テキスト007K";
            this.テキスト007K.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: right; ddo-char-set: 1";
            this.テキスト007K.Tag = "";
            this.テキスト007K.Text = "ITEM07";
            this.テキスト007K.Top = 0.6145833F;
            this.テキスト007K.Width = 0.3583333F;
            // 
            // テキスト008S
            // 
            this.テキスト008S.DataField = "ITEM08";
            this.テキスト008S.Height = 0.15625F;
            this.テキスト008S.Left = 10.86271F;
            this.テキスト008S.Name = "テキスト008S";
            this.テキスト008S.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: right; ddo-char-set: 1";
            this.テキスト008S.Tag = "";
            this.テキスト008S.Text = "ITEM08";
            this.テキスト008S.Top = 0.4373688F;
            this.テキスト008S.Width = 0.3583333F;
            // 
            // テキスト008K
            // 
            this.テキスト008K.DataField = "ITEM08";
            this.テキスト008K.Height = 0.15625F;
            this.テキスト008K.Left = 10.86271F;
            this.テキスト008K.Name = "テキスト008K";
            this.テキスト008K.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: right; ddo-char-set: 1";
            this.テキスト008K.Tag = "";
            this.テキスト008K.Text = "ITEM08";
            this.テキスト008K.Top = 0.6144521F;
            this.テキスト008K.Width = 0.3583333F;
            // 
            // テキスト139
            // 
            this.テキスト139.DataField = "ITEM02";
            this.テキスト139.Height = 0.15625F;
            this.テキスト139.Left = 11.98186F;
            this.テキスト139.Name = "テキスト139";
            this.テキスト139.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: right; ddo-char-set: 1";
            this.テキスト139.Tag = "";
            this.テキスト139.Text = "ITEM02";
            this.テキスト139.Top = 0.4374016F;
            this.テキスト139.Width = 0.7958333F;
            // 
            // テキスト140
            // 
            this.テキスト140.DataField = "ITEM02";
            this.テキスト140.Height = 0.15625F;
            this.テキスト140.Left = 11.98186F;
            this.テキスト140.Name = "テキスト140";
            this.テキスト140.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: right; ddo-char-set: 1";
            this.テキスト140.Tag = "";
            this.テキスト140.Text = "ITEM02";
            this.テキスト140.Top = 0.6144849F;
            this.テキスト140.Width = 0.7958333F;
            // 
            // ラベル159
            // 
            this.ラベル159.Height = 0.15625F;
            this.ラベル159.HyperLink = null;
            this.ラベル159.Left = 12.2811F;
            this.ラベル159.Name = "ラベル159";
            this.ラベル159.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.ラベル159.Tag = "";
            this.ラベル159.Text = "【税込み】";
            this.ラベル159.Top = 0.2291667F;
            this.ラベル159.Width = 0.9480639F;
            // 
            // 直線160
            // 
            this.直線160.Height = 0F;
            this.直線160.Left = 0F;
            this.直線160.LineWeight = 1F;
            this.直線160.Name = "直線160";
            this.直線160.Tag = "";
            this.直線160.Top = 0.3854331F;
            this.直線160.Width = 13.38583F;
            this.直線160.X1 = 0F;
            this.直線160.X2 = 13.38583F;
            this.直線160.Y1 = 0.3854331F;
            this.直線160.Y2 = 0.3854331F;
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.テキスト009,
            this.テキスト010,
            this.テキスト011,
            this.テキスト012,
            this.テキスト013,
            this.テキスト014,
            this.テキスト015,
            this.テキスト016,
            this.テキスト017,
            this.テキスト018,
            this.テキスト019,
            this.テキスト020,
            this.テキスト021,
            this.テキスト022,
            this.数量合計,
            this.金額合計,
            this.直線89});
            this.detail.Height = 0.4168636F;
            this.detail.Name = "detail";
            this.detail.Format += new System.EventHandler(this.detail_Format);
            // 
            // テキスト009
            // 
            this.テキスト009.DataField = "ITEM09";
            this.テキスト009.Height = 0.15625F;
            this.テキスト009.Left = 0.1334646F;
            this.テキスト009.Name = "テキスト009";
            this.テキスト009.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: right; ddo-char-set: 128";
            this.テキスト009.Tag = "";
            this.テキスト009.Text = "ITEM09";
            this.テキスト009.Top = 0.05905512F;
            this.テキスト009.Width = 0.5562499F;
            // 
            // テキスト010
            // 
            this.テキスト010.DataField = "ITEM10";
            this.テキスト010.Height = 0.15625F;
            this.テキスト010.Left = 0.876378F;
            this.テキスト010.Name = "テキスト010";
            this.テキスト010.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: left; ddo-char-set: 128";
            this.テキスト010.Tag = "";
            this.テキスト010.Text = "ITEM10";
            this.テキスト010.Top = 0.06929135F;
            this.テキスト010.Width = 1.95748F;
            // 
            // テキスト011
            // 
            this.テキスト011.DataField = "ITEM11";
            this.テキスト011.Height = 0.15625F;
            this.テキスト011.Left = 3.063008F;
            this.テキスト011.Name = "テキスト011";
            this.テキスト011.OutputFormat = resources.GetString("テキスト011.OutputFormat");
            this.テキスト011.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト011.Tag = "";
            this.テキスト011.Text = "ITEM11";
            this.テキスト011.Top = 0.05905512F;
            this.テキスト011.Width = 1.18125F;
            // 
            // テキスト012
            // 
            this.テキスト012.DataField = "ITEM12";
            this.テキスト012.Height = 0.15625F;
            this.テキスト012.Left = 4.552362F;
            this.テキスト012.Name = "テキスト012";
            this.テキスト012.OutputFormat = resources.GetString("テキスト012.OutputFormat");
            this.テキスト012.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト012.Tag = "";
            this.テキスト012.Text = "ITEM12";
            this.テキスト012.Top = 0.05905512F;
            this.テキスト012.Width = 1.18125F;
            // 
            // テキスト013
            // 
            this.テキスト013.DataField = "ITEM13";
            this.テキスト013.Height = 0.15625F;
            this.テキスト013.Left = 6.083481F;
            this.テキスト013.Name = "テキスト013";
            this.テキスト013.OutputFormat = resources.GetString("テキスト013.OutputFormat");
            this.テキスト013.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト013.Tag = "";
            this.テキスト013.Text = "ITEM13";
            this.テキスト013.Top = 0.05905512F;
            this.テキスト013.Width = 1.18125F;
            // 
            // テキスト014
            // 
            this.テキスト014.DataField = "ITEM14";
            this.テキスト014.Height = 0.15625F;
            this.テキスト014.Left = 7.510253F;
            this.テキスト014.Name = "テキスト014";
            this.テキスト014.OutputFormat = resources.GetString("テキスト014.OutputFormat");
            this.テキスト014.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト014.Tag = "";
            this.テキスト014.Text = "ITEM14";
            this.テキスト014.Top = 0.05905512F;
            this.テキスト014.Width = 1.18125F;
            // 
            // テキスト015
            // 
            this.テキスト015.DataField = "ITEM15";
            this.テキスト015.Height = 0.15625F;
            this.テキスト015.Left = 8.998441F;
            this.テキスト015.Name = "テキスト015";
            this.テキスト015.OutputFormat = resources.GetString("テキスト015.OutputFormat");
            this.テキスト015.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト015.Tag = "";
            this.テキスト015.Text = "ITEM15";
            this.テキスト015.Top = 0.05905512F;
            this.テキスト015.Width = 1.18125F;
            // 
            // テキスト016
            // 
            this.テキスト016.DataField = "ITEM16";
            this.テキスト016.Height = 0.15625F;
            this.テキスト016.Left = 10.42539F;
            this.テキスト016.Name = "テキスト016";
            this.テキスト016.OutputFormat = resources.GetString("テキスト016.OutputFormat");
            this.テキスト016.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト016.Tag = "";
            this.テキスト016.Text = "ITEM16";
            this.テキスト016.Top = 0.05892389F;
            this.テキスト016.Width = 1.18125F;
            // 
            // テキスト017
            // 
            this.テキスト017.DataField = "ITEM17";
            this.テキスト017.Height = 0.15625F;
            this.テキスト017.Left = 2.938008F;
            this.テキスト017.Name = "テキスト017";
            this.テキスト017.OutputFormat = resources.GetString("テキスト017.OutputFormat");
            this.テキスト017.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト017.Tag = "";
            this.テキスト017.Text = "ITEM17";
            this.テキスト017.Top = 0.2257218F;
            this.テキスト017.Width = 1.18125F;
            // 
            // テキスト018
            // 
            this.テキスト018.DataField = "ITEM18";
            this.テキスト018.Height = 0.15625F;
            this.テキスト018.Left = 4.443701F;
            this.テキスト018.Name = "テキスト018";
            this.テキスト018.OutputFormat = resources.GetString("テキスト018.OutputFormat");
            this.テキスト018.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト018.Tag = "";
            this.テキスト018.Text = "ITEM18";
            this.テキスト018.Top = 0.2255906F;
            this.テキスト018.Width = 1.18125F;
            // 
            // テキスト019
            // 
            this.テキスト019.DataField = "ITEM19";
            this.テキスト019.Height = 0.15625F;
            this.テキスト019.Left = 5.958481F;
            this.テキスト019.Name = "テキスト019";
            this.テキスト019.OutputFormat = resources.GetString("テキスト019.OutputFormat");
            this.テキスト019.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト019.Tag = "";
            this.テキスト019.Text = "ITEM19";
            this.テキスト019.Top = 0.2257218F;
            this.テキスト019.Width = 1.18125F;
            // 
            // テキスト020
            // 
            this.テキスト020.DataField = "ITEM20";
            this.テキスト020.Height = 0.15625F;
            this.テキスト020.Left = 7.385253F;
            this.テキスト020.Name = "テキスト020";
            this.テキスト020.OutputFormat = resources.GetString("テキスト020.OutputFormat");
            this.テキスト020.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト020.Tag = "";
            this.テキスト020.Text = "ITEM20";
            this.テキスト020.Top = 0.2257218F;
            this.テキスト020.Width = 1.18125F;
            // 
            // テキスト021
            // 
            this.テキスト021.DataField = "ITEM21";
            this.テキスト021.Height = 0.15625F;
            this.テキスト021.Left = 8.873441F;
            this.テキスト021.Name = "テキスト021";
            this.テキスト021.OutputFormat = resources.GetString("テキスト021.OutputFormat");
            this.テキスト021.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト021.Tag = "";
            this.テキスト021.Text = "ITEM21";
            this.テキスト021.Top = 0.2257218F;
            this.テキスト021.Width = 1.18125F;
            // 
            // テキスト022
            // 
            this.テキスト022.DataField = "ITEM22";
            this.テキスト022.Height = 0.15625F;
            this.テキスト022.Left = 10.30039F;
            this.テキスト022.Name = "テキスト022";
            this.テキスト022.OutputFormat = resources.GetString("テキスト022.OutputFormat");
            this.テキスト022.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト022.Tag = "";
            this.テキスト022.Text = "ITEM22";
            this.テキスト022.Top = 0.2255906F;
            this.テキスト022.Width = 1.18125F;
            // 
            // 数量合計
            // 
            this.数量合計.Height = 0.15625F;
            this.数量合計.Left = 11.86732F;
            this.数量合計.Name = "数量合計";
            this.数量合計.OutputFormat = resources.GetString("数量合計.OutputFormat");
            this.数量合計.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.数量合計.Tag = "";
            this.数量合計.Text = null;
            this.数量合計.Top = 0.05895671F;
            this.数量合計.Width = 1.181376F;
            // 
            // 金額合計
            // 
            this.金額合計.Height = 0.15625F;
            this.金額合計.Left = 11.74245F;
            this.金額合計.Name = "金額合計";
            this.金額合計.OutputFormat = resources.GetString("金額合計.OutputFormat");
            this.金額合計.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.金額合計.Tag = "";
            this.金額合計.Text = "=CCur([ITEM17])+CCur([ITEM18])+CCur([ITEM19])+CCur([ITEM20])+CCur([ITEM21])+CCur(" +
    "[ITEM22])";
            this.金額合計.Top = 0.2256234F;
            this.金額合計.Width = 1.18125F;
            // 
            // 直線89
            // 
            this.直線89.Height = 0F;
            this.直線89.Left = 0.000180468F;
            this.直線89.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.直線89.LineWeight = 0F;
            this.直線89.Name = "直線89";
            this.直線89.Tag = "";
            this.直線89.Top = 0.3923885F;
            this.直線89.Width = 13.38583F;
            this.直線89.X1 = 0.000180468F;
            this.直線89.X2 = 13.38601F;
            this.直線89.Y1 = 0.3923885F;
            this.直線89.Y2 = 0.3923885F;
            // 
            // pageFooter
            // 
            this.pageFooter.Name = "pageFooter";
            // 
            // reportHeader1
            // 
            this.reportHeader1.Name = "reportHeader1";
            // 
            // reportFooter1
            // 
            this.reportFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.テキスト143,
            this.テキスト144,
            this.テキスト145,
            this.テキスト146,
            this.テキスト147,
            this.テキスト148,
            this.テキスト149,
            this.テキスト150,
            this.テキスト151,
            this.テキスト152,
            this.テキスト153,
            this.テキスト154,
            this.数量総合計,
            this.金額総合計,
            this.直線157,
            this.ラベル158});
            this.reportFooter1.Height = 0.3937008F;
            this.reportFooter1.Name = "reportFooter1";
            this.reportFooter1.Format += new System.EventHandler(this.reportFooter1_Format);
            // 
            // テキスト143
            // 
            this.テキスト143.DataField = "ITEM11";
            this.テキスト143.Height = 0.15625F;
            this.テキスト143.Left = 3.062992F;
            this.テキスト143.Name = "テキスト143";
            this.テキスト143.OutputFormat = resources.GetString("テキスト143.OutputFormat");
            this.テキスト143.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: right; ddo-char-set: 1";
            this.テキスト143.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト143.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト143.Tag = "";
            this.テキスト143.Text = "ITEM11";
            this.テキスト143.Top = 0.05905512F;
            this.テキスト143.Width = 1.18125F;
            // 
            // テキスト144
            // 
            this.テキスト144.DataField = "ITEM12";
            this.テキスト144.Height = 0.15625F;
            this.テキスト144.Left = 4.552362F;
            this.テキスト144.Name = "テキスト144";
            this.テキスト144.OutputFormat = resources.GetString("テキスト144.OutputFormat");
            this.テキスト144.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: right; ddo-char-set: 1";
            this.テキスト144.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト144.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト144.Tag = "";
            this.テキスト144.Text = "ITEM12";
            this.テキスト144.Top = 0.05905512F;
            this.テキスト144.Width = 1.18125F;
            // 
            // テキスト145
            // 
            this.テキスト145.DataField = "ITEM13";
            this.テキスト145.Height = 0.15625F;
            this.テキスト145.Left = 6.083465F;
            this.テキスト145.Name = "テキスト145";
            this.テキスト145.OutputFormat = resources.GetString("テキスト145.OutputFormat");
            this.テキスト145.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: right; ddo-char-set: 1";
            this.テキスト145.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト145.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト145.Tag = "";
            this.テキスト145.Text = "ITEM13";
            this.テキスト145.Top = 0.05905512F;
            this.テキスト145.Width = 1.18125F;
            // 
            // テキスト146
            // 
            this.テキスト146.DataField = "ITEM14";
            this.テキスト146.Height = 0.15625F;
            this.テキスト146.Left = 7.510237F;
            this.テキスト146.Name = "テキスト146";
            this.テキスト146.OutputFormat = resources.GetString("テキスト146.OutputFormat");
            this.テキスト146.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: right; ddo-char-set: 1";
            this.テキスト146.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト146.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト146.Tag = "";
            this.テキスト146.Text = "ITEM14";
            this.テキスト146.Top = 0.05905512F;
            this.テキスト146.Width = 1.18125F;
            // 
            // テキスト147
            // 
            this.テキスト147.DataField = "ITEM15";
            this.テキスト147.Height = 0.15625F;
            this.テキスト147.Left = 8.998425F;
            this.テキスト147.Name = "テキスト147";
            this.テキスト147.OutputFormat = resources.GetString("テキスト147.OutputFormat");
            this.テキスト147.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: right; ddo-char-set: 1";
            this.テキスト147.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト147.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト147.Tag = "";
            this.テキスト147.Text = "ITEM15";
            this.テキスト147.Top = 0.05905512F;
            this.テキスト147.Width = 1.18125F;
            // 
            // テキスト148
            // 
            this.テキスト148.DataField = "ITEM16";
            this.テキスト148.Height = 0.15625F;
            this.テキスト148.Left = 10.42537F;
            this.テキスト148.Name = "テキスト148";
            this.テキスト148.OutputFormat = resources.GetString("テキスト148.OutputFormat");
            this.テキスト148.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: right; ddo-char-set: 1";
            this.テキスト148.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト148.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト148.Tag = "";
            this.テキスト148.Text = "ITEM16";
            this.テキスト148.Top = 0.05892389F;
            this.テキスト148.Width = 1.18125F;
            // 
            // テキスト149
            // 
            this.テキスト149.DataField = "ITEM17";
            this.テキスト149.Height = 0.15625F;
            this.テキスト149.Left = 2.938189F;
            this.テキスト149.Name = "テキスト149";
            this.テキスト149.OutputFormat = resources.GetString("テキスト149.OutputFormat");
            this.テキスト149.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: right; ddo-char-set: 1";
            this.テキスト149.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト149.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト149.Tag = "";
            this.テキスト149.Text = "ITEM17";
            this.テキスト149.Top = 0.2153544F;
            this.テキスト149.Width = 1.18125F;
            // 
            // テキスト150
            // 
            this.テキスト150.DataField = "ITEM18";
            this.テキスト150.Height = 0.15625F;
            this.テキスト150.Left = 4.443701F;
            this.テキスト150.Name = "テキスト150";
            this.テキスト150.OutputFormat = resources.GetString("テキスト150.OutputFormat");
            this.テキスト150.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: right; ddo-char-set: 1";
            this.テキスト150.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト150.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト150.Tag = "";
            this.テキスト150.Text = "ITEM18";
            this.テキスト150.Top = 0.2153544F;
            this.テキスト150.Width = 1.18125F;
            // 
            // テキスト151
            // 
            this.テキスト151.DataField = "ITEM19";
            this.テキスト151.Height = 0.15625F;
            this.テキスト151.Left = 5.958465F;
            this.テキスト151.Name = "テキスト151";
            this.テキスト151.OutputFormat = resources.GetString("テキスト151.OutputFormat");
            this.テキスト151.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: right; ddo-char-set: 1";
            this.テキスト151.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト151.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト151.Tag = "";
            this.テキスト151.Text = "ITEM19";
            this.テキスト151.Top = 0.2257218F;
            this.テキスト151.Width = 1.18125F;
            // 
            // テキスト152
            // 
            this.テキスト152.DataField = "ITEM20";
            this.テキスト152.Height = 0.15625F;
            this.テキスト152.Left = 7.385237F;
            this.テキスト152.Name = "テキスト152";
            this.テキスト152.OutputFormat = resources.GetString("テキスト152.OutputFormat");
            this.テキスト152.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: right; ddo-char-set: 1";
            this.テキスト152.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト152.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト152.Tag = "";
            this.テキスト152.Text = "ITEM20";
            this.テキスト152.Top = 0.2257218F;
            this.テキスト152.Width = 1.18125F;
            // 
            // テキスト153
            // 
            this.テキスト153.DataField = "ITEM21";
            this.テキスト153.Height = 0.15625F;
            this.テキスト153.Left = 8.873425F;
            this.テキスト153.Name = "テキスト153";
            this.テキスト153.OutputFormat = resources.GetString("テキスト153.OutputFormat");
            this.テキスト153.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: right; ddo-char-set: 1";
            this.テキスト153.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト153.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト153.Tag = "";
            this.テキスト153.Text = "ITEM21";
            this.テキスト153.Top = 0.2257218F;
            this.テキスト153.Width = 1.18125F;
            // 
            // テキスト154
            // 
            this.テキスト154.DataField = "ITEM22";
            this.テキスト154.Height = 0.15625F;
            this.テキスト154.Left = 10.30037F;
            this.テキスト154.Name = "テキスト154";
            this.テキスト154.OutputFormat = resources.GetString("テキスト154.OutputFormat");
            this.テキスト154.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: right; ddo-char-set: 1";
            this.テキスト154.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト154.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト154.Tag = "";
            this.テキスト154.Text = "ITEM22";
            this.テキスト154.Top = 0.2255906F;
            this.テキスト154.Width = 1.18125F;
            // 
            // 数量総合計
            // 
            this.数量総合計.Height = 0.15625F;
            this.数量総合計.Left = 11.74252F;
            this.数量総合計.Name = "数量総合計";
            this.数量総合計.OutputFormat = resources.GetString("数量総合計.OutputFormat");
            this.数量総合計.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: right; ddo-char-set: 1";
            this.数量総合計.Tag = "";
            this.数量総合計.Text = null;
            this.数量総合計.Top = 0.05895671F;
            this.数量総合計.Width = 1.30616F;
            // 
            // 金額総合計
            // 
            this.金額総合計.Height = 0.15625F;
            this.金額総合計.Left = 11.74243F;
            this.金額総合計.Name = "金額総合計";
            this.金額総合計.OutputFormat = resources.GetString("金額総合計.OutputFormat");
            this.金額総合計.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: right; ddo-char-set: 1";
            this.金額総合計.Tag = "";
            this.金額総合計.Text = null;
            this.金額総合計.Top = 0.2256234F;
            this.金額総合計.Width = 1.18125F;
            // 
            // 直線157
            // 
            this.直線157.Height = 0F;
            this.直線157.Left = 0.0001642704F;
            this.直線157.LineWeight = 0F;
            this.直線157.Name = "直線157";
            this.直線157.Tag = "";
            this.直線157.Top = 0.3923884F;
            this.直線157.Width = 13.38583F;
            this.直線157.X1 = 0.0001642704F;
            this.直線157.X2 = 13.38599F;
            this.直線157.Y1 = 0.3923884F;
            this.直線157.Y2 = 0.3923884F;
            // 
            // ラベル158
            // 
            this.ラベル158.Height = 0.15625F;
            this.ラベル158.HyperLink = null;
            this.ラベル158.Left = 0.6897638F;
            this.ラベル158.Name = "ラベル158";
            this.ラベル158.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-weight: bold; text-align:" +
    " left; ddo-char-set: 1";
            this.ラベル158.Tag = "";
            this.ラベル158.Text = "総計";
            this.ラベル158.Top = 0.1007218F;
            this.ラベル158.Width = 0.7062992F;
            // 
            // HANR4021R
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.3937008F;
            this.PageSettings.Margins.Left = 0.3937008F;
            this.PageSettings.Margins.Right = 0.3937008F;
            this.PageSettings.Margins.Top = 0.3937008F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
            this.PageSettings.PaperHeight = 13.89764F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.B4;
            this.PageSettings.PaperWidth = 9.84252F;
            this.PrintWidth = 13.38583F;
            this.Sections.Add(this.reportHeader1);
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.pageFooter);
            this.Sections.Add(this.reportFooter1);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.テキスト001)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキストData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキストPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル004S)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル004K)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル54)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル61)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル62)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル64)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル65)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル124)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト003S)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト003K)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト004S)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト004K)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト005S)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト005K)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト006S)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト006K)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト007S)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト007K)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト008S)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト008K)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト139)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト140)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル159)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト009)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト010)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト011)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト012)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト013)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト014)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト015)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト016)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト017)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト018)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト019)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト020)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト021)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト022)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.数量合計)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.金額合計)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト143)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト144)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト145)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト146)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト147)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト148)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト149)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト150)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト151)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト152)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト153)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト154)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.数量総合計)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.金額総合計)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル158)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト001;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキストData;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキストPage;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル1;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル3;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線31;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル33;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル34;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル004S;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル004K;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル41;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル42;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル45;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル46;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル49;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル50;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル53;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル54;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル61;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル62;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル64;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル65;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト2;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル124;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト003S;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト003K;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト004S;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト004K;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト005S;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト005K;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト006S;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト006K;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト007S;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト007K;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト008S;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト008K;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト139;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト140;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル159;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線160;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト009;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト010;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト011;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト012;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト013;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト014;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト015;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト016;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト017;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト018;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト019;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト020;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト021;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト022;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 数量合計;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 金額合計;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線89;
        private GrapeCity.ActiveReports.SectionReportModel.ReportHeader reportHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.ReportFooter reportFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト143;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト144;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト145;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト146;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト147;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト148;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト149;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト150;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト151;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト152;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト153;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト154;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 数量総合計;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 金額総合計;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線157;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル158;
    }
}
