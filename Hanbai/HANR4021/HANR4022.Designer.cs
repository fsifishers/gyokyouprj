﻿namespace jp.co.fsi.han.hanr4021
{
    partial class HANR4022
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbxAll = new System.Windows.Forms.GroupBox();
            this.btnSeiikaReleaseAll = new System.Windows.Forms.Button();
            this.btnSeiikaSelectAll = new System.Windows.Forms.Button();
            this.btnGyoshuReleaseAll = new System.Windows.Forms.Button();
            this.btnGyoshuSelectAll = new System.Windows.Forms.Button();
            this.btnLMove = new System.Windows.Forms.Button();
            this.btnRMove = new System.Windows.Forms.Button();
            this.gbxSeiikaCd = new System.Windows.Forms.GroupBox();
            this.lbxSeiikaCd = new System.Windows.Forms.ListBox();
            this.gbxGyoshuIchiran = new System.Windows.Forms.GroupBox();
            this.lbxGyoshuIchiran = new System.Windows.Forms.ListBox();
            this.pnlDebug.SuspendLayout();
            this.gbxAll.SuspendLayout();
            this.gbxSeiikaCd.SuspendLayout();
            this.gbxGyoshuIchiran.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Text = "";
            // 
            // btnF12
            // 
            this.btnF12.Text = "セイイカ";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Size = new System.Drawing.Size(847, 100);
            // 
            // gbxAll
            // 
            this.gbxAll.Controls.Add(this.btnSeiikaReleaseAll);
            this.gbxAll.Controls.Add(this.btnSeiikaSelectAll);
            this.gbxAll.Controls.Add(this.btnGyoshuReleaseAll);
            this.gbxAll.Controls.Add(this.btnGyoshuSelectAll);
            this.gbxAll.Controls.Add(this.btnLMove);
            this.gbxAll.Controls.Add(this.btnRMove);
            this.gbxAll.Controls.Add(this.gbxSeiikaCd);
            this.gbxAll.Controls.Add(this.gbxGyoshuIchiran);
            this.gbxAll.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxAll.Location = new System.Drawing.Point(12, 39);
            this.gbxAll.Name = "gbxAll";
            this.gbxAll.Size = new System.Drawing.Size(502, 387);
            this.gbxAll.TabIndex = 903;
            this.gbxAll.TabStop = false;
            // 
            // btnSeiikaReleaseAll
            // 
            this.btnSeiikaReleaseAll.Location = new System.Drawing.Point(385, 354);
            this.btnSeiikaReleaseAll.Name = "btnSeiikaReleaseAll";
            this.btnSeiikaReleaseAll.Size = new System.Drawing.Size(85, 27);
            this.btnSeiikaReleaseAll.TabIndex = 907;
            this.btnSeiikaReleaseAll.Text = "F7 全解除";
            this.btnSeiikaReleaseAll.UseVisualStyleBackColor = true;
            this.btnSeiikaReleaseAll.Click += new System.EventHandler(this.btnSF7);
            // 
            // btnSeiikaSelectAll
            // 
            this.btnSeiikaSelectAll.Location = new System.Drawing.Point(294, 354);
            this.btnSeiikaSelectAll.Name = "btnSeiikaSelectAll";
            this.btnSeiikaSelectAll.Size = new System.Drawing.Size(85, 27);
            this.btnSeiikaSelectAll.TabIndex = 906;
            this.btnSeiikaSelectAll.Text = "F6 全選択";
            this.btnSeiikaSelectAll.UseVisualStyleBackColor = true;
            this.btnSeiikaSelectAll.Click += new System.EventHandler(this.btnSF6);
            // 
            // btnGyoshuReleaseAll
            // 
            this.btnGyoshuReleaseAll.Location = new System.Drawing.Point(121, 354);
            this.btnGyoshuReleaseAll.Name = "btnGyoshuReleaseAll";
            this.btnGyoshuReleaseAll.Size = new System.Drawing.Size(85, 27);
            this.btnGyoshuReleaseAll.TabIndex = 905;
            this.btnGyoshuReleaseAll.Text = "F5 全解除";
            this.btnGyoshuReleaseAll.UseVisualStyleBackColor = true;
            this.btnGyoshuReleaseAll.Click += new System.EventHandler(this.btnGF5);
            // 
            // btnGyoshuSelectAll
            // 
            this.btnGyoshuSelectAll.Location = new System.Drawing.Point(30, 354);
            this.btnGyoshuSelectAll.Name = "btnGyoshuSelectAll";
            this.btnGyoshuSelectAll.Size = new System.Drawing.Size(85, 27);
            this.btnGyoshuSelectAll.TabIndex = 904;
            this.btnGyoshuSelectAll.Text = "F4 全選択";
            this.btnGyoshuSelectAll.UseVisualStyleBackColor = true;
            this.btnGyoshuSelectAll.Click += new System.EventHandler(this.btnGF4);
            // 
            // btnLMove
            // 
            this.btnLMove.BackColor = System.Drawing.Color.Silver;
            this.btnLMove.Font = new System.Drawing.Font("ＭＳ ゴシック", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnLMove.Location = new System.Drawing.Point(228, 189);
            this.btnLMove.Name = "btnLMove";
            this.btnLMove.Size = new System.Drawing.Size(44, 39);
            this.btnLMove.TabIndex = 3;
            this.btnLMove.Text = "＜";
            this.btnLMove.UseVisualStyleBackColor = false;
            this.btnLMove.Click += new System.EventHandler(this.btnLMove_Click);
            // 
            // btnRMove
            // 
            this.btnRMove.BackColor = System.Drawing.Color.Silver;
            this.btnRMove.Font = new System.Drawing.Font("ＭＳ ゴシック", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnRMove.Location = new System.Drawing.Point(228, 135);
            this.btnRMove.Name = "btnRMove";
            this.btnRMove.Size = new System.Drawing.Size(44, 39);
            this.btnRMove.TabIndex = 2;
            this.btnRMove.Text = "＞";
            this.btnRMove.UseVisualStyleBackColor = false;
            this.btnRMove.Click += new System.EventHandler(this.btnRMove_Click);
            // 
            // gbxSeiikaCd
            // 
            this.gbxSeiikaCd.Controls.Add(this.lbxSeiikaCd);
            this.gbxSeiikaCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxSeiikaCd.ForeColor = System.Drawing.SystemColors.ControlText;
            this.gbxSeiikaCd.Location = new System.Drawing.Point(280, 18);
            this.gbxSeiikaCd.Name = "gbxSeiikaCd";
            this.gbxSeiikaCd.Size = new System.Drawing.Size(204, 330);
            this.gbxSeiikaCd.TabIndex = 1;
            this.gbxSeiikaCd.TabStop = false;
            this.gbxSeiikaCd.Text = "セイイカ";
            // 
            // lbxSeiikaCd
            // 
            this.lbxSeiikaCd.FormattingEnabled = true;
            this.lbxSeiikaCd.Location = new System.Drawing.Point(14, 22);
            this.lbxSeiikaCd.Name = "lbxSeiikaCd";
            this.lbxSeiikaCd.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.lbxSeiikaCd.Size = new System.Drawing.Size(179, 290);
            this.lbxSeiikaCd.TabIndex = 1;
            // 
            // gbxGyoshuIchiran
            // 
            this.gbxGyoshuIchiran.Controls.Add(this.lbxGyoshuIchiran);
            this.gbxGyoshuIchiran.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxGyoshuIchiran.ForeColor = System.Drawing.SystemColors.ControlText;
            this.gbxGyoshuIchiran.Location = new System.Drawing.Point(17, 18);
            this.gbxGyoshuIchiran.Name = "gbxGyoshuIchiran";
            this.gbxGyoshuIchiran.Size = new System.Drawing.Size(204, 330);
            this.gbxGyoshuIchiran.TabIndex = 0;
            this.gbxGyoshuIchiran.TabStop = false;
            this.gbxGyoshuIchiran.Text = "魚種一覧";
            // 
            // lbxGyoshuIchiran
            // 
            this.lbxGyoshuIchiran.FormattingEnabled = true;
            this.lbxGyoshuIchiran.Location = new System.Drawing.Point(12, 22);
            this.lbxGyoshuIchiran.Name = "lbxGyoshuIchiran";
            this.lbxGyoshuIchiran.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.lbxGyoshuIchiran.Size = new System.Drawing.Size(179, 290);
            this.lbxGyoshuIchiran.TabIndex = 0;
            // 
            // HANR4022
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 638);
            this.Controls.Add(this.gbxAll);
            this.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "HANR4022";
            this.Text = "";
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.gbxAll, 0);
            this.pnlDebug.ResumeLayout(false);
            this.gbxAll.ResumeLayout(false);
            this.gbxSeiikaCd.ResumeLayout(false);
            this.gbxGyoshuIchiran.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbxAll;
        private System.Windows.Forms.Button btnLMove;
        private System.Windows.Forms.Button btnRMove;
        private System.Windows.Forms.GroupBox gbxSeiikaCd;
        private System.Windows.Forms.ListBox lbxSeiikaCd;
        private System.Windows.Forms.GroupBox gbxGyoshuIchiran;
        private System.Windows.Forms.ListBox lbxGyoshuIchiran;
        private System.Windows.Forms.Button btnGyoshuReleaseAll;
        private System.Windows.Forms.Button btnGyoshuSelectAll;
        private System.Windows.Forms.Button btnSeiikaReleaseAll;
        private System.Windows.Forms.Button btnSeiikaSelectAll;


    }
}