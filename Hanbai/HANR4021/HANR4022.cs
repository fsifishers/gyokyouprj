﻿using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Reflection;
using System.Windows.Forms;
using System.Runtime.InteropServices;

using jp.co.fsi.common.access;
using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.han.hanr4021
{
    /// <summary>
    /// セイイカ設定(HANR4022)
    /// </summary>
    public partial class HANR4022 : BasePgForm
    {
        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HANR4022()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region メソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // サイズを縮める
            this.Size = new Size(550, 590);
            // Escapeのみ表示
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF1.Visible = false;
            this.btnF2.Visible = false;
            this.btnF3.Visible = false;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF6.Visible = false;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;

            // リストボックスの初期表示
            SearchData();
        }

        /// <summary>
        /// Escキー押下時処理
        /// </summary>
        public override void PressEsc()
        {
            // 設定保存処理
            ShuturyokuData();

            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            GyoshuSelectAll();
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            GyoshuReleaseAll();
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        public override void PressF6()
        {
            SeiikaSelectAll();
        }

        /// <summary>
        /// F7キー押下時処理
        /// </summary>
        public override void PressF7()
        {
            SeiikaReleaseAll();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 選択した項目の移動処理(左→右)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRMove_Click(object sender, System.EventArgs e)
        {
            int[] selectNum = new int[lbxGyoshuIchiran.SelectedIndices.Count];

            for (int i = 0; i < lbxGyoshuIchiran.SelectedIndices.Count; i++)
            {
                selectNum[i] = lbxGyoshuIchiran.SelectedIndices[i];
            }

            // 選択した項目の追加
            for (int i = 0; i < selectNum.Length; i++)
            {
                this.lbxSeiikaCd.Items.Add(this.lbxGyoshuIchiran.Items[selectNum[i]]);
            }

            int l = 1;
            // 選択した項目の削除
            for (int i = 0; i < selectNum.Length; i++)
            {
                if (i == 0)
                {
                    this.lbxGyoshuIchiran.Items.Remove(this.lbxGyoshuIchiran.Items[selectNum[i]]);
                }
                else
                {
                    this.lbxGyoshuIchiran.Items.Remove(this.lbxGyoshuIchiran.Items[selectNum[i] - l]);
                    l++;
                }
            }
        }

        /// <summary>
        /// 選択した項目の移動処理(右→左)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnLMove_Click(object sender, System.EventArgs e)
        {
            {
                int[] selectNum = new int[lbxSeiikaCd.SelectedIndices.Count];

                for (int i = 0; i < lbxSeiikaCd.SelectedIndices.Count; i++)
                {
                    selectNum[i] = lbxSeiikaCd.SelectedIndices[i];
                }

                // 選択した項目の追加
                for (int i = 0; i < selectNum.Length; i++)
                {
                    this.lbxGyoshuIchiran.Items.Add(this.lbxSeiikaCd.Items[selectNum[i]]);
                }

                int l = 1;
                // 選択した項目の削除
                for (int i = 0; i < selectNum.Length; i++)
                {
                    if (i == 0)
                    {
                        this.lbxSeiikaCd.Items.Remove(this.lbxSeiikaCd.Items[selectNum[i]]);
                    }
                    else
                    {
                        this.lbxSeiikaCd.Items.Remove(this.lbxSeiikaCd.Items[selectNum[i] - l]);
                        l++;
                    }
                }
            }
        }

        /// <summary>
        /// F4全選択ボタン押下時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnGF4(object sender, EventArgs e)
        {
            GyoshuSelectAll();
        }

        /// <summary>
        /// F5全解除ボタン押下時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnGF5(object sender, EventArgs e)
        {
            GyoshuReleaseAll();
        }

        /// <summary>
        /// F6全選択ボタン押下時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSF6(object sender, EventArgs e)
        {
            SeiikaSelectAll();
        }

        /// <summary>
        /// F7全解除ボタン押下時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSF7(object sender, EventArgs e)
        {
            SeiikaReleaseAll();
        }
        #endregion

        #region privateメソッド
        [DllImport("User32.dll", EntryPoint = "SendMessage")]
        private static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        private const int LB_SETSEL = 0x185;

        /// <summary>
        /// 魚種一覧データを全選択する
        /// </summary>
        private void GyoshuSelectAll()
        {
            SendMessage(lbxGyoshuIchiran.Handle, LB_SETSEL, 1, -1);
            lbxGyoshuIchiran.SetSelected(0, true);
        }

        /// <summary>
        /// 魚種一覧データを全解除する
        /// </summary>
        private void GyoshuReleaseAll()
        {
            lbxGyoshuIchiran.ClearSelected();
        }

        /// <summary>
        /// セイイカデータを全選択する
        /// </summary>
        private void SeiikaSelectAll()
        {
            SendMessage(lbxSeiikaCd.Handle, LB_SETSEL, 1, -1);
            lbxSeiikaCd.SetSelected(0, true);
        }

        /// <summary>
        /// セイイカデータを全解除する
        /// </summary>
        private void SeiikaReleaseAll()
        {
            lbxSeiikaCd.ClearSelected();
        }

        /// <summary>
        /// データを検索する
        /// </summary>
        private void SearchData()
        {
            // 魚種一覧に表示するデータを取得
            StringBuilder Sql = new StringBuilder();
            DbParamCollection dpc = new DbParamCollection();

            Sql.Append(" SELECT");
            Sql.Append(" A.SHOHIN_CD AS SHOHIN_CD,");
            Sql.Append(" A.SHOHIN_NM AS SHOHIN_NM");
            Sql.Append(" FROM");
            Sql.Append(" TB_HN_SHOHIN AS A");
            Sql.Append(" WHERE");
            Sql.Append(" A.KAISHA_CD = @KAISHA_CD AND");
            Sql.Append(" A.SHOHIN_CD NOT IN");
            Sql.Append(" (SELECT B.GYOSHU_CD FROM TB_HN_GYOSHU_SETTEI AS B WHERE B.KAISHA_CD = A.KAISHA_CD AND 2 = B.KUBUN ) AND");
            Sql.Append(" A.BARCODE1 = 999");
            Sql.Append(" ORDER BY");
            Sql.Append(" A.KAISHA_CD, A.SHOHIN_CD");
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);

            DataTable dtGyosyuIchiran = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            // 魚種一覧にセットする
            foreach (DataRow dr in dtGyosyuIchiran.Rows)
            {
                this.lbxGyoshuIchiran.Items.Add(Util.ToString(dr["SHOHIN_CD"]) + " " + Util.ToString(dr["SHOHIN_NM"]));
            }

            // セイイカに表示するデータを取得
            Sql = new StringBuilder();
            dpc = new DbParamCollection();

            Sql.Append(" SELECT");
            Sql.Append(" A.GYOSHU_CD AS SHOHIN_CD,");
            Sql.Append(" B.SHOHIN_NM AS SHOHIN_NM");
            Sql.Append(" FROM");
            Sql.Append(" TB_HN_GYOSHU_SETTEI AS A");
            Sql.Append(" LEFT OUTER JOIN");
            Sql.Append(" TB_HN_SHOHIN AS B");
            Sql.Append(" ON A.KAISHA_CD = B.KAISHA_CD AND A.GYOSHU_CD = B.SHOHIN_CD");
            Sql.Append(" WHERE");
            Sql.Append(" A.KAISHA_CD = @KAISHA_CD AND  A.KUBUN = 2");
            Sql.Append(" ORDER BY");
            Sql.Append(" A.GYOSHU_CD");
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);

            DataTable dtSeiika = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            // 出力する魚種にセットする
            foreach (DataRow dr in dtSeiika.Rows)
            {
                this.lbxSeiikaCd.Items.Add(Util.ToString(dr["SHOHIN_CD"]) + " " + Util.ToString(dr["SHOHIN_NM"]));
            }
        }

        /// <summary>
        /// 設定を保存する
        /// </summary>
        private void ShuturyokuData()
        {
            try
            {
                this.Dba.BeginTransaction();

                // 魚種設定削除処理
                StringBuilder Sql = new StringBuilder();
                DbParamCollection dpc = new DbParamCollection();
                Sql.Append(" DELETE");
                Sql.Append(" FROM");
                Sql.Append(" TB_HN_GYOSHU_SETTEI");
                Sql.Append(" WHERE");
                Sql.Append(" KAISHA_CD = @KAISHA_CD AND");
                Sql.Append(" KUBUN = 2");
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                // 魚種設定登録処理
                for (int i = 0; i < this.lbxSeiikaCd.Items.Count; i++)
                {
                    // 魚種CDの取得
                    string items = Util.ToString(this.lbxSeiikaCd.Items[i]);
                    string[] gyoshuCd = items.Split(' ');

                    Sql = new StringBuilder();
                    dpc = new DbParamCollection();
                    Sql.Append(" INSERT");
                    Sql.Append(" INTO");
                    Sql.Append(" TB_HN_GYOSHU_SETTEI");
                    Sql.Append(" (");
                    Sql.Append(" KAISHA_CD,");
                    Sql.Append(" GYOSHU_CD,");
                    Sql.Append(" KUBUN,");
                    Sql.Append(" REGIST_DATE");
                    Sql.Append(" )");
                    Sql.Append(" VALUES");
                    Sql.Append(" (");
                    Sql.Append(" @KAISHA_CD,");
                    Sql.Append(" @GYOSHU_CD,");
                    Sql.Append(" @KUBUN,");
                    Sql.Append(" @REGIST_DATE");
                    Sql.Append(" )");
                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                    dpc.SetParam("@GYOSHU_CD", SqlDbType.Decimal, 4, gyoshuCd[0]);
                    dpc.SetParam("@KUBUN", SqlDbType.Decimal, 1, 2);
                    dpc.SetParam("@REGIST_DATE", SqlDbType.DateTime, 20, DateTime.Today);
                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                }

                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }
        }
        #endregion
    }
}
