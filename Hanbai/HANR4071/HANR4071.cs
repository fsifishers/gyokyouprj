﻿using System;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Text;
using System.Reflection;
using System.Windows.Forms;

using GrapeCity.ActiveReports;

using jp.co.fsi.common.access;
using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.han.hanr4071
{
    /// <summary>
    /// 船主別水揚日数一覧表(HANR4071)
    /// </summary>
    public partial class HANR4071 : BasePgForm
    {
        #region プロパティ
        /// <summary>
        /// 画面上最後となるフォーカスのEnterボタン押下時処理変数
        /// </summary>
        private bool _dtFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._dtFlg;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HANR4071()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region メソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            string[] jpDate = Util.ConvJpDate(DateTime.Now, this.Dba);
            // 年指定
            lblDateGengo.Text = jpDate[0];
            txtDateYear.Text = jpDate[2];
            txtDateMonth.Text = jpDate[3];

            // フォーカス設定
            this.txtDateYear.Focus();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 年始定、船主コードにフォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtDateYear":
                case "txtFunanushiCdFr":
                case "txtFunanushiCdTo":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;

            switch (this.ActiveCtlNm)
            {
                case "txtDateYear":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom("COMC9011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.com.comc9011.COMC9011");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblDateGengo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.lblDateGengo.Text = result[1];
                            }
                        }
                    }
                    break;

                case "txtFunanushiCdFr":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom("KOBC9021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.kob.kobc9021.KOBC9021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtFunanushiCdFr.Text = outData[0];
                                this.lblFunanushiCdFr.Text = outData[1];
                            }
                        }
                    }
                    break;

                case "txtFunanushiCdTo":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom("KOBC9021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.kob.kobc9021.KOBC9021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtFunanushiCdTo.Text = outData[0];
                                this.lblFunanushiCdTo.Text = outData[1];
                            }
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
            {
                // プレビュー処理
                DoPrint(true);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("印刷", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }

        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            // 設定画面の起動
            // MEMO:原則としてここで渡す帳票IDの設定はReport.csvに保持していることが前提ですが、
            // 保持していない場合は、設定画面での保存(F6)時に新規に設定が保持されます。
            PrintSettingForm psForm = new PrintSettingForm(new string[1] { "HANR4071R" });
            psForm.ShowDialog();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 年指定の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateYear_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidYear())
            {
                e.Cancel = true;
                this.txtDateYear.SelectAll();
            }
        }

        /// <summary>
        /// 月指定の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateMonth_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidMonth())
            {
                e.Cancel = true;
                this.txtDateMonth.SelectAll();
            }
        }

        /// <summary>
        /// 船主コード(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCodeFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidCodeFr())
            {
                e.Cancel = true;
                this.txtFunanushiCdFr.SelectAll();
            }
        }

        /// <summary>
        /// 船主コード(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCodeTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidCodeTo())
            {
                e.Cancel = true;
                this.txtFunanushiCdTo.SelectAll();
                // Enter処理を無効化
                this._dtFlg = false;
            }
            else
            {
                // Enter処理を有効化
                this._dtFlg = true;
            }
        }

        /// <summary>
        /// 船主コード(至)のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFunanushiCdTo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.Flg)
            {
                // Enter処理を無効化
                this._dtFlg = false;

                // 全項目を再度入力値チェック
                if (!ValidateAll())
                {
                    // エラーありの場合ここで処理終了
                    return;
                }

                if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
                {
                    // ﾌﾟﾚﾋﾞｭｰ処理
                    DoPrint(true);
                }
                else
                {
                    this.txtFunanushiCdTo.Focus();
                }
            }
        }

        #endregion

        #region privateメソッド
        /// <summary>
        /// 年の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidYear()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtDateYear.Text))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }

            // 空の場合、0年として処理
            if (ValChk.IsEmpty(this.txtDateYear.Text))
            {
                this.txtDateYear.Text = "0";
            }

            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDate(Util.FixJpDate(this.lblDateGengo.Text, this.txtDateYear.Text,
                "1", "1", this.Dba));

            return true;
        }

        /// <summary>
        /// 月の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidMonth()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtDateMonth.Text))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }

            // 空の場合、1月として処理
            if (ValChk.IsEmpty(this.txtDateMonth.Text))
            {
                this.txtDateMonth.Text = "1";
            }
            else
            {
                // 12を超える月が入力された場合、12月として処理
                if (Util.ToInt(this.txtDateMonth.Text) > 12)
                {
                    this.txtDateMonth.Text = "12";
                }
                // 1より小さい月が入力された場合、1月として処理
                else if (Util.ToInt(this.txtDateMonth.Text) < 1)
                {
                    this.txtDateMonth.Text = "1";
                }
            }

            return true;
        }

        /// <summary>
        /// 船主コード(自)の入力チェック
        /// </summary>
        private bool IsValidCodeFr()
        {
            if (ValChk.IsEmpty(this.txtFunanushiCdFr.Text))
            {
                this.lblFunanushiCdFr.Text = "先　頭";
            }
            else
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtFunanushiCdFr.Text))
            {
                Msg.Error("コード(自)は数値のみで入力してください。");
                return false;
            }
            else
            {
                // コードを元に名称を取得する
                this.lblFunanushiCdFr.Text = this.Dba.GetName(this.UInfo, "VI_HN_FUNANUSHI", this.txtFunanushiCdFr.Text);
            }

            return true;
        }

        /// <summary>
        /// 船主コード(至)の入力チェック
        /// </summary>
        private bool IsValidCodeTo()
        {
            if (ValChk.IsEmpty(this.txtFunanushiCdTo.Text))
            {
                this.lblFunanushiCdTo.Text = "最　後";
            }
            else
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtFunanushiCdTo.Text))
            {
                Msg.Error("コード(至)は数値のみで入力してください。");
                return false;
            }
            else
            {
                // コードを元に名称を取得する
                this.lblFunanushiCdTo.Text = this.Dba.GetName(this.UInfo, "VI_HN_FUNANUSHI", this.txtFunanushiCdTo.Text);
            }

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 年のチェック
            if (!IsValidYear())
            {
                this.txtDateYear.Focus();
                this.txtDateYear.SelectAll();
                return false;
            }

            // 月のチェック
            if (!IsValidMonth())
            {
                this.txtDateMonth.Focus();
                this.txtDateMonth.SelectAll();
                return false;
            }

            // 船主コード(自)の入力チェック
            if (!IsValidCodeFr())
            {
                this.txtFunanushiCdFr.Focus();
                this.txtFunanushiCdFr.SelectAll();
                return false;
            }
            // 船主コード(至)の入力チェック
            if (!IsValidCodeTo())
            {
                this.txtFunanushiCdTo.Focus();
                this.txtFunanushiCdTo.SelectAll();
                return false;
            }

            return true;
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpDate(string[] arrJpDate)
        {
            this.lblDateGengo.Text = arrJpDate[0];
            this.txtDateYear.Text = arrJpDate[2];
        }

        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview)
        {
            try
            {
                bool dataFlag;

                this.Dba.BeginTransaction();

                // 帳票出力用にワークテーブルにデータを作成
                dataFlag = MakeWkData();

                // 帳票出力
                if (dataFlag)
                {
                    // 取得列の定義
                    StringBuilder cols = new StringBuilder();
                    cols.Append("  ITEM01");
                    cols.Append(" ,ITEM02");
                    cols.Append(" ,ITEM03");
                    cols.Append(" ,ITEM04");
                    cols.Append(" ,ITEM05");
                    cols.Append(" ,ITEM06");
                    cols.Append(" ,ITEM07");
                    cols.Append(" ,ITEM08");
                    cols.Append(" ,ITEM09");
                    cols.Append(" ,ITEM10");
                    cols.Append(" ,ITEM11");
                    cols.Append(" ,ITEM12");
                    cols.Append(" ,ITEM13");
                    cols.Append(" ,ITEM14");
                    cols.Append(" ,ITEM15");
                    cols.Append(" ,ITEM16");
                    cols.Append(" ,ITEM17");
                    cols.Append(" ,ITEM18");
                    cols.Append(" ,ITEM19");
                    cols.Append(" ,ITEM20");
                    cols.Append(" ,ITEM21");
                    cols.Append(" ,ITEM22");
                    cols.Append(" ,ITEM23");
                    cols.Append(" ,ITEM24");
                    cols.Append(" ,ITEM25");
                    cols.Append(" ,ITEM26");
                    cols.Append(" ,ITEM27");
                    cols.Append(" ,ITEM28");
                    cols.Append(" ,ITEM29");
                    cols.Append(" ,ITEM30");
                    cols.Append(" ,ITEM31");
                    cols.Append(" ,ITEM32");
                    cols.Append(" ,ITEM33");
                    cols.Append(" ,ITEM34");
                    cols.Append(" ,ITEM35");
                    cols.Append(" ,ITEM36");
                    cols.Append(" ,ITEM37");
                    cols.Append(" ,ITEM38");
                    cols.Append(" ,ITEM39");
                    cols.Append(" ,ITEM40");
                    cols.Append(" ,ITEM41");
                    cols.Append(" ,ITEM42");
                    cols.Append(" ,ITEM43");
                    cols.Append(" ,ITEM44");
                    cols.Append(" ,ITEM45");
                    cols.Append(" ,ITEM46");
                    cols.Append(" ,ITEM47");
                    cols.Append(" ,ITEM48");

                    // バインドパラメータの設定
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);

                    // データの取得
                    DataTable dtOutput = this.Dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_HN_TBL", "GUID = @GUID", "SORT ASC", dpc);

                    // 帳票オブジェクトをインスタンス化
                    HANR4071R rpt = new HANR4071R(dtOutput);

                    if (isPreview)
                    {
                        // プレビュー画面表示
                        PreviewForm pFrm = new PreviewForm(rpt, this.UnqId);
                        pFrm.WindowState = FormWindowState.Maximized;
                        pFrm.Show();
                    }
                    else
                    {
                        // 直接印刷
                        rpt.Run(false);
                        rpt.Document.Print(true, true, false);
                    }
                }
            }
            finally
            {
                this.Dba.Rollback();
            }
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData()
        {
            #region データ取得の準備
            // 日付範囲を西暦にして取得
            DateTime tmpDate = Util.ConvAdDate(this.lblDateGengo.Text, this.txtDateYear.Text,
                    this.txtDateMonth.Text, "1", this.Dba);
            // 集計年度の設定
            int tmpShukeiNen;
            if (Util.ToInt(this.txtDateMonth.Text) < 4)
            {
                tmpShukeiNen = tmpDate.Year - 1;
            }
            else
            {
                tmpShukeiNen = tmpDate.Year;
            }
            // 年月範囲の設定
            string dataFr;
            string dataTo;
            dataFr = tmpDate.Year + "/" + tmpDate.Month + "/" + tmpDate.Day;
            dataTo = tmpDate.Year + "/" + tmpDate.Month + "/" + DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            // 表示日付設定
            string[] aryJpDate = Util.ConvJpDate(Util.ToString(tmpShukeiNen) + "/12/31", this.Dba);
            string hyojiDate = aryJpDate[0] + aryJpDate[2] + "年分";
            string hyojiDate02 = this.lblDateGengo.Text + this.txtDateYear.Text + "年" + this.txtDateMonth.Text + "月分";

            // 船主コード設定
            string FUNANUSHI_CD_FR;
            string FUNANUSHI_CD_TO;
            if (Util.ToDecimal(txtFunanushiCdFr.Text) > 0)
            {
                FUNANUSHI_CD_FR = txtFunanushiCdFr.Text;
            }
            else
            {
                FUNANUSHI_CD_FR = "0";
            }
            if (Util.ToDecimal(txtFunanushiCdTo.Text) > 0)
            {
                FUNANUSHI_CD_TO = txtFunanushiCdTo.Text;
            }
            else
            {
                FUNANUSHI_CD_TO = "9999";
            }
            int i = 0; // ループ用カウント変数
            int y = 0; // ループ用カウント変数
            int dbSORT = 1;
            // 日数有無用変数
            string nissu01 = "";
            string nissu02 = "";
            string nissu03 = "";
            string nissu04 = "";
            string nissu05 = "";
            string nissu06 = "";
            string nissu07 = "";
            string nissu08 = "";
            string nissu09 = "";
            string nissu10 = "";
            string nissu11 = "";
            string nissu12 = "";
            string nissu13 = "";
            string nissu14 = "";
            string nissu15 = "";
            string nissu16 = "";
            string nissu17 = "";
            string nissu18 = "";
            string nissu19 = "";
            string nissu20 = "";
            string nissu21 = "";
            string nissu22 = "";
            string nissu23 = "";
            string nissu24 = "";
            string nissu25 = "";
            string nissu26 = "";
            string nissu27 = "";
            string nissu28 = "";
            string nissu29 = "";
            string nissu30 = "";
            string nissu31 = "";
            #endregion

            #region メインデータ取得
            // 入力された情報を元にワークテーブルに更新をする
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder sql = new StringBuilder();
            // han.VI_船主別水揚日数日計(VI_HN_SENSHUBRTSU_MZAG_NS_NNKI)の指定年度から発生しているデータを取得
            // NOTE:4～12月のデータと1～3月のデータを別々にINLINE VIEWで取得してFULL OUTER JOINで結合することで
            //      どっちかが欠けていても船主単位でデータを横持ちにしている
            sql.Append(" SELECT");
            sql.Append(" ISNULL(M1.KAISHA_CD, M2.KAISHA_CD) AS KAISHA_CD,");
            sql.Append(" ISNULL(M1.SENSHU_CD, M2.SENSHU_CD) AS SENSHU_CD,");
            sql.Append(" ISNULL(M1.SENSHU_NM, M2.SENSHU_NM) AS SENSHU_NM,");
            sql.Append(" ISNULL(M1.CHIKU_CD, M2.CHIKU_CD) AS CHIKU_CD,");
            sql.Append(" ISNULL(M1.CHIKU_NM, M2.CHIKU_NM) AS CHIKU_NM,");
            sql.Append(" ISNULL(M2.NISSU_1GATSU, 0) AS NISSU_1GATSU,");
            sql.Append(" ISNULL(M2.NISSU_2GATSU, 0) AS NISSU_2GATSU,");
            sql.Append(" ISNULL(M2.NISSU_3GATSU, 0) AS NISSU_3GATSU,");
            sql.Append(" ISNULL(M1.NISSU_4GATSU, 0) AS NISSU_4GATSU,");
            sql.Append(" ISNULL(M1.NISSU_5GATSU, 0) AS NISSU_5GATSU,");
            sql.Append(" ISNULL(M1.NISSU_6GATSU, 0) AS NISSU_6GATSU,");
            sql.Append(" ISNULL(M1.NISSU_7GATSU, 0) AS NISSU_7GATSU,");
            sql.Append(" ISNULL(M1.NISSU_8GATSU, 0) AS NISSU_8GATSU,");
            sql.Append(" ISNULL(M1.NISSU_9GATSU, 0) AS NISSU_9GATSU,");
            sql.Append(" ISNULL(M1.NISSU_10GATSU, 0) AS NISSU_10GATSU,");
            sql.Append(" ISNULL(M1.NISSU_11GATSU, 0) AS NISSU_11GATSU,");
            sql.Append(" ISNULL(M1.NISSU_12GATSU, 0) AS NISSU_12GATSU");

            sql.Append(" FROM");

            sql.Append(" (SELECT");
            sql.Append("  A.KAISHA_CD,");
            sql.Append("  A.SENSHU_CD,");
            sql.Append("  MIN(A.SENSHU_NM) AS SENSHU_NM,");
            sql.Append("  A.CHIKU_CD,");
            sql.Append("  MIN(A.CHIKU_NM) AS CHIKU_NM,");
            sql.Append("  SUM(A.NISSU_4GATSU) AS NISSU_4GATSU,");
            sql.Append("  SUM(A.NISSU_5GATSU) AS NISSU_5GATSU,");
            sql.Append("  SUM(A.NISSU_6GATSU) AS NISSU_6GATSU,");
            sql.Append("  SUM(A.NISSU_7GATSU) AS NISSU_7GATSU,");
            sql.Append("  SUM(A.NISSU_8GATSU) AS NISSU_8GATSU,");
            sql.Append("  SUM(A.NISSU_9GATSU) AS NISSU_9GATSU,");
            sql.Append("  SUM(A.NISSU_10GATSU) AS NISSU_10GATSU,");
            sql.Append("  SUM(A.NISSU_11GATSU) AS NISSU_11GATSU,");
            sql.Append("  SUM(A.NISSU_12GATSU) AS NISSU_12GATSU");
            sql.Append("  FROM");
            sql.Append("  VI_HN_SENSHUBRTSU_MZAG_NS_NNKI AS A");
            sql.Append("  WHERE");
            sql.Append("  A.SHUKEI_NEN = @SHUKEI_NEN AND");
            sql.Append("  A.SENSHU_CD BETWEEN @SENSHU_CD_FR AND @SENSHU_CD_TO");
            sql.Append("  GROUP BY");
            sql.Append("  A.KAISHA_CD, A.SENSHU_CD, A.CHIKU_CD) AS M1");

            sql.Append(" FULL OUTER JOIN");

            sql.Append(" (SELECT");
            sql.Append("  A.KAISHA_CD,");
            sql.Append("  A.SENSHU_CD,");
            sql.Append("  MIN(A.SENSHU_NM) AS SENSHU_NM,");
            sql.Append("  A.CHIKU_CD,");
            sql.Append("  MIN(A.CHIKU_NM) AS CHIKU_NM,");
            sql.Append("  SUM(A.NISSU_1GATSU) AS NISSU_1GATSU,");
            sql.Append("  SUM(A.NISSU_2GATSU) AS NISSU_2GATSU,");
            sql.Append("  SUM(A.NISSU_3GATSU) AS NISSU_3GATSU");
            sql.Append("  FROM");
            sql.Append("  VI_HN_SENSHUBRTSU_MZAG_NS_NNKI AS A");
            sql.Append("  WHERE");
            sql.Append("  A.SHUKEI_NEN = @SHUKEI_NEN_N AND");
            sql.Append("  A.SENSHU_CD BETWEEN @SENSHU_CD_FR AND @SENSHU_CD_TO");
            sql.Append("  GROUP BY");
            sql.Append("  A.KAISHA_CD, A.SENSHU_CD, A.CHIKU_CD) AS M2");

            sql.Append(" ON M1.KAISHA_CD = M2.KAISHA_CD");
            sql.Append(" AND M1.SENSHU_CD = M2.SENSHU_CD");
            sql.Append(" AND M1.CHIKU_CD = M2.CHIKU_CD");
            sql.Append(" ORDER BY");
            sql.Append(" KAISHA_CD, CHIKU_CD, SENSHU_CD ");

            dpc.SetParam("@SHUKEI_NEN", SqlDbType.Decimal, 4, tmpShukeiNen);
            dpc.SetParam("@SHUKEI_NEN_N", SqlDbType.Decimal, 4, tmpShukeiNen + 1);
            dpc.SetParam("@SENSHU_CD_FR", SqlDbType.Decimal, 6, FUNANUSHI_CD_FR);
            dpc.SetParam("@SENSHU_CD_TO", SqlDbType.Decimal, 6, FUNANUSHI_CD_TO);

            DataTable dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);
            #endregion

            if (dtMainLoop.Rows.Count == 0)
            {
                Msg.Info("該当データがありません。");
                return false;
            }
            else
            {
                while (dtMainLoop.Rows.Count > i)
                {
                    #region 指定月の日数データ取得
                    // 入力された情報を元にワークテーブルに更新をする
                    dpc = new DbParamCollection();
                    sql = new StringBuilder();
                    // han.TB_仕切データ(TB_HN_SHIKIRI_DATA)の指定月、指定船主CDから発生しているデータを取得
                    sql.Append(" SELECT");
                    sql.Append(" MONTH(KEIJO_NENGAPPI),");
                    sql.Append(" CASE WHEN (DAY(KEIJO_NENGAPPI) = 1) THEN COUNT(DAY(KEIJO_NENGAPPI)) ELSE 0 END AS NISSU01,");
                    sql.Append(" CASE WHEN (DAY(KEIJO_NENGAPPI) = 2) THEN COUNT(DAY(KEIJO_NENGAPPI)) ELSE 0 END AS NISSU02,");
                    sql.Append(" CASE WHEN (DAY(KEIJO_NENGAPPI) = 3) THEN COUNT(DAY(KEIJO_NENGAPPI)) ELSE 0 END AS NISSU03,");
                    sql.Append(" CASE WHEN (DAY(KEIJO_NENGAPPI) = 4) THEN COUNT(DAY(KEIJO_NENGAPPI)) ELSE 0 END AS NISSU04,");
                    sql.Append(" CASE WHEN (DAY(KEIJO_NENGAPPI) = 5) THEN COUNT(DAY(KEIJO_NENGAPPI)) ELSE 0 END AS NISSU05,");
                    sql.Append(" CASE WHEN (DAY(KEIJO_NENGAPPI) = 6) THEN COUNT(DAY(KEIJO_NENGAPPI)) ELSE 0 END AS NISSU06,");
                    sql.Append(" CASE WHEN (DAY(KEIJO_NENGAPPI) = 7) THEN COUNT(DAY(KEIJO_NENGAPPI)) ELSE 0 END AS NISSU07,");
                    sql.Append(" CASE WHEN (DAY(KEIJO_NENGAPPI) = 8) THEN COUNT(DAY(KEIJO_NENGAPPI)) ELSE 0 END AS NISSU08,");
                    sql.Append(" CASE WHEN (DAY(KEIJO_NENGAPPI) = 9) THEN COUNT(DAY(KEIJO_NENGAPPI)) ELSE 0 END AS NISSU09,");
                    sql.Append(" CASE WHEN (DAY(KEIJO_NENGAPPI) = 10) THEN COUNT(DAY(KEIJO_NENGAPPI)) ELSE 0 END AS NISSU10,");
                    sql.Append(" CASE WHEN (DAY(KEIJO_NENGAPPI) = 11) THEN COUNT(DAY(KEIJO_NENGAPPI)) ELSE 0 END AS NISSU11,");
                    sql.Append(" CASE WHEN (DAY(KEIJO_NENGAPPI) = 12) THEN COUNT(DAY(KEIJO_NENGAPPI)) ELSE 0 END AS NISSU12,");
                    sql.Append(" CASE WHEN (DAY(KEIJO_NENGAPPI) = 13) THEN COUNT(DAY(KEIJO_NENGAPPI)) ELSE 0 END AS NISSU13,");
                    sql.Append(" CASE WHEN (DAY(KEIJO_NENGAPPI) = 14) THEN COUNT(DAY(KEIJO_NENGAPPI)) ELSE 0 END AS NISSU14,");
                    sql.Append(" CASE WHEN (DAY(KEIJO_NENGAPPI) = 15) THEN COUNT(DAY(KEIJO_NENGAPPI)) ELSE 0 END AS NISSU15,");
                    sql.Append(" CASE WHEN (DAY(KEIJO_NENGAPPI) = 16) THEN COUNT(DAY(KEIJO_NENGAPPI)) ELSE 0 END AS NISSU16,");
                    sql.Append(" CASE WHEN (DAY(KEIJO_NENGAPPI) = 17) THEN COUNT(DAY(KEIJO_NENGAPPI)) ELSE 0 END AS NISSU17,");
                    sql.Append(" CASE WHEN (DAY(KEIJO_NENGAPPI) = 18) THEN COUNT(DAY(KEIJO_NENGAPPI)) ELSE 0 END AS NISSU18,");
                    sql.Append(" CASE WHEN (DAY(KEIJO_NENGAPPI) = 19) THEN COUNT(DAY(KEIJO_NENGAPPI)) ELSE 0 END AS NISSU19,");
                    sql.Append(" CASE WHEN (DAY(KEIJO_NENGAPPI) = 20) THEN COUNT(DAY(KEIJO_NENGAPPI)) ELSE 0 END AS NISSU20,");
                    sql.Append(" CASE WHEN (DAY(KEIJO_NENGAPPI) = 21) THEN COUNT(DAY(KEIJO_NENGAPPI)) ELSE 0 END AS NISSU21,");
                    sql.Append(" CASE WHEN (DAY(KEIJO_NENGAPPI) = 22) THEN COUNT(DAY(KEIJO_NENGAPPI)) ELSE 0 END AS NISSU22,");
                    sql.Append(" CASE WHEN (DAY(KEIJO_NENGAPPI) = 23) THEN COUNT(DAY(KEIJO_NENGAPPI)) ELSE 0 END AS NISSU23,");
                    sql.Append(" CASE WHEN (DAY(KEIJO_NENGAPPI) = 24) THEN COUNT(DAY(KEIJO_NENGAPPI)) ELSE 0 END AS NISSU24,");
                    sql.Append(" CASE WHEN (DAY(KEIJO_NENGAPPI) = 25) THEN COUNT(DAY(KEIJO_NENGAPPI)) ELSE 0 END AS NISSU25,");
                    sql.Append(" CASE WHEN (DAY(KEIJO_NENGAPPI) = 26) THEN COUNT(DAY(KEIJO_NENGAPPI)) ELSE 0 END AS NISSU26,");
                    sql.Append(" CASE WHEN (DAY(KEIJO_NENGAPPI) = 27) THEN COUNT(DAY(KEIJO_NENGAPPI)) ELSE 0 END AS NISSU27,");
                    sql.Append(" CASE WHEN (DAY(KEIJO_NENGAPPI) = 28) THEN COUNT(DAY(KEIJO_NENGAPPI)) ELSE 0 END AS NISSU28,");
                    sql.Append(" CASE WHEN (DAY(KEIJO_NENGAPPI) = 29) THEN COUNT(DAY(KEIJO_NENGAPPI)) ELSE 0 END AS NISSU29,");
                    sql.Append(" CASE WHEN (DAY(KEIJO_NENGAPPI) = 30) THEN COUNT(DAY(KEIJO_NENGAPPI)) ELSE 0 END AS NISSU30,");
                    sql.Append(" CASE WHEN (DAY(KEIJO_NENGAPPI) = 31) THEN COUNT(DAY(KEIJO_NENGAPPI)) ELSE 0 END AS NISSU31");
                    sql.Append(" FROM");
                    sql.Append(" TB_HN_SHIKIRI_DATA");
                    sql.Append(" WHERE");
                    sql.Append(" KAISHA_CD = @KAISHA_CD AND");
                    sql.Append(" SENSHU_CD = @SENSHU_CD AND");
                    sql.Append(" SERIBI BETWEEN @SERIBI_FR AND @SERIBI_TO");
                    sql.Append(" GROUP BY ");
                    sql.Append(" KEIJO_NENGAPPI");

                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                    dpc.SetParam("@SENSHU_CD", SqlDbType.Decimal, 4, dtMainLoop.Rows[i]["SENSHU_CD"]);
                    dpc.SetParam("@SERIBI_FR", SqlDbType.DateTime, dataFr);
                    dpc.SetParam("@SERIBI_TO", SqlDbType.DateTime, dataTo);

                    DataTable dtNissuLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);
                    #endregion

                    #region 指定月の水揚日設定
                    while (dtNissuLoop.Rows.Count > y)
                    {
                        if (Util.ToInt(dtNissuLoop.Rows[y]["NISSU01"]) > 0)
                        {
                            nissu01 = "*";
                        }
                        if (Util.ToInt(dtNissuLoop.Rows[y]["NISSU02"]) > 0)
                        {
                            nissu02 = "*";
                        }
                        if (Util.ToInt(dtNissuLoop.Rows[y]["NISSU03"]) > 0)
                        {
                            nissu03 = "*";
                        }
                        if (Util.ToInt(dtNissuLoop.Rows[y]["NISSU04"]) > 0)
                        {
                            nissu04 = "*";
                        }
                        if (Util.ToInt(dtNissuLoop.Rows[y]["NISSU05"]) > 0)
                        {
                            nissu05 = "*";
                        }
                        if (Util.ToInt(dtNissuLoop.Rows[y]["NISSU06"]) > 0)
                        {
                            nissu06 = "*";
                        }
                        if (Util.ToInt(dtNissuLoop.Rows[y]["NISSU07"]) > 0)
                        {
                            nissu07 = "*";
                        }
                        if (Util.ToInt(dtNissuLoop.Rows[y]["NISSU08"]) > 0)
                        {
                            nissu08 = "*";
                        }
                        if (Util.ToInt(dtNissuLoop.Rows[y]["NISSU09"]) > 0)
                        {
                            nissu09 = "*";
                        }
                        if (Util.ToInt(dtNissuLoop.Rows[y]["NISSU10"]) > 0)
                        {
                            nissu10 = "*";
                        }
                        if (Util.ToInt(dtNissuLoop.Rows[y]["NISSU11"]) > 0)
                        {
                            nissu11 = "*";
                        }
                        if (Util.ToInt(dtNissuLoop.Rows[y]["NISSU12"]) > 0)
                        {
                            nissu12 = "*";
                        }
                        if (Util.ToInt(dtNissuLoop.Rows[y]["NISSU13"]) > 0)
                        {
                            nissu13 = "*";
                        }
                        if (Util.ToInt(dtNissuLoop.Rows[y]["NISSU14"]) > 0)
                        {
                            nissu14 = "*";
                        }
                        if (Util.ToInt(dtNissuLoop.Rows[y]["NISSU15"]) > 0)
                        {
                            nissu15 = "*";
                        }
                        if (Util.ToInt(dtNissuLoop.Rows[y]["NISSU16"]) > 0)
                        {
                            nissu16 = "*";
                        }
                        if (Util.ToInt(dtNissuLoop.Rows[y]["NISSU17"]) > 0)
                        {
                            nissu17 = "*";
                        }
                        if (Util.ToInt(dtNissuLoop.Rows[y]["NISSU18"]) > 0)
                        {
                            nissu18 = "*";
                        }
                        if (Util.ToInt(dtNissuLoop.Rows[y]["NISSU19"]) > 0)
                        {
                            nissu19 = "*";
                        }
                        if (Util.ToInt(dtNissuLoop.Rows[y]["NISSU20"]) > 0)
                        {
                            nissu20 = "*";
                        }
                        if (Util.ToInt(dtNissuLoop.Rows[y]["NISSU21"]) > 0)
                        {
                            nissu21 = "*";
                        }
                        if (Util.ToInt(dtNissuLoop.Rows[y]["NISSU22"]) > 0)
                        {
                            nissu22 = "*";
                        }
                        if (Util.ToInt(dtNissuLoop.Rows[y]["NISSU23"]) > 0)
                        {
                            nissu23 = "*";
                        }
                        if (Util.ToInt(dtNissuLoop.Rows[y]["NISSU24"]) > 0)
                        {
                            nissu24 = "*";
                        }
                        if (Util.ToInt(dtNissuLoop.Rows[y]["NISSU25"]) > 0)
                        {
                            nissu25 = "*";
                        }
                        if (Util.ToInt(dtNissuLoop.Rows[y]["NISSU26"]) > 0)
                        {
                            nissu26 = "*";
                        }
                        if (Util.ToInt(dtNissuLoop.Rows[y]["NISSU27"]) > 0)
                        {
                            nissu27 = "*";
                        }
                        if (Util.ToInt(dtNissuLoop.Rows[y]["NISSU28"]) > 0)
                        {
                            nissu28 = "*";
                        }
                        if (Util.ToInt(dtNissuLoop.Rows[y]["NISSU29"]) > 0)
                        {
                            nissu29 = "*";
                        }
                        if (Util.ToInt(dtNissuLoop.Rows[y]["NISSU30"]) > 0)
                        {
                            nissu30 = "*";
                        }
                        if (Util.ToInt(dtNissuLoop.Rows[y]["NISSU31"]) > 0)
                        {
                            nissu31 = "*";
                        }
                        y++;
                    }

                    #endregion

                    #region インサートテーブル
                    sql = new StringBuilder();
                    dpc = new DbParamCollection();
                    sql.Append("INSERT INTO PR_HN_TBL(");
                    sql.Append("  GUID");
                    sql.Append(" ,SORT");
                    sql.Append(" ,ITEM01");
                    sql.Append(" ,ITEM02");
                    sql.Append(" ,ITEM03");
                    sql.Append(" ,ITEM04");
                    sql.Append(" ,ITEM05");
                    sql.Append(" ,ITEM06");
                    sql.Append(" ,ITEM07");
                    sql.Append(" ,ITEM08");
                    sql.Append(" ,ITEM09");
                    sql.Append(" ,ITEM10");
                    sql.Append(" ,ITEM11");
                    sql.Append(" ,ITEM12");
                    sql.Append(" ,ITEM13");
                    sql.Append(" ,ITEM14");
                    sql.Append(" ,ITEM15");
                    sql.Append(" ,ITEM16");
                    sql.Append(" ,ITEM17");
                    sql.Append(" ,ITEM18");
                    sql.Append(" ,ITEM19");
                    sql.Append(" ,ITEM20");
                    sql.Append(" ,ITEM21");
                    sql.Append(" ,ITEM22");
                    sql.Append(" ,ITEM23");
                    sql.Append(" ,ITEM24");
                    sql.Append(" ,ITEM25");
                    sql.Append(" ,ITEM26");
                    sql.Append(" ,ITEM27");
                    sql.Append(" ,ITEM28");
                    sql.Append(" ,ITEM29");
                    sql.Append(" ,ITEM30");
                    sql.Append(" ,ITEM31");
                    sql.Append(" ,ITEM32");
                    sql.Append(" ,ITEM33");
                    sql.Append(" ,ITEM34");
                    sql.Append(" ,ITEM35");
                    sql.Append(" ,ITEM36");
                    sql.Append(" ,ITEM37");
                    sql.Append(" ,ITEM38");
                    sql.Append(" ,ITEM39");
                    sql.Append(" ,ITEM40");
                    sql.Append(" ,ITEM41");
                    sql.Append(" ,ITEM42");
                    sql.Append(" ,ITEM43");
                    sql.Append(" ,ITEM44");
                    sql.Append(" ,ITEM45");
                    sql.Append(" ,ITEM46");
                    sql.Append(" ,ITEM47");
                    sql.Append(" ,ITEM48");
                    sql.Append(") ");
                    sql.Append("VALUES(");
                    sql.Append("  @GUID");
                    sql.Append(" ,@SORT");
                    sql.Append(" ,@ITEM01");
                    sql.Append(" ,@ITEM02");
                    sql.Append(" ,@ITEM03");
                    sql.Append(" ,@ITEM04");
                    sql.Append(" ,@ITEM05");
                    sql.Append(" ,@ITEM06");
                    sql.Append(" ,@ITEM07");
                    sql.Append(" ,@ITEM08");
                    sql.Append(" ,@ITEM09");
                    sql.Append(" ,@ITEM10");
                    sql.Append(" ,@ITEM11");
                    sql.Append(" ,@ITEM12");
                    sql.Append(" ,@ITEM13");
                    sql.Append(" ,@ITEM14");
                    sql.Append(" ,@ITEM15");
                    sql.Append(" ,@ITEM16");
                    sql.Append(" ,@ITEM17");
                    sql.Append(" ,@ITEM18");
                    sql.Append(" ,@ITEM19");
                    sql.Append(" ,@ITEM20");
                    sql.Append(" ,@ITEM21");
                    sql.Append(" ,@ITEM22");
                    sql.Append(" ,@ITEM23");
                    sql.Append(" ,@ITEM24");
                    sql.Append(" ,@ITEM25");
                    sql.Append(" ,@ITEM26");
                    sql.Append(" ,@ITEM27");
                    sql.Append(" ,@ITEM28");
                    sql.Append(" ,@ITEM29");
                    sql.Append(" ,@ITEM30");
                    sql.Append(" ,@ITEM31");
                    sql.Append(" ,@ITEM32");
                    sql.Append(" ,@ITEM33");
                    sql.Append(" ,@ITEM34");
                    sql.Append(" ,@ITEM35");
                    sql.Append(" ,@ITEM36");
                    sql.Append(" ,@ITEM37");
                    sql.Append(" ,@ITEM38");
                    sql.Append(" ,@ITEM39");
                    sql.Append(" ,@ITEM40");
                    sql.Append(" ,@ITEM41");
                    sql.Append(" ,@ITEM42");
                    sql.Append(" ,@ITEM43");
                    sql.Append(" ,@ITEM44");
                    sql.Append(" ,@ITEM45");
                    sql.Append(" ,@ITEM46");
                    sql.Append(" ,@ITEM47");
                    sql.Append(" ,@ITEM48");
                    sql.Append(") ");
                    #endregion

                    #region データ登録
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                    dbSORT++;
                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["CHIKU_CD"]); // 地区コード
                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["CHIKU_NM"]); // 地区名
                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SENSHU_CD"]); // 船主CD
                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SENSHU_NM"]); // 船主名称
                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, nissu01); // 日数01
                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, nissu02); // 日数02
                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, nissu03); // 日数03
                    dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, nissu04); // 日数04
                    dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, nissu05); // 日数05
                    dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, nissu06); // 日数06
                    dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, nissu07); // 日数07
                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, nissu08); // 日数08
                    dpc.SetParam("@ITEM13", SqlDbType.VarChar, 200, nissu09); // 日数09
                    dpc.SetParam("@ITEM14", SqlDbType.VarChar, 200, nissu10); // 日数10
                    dpc.SetParam("@ITEM15", SqlDbType.VarChar, 200, nissu11); // 日数11
                    dpc.SetParam("@ITEM16", SqlDbType.VarChar, 200, nissu12); // 日数12
                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, nissu13); // 日数13
                    dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, nissu14); // 日数14
                    dpc.SetParam("@ITEM19", SqlDbType.VarChar, 200, nissu15); // 日数15
                    dpc.SetParam("@ITEM20", SqlDbType.VarChar, 200, nissu16); // 日数16
                    dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, nissu17); // 日数17
                    dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, nissu18); // 日数18
                    dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, nissu19); // 日数19
                    dpc.SetParam("@ITEM24", SqlDbType.VarChar, 200, nissu20); // 日数20
                    dpc.SetParam("@ITEM25", SqlDbType.VarChar, 200, nissu21); // 日数21
                    dpc.SetParam("@ITEM26", SqlDbType.VarChar, 200, nissu22); // 日数22
                    dpc.SetParam("@ITEM27", SqlDbType.VarChar, 200, nissu23); // 日数23
                    dpc.SetParam("@ITEM28", SqlDbType.VarChar, 200, nissu24); // 日数24
                    dpc.SetParam("@ITEM29", SqlDbType.VarChar, 200, nissu25); // 日数25
                    dpc.SetParam("@ITEM30", SqlDbType.VarChar, 200, nissu26); // 日数26
                    dpc.SetParam("@ITEM31", SqlDbType.VarChar, 200, nissu27); // 日数27
                    dpc.SetParam("@ITEM32", SqlDbType.VarChar, 200, nissu28); // 日数28
                    dpc.SetParam("@ITEM33", SqlDbType.VarChar, 200, nissu29); // 日数29
                    dpc.SetParam("@ITEM34", SqlDbType.VarChar, 200, nissu30); // 日数30
                    dpc.SetParam("@ITEM35", SqlDbType.VarChar, 200, nissu31); // 日数31
                    dpc.SetParam("@ITEM36", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["NISSU_4GATSU"]); // 4月
                    dpc.SetParam("@ITEM37", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["NISSU_5GATSU"]); // 5月
                    dpc.SetParam("@ITEM38", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["NISSU_6GATSU"]); // 6月
                    dpc.SetParam("@ITEM39", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["NISSU_7GATSU"]); // 7月
                    dpc.SetParam("@ITEM40", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["NISSU_8GATSU"]); // 8月
                    dpc.SetParam("@ITEM41", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["NISSU_9GATSU"]); // 9月
                    dpc.SetParam("@ITEM42", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["NISSU_10GATSU"]); // 10月
                    dpc.SetParam("@ITEM43", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["NISSU_11GATSU"]); // 11月
                    dpc.SetParam("@ITEM44", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["NISSU_12GATSU"]); // 12月
                    dpc.SetParam("@ITEM45", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["NISSU_1GATSU"]); // 1月
                    dpc.SetParam("@ITEM46", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["NISSU_2GATSU"]); // 2月
                    dpc.SetParam("@ITEM47", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["NISSU_3GATSU"]); // 3月
                    dpc.SetParam("@ITEM48", SqlDbType.VarChar, 200, hyojiDate02); 

                    this.Dba.ModifyBySql(Util.ToString(sql), dpc);
                    #endregion

                    #region 日数表示の初期化
                    nissu01 = "";
                    nissu02 = "";
                    nissu03 = "";
                    nissu04 = "";
                    nissu05 = "";
                    nissu06 = "";
                    nissu07 = "";
                    nissu08 = "";
                    nissu09 = "";
                    nissu10 = "";
                    nissu11 = "";
                    nissu12 = "";
                    nissu13 = "";
                    nissu14 = "";
                    nissu15 = "";
                    nissu16 = "";
                    nissu17 = "";
                    nissu18 = "";
                    nissu19 = "";
                    nissu20 = "";
                    nissu21 = "";
                    nissu22 = "";
                    nissu23 = "";
                    nissu24 = "";
                    nissu25 = "";
                    nissu26 = "";
                    nissu27 = "";
                    nissu28 = "";
                    nissu29 = "";
                    nissu30 = "";
                    nissu31 = "";
                    #endregion
                    i++;
                    y = 0;
                }
            }

            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
                "SORT",
                "PR_HN_TBL",
                "GUID = @GUID",
                dpc);

            bool dataFlag;
            if (tmpdtPR_HN_TBL.Rows.Count > 0)
            {
                dataFlag = true;
            }
            else
            {
                dataFlag = false;
            }

            return dataFlag;
        }
        #endregion
    }
}
