﻿namespace jp.co.fsi.han.hanr4071
{
    /// <summary>
    /// HANR4071R の概要の説明です。
    /// </summary>
    partial class HANR4071R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(HANR4071R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.テキスト001 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtToday = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.テキスト6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル29 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル30 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル31 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル32 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル33 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル34 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル35 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル36 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル37 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル38 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル39 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル40 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル41 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル42 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル43 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル48 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル50 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル51 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル52 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル53 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル54 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル55 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル56 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル57 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル58 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル59 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル60 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル61 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル62 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル63 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル64 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル65 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル67 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル68 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル69 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル70 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル71 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル72 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル73 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル74 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル75 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル76 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル77 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル78 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル79 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル80 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル138 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル151 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.直線82 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線83 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線84 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線85 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線86 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線87 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線88 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線89 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線90 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線91 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線92 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線93 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線94 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線95 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線96 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線97 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線98 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線99 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線100 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.テキスト003 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト004 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト005 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト006 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト007 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト008 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト009 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト010 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト011 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト012 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト013 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト014 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト015 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト016 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト017 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト018 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト019 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト020 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト021 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト022 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト023 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト024 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト025 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト026 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト027 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト028 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト029 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト030 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト031 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト032 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト033 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト034 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト035 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト036 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト037 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト038 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト039 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト040 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト041 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト042 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト043 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト044 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト045 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト046 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト047 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト152 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.reportHeader1 = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.reportFooter1 = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.ラベル153 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.テキスト154 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト155 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト156 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト157 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト158 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト159 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト160 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト161 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト162 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト163 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト164 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト165 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト166 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.直線167 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.groupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.textBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.groupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト001)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtToday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル54)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル55)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル56)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル57)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル58)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル59)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル60)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル61)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル62)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル63)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル64)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル65)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル67)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル68)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル69)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル70)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル71)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル72)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル73)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル74)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル75)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル76)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル77)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル78)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル79)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル80)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル138)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル151)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト003)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト004)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト005)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト006)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト007)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト008)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト009)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト010)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト011)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト012)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト013)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト014)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト015)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト016)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト017)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト018)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト019)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト020)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト021)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト022)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト023)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト024)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト025)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト026)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト027)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト028)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト029)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト030)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト031)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト032)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト033)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト034)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト035)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト036)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト037)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト038)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト039)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト040)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト041)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト042)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト043)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト044)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト045)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト046)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト047)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト152)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル153)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト154)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト155)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト156)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト157)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト158)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト159)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト160)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト161)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト162)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト163)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト164)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト165)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト166)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.テキスト001,
            this.ラベル2,
            this.txtToday,
            this.ラベル5,
            this.テキスト6,
            this.ラベル8,
            this.ラベル9,
            this.ラベル29,
            this.ラベル30,
            this.ラベル31,
            this.ラベル32,
            this.ラベル33,
            this.ラベル34,
            this.ラベル35,
            this.ラベル36,
            this.ラベル37,
            this.ラベル38,
            this.ラベル39,
            this.ラベル40,
            this.ラベル41,
            this.ラベル42,
            this.ラベル43,
            this.ラベル48,
            this.ラベル50,
            this.ラベル51,
            this.ラベル52,
            this.ラベル53,
            this.ラベル54,
            this.ラベル55,
            this.ラベル56,
            this.ラベル57,
            this.ラベル58,
            this.ラベル59,
            this.ラベル60,
            this.ラベル61,
            this.ラベル62,
            this.ラベル63,
            this.ラベル64,
            this.ラベル65,
            this.ラベル67,
            this.ラベル68,
            this.ラベル69,
            this.ラベル70,
            this.ラベル71,
            this.ラベル72,
            this.ラベル73,
            this.ラベル74,
            this.ラベル75,
            this.ラベル76,
            this.ラベル77,
            this.ラベル78,
            this.ラベル79,
            this.ラベル80,
            this.ラベル138,
            this.ラベル151,
            this.label2,
            this.label3,
            this.line2});
            this.pageHeader.Height = 0.9604167F;
            this.pageHeader.Name = "pageHeader";
            this.pageHeader.Format += new System.EventHandler(this.pageHeader_Format);
            // 
            // テキスト001
            // 
            this.テキスト001.DataField = "ITEM48";
            this.テキスト001.Height = 0.15625F;
            this.テキスト001.Left = 0.3257961F;
            this.テキスト001.Name = "テキスト001";
            this.テキスト001.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.テキスト001.Tag = "";
            this.テキスト001.Text = null;
            this.テキスト001.Top = -7.450581E-09F;
            this.テキスト001.Width = 0.9625F;
            // 
            // ラベル2
            // 
            this.ラベル2.Height = 0.2291667F;
            this.ラベル2.HyperLink = null;
            this.ラベル2.Left = 5.308268F;
            this.ラベル2.Name = "ラベル2";
            this.ラベル2.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 16pt; font-weight: bold; text-align:" +
    " left; ddo-char-set: 1";
            this.ラベル2.Tag = "";
            this.ラベル2.Text = "* * 地区別船主別水揚日数一覧表 * *";
            this.ラベル2.Top = 0F;
            this.ラベル2.Width = 4.065117F;
            // 
            // txtToday
            // 
            this.txtToday.Height = 0.15625F;
            this.txtToday.Left = 11.10039F;
            this.txtToday.Name = "txtToday";
            this.txtToday.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: bold; text-align: right; ddo-char-set: 1";
            this.txtToday.Tag = "";
            this.txtToday.Text = null;
            this.txtToday.Top = 0F;
            this.txtToday.Width = 1.310703F;
            // 
            // ラベル5
            // 
            this.ラベル5.Height = 0.15625F;
            this.ラベル5.HyperLink = null;
            this.ラベル5.Left = 12.45676F;
            this.ラベル5.Name = "ラベル5";
            this.ラベル5.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-weight: bold; text-align:" +
    " left; ddo-char-set: 1";
            this.ラベル5.Tag = "";
            this.ラベル5.Text = "作成";
            this.ラベル5.Top = -7.450581E-09F;
            this.ラベル5.Width = 0.4315367F;
            // 
            // テキスト6
            // 
            this.テキスト6.Height = 0.15625F;
            this.テキスト6.Left = 12.78394F;
            this.テキスト6.Name = "テキスト6";
            this.テキスト6.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: bold; text-align: right; ddo-char-set: 1";
            this.テキスト6.SummaryFunc = GrapeCity.ActiveReports.SectionReportModel.SummaryFunc.Count;
            this.テキスト6.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト6.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.テキスト6.Tag = "";
            this.テキスト6.Text = "=[Page]";
            this.テキスト6.Top = 0F;
            this.テキスト6.Width = 0.6023703F;
            // 
            // ラベル8
            // 
            this.ラベル8.Height = 0.15625F;
            this.ラベル8.HyperLink = null;
            this.ラベル8.Left = 0.2362205F;
            this.ラベル8.Name = "ラベル8";
            this.ラベル8.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-weight: bold; text-align:" +
    " right; ddo-char-set: 1";
            this.ラベル8.Tag = "";
            this.ラベル8.Text = "船主CD";
            this.ラベル8.Top = 0.6770834F;
            this.ラベル8.Width = 0.5374923F;
            // 
            // ラベル9
            // 
            this.ラベル9.Height = 0.15625F;
            this.ラベル9.HyperLink = null;
            this.ラベル9.Left = 0.8362128F;
            this.ラベル9.Name = "ラベル9";
            this.ラベル9.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-weight: bold; text-align:" +
    " left; ddo-char-set: 1";
            this.ラベル9.Tag = "";
            this.ラベル9.Text = "船主名称";
            this.ラベル9.Top = 0.6770834F;
            this.ラベル9.Width = 0.5520833F;
            // 
            // ラベル29
            // 
            this.ラベル29.Height = 0.15625F;
            this.ラベル29.HyperLink = null;
            this.ラベル29.Left = 2.669546F;
            this.ラベル29.Name = "ラベル29";
            this.ラベル29.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "center; ddo-char-set: 1";
            this.ラベル29.Tag = "";
            this.ラベル29.Text = "2";
            this.ラベル29.Top = 0.6354166F;
            this.ラベル29.Width = 0.1145833F;
            // 
            // ラベル30
            // 
            this.ラベル30.Height = 0.15625F;
            this.ラベル30.HyperLink = null;
            this.ラベル30.Left = 2.94038F;
            this.ラベル30.Name = "ラベル30";
            this.ラベル30.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "center; ddo-char-set: 1";
            this.ラベル30.Tag = "";
            this.ラベル30.Text = "3";
            this.ラベル30.Top = 0.6354166F;
            this.ラベル30.Width = 0.1145833F;
            // 
            // ラベル31
            // 
            this.ラベル31.Height = 0.15625F;
            this.ラベル31.HyperLink = null;
            this.ラベル31.Left = 3.200796F;
            this.ラベル31.Name = "ラベル31";
            this.ラベル31.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "center; ddo-char-set: 1";
            this.ラベル31.Tag = "";
            this.ラベル31.Text = "4";
            this.ラベル31.Top = 0.6354166F;
            this.ラベル31.Width = 0.1145833F;
            // 
            // ラベル32
            // 
            this.ラベル32.Height = 0.15625F;
            this.ラベル32.HyperLink = null;
            this.ラベル32.Left = 3.482046F;
            this.ラベル32.Name = "ラベル32";
            this.ラベル32.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "center; ddo-char-set: 1";
            this.ラベル32.Tag = "";
            this.ラベル32.Text = "5";
            this.ラベル32.Top = 0.6354166F;
            this.ラベル32.Width = 0.1145833F;
            // 
            // ラベル33
            // 
            this.ラベル33.Height = 0.15625F;
            this.ラベル33.HyperLink = null;
            this.ラベル33.Left = 3.75288F;
            this.ラベル33.Name = "ラベル33";
            this.ラベル33.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "center; ddo-char-set: 1";
            this.ラベル33.Tag = "";
            this.ラベル33.Text = "6";
            this.ラベル33.Top = 0.6354166F;
            this.ラベル33.Width = 0.1145833F;
            // 
            // ラベル34
            // 
            this.ラベル34.Height = 0.15625F;
            this.ラベル34.HyperLink = null;
            this.ラベル34.Left = 4.021629F;
            this.ラベル34.Name = "ラベル34";
            this.ラベル34.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "center; ddo-char-set: 1";
            this.ラベル34.Tag = "";
            this.ラベル34.Text = "7";
            this.ラベル34.Top = 0.6354166F;
            this.ラベル34.Width = 0.1145833F;
            // 
            // ラベル35
            // 
            this.ラベル35.Height = 0.15625F;
            this.ラベル35.HyperLink = null;
            this.ラベル35.Left = 4.297324F;
            this.ラベル35.Name = "ラベル35";
            this.ラベル35.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "center; ddo-char-set: 1";
            this.ラベル35.Tag = "";
            this.ラベル35.Text = "8";
            this.ラベル35.Top = 0.6354166F;
            this.ラベル35.Width = 0.1145833F;
            // 
            // ラベル36
            // 
            this.ラベル36.Height = 0.15625F;
            this.ラベル36.HyperLink = null;
            this.ラベル36.Left = 4.565379F;
            this.ラベル36.Name = "ラベル36";
            this.ラベル36.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "center; ddo-char-set: 1";
            this.ラベル36.Tag = "";
            this.ラベル36.Text = "9";
            this.ラベル36.Top = 0.6354166F;
            this.ラベル36.Width = 0.1145833F;
            // 
            // ラベル37
            // 
            this.ラベル37.Height = 0.15625F;
            this.ラベル37.HyperLink = null;
            this.ラベル37.Left = 4.809129F;
            this.ラベル37.Name = "ラベル37";
            this.ラベル37.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "center; ddo-char-set: 1";
            this.ラベル37.Tag = "";
            this.ラベル37.Text = "10";
            this.ラベル37.Top = 0.6354166F;
            this.ラベル37.Width = 0.1770833F;
            // 
            // ラベル38
            // 
            this.ラベル38.Height = 0.15625F;
            this.ラベル38.HyperLink = null;
            this.ラベル38.Left = 5.075796F;
            this.ラベル38.Name = "ラベル38";
            this.ラベル38.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "center; ddo-char-set: 1";
            this.ラベル38.Tag = "";
            this.ラベル38.Text = "11";
            this.ラベル38.Top = 0.6354166F;
            this.ラベル38.Width = 0.1770833F;
            // 
            // ラベル39
            // 
            this.ラベル39.Height = 0.15625F;
            this.ラベル39.HyperLink = null;
            this.ラベル39.Left = 5.336213F;
            this.ラベル39.Name = "ラベル39";
            this.ラベル39.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "center; ddo-char-set: 1";
            this.ラベル39.Tag = "";
            this.ラベル39.Text = "12";
            this.ラベル39.Top = 0.6354166F;
            this.ラベル39.Width = 0.1770833F;
            // 
            // ラベル40
            // 
            this.ラベル40.Height = 0.15625F;
            this.ラベル40.HyperLink = null;
            this.ラベル40.Left = 5.607046F;
            this.ラベル40.Name = "ラベル40";
            this.ラベル40.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "center; ddo-char-set: 1";
            this.ラベル40.Tag = "";
            this.ラベル40.Text = "13";
            this.ラベル40.Top = 0.6354166F;
            this.ラベル40.Width = 0.1770833F;
            // 
            // ラベル41
            // 
            this.ラベル41.Height = 0.15625F;
            this.ラベル41.HyperLink = null;
            this.ラベル41.Left = 5.872324F;
            this.ラベル41.Name = "ラベル41";
            this.ラベル41.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "center; ddo-char-set: 1";
            this.ラベル41.Tag = "";
            this.ラベル41.Text = "14";
            this.ラベル41.Top = 0.6354166F;
            this.ラベル41.Width = 0.1770833F;
            // 
            // ラベル42
            // 
            this.ラベル42.Height = 0.15625F;
            this.ラベル42.HyperLink = null;
            this.ラベル42.Left = 6.147324F;
            this.ラベル42.Name = "ラベル42";
            this.ラベル42.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "center; ddo-char-set: 1";
            this.ラベル42.Tag = "";
            this.ラベル42.Text = "15";
            this.ラベル42.Top = 0.6354166F;
            this.ラベル42.Width = 0.1770833F;
            // 
            // ラベル43
            // 
            this.ラベル43.Height = 0.15625F;
            this.ラベル43.HyperLink = null;
            this.ラベル43.Left = 6.419546F;
            this.ラベル43.Name = "ラベル43";
            this.ラベル43.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "left; text-decoration: underline; ddo-char-set: 1";
            this.ラベル43.Tag = "";
            this.ラベル43.Text = "31";
            this.ラベル43.Top = 0.8020834F;
            this.ラベル43.Width = 0.1770833F;
            // 
            // ラベル48
            // 
            this.ラベル48.Height = 0.15625F;
            this.ラベル48.HyperLink = null;
            this.ラベル48.Left = 2.398713F;
            this.ラベル48.Name = "ラベル48";
            this.ラベル48.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "center; ddo-char-set: 1";
            this.ラベル48.Tag = "";
            this.ラベル48.Text = "1";
            this.ラベル48.Top = 0.6354166F;
            this.ラベル48.Width = 0.1145833F;
            // 
            // ラベル50
            // 
            this.ラベル50.Height = 0.15625F;
            this.ラベル50.HyperLink = null;
            this.ラベル50.Left = 2.648713F;
            this.ラベル50.Name = "ラベル50";
            this.ラベル50.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "center; text-decoration: underline; ddo-char-set: 1";
            this.ラベル50.Tag = "";
            this.ラベル50.Text = "17";
            this.ラベル50.Top = 0.8041667F;
            this.ラベル50.Width = 0.1770833F;
            // 
            // ラベル51
            // 
            this.ラベル51.Height = 0.15625F;
            this.ラベル51.HyperLink = null;
            this.ラベル51.Left = 2.90913F;
            this.ラベル51.Name = "ラベル51";
            this.ラベル51.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "center; text-decoration: underline; ddo-char-set: 1";
            this.ラベル51.Tag = "";
            this.ラベル51.Text = "18";
            this.ラベル51.Top = 0.8041667F;
            this.ラベル51.Width = 0.1770833F;
            // 
            // ラベル52
            // 
            this.ラベル52.Height = 0.15625F;
            this.ラベル52.HyperLink = null;
            this.ラベル52.Left = 3.169546F;
            this.ラベル52.Name = "ラベル52";
            this.ラベル52.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "center; text-decoration: underline; ddo-char-set: 1";
            this.ラベル52.Tag = "";
            this.ラベル52.Text = "19";
            this.ラベル52.Top = 0.8041667F;
            this.ラベル52.Width = 0.1770833F;
            // 
            // ラベル53
            // 
            this.ラベル53.Height = 0.15625F;
            this.ラベル53.HyperLink = null;
            this.ラベル53.Left = 3.450796F;
            this.ラベル53.Name = "ラベル53";
            this.ラベル53.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "center; text-decoration: underline; ddo-char-set: 1";
            this.ラベル53.Tag = "";
            this.ラベル53.Text = "20";
            this.ラベル53.Top = 0.8041667F;
            this.ラベル53.Width = 0.1770833F;
            // 
            // ラベル54
            // 
            this.ラベル54.Height = 0.15625F;
            this.ラベル54.HyperLink = null;
            this.ラベル54.Left = 3.72163F;
            this.ラベル54.Name = "ラベル54";
            this.ラベル54.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "center; text-decoration: underline; ddo-char-set: 1";
            this.ラベル54.Tag = "";
            this.ラベル54.Text = "21";
            this.ラベル54.Top = 0.8041667F;
            this.ラベル54.Width = 0.1770833F;
            // 
            // ラベル55
            // 
            this.ラベル55.Height = 0.15625F;
            this.ラベル55.HyperLink = null;
            this.ラベル55.Left = 3.992463F;
            this.ラベル55.Name = "ラベル55";
            this.ラベル55.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "center; text-decoration: underline; ddo-char-set: 1";
            this.ラベル55.Tag = "";
            this.ラベル55.Text = "22";
            this.ラベル55.Top = 0.8041667F;
            this.ラベル55.Width = 0.1770833F;
            // 
            // ラベル56
            // 
            this.ラベル56.Height = 0.15625F;
            this.ラベル56.HyperLink = null;
            this.ラベル56.Left = 4.263296F;
            this.ラベル56.Name = "ラベル56";
            this.ラベル56.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "center; text-decoration: underline; ddo-char-set: 1";
            this.ラベル56.Tag = "";
            this.ラベル56.Text = "23";
            this.ラベル56.Top = 0.8041667F;
            this.ラベル56.Width = 0.1770833F;
            // 
            // ラベル57
            // 
            this.ラベル57.Height = 0.15625F;
            this.ラベル57.HyperLink = null;
            this.ラベル57.Left = 4.534129F;
            this.ラベル57.Name = "ラベル57";
            this.ラベル57.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "center; text-decoration: underline; ddo-char-set: 1";
            this.ラベル57.Tag = "";
            this.ラベル57.Text = "24";
            this.ラベル57.Top = 0.8041667F;
            this.ラベル57.Width = 0.1770833F;
            // 
            // ラベル58
            // 
            this.ラベル58.Height = 0.15625F;
            this.ラベル58.HyperLink = null;
            this.ラベル58.Left = 4.804963F;
            this.ラベル58.Name = "ラベル58";
            this.ラベル58.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "center; text-decoration: underline; ddo-char-set: 1";
            this.ラベル58.Tag = "";
            this.ラベル58.Text = "25";
            this.ラベル58.Top = 0.8041667F;
            this.ラベル58.Width = 0.1770833F;
            // 
            // ラベル59
            // 
            this.ラベル59.Height = 0.15625F;
            this.ラベル59.HyperLink = null;
            this.ラベル59.Left = 5.075796F;
            this.ラベル59.Name = "ラベル59";
            this.ラベル59.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "center; text-decoration: underline; ddo-char-set: 1";
            this.ラベル59.Tag = "";
            this.ラベル59.Text = "26";
            this.ラベル59.Top = 0.8041667F;
            this.ラベル59.Width = 0.1770833F;
            // 
            // ラベル60
            // 
            this.ラベル60.Height = 0.15625F;
            this.ラベル60.HyperLink = null;
            this.ラベル60.Left = 5.336213F;
            this.ラベル60.Name = "ラベル60";
            this.ラベル60.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "center; text-decoration: underline; ddo-char-set: 1";
            this.ラベル60.Tag = "";
            this.ラベル60.Text = "27";
            this.ラベル60.Top = 0.8041667F;
            this.ラベル60.Width = 0.1770833F;
            // 
            // ラベル61
            // 
            this.ラベル61.Height = 0.15625F;
            this.ラベル61.HyperLink = null;
            this.ラベル61.Left = 5.607046F;
            this.ラベル61.Name = "ラベル61";
            this.ラベル61.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "center; text-decoration: underline; ddo-char-set: 1";
            this.ラベル61.Tag = "";
            this.ラベル61.Text = "28";
            this.ラベル61.Top = 0.8041667F;
            this.ラベル61.Width = 0.1770833F;
            // 
            // ラベル62
            // 
            this.ラベル62.Height = 0.15625F;
            this.ラベル62.HyperLink = null;
            this.ラベル62.Left = 5.867463F;
            this.ラベル62.Name = "ラベル62";
            this.ラベル62.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "center; text-decoration: underline; ddo-char-set: 1";
            this.ラベル62.Tag = "";
            this.ラベル62.Text = "29";
            this.ラベル62.Top = 0.8041667F;
            this.ラベル62.Width = 0.1770833F;
            // 
            // ラベル63
            // 
            this.ラベル63.Height = 0.15625F;
            this.ラベル63.HyperLink = null;
            this.ラベル63.Left = 6.148713F;
            this.ラベル63.Name = "ラベル63";
            this.ラベル63.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "center; text-decoration: underline; ddo-char-set: 1";
            this.ラベル63.Tag = "";
            this.ラベル63.Text = "30";
            this.ラベル63.Top = 0.8041667F;
            this.ラベル63.Width = 0.1770833F;
            // 
            // ラベル64
            // 
            this.ラベル64.Height = 0.15625F;
            this.ラベル64.HyperLink = null;
            this.ラベル64.Left = 2.367463F;
            this.ラベル64.Name = "ラベル64";
            this.ラベル64.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "center; text-decoration: underline; ddo-char-set: 1";
            this.ラベル64.Tag = "";
            this.ラベル64.Text = "16";
            this.ラベル64.Top = 0.8041667F;
            this.ラベル64.Width = 0.1770833F;
            // 
            // ラベル65
            // 
            this.ラベル65.Height = 0.15625F;
            this.ラベル65.HyperLink = null;
            this.ラベル65.Left = 6.147324F;
            this.ラベル65.Name = "ラベル65";
            this.ラベル65.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "left; ddo-char-set: 1";
            this.ラベル65.Tag = "";
            this.ラベル65.Text = "＿";
            this.ラベル65.Top = 0.6354166F;
            this.ラベル65.Width = 0.1770833F;
            // 
            // ラベル67
            // 
            this.ラベル67.Height = 0.15625F;
            this.ラベル67.HyperLink = null;
            this.ラベル67.Left = 5.877879F;
            this.ラベル67.Name = "ラベル67";
            this.ラベル67.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "left; ddo-char-set: 1";
            this.ラベル67.Tag = "";
            this.ラベル67.Text = "＿";
            this.ラベル67.Top = 0.6354166F;
            this.ラベル67.Width = 0.1770833F;
            // 
            // ラベル68
            // 
            this.ラベル68.Height = 0.15625F;
            this.ラベル68.HyperLink = null;
            this.ラベル68.Left = 5.596629F;
            this.ラベル68.Name = "ラベル68";
            this.ラベル68.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "left; ddo-char-set: 1";
            this.ラベル68.Tag = "";
            this.ラベル68.Text = "＿";
            this.ラベル68.Top = 0.6354166F;
            this.ラベル68.Width = 0.1770833F;
            // 
            // ラベル69
            // 
            this.ラベル69.Height = 0.15625F;
            this.ラベル69.HyperLink = null;
            this.ラベル69.Left = 5.084824F;
            this.ラベル69.Name = "ラベル69";
            this.ラベル69.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "left; ddo-char-set: 1";
            this.ラベル69.Tag = "";
            this.ラベル69.Text = "＿";
            this.ラベル69.Top = 0.6354166F;
            this.ラベル69.Width = 0.1770833F;
            // 
            // ラベル70
            // 
            this.ラベル70.Height = 0.15625F;
            this.ラベル70.HyperLink = null;
            this.ラベル70.Left = 5.336213F;
            this.ラベル70.Name = "ラベル70";
            this.ラベル70.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "left; ddo-char-set: 1";
            this.ラベル70.Tag = "";
            this.ラベル70.Text = "＿";
            this.ラベル70.Top = 0.6354166F;
            this.ラベル70.Width = 0.1770833F;
            // 
            // ラベル71
            // 
            this.ラベル71.Height = 0.15625F;
            this.ラベル71.HyperLink = null;
            this.ラベル71.Left = 4.809129F;
            this.ラベル71.Name = "ラベル71";
            this.ラベル71.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "left; ddo-char-set: 1";
            this.ラベル71.Tag = "";
            this.ラベル71.Text = "＿";
            this.ラベル71.Top = 0.6354166F;
            this.ラベル71.Width = 0.1770833F;
            // 
            // ラベル72
            // 
            this.ラベル72.Height = 0.15625F;
            this.ラベル72.HyperLink = null;
            this.ラベル72.Left = 4.533435F;
            this.ラベル72.Name = "ラベル72";
            this.ラベル72.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "left; ddo-char-set: 1";
            this.ラベル72.Tag = "";
            this.ラベル72.Text = "＿";
            this.ラベル72.Top = 0.6354166F;
            this.ラベル72.Width = 0.1770833F;
            // 
            // ラベル73
            // 
            this.ラベル73.Height = 0.15625F;
            this.ラベル73.HyperLink = null;
            this.ラベル73.Left = 4.25774F;
            this.ラベル73.Name = "ラベル73";
            this.ラベル73.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "left; ddo-char-set: 1";
            this.ラベル73.Tag = "";
            this.ラベル73.Text = "＿";
            this.ラベル73.Top = 0.6354166F;
            this.ラベル73.Width = 0.1770833F;
            // 
            // ラベル74
            // 
            this.ラベル74.Height = 0.15625F;
            this.ラベル74.HyperLink = null;
            this.ラベル74.Left = 3.982046F;
            this.ラベル74.Name = "ラベル74";
            this.ラベル74.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "left; ddo-char-set: 1";
            this.ラベル74.Tag = "";
            this.ラベル74.Text = "＿";
            this.ラベル74.Top = 0.6354166F;
            this.ラベル74.Width = 0.1770833F;
            // 
            // ラベル75
            // 
            this.ラベル75.Height = 0.15625F;
            this.ラベル75.HyperLink = null;
            this.ラベル75.Left = 3.732046F;
            this.ラベル75.Name = "ラベル75";
            this.ラベル75.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "left; ddo-char-set: 1";
            this.ラベル75.Tag = "";
            this.ラベル75.Text = "＿";
            this.ラベル75.Top = 0.6354166F;
            this.ラベル75.Width = 0.1770833F;
            // 
            // ラベル76
            // 
            this.ラベル76.Height = 0.15625F;
            this.ラベル76.HyperLink = null;
            this.ラベル76.Left = 3.461213F;
            this.ラベル76.Name = "ラベル76";
            this.ラベル76.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "left; ddo-char-set: 1";
            this.ラベル76.Tag = "";
            this.ラベル76.Text = "＿";
            this.ラベル76.Top = 0.6354166F;
            this.ラベル76.Width = 0.1770833F;
            // 
            // ラベル77
            // 
            this.ラベル77.Height = 0.15625F;
            this.ラベル77.HyperLink = null;
            this.ラベル77.Left = 3.155657F;
            this.ラベル77.Name = "ラベル77";
            this.ラベル77.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "left; ddo-char-set: 1";
            this.ラベル77.Tag = "";
            this.ラベル77.Text = "＿";
            this.ラベル77.Top = 0.6354166F;
            this.ラベル77.Width = 0.1770833F;
            // 
            // ラベル78
            // 
            this.ラベル78.Height = 0.15625F;
            this.ラベル78.HyperLink = null;
            this.ラベル78.Left = 2.919546F;
            this.ラベル78.Name = "ラベル78";
            this.ラベル78.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "left; ddo-char-set: 1";
            this.ラベル78.Tag = "";
            this.ラベル78.Text = "＿";
            this.ラベル78.Top = 0.6354166F;
            this.ラベル78.Width = 0.1770833F;
            // 
            // ラベル79
            // 
            this.ラベル79.Height = 0.15625F;
            this.ラベル79.HyperLink = null;
            this.ラベル79.Left = 2.643852F;
            this.ラベル79.Name = "ラベル79";
            this.ラベル79.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "left; ddo-char-set: 1";
            this.ラベル79.Tag = "";
            this.ラベル79.Text = "＿";
            this.ラベル79.Top = 0.6354166F;
            this.ラベル79.Width = 0.1770833F;
            // 
            // ラベル80
            // 
            this.ラベル80.Height = 0.15625F;
            this.ラベル80.HyperLink = null;
            this.ラベル80.Left = 2.368157F;
            this.ラベル80.Name = "ラベル80";
            this.ラベル80.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "left; ddo-char-set: 1";
            this.ラベル80.Tag = "";
            this.ラベル80.Text = "＿";
            this.ラベル80.Top = 0.6354166F;
            this.ラベル80.Width = 0.1770833F;
            // 
            // ラベル138
            // 
            this.ラベル138.Height = 0.15625F;
            this.ラベル138.HyperLink = null;
            this.ラベル138.Left = 7.034129F;
            this.ラベル138.Name = "ラベル138";
            this.ラベル138.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "left; ddo-char-set: 1";
            this.ラベル138.Tag = "";
            this.ラベル138.Text = "4月　　5月　　6月　　7月　　8月　　9月　　10月　　11月　　12月　　1月　　2月　　3月　　";
            this.ラベル138.Top = 0.6354166F;
            this.ラベル138.Width = 5.75F;
            // 
            // ラベル151
            // 
            this.ラベル151.Height = 0.15625F;
            this.ラベル151.HyperLink = null;
            this.ラベル151.Left = 13.14961F;
            this.ラベル151.Name = "ラベル151";
            this.ラベル151.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "left; ddo-char-set: 1";
            this.ラベル151.Tag = "";
            this.ラベル151.Text = "計";
            this.ラベル151.Top = 0.6334646F;
            this.ラベル151.Width = 0.1770833F;
            // 
            // label2
            // 
            this.label2.Height = 0.15625F;
            this.label2.HyperLink = null;
            this.label2.Left = 0.2362205F;
            this.label2.Name = "label2";
            this.label2.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "left; ddo-char-set: 1";
            this.label2.Tag = "";
            this.label2.Text = "地区ｺｰﾄﾞ";
            this.label2.Top = 0.3149607F;
            this.label2.Width = 0.5511811F;
            // 
            // label3
            // 
            this.label3.Height = 0.15625F;
            this.label3.HyperLink = null;
            this.label3.Left = 0.944882F;
            this.label3.Name = "label3";
            this.label3.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "left; ddo-char-set: 1";
            this.label3.Tag = "";
            this.label3.Text = "地区名";
            this.label3.Top = 0.3149607F;
            this.label3.Width = 0.5520833F;
            // 
            // line2
            // 
            this.line2.Height = 0F;
            this.line2.Left = 0F;
            this.line2.LineWeight = 1F;
            this.line2.Name = "line2";
            this.line2.Tag = "";
            this.line2.Top = 0.944882F;
            this.line2.Width = 13.58472F;
            this.line2.X1 = 0F;
            this.line2.X2 = 13.58472F;
            this.line2.Y1 = 0.944882F;
            this.line2.Y2 = 0.944882F;
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.直線82,
            this.直線83,
            this.直線84,
            this.直線85,
            this.直線86,
            this.直線87,
            this.直線88,
            this.直線89,
            this.直線90,
            this.直線91,
            this.直線92,
            this.直線93,
            this.直線94,
            this.直線95,
            this.直線96,
            this.直線97,
            this.直線98,
            this.直線99,
            this.直線100,
            this.テキスト003,
            this.テキスト004,
            this.テキスト005,
            this.テキスト006,
            this.テキスト007,
            this.テキスト008,
            this.テキスト009,
            this.テキスト010,
            this.テキスト011,
            this.テキスト012,
            this.テキスト013,
            this.テキスト014,
            this.テキスト015,
            this.テキスト016,
            this.テキスト017,
            this.テキスト018,
            this.テキスト019,
            this.テキスト020,
            this.テキスト021,
            this.テキスト022,
            this.テキスト023,
            this.テキスト024,
            this.テキスト025,
            this.テキスト026,
            this.テキスト027,
            this.テキスト028,
            this.テキスト029,
            this.テキスト030,
            this.テキスト031,
            this.テキスト032,
            this.テキスト033,
            this.テキスト034,
            this.テキスト035,
            this.テキスト036,
            this.テキスト037,
            this.テキスト038,
            this.テキスト039,
            this.テキスト040,
            this.テキスト041,
            this.テキスト042,
            this.テキスト043,
            this.テキスト044,
            this.テキスト045,
            this.テキスト046,
            this.テキスト047,
            this.テキスト152});
            this.detail.Height = 0.3958333F;
            this.detail.Name = "detail";
            this.detail.Format += new System.EventHandler(this.detail_Format);
            // 
            // 直線82
            // 
            this.直線82.Height = 0F;
            this.直線82.Left = 2.439685F;
            this.直線82.LineWeight = 0F;
            this.直線82.Name = "直線82";
            this.直線82.Tag = "";
            this.直線82.Top = 0.3958333F;
            this.直線82.Width = 4.330556F;
            this.直線82.X1 = 2.439685F;
            this.直線82.X2 = 6.770241F;
            this.直線82.Y1 = 0.3958333F;
            this.直線82.Y2 = 0.3958333F;
            // 
            // 直線83
            // 
            this.直線83.Height = 0F;
            this.直線83.Left = 2.439685F;
            this.直線83.LineWeight = 0F;
            this.直線83.Name = "直線83";
            this.直線83.Tag = "";
            this.直線83.Top = 0F;
            this.直線83.Width = 4.330556F;
            this.直線83.X1 = 2.439685F;
            this.直線83.X2 = 6.770241F;
            this.直線83.Y1 = 0F;
            this.直線83.Y2 = 0F;
            // 
            // 直線84
            // 
            this.直線84.Height = 0F;
            this.直線84.Left = 2.406352F;
            this.直線84.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.直線84.LineWeight = 0F;
            this.直線84.Name = "直線84";
            this.直線84.Tag = "";
            this.直線84.Top = 0.1972222F;
            this.直線84.Width = 4.33125F;
            this.直線84.X1 = 2.406352F;
            this.直線84.X2 = 6.737602F;
            this.直線84.Y1 = 0.1972222F;
            this.直線84.Y2 = 0.1972222F;
            // 
            // 直線85
            // 
            this.直線85.Height = 0.3930556F;
            this.直線85.Left = 2.648018F;
            this.直線85.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.直線85.LineWeight = 0F;
            this.直線85.Name = "直線85";
            this.直線85.Tag = "";
            this.直線85.Top = 0F;
            this.直線85.Width = 0F;
            this.直線85.X1 = 2.648018F;
            this.直線85.X2 = 2.648018F;
            this.直線85.Y1 = 0F;
            this.直線85.Y2 = 0.3930556F;
            // 
            // 直線86
            // 
            this.直線86.Height = 0.3930556F;
            this.直線86.Left = 2.918852F;
            this.直線86.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.直線86.LineWeight = 0F;
            this.直線86.Name = "直線86";
            this.直線86.Tag = "";
            this.直線86.Top = 0F;
            this.直線86.Width = 0F;
            this.直線86.X1 = 2.918852F;
            this.直線86.X2 = 2.918852F;
            this.直線86.Y1 = 0F;
            this.直線86.Y2 = 0.3930556F;
            // 
            // 直線87
            // 
            this.直線87.Height = 0.3930556F;
            this.直線87.Left = 3.179268F;
            this.直線87.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.直線87.LineWeight = 0F;
            this.直線87.Name = "直線87";
            this.直線87.Tag = "";
            this.直線87.Top = 0F;
            this.直線87.Width = 0F;
            this.直線87.X1 = 3.179268F;
            this.直線87.X2 = 3.179268F;
            this.直線87.Y1 = 0F;
            this.直線87.Y2 = 0.3930556F;
            // 
            // 直線88
            // 
            this.直線88.Height = 0.3930556F;
            this.直線88.Left = 3.439685F;
            this.直線88.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.直線88.LineWeight = 0F;
            this.直線88.Name = "直線88";
            this.直線88.Tag = "";
            this.直線88.Top = 0F;
            this.直線88.Width = 0F;
            this.直線88.X1 = 3.439685F;
            this.直線88.X2 = 3.439685F;
            this.直線88.Y1 = 0F;
            this.直線88.Y2 = 0.3930556F;
            // 
            // 直線89
            // 
            this.直線89.Height = 0.3930556F;
            this.直線89.Left = 3.700102F;
            this.直線89.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.直線89.LineWeight = 0F;
            this.直線89.Name = "直線89";
            this.直線89.Tag = "";
            this.直線89.Top = 0F;
            this.直線89.Width = 0F;
            this.直線89.X1 = 3.700102F;
            this.直線89.X2 = 3.700102F;
            this.直線89.Y1 = 0F;
            this.直線89.Y2 = 0.3930556F;
            // 
            // 直線90
            // 
            this.直線90.Height = 0.3930556F;
            this.直線90.Left = 3.970935F;
            this.直線90.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.直線90.LineWeight = 0F;
            this.直線90.Name = "直線90";
            this.直線90.Tag = "";
            this.直線90.Top = 0F;
            this.直線90.Width = 0F;
            this.直線90.X1 = 3.970935F;
            this.直線90.X2 = 3.970935F;
            this.直線90.Y1 = 0F;
            this.直線90.Y2 = 0.3930556F;
            // 
            // 直線91
            // 
            this.直線91.Height = 0.3930556F;
            this.直線91.Left = 4.241768F;
            this.直線91.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.直線91.LineWeight = 0F;
            this.直線91.Name = "直線91";
            this.直線91.Tag = "";
            this.直線91.Top = 0F;
            this.直線91.Width = 0F;
            this.直線91.X1 = 4.241768F;
            this.直線91.X2 = 4.241768F;
            this.直線91.Y1 = 0F;
            this.直線91.Y2 = 0.3930556F;
            // 
            // 直線92
            // 
            this.直線92.Height = 0.3930556F;
            this.直線92.Left = 4.512602F;
            this.直線92.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.直線92.LineWeight = 0F;
            this.直線92.Name = "直線92";
            this.直線92.Tag = "";
            this.直線92.Top = 0F;
            this.直線92.Width = 0F;
            this.直線92.X1 = 4.512602F;
            this.直線92.X2 = 4.512602F;
            this.直線92.Y1 = 0F;
            this.直線92.Y2 = 0.3930556F;
            // 
            // 直線93
            // 
            this.直線93.Height = 0.3930556F;
            this.直線93.Left = 4.783435F;
            this.直線93.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.直線93.LineWeight = 0F;
            this.直線93.Name = "直線93";
            this.直線93.Tag = "";
            this.直線93.Top = 0F;
            this.直線93.Width = 0F;
            this.直線93.X1 = 4.783435F;
            this.直線93.X2 = 4.783435F;
            this.直線93.Y1 = 0F;
            this.直線93.Y2 = 0.3930556F;
            // 
            // 直線94
            // 
            this.直線94.Height = 0.3930556F;
            this.直線94.Left = 5.054268F;
            this.直線94.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.直線94.LineWeight = 0F;
            this.直線94.Name = "直線94";
            this.直線94.Tag = "";
            this.直線94.Top = 0F;
            this.直線94.Width = 0F;
            this.直線94.X1 = 5.054268F;
            this.直線94.X2 = 5.054268F;
            this.直線94.Y1 = 0F;
            this.直線94.Y2 = 0.3930556F;
            // 
            // 直線95
            // 
            this.直線95.Height = 0.3930556F;
            this.直線95.Left = 5.325102F;
            this.直線95.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.直線95.LineWeight = 0F;
            this.直線95.Name = "直線95";
            this.直線95.Tag = "";
            this.直線95.Top = 0F;
            this.直線95.Width = 0F;
            this.直線95.X1 = 5.325102F;
            this.直線95.X2 = 5.325102F;
            this.直線95.Y1 = 0F;
            this.直線95.Y2 = 0.3930556F;
            // 
            // 直線96
            // 
            this.直線96.Height = 0.3930556F;
            this.直線96.Left = 5.595935F;
            this.直線96.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.直線96.LineWeight = 0F;
            this.直線96.Name = "直線96";
            this.直線96.Tag = "";
            this.直線96.Top = 0F;
            this.直線96.Width = 0F;
            this.直線96.X1 = 5.595935F;
            this.直線96.X2 = 5.595935F;
            this.直線96.Y1 = 0F;
            this.直線96.Y2 = 0.3930556F;
            // 
            // 直線97
            // 
            this.直線97.Height = 0.3930556F;
            this.直線97.Left = 5.866768F;
            this.直線97.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.直線97.LineWeight = 0F;
            this.直線97.Name = "直線97";
            this.直線97.Tag = "";
            this.直線97.Top = 0F;
            this.直線97.Width = 0F;
            this.直線97.X1 = 5.866768F;
            this.直線97.X2 = 5.866768F;
            this.直線97.Y1 = 0F;
            this.直線97.Y2 = 0.3930556F;
            // 
            // 直線98
            // 
            this.直線98.Height = 0.3930556F;
            this.直線98.Left = 6.137602F;
            this.直線98.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.直線98.LineWeight = 0F;
            this.直線98.Name = "直線98";
            this.直線98.Tag = "";
            this.直線98.Top = 0F;
            this.直線98.Width = 0F;
            this.直線98.X1 = 6.137602F;
            this.直線98.X2 = 6.137602F;
            this.直線98.Y1 = 0F;
            this.直線98.Y2 = 0.3930556F;
            // 
            // 直線99
            // 
            this.直線99.Height = 0.3930556F;
            this.直線99.Left = 6.408435F;
            this.直線99.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.直線99.LineWeight = 0F;
            this.直線99.Name = "直線99";
            this.直線99.Tag = "";
            this.直線99.Top = 0F;
            this.直線99.Width = 0F;
            this.直線99.X1 = 6.408435F;
            this.直線99.X2 = 6.408435F;
            this.直線99.Y1 = 0F;
            this.直線99.Y2 = 0.3930556F;
            // 
            // 直線100
            // 
            this.直線100.Height = 0.3930556F;
            this.直線100.Left = 6.679268F;
            this.直線100.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.直線100.LineWeight = 0F;
            this.直線100.Name = "直線100";
            this.直線100.Tag = "";
            this.直線100.Top = 0F;
            this.直線100.Width = 0F;
            this.直線100.X1 = 6.679268F;
            this.直線100.X2 = 6.679268F;
            this.直線100.Y1 = 0F;
            this.直線100.Y2 = 0.3930556F;
            // 
            // テキスト003
            // 
            this.テキスト003.DataField = "ITEM03";
            this.テキスト003.Height = 0.15625F;
            this.テキスト003.Left = 0.3980184F;
            this.テキスト003.Name = "テキスト003";
            this.テキスト003.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト003.Tag = "";
            this.テキスト003.Text = "ITEM03";
            this.テキスト003.Top = 0.03888889F;
            this.テキスト003.Width = 0.4833333F;
            // 
            // テキスト004
            // 
            this.テキスト004.DataField = "ITEM04";
            this.テキスト004.Height = 0.15625F;
            this.テキスト004.Left = 0.8876017F;
            this.テキスト004.Name = "テキスト004";
            this.テキスト004.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: left; ddo-char-set: 1";
            this.テキスト004.Tag = "";
            this.テキスト004.Text = "ITEM04";
            this.テキスト004.Top = 0.03888889F;
            this.テキスト004.Width = 1.479721F;
            // 
            // テキスト005
            // 
            this.テキスト005.DataField = "ITEM05";
            this.テキスト005.Height = 0.15625F;
            this.テキスト005.Left = 2.429268F;
            this.テキスト005.Name = "テキスト005";
            this.テキスト005.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト005.Tag = "";
            this.テキスト005.Text = "ITEM05";
            this.テキスト005.Top = 0.02083333F;
            this.テキスト005.Width = 0.1916667F;
            // 
            // テキスト006
            // 
            this.テキスト006.DataField = "ITEM06";
            this.テキスト006.Height = 0.15625F;
            this.テキスト006.Left = 2.668852F;
            this.テキスト006.Name = "テキスト006";
            this.テキスト006.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト006.Tag = "";
            this.テキスト006.Text = "ITEM06";
            this.テキスト006.Top = 0.02083333F;
            this.テキスト006.Width = 0.2333333F;
            // 
            // テキスト007
            // 
            this.テキスト007.DataField = "ITEM07";
            this.テキスト007.Height = 0.15625F;
            this.テキスト007.Left = 2.960518F;
            this.テキスト007.Name = "テキスト007";
            this.テキスト007.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト007.Tag = "";
            this.テキスト007.Text = "ITEM07";
            this.テキスト007.Top = 0.02083333F;
            this.テキスト007.Width = 0.2020833F;
            // 
            // テキスト008
            // 
            this.テキスト008.DataField = "ITEM08";
            this.テキスト008.Height = 0.15625F;
            this.テキスト008.Left = 3.220935F;
            this.テキスト008.Name = "テキスト008";
            this.テキスト008.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト008.Tag = "";
            this.テキスト008.Text = "ITEM08";
            this.テキスト008.Top = 0.02083333F;
            this.テキスト008.Width = 0.1916667F;
            // 
            // テキスト009
            // 
            this.テキスト009.DataField = "ITEM09";
            this.テキスト009.Height = 0.15625F;
            this.テキスト009.Left = 3.460518F;
            this.テキスト009.Name = "テキスト009";
            this.テキスト009.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト009.Tag = "";
            this.テキスト009.Text = "ITEM09";
            this.テキスト009.Top = 0.02083333F;
            this.テキスト009.Width = 0.2333333F;
            // 
            // テキスト010
            // 
            this.テキスト010.DataField = "ITEM10";
            this.テキスト010.Height = 0.15625F;
            this.テキスト010.Left = 3.731352F;
            this.テキスト010.Name = "テキスト010";
            this.テキスト010.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト010.Tag = "";
            this.テキスト010.Text = "ITEM10";
            this.テキスト010.Top = 0.02083333F;
            this.テキスト010.Width = 0.2333333F;
            // 
            // テキスト011
            // 
            this.テキスト011.DataField = "ITEM11";
            this.テキスト011.Height = 0.15625F;
            this.テキスト011.Left = 3.991768F;
            this.テキスト011.Name = "テキスト011";
            this.テキスト011.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト011.Tag = "";
            this.テキスト011.Text = "ITEM11";
            this.テキスト011.Top = 0.02083333F;
            this.テキスト011.Width = 0.2333333F;
            // 
            // テキスト012
            // 
            this.テキスト012.DataField = "ITEM12";
            this.テキスト012.Height = 0.15625F;
            this.テキスト012.Left = 4.252185F;
            this.テキスト012.Name = "テキスト012";
            this.テキスト012.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト012.Tag = "";
            this.テキスト012.Text = "ITEM12";
            this.テキスト012.Top = 0.02083333F;
            this.テキスト012.Width = 0.24375F;
            // 
            // テキスト013
            // 
            this.テキスト013.DataField = "ITEM13";
            this.テキスト013.Height = 0.15625F;
            this.テキスト013.Left = 4.533435F;
            this.テキスト013.Name = "テキスト013";
            this.テキスト013.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト013.Tag = "";
            this.テキスト013.Text = "ITEM13";
            this.テキスト013.Top = 0.02083333F;
            this.テキスト013.Width = 0.2333333F;
            // 
            // テキスト014
            // 
            this.テキスト014.DataField = "ITEM14";
            this.テキスト014.Height = 0.15625F;
            this.テキスト014.Left = 4.804268F;
            this.テキスト014.Name = "テキスト014";
            this.テキスト014.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト014.Tag = "";
            this.テキスト014.Text = "ITEM14";
            this.テキスト014.Top = 0.02083333F;
            this.テキスト014.Width = 0.2333333F;
            // 
            // テキスト015
            // 
            this.テキスト015.DataField = "ITEM15";
            this.テキスト015.Height = 0.15625F;
            this.テキスト015.Left = 5.064685F;
            this.テキスト015.Name = "テキスト015";
            this.テキスト015.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト015.Tag = "";
            this.テキスト015.Text = "ITEM15";
            this.テキスト015.Top = 0.02083333F;
            this.テキスト015.Width = 0.24375F;
            // 
            // テキスト016
            // 
            this.テキスト016.DataField = "ITEM16";
            this.テキスト016.Height = 0.15625F;
            this.テキスト016.Left = 5.345935F;
            this.テキスト016.Name = "テキスト016";
            this.テキスト016.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト016.Tag = "";
            this.テキスト016.Text = "ITEM16";
            this.テキスト016.Top = 0.02083333F;
            this.テキスト016.Width = 0.2333333F;
            // 
            // テキスト017
            // 
            this.テキスト017.DataField = "ITEM17";
            this.テキスト017.Height = 0.15625F;
            this.テキスト017.Left = 5.627185F;
            this.テキスト017.Name = "テキスト017";
            this.テキスト017.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト017.Tag = "";
            this.テキスト017.Text = "ITEM17";
            this.テキスト017.Top = 0.02083333F;
            this.テキスト017.Width = 0.2333333F;
            // 
            // テキスト018
            // 
            this.テキスト018.DataField = "ITEM18";
            this.テキスト018.Height = 0.15625F;
            this.テキスト018.Left = 5.898018F;
            this.テキスト018.Name = "テキスト018";
            this.テキスト018.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト018.Tag = "";
            this.テキスト018.Text = "ITEM18";
            this.テキスト018.Top = 0.02083333F;
            this.テキスト018.Width = 0.2333333F;
            // 
            // テキスト019
            // 
            this.テキスト019.DataField = "ITEM19";
            this.テキスト019.Height = 0.15625F;
            this.テキスト019.Left = 6.168852F;
            this.テキスト019.Name = "テキスト019";
            this.テキスト019.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト019.Tag = "";
            this.テキスト019.Text = "ITEM19";
            this.テキスト019.Top = 0.02083333F;
            this.テキスト019.Width = 0.2333333F;
            // 
            // テキスト020
            // 
            this.テキスト020.DataField = "ITEM20";
            this.テキスト020.Height = 0.15625F;
            this.テキスト020.Left = 2.429268F;
            this.テキスト020.Name = "テキスト020";
            this.テキスト020.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト020.Tag = "";
            this.テキスト020.Text = "ITEM20";
            this.テキスト020.Top = 0.21875F;
            this.テキスト020.Width = 0.1916667F;
            // 
            // テキスト021
            // 
            this.テキスト021.DataField = "ITEM21";
            this.テキスト021.Height = 0.15625F;
            this.テキスト021.Left = 2.668852F;
            this.テキスト021.Name = "テキスト021";
            this.テキスト021.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト021.Tag = "";
            this.テキスト021.Text = "ITEM21";
            this.テキスト021.Top = 0.21875F;
            this.テキスト021.Width = 0.2333333F;
            // 
            // テキスト022
            // 
            this.テキスト022.DataField = "ITEM22";
            this.テキスト022.Height = 0.15625F;
            this.テキスト022.Left = 2.960518F;
            this.テキスト022.Name = "テキスト022";
            this.テキスト022.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト022.Tag = "";
            this.テキスト022.Text = "ITEM22";
            this.テキスト022.Top = 0.21875F;
            this.テキスト022.Width = 0.2020833F;
            // 
            // テキスト023
            // 
            this.テキスト023.DataField = "ITEM23";
            this.テキスト023.Height = 0.15625F;
            this.テキスト023.Left = 3.220935F;
            this.テキスト023.Name = "テキスト023";
            this.テキスト023.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト023.Tag = "";
            this.テキスト023.Text = "ITEM23";
            this.テキスト023.Top = 0.21875F;
            this.テキスト023.Width = 0.1916667F;
            // 
            // テキスト024
            // 
            this.テキスト024.DataField = "ITEM24";
            this.テキスト024.Height = 0.15625F;
            this.テキスト024.Left = 3.460518F;
            this.テキスト024.Name = "テキスト024";
            this.テキスト024.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト024.Tag = "";
            this.テキスト024.Text = "ITEM24";
            this.テキスト024.Top = 0.21875F;
            this.テキスト024.Width = 0.2333333F;
            // 
            // テキスト025
            // 
            this.テキスト025.DataField = "ITEM25";
            this.テキスト025.Height = 0.15625F;
            this.テキスト025.Left = 3.731352F;
            this.テキスト025.Name = "テキスト025";
            this.テキスト025.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト025.Tag = "";
            this.テキスト025.Text = "ITEM25";
            this.テキスト025.Top = 0.21875F;
            this.テキスト025.Width = 0.2333333F;
            // 
            // テキスト026
            // 
            this.テキスト026.DataField = "ITEM26";
            this.テキスト026.Height = 0.15625F;
            this.テキスト026.Left = 3.991768F;
            this.テキスト026.Name = "テキスト026";
            this.テキスト026.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト026.Tag = "";
            this.テキスト026.Text = "ITEM26";
            this.テキスト026.Top = 0.21875F;
            this.テキスト026.Width = 0.2333333F;
            // 
            // テキスト027
            // 
            this.テキスト027.DataField = "ITEM27";
            this.テキスト027.Height = 0.15625F;
            this.テキスト027.Left = 4.252185F;
            this.テキスト027.Name = "テキスト027";
            this.テキスト027.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト027.Tag = "";
            this.テキスト027.Text = "ITEM27";
            this.テキスト027.Top = 0.21875F;
            this.テキスト027.Width = 0.24375F;
            // 
            // テキスト028
            // 
            this.テキスト028.DataField = "ITEM28";
            this.テキスト028.Height = 0.15625F;
            this.テキスト028.Left = 4.533435F;
            this.テキスト028.Name = "テキスト028";
            this.テキスト028.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト028.Tag = "";
            this.テキスト028.Text = "ITEM28";
            this.テキスト028.Top = 0.21875F;
            this.テキスト028.Width = 0.2333333F;
            // 
            // テキスト029
            // 
            this.テキスト029.DataField = "ITEM29";
            this.テキスト029.Height = 0.15625F;
            this.テキスト029.Left = 4.804268F;
            this.テキスト029.Name = "テキスト029";
            this.テキスト029.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト029.Tag = "";
            this.テキスト029.Text = "ITEM29";
            this.テキスト029.Top = 0.21875F;
            this.テキスト029.Width = 0.2333333F;
            // 
            // テキスト030
            // 
            this.テキスト030.DataField = "ITEM30";
            this.テキスト030.Height = 0.15625F;
            this.テキスト030.Left = 5.064685F;
            this.テキスト030.Name = "テキスト030";
            this.テキスト030.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト030.Tag = "";
            this.テキスト030.Text = "ITEM30";
            this.テキスト030.Top = 0.21875F;
            this.テキスト030.Width = 0.24375F;
            // 
            // テキスト031
            // 
            this.テキスト031.DataField = "ITEM31";
            this.テキスト031.Height = 0.15625F;
            this.テキスト031.Left = 5.345935F;
            this.テキスト031.Name = "テキスト031";
            this.テキスト031.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト031.Tag = "";
            this.テキスト031.Text = "ITEM31";
            this.テキスト031.Top = 0.21875F;
            this.テキスト031.Width = 0.2333333F;
            // 
            // テキスト032
            // 
            this.テキスト032.DataField = "ITEM32";
            this.テキスト032.Height = 0.15625F;
            this.テキスト032.Left = 5.627185F;
            this.テキスト032.Name = "テキスト032";
            this.テキスト032.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト032.Tag = "";
            this.テキスト032.Text = "ITEM32";
            this.テキスト032.Top = 0.21875F;
            this.テキスト032.Width = 0.2333333F;
            // 
            // テキスト033
            // 
            this.テキスト033.DataField = "ITEM33";
            this.テキスト033.Height = 0.15625F;
            this.テキスト033.Left = 5.898018F;
            this.テキスト033.Name = "テキスト033";
            this.テキスト033.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト033.Tag = "";
            this.テキスト033.Text = "ITEM33";
            this.テキスト033.Top = 0.21875F;
            this.テキスト033.Width = 0.2333333F;
            // 
            // テキスト034
            // 
            this.テキスト034.DataField = "ITEM34";
            this.テキスト034.Height = 0.15625F;
            this.テキスト034.Left = 6.168852F;
            this.テキスト034.Name = "テキスト034";
            this.テキスト034.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト034.Tag = "";
            this.テキスト034.Text = "ITEM34";
            this.テキスト034.Top = 0.21875F;
            this.テキスト034.Width = 0.2333333F;
            // 
            // テキスト035
            // 
            this.テキスト035.DataField = "ITEM35";
            this.テキスト035.Height = 0.15625F;
            this.テキスト035.Left = 6.418852F;
            this.テキスト035.Name = "テキスト035";
            this.テキスト035.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト035.Tag = "";
            this.テキスト035.Text = "ITEM35";
            this.テキスト035.Top = 0.21875F;
            this.テキスト035.Width = 0.2333333F;
            // 
            // テキスト036
            // 
            this.テキスト036.DataField = "ITEM36";
            this.テキスト036.Height = 0.15625F;
            this.テキスト036.Left = 6.850394F;
            this.テキスト036.Name = "テキスト036";
            this.テキスト036.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト036.Tag = "";
            this.テキスト036.Text = "ITEM36";
            this.テキスト036.Top = 0.04173229F;
            this.テキスト036.Width = 0.30625F;
            // 
            // テキスト037
            // 
            this.テキスト037.DataField = "ITEM37";
            this.テキスト037.Height = 0.15625F;
            this.テキスト037.Left = 7.308728F;
            this.テキスト037.Name = "テキスト037";
            this.テキスト037.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト037.Tag = "";
            this.テキスト037.Text = "ITEM37";
            this.テキスト037.Top = 0.04173229F;
            this.テキスト037.Width = 0.30625F;
            // 
            // テキスト038
            // 
            this.テキスト038.DataField = "ITEM38";
            this.テキスト038.Height = 0.15625F;
            this.テキスト038.Left = 7.756644F;
            this.テキスト038.Name = "テキスト038";
            this.テキスト038.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト038.Tag = "";
            this.テキスト038.Text = "ITEM38";
            this.テキスト038.Top = 0.04173229F;
            this.テキスト038.Width = 0.30625F;
            // 
            // テキスト039
            // 
            this.テキスト039.DataField = "ITEM39";
            this.テキスト039.Height = 0.15625F;
            this.テキスト039.Left = 8.214977F;
            this.テキスト039.Name = "テキスト039";
            this.テキスト039.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト039.Tag = "";
            this.テキスト039.Text = "ITEM39";
            this.テキスト039.Top = 0.04173229F;
            this.テキスト039.Width = 0.30625F;
            // 
            // テキスト040
            // 
            this.テキスト040.DataField = "ITEM40";
            this.テキスト040.Height = 0.15625F;
            this.テキスト040.Left = 8.652477F;
            this.テキスト040.Name = "テキスト040";
            this.テキスト040.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト040.Tag = "";
            this.テキスト040.Text = "ITEM40";
            this.テキスト040.Top = 0.04173229F;
            this.テキスト040.Width = 0.30625F;
            // 
            // テキスト041
            // 
            this.テキスト041.DataField = "ITEM41";
            this.テキスト041.Height = 0.15625F;
            this.テキスト041.Left = 9.100394F;
            this.テキスト041.Name = "テキスト041";
            this.テキスト041.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト041.Tag = "";
            this.テキスト041.Text = "ITEM41";
            this.テキスト041.Top = 0.04173229F;
            this.テキスト041.Width = 0.30625F;
            // 
            // テキスト042
            // 
            this.テキスト042.DataField = "ITEM42";
            this.テキスト042.Height = 0.15625F;
            this.テキスト042.Left = 9.610811F;
            this.テキスト042.Name = "テキスト042";
            this.テキスト042.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト042.Tag = "";
            this.テキスト042.Text = "ITEM42";
            this.テキスト042.Top = 0.04173229F;
            this.テキスト042.Width = 0.30625F;
            // 
            // テキスト043
            // 
            this.テキスト043.DataField = "ITEM43";
            this.テキスト043.Height = 0.15625F;
            this.テキスト043.Left = 10.12123F;
            this.テキスト043.Name = "テキスト043";
            this.テキスト043.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト043.Tag = "";
            this.テキスト043.Text = "ITEM43";
            this.テキスト043.Top = 0.04173229F;
            this.テキスト043.Width = 0.30625F;
            // 
            // テキスト044
            // 
            this.テキスト044.DataField = "ITEM44";
            this.テキスト044.Height = 0.15625F;
            this.テキスト044.Left = 10.64206F;
            this.テキスト044.Name = "テキスト044";
            this.テキスト044.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト044.Tag = "";
            this.テキスト044.Text = "ITEM44";
            this.テキスト044.Top = 0.04173229F;
            this.テキスト044.Width = 0.30625F;
            // 
            // テキスト045
            // 
            this.テキスト045.DataField = "ITEM45";
            this.テキスト045.Height = 0.15625F;
            this.テキスト045.Left = 11.10039F;
            this.テキスト045.Name = "テキスト045";
            this.テキスト045.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト045.Tag = "";
            this.テキスト045.Text = "ITEM45";
            this.テキスト045.Top = 0.04173229F;
            this.テキスト045.Width = 0.30625F;
            // 
            // テキスト046
            // 
            this.テキスト046.DataField = "ITEM46";
            this.テキスト046.Height = 0.15625F;
            this.テキスト046.Left = 11.53789F;
            this.テキスト046.Name = "テキスト046";
            this.テキスト046.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト046.Tag = "";
            this.テキスト046.Text = "ITEM46";
            this.テキスト046.Top = 0.04173229F;
            this.テキスト046.Width = 0.30625F;
            // 
            // テキスト047
            // 
            this.テキスト047.DataField = "ITEM47";
            this.テキスト047.Height = 0.15625F;
            this.テキスト047.Left = 12.00664F;
            this.テキスト047.Name = "テキスト047";
            this.テキスト047.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト047.Tag = "";
            this.テキスト047.Text = "ITEM47";
            this.テキスト047.Top = 0.04173229F;
            this.テキスト047.Width = 0.30625F;
            // 
            // テキスト152
            // 
            this.テキスト152.Height = 0.15625F;
            this.テキスト152.Left = 12.94488F;
            this.テキスト152.Name = "テキスト152";
            this.テキスト152.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト152.Tag = "";
            this.テキスト152.Text = null;
            this.テキスト152.Top = 0.04173229F;
            this.テキスト152.Width = 0.4208333F;
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            // 
            // reportHeader1
            // 
            this.reportHeader1.Height = 0F;
            this.reportHeader1.Name = "reportHeader1";
            // 
            // reportFooter1
            // 
            this.reportFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.ラベル153,
            this.テキスト154,
            this.テキスト155,
            this.テキスト156,
            this.テキスト157,
            this.テキスト158,
            this.テキスト159,
            this.テキスト160,
            this.テキスト161,
            this.テキスト162,
            this.テキスト163,
            this.テキスト164,
            this.テキスト165,
            this.テキスト166,
            this.直線167});
            this.reportFooter1.Height = 0.28125F;
            this.reportFooter1.Name = "reportFooter1";
            this.reportFooter1.Format += new System.EventHandler(this.reportFooter1_Format);
            // 
            // ラベル153
            // 
            this.ラベル153.Height = 0.15625F;
            this.ラベル153.HyperLink = null;
            this.ラベル153.Left = 0.5104166F;
            this.ラベル153.Name = "ラベル153";
            this.ラベル153.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-weight: bold; text-align:" +
    " left; ddo-char-set: 1";
            this.ラベル153.Tag = "";
            this.ラベル153.Text = "総計";
            this.ラベル153.Top = 0.04166667F;
            this.ラベル153.Width = 0.4108433F;
            // 
            // テキスト154
            // 
            this.テキスト154.DataField = "ITEM36";
            this.テキスト154.Height = 0.15625F;
            this.テキスト154.Left = 6.850394F;
            this.テキスト154.Name = "テキスト154";
            this.テキスト154.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: right; ddo-char-set: 1";
            this.テキスト154.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト154.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト154.Tag = "";
            this.テキスト154.Text = null;
            this.テキスト154.Top = 0.04173229F;
            this.テキスト154.Width = 0.30625F;
            // 
            // テキスト155
            // 
            this.テキスト155.DataField = "ITEM37";
            this.テキスト155.Height = 0.15625F;
            this.テキスト155.Left = 7.308728F;
            this.テキスト155.Name = "テキスト155";
            this.テキスト155.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: right; ddo-char-set: 1";
            this.テキスト155.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト155.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト155.Tag = "";
            this.テキスト155.Text = null;
            this.テキスト155.Top = 0.04173229F;
            this.テキスト155.Width = 0.30625F;
            // 
            // テキスト156
            // 
            this.テキスト156.DataField = "ITEM38";
            this.テキスト156.Height = 0.15625F;
            this.テキスト156.Left = 7.756645F;
            this.テキスト156.Name = "テキスト156";
            this.テキスト156.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: right; ddo-char-set: 1";
            this.テキスト156.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト156.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト156.Tag = "";
            this.テキスト156.Text = null;
            this.テキスト156.Top = 0.04173229F;
            this.テキスト156.Width = 0.30625F;
            // 
            // テキスト157
            // 
            this.テキスト157.DataField = "ITEM39";
            this.テキスト157.Height = 0.15625F;
            this.テキスト157.Left = 8.214979F;
            this.テキスト157.Name = "テキスト157";
            this.テキスト157.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: right; ddo-char-set: 1";
            this.テキスト157.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト157.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト157.Tag = "";
            this.テキスト157.Text = null;
            this.テキスト157.Top = 0.04173229F;
            this.テキスト157.Width = 0.30625F;
            // 
            // テキスト158
            // 
            this.テキスト158.DataField = "ITEM40";
            this.テキスト158.Height = 0.15625F;
            this.テキスト158.Left = 8.652479F;
            this.テキスト158.Name = "テキスト158";
            this.テキスト158.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: right; ddo-char-set: 1";
            this.テキスト158.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト158.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト158.Tag = "";
            this.テキスト158.Text = null;
            this.テキスト158.Top = 0.04173229F;
            this.テキスト158.Width = 0.30625F;
            // 
            // テキスト159
            // 
            this.テキスト159.DataField = "ITEM41";
            this.テキスト159.Height = 0.15625F;
            this.テキスト159.Left = 9.100395F;
            this.テキスト159.Name = "テキスト159";
            this.テキスト159.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: right; ddo-char-set: 1";
            this.テキスト159.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト159.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト159.Tag = "";
            this.テキスト159.Text = null;
            this.テキスト159.Top = 0.04173229F;
            this.テキスト159.Width = 0.30625F;
            // 
            // テキスト160
            // 
            this.テキスト160.DataField = "ITEM42";
            this.テキスト160.Height = 0.15625F;
            this.テキスト160.Left = 9.610811F;
            this.テキスト160.Name = "テキスト160";
            this.テキスト160.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: right; ddo-char-set: 1";
            this.テキスト160.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト160.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト160.Tag = "";
            this.テキスト160.Text = null;
            this.テキスト160.Top = 0.04173229F;
            this.テキスト160.Width = 0.30625F;
            // 
            // テキスト161
            // 
            this.テキスト161.DataField = "ITEM43";
            this.テキスト161.Height = 0.15625F;
            this.テキスト161.Left = 10.12123F;
            this.テキスト161.Name = "テキスト161";
            this.テキスト161.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: right; ddo-char-set: 1";
            this.テキスト161.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト161.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト161.Tag = "";
            this.テキスト161.Text = null;
            this.テキスト161.Top = 0.04173229F;
            this.テキスト161.Width = 0.30625F;
            // 
            // テキスト162
            // 
            this.テキスト162.DataField = "ITEM44";
            this.テキスト162.Height = 0.15625F;
            this.テキスト162.Left = 10.64206F;
            this.テキスト162.Name = "テキスト162";
            this.テキスト162.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: right; ddo-char-set: 1";
            this.テキスト162.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト162.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト162.Tag = "";
            this.テキスト162.Text = null;
            this.テキスト162.Top = 0.04173229F;
            this.テキスト162.Width = 0.30625F;
            // 
            // テキスト163
            // 
            this.テキスト163.DataField = "ITEM45";
            this.テキスト163.Height = 0.15625F;
            this.テキスト163.Left = 11.1004F;
            this.テキスト163.Name = "テキスト163";
            this.テキスト163.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: right; ddo-char-set: 1";
            this.テキスト163.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト163.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト163.Tag = "";
            this.テキスト163.Text = null;
            this.テキスト163.Top = 0.04173229F;
            this.テキスト163.Width = 0.30625F;
            // 
            // テキスト164
            // 
            this.テキスト164.DataField = "ITEM46";
            this.テキスト164.Height = 0.15625F;
            this.テキスト164.Left = 11.5379F;
            this.テキスト164.Name = "テキスト164";
            this.テキスト164.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: right; ddo-char-set: 1";
            this.テキスト164.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト164.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト164.Tag = "";
            this.テキスト164.Text = null;
            this.テキスト164.Top = 0.04173229F;
            this.テキスト164.Width = 0.30625F;
            // 
            // テキスト165
            // 
            this.テキスト165.DataField = "ITEM47";
            this.テキスト165.Height = 0.15625F;
            this.テキスト165.Left = 12.00665F;
            this.テキスト165.Name = "テキスト165";
            this.テキスト165.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: right; ddo-char-set: 1";
            this.テキスト165.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト165.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト165.Tag = "";
            this.テキスト165.Text = null;
            this.テキスト165.Top = 0.04173229F;
            this.テキスト165.Width = 0.30625F;
            // 
            // テキスト166
            // 
            this.テキスト166.Height = 0.15625F;
            this.テキスト166.Left = 12.8748F;
            this.テキスト166.Name = "テキスト166";
            this.テキスト166.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: right; ddo-char-set: 1";
            this.テキスト166.Tag = "";
            this.テキスト166.Text = null;
            this.テキスト166.Top = 0.04173229F;
            this.テキスト166.Width = 0.5041667F;
            // 
            // 直線167
            // 
            this.直線167.Height = 0F;
            this.直線167.Left = 0F;
            this.直線167.LineWeight = 1F;
            this.直線167.Name = "直線167";
            this.直線167.Tag = "";
            this.直線167.Top = 0F;
            this.直線167.Width = 13.58472F;
            this.直線167.X1 = 0F;
            this.直線167.X2 = 13.58472F;
            this.直線167.Y1 = 0F;
            this.直線167.Y2 = 0F;
            // 
            // groupHeader1
            // 
            this.groupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.textBox1,
            this.textBox2});
            this.groupHeader1.DataField = "ITEM01";
            this.groupHeader1.Name = "groupHeader1";
            // 
            // textBox1
            // 
            this.textBox1.DataField = "ITEM01";
            this.textBox1.Height = 0.15625F;
            this.textBox1.Left = 0.3259843F;
            this.textBox1.Name = "textBox1";
            this.textBox1.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.5pt; fon" +
    "t-weight: normal; text-align: center; ddo-char-set: 1";
            this.textBox1.Tag = "";
            this.textBox1.Text = "ITEM01";
            this.textBox1.Top = 0F;
            this.textBox1.Width = 0.5339079F;
            // 
            // textBox2
            // 
            this.textBox2.DataField = "ITEM02";
            this.textBox2.Height = 0.15625F;
            this.textBox2.Left = 0.8876017F;
            this.textBox2.Name = "textBox2";
            this.textBox2.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.5pt; fon" +
    "t-weight: normal; text-align: left; ddo-char-set: 1";
            this.textBox2.Tag = "";
            this.textBox2.Text = "ITEM02";
            this.textBox2.Top = 0F;
            this.textBox2.Width = 1.479721F;
            // 
            // groupFooter1
            // 
            this.groupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.label1,
            this.textBox3,
            this.textBox4,
            this.textBox5,
            this.textBox6,
            this.textBox7,
            this.textBox8,
            this.textBox9,
            this.textBox10,
            this.textBox11,
            this.textBox12,
            this.textBox13,
            this.textBox14,
            this.textBox15,
            this.line1});
            this.groupFooter1.Name = "groupFooter1";
            this.groupFooter1.Format += new System.EventHandler(this.groupFooter1_Format);
            // 
            // label1
            // 
            this.label1.Height = 0.15625F;
            this.label1.HyperLink = null;
            this.label1.Left = 0.2149606F;
            this.label1.Name = "label1";
            this.label1.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-weight: bold; text-align:" +
    " left; ddo-char-set: 1";
            this.label1.Tag = "";
            this.label1.Text = "＊＊地区計＊＊";
            this.label1.Top = 0.04173228F;
            this.label1.Width = 1.173147F;
            // 
            // textBox3
            // 
            this.textBox3.DataField = "ITEM36";
            this.textBox3.Height = 0.15625F;
            this.textBox3.Left = 6.850394F;
            this.textBox3.Name = "textBox3";
            this.textBox3.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: right; ddo-char-set: 1";
            this.textBox3.SummaryGroup = "groupHeader1";
            this.textBox3.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox3.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox3.Tag = "";
            this.textBox3.Text = null;
            this.textBox3.Top = 0.04173229F;
            this.textBox3.Width = 0.30625F;
            // 
            // textBox4
            // 
            this.textBox4.DataField = "ITEM37";
            this.textBox4.Height = 0.15625F;
            this.textBox4.Left = 7.308728F;
            this.textBox4.Name = "textBox4";
            this.textBox4.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: right; ddo-char-set: 1";
            this.textBox4.SummaryGroup = "groupHeader1";
            this.textBox4.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox4.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox4.Tag = "";
            this.textBox4.Text = null;
            this.textBox4.Top = 0.04173229F;
            this.textBox4.Width = 0.30625F;
            // 
            // textBox5
            // 
            this.textBox5.DataField = "ITEM38";
            this.textBox5.Height = 0.15625F;
            this.textBox5.Left = 7.756644F;
            this.textBox5.Name = "textBox5";
            this.textBox5.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: right; ddo-char-set: 1";
            this.textBox5.SummaryGroup = "groupHeader1";
            this.textBox5.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox5.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox5.Tag = "";
            this.textBox5.Text = null;
            this.textBox5.Top = 0.04173229F;
            this.textBox5.Width = 0.30625F;
            // 
            // textBox6
            // 
            this.textBox6.DataField = "ITEM39";
            this.textBox6.Height = 0.15625F;
            this.textBox6.Left = 8.214977F;
            this.textBox6.Name = "textBox6";
            this.textBox6.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: right; ddo-char-set: 1";
            this.textBox6.SummaryGroup = "groupHeader1";
            this.textBox6.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox6.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox6.Tag = "";
            this.textBox6.Text = null;
            this.textBox6.Top = 0.04173229F;
            this.textBox6.Width = 0.30625F;
            // 
            // textBox7
            // 
            this.textBox7.DataField = "ITEM40";
            this.textBox7.Height = 0.15625F;
            this.textBox7.Left = 8.652477F;
            this.textBox7.Name = "textBox7";
            this.textBox7.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: right; ddo-char-set: 1";
            this.textBox7.SummaryGroup = "groupHeader1";
            this.textBox7.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox7.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox7.Tag = "";
            this.textBox7.Text = null;
            this.textBox7.Top = 0.04173229F;
            this.textBox7.Width = 0.30625F;
            // 
            // textBox8
            // 
            this.textBox8.DataField = "ITEM41";
            this.textBox8.Height = 0.15625F;
            this.textBox8.Left = 9.100393F;
            this.textBox8.Name = "textBox8";
            this.textBox8.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: right; ddo-char-set: 1";
            this.textBox8.SummaryGroup = "groupHeader1";
            this.textBox8.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox8.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox8.Tag = "";
            this.textBox8.Text = null;
            this.textBox8.Top = 0.04173229F;
            this.textBox8.Width = 0.30625F;
            // 
            // textBox9
            // 
            this.textBox9.DataField = "ITEM42";
            this.textBox9.Height = 0.15625F;
            this.textBox9.Left = 9.610809F;
            this.textBox9.Name = "textBox9";
            this.textBox9.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: right; ddo-char-set: 1";
            this.textBox9.SummaryGroup = "groupHeader1";
            this.textBox9.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox9.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox9.Tag = "";
            this.textBox9.Text = null;
            this.textBox9.Top = 0.04173229F;
            this.textBox9.Width = 0.30625F;
            // 
            // textBox10
            // 
            this.textBox10.DataField = "ITEM43";
            this.textBox10.Height = 0.15625F;
            this.textBox10.Left = 10.12123F;
            this.textBox10.Name = "textBox10";
            this.textBox10.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: right; ddo-char-set: 1";
            this.textBox10.SummaryGroup = "groupHeader1";
            this.textBox10.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox10.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox10.Tag = "";
            this.textBox10.Text = null;
            this.textBox10.Top = 0.04173229F;
            this.textBox10.Width = 0.30625F;
            // 
            // textBox11
            // 
            this.textBox11.DataField = "ITEM44";
            this.textBox11.Height = 0.15625F;
            this.textBox11.Left = 10.64206F;
            this.textBox11.Name = "textBox11";
            this.textBox11.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: right; ddo-char-set: 1";
            this.textBox11.SummaryGroup = "groupHeader1";
            this.textBox11.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox11.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox11.Tag = "";
            this.textBox11.Text = null;
            this.textBox11.Top = 0.04173229F;
            this.textBox11.Width = 0.30625F;
            // 
            // textBox12
            // 
            this.textBox12.DataField = "ITEM45";
            this.textBox12.Height = 0.15625F;
            this.textBox12.Left = 11.10039F;
            this.textBox12.Name = "textBox12";
            this.textBox12.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: right; ddo-char-set: 1";
            this.textBox12.SummaryGroup = "groupHeader1";
            this.textBox12.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox12.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox12.Tag = "";
            this.textBox12.Text = null;
            this.textBox12.Top = 0.04173229F;
            this.textBox12.Width = 0.30625F;
            // 
            // textBox13
            // 
            this.textBox13.DataField = "ITEM46";
            this.textBox13.Height = 0.15625F;
            this.textBox13.Left = 11.53789F;
            this.textBox13.Name = "textBox13";
            this.textBox13.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: right; ddo-char-set: 1";
            this.textBox13.SummaryGroup = "groupHeader1";
            this.textBox13.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox13.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox13.Tag = "";
            this.textBox13.Text = null;
            this.textBox13.Top = 0.04173229F;
            this.textBox13.Width = 0.30625F;
            // 
            // textBox14
            // 
            this.textBox14.DataField = "ITEM47";
            this.textBox14.Height = 0.15625F;
            this.textBox14.Left = 12.00664F;
            this.textBox14.Name = "textBox14";
            this.textBox14.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: right; ddo-char-set: 1";
            this.textBox14.SummaryGroup = "groupHeader1";
            this.textBox14.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox14.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox14.Tag = "";
            this.textBox14.Text = null;
            this.textBox14.Top = 0.04173229F;
            this.textBox14.Width = 0.30625F;
            // 
            // textBox15
            // 
            this.textBox15.Height = 0.15625F;
            this.textBox15.Left = 12.8748F;
            this.textBox15.Name = "textBox15";
            this.textBox15.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: right; ddo-char-set: 1";
            this.textBox15.Tag = "";
            this.textBox15.Text = null;
            this.textBox15.Top = 0.04173229F;
            this.textBox15.Width = 0.5041667F;
            // 
            // line1
            // 
            this.line1.Height = 0F;
            this.line1.Left = 1.490116E-08F;
            this.line1.LineWeight = 1F;
            this.line1.Name = "line1";
            this.line1.Tag = "";
            this.line1.Top = 0F;
            this.line1.Width = 13.58472F;
            this.line1.X1 = 1.490116E-08F;
            this.line1.X2 = 13.58472F;
            this.line1.Y1 = 0F;
            this.line1.Y2 = 0F;
            // 
            // HANR4071R
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.5905512F;
            this.PageSettings.Margins.Left = 0.1968504F;
            this.PageSettings.Margins.Right = 0.1968504F;
            this.PageSettings.Margins.Top = 0.7874016F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
            this.PageSettings.PaperHeight = 13.89764F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.B4;
            this.PageSettings.PaperWidth = 9.84252F;
            this.PrintWidth = 13.93701F;
            this.Sections.Add(this.reportHeader1);
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.groupHeader1);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.groupFooter1);
            this.Sections.Add(this.pageFooter);
            this.Sections.Add(this.reportFooter1);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            this.ReportStart += new System.EventHandler(this.HANR4071R_ReportStart);
            ((System.ComponentModel.ISupportInitialize)(this.テキスト001)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtToday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル54)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル55)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル56)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル57)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル58)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル59)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル60)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル61)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル62)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル63)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル64)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル65)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル67)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル68)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル69)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル70)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル71)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル72)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル73)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル74)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル75)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル76)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル77)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル78)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル79)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル80)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル138)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル151)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト003)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト004)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト005)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト006)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト007)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト008)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト009)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト010)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト011)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト012)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト013)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト014)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト015)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト016)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト017)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト018)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト019)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト020)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト021)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト022)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト023)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト024)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト025)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト026)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト027)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト028)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト029)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト030)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト031)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト032)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト033)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト034)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト035)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト036)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト037)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト038)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト039)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト040)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト041)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト042)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト043)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト044)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト045)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト046)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト047)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト152)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル153)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト154)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト155)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト156)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト157)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト158)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト159)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト160)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト161)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト162)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト163)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト164)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト165)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト166)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.ReportHeader reportHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.ReportFooter reportFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト001;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル2;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト6;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル8;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル9;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル29;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル30;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル31;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル32;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル33;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル34;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル35;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル36;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル37;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル38;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル39;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル40;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル41;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル42;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル43;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル48;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル50;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル51;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル52;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル53;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル54;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル55;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル56;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル57;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル58;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル59;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル60;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル61;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル62;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル63;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル64;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル65;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル67;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル68;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル69;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル70;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル71;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル72;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル73;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル74;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル75;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル76;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル77;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル78;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル79;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル80;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル138;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル151;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線82;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線83;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線84;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線85;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線86;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線87;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線88;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線89;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線90;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線91;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線92;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線93;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線94;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線95;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線96;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線97;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線98;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線99;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線100;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト003;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト004;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト005;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト006;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト007;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト008;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト009;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト010;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト011;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト012;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト013;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト014;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト015;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト016;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト017;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト018;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト019;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト020;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト021;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト022;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト023;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト024;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト025;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト026;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト027;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト028;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト029;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト030;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト031;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト032;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト033;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト034;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト035;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト036;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト037;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト038;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト039;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト040;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト041;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト042;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト043;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト044;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト045;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト046;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト047;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト152;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル153;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト154;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト155;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト156;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト157;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト158;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト159;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト160;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト161;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト162;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト163;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト164;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト165;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト166;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線167;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader groupHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox2;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter groupFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox13;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox14;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox15;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtToday;
        private GrapeCity.ActiveReports.SectionReportModel.Label label2;
        private GrapeCity.ActiveReports.SectionReportModel.Label label3;
        private GrapeCity.ActiveReports.SectionReportModel.Line line2;
    }
}
