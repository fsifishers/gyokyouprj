﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.han.hanc9041
{
    /// <summary>
    /// 漁法の登録(HANC9042)
    /// </summary>
    public partial class HANC9042 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// モード(新規)
        /// </summary>
        private const string MODE_NEW = "1";

        /// <summary>
        /// モード(編集)
        /// </summary>
        private const string MODE_EDIT = "2";
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HANC9042()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="par1">引数1</param>
        public HANC9042(string par1)
            : base(par1)
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 引数：Par1／モード(1:新規、2:変更)、InData：地区コード
            if (MODE_NEW.Equals(this.Par1))
            {
                // 新規モードの初期表示
                InitDispOnNew();
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // 編集モードの初期表示
                InitDispOnEdit();
            }
            else
            {
                // 不正な起動として閉じる
                Msg.Error("不正な起動です。終了します。");
                this.Close();
            }

            // タイトルは非表示
            this.lblTitle.Visible = false;
            // EscapeとF1のみ表示
            this.ShowFButton = true;
            this.btnF6.Location = this.btnF3.Location;
            this.btnF3.Location = this.btnF2.Location;
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF2.Visible = false;
            this.btnF3.Visible = true;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF6.Visible = true;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F3キー押下時処理
        /// </summary>
        public override void PressF3()
        {
            // 新規モード時
            if (MODE_NEW.Equals(this.Par1))
            {
                return;
            }

            if (Msg.ConfYesNo("削除しますか？") == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 入力値をバインドパラメータとしてセットする
            ArrayList delParams = SetDelParams();

            try
            {
                this.Dba.BeginTransaction();

                // データ削除
                // 漁法マスタ
                this.Dba.Delete("TM_HN_GYOHO_MST",
                    "KAISHA_CD = @KAISHA_CD AND GYOHO_CD = @GYOHO_CD",
                    (DbParamCollection)delParams[0]);

                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF6();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF6()
        {
            // 確認メッセージを表示
            string msg = (MODE_NEW.Equals(this.Par1) ? "登録" : "更新") + "しますか？";
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            // 入力値をバインドパラメータとしてセットする
            ArrayList alParamshnGyoho = SetHnGyohoParams();

            try
            {
                if (MODE_NEW.Equals(this.Par1))
                {
                    // データ登録
                    // 共通.漁法マスタ
                    this.Dba.Insert("TM_HN_GYOHO_MST", (DbParamCollection)alParamshnGyoho[0]);
                }
                else if (MODE_EDIT.Equals(this.Par1))
                {
                    // データ更新
                    // 共通.漁法マスタ
                    this.Dba.Update("TM_HN_GYOHO_MST",
                        (DbParamCollection)alParamshnGyoho[1],
                        "KAISHA_CD = @KAISHA_CD AND GYOHO_CD = @GYOHO_CD",
                        (DbParamCollection)alParamshnGyoho[0]);
                }

                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 漁法コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGyohoCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidGyohoCd())
            {
                e.Cancel = true;
                this.txtGyohoCd.SelectAll();
            }
        }

        /// <summary>
        /// 漁法名の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGyohoNm_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidGyohoNm())
            {
                e.Cancel = true;
                this.txtGyohoNm.SelectAll();
            }
        }

        /// <summary>
        /// 漁法カナ名の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGyohoKana_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidGyohoKana())
            {
                e.Cancel = true;
                this.txtGyohoKana.SelectAll();
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 新規モードの初期表示
        /// </summary>
        private void InitDispOnNew()
        {
            // 初期値、入力制御を実装

            // 地区コードの初期値を取得
            // 地区の中でのMAX+1を初期表示する
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            StringBuilder where = new StringBuilder();
            DataTable dtMaxGyoho =
                this.Dba.GetDataTableByConditionWithParams("MAX(GYOHO_CD) AS MAX_CD",
                    "TM_HN_GYOHO_MST", Util.ToString(where), dpc);
            if (dtMaxGyoho.Rows.Count > 0 && !ValChk.IsEmpty(dtMaxGyoho.Rows[0]["MAX_CD"]))
            {
                this.txtGyohoCd.Text = Util.ToString(Util.ToInt(dtMaxGyoho.Rows[0]["MAX_CD"]) + 1);
            }
            else
            {
                this.txtGyohoCd.Text = "1";
            }

            // 漁法名に初期フォーカス
            this.ActiveControl = this.txtGyohoNm;
            this.txtGyohoNm.Focus();

            // 削除ボタン非表示
            this.btnF3.Enabled = false;
        }

        /// <summary>
        /// 編集モードの初期表示
        /// </summary>
        private void InitDispOnEdit()
        {
            // 現在DBに登録されている値、入力制御を実装
            StringBuilder cols = new StringBuilder();
            cols.Append("A.KAISHA_CD");
            cols.Append(" ,A.GYOHO_CD");
            cols.Append(" ,A.GYOHO_NM");
            cols.Append(" ,A.GYOHO_KANA");
            cols.Append(" ,A.REGIST_DATE");
            cols.Append(" ,A.UPDATE_DATE");
            cols.Append(" ,A.SHORI_FLG");

            StringBuilder from = new StringBuilder();
            from.Append("TM_HN_GYOHO_MST AS A");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@GYOHO_CD", SqlDbType.Decimal, 4, Util.ToString(this.InData));

            DataTable dtDispData =
                this.Dba.GetDataTableByConditionWithParams(
                    Util.ToString(cols), Util.ToString(from),
                    "A.KAISHA_CD = @KAISHA_CD AND A.GYOHO_CD = @GYOHO_CD",
                    dpc);

            if (dtDispData.Rows.Count == 0)
            {
                Msg.Error("不正な起動です。終了します。");
                this.Close();
            }

            // 取得した内容を表示
            DataRow drDispData = dtDispData.Rows[0];
            this.txtGyohoCd.Text = Util.ToString(drDispData["GYOHO_CD"]);
            this.txtGyohoNm.Text = Util.ToString(drDispData["GYOHO_NM"]);
            this.txtGyohoKana.Text = Util.ToString(drDispData["GYOHO_KANA"]);

            // 漁法コードは入力不可
            this.lbGyohoCd.Enabled = false;
            this.txtGyohoCd.Enabled = false;
        }

        /// <summary>
        /// 漁法コードの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidGyohoCd()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtGyohoCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 既に存在するコードを入力した場合はエラーとする
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@GYOHO_CD", SqlDbType.Decimal, 4, this.txtGyohoCd.Text);
            StringBuilder where = new StringBuilder("KAISHA_CD = @KAISHA_CD");
            where.Append(" AND GYOHO_CD = @GYOHO_CD");
            DataTable dtGyoho =
                this.Dba.GetDataTableByConditionWithParams("GYOHO_CD",
                    "TM_HN_GYOHO_MST", Util.ToString(where), dpc);
            if (dtGyoho.Rows.Count > 0)
            {
                Msg.Error("既に存在する漁法コードと重複しています。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 漁法名の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidGyohoNm()
        {
            // 40バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtGyohoNm.Text, this.txtGyohoNm.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 漁法カナ名の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidGyohoKana()
        {
            // 20バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtGyohoKana.Text, this.txtGyohoKana.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            if (MODE_NEW.Equals(this.Par1))
            {
                // 漁法コードのチェック
                if (!IsValidGyohoCd())
                {
                    this.txtGyohoCd.Focus();
                    return false;
                }
            }

            // 漁法名のチェック
            if (!IsValidGyohoNm())
            {
                this.txtGyohoNm.Focus();
                return false;
            }
            
            // 漁法カナ名のチェック
            if (!IsValidGyohoKana())
            {
                this.txtGyohoKana.Focus();
                return false;
            }

            return true;
        }

        /// <summary>
        /// TM_HN_GYOHO_MSTに更新するためのパラメータ設定をします。
        /// </summary>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// 更新処理：DbParamCollection*2(Where句,Set句)
        /// </returns>
        private ArrayList SetHnGyohoParams()
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            if (MODE_NEW.Equals(this.Par1))
            {
                // 会社コードと漁法コードを更新パラメータに設定
                updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                updParam.SetParam("@GYOHO_CD", SqlDbType.Decimal, 4, this.txtGyohoCd.Text);
                // 登録日
                updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWDATE");
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // 会社コードと漁法コードをWhere句のパラメータに設定
                DbParamCollection whereParam = new DbParamCollection();
                whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                whereParam.SetParam("@GYOHO_CD", SqlDbType.Decimal, 4, this.txtGyohoCd.Text);
                alParams.Add(whereParam);
            }

            // 漁法名
            updParam.SetParam("@GYOHO_NM", SqlDbType.VarChar, 40, this.txtGyohoNm.Text);
            // 漁法カナ名
            updParam.SetParam("@GYOHO_KANA", SqlDbType.VarChar, 40, this.txtGyohoKana.Text);
            // 更新日
            updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWDATE");
            // 処理フラグ
            updParam.SetParam("@SHORI_FLG", SqlDbType.VarChar, 3, "1");

            alParams.Add(updParam);

            return alParams;
        }

        /// <summary>
        /// TM_HN_GYOHO_MSTからデータを削除
        /// </summary>
        /// <returns>
        /// </returns>
        private ArrayList SetDelParams()
        {
            // 会社コードと漁法コードを削除パラメータに設定
            ArrayList alParams = new ArrayList();
            DbParamCollection dpcDenpyo = new DbParamCollection();
            dpcDenpyo.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpcDenpyo.SetParam("@GYOHO_CD", SqlDbType.VarChar, 6, this.txtGyohoCd.Text);

            alParams.Add(dpcDenpyo);

            return alParams;
        }
        #endregion
    }
}



