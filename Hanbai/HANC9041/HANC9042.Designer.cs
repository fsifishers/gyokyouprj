﻿namespace jp.co.fsi.han.hanc9041
{
    partial class HANC9042
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtGyohoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lbGyohoCd = new System.Windows.Forms.Label();
            this.txtGyohoNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.lbGyohoKana = new System.Windows.Forms.Label();
            this.txtGyohoKana = new jp.co.fsi.common.controls.FsiTextBox();
            this.lbGyohoNM = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Size = new System.Drawing.Size(388, 23);
            this.lblTitle.TabIndex = 999;
            this.lblTitle.Text = "漁法マスタ登録";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(5, 66);
            this.pnlDebug.Size = new System.Drawing.Size(421, 100);
            // 
            // txtGyohoCd
            // 
            this.txtGyohoCd.AutoSizeFromLength = true;
            this.txtGyohoCd.DisplayLength = null;
            this.txtGyohoCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtGyohoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtGyohoCd.Location = new System.Drawing.Point(127, 13);
            this.txtGyohoCd.MaxLength = 4;
            this.txtGyohoCd.Name = "txtGyohoCd";
            this.txtGyohoCd.Size = new System.Drawing.Size(34, 20);
            this.txtGyohoCd.TabIndex = 1;
            this.txtGyohoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGyohoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtGyohoCd_Validating);
            // 
            // lbGyohoCd
            // 
            this.lbGyohoCd.BackColor = System.Drawing.Color.Silver;
            this.lbGyohoCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbGyohoCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lbGyohoCd.Location = new System.Drawing.Point(12, 13);
            this.lbGyohoCd.Name = "lbGyohoCd";
            this.lbGyohoCd.Size = new System.Drawing.Size(115, 20);
            this.lbGyohoCd.TabIndex = 0;
            this.lbGyohoCd.Text = "漁法コード";
            this.lbGyohoCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtGyohoNm
            // 
            this.txtGyohoNm.AutoSizeFromLength = false;
            this.txtGyohoNm.DisplayLength = null;
            this.txtGyohoNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtGyohoNm.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtGyohoNm.Location = new System.Drawing.Point(127, 45);
            this.txtGyohoNm.MaxLength = 40;
            this.txtGyohoNm.Name = "txtGyohoNm";
            this.txtGyohoNm.Size = new System.Drawing.Size(276, 20);
            this.txtGyohoNm.TabIndex = 3;
            this.txtGyohoNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtGyohoNm_Validating);
            // 
            // lbGyohoKana
            // 
            this.lbGyohoKana.BackColor = System.Drawing.Color.Silver;
            this.lbGyohoKana.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbGyohoKana.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lbGyohoKana.Location = new System.Drawing.Point(12, 83);
            this.lbGyohoKana.Name = "lbGyohoKana";
            this.lbGyohoKana.Size = new System.Drawing.Size(115, 20);
            this.lbGyohoKana.TabIndex = 4;
            this.lbGyohoKana.Text = "漁法カナ名";
            this.lbGyohoKana.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtGyohoKana
            // 
            this.txtGyohoKana.AutoSizeFromLength = false;
            this.txtGyohoKana.DisplayLength = null;
            this.txtGyohoKana.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtGyohoKana.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtGyohoKana.Location = new System.Drawing.Point(127, 83);
            this.txtGyohoKana.MaxLength = 20;
            this.txtGyohoKana.Name = "txtGyohoKana";
            this.txtGyohoKana.Size = new System.Drawing.Size(276, 20);
            this.txtGyohoKana.TabIndex = 5;
            // 
            // lbGyohoNM
            // 
            this.lbGyohoNM.BackColor = System.Drawing.Color.Silver;
            this.lbGyohoNM.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lbGyohoNM.Location = new System.Drawing.Point(12, 45);
            this.lbGyohoNM.Name = "lbGyohoNM";
            this.lbGyohoNM.Size = new System.Drawing.Size(115, 20);
            this.lbGyohoNM.TabIndex = 2;
            this.lbGyohoNM.Text = "漁法名";
            this.lbGyohoNM.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // HANC9042
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(413, 169);
            this.Controls.Add(this.txtGyohoNm);
            this.Controls.Add(this.lbGyohoKana);
            this.Controls.Add(this.txtGyohoCd);
            this.Controls.Add(this.txtGyohoKana);
            this.Controls.Add(this.lbGyohoNM);
            this.Controls.Add(this.lbGyohoCd);
            this.Name = "HANC9042";
            this.ShowFButton = true;
            this.Text = "漁法マスタ登録";
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lbGyohoCd, 0);
            this.Controls.SetChildIndex(this.lbGyohoNM, 0);
            this.Controls.SetChildIndex(this.txtGyohoKana, 0);
            this.Controls.SetChildIndex(this.txtGyohoCd, 0);
            this.Controls.SetChildIndex(this.lbGyohoKana, 0);
            this.Controls.SetChildIndex(this.txtGyohoNm, 0);
            this.pnlDebug.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private jp.co.fsi.common.controls.FsiTextBox txtGyohoCd;
        private System.Windows.Forms.Label lbGyohoCd;
        private jp.co.fsi.common.controls.FsiTextBox txtGyohoNm;
        private System.Windows.Forms.Label lbGyohoKana;
        private jp.co.fsi.common.controls.FsiTextBox txtGyohoKana;
        private System.Windows.Forms.Label lbGyohoNM;
    };
}