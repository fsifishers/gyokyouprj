﻿namespace jp.co.fsi.han.hanc9141
{
    partial class HANC9141
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblKanaNm = new System.Windows.Forms.Label();
            this.txtKanaName = new jp.co.fsi.common.controls.SosTextBox();
            this.dgvList = new System.Windows.Forms.DataGridView();
            this.funanushiCd = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.funanushiNm = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.funanushiKanaNm = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.soshinFlg = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ckbAllHyoji = new System.Windows.Forms.CheckBox();
            this.ckbAllCheck = new System.Windows.Forms.CheckBox();
            this.pnlDebug.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvList)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Text = "";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.pnlDebug.Size = new System.Drawing.Size(839, 100);
            // 
            // lblKanaNm
            // 
            this.lblKanaNm.BackColor = System.Drawing.Color.Silver;
            this.lblKanaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKanaNm.Location = new System.Drawing.Point(12, 48);
            this.lblKanaNm.Name = "lblKanaNm";
            this.lblKanaNm.Size = new System.Drawing.Size(224, 25);
            this.lblKanaNm.TabIndex = 1;
            this.lblKanaNm.Text = "カ　ナ　名";
            this.lblKanaNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKanaName
            // 
            this.txtKanaName.AllowDrop = true;
            this.txtKanaName.AutoSizeFromLength = false;
            this.txtKanaName.DisplayLength = null;
            this.txtKanaName.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKanaName.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtKanaName.Location = new System.Drawing.Point(93, 50);
            this.txtKanaName.MaxLength = 30;
            this.txtKanaName.Name = "txtKanaName";
            this.txtKanaName.Size = new System.Drawing.Size(141, 20);
            this.txtKanaName.TabIndex = 2;
            this.txtKanaName.Validating += new System.ComponentModel.CancelEventHandler(this.txtKanaName_Validating);
            // 
            // dgvList
            // 
            this.dgvList.AllowUserToAddRows = false;
            this.dgvList.AllowUserToDeleteRows = false;
            this.dgvList.AllowUserToResizeColumns = false;
            this.dgvList.AllowUserToResizeRows = false;
            this.dgvList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.funanushiCd,
            this.funanushiNm,
            this.funanushiKanaNm,
            this.soshinFlg});
            this.dgvList.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.dgvList.Location = new System.Drawing.Point(12, 82);
            this.dgvList.MultiSelect = false;
            this.dgvList.Name = "dgvList";
            this.dgvList.RowHeadersVisible = false;
            this.dgvList.RowTemplate.Height = 21;
            this.dgvList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvList.Size = new System.Drawing.Size(637, 447);
            this.dgvList.TabIndex = 3;
            this.dgvList.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvList_CellDoubleClick);
            this.dgvList.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvList_KeyDown);
            // 
            // funanushiCd
            // 
            this.funanushiCd.HeaderText = "船主コード";
            this.funanushiCd.Name = "funanushiCd";
            this.funanushiCd.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.funanushiCd.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // funanushiNm
            // 
            this.funanushiNm.HeaderText = "船主名";
            this.funanushiNm.Name = "funanushiNm";
            this.funanushiNm.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.funanushiNm.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.funanushiNm.Width = 225;
            // 
            // funanushiKanaNm
            // 
            this.funanushiKanaNm.HeaderText = "船主カナ名";
            this.funanushiKanaNm.Name = "funanushiKanaNm";
            this.funanushiKanaNm.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.funanushiKanaNm.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.funanushiKanaNm.Width = 200;
            // 
            // soshinFlg
            // 
            this.soshinFlg.HeaderText = "送信フラグ";
            this.soshinFlg.Name = "soshinFlg";
            this.soshinFlg.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // ckbAllHyoji
            // 
            this.ckbAllHyoji.AutoSize = true;
            this.ckbAllHyoji.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F);
            this.ckbAllHyoji.Location = new System.Drawing.Point(392, 49);
            this.ckbAllHyoji.Name = "ckbAllHyoji";
            this.ckbAllHyoji.Size = new System.Drawing.Size(91, 20);
            this.ckbAllHyoji.TabIndex = 902;
            this.ckbAllHyoji.Text = "全て表示";
            this.ckbAllHyoji.UseVisualStyleBackColor = true;
            this.ckbAllHyoji.CheckedChanged += new System.EventHandler(this.ckbAllHyoji_CheckedChanged);
            // 
            // ckbAllCheck
            // 
            this.ckbAllCheck.AutoSize = true;
            this.ckbAllCheck.Checked = true;
            this.ckbAllCheck.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ckbAllCheck.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F);
            this.ckbAllCheck.Location = new System.Drawing.Point(520, 50);
            this.ckbAllCheck.Name = "ckbAllCheck";
            this.ckbAllCheck.Size = new System.Drawing.Size(123, 20);
            this.ckbAllCheck.TabIndex = 903;
            this.ckbAllCheck.Text = "全てチェック";
            this.ckbAllCheck.UseVisualStyleBackColor = true;
            this.ckbAllCheck.CheckedChanged += new System.EventHandler(this.ckbAllCheck_CheckedChanged);
            // 
            // HANC9141
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 638);
            this.Controls.Add(this.ckbAllCheck);
            this.Controls.Add(this.ckbAllHyoji);
            this.Controls.Add(this.dgvList);
            this.Controls.Add(this.txtKanaName);
            this.Controls.Add(this.lblKanaNm);
            this.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "HANC9141";
            this.Text = "ReportSample";
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblKanaNm, 0);
            this.Controls.SetChildIndex(this.txtKanaName, 0);
            this.Controls.SetChildIndex(this.dgvList, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.ckbAllHyoji, 0);
            this.Controls.SetChildIndex(this.ckbAllCheck, 0);
            this.pnlDebug.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblKanaNm;
        private jp.co.fsi.common.controls.SosTextBox txtKanaName;
        private System.Windows.Forms.DataGridView dgvList;
        private System.Windows.Forms.CheckBox ckbAllHyoji;
        private System.Windows.Forms.DataGridViewTextBoxColumn funanushiCd;
        private System.Windows.Forms.DataGridViewTextBoxColumn funanushiNm;
        private System.Windows.Forms.DataGridViewTextBoxColumn funanushiKanaNm;
        private System.Windows.Forms.DataGridViewCheckBoxColumn soshinFlg;
        private System.Windows.Forms.CheckBox ckbAllCheck;

    }
}