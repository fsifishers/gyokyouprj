﻿namespace jp.co.fsi.han.hanc9141
{
    partial class HANC9143
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtPortNo = new jp.co.fsi.common.controls.SosTextBox();
            this.txtHostNm = new jp.co.fsi.common.controls.SosTextBox();
            this.txtMailAddress = new jp.co.fsi.common.controls.SosTextBox();
            this.lblPortNo = new System.Windows.Forms.Label();
            this.lblHostNm = new System.Windows.Forms.Label();
            this.lblMailAddress = new System.Windows.Forms.Label();
            this.txtSubject = new jp.co.fsi.common.controls.SosTextBox();
            this.lblSubject = new System.Windows.Forms.Label();
            this.txtUserNm = new jp.co.fsi.common.controls.SosTextBox();
            this.lblUserNm = new System.Windows.Forms.Label();
            this.txtPass = new jp.co.fsi.common.controls.SosTextBox();
            this.lblPass = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Size = new System.Drawing.Size(466, 23);
            this.lblTitle.Text = "";
            this.lblTitle.Visible = false;
            // 
            // btnF2
            // 
            this.btnF2.Text = "F2";
            this.btnF2.Visible = false;
            // 
            // btnF3
            // 
            this.btnF3.Text = "F3\r\n\r\n削除";
            // 
            // btnF4
            // 
            this.btnF4.Text = "F4";
            this.btnF4.Visible = false;
            // 
            // btnF5
            // 
            this.btnF5.Text = "F5";
            this.btnF5.Visible = false;
            // 
            // btnF7
            // 
            this.btnF7.Visible = false;
            // 
            // btnF6
            // 
            this.btnF6.Text = "F6\r\n\r\n登録";
            // 
            // btnF8
            // 
            this.btnF8.Visible = false;
            // 
            // btnF9
            // 
            this.btnF9.Visible = false;
            // 
            // btnF12
            // 
            this.btnF12.Text = "F12";
            this.btnF12.Visible = false;
            // 
            // btnF11
            // 
            this.btnF11.Visible = false;
            // 
            // btnF10
            // 
            this.btnF10.Visible = false;
            // 
            // pnlDebug
            // 
            this.pnlDebug.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.pnlDebug.Location = new System.Drawing.Point(5, 200);
            this.pnlDebug.Size = new System.Drawing.Size(499, 100);
            // 
            // txtPortNo
            // 
            this.txtPortNo.AutoSizeFromLength = false;
            this.txtPortNo.BackColor = System.Drawing.Color.White;
            this.txtPortNo.DisplayLength = null;
            this.txtPortNo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtPortNo.ImeMode = System.Windows.Forms.ImeMode.Alpha;
            this.txtPortNo.Location = new System.Drawing.Point(157, 72);
            this.txtPortNo.MaxLength = 10;
            this.txtPortNo.Name = "txtPortNo";
            this.txtPortNo.Size = new System.Drawing.Size(318, 20);
            this.txtPortNo.TabIndex = 6;
            this.txtPortNo.Validating += new System.ComponentModel.CancelEventHandler(this.txtPortNo_Validating);
            // 
            // txtHostNm
            // 
            this.txtHostNm.AutoSizeFromLength = false;
            this.txtHostNm.BackColor = System.Drawing.Color.White;
            this.txtHostNm.DisplayLength = null;
            this.txtHostNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtHostNm.ImeMode = System.Windows.Forms.ImeMode.Alpha;
            this.txtHostNm.Location = new System.Drawing.Point(157, 42);
            this.txtHostNm.MaxLength = 30;
            this.txtHostNm.Name = "txtHostNm";
            this.txtHostNm.Size = new System.Drawing.Size(318, 20);
            this.txtHostNm.TabIndex = 4;
            this.txtHostNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtHostNm_Validating);
            // 
            // txtMailAddress
            // 
            this.txtMailAddress.AutoSizeFromLength = false;
            this.txtMailAddress.BackColor = System.Drawing.Color.White;
            this.txtMailAddress.DisplayLength = null;
            this.txtMailAddress.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMailAddress.ImeMode = System.Windows.Forms.ImeMode.Alpha;
            this.txtMailAddress.Location = new System.Drawing.Point(156, 12);
            this.txtMailAddress.MaxLength = 50;
            this.txtMailAddress.Name = "txtMailAddress";
            this.txtMailAddress.Size = new System.Drawing.Size(318, 20);
            this.txtMailAddress.TabIndex = 2;
            this.txtMailAddress.Validating += new System.ComponentModel.CancelEventHandler(this.txtMailAddress_Validating);
            // 
            // lblPortNo
            // 
            this.lblPortNo.BackColor = System.Drawing.Color.Silver;
            this.lblPortNo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblPortNo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblPortNo.Location = new System.Drawing.Point(9, 70);
            this.lblPortNo.Name = "lblPortNo";
            this.lblPortNo.Size = new System.Drawing.Size(470, 25);
            this.lblPortNo.TabIndex = 5;
            this.lblPortNo.Text = "ＰＯＲＴ番号";
            this.lblPortNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblHostNm
            // 
            this.lblHostNm.BackColor = System.Drawing.Color.Silver;
            this.lblHostNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblHostNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblHostNm.Location = new System.Drawing.Point(9, 40);
            this.lblHostNm.Name = "lblHostNm";
            this.lblHostNm.Size = new System.Drawing.Size(470, 25);
            this.lblHostNm.TabIndex = 3;
            this.lblHostNm.Text = "ＨＯＳＴ名";
            this.lblHostNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMailAddress
            // 
            this.lblMailAddress.BackColor = System.Drawing.Color.Silver;
            this.lblMailAddress.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMailAddress.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMailAddress.Location = new System.Drawing.Point(8, 10);
            this.lblMailAddress.Name = "lblMailAddress";
            this.lblMailAddress.Size = new System.Drawing.Size(470, 25);
            this.lblMailAddress.TabIndex = 1;
            this.lblMailAddress.Text = "送信用メールアドレス";
            this.lblMailAddress.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtSubject
            // 
            this.txtSubject.AutoSizeFromLength = false;
            this.txtSubject.BackColor = System.Drawing.Color.White;
            this.txtSubject.DisplayLength = null;
            this.txtSubject.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtSubject.ImeMode = System.Windows.Forms.ImeMode.Alpha;
            this.txtSubject.Location = new System.Drawing.Point(157, 162);
            this.txtSubject.MaxLength = 50;
            this.txtSubject.Name = "txtSubject";
            this.txtSubject.Size = new System.Drawing.Size(318, 20);
            this.txtSubject.TabIndex = 12;
            this.txtSubject.Validating += new System.ComponentModel.CancelEventHandler(this.txtSubject_Validating);
            // 
            // lblSubject
            // 
            this.lblSubject.BackColor = System.Drawing.Color.Silver;
            this.lblSubject.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSubject.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblSubject.Location = new System.Drawing.Point(9, 160);
            this.lblSubject.Name = "lblSubject";
            this.lblSubject.Size = new System.Drawing.Size(470, 25);
            this.lblSubject.TabIndex = 11;
            this.lblSubject.Text = "件名";
            this.lblSubject.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtUserNm
            // 
            this.txtUserNm.AutoSizeFromLength = false;
            this.txtUserNm.BackColor = System.Drawing.Color.White;
            this.txtUserNm.DisplayLength = null;
            this.txtUserNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtUserNm.ImeMode = System.Windows.Forms.ImeMode.Alpha;
            this.txtUserNm.Location = new System.Drawing.Point(157, 102);
            this.txtUserNm.MaxLength = 50;
            this.txtUserNm.Name = "txtUserNm";
            this.txtUserNm.Size = new System.Drawing.Size(318, 20);
            this.txtUserNm.TabIndex = 8;
            this.txtUserNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtUserNm_Validating);
            // 
            // lblUserNm
            // 
            this.lblUserNm.BackColor = System.Drawing.Color.Silver;
            this.lblUserNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblUserNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblUserNm.Location = new System.Drawing.Point(8, 100);
            this.lblUserNm.Name = "lblUserNm";
            this.lblUserNm.Size = new System.Drawing.Size(470, 25);
            this.lblUserNm.TabIndex = 7;
            this.lblUserNm.Text = "ユーザー名";
            this.lblUserNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtPass
            // 
            this.txtPass.AutoSizeFromLength = false;
            this.txtPass.BackColor = System.Drawing.Color.White;
            this.txtPass.DisplayLength = null;
            this.txtPass.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtPass.ImeMode = System.Windows.Forms.ImeMode.Alpha;
            this.txtPass.Location = new System.Drawing.Point(157, 132);
            this.txtPass.MaxLength = 50;
            this.txtPass.Name = "txtPass";
            this.txtPass.Size = new System.Drawing.Size(318, 20);
            this.txtPass.TabIndex = 10;
            this.txtPass.Validating += new System.ComponentModel.CancelEventHandler(this.txtPass_Validating);
            // 
            // lblPass
            // 
            this.lblPass.BackColor = System.Drawing.Color.Silver;
            this.lblPass.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblPass.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblPass.Location = new System.Drawing.Point(9, 130);
            this.lblPass.Name = "lblPass";
            this.lblPass.Size = new System.Drawing.Size(470, 25);
            this.lblPass.TabIndex = 9;
            this.lblPass.Text = "パスワード";
            this.lblPass.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // HANC9143
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(491, 304);
            this.Controls.Add(this.txtPass);
            this.Controls.Add(this.lblPass);
            this.Controls.Add(this.txtUserNm);
            this.Controls.Add(this.lblUserNm);
            this.Controls.Add(this.txtSubject);
            this.Controls.Add(this.lblSubject);
            this.Controls.Add(this.txtPortNo);
            this.Controls.Add(this.txtHostNm);
            this.Controls.Add(this.txtMailAddress);
            this.Controls.Add(this.lblPortNo);
            this.Controls.Add(this.lblHostNm);
            this.Controls.Add(this.lblMailAddress);
            this.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "HANC9143";
            this.Text = "設定画面";
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblMailAddress, 0);
            this.Controls.SetChildIndex(this.lblHostNm, 0);
            this.Controls.SetChildIndex(this.lblPortNo, 0);
            this.Controls.SetChildIndex(this.txtMailAddress, 0);
            this.Controls.SetChildIndex(this.txtHostNm, 0);
            this.Controls.SetChildIndex(this.txtPortNo, 0);
            this.Controls.SetChildIndex(this.lblSubject, 0);
            this.Controls.SetChildIndex(this.txtSubject, 0);
            this.Controls.SetChildIndex(this.lblUserNm, 0);
            this.Controls.SetChildIndex(this.txtUserNm, 0);
            this.Controls.SetChildIndex(this.lblPass, 0);
            this.Controls.SetChildIndex(this.txtPass, 0);
            this.pnlDebug.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblPortNo;
        private System.Windows.Forms.Label lblHostNm;
        private System.Windows.Forms.Label lblMailAddress;
        private jp.co.fsi.common.controls.SosTextBox txtMailAddress;
        private jp.co.fsi.common.controls.SosTextBox txtPortNo;
        private jp.co.fsi.common.controls.SosTextBox txtHostNm;
        private common.controls.SosTextBox txtSubject;
        private System.Windows.Forms.Label lblSubject;
        private common.controls.SosTextBox txtUserNm;
        private System.Windows.Forms.Label lblUserNm;
        private common.controls.SosTextBox txtPass;
        private System.Windows.Forms.Label lblPass;

    }
}