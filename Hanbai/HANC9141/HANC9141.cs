﻿using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Net.Mail;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.han.hanc9141
{
    /// <summary>
    /// メール送信(HANC9141)
    /// </summary>
    public partial class HANC9141 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// モード(コード検索)
        /// </summary>
        private const string MODE_CD_SRC = "1";
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HANC9141()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region メソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.InitForm();は呼び出さなくて構いません。
        /// また、このメソッド内の処理を外出しでこのクラス内にメソッド化するのは構いませんが、
        /// 原則、独自で起動時のイベント処理を実装することは禁じます。
        /// </remarks>
        protected override void InitForm()
        {
            // まずデータが存在し得ない検索条件で検索をし、結果をバインドすることで、
            // 初期状態を作り出す
            SearchData();

            // フォーカス設定
            this.txtKanaName.Focus();
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            // カナ名にフォーカスを戻す
            this.txtKanaName.Focus();
            this.txtKanaName.SelectAll();
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 船主アドレス登録画面の起動
            EditFunanushi(string.Empty);
        }

        /// <summary>
        /// F8キー押下時処理
        /// </summary>
        public override void PressF8()
        {
            // 選択会計年度が今年度かチェック
            DateTime today = DateTime.Today;
            if (Util.GetKaikeiNendo(today, this.Dba) != this.UInfo.KaikeiNendo)
            {
                Msg.Error("選択会計年度が今年度ではありません。");
                return;
            }

            // 確認メッセージを表示
            string msg = "メールを送信しますか？";
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            /* メール送信処理 */
            bool flg = mailSet();

            if(flg)
            {
                Msg.Info("メール送信が完了しました。");
            }
            else
            {
                Msg.Info("メール送信に失敗しました。");
            }
        }

        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            HANC9143 frmHANC9143 = new HANC9143();

            DialogResult result = frmHANC9143.ShowDialog(this);

            if (result == DialogResult.OK)
            {
                // Gridに再度フォーカスをセット
                this.ActiveControl = this.dgvList;
                this.dgvList.Focus();
            }
        }
        #endregion

        #region イベント
        /// <summary>
        /// 表示ボタンの変更時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ckbAllHyoji_CheckedChanged(object sender, System.EventArgs e)
        {
            // 入力された情報を元に検索する
            SearchData();
        }

        /// <summary>
        /// チェックボタンの変更時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ckbAllCheck_CheckedChanged(object sender, System.EventArgs e)
        {
            // チェックボックスの切り替えを行う
            checkOnOff();
        }

        /// <summary>
        /// カナ名検証時の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKanaName_Validating(object sender, CancelEventArgs e)
        {
            //TODO:何かチェックが必要なのかもしれない
            // 入力された情報を元に検索する
            SearchData();
        }

        /// <summary>
        /// グリッドでのキーダウン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                EditFunanushi(Util.ToString(this.dgvList.SelectedRows[0].Cells["funanushiCd"].Value));
                e.Handled = true;
            }
        }

        /// <summary>
        /// グリッドのセルダブルクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            EditFunanushi(Util.ToString(this.dgvList.SelectedRows[0].Cells["funanushiCd"].Value));
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// データを検索する
        /// </summary>
        private void checkOnOff()
        {
            bool flg = false;
            // 「全てチェック」の場合
            if (this.ckbAllCheck.Checked)
            {
                flg = true;
            }

            // 送信フラグのON/OFF設定
            foreach (DataGridViewRow rowDt in this.dgvList.Rows)
            {
                rowDt.Cells[3].Value = flg;
            }
        }

        /// <summary>
        /// データを検索する
        /// </summary>
        private void SearchData()
        {
            // 船主アドレステーブルと船主マスタからデータを取得して表示
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder Sql = new StringBuilder();

            // 船主マスタデータを取得
            Sql.Append("SELECT");
            Sql.Append(" A.SENSHU_CD AS SENSHU_CD,");
            Sql.Append(" CM.TORIHIKISAKI_NM AS SENSHU_NM,");
            Sql.Append(" CM.TORIHIKISAKI_KANA_NM AS SENSHU_KANA ");
            Sql.Append("FROM");
            Sql.Append(" TB_HN_SERI_FUNANUSHU_ADDRESS AS A ");
            Sql.Append("LEFT OUTER JOIN");
            Sql.Append(" TB_CM_TORIHIKISAKI AS CM ");
            Sql.Append("ON");
            Sql.Append(" CM.KAISHA_CD = @KAISHA_CD AND CM.TORIHIKISAKI_CD = A.SENSHU_CD AND CM.HYOJI_FLG = @HYOJI_FLG ");
            Sql.Append("WHERE");
            Sql.Append(" 1 = 1");

            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@HYOJI_FLG", SqlDbType.Decimal, 1, 0);

            // 入力されたカナ名から検索する
            if (!ValChk.IsEmpty(this.txtKanaName.Text))
            {
                Sql.Append(" AND CM.TORIHIKISAKI_KANA_NM LIKE @TORIHIKISAKI_KANA_NM");
                // NOTICE!:部分一致検索をする場合、"%"の分桁数に2バイトを足すこと
                dpc.SetParam("@TORIHIKISAKI_KANA_NM", SqlDbType.VarChar, 30, "%" + this.txtKanaName.Text + "%");
            }
            // 「全て表示」でない場合、有効なアドレスが存在しない船主は表示しない
            if (this.ckbAllHyoji.Checked == false)
            {
                Sql.Append(" AND ((A.MAIL_ADDRESS1 IS NOT NULL AND A.MAIL_ADDRESS1 != '' AND A.ADDRESS1_FLG = 0) OR");
                Sql.Append(" (A.MAIL_ADDRESS2 IS NOT NULL AND A.MAIL_ADDRESS2 != '' AND A.ADDRESS2_FLG = 0) OR");
                Sql.Append(" (A.MAIL_ADDRESS3 IS NOT NULL AND A.MAIL_ADDRESS3 != '' AND A.ADDRESS3_FLG = 0))");
            }

            DataTable dt = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            // 初期処理以外の場合、該当データがなければエラーメッセージを表示
            if (dt.Rows.Count == 0)
            {
                Msg.Info("該当データがありません。");

                dt.Rows.Add(dt.NewRow());
            }

            // 明細のクリア
            this.dgvList.Rows.Clear();
            // 明細へのデータ設定
            int rows = dt.Rows.Count;
            int row = 0;

            // データを明細にセットする
            foreach (DataRow rowDt in dt.Rows)
            {
                // １行分の列（セル）データを設定する
                this.dgvList.Rows.Add();
                this.dgvList.Rows[row].Cells[0].Value = rowDt["SENSHU_CD"].ToString(); // 船主コード
                this.dgvList.Rows[row].Cells[1].Value = rowDt["SENSHU_NM"].ToString(); // 船主名
                this.dgvList.Rows[row].Cells[2].Value = rowDt["SENSHU_KANA"].ToString(); // 船主カナ名
                // メール送信フラグ
                if (this.ckbAllCheck.Checked)
                {
                    // 「全てチェック」の場合
                    this.dgvList.Rows[row].Cells[3].Value = true;
                }
                else
                {
                    // 「全てチェック」でない場合
                    this.dgvList.Rows[row].Cells[3].Value = false;
                }
                row++;
            }
        }

        /// <summary>
        /// 船主を追加編集する
        /// </summary>
        /// <param name="code">船主コード(空：新規登録、以外：編集)</param>
        private void EditFunanushi(string code)
        {
            HANC9142 frmHANC9142;

            if (ValChk.IsEmpty(code))
            {
                // 新規登録モードで登録画面を起動
                frmHANC9142 = new HANC9142("1", this.Par2);
            }
            else
            {
                // 編集モードで登録画面を起動
                frmHANC9142 = new HANC9142("2", this.Par2);
                frmHANC9142.InData = code;
            }

            DialogResult result = frmHANC9142.ShowDialog(this);

            if (result == DialogResult.OK)
            {
                // データを再検索する
                SearchData();
                // Gridに再度フォーカスをセット
                this.ActiveControl = this.dgvList;
                this.dgvList.Focus();
            }
        }

        /// <summary>
        /// メール送信用のデータを作成する
        /// </summary>
        private bool mailSet()
        {
            try
            {
                // 送信用データを取得する
                DataTable getSendDt = getSendMailAddress();
                // 送信用情報をセットする
                string mailAddress = "";
                string subject = "";
                string hpAddress = "";
                string hostNm = "";
                int portNo = 0;
                string userNm = "";
                string password = "";
                if (getSendDt.Rows.Count > 0)
                {
                    mailAddress = Util.ToString(getSendDt.Rows[0]["MAIL_ADDRESS"]);
                    subject = Util.ToString(getSendDt.Rows[0]["SUBJECT"]);
                    hpAddress = Util.ToString(getSendDt.Rows[0]["HP_ADDRESS"]);
                    hostNm = Util.ToString(getSendDt.Rows[0]["HOST_NM"]);
                    portNo = Util.ToInt(getSendDt.Rows[0]["PORT_NO"]);
                    userNm = Util.ToString(getSendDt.Rows[0]["USER_NM"]);
                    password = Util.ToString(getSendDt.Rows[0]["PASSWORD"]);
                }
                string[] today = Util.ConvJpDate(DateTime.Now, this.Dba);
                DateTime dt = DateTime.Now;
                string week = dt.ToString("ddd");

                string address1 = "";
                string address2 = "";
                string address3 = "";
                DataTable getAddressList;
                string funanushiCd;
                DataTable seriDt;
                DataTable seriDtTanka;
                DataTable seriMeisaiDt;
                DataTable seriKojoDt;
                bool flgAdd1;
                bool flgAdd2;
                bool flgAdd3;

                #region データを明細にセットする
                foreach (DataGridViewRow rowDt in this.dgvList.Rows)
                {
                    funanushiCd = Util.ToString(rowDt.Cells[0].Value);
                    // 送信フラグがONの場合、船主CDを格納
                    if (Util.ToString(rowDt.Cells[3].Value) == "True")
                    {
                        // 該当船主コードを取得
                        funanushiCd = Util.ToString(rowDt.Cells[0].Value);

                        // 船主コードに紐づく本日のセリデータを取得
                        seriDt = getSeriData(funanushiCd);
                        seriDtTanka = getSeriDataTanka(funanushiCd);
                        // 該当データが存在しない場合、以降処理を行わない
                        if (seriDt.Rows.Count == 0 || seriDtTanka.Rows.Count == 0)
                        {
                            continue;
                        }

                        seriMeisaiDt = getSeriMeisaiData(funanushiCd);
                        seriKojoDt = getSeriKojoData(funanushiCd);

                        // 該当する船主のアドレスを取得
                        getAddressList = getFunanushiAddress(funanushiCd);

                        // 有効なアドレスが1件以上存在する場合、有効なアドレスをセット
                        if (getAddressList.Rows.Count > 0)
                        {
                            // アドレス1のフラグを初期化
                            if (Util.ToString(getAddressList.Rows[0]["ADDRESS1_FLG"]) == "0")
                            {
                                flgAdd1 = true;
                            }
                            else
                            {
                                flgAdd1 = false;
                            }

                            // アドレス2のフラグを初期化
                            if (Util.ToString(getAddressList.Rows[0]["ADDRESS2_FLG"]) == "0")
                            {
                                flgAdd2 = true;
                            }
                            else
                            {
                                flgAdd2 = false;
                            }

                            // アドレス3のフラグを初期化
                            if (Util.ToString(getAddressList.Rows[0]["ADDRESS3_FLG"]) == "0")
                            {
                                flgAdd3 = true;
                            }
                            else
                            {
                                flgAdd3 = false;
                            }

                            // アドレス1
                            if (flgAdd1 && !ValChk.IsEmpty(getAddressList.Rows[0]["ADDRESS1"]))
                            {
                                address1 = Util.ToString(getAddressList.Rows[0]["ADDRESS1"]);
                            }
                            else if (flgAdd2 && !ValChk.IsEmpty(getAddressList.Rows[0]["ADDRESS2"]))
                            {
                                address1 = Util.ToString(getAddressList.Rows[0]["ADDRESS2"]);
                                flgAdd2 = false;
                            }
                            else if (flgAdd3 && !ValChk.IsEmpty(getAddressList.Rows[0]["ADDRESS3"]))
                            {
                                address1 = Util.ToString(getAddressList.Rows[0]["ADDRESS3"]);
                                flgAdd3 = false;
                            }

                            // アドレス2
                            if (flgAdd2 && !ValChk.IsEmpty(address1) && !ValChk.IsEmpty(getAddressList.Rows[0]["ADDRESS2"]))
                            {
                                address2 = Util.ToString(getAddressList.Rows[0]["ADDRESS2"]);
                            }

                            // アドレス3
                            if (flgAdd3 && !ValChk.IsEmpty(address1) && !ValChk.IsEmpty(address2) && !ValChk.IsEmpty(getAddressList.Rows[0]["ADDRESS3"]))
                            {
                                address3 = Util.ToString(getAddressList.Rows[0]["ADDRESS3"]);
                            }
                            else if(flgAdd3 && !ValChk.IsEmpty(address1) && ValChk.IsEmpty(address2) && !ValChk.IsEmpty(getAddressList.Rows[0]["ADDRESS3"]))
                            {
                                address2 = Util.ToString(getAddressList.Rows[0]["ADDRESS3"]);
                            }

                            // メール内容を生成し、送信する
                            sendMail(seriDt, seriDtTanka, seriMeisaiDt, seriKojoDt, today[5], week, mailAddress, subject, hpAddress, hostNm, portNo, userNm, password, address1, address2, address3);

                            // 初期化
                            address1 = "";
                            address2 = "";
                            address3 = "";
                        }
                    }
                }
                #endregion

                return true;
            }
            catch (System.Exception e)
            {
                return false;
            }
        }
        
        /// <summary>
        /// データを検索する
        /// </summary>
        private DataTable getFunanushiAddress(string code)
        {
            // 船主アドレステーブルと船主マスタからデータを取得して表示
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder Sql = new StringBuilder();

            // 船主マスタデータを取得
            Sql.Append("SELECT");
            Sql.Append(" A.MAIL_ADDRESS1 AS ADDRESS1,");
            Sql.Append(" A.ADDRESS1_FLG AS ADDRESS1_FLG,");
            Sql.Append(" A.MAIL_ADDRESS2 AS ADDRESS2,");
            Sql.Append(" A.ADDRESS2_FLG AS ADDRESS2_FLG,");
            Sql.Append(" A.MAIL_ADDRESS3 AS ADDRESS3,");
            Sql.Append(" A.ADDRESS3_FLG AS ADDRESS3_FLG ");
            Sql.Append("FROM");
            Sql.Append(" TB_HN_SERI_FUNANUSHU_ADDRESS AS A ");
            Sql.Append("WHERE");
            Sql.Append(" SENSHU_CD = @SENSHU_CD AND");
            Sql.Append(" ((A.MAIL_ADDRESS1 IS NOT NULL AND A.MAIL_ADDRESS1 != '' AND A.ADDRESS1_FLG = 0) OR");
            Sql.Append(" (A.MAIL_ADDRESS2 IS NOT NULL AND A.MAIL_ADDRESS2 != '' AND A.ADDRESS2_FLG = 0) OR");
            Sql.Append(" (A.MAIL_ADDRESS3 IS NOT NULL AND A.MAIL_ADDRESS3 != '' AND A.ADDRESS3_FLG = 0))");

            dpc.SetParam("@SENSHU_CD", SqlDbType.VarChar, 4, code);

            DataTable dt = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            return dt;
        }

        /// <summary>
        /// 送信者のメールアドレスを取得する
        /// </summary>
        private DataTable getSendMailAddress()
        {
            // 船主アドレステーブルと船主マスタからデータを取得して表示
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder Sql = new StringBuilder();

            // 船主マスタデータを取得
            Sql.Append("SELECT");
            Sql.Append(" A.TANTO_MAIL_ADDRESS AS MAIL_ADDRESS,");
            Sql.Append(" A.SUBJECT_FUNANUSHI AS SUBJECT,");
            Sql.Append(" A.HP_ADDRESS AS HP_ADDRESS,");
            Sql.Append(" A.HOST_NM AS HOST_NM,");
            Sql.Append(" A.PORT_NO AS PORT_NO, ");
            Sql.Append(" A.USER_NM AS USER_NM, ");
            Sql.Append(" A.PASSWORD AS PASSWORD ");
            Sql.Append("FROM");
            Sql.Append(" TB_HN_SERI_MAIL_SETTING AS A ");
            Sql.Append("WHERE");
            Sql.Append(" ICHIBA_CD = @ICHIBA_CD");

            dpc.SetParam("@ICHIBA_CD", SqlDbType.Decimal, 4, 108);

            DataTable dt = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            return dt;
        }

        /// <summary>
        /// メールを送信する
        /// </summary>
        private void sendMail(
            DataTable seriDt, DataTable seriDtTanka, DataTable seriMeisaiDt, DataTable seriKojoDt, string today, string week, string mailAddress, 
            string subject, string hpAddress, string hostNm, int portNo, string userNm, string password, 
            string address1, string address2, string address3)
        {
            try
            {
                MailMessage msg = new MailMessage();

                // 送信者 ※担当ユーザーアドレス
                msg.From = new MailAddress(mailAddress);
                // 宛先(Bcc) ※担当ユーザーアドレス
                msg.Bcc.Add(new MailAddress(mailAddress));

                // 宛先(To) ※船主のアドレス
                msg.To.Add(new MailAddress(address1));
                // 宛先(Cc) ※船主のアドレス
                if (!ValChk.IsEmpty(address2))
                {
                    msg.CC.Add(new MailAddress(address2));
                }
                if (!ValChk.IsEmpty(address3))
                {
                    msg.CC.Add(new MailAddress(address3));
                }

                // 件名
                msg.Subject = subject;
                // 本文
                msg.BodyEncoding = System.Text.Encoding.GetEncoding(50220);
                msg.Body = hpAddress;
                msg.Body += "\r\nセリ結果を報告します。";

                msg.Body += "\r\n\r\n上場市場 名護漁協";
                msg.Body += "\r\nセリ日 " + today + "(" + week + ")";

                msg.Body += "\r\n会員名 " + Util.ToString(seriDt.Rows[0]["SENSHU_NM"]);
                msg.Body += "\r\n数量計 " + Util.FormatNum(seriDt.Rows[0]["SURYO"], 1) + " Kg";
                msg.Body += "\r\n水揚金額 " + Util.FormatNum(seriDt.Rows[0]["KINGAKU"]) + " 円";
                msg.Body += "\r\n平均単価 " + Util.FormatNum(seriDtTanka.Rows[0]["TANKA"]) + " 円";
                msg.Body += "\r\n差引支払額 " + Util.FormatNum(Util.ToDecimal(seriDt.Rows[0]["KINGAKU"]) - Util.ToDecimal(seriKojoDt.Rows[0]["KOJO"])) + " 円";

                msg.Body += "\r\n\r\n";
                msg.Body += "----------------------------\r\n";
                msg.Body += "山No,魚種,数量,単価,金額\r\n";
                msg.Body += "----------------------------\r\n";

                // セリ明細
                Encoding sjisEnc = Encoding.GetEncoding("Shift_JIS");
                int num; 
                foreach (DataRow row in seriMeisaiDt.Rows)
                {
                    num = sjisEnc.GetByteCount(Util.ToString(row["GYOSHU_NM"]));
                    num = (10 - num / 2);

                    msg.Body += Util.PadZero(Util.ToString(row["YAMA_NO"]), 4) + ", ";
                    msg.Body += String.Format("{0, " + num + "}", Util.ToString(row["GYOSHU_NM"])) + ", ";
                    msg.Body += Util.FormatNum(row["SURYO"], 1) + "Kg, ";
                    msg.Body += Util.FormatNum(row["TANKA"]) + "円, ";
                    msg.Body += Util.FormatNum(row["KINGAKU"]) + "円\r\n";
                    msg.Body += "----------------------------\r\n";
                }

                System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();
                smtp.Host = hostNm;
                smtp.Port = portNo;
                smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                smtp.Credentials = new System.Net.NetworkCredential(userNm, password);
                smtp.Send(msg);

                msg.Dispose();
                smtp.Dispose();
            }
            catch (System.Exception e)
            {
                //Msg.Info("例外が発生しました。");
            }
        }

        /// <summary>
        /// 該当船主のセリ基本情報を取得する
        /// </summary>
        private DataTable getSeriData(string funanushiCd)
        {
            // 船主アドレステーブルと船主マスタからデータを取得して表示
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder Sql = new StringBuilder();

            // 船主マスタデータを取得
            Sql.Append("SELECT");
            Sql.Append(" SENSHU_NM AS SENSHU_NM,");
            Sql.Append(" SUM(MIZUAGE_GOKEI_SURYO) AS SURYO,");
            Sql.Append(" SUM(MIZUAGE_ZEIKOMI_KINGAKU) AS KINGAKU ");
            Sql.Append("FROM");
            Sql.Append(" TB_HN_SHIKIRI_DATA ");
            Sql.Append("WHERE");
            Sql.Append(" KAISHA_CD = @KAISHA_CD AND");
            Sql.Append(" KAIKEI_NENDO = @KAIKEI_NENDO AND");
            Sql.Append(" SERIBI = @TODAY AND");
            Sql.Append(" SENSHU_CD = @SENSHU_CD ");
            Sql.Append("GROUP BY");
            Sql.Append(" SENSHU_CD, SENSHU_NM");

            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            dpc.SetParam("@TODAY", SqlDbType.DateTime, 20, DateTime.Now.Date);
            dpc.SetParam("@SENSHU_CD", SqlDbType.Decimal, 4, funanushiCd);

            DataTable dt = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            return dt;
        }

        /// <summary>
        /// 該当船主のセリ基本情報を取得する
        /// </summary>
        private DataTable getSeriDataTanka(string funanushiCd)
        {
            // 船主アドレステーブルと船主マスタからデータを取得して表示
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder Sql = new StringBuilder();

            // 船主マスタデータを取得
            Sql.Append("SELECT");
            Sql.Append(" AVG(TANKA) AS TANKA ");
            Sql.Append("FROM");
            Sql.Append(" VI_HN_SHIKIRI_MEISAI ");
            Sql.Append("WHERE");
            Sql.Append(" KAISHA_CD = @KAISHA_CD AND");
            Sql.Append(" KAIKEI_NENDO = @KAIKEI_NENDO AND");
            Sql.Append(" SERIBI = @TODAY AND");
            Sql.Append(" SENSHU_CD = @SENSHU_CD ");
            Sql.Append("GROUP BY");
            Sql.Append(" SENSHU_CD, SENSHU_NM");

            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            dpc.SetParam("@TODAY", SqlDbType.DateTime, 20, DateTime.Now.Date);
            dpc.SetParam("@SENSHU_CD", SqlDbType.Decimal, 4, funanushiCd);

            DataTable dt = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            return dt;
        }

        /// <summary>
        /// 該当船主のセリ詳細情報を取得する
        /// </summary>
        private DataTable getSeriMeisaiData(string funanushiCd)
        {
            // 船主アドレステーブルと船主マスタからデータを取得して表示
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder Sql = new StringBuilder();

            // 船主マスタデータを取得
            Sql.Append("SELECT");
            Sql.Append(" A.YAMA_NO AS YAMA_NO,");
            Sql.Append(" SUBSTRING(GYOSHU_NM, 1, 5) AS GYOSHU_NM,");
            Sql.Append(" A.SURYO AS SURYO,");
            Sql.Append(" A.TANKA AS TANKA,");
            Sql.Append(" A.KINGAKU + A.SHOHIZEI AS KINGAKU ");
            Sql.Append("FROM");
            Sql.Append(" VI_HN_SHIKIRI_MEISAI AS A ");
            Sql.Append("WHERE");
            Sql.Append(" KAISHA_CD = @KAISHA_CD AND");
            Sql.Append(" KAIKEI_NENDO = @KAIKEI_NENDO AND");
            Sql.Append(" SERIBI = @TODAY AND");
            Sql.Append(" SENSHU_CD = @SENSHU_CD ");

            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            dpc.SetParam("@TODAY", SqlDbType.DateTime, 20, DateTime.Now.Date);
            dpc.SetParam("@SENSHU_CD", SqlDbType.Decimal, 4, funanushiCd);

            DataTable dt = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            return dt;
        }

        /// <summary>
        /// 該当船主の控除情報を取得する
        /// </summary>
        private DataTable getSeriKojoData(string funanushiCd)
        {
            // 船主アドレステーブルと船主マスタからデータを取得して表示
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder Sql = new StringBuilder();

            // 船主マスタデータを取得
            Sql.Append("SELECT");
            Sql.Append(" (MAX(A.KOJO_KOMOKU1) + MAX(A.KOJO_KOMOKU2) + MAX(A.KOJO_KOMOKU3) + MAX(A.KOJO_KOMOKU4) + MAX(A.KOJO_KOMOKU5) + MAX(A.KOJO_KOMOKU6)) AS KOJO ");
            Sql.Append("FROM");
            Sql.Append(" VI_HN_MIZUAGE_SHIKIRISHO AS A ");
            Sql.Append("WHERE");
            Sql.Append(" KAISHA_CD = @KAISHA_CD AND");
            Sql.Append(" KAIKEI_NENDO = @KAIKEI_NENDO AND");
            Sql.Append(" SERIBI = @TODAY AND");
            Sql.Append(" SENSHU_CD = @SENSHU_CD ");
            Sql.Append("GROUP BY");
            Sql.Append(" SENSHU_CD");

            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            dpc.SetParam("@TODAY", SqlDbType.DateTime, 20, DateTime.Now.Date);
            dpc.SetParam("@SENSHU_CD", SqlDbType.Decimal, 4, funanushiCd);

            DataTable dt = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            return dt;
        }
        #endregion
    }
}
