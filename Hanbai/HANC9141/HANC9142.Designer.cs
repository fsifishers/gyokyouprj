﻿namespace jp.co.fsi.han.hanc9141
{
    partial class HANC9142
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblFunanushiCd = new System.Windows.Forms.Label();
            this.txtFunanushiCd = new jp.co.fsi.common.controls.SosTextBox();
            this.txtMailAddress3 = new jp.co.fsi.common.controls.SosTextBox();
            this.txtMailAddress2 = new jp.co.fsi.common.controls.SosTextBox();
            this.txtMailAddress1 = new jp.co.fsi.common.controls.SosTextBox();
            this.lblMailAddress3 = new System.Windows.Forms.Label();
            this.lblMailAddress2 = new System.Windows.Forms.Label();
            this.lblMailAddress1 = new System.Windows.Forms.Label();
            this.lblFlg1 = new System.Windows.Forms.Label();
            this.cbxFlg1 = new System.Windows.Forms.CheckBox();
            this.cbxFlg2 = new System.Windows.Forms.CheckBox();
            this.lblFlg2 = new System.Windows.Forms.Label();
            this.cbxFlg3 = new System.Windows.Forms.CheckBox();
            this.lblFlg3 = new System.Windows.Forms.Label();
            this.lblFunanushiNm = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Size = new System.Drawing.Size(530, 23);
            this.lblTitle.Text = "";
            this.lblTitle.Visible = false;
            // 
            // btnF2
            // 
            this.btnF2.Text = "F2";
            this.btnF2.Visible = false;
            // 
            // btnF3
            // 
            this.btnF3.Text = "F3\r\n\r\n削除";
            // 
            // btnF4
            // 
            this.btnF4.Text = "F4";
            this.btnF4.Visible = false;
            // 
            // btnF5
            // 
            this.btnF5.Text = "F5";
            this.btnF5.Visible = false;
            // 
            // btnF7
            // 
            this.btnF7.Visible = false;
            // 
            // btnF6
            // 
            this.btnF6.Text = "F6\r\n\r\n登録";
            // 
            // btnF8
            // 
            this.btnF8.Visible = false;
            // 
            // btnF9
            // 
            this.btnF9.Visible = false;
            // 
            // btnF12
            // 
            this.btnF12.Text = "F12";
            this.btnF12.Visible = false;
            // 
            // btnF11
            // 
            this.btnF11.Visible = false;
            // 
            // btnF10
            // 
            this.btnF10.Visible = false;
            // 
            // pnlDebug
            // 
            this.pnlDebug.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.pnlDebug.Location = new System.Drawing.Point(5, 69);
            this.pnlDebug.Size = new System.Drawing.Size(563, 100);
            // 
            // lblFunanushiCd
            // 
            this.lblFunanushiCd.BackColor = System.Drawing.Color.Silver;
            this.lblFunanushiCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFunanushiCd.Location = new System.Drawing.Point(9, 5);
            this.lblFunanushiCd.Name = "lblFunanushiCd";
            this.lblFunanushiCd.Size = new System.Drawing.Size(115, 25);
            this.lblFunanushiCd.TabIndex = 1;
            this.lblFunanushiCd.Text = "船 主 C D";
            this.lblFunanushiCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFunanushiCd
            // 
            this.txtFunanushiCd.AutoSizeFromLength = false;
            this.txtFunanushiCd.DisplayLength = null;
            this.txtFunanushiCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFunanushiCd.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtFunanushiCd.Location = new System.Drawing.Point(81, 7);
            this.txtFunanushiCd.MaxLength = 4;
            this.txtFunanushiCd.Name = "txtFunanushiCd";
            this.txtFunanushiCd.Size = new System.Drawing.Size(40, 20);
            this.txtFunanushiCd.TabIndex = 2;
            this.txtFunanushiCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFunanushiCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtFunanushiCd_Validating);
            // 
            // txtMailAddress3
            // 
            this.txtMailAddress3.AutoSizeFromLength = false;
            this.txtMailAddress3.BackColor = System.Drawing.Color.White;
            this.txtMailAddress3.DisplayLength = null;
            this.txtMailAddress3.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMailAddress3.ImeMode = System.Windows.Forms.ImeMode.Alpha;
            this.txtMailAddress3.Location = new System.Drawing.Point(124, 87);
            this.txtMailAddress3.MaxLength = 50;
            this.txtMailAddress3.Name = "txtMailAddress3";
            this.txtMailAddress3.Size = new System.Drawing.Size(318, 20);
            this.txtMailAddress3.TabIndex = 13;
            this.txtMailAddress3.Validating += new System.ComponentModel.CancelEventHandler(this.txtMailAddress3_Validating);
            // 
            // txtMailAddress2
            // 
            this.txtMailAddress2.AutoSizeFromLength = false;
            this.txtMailAddress2.BackColor = System.Drawing.Color.White;
            this.txtMailAddress2.DisplayLength = null;
            this.txtMailAddress2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMailAddress2.ImeMode = System.Windows.Forms.ImeMode.Alpha;
            this.txtMailAddress2.Location = new System.Drawing.Point(124, 62);
            this.txtMailAddress2.MaxLength = 50;
            this.txtMailAddress2.Name = "txtMailAddress2";
            this.txtMailAddress2.Size = new System.Drawing.Size(318, 20);
            this.txtMailAddress2.TabIndex = 9;
            this.txtMailAddress2.Validating += new System.ComponentModel.CancelEventHandler(this.txtMailAddress2_Validating);
            // 
            // txtMailAddress1
            // 
            this.txtMailAddress1.AutoSizeFromLength = false;
            this.txtMailAddress1.BackColor = System.Drawing.Color.White;
            this.txtMailAddress1.DisplayLength = null;
            this.txtMailAddress1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMailAddress1.ImeMode = System.Windows.Forms.ImeMode.Alpha;
            this.txtMailAddress1.Location = new System.Drawing.Point(124, 37);
            this.txtMailAddress1.MaxLength = 50;
            this.txtMailAddress1.Name = "txtMailAddress1";
            this.txtMailAddress1.Size = new System.Drawing.Size(318, 20);
            this.txtMailAddress1.TabIndex = 5;
            this.txtMailAddress1.Validating += new System.ComponentModel.CancelEventHandler(this.txtMailAddress1_Validating);
            // 
            // lblMailAddress3
            // 
            this.lblMailAddress3.BackColor = System.Drawing.Color.Silver;
            this.lblMailAddress3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMailAddress3.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMailAddress3.Location = new System.Drawing.Point(9, 86);
            this.lblMailAddress3.Name = "lblMailAddress3";
            this.lblMailAddress3.Size = new System.Drawing.Size(438, 25);
            this.lblMailAddress3.TabIndex = 12;
            this.lblMailAddress3.Text = "メールアドレス3";
            this.lblMailAddress3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMailAddress2
            // 
            this.lblMailAddress2.BackColor = System.Drawing.Color.Silver;
            this.lblMailAddress2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMailAddress2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMailAddress2.Location = new System.Drawing.Point(9, 61);
            this.lblMailAddress2.Name = "lblMailAddress2";
            this.lblMailAddress2.Size = new System.Drawing.Size(438, 25);
            this.lblMailAddress2.TabIndex = 8;
            this.lblMailAddress2.Text = "メールアドレス2";
            this.lblMailAddress2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMailAddress1
            // 
            this.lblMailAddress1.BackColor = System.Drawing.Color.Silver;
            this.lblMailAddress1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMailAddress1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMailAddress1.Location = new System.Drawing.Point(9, 36);
            this.lblMailAddress1.Name = "lblMailAddress1";
            this.lblMailAddress1.Size = new System.Drawing.Size(438, 25);
            this.lblMailAddress1.TabIndex = 4;
            this.lblMailAddress1.Text = "メールアドレス1";
            this.lblMailAddress1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblFlg1
            // 
            this.lblFlg1.BackColor = System.Drawing.Color.Silver;
            this.lblFlg1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFlg1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFlg1.Location = new System.Drawing.Point(449, 36);
            this.lblFlg1.Name = "lblFlg1";
            this.lblFlg1.Size = new System.Drawing.Size(95, 25);
            this.lblFlg1.TabIndex = 6;
            this.lblFlg1.Text = "有効フラグ";
            this.lblFlg1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cbxFlg1
            // 
            this.cbxFlg1.AutoSize = true;
            this.cbxFlg1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cbxFlg1.Location = new System.Drawing.Point(525, 41);
            this.cbxFlg1.Name = "cbxFlg1";
            this.cbxFlg1.Size = new System.Drawing.Size(15, 14);
            this.cbxFlg1.TabIndex = 7;
            this.cbxFlg1.UseVisualStyleBackColor = true;
            // 
            // cbxFlg2
            // 
            this.cbxFlg2.AutoSize = true;
            this.cbxFlg2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cbxFlg2.Location = new System.Drawing.Point(525, 66);
            this.cbxFlg2.Name = "cbxFlg2";
            this.cbxFlg2.Size = new System.Drawing.Size(15, 14);
            this.cbxFlg2.TabIndex = 11;
            this.cbxFlg2.UseVisualStyleBackColor = true;
            // 
            // lblFlg2
            // 
            this.lblFlg2.BackColor = System.Drawing.Color.Silver;
            this.lblFlg2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFlg2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFlg2.Location = new System.Drawing.Point(449, 61);
            this.lblFlg2.Name = "lblFlg2";
            this.lblFlg2.Size = new System.Drawing.Size(95, 25);
            this.lblFlg2.TabIndex = 10;
            this.lblFlg2.Text = "有効フラグ";
            this.lblFlg2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cbxFlg3
            // 
            this.cbxFlg3.AutoSize = true;
            this.cbxFlg3.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cbxFlg3.Location = new System.Drawing.Point(525, 91);
            this.cbxFlg3.Name = "cbxFlg3";
            this.cbxFlg3.Size = new System.Drawing.Size(15, 14);
            this.cbxFlg3.TabIndex = 15;
            this.cbxFlg3.UseVisualStyleBackColor = true;
            // 
            // lblFlg3
            // 
            this.lblFlg3.BackColor = System.Drawing.Color.Silver;
            this.lblFlg3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFlg3.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFlg3.Location = new System.Drawing.Point(449, 86);
            this.lblFlg3.Name = "lblFlg3";
            this.lblFlg3.Size = new System.Drawing.Size(95, 25);
            this.lblFlg3.TabIndex = 14;
            this.lblFlg3.Text = "有効フラグ";
            this.lblFlg3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblFunanushiNm
            // 
            this.lblFunanushiNm.BackColor = System.Drawing.Color.Silver;
            this.lblFunanushiNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFunanushiNm.Location = new System.Drawing.Point(126, 5);
            this.lblFunanushiNm.Name = "lblFunanushiNm";
            this.lblFunanushiNm.Size = new System.Drawing.Size(321, 25);
            this.lblFunanushiNm.TabIndex = 3;
            this.lblFunanushiNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // HANC9142
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(555, 173);
            this.Controls.Add(this.lblFunanushiNm);
            this.Controls.Add(this.cbxFlg3);
            this.Controls.Add(this.lblFlg3);
            this.Controls.Add(this.cbxFlg2);
            this.Controls.Add(this.lblFlg2);
            this.Controls.Add(this.cbxFlg1);
            this.Controls.Add(this.lblFlg1);
            this.Controls.Add(this.txtMailAddress3);
            this.Controls.Add(this.txtFunanushiCd);
            this.Controls.Add(this.txtMailAddress2);
            this.Controls.Add(this.lblFunanushiCd);
            this.Controls.Add(this.txtMailAddress1);
            this.Controls.Add(this.lblMailAddress3);
            this.Controls.Add(this.lblMailAddress2);
            this.Controls.Add(this.lblMailAddress1);
            this.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "HANC9142";
            this.Text = "船主アドレス登録";
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblMailAddress1, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.lblMailAddress2, 0);
            this.Controls.SetChildIndex(this.lblMailAddress3, 0);
            this.Controls.SetChildIndex(this.txtMailAddress1, 0);
            this.Controls.SetChildIndex(this.lblFunanushiCd, 0);
            this.Controls.SetChildIndex(this.txtMailAddress2, 0);
            this.Controls.SetChildIndex(this.txtFunanushiCd, 0);
            this.Controls.SetChildIndex(this.txtMailAddress3, 0);
            this.Controls.SetChildIndex(this.lblFlg1, 0);
            this.Controls.SetChildIndex(this.cbxFlg1, 0);
            this.Controls.SetChildIndex(this.lblFlg2, 0);
            this.Controls.SetChildIndex(this.cbxFlg2, 0);
            this.Controls.SetChildIndex(this.lblFlg3, 0);
            this.Controls.SetChildIndex(this.cbxFlg3, 0);
            this.Controls.SetChildIndex(this.lblFunanushiNm, 0);
            this.pnlDebug.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblFunanushiCd;
        private jp.co.fsi.common.controls.SosTextBox txtFunanushiCd;
        private System.Windows.Forms.Label lblMailAddress3;
        private System.Windows.Forms.Label lblMailAddress2;
        private System.Windows.Forms.Label lblMailAddress1;
        private jp.co.fsi.common.controls.SosTextBox txtMailAddress1;
        private jp.co.fsi.common.controls.SosTextBox txtMailAddress3;
        private jp.co.fsi.common.controls.SosTextBox txtMailAddress2;
        private System.Windows.Forms.Label lblFlg1;
        private System.Windows.Forms.CheckBox cbxFlg1;
        private System.Windows.Forms.CheckBox cbxFlg2;
        private System.Windows.Forms.Label lblFlg2;
        private System.Windows.Forms.CheckBox cbxFlg3;
        private System.Windows.Forms.Label lblFlg3;
        private System.Windows.Forms.Label lblFunanushiNm;

    }
}