﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;
using System.Security.Cryptography;
using jp.co.fsi.common.mynumber;

namespace jp.co.fsi.han.hanc9141
{
    /// <summary>
    /// 船主アドレス登録(HANC9142)
    /// </summary>
    public partial class HANC9142 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// モード(新規)
        /// </summary>
        private const string MODE_NEW = "1";

        /// <summary>
        /// モード(編集)
        /// </summary>
        private const string MODE_EDIT = "2";
        #endregion

        #region プロパティ
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HANC9142()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="par1">引数1</param>
        /// <param name="par2">引数2</param>
        public HANC9142(string par1, string par2)
            : base(par1, par2)
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region メソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // タイトルは非表示
            this.lblTitle.Visible = false;

            //ボタン表示
            this.ShowFButton = true;

            // 引数：Par1／モード(1:新規、2:変更)、InData：仕入先コード
            if (MODE_NEW.Equals(this.Par1))
            {
                // 新規モードの初期表示
                InitDispOnNew();
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // 編集モードの初期表示
                InitDispOnEdit();
            }
            else
            {
                // 不正な起動として閉じる
                Msg.Error("不正な起動です。終了します。");
                this.Close();
            }

            // ボタンの配置を調整
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF1.Location = this.btnF2.Location;
            this.btnF6.Location = this.btnF4.Location;

            // ボタンを調整
            this.btnF1.Visible = true;
            this.btnF2.Visible = false;
            this.btnF3.Visible = true;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF6.Visible = true;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 船主コードにフォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtFunanushiCd":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;

            switch (this.ActiveCtlNm)
            {
                case "txtFunanushiCd":

                    // アセンブリのロード
                    asm = Assembly.LoadFrom("KOBC9021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.kob.kobc9021.KOBC9021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtFunanushiCd.Text = outData[0];
                                this.lblFunanushiNm.Text = outData[1];
                            }
                        }
                    }
                    break;

                default:
                    break;
            }
        }

        /// <summary>
        /// F3キー押下時処理
        /// </summary>
        public override void PressF3()
        {
            // 新規モード時
            if (MODE_NEW.Equals(this.Par1))
            {
                return;
            }

            if (Msg.ConfYesNo("削除しますか？") == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 入力値をバインドパラメータとしてセットする
            ArrayList delParams = SetDelParams();

            try
            {
                this.Dba.BeginTransaction();
                this.Dba.Delete("TB_HN_SERI_FUNANUSHU_ADDRESS",
                    "ICHIBA_CD = @ICHIBA_CD AND SENSHU_CD = @SENSHU_CD",
                    (DbParamCollection)delParams[0]);

                // トランザクションをコミット
                this.Dba.Commit();
                Msg.Info("削除しました。");
            }
            catch
            {
                Msg.Info("削除に失敗しました。");
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF6();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF6()
        {
            // 確認メッセージを表示
            string msg = (MODE_NEW.Equals(this.Par1) ? "登録" : "更新") + "しますか？";
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            // 入力値をバインドパラメータとしてセットする
            ArrayList alParams = SetParams();

            try
            {
                this.Dba.BeginTransaction();

                if (MODE_NEW.Equals(this.Par1))
                {
                    // データ登録
                    this.Dba.Insert("TB_HN_SERI_FUNANUSHU_ADDRESS", (DbParamCollection)alParams[0]);
                }
                else if (MODE_EDIT.Equals(this.Par1))
                {
                    // データ更新
                    this.Dba.Update("TB_HN_SERI_FUNANUSHU_ADDRESS",
                        (DbParamCollection)alParams[1],
                        "ICHIBA_CD = @ICHIBA_CD AND SENSHU_CD = @SENSHU_CD",
                        (DbParamCollection)alParams[0]);
                }

                // トランザクションをコミット
                this.Dba.Commit();
                msg = (MODE_NEW.Equals(this.Par1) ? "登録" : "更新") + "しました。";
                Msg.Info(msg);
            }
            catch
            {
                msg = (MODE_NEW.Equals(this.Par1) ? "登録" : "更新") + "に失敗しました。";
                Msg.Info(msg);
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 船主コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFunanushiCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidFunanushiCd())
            {
                e.Cancel = true;
                this.txtFunanushiCd.SelectAll();
            }
        }

        /// <summary>
        /// アドレス1の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMailAddress1_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidMailAddress1())
            {
                e.Cancel = true;
                this.txtMailAddress1.SelectAll();
            }
        }

        /// <summary>
        /// アドレス2の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMailAddress2_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidMailAddress2())
            {
                e.Cancel = true;
                this.txtMailAddress2.SelectAll();
            }
        }

        /// <summary>
        /// アドレス3の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMailAddress3_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidMailAddress3())
            {
                e.Cancel = true;
                this.txtMailAddress3.SelectAll();
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 新規モードの初期表示
        /// </summary>
        private void InitDispOnNew()
        {
            // 正式船主名に初期フォーカス
            this.ActiveControl = this.txtFunanushiCd;
            this.txtFunanushiCd.Focus();

            // 削除ボタン非活性
            this.btnF3.Enabled = false;
        }

        /// <summary>
        /// 編集モードの初期表示
        /// </summary>
        private void InitDispOnEdit()
        {
            // 船主コードを編集不可に設定する
            this.txtFunanushiCd.Enabled = false;

            // 船主アドレステーブルと船主マスタからデータを取得して表示
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder Sql = new StringBuilder();

            // 船主マスタデータを取得
            Sql.Append("SELECT");
            Sql.Append(" A.SENSHU_CD AS SENSHU_CD,");
            Sql.Append(" CM.TORIHIKISAKI_NM AS SENSHU_NM,");
            Sql.Append(" A.MAIL_ADDRESS1 AS ADDRESS1,");
            Sql.Append(" A.ADDRESS1_FLG AS FLG1,");
            Sql.Append(" A.MAIL_ADDRESS2 AS ADDRESS2,");
            Sql.Append(" A.ADDRESS2_FLG AS FLG2,");
            Sql.Append(" A.MAIL_ADDRESS3 AS ADDRESS3,");
            Sql.Append(" A.ADDRESS3_FLG AS FLG3 ");
            Sql.Append("FROM");
            Sql.Append(" TB_HN_SERI_FUNANUSHU_ADDRESS AS A ");
            Sql.Append("LEFT OUTER JOIN");
            Sql.Append(" TB_CM_TORIHIKISAKI AS CM ");
            Sql.Append("ON");
            Sql.Append(" CM.KAISHA_CD = @KAISHA_CD AND CM.TORIHIKISAKI_CD = A.SENSHU_CD AND CM.HYOJI_FLG = @HYOJI_FLG ");
            Sql.Append("WHERE");
            Sql.Append(" A.ICHIBA_CD = @ICHIBA_CD");
            Sql.Append(" AND A.SENSHU_CD = @SENSHU_CD");

            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@HYOJI_FLG", SqlDbType.Decimal, 1, 0);
            dpc.SetParam("@ICHIBA_CD", SqlDbType.Decimal, 3, 108);
            dpc.SetParam("@SENSHU_CD", SqlDbType.Decimal, 4, Util.ToInt(InData));

            DataTable dt = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            if (dt.Rows.Count == 0)
            {
                Msg.Error("不正な起動です。終了します。");
                this.Close();
            }

            // 取得した内容を表示
            DataRow drDispData = dt.Rows[0];
            this.txtFunanushiCd.Text = Util.ToString(drDispData["SENSHU_CD"]); // 船主コード
            this.lblFunanushiNm.Text = Util.ToString(drDispData["SENSHU_NM"]); // 船主名
            this.txtMailAddress1.Text = Util.ToString(drDispData["ADDRESS1"]); // アドレス1
            this.txtMailAddress2.Text = Util.ToString(drDispData["ADDRESS2"]); // アドレス2
            this.txtMailAddress3.Text = Util.ToString(drDispData["ADDRESS3"]); // アドレス3
            // 有効フラグ
            if (Util.ToInt(drDispData["FLG1"]) == 0)
            {
                this.cbxFlg1.Checked = true;
            }
            if (Util.ToInt(drDispData["FLG2"]) == 0)
            {
                this.cbxFlg2.Checked = true;
            }
            if (Util.ToInt(drDispData["FLG3"]) == 0)
            {
                this.cbxFlg3.Checked = true;
            }

            this.btnF3.Enabled = true;
        }

        /// <summary>
        /// 船主コードの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidFunanushiCd()
        {
            // 未入力チェック
            if (ValChk.IsEmpty(this.txtFunanushiCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 数値チェック
            if (!ValChk.IsNumber(this.txtFunanushiCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 船主マスタに存在しないコードを入力した場合はエラーとする
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder Sql = new StringBuilder();
            Sql.Append("SELECT");
            Sql.Append(" CM.TORIHIKISAKI_NM AS SENSHU_NM ");
            Sql.Append("FROM");
            Sql.Append(" TB_HN_TORIHIKISAKI_JOHO AS T ");
            Sql.Append("LEFT OUTER JOIN");
            Sql.Append(" TB_CM_TORIHIKISAKI AS CM ");
            Sql.Append("ON");
            Sql.Append(" CM.KAISHA_CD = @KAISHA_CD AND CM.TORIHIKISAKI_CD = T.TORIHIKISAKI_CD AND CM.HYOJI_FLG = @HYOJI_FLG ");
            Sql.Append("WHERE");
            Sql.Append(" T.TORIHIKISAKI_KUBUN1 = @TORIHIKISAKI_KUBUN1");
            Sql.Append(" AND T.TORIHIKISAKI_CD = @TORIHIKISAKI_CD");

            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@HYOJI_FLG", SqlDbType.Decimal, 1, 0);
            dpc.SetParam("@TORIHIKISAKI_KUBUN1", SqlDbType.Decimal, 1, 1);
            dpc.SetParam("@TORIHIKISAKI_CD", SqlDbType.Decimal, 4, this.txtFunanushiCd.Text);

            DataTable dtF = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
            if (dtF.Rows.Count == 0)
            {
                Msg.Error("船主マスタに登録されていないコードです。");
                return false;
            }
            else
            {
                this.lblFunanushiNm.Text = Util.ToString(dtF.Rows[0]["SENSHU_NM"]); // 船主名

            }

            // 新規作成の場合、
            if (MODE_NEW.Equals(this.Par1))
            {
                // 船主アドレステーブルに既に存在するコードを入力した場合はエラーとする
                dpc = new DbParamCollection();
                dpc.SetParam("@ICHIBA_CD", SqlDbType.Decimal, 3, 108);
                dpc.SetParam("@SENSHU_CD", SqlDbType.Decimal, 4, this.txtFunanushiCd.Text);
                StringBuilder where = new StringBuilder("ICHIBA_CD = @ICHIBA_CD");
                where.Append(" AND SENSHU_CD = @SENSHU_CD");
                DataTable dtA =
                    this.Dba.GetDataTableByConditionWithParams("SENSHU_CD",
                        "TB_HN_SERI_FUNANUSHU_ADDRESS", Util.ToString(where), dpc);
                if (dtA.Rows.Count > 0)
                {
                    Msg.Error("既に存在する船主コードと重複しています。");
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// アドレス1の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidMailAddress1()
        {
            // 指定バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtMailAddress1.Text, this.txtMailAddress1.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// アドレス2の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidMailAddress2()
        {
            // 指定バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtMailAddress2.Text, this.txtMailAddress2.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// アドレス3の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidMailAddress3()
        {
            // 指定バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtMailAddress3.Text, this.txtMailAddress3.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            if (MODE_NEW.Equals(this.Par1))
            {
                // 船主コードのチェック
                if (!IsValidFunanushiCd())
                {
                    this.txtFunanushiCd.Focus();
                    return false;
                }
            }
            // アドレス1のチェック
            if (!IsValidMailAddress1())
            {
                this.txtMailAddress1.Focus();
                return false;
            }
            // アドレス2のチェック
            if (!IsValidMailAddress2())
            {
                this.txtMailAddress2.Focus();
                return false;
            }
            // アドレス3のチェック
            if (!IsValidMailAddress3())
            {
                this.txtMailAddress3.Focus();
                return false;
            }

            return true;
        }

        /// <summary>
        /// TB_HN_SERI_FUNANUSHU_ADDRESSに更新するためのパラメータ設定をします。
        /// </summary>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// 更新処理：DbParamCollection*2(Where句,Set句)
        /// </returns>
        private ArrayList SetParams()
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            if (MODE_NEW.Equals(this.Par1))
            {
                // 市場コードと船主コードを更新パラメータに設定
                updParam.SetParam("@ICHIBA_CD", SqlDbType.Decimal, 3, 108);
                updParam.SetParam("@SENSHU_CD", SqlDbType.Decimal, 4, this.txtFunanushiCd.Text);
                // 登録日
                updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWDATE");
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // 市場コードと船主コードをWhere句のパラメータに設定
                DbParamCollection whereParam = new DbParamCollection();
                whereParam.SetParam("@ICHIBA_CD", SqlDbType.Decimal, 3, 108);
                whereParam.SetParam("@SENSHU_CD", SqlDbType.Decimal, 4, this.txtFunanushiCd.Text);
                alParams.Add(whereParam);
            }

            // メールアドレス
            updParam.SetParam("@MAIL_ADDRESS1", SqlDbType.VarChar, 50, this.txtMailAddress1.Text);
            updParam.SetParam("@MAIL_ADDRESS2", SqlDbType.VarChar, 50, this.txtMailAddress2.Text);
            updParam.SetParam("@MAIL_ADDRESS3", SqlDbType.VarChar, 50, this.txtMailAddress3.Text);
            // アドレス有効フラグ
            int flg1 = 1;
            int flg2 = 1;
            int flg3 = 1;
            if (this.cbxFlg1.Checked)
            {
                flg1 = 0;
            }
            if (this.cbxFlg2.Checked)
            {
                flg2 = 0;
            }
            if (this.cbxFlg3.Checked)
            {
                flg3 = 0;
            }
            updParam.SetParam("@ADDRESS1_FLG", SqlDbType.Decimal, 1, flg1);
            updParam.SetParam("@ADDRESS2_FLG", SqlDbType.Decimal, 1, flg2);
            updParam.SetParam("@ADDRESS3_FLG", SqlDbType.Decimal, 1, flg3);

            alParams.Add(updParam);

            return alParams;
        }

        /// <summary>
        /// TB_HN_SERI_FUNANUSHU_ADDRESSからデータを削除する為のパラメータ
        /// </summary>
        /// <returns>
        /// </returns>
        private ArrayList SetDelParams()
        {
            // 市場コードと船主コードを削除パラメータに設定
            ArrayList alParams = new ArrayList();
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@ICHIBA_CD", SqlDbType.Decimal, 3, 108);
            dpc.SetParam("@SENSHU_CD", SqlDbType.Decimal, 4, this.txtFunanushiCd.Text);

            alParams.Add(dpc);

            return alParams;
        }
        #endregion
    }
}
