﻿namespace jp.co.fsi.hn.hndr1091
{
    partial class HNDR1091
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbxSeriDate = new System.Windows.Forms.GroupBox();
            this.lblCodeBetS = new System.Windows.Forms.Label();
            this.lblSeriDateDayTo = new System.Windows.Forms.Label();
            this.lblSeriDateMonthTo = new System.Windows.Forms.Label();
            this.lblSeriDateYearTo = new System.Windows.Forms.Label();
            this.txtDateDayTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtDateYearTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtDateMonthTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblDateGengoTo = new System.Windows.Forms.Label();
            this.lblSeriDateTo = new System.Windows.Forms.Label();
            this.lblSeriDateDayFr = new System.Windows.Forms.Label();
            this.lblSeriDateMonthFr = new System.Windows.Forms.Label();
            this.lblSeriDateYearFr = new System.Windows.Forms.Label();
            this.txtDateDayFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtDateYearFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtDateMonthFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblDateGengoFr = new System.Windows.Forms.Label();
            this.lblSeriDateFｒ = new System.Windows.Forms.Label();
            this.gbxSeisanKbn = new System.Windows.Forms.GroupBox();
            this.txtSeisanKbn = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblSeisanKbnNm = new System.Windows.Forms.Label();
            this.gbxFunanushiCd = new System.Windows.Forms.GroupBox();
            this.lblFunanushiCdTo = new System.Windows.Forms.Label();
            this.lblCodeBetF = new System.Windows.Forms.Label();
            this.txtFunanushiCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblFunanushiCdFr = new System.Windows.Forms.Label();
            this.txtFunanushiCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.gbxMizuageShisho = new System.Windows.Forms.GroupBox();
            this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMizuageShishoNm = new System.Windows.Forms.Label();
            this.lblMizuageShisho = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.gbxSeriDate.SuspendLayout();
            this.gbxSeisanKbn.SuspendLayout();
            this.gbxFunanushiCd.SuspendLayout();
            this.gbxMizuageShisho.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.TabIndex = 999;
            this.lblTitle.Text = "";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Size = new System.Drawing.Size(847, 100);
            // 
            // gbxSeriDate
            // 
            this.gbxSeriDate.Controls.Add(this.lblCodeBetS);
            this.gbxSeriDate.Controls.Add(this.lblSeriDateDayTo);
            this.gbxSeriDate.Controls.Add(this.lblSeriDateMonthTo);
            this.gbxSeriDate.Controls.Add(this.lblSeriDateYearTo);
            this.gbxSeriDate.Controls.Add(this.txtDateDayTo);
            this.gbxSeriDate.Controls.Add(this.txtDateYearTo);
            this.gbxSeriDate.Controls.Add(this.txtDateMonthTo);
            this.gbxSeriDate.Controls.Add(this.lblDateGengoTo);
            this.gbxSeriDate.Controls.Add(this.lblSeriDateTo);
            this.gbxSeriDate.Controls.Add(this.lblSeriDateDayFr);
            this.gbxSeriDate.Controls.Add(this.lblSeriDateMonthFr);
            this.gbxSeriDate.Controls.Add(this.lblSeriDateYearFr);
            this.gbxSeriDate.Controls.Add(this.txtDateDayFr);
            this.gbxSeriDate.Controls.Add(this.txtDateYearFr);
            this.gbxSeriDate.Controls.Add(this.txtDateMonthFr);
            this.gbxSeriDate.Controls.Add(this.lblDateGengoFr);
            this.gbxSeriDate.Controls.Add(this.lblSeriDateFｒ);
            this.gbxSeriDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.gbxSeriDate.ForeColor = System.Drawing.Color.Black;
            this.gbxSeriDate.Location = new System.Drawing.Point(12, 151);
            this.gbxSeriDate.Name = "gbxSeriDate";
            this.gbxSeriDate.Size = new System.Drawing.Size(526, 63);
            this.gbxSeriDate.TabIndex = 0;
            this.gbxSeriDate.TabStop = false;
            this.gbxSeriDate.Text = "セリ日付範囲";
            // 
            // lblCodeBetS
            // 
            this.lblCodeBetS.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblCodeBetS.ForeColor = System.Drawing.Color.Black;
            this.lblCodeBetS.Location = new System.Drawing.Point(253, 24);
            this.lblCodeBetS.Name = "lblCodeBetS";
            this.lblCodeBetS.Size = new System.Drawing.Size(18, 20);
            this.lblCodeBetS.TabIndex = 8;
            this.lblCodeBetS.Text = "～";
            this.lblCodeBetS.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSeriDateDayTo
            // 
            this.lblSeriDateDayTo.BackColor = System.Drawing.Color.Silver;
            this.lblSeriDateDayTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblSeriDateDayTo.ForeColor = System.Drawing.Color.Black;
            this.lblSeriDateDayTo.Location = new System.Drawing.Point(489, 25);
            this.lblSeriDateDayTo.Name = "lblSeriDateDayTo";
            this.lblSeriDateDayTo.Size = new System.Drawing.Size(20, 18);
            this.lblSeriDateDayTo.TabIndex = 16;
            this.lblSeriDateDayTo.Text = "日";
            this.lblSeriDateDayTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSeriDateMonthTo
            // 
            this.lblSeriDateMonthTo.BackColor = System.Drawing.Color.Silver;
            this.lblSeriDateMonthTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblSeriDateMonthTo.ForeColor = System.Drawing.Color.Black;
            this.lblSeriDateMonthTo.Location = new System.Drawing.Point(434, 25);
            this.lblSeriDateMonthTo.Name = "lblSeriDateMonthTo";
            this.lblSeriDateMonthTo.Size = new System.Drawing.Size(15, 19);
            this.lblSeriDateMonthTo.TabIndex = 14;
            this.lblSeriDateMonthTo.Text = "月";
            this.lblSeriDateMonthTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSeriDateYearTo
            // 
            this.lblSeriDateYearTo.BackColor = System.Drawing.Color.Silver;
            this.lblSeriDateYearTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblSeriDateYearTo.ForeColor = System.Drawing.Color.Black;
            this.lblSeriDateYearTo.Location = new System.Drawing.Point(381, 24);
            this.lblSeriDateYearTo.Name = "lblSeriDateYearTo";
            this.lblSeriDateYearTo.Size = new System.Drawing.Size(17, 21);
            this.lblSeriDateYearTo.TabIndex = 12;
            this.lblSeriDateYearTo.Text = "年";
            this.lblSeriDateYearTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDateDayTo
            // 
            this.txtDateDayTo.AutoSizeFromLength = false;
            this.txtDateDayTo.DisplayLength = null;
            this.txtDateDayTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtDateDayTo.ForeColor = System.Drawing.Color.Black;
            this.txtDateDayTo.Location = new System.Drawing.Point(456, 24);
            this.txtDateDayTo.MaxLength = 2;
            this.txtDateDayTo.Name = "txtDateDayTo";
            this.txtDateDayTo.Size = new System.Drawing.Size(30, 20);
            this.txtDateDayTo.TabIndex = 7;
            this.txtDateDayTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateDayTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtSeriDateDayTo_Validating);
            // 
            // txtDateYearTo
            // 
            this.txtDateYearTo.AutoSizeFromLength = false;
            this.txtDateYearTo.DisplayLength = null;
            this.txtDateYearTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtDateYearTo.ForeColor = System.Drawing.Color.Black;
            this.txtDateYearTo.Location = new System.Drawing.Point(349, 24);
            this.txtDateYearTo.MaxLength = 2;
            this.txtDateYearTo.Name = "txtDateYearTo";
            this.txtDateYearTo.Size = new System.Drawing.Size(30, 20);
            this.txtDateYearTo.TabIndex = 5;
            this.txtDateYearTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateYearTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtSeriDateYearTo_Validating);
            // 
            // txtDateMonthTo
            // 
            this.txtDateMonthTo.AutoSizeFromLength = false;
            this.txtDateMonthTo.DisplayLength = null;
            this.txtDateMonthTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtDateMonthTo.ForeColor = System.Drawing.Color.Black;
            this.txtDateMonthTo.Location = new System.Drawing.Point(402, 24);
            this.txtDateMonthTo.MaxLength = 2;
            this.txtDateMonthTo.Name = "txtDateMonthTo";
            this.txtDateMonthTo.Size = new System.Drawing.Size(30, 20);
            this.txtDateMonthTo.TabIndex = 6;
            this.txtDateMonthTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateMonthTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtSeriDateMonthTo_Validating);
            // 
            // lblDateGengoTo
            // 
            this.lblDateGengoTo.BackColor = System.Drawing.Color.Silver;
            this.lblDateGengoTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDateGengoTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblDateGengoTo.ForeColor = System.Drawing.Color.Black;
            this.lblDateGengoTo.Location = new System.Drawing.Point(304, 24);
            this.lblDateGengoTo.Name = "lblDateGengoTo";
            this.lblDateGengoTo.Size = new System.Drawing.Size(41, 21);
            this.lblDateGengoTo.TabIndex = 10;
            this.lblDateGengoTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSeriDateTo
            // 
            this.lblSeriDateTo.BackColor = System.Drawing.Color.Silver;
            this.lblSeriDateTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSeriDateTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblSeriDateTo.ForeColor = System.Drawing.Color.Black;
            this.lblSeriDateTo.Location = new System.Drawing.Point(301, 21);
            this.lblSeriDateTo.Name = "lblSeriDateTo";
            this.lblSeriDateTo.Size = new System.Drawing.Size(214, 27);
            this.lblSeriDateTo.TabIndex = 9;
            this.lblSeriDateTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSeriDateDayFr
            // 
            this.lblSeriDateDayFr.BackColor = System.Drawing.Color.Silver;
            this.lblSeriDateDayFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblSeriDateDayFr.ForeColor = System.Drawing.Color.Black;
            this.lblSeriDateDayFr.Location = new System.Drawing.Point(208, 25);
            this.lblSeriDateDayFr.Name = "lblSeriDateDayFr";
            this.lblSeriDateDayFr.Size = new System.Drawing.Size(20, 18);
            this.lblSeriDateDayFr.TabIndex = 7;
            this.lblSeriDateDayFr.Text = "日";
            this.lblSeriDateDayFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSeriDateMonthFr
            // 
            this.lblSeriDateMonthFr.BackColor = System.Drawing.Color.Silver;
            this.lblSeriDateMonthFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblSeriDateMonthFr.ForeColor = System.Drawing.Color.Black;
            this.lblSeriDateMonthFr.Location = new System.Drawing.Point(153, 25);
            this.lblSeriDateMonthFr.Name = "lblSeriDateMonthFr";
            this.lblSeriDateMonthFr.Size = new System.Drawing.Size(15, 19);
            this.lblSeriDateMonthFr.TabIndex = 5;
            this.lblSeriDateMonthFr.Text = "月";
            this.lblSeriDateMonthFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSeriDateYearFr
            // 
            this.lblSeriDateYearFr.BackColor = System.Drawing.Color.Silver;
            this.lblSeriDateYearFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblSeriDateYearFr.ForeColor = System.Drawing.Color.Black;
            this.lblSeriDateYearFr.Location = new System.Drawing.Point(100, 24);
            this.lblSeriDateYearFr.Name = "lblSeriDateYearFr";
            this.lblSeriDateYearFr.Size = new System.Drawing.Size(17, 21);
            this.lblSeriDateYearFr.TabIndex = 3;
            this.lblSeriDateYearFr.Text = "年";
            this.lblSeriDateYearFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDateDayFr
            // 
            this.txtDateDayFr.AutoSizeFromLength = false;
            this.txtDateDayFr.DisplayLength = null;
            this.txtDateDayFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtDateDayFr.ForeColor = System.Drawing.Color.Black;
            this.txtDateDayFr.Location = new System.Drawing.Point(175, 24);
            this.txtDateDayFr.MaxLength = 2;
            this.txtDateDayFr.Name = "txtDateDayFr";
            this.txtDateDayFr.Size = new System.Drawing.Size(30, 20);
            this.txtDateDayFr.TabIndex = 4;
            this.txtDateDayFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateDayFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtSeriDateDayFr_Validating);
            // 
            // txtDateYearFr
            // 
            this.txtDateYearFr.AutoSizeFromLength = false;
            this.txtDateYearFr.DisplayLength = null;
            this.txtDateYearFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtDateYearFr.ForeColor = System.Drawing.Color.Black;
            this.txtDateYearFr.Location = new System.Drawing.Point(68, 24);
            this.txtDateYearFr.MaxLength = 2;
            this.txtDateYearFr.Name = "txtDateYearFr";
            this.txtDateYearFr.Size = new System.Drawing.Size(30, 20);
            this.txtDateYearFr.TabIndex = 2;
            this.txtDateYearFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateYearFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtSeriDateYearFr_Validating);
            // 
            // txtDateMonthFr
            // 
            this.txtDateMonthFr.AutoSizeFromLength = false;
            this.txtDateMonthFr.DisplayLength = null;
            this.txtDateMonthFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtDateMonthFr.ForeColor = System.Drawing.Color.Black;
            this.txtDateMonthFr.Location = new System.Drawing.Point(121, 24);
            this.txtDateMonthFr.MaxLength = 2;
            this.txtDateMonthFr.Name = "txtDateMonthFr";
            this.txtDateMonthFr.Size = new System.Drawing.Size(30, 20);
            this.txtDateMonthFr.TabIndex = 3;
            this.txtDateMonthFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateMonthFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtSeriDateMonthFr_Validating);
            // 
            // lblDateGengoFr
            // 
            this.lblDateGengoFr.BackColor = System.Drawing.Color.Silver;
            this.lblDateGengoFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDateGengoFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblDateGengoFr.ForeColor = System.Drawing.Color.Black;
            this.lblDateGengoFr.Location = new System.Drawing.Point(23, 24);
            this.lblDateGengoFr.Name = "lblDateGengoFr";
            this.lblDateGengoFr.Size = new System.Drawing.Size(41, 21);
            this.lblDateGengoFr.TabIndex = 1;
            this.lblDateGengoFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSeriDateFｒ
            // 
            this.lblSeriDateFｒ.BackColor = System.Drawing.Color.Silver;
            this.lblSeriDateFｒ.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSeriDateFｒ.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblSeriDateFｒ.ForeColor = System.Drawing.Color.Black;
            this.lblSeriDateFｒ.Location = new System.Drawing.Point(20, 21);
            this.lblSeriDateFｒ.Name = "lblSeriDateFｒ";
            this.lblSeriDateFｒ.Size = new System.Drawing.Size(214, 27);
            this.lblSeriDateFｒ.TabIndex = 0;
            this.lblSeriDateFｒ.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // gbxSeisanKbn
            // 
            this.gbxSeisanKbn.Controls.Add(this.txtSeisanKbn);
            this.gbxSeisanKbn.Controls.Add(this.lblSeisanKbnNm);
            this.gbxSeisanKbn.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.gbxSeisanKbn.ForeColor = System.Drawing.Color.Black;
            this.gbxSeisanKbn.Location = new System.Drawing.Point(12, 234);
            this.gbxSeisanKbn.Name = "gbxSeisanKbn";
            this.gbxSeisanKbn.Size = new System.Drawing.Size(317, 69);
            this.gbxSeisanKbn.TabIndex = 1;
            this.gbxSeisanKbn.TabStop = false;
            this.gbxSeisanKbn.Text = "精算区分";
            // 
            // txtSeisanKbn
            // 
            this.txtSeisanKbn.AutoSizeFromLength = true;
            this.txtSeisanKbn.DisplayLength = null;
            this.txtSeisanKbn.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtSeisanKbn.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtSeisanKbn.Location = new System.Drawing.Point(22, 25);
            this.txtSeisanKbn.MaxLength = 5;
            this.txtSeisanKbn.Name = "txtSeisanKbn";
            this.txtSeisanKbn.Size = new System.Drawing.Size(51, 20);
            this.txtSeisanKbn.TabIndex = 8;
            this.txtSeisanKbn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSeisanKbn.Validating += new System.ComponentModel.CancelEventHandler(this.txtSeisanKbn_Validating);
            // 
            // lblSeisanKbnNm
            // 
            this.lblSeisanKbnNm.BackColor = System.Drawing.Color.Silver;
            this.lblSeisanKbnNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSeisanKbnNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblSeisanKbnNm.Location = new System.Drawing.Point(74, 26);
            this.lblSeisanKbnNm.Name = "lblSeisanKbnNm";
            this.lblSeisanKbnNm.Size = new System.Drawing.Size(212, 20);
            this.lblSeisanKbnNm.TabIndex = 4;
            this.lblSeisanKbnNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // gbxFunanushiCd
            // 
            this.gbxFunanushiCd.Controls.Add(this.lblFunanushiCdTo);
            this.gbxFunanushiCd.Controls.Add(this.lblCodeBetF);
            this.gbxFunanushiCd.Controls.Add(this.txtFunanushiCdFr);
            this.gbxFunanushiCd.Controls.Add(this.lblFunanushiCdFr);
            this.gbxFunanushiCd.Controls.Add(this.txtFunanushiCdTo);
            this.gbxFunanushiCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.gbxFunanushiCd.ForeColor = System.Drawing.Color.Black;
            this.gbxFunanushiCd.Location = new System.Drawing.Point(12, 324);
            this.gbxFunanushiCd.Name = "gbxFunanushiCd";
            this.gbxFunanushiCd.Size = new System.Drawing.Size(601, 76);
            this.gbxFunanushiCd.TabIndex = 2;
            this.gbxFunanushiCd.TabStop = false;
            this.gbxFunanushiCd.Text = "船主CD範囲";
            // 
            // lblFunanushiCdTo
            // 
            this.lblFunanushiCdTo.BackColor = System.Drawing.Color.Silver;
            this.lblFunanushiCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFunanushiCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblFunanushiCdTo.ForeColor = System.Drawing.Color.Black;
            this.lblFunanushiCdTo.Location = new System.Drawing.Point(359, 32);
            this.lblFunanushiCdTo.Name = "lblFunanushiCdTo";
            this.lblFunanushiCdTo.Size = new System.Drawing.Size(221, 20);
            this.lblFunanushiCdTo.TabIndex = 4;
            this.lblFunanushiCdTo.Text = "最　後";
            this.lblFunanushiCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCodeBetF
            // 
            this.lblCodeBetF.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblCodeBetF.ForeColor = System.Drawing.Color.Black;
            this.lblCodeBetF.Location = new System.Drawing.Point(291, 31);
            this.lblCodeBetF.Name = "lblCodeBetF";
            this.lblCodeBetF.Size = new System.Drawing.Size(18, 20);
            this.lblCodeBetF.TabIndex = 2;
            this.lblCodeBetF.Text = "～";
            this.lblCodeBetF.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFunanushiCdFr
            // 
            this.txtFunanushiCdFr.AutoSizeFromLength = false;
            this.txtFunanushiCdFr.DisplayLength = null;
            this.txtFunanushiCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtFunanushiCdFr.ForeColor = System.Drawing.Color.Black;
            this.txtFunanushiCdFr.Location = new System.Drawing.Point(23, 30);
            this.txtFunanushiCdFr.MaxLength = 4;
            this.txtFunanushiCdFr.Name = "txtFunanushiCdFr";
            this.txtFunanushiCdFr.Size = new System.Drawing.Size(40, 20);
            this.txtFunanushiCdFr.TabIndex = 9;
            this.txtFunanushiCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtCodeFr_Validating);
            // 
            // lblFunanushiCdFr
            // 
            this.lblFunanushiCdFr.BackColor = System.Drawing.Color.Silver;
            this.lblFunanushiCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFunanushiCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblFunanushiCdFr.ForeColor = System.Drawing.Color.Black;
            this.lblFunanushiCdFr.Location = new System.Drawing.Point(66, 31);
            this.lblFunanushiCdFr.Name = "lblFunanushiCdFr";
            this.lblFunanushiCdFr.Size = new System.Drawing.Size(221, 20);
            this.lblFunanushiCdFr.TabIndex = 1;
            this.lblFunanushiCdFr.Text = "先　頭";
            this.lblFunanushiCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFunanushiCdTo
            // 
            this.txtFunanushiCdTo.AutoSizeFromLength = false;
            this.txtFunanushiCdTo.DisplayLength = null;
            this.txtFunanushiCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtFunanushiCdTo.ForeColor = System.Drawing.Color.Black;
            this.txtFunanushiCdTo.Location = new System.Drawing.Point(316, 31);
            this.txtFunanushiCdTo.MaxLength = 4;
            this.txtFunanushiCdTo.Name = "txtFunanushiCdTo";
            this.txtFunanushiCdTo.Size = new System.Drawing.Size(40, 20);
            this.txtFunanushiCdTo.TabIndex = 10;
            this.txtFunanushiCdTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtFunanushiCdTo_KeyDown);
            this.txtFunanushiCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtCodeTo_Validating);
            // 
            // gbxMizuageShisho
            // 
            this.gbxMizuageShisho.Controls.Add(this.txtMizuageShishoCd);
            this.gbxMizuageShisho.Controls.Add(this.lblMizuageShishoNm);
            this.gbxMizuageShisho.Controls.Add(this.lblMizuageShisho);
            this.gbxMizuageShisho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.gbxMizuageShisho.ForeColor = System.Drawing.Color.Black;
            this.gbxMizuageShisho.Location = new System.Drawing.Point(12, 50);
            this.gbxMizuageShisho.Name = "gbxMizuageShisho";
            this.gbxMizuageShisho.Size = new System.Drawing.Size(381, 77);
            this.gbxMizuageShisho.TabIndex = 0;
            this.gbxMizuageShisho.TabStop = false;
            this.gbxMizuageShisho.Text = "水揚支所";
            // 
            // txtMizuageShishoCd
            // 
            this.txtMizuageShishoCd.AutoSizeFromLength = true;
            this.txtMizuageShishoCd.DisplayLength = null;
            this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtMizuageShishoCd.Location = new System.Drawing.Point(90, 33);
            this.txtMizuageShishoCd.MaxLength = 4;
            this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
            this.txtMizuageShishoCd.Size = new System.Drawing.Size(34, 20);
            this.txtMizuageShishoCd.TabIndex = 1;
            this.txtMizuageShishoCd.TabStop = false;
            this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
            // 
            // lblMizuageShishoNm
            // 
            this.lblMizuageShishoNm.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMizuageShishoNm.Location = new System.Drawing.Point(126, 34);
            this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
            this.lblMizuageShishoNm.Size = new System.Drawing.Size(212, 20);
            this.lblMizuageShishoNm.TabIndex = 2;
            this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMizuageShisho
            // 
            this.lblMizuageShisho.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShisho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShisho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblMizuageShisho.Location = new System.Drawing.Point(23, 31);
            this.lblMizuageShisho.Name = "lblMizuageShisho";
            this.lblMizuageShisho.Size = new System.Drawing.Size(320, 25);
            this.lblMizuageShisho.TabIndex = 0;
            this.lblMizuageShisho.Text = "水揚支所";
            this.lblMizuageShisho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // HNDR1091
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 638);
            this.Controls.Add(this.gbxMizuageShisho);
            this.Controls.Add(this.gbxFunanushiCd);
            this.Controls.Add(this.gbxSeriDate);
            this.Controls.Add(this.gbxSeisanKbn);
            this.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "HNDR1091";
            this.Text = "";
            this.Controls.SetChildIndex(this.gbxSeisanKbn, 0);
            this.Controls.SetChildIndex(this.gbxSeriDate, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.gbxFunanushiCd, 0);
            this.Controls.SetChildIndex(this.gbxMizuageShisho, 0);
            this.pnlDebug.ResumeLayout(false);
            this.gbxSeriDate.ResumeLayout(false);
            this.gbxSeriDate.PerformLayout();
            this.gbxSeisanKbn.ResumeLayout(false);
            this.gbxSeisanKbn.PerformLayout();
            this.gbxFunanushiCd.ResumeLayout(false);
            this.gbxFunanushiCd.PerformLayout();
            this.gbxMizuageShisho.ResumeLayout(false);
            this.gbxMizuageShisho.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbxSeriDate;
        private jp.co.fsi.common.controls.FsiTextBox txtDateYearFr;
        private System.Windows.Forms.Label lblDateGengoFr;
        private System.Windows.Forms.Label lblSeriDateFｒ;
        private System.Windows.Forms.Label lblSeriDateDayFr;
        private System.Windows.Forms.Label lblSeriDateMonthFr;
        private System.Windows.Forms.Label lblSeriDateYearFr;
        private jp.co.fsi.common.controls.FsiTextBox txtDateDayFr;
        private jp.co.fsi.common.controls.FsiTextBox txtDateMonthFr;
        private System.Windows.Forms.GroupBox gbxSeisanKbn;
        private System.Windows.Forms.GroupBox gbxFunanushiCd;
        private System.Windows.Forms.Label lblFunanushiCdTo;
        private System.Windows.Forms.Label lblCodeBetF;
        private common.controls.FsiTextBox txtFunanushiCdFr;
        private System.Windows.Forms.Label lblFunanushiCdFr;
        private common.controls.FsiTextBox txtFunanushiCdTo;
        private System.Windows.Forms.Label lblSeriDateDayTo;
        private System.Windows.Forms.Label lblSeriDateMonthTo;
        private System.Windows.Forms.Label lblSeriDateYearTo;
        private common.controls.FsiTextBox txtDateDayTo;
        private common.controls.FsiTextBox txtDateYearTo;
        private common.controls.FsiTextBox txtDateMonthTo;
        private System.Windows.Forms.Label lblDateGengoTo;
        private System.Windows.Forms.Label lblSeriDateTo;
        private System.Windows.Forms.Label lblCodeBetS;
        private System.Windows.Forms.GroupBox gbxMizuageShisho;
        private common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private System.Windows.Forms.Label lblMizuageShisho;
        private common.controls.FsiTextBox txtSeisanKbn;
        private System.Windows.Forms.Label lblSeisanKbnNm;
    }
}