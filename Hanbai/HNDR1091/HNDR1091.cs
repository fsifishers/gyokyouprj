﻿using System;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Reflection;

using GrapeCity.ActiveReports;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.hn.hndr1091
{
    /// <summary>
    /// 支払明細書出力(HNDR1091)
    /// </summary>
    public partial class HNDR1091 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// 精算区分(地区内ｾﾘ)
        /// </summary>
        private const string CHIKUNAI = "3";

        /// <summary>
        /// 精算区分(浜売り)
        /// </summary>
        private const string HAMA_URI = "2";

        /// <summary>
        /// 精算区分(地区外ｾﾘ)
        /// </summary>
        private const string CHIKUGAI = "1";

        /// <summary>
        /// 明細行数
        /// </summary>
        private const int rptRowCnt = 23;

        /// <summary>
        /// レポート項目数
        /// </summary>
        private const int rptItemCnt = 27;
        #endregion

        #region プロパティ
        /// <summary>
        /// 画面上最後となるフォーカスのEnterボタン押下時処理用変数
        /// </summary>
        private bool _dtFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._dtFlg;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HNDR1091()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            string[] jpDate = Util.ConvJpDate(DateTime.Now, this.Dba);
            // 日付範囲前
            lblDateGengoFr.Text = jpDate[0];
            txtDateYearFr.Text = jpDate[2];
            txtDateMonthFr.Text = jpDate[3];
            txtDateDayFr.Text = jpDate[4];
            // 日付範囲後
            lblDateGengoTo.Text = jpDate[0];
            txtDateYearTo.Text = jpDate[2];
            txtDateMonthTo.Text = jpDate[3];
            txtDateDayTo.Text = jpDate[4];
            // 精算区分地区内ｾﾘにチェック
            this.txtSeisanKbn.Text = "0";
            IsValidSeisanKbn();
#if DEBUG
            // 水揚支所
            this.txtMizuageShishoCd.Text = "1";
#else
            // 水揚支所
            this.txtMizuageShishoCd.Text = this.UInfo.ShishoCd; //"1"; 
#endif
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
            this.txtMizuageShishoCd.Enabled = (this.txtMizuageShishoCd.Text == "1") ? true : false;

            // フォーカス設定
            this.txtDateYearFr.Focus();
            // Enter処理を無効化
            this._dtFlg = false;
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 日付(年)、船主コード範囲にフォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd":
                case "txtDateYearFr":
                case "txtDateYearTo":
                case "txtFunanushiCdFr":
                case "txtFunanushiCdTo":
                case "txtSeisanKbn":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;
            String[] result;
            string stCurrentDir = System.IO.Directory.GetCurrentDirectory();

            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd": // 水揚支所
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM2031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2031.CMCM2031");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.InData = this.txtMizuageShishoCd.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.txtMizuageShishoCd.Text = result[0];
                                this.lblMizuageShishoNm.Text = result[1];
                            }
                        }
                    }
                    break;

                case "txtDateYearFr":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblDateGengoFr.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.lblDateGengoFr.Text = result[1];

                                // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
                                DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                                    this.txtDateMonthFr.Text, "1", this.Dba);
                                int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);
                                if (Util.ToInt(this.txtDateDayFr.Text) > lastDayInMonth)
                                {
                                    this.txtDateDayFr.Text = Util.ToString(lastDayInMonth);
                                }

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblDateGengoFr.Text,
                                        this.txtDateYearFr.Text,
                                        this.txtDateMonthFr.Text,
                                        this.txtDateDayFr.Text,
                                        this.Dba);
                                this.lblDateGengoFr.Text = arrJpDate[0];
                                this.txtDateYearFr.Text = arrJpDate[2];
                                this.txtDateMonthFr.Text = arrJpDate[3];
                                this.txtDateDayFr.Text = arrJpDate[4];
                            }
                        }
                    }
                    break;

                case "txtDateYearTo":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblDateGengoTo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.lblDateGengoTo.Text = result[1];

                                // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
                                DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                                    this.txtDateMonthTo.Text, "1", this.Dba);
                                int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);
                                if (Util.ToInt(this.txtDateDayTo.Text) > lastDayInMonth)
                                {
                                    this.txtDateDayTo.Text = Util.ToString(lastDayInMonth);
                                }

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblDateGengoTo.Text,
                                        this.txtDateYearTo.Text,
                                        this.txtDateMonthTo.Text,
                                        this.txtDateDayTo.Text,
                                        this.Dba);
                                this.lblDateGengoTo.Text = arrJpDate[0];
                                this.txtDateYearTo.Text = arrJpDate[2];
                                this.txtDateMonthTo.Text = arrJpDate[3];
                                this.txtDateDayTo.Text = arrJpDate[4];
                            }
                        }
                    }
                    break;

                case "txtFunanushiCdFr":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM2011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2011.CMCM2011");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtFunanushiCdFr.Text = outData[0];
                                this.lblFunanushiCdFr.Text = outData[1];
                            }
                        }
                    }
                    break;

                case "txtFunanushiCdTo":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM2011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2011.CMCM2011");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtFunanushiCdTo.Text = outData[0];
                                this.lblFunanushiCdTo.Text = outData[1];
                            }
                        }
                    }
                    break;

                case "txtSeisanKbn":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"CMCM1041.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1041.CMCM1041");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "TB_HN_COMBO_DATA_SEISAN";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtSeisanKbn.Text = outData[0];
                                this.lblSeisanKbnNm.Text = outData[1];
                            }
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            // プレビュー処理
            DoPrint(true);
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfYesNo("実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }

        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            // 設定画面の起動
            // MEMO:原則としてここで渡す帳票IDの設定はReport.csvに保持していることが前提ですが、
            // 保持していない場合は、設定画面での保存(F6)時に新規に設定が保持されます。
            PrintSettingForm psForm = new PrintSettingForm(new string[1] { "HNDR1091R" });
            psForm.ShowDialog();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 水揚支所入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMizuageShishoCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                e.Cancel = true;
                this.txtMizuageShishoCd.SelectAll();
                this.txtMizuageShishoCd.Focus();
            }
        }
        /// <summary>
        /// 年(自)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSeriDateYearFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtDateYearFr.Text, this.txtDateYearFr.MaxLength))
            {
                e.Cancel = true;
                this.txtDateYearFr.SelectAll();
            }
            else
            {
                this.txtDateYearFr.Text = Util.ToString(IsValid.SetYear(this.txtDateYearFr.Text));
                CheckSeriDateFr();
                SetSeriDateFr();
            }
        }

        /// <summary>
        /// 月(自)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSeriDateMonthFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtDateMonthFr.Text, this.txtDateMonthFr.MaxLength))
            {
                e.Cancel = true;
                this.txtDateMonthFr.SelectAll();
            }
            else
            {
                this.txtDateMonthFr.Text = Util.ToString(IsValid.SetMonth(this.txtDateMonthFr.Text));
                CheckSeriDateFr();
                SetSeriDateFr();
            }
        }

        /// <summary>
        /// 日(自)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSeriDateDayFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsDay(this.txtDateDayFr.Text, this.txtDateDayFr.MaxLength))
            {
                e.Cancel = true;
                this.txtDateDayFr.SelectAll();
            }
            else
            {
                this.txtDateDayFr.Text = Util.ToString(IsValid.SetDay(this.txtDateDayFr.Text));
                CheckSeriDateFr();
                SetSeriDateFr();
            }
        }

        /// <summary>
        /// 年(至)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSeriDateYearTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtDateYearTo.Text, this.txtDateYearTo.MaxLength))
            {
                e.Cancel = true;
                this.txtDateYearTo.SelectAll();
            }
            else
            {
                this.txtDateYearTo.Text = Util.ToString(IsValid.SetYear(this.txtDateYearTo.Text));
                CheckSeriDateTo();
                SetSeriDateTo();
            }
        }

        /// <summary>
        /// 月(至)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSeriDateMonthTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtDateMonthTo.Text, this.txtDateMonthTo.MaxLength))
            {
                e.Cancel = true;
                this.txtDateMonthTo.SelectAll();
            }
            else
            {
                this.txtDateMonthTo.Text = Util.ToString(IsValid.SetMonth(this.txtDateMonthTo.Text));
                CheckSeriDateTo();
                SetSeriDateTo();
            }
        }

        /// <summary>
        /// 日(至)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSeriDateDayTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsDay(this.txtDateDayTo.Text, this.txtDateDayTo.MaxLength))
            {
                e.Cancel = true;
                this.txtDateDayTo.SelectAll();
            }
            else
            {
                this.txtDateDayTo.Text = Util.ToString(IsValid.SetDay(this.txtDateDayTo.Text));
                CheckSeriDateTo();
                SetSeriDateTo();
            }
        }

        /// <summary>
        /// 船主コード(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCodeFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidFunanushiCdFr())
            {
                e.Cancel = true;
                this.txtFunanushiCdFr.SelectAll();
            }
        }

        /// <summary>
        /// 船主コード(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCodeTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidFunanushiCdTo())
            {
                e.Cancel = true;
                this.txtFunanushiCdTo.SelectAll();
                // Enter処理を無効化
                this._dtFlg = false;
            }
            // Enter処理を有効化
            this._dtFlg = true;
        }

        /// <summary>
        /// 精算区分の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSeisanKbn_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidSeisanKbn())
            {
                e.Cancel = true;
                this.txtSeisanKbn.SelectAll();
            }
        }

        private void txtFunanushiCdTo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.Flg)
            {
                // Enter処理を無効化
                this._dtFlg = false;

                // 全項目を再度入力値チェック
                if (!ValidateAll())
                {
                    // エラーありの場合ここで処理終了
                    return;
                }

                if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
                {
                    // ﾌﾟﾚﾋﾞｭｰ処理
                    DoPrint(true);
                }
                else
                {
                    this.txtFunanushiCdTo.Focus();
                }
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 水揚支所の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool IsValidMizuageShishoCd()
        {
            // 空 又は 0入力の場合
            if (ValChk.IsEmpty(this.txtMizuageShishoCd.Text) || Equals(this.txtMizuageShishoCd.Text, "0"))
            {
                // 水揚支所名称を表示する
                this.txtMizuageShishoCd.Text = "0";
                this.lblMizuageShishoNm.Text = "全て";
                return true;
            }
            // 水揚支所名称を表示する
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);
            if (ValChk.IsEmpty(this.lblMizuageShishoNm.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }
            return true;
        }

        /// <summary>
        /// 年月日(自)の月末入力チェック
        /// </summary>
        /// 
        private void CheckSeriDateFr()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                this.txtDateMonthFr.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDateDayFr.Text) > lastDayInMonth)
            {
                this.txtDateDayFr.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(自)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetSeriDateFr()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetSeriDateFr(Util.FixJpDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                this.txtDateMonthFr.Text, this.txtDateDayFr.Text, this.Dba));
        }

        /// <summary>
        /// 年月日(至)の月末入力チェック
        /// </summary>
        /// 
        private void CheckSeriDateTo()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                this.txtDateMonthTo.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDateDayTo.Text) > lastDayInMonth)
            {
                this.txtDateDayTo.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(至)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetSeriDateTo()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetSeriDateTo(Util.FixJpDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                this.txtDateMonthTo.Text, this.txtDateDayTo.Text, this.Dba));
        }

        /// <summary>
        /// 船主コード(自)の入力チェック
        /// </summary>
        private bool IsValidFunanushiCdFr()
        {
            if (ValChk.IsEmpty(this.txtFunanushiCdFr.Text))
            {
                this.lblFunanushiCdFr.Text = "先　頭";
            }
            else
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtFunanushiCdFr.Text))
            {
                Msg.Notice("コード(自)は数値のみで入力してください。");
                return false;
            }
            else
            {
                // コードを元に名称を取得する
                this.lblFunanushiCdFr.Text = this.Dba.GetName(this.UInfo, "VI_HN_FUNANUSHI", "", this.txtFunanushiCdFr.Text);
            }

            return true;
        }

        /// <summary>
        /// 船主コード(至)の入力チェック
        /// </summary>
        private bool IsValidFunanushiCdTo()
        {
            if (ValChk.IsEmpty(this.txtFunanushiCdTo.Text))
            {
                this.lblFunanushiCdTo.Text = "最　後";
            }
            else
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtFunanushiCdTo.Text))
            {
                Msg.Notice("コード(至)は数値のみで入力してください。");
                return false;
            }
            else
            {
                // コードを元に名称を取得する
                this.lblFunanushiCdTo.Text = this.Dba.GetName(this.UInfo, "VI_HN_FUNANUSHI", "", this.txtFunanushiCdTo.Text);
            }

            return true;
        }

        /// <summary>
        /// 精算区分の入力チェック
        /// </summary>
        private bool IsValidSeisanKbn()
        {
            // 最大桁数チェック
            if (!ValChk.IsWithinLength(this.txtSeisanKbn.Text, this.txtSeisanKbn.MaxLength))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }
            if (ValChk.IsEmpty(this.txtSeisanKbn.Text) || this.txtSeisanKbn.Text == "0")
            {
                this.txtSeisanKbn.Text = "0";
                this.lblSeisanKbnNm.Text = "全て";
            }
            else
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtSeisanKbn.Text))
            {
                Msg.Notice("精算区分は数値のみで入力してください。");
                return false;
            }
            else
            {
                // コードを元に名称を取得する
                this.lblSeisanKbnNm.Text = this.Dba.GetName(this.UInfo, "TB_HN_COMBO_DATA_SEISAN", this.txtMizuageShishoCd.Text, this.txtSeisanKbn.Text);
                if (ValChk.IsEmpty(this.lblSeisanKbnNm.Text))
                {
                    Msg.Notice("入力に誤りがあります。");
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 水揚支所の入力チェック
            if (!IsValidMizuageShishoCd())
            {
                this.txtMizuageShishoCd.Focus();
                this.txtMizuageShishoCd.SelectAll();
                return false;
            }
            // 年(自)のチェック
            if (!IsValid.IsYear(this.txtDateYearFr.Text, this.txtDateYearFr.MaxLength))
            {
                this.txtDateYearFr.Focus();
                this.txtDateYearFr.SelectAll();
                return false;
            }
            // 月(自)のチェック
            if (!IsValid.IsMonth(this.txtDateMonthFr.Text, this.txtDateMonthFr.MaxLength))
            {
                this.txtDateMonthFr.Focus();
                this.txtDateMonthFr.SelectAll();
                return false;
            }
            // 日(自)のチェック
            if (!IsValid.IsDay(this.txtDateDayFr.Text, this.txtDateDayFr.MaxLength))
            {
                this.txtDateDayFr.Focus();
                this.txtDateDayFr.SelectAll();
                return false;
            }
            // 年月日(自)の月末入力チェック処理
            CheckSeriDateFr();
            // 年月日(自)の正しい和暦への変換処理
            SetSeriDateFr();

            // 年(至)のチェック
            if (!IsValid.IsYear(this.txtDateYearTo.Text, this.txtDateYearTo.MaxLength))
            {
                this.txtDateYearTo.Focus();
                this.txtDateYearTo.SelectAll();
                return false;
            }
            // 月(至)のチェック
            if (!IsValid.IsMonth(this.txtDateMonthTo.Text, this.txtDateMonthTo.MaxLength))
            {
                this.txtDateMonthTo.Focus();
                this.txtDateMonthTo.SelectAll();
                return false;
            }
            // 日(至)のチェック
            if (!IsValid.IsDay(this.txtDateDayTo.Text, this.txtDateDayTo.MaxLength))
            {
                this.txtDateDayTo.Focus();
                this.txtDateDayTo.SelectAll();
                return false;
            }
            // 年月日(至)の月末入力チェック処理
            CheckSeriDateTo();
            // 年月日(至)の正しい和暦への変換処理
            SetSeriDateTo();

            // 船主コード(自)の入力チェック
            if (!IsValidFunanushiCdFr())
            {
                this.txtFunanushiCdFr.Focus();
                this.txtFunanushiCdFr.SelectAll();
                return false;
            }
            // 船主コード(至)の入力チェック
            if (!IsValidFunanushiCdTo())
            {
                this.txtFunanushiCdTo.Focus();
                this.txtFunanushiCdTo.SelectAll();
                return false;
            }

            if (!IsValidSeisanKbn())
            {
                this.txtSeisanKbn.Focus();
                this.txtSeisanKbn.SelectAll();
                return false;
            }
            return true;
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetSeriDateFr(string[] arrJpDate)
        {
            this.lblDateGengoFr.Text = arrJpDate[0];
            this.txtDateYearFr.Text = arrJpDate[2];
            this.txtDateMonthFr.Text = arrJpDate[3];
            this.txtDateDayFr.Text = arrJpDate[4];
        }
        private void SetSeriDateTo(string[] arrJpDate)
        {
            this.lblDateGengoTo.Text = arrJpDate[0];
            this.txtDateYearTo.Text = arrJpDate[2];
            this.txtDateMonthTo.Text = arrJpDate[3];
            this.txtDateDayTo.Text = arrJpDate[4];
        }

        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview)
        {
            bool dataFlag;
            try
            {
#if DEBUG
                //ワークテーブルのクリア
                this.Dba.DeleteWork("PR_HN_TBL", this.UnqId);
#endif

                this.Dba.BeginTransaction();

                // 帳票出力用にワークテーブルにデータを作成
                dataFlag = MakeWkData();
                //this.Dba.Commit();

                // 帳票出力
                if (dataFlag)
                {
                    // 取得列の定義
                    StringBuilder cols = Util.ColsArray(126, "");

                    // バインドパラメータの設定
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);

                    // データの取得
                    DataTable dtOutput = this.Dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_HN_TBL", "GUID = @GUID", "SORT ASC", dpc);

                    // 帳票オブジェクトをインスタンス化
                    HNDR1091R rpt = new HNDR1091R(dtOutput);
                    rpt.Document.Printer.DocumentName = this.Text;
                    rpt.Document.Name = this.Text;

                    if (isPreview)
                    {
                        // プレビュー画面表示
                        PreviewForm pFrm = new PreviewForm(rpt, this.UnqId);
                        pFrm.WindowState = FormWindowState.Maximized;
                        pFrm.Show();
                    }
                    else
                    {
                        // 直接印刷
                        rpt.Run(false);
                        rpt.Document.Print(true, true, false);
                    }
                }
            }
            finally
            {
#if DEBUG
                this.Dba.Commit();
#else
                this.Dba.Rollback();
#endif
            }
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData()
        {
            #region データ取得の準備
            // 日付(セリ日付)を西暦にして取得
            DateTime tmpSeriDateFr = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                    this.txtDateMonthFr.Text, this.txtDateDayFr.Text, this.Dba);
            // 日付(支払予定日)を西暦にして取得
            DateTime tmpSeriDateTo = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                    this.txtDateMonthTo.Text, this.txtDateDayTo.Text, this.Dba);
            // 日付を和暦で保持
            string[] jpDate = Util.ConvJpDate(DateTime.Now, this.Dba);
            string[] tmpSeriWarekiFr = Util.ConvJpDate(tmpSeriDateFr, this.Dba);
            string[] tmpSeriWarekiTo = Util.ConvJpDate(tmpSeriDateTo, this.Dba);
            // 日付の0詰め設定
            jpDate[3] = jpDate[3].PadLeft(2, '0');
            jpDate[4] = jpDate[4].PadLeft(2, '0');
            tmpSeriWarekiFr[3] = tmpSeriWarekiFr[3].PadLeft(2, '0');
            tmpSeriWarekiFr[4] = tmpSeriWarekiFr[4].PadLeft(2, '0');
            tmpSeriWarekiTo[3] = tmpSeriWarekiTo[3].PadLeft(2, '0');
            tmpSeriWarekiTo[4] = tmpSeriWarekiTo[4].PadLeft(2, '0');

            string outDate = string.Format("{0} {1}年 {2}月 {3}日", jpDate[0], jpDate[2], jpDate[3], jpDate[4]);
            string taishoKikan = string.Format("{0} {1}年 {2}月 {3}日", tmpSeriWarekiFr[0], tmpSeriWarekiFr[2], tmpSeriWarekiFr[3], tmpSeriWarekiFr[4])
                          + " ～ "
                          + string.Format("{0} {1}年 {2}月 {3}日", tmpSeriWarekiTo[0], tmpSeriWarekiTo[2], tmpSeriWarekiTo[3], tmpSeriWarekiTo[4]);
            // 船主コード設定
            string FUNANUSHI_CD_FR;
            string FUNANUSHI_CD_TO;
            if (Util.ToDecimal(txtFunanushiCdFr.Text) > 0)
            {
                FUNANUSHI_CD_FR = txtFunanushiCdFr.Text;
            }
            else
            {
                FUNANUSHI_CD_FR = "0";
            }
            if (Util.ToDecimal(txtFunanushiCdTo.Text) > 0)
            {
                FUNANUSHI_CD_TO = txtFunanushiCdTo.Text;
            }
            else
            {
                FUNANUSHI_CD_TO = "99999";
            }

            // 精算区分設定
            string seisanKubunSettei = this.txtSeisanKbn.Text;
            ///会社情報の取得
            string jyusho = Util.ToString(this.UInfo.KaikeiSettings["JUSHO1"]) +
                            Util.ToString(this.UInfo.KaikeiSettings["JUSHO2"]) +
                            Util.ToString(this.UInfo.KaikeiSettings["JUSHO3"]);
            string tel = Util.ToString(this.UInfo.KaikeiSettings["DENWA_BANGO"]);
            string fax = Util.ToString(this.UInfo.KaikeiSettings["FAX_BANGO"]);

            int dbSORT = 1;
            int dbFunanushiSortCont = 1;
            string dbFunanushiSORT = "";
            int i = 0;                      // メインループ用カウント変数
            int j = 0;                      // サブカウント変数
            int k = 1;                      // ACCESS改ページ判断用カウント変数
            int pageCount = 1;              // ACCESS改ページ用カウント変数
            string tmpKojoNm = "";          // 控除名変数
            int tmpKojoKingaku = 0;         // 控除金額変数
            int tmpLastPageData = 0;        // 船主毎最終頁表示データ残数
            int tmpKojoHyojiCount = 1;      // 控除項目表示数用変数
            decimal tmpMeisaiCount = 0;     // 船主データ件数変数
            int tmpPage = 0;                // 船主CD毎の件数によるﾍﾟｰｼﾞ設定変数
            int before = -1;                // 改ページ比較用カウント変数
            int next = 1;                   // 改ページ比較用カウント変数
            int n = 0;                      // 空行表示カウント変数
            int lastNum = i;                // 最終残高表示用変数
            int flag = 0;                   // 空行表示判断用変数
            string shishoCd = this.txtMizuageShishoCd.Text;

            DbParamCollection dpc = new DbParamCollection();
            StringBuilder Sql = new StringBuilder();

            // han.TM_控除科目設定(TM_HN_KOJO_KAMOKU_SETTEI)からデータを取得
            Sql.Append(" SELECT");
            Sql.Append(" SETTEI_CD, KOJO_KOMOKU_NM");
            Sql.Append(" FROM");
            Sql.Append(" TB_HN_KOJO_KAMOKU_SETTEI");
            Sql.Append(" WHERE ");
            Sql.Append(" KAISHA_CD = @KAISHA_CD AND ");
            Sql.Append(" SHISHO_CD = @SHISHO_CD ");
            Sql.Append(" ORDER BY");
            Sql.Append(" SETTEI_CD");
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            DataTable dtSubLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
            string[] tmpKK = new string[dtSubLoop.Rows.Count];          // 控除項目変数
            // カラム名を準備
            while (dtSubLoop.Rows.Count > j)
            {
                tmpKK[j] = "KOJO_KOMOKU" + Util.ToInt(dtSubLoop.Rows[j]["SETTEI_CD"]);
                j++;
            }
            j = 0; // サブカウント変数の初期化
            #endregion

            #region メインデータ取得
            dpc = new DbParamCollection();
            Sql = new StringBuilder();

            // han.VI_水揚仕切書(VI_HN_MIZUAGE_SHIKIRISHO)の日付から発生しているデータを取得
            Sql.Append(" SELECT");
            Sql.Append(" B.SENSHU_CD,");
            Sql.Append(" B.SENSHU_NM,");
            Sql.Append(" B.TSUKIHI,");
            Sql.Append(" COUNT(B.SENSHU_CD) AS KENSU,");
            Sql.Append(" SUM(B.SURYO) AS SURYO,");
            //Sql.Append(" SUM(B.HEIKIN_TANKA) AS HEIKIN_TANKA,");
            Sql.Append(" dbo.FNC_MarumeS(SUM(B.MIZUAGE_KINGAKU) / SUM(B.SURYO), 2, 2) AS HEIKIN_TANKA,");
            Sql.Append(" SUM(B.MIZUAGE_KINGAKU) AS MIZUAGE_KINGAKU,");
            Sql.Append(" SUM(B.KOJO_KINGAKU) AS KOJO ");
            Sql.Append(" FROM");
            Sql.Append(" TB_CM_TORIHIKISAKI AS A");
            Sql.Append(" LEFT OUTER JOIN");
            Sql.Append(" VI_HN_SHIHARAI_MEISAI AS B");
            Sql.Append(" ON");
            Sql.Append(" A.KAISHA_CD = B.KAISHA_CD AND");
            Sql.Append(" A.TORIHIKISAKI_CD = B.SENSHU_CD");
            Sql.Append(" WHERE");
            Sql.Append(" B.KAISHA_CD = @KAISHA_CD AND");
            Sql.Append(" B.SEISAN_BI BETWEEN @DATE_FR AND @DATE_TO AND");
            if (shishoCd != "0")
            {
                Sql.Append(" B.SHISHO_CD = @SHISHO_CD AND");
            }
            if (seisanKubunSettei != "0")
            {
                Sql.Append(" B.SEISAN_KUBUN = @SEISAN_KUBUN AND");
            }
            Sql.Append(" B.SENSHU_CD BETWEEN @FUNANUSHI_CD_FR AND @FUNANUSHI_CD_TO");
            Sql.Append(" GROUP BY");
            Sql.Append(" B.KAISHA_CD,");
            if (shishoCd != "0")
            {
                Sql.Append(" B.SHISHO_CD,");
            }
            Sql.Append(" B.SENSHU_CD,");
            Sql.Append(" B.SENSHU_NM,");
            Sql.Append(" B.SEISAN_BI,");
            //Sql.Append(" B.HEIKIN_TANKA,");
            Sql.Append(" B.TSUKIHI");
            Sql.Append(" ORDER BY");
            Sql.Append(" B.SENSHU_CD,");
            Sql.Append(" B.SEISAN_BI,");
            Sql.Append(" B.TSUKIHI");
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd); 
            dpc.SetParam("@DATE_FR", SqlDbType.VarChar, 10, tmpSeriDateFr.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@DATE_TO", SqlDbType.VarChar, 10, tmpSeriDateTo.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@FUNANUSHI_CD_FR", SqlDbType.VarChar, 6, FUNANUSHI_CD_FR);
            dpc.SetParam("@FUNANUSHI_CD_TO", SqlDbType.VarChar, 6, FUNANUSHI_CD_TO);
            dpc.SetParam("@SEISAN_KUBUN", SqlDbType.VarChar, 1, seisanKubunSettei);
            DataTable dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
            #endregion

            if (dtMainLoop.Rows.Count == 0)
            {
                Msg.Info("該当データがありません。");
                return false;
            }
            else
            {
                OrderedDictionary kojoNmData = new OrderedDictionary();
                OrderedDictionary kojoKingakuData = new OrderedDictionary();

                while (dtMainLoop.Rows.Count > i)
                {
                    #region 船主別のデータ件数を取得
                    // 現在の船主コードと前の船主コードが一致しない場合
                    if (i == 0)
                    {
                        dpc = new DbParamCollection();
                        Sql = new StringBuilder();

                        Sql.Append(" SELECT");
                        Sql.Append(" COUNT");
                        Sql.Append(" (SENSHU_CD) AS MEISAI_COUNT");
                        Sql.Append(" FROM");
                        Sql.Append(" VI_HN_SHIHARAI_MEISAI");
                        Sql.Append(" WHERE");
                        Sql.Append(" KAISHA_CD = @KAISHA_CD AND");
                        Sql.Append(" SEISAN_BI BETWEEN @DATE_FR AND @DATE_TO AND");
                        if (shishoCd != "0")
                        {
                            Sql.Append(" SHISHO_CD = @SHISHO_CD AND");
                        }
                        if (seisanKubunSettei != "0")
                        {
                            Sql.Append(" SEISAN_KUBUN = @SEISAN_KUBUN AND");
                        }
                        Sql.Append(" SENSHU_CD = @SENSHU_CD ");
                        dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                        dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
                        dpc.SetParam("@DATE_FR", SqlDbType.VarChar, 10, tmpSeriDateFr.Date.ToString("yyyy/MM/dd"));
                        dpc.SetParam("@DATE_TO", SqlDbType.VarChar, 10, tmpSeriDateTo.Date.ToString("yyyy/MM/dd"));
                        dpc.SetParam("@SENSHU_CD", SqlDbType.VarChar, 6, Util.ToInt(dtMainLoop.Rows[i]["SENSHU_CD"]));
                        dpc.SetParam("@SEISAN_KUBUN", SqlDbType.VarChar, 1, seisanKubunSettei);

                        DataTable dtFunanushiCount = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
                        tmpMeisaiCount = (decimal)(Util.ToInt(dtFunanushiCount.Rows[0]["MEISAI_COUNT"]) - 1) / 20;
                        tmpPage = (int)Math.Truncate(tmpMeisaiCount) + 1;
                        tmpLastPageData = Util.ToInt(dtFunanushiCount.Rows[0]["MEISAI_COUNT"]) - (int)Math.Truncate(tmpMeisaiCount) * 20;
                    }
                    else if (Util.ToInt(dtMainLoop.Rows[i]["SENSHU_CD"]) != Util.ToInt(dtMainLoop.Rows[before]["SENSHU_CD"]))
                    {
                        dpc = new DbParamCollection();
                        Sql = new StringBuilder();

                        Sql.Append(" SELECT");
                        Sql.Append(" COUNT");
                        Sql.Append(" (SENSHU_CD) AS MEISAI_COUNT");
                        Sql.Append(" FROM");
                        Sql.Append(" VI_HN_SHIHARAI_MEISAI");
                        Sql.Append(" WHERE");
                        Sql.Append(" KAISHA_CD = @KAISHA_CD AND");
                        Sql.Append(" SEISAN_BI BETWEEN @DATE_FR AND @DATE_TO AND");
                        if (shishoCd != "0")
                        {
                            Sql.Append(" SHISHO_CD = @SHISHO_CD AND");
                        }
                        if (seisanKubunSettei != "0")
                        {
                            Sql.Append(" SEISAN_KUBUN = @SEISAN_KUBUN AND");
                        }
                        Sql.Append(" SENSHU_CD = @SENSHU_CD ");
                        dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                        dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
                        dpc.SetParam("@DATE_FR", SqlDbType.VarChar, 10, tmpSeriDateFr.Date.ToString("yyyy/MM/dd"));
                        dpc.SetParam("@DATE_TO", SqlDbType.VarChar, 10, tmpSeriDateTo.Date.ToString("yyyy/MM/dd"));
                        dpc.SetParam("@SENSHU_CD", SqlDbType.VarChar, 6, Util.ToInt(dtMainLoop.Rows[i]["SENSHU_CD"]));
                        dpc.SetParam("@SEISAN_KUBUN", SqlDbType.VarChar, 1, seisanKubunSettei);

                        DataTable dtFunanushiCount = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
                        tmpMeisaiCount = (decimal)(Util.ToInt(dtFunanushiCount.Rows[0]["MEISAI_COUNT"]) - 1) / rptRowCnt;
                        tmpPage = (int)Math.Truncate(tmpMeisaiCount) + 1;
                        tmpLastPageData = Util.ToInt(dtFunanushiCount.Rows[0]["MEISAI_COUNT"]) - (int)Math.Truncate(tmpMeisaiCount) * rptRowCnt;
                    }
                    #endregion

                    #region 船主別の控除項目を取得
                    if (i == 0)
                    {
                        dpc = new DbParamCollection();
                        Sql = new StringBuilder();

                        Sql.Append(" SELECT");
                        while (dtSubLoop.Rows.Count - 1 > j)
                        {
                            Sql.Append(" SUM(" + tmpKK[j] + ") AS " + tmpKK[j] + ",");
                            j++;
                        }
                        Sql.Append(" SUM(" + tmpKK[j] + ") AS " + tmpKK[j]);
                        Sql.Append(" FROM");
                        Sql.Append(" TB_HN_KOJO_DATA");
                        Sql.Append(" WHERE");
                        Sql.Append(" KAISHA_CD = @KAISHA_CD AND");
                        Sql.Append(" SENSHU_CD = @SENSHU_CD AND");
                        if (shishoCd != "0")
                        {
                            Sql.Append(" SHISHO_CD = @SHISHO_CD AND");
                        }
                        if (seisanKubunSettei != "0")
                        {
                            Sql.Append(" SEISAN_KUBUN = @SEISAN_KUBUN AND");
                        }
                        Sql.Append(" SEISAN_BI BETWEEN @DATE_FR AND @DATE_TO");
                        dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                        dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
                        dpc.SetParam("@SENSHU_CD", SqlDbType.Decimal, 4, dtMainLoop.Rows[i]["SENSHU_CD"]);
                        dpc.SetParam("@DATE_FR", SqlDbType.VarChar, 10, tmpSeriDateFr.Date.ToString("yyyy/MM/dd"));
                        dpc.SetParam("@DATE_TO", SqlDbType.VarChar, 10, tmpSeriDateTo.Date.ToString("yyyy/MM/dd"));
                        dpc.SetParam("@SEISAN_KUBUN", SqlDbType.VarChar, 1, seisanKubunSettei);

                        DataTable dtKojoKingakuLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
                        j = 0;

                        kojoNmData = new OrderedDictionary();
                        kojoKingakuData = new OrderedDictionary();

                        while (dtSubLoop.Rows.Count > j)
                        {
                            tmpKojoKingaku = Util.ToInt(dtKojoKingakuLoop.Rows[0][tmpKK[j]]);
                            if (tmpKojoKingaku != 0)
                            {
                                tmpKojoNm = Util.ToString(dtSubLoop.Rows[j]["KOJO_KOMOKU_NM"]);
                                kojoNmData.Add(j, tmpKojoNm);
                                kojoKingakuData.Add(j, tmpKojoKingaku);
                            }
                            j++;
                        }
                        j = 0;
                    }
                    else if (Util.ToInt(dtMainLoop.Rows[i]["SENSHU_CD"]) != Util.ToInt(dtMainLoop.Rows[before]["SENSHU_CD"]))
                    {
                        dpc = new DbParamCollection();
                        Sql = new StringBuilder();

                        Sql.Append(" SELECT");
                        while (dtSubLoop.Rows.Count - 1 > j)
                        {
                            Sql.Append(" SUM( " + tmpKK[j] + " ) AS " + tmpKK[j] + ",");
                            j++;
                        }
                        Sql.Append(" SUM( " + tmpKK[j] + " ) AS " + tmpKK[j]);
                        Sql.Append(" FROM");
                        Sql.Append(" TB_HN_KOJO_DATA");
                        Sql.Append(" WHERE");
                        Sql.Append(" KAISHA_CD = @KAISHA_CD AND");
                        Sql.Append(" SENSHU_CD = @SENSHU_CD AND");
                        if (shishoCd != "0")
                        {
                            Sql.Append(" SHISHO_CD = @SHISHO_CD AND");
                        }
                        if (seisanKubunSettei != "0")
                        {
                            Sql.Append(" SEISAN_KUBUN = @SEISAN_KUBUN AND");
                        }
                        Sql.Append(" SEISAN_BI BETWEEN @DATE_FR AND @DATE_TO");
                        dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                        dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
                        dpc.SetParam("@SENSHU_CD", SqlDbType.Decimal, 4, dtMainLoop.Rows[i]["SENSHU_CD"]);
                        dpc.SetParam("@DATE_FR", SqlDbType.VarChar, 10, tmpSeriDateFr.Date.ToString("yyyy/MM/dd"));
                        dpc.SetParam("@DATE_TO", SqlDbType.VarChar, 10, tmpSeriDateTo.Date.ToString("yyyy/MM/dd"));
                        dpc.SetParam("@SEISAN_KUBUN", SqlDbType.VarChar, 1, seisanKubunSettei);

                        DataTable dtKojoKingakuLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
                        j = 0;

                        kojoNmData = new OrderedDictionary();
                        kojoKingakuData = new OrderedDictionary();
                        while (dtSubLoop.Rows.Count > j)
                        {
                            tmpKojoKingaku = Util.ToInt(dtKojoKingakuLoop.Rows[0][tmpKK[j]]);
                            if (tmpKojoKingaku != 0)
                            {
                                tmpKojoNm = Util.ToString(dtSubLoop.Rows[j]["KOJO_KOMOKU_NM"]);
                                kojoNmData.Add(j, tmpKojoNm);
                                kojoKingakuData.Add(j, tmpKojoKingaku);
                            }
                            j++;
                        }
                        j = 0;
                    }
                    #endregion

                    dbFunanushiSORT = Util.ToString(dbFunanushiSortCont);
                    if (dbFunanushiSORT.Length == 1)
                    {
                        dbFunanushiSORT = "00" + dbFunanushiSORT;
                    }
                    else if (dbFunanushiSORT.Length == 2)
                    {
                        dbFunanushiSORT = "0" + dbFunanushiSORT;
                    }

                    dpc = new DbParamCollection();
                    Sql = new StringBuilder();
                        
                    #region インサートテーブル
                    Sql = new StringBuilder();
                    dpc = new DbParamCollection();
                    Sql.Append("INSERT INTO PR_HN_TBL(");
                    Sql.Append("  GUID");
                    Sql.Append(" ,SORT");
                    Sql.Append(" ," + Util.ColsArray(rptItemCnt, ""));
                    Sql.Append(") ");
                    Sql.Append("VALUES(");
                    Sql.Append("  @GUID");
                    Sql.Append(" ,@SORT");
                    Sql.Append(" ," + Util.ColsArray(rptItemCnt, "@"));
                    Sql.Append(") ");
                    #endregion

                    // 月日の表示形式を設定
                    string[] tmpTsukihiData = Util.ConvJpDate(Util.ToDateStr(dtMainLoop.Rows[i]["TSUKIHI"]), this.Dba);
                    if (tmpTsukihiData[3].Length == 1)
                    {
                        tmpTsukihiData[3] = "0" + tmpTsukihiData[3];
                    }
                    if (tmpTsukihiData[4].Length == 1)
                    {
                        tmpTsukihiData[4] = "0" + tmpTsukihiData[4];
                    }
                    string tmpTsukihi = tmpTsukihiData[3] + "/" + tmpTsukihiData[4];

                    // 各船主データの最終頁
                    if (pageCount == tmpPage)
                    {
                        if (tmpLastPageData >= kojoNmData.Count)
                        {
                            #region 控除項目数より表示データ数が多い場合
                            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                            dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                            dbSORT++;
                            dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, jpDate[2]); // 現在の年月日(年)
                            dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, jpDate[3]); // 現在の年月日(月)
                            dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, jpDate[4]); // 現在の年月日(日)
                            dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpSeriWarekiFr[2]); // セリ日(年)(自)
                            dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpSeriWarekiFr[3]); // セリ日(月)(自)
                            dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpSeriWarekiFr[4]); // セリ日(日)(自)
                            dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, tmpSeriWarekiTo[2]); // セリ日(年)(至)
                            dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, tmpSeriWarekiTo[3]); // セリ日(月)(至)
                            dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, tmpSeriWarekiTo[4]); // セリ日(日)(至)
                            dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SENSHU_CD"]); // 船主コード
                            dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SENSHU_NM"]); // 船主名称
                            dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, pageCount); // 改ページ用カウント
                            dpc.SetParam("@ITEM13", SqlDbType.VarChar, 200, tmpTsukihi); // 月日
                            dpc.SetParam("@ITEM14", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["KENSU"]); // 件数
                            dpc.SetParam("@ITEM15", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["SURYO"], 1)); // 数量
                            dpc.SetParam("@ITEM16", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["HEIKIN_TANKA"], 0)); // 平均単価
                            dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["MIZUAGE_KINGAKU"], 0)); // 水揚金額
                            dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["KOJO"], 0)); // 控除金額
                            if (k % rptRowCnt == 0)
                            {
                                j = 0;
                            }
                            if (kojoNmData.Count > j)
                            {
                                dpc.SetParam("@ITEM19", SqlDbType.VarChar, 200, kojoNmData[j]); // 控除項目名
                                dpc.SetParam("@ITEM20", SqlDbType.VarChar, 200, Util.FormatNum(kojoKingakuData[j])); // 控除項目金額
                                j++;
                            }
                            else
                            {
                                dpc.SetParam("@ITEM19", SqlDbType.VarChar, 200, ""); // 控除項目名
                                dpc.SetParam("@ITEM20", SqlDbType.VarChar, 200, 0); // 控除項目金額
                            }
                            dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, dbFunanushiSORT); // ソート用カウント
                            dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, UInfo.KaishaNm); // 会社名
                            dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, jyusho); // 住所
                            dpc.SetParam("@ITEM24", SqlDbType.VarChar, 200, tel); // 電話番号
                            dpc.SetParam("@ITEM25", SqlDbType.VarChar, 200, fax); // FAX番号
                            dpc.SetParam("@ITEM26", SqlDbType.VarChar, 200, taishoKikan); // 対象期間
                            dpc.SetParam("@ITEM27", SqlDbType.VarChar, 200, outDate); // 出力日付

                            this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                            #endregion
                        }
                        else
                        {
                            #region 控除項目数より表示データ数が少ない場合
                            // データの数だけ表示
                            if (tmpLastPageData >= tmpKojoHyojiCount)
                            {
                                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                dbSORT++;
                                dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, jpDate[2]); // 現在の年月日(年)
                                dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, jpDate[3]); // 現在の年月日(月)
                                dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, jpDate[4]); // 現在の年月日(日)
                                dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpSeriWarekiFr[2]); // セリ日(年)(自)
                                dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpSeriWarekiFr[3]); // セリ日(月)(自)
                                dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpSeriWarekiFr[4]); // セリ日(日)(自)
                                dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, tmpSeriWarekiTo[2]); // セリ日(年)(至)
                                dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, tmpSeriWarekiTo[3]); // セリ日(月)(至)
                                dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, tmpSeriWarekiTo[4]); // セリ日(日)(至)
                                dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SENSHU_CD"]); // 船主コード
                                dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SENSHU_NM"]); // 船主名称
                                dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, pageCount); // 改ページ用カウント
                                dpc.SetParam("@ITEM13", SqlDbType.VarChar, 200, tmpTsukihi); // 月日
                                dpc.SetParam("@ITEM14", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["KENSU"]); // 件数
                                dpc.SetParam("@ITEM15", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["SURYO"], 1)); // 数量
                                dpc.SetParam("@ITEM16", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["HEIKIN_TANKA"], 0)); // 平均単価
                                dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["MIZUAGE_KINGAKU"], 0)); // 水揚金額
                                dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["KOJO"], 0)); // 控除金額
                                if (k % rptRowCnt == 0)
                                {
                                    j = 0;
                                }
                                dpc.SetParam("@ITEM19", SqlDbType.VarChar, 200, kojoNmData[j]); // 控除項目名
                                dpc.SetParam("@ITEM20", SqlDbType.VarChar, 200, Util.FormatNum(kojoKingakuData[j])); // 控除項目金額
                                dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, dbFunanushiSORT); // ソート用カウント
                                dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, UInfo.KaishaNm); // 会社名
                                dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, jyusho); // 住所
                                dpc.SetParam("@ITEM24", SqlDbType.VarChar, 200, tel); // 電話番号
                                dpc.SetParam("@ITEM25", SqlDbType.VarChar, 200, fax); // FAX番号
                                dpc.SetParam("@ITEM26", SqlDbType.VarChar, 200, taishoKikan); // 対象期間
                                dpc.SetParam("@ITEM27", SqlDbType.VarChar, 200, outDate); // 出力日付

                                this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                                tmpKojoHyojiCount++;
                                j++;
                            }
                            if (tmpLastPageData + 1 == tmpKojoHyojiCount)
                            {
                                if (k % rptRowCnt == 0)
                                {
                                    j = 0;
                                }
                                while (kojoNmData.Count > j)
                                {
                                    #region インサートテーブル
                                    Sql = new StringBuilder();
                                    dpc = new DbParamCollection();
                                    Sql.Append("INSERT INTO PR_HN_TBL(");
                                    Sql.Append("  GUID");
                                    Sql.Append(" ,SORT");
                                    Sql.Append(" ," + Util.ColsArray(rptItemCnt, ""));
                                    Sql.Append(") ");
                                    Sql.Append("VALUES(");
                                    Sql.Append("  @GUID");
                                    Sql.Append(" ,@SORT");
                                    Sql.Append(" ," + Util.ColsArray(rptItemCnt, "@"));
                                    Sql.Append(") ");
                                    #endregion

                                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                                    dbSORT++;
                                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, jpDate[2]); // 現在の年月日(年)
                                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, jpDate[3]); // 現在の年月日(月)
                                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, jpDate[4]); // 現在の年月日(日)
                                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpSeriWarekiFr[2]); // セリ日(年)(自)
                                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpSeriWarekiFr[3]); // セリ日(月)(自)
                                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpSeriWarekiFr[4]); // セリ日(日)(自)
                                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, tmpSeriWarekiTo[2]); // セリ日(年)(至)
                                    dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, tmpSeriWarekiTo[3]); // セリ日(月)(至)
                                    dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, tmpSeriWarekiTo[4]); // セリ日(日)(至)
                                    dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SENSHU_CD"]); // 船主コード
                                    dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SENSHU_NM"]); // 船主名称
                                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, pageCount); // 改ページ用カウント
                                    dpc.SetParam("@ITEM13", SqlDbType.VarChar, 200, ""); // 月日
                                    dpc.SetParam("@ITEM14", SqlDbType.VarChar, 200, 0); // 件数
                                    dpc.SetParam("@ITEM15", SqlDbType.VarChar, 200, 0); // 数量
                                    dpc.SetParam("@ITEM16", SqlDbType.VarChar, 200, 0); // 平均単価
                                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, 0); // 水揚金額
                                    dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, 0); // 控除金額
                                    dpc.SetParam("@ITEM19", SqlDbType.VarChar, 200, kojoNmData[j]); // 控除項目名
                                    dpc.SetParam("@ITEM20", SqlDbType.VarChar, 200, Util.FormatNum(kojoKingakuData[j])); // 控除項目金額
                                    dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, dbFunanushiSORT); // ソート用カウント
                                    dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, UInfo.KaishaNm); // 会社名
                                    dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, jyusho); // 住所
                                    dpc.SetParam("@ITEM24", SqlDbType.VarChar, 200, tel); // 電話番号
                                    dpc.SetParam("@ITEM25", SqlDbType.VarChar, 200, fax); // FAX番号
                                    dpc.SetParam("@ITEM26", SqlDbType.VarChar, 200, taishoKikan); // 対象期間
                                    dpc.SetParam("@ITEM27", SqlDbType.VarChar, 200, outDate); // 出力日付

                                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                                    j++;

                                    if (kojoNmData.Count >= j)
                                    {
                                        n++;
                                    }
                                }
                            }
                            #endregion
                        }
                    }
                    else
                    {
                        #region データ登録
                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                        dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                        dbSORT++;
                        dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, jpDate[2]); // 現在の年月日(年)
                        dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, jpDate[3]); // 現在の年月日(月)
                        dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, jpDate[4]); // 現在の年月日(日)
                        dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpSeriWarekiFr[2]); // セリ日(年)(自)
                        dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpSeriWarekiFr[3]); // セリ日(月)(自)
                        dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpSeriWarekiFr[4]); // セリ日(日)(自)
                        dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, tmpSeriWarekiTo[2]); // セリ日(年)(至)
                        dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, tmpSeriWarekiTo[3]); // セリ日(月)(至)
                        dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, tmpSeriWarekiTo[4]); // セリ日(日)(至)
                        dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SENSHU_CD"]); // 船主コード
                        dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SENSHU_NM"]); // 船主名称
                        dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, pageCount); // 改ページ用カウント
                        dpc.SetParam("@ITEM13", SqlDbType.VarChar, 200, tmpTsukihi); // 月日
                        dpc.SetParam("@ITEM14", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["KENSU"]); // 件数
                        dpc.SetParam("@ITEM15", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["SURYO"], 1)); // 数量
                        dpc.SetParam("@ITEM16", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["HEIKIN_TANKA"], 0)); // 平均単価
                        dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["MIZUAGE_KINGAKU"], 0)); // 水揚金額
                        dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["KOJO"], 0)); // 控除金額
                        if (k % rptRowCnt == 0)
                        {
                            j = 0;
                        }
                        if (kojoNmData.Count > j)
                        {
                            dpc.SetParam("@ITEM19", SqlDbType.VarChar, 200, kojoNmData[j]); // 控除項目名
                            dpc.SetParam("@ITEM20", SqlDbType.VarChar, 200, Util.FormatNum(kojoKingakuData[j])); // 控除項目金額
                            j++;
                        }
                        else
                        {
                            dpc.SetParam("@ITEM19", SqlDbType.VarChar, 200, ""); // 控除項目名
                            dpc.SetParam("@ITEM20", SqlDbType.VarChar, 200, 0); // 控除項目金額
                        }
                        dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, dbFunanushiSORT); // ソート用カウント
                        dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, UInfo.KaishaNm); // 会社名
                        dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, jyusho); // 住所
                        dpc.SetParam("@ITEM24", SqlDbType.VarChar, 200, tel); // 電話番号
                        dpc.SetParam("@ITEM25", SqlDbType.VarChar, 200, fax); // FAX番号
                        dpc.SetParam("@ITEM26", SqlDbType.VarChar, 200, taishoKikan); // 対象期間
                        dpc.SetParam("@ITEM27", SqlDbType.VarChar, 200, outDate); // 出力日付

                        this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                        #endregion
                    }

                    // 現在の船主コードと前の船主コードが一致しない場合、ACCESS改ページ判断用カウント変数Kを初期化
                    if (i != 0 && Util.ToInt(dtMainLoop.Rows[i]["SENSHU_CD"]) == Util.ToInt(dtMainLoop.Rows[before]["SENSHU_CD"]))
                    {
                        k++;
                    }
                    else
                    {
                        k = 1;
                    }

                    if (k % rptRowCnt == 0)
                    {
                        pageCount++;
                    }

                    // 空行表示準備 // 最大表示数15行毎でnを初期化
                    if (Util.ToInt(dtMainLoop.Rows.Count) != next && Util.ToInt(dtMainLoop.Rows[i]["SENSHU_CD"]) == Util.ToInt(dtMainLoop.Rows[next]["SENSHU_CD"]))
                    {
                        n++;
                        if (n % rptRowCnt == 0)
                        {
                            n = 0;
                        }
                    }
                    else
                    {
                        n++;
                        flag = 1;
                    }

                    lastNum = i;

                    // flagが1の場合、15行目まで空行表示
                    while (flag == 1 && n < rptRowCnt)
                    {
                        dpc = new DbParamCollection();
                        Sql = new StringBuilder();

                        #region インサートテーブル
                        Sql = new StringBuilder();
                        dpc = new DbParamCollection();
                        Sql.Append("INSERT INTO PR_HN_TBL(");
                        Sql.Append("  GUID");
                        Sql.Append(" ,SORT");
                        Sql.Append(" ," + Util.ColsArray(rptItemCnt, ""));
                        Sql.Append(") ");
                        Sql.Append("VALUES(");
                        Sql.Append("  @GUID");
                        Sql.Append(" ,@SORT");
                        Sql.Append(" ," + Util.ColsArray(rptItemCnt, "@"));
                        Sql.Append(") ");
                        #endregion

                        #region データ登録
                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                        dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                        dbSORT++;
                        dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, jpDate[2]); // 現在の年月日(年)
                        dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, jpDate[3]); // 現在の年月日(月)
                        dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, jpDate[4]); // 現在の年月日(日)
                        dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpSeriWarekiFr[2]); // セリ日(年)(自)
                        dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpSeriWarekiFr[3]); // セリ日(月)(自)
                        dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpSeriWarekiFr[4]); // セリ日(日)(自)
                        dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, tmpSeriWarekiTo[2]); // セリ日(年)(至)
                        dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, tmpSeriWarekiTo[3]); // セリ日(月)(至)
                        dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, tmpSeriWarekiTo[4]); // セリ日(日)(至)
                        dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SENSHU_CD"]); // 船主コード
                        dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SENSHU_NM"]); // 船主名称
                        dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, pageCount); // 改ページ用カウント
                        dpc.SetParam("@ITEM13", SqlDbType.VarChar, 200, ""); // 月日
                        dpc.SetParam("@ITEM14", SqlDbType.VarChar, 200, 0); // 件数
                        dpc.SetParam("@ITEM15", SqlDbType.VarChar, 200, 0); // 数量
                        dpc.SetParam("@ITEM16", SqlDbType.VarChar, 200, 0); // 平均単価
                        dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, 0); // 水揚金額
                        dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, 0); // 控除金額
                        dpc.SetParam("@ITEM19", SqlDbType.VarChar, 200, ""); // 控除項目名
                        dpc.SetParam("@ITEM20", SqlDbType.VarChar, 200, 0); // 控除項目金額
                        dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, dbFunanushiSORT); // ソート用カウント
                        dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, UInfo.KaishaNm); // 会社名
                        dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, jyusho); // 住所
                        dpc.SetParam("@ITEM24", SqlDbType.VarChar, 200, tel); // 電話番号
                        dpc.SetParam("@ITEM25", SqlDbType.VarChar, 200, fax); // FAX番号
                        dpc.SetParam("@ITEM26", SqlDbType.VarChar, 200, taishoKikan); // 対象期間
                        dpc.SetParam("@ITEM27", SqlDbType.VarChar, 200, outDate); // 出力日付

                        this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                        #endregion

                        n++;
                    }

                    // 現在の仲買人コードと次の仲買人コードが一致しない場合、n,j,tmpKojoHyojiCount,pageCount,mizuageFlagを初期化
                    if (Util.ToInt(dtMainLoop.Rows.Count) != next && Util.ToInt(dtMainLoop.Rows[i]["SENSHU_CD"]) != Util.ToInt(dtMainLoop.Rows[next]["SENSHU_CD"]))
                    {
                        n = 0;
                        j = 0;
                        tmpKojoHyojiCount = 1;
                        pageCount = 1;
                        dbFunanushiSortCont++;
                    }
                    i++;
                    before++;
                    next++;
                    flag = 0;

                }
            }

            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
                "SORT",
                "PR_HN_TBL",
                "GUID = @GUID",
                dpc);

            bool dataFlag;
            if (tmpdtPR_HN_TBL.Rows.Count > 0)
            {
                dataFlag = true;
            }
            else
            {
                dataFlag = false;
            }

            return dataFlag;

        }

        #endregion

    }
}
