﻿using System;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Text;
using System.Reflection;
using System.Windows.Forms;

using GrapeCity.ActiveReports;

using jp.co.fsi.common.access;
using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.han.hanr4041
{
    /// <summary>
    /// 組合員別資格申請書(HANR4041)
    /// </summary>
    public partial class HANR4041 : BasePgForm
    {
        #region プロパティ
        /// <summary>
        /// 画面上最後となるフォーカスのEnterボタン押下時処理変数
        /// </summary>
        private bool _dtFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._dtFlg;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HANR4041()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region メソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            string[] jpDate = Util.ConvJpDate(DateTime.Now, this.Dba);
            // 年指定
            lblDateGengo.Text = jpDate[0];
            txtDateYear.Text = jpDate[2];

            // フォーカス設定
            this.txtDateYear.Focus();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 年始定、船主コードにフォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtDateYear":
                case "txtFunanushiCdFr":
                case "txtFunanushiCdTo":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;

            switch (this.ActiveCtlNm)
            {
                case "txtDateYear":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom("COMC9011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.com.comc9011.COMC9011");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblDateGengo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.lblDateGengo.Text = result[1];
                            }
                        }
                    }
                    break;

                case "txtFunanushiCdFr":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom("KOBC9021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.kob.kobc9021.KOBC9021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtFunanushiCdFr.Text = outData[0];
                                this.lblFunanushiCdFr.Text = outData[1];
                            }
                        }
                    }
                    break;

                case "txtFunanushiCdTo":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom("KOBC9021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.kob.kobc9021.KOBC9021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtFunanushiCdTo.Text = outData[0];
                                this.lblFunanushiCdTo.Text = outData[1];
                            }
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }
            if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
            {
                // プレビュー処理
                DoPrint(true);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("印刷", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }


        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            // 設定画面の起動
            // MEMO:原則としてここで渡す帳票IDの設定はReport.csvに保持していることが前提ですが、
            // 保持していない場合は、設定画面での保存(F6)時に新規に設定が保持されます。
            PrintSettingForm psForm = new PrintSettingForm(new string[1] { "HANR4041R" });
            psForm.ShowDialog();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 年指定の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateYear_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidYear())
            {
                e.Cancel = true;
                this.txtDateYear.SelectAll();
            }
        }

        /// <summary>
        /// 船主コード(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCodeFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidCodeFr())
            {
                e.Cancel = true;
                this.txtFunanushiCdFr.SelectAll();
            }
        }

        /// <summary>
        /// 船主コード(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCodeTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidCodeTo())
            {
                e.Cancel = true;
                this.txtFunanushiCdTo.SelectAll();
                // Enter処理を無効化
                this._dtFlg = false;
            }
            else
            {
                // Enter処理を有効化
                this._dtFlg = true;
            }
        }

        /// <summary>
        /// 船主コード(至)のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFunanushiCdTo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.Flg)
            {
                // Enter処理を無効化
                this._dtFlg = false;

                // 全項目を再度入力値チェック
                if (!ValidateAll())
                {
                    // エラーありの場合ここで処理終了
                    return;
                }

                if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
                {
                    // ﾌﾟﾚﾋﾞｭｰ処理
                    DoPrint(true);
                }
                else
                {
                    this.txtFunanushiCdFr.Focus();
                }
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 年の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidYear()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtDateYear.Text))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }

            // 空の場合、0年として処理
            if (ValChk.IsEmpty(this.txtDateYear.Text))
            {
                this.txtDateYear.Text = "0";
            }

            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDate(Util.FixJpDate(this.lblDateGengo.Text, this.txtDateYear.Text,
                "1", "1", this.Dba));

            return true;
        }

        /// <summary>
        /// 船主コード(自)の入力チェック
        /// </summary>
        private bool IsValidCodeFr()
        {
            if (ValChk.IsEmpty(this.txtFunanushiCdFr.Text))
            {
                this.lblFunanushiCdFr.Text = "先　頭";
            }
            else
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtFunanushiCdFr.Text))
            {
                Msg.Error("コード(自)は数値のみで入力してください。");
                return false;
            }
            else
            {
                // コードを元に名称を取得する
                this.lblFunanushiCdFr.Text = this.Dba.GetName(this.UInfo, "VI_HN_FUNANUSHI", this.txtFunanushiCdFr.Text);
            }

            return true;
        }

        /// <summary>
        /// 船主コード(至)の入力チェック
        /// </summary>
        private bool IsValidCodeTo()
        {
            if (ValChk.IsEmpty(this.txtFunanushiCdTo.Text))
            {
                this.lblFunanushiCdTo.Text = "最　後";
            }
            else
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtFunanushiCdTo.Text))
            {
                Msg.Error("コード(至)は数値のみで入力してください。");
                return false;
            }
            else
            {
                // コードを元に名称を取得する
                this.lblFunanushiCdTo.Text = this.Dba.GetName(this.UInfo, "VI_HN_FUNANUSHI", this.txtFunanushiCdTo.Text);
            }

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 年のチェック
            if (!IsValidYear())
            {
                this.txtDateYear.Focus();
                this.txtDateYear.SelectAll();
                return false;
            }

            // 船主コード(自)の入力チェック
            if (!IsValidCodeFr())
            {
                this.txtFunanushiCdFr.Focus();
                this.txtFunanushiCdFr.SelectAll();
                return false;
            }
            // 船主コード(至)の入力チェック
            if (!IsValidCodeTo())
            {
                this.txtFunanushiCdTo.Focus();
                this.txtFunanushiCdTo.SelectAll();
                return false;
            }

            return true;
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpDate(string[] arrJpDate)
        {
            this.lblDateGengo.Text = arrJpDate[0];
            this.txtDateYear.Text = arrJpDate[2];
        }

        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview)
        {
            try
            {
                bool dataFlag;

                this.Dba.BeginTransaction();

                // 帳票出力用にワークテーブルにデータを作成
                dataFlag = MakeWkData();

                // 帳票出力
                if (dataFlag)
                {
                    // 取得列の定義
                    StringBuilder cols = new StringBuilder();
                    cols.Append("  ITEM01");
                    cols.Append(" ,ITEM02");
                    cols.Append(" ,ITEM03");
                    cols.Append(" ,ITEM04");
                    cols.Append(" ,ITEM05");
                    cols.Append(" ,ITEM06");
                    cols.Append(" ,ITEM07");
                    cols.Append(" ,ITEM08");
                    cols.Append(" ,ITEM09");
                    cols.Append(" ,ITEM10");
                    cols.Append(" ,ITEM11");
                    cols.Append(" ,ITEM12");
                    cols.Append(" ,ITEM13");
                    cols.Append(" ,ITEM14");
                    cols.Append(" ,ITEM15");
                    cols.Append(" ,ITEM16");
                    cols.Append(" ,ITEM17");
                    cols.Append(" ,ITEM18");
                    cols.Append(" ,ITEM19");
                    cols.Append(" ,ITEM20");
                    cols.Append(" ,ITEM21");
                    cols.Append(" ,ITEM22");
                    cols.Append(" ,ITEM23");
                    cols.Append(" ,ITEM24");
                    cols.Append(" ,ITEM25");
                    cols.Append(" ,ITEM26");
                    cols.Append(" ,ITEM27");
                    cols.Append(" ,ITEM28");
                    cols.Append(" ,ITEM29");
                    cols.Append(" ,ITEM30");
                    cols.Append(" ,ITEM31");
                    cols.Append(" ,ITEM32");
                    cols.Append(" ,ITEM33");
                    cols.Append(" ,ITEM34");
                    cols.Append(" ,ITEM35");
                    cols.Append(" ,ITEM36");
                    cols.Append(" ,ITEM37");
                    cols.Append(" ,ITEM38");
                    cols.Append(" ,ITEM39");
                    cols.Append(" ,ITEM40");

                    // バインドパラメータの設定
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);

                    // データの取得
                    DataTable dtOutput = this.Dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_HN_TBL", "GUID = @GUID", "SORT ASC", dpc);

                    // 帳票オブジェクトをインスタンス化
                    HANR4041R rpt = new HANR4041R(dtOutput);

                    if (isPreview)
                    {
                        // プレビュー画面表示
                        PreviewForm pFrm = new PreviewForm(rpt, this.UnqId);
                        pFrm.WindowState = FormWindowState.Maximized;
                        pFrm.Show();
                    }
                    else
                    {
                        // 直接印刷
                        rpt.Run(false);
                        rpt.Document.Print(true, true, false);
                    }
                }
            }
            finally
            {
                this.Dba.Rollback();
            }
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData()
        {
            #region データ取得の準備
            // 日付範囲を西暦にして取得
            DateTime tmpDate = Util.ConvAdDate(this.lblDateGengo.Text, this.txtDateYear.Text,
                    "1", "1", this.Dba);
            // 表示日付設定
            string hyojiDate = this.lblDateGengo.Text + this.txtDateYear.Text + "年";

            // 船主コード設定
            string FUNANUSHI_CD_FR;
            string FUNANUSHI_CD_TO;
            if (Util.ToDecimal(txtFunanushiCdFr.Text) > 0)
            {
                FUNANUSHI_CD_FR = txtFunanushiCdFr.Text;
            }
            else
            {
                FUNANUSHI_CD_FR = "0";
            }
            if (Util.ToDecimal(txtFunanushiCdTo.Text) > 0)
            {
                FUNANUSHI_CD_TO = txtFunanushiCdTo.Text;
            }
            else
            {
                FUNANUSHI_CD_TO = "9999";
            }
            int i = 0; // ループ用カウント変数
            int dbSORT = 1;
            #endregion

            #region メインデータ取得
            // 入力された情報を元にワークテーブルに更新をする
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder Sql = new StringBuilder();
            // han.VI_水揚高年計(VI_HN_MIZUAGEDAKA_NENKEI)の日付範囲から発生しているデータを取得
            Sql.Append(" SELECT");
            Sql.Append(" SENSHU_CD,");
            Sql.Append(" SENSHU_NM,");
            Sql.Append(" SUM(\"SURYO_1GATSU\") AS \"SURYO_1GATSU\",");
            Sql.Append(" SUM(\"SURYO_2GATSU\") AS \"SURYO_2GATSU\",");
            Sql.Append(" SUM(\"SURYO_3GATSU\") AS \"SURYO_3GATSU\",");
            Sql.Append(" SUM(\"SURYO_4GATSU\") AS \"SURYO_4GATSU\",");
            Sql.Append(" SUM(\"SURYO_5GATSU\") AS \"SURYO_5GATSU\",");
            Sql.Append(" SUM(\"SURYO_6GATSU\") AS \"SURYO_6GATSU\",");
            Sql.Append(" SUM(\"SURYO_7GATSU\") AS \"SURYO_7GATSU\",");
            Sql.Append(" SUM(\"SURYO_8GATSU\") AS \"SURYO_8GATSU\",");
            Sql.Append(" SUM(\"SURYO_9GATSU\") AS \"SURYO_9GATSU\",");
            Sql.Append(" SUM(\"SURYO_10GATSU\") AS \"SURYO_10GATSU\",");
            Sql.Append(" SUM(\"SURYO_11GATSU\") AS \"SURYO_11GATSU\",");
            Sql.Append(" SUM(\"SURYO_12GATSU\") AS \"SURYO_12GATSU\",");
            Sql.Append(" SUM(\"KINGAKU_1GATSU\") AS \"KINGAKU_1GATSU\",");
            Sql.Append(" SUM(\"KINGAKU_2GATSU\") AS \"KINGAKU_2GATSU\",");
            Sql.Append(" SUM(\"KINGAKU_3GATSU\") AS \"KINGAKU_3GATSU\",");
            Sql.Append(" SUM(\"KINGAKU_4GATSU\") AS \"KINGAKU_4GATSU\",");
            Sql.Append(" SUM(\"KINGAKU_5GATSU\") AS \"KINGAKU_5GATSU\",");
            Sql.Append(" SUM(\"KINGAKU_6GATSU\") AS \"KINGAKU_6GATSU\",");
            Sql.Append(" SUM(\"KINGAKU_7GATSU\") AS \"KINGAKU_7GATSU\",");
            Sql.Append(" SUM(\"KINGAKU_8GATSU\") AS \"KINGAKU_8GATSU\",");
            Sql.Append(" SUM(\"KINGAKU_9GATSU\") AS \"KINGAKU_9GATSU\",");
            Sql.Append(" SUM(\"KINGAKU_10GATSU\") AS \"KINGAKU_10GATSU\",");
            Sql.Append(" SUM(\"KINGAKU_11GATSU\") AS \"KINGAKU_11GATSU\",");
            Sql.Append(" SUM(\"KINGAKU_12GATSU\") AS \"KINGAKU_12GATSU\",");
            Sql.Append(" SUM(\"HANBAI_TESURYO_1GATSU\") AS \"HANBAI_TESURYO_1GATSU\",");
            Sql.Append(" SUM(\"HANBAI_TESURYO_2GATSU\") AS \"HANBAI_TESURYO_2GATSU\",");
            Sql.Append(" SUM(\"HANBAI_TESURYO_3GATSU\") AS \"HANBAI_TESURYO_3GATSU\",");
            Sql.Append(" SUM(\"HANBAI_TESURYO_4GATSU\") AS \"HANBAI_TESURYO_4GATSU\",");
            Sql.Append(" SUM(\"HANBAI_TESURYO_5GATSU\") AS \"HANBAI_TESURYO_5GATSU\",");
            Sql.Append(" SUM(\"HANBAI_TESURYO_6GATSU\") AS \"HANBAI_TESURYO_6GATSU\",");
            Sql.Append(" SUM(\"HANBAI_TESURYO_7GATSU\") AS \"HANBAI_TESURYO_7GATSU\",");
            Sql.Append(" SUM(\"HANBAI_TESURYO_8GATSU\") AS \"HANBAI_TESURYO_8GATSU\",");
            Sql.Append(" SUM(\"HANBAI_TESURYO_9GATSU\") AS \"HANBAI_TESURYO_9GATSU\",");
            Sql.Append(" SUM(\"HANBAI_TESURYO_10GATSU\") AS \"HANBAI_TESURYO_10GATSU\",");
            Sql.Append(" SUM(\"HANBAI_TESURYO_11GATSU\") AS \"HANBAI_TESURYO_11GATSU\",");
            Sql.Append(" SUM(\"HANBAI_TESURYO_12GATSU\") AS \"HANBAI_TESURYO_12GATSU\",");
            Sql.Append(" SUM(\"MIZUAGE_KAISU_1GATSU\") AS \"MIZUAGE_KAISU_1GATSU\",");
            Sql.Append(" SUM(\"MIZUAGE_KAISU_2GATSU\") AS \"MIZUAGE_KAISU_2GATSU\",");
            Sql.Append(" SUM(\"MIZUAGE_KAISU_3GATSU\") AS \"MIZUAGE_KAISU_3GATSU\",");
            Sql.Append(" SUM(\"MIZUAGE_KAISU_4GATSU\") AS \"MIZUAGE_KAISU_4GATSU\",");
            Sql.Append(" SUM(\"MIZUAGE_KAISU_5GATSU\") AS \"MIZUAGE_KAISU_5GATSU\",");
            Sql.Append(" SUM(\"MIZUAGE_KAISU_6GATSU\") AS \"MIZUAGE_KAISU_6GATSU\",");
            Sql.Append(" SUM(\"MIZUAGE_KAISU_7GATSU\") AS \"MIZUAGE_KAISU_7GATSU\",");
            Sql.Append(" SUM(\"MIZUAGE_KAISU_8GATSU\") AS \"MIZUAGE_KAISU_8GATSU\",");
            Sql.Append(" SUM(\"MIZUAGE_KAISU_9GATSU\") AS \"MIZUAGE_KAISU_9GATSU\",");
            Sql.Append(" SUM(\"MIZUAGE_KAISU_10GATSU\") AS \"MIZUAGE_KAISU_10GATSU\",");
            Sql.Append(" SUM(\"MIZUAGE_KAISU_11GATSU\") AS \"MIZUAGE_KAISU_11GATSU\",");
            Sql.Append(" SUM(\"MIZUAGE_KAISU_12GATSU\") AS \"MIZUAGE_KAISU_12GATSU\",");
            Sql.Append(" SUM(\"HANBAI_TESURYO_1GATSU\") + SUM(\"HANBAI_TESURYO_2GATSU\") + SUM(\"HANBAI_TESURYO_3GATSU\") +");
            Sql.Append(" SUM(\"HANBAI_TESURYO_4GATSU\") + SUM(\"HANBAI_TESURYO_5GATSU\") + SUM(\"HANBAI_TESURYO_6GATSU\") +");
            Sql.Append(" SUM(\"HANBAI_TESURYO_7GATSU\") + SUM(\"HANBAI_TESURYO_8GATSU\") + SUM(\"HANBAI_TESURYO_9GATSU\") +");
            Sql.Append(" SUM(\"HANBAI_TESURYO_10GATSU\") + SUM(\"HANBAI_TESURYO_11GATSU\") + SUM(\"HANBAI_TESURYO_12GATSU\") AS GOKEI_HANBAI_TESURYO");
            Sql.Append(" FROM");
            Sql.Append(" VI_HN_MIZUAGEDAKA_NENKEI");
            Sql.Append(" WHERE");
            Sql.Append(" SHUKEI_NEN = @SHUKEI_NEN AND");
            Sql.Append(" SENSHU_CD BETWEEN @SENSHU_CD_FR AND @SENSHU_CD_TO");
            Sql.Append(" GROUP BY");
            Sql.Append(" SENSHU_CD, SENSHU_NM ,SHUKEI_NEN");
            Sql.Append(" ORDER BY");
            Sql.Append(" SHUKEI_NEN,");
            Sql.Append(" SENSHU_CD");
            dpc.SetParam("@SHUKEI_NEN", SqlDbType.Decimal, 4, tmpDate.Year);
            dpc.SetParam("@SENSHU_CD_FR", SqlDbType.Decimal, 6, FUNANUSHI_CD_FR);
            dpc.SetParam("@SENSHU_CD_TO", SqlDbType.Decimal, 6, FUNANUSHI_CD_TO);

            DataTable dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
            #endregion

            if (dtMainLoop.Rows.Count == 0)
            {
                Msg.Info("該当データがありません。");
                return false;
            }
            else
            {
                while (dtMainLoop.Rows.Count > i)
                {
                    #region インサートテーブル
                    Sql = new StringBuilder();
                    dpc = new DbParamCollection();
                    Sql.Append("INSERT INTO PR_HN_TBL(");
                    Sql.Append("  GUID");
                    Sql.Append(" ,SORT");
                    Sql.Append(" ,ITEM01");
                    Sql.Append(" ,ITEM02");
                    Sql.Append(" ,ITEM03");
                    Sql.Append(" ,ITEM04");
                    Sql.Append(" ,ITEM05");
                    Sql.Append(" ,ITEM06");
                    Sql.Append(" ,ITEM07");
                    Sql.Append(" ,ITEM08");
                    Sql.Append(" ,ITEM09");
                    Sql.Append(" ,ITEM10");
                    Sql.Append(" ,ITEM11");
                    Sql.Append(" ,ITEM12");
                    Sql.Append(" ,ITEM13");
                    Sql.Append(" ,ITEM14");
                    Sql.Append(" ,ITEM15");
                    Sql.Append(" ,ITEM16");
                    Sql.Append(" ,ITEM17");
                    Sql.Append(" ,ITEM18");
                    Sql.Append(" ,ITEM19");
                    Sql.Append(" ,ITEM20");
                    Sql.Append(" ,ITEM21");
                    Sql.Append(" ,ITEM22");
                    Sql.Append(" ,ITEM23");
                    Sql.Append(" ,ITEM24");
                    Sql.Append(" ,ITEM25");
                    Sql.Append(" ,ITEM26");
                    Sql.Append(" ,ITEM27");
                    Sql.Append(" ,ITEM28");
                    Sql.Append(" ,ITEM29");
                    Sql.Append(" ,ITEM30");
                    Sql.Append(" ,ITEM31");
                    Sql.Append(" ,ITEM32");
                    Sql.Append(" ,ITEM33");
                    Sql.Append(" ,ITEM34");
                    Sql.Append(" ,ITEM35");
                    Sql.Append(" ,ITEM36");
                    Sql.Append(" ,ITEM37");
                    Sql.Append(" ,ITEM38");
                    Sql.Append(" ,ITEM39");
                    Sql.Append(" ,ITEM40");
                    Sql.Append(") ");
                    Sql.Append("VALUES(");
                    Sql.Append("  @GUID");
                    Sql.Append(" ,@SORT");
                    Sql.Append(" ,@ITEM01");
                    Sql.Append(" ,@ITEM02");
                    Sql.Append(" ,@ITEM03");
                    Sql.Append(" ,@ITEM04");
                    Sql.Append(" ,@ITEM05");
                    Sql.Append(" ,@ITEM06");
                    Sql.Append(" ,@ITEM07");
                    Sql.Append(" ,@ITEM08");
                    Sql.Append(" ,@ITEM09");
                    Sql.Append(" ,@ITEM10");
                    Sql.Append(" ,@ITEM11");
                    Sql.Append(" ,@ITEM12");
                    Sql.Append(" ,@ITEM13");
                    Sql.Append(" ,@ITEM14");
                    Sql.Append(" ,@ITEM15");
                    Sql.Append(" ,@ITEM16");
                    Sql.Append(" ,@ITEM17");
                    Sql.Append(" ,@ITEM18");
                    Sql.Append(" ,@ITEM19");
                    Sql.Append(" ,@ITEM20");
                    Sql.Append(" ,@ITEM21");
                    Sql.Append(" ,@ITEM22");
                    Sql.Append(" ,@ITEM23");
                    Sql.Append(" ,@ITEM24");
                    Sql.Append(" ,@ITEM25");
                    Sql.Append(" ,@ITEM26");
                    Sql.Append(" ,@ITEM27");
                    Sql.Append(" ,@ITEM28");
                    Sql.Append(" ,@ITEM29");
                    Sql.Append(" ,@ITEM30");
                    Sql.Append(" ,@ITEM31");
                    Sql.Append(" ,@ITEM32");
                    Sql.Append(" ,@ITEM33");
                    Sql.Append(" ,@ITEM34");
                    Sql.Append(" ,@ITEM35");
                    Sql.Append(" ,@ITEM36");
                    Sql.Append(" ,@ITEM37");
                    Sql.Append(" ,@ITEM38");
                    Sql.Append(" ,@ITEM39");
                    Sql.Append(" ,@ITEM40");
                    Sql.Append(") ");
                    #endregion

                    #region データ登録
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                    dbSORT++;
                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, hyojiDate); // 年指定
                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SENSHU_CD"]); // 船主CD
                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SENSHU_NM"]); // 船主名称
                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["MIZUAGE_KAISU_1GATSU"])); // 1月回数
                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["MIZUAGE_KAISU_2GATSU"])); // 2月回数
                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["MIZUAGE_KAISU_3GATSU"])); // 3月回数
                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["MIZUAGE_KAISU_4GATSU"])); // 4月回数
                    dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["MIZUAGE_KAISU_5GATSU"])); // 5月回数
                    dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["MIZUAGE_KAISU_6GATSU"])); // 6月回数
                    dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["MIZUAGE_KAISU_7GATSU"])); // 7月回数
                    dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["MIZUAGE_KAISU_8GATSU"])); // 8月回数
                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["MIZUAGE_KAISU_9GATSU"])); // 9月回数
                    dpc.SetParam("@ITEM13", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["MIZUAGE_KAISU_10GATSU"])); // 10月回数
                    dpc.SetParam("@ITEM14", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["MIZUAGE_KAISU_11GATSU"])); // 11月回数
                    dpc.SetParam("@ITEM15", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["MIZUAGE_KAISU_12GATSU"])); // 12月回数
                    dpc.SetParam("@ITEM16", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["SURYO_1GATSU"], 2)); // 1月数量
                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["SURYO_2GATSU"], 2)); // 2月数量
                    dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["SURYO_3GATSU"], 2)); // 3月数量
                    dpc.SetParam("@ITEM19", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["SURYO_4GATSU"], 2)); // 4月数量
                    dpc.SetParam("@ITEM20", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["SURYO_5GATSU"], 2)); // 5月数量
                    dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["SURYO_6GATSU"], 2)); // 6月数量
                    dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["SURYO_7GATSU"], 2)); // 7月数量
                    dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["SURYO_8GATSU"], 2)); // 8月数量
                    dpc.SetParam("@ITEM24", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["SURYO_9GATSU"], 2)); // 9月数量
                    dpc.SetParam("@ITEM25", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["SURYO_10GATSU"], 2)); // 10月数量
                    dpc.SetParam("@ITEM26", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["SURYO_11GATSU"], 2)); // 11月数量
                    dpc.SetParam("@ITEM27", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["SURYO_12GATSU"], 2)); // 12月数量 
                    dpc.SetParam("@ITEM28", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["KINGAKU_1GATSU"])); // 1月金額
                    dpc.SetParam("@ITEM29", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["KINGAKU_2GATSU"])); // 2月金額
                    dpc.SetParam("@ITEM30", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["KINGAKU_3GATSU"])); // 3月金額
                    dpc.SetParam("@ITEM31", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["KINGAKU_4GATSU"])); // 4月金額
                    dpc.SetParam("@ITEM32", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["KINGAKU_5GATSU"])); // 5月金額
                    dpc.SetParam("@ITEM33", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["KINGAKU_6GATSU"])); // 6月金額
                    dpc.SetParam("@ITEM34", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["KINGAKU_7GATSU"])); // 7月金額
                    dpc.SetParam("@ITEM35", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["KINGAKU_8GATSU"])); // 8月金額
                    dpc.SetParam("@ITEM36", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["KINGAKU_9GATSU"])); // 9月金額
                    dpc.SetParam("@ITEM37", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["KINGAKU_10GATSU"])); // 10月金額
                    dpc.SetParam("@ITEM38", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["KINGAKU_11GATSU"])); // 11月金額
                    dpc.SetParam("@ITEM39", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["KINGAKU_12GATSU"])); // 12月金額
                    dpc.SetParam("@ITEM40", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["GOKEI_HANBAI_TESURYO"])); // 合計販売手数料
                        
                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                    #endregion

                    i++;
                }
            }

            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
                "SORT",
                "PR_HN_TBL",
                "GUID = @GUID",
                dpc);

            bool dataFlag;
            if (tmpdtPR_HN_TBL.Rows.Count > 0)
            {
                dataFlag = true;
            }
            else
            {
                dataFlag = false;
            }

            return dataFlag;
        }
        #endregion
    }
}
