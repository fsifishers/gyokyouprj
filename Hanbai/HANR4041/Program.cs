﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

using jp.co.fsi.common.util;

namespace jp.co.fsi.han.hanr4041
{
    static class Program
    {
        /// <summary>
        /// アプリケーションのメイン エントリ ポイントです。
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.ThreadException += new System.Threading.ThreadExceptionEventHandler(Application_ThreadException);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new HANR4041());
        }

        /// <summary>
        /// 例外発生時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
            Logger lg = new Logger();
            lg.Output(Path.GetFileName(Application.ExecutablePath), e.Exception);
            Msg.Error("エラーが発生しました。\n開発元までお問い合わせ下さい。");
            Application.Exit();
        }
    }
}
