﻿namespace jp.co.fsi.han.hanr3081
{
    /// <summary>
    /// HANR3081R の概要の説明です。
    /// </summary>
    partial class HANR3081R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(HANR3081R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.reportInfo1 = new GrapeCity.ActiveReports.SectionReportModel.ReportInfo();
            this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtPageCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblTitle06 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtTitleName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtKaishaNm = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDateHani = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblCd = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblNakagaininNm = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblHasseibi = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblHasseiSuryo = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSengetuZandaka = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTougetuKarikata = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTougetuKashikata = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblZandaka = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblShouhizei = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtHasseisuryo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtKarikata = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtKashikata = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtZandaka = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtShouhizei = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtHasseibi = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.reportHeader1 = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.reportFooter1 = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.txtTotal08 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotal09 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotal10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotal12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotal11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotal02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotal03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotal04 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotal05 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotal06 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotal07 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtTotal01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.groupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.txtCd = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtNakagaininNm = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtZenZandaka = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.groupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.txtSubTotal08 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSubTotal09 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSubTotal10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSubTotal12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSubTotal02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSubTotal03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSubTotal04 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSubTotal05 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSubTotal06 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSubTotal07 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblGekkei = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblRuikei = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtSubTotal01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSubTotal11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.reportInfo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle06)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKaishaNm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateHani)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNakagaininNm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHasseibi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHasseiSuryo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSengetuZandaka)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTougetuKarikata)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTougetuKashikata)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblZandaka)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblShouhizei)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHasseisuryo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKarikata)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKashikata)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZandaka)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShouhizei)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHasseibi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal08)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal09)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal04)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal05)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal06)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal07)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNakagaininNm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZenZandaka)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubTotal08)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubTotal09)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubTotal10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubTotal12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubTotal02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubTotal03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubTotal04)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubTotal05)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubTotal06)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubTotal07)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGekkei)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRuikei)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubTotal01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubTotal11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.reportInfo1,
            this.lblPage,
            this.txtPageCount,
            this.lblTitle06,
            this.txtTitleName,
            this.txtKaishaNm,
            this.txtDateHani,
            this.lblCd,
            this.lblNakagaininNm,
            this.lblHasseibi,
            this.lblHasseiSuryo,
            this.lblSengetuZandaka,
            this.lblTougetuKarikata,
            this.lblTougetuKashikata,
            this.lblZandaka,
            this.lblShouhizei,
            this.line1,
            this.line2});
            this.pageHeader.Height = 1.114583F;
            this.pageHeader.Name = "pageHeader";
            // 
            // reportInfo1
            // 
            this.reportInfo1.FormatString = "{RunDateTime:yyyy/MM/dd}";
            this.reportInfo1.Height = 0.1574803F;
            this.reportInfo1.Left = 7.592914F;
            this.reportInfo1.Name = "reportInfo1";
            this.reportInfo1.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; ddo-char-set: 1";
            this.reportInfo1.Top = 0.1299213F;
            this.reportInfo1.Width = 0.75F;
            // 
            // lblPage
            // 
            this.lblPage.Height = 0.1574803F;
            this.lblPage.HyperLink = null;
            this.lblPage.Left = 8.658657F;
            this.lblPage.Name = "lblPage";
            this.lblPage.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle; ddo-char-set: 1";
            this.lblPage.Text = "頁";
            this.lblPage.Top = 0.1299213F;
            this.lblPage.Width = 0.1590552F;
            // 
            // txtPageCount
            // 
            this.txtPageCount.Height = 0.1574803F;
            this.txtPageCount.Left = 8.342915F;
            this.txtPageCount.MultiLine = false;
            this.txtPageCount.Name = "txtPageCount";
            this.txtPageCount.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle; ddo-char-set: 1";
            this.txtPageCount.SummaryFunc = GrapeCity.ActiveReports.SectionReportModel.SummaryFunc.Count;
            this.txtPageCount.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtPageCount.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.txtPageCount.Text = "999";
            this.txtPageCount.Top = 0.1299213F;
            this.txtPageCount.Width = 0.2952756F;
            // 
            // lblTitle06
            // 
            this.lblTitle06.Height = 0.2F;
            this.lblTitle06.HyperLink = null;
            this.lblTitle06.Left = 7.721261F;
            this.lblTitle06.Name = "lblTitle06";
            this.lblTitle06.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; d" +
    "do-char-set: 1";
            this.lblTitle06.Text = "【税込み】";
            this.lblTitle06.Top = 0.2874016F;
            this.lblTitle06.Width = 1F;
            // 
            // txtTitleName
            // 
            this.txtTitleName.Height = 0.2874016F;
            this.txtTitleName.Left = 3.110236F;
            this.txtTitleName.MultiLine = false;
            this.txtTitleName.Name = "txtTitleName";
            this.txtTitleName.Style = "font-family: ＭＳ 明朝; font-size: 15.75pt; font-weight: bold; text-align: center; te" +
    "xt-decoration: none; vertical-align: middle; ddo-char-set: 1";
            this.txtTitleName.Text = "販売未収金元帳(月報)";
            this.txtTitleName.Top = 0F;
            this.txtTitleName.Width = 2.833465F;
            // 
            // txtKaishaNm
            // 
            this.txtKaishaNm.DataField = "ITEM01";
            this.txtKaishaNm.Height = 0.2F;
            this.txtKaishaNm.Left = 0.08267713F;
            this.txtKaishaNm.MultiLine = false;
            this.txtKaishaNm.Name = "txtKaishaNm";
            this.txtKaishaNm.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; ddo-char-set: 1";
            this.txtKaishaNm.Text = "txtKaishaNm";
            this.txtKaishaNm.Top = 0.1685041F;
            this.txtKaishaNm.Width = 2.798819F;
            // 
            // txtDateHani
            // 
            this.txtDateHani.DataField = "ITEM02";
            this.txtDateHani.Height = 0.2F;
            this.txtDateHani.Left = 3.110236F;
            this.txtDateHani.Name = "txtDateHani";
            this.txtDateHani.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; d" +
    "do-char-set: 1";
            this.txtDateHani.Text = "txtDateHani";
            this.txtDateHani.Top = 0.368504F;
            this.txtDateHani.Width = 2.833465F;
            // 
            // lblCd
            // 
            this.lblCd.Height = 0.2F;
            this.lblCd.HyperLink = null;
            this.lblCd.Left = 0.08267719F;
            this.lblCd.Name = "lblCd";
            this.lblCd.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: right; vert" +
    "ical-align: middle";
            this.lblCd.Text = "コード";
            this.lblCd.Top = 0.8610237F;
            this.lblCd.Width = 0.4740158F;
            // 
            // lblNakagaininNm
            // 
            this.lblNakagaininNm.Height = 0.2F;
            this.lblNakagaininNm.HyperLink = null;
            this.lblNakagaininNm.Left = 0.7086615F;
            this.lblNakagaininNm.Name = "lblNakagaininNm";
            this.lblNakagaininNm.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: left; verti" +
    "cal-align: middle";
            this.lblNakagaininNm.Text = "仲 買 人 氏 名";
            this.lblNakagaininNm.Top = 0.8610237F;
            this.lblNakagaininNm.Width = 1.538583F;
            // 
            // lblHasseibi
            // 
            this.lblHasseibi.Height = 0.2F;
            this.lblHasseibi.HyperLink = null;
            this.lblHasseibi.Left = 2.343307F;
            this.lblHasseibi.Name = "lblHasseibi";
            this.lblHasseibi.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; ver" +
    "tical-align: middle";
            this.lblHasseibi.Text = "発 生 日";
            this.lblHasseibi.Top = 0.8610237F;
            this.lblHasseibi.Width = 0.7208661F;
            // 
            // lblHasseiSuryo
            // 
            this.lblHasseiSuryo.Height = 0.2F;
            this.lblHasseiSuryo.HyperLink = null;
            this.lblHasseiSuryo.Left = 3.168504F;
            this.lblHasseiSuryo.Name = "lblHasseiSuryo";
            this.lblHasseiSuryo.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: right; vert" +
    "ical-align: middle";
            this.lblHasseiSuryo.Text = "発 生 数 量";
            this.lblHasseiSuryo.Top = 0.8610237F;
            this.lblHasseiSuryo.Width = 0.8657479F;
            // 
            // lblSengetuZandaka
            // 
            this.lblSengetuZandaka.Height = 0.2F;
            this.lblSengetuZandaka.HyperLink = null;
            this.lblSengetuZandaka.Left = 4.190945F;
            this.lblSengetuZandaka.Name = "lblSengetuZandaka";
            this.lblSengetuZandaka.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: right; vert" +
    "ical-align: middle";
            this.lblSengetuZandaka.Text = "前 月 残 高";
            this.lblSengetuZandaka.Top = 0.8610237F;
            this.lblSengetuZandaka.Width = 0.8283469F;
            // 
            // lblTougetuKarikata
            // 
            this.lblTougetuKarikata.Height = 0.2F;
            this.lblTougetuKarikata.HyperLink = null;
            this.lblTougetuKarikata.Left = 5.139764F;
            this.lblTougetuKarikata.Name = "lblTougetuKarikata";
            this.lblTougetuKarikata.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: right; vert" +
    "ical-align: middle";
            this.lblTougetuKarikata.Text = "当月借方発生";
            this.lblTougetuKarikata.Top = 0.8610237F;
            this.lblTougetuKarikata.Width = 1.062598F;
            // 
            // lblTougetuKashikata
            // 
            this.lblTougetuKashikata.Height = 0.2F;
            this.lblTougetuKashikata.HyperLink = null;
            this.lblTougetuKashikata.Left = 6.269685F;
            this.lblTougetuKashikata.Name = "lblTougetuKashikata";
            this.lblTougetuKashikata.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: right; vert" +
    "ical-align: middle";
            this.lblTougetuKashikata.Text = "当月貸方発生";
            this.lblTougetuKashikata.Top = 0.8610237F;
            this.lblTougetuKashikata.Width = 1.001575F;
            // 
            // lblZandaka
            // 
            this.lblZandaka.Height = 0.2F;
            this.lblZandaka.HyperLink = null;
            this.lblZandaka.Left = 7.344095F;
            this.lblZandaka.Name = "lblZandaka";
            this.lblZandaka.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; ver" +
    "tical-align: middle";
            this.lblZandaka.Text = "残    高";
            this.lblZandaka.Top = 0.8610237F;
            this.lblZandaka.Width = 0.8767719F;
            // 
            // lblShouhizei
            // 
            this.lblShouhizei.Height = 0.2F;
            this.lblShouhizei.HyperLink = null;
            this.lblShouhizei.Left = 8.279922F;
            this.lblShouhizei.Name = "lblShouhizei";
            this.lblShouhizei.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: right; vert" +
    "ical-align: middle";
            this.lblShouhizei.Text = "消 費 税";
            this.lblShouhizei.Top = 0.8610237F;
            this.lblShouhizei.Width = 0.64213F;
            // 
            // line1
            // 
            this.line1.Height = 8.940697E-08F;
            this.line1.Left = 3.110236F;
            this.line1.LineWeight = 1F;
            this.line1.Name = "line1";
            this.line1.Top = 0.2874015F;
            this.line1.Width = 2.833463F;
            this.line1.X1 = 3.110236F;
            this.line1.X2 = 5.943699F;
            this.line1.Y1 = 0.2874016F;
            this.line1.Y2 = 0.2874015F;
            // 
            // line2
            // 
            this.line2.Height = 0F;
            this.line2.Left = 0F;
            this.line2.LineWeight = 1F;
            this.line2.Name = "line2";
            this.line2.Top = 1.061024F;
            this.line2.Width = 9.069292F;
            this.line2.X1 = 0F;
            this.line2.X2 = 9.069292F;
            this.line2.Y1 = 1.061024F;
            this.line2.Y2 = 1.061024F;
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtHasseisuryo,
            this.txtKarikata,
            this.txtKashikata,
            this.txtZandaka,
            this.txtShouhizei,
            this.txtHasseibi});
            this.detail.Height = 0.2187501F;
            this.detail.Name = "detail";
            // 
            // txtHasseisuryo
            // 
            this.txtHasseisuryo.DataField = "ITEM06";
            this.txtHasseisuryo.Height = 0.2F;
            this.txtHasseisuryo.Left = 3.177953F;
            this.txtHasseisuryo.MultiLine = false;
            this.txtHasseisuryo.Name = "txtHasseisuryo";
            this.txtHasseisuryo.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtHasseisuryo.Text = "txtHasseisuryo";
            this.txtHasseisuryo.Top = 0F;
            this.txtHasseisuryo.Width = 0.8562993F;
            // 
            // txtKarikata
            // 
            this.txtKarikata.DataField = "ITEM08";
            this.txtKarikata.Height = 0.2F;
            this.txtKarikata.Left = 5.139764F;
            this.txtKarikata.MultiLine = false;
            this.txtKarikata.Name = "txtKarikata";
            this.txtKarikata.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtKarikata.Text = "txtKarikata";
            this.txtKarikata.Top = 0F;
            this.txtKarikata.Width = 1.0626F;
            // 
            // txtKashikata
            // 
            this.txtKashikata.DataField = "ITEM09";
            this.txtKashikata.Height = 0.2F;
            this.txtKashikata.Left = 6.269685F;
            this.txtKashikata.MultiLine = false;
            this.txtKashikata.Name = "txtKashikata";
            this.txtKashikata.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtKashikata.Text = "txtKashikata";
            this.txtKashikata.Top = 0F;
            this.txtKashikata.Width = 1.001576F;
            // 
            // txtZandaka
            // 
            this.txtZandaka.DataField = "ITEM10";
            this.txtZandaka.Height = 0.2F;
            this.txtZandaka.Left = 7.344095F;
            this.txtZandaka.MultiLine = false;
            this.txtZandaka.Name = "txtZandaka";
            this.txtZandaka.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtZandaka.Text = "txtZandaka";
            this.txtZandaka.Top = 0F;
            this.txtZandaka.Width = 0.8767719F;
            // 
            // txtShouhizei
            // 
            this.txtShouhizei.DataField = "ITEM11";
            this.txtShouhizei.Height = 0.2F;
            this.txtShouhizei.Left = 8.279922F;
            this.txtShouhizei.MultiLine = false;
            this.txtShouhizei.Name = "txtShouhizei";
            this.txtShouhizei.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtShouhizei.Text = "txtShouhizei";
            this.txtShouhizei.Top = 0F;
            this.txtShouhizei.Width = 0.642127F;
            // 
            // txtHasseibi
            // 
            this.txtHasseibi.DataField = "ITEM05";
            this.txtHasseibi.Height = 0.2F;
            this.txtHasseibi.Left = 2.343307F;
            this.txtHasseibi.MultiLine = false;
            this.txtHasseibi.Name = "txtHasseibi";
            this.txtHasseibi.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: center; ddo-char-set: 1";
            this.txtHasseibi.Text = "txtHasseibi";
            this.txtHasseibi.Top = 0F;
            this.txtHasseibi.Width = 0.7208664F;
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            // 
            // reportHeader1
            // 
            this.reportHeader1.Height = 0F;
            this.reportHeader1.Name = "reportHeader1";
            // 
            // reportFooter1
            // 
            this.reportFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtTotal08,
            this.txtTotal09,
            this.txtTotal10,
            this.txtTotal12,
            this.txtTotal11,
            this.txtTotal02,
            this.txtTotal03,
            this.txtTotal04,
            this.txtTotal05,
            this.txtTotal06,
            this.txtTotal07,
            this.label1,
            this.label2,
            this.txtTotal01});
            this.reportFooter1.Height = 0.5104167F;
            this.reportFooter1.Name = "reportFooter1";
            this.reportFooter1.Format += new System.EventHandler(this.reportFooter1_Format);
            // 
            // txtTotal08
            // 
            this.txtTotal08.DataField = "ITEM13";
            this.txtTotal08.Height = 0.2F;
            this.txtTotal08.Left = 4.086221F;
            this.txtTotal08.MultiLine = false;
            this.txtTotal08.Name = "txtTotal08";
            this.txtTotal08.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtTotal08.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtTotal08.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtTotal08.Text = "txtTotal08";
            this.txtTotal08.Top = 0.2649606F;
            this.txtTotal08.Width = 0.9330754F;
            // 
            // txtTotal09
            // 
            this.txtTotal09.DataField = "ITEM14";
            this.txtTotal09.Height = 0.2F;
            this.txtTotal09.Left = 5.077166F;
            this.txtTotal09.MultiLine = false;
            this.txtTotal09.Name = "txtTotal09";
            this.txtTotal09.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtTotal09.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtTotal09.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtTotal09.Text = "txtTotal09";
            this.txtTotal09.Top = 0.2649606F;
            this.txtTotal09.Width = 1.125201F;
            // 
            // txtTotal10
            // 
            this.txtTotal10.DataField = "ITEM15";
            this.txtTotal10.Height = 0.2F;
            this.txtTotal10.Left = 6.269685F;
            this.txtTotal10.MultiLine = false;
            this.txtTotal10.Name = "txtTotal10";
            this.txtTotal10.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtTotal10.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtTotal10.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtTotal10.Text = "txtTotal10";
            this.txtTotal10.Top = 0.2649606F;
            this.txtTotal10.Width = 1.00158F;
            // 
            // txtTotal12
            // 
            this.txtTotal12.DataField = "ITEM16";
            this.txtTotal12.Height = 0.2F;
            this.txtTotal12.Left = 8.279922F;
            this.txtTotal12.MultiLine = false;
            this.txtTotal12.Name = "txtTotal12";
            this.txtTotal12.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtTotal12.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtTotal12.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtTotal12.Text = "txtTotal12";
            this.txtTotal12.Top = 0.2649606F;
            this.txtTotal12.Width = 0.6421299F;
            // 
            // txtTotal11
            // 
            this.txtTotal11.DataField = "ITEM13+ITEM14-ITEM15";
            this.txtTotal11.Height = 0.2F;
            this.txtTotal11.Left = 7.344095F;
            this.txtTotal11.MultiLine = false;
            this.txtTotal11.Name = "txtTotal11";
            this.txtTotal11.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtTotal11.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtTotal11.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtTotal11.Text = "txtTotal11";
            this.txtTotal11.Top = 0.2649606F;
            this.txtTotal11.Width = 0.8767757F;
            // 
            // txtTotal02
            // 
            this.txtTotal02.DataField = "ITEM07";
            this.txtTotal02.Height = 0.2F;
            this.txtTotal02.Left = 4.086221F;
            this.txtTotal02.MultiLine = false;
            this.txtTotal02.Name = "txtTotal02";
            this.txtTotal02.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtTotal02.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtTotal02.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtTotal02.Text = "txtTotal02";
            this.txtTotal02.Top = 0F;
            this.txtTotal02.Width = 0.9330754F;
            // 
            // txtTotal03
            // 
            this.txtTotal03.DataField = "ITEM08";
            this.txtTotal03.Height = 0.2F;
            this.txtTotal03.Left = 5.077166F;
            this.txtTotal03.MultiLine = false;
            this.txtTotal03.Name = "txtTotal03";
            this.txtTotal03.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtTotal03.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtTotal03.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtTotal03.Text = "txtTotal03";
            this.txtTotal03.Top = 0F;
            this.txtTotal03.Width = 1.125201F;
            // 
            // txtTotal04
            // 
            this.txtTotal04.DataField = "ITEM09";
            this.txtTotal04.Height = 0.2F;
            this.txtTotal04.Left = 6.269685F;
            this.txtTotal04.MultiLine = false;
            this.txtTotal04.Name = "txtTotal04";
            this.txtTotal04.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtTotal04.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtTotal04.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtTotal04.Text = "txtTotal04";
            this.txtTotal04.Top = 0F;
            this.txtTotal04.Width = 1.00158F;
            // 
            // txtTotal05
            // 
            this.txtTotal05.DataField = "ITEM07+ITEM08-ITEM09";
            this.txtTotal05.Height = 0.2F;
            this.txtTotal05.Left = 7.344095F;
            this.txtTotal05.MultiLine = false;
            this.txtTotal05.Name = "txtTotal05";
            this.txtTotal05.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtTotal05.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtTotal05.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtTotal05.Text = "txtTotal05";
            this.txtTotal05.Top = 0F;
            this.txtTotal05.Width = 0.8767757F;
            // 
            // txtTotal06
            // 
            this.txtTotal06.DataField = "ITEM11";
            this.txtTotal06.Height = 0.2F;
            this.txtTotal06.Left = 8.279922F;
            this.txtTotal06.MultiLine = false;
            this.txtTotal06.Name = "txtTotal06";
            this.txtTotal06.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtTotal06.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtTotal06.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtTotal06.Text = "txtTotal06";
            this.txtTotal06.Top = 0F;
            this.txtTotal06.Width = 0.6421299F;
            // 
            // txtTotal07
            // 
            this.txtTotal07.DataField = "ITEM12";
            this.txtTotal07.Height = 0.2F;
            this.txtTotal07.Left = 3.110236F;
            this.txtTotal07.MultiLine = false;
            this.txtTotal07.Name = "txtTotal07";
            this.txtTotal07.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtTotal07.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtTotal07.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtTotal07.Text = "txtTotal07";
            this.txtTotal07.Top = 0.2649606F;
            this.txtTotal07.Width = 0.9240186F;
            // 
            // label1
            // 
            this.label1.Height = 0.2F;
            this.label1.HyperLink = null;
            this.label1.Left = 0.7086616F;
            this.label1.Name = "label1";
            this.label1.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; ddo-char-set: 1";
            this.label1.Text = "【月計合計】";
            this.label1.Top = 0F;
            this.label1.Width = 1.009055F;
            // 
            // label2
            // 
            this.label2.Height = 0.2F;
            this.label2.HyperLink = null;
            this.label2.Left = 0.7086616F;
            this.label2.Name = "label2";
            this.label2.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; ddo-char-set: 1";
            this.label2.Text = "【累計合計】";
            this.label2.Top = 0.2649606F;
            this.label2.Width = 1.009055F;
            // 
            // txtTotal01
            // 
            this.txtTotal01.DataField = "ITEM06";
            this.txtTotal01.Height = 0.2F;
            this.txtTotal01.Left = 3.110236F;
            this.txtTotal01.MultiLine = false;
            this.txtTotal01.Name = "txtTotal01";
            this.txtTotal01.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtTotal01.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtTotal01.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtTotal01.Text = "txtTotal01";
            this.txtTotal01.Top = 0F;
            this.txtTotal01.Width = 0.9240158F;
            // 
            // groupHeader1
            // 
            this.groupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtCd,
            this.txtNakagaininNm,
            this.txtZenZandaka});
            this.groupHeader1.Height = 0.21875F;
            this.groupHeader1.Name = "groupHeader1";
            // 
            // txtCd
            // 
            this.txtCd.DataField = "ITEM03";
            this.txtCd.Height = 0.2F;
            this.txtCd.Left = 0.08267717F;
            this.txtCd.Name = "txtCd";
            this.txtCd.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; dd" +
    "o-char-set: 1";
            this.txtCd.Text = "txtCd";
            this.txtCd.Top = 0F;
            this.txtCd.Width = 0.4740158F;
            // 
            // txtNakagaininNm
            // 
            this.txtNakagaininNm.DataField = "ITEM04";
            this.txtNakagaininNm.Height = 0.2F;
            this.txtNakagaininNm.Left = 0.7086615F;
            this.txtNakagaininNm.MultiLine = false;
            this.txtNakagaininNm.Name = "txtNakagaininNm";
            this.txtNakagaininNm.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; ddo-char-set: 1";
            this.txtNakagaininNm.Text = "txtNakagaininNm";
            this.txtNakagaininNm.Top = 0F;
            this.txtNakagaininNm.Width = 1.538583F;
            // 
            // txtZenZandaka
            // 
            this.txtZenZandaka.DataField = "ITEM07";
            this.txtZenZandaka.Height = 0.2F;
            this.txtZenZandaka.Left = 4.190945F;
            this.txtZenZandaka.MultiLine = false;
            this.txtZenZandaka.Name = "txtZenZandaka";
            this.txtZenZandaka.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtZenZandaka.Text = "txtZenZandaka";
            this.txtZenZandaka.Top = 0F;
            this.txtZenZandaka.Width = 0.8283467F;
            // 
            // groupFooter1
            // 
            this.groupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtSubTotal08,
            this.txtSubTotal09,
            this.txtSubTotal10,
            this.txtSubTotal12,
            this.txtSubTotal02,
            this.txtSubTotal03,
            this.txtSubTotal04,
            this.txtSubTotal05,
            this.txtSubTotal06,
            this.txtSubTotal07,
            this.lblGekkei,
            this.lblRuikei,
            this.txtSubTotal01,
            this.txtSubTotal11});
            this.groupFooter1.Height = 0.4895832F;
            this.groupFooter1.Name = "groupFooter1";
            this.groupFooter1.Format += new System.EventHandler(this.groupFooter1_Format);
            // 
            // txtSubTotal08
            // 
            this.txtSubTotal08.DataField = "ITEM13";
            this.txtSubTotal08.Height = 0.2F;
            this.txtSubTotal08.Left = 4.190945F;
            this.txtSubTotal08.MultiLine = false;
            this.txtSubTotal08.Name = "txtSubTotal08";
            this.txtSubTotal08.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtSubTotal08.SummaryGroup = "groupHeader1";
            this.txtSubTotal08.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtSubTotal08.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtSubTotal08.Text = "txtSubTotal08";
            this.txtSubTotal08.Top = 0.253937F;
            this.txtSubTotal08.Width = 0.8283467F;
            // 
            // txtSubTotal09
            // 
            this.txtSubTotal09.DataField = "ITEM14";
            this.txtSubTotal09.Height = 0.2F;
            this.txtSubTotal09.Left = 5.139764F;
            this.txtSubTotal09.MultiLine = false;
            this.txtSubTotal09.Name = "txtSubTotal09";
            this.txtSubTotal09.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtSubTotal09.SummaryGroup = "groupHeader1";
            this.txtSubTotal09.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtSubTotal09.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtSubTotal09.Text = "txtSubTotal09";
            this.txtSubTotal09.Top = 0.253937F;
            this.txtSubTotal09.Width = 1.062599F;
            // 
            // txtSubTotal10
            // 
            this.txtSubTotal10.DataField = "ITEM15";
            this.txtSubTotal10.Height = 0.2F;
            this.txtSubTotal10.Left = 6.27874F;
            this.txtSubTotal10.MultiLine = false;
            this.txtSubTotal10.Name = "txtSubTotal10";
            this.txtSubTotal10.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtSubTotal10.SummaryGroup = "groupHeader1";
            this.txtSubTotal10.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtSubTotal10.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtSubTotal10.Text = "txtSubTotal10";
            this.txtSubTotal10.Top = 0.253937F;
            this.txtSubTotal10.Width = 0.9925189F;
            // 
            // txtSubTotal12
            // 
            this.txtSubTotal12.DataField = "ITEM16";
            this.txtSubTotal12.Height = 0.2F;
            this.txtSubTotal12.Left = 8.279922F;
            this.txtSubTotal12.MultiLine = false;
            this.txtSubTotal12.Name = "txtSubTotal12";
            this.txtSubTotal12.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtSubTotal12.SummaryGroup = "groupHeader1";
            this.txtSubTotal12.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtSubTotal12.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtSubTotal12.Text = "txtSubTotal12";
            this.txtSubTotal12.Top = 0.253937F;
            this.txtSubTotal12.Width = 0.6421261F;
            // 
            // txtSubTotal02
            // 
            this.txtSubTotal02.DataField = "ITEM07";
            this.txtSubTotal02.Height = 0.2F;
            this.txtSubTotal02.Left = 4.190945F;
            this.txtSubTotal02.MultiLine = false;
            this.txtSubTotal02.Name = "txtSubTotal02";
            this.txtSubTotal02.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtSubTotal02.SummaryGroup = "groupHeader1";
            this.txtSubTotal02.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtSubTotal02.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtSubTotal02.Text = "txtSubTotal02";
            this.txtSubTotal02.Top = 0F;
            this.txtSubTotal02.Width = 0.8283467F;
            // 
            // txtSubTotal03
            // 
            this.txtSubTotal03.DataField = "ITEM08";
            this.txtSubTotal03.Height = 0.2F;
            this.txtSubTotal03.Left = 5.130709F;
            this.txtSubTotal03.MultiLine = false;
            this.txtSubTotal03.Name = "txtSubTotal03";
            this.txtSubTotal03.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtSubTotal03.SummaryGroup = "groupHeader1";
            this.txtSubTotal03.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtSubTotal03.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtSubTotal03.Text = "txtSubTotal03";
            this.txtSubTotal03.Top = 0F;
            this.txtSubTotal03.Width = 1.071654F;
            // 
            // txtSubTotal04
            // 
            this.txtSubTotal04.DataField = "ITEM09";
            this.txtSubTotal04.Height = 0.2F;
            this.txtSubTotal04.Left = 6.269685F;
            this.txtSubTotal04.MultiLine = false;
            this.txtSubTotal04.Name = "txtSubTotal04";
            this.txtSubTotal04.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtSubTotal04.SummaryGroup = "groupHeader1";
            this.txtSubTotal04.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtSubTotal04.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtSubTotal04.Text = "txtSubTotal04";
            this.txtSubTotal04.Top = 0F;
            this.txtSubTotal04.Width = 1.001575F;
            // 
            // txtSubTotal05
            // 
            this.txtSubTotal05.DataField = "ITEM07+ITEM08-ITEM09";
            this.txtSubTotal05.Height = 0.2F;
            this.txtSubTotal05.Left = 7.344095F;
            this.txtSubTotal05.MultiLine = false;
            this.txtSubTotal05.Name = "txtSubTotal05";
            this.txtSubTotal05.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtSubTotal05.SummaryGroup = "groupHeader1";
            this.txtSubTotal05.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtSubTotal05.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtSubTotal05.Text = "txtSubTotal05";
            this.txtSubTotal05.Top = 0F;
            this.txtSubTotal05.Width = 0.87677F;
            // 
            // txtSubTotal06
            // 
            this.txtSubTotal06.DataField = "ITEM11";
            this.txtSubTotal06.Height = 0.2F;
            this.txtSubTotal06.Left = 8.279922F;
            this.txtSubTotal06.MultiLine = false;
            this.txtSubTotal06.Name = "txtSubTotal06";
            this.txtSubTotal06.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtSubTotal06.SummaryGroup = "groupHeader1";
            this.txtSubTotal06.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtSubTotal06.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtSubTotal06.Text = "txtSubTotal06";
            this.txtSubTotal06.Top = 0F;
            this.txtSubTotal06.Width = 0.6421261F;
            // 
            // txtSubTotal07
            // 
            this.txtSubTotal07.DataField = "ITEM12";
            this.txtSubTotal07.Height = 0.2F;
            this.txtSubTotal07.Left = 3.168504F;
            this.txtSubTotal07.MultiLine = false;
            this.txtSubTotal07.Name = "txtSubTotal07";
            this.txtSubTotal07.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtSubTotal07.SummaryGroup = "groupHeader1";
            this.txtSubTotal07.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtSubTotal07.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtSubTotal07.Text = "txtSubTotal07";
            this.txtSubTotal07.Top = 0.253937F;
            this.txtSubTotal07.Width = 0.8657498F;
            // 
            // lblGekkei
            // 
            this.lblGekkei.Height = 0.2F;
            this.lblGekkei.HyperLink = null;
            this.lblGekkei.Left = 2.343307F;
            this.lblGekkei.Name = "lblGekkei";
            this.lblGekkei.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; ddo-char-set: 1";
            this.lblGekkei.Text = "月 計";
            this.lblGekkei.Top = 0F;
            this.lblGekkei.Width = 0.5381892F;
            // 
            // lblRuikei
            // 
            this.lblRuikei.Height = 0.2F;
            this.lblRuikei.HyperLink = null;
            this.lblRuikei.Left = 2.343307F;
            this.lblRuikei.Name = "lblRuikei";
            this.lblRuikei.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; ddo-char-set: 1";
            this.lblRuikei.Text = "累 計";
            this.lblRuikei.Top = 0.253937F;
            this.lblRuikei.Width = 0.5381893F;
            // 
            // txtSubTotal01
            // 
            this.txtSubTotal01.DataField = "ITEM06";
            this.txtSubTotal01.Height = 0.2F;
            this.txtSubTotal01.Left = 3.168504F;
            this.txtSubTotal01.MultiLine = false;
            this.txtSubTotal01.Name = "txtSubTotal01";
            this.txtSubTotal01.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtSubTotal01.SummaryGroup = "groupHeader1";
            this.txtSubTotal01.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtSubTotal01.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtSubTotal01.Text = "txtSubTotal01";
            this.txtSubTotal01.Top = 0F;
            this.txtSubTotal01.Width = 0.8657479F;
            // 
            // txtSubTotal11
            // 
            this.txtSubTotal11.DataField = "ITEM13+ITEM14-ITEM15";
            this.txtSubTotal11.Height = 0.2F;
            this.txtSubTotal11.Left = 7.344095F;
            this.txtSubTotal11.MultiLine = false;
            this.txtSubTotal11.Name = "txtSubTotal11";
            this.txtSubTotal11.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtSubTotal11.SummaryGroup = "groupHeader1";
            this.txtSubTotal11.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtSubTotal11.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtSubTotal11.Text = "txtSubTotal11";
            this.txtSubTotal11.Top = 0.253937F;
            this.txtSubTotal11.Width = 0.8767757F;
            // 
            // HANR3081R
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.7874016F;
            this.PageSettings.Margins.Left = 0.5314961F;
            this.PageSettings.Margins.Right = 0.5314961F;
            this.PageSettings.Margins.Top = 0.3937008F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Portrait;
            this.PageSettings.PaperHeight = 13.89764F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.B4;
            this.PageSettings.PaperWidth = 9.84252F;
            this.PrintWidth = 9.055119F;
            this.Sections.Add(this.reportHeader1);
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.groupHeader1);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.groupFooter1);
            this.Sections.Add(this.pageFooter);
            this.Sections.Add(this.reportFooter1);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.reportInfo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle06)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKaishaNm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateHani)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNakagaininNm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHasseibi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHasseiSuryo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSengetuZandaka)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTougetuKarikata)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTougetuKashikata)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblZandaka)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblShouhizei)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHasseisuryo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKarikata)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKashikata)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZandaka)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShouhizei)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHasseibi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal08)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal09)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal04)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal05)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal06)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal07)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNakagaininNm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZenZandaka)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubTotal08)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubTotal09)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubTotal10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubTotal12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubTotal02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubTotal03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubTotal04)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubTotal05)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubTotal06)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubTotal07)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGekkei)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRuikei)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubTotal01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubTotal11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.ReportInfo reportInfo1;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPageCount;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle06;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitleName;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKaishaNm;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDateHani;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblCd;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblNakagaininNm;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblHasseibi;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblHasseiSuryo;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSengetuZandaka;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTougetuKarikata;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTougetuKashikata;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblZandaka;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblShouhizei;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
        private GrapeCity.ActiveReports.SectionReportModel.Line line2;
        private GrapeCity.ActiveReports.SectionReportModel.ReportHeader reportHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.ReportFooter reportFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader groupHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter groupFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHasseisuryo;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKarikata;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKashikata;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtZandaka;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtShouhizei;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCd;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNakagaininNm;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtZenZandaka;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHasseibi;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal08;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal09;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal02;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal03;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal04;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal05;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal06;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal07;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSubTotal08;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSubTotal09;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSubTotal10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSubTotal12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSubTotal02;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSubTotal03;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSubTotal04;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSubTotal05;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSubTotal06;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSubTotal07;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblGekkei;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblRuikei;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSubTotal01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSubTotal11;
    }
}
