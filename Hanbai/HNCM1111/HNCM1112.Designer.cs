﻿namespace jp.co.fsi.hn.hncm1111
{
    partial class HNCM1112
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblKenGyoCd = new System.Windows.Forms.Label();
            this.txtKenGyoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKenGyoNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKenGyoNm = new System.Windows.Forms.Label();
            this.txtGyoshuNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblGyoshuNm = new System.Windows.Forms.Label();
            this.txtGyoshuCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblGyoshuCd = new System.Windows.Forms.Label();
            this.gbxNagoGyo = new System.Windows.Forms.GroupBox();
            this.gbxGinozaGyo = new System.Windows.Forms.GroupBox();
            this.pnlDebug.SuspendLayout();
            this.gbxNagoGyo.SuspendLayout();
            this.gbxGinozaGyo.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Size = new System.Drawing.Size(266, 23);
            this.lblTitle.Text = "";
            // 
            // btnF3
            // 
            this.btnF3.Text = "F3\r\n\r\n削除";
            // 
            // btnF6
            // 
            this.btnF6.Text = "F6\r\n\r\n登録";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(5, 166);
            this.pnlDebug.Size = new System.Drawing.Size(299, 100);
            // 
            // lblKenGyoCd
            // 
            this.lblKenGyoCd.BackColor = System.Drawing.Color.Silver;
            this.lblKenGyoCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKenGyoCd.Location = new System.Drawing.Point(10, 20);
            this.lblKenGyoCd.Name = "lblKenGyoCd";
            this.lblKenGyoCd.Size = new System.Drawing.Size(154, 25);
            this.lblKenGyoCd.TabIndex = 1;
            this.lblKenGyoCd.Text = "名護魚種CD";
            this.lblKenGyoCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKenGyoCd
            // 
            this.txtKenGyoCd.AutoSizeFromLength = false;
            this.txtKenGyoCd.DisplayLength = null;
            this.txtKenGyoCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKenGyoCd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.txtKenGyoCd.Location = new System.Drawing.Point(109, 22);
            this.txtKenGyoCd.MaxLength = 6;
            this.txtKenGyoCd.Name = "txtKenGyoCd";
            this.txtKenGyoCd.Size = new System.Drawing.Size(52, 20);
            this.txtKenGyoCd.TabIndex = 1;
            this.txtKenGyoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKenGyoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtKenGyoCd_Validating);
            // 
            // txtKenGyoNm
            // 
            this.txtKenGyoNm.AutoSizeFromLength = false;
            this.txtKenGyoNm.DisplayLength = null;
            this.txtKenGyoNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKenGyoNm.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtKenGyoNm.Location = new System.Drawing.Point(109, 56);
            this.txtKenGyoNm.MaxLength = 20;
            this.txtKenGyoNm.Name = "txtKenGyoNm";
            this.txtKenGyoNm.Size = new System.Drawing.Size(135, 20);
            this.txtKenGyoNm.TabIndex = 2;
            this.txtKenGyoNm.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtKenGyoNm_KeyDown);
            this.txtKenGyoNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtKenGyoNm_Validating);
            // 
            // lblKenGyoNm
            // 
            this.lblKenGyoNm.BackColor = System.Drawing.Color.Silver;
            this.lblKenGyoNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblKenGyoNm.Location = new System.Drawing.Point(10, 54);
            this.lblKenGyoNm.Name = "lblKenGyoNm";
            this.lblKenGyoNm.Size = new System.Drawing.Size(237, 25);
            this.lblKenGyoNm.TabIndex = 3;
            this.lblKenGyoNm.Text = "名護魚種名";
            this.lblKenGyoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtGyoshuNm
            // 
            this.txtGyoshuNm.AutoSizeFromLength = false;
            this.txtGyoshuNm.DisplayLength = null;
            this.txtGyoshuNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGyoshuNm.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtGyoshuNm.Location = new System.Drawing.Point(109, 56);
            this.txtGyoshuNm.MaxLength = 20;
            this.txtGyoshuNm.Name = "txtGyoshuNm";
            this.txtGyoshuNm.Size = new System.Drawing.Size(135, 20);
            this.txtGyoshuNm.TabIndex = 4;
            this.txtGyoshuNm.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtGyoshuNm_KeyDown);
            this.txtGyoshuNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtGyoshuNm_Validating);
            // 
            // lblGyoshuNm
            // 
            this.lblGyoshuNm.BackColor = System.Drawing.Color.Silver;
            this.lblGyoshuNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblGyoshuNm.Location = new System.Drawing.Point(10, 54);
            this.lblGyoshuNm.Name = "lblGyoshuNm";
            this.lblGyoshuNm.Size = new System.Drawing.Size(237, 25);
            this.lblGyoshuNm.TabIndex = 5;
            this.lblGyoshuNm.Text = "魚 種 名";
            this.lblGyoshuNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtGyoshuCd
            // 
            this.txtGyoshuCd.AutoSizeFromLength = false;
            this.txtGyoshuCd.DisplayLength = null;
            this.txtGyoshuCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGyoshuCd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.txtGyoshuCd.Location = new System.Drawing.Point(109, 22);
            this.txtGyoshuCd.MaxLength = 6;
            this.txtGyoshuCd.Name = "txtGyoshuCd";
            this.txtGyoshuCd.Size = new System.Drawing.Size(52, 20);
            this.txtGyoshuCd.TabIndex = 3;
            this.txtGyoshuCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGyoshuCd.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtGyoshuCd_KeyDown);
            this.txtGyoshuCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtGyoshuCd_Validating);
            // 
            // lblGyoshuCd
            // 
            this.lblGyoshuCd.BackColor = System.Drawing.Color.Silver;
            this.lblGyoshuCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblGyoshuCd.Location = new System.Drawing.Point(10, 20);
            this.lblGyoshuCd.Name = "lblGyoshuCd";
            this.lblGyoshuCd.Size = new System.Drawing.Size(154, 25);
            this.lblGyoshuCd.TabIndex = 7;
            this.lblGyoshuCd.Text = "魚種CD";
            this.lblGyoshuCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // gbxNagoGyo
            // 
            this.gbxNagoGyo.Controls.Add(this.txtKenGyoNm);
            this.gbxNagoGyo.Controls.Add(this.txtKenGyoCd);
            this.gbxNagoGyo.Controls.Add(this.lblKenGyoCd);
            this.gbxNagoGyo.Controls.Add(this.lblKenGyoNm);
            this.gbxNagoGyo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxNagoGyo.Location = new System.Drawing.Point(12, 12);
            this.gbxNagoGyo.Name = "gbxNagoGyo";
            this.gbxNagoGyo.Size = new System.Drawing.Size(267, 91);
            this.gbxNagoGyo.TabIndex = 902;
            this.gbxNagoGyo.TabStop = false;
            this.gbxNagoGyo.Text = "名護漁協";
            // 
            // gbxGinozaGyo
            // 
            this.gbxGinozaGyo.Controls.Add(this.txtGyoshuNm);
            this.gbxGinozaGyo.Controls.Add(this.txtGyoshuCd);
            this.gbxGinozaGyo.Controls.Add(this.lblGyoshuNm);
            this.gbxGinozaGyo.Controls.Add(this.lblGyoshuCd);
            this.gbxGinozaGyo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxGinozaGyo.Location = new System.Drawing.Point(12, 109);
            this.gbxGinozaGyo.Name = "gbxGinozaGyo";
            this.gbxGinozaGyo.Size = new System.Drawing.Size(267, 94);
            this.gbxGinozaGyo.TabIndex = 903;
            this.gbxGinozaGyo.TabStop = false;
            this.gbxGinozaGyo.Text = "漁協";
            // 
            // HNCM1112
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(291, 269);
            this.Controls.Add(this.gbxGinozaGyo);
            this.Controls.Add(this.gbxNagoGyo);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "HNCM1112";
            this.ShowFButton = true;
            this.Text = "魚種変換マスタ入力";
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.gbxNagoGyo, 0);
            this.Controls.SetChildIndex(this.gbxGinozaGyo, 0);
            this.pnlDebug.ResumeLayout(false);
            this.gbxNagoGyo.ResumeLayout(false);
            this.gbxNagoGyo.PerformLayout();
            this.gbxGinozaGyo.ResumeLayout(false);
            this.gbxGinozaGyo.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblKenGyoCd;
        private jp.co.fsi.common.controls.FsiTextBox txtKenGyoCd;
        private jp.co.fsi.common.controls.FsiTextBox txtKenGyoNm;
        private System.Windows.Forms.Label lblKenGyoNm;
        private jp.co.fsi.common.controls.FsiTextBox txtGyoshuNm;
        private System.Windows.Forms.Label lblGyoshuNm;
        private jp.co.fsi.common.controls.FsiTextBox txtGyoshuCd;
        private System.Windows.Forms.Label lblGyoshuCd;
        private System.Windows.Forms.GroupBox gbxNagoGyo;
        private System.Windows.Forms.GroupBox gbxGinozaGyo;


    }
}