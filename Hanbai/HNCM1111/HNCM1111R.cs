﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using jp.co.fsi.common.report;

namespace jp.co.fsi.hn.hncm1111
{
    /// <summary>
    /// HANC9151R の帳票
    /// </summary>
    public partial class HNCM1111R : BaseReport
    {

        public HNCM1111R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }

        /// <summary>
        /// ページヘッダーの設定
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pageHeader_Format(object sender, EventArgs e)
        {
            //和暦でDataTimeを文字列に変換する
            System.Globalization.CultureInfo ci =
                new System.Globalization.CultureInfo("ja-JP", false);
            ci.DateTimeFormat.Calendar = new System.Globalization.JapaneseCalendar();

            this.txtToday.Text = DateTime.Now.ToString("gy年MM月dd日(dddd)", ci);
        }
    }
}
