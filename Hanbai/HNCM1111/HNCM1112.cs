﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.hn.hncm1111
{
    /// <summary>
    /// 魚種変換マスタメンテ(変更・追加)(HANC9152)
    /// </summary>
    public partial class HNCM1112 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// モード(新規)
        /// </summary>
        private const string MODE_NEW = "1";

        /// <summary>
        /// モード(編集)
        /// </summary>
        private const string MODE_EDIT = "2";
        #endregion

       #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HNCM1112()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="par1">引数1</param>
        public HNCM1112(string par1)
            : base(par1)
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 引数：Par1／モード(1:新規、2:変更)、InData：名護漁協コード
            if (MODE_NEW.Equals(this.Par1))
            {
                // 新規モードの初期表示
                InitDispOnNew();
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // 編集モードの初期表示
                InitDispOnEdit();
            }
            else
            {
                // 不正な起動として閉じる
                Msg.Error("不正な起動です。終了します。");
                this.Close();
            }

            // タイトルは非表示
            this.lblTitle.Visible = false;
            // EscapeとF1のみ表示
            this.ShowFButton = true;
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF1.Location = this.btnF2.Location;
            this.btnF6.Location = this.btnF4.Location;
            this.btnF2.Visible = false;
            this.btnF3.Visible = true;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF6.Visible = true;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;

            this.gbxGinozaGyo.Text = UInfo.KaishaNm;
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // フォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtGyoshuCd":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// Escボタン押下時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;
            string stCurrentDir = System.IO.Directory.GetCurrentDirectory();

            switch (this.ActiveCtlNm)
            {
                case "txtGyoshuCd":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom(stCurrentDir + @"\EXE\"+"HNCM1051.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.hn.hncm1051.HNCM1051");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            if (this.ActiveCtlNm == "txtGyoshuCd")
                            {
                                frm.InData = this.txtGyoshuCd.Text;
                            }
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                if (this.ActiveCtlNm == "txtGyoshuCd")
                                {
                                    // 魚種CDが入力されたら県漁連魚種CD、名を有効にする。
                                    // 魚種名は変更出来てはいけないため無効。
                                    this.txtKenGyoCd.Enabled = true;
                                    this.txtKenGyoNm.Enabled = true;
                                    this.txtGyoshuNm.Enabled = false;

                                    this.txtGyoshuCd.Text = outData[0];
                                    this.txtGyoshuNm.Text = outData[1];
                                    this.txtKenGyoNm.Text = outData[1];

                                    this.txtKenGyoCd.Focus();

                                    // 魚種CDを非活性にする
                                    this.txtGyoshuCd.Enabled = false;

                                    // 削除ボタン表示する
                                    this.btnF3.Enabled = true;
                                }

                            }
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// F3キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF3();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF3()
        {
            if (!this.btnF3.Enabled)
            {
                return;
            }

            if (Msg.ConfYesNo("削除しますか？") == DialogResult.No)
            {
                return;
            }

            try
            {
                // トランザクションの開始
                this.Dba.BeginTransaction();

                DbParamCollection whereParam;
                whereParam = new DbParamCollection();

                whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
                whereParam.SetParam("@GYOSHU_CD", SqlDbType.Decimal, 6, this.txtGyoshuCd.Text);
                this.Dba.Delete("TB_HN_GYOSHU_CD_HENKAN_MST",
                    "KAISHA_CD = @KAISHA_CD AND GYOSHU_CD = @GYOSHU_CD",
                    whereParam);

                // トランザクションをコミット
                this.Dba.Commit();

                Msg.Info("削除しました。");
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF6();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF6()
        {
            DataTable a = GetGyoshuCd(Util.ToDecimal(this.txtGyoshuCd.Text));

            if (Util.ToDecimal(a.Rows[0]["KENSU"]) == 0)
            {
                this.Par1 = "1";
            }
            else
            {
                this.Par1 = "2";
            }
            // 確認メッセージを表示
            string msg = (MODE_NEW.Equals(this.Par1) ? "登録" : "更新") + "しますか？";
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            // 入力値をバインドパラメータとしてセットする
            ArrayList alParamsCmTanto = SetCmGyoshuParams();

            try
            {
                // トランザクションの開始
                this.Dba.BeginTransaction();

                if (MODE_NEW.Equals(this.Par1))
                {
                    // データ登録
                    // 共通.商品マスタ // 魚種CD変換マスタメンテ
                    this.Dba.Insert("TB_HN_GYOSHU_CD_HENKAN_MST", (DbParamCollection)alParamsCmTanto[0]);
                }
                else if (MODE_EDIT.Equals(this.Par1))
                {
                    // データ更新
                    // 共通.商品マスタ // 魚種CD変換マスタメンテ
                    this.Dba.Update("TB_HN_GYOSHU_CD_HENKAN_MST",
                        (DbParamCollection)alParamsCmTanto[1],
                        // GYOSHU_CDとGYOSHU_CD_WHEREがある
                        // 更新だからWHEREの方から取ってくる。
                        "KAISHA_CD = @KAISHA_CD AND GYOSHU_CD = @GYOSHU_CD_WHERE",
                        (DbParamCollection)alParamsCmTanto[0]);
                }

                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            Msg.Info("登録しました。");
            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 県漁連魚種CDの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKenGyoCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidKenGyoCd())
            {
                e.Cancel = true;
                this.txtKenGyoCd.SelectAll();
            }
        }

        /// <summary>
        /// 県漁連魚種名の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKenGyoNm_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidKenGyoNm())
            {
                e.Cancel = true;
                this.txtKenGyoNm.SelectAll();
            }
        }

        /// <summary>
        /// 県漁連魚種名Enter押下時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKenGyoNm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.Par1 == "1")
            {
                this.PressF6();
            }
        }

        /// <summary>
        /// 魚種CDEnter押下時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGyoshuCd_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (!IsValidGyoshu())
                {
                    this.txtGyoshuCd.Focus();
                }
            }
        }

        private void txtGyoshuCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidGyoshu())
            {
                e.Cancel = true;
                this.txtGyoshuCd.Focus();
                this.txtGyoshuCd.SelectAll();
            }
        }

        /// <summary>
        /// 魚種名の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGyoshuNm_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidGyoshuNm())
            {
                e.Cancel = true;
                this.txtGyoshuNm.SelectAll();
            }
        }

        private void txtGyoshuNm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.PressF6();
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 新規モードの初期表示
        /// </summary>
        private void InitDispOnNew()
        {
            // 初期値、入力制御を実装
            // 初期値を設定
            this.txtKenGyoCd.Text = "0";
            this.txtGyoshuCd.Text = "0";
            // コントロール制御
            this.txtKenGyoCd.Enabled = false;
            this.txtKenGyoNm.Enabled = false;
            this.txtGyoshuNm.Enabled = false;

            // 県漁連魚種CDに初期フォーカス
            this.ActiveControl = this.txtGyoshuCd;
            this.txtGyoshuCd.Focus();

            // 削除ボタン非表示
            this.btnF3.Enabled = false;
        }

        /// <summary>
        /// 編集モードの初期表示
        /// </summary>
        private void InitDispOnEdit()
        {
            Array indata = (Array)InData;
            // 現在DBに登録されている値、入力制御を実装
            StringBuilder cols = new StringBuilder();
            cols.Append("KAISHA_CD");
            cols.Append("    ,KEN_GYOREN_GYOSHU_CD");
            cols.Append("    ,KEN_GYOREN_GYOSHU_NM");
            cols.Append("    ,GYOSHU_CD");
            cols.Append("    ,GYOSHU_NM");

            StringBuilder from = new StringBuilder();
            from.Append("TB_HN_GYOSHU_CD_HENKAN_MST");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@KEN_GYOREN_GYOSHU_CD", SqlDbType.Decimal, 6, indata.GetValue(0));
            dpc.SetParam("@GYOSHU_CD", SqlDbType.Decimal, 6, indata.GetValue(1));

            DataTable dtDispData =
                this.Dba.GetDataTableByConditionWithParams(
                    Util.ToString(cols), Util.ToString(from),
                    "KAISHA_CD = @KAISHA_CD AND KEN_GYOREN_GYOSHU_CD = @KEN_GYOREN_GYOSHU_CD AND GYOSHU_CD = @GYOSHU_CD ",
                    dpc);

            if (dtDispData.Rows.Count == 0)
            {
                Msg.Error("不正な起動です。終了します。");
                this.Close();
            }
            else
            {
                // 最初は県漁連魚種CD、名、魚種名を有効にする
                this.txtKenGyoCd.Enabled = true;
                this.txtKenGyoNm.Enabled = true;
                this.txtGyoshuNm.Enabled = true;
                // 取得した内容を表示
                DataRow drDispData = dtDispData.Rows[0];
                this.txtKenGyoCd.Text = Util.ToString(drDispData["KEN_GYOREN_GYOSHU_CD"]);
                this.txtKenGyoNm.Text = Util.ToString(drDispData["KEN_GYOREN_GYOSHU_NM"]);
                this.txtGyoshuCd.Text = Util.ToString(drDispData["GYOSHU_CD"]);
                this.txtGyoshuNm.Text = Util.ToString(drDispData["GYOSHU_NM"]);

                // 県漁連魚種CDにフォーカス
                this.ActiveControl = this.txtKenGyoCd;
                this.txtKenGyoCd.Focus();
                this.txtKenGyoCd.SelectAll();

            }
        }

        /// <summary>
        /// 県漁連魚種CDの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidKenGyoCd()
        {
            // 未入力はエラーとする
            if (ValChk.IsEmpty(this.txtKenGyoCd.Text))
            {
                Msg.Error("入力してください。");
                return false;
            }

            // 0はエラーとする
            if (this.txtKenGyoCd.Text == "0")
            {
                Msg.Error("0は登録できません。");
                return false;
            }

            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtKenGyoCd.Text))
            {
                Msg.Error("数字で入力してください。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 県漁連魚種名の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidKenGyoNm()
        {
            // 未入力はエラーとする
            if (ValChk.IsEmpty(this.txtKenGyoNm.Text))
            {
                Msg.Error("入力してください。");
                return false;
            }
            // 0はエラーとする
            if (this.txtKenGyoNm.Text == "0")
            {
                Msg.Error("0は登録できません。");
                return false;
            }
            // 40バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtKenGyoNm.Text, this.txtKenGyoNm.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 魚種名の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidGyoshuNm()
        {
            // 未入力はエラーとする
            if (ValChk.IsEmpty(this.txtGyoshuNm.Text))
            {
                Msg.Error("入力してください。");
                return false;
            }
            // 0はエラーとする
            if (this.txtGyoshuNm.Text == "0")
            {
                Msg.Error("0は登録できません。");
                return false;
            }
            // 40バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtGyoshuNm.Text, this.txtGyoshuNm.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 魚種CDの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidGyoshu()
        {
            // 未入力はエラー
            if (ValChk.IsEmpty(this.txtGyoshuCd.Text))
            {
                Msg.Error("入力してください。");
                return false;
            }
            // 0はエラーとする
            if (this.txtGyoshuCd.Text == "0")
            {
                Msg.Error("0は登録できません。");
                return false;
            }
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtGyoshuCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 新規の場合
            if (MODE_NEW == (this.Par1))
            {
                // 空でない場合、入力された魚種CDから検索する
                if (!ValChk.IsEmpty(this.txtGyoshuCd.Text))
                {
                    // 現在DBに登録されている値をtextboxに表示
                    StringBuilder cols = new StringBuilder();
                    cols.Append("KAISHA_CD");
                    cols.Append("    ,GYOSHU_CD");
                    cols.Append("    ,GYOSHU_NM");

                    StringBuilder from = new StringBuilder();
                    from.Append("VI_HN_GYOSHU");

                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
                    dpc.SetParam("@GYOSHU_CD", SqlDbType.Decimal, 6, Util.ToDecimal(this.txtGyoshuCd.Text));
                    DataTable dtDispData =
                        this.Dba.GetDataTableByConditionWithParams(
                            Util.ToString(cols), Util.ToString(from),
                            "KAISHA_CD = @KAISHA_CD AND GYOSHU_CD = @GYOSHU_CD ",
                            dpc);

                    if (dtDispData.Rows.Count == 0)
                    {
                        Msg.Error("データがありません。");
                        // 魚種CDにFocusを当てる
                        this.txtGyoshuCd.Focus();
                        this.txtGyoshuCd.SelectAll();
                        return false;
                    }
                    else
                    {
                        // 取得した内容を表示
                        DataRow drDispData = dtDispData.Rows[0];
                        this.txtGyoshuCd.Text = Util.ToString(drDispData["GYOSHU_CD"]);
                        this.txtGyoshuNm.Text = Util.ToString(drDispData["GYOSHU_NM"]);
                        if (this.txtKenGyoNm.Text == "")
                            this.txtKenGyoNm.Text = Util.ToString(drDispData["GYOSHU_NM"]);

                        // 県漁連魚種CD、名を有効にする
                        this.txtKenGyoCd.Enabled = true;
                        this.txtKenGyoNm.Enabled = true;
                        // 県漁連魚種CDにFocusを当てる
                        this.txtKenGyoCd.Focus();

                        // 魚種CDを非活性にする
                        this.txtGyoshuCd.Enabled = false;
                        this.txtGyoshuNm.Enabled = false;

                        // 削除ボタン表示する
                        this.btnF3.Enabled = true;
                    }
                }
            }
            // 編集の場合
            else if (MODE_EDIT.Equals(this.Par1))
            {
                /**/
                // 空でない場合、入力された魚種CDから検索する
                if (!ValChk.IsEmpty(this.txtGyoshuCd.Text))
                {
                    // 現在DBに登録されている値をtextboxに表示
                    StringBuilder cols = new StringBuilder();
                    cols.Append("KAISHA_CD");
                    cols.Append("    ,GYOSHU_CD");
                    cols.Append("    ,GYOSHU_NM");

                    StringBuilder from = new StringBuilder();
                    from.Append("VI_HN_GYOSHU");

                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
                    dpc.SetParam("@GYOSHU_CD", SqlDbType.Decimal, 13, Util.ToDecimal(this.txtGyoshuCd.Text));
                    DataTable dtDispData =
                        this.Dba.GetDataTableByConditionWithParams(
                            Util.ToString(cols), Util.ToString(from),
                            "KAISHA_CD = @KAISHA_CD AND GYOSHU_CD = @GYOSHU_CD ",
                            dpc);


                    if (dtDispData.Rows.Count == 0)
                    {
                        Msg.Error("データがありません。");
                        // 魚種名を無効にする
                        this.txtGyoshuNm.Enabled = false;
                        // 県漁連魚種CD、名を有効にする
                        this.txtKenGyoCd.Enabled = false;
                        this.txtKenGyoNm.Enabled = false;
                        // 全部一旦消してからデータ入れるため
                        this.txtGyoshuNm.Text = "";
                        this.txtKenGyoCd.Text = "";
                        this.txtKenGyoNm.Text = "";
                        // 魚種CDにFocusを当てる
                        this.txtGyoshuCd.Focus();
                        this.txtGyoshuCd.SelectAll();
                        return false;
                    }
                    else
                    {
                        // 取得した内容を表示
                        DataRow drDispData = dtDispData.Rows[0];
                        this.txtGyoshuCd.Text = Util.ToString(drDispData["GYOSHU_CD"]);
                        if (this.txtGyoshuNm.Text == "")
                            this.txtGyoshuNm.Text = Util.ToString(drDispData["GYOSHU_NM"]);
                        if (this.txtKenGyoNm.Text == "")
                            this.txtKenGyoNm.Text = Util.ToString(drDispData["GYOSHU_NM"]);

                        // 県漁連魚種CD、名を有効にする
                        this.txtKenGyoCd.Enabled = true;
                        this.txtKenGyoNm.Enabled = true;
                        //// 魚種名を無効にする
                        //this.txtGyoshuNm.Enabled = false;
                        //// 県漁連魚種CDにFocusを当てる
                        //this.txtKenGyoCd.Focus();
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 県漁連魚種CDのチェック
            if (!IsValidKenGyoCd())
            {
                this.txtKenGyoCd.Focus();
                return false;
            }

            // 県漁連魚種名のチェック
            if (!IsValidKenGyoNm())
            {
                this.txtKenGyoNm.Focus();
                return false;
            }

            // 魚種名のチェック
            if (!IsValidGyoshuNm())
            {
                this.txtGyoshuNm.Focus();
                return false;
            }

            // 魚種CDのチェック
            if (!IsValidGyoshu())
            {
                this.txtGyoshuCd.Focus();
                return false;
            }

            return true;
        }

        /// <summary>
        /// TB_HN_GYOSHU_CD_HENKAN_MSTに更新するためのパラメータ設定をします。
        /// </summary>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// 更新処理：DbParamCollection*2(Where句,Set句)
        /// </returns>
        private ArrayList SetCmGyoshuParams()
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();
            // 登録
            if (MODE_NEW.Equals(this.Par1))
            {
                // 会社CD,魚種CDをパラメータに設定
                updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                updParam.SetParam("@GYOSHU_CD", SqlDbType.Decimal, 6, this.txtGyoshuCd.Text);
                // 登録日
                updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWDATE");
                // 処理フラグ
                updParam.SetParam("@SHORI_FLG", SqlDbType.Decimal, 1, 0);
            }
            // 更新
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // 会社CDをWhere句のパラメータに設定
                DbParamCollection whereParam = new DbParamCollection();
                whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                whereParam.SetParam("@GYOSHU_CD_WHERE", SqlDbType.Decimal, 6, Util.ToDecimal(this.txtGyoshuCd.Text));
                alParams.Add(whereParam);
                // 処理フラグ
                updParam.SetParam("@SHORI_FLG", SqlDbType.Decimal, 1, 1);
            }

            // 名護魚種CD
            updParam.SetParam("@KEN_GYOREN_GYOSHU_CD", SqlDbType.Decimal, 6, Util.ToDecimal(this.txtKenGyoCd.Text));
            // 名護魚種名
            updParam.SetParam("@KEN_GYOREN_GYOSHU_NM", SqlDbType.VarChar, 40, this.txtKenGyoNm.Text);
            // 魚種名
            updParam.SetParam("@GYOSHU_NM", SqlDbType.VarChar, 40, this.txtGyoshuNm.Text);
            // 更新日
            updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWDATE");

            alParams.Add(updParam);

            return alParams;
        }

        /// <summary>
        /// 魚種コードの存在チェック(宜野座)
        /// </summary>
        /// <param name="gyoshuCd">魚種コード</param>
        /// <returns>取得したデータ</returns>
        private DataTable GetGyoshuCd(decimal gyoshuCd)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT ");
            sql.Append("  COUNT(*) AS KENSU ");
            sql.Append("FROM ");
            sql.Append("  TB_HN_GYOSHU_CD_HENKAN_MST ");
            sql.Append("WHERE ");
            sql.Append("    KAISHA_CD = @KAISHA_CD ");
            sql.Append("AND GYOSHU_CD = @GYOSHU_CD ");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@GYOSHU_CD", SqlDbType.Decimal, 6, gyoshuCd);

            DataTable dtResult = this.Dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            return dtResult;
        }
        #endregion
    }
}
