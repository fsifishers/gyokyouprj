﻿namespace jp.co.fsi.hn.hncm1111
{
    /// <summary>
    /// HANC9151R の概要の説明です。
    /// </summary>
    partial class HNCM1111R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(HNCM1111R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.textBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblTitle01 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtToday = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtPageCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblSakuseibi = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtTitle04 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtKenGyoCd = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtKenGyoNm = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGyoshuCd = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGyoshuNm = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line18 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line16 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtToday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSakuseibi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle04)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKenGyoCd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKenGyoNm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyoshuCd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyoshuNm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.textBox1,
            this.lblTitle01,
            this.line1,
            this.txtToday,
            this.lblPage,
            this.txtPageCount,
            this.lblSakuseibi,
            this.txtTitle04,
            this.txtTitle03,
            this.txtTitle02,
            this.txtTitle01,
            this.line3,
            this.line6});
            this.pageHeader.Height = 1.090551F;
            this.pageHeader.Name = "pageHeader";
            this.pageHeader.Format += new System.EventHandler(this.pageHeader_Format);
            // 
            // textBox1
            // 
            this.textBox1.Height = 0.3070866F;
            this.textBox1.Left = 0F;
            this.textBox1.MultiLine = false;
            this.textBox1.Name = "textBox1";
            this.textBox1.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: bold; text-align: center; v" +
    "ertical-align: middle";
            this.textBox1.Text = null;
            this.textBox1.Top = 0.7767717F;
            this.textBox1.Width = 6.931497F;
            // 
            // lblTitle01
            // 
            this.lblTitle01.Height = 0.2464567F;
            this.lblTitle01.HyperLink = null;
            this.lblTitle01.Left = 1.764567F;
            this.lblTitle01.Name = "lblTitle01";
            this.lblTitle01.Style = "font-family: ＭＳ 明朝; font-size: 15.75pt; font-weight: bold; text-align: center";
            this.lblTitle01.Text = "魚種コード変換マスタ一覧";
            this.lblTitle01.Top = 0.0937008F;
            this.lblTitle01.Width = 3.467717F;
            // 
            // line1
            // 
            this.line1.Height = 0F;
            this.line1.Left = 2.061811F;
            this.line1.LineWeight = 1F;
            this.line1.Name = "line1";
            this.line1.Top = 0.3401575F;
            this.line1.Width = 2.857874F;
            this.line1.X1 = 2.061811F;
            this.line1.X2 = 4.919685F;
            this.line1.Y1 = 0.3401575F;
            this.line1.Y2 = 0.3401575F;
            // 
            // txtToday
            // 
            this.txtToday.Height = 0.2070866F;
            this.txtToday.Left = 5.807874F;
            this.txtToday.MultiLine = false;
            this.txtToday.Name = "txtToday";
            this.txtToday.OutputFormat = resources.GetString("txtToday.OutputFormat");
            this.txtToday.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: bottom; d" +
    "do-char-set: 1";
            this.txtToday.Text = "ggyy年M月d日";
            this.txtToday.Top = 0.4433071F;
            this.txtToday.Width = 1.201969F;
            // 
            // lblPage
            // 
            this.lblPage.Height = 0.2070866F;
            this.lblPage.HyperLink = null;
            this.lblPage.Left = 6.426378F;
            this.lblPage.Name = "lblPage";
            this.lblPage.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: bottom; d" +
    "do-char-set: 1";
            this.lblPage.Text = "No.";
            this.lblPage.Top = 0.2362205F;
            this.lblPage.Width = 0.3401575F;
            // 
            // txtPageCount
            // 
            this.txtPageCount.Height = 0.2070866F;
            this.txtPageCount.Left = 6.766535F;
            this.txtPageCount.MultiLine = false;
            this.txtPageCount.Name = "txtPageCount";
            this.txtPageCount.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: bottom; d" +
    "do-char-set: 1";
            this.txtPageCount.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtPageCount.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.txtPageCount.Text = "999";
            this.txtPageCount.Top = 0.2362205F;
            this.txtPageCount.Width = 0.2433071F;
            // 
            // lblSakuseibi
            // 
            this.lblSakuseibi.Height = 0.2070866F;
            this.lblSakuseibi.HyperLink = null;
            this.lblSakuseibi.Left = 5.263386F;
            this.lblSakuseibi.Name = "lblSakuseibi";
            this.lblSakuseibi.Style = "font-family: ＭＳ 明朝; font-size: 10pt; text-align: right; vertical-align: bottom; d" +
    "do-char-set: 1";
            this.lblSakuseibi.Text = "作成日";
            this.lblSakuseibi.Top = 0.4433071F;
            this.lblSakuseibi.Width = 0.5444884F;
            // 
            // txtTitle04
            // 
            this.txtTitle04.Height = 0.2133859F;
            this.txtTitle04.Left = 4.770079F;
            this.txtTitle04.MultiLine = false;
            this.txtTitle04.Name = "txtTitle04";
            this.txtTitle04.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; v" +
    "ertical-align: middle; ddo-char-set: 128";
            this.txtTitle04.Text = "魚種名";
            this.txtTitle04.Top = 0.8744095F;
            this.txtTitle04.Width = 2.275984F;
            // 
            // txtTitle03
            // 
            this.txtTitle03.Height = 0.2133859F;
            this.txtTitle03.Left = 3.594095F;
            this.txtTitle03.MultiLine = false;
            this.txtTitle03.Name = "txtTitle03";
            this.txtTitle03.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; v" +
    "ertical-align: middle; ddo-char-set: 128";
            this.txtTitle03.Text = "魚種CD";
            this.txtTitle03.Top = 0.8704725F;
            this.txtTitle03.Width = 1.175984F;
            // 
            // txtTitle02
            // 
            this.txtTitle02.Height = 0.2114173F;
            this.txtTitle02.Left = 1.183858F;
            this.txtTitle02.MultiLine = false;
            this.txtTitle02.Name = "txtTitle02";
            this.txtTitle02.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; v" +
    "ertical-align: middle; ddo-char-set: 128";
            this.txtTitle02.Text = "名護魚種名";
            this.txtTitle02.Top = 0.872441F;
            this.txtTitle02.Width = 2.410236F;
            // 
            // txtTitle01
            // 
            this.txtTitle01.Height = 0.2114174F;
            this.txtTitle01.Left = 0F;
            this.txtTitle01.MultiLine = false;
            this.txtTitle01.Name = "txtTitle01";
            this.txtTitle01.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; v" +
    "ertical-align: middle; ddo-char-set: 128";
            this.txtTitle01.Text = "名護魚種CD";
            this.txtTitle01.Top = 0.872441F;
            this.txtTitle01.Width = 1.183858F;
            // 
            // line3
            // 
            this.line3.Height = 0F;
            this.line3.Left = 0F;
            this.line3.LineWeight = 3F;
            this.line3.Name = "line3";
            this.line3.Top = 1.083858F;
            this.line3.Width = 7.046063F;
            this.line3.X1 = 0F;
            this.line3.X2 = 7.046063F;
            this.line3.Y1 = 1.083858F;
            this.line3.Y2 = 1.083858F;
            // 
            // line6
            // 
            this.line6.Height = 8.940697E-08F;
            this.line6.Left = 6.426378F;
            this.line6.LineWeight = 1F;
            this.line6.Name = "line6";
            this.line6.Top = 0.4433071F;
            this.line6.Width = 0.5834651F;
            this.line6.X1 = 6.426378F;
            this.line6.X2 = 7.009843F;
            this.line6.Y1 = 0.4433071F;
            this.line6.Y2 = 0.4433072F;
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtKenGyoCd,
            this.txtKenGyoNm,
            this.txtGyoshuCd,
            this.txtGyoshuNm,
            this.line18,
            this.line4,
            this.line11,
            this.line16,
            this.line5,
            this.line2,
            this.line7});
            this.detail.Height = 0.2291339F;
            this.detail.Name = "detail";
            // 
            // txtKenGyoCd
            // 
            this.txtKenGyoCd.DataField = "ITEM01";
            this.txtKenGyoCd.Height = 0.1979167F;
            this.txtKenGyoCd.Left = 0.07283465F;
            this.txtKenGyoCd.Name = "txtKenGyoCd";
            this.txtKenGyoCd.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 128";
            this.txtKenGyoCd.Text = null;
            this.txtKenGyoCd.Top = 0F;
            this.txtKenGyoCd.Width = 0.9858268F;
            // 
            // txtKenGyoNm
            // 
            this.txtKenGyoNm.DataField = "ITEM02";
            this.txtKenGyoNm.Height = 0.1979167F;
            this.txtKenGyoNm.Left = 1.215354F;
            this.txtKenGyoNm.Name = "txtKenGyoNm";
            this.txtKenGyoNm.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; vertical-align: middle";
            this.txtKenGyoNm.Text = null;
            this.txtKenGyoNm.Top = 0F;
            this.txtKenGyoNm.Width = 2.326772F;
            // 
            // txtGyoshuCd
            // 
            this.txtGyoshuCd.DataField = "ITEM03";
            this.txtGyoshuCd.Height = 0.1979167F;
            this.txtGyoshuCd.Left = 3.664961F;
            this.txtGyoshuCd.Name = "txtGyoshuCd";
            this.txtGyoshuCd.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 128";
            this.txtGyoshuCd.Text = null;
            this.txtGyoshuCd.Top = 0F;
            this.txtGyoshuCd.Width = 1.051181F;
            // 
            // txtGyoshuNm
            // 
            this.txtGyoshuNm.DataField = "ITEM04";
            this.txtGyoshuNm.Height = 0.1979167F;
            this.txtGyoshuNm.Left = 4.801575F;
            this.txtGyoshuNm.Name = "txtGyoshuNm";
            this.txtGyoshuNm.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; vertical-align: middle; ddo-char-set: 128";
            this.txtGyoshuNm.Text = null;
            this.txtGyoshuNm.Top = 0F;
            this.txtGyoshuNm.Width = 2.208268F;
            // 
            // line18
            // 
            this.line18.Height = 0.2291339F;
            this.line18.Left = 7.043306F;
            this.line18.LineWeight = 1F;
            this.line18.Name = "line18";
            this.line18.Top = 0F;
            this.line18.Width = 0F;
            this.line18.X1 = 7.043306F;
            this.line18.X2 = 7.043306F;
            this.line18.Y1 = 0F;
            this.line18.Y2 = 0.2291339F;
            // 
            // line4
            // 
            this.line4.Height = 0.2291339F;
            this.line4.Left = 4.77008F;
            this.line4.LineWeight = 1F;
            this.line4.Name = "line4";
            this.line4.Top = 0F;
            this.line4.Width = 0F;
            this.line4.X1 = 4.77008F;
            this.line4.X2 = 4.77008F;
            this.line4.Y1 = 0F;
            this.line4.Y2 = 0.2291339F;
            // 
            // line11
            // 
            this.line11.Height = 0.2291339F;
            this.line11.Left = 3.594095F;
            this.line11.LineWeight = 1F;
            this.line11.Name = "line11";
            this.line11.Top = 0F;
            this.line11.Width = 0F;
            this.line11.X1 = 3.594095F;
            this.line11.X2 = 3.594095F;
            this.line11.Y1 = 0F;
            this.line11.Y2 = 0.2291339F;
            // 
            // line16
            // 
            this.line16.Height = 0.2291339F;
            this.line16.Left = 1.183858F;
            this.line16.LineWeight = 1F;
            this.line16.Name = "line16";
            this.line16.Top = 0F;
            this.line16.Width = 0F;
            this.line16.X1 = 1.183858F;
            this.line16.X2 = 1.183858F;
            this.line16.Y1 = 0F;
            this.line16.Y2 = 0.2291339F;
            // 
            // line5
            // 
            this.line5.Height = 0F;
            this.line5.Left = 0F;
            this.line5.LineWeight = 1F;
            this.line5.Name = "line5";
            this.line5.Top = 0F;
            this.line5.Width = 7.046062F;
            this.line5.X1 = 0F;
            this.line5.X2 = 7.046062F;
            this.line5.Y1 = 0F;
            this.line5.Y2 = 0F;
            // 
            // line2
            // 
            this.line2.Height = 0F;
            this.line2.Left = 0F;
            this.line2.LineWeight = 1F;
            this.line2.Name = "line2";
            this.line2.Top = 0.2291339F;
            this.line2.Width = 7.046062F;
            this.line2.X1 = 0F;
            this.line2.X2 = 7.046062F;
            this.line2.Y1 = 0.2291339F;
            this.line2.Y2 = 0.2291339F;
            // 
            // line7
            // 
            this.line7.Height = 0.2291339F;
            this.line7.Left = 0F;
            this.line7.LineWeight = 1F;
            this.line7.Name = "line7";
            this.line7.Top = 0F;
            this.line7.Width = 0F;
            this.line7.X1 = 0F;
            this.line7.X2 = 0F;
            this.line7.Y1 = 0F;
            this.line7.Y2 = 0.2291339F;
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            // 
            // HNCM1111R
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.7874016F;
            this.PageSettings.Margins.Left = 0.7086614F;
            this.PageSettings.Margins.Right = 0.472441F;
            this.PageSettings.Margins.Top = 0.5905512F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Portrait;
            this.PageSettings.PaperHeight = 11.69291F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.267716F;
            this.PrintWidth = 7.046063F;
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.pageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtToday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSakuseibi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle04)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKenGyoCd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKenGyoNm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyoshuCd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGyoshuNm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox1;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle01;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtToday;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPageCount;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSakuseibi;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle04;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle03;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle02;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle01;
        private GrapeCity.ActiveReports.SectionReportModel.Line line3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKenGyoCd;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKenGyoNm;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGyoshuCd;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGyoshuNm;
        private GrapeCity.ActiveReports.SectionReportModel.Line line6;
        private GrapeCity.ActiveReports.SectionReportModel.Line line18;
        private GrapeCity.ActiveReports.SectionReportModel.Line line4;
        private GrapeCity.ActiveReports.SectionReportModel.Line line11;
        private GrapeCity.ActiveReports.SectionReportModel.Line line16;
        private GrapeCity.ActiveReports.SectionReportModel.Line line5;
        private GrapeCity.ActiveReports.SectionReportModel.Line line2;
        private GrapeCity.ActiveReports.SectionReportModel.Line line7;
    }
}
