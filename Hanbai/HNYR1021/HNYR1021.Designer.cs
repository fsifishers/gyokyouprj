﻿namespace jp.co.fsi.hn.hnyr1021
{
    partial class HNYR1021
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbxDate = new System.Windows.Forms.GroupBox();
            this.rdoShimoki = new System.Windows.Forms.RadioButton();
            this.lblDateYear = new System.Windows.Forms.Label();
            this.rdoKamiki = new System.Windows.Forms.RadioButton();
            this.txtDateYear = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblDateGengo = new System.Windows.Forms.Label();
            this.lblDate = new System.Windows.Forms.Label();
            this.gbxFunanushiCd = new System.Windows.Forms.GroupBox();
            this.lblFunanushiCdTo = new System.Windows.Forms.Label();
            this.lblCodeBet = new System.Windows.Forms.Label();
            this.txtFunanushiCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblFunanushiCdFr = new System.Windows.Forms.Label();
            this.txtFunanushiCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.gbxMizuageShisho = new System.Windows.Forms.GroupBox();
            this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMizuageShishoNm = new System.Windows.Forms.Label();
            this.lblMizuageShisho = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.gbxDate.SuspendLayout();
            this.gbxFunanushiCd.SuspendLayout();
            this.gbxMizuageShisho.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Text = "操業制限損失保証一覧表";
            // 
            // btnF12
            // 
            this.btnF12.Text = "F12";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Size = new System.Drawing.Size(847, 100);
            // 
            // gbxDate
            // 
            this.gbxDate.Controls.Add(this.rdoShimoki);
            this.gbxDate.Controls.Add(this.lblDateYear);
            this.gbxDate.Controls.Add(this.rdoKamiki);
            this.gbxDate.Controls.Add(this.txtDateYear);
            this.gbxDate.Controls.Add(this.lblDateGengo);
            this.gbxDate.Controls.Add(this.lblDate);
            this.gbxDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.gbxDate.ForeColor = System.Drawing.Color.Black;
            this.gbxDate.Location = new System.Drawing.Point(12, 138);
            this.gbxDate.Name = "gbxDate";
            this.gbxDate.Size = new System.Drawing.Size(231, 66);
            this.gbxDate.TabIndex = 1;
            this.gbxDate.TabStop = false;
            this.gbxDate.Text = "年指定";
            // 
            // rdoShimoki
            // 
            this.rdoShimoki.AutoSize = true;
            this.rdoShimoki.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoShimoki.Location = new System.Drawing.Point(136, 35);
            this.rdoShimoki.Name = "rdoShimoki";
            this.rdoShimoki.Size = new System.Drawing.Size(53, 17);
            this.rdoShimoki.TabIndex = 5;
            this.rdoShimoki.TabStop = true;
            this.rdoShimoki.Text = "下期";
            this.rdoShimoki.UseVisualStyleBackColor = true;
            // 
            // lblDateYear
            // 
            this.lblDateYear.BackColor = System.Drawing.Color.Silver;
            this.lblDateYear.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateYear.ForeColor = System.Drawing.Color.Black;
            this.lblDateYear.Location = new System.Drawing.Point(89, 24);
            this.lblDateYear.Name = "lblDateYear";
            this.lblDateYear.Size = new System.Drawing.Size(17, 21);
            this.lblDateYear.TabIndex = 3;
            this.lblDateYear.Text = "年";
            this.lblDateYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // rdoKamiki
            // 
            this.rdoKamiki.AutoSize = true;
            this.rdoKamiki.Checked = true;
            this.rdoKamiki.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoKamiki.Location = new System.Drawing.Point(136, 17);
            this.rdoKamiki.Name = "rdoKamiki";
            this.rdoKamiki.Size = new System.Drawing.Size(53, 17);
            this.rdoKamiki.TabIndex = 4;
            this.rdoKamiki.TabStop = true;
            this.rdoKamiki.Text = "上期";
            this.rdoKamiki.UseVisualStyleBackColor = true;
            // 
            // txtDateYear
            // 
            this.txtDateYear.AutoSizeFromLength = false;
            this.txtDateYear.DisplayLength = null;
            this.txtDateYear.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateYear.ForeColor = System.Drawing.Color.Black;
            this.txtDateYear.Location = new System.Drawing.Point(57, 24);
            this.txtDateYear.MaxLength = 2;
            this.txtDateYear.Name = "txtDateYear";
            this.txtDateYear.Size = new System.Drawing.Size(30, 20);
            this.txtDateYear.TabIndex = 2;
            this.txtDateYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateYear.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateYear_Validating);
            // 
            // lblDateGengo
            // 
            this.lblDateGengo.BackColor = System.Drawing.Color.Silver;
            this.lblDateGengo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDateGengo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateGengo.ForeColor = System.Drawing.Color.Black;
            this.lblDateGengo.Location = new System.Drawing.Point(12, 24);
            this.lblDateGengo.Name = "lblDateGengo";
            this.lblDateGengo.Size = new System.Drawing.Size(41, 21);
            this.lblDateGengo.TabIndex = 1;
            this.lblDateGengo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDate
            // 
            this.lblDate.BackColor = System.Drawing.Color.Silver;
            this.lblDate.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDate.ForeColor = System.Drawing.Color.Black;
            this.lblDate.Location = new System.Drawing.Point(9, 21);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(103, 27);
            this.lblDate.TabIndex = 1;
            this.lblDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // gbxFunanushiCd
            // 
            this.gbxFunanushiCd.Controls.Add(this.lblFunanushiCdTo);
            this.gbxFunanushiCd.Controls.Add(this.lblCodeBet);
            this.gbxFunanushiCd.Controls.Add(this.txtFunanushiCdFr);
            this.gbxFunanushiCd.Controls.Add(this.lblFunanushiCdFr);
            this.gbxFunanushiCd.Controls.Add(this.txtFunanushiCdTo);
            this.gbxFunanushiCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.gbxFunanushiCd.ForeColor = System.Drawing.Color.Black;
            this.gbxFunanushiCd.Location = new System.Drawing.Point(12, 222);
            this.gbxFunanushiCd.Name = "gbxFunanushiCd";
            this.gbxFunanushiCd.Size = new System.Drawing.Size(579, 75);
            this.gbxFunanushiCd.TabIndex = 3;
            this.gbxFunanushiCd.TabStop = false;
            this.gbxFunanushiCd.Text = "船主CD範囲";
            // 
            // lblFunanushiCdTo
            // 
            this.lblFunanushiCdTo.BackColor = System.Drawing.Color.Silver;
            this.lblFunanushiCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFunanushiCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFunanushiCdTo.Location = new System.Drawing.Point(349, 32);
            this.lblFunanushiCdTo.Name = "lblFunanushiCdTo";
            this.lblFunanushiCdTo.Size = new System.Drawing.Size(217, 20);
            this.lblFunanushiCdTo.TabIndex = 4;
            this.lblFunanushiCdTo.Text = "最　後";
            this.lblFunanushiCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCodeBet
            // 
            this.lblCodeBet.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblCodeBet.Location = new System.Drawing.Point(280, 31);
            this.lblCodeBet.Name = "lblCodeBet";
            this.lblCodeBet.Size = new System.Drawing.Size(18, 20);
            this.lblCodeBet.TabIndex = 2;
            this.lblCodeBet.Text = "～";
            this.lblCodeBet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFunanushiCdFr
            // 
            this.txtFunanushiCdFr.AutoSizeFromLength = false;
            this.txtFunanushiCdFr.DisplayLength = null;
            this.txtFunanushiCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFunanushiCdFr.Location = new System.Drawing.Point(13, 32);
            this.txtFunanushiCdFr.MaxLength = 4;
            this.txtFunanushiCdFr.Name = "txtFunanushiCdFr";
            this.txtFunanushiCdFr.Size = new System.Drawing.Size(40, 20);
            this.txtFunanushiCdFr.TabIndex = 0;
            this.txtFunanushiCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFunanushiCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtCodeFr_Validating);
            // 
            // lblFunanushiCdFr
            // 
            this.lblFunanushiCdFr.BackColor = System.Drawing.Color.Silver;
            this.lblFunanushiCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFunanushiCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFunanushiCdFr.Location = new System.Drawing.Point(56, 32);
            this.lblFunanushiCdFr.Name = "lblFunanushiCdFr";
            this.lblFunanushiCdFr.Size = new System.Drawing.Size(217, 20);
            this.lblFunanushiCdFr.TabIndex = 1;
            this.lblFunanushiCdFr.Text = "先　頭";
            this.lblFunanushiCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFunanushiCdTo
            // 
            this.txtFunanushiCdTo.AutoSizeFromLength = false;
            this.txtFunanushiCdTo.DisplayLength = null;
            this.txtFunanushiCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFunanushiCdTo.Location = new System.Drawing.Point(306, 32);
            this.txtFunanushiCdTo.MaxLength = 4;
            this.txtFunanushiCdTo.Name = "txtFunanushiCdTo";
            this.txtFunanushiCdTo.Size = new System.Drawing.Size(40, 20);
            this.txtFunanushiCdTo.TabIndex = 3;
            this.txtFunanushiCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFunanushiCdTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtFunanushiCdTo_KeyDown);
            this.txtFunanushiCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtCodeTo_Validating);
            // 
            // gbxMizuageShisho
            // 
            this.gbxMizuageShisho.Controls.Add(this.txtMizuageShishoCd);
            this.gbxMizuageShisho.Controls.Add(this.lblMizuageShishoNm);
            this.gbxMizuageShisho.Controls.Add(this.lblMizuageShisho);
            this.gbxMizuageShisho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.gbxMizuageShisho.ForeColor = System.Drawing.Color.Black;
            this.gbxMizuageShisho.Location = new System.Drawing.Point(12, 51);
            this.gbxMizuageShisho.Name = "gbxMizuageShisho";
            this.gbxMizuageShisho.Size = new System.Drawing.Size(334, 77);
            this.gbxMizuageShisho.TabIndex = 0;
            this.gbxMizuageShisho.TabStop = false;
            this.gbxMizuageShisho.Text = "水揚支所";
            // 
            // txtMizuageShishoCd
            // 
            this.txtMizuageShishoCd.AutoSizeFromLength = true;
            this.txtMizuageShishoCd.DisplayLength = null;
            this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtMizuageShishoCd.Location = new System.Drawing.Point(74, 33);
            this.txtMizuageShishoCd.MaxLength = 4;
            this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
            this.txtMizuageShishoCd.Size = new System.Drawing.Size(34, 20);
            this.txtMizuageShishoCd.TabIndex = 1;
            this.txtMizuageShishoCd.TabStop = false;
            this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
            // 
            // lblMizuageShishoNm
            // 
            this.lblMizuageShishoNm.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMizuageShishoNm.Location = new System.Drawing.Point(109, 33);
            this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
            this.lblMizuageShishoNm.Size = new System.Drawing.Size(212, 20);
            this.lblMizuageShishoNm.TabIndex = 2;
            this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMizuageShisho
            // 
            this.lblMizuageShisho.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShisho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShisho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblMizuageShisho.Location = new System.Drawing.Point(10, 31);
            this.lblMizuageShisho.Name = "lblMizuageShisho";
            this.lblMizuageShisho.Size = new System.Drawing.Size(315, 25);
            this.lblMizuageShisho.TabIndex = 0;
            this.lblMizuageShisho.Text = "水揚支所";
            this.lblMizuageShisho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // HNYR1021
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 638);
            this.Controls.Add(this.gbxMizuageShisho);
            this.Controls.Add(this.gbxFunanushiCd);
            this.Controls.Add(this.gbxDate);
            this.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "HNYR1021";
            this.Text = "";
            this.Controls.SetChildIndex(this.gbxDate, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.gbxFunanushiCd, 0);
            this.Controls.SetChildIndex(this.gbxMizuageShisho, 0);
            this.pnlDebug.ResumeLayout(false);
            this.gbxDate.ResumeLayout(false);
            this.gbxDate.PerformLayout();
            this.gbxFunanushiCd.ResumeLayout(false);
            this.gbxFunanushiCd.PerformLayout();
            this.gbxMizuageShisho.ResumeLayout(false);
            this.gbxMizuageShisho.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbxDate;
        private jp.co.fsi.common.controls.FsiTextBox txtDateYear;
        private System.Windows.Forms.Label lblDateGengo;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.Label lblDateYear;
        private System.Windows.Forms.GroupBox gbxFunanushiCd;
        private System.Windows.Forms.Label lblFunanushiCdTo;
        private System.Windows.Forms.Label lblCodeBet;
        private common.controls.FsiTextBox txtFunanushiCdFr;
        private System.Windows.Forms.Label lblFunanushiCdFr;
        private common.controls.FsiTextBox txtFunanushiCdTo;
        private System.Windows.Forms.RadioButton rdoShimoki;
        private System.Windows.Forms.RadioButton rdoKamiki;
        private System.Windows.Forms.GroupBox gbxMizuageShisho;
        private common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private System.Windows.Forms.Label lblMizuageShisho;
    }
}