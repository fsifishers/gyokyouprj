﻿using jp.co.fsi.common.report;
using System.Data;
using jp.co.fsi.common.util;
using System;

namespace jp.co.fsi.hn.hnyr1021
{
    /// <summary>
    /// HNYR1021R の概要の説明です。
    /// </summary>
    public partial class HNYR1021R : BaseReport
    {

        public HNYR1021R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }

        private void detail_Format(object sender, System.EventArgs e)
        {
            decimal suryogokei = Util.ToDecimal(this.テキスト011.Value) + Util.ToDecimal(this.テキスト012.Value) + Util.ToDecimal(this.テキスト013.Value)
                                 + Util.ToDecimal(this.テキスト014.Value) + Util.ToDecimal(this.テキスト015.Value) + Util.ToDecimal(this.テキスト016.Value);
            this.数量合計.Value = Util.ToString(suryogokei);

            decimal kingakugokei = Util.ToDecimal(this.テキスト017.Value) + Util.ToDecimal(this.テキスト018.Value) + Util.ToDecimal(this.テキスト019.Value)
                                 + Util.ToDecimal(this.テキスト020.Value) + Util.ToDecimal(this.テキスト021.Value) + Util.ToDecimal(this.テキスト022.Value);
            this.金額合計.Value = Util.ToString(kingakugokei);
        }

        private void reportFooter1_Format(object sender, System.EventArgs e)
        {
            decimal suryogokei = Util.ToDecimal(this.テキスト143.Value) + Util.ToDecimal(this.テキスト144.Value) + Util.ToDecimal(this.テキスト145.Value)
                                 + Util.ToDecimal(this.テキスト146.Value) + Util.ToDecimal(this.テキスト147.Value) + Util.ToDecimal(this.テキスト148.Value);
            this.数量総合計.Value = Util.ToString(suryogokei);

            decimal kingakugokei = Util.ToDecimal(this.テキスト149.Value) + Util.ToDecimal(this.テキスト150.Value) + Util.ToDecimal(this.テキスト151.Value)
                                 + Util.ToDecimal(this.テキスト152.Value) + Util.ToDecimal(this.テキスト153.Value) + Util.ToDecimal(this.テキスト154.Value);
            this.金額総合計.Value = Util.ToString(kingakugokei);
        }

        private void pageHeader_Format(object sender, System.EventArgs e)
        {
            テキストData.Text = DateTime.Now.ToString("yyyy/MM/dd");
        }
    }
}
