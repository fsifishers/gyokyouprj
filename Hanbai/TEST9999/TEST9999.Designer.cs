﻿namespace jp.co.fsi.han.test9999
{
    partial class TEST9999
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblDateFr = new System.Windows.Forms.Label();
            this.lblDateGengoFr = new System.Windows.Forms.Label();
            this.txtDateMonthFr = new jp.co.fsi.common.controls.SosTextBox();
            this.txtDateYearFr = new jp.co.fsi.common.controls.SosTextBox();
            this.txtDateDayFr = new jp.co.fsi.common.controls.SosTextBox();
            this.labelDateYearFr = new System.Windows.Forms.Label();
            this.lblDateMonthFr = new System.Windows.Forms.Label();
            this.lblDateDayFr = new System.Windows.Forms.Label();
            this.lblDateDayTo = new System.Windows.Forms.Label();
            this.lblDateMonthTo = new System.Windows.Forms.Label();
            this.labelDateYearTo = new System.Windows.Forms.Label();
            this.txtDateDayTo = new jp.co.fsi.common.controls.SosTextBox();
            this.txtDateYearTo = new jp.co.fsi.common.controls.SosTextBox();
            this.txtDateMonthTo = new jp.co.fsi.common.controls.SosTextBox();
            this.lblDateGengoTo = new System.Windows.Forms.Label();
            this.lblDateTo = new System.Windows.Forms.Label();
            this.lblDateToBet = new System.Windows.Forms.Label();
            this.lblShiharaiKbn = new System.Windows.Forms.Label();
            this.txtShiharaiKbn = new jp.co.fsi.common.controls.SosTextBox();
            this.rdoChikugai = new System.Windows.Forms.RadioButton();
            this.rdoHamauri = new System.Windows.Forms.RadioButton();
            this.rdoChikunai = new System.Windows.Forms.RadioButton();
            this.pnlDebug.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Text = "動作テスト";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Size = new System.Drawing.Size(847, 100);
            this.pnlDebug.Visible = false;
            // 
            // lblDateFr
            // 
            this.lblDateFr.Location = new System.Drawing.Point(0, 0);
            this.lblDateFr.Name = "lblDateFr";
            this.lblDateFr.Size = new System.Drawing.Size(100, 23);
            this.lblDateFr.TabIndex = 0;
            // 
            // lblDateGengoFr
            // 
            this.lblDateGengoFr.Location = new System.Drawing.Point(0, 0);
            this.lblDateGengoFr.Name = "lblDateGengoFr";
            this.lblDateGengoFr.Size = new System.Drawing.Size(100, 23);
            this.lblDateGengoFr.TabIndex = 0;
            // 
            // txtDateMonthFr
            // 
            this.txtDateMonthFr.AutoSizeFromLength = false;
            this.txtDateMonthFr.DisplayLength = null;
            this.txtDateMonthFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtDateMonthFr.Location = new System.Drawing.Point(0, 0);
            this.txtDateMonthFr.Name = "txtDateMonthFr";
            this.txtDateMonthFr.Size = new System.Drawing.Size(100, 20);
            this.txtDateMonthFr.TabIndex = 0;
            // 
            // txtDateYearFr
            // 
            this.txtDateYearFr.AutoSizeFromLength = false;
            this.txtDateYearFr.DisplayLength = null;
            this.txtDateYearFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtDateYearFr.Location = new System.Drawing.Point(0, 0);
            this.txtDateYearFr.Name = "txtDateYearFr";
            this.txtDateYearFr.Size = new System.Drawing.Size(100, 20);
            this.txtDateYearFr.TabIndex = 0;
            // 
            // txtDateDayFr
            // 
            this.txtDateDayFr.AutoSizeFromLength = false;
            this.txtDateDayFr.DisplayLength = null;
            this.txtDateDayFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtDateDayFr.Location = new System.Drawing.Point(0, 0);
            this.txtDateDayFr.Name = "txtDateDayFr";
            this.txtDateDayFr.Size = new System.Drawing.Size(100, 20);
            this.txtDateDayFr.TabIndex = 0;
            // 
            // labelDateYearFr
            // 
            this.labelDateYearFr.Location = new System.Drawing.Point(0, 0);
            this.labelDateYearFr.Name = "labelDateYearFr";
            this.labelDateYearFr.Size = new System.Drawing.Size(100, 23);
            this.labelDateYearFr.TabIndex = 0;
            // 
            // lblDateMonthFr
            // 
            this.lblDateMonthFr.Location = new System.Drawing.Point(0, 0);
            this.lblDateMonthFr.Name = "lblDateMonthFr";
            this.lblDateMonthFr.Size = new System.Drawing.Size(100, 23);
            this.lblDateMonthFr.TabIndex = 0;
            // 
            // lblDateDayFr
            // 
            this.lblDateDayFr.Location = new System.Drawing.Point(0, 0);
            this.lblDateDayFr.Name = "lblDateDayFr";
            this.lblDateDayFr.Size = new System.Drawing.Size(100, 23);
            this.lblDateDayFr.TabIndex = 0;
            // 
            // lblDateDayTo
            // 
            this.lblDateDayTo.Location = new System.Drawing.Point(0, 0);
            this.lblDateDayTo.Name = "lblDateDayTo";
            this.lblDateDayTo.Size = new System.Drawing.Size(100, 23);
            this.lblDateDayTo.TabIndex = 0;
            // 
            // lblDateMonthTo
            // 
            this.lblDateMonthTo.Location = new System.Drawing.Point(0, 0);
            this.lblDateMonthTo.Name = "lblDateMonthTo";
            this.lblDateMonthTo.Size = new System.Drawing.Size(100, 23);
            this.lblDateMonthTo.TabIndex = 0;
            // 
            // labelDateYearTo
            // 
            this.labelDateYearTo.Location = new System.Drawing.Point(0, 0);
            this.labelDateYearTo.Name = "labelDateYearTo";
            this.labelDateYearTo.Size = new System.Drawing.Size(100, 23);
            this.labelDateYearTo.TabIndex = 0;
            // 
            // txtDateDayTo
            // 
            this.txtDateDayTo.AutoSizeFromLength = false;
            this.txtDateDayTo.DisplayLength = null;
            this.txtDateDayTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtDateDayTo.Location = new System.Drawing.Point(0, 0);
            this.txtDateDayTo.Name = "txtDateDayTo";
            this.txtDateDayTo.Size = new System.Drawing.Size(100, 20);
            this.txtDateDayTo.TabIndex = 0;
            // 
            // txtDateYearTo
            // 
            this.txtDateYearTo.AutoSizeFromLength = false;
            this.txtDateYearTo.DisplayLength = null;
            this.txtDateYearTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtDateYearTo.Location = new System.Drawing.Point(0, 0);
            this.txtDateYearTo.Name = "txtDateYearTo";
            this.txtDateYearTo.Size = new System.Drawing.Size(100, 20);
            this.txtDateYearTo.TabIndex = 0;
            // 
            // txtDateMonthTo
            // 
            this.txtDateMonthTo.AutoSizeFromLength = false;
            this.txtDateMonthTo.DisplayLength = null;
            this.txtDateMonthTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtDateMonthTo.Location = new System.Drawing.Point(0, 0);
            this.txtDateMonthTo.Name = "txtDateMonthTo";
            this.txtDateMonthTo.Size = new System.Drawing.Size(100, 20);
            this.txtDateMonthTo.TabIndex = 0;
            // 
            // lblDateGengoTo
            // 
            this.lblDateGengoTo.Location = new System.Drawing.Point(0, 0);
            this.lblDateGengoTo.Name = "lblDateGengoTo";
            this.lblDateGengoTo.Size = new System.Drawing.Size(100, 23);
            this.lblDateGengoTo.TabIndex = 0;
            // 
            // lblDateTo
            // 
            this.lblDateTo.Location = new System.Drawing.Point(0, 0);
            this.lblDateTo.Name = "lblDateTo";
            this.lblDateTo.Size = new System.Drawing.Size(100, 23);
            this.lblDateTo.TabIndex = 0;
            // 
            // lblDateToBet
            // 
            this.lblDateToBet.Location = new System.Drawing.Point(0, 0);
            this.lblDateToBet.Name = "lblDateToBet";
            this.lblDateToBet.Size = new System.Drawing.Size(100, 23);
            this.lblDateToBet.TabIndex = 0;
            // 
            // lblShiharaiKbn
            // 
            this.lblShiharaiKbn.Location = new System.Drawing.Point(0, 0);
            this.lblShiharaiKbn.Name = "lblShiharaiKbn";
            this.lblShiharaiKbn.Size = new System.Drawing.Size(100, 23);
            this.lblShiharaiKbn.TabIndex = 0;
            // 
            // txtShiharaiKbn
            // 
            this.txtShiharaiKbn.AutoSizeFromLength = false;
            this.txtShiharaiKbn.DisplayLength = null;
            this.txtShiharaiKbn.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtShiharaiKbn.Location = new System.Drawing.Point(0, 0);
            this.txtShiharaiKbn.Name = "txtShiharaiKbn";
            this.txtShiharaiKbn.Size = new System.Drawing.Size(100, 20);
            this.txtShiharaiKbn.TabIndex = 0;
            // 
            // rdoChikugai
            // 
            this.rdoChikugai.Location = new System.Drawing.Point(0, 0);
            this.rdoChikugai.Name = "rdoChikugai";
            this.rdoChikugai.Size = new System.Drawing.Size(104, 24);
            this.rdoChikugai.TabIndex = 0;
            // 
            // rdoHamauri
            // 
            this.rdoHamauri.Location = new System.Drawing.Point(0, 0);
            this.rdoHamauri.Name = "rdoHamauri";
            this.rdoHamauri.Size = new System.Drawing.Size(104, 24);
            this.rdoHamauri.TabIndex = 0;
            // 
            // rdoChikunai
            // 
            this.rdoChikunai.Location = new System.Drawing.Point(0, 0);
            this.rdoChikunai.Name = "rdoChikunai";
            this.rdoChikunai.Size = new System.Drawing.Size(104, 24);
            this.rdoChikunai.TabIndex = 0;
            // 
            // TEST9999
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 638);
            this.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "TEST9999";
            this.Text = "動作テスト";
            this.pnlDebug.ResumeLayout(false);
            this.ResumeLayout(false);

        }
        #endregion

        private System.Windows.Forms.Label lblDateFr;
        private System.Windows.Forms.Label lblDateGengoFr;
        private jp.co.fsi.common.controls.SosTextBox txtDateMonthFr;
        private jp.co.fsi.common.controls.SosTextBox txtDateYearFr;
        private jp.co.fsi.common.controls.SosTextBox txtDateDayFr;
        private System.Windows.Forms.Label labelDateYearFr;
        private System.Windows.Forms.Label lblDateMonthFr;
        private System.Windows.Forms.Label lblDateDayFr;
        private System.Windows.Forms.Label lblShiharaiKbn;
        private common.controls.SosTextBox txtShiharaiKbn;
        private System.Windows.Forms.Label lblDateDayTo;
        private System.Windows.Forms.Label lblDateMonthTo;
        private System.Windows.Forms.Label labelDateYearTo;
        private common.controls.SosTextBox txtDateDayTo;
        private common.controls.SosTextBox txtDateYearTo;
        private common.controls.SosTextBox txtDateMonthTo;
        private System.Windows.Forms.Label lblDateGengoTo;
        private System.Windows.Forms.Label lblDateTo;
        private System.Windows.Forms.Label lblDateToBet;
        private System.Windows.Forms.RadioButton rdoChikugai;
        private System.Windows.Forms.RadioButton rdoHamauri;
        private System.Windows.Forms.RadioButton rdoChikunai;
    }
}