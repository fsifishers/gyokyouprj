﻿using System;
using System.ComponentModel;
using System.Data;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.han.test9999
{
    /// <summary>
    /// 動作テスト(TEST9999)
    /// </summary>
    public partial class TEST9999 : BasePgForm
    {
        #region 定数
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public TEST9999()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            Msg.Info("動作テスト完了です！ご協力ありがとうございます！");
            this.Close();
        }
        #endregion

        #region イベント
        #endregion

        #region privateメソッド
        #endregion
    }
}
