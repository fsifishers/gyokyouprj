﻿using System;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.han.hanr1091
{
    /// <summary>
    /// セリ精算処理(HANR1091)
    /// </summary>
    public partial class HANR1091 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// 文字コード(SJIS)
        /// </summary>
        private const int CHR_CD_SJIS = 932; // "Shift_JIS"と同じはず
        #endregion

        #region メンバ変数（フィールド）
        /// <summary>
        /// 地区コードでエンターキーが押されたフラグ
        /// </summary>
        private bool enterKeyIsPressedInTxtAreaCd = false;

        // WebClientオブジェクト
        System.Net.WebClient wc;
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HANR1091()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            string[] jpDate = Util.ConvJpDate(DateTime.Now, this.Dba);
            lblDateGengoFr.Text = jpDate[0];
            txtDateYearFr.Text = jpDate[2];
            txtDateMonthFr.Text = jpDate[3];
            txtDateDayFr.Text = jpDate[4];

            lblDateGengoTo.Text = jpDate[0];
            txtDateYearTo.Text = jpDate[2];
            txtDateMonthTo.Text = jpDate[3];
            txtDateDayTo.Text = jpDate[4];

            // 初期フォーカス
            txtDateYearFr.Focus();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 日付、地区コードに
            // フォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtDateYearFr":
                case "txtDateYearTo":
                case "txtAreaCd":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;

            switch (this.ActiveCtlNm)
            {
                case "txtAreaCd":
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom("HANC9031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.han.hanc9031.HANC9031");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtAreaCd.Text = outData[0];
                                this.lblAreaNm.Text = outData[1];
                            }
                        }
                    }
                    break;

                case "txtDateYearFr":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom("COMC9011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.com.comc9011.COMC9011");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblDateGengoFr.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.lblDateGengoFr.Text = result[1];

                                // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
                                DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                                    this.txtDateMonthFr.Text, "1", this.Dba);
                                int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);
                                if (Util.ToInt(this.txtDateDayFr.Text) > lastDayInMonth)
                                {
                                    this.txtDateDayFr.Text = Util.ToString(lastDayInMonth);
                                }

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblDateGengoFr.Text,
                                        this.txtDateYearFr.Text,
                                        this.txtDateMonthFr.Text,
                                        this.txtDateDayFr.Text,
                                        this.Dba);
                                this.lblDateGengoFr.Text = arrJpDate[0];
                                this.txtDateYearFr.Text = arrJpDate[2];
                                this.txtDateMonthFr.Text = arrJpDate[3];
                                this.txtDateDayFr.Text = arrJpDate[4];
                            }
                        }
                    }
                    break;

                case "txtDateYearTo":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom("COMC9011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.com.comc9011.COMC9011");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblDateGengoTo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.lblDateGengoTo.Text = result[1];

                                // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
                                DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                                    this.txtDateMonthTo.Text, "1", this.Dba);
                                int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);
                                if (Util.ToInt(this.txtDateDayTo.Text) > lastDayInMonth)
                                {
                                    this.txtDateDayTo.Text = Util.ToString(lastDayInMonth);
                                }

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblDateGengoTo.Text,
                                        this.txtDateYearTo.Text,
                                        this.txtDateMonthTo.Text,
                                        this.txtDateDayTo.Text,
                                        this.Dba);
                                this.lblDateGengoTo.Text = arrJpDate[0];
                                this.txtDateYearTo.Text = arrJpDate[2];
                                this.txtDateMonthTo.Text = arrJpDate[3];
                                this.txtDateDayTo.Text = arrJpDate[4];
                            }
                        }
                    }
                    break;

            }
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF6();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF6()
        {
            // 会計年度の凍結処理チェック 凍結されていた場合はアラートを表示し処理を行なわない
            // ※ファイル作成時のSQLが会計年度に拘っていない かつ 年度またぎの日付を設定される場合を考慮し、下記コメントアウト kisemori
            /*
            if (Util.GetKaikeiNendoFixedFlg(this.UInfo.KaikeiNendo, this.Dba))
            {
                Msg.Error("この会計年度は凍結されています。");
                return;
            }
            */

            DateTime today = DateTime.Today;

            // 入力日付に該当する会計年度が今年度と一致するかチェック
            DateTime DENPYO_DATE_FR = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                    this.txtDateMonthFr.Text, this.txtDateDayFr.Text, this.Dba);
            if (Util.GetKaikeiNendo(DENPYO_DATE_FR, this.Dba) != Util.GetKaikeiNendo(today, this.Dba))
            {
                if (Msg.ConfNmYesNo("送信データ作成", "入力日付(自)が今年度の会計年度の範囲外ですが、宜しいですか？") == DialogResult.No)
                {
                    return;
                }
            }
            DateTime DENPYO_DATE_TO = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                    this.txtDateMonthTo.Text, this.txtDateDayTo.Text, this.Dba);
            if (Util.GetKaikeiNendo(DENPYO_DATE_TO, this.Dba) != Util.GetKaikeiNendo(today, this.Dba))
            {
                if (Msg.ConfNmYesNo("送信データ作成", "入力日付(至)が今年度の会計年度の範囲外ですが、宜しいですか？") == DialogResult.No)
                {
                    return;
                }
            }

            // 選択会計年度が今年度かチェック
            // ※ファイル作成時のSQLが会計年度に拘っていない かつ 年度またぎの日付を設定される場合を考慮し、下記コメントアウト kisemori
            /*
            if (Util.GetKaikeiNendo(today, this.Dba) != this.UInfo.KaikeiNendo)
            {
                if (Msg.ConfNmYesNo("送信データ作成", "選択会計年度が今年度ではありませんが、宜しいですか？") == DialogResult.No)
                {
                    return;
                }
            }
            */

            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            // 送信データ作成＆出力
            OutputSendFiles("old");

            this.txtAreaCd.Focus();
        }

        /// <summary>
        /// F7キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF7();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF7()
        {
            DateTime today = DateTime.Today;

            // 入力日付に該当する会計年度が今年度と一致するかチェック
            DateTime DENPYO_DATE_FR = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                    this.txtDateMonthFr.Text, this.txtDateDayFr.Text, this.Dba);
            if (Util.GetKaikeiNendo(DENPYO_DATE_FR, this.Dba) != Util.GetKaikeiNendo(today, this.Dba))
            {
                if (Msg.ConfNmYesNo("送信データ作成", "入力日付(自)が今年度の会計年度の範囲外ですが、宜しいですか？") == DialogResult.No)
                {
                    return;
                }
            }
            DateTime DENPYO_DATE_TO = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                    this.txtDateMonthTo.Text, this.txtDateDayTo.Text, this.Dba);
            if (Util.GetKaikeiNendo(DENPYO_DATE_TO, this.Dba) != Util.GetKaikeiNendo(today, this.Dba))
            {
                if (Msg.ConfNmYesNo("送信データ作成", "入力日付(至)が今年度の会計年度の範囲外ですが、宜しいですか？") == DialogResult.No)
                {
                    return;
                }
            }

            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            // 送信データ作成＆出力
            OutputSendFiles("new");

            this.txtAreaCd.Focus();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 地区コードの値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtAreaCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidAreaCd())
            {
                e.Cancel = true;
                this.txtAreaCd.SelectAll();
                this.lblAreaNm.Text = "";
                return;
            }

            // コードを元に名称を取得する
            this.lblAreaNm.Text = this.Dba.GetName(this.UInfo, "TM_HN_CHIKU_MST", this.txtAreaCd.Text);
        }

        /// <summary>
        /// 和暦(年)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateYearFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidDateYear(this.txtDateYearFr.Text))
            {
                e.Cancel = true;
                this.txtDateYearFr.SelectAll();
            }
            else
            {
                // 空の場合、0年として処理
                if (ValChk.IsEmpty(this.txtDateYearFr.Text))
                {
                    this.txtDateYearFr.Text = "0";
                }

                // 年月日(自)の月末入力チェック
                this.txtDateDayFr.Text = CheckDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text, this.txtDateMonthFr.Text, this.txtDateDayFr.Text);
                SetDateFr();
            }
        }

        /// <summary>
        /// 和暦(月)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateMonth_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidDateMonth(this.txtDateMonthFr.Text))
            {
                e.Cancel = true;
                this.txtDateMonthFr.SelectAll();
            }
            else
            {
                // 入力月が範囲外時の場合に1又は12月を返す
                this.txtDateMonthFr.Text = ValueOfTheCaseOutsideTheMonthRange(this.txtDateMonthFr.Text);

                // 年月日(自)の月末入力チェック
                this.txtDateDayFr.Text = CheckDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text, this.txtDateMonthFr.Text, this.txtDateDayFr.Text);
                SetDateFr();
            }
        }

        /// <summary>
        /// 和暦(日)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateDay_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidDateDay(this.txtDateDayFr.Text))
            {
                e.Cancel = true;
                this.txtDateDayFr.SelectAll();
            }
            else
            {
                // 入力日が範囲外時の場合に1日を返す
                this.txtDateDayFr.Text = this.ValueOfTheCaseOutsideTheDateRange(this.txtDateDayFr.Text);

                // 年月日(自)の月末入力チェック
                this.txtDateDayFr.Text = CheckDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text, this.txtDateMonthFr.Text, this.txtDateDayFr.Text);
                SetDateFr();
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 年(自)の入力チェック
        /// </summary>
        /// <param name="dateYear">入力年</param>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidDateYear(string dateYear)
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(dateYear))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 月(自)の入力チェック
        /// </summary>
        /// <param name="dateMonth">入力月</param>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidDateMonth(string dateMonth)
        {
            // 数字以外が入力されたらエラーメッセージ
            //if (!ValChk.IsNumber(this.txtDateMonthFr.Text))
            if (!ValChk.IsNumber(dateMonth))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 日(自)の入力チェック
        /// </summary>
        /// <param name="dateDay">入力日</param>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidDateDay(string dateDay)
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(dateDay))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 年月日(自)の月末入力チェック
        /// </summary>
        /// <param name="dateGengo">元号</param>
        /// <param name="dateYear">入力年</param>
        /// <param name="dateMonth">入力月</param>
        /// <param name="dateDay">入力日</param>
        /// <returns>string 月末日</returns>
        /// 
        private string CheckDate(string dateGengo, string dateYear, string dateMonth, string dateDay)
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(dateGengo, dateYear, dateMonth, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            //if (Util.ToInt(this.txtDateDayFr.Text) > lastDayInMonth)
            if (Util.ToInt(dateDay) > lastDayInMonth)
            {
                //this.txtDateDayFr.Text = Util.ToString(lastDayInMonth);
                dateDay = Util.ToString(lastDayInMonth);
            }

            return dateDay;
        }

        /// <summary>
        /// 年月日(自)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetDateFr()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDateFr(Util.FixJpDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                this.txtDateMonthFr.Text, this.txtDateDayFr.Text, this.Dba));
        }

        /// <summary>
        /// 年月日(至)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetDateTo()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDateTo(Util.FixJpDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                this.txtDateMonthTo.Text, this.txtDateDayTo.Text, this.Dba));
        }

        /// <summary>
        /// 和暦(年)(至)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateYearTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidDateYear(this.txtDateYearTo.Text))
            {
                e.Cancel = true;
                this.txtDateYearTo.SelectAll();
            }
            else
            {
                // 空の場合、0年として処理
                if (ValChk.IsEmpty(this.txtDateYearTo.Text))
                {
                    this.txtDateYearTo.Text = "0";
                }

                // 年月日(自)の月末入力チェック
                this.txtDateDayTo.Text = CheckDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text, this.txtDateMonthTo.Text, this.txtDateDayTo.Text);
                SetDateTo();
            }
        }

        /// <summary>
        /// 和暦(月)(至)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateMonthTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidDateMonth(this.txtDateMonthTo.Text))
            {
                e.Cancel = true;
                this.txtDateMonthTo.SelectAll();
            }
            else
            {
                // 入力月が範囲外時の場合に1又は12月を返す
                this.txtDateMonthTo.Text = ValueOfTheCaseOutsideTheMonthRange(this.txtDateMonthTo.Text);

                // 年月日(自)の月末入力チェック
                this.txtDateDayTo.Text = CheckDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text, this.txtDateMonthTo.Text, this.txtDateDayTo.Text);
                SetDateTo();
            }
        }

        /// <summary>
        /// 和暦(日)(至)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateDayTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidDateDay(this.txtDateDayTo.Text))
            {
                e.Cancel = true;
                this.txtDateDayTo.SelectAll();
            }
            else
            {
                // 入力日が範囲外時の場合に1日を返す
                this.txtDateDayTo.Text = this.ValueOfTheCaseOutsideTheDateRange(this.txtDateDayTo.Text);

                // 年月日(自)の月末入力チェック
                this.txtDateDayTo.Text = CheckDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text, this.txtDateMonthTo.Text, this.txtDateDayTo.Text);
                SetDateTo();
            }
        }

        /// <summary>
        /// 入力月が範囲外時の場合に1又は12月を返す（入力チェック後に呼び出すこと）
        /// </summary>
        /// <param name="dateMonth">入力された月</param>
        /// <returns>月の範囲内に収めた月</returns>
        private string ValueOfTheCaseOutsideTheMonthRange(string dateMonth)
        {
            // 空の場合、1月として処理
            if (ValChk.IsEmpty(dateMonth))
            {
                dateMonth = "1";
            }

            int inputMonth = Util.ToInt(dateMonth);

            // 12を超える月が入力された場合、12月として処理
            if (inputMonth > 12)
            {
                dateMonth = "12";
            }
            // 1より小さい月が入力された場合、1月として処理
            else if (inputMonth < 1)
            {
                dateMonth = "1";
            }

            return dateMonth;
        }

        /// <summary>
        /// 入力日が範囲外時の日を返す
        /// </summary>
        /// <returns>string 年月日の日</returns>
        private string ValueOfTheCaseOutsideTheDateRange(string dateDay)
        {
            if (ValChk.IsEmpty(dateDay))
            {
                // 空の場合、1日として処理
                dateDay = "1";
            }
            else
            {
                // 1より小さい日が入力された場合、1日として処理
                if (Util.ToInt(dateDay) < 1)
                {
                    dateDay = "1";
                }
            }

            return dateDay;
        }

        /// <summary>
        /// 地区コードの入力チェック
        /// </summary>
        private bool IsValidAreaCd()
        {
            if (ValChk.IsEmpty(this.txtAreaCd.Text))
            {
                Msg.Error("地区コードを指定してください。");
                return false;
            }

            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtAreaCd.Text))
            {
                Msg.Error("地区コードは数値のみで入力してください。");
                return false;
            }

            if (ValChk.IsEmpty(this.Dba.GetName(this.UInfo, "TM_HN_CHIKU_MST", this.txtAreaCd.Text)))
            {
                Msg.Error("存在しない地区コードです。");
                return false;
            }

            // 地区コード入力の最後にエンターキーが押下された場合は、ファイル出力処理を行う
            if (this.enterKeyIsPressedInTxtAreaCd)
            {
                this.enterKeyIsPressedInTxtAreaCd = false;

                // 全て項目チェックと送信ファイルを作成して出力する
                PressF6();
            }

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 年(自)のチェック
            if (!IsValidDateYear(this.txtDateYearFr.Text))
            {
                this.txtDateYearFr.Focus();
                this.txtDateYearFr.SelectAll();
                return false;
            }
            // 月(自)のチェック
            if (!IsValidDateMonth(this.txtDateMonthFr.Text))
            {
                this.txtDateMonthFr.Focus();
                this.txtDateMonthFr.SelectAll();
                return false;
            }
            else
            {
                // 入力月が範囲外時の場合に1又は12月を返す
                this.txtDateMonthFr.Text = ValueOfTheCaseOutsideTheMonthRange(this.txtDateMonthFr.Text);
            }
            // 日(自)のチェック
            if (!IsValidDateDay(this.txtDateDayTo.Text))
            {
                this.txtDateDayFr.Focus();
                this.txtDateDayFr.SelectAll();
                return false;
            }
            else
            {
                // 入力日が範囲外時の場合に1日を返す
                this.txtDateDayFr.Text = this.ValueOfTheCaseOutsideTheDateRange(this.txtDateDayFr.Text);
            }
            // 年月日(自)の月末入力チェック処理
            this.txtDateDayFr.Text = CheckDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text, this.txtDateMonthFr.Text, this.txtDateDayFr.Text);
            // 年月日(自)の正しい和暦への変換処理
            SetDateFr();

            // 年(至)のチェック
            if (!IsValidDateYear(this.txtDateYearTo.Text))
            {
                this.txtDateYearTo.Focus();
                this.txtDateYearTo.SelectAll();
                return false;
            }
            // 月(至)のチェック
            if (!IsValidDateMonth(this.txtDateMonthTo.Text))
            {
                this.txtDateMonthTo.Focus();
                this.txtDateMonthTo.SelectAll();
                return false;
            }
            else
            {
                // 入力月が範囲外時の場合に1又は12月を返す
                this.txtDateMonthTo.Text = ValueOfTheCaseOutsideTheMonthRange(this.txtDateMonthTo.Text);
            }
            // 日(至)のチェック
            if (!IsValidDateDay(this.txtDateDayTo.Text))
            {
                this.txtDateDayTo.Focus();
                this.txtDateDayTo.SelectAll();
                return false;
            }
            else
            {
                // 入力日が範囲外時の場合に1日を返す
                this.txtDateDayTo.Text = this.ValueOfTheCaseOutsideTheDateRange(this.txtDateDayTo.Text);
            }
            // 年月日(自)の月末入力チェック処理
            this.txtDateDayTo.Text = CheckDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text, this.txtDateMonthTo.Text, this.txtDateDayTo.Text);
            // 年月日(自)の正しい和暦への変換処理
            SetDateTo();


            // 地区コードの入力チェック
            if (!IsValidAreaCd())
            {
                this.txtAreaCd.Focus();
                this.txtAreaCd.SelectAll();
                this.lblAreaNm.Text = "";
                return false;
            }
            // コードを元に名称を取得する
            this.lblAreaNm.Text = this.Dba.GetName(this.UInfo, "TM_HN_CHIKU_MST", this.txtAreaCd.Text);

            return true;
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。(自)
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpDateFr(string[] arrJpDate)
        {
            this.lblDateGengoFr.Text = arrJpDate[0];
            this.txtDateYearFr.Text = arrJpDate[2];
            this.txtDateMonthFr.Text = arrJpDate[3];
            this.txtDateDayFr.Text = arrJpDate[4];
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。(至)
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpDateTo(string[] arrJpDate)
        {
            this.lblDateGengoTo.Text = arrJpDate[0];
            this.txtDateYearTo.Text = arrJpDate[2];
            this.txtDateMonthTo.Text = arrJpDate[3];
            this.txtDateDayTo.Text = arrJpDate[4];
        }

        /// <summary>
        /// 送信ファイルを作成して出力する
        /// </summary>
        private void OutputSendFiles(string type)
        {
            // 確認メッセージ「信用データをテキスト出力します。よろしいですか？」
            if (Msg.ConfOKCancel("送信データをテキスト出力します。よろしいですか？") == DialogResult.Cancel)
            {
                return;
            }

            // ディレクトリは、送信パステーブル（TB_HN_SOSHIN_PATH）を参照する
            string dirPath = this.getSendFilePath(this.txtAreaCd.Text);
            string chiku = "";

            if (ValChk.IsEmpty(dirPath))
            {
                Msg.Error("送信データの保存先を取得できませんでした。");
                return;
            }

            if (type == "old")
            {
                if (!System.IO.Directory.Exists(dirPath))
                {
                    Msg.Error("送信データの保存先フォルダが見つかりません。");
                    return;
                }
            }
            else if (type == "new")
            {
                // ディレクトリは、送信パステーブル（TB_HN_SOSHIN_PATH）を参照する
                chiku = this.getSendFilePathChiku(this.txtAreaCd.Text);
                dirPath = "";

                wc = new System.Net.WebClient();
                //ログインユーザー名とパスワードを指定
                wc.Credentials = new System.Net.NetworkCredential("jf-nago-jp", "c4EqXtWa");
            }

            // 日付項目を西暦に変換
            DateTime dateFr = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                this.txtDateMonthFr.Text, this.txtDateDayFr.Text, this.Dba);
            DateTime dateTo = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                this.txtDateMonthTo.Text, this.txtDateDayTo.Text, this.Dba);

            bool result = false;

            try
            {
                // 仕切りの送信ファイル出力
                result = putSikiriFile(this.txtAreaCd.Text, dateFr.ToString("yyyy/MM/dd"), dateTo.ToString("yyyy/MM/dd"), dirPath, type);
                if (!result)
                {
                    // 処理を中止
                    Msg.Error("仕切りファイル出力時にエラーが発生しました。処理を中止します。");
                    return;
                }
                else if (type == "new")
                {
                    //FTPサーバーにアップロード
                    wc.UploadFile("ftp://wx21.wadax.ne.jp/data/transfer/" + chiku + "/SIKIRI.txt", "SIKIRI.txt");

                    System.IO.File.Delete("SIKIRI.txt");
                }

                // 仕切り明細の送信ファイル出力
                result = putSikiriMeisaiFile(this.txtAreaCd.Text, dateFr.ToString("yyyy/MM/dd"), dateTo.ToString("yyyy/MM/dd"), dirPath, type);
                if (!result)
                {
                    // 処理を中止
                    Msg.Error("仕切り明細ファイル出力時にエラーが発生しました。処理を中止します。");
                    return;
                }
                else if (type == "new")
                {
                    //FTPサーバーにアップロード
                    wc.UploadFile("ftp://wx21.wadax.ne.jp/data/transfer/" + chiku + "/SIKIMEI.txt", "SIKIMEI.txt");

                    System.IO.File.Delete("SIKIMEI.txt");
                }

                // 控除の送信ファイル出力
                result = putKoujoFile(this.txtAreaCd.Text, dateFr.ToString("yyyy/MM/dd"), dateTo.ToString("yyyy/MM/dd"), dirPath, type);
                if (!result)
                {
                    // 処理を中止
                    Msg.Error("控除ファイル出力時にエラーが発生しました。処理を中止します。");
                    return;
                }
                else if (type == "new")
                {
                    //FTPサーバーにアップロード
                    wc.UploadFile("ftp://wx21.wadax.ne.jp/data/transfer/" + chiku + "/KOUJYO.txt", "KOUJYO.txt");

                    System.IO.File.Delete("KOUJYO.txt");
                }

                Msg.Info("データの書き込み処理を終了しました。");
            }
            catch (Exception e)
            {
                Msg.Notice(e.Message);
                return;
            }
        }

        /// <summary>
        /// 送信ファイルの保存先ディレクトリ取得
        /// </summary>
        /// <param name="chikuCd">地区コード</param>
        /// <returns>string 保存先のディレクトリ</returns>
        private string getSendFilePath(string chikuCd)
        {
            string dir = string.Empty;

            DbParamCollection dpc = new DbParamCollection();

            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);

            // 検索する地区コードをセット
            dpc.SetParam("@CHIKU_CD", SqlDbType.Decimal, 4, chikuCd);

            string cols = "PATH";

            string from = "TB_HN_SOSHIN_PATH AS A";

            string where = "A.KAISHA_CD = @KAISHA_CD AND";
            where += "  A.CHIKU_CD = @CHIKU_CD";

            DataTable dtPath;
            try
            {
                // 保存先取得
                dtPath = this.Dba.GetDataTableByConditionWithParams(cols, from, where, dpc);
            }
            catch (Exception e)
            {
                Msg.Notice(e.Message);
                return dir;
            }

            if (dtPath.Rows.Count == 0 || ValChk.IsEmpty(dtPath.Rows[0]["PATH"]))
            {
                //Msg.Error("保存先を取得できませんでした。");
            }
            else
            {
                dir = dtPath.Rows[0]["PATH"].ToString();
            }

            return dir;
        }

        /// <summary>
        /// 地区名取得
        /// </summary>
        /// <param name="chikuCd">地区コード</param>
        /// <returns>string 地区名</returns>
        private string getSendFilePathChiku(string chikuCd)
        {
            string dir = string.Empty;

            DbParamCollection dpc = new DbParamCollection();

            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);

            // 検索する地区コードをセット
            dpc.SetParam("@CHIKU_CD", SqlDbType.Decimal, 4, chikuCd);

            string cols = "CHIKU_NM";

            string from = "TM_HN_CHIKU_MST AS A";

            string where = "A.KAISHA_CD = @KAISHA_CD AND";
            where += "  A.CHIKU_CD = @CHIKU_CD";

            DataTable dtPath;
            try
            {
                // 地区名取得
                dtPath = this.Dba.GetDataTableByConditionWithParams(cols, from, where, dpc);
            }
            catch (Exception e)
            {
                Msg.Notice(e.Message);
                return dir;
            }

            if (dtPath.Rows.Count == 0 || ValChk.IsEmpty(dtPath.Rows[0]["CHIKU_NM"]))
            {
            }
            else
            {
                dir = dtPath.Rows[0]["CHIKU_NM"].ToString();
            }

            return dir;
        }

        /// <summary>
        /// 送信ファイル「仕切り」出力
        /// </summary>
        private bool putSikiriFile(string chikuCd, string fromDate, string toDate, string dirPath, string type)
        {
            bool result = false;
            DbParamCollection dpc = new DbParamCollection();

            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);

            // 検索する地区コードをセット
            dpc.SetParam("@CHIKU_CD", SqlDbType.Decimal, 4, chikuCd);

            // 検索するセリ日付範囲をセット
            dpc.SetParam("@DATE_FR", SqlDbType.DateTime, fromDate);
            dpc.SetParam("@DATE_TO", SqlDbType.DateTime, toDate);

            StringBuilder cols = new StringBuilder();
            cols.Append(" A.DENPYO_BANGO,");
            cols.Append(" A.SERIBI,");
            cols.Append(" A.SEISAN_NO,");
            cols.Append(" A.SENSHU_CD,");
            cols.Append(" A.GYOHO_CD,");
            cols.Append(" A.GYOGYO_TESURYO,");
            cols.Append(" A.MIZUAGE_GOKEI_SURYO,");
            cols.Append(" A.MIZUAGE_ZEINUKI_KINGAKU,");
            cols.Append(" A.MIZUAGE_SHOHIZEIGAKU,");
            cols.Append(" A.MIZUAGE_ZEIKOMI_KINGAKU,");
            cols.Append(" A.MIZUAGE_SHISHO_KUBUN,");
            cols.Append(" A.KEIJO_NENGAPPI, ");
            cols.Append(" B.CHIKU_CD, ");
            cols.Append(" A.HAKOSU ");

            StringBuilder from = new StringBuilder();
            from.Append(" TB_HN_SHIKIRI_DATA AS A ");
            from.Append(" LEFT OUTER JOIN TB_CM_TORIHIKISAKI AS B ");
            from.Append(" ON A.KAISHA_CD = B.KAISHA_CD ");
            from.Append(" AND A.SENSHU_CD = B.TORIHIKISAKI_CD ");

            StringBuilder where = new StringBuilder();
            where.Append(" A.KAISHA_CD = @KAISHA_CD AND ");
            where.Append(" A.SEISAN_KUBUN = 3 AND ");
            where.Append(" A.SERIBI BETWEEN @DATE_FR AND @DATE_TO AND ");
            where.Append(" B.CHIKU_CD = @CHIKU_CD ");

            string order = " A.SENSHU_CD ";

            DataTable dt;

            try
            {
                // データ取得
                dt = this.Dba.GetDataTableByConditionWithParams(cols.ToString(), from.ToString(), where.ToString(), order, dpc);
            }
            catch (Exception e)
            {
                Msg.Notice(e.Message);
                return result;
            }

            if (dt.Rows.Count == 0)
            {
                // 本当にエラーで処理終了にしていいのか？
                // 0バイトのファイルを出力するのはどうか？
                Msg.Error("該当データがありません。");
                return result;
            }

            // --- これ以降は、テキストファイル出力処理 ---
            //CSVファイルに書き込むときに使うEncoding
            System.Text.Encoding enc =
                System.Text.Encoding.GetEncoding(CHR_CD_SJIS);

            // ファイルのストリーム宣言
            System.IO.StreamWriter sr;
            try
            {
                // ※他のソースから SIKIRI.TXT をアクセスする場合は、config.xml に SIKIRI.TXT を設定しておくといいらしい
                sr = new System.IO.StreamWriter(dirPath + "SIKIRI.TXT", false, enc);
            }
            catch (Exception e)
            {
                Msg.Notice(e.Message);
                return result;
            }

            // ループ変数
            int i = 0;
            decimal hakoKingaku;

            try
            {
                //レコードを書き込む
                foreach (DataRow row in dt.Rows)
                {
                    sr.Write(Util.ToString(dt.Rows[i]["DENPYO_BANGO"]) + ',');
                    sr.Write(Util.ToDate(dt.Rows[i]["SERIBI"]).ToString("yyyy/MM/dd") + ',');
                    sr.Write(Util.ToString(dt.Rows[i]["SEISAN_NO"]) + ',');
                    sr.Write(Util.ToString(dt.Rows[i]["SENSHU_CD"]) + ',');
                    sr.Write("1,");
                    sr.Write(Util.ToString(dt.Rows[i]["GYOHO_CD"]) + ',');
                    sr.Write(Util.ToString(Util.Round(Util.ToDecimal(dt.Rows[i]["GYOGYO_TESURYO"]), 0)) + ',');
                    sr.Write(",");
                    sr.Write("0,");
                    sr.Write(Util.ToString((double)(Util.Round(Util.ToDecimal(dt.Rows[i]["MIZUAGE_GOKEI_SURYO"]), 1))) + ',');
                    //sr.Write(Util.ToString(Util.Round(Util.ToDecimal(dt.Rows[i]["MIZUAGE_GOKEI_SURYO"]), 1)) + ',');
                    sr.Write(Util.ToString(dt.Rows[i]["MIZUAGE_ZEINUKI_KINGAKU"]) + ',');
                    sr.Write(Util.ToString(dt.Rows[i]["MIZUAGE_SHOHIZEIGAKU"]) + ',');
                    sr.Write(Util.ToString(dt.Rows[i]["MIZUAGE_ZEIKOMI_KINGAKU"]) + ',');
                    sr.Write("0,");
                    sr.Write(Util.ToString(dt.Rows[i]["MIZUAGE_SHISHO_KUBUN"]) + ',');
                    sr.Write("0,");
                    sr.Write("0,");
                    sr.Write(Util.ToDate(dt.Rows[i]["KEIJO_NENGAPPI"]).ToString("yyyy/MM/dd") + ',');
                    switch (Util.ToString(dt.Rows[i]["CHIKU_CD"]))
                    {
                        case "2":
                        case "5":
                        case "6":
                            sr.Write("0,");
                            hakoKingaku = (Util.ToDecimal(dt.Rows[i]["HAKOSU"]) * 60);
                            sr.Write(Util.ToString(hakoKingaku));
                            break;
                        default:
                            sr.Write("0");
                            break;
                    }

                    //改行する
                    sr.Write("\r\n");
                    i++;
                }

                result = true;
            }
            catch (Exception e)
            {
                Msg.Notice(e.Message);
            }
            finally
            {
                // ファイルのストリームを閉じる
                sr.Close();
            }

            return result;
        }

        /// <summary>
        /// 送信ファイル「仕切り明細」出力
        /// </summary>
        private bool putSikiriMeisaiFile(string chikuCd, string fromDate, string toDate, string dirPath, string type)
        {
            bool result = false;
            DbParamCollection dpc = new DbParamCollection();

            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);

            // 検索する地区コードをセット
            dpc.SetParam("@CHIKU_CD", SqlDbType.Decimal, 4, chikuCd);

            // 検索するセリ日付範囲をセット
            dpc.SetParam("@DATE_FR", SqlDbType.DateTime, fromDate);
            dpc.SetParam("@DATE_TO", SqlDbType.DateTime, toDate);

            StringBuilder cols = new StringBuilder();
            cols.Append(" A.DENPYO_BANGO,");
            cols.Append(" A.GYO_NO,");
            cols.Append(" A.SERIBI,");
            cols.Append(" A.YAMA_NO,");
            cols.Append(" A.GYOSHU_CD,");
            cols.Append(" A.SURYO,");
            cols.Append(" A.TANKA,");
            cols.Append(" A.KINGAKU,");
            cols.Append(" A.NAKAGAININ_CD,");
            cols.Append(" A.PAYAO_NO ");

            StringBuilder from = new StringBuilder();
            from.Append(" VI_HN_SHIKIRI_MEISAI AS A ");
            from.Append(" LEFT OUTER JOIN TB_CM_TORIHIKISAKI AS B ");
            from.Append(" ON A.KAISHA_CD = B.KAISHA_CD ");
            from.Append(" AND A.SENSHU_CD = B.TORIHIKISAKI_CD ");

            StringBuilder where = new StringBuilder();
            where.Append(" A.KAISHA_CD = @KAISHA_CD AND ");
            where.Append(" A.SERIBI BETWEEN @DATE_FR AND @DATE_TO AND ");
            where.Append(" A.SEISAN_KUBUN = 3 AND ");
            where.Append(" B.CHIKU_CD = @CHIKU_CD ");

            string order = " A.SENSHU_CD ";

            DataTable dt;

            try
            {
                dt = this.Dba.GetDataTableByConditionWithParams(cols.ToString(), from.ToString(), where.ToString(), order, dpc);
            }
            catch (Exception e)
            {
                Msg.Notice(e.Message);
                return result;
            }

            if (dt.Rows.Count == 0)
            {
                // 本当にエラーで処理終了にしていいのか？
                // 0バイトのファイルを出力するのはどうか？
                Msg.Error("該当データがありません。");
                return result;
            }

            // --- これ以降は、テキストファイル出力処理 ---
            //CSVファイルに書き込むときに使うEncoding
            System.Text.Encoding enc =
                System.Text.Encoding.GetEncoding(CHR_CD_SJIS);

            // ファイルのストリーム宣言
            System.IO.StreamWriter sr;
            try
            {
                // ※他のソースから SIKIMEI.TXT をアクセスする場合は、config.xml に SIKIMEI.TXT を設定しておくといいらしい
                sr = new System.IO.StreamWriter(dirPath + "SIKIMEI.TXT", false, enc);
            }
            catch (Exception e)
            {
                Msg.Notice(e.Message);
                return result;
            }

            // ループ変数
            int i = 0;

            try
            {
                //レコードを書き込む
                foreach (DataRow row in dt.Rows)
                {
                    sr.Write(Util.ToString(dt.Rows[i]["DENPYO_BANGO"]) + ',');
                    sr.Write(Util.ToString(dt.Rows[i]["GYO_NO"]) + ',');
                    sr.Write(Util.ToDate(dt.Rows[i]["SERIBI"]).ToString("yyyy/MM/dd") + ',');
                    sr.Write(Util.ToString(dt.Rows[i]["YAMA_NO"]) + ',');
                    sr.Write(Util.ToString(dt.Rows[i]["GYOSHU_CD"]) + ',');
                    sr.Write(Util.ToString(Util.ToDecimal(dt.Rows[i]["SURYO"]).ToString("0.#")) + ','); // 小数点が無ければ、小数点第１位（0）を出力抑止する
                    sr.Write(Util.ToString(dt.Rows[i]["TANKA"]) + ',');
                    sr.Write(Util.ToString(dt.Rows[i]["KINGAKU"]) + ',');
                    sr.Write(Util.ToString(dt.Rows[i]["NAKAGAININ_CD"]) + ',');
                    sr.Write(Util.ToString(dt.Rows[i]["PAYAO_NO"]));

                    //改行する
                    sr.Write("\r\n");
                    i++;
                }

                result = true;
            }
            catch (Exception e)
            {
                Msg.Notice(e.Message);
            }
            finally
            {
                // ファイルのストリームを閉じる
                sr.Close();
            }

            return result;
        }

        /// <summary>
        /// 送信ファイル「控除」出力
        /// </summary>
        private bool putKoujoFile(string chikuCd, string fromDate, string toDate, string dirPath, string type)
        {
            bool result = false;
            DbParamCollection dpc = new DbParamCollection();

            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);

            // 検索する地区コードをセット
            dpc.SetParam("@CHIKU_CD", SqlDbType.Decimal, 4, chikuCd);

            // 検索するセリ日付範囲をセット
            dpc.SetParam("@DATE_FR", SqlDbType.DateTime, fromDate);
            dpc.SetParam("@DATE_TO", SqlDbType.DateTime, toDate);

            StringBuilder cols = new StringBuilder();
            cols.Append(" A.SEISAN_BANGO,");
            cols.Append(" A.SEISAN_BI,");
            cols.Append(" A.SENSHU_CD,");
            cols.Append(" A.KAISHA_CD,");
            cols.Append(" A.ZEIKOMI_MIZUAGE_KINGAKU,");
            cols.Append(" A.KOJO_KOMOKU1,");
            cols.Append(" A.KOJO_KOMOKU2,");
            cols.Append(" A.KOJO_KOMOKU3,");
            cols.Append(" A.KOJO_GOKEIGAKU,");
            cols.Append(" A.SASHIHIKI_SEISANGAKU ");

            StringBuilder from = new StringBuilder();
            from.Append(" TB_HN_KOJO_DATA AS A ");
            from.Append(" LEFT OUTER JOIN TB_CM_TORIHIKISAKI AS B ");
            from.Append(" ON A.KAISHA_CD = B.KAISHA_CD ");
            from.Append(" AND A.SENSHU_CD = B.TORIHIKISAKI_CD ");

            StringBuilder where = new StringBuilder();
            where.Append(" A.KAISHA_CD = @KAISHA_CD AND ");
            where.Append(" A.SEISAN_BI BETWEEN @DATE_FR AND @DATE_TO AND ");
            where.Append(" A.SEISAN_KUBUN = 3 AND ");
            where.Append(" B.CHIKU_CD = @CHIKU_CD ");

            string order = " A.SENSHU_CD ";

            DataTable dt;

            try
            {
                dt = this.Dba.GetDataTableByConditionWithParams(cols.ToString(), from.ToString(), where.ToString(), order, dpc);
            }
            catch (Exception e)
            {
                Msg.Notice(e.Message);
                return result;
            }

            if (dt.Rows.Count == 0)
            {
                // 本当にエラーで処理終了にしていいのか？
                // 0バイトのファイルを出力するのはどうか？
                Msg.Error("該当データがありません。");
                return result;
            }

            // --- これ以降は、テキストファイル出力処理 ---
            //CSVファイルに書き込むときに使うEncoding
            System.Text.Encoding enc =
                System.Text.Encoding.GetEncoding(CHR_CD_SJIS);

            // ファイルのストリーム宣言
            System.IO.StreamWriter sr;
            try
            {
                // ※他のソースから KOUJYO.TXT をアクセスする場合は、config.xml に KOUJYO.TXT を設定しておくといいらしい
                sr = new System.IO.StreamWriter(dirPath + "KOUJYO.TXT", false, enc);
            }
            catch (Exception e)
            {
                Msg.Notice(e.Message);
                return result;
            }

            // ループ変数
            int i = 0;

            try
            {
                //レコードを書き込む
                foreach (DataRow row in dt.Rows)
                {
                    sr.Write(Util.ToString(dt.Rows[i]["SEISAN_BANGO"]) + ',');
                    sr.Write(Util.ToDate(dt.Rows[i]["SEISAN_BI"]).ToString("yyyy/MM/dd") + ',');
                    sr.Write(Util.ToString(dt.Rows[i]["SENSHU_CD"]) + ',');
                    sr.Write('"' + Util.ToString(dt.Rows[i]["KAISHA_CD"]) + "\",");
                    sr.Write("0,0,"); // 2カラム連続して0を出力する
                    sr.Write(Util.ToString(dt.Rows[i]["ZEIKOMI_MIZUAGE_KINGAKU"]) + ',');
                    sr.Write(Util.ToString(dt.Rows[i]["KOJO_KOMOKU1"]) + ',');
                    sr.Write("0,0,0,0,0,"); // 5カラム連続して0を出力する
                    sr.Write(Util.ToString(dt.Rows[i]["KOJO_KOMOKU2"]) + ',');
                    sr.Write("0,0,0,"); // 3カラム連続して0を出力する
                    sr.Write(Util.ToString(dt.Rows[i]["KOJO_KOMOKU3"]) + ',');
                    sr.Write("0,0,0,0,0,0,0,0,0,0,0,0,"); // 12カラム連続して0を出力する
                    sr.Write(Util.ToString(dt.Rows[i]["KOJO_GOKEIGAKU"]) + ',');
                    sr.Write("0,");
                    sr.Write(Util.ToString(dt.Rows[i]["SASHIHIKI_SEISANGAKU"]) + ',');
                    sr.Write("0,0,0,0,0,0,0,0,0,0,"); // 10カラム連続して0を出力する
                    sr.Write("0");　// 最終カラム

                    //改行する
                    sr.Write("\r\n");
                    i++;
                }

                result = true;
            }
            catch (Exception e)
            {
                Msg.Notice(e.Message);
            }
            finally
            {
                // ファイルのストリームを閉じる
                sr.Close();
            }

            return result;
        }

        /// <summary>
        /// 地区CD入力欄でEnterキーが押下されたことを記録する
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtAreaCd_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.enterKeyIsPressedInTxtAreaCd = true;
            }
            else
            {
                this.enterKeyIsPressedInTxtAreaCd = false;
            }
        }
        #endregion
    }
}
