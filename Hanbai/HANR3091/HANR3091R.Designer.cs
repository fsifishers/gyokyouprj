﻿namespace jp.co.fsi.han.hanr3091
{
    /// <summary>
    /// HANR3091R の概要の説明です。
    /// </summary>
    partial class HANR3091R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(HANR3091R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.reportInfo1 = new GrapeCity.ActiveReports.SectionReportModel.ReportInfo();
            this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtPageCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblTitle06 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtTitleName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtKaishaNm = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDateHani = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblCd = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblNakagaininNm = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblHasseiSuryo = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSengetuZandaka = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTougetuKarikata = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTougetuKashikata = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblZandaka = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblShouhizei = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.textBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.reportHeader1 = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.reportFooter1 = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtTotal03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotal02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotal01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotal04 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotal05 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotal06 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.reportInfo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle06)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKaishaNm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateHani)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNakagaininNm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHasseiSuryo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSengetuZandaka)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTougetuKarikata)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTougetuKashikata)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblZandaka)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblShouhizei)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal04)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal05)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal06)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.reportInfo1,
            this.lblPage,
            this.txtPageCount,
            this.lblTitle06,
            this.txtTitleName,
            this.txtKaishaNm,
            this.txtDateHani,
            this.lblCd,
            this.lblNakagaininNm,
            this.lblHasseiSuryo,
            this.lblSengetuZandaka,
            this.lblTougetuKarikata,
            this.lblTougetuKashikata,
            this.lblZandaka,
            this.lblShouhizei,
            this.line1,
            this.line2});
            this.pageHeader.Height = 1.25F;
            this.pageHeader.Name = "pageHeader";
            // 
            // reportInfo1
            // 
            this.reportInfo1.FormatString = "{RunDateTime:yyyy/MM/dd}";
            this.reportInfo1.Height = 0.1574803F;
            this.reportInfo1.Left = 7.367323F;
            this.reportInfo1.Name = "reportInfo1";
            this.reportInfo1.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; ddo-char-set: 1";
            this.reportInfo1.Top = 0.4448819F;
            this.reportInfo1.Width = 0.75F;
            // 
            // lblPage
            // 
            this.lblPage.Height = 0.1574803F;
            this.lblPage.HyperLink = null;
            this.lblPage.Left = 8.433065F;
            this.lblPage.Name = "lblPage";
            this.lblPage.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle; ddo-char-set: 1";
            this.lblPage.Text = "頁";
            this.lblPage.Top = 0.4448819F;
            this.lblPage.Width = 0.1590552F;
            // 
            // txtPageCount
            // 
            this.txtPageCount.Height = 0.1574803F;
            this.txtPageCount.Left = 8.117323F;
            this.txtPageCount.MultiLine = false;
            this.txtPageCount.Name = "txtPageCount";
            this.txtPageCount.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: middle; ddo-char-set: 1";
            this.txtPageCount.SummaryFunc = GrapeCity.ActiveReports.SectionReportModel.SummaryFunc.Count;
            this.txtPageCount.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtPageCount.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.txtPageCount.Text = "999";
            this.txtPageCount.Top = 0.4448819F;
            this.txtPageCount.Width = 0.2952756F;
            // 
            // lblTitle06
            // 
            this.lblTitle06.Height = 0.2F;
            this.lblTitle06.HyperLink = null;
            this.lblTitle06.Left = 7.690153F;
            this.lblTitle06.Name = "lblTitle06";
            this.lblTitle06.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: center; d" +
    "do-char-set: 1";
            this.lblTitle06.Text = "【税込み】";
            this.lblTitle06.Top = 0.6448819F;
            this.lblTitle06.Width = 0.7224407F;
            // 
            // txtTitleName
            // 
            this.txtTitleName.Height = 0.2874016F;
            this.txtTitleName.Left = 3.110236F;
            this.txtTitleName.MultiLine = false;
            this.txtTitleName.Name = "txtTitleName";
            this.txtTitleName.Style = "font-family: ＭＳ 明朝; font-size: 15.75pt; font-weight: bold; text-align: center; te" +
    "xt-decoration: none; vertical-align: middle";
            this.txtTitleName.Text = "販売未収金一覧表";
            this.txtTitleName.Top = 0F;
            this.txtTitleName.Width = 2.833465F;
            // 
            // txtKaishaNm
            // 
            this.txtKaishaNm.DataField = "ITEM01";
            this.txtKaishaNm.Height = 0.2F;
            this.txtKaishaNm.Left = 0.08267717F;
            this.txtKaishaNm.MultiLine = false;
            this.txtKaishaNm.Name = "txtKaishaNm";
            this.txtKaishaNm.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; ddo-char-set: 1";
            this.txtKaishaNm.Text = "txtKaishaNm";
            this.txtKaishaNm.Top = 0.4023622F;
            this.txtKaishaNm.Width = 2.819292F;
            // 
            // txtDateHani
            // 
            this.txtDateHani.DataField = "ITEM02";
            this.txtDateHani.Height = 0.2F;
            this.txtDateHani.Left = 3.110236F;
            this.txtDateHani.Name = "txtDateHani";
            this.txtDateHani.Style = "font-family: ＭＳ 明朝; font-size: 12.75pt; font-weight: normal; text-align: center; " +
    "ddo-char-set: 1";
            this.txtDateHani.Text = "txtDateHani";
            this.txtDateHani.Top = 0.3582678F;
            this.txtDateHani.Width = 2.833465F;
            // 
            // lblCd
            // 
            this.lblCd.Height = 0.2F;
            this.lblCd.HyperLink = null;
            this.lblCd.Left = 0.08267745F;
            this.lblCd.Name = "lblCd";
            this.lblCd.Style = "font-family: ＭＳ 明朝; font-size: 10pt; font-weight: bold; text-align: right; vertic" +
    "al-align: middle; ddo-char-set: 1";
            this.lblCd.Text = "コード";
            this.lblCd.Top = 1.007087F;
            this.lblCd.Width = 0.4740158F;
            // 
            // lblNakagaininNm
            // 
            this.lblNakagaininNm.Height = 0.2F;
            this.lblNakagaininNm.HyperLink = null;
            this.lblNakagaininNm.Left = 0.708662F;
            this.lblNakagaininNm.Name = "lblNakagaininNm";
            this.lblNakagaininNm.Style = "font-family: ＭＳ 明朝; font-size: 10pt; font-weight: bold; text-align: left; vertica" +
    "l-align: middle; ddo-char-set: 1";
            this.lblNakagaininNm.Text = "仲 買 人 氏 名";
            this.lblNakagaininNm.Top = 1.007087F;
            this.lblNakagaininNm.Width = 1.590551F;
            // 
            // lblHasseiSuryo
            // 
            this.lblHasseiSuryo.Height = 0.2F;
            this.lblHasseiSuryo.HyperLink = null;
            this.lblHasseiSuryo.Left = 2.416142F;
            this.lblHasseiSuryo.Name = "lblHasseiSuryo";
            this.lblHasseiSuryo.Style = "font-family: ＭＳ 明朝; font-size: 10pt; font-weight: bold; text-align: right; vertic" +
    "al-align: middle; ddo-char-set: 1";
            this.lblHasseiSuryo.Text = "発 生 数 量";
            this.lblHasseiSuryo.Top = 1.007087F;
            this.lblHasseiSuryo.Width = 0.8972442F;
            // 
            // lblSengetuZandaka
            // 
            this.lblSengetuZandaka.Height = 0.2F;
            this.lblSengetuZandaka.HyperLink = null;
            this.lblSengetuZandaka.Left = 3.490158F;
            this.lblSengetuZandaka.Name = "lblSengetuZandaka";
            this.lblSengetuZandaka.Style = "font-family: ＭＳ 明朝; font-size: 10pt; font-weight: bold; text-align: right; vertic" +
    "al-align: middle; ddo-char-set: 1";
            this.lblSengetuZandaka.Text = "前 月 残 高";
            this.lblSengetuZandaka.Top = 1.007087F;
            this.lblSengetuZandaka.Width = 0.9188983F;
            // 
            // lblTougetuKarikata
            // 
            this.lblTougetuKarikata.Height = 0.2F;
            this.lblTougetuKarikata.HyperLink = null;
            this.lblTougetuKarikata.Left = 4.584252F;
            this.lblTougetuKarikata.Name = "lblTougetuKarikata";
            this.lblTougetuKarikata.Style = "font-family: ＭＳ 明朝; font-size: 10pt; font-weight: bold; text-align: right; vertic" +
    "al-align: middle; ddo-char-set: 1";
            this.lblTougetuKarikata.Text = "当月借方発生";
            this.lblTougetuKarikata.Top = 1.007087F;
            this.lblTougetuKarikata.Width = 1.062598F;
            // 
            // lblTougetuKashikata
            // 
            this.lblTougetuKashikata.Height = 0.2F;
            this.lblTougetuKashikata.HyperLink = null;
            this.lblTougetuKashikata.Left = 5.848032F;
            this.lblTougetuKashikata.Name = "lblTougetuKashikata";
            this.lblTougetuKashikata.Style = "font-family: ＭＳ 明朝; font-size: 10pt; font-weight: bold; text-align: right; vertic" +
    "al-align: middle; ddo-char-set: 1";
            this.lblTougetuKashikata.Text = "当月貸方発生";
            this.lblTougetuKashikata.Top = 1.007087F;
            this.lblTougetuKashikata.Width = 1.062599F;
            // 
            // lblZandaka
            // 
            this.lblZandaka.Height = 0.2F;
            this.lblZandaka.HyperLink = null;
            this.lblZandaka.Left = 7.006299F;
            this.lblZandaka.Name = "lblZandaka";
            this.lblZandaka.Style = "font-family: ＭＳ 明朝; font-size: 10pt; font-weight: bold; text-align: right; vertic" +
    "al-align: middle; ddo-char-set: 1";
            this.lblZandaka.Text = "繰 越 残 高";
            this.lblZandaka.Top = 1.007087F;
            this.lblZandaka.Width = 0.9507885F;
            // 
            // lblShouhizei
            // 
            this.lblShouhizei.Height = 0.2F;
            this.lblShouhizei.HyperLink = null;
            this.lblShouhizei.Left = 8.040552F;
            this.lblShouhizei.Name = "lblShouhizei";
            this.lblShouhizei.Style = "font-family: ＭＳ 明朝; font-size: 10pt; font-weight: bold; text-align: right; vertic" +
    "al-align: middle; ddo-char-set: 1";
            this.lblShouhizei.Text = "消 費 税 額";
            this.lblShouhizei.Top = 1.007087F;
            this.lblShouhizei.Width = 0.9405546F;
            // 
            // line1
            // 
            this.line1.Height = 0F;
            this.line1.Left = 3.558268F;
            this.line1.LineWeight = 1F;
            this.line1.Name = "line1";
            this.line1.Top = 0.2874016F;
            this.line1.Width = 1.916535F;
            this.line1.X1 = 3.558268F;
            this.line1.X2 = 5.474803F;
            this.line1.Y1 = 0.2874016F;
            this.line1.Y2 = 0.2874016F;
            // 
            // line2
            // 
            this.line2.Height = 0F;
            this.line2.Left = 2.384186E-07F;
            this.line2.LineWeight = 1F;
            this.line2.Name = "line2";
            this.line2.Top = 1.207088F;
            this.line2.Width = 9.069292F;
            this.line2.X1 = 2.384186E-07F;
            this.line2.X2 = 9.069292F;
            this.line2.Y1 = 1.207088F;
            this.line2.Y2 = 1.207088F;
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.textBox1,
            this.textBox6,
            this.textBox9,
            this.textBox2,
            this.textBox7,
            this.textBox10,
            this.textBox11,
            this.textBox12});
            this.detail.Height = 0.2188976F;
            this.detail.Name = "detail";
            // 
            // textBox1
            // 
            this.textBox1.DataField = "ITEM03";
            this.textBox1.Height = 0.2F;
            this.textBox1.Left = 0.08267689F;
            this.textBox1.Name = "textBox1";
            this.textBox1.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; dd" +
    "o-char-set: 1";
            this.textBox1.Text = "textBox1";
            this.textBox1.Top = 0F;
            this.textBox1.Width = 0.4740158F;
            // 
            // textBox6
            // 
            this.textBox6.DataField = "ITEM04";
            this.textBox6.Height = 0.2F;
            this.textBox6.Left = 0.7086616F;
            this.textBox6.MultiLine = false;
            this.textBox6.Name = "textBox6";
            this.textBox6.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; ddo-char-set: 1";
            this.textBox6.Text = "textBox6";
            this.textBox6.Top = 0F;
            this.textBox6.Width = 1.590551F;
            // 
            // textBox9
            // 
            this.textBox9.DataField = "ITEM07";
            this.textBox9.Height = 0.2F;
            this.textBox9.Left = 4.584252F;
            this.textBox9.Name = "textBox9";
            this.textBox9.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox9.Text = "textBox9";
            this.textBox9.Top = 0F;
            this.textBox9.Width = 1.062598F;
            // 
            // textBox2
            // 
            this.textBox2.DataField = "ITEM06";
            this.textBox2.Height = 0.2F;
            this.textBox2.Left = 3.490158F;
            this.textBox2.Name = "textBox2";
            this.textBox2.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox2.Text = "textBox2";
            this.textBox2.Top = 0F;
            this.textBox2.Width = 0.9188979F;
            // 
            // textBox7
            // 
            this.textBox7.DataField = "ITEM05";
            this.textBox7.Height = 0.2F;
            this.textBox7.Left = 2.364173F;
            this.textBox7.Name = "textBox7";
            this.textBox7.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox7.Text = "textBox7";
            this.textBox7.Top = 0F;
            this.textBox7.Width = 0.9492133F;
            // 
            // textBox10
            // 
            this.textBox10.DataField = "ITEM08";
            this.textBox10.Height = 0.2F;
            this.textBox10.Left = 5.848032F;
            this.textBox10.Name = "textBox10";
            this.textBox10.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox10.Text = "textBox10";
            this.textBox10.Top = 0F;
            this.textBox10.Width = 1.0626F;
            // 
            // textBox11
            // 
            this.textBox11.DataField = "ITEM09";
            this.textBox11.Height = 0.2F;
            this.textBox11.Left = 7.006299F;
            this.textBox11.Name = "textBox11";
            this.textBox11.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox11.Text = "textBox11";
            this.textBox11.Top = 0F;
            this.textBox11.Width = 0.950789F;
            // 
            // textBox12
            // 
            this.textBox12.DataField = "ITEM10";
            this.textBox12.Height = 0.2F;
            this.textBox12.Left = 8.040552F;
            this.textBox12.Name = "textBox12";
            this.textBox12.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox12.Text = "textBox12";
            this.textBox12.Top = 0F;
            this.textBox12.Width = 0.9405508F;
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            // 
            // reportHeader1
            // 
            this.reportHeader1.Height = 0F;
            this.reportHeader1.Name = "reportHeader1";
            // 
            // reportFooter1
            // 
            this.reportFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.label1,
            this.txtTotal03,
            this.txtTotal02,
            this.txtTotal01,
            this.txtTotal04,
            this.txtTotal05,
            this.txtTotal06});
            this.reportFooter1.Height = 0.3020834F;
            this.reportFooter1.Name = "reportFooter1";
            this.reportFooter1.Format += new System.EventHandler(this.reportFooter1_Format);
            // 
            // label1
            // 
            this.label1.Height = 0.2F;
            this.label1.HyperLink = null;
            this.label1.Left = 0.7086615F;
            this.label1.Name = "label1";
            this.label1.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; ddo-char-set: 1";
            this.label1.Text = "【合    計】";
            this.label1.Top = 0.1019685F;
            this.label1.Width = 1.009055F;
            // 
            // txtTotal03
            // 
            this.txtTotal03.DataField = "ITEM07";
            this.txtTotal03.Height = 0.2F;
            this.txtTotal03.Left = 4.459055F;
            this.txtTotal03.Name = "txtTotal03";
            this.txtTotal03.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtTotal03.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtTotal03.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtTotal03.Text = "txtTotal03";
            this.txtTotal03.Top = 0.1019685F;
            this.txtTotal03.Width = 1.187794F;
            // 
            // txtTotal02
            // 
            this.txtTotal02.DataField = "ITEM06";
            this.txtTotal02.Height = 0.2F;
            this.txtTotal02.Left = 3.354725F;
            this.txtTotal02.Name = "txtTotal02";
            this.txtTotal02.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtTotal02.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtTotal02.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtTotal02.Text = "txtTotal02";
            this.txtTotal02.Top = 0.1019685F;
            this.txtTotal02.Width = 1.054332F;
            // 
            // txtTotal01
            // 
            this.txtTotal01.DataField = "ITEM05";
            this.txtTotal01.Height = 0.2F;
            this.txtTotal01.Left = 2.332677F;
            this.txtTotal01.Name = "txtTotal01";
            this.txtTotal01.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtTotal01.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtTotal01.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtTotal01.Text = "txtTotal01";
            this.txtTotal01.Top = 0.1019685F;
            this.txtTotal01.Width = 0.9807093F;
            // 
            // txtTotal04
            // 
            this.txtTotal04.DataField = "ITEM08";
            this.txtTotal04.Height = 0.2F;
            this.txtTotal04.Left = 5.691733F;
            this.txtTotal04.Name = "txtTotal04";
            this.txtTotal04.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtTotal04.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtTotal04.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtTotal04.Text = "txtTotal04";
            this.txtTotal04.Top = 0.1019685F;
            this.txtTotal04.Width = 1.218899F;
            // 
            // txtTotal05
            // 
            this.txtTotal05.DataField = "ITEM09";
            this.txtTotal05.Height = 0.2F;
            this.txtTotal05.Left = 6.943701F;
            this.txtTotal05.Name = "txtTotal05";
            this.txtTotal05.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtTotal05.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtTotal05.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtTotal05.Text = "txtTotal05";
            this.txtTotal05.Top = 0.1019685F;
            this.txtTotal05.Width = 1.013387F;
            // 
            // txtTotal06
            // 
            this.txtTotal06.DataField = "ITEM10";
            this.txtTotal06.Height = 0.2F;
            this.txtTotal06.Left = 7.998819F;
            this.txtTotal06.Name = "txtTotal06";
            this.txtTotal06.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtTotal06.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtTotal06.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtTotal06.Text = "txtTotal06";
            this.txtTotal06.Top = 0.1019685F;
            this.txtTotal06.Width = 0.9822836F;
            // 
            // HANR3091R
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.7874016F;
            this.PageSettings.Margins.Left = 0.5314961F;
            this.PageSettings.Margins.Right = 0.5314961F;
            this.PageSettings.Margins.Top = 0.5905512F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Portrait;
            this.PageSettings.PaperHeight = 13.89764F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.B4;
            this.PageSettings.PaperWidth = 9.84252F;
            this.PrintWidth = 9.055119F;
            this.Sections.Add(this.reportHeader1);
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.pageFooter);
            this.Sections.Add(this.reportFooter1);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.reportInfo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle06)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKaishaNm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateHani)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNakagaininNm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHasseiSuryo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSengetuZandaka)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTougetuKarikata)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTougetuKashikata)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblZandaka)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblShouhizei)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal04)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal05)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal06)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.ReportInfo reportInfo1;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPageCount;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle06;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitleName;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKaishaNm;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDateHani;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblCd;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblNakagaininNm;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblHasseiSuryo;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSengetuZandaka;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTougetuKarikata;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTougetuKashikata;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblZandaka;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblShouhizei;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
        private GrapeCity.ActiveReports.SectionReportModel.Line line2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox12;
        private GrapeCity.ActiveReports.SectionReportModel.ReportHeader reportHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.ReportFooter reportFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal03;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal02;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal04;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal05;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal06;
    }
}
