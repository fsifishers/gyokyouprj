﻿using System;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Text;
using System.Reflection;
using System.Windows.Forms;

using GrapeCity.ActiveReports;

using jp.co.fsi.common.access;
using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.han.hanr3091
{
    /// <summary>
    /// 販売未収金一覧表(HANR3091)
    /// </summary>
    public partial class HANR3091 : BasePgForm
    {
        #region プロパティ
        /// <summary>
        /// 画面上最後となるフォーカスのEnterボタン押下時処理用変数
        /// </summary>
        private bool _dtFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._dtFlg;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HANR3091()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region メソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 水揚支所
            this.txtMizuageShishoCd.Text = "1";
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_HN_COMBO_DATA_MIZUAGE", this.txtMizuageShishoCd.Text);

            // 現在の年号を取得する
            string[] jpDate = Util.ConvJpDate(DateTime.Now, this.Dba);
            //今月の最初の日を取得する
            DateTime dtMonthFr = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            string[] jpMontFr = Util.ConvJpDate(dtMonthFr, this.Dba);
            // 日付範囲前
            this.lblDateGengoFr.Text = Util.ToString(jpMontFr[0]);
            this.txtDateYearFr.Text = jpMontFr[2];
            this.txtDateMonthFr.Text = jpMontFr[3];
            this.txtDateDayFr.Text = jpMontFr[4];
            // 日付範囲後
            lblDateGengoTo.Text = jpDate[0];
            txtDateYearTo.Text = jpDate[2];
            txtDateMonthTo.Text = jpDate[3];
            txtDateDayTo.Text = jpDate[4];

            // 初期フォーカス
            txtDateYearFr.Focus();

            // Enter処理を無効化
            this._dtFlg = false;
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 水揚支所,日付(年),仲買人コードにフォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd":
                case "txtDateYearFr":
                case "txtDateYearTo":
                case "txtNakagaininCdFr":
                case "txtNakagaininCdTo":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;
            String[] result;

            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd": // 水揚支所
                    result = this.openSearchWindow("COMC8011", "TB_HN_COMBO_DATA_MIZUAGE", this.txtMizuageShishoCd.Text);
                    if (!ValChk.IsEmpty(result[0]))
                    {
                        this.txtMizuageShishoCd.Text = result[0];
                        this.lblMizuageShishoNm.Text = result[1];
                    }
                    break;

                case "txtDateYearFr":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom("COMC9011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.com.comc9011.COMC9011");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblDateGengoFr.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.lblDateGengoFr.Text = result[1];

                                // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
                                DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                                    this.txtDateMonthFr.Text, "1", this.Dba);
                                int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);
                                if (Util.ToInt(this.txtDateDayFr.Text) > lastDayInMonth)
                                {
                                    this.txtDateDayFr.Text = Util.ToString(lastDayInMonth);
                                }

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblDateGengoFr.Text,
                                        this.txtDateYearFr.Text,
                                        this.txtDateMonthFr.Text,
                                        this.txtDateDayFr.Text,
                                        this.Dba);
                                this.lblDateGengoFr.Text = arrJpDate[0];
                                this.txtDateYearFr.Text = arrJpDate[2];
                                this.txtDateMonthFr.Text = arrJpDate[3];
                                this.txtDateDayFr.Text = arrJpDate[4];
                            }
                        }
                    }
                    break;

                case "txtDateYearTo":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom("COMC9011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.com.comc9011.COMC9011");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblDateGengoTo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.lblDateGengoTo.Text = result[1];

                                // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
                                DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                                    this.txtDateMonthTo.Text, "1", this.Dba);
                                int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);
                                if (Util.ToInt(this.txtDateDayTo.Text) > lastDayInMonth)
                                {
                                    this.txtDateDayTo.Text = Util.ToString(lastDayInMonth);
                                }

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblDateGengoTo.Text,
                                        this.txtDateYearTo.Text,
                                        this.txtDateMonthTo.Text,
                                        this.txtDateDayTo.Text,
                                        this.Dba);
                                this.lblDateGengoTo.Text = arrJpDate[0];
                                this.txtDateYearTo.Text = arrJpDate[2];
                                this.txtDateMonthTo.Text = arrJpDate[3];
                                this.txtDateDayTo.Text = arrJpDate[4];
                            }
                        }
                    }
                    break;

                case "txtNakagaininCdFr":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom("HANC9021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.han.hanc9021.HANC9021");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtNakagaininCdFr.Text = outData[0];
                                this.lblNakagaininCdFr.Text = outData[1];
                            }
                        }
                    }
                    break;

                case "txtNakagaininCdTo":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom("HANC9021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.han.hanc9021.HANC9021");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtNakagaininCdTo.Text = outData[0];
                                this.lblNakagaininCdTo.Text = outData[1];
                            }
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
            {
                // ﾌﾟﾚﾋﾞｭｰ処理
                DoPrint(true);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("印刷", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }

        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            // 設定画面の起動
            // MEMO:原則としてここで渡す帳票IDの設定はReport.csvに保持していることが前提ですが、
            // 保持していない場合は、設定画面での保存(F6)時に新規に設定が保持されます。
            PrintSettingForm psForm = new PrintSettingForm(new string[1] { "HANR3091R" });
            psForm.ShowDialog();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 水揚支所入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMizuageShishoCd_Validating(object sender, CancelEventArgs e)
        {
            if (!this.IsValidMizuageShishoCd())
            {
                e.Cancel = true;

                this.lblMizuageShishoNm.Text = "";
                this.txtMizuageShishoCd.SelectAll();
                this.txtMizuageShishoCd.Focus();
            }
        }

        /// <summary>
        /// 和暦(年)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateYearFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidDateYearFr())
            {
                e.Cancel = true;
                this.txtDateYearFr.SelectAll();
            }
            else
            {
                CheckDateFr();
                SetDateFr();
            }
        }

        /// <summary>
        /// 和暦(月)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateMonthFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidDateMonthFr())
            {
                e.Cancel = true;
                this.txtDateMonthFr.SelectAll();
            }
            else
            {
                CheckDateFr();
                SetDateFr();
            }
        }

        /// <summary>
        /// 和暦(日)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateDayFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidDateDayFr())
            {
                e.Cancel = true;
                this.txtDateDayFr.SelectAll();
            }
            else
            {
                CheckDateFr();
                SetDateFr();
            }
        }

        /// <summary>
        /// 和暦(年)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateYearTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidDateYearTo())
            {
                e.Cancel = true;
                this.txtDateYearTo.SelectAll();
            }
            else
            {
                CheckDateTo();
                SetDateTo();
            }
        }

        /// <summary>
        /// 和暦(月)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateMonthTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidDateMonthTo())
            {
                e.Cancel = true;
                this.txtDateMonthTo.SelectAll();
            }
            else
            {
                CheckDateTo();
                SetDateTo();
            }
        }

        /// <summary>
        /// 和暦(日)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateDayTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidDateDayTo())
            {
                e.Cancel = true;
                this.txtDateDayTo.SelectAll();
            }
            else
            {
                CheckDateTo();
                SetDateTo();
            }
        }

        /// <summary>
        /// 仲買人コード(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtNakagaininCdFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidNakagaininCdFr())
            {
                e.Cancel = true;
                this.txtNakagaininCdFr.SelectAll();
            }
        }

        /// <summary>
        /// 仲買人コード(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtNakagaininCdTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidNakagaininCdTo())
            {
                e.Cancel = true;
                this.txtNakagaininCdTo.SelectAll();

                // Enter処理を無効化
                this._dtFlg = false;
            }
            else
            {
                // Enter処理を有効化
                this._dtFlg = true;
            }
        }

        /// <summary>
        /// 仲買人コード(至)のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtNakagaininCdTo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.Flg)
            {
                // Enter処理を無効化
                this._dtFlg = false;

                // 全項目を再度入力値チェック
                if (!ValidateAll())
                {
                    // エラーありの場合ここで処理終了
                    return;
                }

                if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
                {
                    // ﾌﾟﾚﾋﾞｭｰ処理
                    DoPrint(true);
                }
                else
                {
                    this.txtNakagaininCdTo.Focus();
                }
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 水揚支所の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool IsValidMizuageShishoCd()
        {
            // 空入力の場合
            if (ValChk.IsEmpty(this.txtMizuageShishoCd.Text))
            {
                // 水揚支所名称を表示する
                this.lblMizuageShishoNm.Text = "全て";
                return true;
            }

            // 最大桁数チェック
            if (!ValChk.IsWithinLength(this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.MaxLength))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            // 数値チェック
            if (!ValChk.IsNumber(this.txtMizuageShishoCd.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                return false;
            }

            // 0入力の場合
            if (Equals(this.txtMizuageShishoCd.Text, "0"))
            {
                // 水揚支所名称を表示する
                this.lblMizuageShishoNm.Text = "全て";
                return true;
            }

            // 水揚支所名称を表示する
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_HN_COMBO_DATA_MIZUAGE", this.txtMizuageShishoCd.Text);

            if (ValChk.IsEmpty(this.lblMizuageShishoNm.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 年(自)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidDateYearFr()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtDateYearFr.Text))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }
            // 空の場合、0年として処理
            if (ValChk.IsEmpty(this.txtDateYearFr.Text))
            {
                this.txtDateYearFr.Text = "0";
            }

            return true;
        }

        /// <summary>
        /// 月(自)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidDateMonthFr()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtDateMonthFr.Text))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }
            // 空の場合、1月として処理
            if (ValChk.IsEmpty(this.txtDateMonthFr.Text))
            {
                this.txtDateMonthFr.Text = "1";
            }
            else
            {
                // 12を超える月が入力された場合、12月として処理
                if (Util.ToInt(this.txtDateMonthFr.Text) > 12)
                {
                    this.txtDateMonthFr.Text = "12";
                }
                // 1より小さい月が入力された場合、1月として処理
                else if (Util.ToInt(this.txtDateMonthFr.Text) < 1)
                {
                    this.txtDateMonthFr.Text = "1";
                }
            }

            return true;
        }

        /// <summary>
        /// 日(自)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidDateDayFr()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtDateDayFr.Text))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }

            if (ValChk.IsEmpty(this.txtDateDayFr.Text))
            {
                // 空の場合、1日として処理
                this.txtDateDayFr.Text = "1";
            }
            else
            {
                // 1より小さい日が入力された場合、1日として処理
                if (Util.ToInt(this.txtDateDayFr.Text) < 1)
                {
                    this.txtDateDayFr.Text = "1";
                }
            }

            return true;
        }

        /// <summary>
        /// 年月日(自)の月末入力チェック
        /// </summary>
        /// 
        private void CheckDateFr()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                this.txtDateMonthFr.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDateDayFr.Text) > lastDayInMonth)
            {
                this.txtDateDayFr.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(自)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetDateFr()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDateFr(Util.FixJpDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                this.txtDateMonthFr.Text, this.txtDateDayFr.Text, this.Dba));
        }

        /// <summary>
        /// 年(至)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidDateYearTo()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtDateYearTo.Text))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }
            // 空の場合、0年として処理
            if (ValChk.IsEmpty(this.txtDateYearTo.Text))
            {
                this.txtDateYearTo.Text = "0";
            }

            return true;
        }

        /// <summary>
        /// 月(至)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidDateMonthTo()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtDateMonthTo.Text))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }
            // 空の場合、1月として処理
            if (ValChk.IsEmpty(this.txtDateMonthTo.Text))
            {
                this.txtDateMonthTo.Text = "1";
            }
            else
            {
                // 12を超える月が入力された場合、12月として処理
                if (Util.ToInt(this.txtDateMonthTo.Text) > 12)
                {
                    this.txtDateMonthTo.Text = "12";
                }
                // 1より小さい月が入力された場合、1月として処理
                else if (Util.ToInt(this.txtDateMonthTo.Text) < 1)
                {
                    this.txtDateMonthTo.Text = "1";
                }
            }

            return true;
        }

        /// <summary>
        /// 日(至)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidDateDayTo()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtDateDayTo.Text))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }

            if (ValChk.IsEmpty(this.txtDateDayTo.Text))
            {
                // 空の場合、1日として処理
                this.txtDateDayTo.Text = "1";
            }
            else
            {
                // 1より小さい日が入力された場合、1日として処理
                if (Util.ToInt(this.txtDateDayTo.Text) < 1)
                {
                    this.txtDateDayTo.Text = "1";
                }
            }

            return true;
        }

        /// <summary>
        /// 年月日(至)の月末入力チェック
        /// </summary>
        /// 
        private void CheckDateTo()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                this.txtDateMonthTo.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDateDayTo.Text) > lastDayInMonth)
            {
                this.txtDateDayTo.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(至)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetDateTo()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDateTo(Util.FixJpDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                this.txtDateMonthTo.Text, this.txtDateDayTo.Text, this.Dba));
        }

        /// <summary>
        /// 仲買人コード(自)の入力チェック
        /// </summary>
        private bool IsValidNakagaininCdFr()
        {
            if (ValChk.IsEmpty(this.txtNakagaininCdFr.Text))
            {
                this.lblNakagaininCdFr.Text = "先　頭";
            }
            else
                // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
                if (!ValChk.IsNumber(this.txtNakagaininCdFr.Text))
                {
                    Msg.Error("仲買人コードは数値のみで入力してください。");
                    return false;
                }
                else
                {
                    // コードを元に名称を取得する
                    this.lblNakagaininCdFr.Text = this.Dba.GetName(this.UInfo, "VI_HN_NAKAGAI", this.txtNakagaininCdFr.Text);
                }

            return true;
        }

        /// <summary>
        /// 仲買人コード(至)の入力チェック
        /// </summary>
        private bool IsValidNakagaininCdTo()
        {
            if (ValChk.IsEmpty(this.txtNakagaininCdTo.Text))
            {
                this.lblNakagaininCdTo.Text = "最　後";
            }
            else
                // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
                if (!ValChk.IsNumber(this.txtNakagaininCdTo.Text))
                {
                    Msg.Error("仲買人コードは数値のみで入力してください。");
                    return false;
                }
                else
                {
                    // コードを元に名称を取得する
                    this.lblNakagaininCdTo.Text = this.Dba.GetName(this.UInfo, "VI_HN_NAKAGAI", this.txtNakagaininCdTo.Text);
                }

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 水揚支所の入力チェック
            if (!IsValidMizuageShishoCd())
            {
                this.txtMizuageShishoCd.Focus();
                this.txtMizuageShishoCd.SelectAll();
                return false;
            }

            // 年(自)のチェック
            if (!IsValidDateYearFr())
            {
                this.txtDateYearFr.Focus();
                this.txtDateYearFr.SelectAll();
                return false;
            }
            // 月(自)のチェック
            if (!IsValidDateMonthFr())
            {
                this.txtDateMonthFr.Focus();
                this.txtDateMonthFr.SelectAll();
                return false;
            }
            // 日(自)のチェック
            if (!IsValidDateDayFr())
            {
                this.txtDateDayFr.Focus();
                this.txtDateDayFr.SelectAll();
                return false;
            }
            // 年月日(自)の月末入力チェック処理
            CheckDateFr();
            // 年月日(自)の正しい和暦への変換処理
            SetDateFr();

            // 年(至)のチェック
            if (!IsValidDateYearTo())
            {
                this.txtDateYearTo.Focus();
                this.txtDateYearTo.SelectAll();
                return false;
            }
            // 月(至)のチェック
            if (!IsValidDateMonthTo())
            {
                this.txtDateMonthTo.Focus();
                this.txtDateMonthTo.SelectAll();
                return false;
            }
            // 日(至)のチェック
            if (!IsValidDateDayTo())
            {
                this.txtDateDayTo.Focus();
                this.txtDateDayTo.SelectAll();
                return false;
            }
            // 年月日(至)の月末入力チェック処理
            CheckDateTo();
            // 年月日(至)の正しい和暦への変換処理
            SetDateTo();

            // 仲買人コード(自)の入力チェック
            if (!IsValidNakagaininCdFr())
            {
                this.txtNakagaininCdFr.Focus();
                this.txtNakagaininCdFr.SelectAll();
                return false;
            }
            // 仲買人コード(至)の入力チェック
            if (!IsValidNakagaininCdTo())
            {
                this.txtNakagaininCdTo.Focus();
                this.txtNakagaininCdTo.SelectAll();
                return false;
            }

            return true;
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpDateFr(string[] arrJpDate)
        {
            this.lblDateGengoFr.Text = arrJpDate[0];
            this.txtDateYearFr.Text = arrJpDate[2];
            this.txtDateMonthFr.Text = arrJpDate[3];
            this.txtDateDayFr.Text = arrJpDate[4];
        }
        private void SetJpDateTo(string[] arrJpDate)
        {
            this.lblDateGengoTo.Text = arrJpDate[0];
            this.txtDateYearTo.Text = arrJpDate[2];
            this.txtDateMonthTo.Text = arrJpDate[3];
            this.txtDateDayTo.Text = arrJpDate[4];
        }

        /// <summary>
        /// 検索サブウィンドウオープン
        /// </summary>
        /// <param name="moduleName">例："COMC8111"</param>
        /// <param name="para1">例："TB_HN_COMBO_DATA_MIZUAGE"</param>
        /// <param name="textBoxOfCode">例：this.txtGyogyoushuCd.Text</param>
        /// <param name="indata">例： this.txtGyogyoushuCd.Text</param>
        /// <returns>String[0]：検索結果から選択したコード , String[1]：検索結果から選択した名称</returns>
        private String[] openSearchWindow(String moduleName, String para1, String indata)
        {
            string[] result = { "", "" };

            // ネームスペースに使うモジュール名の小文字
            string lowerModuleName = moduleName.ToLower();

            // ネームスペースの末尾
            string nameSpace = lowerModuleName.Substring(0, 3);

            // アセンブリのロード
            Assembly asm = Assembly.LoadFrom(moduleName + ".exe");

            // フォーム作成
            string moduleNameSpace = "jp.co.fsi." + nameSpace + "." + lowerModuleName + "." + moduleName;
            Type t = asm.GetType(moduleNameSpace);

            if (t != null)
            {
                Object obj = Activator.CreateInstance(t);
                if (obj != null)
                {
                    BasePgForm frm = (BasePgForm)obj;
                    frm.Par1 = para1;
                    frm.InData = indata;
                    frm.ShowDialog(this);

                    if (frm.DialogResult == DialogResult.OK)
                    {
                        string[] ret = (string[])frm.OutData;
                        result[0] = ret[0];
                        result[1] = ret[1];
                        return result;
                    }
                }
            }

            return result;
        }


        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview)
        {
            try
            {
                this.Dba.BeginTransaction();

                // 帳票出力用にワークテーブルにデータを作成
                bool dataFlag = MakeWkData();

                // 帳票出力
                if (dataFlag)
                {
                    // 取得列の定義
                    StringBuilder cols = new StringBuilder();
                    cols.Append("  ITEM01");
                    cols.Append(" ,ITEM02");
                    cols.Append(" ,ITEM03");
                    cols.Append(" ,ITEM04");
                    cols.Append(" ,ITEM05");
                    cols.Append(" ,ITEM06");
                    cols.Append(" ,ITEM07");
                    cols.Append(" ,ITEM08");
                    cols.Append(" ,ITEM09");
                    cols.Append(" ,ITEM10");

                    // バインドパラメータの設定
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);

                    // データの取得
                    DataTable dtOutput = this.Dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_HN_TBL", "GUID = @GUID", "SORT ASC", dpc);

                    if (isPreview)
                    {
                        // 帳票オブジェクトをインスタンス化
                        HANR3091R rpt = new HANR3091R(dtOutput);
                        PreviewForm pFrm = new PreviewForm(rpt, this.UnqId);
                        // プレビュー画面表示
                        pFrm.WindowState = FormWindowState.Maximized;
                        pFrm.Show();
                    }
                    else
                    {
                        // 帳票オブジェクトをインスタンス化
                        HANR3091R rpt = new HANR3091R(dtOutput);
                        // 直接印刷
                        rpt.Run(false);
                        rpt.Document.Print(true, true, false);
                    }
                }
            }
            finally
            {
                this.Dba.Rollback();
            }
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData()
        {
            // 会計期間開始日を取得
            DateTime kaikeiNendoKaishiBi = Util.ToDate(this.UInfo.KaikeiSettings["KAIKEI_KIKAN_KAISHIBI"]);

            // 日付範囲を西暦にして取得
            DateTime tmpDateFr = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                    this.txtDateMonthFr.Text, this.txtDateDayFr.Text, this.Dba);
            DateTime tmpDateTo = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                    this.txtDateMonthTo.Text, this.txtDateDayTo.Text, this.Dba);
            DateTime tmpDateTo2 = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                    this.txtDateMonthTo.Text, Util.ToString(DateTime.DaysInMonth(tmpDateTo.Year, Util.ToInt(this.txtDateMonthTo.Text))), this.Dba);

            // 会計年度の設定
            string kaikeiNendo;
            if (kaikeiNendoKaishiBi.Month <= tmpDateFr.Month)
            {
                kaikeiNendo = Util.ToString(tmpDateFr.Year);
            }
            else
            {
                kaikeiNendo = Util.ToString(tmpDateFr.Year - 1);
            }

            // 日付範囲を和暦で保持
            string[] tmpjpDateFr = Util.ConvJpDate(tmpDateFr, this.Dba);
            string[] tmpjpDateTo = Util.ConvJpDate(tmpDateTo, this.Dba);
            string[] tmpjpDateTo2 = Util.ConvJpDate(tmpDateTo2, this.Dba);

            // 表示する日付を設定する
            string hyojiDate; // 表示用日付
            string dateFrChk = string.Format("{0}{1}{2}{3}", tmpjpDateFr[0], tmpjpDateFr[2], tmpjpDateFr[3], tmpjpDateFr[4]);
            string dateToChk = string.Format("{0}{1}{2}{3}", tmpjpDateTo[0], tmpjpDateTo[2], tmpjpDateTo[3], tmpjpDateTo[4]);
            string dateFrChk2 = string.Format("{0}{1}{2}{3}", tmpjpDateTo[0], tmpjpDateTo[2], tmpjpDateTo[3], "1");
            string dateToChk2 = string.Format("{0}{1}{2}{3}", tmpjpDateTo[0], tmpjpDateTo[2], tmpjpDateTo[3], tmpjpDateTo2[4]);
            if (dateFrChk == dateFrChk2 && dateToChk == dateToChk2)
            {
                hyojiDate = string.Format("{0}{1}年{2}月", tmpjpDateFr[0], tmpjpDateFr[2], tmpjpDateFr[3]) + "度";
            }
            else
            {
                hyojiDate = string.Format("{0}{1}年{2}月{3}日", tmpjpDateFr[0], tmpjpDateFr[2], tmpjpDateFr[3], tmpjpDateFr[4])
                          + "～"
                          + string.Format("{0}{1}年{2}月{3}日", tmpjpDateTo[0], tmpjpDateTo[2], tmpjpDateTo[3], tmpjpDateTo[4]);
            }

            // ループ用カウント変数
            int i = 1; 

            // 帳票表示用変数
            Decimal suryo = 0;
            Decimal shohizei = 0;
            Decimal karikata = 0;
            Decimal kashikata = 0;
            Decimal zandaka = 0;

            // 仲買人コード設定
            string nakagaininCdFr;
            string nakagaininCdTo;
            if (Util.ToDecimal(txtNakagaininCdFr.Text) > 0)
            {
                nakagaininCdFr = txtNakagaininCdFr.Text;
            }
            else
            {
                nakagaininCdFr = "0";
            }
            if (Util.ToDecimal(txtNakagaininCdTo.Text) > 0)
            {
                nakagaininCdTo = txtNakagaininCdTo.Text;
            }
            else
            {
                nakagaininCdTo = "9999";
            }

            // 選択した日付に発生しているデータの取得
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder Sql = new StringBuilder();
            Sql.Append(" SELECT");
            Sql.Append("     A.TORIHIKISAKI_CD            AS KAIIN_BANGO,");
            Sql.Append("     MIN(A.TORIHIKISAKI_NM)  AS KAIIN_NM,");
            Sql.Append("     MIN(A.DENWA_BANGO)       AS DENWA_BANGO,");
            Sql.Append("     MIN(A.FAX_BANGO)     AS FAX_BANGO,");
            Sql.Append("     MIN(E.SHOHIZEI_HASU_SHORI) AS SHOHIZEI_HASU_SHORI,");
            Sql.Append("     MIN(2)               AS SHOHIZEI_TENKA_HOHO,");
            Sql.Append("     SUM(CASE WHEN C.TAISHAKU_KUBUN = D.TAISHAKU_KUBUN THEN C.ZEIKOMI_KINGAKU ELSE (C.ZEIKOMI_KINGAKU * -1) END)                       AS ZANDAKA,");
            Sql.Append("     SUM(CASE WHEN C.DENPYO_DATE < @DENPYO_DATE_FR  THEN         (CASE WHEN C.TAISHAKU_KUBUN = D.TAISHAKU_KUBUN THEN C.ZEIKOMI_KINGAKU ELSE (C.ZEIKOMI_KINGAKU * -1) END)     ELSE 0 END)     AS KISHU_ZAN");
            Sql.Append(" FROM");
            Sql.Append("     VI_HN_TORIHIKISAKI_JOHO AS A  ");
            Sql.Append(" LEFT OUTER JOIN  ");
            Sql.Append("     TB_HN_KISHU_ZAN_KAMOKU AS B    ");
            Sql.Append("     ON A.KAISHA_CD     = B.KAISHA_CD   AND ");
            Sql.Append("     2 = B.MOTOCHO_KUBUN  ");
            Sql.Append(" LEFT OUTER JOIN  ");
            Sql.Append("     TB_ZM_SHIWAKE_MEISAI AS C    ");
            Sql.Append("     ON A.KAISHA_CD     = C.KAISHA_CD   AND ");
            Sql.Append("     A.TORIHIKISAKI_CD   = C.HOJO_KAMOKU_CD   AND ");
            Sql.Append("     B.KANJO_KAMOKU_CD = C.KANJO_KAMOKU_CD   AND ");
            Sql.Append("     C.DENPYO_DATE       < @DENPYO_DATE_TO  AND ");
            Sql.Append("     C.KAIKEI_NENDO = @KAIKEI_NENDO ");
            Sql.Append(" LEFT OUTER JOIN  ");
            Sql.Append("     TB_ZM_KANJO_KAMOKU AS D    ");
            Sql.Append("     ON B.KAISHA_CD     = D.KAISHA_CD   AND ");
            Sql.Append("     B.KANJO_KAMOKU_CD = D.KANJO_KAMOKU_CD AND ");
            Sql.Append("     C.KAIKEI_NENDO = D.KAIKEI_NENDO  ");
            Sql.Append(" LEFT OUTER JOIN  ");
            Sql.Append("     TB_ZM_SHOHIZEI_JOHO AS E    ");
            Sql.Append("     ON A.KAISHA_CD     = E.KAISHA_CD  AND ");
            Sql.Append("     E.KESSANKI     = @KESSANKI AND ");
            Sql.Append("     C.KAIKEI_NENDO = E.KAIKEI_NENDO ");
            Sql.Append(" WHERE");
            Sql.Append(" 	 C.KAIKEI_NENDO = @KAIKEI_NENDO AND ");
            Sql.Append("     A.KAISHA_CD = @KAISHA_CD AND");
            Sql.Append("     A.TORIHIKISAKI_CD  BETWEEN @TORIHIKISAKI_CD_FR AND @TORIHIKISAKI_CD_TO AND");
            Sql.Append("     A.TORIHIKISAKI_KUBUN2 = 2");
            Sql.Append(" GROUP BY");
            Sql.Append("     A.TORIHIKISAKI_CD");
            Sql.Append(" ORDER BY");
            Sql.Append("     A.TORIHIKISAKI_CD ASC");

            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, kaikeiNendo);
            dpc.SetParam("@KESSANKI", SqlDbType.Decimal, 2, this.UInfo.KessanKi);
            dpc.SetParam("@DENPYO_DATE_FR", SqlDbType.VarChar, 10, tmpDateFr.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@DENPYO_DATE_TO", SqlDbType.VarChar, 10, tmpDateTo.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@TORIHIKISAKI_CD_FR", SqlDbType.VarChar, 4, nakagaininCdFr);
            dpc.SetParam("@TORIHIKISAKI_CD_TO", SqlDbType.VarChar, 4, nakagaininCdTo);

            DataTable dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            if (dtMainLoop.Rows.Count == 0)
            {
                Msg.Info("該当データがありません。");
                return false;
            }
            else
            {
                foreach (DataRow dr in dtMainLoop.Rows)
                {
                    // 当月借方発生
                    Sql = new StringBuilder();
                    dpc = new DbParamCollection();
                    Sql.Append(" SELECT");
                    Sql.Append("     B.HOJO_KAMOKU_CD    AS TORIHIKISAKI_CD,");
                    Sql.Append("     B.ZEIKOMI_KINGAKU     AS ZEIKOMI_KINGAKU,");
                    Sql.Append("     (SELECT");
                    Sql.Append("         CASE WHEN (ISNULL(Y.KAMOKU_BUNRUI_CD, 0) = 21110) THEN 1 ELSE 0 END");
                    Sql.Append("     FROM");
                    Sql.Append("         TB_ZM_SHIWAKE_MEISAI AS X  ");
                    Sql.Append("     LEFT OUTER JOIN ");
                    Sql.Append("         TB_ZM_KANJO_KAMOKU AS Y    ");
                    Sql.Append("         ON (X.KAISHA_CD = Y.KAISHA_CD)   AND ");
                    Sql.Append("         (X.KANJO_KAMOKU_CD = Y.KANJO_KAMOKU_CD) AND");
                    Sql.Append(" 		X.KAIKEI_NENDO = Y.KAIKEI_NENDO");
                    Sql.Append("     WHERE");
                    Sql.Append(" 		X.KAIKEI_NENDO = @KAIKEI_NENDO AND");
                    Sql.Append("         X.KAISHA_CD = B.KAISHA_CD   AND");
                    Sql.Append("         X.DENPYO_BANGO   = B.DENPYO_BANGO   AND");
                    Sql.Append("         X.GYO_BANGO     = B.GYO_BANGO   AND");
                    Sql.Append("         X.TAISHAKU_KUBUN   = (CASE WHEN B.TAISHAKU_KUBUN = 1 THEN 2 ELSE 1 END)   AND");
                    Sql.Append("         X.MEISAI_KUBUN   = B.MEISAI_KUBUN");
                    Sql.Append("     )    AS TAISHO_KUBUN");
                    Sql.Append(" FROM");
                    Sql.Append("     TB_HN_NYUKIN_KAMOKU AS A  ");
                    Sql.Append(" LEFT OUTER JOIN  ");
                    Sql.Append("     TB_ZM_SHIWAKE_MEISAI AS B    ");
                    Sql.Append("     ON A.KAISHA_CD     = B.KAISHA_CD   AND ");
                    Sql.Append("     1                = B.DENPYO_KUBUN   AND ");
                    Sql.Append("     A.KANJO_KAMOKU_CD = B.KANJO_KAMOKU_CD  ");
                    Sql.Append(" LEFT OUTER JOIN  ");
                    Sql.Append("     TB_ZM_KANJO_KAMOKU AS C    ");
                    Sql.Append("     ON A.KAISHA_CD     = C.KAISHA_CD   AND ");
                    Sql.Append("     A.KANJO_KAMOKU_CD = C.KANJO_KAMOKU_CD AND");
                    Sql.Append(" 	B.KAIKEI_NENDO = C.KAIKEI_NENDO");
                    Sql.Append(" WHERE");
                    Sql.Append(" 	B.KAIKEI_NENDO = @KAIKEI_NENDO AND");
                    Sql.Append("     A.KAISHA_CD     = @KAISHA_CD AND");
                    Sql.Append("     A.MOTOCHO_KUBUN       = 1 AND");
                    Sql.Append("     B.HOJO_KAMOKU_CD  = @HOJO_KAMOKU_CD AND");
                    Sql.Append("     B.DENPYO_DATE Between @DENPYO_DATE_FR AND @DENPYO_DATE_TO AND");
                    Sql.Append("     B.TAISHAKU_KUBUN  =  C.TAISHAKU_KUBUN AND");
                    Sql.Append("     B.SHIWAKE_SAKUSEI_KUBUN IS NULL");

                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                    dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, kaikeiNendo);
                    dpc.SetParam("@DENPYO_DATE_FR", SqlDbType.VarChar, 10, tmpDateFr.Date.ToString("yyyy/MM/dd"));
                    dpc.SetParam("@DENPYO_DATE_TO", SqlDbType.VarChar, 10, tmpDateTo.Date.ToString("yyyy/MM/dd"));
                    dpc.SetParam("@HOJO_KAMOKU_CD", SqlDbType.VarChar, 4, Util.ToDecimal(dr["KAIIN_BANGO"]));

                    DataTable dtKarikata2 = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

                    // 当月貸方発生
                    Sql = new StringBuilder();
                    dpc = new DbParamCollection();
                    Sql.Append(" SELECT");
                    Sql.Append("     B.HOJO_KAMOKU_CD    AS TORIHIKISAKI_CD,");
                    Sql.Append("     B.ZEIKOMI_KINGAKU     AS ZEIKOMI_KINGAKU,");
                    Sql.Append("     (SELECT");
                    Sql.Append("         CASE WHEN (ISNULL(Y.KAMOKU_BUNRUI_CD, 0) = 21110) THEN 1 ELSE 0 END");
                    Sql.Append("     FROM");
                    Sql.Append("         TB_ZM_SHIWAKE_MEISAI AS X  ");
                    Sql.Append("     LEFT OUTER JOIN ");
                    Sql.Append("         TB_ZM_KANJO_KAMOKU AS Y    ");
                    Sql.Append("         ON (X.KAISHA_CD = Y.KAISHA_CD)   AND ");
                    Sql.Append("         (X.KANJO_KAMOKU_CD = Y.KANJO_KAMOKU_CD) AND ");
                    Sql.Append(" 		(X.KAIKEI_NENDO = Y.KAIKEI_NENDO)");
                    Sql.Append("     WHERE");
                    Sql.Append("         X.KAIKEI_NENDO = @KAIKEI_NENDO   AND");
                    Sql.Append("         X.KAISHA_CD = B.KAISHA_CD   AND");
                    Sql.Append("         X.DENPYO_BANGO   = B.DENPYO_BANGO   AND");
                    Sql.Append("         X.GYO_BANGO     = B.GYO_BANGO   AND");
                    Sql.Append("         X.TAISHAKU_KUBUN   = (CASE WHEN B.TAISHAKU_KUBUN = 1 THEN 2 ELSE 1 END)   AND");
                    Sql.Append("         X.MEISAI_KUBUN   = B.MEISAI_KUBUN");
                    Sql.Append("     )    AS TAISHO_KUBUN");
                    Sql.Append(" FROM");
                    Sql.Append("     TB_HN_NYUKIN_KAMOKU AS A  ");
                    Sql.Append(" LEFT OUTER JOIN  ");
                    Sql.Append("     TB_ZM_SHIWAKE_MEISAI AS B    ");
                    Sql.Append("     ON A.KAISHA_CD     = B.KAISHA_CD   AND ");
                    Sql.Append("     1                = B.DENPYO_KUBUN   AND ");
                    Sql.Append("     A.KANJO_KAMOKU_CD = B.KANJO_KAMOKU_CD  ");
                    Sql.Append(" LEFT OUTER JOIN  ");
                    Sql.Append("     TB_ZM_KANJO_KAMOKU AS C    ");
                    Sql.Append("     ON A.KAISHA_CD     = C.KAISHA_CD   AND ");
                    Sql.Append("     A.KANJO_KAMOKU_CD = C.KANJO_KAMOKU_CD AND");
                    Sql.Append(" 	 B.KAIKEI_NENDO = C.KAIKEI_NENDO");
                    Sql.Append(" WHERE");
                    Sql.Append(" 	 B.KAIKEI_NENDO = @KAIKEI_NENDO AND ");
                    Sql.Append("     A.KAISHA_CD     = @KAISHA_CD AND");
                    Sql.Append("     A.MOTOCHO_KUBUN       = 1 AND");
                    Sql.Append("     B.HOJO_KAMOKU_CD = @HOJO_KAMOKU_CD AND");
                    Sql.Append("     B.DENPYO_DATE Between @DENPYO_DATE_FR AND @DENPYO_DATE_TO AND");
                    Sql.Append("     B.TAISHAKU_KUBUN  <>  C.TAISHAKU_KUBUN");
                    Sql.Append(" ORDER BY ");
                    Sql.Append(" 	 TORIHIKISAKI_CD");

                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                    dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, kaikeiNendo);
                    dpc.SetParam("@DENPYO_DATE_FR", SqlDbType.VarChar, 10, tmpDateFr.Date.ToString("yyyy/MM/dd"));
                    dpc.SetParam("@DENPYO_DATE_TO", SqlDbType.VarChar, 10, tmpDateTo.Date.ToString("yyyy/MM/dd"));
                    dpc.SetParam("@HOJO_KAMOKU_CD", SqlDbType.VarChar, 4, Util.ToDecimal(dr["KAIIN_BANGO"]));

                    DataTable dtKashikata = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

                    // 発生数量,当月借方発生,消費税額
                    Sql = new StringBuilder();
                    dpc = new DbParamCollection();
                    Sql.Append(" SELECT");
                    Sql.Append("     A.SEISAN_BI   AS SEISAN_BI,");
                    Sql.Append("     A.NAKAGAININ_CD   AS TORIHIKISAKI_CD,");
                    Sql.Append("     A.ZEI_RITSU           AS ZEI_RITSU,");
                    Sql.Append("     B.SHOHIZEI_NYURYOKU_HOHO AS SHOHIZEI_NYURYOKU_HOHO,");
                    Sql.Append("     SUM(A.SURYO) AS MIZUAGE_SURYO,");
                    Sql.Append("     SUM(A.URIAGE_KINGAKU) AS URIAGE_KINGAKU,");
                    Sql.Append("     SUM(A.TESURYO) AS TESURYO,");
                    Sql.Append("     SUM(A.SHOHINDAI) AS SHOHINDAI,");
                    Sql.Append("     ROUND((SUM(A.URIAGE_KINGAKU) * (A.ZEI_RITSU/100)),0) AS SHOHIZEI,");
                    Sql.Append("     SUM(A.URIAGE_KINGAKU) AS ZEINUKI_KINGAKU,");
                    Sql.Append("     SUM(A.SHOHIZEI)   AS URIAGE_SHOHIZEI");
                    Sql.Append(" FROM");
                    Sql.Append("     VI_HN_SERI_MOTOCHO AS A ");
                    Sql.Append(" LEFT OUTER JOIN ");
                    Sql.Append("     VI_HN_TORIHIKISAKI_JOHO AS B  ");
                    Sql.Append("     ON A.KAISHA_CD = B.KAISHA_CD AND ");
                    Sql.Append("     A.NAKAGAININ_CD = B.TORIHIKISAKI_CD");
                    Sql.Append(" WHERE");
                    Sql.Append("     A.KAISHA_CD     = @KAISHA_CD AND");
                    Sql.Append("     A.TORIHIKI_KUBUN1     = 1 AND");
                    Sql.Append("     A.SEISAN_BI Between @DENPYO_DATE_FR AND @DENPYO_DATE_TO AND");
                    Sql.Append("     (A.NAKAGAININ_CD = @NAKAGAININ_CD)");
                    Sql.Append(" GROUP BY");
                    Sql.Append("     A.SEISAN_BI,");
                    Sql.Append("     A.NAKAGAININ_CD,");
                    Sql.Append("     A.ZEI_RITSU,");
                    Sql.Append("     B.SHOHIZEI_NYURYOKU_HOHO");
                    Sql.Append(" ORDER BY");
                    Sql.Append("     A.NAKAGAININ_CD");

                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                    dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, kaikeiNendo);
                    dpc.SetParam("@DENPYO_DATE_FR", SqlDbType.VarChar, 10, tmpDateFr.Date.ToString("yyyy/MM/dd"));
                    dpc.SetParam("@DENPYO_DATE_TO", SqlDbType.VarChar, 10, tmpDateTo.Date.ToString("yyyy/MM/dd"));
                    dpc.SetParam("@NAKAGAININ_CD", SqlDbType.VarChar, 4, Util.ToDecimal(dr["KAIIN_BANGO"]));

                    DataTable dtKarikata = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

                    if (!(0.Equals(dtKashikata.Rows.Count) && 0.Equals(dtKarikata.Rows.Count)) || Util.ToDecimal(dr["ZANDAKA"]) > 0)
                    {
                        // 金額の計算
                        foreach (DataRow dr2 in dtKarikata2.Rows)
                        {
                            karikata += Util.ToDecimal(dr2["ZEIKOMI_KINGAKU"]);
                        }

                        foreach (DataRow dr2 in dtKashikata.Rows)
                        {
                            kashikata += Util.ToDecimal(dr2["ZEIKOMI_KINGAKU"]);
                        }

                        foreach (DataRow dr2 in dtKarikata.Rows)
                        {
                            suryo += Util.ToDecimal(dr2["MIZUAGE_SURYO"]);
                            karikata += Util.ToDecimal(dr2["ZEINUKI_KINGAKU"]) + Util.ToDecimal(dr2["SHOHIZEI"]);
                            shohizei += Util.ToDecimal(dr2["SHOHIZEI"]);
                        }

                        zandaka = Util.ToDecimal(dr["KISHU_ZAN"]) + karikata - kashikata;

                        // 取得したデータをワークテーブルに更新をする
                        Sql = new StringBuilder();
                        dpc = new DbParamCollection();
                        Sql.Append("INSERT INTO PR_HN_TBL(");
                        Sql.Append("  GUID");
                        Sql.Append(" ,SORT");
                        Sql.Append(" ,ITEM01");
                        Sql.Append(" ,ITEM02");
                        Sql.Append(" ,ITEM03");
                        Sql.Append(" ,ITEM04");
                        Sql.Append(" ,ITEM05");
                        Sql.Append(" ,ITEM06");
                        Sql.Append(" ,ITEM07");
                        Sql.Append(" ,ITEM08");
                        Sql.Append(" ,ITEM09");
                        Sql.Append(" ,ITEM10");
                        Sql.Append(") ");
                        Sql.Append("VALUES(");
                        Sql.Append("  @GUID");
                        Sql.Append(" ,@SORT");
                        Sql.Append(" ,@ITEM01");
                        Sql.Append(" ,@ITEM02");
                        Sql.Append(" ,@ITEM03");
                        Sql.Append(" ,@ITEM04");
                        Sql.Append(" ,@ITEM05");
                        Sql.Append(" ,@ITEM06");
                        Sql.Append(" ,@ITEM07");
                        Sql.Append(" ,@ITEM08");
                        Sql.Append(" ,@ITEM09");
                        Sql.Append(" ,@ITEM10");
                        Sql.Append(") ");

                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                        dpc.SetParam("@SORT", SqlDbType.VarChar, 4, i);
                        // ページヘッダーデータを設定
                        dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);
                        dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, hyojiDate);
                        // データを設定
                        dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dr["KAIIN_BANGO"]);
                        dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dr["KAIIN_NM"]);
                        dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, Util.FormatNum(suryo, 1));
                        dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, Util.FormatNum(Util.ToDecimal(dr["KISHU_ZAN"])));
                        dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, Util.FormatNum(karikata));
                        dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, Util.FormatNum(kashikata));
                        dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, Util.FormatNum(zandaka));
                        dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, Util.FormatNum(shohizei));

                        this.Dba.ModifyBySql(Util.ToString(Sql), dpc);

                        // 金額のリセット
                        suryo = 0;
                        karikata = 0;
                        kashikata = 0;
                        shohizei = 0;
                        i++;
                    }
                }
            }

            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
                "SORT",
                "PR_HN_TBL",
                "GUID = @GUID",
                dpc);

            bool dataFlag;
            if (tmpdtPR_HN_TBL.Rows.Count > 0)
            {
                dataFlag = true;
            }
            else
            {
                dataFlag = false;
            }
            
            return dataFlag;

        }

        #endregion

    }
}
