﻿using System;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.access;
using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.han.hanr1051
{
    /// <summary>
    /// 水揚仕切日計表(HANR1051)
    /// </summary>
    public partial class HANR1051 : BasePgForm
    {
        #region プロパティ
        /// <summary>
        /// 画面上最後となるフォーカスのEnterボタン押下時処理用変数
        /// </summary>
        private bool _dtFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._dtFlg;
            }
        }
        #endregion

        #region 定数
        /// <summary>
        /// 支払区分選択(全て)
        /// </summary>
        private const string KOZA_ALL = "0";

        /// <summary>
        /// 支払区分選択(口座有り)
        /// </summary>
        private const string KOZA_ARI = "1";

        /// <summary>
        /// 支払区分選択(口座無し)
        /// </summary>
        private const string KOZA_NASHI = "2";

        /// <summary>
        /// 精算区分(地区内ｾﾘ)
        /// </summary>
        private const string CHIKUNAI = "3";

        /// <summary>
        /// 精算区分(浜売り)
        /// </summary>
        private const string HAMA_URI = "2";

        /// <summary>
        /// 精算区分(地区外ｾﾘ)
        /// </summary>
        private const string CHIKUGAI = "1";

        /// <summary>
        /// 精算区分(全て)
        /// </summary>
        private const string SEISAN_ALL = "0";
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HANR1051()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region メソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 水揚支所
            this.txtMizuageShishoCd.Text = "1";
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_HN_COMBO_DATA_MIZUAGE", this.txtMizuageShishoCd.Text);

            string[] jpDate = Util.ConvJpDate(DateTime.Now, this.Dba);
            // 日付範囲前
            lblDateGengoFr.Text = jpDate[0];
            txtDateYearFr.Text = jpDate[2];
            txtDateMonthFr.Text = jpDate[3];
            txtDateDayFr.Text = jpDate[4];
            // 日付範囲後
            lblDateGengoTo.Text = jpDate[0];
            txtDateYearTo.Text = jpDate[2];
            txtDateMonthTo.Text = jpDate[3];
            txtDateDayTo.Text = jpDate[4];
            // 支払区分選択
            txtShiharaiKubun.Text = "0";
            // 精算区分地区内ｾﾘにチェック
            rdoChikunai.Checked = true;

            // フォーカス設定
            this.txtDateYearFr.Focus();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 日付(年)、水揚支所にフォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd":
                case "txtDateYearFr":
                case "txtDateYearTo":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;
            String[] result;

            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd": // 水揚支所
                    result = this.openSearchWindow("COMC8011", "TB_HN_COMBO_DATA_MIZUAGE", this.txtMizuageShishoCd.Text);
                    if (!ValChk.IsEmpty(result[0]))
                    {
                        this.txtMizuageShishoCd.Text = result[0];
                        this.lblMizuageShishoNm.Text = result[1];
                    }
                    break;

                case "txtDateYearFr":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom("COMC9011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.com.comc9011.COMC9011");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblDateGengoFr.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.lblDateGengoFr.Text = result[1];

                                // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
                                DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                                    this.txtDateMonthFr.Text, "1", this.Dba);
                                int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);
                                if (Util.ToInt(this.txtDateDayFr.Text) > lastDayInMonth)
                                {
                                    this.txtDateDayFr.Text = Util.ToString(lastDayInMonth);
                                }

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblDateGengoFr.Text,
                                        this.txtDateYearFr.Text,
                                        this.txtDateMonthFr.Text,
                                        this.txtDateDayFr.Text,
                                        this.Dba);
                                this.lblDateGengoFr.Text = arrJpDate[0];
                                this.txtDateYearFr.Text = arrJpDate[2];
                                this.txtDateMonthFr.Text = arrJpDate[3];
                                this.txtDateDayFr.Text = arrJpDate[4];
                            }
                        }
                    }
                    break;

                case "txtDateYearTo":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom("COMC9011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.com.comc9011.COMC9011");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblDateGengoTo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                result = (String[])frm.OutData;
                                this.lblDateGengoTo.Text = result[1];

                                // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
                                DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                                    this.txtDateMonthTo.Text, "1", this.Dba);
                                int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);
                                if (Util.ToInt(this.txtDateDayTo.Text) > lastDayInMonth)
                                {
                                    this.txtDateDayTo.Text = Util.ToString(lastDayInMonth);
                                }

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblDateGengoTo.Text,
                                        this.txtDateYearTo.Text,
                                        this.txtDateMonthTo.Text,
                                        this.txtDateDayTo.Text,
                                        this.Dba);
                                this.lblDateGengoTo.Text = arrJpDate[0];
                                this.txtDateYearTo.Text = arrJpDate[2];
                                this.txtDateMonthTo.Text = arrJpDate[3];
                                this.txtDateDayTo.Text = arrJpDate[4];
                            }
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            // プレビュー処理
            DoPrint(true);
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfYesNo("実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }

        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            // 設定画面の起動
            // MEMO:原則としてここで渡す帳票IDの設定はReport.csvに保持していることが前提ですが、
            // 保持していない場合は、設定画面での保存(F6)時に新規に設定が保持されます。
            PrintSettingForm psForm = new PrintSettingForm(new string[1] { "HANR1051R" });
            psForm.ShowDialog();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 水揚支所入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMizuageShishoCd_Validating(object sender, CancelEventArgs e)
        {
            if (!this.isValidMizuageShishoCd())
            {
                e.Cancel = true;

                this.lblMizuageShishoNm.Text = "";
                this.txtMizuageShishoCd.SelectAll();
                this.txtMizuageShishoCd.Focus();
            }
        }

        /// <summary>
        /// 年(自)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateYearFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidDateYearFr())
            {
                e.Cancel = true;
                this.txtDateYearFr.SelectAll();
            }
            else
            {
                CheckJpDateFr();
                SetJpDateFr();
            }
        }

        /// <summary>
        /// 月(自)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateMonthFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidDateMonthFr())
            {
                e.Cancel = true;
                this.txtDateMonthFr.SelectAll();
            }
            else
            {
                CheckJpDateFr();
                SetJpDateFr();
            }
        }

        /// <summary>
        /// 日(自)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateDayFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidDateDayFr())
            {
                e.Cancel = true;
                this.txtDateDayFr.SelectAll();
            }
            else
            {
                CheckJpDateFr();
                SetJpDateFr();
            }
        }
        
        /// <summary>
        /// 年(至)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateYearTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidDateYearTo())
            {
                e.Cancel = true;
                this.txtDateYearTo.SelectAll();
            }
            else
            {
                CheckJpDateTo();
                SetJpDateTo();
            }
        }

        /// <summary>
        /// 月(至)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateMonthTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidDateMonthTo())
            {
                e.Cancel = true;
                this.txtDateMonthTo.SelectAll();
            }
            else
            {
                CheckJpDateTo();
                SetJpDateTo();
            }
        }

        /// <summary>
        /// 日(至)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateDayTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidDateDayTo())
            {
                e.Cancel = true;
                this.txtDateDayTo.SelectAll();
            }
            else
            {
                CheckJpDateTo();
                SetJpDateTo();
            }
        }

        /// <summary>
        /// 支払区分選択の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShiharaiKubun_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShiharaiKubun())
            {
                e.Cancel = true;
                this.txtShiharaiKubun.SelectAll();

                // Enter処理を無効化
                this._dtFlg = false;
            }
            else
            {
                // Enter処理を有効化
                this._dtFlg = true;
            }
        }

        /// <summary>
        /// 支払区分のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShiharaiKubun_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.Flg)
            {
                // Enter処理を無効化
                this._dtFlg = false;

                // 全項目を再度入力値チェック
                if (!ValidateAll())
                {
                    // エラーありの場合ここで処理終了
                    return;
                }

                if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
                {
                    // ﾌﾟﾚﾋﾞｭｰ処理
                    DoPrint(true);
                }
                else
                {
                    this.txtShiharaiKubun.Focus();
                }
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 水揚支所の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool isValidMizuageShishoCd()
        {
            // 空入力の場合
            if (ValChk.IsEmpty(this.txtMizuageShishoCd.Text))
            {
                // 水揚支所名称を表示する
                this.lblMizuageShishoNm.Text = "全て";
                return true;
            }

            // 最大桁数チェック
            if (!ValChk.IsWithinLength(this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.MaxLength))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            if (!ValChk.IsNumber(this.txtMizuageShishoCd.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                return false;
            }

            // 0入力の場合
            if (Equals(this.txtMizuageShishoCd.Text, "0"))
            {
                // 水揚支所名称を表示する
                this.lblMizuageShishoNm.Text = "全て";
                return true;
            }

            // 水揚支所名称を表示する
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_HN_COMBO_DATA_MIZUAGE", this.txtMizuageShishoCd.Text);

            if (ValChk.IsEmpty(this.lblMizuageShishoNm.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 年(自)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidDateYearFr()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtDateYearFr.Text))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }
            // 空の場合、0年として処理
            if (ValChk.IsEmpty(this.txtDateYearFr.Text))
            {
                this.txtDateYearFr.Text = "0";
            }

            return true;
        }

        /// <summary>
        /// 月(自)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidDateMonthFr()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtDateMonthFr.Text))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }
            // 空の場合、1月として処理
            if (ValChk.IsEmpty(this.txtDateMonthFr.Text))
            {
                this.txtDateMonthFr.Text = "1";
            }
            else
            {
                // 12を超える月が入力された場合、12月として処理
                if (Util.ToInt(this.txtDateMonthFr.Text) > 12)
                {
                    this.txtDateMonthFr.Text = "12";
                }
                // 1より小さい月が入力された場合、1月として処理
                else if (Util.ToInt(this.txtDateMonthFr.Text) < 1)
                {
                    this.txtDateMonthFr.Text = "1";
                }
            }

            return true;
        }

        /// <summary>
        /// 日(自)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidDateDayFr()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtDateDayFr.Text))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }

            if (ValChk.IsEmpty(this.txtDateDayFr.Text))
            {
                // 空の場合、1日として処理
                this.txtDateDayFr.Text = "1";
            }
            else
            {
                // 1より小さい日が入力された場合、1日として処理
                if (Util.ToInt(this.txtDateDayFr.Text) < 1)
                {
                    this.txtDateDayFr.Text = "1";
                }
            }

            return true;
        }

        /// <summary>
        /// 年月日(自)の月末入力チェック
        /// </summary>
        /// 
        private void CheckJpDateFr()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                this.txtDateMonthFr.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDateDayFr.Text) > lastDayInMonth)
            {
                this.txtDateDayFr.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(自)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetJpDateFr()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDateFr(Util.FixJpDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                this.txtDateMonthFr.Text, this.txtDateDayFr.Text, this.Dba));
        }

        /// <summary>
        /// 年(至)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidDateYearTo()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtDateYearTo.Text))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }
            // 空の場合、0年として処理
            if (ValChk.IsEmpty(this.txtDateYearTo.Text))
            {
                this.txtDateYearTo.Text = "0";
            }

            return true;
        }

        /// <summary>
        /// 月(至)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidDateMonthTo()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtDateMonthTo.Text))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }
            // 空の場合、1月として処理
            if (ValChk.IsEmpty(this.txtDateMonthTo.Text))
            {
                this.txtDateMonthTo.Text = "1";
            }
            else
            {
                // 12を超える月が入力された場合、12月として処理
                if (Util.ToInt(this.txtDateMonthTo.Text) > 12)
                {
                    this.txtDateMonthTo.Text = "12";
                }
                // 1より小さい月が入力された場合、1月として処理
                else if (Util.ToInt(this.txtDateMonthTo.Text) < 1)
                {
                    this.txtDateMonthTo.Text = "1";
                }
            }

            return true;
        }

        /// <summary>
        /// 日(至)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidDateDayTo()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtDateDayTo.Text))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }

            if (ValChk.IsEmpty(this.txtDateDayTo.Text))
            {
                // 空の場合、1日として処理
                this.txtDateDayTo.Text = "1";
            }
            else
            {
                // 1より小さい日が入力された場合、1日として処理
                if (Util.ToInt(this.txtDateDayTo.Text) < 1)
                {
                    this.txtDateDayTo.Text = "1";
                }
            }

            return true;
        }

        /// <summary>
        /// 年月日(至)の月末入力チェック
        /// </summary>
        /// 
        private void CheckJpDateTo()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                this.txtDateMonthTo.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDateDayTo.Text) > lastDayInMonth)
            {
                this.txtDateDayTo.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(至)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetJpDateTo()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDateTo(Util.FixJpDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                this.txtDateMonthTo.Text, this.txtDateDayTo.Text, this.Dba));
        }

        /// <summary>
        /// 支払区分選択の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShiharaiKubun()
        {
            // 未入力は0とする
            if (ValChk.IsEmpty(this.txtShiharaiKubun.Text))
            {
                this.txtShiharaiKubun.Text = "0";
            }

            // 0,1,2のみ入力を許可
            if (!ValChk.IsNumber(this.txtShiharaiKubun.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            int intval = Util.ToInt(this.txtShiharaiKubun.Text);
            if (intval < 0 || intval > 2)
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 水揚支所の入力チェック
            if (!isValidMizuageShishoCd())
            {
                this.txtMizuageShishoCd.Focus();
                this.txtMizuageShishoCd.SelectAll();
                return false;
            }

            // 年(自)のチェック
            if (!IsValidDateYearFr())
            {
                this.txtDateYearFr.Focus();
                this.txtDateYearFr.SelectAll();
                return false;
            }
            // 月(自)のチェック
            if (!IsValidDateMonthFr())
            {
                this.txtDateMonthFr.Focus();
                this.txtDateMonthFr.SelectAll();
                return false;
            }
            // 日(自)のチェック
            if (!IsValidDateDayFr())
            {
                this.txtDateDayFr.Focus();
                this.txtDateDayFr.SelectAll();
                return false;
            }
            // 年月日(自)の月末入力チェック処理
            CheckJpDateFr();
            // 年月日(自)の正しい和暦への変換処理
            SetJpDateFr();

            // 年(至)のチェック
            if (!IsValidDateYearTo())
            {
                this.txtDateYearTo.Focus();
                this.txtDateYearTo.SelectAll();
                return false;
            }
            // 月(至)のチェック
            if (!IsValidDateMonthTo())
            {
                this.txtDateMonthTo.Focus();
                this.txtDateMonthTo.SelectAll();
                return false;
            }
            // 日(至)のチェック
            if (!IsValidDateDayTo())
            {
                this.txtDateDayTo.Focus();
                this.txtDateDayTo.SelectAll();
                return false;
            }
            // 年月日(至)の月末入力チェック処理
            CheckJpDateTo();
            // 年月日(至)の正しい和暦への変換処理
            SetJpDateTo();

            // 支払区分選択の入力チェック
            IsValidShiharaiKubun();

            return true;
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpDateFr(string[] arrJpDate)
        {
            this.lblDateGengoFr.Text = arrJpDate[0];
            this.txtDateYearFr.Text = arrJpDate[2];
            this.txtDateMonthFr.Text = arrJpDate[3];
            this.txtDateDayFr.Text = arrJpDate[4];
        }
        private void SetJpDateTo(string[] arrJpDate)
        {
            this.lblDateGengoTo.Text = arrJpDate[0];
            this.txtDateYearTo.Text = arrJpDate[2];
            this.txtDateMonthTo.Text = arrJpDate[3];
            this.txtDateDayTo.Text = arrJpDate[4];
        }

        /// <summary>
        /// 検索サブウィンドウオープン
        /// </summary>
        /// <param name="moduleName">例："COMC8111"</param>
        /// <param name="para1">例："TB_HN_COMBO_DATA_MIZUAGE"</param>
        /// <param name="textBoxOfCode">例：this.txtGyogyoushuCd.Text</param>
        /// <param name="indata">例： this.txtGyogyoushuCd.Text</param>
        /// <returns>String[0]：検索結果から選択したコード , String[1]：検索結果から選択した名称</returns>
        private String[] openSearchWindow(String moduleName, String para1, String indata)
        {
            string[] result = { "", "" };

            // ネームスペースに使うモジュール名の小文字
            string lowerModuleName = moduleName.ToLower();

            // ネームスペースの末尾
            string nameSpace = lowerModuleName.Substring(0, 3);

            // アセンブリのロード
            Assembly asm = Assembly.LoadFrom(moduleName + ".exe");

            // フォーム作成
            string moduleNameSpace = "jp.co.fsi." + nameSpace + "." + lowerModuleName + "." + moduleName;
            Type t = asm.GetType(moduleNameSpace);

            if (t != null)
            {
                Object obj = Activator.CreateInstance(t);
                if (obj != null)
                {
                    BasePgForm frm = (BasePgForm)obj;
                    frm.Par1 = para1;
                    frm.InData = indata;
                    frm.ShowDialog(this);

                    if (frm.DialogResult == DialogResult.OK)
                    {
                        string[] ret = (string[])frm.OutData;
                        result[0] = ret[0];
                        result[1] = ret[1];
                        return result;
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview)
        {
            bool dataFlag;
            try
            {
                this.Dba.BeginTransaction();

                // 帳票出力用にワークテーブルにデータを作成
                dataFlag = MakeWkData();
                this.Dba.Commit();
            }
            finally
            {
                this.Dba.Rollback();
            }

            // 帳票出力
            if (dataFlag)
            {
                // 現状アクセスだが、アクティブレポートに移行します。
                Report rpt = new Report();
                rpt.OutputReport(Path.Combine(Util.GetPath(), Constants.REP_DIR, "HANR1051.mdb"), "R_HANR1051", this.UnqId, isPreview);
            }

            // ワークテーブルに作成したデータを削除
            try
            {
                this.Dba.BeginTransaction();

                // 帳票出力用に作成したデータを削除
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                this.Dba.Delete("PR_HN_TBL", "GUID = @GUID", dpc);
                this.Dba.Commit();
            }
            finally
            {
                this.Dba.Rollback();
            }
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData()
        {
            // 日付範囲を西暦にして取得
            DateTime tmpDateFr = Util.ConvAdDate(this.lblDateGengoFr.Text, this.txtDateYearFr.Text,
                    this.txtDateMonthFr.Text, this.txtDateDayFr.Text, this.Dba);
            DateTime tmpDateTo = Util.ConvAdDate(this.lblDateGengoTo.Text, this.txtDateYearTo.Text,
                    this.txtDateMonthTo.Text, this.txtDateDayTo.Text, this.Dba);
            // 日付範囲を和暦で保持
            string[] tmpjpDateFr = Util.ConvJpDate(tmpDateFr, this.Dba);
            string[] tmpjpDateTo = Util.ConvJpDate(tmpDateTo, this.Dba);

            // 支払区分選択設定
            string shiharaiKubunSettei;
            shiharaiKubunSettei = KOZA_ALL;
            string tmpShiharaiKubunNm;
            tmpShiharaiKubunNm = "( 全て )";
            if (txtShiharaiKubun.Text == "2")
            {
                shiharaiKubunSettei = KOZA_NASHI;
                tmpShiharaiKubunNm = "( 口座無し )";
            }
            else if (txtShiharaiKubun.Text == "1")
            {
                shiharaiKubunSettei = KOZA_ARI;
                tmpShiharaiKubunNm = "( 口座有り )";
            }
            else if (txtShiharaiKubun.Text == "0")
            {
                shiharaiKubunSettei = KOZA_ALL;
                tmpShiharaiKubunNm = "( 全て )";
            }

            // 精算区分設定
            string seisanKubunSettei;
            seisanKubunSettei = "SEIKU" + CHIKUNAI + "_KENSU";
            string tmpSeisanKubunNm;
            tmpSeisanKubunNm = "地区内セリ";
            if (rdoChikunai.Checked)
            {
                seisanKubunSettei = "SEIKU" + CHIKUNAI + "_KENSU";
                tmpSeisanKubunNm = "地区内セリ";
            }
            else if (rdoHamauri.Checked)
            {
                seisanKubunSettei = "SEIKU" + HAMA_URI + "_KENSU";
                tmpSeisanKubunNm = "浜売り";
            }
            else if (rdoChikugai.Checked)
            {
                seisanKubunSettei = "SEIKU" + CHIKUGAI + "_KENSU";
                tmpSeisanKubunNm = "地区外セリ";
            }
            else if (rdoAll.Checked)
            {
                seisanKubunSettei = SEISAN_ALL;
                tmpSeisanKubunNm = "全て";
            }

            // 入力された情報を元にワークテーブルに更新をする
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder Sql = new StringBuilder();

            // han.VI_水揚仕切日計表(VI_HN_MIZUAGE_SKR_NIKKEIHYO)の日付範囲から発生しているデータを取得
            Sql.Append("SELECT");
            Sql.Append(" * ");
            Sql.Append("FROM");
            Sql.Append(" VI_HN_MIZUAGE_SKR_NIKKEIHYO ");
            Sql.Append("WHERE");
            Sql.Append(" KAISHA_CD = @KAISHA_CD AND");
            if (shiharaiKubunSettei == "1")
            {
                Sql.Append(" SHIHARAI_KUBUN = 1 AND");
            }
            else if (shiharaiKubunSettei == "2")
            {
                Sql.Append(" SHIHARAI_KUBUN <> 1 AND");
            }
            Sql.Append(" SEISAN_BI BETWEEN @DATE_FR AND @DATE_TO");
            if (seisanKubunSettei != "0")
            {
                Sql.Append(" AND ");
                Sql.Append(seisanKubunSettei + ">= 1");
            }
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@DATE_FR", SqlDbType.VarChar, 10, tmpDateFr.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@DATE_TO", SqlDbType.VarChar, 10, tmpDateTo.Date.ToString("yyyy/MM/dd"));

            DataTable dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            if (dtMainLoop.Rows.Count == 0)
            {
                Msg.Info("該当データがありません。");
                return false;
            }
            else
            {
                if (Msg.ConfYesNo("実行しますか？") == DialogResult.Yes)
                {
                    #region メインループ準備処理
                    int dbSORT = 1;
                    int i = 0; // ループ用カウント変数
                    string tmpSeikuKensu = ""; // 精区件数変数
                    string tmpSeikuSuryo = ""; // 精区数量変数
                    string tmpSeikuZeinukiKingaku = ""; // 精区税抜金額変数
                    string tmpSeikuShohizei = ""; // 精区消費税変数
                    string tmpSeikuZeikomiKingaku = ""; // 精区税込金額変数
                    string tmpSeikuKojoKomoku = ""; // 精区控除項目変数
                    int condKind = 0; // 切り分け用変数
                    #endregion
                    
                    while (dtMainLoop.Rows.Count > i)
                    {
                        #region データの準備
                        if (rdoChikunai.Checked)
                        {
                            condKind = 1;
                        }
                        else if (rdoHamauri.Checked)
                        {
                            condKind = 2;
                        }
                        else if (rdoChikugai.Checked)
                        {
                            condKind = 3;
                        }
                        else if (rdoAll.Checked)
                        {
                            if (Util.ToInt(dtMainLoop.Rows[i]["SEIKU1_KENSU"]) != 0)
                            {
                                condKind = 1;
                            }
                            else if (Util.ToInt(dtMainLoop.Rows[i]["SEIKU2_KENSU"]) != 0)
                            {
                                condKind = 2;
                            }
                            else if (Util.ToInt(dtMainLoop.Rows[i]["SEIKU2_KENSU"]) != 0)
                            {
                                condKind = 3;
                            }
                        }
                        switch (condKind)
                        {
                            case 1:
                                tmpSeikuKensu = Util.ToString(dtMainLoop.Rows[i]["SEIKU3_KENSU"]); // 精区件数
                                tmpSeikuSuryo = Util.ToString(dtMainLoop.Rows[i]["SEIKU3_SURYO"]); // 精区数量
                                tmpSeikuZeinukiKingaku = Util.ToString(dtMainLoop.Rows[i]["SEIKU3_ZEINUKI_KINGAKU"]); // 精区税抜金額
                                tmpSeikuShohizei = Util.ToString(dtMainLoop.Rows[i]["SEIKU3_SHOHIZEI"]); // 精区消費税
                                tmpSeikuZeikomiKingaku = Util.ToString(dtMainLoop.Rows[i]["SEIKU3_ZEIKOMI_KINGAKU"]); // 精区税込金額
                                tmpSeikuKojoKomoku = Util.ToString(dtMainLoop.Rows[i]["SEIKU3KOJO_KOMOKU1"]); // 精区控除項目
                                break;
                            case 2:
                                tmpSeikuKensu = Util.ToString(dtMainLoop.Rows[i]["SEIKU2_KENSU"]); // 精区件数
                                tmpSeikuSuryo = Util.ToString(dtMainLoop.Rows[i]["SEIKU2_SURYO"]); // 精区数量
                                tmpSeikuZeinukiKingaku = Util.ToString(dtMainLoop.Rows[i]["SEIKU2_ZEINUKI_KINGAKU"]); // 精区税抜金額
                                tmpSeikuShohizei = Util.ToString(dtMainLoop.Rows[i]["SEIKU2_SHOHIZEI"]); // 精区消費税
                                tmpSeikuZeikomiKingaku = Util.ToString(dtMainLoop.Rows[i]["SEIKU2_ZEIKOMI_KINGAKU"]); // 精区税込金額
                                tmpSeikuKojoKomoku = Util.ToString(dtMainLoop.Rows[i]["SEIKU2KOJO_KOMOKU1"]); // 精区控除項目
                                break;
                            case 3:
                                tmpSeikuKensu = Util.ToString(dtMainLoop.Rows[i]["SEIKU1_KENSU"]); // 精区件数
                                tmpSeikuSuryo = Util.ToString(dtMainLoop.Rows[i]["SEIKU1_SURYO"]); // 精区数量
                                tmpSeikuZeinukiKingaku = Util.ToString(dtMainLoop.Rows[i]["SEIKU1_ZEINUKI_KINGAKU"]); // 精区税抜金額
                                tmpSeikuShohizei = Util.ToString(dtMainLoop.Rows[i]["SEIKU1_SHOHIZEI"]); // 精区消費税
                                tmpSeikuZeikomiKingaku = Util.ToString(dtMainLoop.Rows[i]["SEIKU1_ZEIKOMI_KINGAKU"]); // 精区税込金額
                                tmpSeikuKojoKomoku = Util.ToString(dtMainLoop.Rows[i]["SEIKU1KOJO_KOMOKU2"]); // 精区控除項目
                                break;
                        }
                        #endregion

                        dpc = new DbParamCollection();
                        Sql = new StringBuilder();

                        #region インサートテーブル
                        Sql = new StringBuilder();
                        dpc = new DbParamCollection();
                        Sql.Append("INSERT INTO PR_HN_TBL(");
                        Sql.Append("  GUID");
                        Sql.Append(" ,SORT");
                        Sql.Append(" ,ITEM01");
                        Sql.Append(" ,ITEM02");
                        Sql.Append(" ,ITEM03");
                        Sql.Append(" ,ITEM04");
                        Sql.Append(" ,ITEM05");
                        Sql.Append(" ,ITEM06");
                        Sql.Append(" ,ITEM07");
                        Sql.Append(" ,ITEM08");
                        Sql.Append(" ,ITEM09");
                        Sql.Append(" ,ITEM10");
                        Sql.Append(" ,ITEM11");
                        Sql.Append(" ,ITEM12");
                        Sql.Append(" ,ITEM13");
                        Sql.Append(") ");
                        Sql.Append("VALUES(");
                        Sql.Append("  @GUID");
                        Sql.Append(" ,@SORT");
                        Sql.Append(" ,@ITEM01");
                        Sql.Append(" ,@ITEM02");
                        Sql.Append(" ,@ITEM03");
                        Sql.Append(" ,@ITEM04");
                        Sql.Append(" ,@ITEM05");
                        Sql.Append(" ,@ITEM06");
                        Sql.Append(" ,@ITEM07");
                        Sql.Append(" ,@ITEM08");
                        Sql.Append(" ,@ITEM09");
                        Sql.Append(" ,@ITEM10");
                        Sql.Append(" ,@ITEM11");
                        Sql.Append(" ,@ITEM12");
                        Sql.Append(" ,@ITEM13");
                        Sql.Append(") ");
                        #endregion

                        #region データ登録
                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                        dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                        dbSORT++;
                        dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 組合名
                        dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, Util.ToDateStr(dtMainLoop.Rows[i]["SEISAN_BI"])); // セリ日
                        dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, tmpShiharaiKubunNm); // 支払区分
                        dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, tmpSeisanKubunNm); // 精算区分
                        dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SENSHU_CD"]); // コード
                        dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["SENSHU_NM"]); // 組合員
                        dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, tmpSeikuKensu); // 伝票件数
                        dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, Util.FormatNum(tmpSeikuSuryo, 1)); // 数量
                        dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["TANKA"], 0)); // 単価
                        dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, Util.FormatNum(tmpSeikuZeinukiKingaku)); // 水揚金額
                        dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, Util.FormatNum(tmpSeikuShohizei)); // 消費税
                        dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, Util.FormatNum(tmpSeikuZeikomiKingaku)); // 税込合計
                        dpc.SetParam("@ITEM13", SqlDbType.VarChar, 200, Util.FormatNum(tmpSeikuKojoKomoku)); // 漁協手数料

                        this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                        #endregion

                        i++;
                    }
                }
            }

            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
                "SORT",
                "PR_HN_TBL",
                "GUID = @GUID",
                dpc);

            bool dataFlag;
            if (tmpdtPR_HN_TBL.Rows.Count > 0)
            {
                dataFlag = true;
            }
            else
            {
                dataFlag = false;
            }

            return dataFlag;

        }
        #endregion
    }
}
