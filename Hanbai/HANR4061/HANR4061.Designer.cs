﻿namespace jp.co.fsi.han.hanr4061
{
    partial class HANR4061
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbxDate = new System.Windows.Forms.GroupBox();
            this.lblDateYear = new System.Windows.Forms.Label();
            this.txtDateYear = new jp.co.fsi.common.controls.SosTextBox();
            this.lblDateGengo = new System.Windows.Forms.Label();
            this.lblDate = new System.Windows.Forms.Label();
            this.gbxNakagaininCd = new System.Windows.Forms.GroupBox();
            this.lblNakagaininCdTo = new System.Windows.Forms.Label();
            this.lblCodeBet = new System.Windows.Forms.Label();
            this.txtNakagaininCdFr = new jp.co.fsi.common.controls.SosTextBox();
            this.lblNakagaininCdFr = new System.Windows.Forms.Label();
            this.txtNakagaininCdTo = new jp.co.fsi.common.controls.SosTextBox();
            this.pnlDebug.SuspendLayout();
            this.gbxDate.SuspendLayout();
            this.gbxNakagaininCd.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Text = "";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Size = new System.Drawing.Size(847, 100);
            // 
            // gbxDate
            // 
            this.gbxDate.Controls.Add(this.lblDateYear);
            this.gbxDate.Controls.Add(this.txtDateYear);
            this.gbxDate.Controls.Add(this.lblDateGengo);
            this.gbxDate.Controls.Add(this.lblDate);
            this.gbxDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxDate.ForeColor = System.Drawing.Color.Black;
            this.gbxDate.Location = new System.Drawing.Point(12, 50);
            this.gbxDate.Name = "gbxDate";
            this.gbxDate.Size = new System.Drawing.Size(136, 63);
            this.gbxDate.TabIndex = 1;
            this.gbxDate.TabStop = false;
            this.gbxDate.Text = "年指定";
            // 
            // lblDateYear
            // 
            this.lblDateYear.BackColor = System.Drawing.Color.Silver;
            this.lblDateYear.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateYear.ForeColor = System.Drawing.Color.Black;
            this.lblDateYear.Location = new System.Drawing.Point(97, 24);
            this.lblDateYear.Name = "lblDateYear";
            this.lblDateYear.Size = new System.Drawing.Size(17, 21);
            this.lblDateYear.TabIndex = 3;
            this.lblDateYear.Text = "年";
            this.lblDateYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDateYear
            // 
            this.txtDateYear.AutoSizeFromLength = false;
            this.txtDateYear.DisplayLength = null;
            this.txtDateYear.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateYear.ForeColor = System.Drawing.Color.Black;
            this.txtDateYear.Location = new System.Drawing.Point(65, 24);
            this.txtDateYear.MaxLength = 2;
            this.txtDateYear.Name = "txtDateYear";
            this.txtDateYear.Size = new System.Drawing.Size(30, 20);
            this.txtDateYear.TabIndex = 1;
            this.txtDateYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateYear.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateYear_Validating);
            // 
            // lblDateGengo
            // 
            this.lblDateGengo.BackColor = System.Drawing.Color.Silver;
            this.lblDateGengo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDateGengo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateGengo.ForeColor = System.Drawing.Color.Black;
            this.lblDateGengo.Location = new System.Drawing.Point(20, 24);
            this.lblDateGengo.Name = "lblDateGengo";
            this.lblDateGengo.Size = new System.Drawing.Size(41, 21);
            this.lblDateGengo.TabIndex = 1;
            this.lblDateGengo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDate
            // 
            this.lblDate.BackColor = System.Drawing.Color.Silver;
            this.lblDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDate.ForeColor = System.Drawing.Color.Black;
            this.lblDate.Location = new System.Drawing.Point(17, 21);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(103, 27);
            this.lblDate.TabIndex = 1;
            this.lblDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // gbxNakagaininCd
            // 
            this.gbxNakagaininCd.Controls.Add(this.lblNakagaininCdTo);
            this.gbxNakagaininCd.Controls.Add(this.lblCodeBet);
            this.gbxNakagaininCd.Controls.Add(this.txtNakagaininCdFr);
            this.gbxNakagaininCd.Controls.Add(this.lblNakagaininCdFr);
            this.gbxNakagaininCd.Controls.Add(this.txtNakagaininCdTo);
            this.gbxNakagaininCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxNakagaininCd.ForeColor = System.Drawing.Color.Black;
            this.gbxNakagaininCd.Location = new System.Drawing.Point(12, 119);
            this.gbxNakagaininCd.Name = "gbxNakagaininCd";
            this.gbxNakagaininCd.Size = new System.Drawing.Size(598, 75);
            this.gbxNakagaininCd.TabIndex = 2;
            this.gbxNakagaininCd.TabStop = false;
            this.gbxNakagaininCd.Text = "仲買人CD範囲";
            // 
            // lblNakagaininCdTo
            // 
            this.lblNakagaininCdTo.BackColor = System.Drawing.Color.Silver;
            this.lblNakagaininCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblNakagaininCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblNakagaininCdTo.Location = new System.Drawing.Point(355, 30);
            this.lblNakagaininCdTo.Name = "lblNakagaininCdTo";
            this.lblNakagaininCdTo.Size = new System.Drawing.Size(217, 20);
            this.lblNakagaininCdTo.TabIndex = 4;
            this.lblNakagaininCdTo.Text = "最　後";
            this.lblNakagaininCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCodeBet
            // 
            this.lblCodeBet.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblCodeBet.Location = new System.Drawing.Point(286, 29);
            this.lblCodeBet.Name = "lblCodeBet";
            this.lblCodeBet.Size = new System.Drawing.Size(18, 20);
            this.lblCodeBet.TabIndex = 2;
            this.lblCodeBet.Text = "～";
            this.lblCodeBet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtNakagaininCdFr
            // 
            this.txtNakagaininCdFr.AutoSizeFromLength = false;
            this.txtNakagaininCdFr.DisplayLength = null;
            this.txtNakagaininCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtNakagaininCdFr.Location = new System.Drawing.Point(19, 30);
            this.txtNakagaininCdFr.MaxLength = 4;
            this.txtNakagaininCdFr.Name = "txtNakagaininCdFr";
            this.txtNakagaininCdFr.Size = new System.Drawing.Size(40, 20);
            this.txtNakagaininCdFr.TabIndex = 2;
            this.txtNakagaininCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtNakagaininCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtCodeFr_Validating);
            // 
            // lblNakagaininCdFr
            // 
            this.lblNakagaininCdFr.BackColor = System.Drawing.Color.Silver;
            this.lblNakagaininCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblNakagaininCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblNakagaininCdFr.Location = new System.Drawing.Point(62, 30);
            this.lblNakagaininCdFr.Name = "lblNakagaininCdFr";
            this.lblNakagaininCdFr.Size = new System.Drawing.Size(217, 20);
            this.lblNakagaininCdFr.TabIndex = 1;
            this.lblNakagaininCdFr.Text = "先　頭";
            this.lblNakagaininCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtNakagaininCdTo
            // 
            this.txtNakagaininCdTo.AutoSizeFromLength = false;
            this.txtNakagaininCdTo.DisplayLength = null;
            this.txtNakagaininCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtNakagaininCdTo.Location = new System.Drawing.Point(312, 30);
            this.txtNakagaininCdTo.MaxLength = 4;
            this.txtNakagaininCdTo.Name = "txtNakagaininCdTo";
            this.txtNakagaininCdTo.Size = new System.Drawing.Size(40, 20);
            this.txtNakagaininCdTo.TabIndex = 3;
            this.txtNakagaininCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtNakagaininCdTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNakagaininCdTo_KeyDown);
            this.txtNakagaininCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtCodeTo_Validating);
            // 
            // HANR4061
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 638);
            this.Controls.Add(this.gbxNakagaininCd);
            this.Controls.Add(this.gbxDate);
            this.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "HANR4061";
            this.Text = "";
            this.Controls.SetChildIndex(this.gbxDate, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.gbxNakagaininCd, 0);
            this.pnlDebug.ResumeLayout(false);
            this.gbxDate.ResumeLayout(false);
            this.gbxDate.PerformLayout();
            this.gbxNakagaininCd.ResumeLayout(false);
            this.gbxNakagaininCd.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbxDate;
        private jp.co.fsi.common.controls.SosTextBox txtDateYear;
        private System.Windows.Forms.Label lblDateGengo;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.Label lblDateYear;
        private System.Windows.Forms.GroupBox gbxNakagaininCd;
        private System.Windows.Forms.Label lblNakagaininCdTo;
        private System.Windows.Forms.Label lblCodeBet;
        private common.controls.SosTextBox txtNakagaininCdFr;
        private System.Windows.Forms.Label lblNakagaininCdFr;
        private common.controls.SosTextBox txtNakagaininCdTo;

    }
}