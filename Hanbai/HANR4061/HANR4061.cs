﻿using System;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Text;
using System.Reflection;
using System.Windows.Forms;

using GrapeCity.ActiveReports;

using jp.co.fsi.common.access;
using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.han.hanr4061
{
    /// <summary>
    /// 鮮魚買付高証明書(HANR4061)
    /// </summary>
    public partial class HANR4061 : BasePgForm
    {
        #region プロパティ
        /// <summary>
        /// 画面上最後となるフォーカスのEnterボタン押下時処理変数
        /// </summary>
        private bool _dtFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._dtFlg;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HANR4061()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region メソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            string[] jpDate = Util.ConvJpDate(DateTime.Now, this.Dba);
            // 年指定
            lblDateGengo.Text = jpDate[0];
            txtDateYear.Text = jpDate[2];

            // フォーカス設定
            this.txtDateYear.Focus();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 年始定、仲買人コードにフォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtDateYear":
                case "txtNakagaininCdFr":
                case "txtNakagaininCdTo":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;

            switch (this.ActiveCtlNm)
            {
                case "txtDateYear":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom("COMC9011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.com.comc9011.COMC9011");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblDateGengo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.lblDateGengo.Text = result[1];
                            }
                        }
                    }
                    break;

                case "txtNakagaininCdFr":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom("HANC9021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.han.hanc9021.HANC9021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtNakagaininCdFr.Text = outData[0];
                                this.lblNakagaininCdFr.Text = outData[1];
                            }
                        }
                    }
                    break;

                case "txtNakagaininCdTo":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom("HANC9021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.han.hanc9021.HANC9021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtNakagaininCdTo.Text = outData[0];
                                this.lblNakagaininCdTo.Text = outData[1];
                            }
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
            {
                // プレビュー処理
                DoPrint(true);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("印刷", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }

        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            // 設定画面の起動
            // MEMO:原則としてここで渡す帳票IDの設定はReport.csvに保持していることが前提ですが、
            // 保持していない場合は、設定画面での保存(F6)時に新規に設定が保持されます。
            PrintSettingForm psForm = new PrintSettingForm(new string[1] { "HANR4061R" });
            psForm.ShowDialog();
        }

        #endregion

        #region イベント
        /// <summary>
        /// 年指定の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateYear_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidYear())
            {
                e.Cancel = true;
                this.txtDateYear.SelectAll();
            }
        }

        /// <summary>
        /// 仲買人コード(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCodeFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidCodeFr())
            {
                e.Cancel = true;
                this.txtNakagaininCdFr.SelectAll();
            }
        }

        /// <summary>
        /// 仲買人コード(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCodeTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidCodeTo())
            {
                e.Cancel = true;
                this.txtNakagaininCdTo.SelectAll();
                  // Enter処理を無効化
                this._dtFlg = false;
            }
            else
            {
                // Enter処理を有効化
                this._dtFlg = true;
            }
        }

        /// <summary>
        /// 仲買人コード(至)のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtNakagaininCdTo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.Flg)
            {
                // Enter処理を無効化
                this._dtFlg = false;

                // 全項目を再度入力値チェック
                if (!ValidateAll())
                {
                    // エラーありの場合ここで処理終了
                    return;
                }

                if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
                {
                    // ﾌﾟﾚﾋﾞｭｰ処理
                    DoPrint(true);
                }
                else
                {
                    this.txtNakagaininCdTo.Focus();
                }
            }
        }

        #endregion

        #region privateメソッド
        /// <summary>
        /// 年の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidYear()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtDateYear.Text))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }

            // 空の場合、0年として処理
            if (ValChk.IsEmpty(this.txtDateYear.Text))
            {
                this.txtDateYear.Text = "0";
            }

            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDate(Util.FixJpDate(this.lblDateGengo.Text, this.txtDateYear.Text,
                "1", "1", this.Dba));

            return true;
        }

        /// <summary>
        /// 仲買人コード(自)の入力チェック
        /// </summary>
        private bool IsValidCodeFr()
        {
            if (ValChk.IsEmpty(this.txtNakagaininCdFr.Text))
            {
                this.lblNakagaininCdFr.Text = "先　頭";
            }
            else
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtNakagaininCdFr.Text))
            {
                Msg.Error("コード(自)は数値のみで入力してください。");
                return false;
            }
            else
            {
                // コードを元に取引先名称を取得する
                this.lblNakagaininCdFr.Text = this.Dba.GetName(this.UInfo, "VI_HN_NAKAGAI", this.txtNakagaininCdFr.Text);
            }

            return true;
        }

        /// <summary>
        /// 仲買人コード(至)の入力チェック
        /// </summary>
        private bool IsValidCodeTo()
        {
            if (ValChk.IsEmpty(this.txtNakagaininCdTo.Text))
            {
                this.lblNakagaininCdTo.Text = "最　後";
            }
            else
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtNakagaininCdTo.Text))
            {
                Msg.Error("コード(至)は数値のみで入力してください。");
                return false;
            }
            else
            {
                // コードを元に取引先名称を取得する
                this.lblNakagaininCdTo.Text = this.Dba.GetName(this.UInfo, "VI_HN_NAKAGAI", this.txtNakagaininCdTo.Text);
            }

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 年のチェック
            if (!IsValidYear())
            {
                this.txtDateYear.Focus();
                this.txtDateYear.SelectAll();
                return false;
            }

            // 仲買人コード(自)の入力チェック
            if (!IsValidCodeFr())
            {
                this.txtNakagaininCdFr.Focus();
                this.txtNakagaininCdFr.SelectAll();
                return false;
            }
            // 仲買人コード(至)の入力チェック
            if (!IsValidCodeTo())
            {
                this.txtNakagaininCdTo.Focus();
                this.txtNakagaininCdTo.SelectAll();
                return false;
            }

            return true;
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpDate(string[] arrJpDate)
        {
            this.lblDateGengo.Text = arrJpDate[0];
            this.txtDateYear.Text = arrJpDate[2];
        }

        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview)
        {
            try
            {
                bool dataFlag;

                this.Dba.BeginTransaction();

                // 帳票出力用にワークテーブルにデータを作成
                dataFlag = MakeWkData();

                // 帳票出力
                if (dataFlag)
                {
                    // 取得列の定義
                    StringBuilder cols = new StringBuilder();
                    cols.Append("  ITEM01");
                    cols.Append(" ,ITEM02");
                    cols.Append(" ,ITEM03");
                    cols.Append(" ,ITEM04");
                    cols.Append(" ,ITEM05");
                    cols.Append(" ,ITEM06");
                    cols.Append(" ,ITEM07");
                    cols.Append(" ,ITEM08");
                    cols.Append(" ,ITEM09");
                    cols.Append(" ,ITEM10");
                    cols.Append(" ,ITEM11");
                    cols.Append(" ,ITEM12");
                    cols.Append(" ,ITEM13");
                    cols.Append(" ,ITEM14");
                    cols.Append(" ,ITEM15");
                    cols.Append(" ,ITEM16");
                    cols.Append(" ,ITEM17");
                    cols.Append(" ,ITEM18");
                    cols.Append(" ,ITEM19");
                    cols.Append(" ,ITEM20");
                    cols.Append(" ,ITEM21");
                    cols.Append(" ,ITEM22");
                    cols.Append(" ,ITEM23");
                    cols.Append(" ,ITEM24");
                    cols.Append(" ,ITEM25");
                    cols.Append(" ,ITEM26");
                    cols.Append(" ,ITEM27");
                    cols.Append(" ,ITEM28");
                    cols.Append(" ,ITEM29");
                    cols.Append(" ,ITEM30");
                    cols.Append(" ,ITEM31");
                    cols.Append(" ,ITEM32");
                    cols.Append(" ,ITEM33");
                    cols.Append(" ,ITEM34");
                    cols.Append(" ,ITEM35");
                    cols.Append(" ,ITEM36");
                    cols.Append(" ,ITEM37");
                    cols.Append(" ,ITEM38");
                    cols.Append(" ,ITEM39");
                    cols.Append(" ,ITEM40");
                    cols.Append(" ,ITEM41");
                    cols.Append(" ,ITEM42");
                    cols.Append(" ,ITEM43");
                    cols.Append(" ,ITEM44");
                    cols.Append(" ,ITEM45");
                    cols.Append(" ,ITEM46");
                    cols.Append(" ,ITEM47");
                    cols.Append(" ,ITEM48");
                    cols.Append(" ,ITEM49");
                    cols.Append(" ,ITEM50");
                    cols.Append(" ,ITEM51");
                    cols.Append(" ,ITEM52");
                    cols.Append(" ,ITEM53");
                    cols.Append(" ,ITEM54");

                    // バインドパラメータの設定
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);

                    // データの取得
                    DataTable dtOutput = this.Dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_HN_TBL", "GUID = @GUID", "SORT ASC", dpc);

                    // 帳票オブジェクトをインスタンス化
                    HANR4061R rpt = new HANR4061R(dtOutput);

                    if (isPreview)
                    {
                        // プレビュー画面表示
                        PreviewForm pFrm = new PreviewForm(rpt, this.UnqId);
                        pFrm.WindowState = FormWindowState.Maximized;
                        pFrm.Show();
                    }
                    else
                    {
                        // 直接印刷
                        rpt.Run(false);
                        rpt.Document.Print(true, true, false);
                    }
                }
            }
            finally
            {
                this.Dba.Rollback();
            }
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData()
        {
            #region データ取得の準備
            // 日付範囲を西暦にして取得
            DateTime tmpDate = Util.ConvAdDate(this.lblDateGengo.Text, this.txtDateYear.Text,
                    "1", "1", this.Dba);
            // 表示日付設定
            string hyojiDate = this.lblDateGengo.Text + this.txtDateYear.Text + "年";

            // 仲買人コード設定
            string NAKAGAININ_CD_FR;
            string NAKAGAININ_CD_TO;
            if (Util.ToDecimal(txtNakagaininCdFr.Text) > 0)
            {
                NAKAGAININ_CD_FR = txtNakagaininCdFr.Text;
            }
            else
            {
                NAKAGAININ_CD_FR = "0";
            }
            if (Util.ToDecimal(txtNakagaininCdTo.Text) > 0)
            {
                NAKAGAININ_CD_TO = txtNakagaininCdTo.Text;
            }
            else
            {
                NAKAGAININ_CD_TO = "9999";
            }
            int i = 0; // ループ用カウント変数
            int dbSORT = 1;
            string nakagaininJusho;
            #endregion

            #region メインデータ取得
            // 入力された情報を元にワークテーブルに更新をする
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder Sql = new StringBuilder();
            // Com.TB_会社情報(TB_CM_KAISHA_JOHO)のデータを取得
            Sql.Append(" SELECT");
            Sql.Append(" KAISHA_NM,");
            Sql.Append(" JUSHO1,");
            Sql.Append(" DAIHYOSHA_NM");
            Sql.Append(" FROM");
            Sql.Append(" TB_CM_KAISHA_JOHO");
            Sql.Append(" WHERE");
            Sql.Append(" KAISHA_CD = @KAISHA_CD");
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);

            DataTable dtKaishaJoho = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            dpc = new DbParamCollection();
            Sql = new StringBuilder();
            // han.VI_鮮魚買取高年計(VI_HN_SENGYO_KAITORIDAKA_NNKI)
            // han.VI_取引先情報(VI_HN_TORIHIKISAKI_JOHO)の日付範囲から発生しているデータを取得
            Sql.Append(" SELECT");
            Sql.Append(" A.NAKAGAININ_CD,");
            Sql.Append(" A.NAKAGAININ_NM,");
            Sql.Append(" SUM(\"SURYO_1GATSU\") AS \"SURYO_1GATSU\",");
            Sql.Append(" SUM(\"SURYO_2GATSU\") AS \"SURYO_2GATSU\",");
            Sql.Append(" SUM(\"SURYO_3GATSU\") AS \"SURYO_3GATSU\",");
            Sql.Append(" SUM(\"SURYO_4GATSU\") AS \"SURYO_4GATSU\",");
            Sql.Append(" SUM(\"SURYO_5GATSU\") AS \"SURYO_5GATSU\",");
            Sql.Append(" SUM(\"SURYO_6GATSU\") AS \"SURYO_6GATSU\",");
            Sql.Append(" SUM(\"SURYO_7GATSU\") AS \"SURYO_7GATSU\",");
            Sql.Append(" SUM(\"SURYO_8GATSU\") AS \"SURYO_8GATSU\",");
            Sql.Append(" SUM(\"SURYO_9GATSU\") AS \"SURYO_9GATSU\",");
            Sql.Append(" SUM(\"SURYO_10GATSU\") AS \"SURYO_10GATSU\",");
            Sql.Append(" SUM(\"SURYO_11GATSU\") AS \"SURYO_11GATSU\",");
            Sql.Append(" SUM(\"SURYO_12GATSU\") AS \"SURYO_12GATSU\",");
            Sql.Append(" SUM(\"KINGAKU_1GATSU\") AS \"KINGAKU_1GATSU\",");
            Sql.Append(" SUM(\"KINGAKU_2GATSU\") AS \"KINGAKU_2GATSU\",");
            Sql.Append(" SUM(\"KINGAKU_3GATSU\") AS \"KINGAKU_3GATSU\",");
            Sql.Append(" SUM(\"KINGAKU_4GATSU\") AS \"KINGAKU_4GATSU\",");
            Sql.Append(" SUM(\"KINGAKU_5GATSU\") AS \"KINGAKU_5GATSU\",");
            Sql.Append(" SUM(\"KINGAKU_6GATSU\") AS \"KINGAKU_6GATSU\",");
            Sql.Append(" SUM(\"KINGAKU_7GATSU\") AS \"KINGAKU_7GATSU\",");
            Sql.Append(" SUM(\"KINGAKU_8GATSU\") AS \"KINGAKU_8GATSU\",");
            Sql.Append(" SUM(\"KINGAKU_9GATSU\") AS \"KINGAKU_9GATSU\",");
            Sql.Append(" SUM(\"KINGAKU_10GATSU\") AS \"KINGAKU_10GATSU\",");
            Sql.Append(" SUM(\"KINGAKU_11GATSU\") AS \"KINGAKU_11GATSU\",");
            Sql.Append(" SUM(\"KINGAKU_12GATSU\") AS \"KINGAKU_12GATSU\",");
            Sql.Append(" SUM(\"SHOHIZEI_1GATSU\") AS \"SHOHIZEI_1GATSU\",");
            Sql.Append(" SUM(\"SHOHIZEI_2GATSU\") AS \"SHOHIZEI_2GATSU\",");
            Sql.Append(" SUM(\"SHOHIZEI_3GATSU\") AS \"SHOHIZEI_3GATSU\",");
            Sql.Append(" SUM(\"SHOHIZEI_4GATSU\") AS \"SHOHIZEI_4GATSU\",");
            Sql.Append(" SUM(\"SHOHIZEI_5GATSU\") AS \"SHOHIZEI_5GATSU\",");
            Sql.Append(" SUM(\"SHOHIZEI_6GATSU\") AS \"SHOHIZEI_6GATSU\",");
            Sql.Append(" SUM(\"SHOHIZEI_7GATSU\") AS \"SHOHIZEI_7GATSU\",");
            Sql.Append(" SUM(\"SHOHIZEI_8GATSU\") AS \"SHOHIZEI_8GATSU\",");
            Sql.Append(" SUM(\"SHOHIZEI_9GATSU\") AS \"SHOHIZEI_9GATSU\",");
            Sql.Append(" SUM(\"SHOHIZEI_10GATSU\") AS \"SHOHIZEI_10GATSU\",");
            Sql.Append(" SUM(\"SHOHIZEI_11GATSU\") AS \"SHOHIZEI_11GATSU\",");
            Sql.Append(" SUM(\"SHOHIZEI_12GATSU\") AS \"SHOHIZEI_12GATSU\",");
            Sql.Append(" SUM(\"ZEIKOMI_KINGAKU_1GATSU\") AS \"ZEIKOMI_KINGAKU_1GATSU\",");
            Sql.Append(" SUM(\"ZEIKOMI_KINGAKU_2GATSU\") AS \"ZEIKOMI_KINGAKU_2GATSU\",");
            Sql.Append(" SUM(\"ZEIKOMI_KINGAKU_3GATSU\") AS \"ZEIKOMI_KINGAKU_3GATSU\",");
            Sql.Append(" SUM(\"ZEIKOMI_KINGAKU_4GATSU\") AS \"ZEIKOMI_KINGAKU_4GATSU\",");
            Sql.Append(" SUM(\"ZEIKOMI_KINGAKU_5GATSU\") AS \"ZEIKOMI_KINGAKU_5GATSU\",");
            Sql.Append(" SUM(\"ZEIKOMI_KINGAKU_6GATSU\") AS \"ZEIKOMI_KINGAKU_6GATSU\",");
            Sql.Append(" SUM(\"ZEIKOMI_KINGAKU_7GATSU\") AS \"ZEIKOMI_KINGAKU_7GATSU\",");
            Sql.Append(" SUM(\"ZEIKOMI_KINGAKU_8GATSU\") AS \"ZEIKOMI_KINGAKU_8GATSU\",");
            Sql.Append(" SUM(\"ZEIKOMI_KINGAKU_9GATSU\") AS \"ZEIKOMI_KINGAKU_9GATSU\",");
            Sql.Append(" SUM(\"ZEIKOMI_KINGAKU_10GATSU\") AS \"ZEIKOMI_KINGAKU_10GATSU\",");
            Sql.Append(" SUM(\"ZEIKOMI_KINGAKU_11GATSU\") AS \"ZEIKOMI_KINGAKU_11GATSU\",");
            Sql.Append(" SUM(\"ZEIKOMI_KINGAKU_12GATSU\") AS \"ZEIKOMI_KINGAKU_12GATSU\",");
            Sql.Append(" B.JUSHO1,");
            Sql.Append(" B.JUSHO2,");
            Sql.Append(" B.TORIHIKISAKI_NM");
            Sql.Append(" FROM ");
            Sql.Append(" VI_HN_SENGYO_KAITORIDAKA_NNKI AS A ");
            Sql.Append(" LEFT OUTER JOIN ");
            Sql.Append(" VI_HN_TORIHIKISAKI_JOHO AS B ");
            Sql.Append(" ON A.KAISHA_CD = B.KAISHA_CD ");
            Sql.Append(" AND A.NAKAGAININ_CD = B.TORIHIKISAKI_CD ");
            Sql.Append(" WHERE");
            Sql.Append(" A.KAISHA_CD = @KAISHA_CD AND");
            Sql.Append(" B.TORIHIKISAKI_KUBUN2 = 2 AND");
            Sql.Append(" A.SHUKEI_NEN = @SHUKEI_NEN AND");
            Sql.Append(" A.NAKAGAININ_CD BETWEEN @NAKAGAININ_CD_FR AND @NAKAGAININ_CD_TO");
            Sql.Append(" GROUP BY");
            Sql.Append(" A.NAKAGAININ_CD, A.NAKAGAININ_NM, A.SHUKEI_NEN, B.JUSHO1, B.JUSHO2, B.TORIHIKISAKI_NM");
            Sql.Append(" ORDER BY");
            Sql.Append(" A.NAKAGAININ_CD");
            dpc.SetParam("@SHUKEI_NEN", SqlDbType.Decimal, 4, tmpDate.Year);
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@NAKAGAININ_CD_FR", SqlDbType.Decimal, 6, NAKAGAININ_CD_FR);
            dpc.SetParam("@NAKAGAININ_CD_TO", SqlDbType.Decimal, 6, NAKAGAININ_CD_TO);
            DataTable dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
            #endregion

            if (dtMainLoop.Rows.Count == 0)
            {
                Msg.Info("該当データがありません。");
                return false;
            }
            else
            {
                string shamei = Util.ToString(dtKaishaJoho.Rows[0]["KAISHA_NM"]);
                //string jusho = Util.ToString(dtKaishaJoho.Rows[0]["JUSHO1"]);
                string jusho = "名 護 市 城 三 丁 目 1 番 1 号";// 固定？
                string shimei = Util.ToString(dtKaishaJoho.Rows[0]["DAIHYOSHA_NM"]);

                while (dtMainLoop.Rows.Count > i)
                {
                    #region インサートテーブル
                    Sql = new StringBuilder();
                    dpc = new DbParamCollection();
                    Sql.Append("INSERT INTO PR_HN_TBL(");
                    Sql.Append("  GUID");
                    Sql.Append(" ,SORT");
                    Sql.Append(" ,ITEM01");
                    Sql.Append(" ,ITEM02");
                    Sql.Append(" ,ITEM03");
                    Sql.Append(" ,ITEM04");
                    Sql.Append(" ,ITEM05");
                    Sql.Append(" ,ITEM06");
                    Sql.Append(" ,ITEM07");
                    Sql.Append(" ,ITEM08");
                    Sql.Append(" ,ITEM09");
                    Sql.Append(" ,ITEM10");
                    Sql.Append(" ,ITEM11");
                    Sql.Append(" ,ITEM12");
                    Sql.Append(" ,ITEM13");
                    Sql.Append(" ,ITEM14");
                    Sql.Append(" ,ITEM15");
                    Sql.Append(" ,ITEM16");
                    Sql.Append(" ,ITEM17");
                    Sql.Append(" ,ITEM18");
                    Sql.Append(" ,ITEM19");
                    Sql.Append(" ,ITEM20");
                    Sql.Append(" ,ITEM21");
                    Sql.Append(" ,ITEM22");
                    Sql.Append(" ,ITEM23");
                    Sql.Append(" ,ITEM24");
                    Sql.Append(" ,ITEM25");
                    Sql.Append(" ,ITEM26");
                    Sql.Append(" ,ITEM27");
                    Sql.Append(" ,ITEM28");
                    Sql.Append(" ,ITEM29");
                    Sql.Append(" ,ITEM30");
                    Sql.Append(" ,ITEM31");
                    Sql.Append(" ,ITEM32");
                    Sql.Append(" ,ITEM33");
                    Sql.Append(" ,ITEM34");
                    Sql.Append(" ,ITEM35");
                    Sql.Append(" ,ITEM36");
                    Sql.Append(" ,ITEM37");
                    Sql.Append(" ,ITEM38");
                    Sql.Append(" ,ITEM39");
                    Sql.Append(" ,ITEM40");
                    Sql.Append(" ,ITEM41");
                    Sql.Append(" ,ITEM42");
                    Sql.Append(" ,ITEM43");
                    Sql.Append(" ,ITEM44");
                    Sql.Append(" ,ITEM45");
                    Sql.Append(" ,ITEM46");
                    Sql.Append(" ,ITEM47");
                    Sql.Append(" ,ITEM48");
                    Sql.Append(" ,ITEM49");
                    Sql.Append(" ,ITEM50");
                    Sql.Append(" ,ITEM51");
                    Sql.Append(" ,ITEM52");
                    Sql.Append(" ,ITEM53");
                    Sql.Append(" ,ITEM54");
                    Sql.Append(") ");
                    Sql.Append("VALUES(");
                    Sql.Append("  @GUID");
                    Sql.Append(" ,@SORT");
                    Sql.Append(" ,@ITEM01");
                    Sql.Append(" ,@ITEM02");
                    Sql.Append(" ,@ITEM03");
                    Sql.Append(" ,@ITEM04");
                    Sql.Append(" ,@ITEM05");
                    Sql.Append(" ,@ITEM06");
                    Sql.Append(" ,@ITEM07");
                    Sql.Append(" ,@ITEM08");
                    Sql.Append(" ,@ITEM09");
                    Sql.Append(" ,@ITEM10");
                    Sql.Append(" ,@ITEM11");
                    Sql.Append(" ,@ITEM12");
                    Sql.Append(" ,@ITEM13");
                    Sql.Append(" ,@ITEM14");
                    Sql.Append(" ,@ITEM15");
                    Sql.Append(" ,@ITEM16");
                    Sql.Append(" ,@ITEM17");
                    Sql.Append(" ,@ITEM18");
                    Sql.Append(" ,@ITEM19");
                    Sql.Append(" ,@ITEM20");
                    Sql.Append(" ,@ITEM21");
                    Sql.Append(" ,@ITEM22");
                    Sql.Append(" ,@ITEM23");
                    Sql.Append(" ,@ITEM24");
                    Sql.Append(" ,@ITEM25");
                    Sql.Append(" ,@ITEM26");
                    Sql.Append(" ,@ITEM27");
                    Sql.Append(" ,@ITEM28");
                    Sql.Append(" ,@ITEM29");
                    Sql.Append(" ,@ITEM30");
                    Sql.Append(" ,@ITEM31");
                    Sql.Append(" ,@ITEM32");
                    Sql.Append(" ,@ITEM33");
                    Sql.Append(" ,@ITEM34");
                    Sql.Append(" ,@ITEM35");
                    Sql.Append(" ,@ITEM36");
                    Sql.Append(" ,@ITEM37");
                    Sql.Append(" ,@ITEM38");
                    Sql.Append(" ,@ITEM39");
                    Sql.Append(" ,@ITEM40");
                    Sql.Append(" ,@ITEM41");
                    Sql.Append(" ,@ITEM42");
                    Sql.Append(" ,@ITEM43");
                    Sql.Append(" ,@ITEM44");
                    Sql.Append(" ,@ITEM45");
                    Sql.Append(" ,@ITEM46");
                    Sql.Append(" ,@ITEM47");
                    Sql.Append(" ,@ITEM48");
                    Sql.Append(" ,@ITEM49");
                    Sql.Append(" ,@ITEM50");
                    Sql.Append(" ,@ITEM51");
                    Sql.Append(" ,@ITEM52");
                    Sql.Append(" ,@ITEM53");
                    Sql.Append(" ,@ITEM54");
                    Sql.Append(") ");
                    #endregion

                    #region データ登録
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                    dbSORT++;
                    nakagaininJusho = Util.ToString(dtMainLoop.Rows[i]["JUSHO1"]);
                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, nakagaininJusho); // 住所1
                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["TORIHIKISAKI_NM"]); // 氏名
                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, hyojiDate); // 年指定
                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["SURYO_1GATSU"], 1)); // 1月数量
                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["KINGAKU_1GATSU"])); // 1月金額
                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["SHOHIZEI_1GATSU"])); // 1月消費税
                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["ZEIKOMI_KINGAKU_1GATSU"])); // 1月買付金額
                    dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["SURYO_2GATSU"], 1)); // 2月数量
                    dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["KINGAKU_2GATSU"])); // 2月金額
                    dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["SHOHIZEI_2GATSU"])); // 2月消費税
                    dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["ZEIKOMI_KINGAKU_2GATSU"])); // 2月買付金額
                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["SURYO_3GATSU"], 1)); // 3月数量
                    dpc.SetParam("@ITEM13", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["KINGAKU_3GATSU"])); // 3月金額
                    dpc.SetParam("@ITEM14", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["SHOHIZEI_3GATSU"])); // 3月消費税
                    dpc.SetParam("@ITEM15", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["ZEIKOMI_KINGAKU_3GATSU"])); // 3月買付金額
                    dpc.SetParam("@ITEM16", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["SURYO_4GATSU"], 1)); // 4月数量
                    dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["KINGAKU_4GATSU"])); // 4月金額
                    dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["SHOHIZEI_4GATSU"])); // 4月消費税
                    dpc.SetParam("@ITEM19", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["ZEIKOMI_KINGAKU_4GATSU"])); // 4月買付金額
                    dpc.SetParam("@ITEM20", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["SURYO_5GATSU"], 1)); // 5月数量
                    dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["KINGAKU_5GATSU"])); // 5月金額
                    dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["SHOHIZEI_5GATSU"])); // 5月消費税
                    dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["ZEIKOMI_KINGAKU_5GATSU"])); // 5買付金額
                    dpc.SetParam("@ITEM24", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["SURYO_6GATSU"], 1)); // 6月数量
                    dpc.SetParam("@ITEM25", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["KINGAKU_6GATSU"])); // 6月金額
                    dpc.SetParam("@ITEM26", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["SHOHIZEI_6GATSU"])); // 6月消費税
                    dpc.SetParam("@ITEM27", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["ZEIKOMI_KINGAKU_6GATSU"])); // 6月買付金額
                    dpc.SetParam("@ITEM28", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["SURYO_7GATSU"], 1)); // 7月数量
                    dpc.SetParam("@ITEM29", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["KINGAKU_7GATSU"])); // 7月金額
                    dpc.SetParam("@ITEM30", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["SHOHIZEI_7GATSU"])); // 7月消費税
                    dpc.SetParam("@ITEM31", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["ZEIKOMI_KINGAKU_7GATSU"])); // 7月買付金額
                    dpc.SetParam("@ITEM32", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["SURYO_8GATSU"], 1)); // 8月数量
                    dpc.SetParam("@ITEM33", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["KINGAKU_8GATSU"])); // 8月金額
                    dpc.SetParam("@ITEM34", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["SHOHIZEI_8GATSU"])); // 8月消費税
                    dpc.SetParam("@ITEM35", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["ZEIKOMI_KINGAKU_8GATSU"])); // 8月買付金額
                    dpc.SetParam("@ITEM36", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["SURYO_9GATSU"], 1)); // 9月数量
                    dpc.SetParam("@ITEM37", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["KINGAKU_9GATSU"])); // 9月金額
                    dpc.SetParam("@ITEM38", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["SHOHIZEI_9GATSU"])); // 9月消費税
                    dpc.SetParam("@ITEM39", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["ZEIKOMI_KINGAKU_9GATSU"])); // 9月買付金額
                    dpc.SetParam("@ITEM40", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["SURYO_10GATSU"], 1)); // 10月数量
                    dpc.SetParam("@ITEM41", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["KINGAKU_10GATSU"])); // 10月金額
                    dpc.SetParam("@ITEM42", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["SHOHIZEI_10GATSU"])); // 10月消費税
                    dpc.SetParam("@ITEM43", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["ZEIKOMI_KINGAKU_10GATSU"])); // 10月買付金額
                    dpc.SetParam("@ITEM44", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["SURYO_11GATSU"], 1)); // 11月数量
                    dpc.SetParam("@ITEM45", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["KINGAKU_11GATSU"])); // 11月金額
                    dpc.SetParam("@ITEM46", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["SHOHIZEI_11GATSU"])); // 11月消費税
                    dpc.SetParam("@ITEM47", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["ZEIKOMI_KINGAKU_11GATSU"])); // 11月買付金額
                    dpc.SetParam("@ITEM48", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["SURYO_12GATSU"], 1)); // 12月数量
                    dpc.SetParam("@ITEM49", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["KINGAKU_12GATSU"])); // 12月金額
                    dpc.SetParam("@ITEM50", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["SHOHIZEI_12GATSU"])); // 12月消費税
                    dpc.SetParam("@ITEM51", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["ZEIKOMI_KINGAKU_12GATSU"])); // 12月買付金額
                    dpc.SetParam("@ITEM52", SqlDbType.VarChar, 200, jusho); // 住所
                    dpc.SetParam("@ITEM53", SqlDbType.VarChar, 200, shamei); // 社名
                    dpc.SetParam("@ITEM54", SqlDbType.VarChar, 200, shimei); // 氏名

                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                    #endregion

                    i++;
                }
            }

            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
                "SORT",
                "PR_HN_TBL",
                "GUID = @GUID",
                dpc);

            bool dataFlag;
            if (tmpdtPR_HN_TBL.Rows.Count > 0)
            {
                dataFlag = true;
            }
            else
            {
                dataFlag = false;
            }

            return dataFlag;
        }
        #endregion
    }
}
