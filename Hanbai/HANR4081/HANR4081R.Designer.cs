﻿namespace jp.co.fsi.han.hanr4081
{
    /// <summary>
    /// HANR4081R の概要の説明です。
    /// </summary>
    partial class HANR4081R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(HANR4081R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.テキスト001 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtToday = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキストpage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.直線2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.ラベル33 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル34 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル37 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル38 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル41 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル42 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル45 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル46 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル49 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル50 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル53 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル54 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル61 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル64 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル65 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.直線123 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.テキスト002 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト003 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト004 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト005 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト006 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト007 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト008 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト009 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト010 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト011 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト012 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト013 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト014 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト015 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト028 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.直線89 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線90 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.reportHeader1 = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.reportFooter1 = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.ラベル20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.テキスト031 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト032 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト033 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト034 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト035 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト036 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト037 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト038 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト039 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト040 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト041 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト042 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト055 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.直線120 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト001)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtToday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキストpage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル54)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル61)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル64)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル65)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト002)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト003)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト004)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト005)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト006)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト007)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト008)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト009)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト010)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト011)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト012)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト013)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト014)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト015)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト028)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト031)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト032)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト033)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト034)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト035)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト036)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト037)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト038)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト039)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト040)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト041)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト042)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト055)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.テキスト001,
            this.txtToday,
            this.テキストpage,
            this.ラベル1,
            this.ラベル3,
            this.直線2,
            this.ラベル33,
            this.ラベル34,
            this.ラベル37,
            this.ラベル38,
            this.ラベル41,
            this.ラベル42,
            this.ラベル45,
            this.ラベル46,
            this.ラベル49,
            this.ラベル50,
            this.ラベル53,
            this.ラベル54,
            this.ラベル61,
            this.ラベル64,
            this.ラベル65,
            this.直線123});
            this.pageHeader.Height = 0.8661417F;
            this.pageHeader.Name = "pageHeader";
            this.pageHeader.Format += new System.EventHandler(this.pageHeader_Format);
            // 
            // テキスト001
            // 
            this.テキスト001.DataField = "ITEM01";
            this.テキスト001.Height = 0.15625F;
            this.テキスト001.Left = 0.5520833F;
            this.テキスト001.Name = "テキスト001";
            this.テキスト001.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト001.Tag = "";
            this.テキスト001.Text = "ITEM01";
            this.テキスト001.Top = 0.07291666F;
            this.テキスト001.Width = 0.9439798F;
            // 
            // txtToday
            // 
            this.txtToday.Height = 0.15625F;
            this.txtToday.Left = 10.04252F;
            this.txtToday.Name = "txtToday";
            this.txtToday.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.txtToday.Tag = "";
            this.txtToday.Text = "=Now()";
            this.txtToday.Top = 0F;
            this.txtToday.Width = 1.182978F;
            // 
            // テキストpage
            // 
            this.テキストpage.Height = 0.15625F;
            this.テキストpage.Left = 12.40625F;
            this.テキストpage.Name = "テキストpage";
            this.テキストpage.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキストpage.SummaryFunc = GrapeCity.ActiveReports.SectionReportModel.SummaryFunc.Count;
            this.テキストpage.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキストpage.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.テキストpage.Tag = "";
            this.テキストpage.Text = "=[Page]";
            this.テキストpage.Top = 0F;
            this.テキストpage.Width = 0.5579778F;
            // 
            // ラベル1
            // 
            this.ラベル1.Height = 0.15625F;
            this.ラベル1.HyperLink = null;
            this.ラベル1.Left = 0.8661418F;
            this.ラベル1.Name = "ラベル1";
            this.ラベル1.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-weight: bold; text-align:" +
    " left; ddo-char-set: 1";
            this.ラベル1.Tag = "";
            this.ラベル1.Text = "船主名称";
            this.ラベル1.Top = 0.3956693F;
            this.ラベル1.Width = 0.6928313F;
            // 
            // ラベル3
            // 
            this.ラベル3.Height = 0.15625F;
            this.ラベル3.HyperLink = null;
            this.ラベル3.Left = 3.409396F;
            this.ラベル3.Name = "ラベル3";
            this.ラベル3.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-weight: bold; text-align:" +
    " right; ddo-char-set: 1";
            this.ラベル3.Tag = "";
            this.ラベル3.Text = "1月手数料";
            this.ラベル3.Top = 0.4409449F;
            this.ラベル3.Width = 0.7677167F;
            // 
            // 直線2
            // 
            this.直線2.Height = 0F;
            this.直線2.Left = 0F;
            this.直線2.LineWeight = 0F;
            this.直線2.Name = "直線2";
            this.直線2.Tag = "";
            this.直線2.Top = 0.8503938F;
            this.直線2.Width = 13.19097F;
            this.直線2.X1 = 0F;
            this.直線2.X2 = 13.19097F;
            this.直線2.Y1 = 0.8503938F;
            this.直線2.Y2 = 0.8503938F;
            // 
            // ラベル33
            // 
            this.ラベル33.Height = 0.15625F;
            this.ラベル33.HyperLink = null;
            this.ラベル33.Left = 0.2793109F;
            this.ラベル33.Name = "ラベル33";
            this.ラベル33.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-weight: bold; text-align:" +
    " left; ddo-char-set: 1";
            this.ラベル33.Tag = "";
            this.ラベル33.Text = "船主CD";
            this.ラベル33.Top = 0.4305556F;
            this.ラベル33.Width = 0.5269883F;
            // 
            // ラベル34
            // 
            this.ラベル34.Height = 0.15625F;
            this.ラベル34.HyperLink = null;
            this.ラベル34.Left = 3.409396F;
            this.ラベル34.Name = "ラベル34";
            this.ラベル34.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-weight: bold; text-align:" +
    " right; ddo-char-set: 1";
            this.ラベル34.Tag = "";
            this.ラベル34.Text = "7月手数料";
            this.ラベル34.Top = 0.6602362F;
            this.ラベル34.Width = 0.7677167F;
            // 
            // ラベル37
            // 
            this.ラベル37.Height = 0.15625F;
            this.ラベル37.HyperLink = null;
            this.ラベル37.Left = 4.80315F;
            this.ラベル37.Name = "ラベル37";
            this.ラベル37.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-weight: bold; text-align:" +
    " right; ddo-char-set: 1";
            this.ラベル37.Tag = "";
            this.ラベル37.Text = "2月手数料";
            this.ラベル37.Top = 0.4409449F;
            this.ラベル37.Width = 0.7677167F;
            // 
            // ラベル38
            // 
            this.ラベル38.Height = 0.15625F;
            this.ラベル38.HyperLink = null;
            this.ラベル38.Left = 4.80315F;
            this.ラベル38.Name = "ラベル38";
            this.ラベル38.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-weight: bold; text-align:" +
    " right; ddo-char-set: 1";
            this.ラベル38.Tag = "";
            this.ラベル38.Text = "8月手数料";
            this.ラベル38.Top = 0.6602362F;
            this.ラベル38.Width = 0.7677167F;
            // 
            // ラベル41
            // 
            this.ラベル41.Height = 0.15625F;
            this.ラベル41.HyperLink = null;
            this.ラベル41.Left = 6.181621F;
            this.ラベル41.Name = "ラベル41";
            this.ラベル41.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-weight: bold; text-align:" +
    " right; ddo-char-set: 1";
            this.ラベル41.Tag = "";
            this.ラベル41.Text = "3月手数料";
            this.ラベル41.Top = 0.4409449F;
            this.ラベル41.Width = 0.7677167F;
            // 
            // ラベル42
            // 
            this.ラベル42.Height = 0.15625F;
            this.ラベル42.HyperLink = null;
            this.ラベル42.Left = 6.181621F;
            this.ラベル42.Name = "ラベル42";
            this.ラベル42.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-weight: bold; text-align:" +
    " right; ddo-char-set: 1";
            this.ラベル42.Tag = "";
            this.ラベル42.Text = "9月手数料";
            this.ラベル42.Top = 0.6602362F;
            this.ラベル42.Width = 0.7677167F;
            // 
            // ラベル45
            // 
            this.ラベル45.Height = 0.15625F;
            this.ラベル45.HyperLink = null;
            this.ラベル45.Left = 7.556621F;
            this.ラベル45.Name = "ラベル45";
            this.ラベル45.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-weight: bold; text-align:" +
    " right; ddo-char-set: 1";
            this.ラベル45.Tag = "";
            this.ラベル45.Text = "4月手数料";
            this.ラベル45.Top = 0.4409449F;
            this.ラベル45.Width = 0.7677167F;
            // 
            // ラベル46
            // 
            this.ラベル46.Height = 0.15625F;
            this.ラベル46.HyperLink = null;
            this.ラベル46.Left = 7.494121F;
            this.ラベル46.Name = "ラベル46";
            this.ラベル46.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-weight: bold; text-align:" +
    " right; ddo-char-set: 1";
            this.ラベル46.Tag = "";
            this.ラベル46.Text = "10月手数料";
            this.ラベル46.Top = 0.6602362F;
            this.ラベル46.Width = 0.8302167F;
            // 
            // ラベル49
            // 
            this.ラベル49.Height = 0.15625F;
            this.ラベル49.HyperLink = null;
            this.ラベル49.Left = 8.935099F;
            this.ラベル49.Name = "ラベル49";
            this.ラベル49.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-weight: bold; text-align:" +
    " right; ddo-char-set: 1";
            this.ラベル49.Tag = "";
            this.ラベル49.Text = "5月手数料";
            this.ラベル49.Top = 0.4409449F;
            this.ラベル49.Width = 0.7677167F;
            // 
            // ラベル50
            // 
            this.ラベル50.Height = 0.15625F;
            this.ラベル50.HyperLink = null;
            this.ラベル50.Left = 8.872599F;
            this.ラベル50.Name = "ラベル50";
            this.ラベル50.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-weight: bold; text-align:" +
    " right; ddo-char-set: 1";
            this.ラベル50.Tag = "";
            this.ラベル50.Text = "11月手数料";
            this.ラベル50.Top = 0.6602362F;
            this.ラベル50.Width = 0.8302167F;
            // 
            // ラベル53
            // 
            this.ラベル53.Height = 0.15625F;
            this.ラベル53.HyperLink = null;
            this.ラベル53.Left = 10.31288F;
            this.ラベル53.Name = "ラベル53";
            this.ラベル53.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-weight: bold; text-align:" +
    " right; ddo-char-set: 1";
            this.ラベル53.Tag = "";
            this.ラベル53.Text = "6月手数料";
            this.ラベル53.Top = 0.4409449F;
            this.ラベル53.Width = 0.7677167F;
            // 
            // ラベル54
            // 
            this.ラベル54.Height = 0.15625F;
            this.ラベル54.HyperLink = null;
            this.ラベル54.Left = 10.25038F;
            this.ラベル54.Name = "ラベル54";
            this.ラベル54.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-weight: bold; text-align:" +
    " right; ddo-char-set: 1";
            this.ラベル54.Tag = "";
            this.ラベル54.Text = "12月手数料";
            this.ラベル54.Top = 0.6602362F;
            this.ラベル54.Width = 0.8302167F;
            // 
            // ラベル61
            // 
            this.ラベル61.Height = 0.15625F;
            this.ラベル61.HyperLink = null;
            this.ラベル61.Left = 11.91682F;
            this.ラベル61.Name = "ラベル61";
            this.ラベル61.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-weight: bold; text-align:" +
    " center; ddo-char-set: 1";
            this.ラベル61.Tag = "";
            this.ラベル61.Text = "合計手数料";
            this.ラベル61.Top = 0.4409722F;
            this.ラベル61.Width = 0.9428313F;
            // 
            // ラベル64
            // 
            this.ラベル64.Height = 0.15625F;
            this.ラベル64.HyperLink = null;
            this.ラベル64.Left = 11.34514F;
            this.ラベル64.Name = "ラベル64";
            this.ラベル64.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-weight: normal; text-alig" +
    "n: left; ddo-char-set: 1";
            this.ラベル64.Tag = "";
            this.ラベル64.Text = "作成";
            this.ラベル64.Top = 0F;
            this.ラベル64.Width = 0.3871444F;
            // 
            // ラベル65
            // 
            this.ラベル65.Height = 0.25F;
            this.ラベル65.HyperLink = null;
            this.ラベル65.Left = 5.6875F;
            this.ラベル65.Name = "ラベル65";
            this.ラベル65.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 16pt; font-weight: bold; text-align:" +
    " left; ddo-char-set: 1";
            this.ラベル65.Tag = "";
            this.ラベル65.Text = "組合手数料年計表";
            this.ラベル65.Top = 0F;
            this.ラベル65.Width = 2.052083F;
            // 
            // 直線123
            // 
            this.直線123.Height = 0F;
            this.直線123.Left = 0F;
            this.直線123.LineWeight = 0F;
            this.直線123.Name = "直線123";
            this.直線123.Tag = "";
            this.直線123.Top = 0.3958333F;
            this.直線123.Width = 13.19097F;
            this.直線123.X1 = 0F;
            this.直線123.X2 = 13.19097F;
            this.直線123.Y1 = 0.3958333F;
            this.直線123.Y2 = 0.3958333F;
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.テキスト002,
            this.テキスト003,
            this.テキスト004,
            this.テキスト005,
            this.テキスト006,
            this.テキスト007,
            this.テキスト008,
            this.テキスト009,
            this.テキスト010,
            this.テキスト011,
            this.テキスト012,
            this.テキスト013,
            this.テキスト014,
            this.テキスト015,
            this.テキスト028,
            this.直線89,
            this.直線90});
            this.detail.Height = 0.4330709F;
            this.detail.Name = "detail";
            // 
            // テキスト002
            // 
            this.テキスト002.DataField = "ITEM02";
            this.テキスト002.Height = 0.15625F;
            this.テキスト002.Left = 0.1562988F;
            this.テキスト002.Name = "テキスト002";
            this.テキスト002.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; ddo-char-set: 128";
            this.テキスト002.Tag = "";
            this.テキスト002.Text = "ITEM02";
            this.テキスト002.Top = 0.01968504F;
            this.テキスト002.Width = 0.5771165F;
            // 
            // テキスト003
            // 
            this.テキスト003.CanGrow = false;
            this.テキスト003.DataField = "ITEM03";
            this.テキスト003.Height = 0.15625F;
            this.テキスト003.Left = 0.8661418F;
            this.テキスト003.Name = "テキスト003";
            this.テキスト003.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: left; ddo-char-set: 128";
            this.テキスト003.Tag = "";
            this.テキスト003.Text = "ITEM03";
            this.テキスト003.Top = 0.01968504F;
            this.テキスト003.Width = 2.023426F;
            // 
            // テキスト004
            // 
            this.テキスト004.DataField = "ITEM04";
            this.テキスト004.Height = 0.15625F;
            this.テキスト004.Left = 2.933415F;
            this.テキスト004.Name = "テキスト004";
            this.テキスト004.OutputFormat = resources.GetString("テキスト004.OutputFormat");
            this.テキスト004.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト004.Tag = "";
            this.テキスト004.Text = "ITEM04";
            this.テキスト004.Top = 0.01968504F;
            this.テキスト004.Width = 1.18125F;
            // 
            // テキスト005
            // 
            this.テキスト005.DataField = "ITEM05";
            this.テキスト005.Height = 0.15625F;
            this.テキスト005.Left = 4.333416F;
            this.テキスト005.Name = "テキスト005";
            this.テキスト005.OutputFormat = resources.GetString("テキスト005.OutputFormat");
            this.テキスト005.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト005.Tag = "";
            this.テキスト005.Text = "ITEM05";
            this.テキスト005.Top = 0.01968504F;
            this.テキスト005.Width = 1.18125F;
            // 
            // テキスト006
            // 
            this.テキスト006.DataField = "ITEM06";
            this.テキスト006.Height = 0.15625F;
            this.テキスト006.Left = 5.705637F;
            this.テキスト006.Name = "テキスト006";
            this.テキスト006.OutputFormat = resources.GetString("テキスト006.OutputFormat");
            this.テキスト006.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト006.Tag = "";
            this.テキスト006.Text = "ITEM06";
            this.テキスト006.Top = 0.01968504F;
            this.テキスト006.Width = 1.18125F;
            // 
            // テキスト007
            // 
            this.テキスト007.DataField = "ITEM07";
            this.テキスト007.Height = 0.15625F;
            this.テキスト007.Left = 7.080637F;
            this.テキスト007.Name = "テキスト007";
            this.テキスト007.OutputFormat = resources.GetString("テキスト007.OutputFormat");
            this.テキスト007.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト007.Tag = "";
            this.テキスト007.Text = "ITEM07";
            this.テキスト007.Top = 0.01968504F;
            this.テキスト007.Width = 1.18125F;
            // 
            // テキスト008
            // 
            this.テキスト008.DataField = "ITEM08";
            this.テキスト008.Height = 0.15625F;
            this.テキスト008.Left = 8.45911F;
            this.テキスト008.Name = "テキスト008";
            this.テキスト008.OutputFormat = resources.GetString("テキスト008.OutputFormat");
            this.テキスト008.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト008.Tag = "";
            this.テキスト008.Text = "ITEM08";
            this.テキスト008.Top = 0.01968504F;
            this.テキスト008.Width = 1.18125F;
            // 
            // テキスト009
            // 
            this.テキスト009.DataField = "ITEM09";
            this.テキスト009.Height = 0.15625F;
            this.テキスト009.Left = 9.836887F;
            this.テキスト009.Name = "テキスト009";
            this.テキスト009.OutputFormat = resources.GetString("テキスト009.OutputFormat");
            this.テキスト009.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト009.Tag = "";
            this.テキスト009.Text = "ITEM09";
            this.テキスト009.Top = 0.01968504F;
            this.テキスト009.Width = 1.18125F;
            // 
            // テキスト010
            // 
            this.テキスト010.DataField = "ITEM10";
            this.テキスト010.Height = 0.15625F;
            this.テキスト010.Left = 2.937402F;
            this.テキスト010.Name = "テキスト010";
            this.テキスト010.OutputFormat = resources.GetString("テキスト010.OutputFormat");
            this.テキスト010.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト010.Tag = "";
            this.テキスト010.Text = "ITEM10";
            this.テキスト010.Top = 0.2086614F;
            this.テキスト010.Width = 1.18125F;
            // 
            // テキスト011
            // 
            this.テキスト011.DataField = "ITEM11";
            this.テキスト011.Height = 0.15625F;
            this.テキスト011.Left = 4.337401F;
            this.テキスト011.Name = "テキスト011";
            this.テキスト011.OutputFormat = resources.GetString("テキスト011.OutputFormat");
            this.テキスト011.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト011.Tag = "";
            this.テキスト011.Text = "ITEM11";
            this.テキスト011.Top = 0.2086614F;
            this.テキスト011.Width = 1.18125F;
            // 
            // テキスト012
            // 
            this.テキスト012.DataField = "ITEM12";
            this.テキスト012.Height = 0.15625F;
            this.テキスト012.Left = 5.709623F;
            this.テキスト012.Name = "テキスト012";
            this.テキスト012.OutputFormat = resources.GetString("テキスト012.OutputFormat");
            this.テキスト012.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト012.Tag = "";
            this.テキスト012.Text = "ITEM12";
            this.テキスト012.Top = 0.2086614F;
            this.テキスト012.Width = 1.18125F;
            // 
            // テキスト013
            // 
            this.テキスト013.DataField = "ITEM13";
            this.テキスト013.Height = 0.15625F;
            this.テキスト013.Left = 7.084623F;
            this.テキスト013.Name = "テキスト013";
            this.テキスト013.OutputFormat = resources.GetString("テキスト013.OutputFormat");
            this.テキスト013.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト013.Tag = "";
            this.テキスト013.Text = "ITEM13";
            this.テキスト013.Top = 0.2086614F;
            this.テキスト013.Width = 1.18125F;
            // 
            // テキスト014
            // 
            this.テキスト014.DataField = "ITEM14";
            this.テキスト014.Height = 0.15625F;
            this.テキスト014.Left = 8.463097F;
            this.テキスト014.Name = "テキスト014";
            this.テキスト014.OutputFormat = resources.GetString("テキスト014.OutputFormat");
            this.テキスト014.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト014.Tag = "";
            this.テキスト014.Text = "ITEM14";
            this.テキスト014.Top = 0.2086614F;
            this.テキスト014.Width = 1.18125F;
            // 
            // テキスト015
            // 
            this.テキスト015.DataField = "ITEM15";
            this.テキスト015.Height = 0.15625F;
            this.テキスト015.Left = 9.840874F;
            this.テキスト015.Name = "テキスト015";
            this.テキスト015.OutputFormat = resources.GetString("テキスト015.OutputFormat");
            this.テキスト015.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト015.Tag = "";
            this.テキスト015.Text = "ITEM15";
            this.テキスト015.Top = 0.2086614F;
            this.テキスト015.Width = 1.18125F;
            // 
            // テキスト028
            // 
            this.テキスト028.DataField = "ITEM16";
            this.テキスト028.Height = 0.15625F;
            this.テキスト028.Left = 11.73228F;
            this.テキスト028.Name = "テキスト028";
            this.テキスト028.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト028.Tag = "";
            this.テキスト028.Text = "ITEM16";
            this.テキスト028.Top = 0.01968504F;
            this.テキスト028.Width = 1.199048F;
            // 
            // 直線89
            // 
            this.直線89.Height = 0F;
            this.直線89.Left = 0F;
            this.直線89.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.直線89.LineWeight = 0F;
            this.直線89.Name = "直線89";
            this.直線89.Tag = "";
            this.直線89.Top = 0.3937008F;
            this.直線89.Width = 13.19592F;
            this.直線89.X1 = 0F;
            this.直線89.X2 = 13.19592F;
            this.直線89.Y1 = 0.3937008F;
            this.直線89.Y2 = 0.3937008F;
            // 
            // 直線90
            // 
            this.直線90.Height = 0F;
            this.直線90.Left = 8.204579E-05F;
            this.直線90.LineWeight = 0F;
            this.直線90.Name = "直線90";
            this.直線90.Tag = "";
            this.直線90.Top = 0.6562501F;
            this.直線90.Width = 13.19097F;
            this.直線90.X1 = 8.204579E-05F;
            this.直線90.X2 = 13.19105F;
            this.直線90.Y1 = 0.6562501F;
            this.直線90.Y2 = 0.6562501F;
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0.6875F;
            this.pageFooter.Name = "pageFooter";
            // 
            // reportHeader1
            // 
            this.reportHeader1.Name = "reportHeader1";
            // 
            // reportFooter1
            // 
            this.reportFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.ラベル20,
            this.テキスト031,
            this.テキスト032,
            this.テキスト033,
            this.テキスト034,
            this.テキスト035,
            this.テキスト036,
            this.テキスト037,
            this.テキスト038,
            this.テキスト039,
            this.テキスト040,
            this.テキスト041,
            this.テキスト042,
            this.テキスト055,
            this.直線120});
            this.reportFooter1.Height = 0.4053479F;
            this.reportFooter1.Name = "reportFooter1";
            // 
            // ラベル20
            // 
            this.ラベル20.Height = 0.2166011F;
            this.ラベル20.HyperLink = null;
            this.ラベル20.Left = 0.414193F;
            this.ラベル20.Name = "ラベル20";
            this.ラベル20.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: bold; text-ali" +
    "gn: left; ddo-char-set: 128";
            this.ラベル20.Tag = "";
            this.ラベル20.Text = "総計";
            this.ラベル20.Top = 0.04166667F;
            this.ラベル20.Width = 0.5976181F;
            // 
            // テキスト031
            // 
            this.テキスト031.DataField = "ITEM04";
            this.テキスト031.Height = 0.15625F;
            this.テキスト031.Left = 2.945276F;
            this.テキスト031.Name = "テキスト031";
            this.テキスト031.OutputFormat = resources.GetString("テキスト031.OutputFormat");
            this.テキスト031.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト031.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト031.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト031.Tag = "";
            this.テキスト031.Text = "ITEM04";
            this.テキスト031.Top = 0.02362205F;
            this.テキスト031.Width = 1.18125F;
            // 
            // テキスト032
            // 
            this.テキスト032.DataField = "ITEM05";
            this.テキスト032.Height = 0.15625F;
            this.テキスト032.Left = 4.345276F;
            this.テキスト032.Name = "テキスト032";
            this.テキスト032.OutputFormat = resources.GetString("テキスト032.OutputFormat");
            this.テキスト032.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト032.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト032.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト032.Tag = "";
            this.テキスト032.Text = "ITEM05";
            this.テキスト032.Top = 0.02362205F;
            this.テキスト032.Width = 1.18125F;
            // 
            // テキスト033
            // 
            this.テキスト033.DataField = "ITEM06";
            this.テキスト033.Height = 0.15625F;
            this.テキスト033.Left = 5.717498F;
            this.テキスト033.Name = "テキスト033";
            this.テキスト033.OutputFormat = resources.GetString("テキスト033.OutputFormat");
            this.テキスト033.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト033.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト033.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト033.Tag = "";
            this.テキスト033.Text = "ITEM06";
            this.テキスト033.Top = 0.02362205F;
            this.テキスト033.Width = 1.18125F;
            // 
            // テキスト034
            // 
            this.テキスト034.DataField = "ITEM07";
            this.テキスト034.Height = 0.15625F;
            this.テキスト034.Left = 7.092498F;
            this.テキスト034.Name = "テキスト034";
            this.テキスト034.OutputFormat = resources.GetString("テキスト034.OutputFormat");
            this.テキスト034.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト034.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト034.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト034.Tag = "";
            this.テキスト034.Text = "ITEM07";
            this.テキスト034.Top = 0.02362205F;
            this.テキスト034.Width = 1.18125F;
            // 
            // テキスト035
            // 
            this.テキスト035.DataField = "ITEM08";
            this.テキスト035.Height = 0.15625F;
            this.テキスト035.Left = 8.470971F;
            this.テキスト035.Name = "テキスト035";
            this.テキスト035.OutputFormat = resources.GetString("テキスト035.OutputFormat");
            this.テキスト035.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト035.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト035.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト035.Tag = "";
            this.テキスト035.Text = "ITEM08";
            this.テキスト035.Top = 0.02362205F;
            this.テキスト035.Width = 1.18125F;
            // 
            // テキスト036
            // 
            this.テキスト036.DataField = "ITEM09";
            this.テキスト036.Height = 0.15625F;
            this.テキスト036.Left = 9.848748F;
            this.テキスト036.Name = "テキスト036";
            this.テキスト036.OutputFormat = resources.GetString("テキスト036.OutputFormat");
            this.テキスト036.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト036.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト036.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト036.Tag = "";
            this.テキスト036.Text = "ITEM09";
            this.テキスト036.Top = 0.02362205F;
            this.テキスト036.Width = 1.18125F;
            // 
            // テキスト037
            // 
            this.テキスト037.DataField = "ITEM10";
            this.テキスト037.Height = 0.15625F;
            this.テキスト037.Left = 2.945276F;
            this.テキスト037.Name = "テキスト037";
            this.テキスト037.OutputFormat = resources.GetString("テキスト037.OutputFormat");
            this.テキスト037.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト037.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト037.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト037.Tag = "";
            this.テキスト037.Text = "ITEM10";
            this.テキスト037.Top = 0.1956201F;
            this.テキスト037.Width = 1.18125F;
            // 
            // テキスト038
            // 
            this.テキスト038.DataField = "ITEM11";
            this.テキスト038.Height = 0.15625F;
            this.テキスト038.Left = 4.345276F;
            this.テキスト038.Name = "テキスト038";
            this.テキスト038.OutputFormat = resources.GetString("テキスト038.OutputFormat");
            this.テキスト038.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト038.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト038.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト038.Tag = "";
            this.テキスト038.Text = "ITEM11";
            this.テキスト038.Top = 0.1956201F;
            this.テキスト038.Width = 1.18125F;
            // 
            // テキスト039
            // 
            this.テキスト039.DataField = "ITEM12";
            this.テキスト039.Height = 0.15625F;
            this.テキスト039.Left = 5.717498F;
            this.テキスト039.Name = "テキスト039";
            this.テキスト039.OutputFormat = resources.GetString("テキスト039.OutputFormat");
            this.テキスト039.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト039.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト039.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト039.Tag = "";
            this.テキスト039.Text = "ITEM12";
            this.テキスト039.Top = 0.1956201F;
            this.テキスト039.Width = 1.18125F;
            // 
            // テキスト040
            // 
            this.テキスト040.DataField = "ITEM13";
            this.テキスト040.Height = 0.15625F;
            this.テキスト040.Left = 7.092498F;
            this.テキスト040.Name = "テキスト040";
            this.テキスト040.OutputFormat = resources.GetString("テキスト040.OutputFormat");
            this.テキスト040.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト040.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト040.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト040.Tag = "";
            this.テキスト040.Text = "ITEM13";
            this.テキスト040.Top = 0.1956201F;
            this.テキスト040.Width = 1.18125F;
            // 
            // テキスト041
            // 
            this.テキスト041.DataField = "ITEM14";
            this.テキスト041.Height = 0.15625F;
            this.テキスト041.Left = 8.470971F;
            this.テキスト041.Name = "テキスト041";
            this.テキスト041.OutputFormat = resources.GetString("テキスト041.OutputFormat");
            this.テキスト041.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト041.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト041.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト041.Tag = "";
            this.テキスト041.Text = "ITEM14";
            this.テキスト041.Top = 0.1956201F;
            this.テキスト041.Width = 1.18125F;
            // 
            // テキスト042
            // 
            this.テキスト042.DataField = "ITEM15";
            this.テキスト042.Height = 0.15625F;
            this.テキスト042.Left = 9.848748F;
            this.テキスト042.Name = "テキスト042";
            this.テキスト042.OutputFormat = resources.GetString("テキスト042.OutputFormat");
            this.テキスト042.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト042.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト042.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト042.Tag = "";
            this.テキスト042.Text = "ITEM15";
            this.テキスト042.Top = 0.1956201F;
            this.テキスト042.Width = 1.18125F;
            // 
            // テキスト055
            // 
            this.テキスト055.DataField = "ITEM16";
            this.テキスト055.Height = 0.15625F;
            this.テキスト055.Left = 11.65664F;
            this.テキスト055.Name = "テキスト055";
            this.テキスト055.OutputFormat = resources.GetString("テキスト055.OutputFormat");
            this.テキスト055.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト055.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト055.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.テキスト055.Tag = "";
            this.テキスト055.Text = "ITEM16";
            this.テキスト055.Top = 0.02362205F;
            this.テキスト055.Width = 1.271965F;
            // 
            // 直線120
            // 
            this.直線120.Height = 0F;
            this.直線120.Left = 0F;
            this.直線120.LineWeight = 0F;
            this.直線120.Name = "直線120";
            this.直線120.Tag = "";
            this.直線120.Top = 0.3740158F;
            this.直線120.Width = 13.19097F;
            this.直線120.X1 = 0F;
            this.直線120.X2 = 13.19097F;
            this.直線120.Y1 = 0.3740158F;
            this.直線120.Y2 = 0.3740158F;
            // 
            // HANR4081R
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.3937007F;
            this.PageSettings.Margins.Left = 0.5708662F;
            this.PageSettings.Margins.Right = 0.5708662F;
            this.PageSettings.Margins.Top = 0.3937008F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
            this.PageSettings.PaperHeight = 13.89764F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.B4;
            this.PageSettings.PaperWidth = 9.84252F;
            this.PrintWidth = 13.18898F;
            this.Sections.Add(this.reportHeader1);
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.pageFooter);
            this.Sections.Add(this.reportFooter1);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.テキスト001)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtToday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキストpage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル54)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル61)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル64)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル65)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト002)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト003)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト004)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト005)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト006)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト007)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト008)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト009)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト010)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト011)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト012)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト013)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト014)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト015)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト028)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト031)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト032)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト033)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト034)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト035)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト036)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト037)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト038)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト039)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト040)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト041)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト042)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト055)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト001;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtToday;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキストpage;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル1;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル3;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線2;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル33;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル34;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル37;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル38;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル41;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル42;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル45;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル46;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル49;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル50;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル53;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル54;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル61;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル64;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル65;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線123;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト002;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト003;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト004;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト005;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト006;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト007;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト008;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト009;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト010;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト011;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト012;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト013;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト014;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト015;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト028;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線89;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線90;
        private GrapeCity.ActiveReports.SectionReportModel.ReportHeader reportHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.ReportFooter reportFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル20;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト031;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト032;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト033;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト034;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト035;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト036;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト037;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト038;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト039;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト040;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト041;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト042;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト055;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線120;
    }
}
