﻿namespace jp.co.fsi.hn.hndb1021
{
    partial class HNDB1021
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtDateMonthFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtDateDayFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.labelDateYearFr = new System.Windows.Forms.Label();
            this.lblDateMonthFr = new System.Windows.Forms.Label();
            this.lblDateDayFr = new System.Windows.Forms.Label();
            this.lblGengoTo = new System.Windows.Forms.Label();
            this.lblGengoFr = new System.Windows.Forms.Label();
            this.lblDateDayTo = new System.Windows.Forms.Label();
            this.lblDateMonthTo = new System.Windows.Forms.Label();
            this.lblDateBet = new System.Windows.Forms.Label();
            this.labelDateYearTo = new System.Windows.Forms.Label();
            this.txtDateDayTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtDateYearTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtDateYearFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtDateMonthTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblGengo = new System.Windows.Forms.Label();
            this.lblMessage = new System.Windows.Forms.Label();
            this.lblFurikomibiNote = new System.Windows.Forms.Label();
            this.lblTekiyoNote = new System.Windows.Forms.Label();
            this.lblFurikomibiDay = new System.Windows.Forms.Label();
            this.lblFurikomibiMonth = new System.Windows.Forms.Label();
            this.lblFurikomibiYear = new System.Windows.Forms.Label();
            this.txtDay = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtFurikomibiYear = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtMonth = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtTekiyo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblFloppy = new System.Windows.Forms.Label();
            this.rdoFutsuMATOME = new System.Windows.Forms.RadioButton();
            this.rdoHonninTsumitate = new System.Windows.Forms.RadioButton();
            this.rdoFutsu = new System.Windows.Forms.RadioButton();
            this.rdoAzukarikin = new System.Windows.Forms.RadioButton();
            this.rdoTsumitate = new System.Windows.Forms.RadioButton();
            this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMizuageShishoNm = new System.Windows.Forms.Label();
            this.lblMizuageShisho = new System.Windows.Forms.Label();
            this.fsiTableLayoutPanel1 = new jp.co.fsi.common.FsiTableLayoutPanel();
            this.fsiPanel1 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel2 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel3 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel4 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel5 = new jp.co.fsi.common.FsiPanel();
            this.fsiPanel6 = new jp.co.fsi.common.FsiPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.fsiTableLayoutPanel1.SuspendLayout();
            this.fsiPanel1.SuspendLayout();
            this.fsiPanel2.SuspendLayout();
            this.fsiPanel3.SuspendLayout();
            this.fsiPanel4.SuspendLayout();
            this.fsiPanel5.SuspendLayout();
            this.fsiPanel6.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlDebug
            // 
            this.pnlDebug.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.pnlDebug.Location = new System.Drawing.Point(7, 755);
            this.pnlDebug.Size = new System.Drawing.Size(1348, 100);
            // 
            // lblTitle
            // 
            this.lblTitle.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTitle.Size = new System.Drawing.Size(1340, 31);
            this.lblTitle.TabIndex = 999;
            this.lblTitle.Text = "信用連携";
            // 
            // txtDateMonthFr
            // 
            this.txtDateMonthFr.AutoSizeFromLength = true;
            this.txtDateMonthFr.DisplayLength = null;
            this.txtDateMonthFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateMonthFr.Location = new System.Drawing.Point(169, 6);
            this.txtDateMonthFr.MaxLength = 2;
            this.txtDateMonthFr.Name = "txtDateMonthFr";
            this.txtDateMonthFr.Size = new System.Drawing.Size(20, 23);
            this.txtDateMonthFr.TabIndex = 4;
            this.txtDateMonthFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateMonthFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthFr_Validating);
            // 
            // txtDateDayFr
            // 
            this.txtDateDayFr.AutoSizeFromLength = true;
            this.txtDateDayFr.DisplayLength = null;
            this.txtDateDayFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateDayFr.Location = new System.Drawing.Point(209, 6);
            this.txtDateDayFr.MaxLength = 2;
            this.txtDateDayFr.Name = "txtDateDayFr";
            this.txtDateDayFr.Size = new System.Drawing.Size(20, 23);
            this.txtDateDayFr.TabIndex = 6;
            this.txtDateDayFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateDayFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDayFr_Validating);
            // 
            // labelDateYearFr
            // 
            this.labelDateYearFr.BackColor = System.Drawing.Color.Silver;
            this.labelDateYearFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.labelDateYearFr.Location = new System.Drawing.Point(148, 7);
            this.labelDateYearFr.Name = "labelDateYearFr";
            this.labelDateYearFr.Size = new System.Drawing.Size(18, 20);
            this.labelDateYearFr.TabIndex = 3;
            this.labelDateYearFr.Tag = "CHANGE";
            this.labelDateYearFr.Text = "年";
            this.labelDateYearFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateMonthFr
            // 
            this.lblDateMonthFr.BackColor = System.Drawing.Color.Silver;
            this.lblDateMonthFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateMonthFr.Location = new System.Drawing.Point(190, 7);
            this.lblDateMonthFr.Name = "lblDateMonthFr";
            this.lblDateMonthFr.Size = new System.Drawing.Size(18, 20);
            this.lblDateMonthFr.TabIndex = 5;
            this.lblDateMonthFr.Tag = "CHANGE";
            this.lblDateMonthFr.Text = "月";
            this.lblDateMonthFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateDayFr
            // 
            this.lblDateDayFr.BackColor = System.Drawing.Color.Silver;
            this.lblDateDayFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateDayFr.Location = new System.Drawing.Point(230, 7);
            this.lblDateDayFr.Name = "lblDateDayFr";
            this.lblDateDayFr.Size = new System.Drawing.Size(18, 20);
            this.lblDateDayFr.TabIndex = 7;
            this.lblDateDayFr.Tag = "CHANGE";
            this.lblDateDayFr.Text = "日";
            this.lblDateDayFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblGengoTo
            // 
            this.lblGengoTo.BackColor = System.Drawing.Color.LightCyan;
            this.lblGengoTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGengoTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGengoTo.Location = new System.Drawing.Point(277, 6);
            this.lblGengoTo.Name = "lblGengoTo";
            this.lblGengoTo.Size = new System.Drawing.Size(41, 24);
            this.lblGengoTo.TabIndex = 10;
            this.lblGengoTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblGengoFr
            // 
            this.lblGengoFr.BackColor = System.Drawing.Color.LightCyan;
            this.lblGengoFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGengoFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGengoFr.Location = new System.Drawing.Point(83, 6);
            this.lblGengoFr.Name = "lblGengoFr";
            this.lblGengoFr.Size = new System.Drawing.Size(41, 24);
            this.lblGengoFr.TabIndex = 1;
            this.lblGengoFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateDayTo
            // 
            this.lblDateDayTo.BackColor = System.Drawing.Color.Silver;
            this.lblDateDayTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateDayTo.Location = new System.Drawing.Point(423, 7);
            this.lblDateDayTo.Name = "lblDateDayTo";
            this.lblDateDayTo.Size = new System.Drawing.Size(18, 20);
            this.lblDateDayTo.TabIndex = 16;
            this.lblDateDayTo.Tag = "CHANGE";
            this.lblDateDayTo.Text = "日";
            this.lblDateDayTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateMonthTo
            // 
            this.lblDateMonthTo.BackColor = System.Drawing.Color.Silver;
            this.lblDateMonthTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateMonthTo.Location = new System.Drawing.Point(383, 7);
            this.lblDateMonthTo.Name = "lblDateMonthTo";
            this.lblDateMonthTo.Size = new System.Drawing.Size(18, 20);
            this.lblDateMonthTo.TabIndex = 14;
            this.lblDateMonthTo.Tag = "CHANGE";
            this.lblDateMonthTo.Text = "月";
            this.lblDateMonthTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateBet
            // 
            this.lblDateBet.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateBet.Location = new System.Drawing.Point(251, 6);
            this.lblDateBet.Name = "lblDateBet";
            this.lblDateBet.Size = new System.Drawing.Size(20, 25);
            this.lblDateBet.TabIndex = 8;
            this.lblDateBet.Tag = "CHANGE";
            this.lblDateBet.Text = "～";
            this.lblDateBet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelDateYearTo
            // 
            this.labelDateYearTo.BackColor = System.Drawing.Color.Silver;
            this.labelDateYearTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.labelDateYearTo.Location = new System.Drawing.Point(343, 7);
            this.labelDateYearTo.Name = "labelDateYearTo";
            this.labelDateYearTo.Size = new System.Drawing.Size(18, 20);
            this.labelDateYearTo.TabIndex = 12;
            this.labelDateYearTo.Tag = "CHANGE";
            this.labelDateYearTo.Text = "年";
            this.labelDateYearTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDateDayTo
            // 
            this.txtDateDayTo.AutoSizeFromLength = true;
            this.txtDateDayTo.DisplayLength = null;
            this.txtDateDayTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateDayTo.Location = new System.Drawing.Point(403, 6);
            this.txtDateDayTo.MaxLength = 2;
            this.txtDateDayTo.Name = "txtDateDayTo";
            this.txtDateDayTo.Size = new System.Drawing.Size(20, 23);
            this.txtDateDayTo.TabIndex = 15;
            this.txtDateDayTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateDayTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDayTo_Validating);
            // 
            // txtDateYearTo
            // 
            this.txtDateYearTo.AutoSizeFromLength = true;
            this.txtDateYearTo.DisplayLength = null;
            this.txtDateYearTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateYearTo.Location = new System.Drawing.Point(323, 6);
            this.txtDateYearTo.MaxLength = 2;
            this.txtDateYearTo.Name = "txtDateYearTo";
            this.txtDateYearTo.Size = new System.Drawing.Size(20, 23);
            this.txtDateYearTo.TabIndex = 11;
            this.txtDateYearTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateYearTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtYearTo_Validating);
            // 
            // txtDateYearFr
            // 
            this.txtDateYearFr.AutoSizeFromLength = true;
            this.txtDateYearFr.DisplayLength = null;
            this.txtDateYearFr.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateYearFr.Location = new System.Drawing.Point(128, 6);
            this.txtDateYearFr.MaxLength = 2;
            this.txtDateYearFr.Name = "txtDateYearFr";
            this.txtDateYearFr.Size = new System.Drawing.Size(20, 23);
            this.txtDateYearFr.TabIndex = 2;
            this.txtDateYearFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateYearFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtYearFr_Validating);
            // 
            // txtDateMonthTo
            // 
            this.txtDateMonthTo.AutoSizeFromLength = true;
            this.txtDateMonthTo.DisplayLength = null;
            this.txtDateMonthTo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateMonthTo.Location = new System.Drawing.Point(364, 6);
            this.txtDateMonthTo.MaxLength = 2;
            this.txtDateMonthTo.Name = "txtDateMonthTo";
            this.txtDateMonthTo.Size = new System.Drawing.Size(20, 23);
            this.txtDateMonthTo.TabIndex = 13;
            this.txtDateMonthTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateMonthTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthTo_Validating);
            // 
            // lblGengo
            // 
            this.lblGengo.BackColor = System.Drawing.Color.LightCyan;
            this.lblGengo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGengo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGengo.Location = new System.Drawing.Point(403, 6);
            this.lblGengo.Name = "lblGengo";
            this.lblGengo.Size = new System.Drawing.Size(41, 24);
            this.lblGengo.TabIndex = 4;
            this.lblGengo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMessage
            // 
            this.lblMessage.BackColor = System.Drawing.Color.Transparent;
            this.lblMessage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblMessage.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMessage.Location = new System.Drawing.Point(0, 0);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(924, 36);
            this.lblMessage.TabIndex = 12;
            this.lblMessage.Text = "通帳印字用の摘要メッセージと振込日を入力して下さい";
            this.lblMessage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblFurikomibiNote
            // 
            this.lblFurikomibiNote.BackColor = System.Drawing.Color.Transparent;
            this.lblFurikomibiNote.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFurikomibiNote.Location = new System.Drawing.Point(578, 8);
            this.lblFurikomibiNote.Name = "lblFurikomibiNote";
            this.lblFurikomibiNote.Size = new System.Drawing.Size(111, 20);
            this.lblFurikomibiNote.TabIndex = 11;
            this.lblFurikomibiNote.Tag = "CHANGE";
            this.lblFurikomibiNote.Text = "（和暦入力）";
            this.lblFurikomibiNote.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTekiyoNote
            // 
            this.lblTekiyoNote.BackColor = System.Drawing.Color.Transparent;
            this.lblTekiyoNote.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTekiyoNote.Location = new System.Drawing.Point(226, 8);
            this.lblTekiyoNote.Name = "lblTekiyoNote";
            this.lblTekiyoNote.Size = new System.Drawing.Size(155, 20);
            this.lblTekiyoNote.TabIndex = 2;
            this.lblTekiyoNote.Tag = "CHANGE";
            this.lblTekiyoNote.Text = "（１２文字入力）";
            this.lblTekiyoNote.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblFurikomibiDay
            // 
            this.lblFurikomibiDay.BackColor = System.Drawing.Color.Silver;
            this.lblFurikomibiDay.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFurikomibiDay.Location = new System.Drawing.Point(554, 8);
            this.lblFurikomibiDay.Name = "lblFurikomibiDay";
            this.lblFurikomibiDay.Size = new System.Drawing.Size(18, 20);
            this.lblFurikomibiDay.TabIndex = 10;
            this.lblFurikomibiDay.Tag = "CHANGE";
            this.lblFurikomibiDay.Text = "日";
            this.lblFurikomibiDay.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblFurikomibiMonth
            // 
            this.lblFurikomibiMonth.BackColor = System.Drawing.Color.Silver;
            this.lblFurikomibiMonth.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFurikomibiMonth.Location = new System.Drawing.Point(511, 8);
            this.lblFurikomibiMonth.Name = "lblFurikomibiMonth";
            this.lblFurikomibiMonth.Size = new System.Drawing.Size(18, 20);
            this.lblFurikomibiMonth.TabIndex = 8;
            this.lblFurikomibiMonth.Tag = "CHANGE";
            this.lblFurikomibiMonth.Text = "月";
            this.lblFurikomibiMonth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblFurikomibiYear
            // 
            this.lblFurikomibiYear.BackColor = System.Drawing.Color.Silver;
            this.lblFurikomibiYear.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFurikomibiYear.Location = new System.Drawing.Point(469, 8);
            this.lblFurikomibiYear.Name = "lblFurikomibiYear";
            this.lblFurikomibiYear.Size = new System.Drawing.Size(18, 20);
            this.lblFurikomibiYear.TabIndex = 6;
            this.lblFurikomibiYear.Tag = "CHANGE";
            this.lblFurikomibiYear.Text = "年";
            this.lblFurikomibiYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDay
            // 
            this.txtDay.AutoSizeFromLength = true;
            this.txtDay.DisplayLength = null;
            this.txtDay.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDay.Location = new System.Drawing.Point(532, 6);
            this.txtDay.MaxLength = 2;
            this.txtDay.Name = "txtDay";
            this.txtDay.Size = new System.Drawing.Size(20, 23);
            this.txtDay.TabIndex = 9;
            this.txtDay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDay.Validating += new System.ComponentModel.CancelEventHandler(this.txtDay_Validating);
            // 
            // txtFurikomibiYear
            // 
            this.txtFurikomibiYear.AutoSizeFromLength = true;
            this.txtFurikomibiYear.DisplayLength = null;
            this.txtFurikomibiYear.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFurikomibiYear.Location = new System.Drawing.Point(449, 6);
            this.txtFurikomibiYear.MaxLength = 2;
            this.txtFurikomibiYear.Name = "txtFurikomibiYear";
            this.txtFurikomibiYear.Size = new System.Drawing.Size(20, 23);
            this.txtFurikomibiYear.TabIndex = 5;
            this.txtFurikomibiYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFurikomibiYear.Validating += new System.ComponentModel.CancelEventHandler(this.txtYear_Validating);
            // 
            // txtMonth
            // 
            this.txtMonth.AutoSizeFromLength = true;
            this.txtMonth.DisplayLength = null;
            this.txtMonth.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMonth.Location = new System.Drawing.Point(490, 6);
            this.txtMonth.MaxLength = 2;
            this.txtMonth.Name = "txtMonth";
            this.txtMonth.Size = new System.Drawing.Size(20, 23);
            this.txtMonth.TabIndex = 7;
            this.txtMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonth.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonth_Validating);
            // 
            // txtTekiyo
            // 
            this.txtTekiyo.AutoSizeFromLength = true;
            this.txtTekiyo.DisplayLength = null;
            this.txtTekiyo.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTekiyo.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtTekiyo.Location = new System.Drawing.Point(121, 6);
            this.txtTekiyo.MaxLength = 12;
            this.txtTekiyo.Name = "txtTekiyo";
            this.txtTekiyo.Size = new System.Drawing.Size(99, 23);
            this.txtTekiyo.TabIndex = 1;
            this.txtTekiyo.Validating += new System.ComponentModel.CancelEventHandler(this.txtTekiyo_Validating);
            // 
            // lblFloppy
            // 
            this.lblFloppy.BackColor = System.Drawing.Color.Transparent;
            this.lblFloppy.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblFloppy.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFloppy.ForeColor = System.Drawing.Color.Red;
            this.lblFloppy.Location = new System.Drawing.Point(0, 0);
            this.lblFloppy.Name = "lblFloppy";
            this.lblFloppy.Size = new System.Drawing.Size(924, 41);
            this.lblFloppy.TabIndex = 4;
            this.lblFloppy.Text = "信用移行用のフロッピーをセットして下さい！！！";
            this.lblFloppy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // rdoFutsuMATOME
            // 
            this.rdoFutsuMATOME.AutoSize = true;
            this.rdoFutsuMATOME.Checked = true;
            this.rdoFutsuMATOME.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoFutsuMATOME.Location = new System.Drawing.Point(83, 8);
            this.rdoFutsuMATOME.Name = "rdoFutsuMATOME";
            this.rdoFutsuMATOME.Size = new System.Drawing.Size(106, 20);
            this.rdoFutsuMATOME.TabIndex = 1;
            this.rdoFutsuMATOME.TabStop = true;
            this.rdoFutsuMATOME.Tag = "CHANGE";
            this.rdoFutsuMATOME.Text = "振込まとめ";
            this.rdoFutsuMATOME.UseVisualStyleBackColor = true;
            // 
            // rdoHonninTsumitate
            // 
            this.rdoHonninTsumitate.AutoSize = true;
            this.rdoHonninTsumitate.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoHonninTsumitate.Location = new System.Drawing.Point(497, 8);
            this.rdoHonninTsumitate.Name = "rdoHonninTsumitate";
            this.rdoHonninTsumitate.Size = new System.Drawing.Size(122, 20);
            this.rdoHonninTsumitate.TabIndex = 5;
            this.rdoHonninTsumitate.Tag = "CHANGE";
            this.rdoHonninTsumitate.Text = "本人積立振込";
            this.rdoHonninTsumitate.UseVisualStyleBackColor = true;
            this.rdoHonninTsumitate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rdoHonninTsumitate_KeyDown);
            // 
            // rdoFutsu
            // 
            this.rdoFutsu.AutoSize = true;
            this.rdoFutsu.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoFutsu.Location = new System.Drawing.Point(194, 8);
            this.rdoFutsu.Name = "rdoFutsu";
            this.rdoFutsu.Size = new System.Drawing.Size(90, 20);
            this.rdoFutsu.TabIndex = 2;
            this.rdoFutsu.Tag = "CHANGE";
            this.rdoFutsu.Text = "普通振込";
            this.rdoFutsu.UseVisualStyleBackColor = true;
            this.rdoFutsu.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rdoFutsu_KeyDown);
            // 
            // rdoAzukarikin
            // 
            this.rdoAzukarikin.AutoSize = true;
            this.rdoAzukarikin.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoAzukarikin.Location = new System.Drawing.Point(386, 8);
            this.rdoAzukarikin.Name = "rdoAzukarikin";
            this.rdoAzukarikin.Size = new System.Drawing.Size(106, 20);
            this.rdoAzukarikin.TabIndex = 4;
            this.rdoAzukarikin.Tag = "CHANGE";
            this.rdoAzukarikin.Text = "預り金振込";
            this.rdoAzukarikin.UseVisualStyleBackColor = true;
            this.rdoAzukarikin.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rdoAzukarikin_KeyDown);
            // 
            // rdoTsumitate
            // 
            this.rdoTsumitate.AutoSize = true;
            this.rdoTsumitate.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoTsumitate.Location = new System.Drawing.Point(289, 8);
            this.rdoTsumitate.Name = "rdoTsumitate";
            this.rdoTsumitate.Size = new System.Drawing.Size(90, 20);
            this.rdoTsumitate.TabIndex = 3;
            this.rdoTsumitate.Tag = "CHANGE";
            this.rdoTsumitate.Text = "積立振込";
            this.rdoTsumitate.UseVisualStyleBackColor = true;
            this.rdoTsumitate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rdoTsumitate_KeyDown);
            // 
            // txtMizuageShishoCd
            // 
            this.txtMizuageShishoCd.AutoSizeFromLength = true;
            this.txtMizuageShishoCd.DisplayLength = null;
            this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtMizuageShishoCd.Location = new System.Drawing.Point(85, 7);
            this.txtMizuageShishoCd.MaxLength = 4;
            this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
            this.txtMizuageShishoCd.Size = new System.Drawing.Size(34, 23);
            this.txtMizuageShishoCd.TabIndex = 1;
            this.txtMizuageShishoCd.TabStop = false;
            this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
            // 
            // lblMizuageShishoNm
            // 
            this.lblMizuageShishoNm.BackColor = System.Drawing.Color.LightCyan;
            this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMizuageShishoNm.Location = new System.Drawing.Point(123, 6);
            this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
            this.lblMizuageShishoNm.Size = new System.Drawing.Size(212, 24);
            this.lblMizuageShishoNm.TabIndex = 2;
            this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMizuageShisho
            // 
            this.lblMizuageShisho.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShisho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShisho.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblMizuageShisho.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMizuageShisho.Location = new System.Drawing.Point(0, 0);
            this.lblMizuageShisho.Name = "lblMizuageShisho";
            this.lblMizuageShisho.Size = new System.Drawing.Size(924, 36);
            this.lblMizuageShisho.TabIndex = 0;
            this.lblMizuageShisho.Tag = "CHANGE";
            this.lblMizuageShisho.Text = "水揚支所";
            this.lblMizuageShisho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fsiTableLayoutPanel1
            // 
            this.fsiTableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.fsiTableLayoutPanel1.ColumnCount = 1;
            this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.fsiTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel6, 0, 5);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel5, 0, 4);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel4, 0, 3);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel3, 0, 2);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel2, 0, 1);
            this.fsiTableLayoutPanel1.Controls.Add(this.fsiPanel1, 0, 0);
            this.fsiTableLayoutPanel1.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.fsiTableLayoutPanel1.Location = new System.Drawing.Point(11, 43);
            this.fsiTableLayoutPanel1.Name = "fsiTableLayoutPanel1";
            this.fsiTableLayoutPanel1.RowCount = 6;
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.fsiTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.fsiTableLayoutPanel1.Size = new System.Drawing.Size(932, 264);
            this.fsiTableLayoutPanel1.TabIndex = 1000;
            // 
            // fsiPanel1
            // 
            this.fsiPanel1.Controls.Add(this.txtMizuageShishoCd);
            this.fsiPanel1.Controls.Add(this.lblMizuageShishoNm);
            this.fsiPanel1.Controls.Add(this.lblMizuageShisho);
            this.fsiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel1.Location = new System.Drawing.Point(4, 4);
            this.fsiPanel1.Name = "fsiPanel1";
            this.fsiPanel1.Size = new System.Drawing.Size(924, 36);
            this.fsiPanel1.TabIndex = 0;
            // 
            // fsiPanel2
            // 
            this.fsiPanel2.Controls.Add(this.rdoHonninTsumitate);
            this.fsiPanel2.Controls.Add(this.rdoFutsuMATOME);
            this.fsiPanel2.Controls.Add(this.rdoAzukarikin);
            this.fsiPanel2.Controls.Add(this.rdoFutsu);
            this.fsiPanel2.Controls.Add(this.rdoTsumitate);
            this.fsiPanel2.Controls.Add(this.label2);
            this.fsiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel2.Location = new System.Drawing.Point(4, 47);
            this.fsiPanel2.Name = "fsiPanel2";
            this.fsiPanel2.Size = new System.Drawing.Size(924, 36);
            this.fsiPanel2.TabIndex = 1;
            // 
            // fsiPanel3
            // 
            this.fsiPanel3.Controls.Add(this.lblGengoTo);
            this.fsiPanel3.Controls.Add(this.lblGengoFr);
            this.fsiPanel3.Controls.Add(this.lblDateDayTo);
            this.fsiPanel3.Controls.Add(this.txtDateMonthFr);
            this.fsiPanel3.Controls.Add(this.lblDateDayFr);
            this.fsiPanel3.Controls.Add(this.txtDateMonthTo);
            this.fsiPanel3.Controls.Add(this.lblDateMonthTo);
            this.fsiPanel3.Controls.Add(this.txtDateYearFr);
            this.fsiPanel3.Controls.Add(this.lblDateBet);
            this.fsiPanel3.Controls.Add(this.txtDateYearTo);
            this.fsiPanel3.Controls.Add(this.lblDateMonthFr);
            this.fsiPanel3.Controls.Add(this.txtDateDayFr);
            this.fsiPanel3.Controls.Add(this.labelDateYearTo);
            this.fsiPanel3.Controls.Add(this.txtDateDayTo);
            this.fsiPanel3.Controls.Add(this.labelDateYearFr);
            this.fsiPanel3.Controls.Add(this.label3);
            this.fsiPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel3.Location = new System.Drawing.Point(4, 90);
            this.fsiPanel3.Name = "fsiPanel3";
            this.fsiPanel3.Size = new System.Drawing.Size(924, 36);
            this.fsiPanel3.TabIndex = 2;
            // 
            // fsiPanel4
            // 
            this.fsiPanel4.Controls.Add(this.lblGengo);
            this.fsiPanel4.Controls.Add(this.lblFurikomibiNote);
            this.fsiPanel4.Controls.Add(this.txtTekiyo);
            this.fsiPanel4.Controls.Add(this.lblTekiyoNote);
            this.fsiPanel4.Controls.Add(this.lblFurikomibiDay);
            this.fsiPanel4.Controls.Add(this.lblFurikomibiMonth);
            this.fsiPanel4.Controls.Add(this.txtMonth);
            this.fsiPanel4.Controls.Add(this.lblFurikomibiYear);
            this.fsiPanel4.Controls.Add(this.txtFurikomibiYear);
            this.fsiPanel4.Controls.Add(this.txtDay);
            this.fsiPanel4.Controls.Add(this.label4);
            this.fsiPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel4.Location = new System.Drawing.Point(4, 133);
            this.fsiPanel4.Name = "fsiPanel4";
            this.fsiPanel4.Size = new System.Drawing.Size(924, 36);
            this.fsiPanel4.TabIndex = 3;
            // 
            // fsiPanel5
            // 
            this.fsiPanel5.Controls.Add(this.lblMessage);
            this.fsiPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel5.Location = new System.Drawing.Point(4, 176);
            this.fsiPanel5.Name = "fsiPanel5";
            this.fsiPanel5.Size = new System.Drawing.Size(924, 36);
            this.fsiPanel5.TabIndex = 4;
            // 
            // fsiPanel6
            // 
            this.fsiPanel6.Controls.Add(this.lblFloppy);
            this.fsiPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fsiPanel6.Location = new System.Drawing.Point(4, 219);
            this.fsiPanel6.Name = "fsiPanel6";
            this.fsiPanel6.Size = new System.Drawing.Size(924, 41);
            this.fsiPanel6.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Silver;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(924, 36);
            this.label2.TabIndex = 1;
            this.label2.Tag = "CHANGE";
            this.label2.Text = "作成区分";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Silver;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(924, 36);
            this.label3.TabIndex = 1;
            this.label3.Tag = "CHANGE";
            this.label3.Text = "日付範囲";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Silver;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("ＭＳ 明朝", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label4.Location = new System.Drawing.Point(0, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(924, 36);
            this.label4.TabIndex = 2;
            this.label4.Tag = "CHANGE";
            this.label4.Text = "摘要・振込日";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // HNDB1021
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1340, 678);
            this.Controls.Add(this.fsiTableLayoutPanel1);
            this.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "HNDB1021";
            this.Text = "信用連携";
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.fsiTableLayoutPanel1, 0);
            this.pnlDebug.ResumeLayout(false);
            this.fsiTableLayoutPanel1.ResumeLayout(false);
            this.fsiPanel1.ResumeLayout(false);
            this.fsiPanel1.PerformLayout();
            this.fsiPanel2.ResumeLayout(false);
            this.fsiPanel2.PerformLayout();
            this.fsiPanel3.ResumeLayout(false);
            this.fsiPanel3.PerformLayout();
            this.fsiPanel4.ResumeLayout(false);
            this.fsiPanel4.PerformLayout();
            this.fsiPanel5.ResumeLayout(false);
            this.fsiPanel6.ResumeLayout(false);
            this.ResumeLayout(false);

        }
        #endregion

        private jp.co.fsi.common.controls.FsiTextBox txtDateMonthFr;
        private jp.co.fsi.common.controls.FsiTextBox txtDateDayFr;
        private System.Windows.Forms.Label labelDateYearFr;
        private System.Windows.Forms.Label lblDateMonthFr;
        private System.Windows.Forms.Label lblDateDayFr;
        private System.Windows.Forms.Label lblDateDayTo;
        private System.Windows.Forms.Label lblDateMonthTo;
        private System.Windows.Forms.Label labelDateYearTo;
        private common.controls.FsiTextBox txtDateDayTo;
        private common.controls.FsiTextBox txtDateYearTo;
        private common.controls.FsiTextBox txtDateMonthTo;
        private System.Windows.Forms.Label lblDateBet;
        private common.controls.FsiTextBox txtDateYearFr;
        private common.controls.FsiTextBox txtTekiyo;
        private System.Windows.Forms.Label lblTekiyoNote;
        private System.Windows.Forms.Label lblFurikomibiDay;
        private System.Windows.Forms.Label lblFurikomibiMonth;
        private System.Windows.Forms.Label lblFurikomibiYear;
        private common.controls.FsiTextBox txtDay;
        private common.controls.FsiTextBox txtFurikomibiYear;
        private common.controls.FsiTextBox txtMonth;
        private System.Windows.Forms.Label lblFurikomibiNote;
        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.Label lblFloppy;
        private System.Windows.Forms.RadioButton rdoHonninTsumitate;
        private System.Windows.Forms.RadioButton rdoFutsu;
        private System.Windows.Forms.RadioButton rdoAzukarikin;
        private System.Windows.Forms.RadioButton rdoTsumitate;
        private System.Windows.Forms.Label lblGengoFr;
        private System.Windows.Forms.Label lblGengoTo;
        private System.Windows.Forms.Label lblGengo;
        private common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private System.Windows.Forms.Label lblMizuageShisho;
        private System.Windows.Forms.RadioButton rdoFutsuMATOME;
        private common.FsiTableLayoutPanel fsiTableLayoutPanel1;
        private common.FsiPanel fsiPanel6;
        private common.FsiPanel fsiPanel5;
        private common.FsiPanel fsiPanel4;
        private common.FsiPanel fsiPanel3;
        private common.FsiPanel fsiPanel2;
        private System.Windows.Forms.Label label2;
        private common.FsiPanel fsiPanel1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
    }
}
