﻿using System;
using System.Data;

using jp.co.fsi.common.report;

namespace jp.co.fsi.hn.hndb1021
{
    /// <summary>
    /// 信用連携の帳票(HNDB1021R)「信用自動振込一覧表」
    /// </summary>
    public partial class HNDB1021R : BaseReport
    {
        // 印刷日付
        private string today = "";

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="tgtData"></param>
        public HNDB1021R(DataTable tgtData)
            : base(tgtData)
        {
            // 本日日付（印刷日付）
            this.today = DateTime.Now.ToString("yyyy/MM/dd");

            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }

        /// <summary>
        /// DataInitializeイベント
        /// ワークに持っていない項目を出力する際に、非連結項目を定義して下さい。
        /// </summary>
        private void HNDB1021R_DataInitialize(object sender, EventArgs e)
        {
            // 本日日付（印刷日付）
            this.Fields.Add("today");
            this.Fields["today"].Value = this.today;
        }

    }
}
