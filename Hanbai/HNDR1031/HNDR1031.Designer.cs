﻿namespace jp.co.fsi.hn.hndr1031
{
    partial class HNDR1031
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }
        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblDateFr = new System.Windows.Forms.Label();
            this.lblDateGengoFr = new System.Windows.Forms.Label();
            this.txtDateMonthFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtDateYearFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtDateDayFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.labelDateYearFr = new System.Windows.Forms.Label();
            this.lblDateMonthFr = new System.Windows.Forms.Label();
            this.lblDateDayFr = new System.Windows.Forms.Label();
            this.gbxDate = new System.Windows.Forms.GroupBox();
            this.lblDateDayTo = new System.Windows.Forms.Label();
            this.lblDateMonthTo = new System.Windows.Forms.Label();
            this.lblDateBet = new System.Windows.Forms.Label();
            this.labelDateYearTo = new System.Windows.Forms.Label();
            this.txtDateDayTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtDateYearTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtDateMonthTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblDateGengoTo = new System.Windows.Forms.Label();
            this.lblDateTo = new System.Windows.Forms.Label();
            this.gbxHyoji = new System.Windows.Forms.GroupBox();
            this.rdoGyoshuBunrui = new System.Windows.Forms.RadioButton();
            this.rdoGyoshu = new System.Windows.Forms.RadioButton();
            this.gbxMizuageShisho = new System.Windows.Forms.GroupBox();
            this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMizuageShishoNm = new System.Windows.Forms.Label();
            this.lblMizuageShisho = new System.Windows.Forms.Label();
            this.gbxSeisanKbn = new System.Windows.Forms.GroupBox();
            this.txtSeisanKbn = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblSeisanKbnNm = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.gbxDate.SuspendLayout();
            this.gbxHyoji.SuspendLayout();
            this.gbxMizuageShisho.SuspendLayout();
            this.gbxSeisanKbn.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.TabIndex = 999;
            this.lblTitle.Text = "";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Size = new System.Drawing.Size(847, 100);
            // 
            // lblDateFr
            // 
            this.lblDateFr.BackColor = System.Drawing.Color.Silver;
            this.lblDateFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDateFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblDateFr.Location = new System.Drawing.Point(10, 33);
            this.lblDateFr.Name = "lblDateFr";
            this.lblDateFr.Size = new System.Drawing.Size(199, 26);
            this.lblDateFr.TabIndex = 1;
            // 
            // lblDateGengoFr
            // 
            this.lblDateGengoFr.BackColor = System.Drawing.Color.Silver;
            this.lblDateGengoFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDateGengoFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblDateGengoFr.Location = new System.Drawing.Point(12, 36);
            this.lblDateGengoFr.Name = "lblDateGengoFr";
            this.lblDateGengoFr.Size = new System.Drawing.Size(41, 20);
            this.lblDateGengoFr.TabIndex = 1;
            this.lblDateGengoFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDateMonthFr
            // 
            this.txtDateMonthFr.AutoSizeFromLength = false;
            this.txtDateMonthFr.DisplayLength = null;
            this.txtDateMonthFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtDateMonthFr.Location = new System.Drawing.Point(105, 35);
            this.txtDateMonthFr.MaxLength = 2;
            this.txtDateMonthFr.Name = "txtDateMonthFr";
            this.txtDateMonthFr.Size = new System.Drawing.Size(30, 20);
            this.txtDateMonthFr.TabIndex = 3;
            this.txtDateMonthFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateMonthFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateMonthFr_Validating);
            // 
            // txtDateYearFr
            // 
            this.txtDateYearFr.AutoSizeFromLength = false;
            this.txtDateYearFr.DisplayLength = null;
            this.txtDateYearFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtDateYearFr.Location = new System.Drawing.Point(55, 35);
            this.txtDateYearFr.MaxLength = 2;
            this.txtDateYearFr.Name = "txtDateYearFr";
            this.txtDateYearFr.Size = new System.Drawing.Size(30, 20);
            this.txtDateYearFr.TabIndex = 2;
            this.txtDateYearFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateYearFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateYearFr_Validating);
            // 
            // txtDateDayFr
            // 
            this.txtDateDayFr.AutoSizeFromLength = false;
            this.txtDateDayFr.DisplayLength = null;
            this.txtDateDayFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtDateDayFr.Location = new System.Drawing.Point(155, 35);
            this.txtDateDayFr.MaxLength = 2;
            this.txtDateDayFr.Name = "txtDateDayFr";
            this.txtDateDayFr.Size = new System.Drawing.Size(30, 20);
            this.txtDateDayFr.TabIndex = 4;
            this.txtDateDayFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateDayFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateDayFr_Validating);
            // 
            // labelDateYearFr
            // 
            this.labelDateYearFr.BackColor = System.Drawing.Color.Silver;
            this.labelDateYearFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.labelDateYearFr.Location = new System.Drawing.Point(87, 35);
            this.labelDateYearFr.Name = "labelDateYearFr";
            this.labelDateYearFr.Size = new System.Drawing.Size(17, 21);
            this.labelDateYearFr.TabIndex = 3;
            this.labelDateYearFr.Text = "年";
            this.labelDateYearFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateMonthFr
            // 
            this.lblDateMonthFr.BackColor = System.Drawing.Color.Silver;
            this.lblDateMonthFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblDateMonthFr.Location = new System.Drawing.Point(137, 35);
            this.lblDateMonthFr.Name = "lblDateMonthFr";
            this.lblDateMonthFr.Size = new System.Drawing.Size(15, 19);
            this.lblDateMonthFr.TabIndex = 5;
            this.lblDateMonthFr.Text = "月";
            this.lblDateMonthFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateDayFr
            // 
            this.lblDateDayFr.BackColor = System.Drawing.Color.Silver;
            this.lblDateDayFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblDateDayFr.Location = new System.Drawing.Point(187, 35);
            this.lblDateDayFr.Name = "lblDateDayFr";
            this.lblDateDayFr.Size = new System.Drawing.Size(20, 18);
            this.lblDateDayFr.TabIndex = 7;
            this.lblDateDayFr.Text = "日";
            this.lblDateDayFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // gbxDate
            // 
            this.gbxDate.Controls.Add(this.lblDateDayTo);
            this.gbxDate.Controls.Add(this.lblDateDayFr);
            this.gbxDate.Controls.Add(this.lblDateMonthTo);
            this.gbxDate.Controls.Add(this.lblDateBet);
            this.gbxDate.Controls.Add(this.lblDateMonthFr);
            this.gbxDate.Controls.Add(this.labelDateYearTo);
            this.gbxDate.Controls.Add(this.labelDateYearFr);
            this.gbxDate.Controls.Add(this.txtDateDayTo);
            this.gbxDate.Controls.Add(this.txtDateDayFr);
            this.gbxDate.Controls.Add(this.txtDateYearTo);
            this.gbxDate.Controls.Add(this.txtDateYearFr);
            this.gbxDate.Controls.Add(this.txtDateMonthTo);
            this.gbxDate.Controls.Add(this.txtDateMonthFr);
            this.gbxDate.Controls.Add(this.lblDateGengoTo);
            this.gbxDate.Controls.Add(this.lblDateTo);
            this.gbxDate.Controls.Add(this.lblDateGengoFr);
            this.gbxDate.Controls.Add(this.lblDateFr);
            this.gbxDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.gbxDate.ForeColor = System.Drawing.SystemColors.ControlText;
            this.gbxDate.Location = new System.Drawing.Point(12, 153);
            this.gbxDate.Name = "gbxDate";
            this.gbxDate.Size = new System.Drawing.Size(452, 88);
            this.gbxDate.TabIndex = 2;
            this.gbxDate.TabStop = false;
            this.gbxDate.Text = "セリ日付範囲";
            // 
            // lblDateDayTo
            // 
            this.lblDateDayTo.BackColor = System.Drawing.Color.Silver;
            this.lblDateDayTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblDateDayTo.Location = new System.Drawing.Point(416, 35);
            this.lblDateDayTo.Name = "lblDateDayTo";
            this.lblDateDayTo.Size = new System.Drawing.Size(20, 18);
            this.lblDateDayTo.TabIndex = 16;
            this.lblDateDayTo.Text = "日";
            this.lblDateDayTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateMonthTo
            // 
            this.lblDateMonthTo.BackColor = System.Drawing.Color.Silver;
            this.lblDateMonthTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblDateMonthTo.Location = new System.Drawing.Point(366, 35);
            this.lblDateMonthTo.Name = "lblDateMonthTo";
            this.lblDateMonthTo.Size = new System.Drawing.Size(15, 19);
            this.lblDateMonthTo.TabIndex = 14;
            this.lblDateMonthTo.Text = "月";
            this.lblDateMonthTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateBet
            // 
            this.lblDateBet.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblDateBet.Location = new System.Drawing.Point(213, 33);
            this.lblDateBet.Name = "lblDateBet";
            this.lblDateBet.Size = new System.Drawing.Size(20, 25);
            this.lblDateBet.TabIndex = 8;
            this.lblDateBet.Text = "～";
            this.lblDateBet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelDateYearTo
            // 
            this.labelDateYearTo.BackColor = System.Drawing.Color.Silver;
            this.labelDateYearTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.labelDateYearTo.Location = new System.Drawing.Point(316, 35);
            this.labelDateYearTo.Name = "labelDateYearTo";
            this.labelDateYearTo.Size = new System.Drawing.Size(17, 21);
            this.labelDateYearTo.TabIndex = 12;
            this.labelDateYearTo.Text = "年";
            this.labelDateYearTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDateDayTo
            // 
            this.txtDateDayTo.AutoSizeFromLength = false;
            this.txtDateDayTo.DisplayLength = null;
            this.txtDateDayTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtDateDayTo.Location = new System.Drawing.Point(384, 35);
            this.txtDateDayTo.MaxLength = 2;
            this.txtDateDayTo.Name = "txtDateDayTo";
            this.txtDateDayTo.Size = new System.Drawing.Size(30, 20);
            this.txtDateDayTo.TabIndex = 7;
            this.txtDateDayTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateDayTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateDayTo_Validating);
            // 
            // txtDateYearTo
            // 
            this.txtDateYearTo.AutoSizeFromLength = false;
            this.txtDateYearTo.DisplayLength = null;
            this.txtDateYearTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtDateYearTo.Location = new System.Drawing.Point(284, 35);
            this.txtDateYearTo.MaxLength = 2;
            this.txtDateYearTo.Name = "txtDateYearTo";
            this.txtDateYearTo.Size = new System.Drawing.Size(30, 20);
            this.txtDateYearTo.TabIndex = 5;
            this.txtDateYearTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateYearTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateYearTo_Validating);
            // 
            // txtDateMonthTo
            // 
            this.txtDateMonthTo.AutoSizeFromLength = false;
            this.txtDateMonthTo.DisplayLength = null;
            this.txtDateMonthTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtDateMonthTo.Location = new System.Drawing.Point(334, 35);
            this.txtDateMonthTo.MaxLength = 2;
            this.txtDateMonthTo.Name = "txtDateMonthTo";
            this.txtDateMonthTo.Size = new System.Drawing.Size(30, 20);
            this.txtDateMonthTo.TabIndex = 6;
            this.txtDateMonthTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateMonthTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateMonthTo_Validating);
            // 
            // lblDateGengoTo
            // 
            this.lblDateGengoTo.BackColor = System.Drawing.Color.Silver;
            this.lblDateGengoTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDateGengoTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblDateGengoTo.Location = new System.Drawing.Point(241, 36);
            this.lblDateGengoTo.Name = "lblDateGengoTo";
            this.lblDateGengoTo.Size = new System.Drawing.Size(41, 19);
            this.lblDateGengoTo.TabIndex = 10;
            this.lblDateGengoTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateTo
            // 
            this.lblDateTo.BackColor = System.Drawing.Color.Silver;
            this.lblDateTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDateTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblDateTo.Location = new System.Drawing.Point(239, 33);
            this.lblDateTo.Name = "lblDateTo";
            this.lblDateTo.Size = new System.Drawing.Size(199, 26);
            this.lblDateTo.TabIndex = 9;
            // 
            // gbxHyoji
            // 
            this.gbxHyoji.Controls.Add(this.rdoGyoshuBunrui);
            this.gbxHyoji.Controls.Add(this.rdoGyoshu);
            this.gbxHyoji.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.gbxHyoji.ForeColor = System.Drawing.SystemColors.ControlText;
            this.gbxHyoji.Location = new System.Drawing.Point(410, 57);
            this.gbxHyoji.Name = "gbxHyoji";
            this.gbxHyoji.Size = new System.Drawing.Size(105, 88);
            this.gbxHyoji.TabIndex = 1;
            this.gbxHyoji.TabStop = false;
            // 
            // rdoGyoshuBunrui
            // 
            this.rdoGyoshuBunrui.AutoSize = true;
            this.rdoGyoshuBunrui.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.rdoGyoshuBunrui.Location = new System.Drawing.Point(16, 50);
            this.rdoGyoshuBunrui.Name = "rdoGyoshuBunrui";
            this.rdoGyoshuBunrui.Size = new System.Drawing.Size(81, 17);
            this.rdoGyoshuBunrui.TabIndex = 1;
            this.rdoGyoshuBunrui.TabStop = true;
            this.rdoGyoshuBunrui.Text = "魚種分類";
            this.rdoGyoshuBunrui.UseVisualStyleBackColor = true;
            // 
            // rdoGyoshu
            // 
            this.rdoGyoshu.AutoSize = true;
            this.rdoGyoshu.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.rdoGyoshu.Location = new System.Drawing.Point(16, 21);
            this.rdoGyoshu.Name = "rdoGyoshu";
            this.rdoGyoshu.Size = new System.Drawing.Size(53, 17);
            this.rdoGyoshu.TabIndex = 0;
            this.rdoGyoshu.TabStop = true;
            this.rdoGyoshu.Text = "魚種";
            this.rdoGyoshu.UseVisualStyleBackColor = true;
            // 
            // gbxMizuageShisho
            // 
            this.gbxMizuageShisho.Controls.Add(this.txtMizuageShishoCd);
            this.gbxMizuageShisho.Controls.Add(this.lblMizuageShishoNm);
            this.gbxMizuageShisho.Controls.Add(this.lblMizuageShisho);
            this.gbxMizuageShisho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.gbxMizuageShisho.ForeColor = System.Drawing.Color.Black;
            this.gbxMizuageShisho.Location = new System.Drawing.Point(12, 57);
            this.gbxMizuageShisho.Name = "gbxMizuageShisho";
            this.gbxMizuageShisho.Size = new System.Drawing.Size(381, 77);
            this.gbxMizuageShisho.TabIndex = 0;
            this.gbxMizuageShisho.TabStop = false;
            this.gbxMizuageShisho.Text = "水揚支所";
            // 
            // txtMizuageShishoCd
            // 
            this.txtMizuageShishoCd.AutoSizeFromLength = true;
            this.txtMizuageShishoCd.DisplayLength = null;
            this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtMizuageShishoCd.Location = new System.Drawing.Point(75, 33);
            this.txtMizuageShishoCd.MaxLength = 4;
            this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
            this.txtMizuageShishoCd.Size = new System.Drawing.Size(34, 20);
            this.txtMizuageShishoCd.TabIndex = 1;
            this.txtMizuageShishoCd.TabStop = false;
            this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
            // 
            // lblMizuageShishoNm
            // 
            this.lblMizuageShishoNm.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMizuageShishoNm.Location = new System.Drawing.Point(110, 33);
            this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
            this.lblMizuageShishoNm.Size = new System.Drawing.Size(212, 20);
            this.lblMizuageShishoNm.TabIndex = 2;
            this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMizuageShisho
            // 
            this.lblMizuageShisho.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShisho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShisho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblMizuageShisho.Location = new System.Drawing.Point(10, 31);
            this.lblMizuageShisho.Name = "lblMizuageShisho";
            this.lblMizuageShisho.Size = new System.Drawing.Size(316, 25);
            this.lblMizuageShisho.TabIndex = 0;
            this.lblMizuageShisho.Text = "水揚支所";
            this.lblMizuageShisho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // gbxSeisanKbn
            // 
            this.gbxSeisanKbn.Controls.Add(this.txtSeisanKbn);
            this.gbxSeisanKbn.Controls.Add(this.lblSeisanKbnNm);
            this.gbxSeisanKbn.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxSeisanKbn.ForeColor = System.Drawing.Color.Black;
            this.gbxSeisanKbn.Location = new System.Drawing.Point(12, 261);
            this.gbxSeisanKbn.Name = "gbxSeisanKbn";
            this.gbxSeisanKbn.Size = new System.Drawing.Size(345, 69);
            this.gbxSeisanKbn.TabIndex = 1000;
            this.gbxSeisanKbn.TabStop = false;
            this.gbxSeisanKbn.Text = "精算区分";
            // 
            // txtSeisanKbn
            // 
            this.txtSeisanKbn.AutoSizeFromLength = true;
            this.txtSeisanKbn.DisplayLength = null;
            this.txtSeisanKbn.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtSeisanKbn.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtSeisanKbn.Location = new System.Drawing.Point(15, 27);
            this.txtSeisanKbn.MaxLength = 5;
            this.txtSeisanKbn.Name = "txtSeisanKbn";
            this.txtSeisanKbn.Size = new System.Drawing.Size(51, 20);
            this.txtSeisanKbn.TabIndex = 8;
            this.txtSeisanKbn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSeisanKbn.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSeisanKbn_KeyDown);
            this.txtSeisanKbn.Validating += new System.ComponentModel.CancelEventHandler(this.txtSeisanKbn_Validating);
            // 
            // lblSeisanKbnNm
            // 
            this.lblSeisanKbnNm.BackColor = System.Drawing.Color.Silver;
            this.lblSeisanKbnNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSeisanKbnNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblSeisanKbnNm.Location = new System.Drawing.Point(69, 27);
            this.lblSeisanKbnNm.Name = "lblSeisanKbnNm";
            this.lblSeisanKbnNm.Size = new System.Drawing.Size(212, 20);
            this.lblSeisanKbnNm.TabIndex = 6;
            this.lblSeisanKbnNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // HNDR1031
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 638);
            this.Controls.Add(this.gbxSeisanKbn);
            this.Controls.Add(this.gbxMizuageShisho);
            this.Controls.Add(this.gbxDate);
            this.Controls.Add(this.gbxHyoji);
            this.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "HNDR1031";
            this.Text = "";
            this.Controls.SetChildIndex(this.gbxHyoji, 0);
            this.Controls.SetChildIndex(this.gbxDate, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.gbxMizuageShisho, 0);
            this.Controls.SetChildIndex(this.gbxSeisanKbn, 0);
            this.pnlDebug.ResumeLayout(false);
            this.gbxDate.ResumeLayout(false);
            this.gbxDate.PerformLayout();
            this.gbxHyoji.ResumeLayout(false);
            this.gbxHyoji.PerformLayout();
            this.gbxMizuageShisho.ResumeLayout(false);
            this.gbxMizuageShisho.PerformLayout();
            this.gbxSeisanKbn.ResumeLayout(false);
            this.gbxSeisanKbn.PerformLayout();
            this.ResumeLayout(false);

        }
        #endregion

        private System.Windows.Forms.Label lblDateFr;
        private System.Windows.Forms.Label lblDateGengoFr;
        private jp.co.fsi.common.controls.FsiTextBox txtDateMonthFr;
        private jp.co.fsi.common.controls.FsiTextBox txtDateYearFr;
        private jp.co.fsi.common.controls.FsiTextBox txtDateDayFr;
        private System.Windows.Forms.Label labelDateYearFr;
        private System.Windows.Forms.Label lblDateMonthFr;
        private System.Windows.Forms.Label lblDateDayFr;
        private System.Windows.Forms.GroupBox gbxDate;
        private System.Windows.Forms.Label lblDateDayTo;
        private System.Windows.Forms.Label lblDateMonthTo;
        private System.Windows.Forms.Label labelDateYearTo;
        private common.controls.FsiTextBox txtDateDayTo;
        private common.controls.FsiTextBox txtDateYearTo;
        private common.controls.FsiTextBox txtDateMonthTo;
        private System.Windows.Forms.Label lblDateGengoTo;
        private System.Windows.Forms.Label lblDateTo;
        private System.Windows.Forms.Label lblDateBet;
        private System.Windows.Forms.GroupBox gbxHyoji;
        private System.Windows.Forms.RadioButton rdoGyoshuBunrui;
        private System.Windows.Forms.RadioButton rdoGyoshu;
        private System.Windows.Forms.GroupBox gbxMizuageShisho;
        private common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private System.Windows.Forms.Label lblMizuageShisho;
        private System.Windows.Forms.GroupBox gbxSeisanKbn;
        private common.controls.FsiTextBox txtSeisanKbn;
        private System.Windows.Forms.Label lblSeisanKbnNm;
    }
}
