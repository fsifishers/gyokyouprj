﻿using System.Data;

using jp.co.fsi.common.util;
using jp.co.fsi.common.report;

namespace jp.co.fsi.hn.hndr1031
{
    /// <summary>
    /// HNDR1031R の概要の説明です。
    /// </summary>
    public partial class HNDR1031R : BaseReport
    {

        public HNDR1031R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }

        // 中値
        decimal nakane;

        private void groupFooter2_Format(object sender, System.EventArgs e)
        {
            nakane = Util.Round(Util.ToDecimal(this.textBox5.Text) / Util.ToDecimal(this.textBox4.Text),0);
            textBox7.Text = Util.ToString(Util.FormatNum(nakane));
        }

        private void groupFooter1_Format(object sender, System.EventArgs e)
        {
            nakane = Util.Round(Util.ToDecimal(this.textBox13.Text) / Util.ToDecimal(this.textBox12.Text), 0);
            textBox15.Text = Util.ToString(Util.FormatNum(nakane));
        }
    }
}
