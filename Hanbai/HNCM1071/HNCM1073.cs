﻿using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.hn.hncm1071
{
    /// <summary>
    /// 控除項目設定(HNCM1073)
    /// </summary>
    public partial class HNCM1073 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// モード(コード検索)
        /// </summary>
        private const string MODE_CD_SRC = "1";

        /// <summary>
        /// 検索画面用画面タイトル
        /// </summary>
        private const string SEARCH_TITLE = "控除科目の検索";
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HNCM1073()
        {
            InitializeComponent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.InitForm();は呼び出さなくて構いません。
        /// また、このメソッド内の処理を外出しでこのクラス内にメソッド化するのは構いませんが、
        /// 原則、独自で起動時のイベント処理を実装することは禁じます。
        /// </remarks>
        protected override void InitForm()
        {
            // フォームのキャプションにラベルタイトルのtextを設定する
            this.Text = SEARCH_TITLE;
            // Escapeのみ表示
            this.ShowFButton = true;
            this.btnF1.Visible = false;
            this.btnF2.Visible = false;
            this.btnF3.Visible = false;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF6.Visible = false;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
           this.DialogResult = DialogResult.Cancel;
           base.PressEsc();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 初期表示
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HNCM1073_Shown(object sender, System.EventArgs e)
        {
            SearchData(true);
        }

        /// <summary>
        /// グリッドでのキーダウン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                ReturnVal();
            }
        }

        /// <summary>
        /// グリッドのセルダブルクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            ReturnVal();
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// データを検索する
        /// </summary>
        /// <param name="isInitial">初期処理であるかどうか</param>
        private void SearchData(bool isInitial)
        {
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder sql = new StringBuilder();
            DataTable dtResult;

            // par1が１の場合控除項目設定を表示
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                // 画面タイトルを設定
                this.Text = "控除項目設定検索";
                // データグリッドビュータイトルを設定
                this.dgvList.Columns[0].HeaderText = "控 除 項 目 設 定";

                // TB_控除区分マスタテーブルからデータを取得して表示
                dpc = new DbParamCollection();
                sql = new StringBuilder();
                sql.Append("SELECT");
                sql.Append(" KUBUN_NM ");
                sql.Append("FROM");
                sql.Append(" TB_HN_KOJO_KUBUN_MST ");
                sql.Append("WHERE");
                sql.Append(" KAISHA_CD = @KAISHA_CD");
                sql.Append(" AND KOJO_KUBUN = 1");
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                dtResult = this.Dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

                foreach (DataRow dr in dtResult.Rows)
                {
                    this.dgvList.Rows.Add(dr["KUBUN_NM"]);
                }
            }
            else
            {
                // 画面タイトルを設定
                this.Text = "控除対象仕切項目検索";
                // データグリッドビュータイトルを設定
                this.dgvList.Columns[0].HeaderText = "控 除 対 象 仕 切 項 目";

                // TB_控除区分マスタテーブルからデータを取得して表示
                dpc = new DbParamCollection();
                sql = new StringBuilder();
                sql.Append("SELECT");
                sql.Append(" KUBUN_NM ");
                sql.Append("FROM ");
                sql.Append(" TB_HN_KOJO_KUBUN_MST ");
                sql.Append("WHERE");
                sql.Append(" KAISHA_CD = @KAISHA_CD");
                sql.Append(" AND KOJO_KUBUN = 2");
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                dtResult = this.Dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

                foreach (DataRow dr in dtResult.Rows)
                {
                    this.dgvList.Rows.Add(dr["KUBUN_NM"]);
                }
            }
            
            if (dtResult.Rows.Count == 0)
            {
                Msg.Info("該当データがありません。");
                PressEsc();
            }
            // ユーザーによるソートを禁止させる
            foreach (DataGridViewColumn c in this.dgvList.Columns)
                c.SortMode = DataGridViewColumnSortMode.NotSortable;

            // フォントを設定する
            this.dgvList.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F, FontStyle.Regular);
            this.dgvList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.dgvList.DefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F);

            // 列幅を設定する
            this.dgvList.Columns[0].Width = 350;

        }

        /// <summary>
        /// 呼び出し元に戻り値を返す
        /// </summary>
        private void ReturnVal()
        {
            this.OutData = new string[1] { 
                Util.ToString(this.dgvList.SelectedRows[0].Cells["KOJO_KOMOKU_SETTEI"].Value),
                };
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        #endregion
    }
}
