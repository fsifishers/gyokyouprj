﻿namespace jp.co.fsi.hn.hncm1071
{
    partial class HNCM1072
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtSetteiCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblSetteiCd = new System.Windows.Forms.Label();
            this.pnlMain = new jp.co.fsi.common.FsiPanel();
            this.lblBumonnCd = new System.Windows.Forms.Label();
            this.lblHojoKamokuCd = new System.Windows.Forms.Label();
            this.lblKanjoKamokuCd = new System.Windows.Forms.Label();
            this.lblKamokbn = new System.Windows.Forms.Label();
            this.lblTaishakKbnn = new System.Windows.Forms.Label();
            this.txtTaishakKbn = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblJigyoKubn = new System.Windows.Forms.Label();
            this.txtJigyoKbn = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblJigyoKbn = new System.Windows.Forms.Label();
            this.lblZeiKbnn = new System.Windows.Forms.Label();
            this.txtZeiKbn = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblZeiKbn = new System.Windows.Forms.Label();
            this.txtBumonCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblBumonCd = new System.Windows.Forms.Label();
            this.txtHojoKamokCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblHojoKamokCd = new System.Windows.Forms.Label();
            this.txtKanjoKamokCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKeisanFugo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKojoTaishoShikriKomok = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKojoTaishoShikriKomok = new System.Windows.Forms.Label();
            this.lblKeisanhugo = new System.Windows.Forms.Label();
            this.lblKanjoKamokCd = new System.Windows.Forms.Label();
            this.lblTaishakKbn = new System.Windows.Forms.Label();
            this.lblKojoKomokNm = new System.Windows.Forms.Label();
            this.txtKojoKomokSettei = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKojoKomokSettei = new System.Windows.Forms.Label();
            this.txtKojoKomokNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShishoNm = new System.Windows.Forms.Label();
            this.lblShisho = new System.Windows.Forms.Label();
            this.panel1 = new jp.co.fsi.common.FsiPanel();
            this.chkKeisanKbn = new System.Windows.Forms.CheckBox();
            this.txtKomoku = new jp.co.fsi.common.controls.FsiTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtSeisanKbn = new jp.co.fsi.common.controls.FsiTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lblShzNrkHohoNm = new System.Windows.Forms.Label();
            this.txtShohizeiNyuryokuHoho = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShohizeiNyuryokuHoho = new System.Windows.Forms.Label();
            this.txtScript = new jp.co.fsi.common.controls.FsiTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtShikiriSeisanKbn = new jp.co.fsi.common.controls.FsiTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.pnlMain.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Size = new System.Drawing.Size(656, 23);
            this.lblTitle.TabIndex = 999;
            this.lblTitle.Text = "";
            this.lblTitle.Visible = false;
            // 
            // pnlDebug
            // 
            this.pnlDebug.Controls.Add(this.label6);
            this.pnlDebug.Location = new System.Drawing.Point(5, 489);
            this.pnlDebug.Size = new System.Drawing.Size(689, 100);
            this.pnlDebug.Controls.SetChildIndex(this.btnF6, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF7, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF5, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF8, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF4, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF9, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF3, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF10, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF2, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF11, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF1, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF12, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnEsc, 0);
            this.pnlDebug.Controls.SetChildIndex(this.label6, 0);
            // 
            // txtSetteiCd
            // 
            this.txtSetteiCd.AutoSizeFromLength = true;
            this.txtSetteiCd.DisplayLength = null;
            this.txtSetteiCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtSetteiCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtSetteiCd.Location = new System.Drawing.Point(134, 48);
            this.txtSetteiCd.MaxLength = 6;
            this.txtSetteiCd.Name = "txtSetteiCd";
            this.txtSetteiCd.Size = new System.Drawing.Size(48, 20);
            this.txtSetteiCd.TabIndex = 1;
            this.txtSetteiCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSetteiCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtSetteiCd_Validating);
            // 
            // lblSetteiCd
            // 
            this.lblSetteiCd.BackColor = System.Drawing.Color.Silver;
            this.lblSetteiCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSetteiCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblSetteiCd.Location = new System.Drawing.Point(12, 46);
            this.lblSetteiCd.Name = "lblSetteiCd";
            this.lblSetteiCd.Size = new System.Drawing.Size(174, 25);
            this.lblSetteiCd.TabIndex = 0;
            this.lblSetteiCd.Text = "設定コード";
            this.lblSetteiCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnlMain
            // 
            this.pnlMain.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlMain.Controls.Add(this.lblBumonnCd);
            this.pnlMain.Controls.Add(this.lblHojoKamokuCd);
            this.pnlMain.Controls.Add(this.lblKanjoKamokuCd);
            this.pnlMain.Controls.Add(this.lblKamokbn);
            this.pnlMain.Controls.Add(this.lblTaishakKbnn);
            this.pnlMain.Controls.Add(this.txtTaishakKbn);
            this.pnlMain.Controls.Add(this.lblJigyoKubn);
            this.pnlMain.Controls.Add(this.txtJigyoKbn);
            this.pnlMain.Controls.Add(this.lblJigyoKbn);
            this.pnlMain.Controls.Add(this.lblZeiKbnn);
            this.pnlMain.Controls.Add(this.txtZeiKbn);
            this.pnlMain.Controls.Add(this.lblZeiKbn);
            this.pnlMain.Controls.Add(this.txtBumonCd);
            this.pnlMain.Controls.Add(this.lblBumonCd);
            this.pnlMain.Controls.Add(this.txtHojoKamokCd);
            this.pnlMain.Controls.Add(this.lblHojoKamokCd);
            this.pnlMain.Controls.Add(this.txtKanjoKamokCd);
            this.pnlMain.Controls.Add(this.txtKeisanFugo);
            this.pnlMain.Controls.Add(this.txtKojoTaishoShikriKomok);
            this.pnlMain.Controls.Add(this.lblKojoTaishoShikriKomok);
            this.pnlMain.Controls.Add(this.lblKeisanhugo);
            this.pnlMain.Controls.Add(this.lblKanjoKamokCd);
            this.pnlMain.Controls.Add(this.lblTaishakKbn);
            this.pnlMain.Location = new System.Drawing.Point(9, 132);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(421, 232);
            this.pnlMain.TabIndex = 7;
            // 
            // lblBumonnCd
            // 
            this.lblBumonnCd.BackColor = System.Drawing.Color.Silver;
            this.lblBumonnCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBumonnCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblBumonnCd.Location = new System.Drawing.Point(182, 118);
            this.lblBumonnCd.Name = "lblBumonnCd";
            this.lblBumonnCd.Size = new System.Drawing.Size(227, 20);
            this.lblBumonnCd.TabIndex = 13;
            this.lblBumonnCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblHojoKamokuCd
            // 
            this.lblHojoKamokuCd.BackColor = System.Drawing.Color.Silver;
            this.lblHojoKamokuCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblHojoKamokuCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblHojoKamokuCd.Location = new System.Drawing.Point(200, 90);
            this.lblHojoKamokuCd.Name = "lblHojoKamokuCd";
            this.lblHojoKamokuCd.Size = new System.Drawing.Size(210, 20);
            this.lblHojoKamokuCd.TabIndex = 10;
            this.lblHojoKamokuCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKanjoKamokuCd
            // 
            this.lblKanjoKamokuCd.BackColor = System.Drawing.Color.Silver;
            this.lblKanjoKamokuCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKanjoKamokuCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblKanjoKamokuCd.Location = new System.Drawing.Point(182, 62);
            this.lblKanjoKamokuCd.Name = "lblKanjoKamokuCd";
            this.lblKanjoKamokuCd.Size = new System.Drawing.Size(227, 20);
            this.lblKanjoKamokuCd.TabIndex = 7;
            this.lblKanjoKamokuCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKamokbn
            // 
            this.lblKamokbn.BackColor = System.Drawing.Color.Silver;
            this.lblKamokbn.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblKamokbn.Location = new System.Drawing.Point(200, 34);
            this.lblKamokbn.Name = "lblKamokbn";
            this.lblKamokbn.Size = new System.Drawing.Size(210, 20);
            this.lblKamokbn.TabIndex = 4;
            this.lblKamokbn.Text = "1:正　2:負";
            this.lblKamokbn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTaishakKbnn
            // 
            this.lblTaishakKbnn.BackColor = System.Drawing.Color.Silver;
            this.lblTaishakKbnn.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTaishakKbnn.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblTaishakKbnn.Location = new System.Drawing.Point(182, 202);
            this.lblTaishakKbnn.Name = "lblTaishakKbnn";
            this.lblTaishakKbnn.Size = new System.Drawing.Size(227, 20);
            this.lblTaishakKbnn.TabIndex = 22;
            this.lblTaishakKbnn.Text = "1:借方　2:貸方";
            this.lblTaishakKbnn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTaishakKbn
            // 
            this.txtTaishakKbn.AutoSizeFromLength = true;
            this.txtTaishakKbn.DisplayLength = 2;
            this.txtTaishakKbn.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtTaishakKbn.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtTaishakKbn.Location = new System.Drawing.Point(124, 202);
            this.txtTaishakKbn.MaxLength = 3;
            this.txtTaishakKbn.Name = "txtTaishakKbn";
            this.txtTaishakKbn.Size = new System.Drawing.Size(55, 20);
            this.txtTaishakKbn.TabIndex = 21;
            this.txtTaishakKbn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTaishakKbn.Validating += new System.ComponentModel.CancelEventHandler(this.txtTaishakKbn_Validating);
            // 
            // lblJigyoKubn
            // 
            this.lblJigyoKubn.BackColor = System.Drawing.Color.Silver;
            this.lblJigyoKubn.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblJigyoKubn.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblJigyoKubn.Location = new System.Drawing.Point(182, 174);
            this.lblJigyoKubn.Name = "lblJigyoKubn";
            this.lblJigyoKubn.Size = new System.Drawing.Size(227, 20);
            this.lblJigyoKubn.TabIndex = 19;
            this.lblJigyoKubn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtJigyoKbn
            // 
            this.txtJigyoKbn.AutoSizeFromLength = true;
            this.txtJigyoKbn.DisplayLength = null;
            this.txtJigyoKbn.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtJigyoKbn.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtJigyoKbn.Location = new System.Drawing.Point(124, 174);
            this.txtJigyoKbn.MaxLength = 3;
            this.txtJigyoKbn.Name = "txtJigyoKbn";
            this.txtJigyoKbn.Size = new System.Drawing.Size(55, 20);
            this.txtJigyoKbn.TabIndex = 18;
            this.txtJigyoKbn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtJigyoKbn.Validating += new System.ComponentModel.CancelEventHandler(this.txtZigyoKbn_Validating);
            // 
            // lblJigyoKbn
            // 
            this.lblJigyoKbn.BackColor = System.Drawing.Color.Silver;
            this.lblJigyoKbn.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblJigyoKbn.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblJigyoKbn.Location = new System.Drawing.Point(3, 172);
            this.lblJigyoKbn.Name = "lblJigyoKbn";
            this.lblJigyoKbn.Size = new System.Drawing.Size(411, 25);
            this.lblJigyoKbn.TabIndex = 17;
            this.lblJigyoKbn.Text = "事　業　区　分";
            this.lblJigyoKbn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblZeiKbnn
            // 
            this.lblZeiKbnn.BackColor = System.Drawing.Color.Silver;
            this.lblZeiKbnn.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblZeiKbnn.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblZeiKbnn.Location = new System.Drawing.Point(182, 147);
            this.lblZeiKbnn.Name = "lblZeiKbnn";
            this.lblZeiKbnn.Size = new System.Drawing.Size(227, 20);
            this.lblZeiKbnn.TabIndex = 16;
            this.lblZeiKbnn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtZeiKbn
            // 
            this.txtZeiKbn.AutoSizeFromLength = true;
            this.txtZeiKbn.DisplayLength = null;
            this.txtZeiKbn.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtZeiKbn.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtZeiKbn.Location = new System.Drawing.Point(124, 146);
            this.txtZeiKbn.MaxLength = 4;
            this.txtZeiKbn.Name = "txtZeiKbn";
            this.txtZeiKbn.Size = new System.Drawing.Size(55, 20);
            this.txtZeiKbn.TabIndex = 15;
            this.txtZeiKbn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtZeiKbn.Validating += new System.ComponentModel.CancelEventHandler(this.txtZeiKbn_Validating);
            // 
            // lblZeiKbn
            // 
            this.lblZeiKbn.BackColor = System.Drawing.Color.Silver;
            this.lblZeiKbn.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblZeiKbn.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblZeiKbn.Location = new System.Drawing.Point(3, 144);
            this.lblZeiKbn.Name = "lblZeiKbn";
            this.lblZeiKbn.Size = new System.Drawing.Size(411, 25);
            this.lblZeiKbn.TabIndex = 14;
            this.lblZeiKbn.Text = "税　　区　　分";
            this.lblZeiKbn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtBumonCd
            // 
            this.txtBumonCd.AutoSizeFromLength = true;
            this.txtBumonCd.DisplayLength = null;
            this.txtBumonCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtBumonCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtBumonCd.Location = new System.Drawing.Point(124, 118);
            this.txtBumonCd.MaxLength = 7;
            this.txtBumonCd.Name = "txtBumonCd";
            this.txtBumonCd.Size = new System.Drawing.Size(55, 20);
            this.txtBumonCd.TabIndex = 12;
            this.txtBumonCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBumonCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtBumonCd_Validating);
            // 
            // lblBumonCd
            // 
            this.lblBumonCd.BackColor = System.Drawing.Color.Silver;
            this.lblBumonCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBumonCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblBumonCd.Location = new System.Drawing.Point(3, 116);
            this.lblBumonCd.Name = "lblBumonCd";
            this.lblBumonCd.Size = new System.Drawing.Size(411, 25);
            this.lblBumonCd.TabIndex = 11;
            this.lblBumonCd.Text = "部門コード";
            this.lblBumonCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtHojoKamokCd
            // 
            this.txtHojoKamokCd.AutoSizeFromLength = true;
            this.txtHojoKamokCd.DisplayLength = null;
            this.txtHojoKamokCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtHojoKamokCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtHojoKamokCd.Location = new System.Drawing.Point(124, 90);
            this.txtHojoKamokCd.MaxLength = 10;
            this.txtHojoKamokCd.Name = "txtHojoKamokCd";
            this.txtHojoKamokCd.Size = new System.Drawing.Size(76, 20);
            this.txtHojoKamokCd.TabIndex = 9;
            this.txtHojoKamokCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtHojoKamokCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtHojoKamokCd_Validating);
            // 
            // lblHojoKamokCd
            // 
            this.lblHojoKamokCd.BackColor = System.Drawing.Color.Silver;
            this.lblHojoKamokCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblHojoKamokCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblHojoKamokCd.Location = new System.Drawing.Point(3, 88);
            this.lblHojoKamokCd.Name = "lblHojoKamokCd";
            this.lblHojoKamokCd.Size = new System.Drawing.Size(411, 25);
            this.lblHojoKamokCd.TabIndex = 8;
            this.lblHojoKamokCd.Text = "補助科目コード";
            this.lblHojoKamokCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKanjoKamokCd
            // 
            this.txtKanjoKamokCd.AutoSizeFromLength = false;
            this.txtKanjoKamokCd.DisplayLength = null;
            this.txtKanjoKamokCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtKanjoKamokCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtKanjoKamokCd.Location = new System.Drawing.Point(124, 62);
            this.txtKanjoKamokCd.MaxLength = 7;
            this.txtKanjoKamokCd.Name = "txtKanjoKamokCd";
            this.txtKanjoKamokCd.Size = new System.Drawing.Size(55, 20);
            this.txtKanjoKamokCd.TabIndex = 6;
            this.txtKanjoKamokCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKanjoKamokCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtKanjoKamokCd_Validating);
            // 
            // txtKeisanFugo
            // 
            this.txtKeisanFugo.AutoSizeFromLength = false;
            this.txtKeisanFugo.DisplayLength = null;
            this.txtKeisanFugo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtKeisanFugo.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtKeisanFugo.Location = new System.Drawing.Point(124, 34);
            this.txtKeisanFugo.MaxLength = 1;
            this.txtKeisanFugo.Name = "txtKeisanFugo";
            this.txtKeisanFugo.Size = new System.Drawing.Size(55, 20);
            this.txtKeisanFugo.TabIndex = 3;
            this.txtKeisanFugo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKeisanFugo.Validating += new System.ComponentModel.CancelEventHandler(this.txtKeisanhugo_Validating);
            // 
            // txtKojoTaishoShikriKomok
            // 
            this.txtKojoTaishoShikriKomok.AutoSizeFromLength = true;
            this.txtKojoTaishoShikriKomok.DisplayLength = null;
            this.txtKojoTaishoShikriKomok.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtKojoTaishoShikriKomok.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtKojoTaishoShikriKomok.Location = new System.Drawing.Point(124, 6);
            this.txtKojoTaishoShikriKomok.MaxLength = 15;
            this.txtKojoTaishoShikriKomok.Name = "txtKojoTaishoShikriKomok";
            this.txtKojoTaishoShikriKomok.Size = new System.Drawing.Size(111, 20);
            this.txtKojoTaishoShikriKomok.TabIndex = 1;
            this.txtKojoTaishoShikriKomok.Validating += new System.ComponentModel.CancelEventHandler(this.txtKojoTaishoShikriKomok_Validating);
            // 
            // lblKojoTaishoShikriKomok
            // 
            this.lblKojoTaishoShikriKomok.BackColor = System.Drawing.Color.Silver;
            this.lblKojoTaishoShikriKomok.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKojoTaishoShikriKomok.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblKojoTaishoShikriKomok.Location = new System.Drawing.Point(3, 4);
            this.lblKojoTaishoShikriKomok.Name = "lblKojoTaishoShikriKomok";
            this.lblKojoTaishoShikriKomok.Size = new System.Drawing.Size(236, 25);
            this.lblKojoTaishoShikriKomok.TabIndex = 0;
            this.lblKojoTaishoShikriKomok.Text = "控除対象仕切項目";
            this.lblKojoTaishoShikriKomok.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKeisanhugo
            // 
            this.lblKeisanhugo.BackColor = System.Drawing.Color.Silver;
            this.lblKeisanhugo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKeisanhugo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblKeisanhugo.Location = new System.Drawing.Point(3, 32);
            this.lblKeisanhugo.Name = "lblKeisanhugo";
            this.lblKeisanhugo.Size = new System.Drawing.Size(411, 25);
            this.lblKeisanhugo.TabIndex = 2;
            this.lblKeisanhugo.Text = "計　算　符　号";
            this.lblKeisanhugo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKanjoKamokCd
            // 
            this.lblKanjoKamokCd.BackColor = System.Drawing.Color.Silver;
            this.lblKanjoKamokCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKanjoKamokCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblKanjoKamokCd.Location = new System.Drawing.Point(3, 60);
            this.lblKanjoKamokCd.Name = "lblKanjoKamokCd";
            this.lblKanjoKamokCd.Size = new System.Drawing.Size(411, 25);
            this.lblKanjoKamokCd.TabIndex = 5;
            this.lblKanjoKamokCd.Text = "勘定科目コード";
            this.lblKanjoKamokCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTaishakKbn
            // 
            this.lblTaishakKbn.BackColor = System.Drawing.Color.Silver;
            this.lblTaishakKbn.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTaishakKbn.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblTaishakKbn.Location = new System.Drawing.Point(3, 200);
            this.lblTaishakKbn.Name = "lblTaishakKbn";
            this.lblTaishakKbn.Size = new System.Drawing.Size(411, 25);
            this.lblTaishakKbn.TabIndex = 20;
            this.lblTaishakKbn.Text = "貸　借　区　分";
            this.lblTaishakKbn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKojoKomokNm
            // 
            this.lblKojoKomokNm.BackColor = System.Drawing.Color.Silver;
            this.lblKojoKomokNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKojoKomokNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblKojoKomokNm.Location = new System.Drawing.Point(12, 102);
            this.lblKojoKomokNm.Name = "lblKojoKomokNm";
            this.lblKojoKomokNm.Size = new System.Drawing.Size(396, 25);
            this.lblKojoKomokNm.TabIndex = 4;
            this.lblKojoKomokNm.Text = "控除項目名称";
            this.lblKojoKomokNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKojoKomokSettei
            // 
            this.txtKojoKomokSettei.AutoSizeFromLength = false;
            this.txtKojoKomokSettei.DisplayLength = null;
            this.txtKojoKomokSettei.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtKojoKomokSettei.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtKojoKomokSettei.Location = new System.Drawing.Point(134, 76);
            this.txtKojoKomokSettei.MaxLength = 15;
            this.txtKojoKomokSettei.Name = "txtKojoKomokSettei";
            this.txtKojoKomokSettei.Size = new System.Drawing.Size(270, 20);
            this.txtKojoKomokSettei.TabIndex = 3;
            this.txtKojoKomokSettei.Validating += new System.ComponentModel.CancelEventHandler(this.txtKojoKomokSettei_Validating);
            // 
            // lblKojoKomokSettei
            // 
            this.lblKojoKomokSettei.BackColor = System.Drawing.Color.Silver;
            this.lblKojoKomokSettei.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKojoKomokSettei.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblKojoKomokSettei.Location = new System.Drawing.Point(12, 74);
            this.lblKojoKomokSettei.Name = "lblKojoKomokSettei";
            this.lblKojoKomokSettei.Size = new System.Drawing.Size(396, 25);
            this.lblKojoKomokSettei.TabIndex = 2;
            this.lblKojoKomokSettei.Text = "控除項目設定";
            this.lblKojoKomokSettei.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKojoKomokNm
            // 
            this.txtKojoKomokNm.AutoSizeFromLength = false;
            this.txtKojoKomokNm.DisplayLength = null;
            this.txtKojoKomokNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtKojoKomokNm.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtKojoKomokNm.Location = new System.Drawing.Point(134, 104);
            this.txtKojoKomokNm.MaxLength = 15;
            this.txtKojoKomokNm.Name = "txtKojoKomokNm";
            this.txtKojoKomokNm.Size = new System.Drawing.Size(270, 20);
            this.txtKojoKomokNm.TabIndex = 5;
            this.txtKojoKomokNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtKojoKomokNm_Validating);
            // 
            // txtShishoCd
            // 
            this.txtShishoCd.AutoSizeFromLength = true;
            this.txtShishoCd.DisplayLength = null;
            this.txtShishoCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtShishoCd.Location = new System.Drawing.Point(89, 15);
            this.txtShishoCd.MaxLength = 4;
            this.txtShishoCd.Name = "txtShishoCd";
            this.txtShishoCd.Size = new System.Drawing.Size(34, 20);
            this.txtShishoCd.TabIndex = 0;
            this.txtShishoCd.TabStop = false;
            this.txtShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtShishoCd_Validating);
            // 
            // lblShishoNm
            // 
            this.lblShishoNm.BackColor = System.Drawing.Color.Silver;
            this.lblShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShishoNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShishoNm.Location = new System.Drawing.Point(124, 15);
            this.lblShishoNm.Name = "lblShishoNm";
            this.lblShishoNm.Size = new System.Drawing.Size(212, 20);
            this.lblShishoNm.TabIndex = 1002;
            this.lblShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShisho
            // 
            this.lblShisho.BackColor = System.Drawing.Color.Silver;
            this.lblShisho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShisho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblShisho.Location = new System.Drawing.Point(12, 13);
            this.lblShisho.Name = "lblShisho";
            this.lblShisho.Size = new System.Drawing.Size(328, 25);
            this.lblShisho.TabIndex = 1000;
            this.lblShisho.Text = "支　　所";
            this.lblShisho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.chkKeisanKbn);
            this.panel1.Controls.Add(this.txtKomoku);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.txtSeisanKbn);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.lblShzNrkHohoNm);
            this.panel1.Controls.Add(this.txtShohizeiNyuryokuHoho);
            this.panel1.Controls.Add(this.lblShohizeiNyuryokuHoho);
            this.panel1.Controls.Add(this.txtScript);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(9, 371);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(660, 124);
            this.panel1.TabIndex = 23;
            // 
            // chkKeisanKbn
            // 
            this.chkKeisanKbn.AutoSize = true;
            this.chkKeisanKbn.Location = new System.Drawing.Point(9, 6);
            this.chkKeisanKbn.Name = "chkKeisanKbn";
            this.chkKeisanKbn.Size = new System.Drawing.Size(68, 16);
            this.chkKeisanKbn.TabIndex = 0;
            this.chkKeisanKbn.Text = "計算有り";
            this.chkKeisanKbn.UseVisualStyleBackColor = true;
            this.chkKeisanKbn.CheckedChanged += new System.EventHandler(this.chkKeisanKbn_CheckedChanged);
            // 
            // txtKomoku
            // 
            this.txtKomoku.AutoSizeFromLength = false;
            this.txtKomoku.DisplayLength = null;
            this.txtKomoku.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtKomoku.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKomoku.Location = new System.Drawing.Point(104, 91);
            this.txtKomoku.MaxLength = 15;
            this.txtKomoku.Name = "txtKomoku";
            this.txtKomoku.Size = new System.Drawing.Size(133, 20);
            this.txtKomoku.TabIndex = 6;
            this.txtKomoku.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Silver;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label4.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.label4.Location = new System.Drawing.Point(9, 89);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(236, 25);
            this.label4.TabIndex = 5;
            this.label4.Text = "計算項目";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Silver;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.label3.Location = new System.Drawing.Point(272, 31);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(377, 25);
            this.label3.TabIndex = 35;
            this.label3.Text = "対象となる精算区分をカンマ区切りで入力してください。";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtSeisanKbn
            // 
            this.txtSeisanKbn.AutoSizeFromLength = false;
            this.txtSeisanKbn.DisplayLength = null;
            this.txtSeisanKbn.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtSeisanKbn.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtSeisanKbn.Location = new System.Drawing.Point(154, 33);
            this.txtSeisanKbn.MaxLength = 20;
            this.txtSeisanKbn.Name = "txtSeisanKbn";
            this.txtSeisanKbn.Size = new System.Drawing.Size(114, 20);
            this.txtSeisanKbn.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Silver;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.label2.Location = new System.Drawing.Point(9, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(261, 25);
            this.label2.TabIndex = 1;
            this.label2.Text = "精算処理対象精算区分";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShzNrkHohoNm
            // 
            this.lblShzNrkHohoNm.BackColor = System.Drawing.Color.Silver;
            this.lblShzNrkHohoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShzNrkHohoNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShzNrkHohoNm.Location = new System.Drawing.Point(388, 90);
            this.lblShzNrkHohoNm.Name = "lblShzNrkHohoNm";
            this.lblShzNrkHohoNm.Size = new System.Drawing.Size(223, 25);
            this.lblShzNrkHohoNm.TabIndex = 9;
            this.lblShzNrkHohoNm.Text = "税抜き入力（自動計算あり）";
            this.lblShzNrkHohoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShohizeiNyuryokuHoho
            // 
            this.txtShohizeiNyuryokuHoho.AutoSizeFromLength = false;
            this.txtShohizeiNyuryokuHoho.BackColor = System.Drawing.Color.White;
            this.txtShohizeiNyuryokuHoho.DisplayLength = null;
            this.txtShohizeiNyuryokuHoho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShohizeiNyuryokuHoho.Location = new System.Drawing.Point(368, 92);
            this.txtShohizeiNyuryokuHoho.MaxLength = 1;
            this.txtShohizeiNyuryokuHoho.Name = "txtShohizeiNyuryokuHoho";
            this.txtShohizeiNyuryokuHoho.Size = new System.Drawing.Size(15, 20);
            this.txtShohizeiNyuryokuHoho.TabIndex = 8;
            this.txtShohizeiNyuryokuHoho.Text = "2";
            this.txtShohizeiNyuryokuHoho.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblShohizeiNyuryokuHoho
            // 
            this.lblShohizeiNyuryokuHoho.BackColor = System.Drawing.Color.Silver;
            this.lblShohizeiNyuryokuHoho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShohizeiNyuryokuHoho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShohizeiNyuryokuHoho.Location = new System.Drawing.Point(265, 90);
            this.lblShohizeiNyuryokuHoho.Name = "lblShohizeiNyuryokuHoho";
            this.lblShohizeiNyuryokuHoho.Size = new System.Drawing.Size(121, 25);
            this.lblShohizeiNyuryokuHoho.TabIndex = 7;
            this.lblShohizeiNyuryokuHoho.Text = "消費税入力方法";
            this.lblShohizeiNyuryokuHoho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtScript
            // 
            this.txtScript.AutoSizeFromLength = false;
            this.txtScript.DisplayLength = null;
            this.txtScript.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtScript.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtScript.Location = new System.Drawing.Point(105, 61);
            this.txtScript.MaxLength = 100;
            this.txtScript.Name = "txtScript";
            this.txtScript.Size = new System.Drawing.Size(541, 20);
            this.txtScript.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Silver;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.label1.Location = new System.Drawing.Point(9, 59);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(646, 25);
            this.label1.TabIndex = 3;
            this.label1.Text = "計算式";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShikiriSeisanKbn
            // 
            this.txtShikiriSeisanKbn.AutoSizeFromLength = false;
            this.txtShikiriSeisanKbn.DisplayLength = null;
            this.txtShikiriSeisanKbn.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtShikiriSeisanKbn.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtShikiriSeisanKbn.Location = new System.Drawing.Point(157, 502);
            this.txtShikiriSeisanKbn.MaxLength = 20;
            this.txtShikiriSeisanKbn.Name = "txtShikiriSeisanKbn";
            this.txtShikiriSeisanKbn.Size = new System.Drawing.Size(114, 20);
            this.txtShikiriSeisanKbn.TabIndex = 1006;
            this.txtShikiriSeisanKbn.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LastControl_KeyDown);
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Silver;
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label5.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.label5.Location = new System.Drawing.Point(12, 500);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(261, 25);
            this.label5.TabIndex = 1005;
            this.label5.Text = "仕切書対象精算区分";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.Silver;
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label6.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.label6.Location = new System.Drawing.Point(270, 10);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(377, 25);
            this.label6.TabIndex = 1007;
            this.label6.Text = "対象となる精算区分をカンマ区切りで入力してください。";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // HNCM1072
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(681, 592);
            this.Controls.Add(this.txtShikiriSeisanKbn);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.txtShishoCd);
            this.Controls.Add(this.lblShishoNm);
            this.Controls.Add(this.lblShisho);
            this.Controls.Add(this.txtKojoKomokNm);
            this.Controls.Add(this.pnlMain);
            this.Controls.Add(this.txtSetteiCd);
            this.Controls.Add(this.lblSetteiCd);
            this.Controls.Add(this.txtKojoKomokSettei);
            this.Controls.Add(this.lblKojoKomokNm);
            this.Controls.Add(this.lblKojoKomokSettei);
            this.Name = "HNCM1072";
            this.ShowFButton = true;
            this.Text = "控除項目の設定";
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.lblKojoKomokSettei, 0);
            this.Controls.SetChildIndex(this.lblKojoKomokNm, 0);
            this.Controls.SetChildIndex(this.txtKojoKomokSettei, 0);
            this.Controls.SetChildIndex(this.lblSetteiCd, 0);
            this.Controls.SetChildIndex(this.txtSetteiCd, 0);
            this.Controls.SetChildIndex(this.pnlMain, 0);
            this.Controls.SetChildIndex(this.txtKojoKomokNm, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblShisho, 0);
            this.Controls.SetChildIndex(this.lblShishoNm, 0);
            this.Controls.SetChildIndex(this.txtShishoCd, 0);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.label5, 0);
            this.Controls.SetChildIndex(this.txtShikiriSeisanKbn, 0);
            this.pnlDebug.ResumeLayout(false);
            this.pnlMain.ResumeLayout(false);
            this.pnlMain.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private jp.co.fsi.common.controls.FsiTextBox txtSetteiCd;
        private System.Windows.Forms.Label lblSetteiCd;
        private jp.co.fsi.common.FsiPanel pnlMain;
        private jp.co.fsi.common.controls.FsiTextBox txtKojoKomokSettei;
        private System.Windows.Forms.Label lblKojoKomokSettei;
        private System.Windows.Forms.Label lblKojoKomokNm;
        private System.Windows.Forms.Label lblKojoTaishoShikriKomok;
        private jp.co.fsi.common.controls.FsiTextBox txtKojoTaishoShikriKomok;
        private jp.co.fsi.common.controls.FsiTextBox txtKeisanFugo;
        private System.Windows.Forms.Label lblKeisanhugo;
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoKamokCd;
        private System.Windows.Forms.Label lblKanjoKamokCd;
        private System.Windows.Forms.Label lblHojoKamokCd;
        private jp.co.fsi.common.controls.FsiTextBox txtHojoKamokCd;
        private jp.co.fsi.common.controls.FsiTextBox txtBumonCd;
        private System.Windows.Forms.Label lblBumonCd;
        private System.Windows.Forms.Label lblZeiKbnn;
        private jp.co.fsi.common.controls.FsiTextBox txtZeiKbn;
        private System.Windows.Forms.Label lblZeiKbn;
        private System.Windows.Forms.Label lblJigyoKbn;
        private jp.co.fsi.common.controls.FsiTextBox txtJigyoKbn;
        private System.Windows.Forms.Label lblJigyoKubn;
        private jp.co.fsi.common.controls.FsiTextBox txtTaishakKbn;
        private System.Windows.Forms.Label lblTaishakKbnn;
        private System.Windows.Forms.Label lblTaishakKbn;
        private System.Windows.Forms.Label lblBumonnCd;
        private System.Windows.Forms.Label lblHojoKamokuCd;
        private System.Windows.Forms.Label lblKanjoKamokuCd;
        private System.Windows.Forms.Label lblKamokbn;
        private common.controls.FsiTextBox txtKojoKomokNm;
        private common.controls.FsiTextBox txtShishoCd;
        private System.Windows.Forms.Label lblShishoNm;
        private System.Windows.Forms.Label lblShisho;
        private jp.co.fsi.common.FsiPanel panel1;
        private System.Windows.Forms.Label lblShzNrkHohoNm;
        private common.controls.FsiTextBox txtShohizeiNyuryokuHoho;
        private System.Windows.Forms.Label lblShohizeiNyuryokuHoho;
        private common.controls.FsiTextBox txtScript;
        private System.Windows.Forms.Label label1;
        private common.controls.FsiTextBox txtKomoku;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private common.controls.FsiTextBox txtSeisanKbn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox chkKeisanKbn;
        private common.controls.FsiTextBox txtShikiriSeisanKbn;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
    }
}