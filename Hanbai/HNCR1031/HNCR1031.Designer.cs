﻿namespace jp.co.fsi.hn.hncr1031
{
    partial class HNCR1031
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbxShiresakiCd = new System.Windows.Forms.GroupBox();
            this.lblGyoshuCdTo = new System.Windows.Forms.Label();
            this.lblCodeBet = new System.Windows.Forms.Label();
            this.txtGyoshuCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblGyoshuCdFr = new System.Windows.Forms.Label();
            this.txtGyoshuCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.gbxMizuageShisho = new System.Windows.Forms.GroupBox();
            this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMizuageShishoNm = new System.Windows.Forms.Label();
            this.lblMizuageShisho = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.gbxShiresakiCd.SuspendLayout();
            this.gbxMizuageShisho.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.TabIndex = 999;
            this.lblTitle.Text = "魚種マスタ一覧";
            // 
            // btnF2
            // 
            this.btnF2.Visible = false;
            // 
            // btnF3
            // 
            this.btnF3.Visible = false;
            // 
            // btnF8
            // 
            this.btnF8.Visible = false;
            // 
            // btnF9
            // 
            this.btnF9.Visible = false;
            // 
            // btnF11
            // 
            this.btnF11.Visible = false;
            // 
            // btnF10
            // 
            this.btnF10.Visible = false;
            // 
            // pnlDebug
            // 
            this.pnlDebug.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.pnlDebug.Size = new System.Drawing.Size(866, 99);
            // 
            // gbxShiresakiCd
            // 
            this.gbxShiresakiCd.Controls.Add(this.lblGyoshuCdTo);
            this.gbxShiresakiCd.Controls.Add(this.lblCodeBet);
            this.gbxShiresakiCd.Controls.Add(this.txtGyoshuCdFr);
            this.gbxShiresakiCd.Controls.Add(this.lblGyoshuCdFr);
            this.gbxShiresakiCd.Controls.Add(this.txtGyoshuCdTo);
            this.gbxShiresakiCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.gbxShiresakiCd.ForeColor = System.Drawing.Color.Black;
            this.gbxShiresakiCd.Location = new System.Drawing.Point(12, 148);
            this.gbxShiresakiCd.Name = "gbxShiresakiCd";
            this.gbxShiresakiCd.Size = new System.Drawing.Size(579, 75);
            this.gbxShiresakiCd.TabIndex = 0;
            this.gbxShiresakiCd.TabStop = false;
            this.gbxShiresakiCd.Text = "魚種CD範囲";
            // 
            // lblGyoshuCdTo
            // 
            this.lblGyoshuCdTo.BackColor = System.Drawing.Color.Silver;
            this.lblGyoshuCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGyoshuCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblGyoshuCdTo.Location = new System.Drawing.Point(349, 32);
            this.lblGyoshuCdTo.Name = "lblGyoshuCdTo";
            this.lblGyoshuCdTo.Size = new System.Drawing.Size(217, 20);
            this.lblGyoshuCdTo.TabIndex = 4;
            this.lblGyoshuCdTo.Text = "最　後";
            this.lblGyoshuCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCodeBet
            // 
            this.lblCodeBet.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblCodeBet.Location = new System.Drawing.Point(279, 31);
            this.lblCodeBet.Name = "lblCodeBet";
            this.lblCodeBet.Size = new System.Drawing.Size(18, 20);
            this.lblCodeBet.TabIndex = 2;
            this.lblCodeBet.Text = "～";
            this.lblCodeBet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtGyoshuCdFr
            // 
            this.txtGyoshuCdFr.AutoSizeFromLength = false;
            this.txtGyoshuCdFr.DisplayLength = null;
            this.txtGyoshuCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtGyoshuCdFr.Location = new System.Drawing.Point(13, 32);
            this.txtGyoshuCdFr.MaxLength = 4;
            this.txtGyoshuCdFr.Name = "txtGyoshuCdFr";
            this.txtGyoshuCdFr.Size = new System.Drawing.Size(40, 20);
            this.txtGyoshuCdFr.TabIndex = 0;
            this.txtGyoshuCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGyoshuCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtGyoshuCdFr_Validating);
            // 
            // lblGyoshuCdFr
            // 
            this.lblGyoshuCdFr.BackColor = System.Drawing.Color.Silver;
            this.lblGyoshuCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGyoshuCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblGyoshuCdFr.Location = new System.Drawing.Point(56, 32);
            this.lblGyoshuCdFr.Name = "lblGyoshuCdFr";
            this.lblGyoshuCdFr.Size = new System.Drawing.Size(217, 20);
            this.lblGyoshuCdFr.TabIndex = 1;
            this.lblGyoshuCdFr.Text = "先　頭";
            this.lblGyoshuCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtGyoshuCdTo
            // 
            this.txtGyoshuCdTo.AutoSizeFromLength = false;
            this.txtGyoshuCdTo.DisplayLength = null;
            this.txtGyoshuCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtGyoshuCdTo.Location = new System.Drawing.Point(306, 32);
            this.txtGyoshuCdTo.MaxLength = 4;
            this.txtGyoshuCdTo.Name = "txtGyoshuCdTo";
            this.txtGyoshuCdTo.Size = new System.Drawing.Size(40, 20);
            this.txtGyoshuCdTo.TabIndex = 3;
            this.txtGyoshuCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGyoshuCdTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtGyoshuCdTo_KeyDown);
            this.txtGyoshuCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtGyoshuCdTo_Validating);
            // 
            // gbxMizuageShisho
            // 
            this.gbxMizuageShisho.Controls.Add(this.txtMizuageShishoCd);
            this.gbxMizuageShisho.Controls.Add(this.lblMizuageShishoNm);
            this.gbxMizuageShisho.Controls.Add(this.lblMizuageShisho);
            this.gbxMizuageShisho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.gbxMizuageShisho.ForeColor = System.Drawing.Color.Black;
            this.gbxMizuageShisho.Location = new System.Drawing.Point(12, 50);
            this.gbxMizuageShisho.Name = "gbxMizuageShisho";
            this.gbxMizuageShisho.Size = new System.Drawing.Size(338, 77);
            this.gbxMizuageShisho.TabIndex = 1000;
            this.gbxMizuageShisho.TabStop = false;
            this.gbxMizuageShisho.Text = "支所";
            // 
            // txtMizuageShishoCd
            // 
            this.txtMizuageShishoCd.AutoSizeFromLength = true;
            this.txtMizuageShishoCd.DisplayLength = null;
            this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtMizuageShishoCd.Location = new System.Drawing.Point(53, 33);
            this.txtMizuageShishoCd.MaxLength = 4;
            this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
            this.txtMizuageShishoCd.Size = new System.Drawing.Size(34, 20);
            this.txtMizuageShishoCd.TabIndex = 1;
            this.txtMizuageShishoCd.TabStop = false;
            this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
            // 
            // lblMizuageShishoNm
            // 
            this.lblMizuageShishoNm.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMizuageShishoNm.Location = new System.Drawing.Point(88, 34);
            this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
            this.lblMizuageShishoNm.Size = new System.Drawing.Size(212, 20);
            this.lblMizuageShishoNm.TabIndex = 2;
            this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMizuageShisho
            // 
            this.lblMizuageShisho.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShisho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShisho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblMizuageShisho.Location = new System.Drawing.Point(13, 31);
            this.lblMizuageShisho.Name = "lblMizuageShisho";
            this.lblMizuageShisho.Size = new System.Drawing.Size(293, 27);
            this.lblMizuageShisho.TabIndex = 0;
            this.lblMizuageShisho.Text = "支所";
            this.lblMizuageShisho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // HNCR1031
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 638);
            this.Controls.Add(this.gbxMizuageShisho);
            this.Controls.Add(this.gbxShiresakiCd);
            this.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "HNCR1031";
            this.ShowFButton = true;
            this.Text = "ReportSample";
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.gbxShiresakiCd, 0);
            this.Controls.SetChildIndex(this.gbxMizuageShisho, 0);
            this.pnlDebug.ResumeLayout(false);
            this.gbxShiresakiCd.ResumeLayout(false);
            this.gbxShiresakiCd.PerformLayout();
            this.gbxMizuageShisho.ResumeLayout(false);
            this.gbxMizuageShisho.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbxShiresakiCd;
        private System.Windows.Forms.Label lblGyoshuCdTo;
        private System.Windows.Forms.Label lblCodeBet;
        private common.controls.FsiTextBox txtGyoshuCdFr;
        private System.Windows.Forms.Label lblGyoshuCdFr;
        private common.controls.FsiTextBox txtGyoshuCdTo;
        private System.Windows.Forms.GroupBox gbxMizuageShisho;
        private common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private System.Windows.Forms.Label lblMizuageShisho;
    }
}