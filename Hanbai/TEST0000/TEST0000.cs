﻿using System;
using System.ComponentModel;
using System.Data;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Diagnostics;
using System.IO;

namespace jp.co.fsi.han.test0000
{
    /// <summary>
    /// 動作テスト(TEST0000)
    /// </summary>
    public partial class TEST0000 : BasePgForm
    {
        #region 定数
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public TEST0000()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            Msg.Info("テストPGを叩きます。");
            // 下記、テストとして大嶺PC指定
            //Process.Start(@"\\192.168.51.217\prg\TEST9999.exe");
            // 下記、テストとして名護サーバーPC指定
            //Process.Start(@"\\192.168.40.150\SealaCube名護漁協\test\TEST9999.exe");
            Process.Start(@"\\NAGO_ERP_SV\test\TEST9999.exe");
            //C:\SealaCube名護漁協\prg
        }
        #endregion

        #region イベント
        #endregion

        #region privateメソッド
        #endregion
    }
}
