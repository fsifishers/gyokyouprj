﻿using System;
using System.Data;
using System.Drawing;
//using System.Collections;
//using System.Collections.Generic;
//using System.ComponentModel;

using jp.co.fsi.common.report;
using jp.co.fsi.common.util;

namespace jp.co.fsi.hn.hnmr1051
{
    /// <summary>
    /// HNMR1051R パヤオ別魚種別分類別水揚月報の帳票  ※実は累計出力！
    /// </summary>
    public partial class HNMR1051R : BaseReport
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="tgtData">出力対象データ</param>
        public HNMR1051R(DataTable tgtData)
            : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }

        /// <summary>
        /// DataInitializeイベント
        /// ワークに持っていない項目を出力する際に、非連結項目を定義して下さい。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HNMR1051R_DataInitialize(object sender, EventArgs e)
        {
            // 平均単価
            this.Fields.Add("Amount");
        }

        private void HNMR1051R_FetchData(object sender, FetchEventArgs eArgs)
        {
            // 平均単価の計算（式：金額÷数量）
            this.Fields["Amount"].Value = Math.Round(Util.ToDecimal(this.Fields["ITEM08"].Value) / Util.ToDecimal(this.Fields["ITEM06"].Value), MidpointRounding.AwayFromZero);
        }


    }
}
