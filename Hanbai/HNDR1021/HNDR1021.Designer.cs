﻿namespace jp.co.fsi.hn.hndr1021
{
    partial class HNDR1021
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbxSeriDate = new System.Windows.Forms.GroupBox();
            this.lblSeriDateDay = new System.Windows.Forms.Label();
            this.lblSeriDateMonth = new System.Windows.Forms.Label();
            this.lblSeriDateYear = new System.Windows.Forms.Label();
            this.txtSeriDateDay = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtSeriDateYear = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtSeriDateMonth = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblSeriDateGengo = new System.Windows.Forms.Label();
            this.lblSeriDate = new System.Windows.Forms.Label();
            this.gbxFunanushiCd = new System.Windows.Forms.GroupBox();
            this.lblFunanushiCdTo = new System.Windows.Forms.Label();
            this.lblCodeBet = new System.Windows.Forms.Label();
            this.txtFunanushiCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblFunanushiCdFr = new System.Windows.Forms.Label();
            this.txtFunanushiCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.gbxShiharaiDate = new System.Windows.Forms.GroupBox();
            this.lblShiharaiDateDay = new System.Windows.Forms.Label();
            this.lblShiharaiDateMonth = new System.Windows.Forms.Label();
            this.lblShiharaiDateYear = new System.Windows.Forms.Label();
            this.txtShiharaiDateDay = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtShiharaiDateYear = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtShiharaiDateMonth = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShiharaiDateGengo = new System.Windows.Forms.Label();
            this.lblShiharaiDate = new System.Windows.Forms.Label();
            this.gbxMizuageShisho = new System.Windows.Forms.GroupBox();
            this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMizuageShishoNm = new System.Windows.Forms.Label();
            this.lblMizuageShisho = new System.Windows.Forms.Label();
            this.gbxSeisanKbn = new System.Windows.Forms.GroupBox();
            this.txtSeisanKbn = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblSeisanKbnNm = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.gbxSeriDate.SuspendLayout();
            this.gbxFunanushiCd.SuspendLayout();
            this.gbxShiharaiDate.SuspendLayout();
            this.gbxMizuageShisho.SuspendLayout();
            this.gbxSeisanKbn.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.TabIndex = 999;
            this.lblTitle.Text = "水揚仕切書出力";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Size = new System.Drawing.Size(847, 100);
            // 
            // gbxSeriDate
            // 
            this.gbxSeriDate.Controls.Add(this.lblSeriDateDay);
            this.gbxSeriDate.Controls.Add(this.lblSeriDateMonth);
            this.gbxSeriDate.Controls.Add(this.lblSeriDateYear);
            this.gbxSeriDate.Controls.Add(this.txtSeriDateDay);
            this.gbxSeriDate.Controls.Add(this.txtSeriDateYear);
            this.gbxSeriDate.Controls.Add(this.txtSeriDateMonth);
            this.gbxSeriDate.Controls.Add(this.lblSeriDateGengo);
            this.gbxSeriDate.Controls.Add(this.lblSeriDate);
            this.gbxSeriDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.gbxSeriDate.ForeColor = System.Drawing.Color.Black;
            this.gbxSeriDate.Location = new System.Drawing.Point(12, 140);
            this.gbxSeriDate.Name = "gbxSeriDate";
            this.gbxSeriDate.Size = new System.Drawing.Size(281, 72);
            this.gbxSeriDate.TabIndex = 1;
            this.gbxSeriDate.TabStop = false;
            this.gbxSeriDate.Text = "セリ日付";
            // 
            // lblSeriDateDay
            // 
            this.lblSeriDateDay.BackColor = System.Drawing.Color.Silver;
            this.lblSeriDateDay.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblSeriDateDay.ForeColor = System.Drawing.Color.Black;
            this.lblSeriDateDay.Location = new System.Drawing.Point(213, 30);
            this.lblSeriDateDay.Name = "lblSeriDateDay";
            this.lblSeriDateDay.Size = new System.Drawing.Size(20, 18);
            this.lblSeriDateDay.TabIndex = 7;
            this.lblSeriDateDay.Text = "日";
            this.lblSeriDateDay.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSeriDateMonth
            // 
            this.lblSeriDateMonth.BackColor = System.Drawing.Color.Silver;
            this.lblSeriDateMonth.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblSeriDateMonth.ForeColor = System.Drawing.Color.Black;
            this.lblSeriDateMonth.Location = new System.Drawing.Point(158, 30);
            this.lblSeriDateMonth.Name = "lblSeriDateMonth";
            this.lblSeriDateMonth.Size = new System.Drawing.Size(15, 19);
            this.lblSeriDateMonth.TabIndex = 5;
            this.lblSeriDateMonth.Text = "月";
            this.lblSeriDateMonth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSeriDateYear
            // 
            this.lblSeriDateYear.BackColor = System.Drawing.Color.Silver;
            this.lblSeriDateYear.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblSeriDateYear.ForeColor = System.Drawing.Color.Black;
            this.lblSeriDateYear.Location = new System.Drawing.Point(105, 29);
            this.lblSeriDateYear.Name = "lblSeriDateYear";
            this.lblSeriDateYear.Size = new System.Drawing.Size(17, 21);
            this.lblSeriDateYear.TabIndex = 3;
            this.lblSeriDateYear.Text = "年";
            this.lblSeriDateYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtSeriDateDay
            // 
            this.txtSeriDateDay.AutoSizeFromLength = false;
            this.txtSeriDateDay.DisplayLength = null;
            this.txtSeriDateDay.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtSeriDateDay.ForeColor = System.Drawing.Color.Black;
            this.txtSeriDateDay.Location = new System.Drawing.Point(180, 29);
            this.txtSeriDateDay.MaxLength = 2;
            this.txtSeriDateDay.Name = "txtSeriDateDay";
            this.txtSeriDateDay.Size = new System.Drawing.Size(30, 20);
            this.txtSeriDateDay.TabIndex = 4;
            this.txtSeriDateDay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSeriDateDay.Validating += new System.ComponentModel.CancelEventHandler(this.txtSeriDateDay_Validating);
            // 
            // txtSeriDateYear
            // 
            this.txtSeriDateYear.AutoSizeFromLength = false;
            this.txtSeriDateYear.DisplayLength = null;
            this.txtSeriDateYear.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtSeriDateYear.ForeColor = System.Drawing.Color.Black;
            this.txtSeriDateYear.Location = new System.Drawing.Point(73, 29);
            this.txtSeriDateYear.MaxLength = 2;
            this.txtSeriDateYear.Name = "txtSeriDateYear";
            this.txtSeriDateYear.Size = new System.Drawing.Size(30, 20);
            this.txtSeriDateYear.TabIndex = 2;
            this.txtSeriDateYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSeriDateYear.Validating += new System.ComponentModel.CancelEventHandler(this.txtSeriDateYear_Validating);
            // 
            // txtSeriDateMonth
            // 
            this.txtSeriDateMonth.AutoSizeFromLength = false;
            this.txtSeriDateMonth.DisplayLength = null;
            this.txtSeriDateMonth.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtSeriDateMonth.ForeColor = System.Drawing.Color.Black;
            this.txtSeriDateMonth.Location = new System.Drawing.Point(126, 29);
            this.txtSeriDateMonth.MaxLength = 2;
            this.txtSeriDateMonth.Name = "txtSeriDateMonth";
            this.txtSeriDateMonth.Size = new System.Drawing.Size(30, 20);
            this.txtSeriDateMonth.TabIndex = 3;
            this.txtSeriDateMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSeriDateMonth.Validating += new System.ComponentModel.CancelEventHandler(this.txtSeriDateMonth_Validating);
            // 
            // lblSeriDateGengo
            // 
            this.lblSeriDateGengo.BackColor = System.Drawing.Color.Silver;
            this.lblSeriDateGengo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSeriDateGengo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblSeriDateGengo.ForeColor = System.Drawing.Color.Black;
            this.lblSeriDateGengo.Location = new System.Drawing.Point(28, 29);
            this.lblSeriDateGengo.Name = "lblSeriDateGengo";
            this.lblSeriDateGengo.Size = new System.Drawing.Size(41, 21);
            this.lblSeriDateGengo.TabIndex = 1;
            this.lblSeriDateGengo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSeriDate
            // 
            this.lblSeriDate.BackColor = System.Drawing.Color.Silver;
            this.lblSeriDate.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSeriDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblSeriDate.ForeColor = System.Drawing.Color.Black;
            this.lblSeriDate.Location = new System.Drawing.Point(25, 26);
            this.lblSeriDate.Name = "lblSeriDate";
            this.lblSeriDate.Size = new System.Drawing.Size(214, 27);
            this.lblSeriDate.TabIndex = 1;
            this.lblSeriDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // gbxFunanushiCd
            // 
            this.gbxFunanushiCd.Controls.Add(this.lblFunanushiCdTo);
            this.gbxFunanushiCd.Controls.Add(this.lblCodeBet);
            this.gbxFunanushiCd.Controls.Add(this.txtFunanushiCdFr);
            this.gbxFunanushiCd.Controls.Add(this.lblFunanushiCdFr);
            this.gbxFunanushiCd.Controls.Add(this.txtFunanushiCdTo);
            this.gbxFunanushiCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.gbxFunanushiCd.ForeColor = System.Drawing.Color.Black;
            this.gbxFunanushiCd.Location = new System.Drawing.Point(12, 305);
            this.gbxFunanushiCd.Name = "gbxFunanushiCd";
            this.gbxFunanushiCd.Size = new System.Drawing.Size(596, 85);
            this.gbxFunanushiCd.TabIndex = 4;
            this.gbxFunanushiCd.TabStop = false;
            this.gbxFunanushiCd.Text = "船主CD範囲";
            // 
            // lblFunanushiCdTo
            // 
            this.lblFunanushiCdTo.BackColor = System.Drawing.Color.Silver;
            this.lblFunanushiCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFunanushiCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblFunanushiCdTo.ForeColor = System.Drawing.Color.Black;
            this.lblFunanushiCdTo.Location = new System.Drawing.Point(362, 36);
            this.lblFunanushiCdTo.Name = "lblFunanushiCdTo";
            this.lblFunanushiCdTo.Size = new System.Drawing.Size(212, 20);
            this.lblFunanushiCdTo.TabIndex = 4;
            this.lblFunanushiCdTo.Text = "最　後";
            this.lblFunanushiCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCodeBet
            // 
            this.lblCodeBet.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblCodeBet.ForeColor = System.Drawing.Color.Black;
            this.lblCodeBet.Location = new System.Drawing.Point(291, 35);
            this.lblCodeBet.Name = "lblCodeBet";
            this.lblCodeBet.Size = new System.Drawing.Size(18, 20);
            this.lblCodeBet.TabIndex = 2;
            this.lblCodeBet.Text = "～";
            this.lblCodeBet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFunanushiCdFr
            // 
            this.txtFunanushiCdFr.AutoSizeFromLength = false;
            this.txtFunanushiCdFr.DisplayLength = null;
            this.txtFunanushiCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtFunanushiCdFr.ForeColor = System.Drawing.Color.Black;
            this.txtFunanushiCdFr.Location = new System.Drawing.Point(26, 35);
            this.txtFunanushiCdFr.MaxLength = 4;
            this.txtFunanushiCdFr.Name = "txtFunanushiCdFr";
            this.txtFunanushiCdFr.Size = new System.Drawing.Size(40, 20);
            this.txtFunanushiCdFr.TabIndex = 9;
            this.txtFunanushiCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFunanushiCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtCodeFr_Validating);
            // 
            // lblFunanushiCdFr
            // 
            this.lblFunanushiCdFr.BackColor = System.Drawing.Color.Silver;
            this.lblFunanushiCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFunanushiCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblFunanushiCdFr.ForeColor = System.Drawing.Color.Black;
            this.lblFunanushiCdFr.Location = new System.Drawing.Point(69, 36);
            this.lblFunanushiCdFr.Name = "lblFunanushiCdFr";
            this.lblFunanushiCdFr.Size = new System.Drawing.Size(212, 20);
            this.lblFunanushiCdFr.TabIndex = 1;
            this.lblFunanushiCdFr.Text = "先　頭";
            this.lblFunanushiCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFunanushiCdTo
            // 
            this.txtFunanushiCdTo.AutoSizeFromLength = false;
            this.txtFunanushiCdTo.DisplayLength = null;
            this.txtFunanushiCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtFunanushiCdTo.ForeColor = System.Drawing.Color.Black;
            this.txtFunanushiCdTo.Location = new System.Drawing.Point(319, 35);
            this.txtFunanushiCdTo.MaxLength = 4;
            this.txtFunanushiCdTo.Name = "txtFunanushiCdTo";
            this.txtFunanushiCdTo.Size = new System.Drawing.Size(40, 20);
            this.txtFunanushiCdTo.TabIndex = 10;
            this.txtFunanushiCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFunanushiCdTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtFunanushiCdTo_KeyDown);
            this.txtFunanushiCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtCodeTo_Validating);
            // 
            // gbxShiharaiDate
            // 
            this.gbxShiharaiDate.Controls.Add(this.lblShiharaiDateDay);
            this.gbxShiharaiDate.Controls.Add(this.lblShiharaiDateMonth);
            this.gbxShiharaiDate.Controls.Add(this.lblShiharaiDateYear);
            this.gbxShiharaiDate.Controls.Add(this.txtShiharaiDateDay);
            this.gbxShiharaiDate.Controls.Add(this.txtShiharaiDateYear);
            this.gbxShiharaiDate.Controls.Add(this.txtShiharaiDateMonth);
            this.gbxShiharaiDate.Controls.Add(this.lblShiharaiDateGengo);
            this.gbxShiharaiDate.Controls.Add(this.lblShiharaiDate);
            this.gbxShiharaiDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.gbxShiharaiDate.ForeColor = System.Drawing.Color.Black;
            this.gbxShiharaiDate.Location = new System.Drawing.Point(316, 140);
            this.gbxShiharaiDate.Name = "gbxShiharaiDate";
            this.gbxShiharaiDate.Size = new System.Drawing.Size(288, 72);
            this.gbxShiharaiDate.TabIndex = 2;
            this.gbxShiharaiDate.TabStop = false;
            this.gbxShiharaiDate.Text = "支払予定日";
            // 
            // lblShiharaiDateDay
            // 
            this.lblShiharaiDateDay.BackColor = System.Drawing.Color.Silver;
            this.lblShiharaiDateDay.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblShiharaiDateDay.ForeColor = System.Drawing.Color.Black;
            this.lblShiharaiDateDay.Location = new System.Drawing.Point(220, 30);
            this.lblShiharaiDateDay.Name = "lblShiharaiDateDay";
            this.lblShiharaiDateDay.Size = new System.Drawing.Size(20, 18);
            this.lblShiharaiDateDay.TabIndex = 7;
            this.lblShiharaiDateDay.Text = "日";
            this.lblShiharaiDateDay.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShiharaiDateMonth
            // 
            this.lblShiharaiDateMonth.BackColor = System.Drawing.Color.Silver;
            this.lblShiharaiDateMonth.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblShiharaiDateMonth.ForeColor = System.Drawing.Color.Black;
            this.lblShiharaiDateMonth.Location = new System.Drawing.Point(165, 30);
            this.lblShiharaiDateMonth.Name = "lblShiharaiDateMonth";
            this.lblShiharaiDateMonth.Size = new System.Drawing.Size(15, 19);
            this.lblShiharaiDateMonth.TabIndex = 5;
            this.lblShiharaiDateMonth.Text = "月";
            this.lblShiharaiDateMonth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShiharaiDateYear
            // 
            this.lblShiharaiDateYear.BackColor = System.Drawing.Color.Silver;
            this.lblShiharaiDateYear.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblShiharaiDateYear.ForeColor = System.Drawing.Color.Black;
            this.lblShiharaiDateYear.Location = new System.Drawing.Point(112, 29);
            this.lblShiharaiDateYear.Name = "lblShiharaiDateYear";
            this.lblShiharaiDateYear.Size = new System.Drawing.Size(17, 21);
            this.lblShiharaiDateYear.TabIndex = 3;
            this.lblShiharaiDateYear.Text = "年";
            this.lblShiharaiDateYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShiharaiDateDay
            // 
            this.txtShiharaiDateDay.AutoSizeFromLength = false;
            this.txtShiharaiDateDay.DisplayLength = null;
            this.txtShiharaiDateDay.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtShiharaiDateDay.ForeColor = System.Drawing.Color.Black;
            this.txtShiharaiDateDay.Location = new System.Drawing.Point(187, 29);
            this.txtShiharaiDateDay.MaxLength = 2;
            this.txtShiharaiDateDay.Name = "txtShiharaiDateDay";
            this.txtShiharaiDateDay.Size = new System.Drawing.Size(30, 20);
            this.txtShiharaiDateDay.TabIndex = 7;
            this.txtShiharaiDateDay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShiharaiDateDay.Validating += new System.ComponentModel.CancelEventHandler(this.txtShiharaiDateDay_Validating);
            // 
            // txtShiharaiDateYear
            // 
            this.txtShiharaiDateYear.AutoSizeFromLength = false;
            this.txtShiharaiDateYear.DisplayLength = null;
            this.txtShiharaiDateYear.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtShiharaiDateYear.ForeColor = System.Drawing.Color.Black;
            this.txtShiharaiDateYear.Location = new System.Drawing.Point(80, 29);
            this.txtShiharaiDateYear.MaxLength = 2;
            this.txtShiharaiDateYear.Name = "txtShiharaiDateYear";
            this.txtShiharaiDateYear.Size = new System.Drawing.Size(30, 20);
            this.txtShiharaiDateYear.TabIndex = 5;
            this.txtShiharaiDateYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShiharaiDateYear.Validating += new System.ComponentModel.CancelEventHandler(this.txtShiharaiDateYear_Validating);
            // 
            // txtShiharaiDateMonth
            // 
            this.txtShiharaiDateMonth.AutoSizeFromLength = false;
            this.txtShiharaiDateMonth.DisplayLength = null;
            this.txtShiharaiDateMonth.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtShiharaiDateMonth.ForeColor = System.Drawing.Color.Black;
            this.txtShiharaiDateMonth.Location = new System.Drawing.Point(133, 29);
            this.txtShiharaiDateMonth.MaxLength = 2;
            this.txtShiharaiDateMonth.Name = "txtShiharaiDateMonth";
            this.txtShiharaiDateMonth.Size = new System.Drawing.Size(30, 20);
            this.txtShiharaiDateMonth.TabIndex = 6;
            this.txtShiharaiDateMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShiharaiDateMonth.Validating += new System.ComponentModel.CancelEventHandler(this.txtShiharaiDateMonth_Validating);
            // 
            // lblShiharaiDateGengo
            // 
            this.lblShiharaiDateGengo.BackColor = System.Drawing.Color.Silver;
            this.lblShiharaiDateGengo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShiharaiDateGengo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblShiharaiDateGengo.ForeColor = System.Drawing.Color.Black;
            this.lblShiharaiDateGengo.Location = new System.Drawing.Point(35, 29);
            this.lblShiharaiDateGengo.Name = "lblShiharaiDateGengo";
            this.lblShiharaiDateGengo.Size = new System.Drawing.Size(41, 21);
            this.lblShiharaiDateGengo.TabIndex = 1;
            this.lblShiharaiDateGengo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShiharaiDate
            // 
            this.lblShiharaiDate.BackColor = System.Drawing.Color.Silver;
            this.lblShiharaiDate.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShiharaiDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblShiharaiDate.ForeColor = System.Drawing.Color.Black;
            this.lblShiharaiDate.Location = new System.Drawing.Point(32, 26);
            this.lblShiharaiDate.Name = "lblShiharaiDate";
            this.lblShiharaiDate.Size = new System.Drawing.Size(214, 27);
            this.lblShiharaiDate.TabIndex = 1;
            this.lblShiharaiDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // gbxMizuageShisho
            // 
            this.gbxMizuageShisho.Controls.Add(this.txtMizuageShishoCd);
            this.gbxMizuageShisho.Controls.Add(this.lblMizuageShishoNm);
            this.gbxMizuageShisho.Controls.Add(this.lblMizuageShisho);
            this.gbxMizuageShisho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.gbxMizuageShisho.ForeColor = System.Drawing.Color.Black;
            this.gbxMizuageShisho.Location = new System.Drawing.Point(12, 47);
            this.gbxMizuageShisho.Name = "gbxMizuageShisho";
            this.gbxMizuageShisho.Size = new System.Drawing.Size(381, 77);
            this.gbxMizuageShisho.TabIndex = 0;
            this.gbxMizuageShisho.TabStop = false;
            this.gbxMizuageShisho.Text = "水揚支所";
            // 
            // txtMizuageShishoCd
            // 
            this.txtMizuageShishoCd.AutoSizeFromLength = true;
            this.txtMizuageShishoCd.DisplayLength = null;
            this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtMizuageShishoCd.Location = new System.Drawing.Point(93, 33);
            this.txtMizuageShishoCd.MaxLength = 4;
            this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
            this.txtMizuageShishoCd.Size = new System.Drawing.Size(34, 20);
            this.txtMizuageShishoCd.TabIndex = 1;
            this.txtMizuageShishoCd.TabStop = false;
            this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
            // 
            // lblMizuageShishoNm
            // 
            this.lblMizuageShishoNm.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMizuageShishoNm.Location = new System.Drawing.Point(131, 33);
            this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
            this.lblMizuageShishoNm.Size = new System.Drawing.Size(212, 20);
            this.lblMizuageShishoNm.TabIndex = 2;
            this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMizuageShisho
            // 
            this.lblMizuageShisho.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShisho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShisho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblMizuageShisho.Location = new System.Drawing.Point(28, 31);
            this.lblMizuageShisho.Name = "lblMizuageShisho";
            this.lblMizuageShisho.Size = new System.Drawing.Size(321, 25);
            this.lblMizuageShisho.TabIndex = 0;
            this.lblMizuageShisho.Text = "水揚支所";
            this.lblMizuageShisho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // gbxSeisanKbn
            // 
            this.gbxSeisanKbn.Controls.Add(this.txtSeisanKbn);
            this.gbxSeisanKbn.Controls.Add(this.lblSeisanKbnNm);
            this.gbxSeisanKbn.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.gbxSeisanKbn.ForeColor = System.Drawing.Color.Black;
            this.gbxSeisanKbn.Location = new System.Drawing.Point(12, 224);
            this.gbxSeisanKbn.Name = "gbxSeisanKbn";
            this.gbxSeisanKbn.Size = new System.Drawing.Size(317, 69);
            this.gbxSeisanKbn.TabIndex = 3;
            this.gbxSeisanKbn.TabStop = false;
            this.gbxSeisanKbn.Text = "精算区分";
            // 
            // txtSeisanKbn
            // 
            this.txtSeisanKbn.AutoSizeFromLength = true;
            this.txtSeisanKbn.DisplayLength = null;
            this.txtSeisanKbn.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtSeisanKbn.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtSeisanKbn.Location = new System.Drawing.Point(26, 25);
            this.txtSeisanKbn.MaxLength = 4;
            this.txtSeisanKbn.Name = "txtSeisanKbn";
            this.txtSeisanKbn.Size = new System.Drawing.Size(34, 20);
            this.txtSeisanKbn.TabIndex = 8;
            this.txtSeisanKbn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSeisanKbn.Validating += new System.ComponentModel.CancelEventHandler(this.txtSeisanKbn_Validating);
            // 
            // lblSeisanKbnNm
            // 
            this.lblSeisanKbnNm.BackColor = System.Drawing.Color.Silver;
            this.lblSeisanKbnNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSeisanKbnNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblSeisanKbnNm.Location = new System.Drawing.Point(63, 25);
            this.lblSeisanKbnNm.Name = "lblSeisanKbnNm";
            this.lblSeisanKbnNm.Size = new System.Drawing.Size(212, 20);
            this.lblSeisanKbnNm.TabIndex = 4;
            this.lblSeisanKbnNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // HNDR1021
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 638);
            this.Controls.Add(this.gbxSeisanKbn);
            this.Controls.Add(this.gbxMizuageShisho);
            this.Controls.Add(this.gbxShiharaiDate);
            this.Controls.Add(this.gbxFunanushiCd);
            this.Controls.Add(this.gbxSeriDate);
            this.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "HNDR1021";
            this.Text = "";
            this.Controls.SetChildIndex(this.gbxSeriDate, 0);
            this.Controls.SetChildIndex(this.gbxFunanushiCd, 0);
            this.Controls.SetChildIndex(this.gbxShiharaiDate, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.gbxMizuageShisho, 0);
            this.Controls.SetChildIndex(this.gbxSeisanKbn, 0);
            this.pnlDebug.ResumeLayout(false);
            this.gbxSeriDate.ResumeLayout(false);
            this.gbxSeriDate.PerformLayout();
            this.gbxFunanushiCd.ResumeLayout(false);
            this.gbxFunanushiCd.PerformLayout();
            this.gbxShiharaiDate.ResumeLayout(false);
            this.gbxShiharaiDate.PerformLayout();
            this.gbxMizuageShisho.ResumeLayout(false);
            this.gbxMizuageShisho.PerformLayout();
            this.gbxSeisanKbn.ResumeLayout(false);
            this.gbxSeisanKbn.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbxSeriDate;
        private jp.co.fsi.common.controls.FsiTextBox txtSeriDateYear;
        private System.Windows.Forms.Label lblSeriDateGengo;
        private System.Windows.Forms.Label lblSeriDate;
        private System.Windows.Forms.Label lblSeriDateDay;
        private System.Windows.Forms.Label lblSeriDateMonth;
        private System.Windows.Forms.Label lblSeriDateYear;
        private jp.co.fsi.common.controls.FsiTextBox txtSeriDateDay;
        private jp.co.fsi.common.controls.FsiTextBox txtSeriDateMonth;
        private System.Windows.Forms.GroupBox gbxFunanushiCd;
        private System.Windows.Forms.Label lblFunanushiCdTo;
        private System.Windows.Forms.Label lblCodeBet;
        private common.controls.FsiTextBox txtFunanushiCdFr;
        private System.Windows.Forms.Label lblFunanushiCdFr;
        private common.controls.FsiTextBox txtFunanushiCdTo;
        private System.Windows.Forms.GroupBox gbxShiharaiDate;
        private System.Windows.Forms.Label lblShiharaiDateDay;
        private System.Windows.Forms.Label lblShiharaiDateMonth;
        private System.Windows.Forms.Label lblShiharaiDateYear;
        private common.controls.FsiTextBox txtShiharaiDateDay;
        private common.controls.FsiTextBox txtShiharaiDateYear;
        private common.controls.FsiTextBox txtShiharaiDateMonth;
        private System.Windows.Forms.Label lblShiharaiDateGengo;
        private System.Windows.Forms.Label lblShiharaiDate;
        private System.Windows.Forms.GroupBox gbxMizuageShisho;
        private common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private System.Windows.Forms.Label lblMizuageShisho;
        private System.Windows.Forms.GroupBox gbxSeisanKbn;
        private common.controls.FsiTextBox txtSeisanKbn;
        private System.Windows.Forms.Label lblSeisanKbnNm;
    }
}