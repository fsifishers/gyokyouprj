﻿using System.Data;

using jp.co.fsi.common.report;

namespace jp.co.fsi.hn.hndr1021
{
    /// <summary>
    /// HANR1021R の概要の説明です。
    /// </summary>
    public partial class HNDR1021R : BaseReport
    {

        public HNDR1021R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }

    }
}
