﻿namespace jp.co.fsi.hn.hndr1021
{
    /// <summary>
    /// HANR2021R の概要の説明です。
    /// </summary>
    partial class HNDR1021R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HNDR1021R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.テキスト10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox21 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox22 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox23 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox24 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox25 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox26 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox27 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox28 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox29 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox30 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox31 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox32 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox33 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox34 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox35 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox36 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox37 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox38 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox39 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox40 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox41 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox42 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox43 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox44 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox45 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox46 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox47 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox48 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox49 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox50 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox51 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox54 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox55 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox56 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox57 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox58 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox59 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox61 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox62 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox63 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox64 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox65 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox66 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox67 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox68 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox69 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox70 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox71 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox72 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox73 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox75 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox76 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox77 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox78 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox79 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox82 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox83 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox84 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox85 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox86 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox89 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox90 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox91 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox92 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox93 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox96 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox97 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox98 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox99 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox100 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox103 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox104 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox105 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox106 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox107 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox110 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox111 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox112 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox113 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox114 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox117 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox118 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox119 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox120 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox121 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line15 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line16 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line17 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line18 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line19 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line20 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line21 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line22 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line23 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line24 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line25 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line26 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line27 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line28 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line29 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line30 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line31 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line32 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line33 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line34 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line35 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line36 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line37 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line38 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line39 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line40 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line41 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtTekiyoKoza = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox53 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line46 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line47 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line48 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.textBox7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox74 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox81 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox87 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox88 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox94 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox95 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox101 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.groupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.groupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.groupHeader2 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox52 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox80 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.crossSectionBox1 = new GrapeCity.ActiveReports.SectionReportModel.CrossSectionBox();
            this.textBox5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblInfo = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line42 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line43 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line44 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line45 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox60 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox102 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.groupFooter2 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.picture1 = new GrapeCity.ActiveReports.SectionReportModel.Picture();
            this.ラベル6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox54)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox55)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox56)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox57)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox58)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox59)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox61)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox62)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox63)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox64)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox65)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox66)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox67)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox68)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox69)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox70)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox71)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox72)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox73)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox75)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox76)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox77)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox78)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox79)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox82)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox83)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox84)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox85)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox86)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox89)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox90)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox91)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox92)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox93)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox96)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox97)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox98)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox99)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox100)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox103)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox104)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox105)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox106)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox107)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox110)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox111)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox112)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox113)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox114)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox117)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox118)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox119)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox120)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox121)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTekiyoKoza)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox74)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox81)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox87)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox88)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox94)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox95)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox101)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox80)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox60)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox102)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Height = 0F;
            this.pageHeader.Name = "pageHeader";
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.テキスト10,
            this.テキスト11,
            this.テキスト12,
            this.テキスト13,
            this.テキスト14,
            this.テキスト17,
            this.テキスト18,
            this.textBox2,
            this.textBox3,
            this.textBox4,
            this.textBox8,
            this.textBox9,
            this.textBox10,
            this.textBox12,
            this.textBox13,
            this.textBox14,
            this.textBox15,
            this.textBox16,
            this.textBox17,
            this.textBox18,
            this.textBox19,
            this.textBox20,
            this.textBox21,
            this.textBox22,
            this.textBox23,
            this.textBox24,
            this.textBox25,
            this.textBox26,
            this.textBox27,
            this.textBox28,
            this.textBox29,
            this.textBox30,
            this.textBox31,
            this.textBox32,
            this.textBox33,
            this.textBox34,
            this.textBox35,
            this.textBox36,
            this.textBox37,
            this.textBox38,
            this.textBox39,
            this.textBox40,
            this.textBox41,
            this.textBox42,
            this.textBox43,
            this.textBox44,
            this.textBox45,
            this.textBox46,
            this.textBox47,
            this.textBox48,
            this.textBox49,
            this.textBox50,
            this.textBox51,
            this.textBox54,
            this.textBox55,
            this.textBox56,
            this.textBox57,
            this.textBox58,
            this.textBox59,
            this.textBox61,
            this.textBox62,
            this.textBox63,
            this.textBox64,
            this.textBox65,
            this.textBox66,
            this.textBox67,
            this.textBox68,
            this.textBox69,
            this.textBox70,
            this.textBox71,
            this.textBox72,
            this.textBox73,
            this.textBox75,
            this.textBox76,
            this.textBox77,
            this.textBox78,
            this.textBox79,
            this.textBox82,
            this.textBox83,
            this.textBox84,
            this.textBox85,
            this.textBox86,
            this.textBox89,
            this.textBox90,
            this.textBox91,
            this.textBox92,
            this.textBox93,
            this.textBox96,
            this.textBox97,
            this.textBox98,
            this.textBox99,
            this.textBox100,
            this.textBox103,
            this.textBox104,
            this.textBox105,
            this.textBox106,
            this.textBox107,
            this.textBox110,
            this.textBox111,
            this.textBox112,
            this.textBox113,
            this.textBox114,
            this.textBox117,
            this.textBox118,
            this.textBox119,
            this.textBox120,
            this.textBox121,
            this.label7,
            this.line3,
            this.line4,
            this.line5,
            this.line6,
            this.line7,
            this.line8,
            this.line9,
            this.line11,
            this.line12,
            this.line13,
            this.line14,
            this.line15,
            this.line16,
            this.line17,
            this.line18,
            this.line19,
            this.line20,
            this.line21,
            this.line22,
            this.line23,
            this.line24,
            this.line25,
            this.line26,
            this.line27,
            this.line28,
            this.line29,
            this.line30,
            this.line31,
            this.line32,
            this.line33,
            this.line34,
            this.line35,
            this.line36,
            this.line37,
            this.line38,
            this.line39,
            this.line40,
            this.line41,
            this.txtTekiyoKoza,
            this.textBox11,
            this.textBox53,
            this.line46,
            this.line47,
            this.line48,
            this.textBox7,
            this.textBox74,
            this.textBox81,
            this.textBox87,
            this.textBox88,
            this.textBox94,
            this.textBox95,
            this.textBox101});
            this.detail.Height = 6.125132F;
            this.detail.Name = "detail";
            // 
            // テキスト10
            // 
            this.テキスト10.DataField = "ITEM07";
            this.テキスト10.Height = 0.2188976F;
            this.テキスト10.Left = 0.02401569F;
            this.テキスト10.Name = "テキスト10";
            this.テキスト10.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.テキスト10.Tag = "";
            this.テキスト10.Text = null;
            this.テキスト10.Top = 0.05944882F;
            this.テキスト10.Width = 0.3874016F;
            // 
            // テキスト11
            // 
            this.テキスト11.DataField = "ITEM08";
            this.テキスト11.Height = 0.21875F;
            this.テキスト11.Left = 0.5023623F;
            this.テキスト11.MultiLine = false;
            this.テキスト11.Name = "テキスト11";
            this.テキスト11.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: left; vertical-align: middle; ddo-char-set: 1";
            this.テキスト11.Tag = "";
            this.テキスト11.Text = null;
            this.テキスト11.Top = 0.05944882F;
            this.テキスト11.Width = 1.47441F;
            // 
            // テキスト12
            // 
            this.テキスト12.DataField = "ITEM09";
            this.テキスト12.Height = 0.21875F;
            this.テキスト12.Left = 2.058661F;
            this.テキスト12.Name = "テキスト12";
            this.テキスト12.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.テキスト12.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.テキスト12.Tag = "";
            this.テキスト12.Text = null;
            this.テキスト12.Top = 0.05944882F;
            this.テキスト12.Width = 0.6645671F;
            // 
            // テキスト13
            // 
            this.テキスト13.DataField = "ITEM10";
            this.テキスト13.Height = 0.21875F;
            this.テキスト13.Left = 2.78937F;
            this.テキスト13.Name = "テキスト13";
            this.テキスト13.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.テキスト13.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.テキスト13.Tag = "";
            this.テキスト13.Text = null;
            this.テキスト13.Top = 0.05944882F;
            this.テキスト13.Width = 0.7480319F;
            // 
            // テキスト14
            // 
            this.テキスト14.DataField = "ITEM11";
            this.テキスト14.Height = 0.21875F;
            this.テキスト14.Left = 3.611418F;
            this.テキスト14.Name = "テキスト14";
            this.テキスト14.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.テキスト14.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.テキスト14.Tag = "";
            this.テキスト14.Text = null;
            this.テキスト14.Top = 0.05944882F;
            this.テキスト14.Width = 1.016929F;
            // 
            // テキスト17
            // 
            this.テキスト17.Height = 0.21875F;
            this.テキスト17.Left = 4.694882F;
            this.テキスト17.Name = "テキスト17";
            this.テキスト17.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.テキスト17.Tag = "";
            this.テキスト17.Text = "水 揚 金 額";
            this.テキスト17.Top = 0.05944882F;
            this.テキスト17.Width = 1.108268F;
            // 
            // テキスト18
            // 
            this.テキスト18.DataField = "ITEM97";
            this.テキスト18.Height = 0.21875F;
            this.テキスト18.Left = 5.87126F;
            this.テキスト18.Name = "テキスト18";
            this.テキスト18.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.テキスト18.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.テキスト18.Tag = "";
            this.テキスト18.Text = "ITEM97";
            this.テキスト18.Top = 0.05944882F;
            this.テキスト18.Width = 1.187796F;
            // 
            // textBox2
            // 
            this.textBox2.DataField = "ITEM12";
            this.textBox2.Height = 0.2188976F;
            this.textBox2.Left = 0.02401569F;
            this.textBox2.Name = "textBox2";
            this.textBox2.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox2.Tag = "";
            this.textBox2.Text = null;
            this.textBox2.Top = 0.3928477F;
            this.textBox2.Width = 0.3874016F;
            // 
            // textBox3
            // 
            this.textBox3.DataField = "ITEM13";
            this.textBox3.Height = 0.21875F;
            this.textBox3.Left = 0.5023622F;
            this.textBox3.MultiLine = false;
            this.textBox3.Name = "textBox3";
            this.textBox3.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: left; vertical-align: middle; ddo-char-set: 1";
            this.textBox3.Tag = "";
            this.textBox3.Text = null;
            this.textBox3.Top = 0.3928477F;
            this.textBox3.Width = 1.47441F;
            // 
            // textBox4
            // 
            this.textBox4.DataField = "ITEM14";
            this.textBox4.Height = 0.21875F;
            this.textBox4.Left = 2.058662F;
            this.textBox4.Name = "textBox4";
            this.textBox4.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.textBox4.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox4.Tag = "";
            this.textBox4.Text = null;
            this.textBox4.Top = 0.3928477F;
            this.textBox4.Width = 0.6645671F;
            // 
            // textBox8
            // 
            this.textBox8.DataField = "ITEM15";
            this.textBox8.Height = 0.21875F;
            this.textBox8.Left = 2.78937F;
            this.textBox8.Name = "textBox8";
            this.textBox8.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.textBox8.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox8.Tag = "";
            this.textBox8.Text = null;
            this.textBox8.Top = 0.3928477F;
            this.textBox8.Width = 0.7480319F;
            // 
            // textBox9
            // 
            this.textBox9.DataField = "ITEM16";
            this.textBox9.Height = 0.21875F;
            this.textBox9.Left = 3.611417F;
            this.textBox9.Name = "textBox9";
            this.textBox9.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.textBox9.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox9.Tag = "";
            this.textBox9.Text = null;
            this.textBox9.Top = 0.3928477F;
            this.textBox9.Width = 1.016142F;
            // 
            // textBox10
            // 
            this.textBox10.DataField = "ITEM110";
            this.textBox10.Height = 0.21875F;
            this.textBox10.Left = 5.043701F;
            this.textBox10.Name = "textBox10";
            this.textBox10.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: left; vertical-align: middle; ddo-char-set: 1";
            this.textBox10.Tag = "";
            this.textBox10.Text = "控除項目１";
            this.textBox10.Top = 0.3929134F;
            this.textBox10.Width = 1.019291F;
            // 
            // textBox12
            // 
            this.textBox12.DataField = "ITEM17";
            this.textBox12.Height = 0.2188976F;
            this.textBox12.Left = 0.02401569F;
            this.textBox12.Name = "textBox12";
            this.textBox12.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox12.Tag = "";
            this.textBox12.Text = null;
            this.textBox12.Top = 0.7263123F;
            this.textBox12.Width = 0.3874016F;
            // 
            // textBox13
            // 
            this.textBox13.DataField = "ITEM18";
            this.textBox13.Height = 0.21875F;
            this.textBox13.Left = 0.5023622F;
            this.textBox13.MultiLine = false;
            this.textBox13.Name = "textBox13";
            this.textBox13.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: left; vertical-align: middle; ddo-char-set: 1";
            this.textBox13.Tag = "";
            this.textBox13.Text = null;
            this.textBox13.Top = 0.7263123F;
            this.textBox13.Width = 1.47441F;
            // 
            // textBox14
            // 
            this.textBox14.DataField = "ITEM19";
            this.textBox14.Height = 0.21875F;
            this.textBox14.Left = 2.058662F;
            this.textBox14.Name = "textBox14";
            this.textBox14.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.textBox14.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox14.Tag = "";
            this.textBox14.Text = null;
            this.textBox14.Top = 0.7263123F;
            this.textBox14.Width = 0.6645671F;
            // 
            // textBox15
            // 
            this.textBox15.DataField = "ITEM20";
            this.textBox15.Height = 0.21875F;
            this.textBox15.Left = 2.78937F;
            this.textBox15.Name = "textBox15";
            this.textBox15.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.textBox15.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox15.Tag = "";
            this.textBox15.Text = null;
            this.textBox15.Top = 0.7263123F;
            this.textBox15.Width = 0.7480319F;
            // 
            // textBox16
            // 
            this.textBox16.DataField = "ITEM21";
            this.textBox16.Height = 0.21875F;
            this.textBox16.Left = 3.611417F;
            this.textBox16.Name = "textBox16";
            this.textBox16.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.textBox16.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox16.Tag = "";
            this.textBox16.Text = null;
            this.textBox16.Top = 0.7263123F;
            this.textBox16.Width = 1.016142F;
            // 
            // textBox17
            // 
            this.textBox17.DataField = "ITEM111";
            this.textBox17.Height = 0.21875F;
            this.textBox17.Left = 5.043701F;
            this.textBox17.Name = "textBox17";
            this.textBox17.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: left; vertical-align: middle; ddo-char-set: 1";
            this.textBox17.Tag = "";
            this.textBox17.Text = "控除項目２";
            this.textBox17.Top = 0.726378F;
            this.textBox17.Width = 0.9267716F;
            // 
            // textBox18
            // 
            this.textBox18.DataField = "ITEM99";
            this.textBox18.Height = 0.21875F;
            this.textBox18.Left = 5.937008F;
            this.textBox18.Name = "textBox18";
            this.textBox18.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.textBox18.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox18.Tag = "";
            this.textBox18.Text = "ITEM99";
            this.textBox18.Top = 0.726378F;
            this.textBox18.Width = 1.125197F;
            // 
            // textBox19
            // 
            this.textBox19.DataField = "ITEM22";
            this.textBox19.Height = 0.2188976F;
            this.textBox19.Left = 0.02401569F;
            this.textBox19.Name = "textBox19";
            this.textBox19.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox19.Tag = "";
            this.textBox19.Text = null;
            this.textBox19.Top = 1.059777F;
            this.textBox19.Width = 0.3874016F;
            // 
            // textBox20
            // 
            this.textBox20.DataField = "ITEM23";
            this.textBox20.Height = 0.21875F;
            this.textBox20.Left = 0.5023622F;
            this.textBox20.MultiLine = false;
            this.textBox20.Name = "textBox20";
            this.textBox20.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: left; vertical-align: middle; ddo-char-set: 1";
            this.textBox20.Tag = "";
            this.textBox20.Text = null;
            this.textBox20.Top = 1.059777F;
            this.textBox20.Width = 1.47441F;
            // 
            // textBox21
            // 
            this.textBox21.DataField = "ITEM24";
            this.textBox21.Height = 0.21875F;
            this.textBox21.Left = 2.058662F;
            this.textBox21.Name = "textBox21";
            this.textBox21.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.textBox21.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox21.Tag = "";
            this.textBox21.Text = null;
            this.textBox21.Top = 1.059777F;
            this.textBox21.Width = 0.6645671F;
            // 
            // textBox22
            // 
            this.textBox22.DataField = "ITEM25";
            this.textBox22.Height = 0.21875F;
            this.textBox22.Left = 2.78937F;
            this.textBox22.Name = "textBox22";
            this.textBox22.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.textBox22.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox22.Tag = "";
            this.textBox22.Text = null;
            this.textBox22.Top = 1.059777F;
            this.textBox22.Width = 0.7480319F;
            // 
            // textBox23
            // 
            this.textBox23.DataField = "ITEM26";
            this.textBox23.Height = 0.21875F;
            this.textBox23.Left = 3.611416F;
            this.textBox23.Name = "textBox23";
            this.textBox23.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.textBox23.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox23.Tag = "";
            this.textBox23.Text = null;
            this.textBox23.Top = 1.059777F;
            this.textBox23.Width = 1.016142F;
            // 
            // textBox24
            // 
            this.textBox24.DataField = "ITEM112";
            this.textBox24.Height = 0.21875F;
            this.textBox24.Left = 5.037008F;
            this.textBox24.Name = "textBox24";
            this.textBox24.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: left; vertical-align: middle; ddo-char-set: 1";
            this.textBox24.Tag = "";
            this.textBox24.Text = "控除項目３";
            this.textBox24.Top = 1.059843F;
            this.textBox24.Width = 0.9267716F;
            // 
            // textBox25
            // 
            this.textBox25.DataField = "ITEM100";
            this.textBox25.Height = 0.21875F;
            this.textBox25.Left = 5.937008F;
            this.textBox25.Name = "textBox25";
            this.textBox25.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.textBox25.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox25.Tag = "";
            this.textBox25.Text = "ITEM100";
            this.textBox25.Top = 1.059843F;
            this.textBox25.Width = 1.125197F;
            // 
            // textBox26
            // 
            this.textBox26.DataField = "ITEM27";
            this.textBox26.Height = 0.2188976F;
            this.textBox26.Left = 0.02519679F;
            this.textBox26.Name = "textBox26";
            this.textBox26.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox26.Tag = "";
            this.textBox26.Text = null;
            this.textBox26.Top = 1.393242F;
            this.textBox26.Width = 0.3874016F;
            // 
            // textBox27
            // 
            this.textBox27.DataField = "ITEM28";
            this.textBox27.Height = 0.21875F;
            this.textBox27.Left = 0.5023622F;
            this.textBox27.MultiLine = false;
            this.textBox27.Name = "textBox27";
            this.textBox27.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: left; vertical-align: middle; ddo-char-set: 1";
            this.textBox27.Tag = "";
            this.textBox27.Text = null;
            this.textBox27.Top = 1.393242F;
            this.textBox27.Width = 1.47441F;
            // 
            // textBox28
            // 
            this.textBox28.DataField = "ITEM29";
            this.textBox28.Height = 0.21875F;
            this.textBox28.Left = 2.058662F;
            this.textBox28.Name = "textBox28";
            this.textBox28.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.textBox28.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox28.Tag = "";
            this.textBox28.Text = null;
            this.textBox28.Top = 1.393242F;
            this.textBox28.Width = 0.6645671F;
            // 
            // textBox29
            // 
            this.textBox29.DataField = "ITEM30";
            this.textBox29.Height = 0.21875F;
            this.textBox29.Left = 2.78937F;
            this.textBox29.Name = "textBox29";
            this.textBox29.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.textBox29.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox29.Tag = "";
            this.textBox29.Text = null;
            this.textBox29.Top = 1.393242F;
            this.textBox29.Width = 0.7480319F;
            // 
            // textBox30
            // 
            this.textBox30.DataField = "ITEM31";
            this.textBox30.Height = 0.21875F;
            this.textBox30.Left = 3.611417F;
            this.textBox30.Name = "textBox30";
            this.textBox30.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.textBox30.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox30.Tag = "";
            this.textBox30.Text = null;
            this.textBox30.Top = 1.393242F;
            this.textBox30.Width = 1.016142F;
            // 
            // textBox31
            // 
            this.textBox31.DataField = "ITEM113";
            this.textBox31.Height = 0.21875F;
            this.textBox31.Left = 5.037008F;
            this.textBox31.Name = "textBox31";
            this.textBox31.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: left; vertical-align: middle; ddo-char-set: 1";
            this.textBox31.Tag = "";
            this.textBox31.Text = "控除項目４";
            this.textBox31.Top = 1.393307F;
            this.textBox31.Width = 0.9200788F;
            // 
            // textBox32
            // 
            this.textBox32.DataField = "ITEM101";
            this.textBox32.Height = 0.21875F;
            this.textBox32.Left = 5.937008F;
            this.textBox32.Name = "textBox32";
            this.textBox32.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.textBox32.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox32.Tag = "";
            this.textBox32.Text = "ITEM101";
            this.textBox32.Top = 1.393307F;
            this.textBox32.Width = 1.122047F;
            // 
            // textBox33
            // 
            this.textBox33.DataField = "ITEM32";
            this.textBox33.Height = 0.2188976F;
            this.textBox33.Left = 0.02401569F;
            this.textBox33.Name = "textBox33";
            this.textBox33.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox33.Tag = "";
            this.textBox33.Text = null;
            this.textBox33.Top = 1.726706F;
            this.textBox33.Width = 0.3874016F;
            // 
            // textBox34
            // 
            this.textBox34.DataField = "ITEM33";
            this.textBox34.Height = 0.21875F;
            this.textBox34.Left = 0.5027559F;
            this.textBox34.MultiLine = false;
            this.textBox34.Name = "textBox34";
            this.textBox34.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: left; vertical-align: middle; ddo-char-set: 1";
            this.textBox34.Tag = "";
            this.textBox34.Text = null;
            this.textBox34.Top = 1.726706F;
            this.textBox34.Width = 1.47441F;
            // 
            // textBox35
            // 
            this.textBox35.DataField = "ITEM34";
            this.textBox35.Height = 0.21875F;
            this.textBox35.Left = 2.059056F;
            this.textBox35.Name = "textBox35";
            this.textBox35.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.textBox35.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox35.Tag = "";
            this.textBox35.Text = null;
            this.textBox35.Top = 1.726706F;
            this.textBox35.Width = 0.6645671F;
            // 
            // textBox36
            // 
            this.textBox36.DataField = "ITEM35";
            this.textBox36.Height = 0.21875F;
            this.textBox36.Left = 2.789764F;
            this.textBox36.Name = "textBox36";
            this.textBox36.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.textBox36.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox36.Tag = "";
            this.textBox36.Text = null;
            this.textBox36.Top = 1.726706F;
            this.textBox36.Width = 0.7480319F;
            // 
            // textBox37
            // 
            this.textBox37.DataField = "ITEM36";
            this.textBox37.Height = 0.21875F;
            this.textBox37.Left = 3.61181F;
            this.textBox37.Name = "textBox37";
            this.textBox37.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.textBox37.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox37.Tag = "";
            this.textBox37.Text = null;
            this.textBox37.Top = 1.726706F;
            this.textBox37.Width = 1.016142F;
            // 
            // textBox38
            // 
            this.textBox38.DataField = "ITEM114";
            this.textBox38.Height = 0.21875F;
            this.textBox38.Left = 5.037008F;
            this.textBox38.Name = "textBox38";
            this.textBox38.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: left; vertical-align: middle; ddo-char-set: 1";
            this.textBox38.Tag = "";
            this.textBox38.Text = "控除項目５";
            this.textBox38.Top = 1.726772F;
            this.textBox38.Width = 0.9200788F;
            // 
            // textBox39
            // 
            this.textBox39.DataField = "ITEM102";
            this.textBox39.Height = 0.21875F;
            this.textBox39.Left = 5.937008F;
            this.textBox39.Name = "textBox39";
            this.textBox39.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.textBox39.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox39.Tag = "";
            this.textBox39.Text = "ITEM102";
            this.textBox39.Top = 1.726772F;
            this.textBox39.Width = 1.122441F;
            // 
            // textBox40
            // 
            this.textBox40.DataField = "ITEM37";
            this.textBox40.Height = 0.2188976F;
            this.textBox40.Left = 0.02519679F;
            this.textBox40.Name = "textBox40";
            this.textBox40.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox40.Tag = "";
            this.textBox40.Text = null;
            this.textBox40.Top = 2.060171F;
            this.textBox40.Width = 0.3874016F;
            // 
            // textBox41
            // 
            this.textBox41.DataField = "ITEM38";
            this.textBox41.Height = 0.21875F;
            this.textBox41.Left = 0.5027559F;
            this.textBox41.MultiLine = false;
            this.textBox41.Name = "textBox41";
            this.textBox41.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: left; vertical-align: middle; ddo-char-set: 1";
            this.textBox41.Tag = "";
            this.textBox41.Text = null;
            this.textBox41.Top = 2.060171F;
            this.textBox41.Width = 1.47441F;
            // 
            // textBox42
            // 
            this.textBox42.DataField = "ITEM39";
            this.textBox42.Height = 0.21875F;
            this.textBox42.Left = 2.059056F;
            this.textBox42.Name = "textBox42";
            this.textBox42.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.textBox42.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox42.Tag = "";
            this.textBox42.Text = null;
            this.textBox42.Top = 2.060171F;
            this.textBox42.Width = 0.6645671F;
            // 
            // textBox43
            // 
            this.textBox43.DataField = "ITEM40";
            this.textBox43.Height = 0.21875F;
            this.textBox43.Left = 2.789764F;
            this.textBox43.Name = "textBox43";
            this.textBox43.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.textBox43.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox43.Tag = "";
            this.textBox43.Text = null;
            this.textBox43.Top = 2.060171F;
            this.textBox43.Width = 0.7480319F;
            // 
            // textBox44
            // 
            this.textBox44.DataField = "ITEM41";
            this.textBox44.Height = 0.21875F;
            this.textBox44.Left = 3.61181F;
            this.textBox44.Name = "textBox44";
            this.textBox44.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.textBox44.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox44.Tag = "";
            this.textBox44.Text = null;
            this.textBox44.Top = 2.060171F;
            this.textBox44.Width = 1.016142F;
            // 
            // textBox45
            // 
            this.textBox45.DataField = "ITEM115";
            this.textBox45.Height = 0.21875F;
            this.textBox45.Left = 5.037008F;
            this.textBox45.Name = "textBox45";
            this.textBox45.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: left; vertical-align: middle; ddo-char-set: 1";
            this.textBox45.Tag = "";
            this.textBox45.Text = "控除項目６";
            this.textBox45.Top = 2.060236F;
            this.textBox45.Width = 0.9200788F;
            // 
            // textBox46
            // 
            this.textBox46.DataField = "ITEM103";
            this.textBox46.Height = 0.21875F;
            this.textBox46.Left = 5.937008F;
            this.textBox46.Name = "textBox46";
            this.textBox46.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.textBox46.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox46.Tag = "";
            this.textBox46.Text = "ITEM103";
            this.textBox46.Top = 2.060236F;
            this.textBox46.Width = 1.122441F;
            // 
            // textBox47
            // 
            this.textBox47.DataField = "ITEM42";
            this.textBox47.Height = 0.2188976F;
            this.textBox47.Left = 0.02401569F;
            this.textBox47.Name = "textBox47";
            this.textBox47.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox47.Tag = "";
            this.textBox47.Text = null;
            this.textBox47.Top = 2.393635F;
            this.textBox47.Width = 0.3874016F;
            // 
            // textBox48
            // 
            this.textBox48.DataField = "ITEM43";
            this.textBox48.Height = 0.21875F;
            this.textBox48.Left = 0.5027559F;
            this.textBox48.MultiLine = false;
            this.textBox48.Name = "textBox48";
            this.textBox48.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: left; vertical-align: middle; ddo-char-set: 1";
            this.textBox48.Tag = "";
            this.textBox48.Text = null;
            this.textBox48.Top = 2.393635F;
            this.textBox48.Width = 1.47441F;
            // 
            // textBox49
            // 
            this.textBox49.DataField = "ITEM44";
            this.textBox49.Height = 0.21875F;
            this.textBox49.Left = 2.059056F;
            this.textBox49.Name = "textBox49";
            this.textBox49.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.textBox49.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox49.Tag = "";
            this.textBox49.Text = null;
            this.textBox49.Top = 2.393635F;
            this.textBox49.Width = 0.6645671F;
            // 
            // textBox50
            // 
            this.textBox50.DataField = "ITEM45";
            this.textBox50.Height = 0.21875F;
            this.textBox50.Left = 2.789764F;
            this.textBox50.Name = "textBox50";
            this.textBox50.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.textBox50.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox50.Tag = "";
            this.textBox50.Text = null;
            this.textBox50.Top = 2.393635F;
            this.textBox50.Width = 0.7480319F;
            // 
            // textBox51
            // 
            this.textBox51.DataField = "ITEM46";
            this.textBox51.Height = 0.21875F;
            this.textBox51.Left = 3.61181F;
            this.textBox51.Name = "textBox51";
            this.textBox51.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.textBox51.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox51.Tag = "";
            this.textBox51.Text = null;
            this.textBox51.Top = 2.393635F;
            this.textBox51.Width = 1.016142F;
            // 
            // textBox54
            // 
            this.textBox54.DataField = "ITEM47";
            this.textBox54.Height = 0.2188976F;
            this.textBox54.Left = 0.02519679F;
            this.textBox54.Name = "textBox54";
            this.textBox54.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox54.Tag = "";
            this.textBox54.Text = null;
            this.textBox54.Top = 2.725131F;
            this.textBox54.Width = 0.3874016F;
            // 
            // textBox55
            // 
            this.textBox55.DataField = "ITEM48";
            this.textBox55.Height = 0.21875F;
            this.textBox55.Left = 0.5027559F;
            this.textBox55.MultiLine = false;
            this.textBox55.Name = "textBox55";
            this.textBox55.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: left; vertical-align: middle; ddo-char-set: 1";
            this.textBox55.Tag = "";
            this.textBox55.Text = null;
            this.textBox55.Top = 2.725131F;
            this.textBox55.Width = 1.47441F;
            // 
            // textBox56
            // 
            this.textBox56.DataField = "ITEM49";
            this.textBox56.Height = 0.21875F;
            this.textBox56.Left = 2.059054F;
            this.textBox56.Name = "textBox56";
            this.textBox56.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.textBox56.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox56.Tag = "";
            this.textBox56.Text = null;
            this.textBox56.Top = 2.725131F;
            this.textBox56.Width = 0.6645671F;
            // 
            // textBox57
            // 
            this.textBox57.DataField = "ITEM50";
            this.textBox57.Height = 0.21875F;
            this.textBox57.Left = 2.789763F;
            this.textBox57.Name = "textBox57";
            this.textBox57.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.textBox57.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox57.Tag = "";
            this.textBox57.Text = null;
            this.textBox57.Top = 2.725131F;
            this.textBox57.Width = 0.7480319F;
            // 
            // textBox58
            // 
            this.textBox58.DataField = "ITEM51";
            this.textBox58.Height = 0.21875F;
            this.textBox58.Left = 3.611811F;
            this.textBox58.Name = "textBox58";
            this.textBox58.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.textBox58.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox58.Tag = "";
            this.textBox58.Text = null;
            this.textBox58.Top = 2.725131F;
            this.textBox58.Width = 1.016142F;
            // 
            // textBox59
            // 
            this.textBox59.Height = 0.21875F;
            this.textBox59.Left = 4.944489F;
            this.textBox59.Name = "textBox59";
            this.textBox59.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: left; vertical-align: middle; ddo-char-set: 1";
            this.textBox59.Tag = "";
            this.textBox59.Text = "税込控除額合計";
            this.textBox59.Top = 3.706693F;
            this.textBox59.Width = 1.019185F;
            // 
            // textBox61
            // 
            this.textBox61.DataField = "ITEM52";
            this.textBox61.Height = 0.2188976F;
            this.textBox61.Left = 0.02519679F;
            this.textBox61.Name = "textBox61";
            this.textBox61.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox61.Tag = "";
            this.textBox61.Text = null;
            this.textBox61.Top = 3.058596F;
            this.textBox61.Width = 0.3874016F;
            // 
            // textBox62
            // 
            this.textBox62.DataField = "ITEM53";
            this.textBox62.Height = 0.21875F;
            this.textBox62.Left = 0.5027559F;
            this.textBox62.MultiLine = false;
            this.textBox62.Name = "textBox62";
            this.textBox62.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: left; vertical-align: middle; ddo-char-set: 1";
            this.textBox62.Tag = "";
            this.textBox62.Text = null;
            this.textBox62.Top = 3.058596F;
            this.textBox62.Width = 1.47441F;
            // 
            // textBox63
            // 
            this.textBox63.DataField = "ITEM54";
            this.textBox63.Height = 0.21875F;
            this.textBox63.Left = 2.059054F;
            this.textBox63.Name = "textBox63";
            this.textBox63.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.textBox63.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox63.Tag = "";
            this.textBox63.Text = null;
            this.textBox63.Top = 3.058596F;
            this.textBox63.Width = 0.6645671F;
            // 
            // textBox64
            // 
            this.textBox64.DataField = "ITEM55";
            this.textBox64.Height = 0.21875F;
            this.textBox64.Left = 2.789763F;
            this.textBox64.Name = "textBox64";
            this.textBox64.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.textBox64.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox64.Tag = "";
            this.textBox64.Text = null;
            this.textBox64.Top = 3.058596F;
            this.textBox64.Width = 0.7480319F;
            // 
            // textBox65
            // 
            this.textBox65.DataField = "ITEM56";
            this.textBox65.Height = 0.21875F;
            this.textBox65.Left = 3.611811F;
            this.textBox65.Name = "textBox65";
            this.textBox65.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.textBox65.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox65.Tag = "";
            this.textBox65.Text = null;
            this.textBox65.Top = 3.058596F;
            this.textBox65.Width = 1.016142F;
            // 
            // textBox66
            // 
            this.textBox66.Height = 0.3334646F;
            this.textBox66.Left = 4.667717F;
            this.textBox66.Name = "textBox66";
            this.textBox66.Style = "background-color: Navy; color: #E0E0E0; font-family: ＭＳ 明朝; font-size: 11.25pt; f" +
    "ont-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 12" +
    "8";
            this.textBox66.Tag = "";
            this.textBox66.Text = "差引支払額";
            this.textBox66.Top = 3.989764F;
            this.textBox66.Width = 1.005511F;
            // 
            // textBox67
            // 
            this.textBox67.DataField = "ITEM109";
            this.textBox67.Height = 0.21875F;
            this.textBox67.Left = 5.716142F;
            this.textBox67.Name = "textBox67";
            this.textBox67.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.textBox67.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox67.Tag = "";
            this.textBox67.Text = "ITEM109";
            this.textBox67.Top = 4.040158F;
            this.textBox67.Width = 1.342914F;
            // 
            // textBox68
            // 
            this.textBox68.DataField = "ITEM57";
            this.textBox68.Height = 0.2188976F;
            this.textBox68.Left = 0.02519679F;
            this.textBox68.Name = "textBox68";
            this.textBox68.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox68.Tag = "";
            this.textBox68.Text = null;
            this.textBox68.Top = 3.373163F;
            this.textBox68.Width = 0.3874016F;
            // 
            // textBox69
            // 
            this.textBox69.DataField = "ITEM58";
            this.textBox69.Height = 0.21875F;
            this.textBox69.Left = 0.5031496F;
            this.textBox69.MultiLine = false;
            this.textBox69.Name = "textBox69";
            this.textBox69.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: left; vertical-align: middle; ddo-char-set: 1";
            this.textBox69.Tag = "";
            this.textBox69.Text = null;
            this.textBox69.Top = 3.373163F;
            this.textBox69.Width = 1.47441F;
            // 
            // textBox70
            // 
            this.textBox70.DataField = "ITEM59";
            this.textBox70.Height = 0.21875F;
            this.textBox70.Left = 2.059448F;
            this.textBox70.Name = "textBox70";
            this.textBox70.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.textBox70.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox70.Tag = "";
            this.textBox70.Text = null;
            this.textBox70.Top = 3.373163F;
            this.textBox70.Width = 0.6645671F;
            // 
            // textBox71
            // 
            this.textBox71.DataField = "ITEM60";
            this.textBox71.Height = 0.21875F;
            this.textBox71.Left = 2.790157F;
            this.textBox71.Name = "textBox71";
            this.textBox71.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.textBox71.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox71.Tag = "";
            this.textBox71.Text = null;
            this.textBox71.Top = 3.373163F;
            this.textBox71.Width = 0.7480319F;
            // 
            // textBox72
            // 
            this.textBox72.DataField = "ITEM61";
            this.textBox72.Height = 0.21875F;
            this.textBox72.Left = 3.612205F;
            this.textBox72.Name = "textBox72";
            this.textBox72.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.textBox72.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox72.Tag = "";
            this.textBox72.Text = null;
            this.textBox72.Top = 3.373163F;
            this.textBox72.Width = 1.016142F;
            // 
            // textBox73
            // 
            this.textBox73.Height = 0.1562989F;
            this.textBox73.Left = 4.842126F;
            this.textBox73.Name = "textBox73";
            this.textBox73.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: left; ddo-char-set: 1";
            this.textBox73.Tag = "";
            this.textBox73.Text = "摘 要";
            this.textBox73.Top = 4.373622F;
            this.textBox73.Width = 0.4019685F;
            // 
            // textBox75
            // 
            this.textBox75.DataField = "ITEM62";
            this.textBox75.Height = 0.2188976F;
            this.textBox75.Left = 0.02480309F;
            this.textBox75.Name = "textBox75";
            this.textBox75.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox75.Tag = "";
            this.textBox75.Text = null;
            this.textBox75.Top = 3.706628F;
            this.textBox75.Width = 0.3874016F;
            // 
            // textBox76
            // 
            this.textBox76.DataField = "ITEM63";
            this.textBox76.Height = 0.21875F;
            this.textBox76.Left = 0.5023622F;
            this.textBox76.MultiLine = false;
            this.textBox76.Name = "textBox76";
            this.textBox76.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: left; vertical-align: middle; ddo-char-set: 1";
            this.textBox76.Tag = "";
            this.textBox76.Text = null;
            this.textBox76.Top = 3.706628F;
            this.textBox76.Width = 1.47441F;
            // 
            // textBox77
            // 
            this.textBox77.DataField = "ITEM64";
            this.textBox77.Height = 0.21875F;
            this.textBox77.Left = 2.058661F;
            this.textBox77.Name = "textBox77";
            this.textBox77.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.textBox77.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox77.Tag = "";
            this.textBox77.Text = null;
            this.textBox77.Top = 3.706628F;
            this.textBox77.Width = 0.6645671F;
            // 
            // textBox78
            // 
            this.textBox78.DataField = "ITEM65";
            this.textBox78.Height = 0.21875F;
            this.textBox78.Left = 2.78937F;
            this.textBox78.Name = "textBox78";
            this.textBox78.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.textBox78.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox78.Tag = "";
            this.textBox78.Text = null;
            this.textBox78.Top = 3.706628F;
            this.textBox78.Width = 0.7480319F;
            // 
            // textBox79
            // 
            this.textBox79.DataField = "ITEM66";
            this.textBox79.Height = 0.21875F;
            this.textBox79.Left = 3.611418F;
            this.textBox79.Name = "textBox79";
            this.textBox79.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.textBox79.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox79.Tag = "";
            this.textBox79.Text = null;
            this.textBox79.Top = 3.706628F;
            this.textBox79.Width = 1.016142F;
            // 
            // textBox82
            // 
            this.textBox82.DataField = "ITEM67";
            this.textBox82.Height = 0.2188976F;
            this.textBox82.Left = 0.02480309F;
            this.textBox82.Name = "textBox82";
            this.textBox82.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox82.Tag = "";
            this.textBox82.Text = null;
            this.textBox82.Top = 4.040094F;
            this.textBox82.Width = 0.3874016F;
            // 
            // textBox83
            // 
            this.textBox83.DataField = "ITEM68";
            this.textBox83.Height = 0.21875F;
            this.textBox83.Left = 0.5023622F;
            this.textBox83.MultiLine = false;
            this.textBox83.Name = "textBox83";
            this.textBox83.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: left; vertical-align: middle; ddo-char-set: 1";
            this.textBox83.Tag = "";
            this.textBox83.Text = null;
            this.textBox83.Top = 4.040094F;
            this.textBox83.Width = 1.47441F;
            // 
            // textBox84
            // 
            this.textBox84.DataField = "ITEM69";
            this.textBox84.Height = 0.21875F;
            this.textBox84.Left = 2.05866F;
            this.textBox84.Name = "textBox84";
            this.textBox84.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.textBox84.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox84.Tag = "";
            this.textBox84.Text = null;
            this.textBox84.Top = 4.040094F;
            this.textBox84.Width = 0.6645671F;
            // 
            // textBox85
            // 
            this.textBox85.DataField = "ITEM70";
            this.textBox85.Height = 0.21875F;
            this.textBox85.Left = 2.789369F;
            this.textBox85.Name = "textBox85";
            this.textBox85.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.textBox85.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox85.Tag = "";
            this.textBox85.Text = null;
            this.textBox85.Top = 4.040094F;
            this.textBox85.Width = 0.7480319F;
            // 
            // textBox86
            // 
            this.textBox86.DataField = "ITEM71";
            this.textBox86.Height = 0.21875F;
            this.textBox86.Left = 3.611417F;
            this.textBox86.Name = "textBox86";
            this.textBox86.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.textBox86.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox86.Tag = "";
            this.textBox86.Text = null;
            this.textBox86.Top = 4.040094F;
            this.textBox86.Width = 1.016142F;
            // 
            // textBox89
            // 
            this.textBox89.DataField = "ITEM72";
            this.textBox89.Height = 0.2188976F;
            this.textBox89.Left = 0.02480309F;
            this.textBox89.Name = "textBox89";
            this.textBox89.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox89.Tag = "";
            this.textBox89.Text = null;
            this.textBox89.Top = 4.373559F;
            this.textBox89.Width = 0.3874016F;
            // 
            // textBox90
            // 
            this.textBox90.DataField = "ITEM73";
            this.textBox90.Height = 0.21875F;
            this.textBox90.Left = 0.5023622F;
            this.textBox90.MultiLine = false;
            this.textBox90.Name = "textBox90";
            this.textBox90.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: left; vertical-align: middle; ddo-char-set: 1";
            this.textBox90.Tag = "";
            this.textBox90.Text = null;
            this.textBox90.Top = 4.373559F;
            this.textBox90.Width = 1.47441F;
            // 
            // textBox91
            // 
            this.textBox91.DataField = "ITEM74";
            this.textBox91.Height = 0.21875F;
            this.textBox91.Left = 2.058661F;
            this.textBox91.Name = "textBox91";
            this.textBox91.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.textBox91.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox91.Tag = "";
            this.textBox91.Text = null;
            this.textBox91.Top = 4.373559F;
            this.textBox91.Width = 0.6645671F;
            // 
            // textBox92
            // 
            this.textBox92.DataField = "ITEM75";
            this.textBox92.Height = 0.21875F;
            this.textBox92.Left = 2.78937F;
            this.textBox92.Name = "textBox92";
            this.textBox92.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.textBox92.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox92.Tag = "";
            this.textBox92.Text = null;
            this.textBox92.Top = 4.373559F;
            this.textBox92.Width = 0.7480319F;
            // 
            // textBox93
            // 
            this.textBox93.DataField = "ITEM76";
            this.textBox93.Height = 0.21875F;
            this.textBox93.Left = 3.611418F;
            this.textBox93.Name = "textBox93";
            this.textBox93.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.textBox93.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox93.Tag = "";
            this.textBox93.Text = null;
            this.textBox93.Top = 4.373559F;
            this.textBox93.Width = 1.016142F;
            // 
            // textBox96
            // 
            this.textBox96.DataField = "ITEM77";
            this.textBox96.Height = 0.2188976F;
            this.textBox96.Left = 0.02480309F;
            this.textBox96.Name = "textBox96";
            this.textBox96.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox96.Tag = "";
            this.textBox96.Text = null;
            this.textBox96.Top = 4.707024F;
            this.textBox96.Width = 0.3874016F;
            // 
            // textBox97
            // 
            this.textBox97.DataField = "ITEM78";
            this.textBox97.Height = 0.21875F;
            this.textBox97.Left = 0.5031496F;
            this.textBox97.MultiLine = false;
            this.textBox97.Name = "textBox97";
            this.textBox97.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: left; vertical-align: middle; ddo-char-set: 1";
            this.textBox97.Tag = "";
            this.textBox97.Text = null;
            this.textBox97.Top = 4.707024F;
            this.textBox97.Width = 1.47441F;
            // 
            // textBox98
            // 
            this.textBox98.DataField = "ITEM79";
            this.textBox98.Height = 0.21875F;
            this.textBox98.Left = 2.059449F;
            this.textBox98.Name = "textBox98";
            this.textBox98.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.textBox98.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox98.Tag = "";
            this.textBox98.Text = null;
            this.textBox98.Top = 4.707024F;
            this.textBox98.Width = 0.6645671F;
            // 
            // textBox99
            // 
            this.textBox99.DataField = "ITEM80";
            this.textBox99.Height = 0.21875F;
            this.textBox99.Left = 2.790157F;
            this.textBox99.Name = "textBox99";
            this.textBox99.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.textBox99.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox99.Tag = "";
            this.textBox99.Text = null;
            this.textBox99.Top = 4.707024F;
            this.textBox99.Width = 0.7480319F;
            // 
            // textBox100
            // 
            this.textBox100.DataField = "ITEM81";
            this.textBox100.Height = 0.21875F;
            this.textBox100.Left = 3.612205F;
            this.textBox100.Name = "textBox100";
            this.textBox100.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.textBox100.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox100.Tag = "";
            this.textBox100.Text = null;
            this.textBox100.Top = 4.707024F;
            this.textBox100.Width = 1.016142F;
            // 
            // textBox103
            // 
            this.textBox103.DataField = "ITEM82";
            this.textBox103.Height = 0.2188976F;
            this.textBox103.Left = 0.02519679F;
            this.textBox103.Name = "textBox103";
            this.textBox103.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox103.Tag = "";
            this.textBox103.Text = null;
            this.textBox103.Top = 5.040489F;
            this.textBox103.Width = 0.3874016F;
            // 
            // textBox104
            // 
            this.textBox104.DataField = "ITEM83";
            this.textBox104.Height = 0.21875F;
            this.textBox104.Left = 0.5031496F;
            this.textBox104.MultiLine = false;
            this.textBox104.Name = "textBox104";
            this.textBox104.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: left; vertical-align: middle; ddo-char-set: 1";
            this.textBox104.Tag = "";
            this.textBox104.Text = null;
            this.textBox104.Top = 5.040489F;
            this.textBox104.Width = 1.47441F;
            // 
            // textBox105
            // 
            this.textBox105.DataField = "ITEM84";
            this.textBox105.Height = 0.21875F;
            this.textBox105.Left = 2.059449F;
            this.textBox105.Name = "textBox105";
            this.textBox105.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.textBox105.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox105.Tag = "";
            this.textBox105.Text = null;
            this.textBox105.Top = 5.040489F;
            this.textBox105.Width = 0.6645671F;
            // 
            // textBox106
            // 
            this.textBox106.DataField = "ITEM85";
            this.textBox106.Height = 0.21875F;
            this.textBox106.Left = 2.790157F;
            this.textBox106.Name = "textBox106";
            this.textBox106.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.textBox106.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox106.Tag = "";
            this.textBox106.Text = null;
            this.textBox106.Top = 5.040489F;
            this.textBox106.Width = 0.7480319F;
            // 
            // textBox107
            // 
            this.textBox107.DataField = "ITEM86";
            this.textBox107.Height = 0.21875F;
            this.textBox107.Left = 3.612203F;
            this.textBox107.Name = "textBox107";
            this.textBox107.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.textBox107.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox107.Tag = "";
            this.textBox107.Text = null;
            this.textBox107.Top = 5.040489F;
            this.textBox107.Width = 1.016142F;
            // 
            // textBox110
            // 
            this.textBox110.DataField = "ITEM87";
            this.textBox110.Height = 0.2188976F;
            this.textBox110.Left = 0.02086608F;
            this.textBox110.Name = "textBox110";
            this.textBox110.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox110.Tag = "";
            this.textBox110.Text = null;
            this.textBox110.Top = 5.373953F;
            this.textBox110.Width = 0.3874016F;
            // 
            // textBox111
            // 
            this.textBox111.DataField = "ITEM88";
            this.textBox111.Height = 0.21875F;
            this.textBox111.Left = 0.5023622F;
            this.textBox111.MultiLine = false;
            this.textBox111.Name = "textBox111";
            this.textBox111.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: left; vertical-align: middle; ddo-char-set: 1";
            this.textBox111.Tag = "";
            this.textBox111.Text = null;
            this.textBox111.Top = 5.373953F;
            this.textBox111.Width = 1.47441F;
            // 
            // textBox112
            // 
            this.textBox112.DataField = "ITEM89";
            this.textBox112.Height = 0.21875F;
            this.textBox112.Left = 2.058661F;
            this.textBox112.Name = "textBox112";
            this.textBox112.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.textBox112.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox112.Tag = "";
            this.textBox112.Text = null;
            this.textBox112.Top = 5.373953F;
            this.textBox112.Width = 0.6645671F;
            // 
            // textBox113
            // 
            this.textBox113.DataField = "ITEM90";
            this.textBox113.Height = 0.21875F;
            this.textBox113.Left = 2.78937F;
            this.textBox113.Name = "textBox113";
            this.textBox113.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.textBox113.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox113.Tag = "";
            this.textBox113.Text = null;
            this.textBox113.Top = 5.373953F;
            this.textBox113.Width = 0.7480319F;
            // 
            // textBox114
            // 
            this.textBox114.DataField = "ITEM91";
            this.textBox114.Height = 0.21875F;
            this.textBox114.Left = 3.611416F;
            this.textBox114.Name = "textBox114";
            this.textBox114.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.textBox114.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox114.Tag = "";
            this.textBox114.Text = null;
            this.textBox114.Top = 5.373953F;
            this.textBox114.Width = 1.016142F;
            // 
            // textBox117
            // 
            this.textBox117.DataField = "ITEM92";
            this.textBox117.Height = 0.2188976F;
            this.textBox117.Left = 0.01968498F;
            this.textBox117.Name = "textBox117";
            this.textBox117.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox117.Tag = "";
            this.textBox117.Text = null;
            this.textBox117.Top = 5.707418F;
            this.textBox117.Width = 0.3874016F;
            // 
            // textBox118
            // 
            this.textBox118.DataField = "ITEM93";
            this.textBox118.Height = 0.21875F;
            this.textBox118.Left = 0.5023622F;
            this.textBox118.MultiLine = false;
            this.textBox118.Name = "textBox118";
            this.textBox118.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: left; vertical-align: middle; ddo-char-set: 1";
            this.textBox118.Tag = "";
            this.textBox118.Text = null;
            this.textBox118.Top = 5.707418F;
            this.textBox118.Width = 1.47441F;
            // 
            // textBox119
            // 
            this.textBox119.DataField = "ITEM94";
            this.textBox119.Height = 0.21875F;
            this.textBox119.Left = 2.058662F;
            this.textBox119.Name = "textBox119";
            this.textBox119.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.textBox119.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox119.Tag = "";
            this.textBox119.Text = null;
            this.textBox119.Top = 5.707418F;
            this.textBox119.Width = 0.6645671F;
            // 
            // textBox120
            // 
            this.textBox120.DataField = "ITEM95";
            this.textBox120.Height = 0.21875F;
            this.textBox120.Left = 2.78937F;
            this.textBox120.Name = "textBox120";
            this.textBox120.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.textBox120.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox120.Tag = "";
            this.textBox120.Text = null;
            this.textBox120.Top = 5.707418F;
            this.textBox120.Width = 0.7480319F;
            // 
            // textBox121
            // 
            this.textBox121.DataField = "ITEM96";
            this.textBox121.Height = 0.21875F;
            this.textBox121.Left = 3.611416F;
            this.textBox121.Name = "textBox121";
            this.textBox121.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.textBox121.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox121.Tag = "";
            this.textBox121.Text = null;
            this.textBox121.Top = 5.707418F;
            this.textBox121.Width = 1.016142F;
            // 
            // label7
            // 
            this.label7.Height = 2.665355F;
            this.label7.HyperLink = null;
            this.label7.Left = 4.694882F;
            this.label7.LineSpacing = 20F;
            this.label7.Name = "label7";
            this.label7.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt; text-align: center; text-justify: auto; v" +
    "ertical-align: middle; ddo-char-set: 1; ddo-font-vertical: true";
            this.label7.Text = "控除項目";
            this.label7.Top = 0.2783465F;
            this.label7.Width = 0.2287402F;
            // 
            // line3
            // 
            this.line3.Height = 0F;
            this.line3.Left = 4.814567F;
            this.line3.LineWeight = 1F;
            this.line3.Name = "line3";
            this.line3.Top = 4.529922F;
            this.line3.Width = 0.427083F;
            this.line3.X1 = 4.814567F;
            this.line3.X2 = 5.24165F;
            this.line3.Y1 = 4.529922F;
            this.line3.Y2 = 4.529922F;
            // 
            // line4
            // 
            this.line4.Height = 0F;
            this.line4.Left = 4.814567F;
            this.line4.LineWeight = 1F;
            this.line4.Name = "line4";
            this.line4.Top = 4.557087F;
            this.line4.Width = 0.427083F;
            this.line4.X1 = 4.814567F;
            this.line4.X2 = 5.24165F;
            this.line4.Y1 = 4.557087F;
            this.line4.Y2 = 4.557087F;
            // 
            // line5
            // 
            this.line5.Height = 6F;
            this.line5.Left = 0.4409449F;
            this.line5.LineWeight = 1F;
            this.line5.Name = "line5";
            this.line5.Top = 0F;
            this.line5.Width = 0F;
            this.line5.X1 = 0.4409449F;
            this.line5.X2 = 0.4409449F;
            this.line5.Y1 = 0F;
            this.line5.Y2 = 6F;
            // 
            // line6
            // 
            this.line6.Height = 6F;
            this.line6.Left = 2.032677F;
            this.line6.LineWeight = 1F;
            this.line6.Name = "line6";
            this.line6.Top = 0F;
            this.line6.Width = 0F;
            this.line6.X1 = 2.032677F;
            this.line6.X2 = 2.032677F;
            this.line6.Y1 = 0F;
            this.line6.Y2 = 6F;
            // 
            // line7
            // 
            this.line7.Height = 6F;
            this.line7.Left = 2.761811F;
            this.line7.LineWeight = 1F;
            this.line7.Name = "line7";
            this.line7.Top = 0F;
            this.line7.Width = 0F;
            this.line7.X1 = 2.761811F;
            this.line7.X2 = 2.761811F;
            this.line7.Y1 = 0F;
            this.line7.Y2 = 6F;
            // 
            // line8
            // 
            this.line8.Height = 6F;
            this.line8.Left = 3.578346F;
            this.line8.LineWeight = 1F;
            this.line8.Name = "line8";
            this.line8.Top = 0F;
            this.line8.Width = 0.0001809597F;
            this.line8.X1 = 3.578346F;
            this.line8.X2 = 3.578527F;
            this.line8.Y1 = 0F;
            this.line8.Y2 = 6F;
            // 
            // line9
            // 
            this.line9.Height = 6F;
            this.line9.Left = 4.667717F;
            this.line9.LineWeight = 1F;
            this.line9.Name = "line9";
            this.line9.Top = 0F;
            this.line9.Width = 0F;
            this.line9.X1 = 4.667717F;
            this.line9.X2 = 4.667717F;
            this.line9.Y1 = 0F;
            this.line9.Y2 = 6F;
            // 
            // line11
            // 
            this.line11.Height = 0F;
            this.line11.Left = 0.02637795F;
            this.line11.LineWeight = 1F;
            this.line11.Name = "line11";
            this.line11.Top = 0.3311024F;
            this.line11.Width = 4.640945F;
            this.line11.X1 = 0.02637795F;
            this.line11.X2 = 4.667323F;
            this.line11.Y1 = 0.3311024F;
            this.line11.Y2 = 0.3311024F;
            // 
            // line12
            // 
            this.line12.Height = 0F;
            this.line12.Left = 0.02637795F;
            this.line12.LineWeight = 1F;
            this.line12.Name = "line12";
            this.line12.Top = 0.6669292F;
            this.line12.Width = 4.640945F;
            this.line12.X1 = 0.02637795F;
            this.line12.X2 = 4.667323F;
            this.line12.Y1 = 0.6669292F;
            this.line12.Y2 = 0.6669292F;
            // 
            // line13
            // 
            this.line13.Height = 0F;
            this.line13.Left = 0.02637795F;
            this.line13.LineWeight = 1F;
            this.line13.Name = "line13";
            this.line13.Top = 1F;
            this.line13.Width = 4.640945F;
            this.line13.X1 = 0.02637795F;
            this.line13.X2 = 4.667323F;
            this.line13.Y1 = 1F;
            this.line13.Y2 = 1F;
            // 
            // line14
            // 
            this.line14.Height = 0F;
            this.line14.Left = 0.02637795F;
            this.line14.LineWeight = 1F;
            this.line14.Name = "line14";
            this.line14.Top = 1.333465F;
            this.line14.Width = 4.640945F;
            this.line14.X1 = 0.02637795F;
            this.line14.X2 = 4.667323F;
            this.line14.Y1 = 1.333465F;
            this.line14.Y2 = 1.333465F;
            // 
            // line15
            // 
            this.line15.Height = 0F;
            this.line15.Left = 0.02637795F;
            this.line15.LineWeight = 1F;
            this.line15.Name = "line15";
            this.line15.Top = 1.666929F;
            this.line15.Width = 4.640945F;
            this.line15.X1 = 0.02637795F;
            this.line15.X2 = 4.667323F;
            this.line15.Y1 = 1.666929F;
            this.line15.Y2 = 1.666929F;
            // 
            // line16
            // 
            this.line16.Height = 0F;
            this.line16.Left = 0.02637795F;
            this.line16.LineWeight = 1F;
            this.line16.Name = "line16";
            this.line16.Top = 2F;
            this.line16.Width = 4.640945F;
            this.line16.X1 = 0.02637795F;
            this.line16.X2 = 4.667323F;
            this.line16.Y1 = 2F;
            this.line16.Y2 = 2F;
            // 
            // line17
            // 
            this.line17.Height = 0F;
            this.line17.Left = 0.02637795F;
            this.line17.LineWeight = 1F;
            this.line17.Name = "line17";
            this.line17.Top = 2.333465F;
            this.line17.Width = 4.640945F;
            this.line17.X1 = 0.02637795F;
            this.line17.X2 = 4.667323F;
            this.line17.Y1 = 2.333465F;
            this.line17.Y2 = 2.333465F;
            // 
            // line18
            // 
            this.line18.Height = 0F;
            this.line18.Left = 0.02637795F;
            this.line18.LineWeight = 1F;
            this.line18.Name = "line18";
            this.line18.Top = 2.666929F;
            this.line18.Width = 4.640945F;
            this.line18.X1 = 0.02637795F;
            this.line18.X2 = 4.667323F;
            this.line18.Y1 = 2.666929F;
            this.line18.Y2 = 2.666929F;
            // 
            // line19
            // 
            this.line19.Height = 0F;
            this.line19.Left = 0.02637795F;
            this.line19.LineWeight = 1F;
            this.line19.Name = "line19";
            this.line19.Top = 3.341733F;
            this.line19.Width = 4.640945F;
            this.line19.X1 = 0.02637795F;
            this.line19.X2 = 4.667323F;
            this.line19.Y1 = 3.341733F;
            this.line19.Y2 = 3.341733F;
            // 
            // line20
            // 
            this.line20.Height = 0F;
            this.line20.Left = 0.02637795F;
            this.line20.LineWeight = 1F;
            this.line20.Name = "line20";
            this.line20.Top = 3.656299F;
            this.line20.Width = 4.640945F;
            this.line20.X1 = 0.02637795F;
            this.line20.X2 = 4.667323F;
            this.line20.Y1 = 3.656299F;
            this.line20.Y2 = 3.656299F;
            // 
            // line21
            // 
            this.line21.Height = 0F;
            this.line21.Left = 0.02637795F;
            this.line21.LineWeight = 1F;
            this.line21.Name = "line21";
            this.line21.Top = 3.989764F;
            this.line21.Width = 4.640945F;
            this.line21.X1 = 0.02637795F;
            this.line21.X2 = 4.667323F;
            this.line21.Y1 = 3.989764F;
            this.line21.Y2 = 3.989764F;
            // 
            // line22
            // 
            this.line22.Height = 0F;
            this.line22.Left = 0.02637795F;
            this.line22.LineWeight = 1F;
            this.line22.Name = "line22";
            this.line22.Top = 4.312599F;
            this.line22.Width = 4.640945F;
            this.line22.X1 = 0.02637795F;
            this.line22.X2 = 4.667323F;
            this.line22.Y1 = 4.312599F;
            this.line22.Y2 = 4.312599F;
            // 
            // line23
            // 
            this.line23.Height = 0F;
            this.line23.Left = 0.02637795F;
            this.line23.LineWeight = 1F;
            this.line23.Name = "line23";
            this.line23.Top = 4.6563F;
            this.line23.Width = 4.640945F;
            this.line23.X1 = 0.02637795F;
            this.line23.X2 = 4.667323F;
            this.line23.Y1 = 4.6563F;
            this.line23.Y2 = 4.6563F;
            // 
            // line24
            // 
            this.line24.Height = 0F;
            this.line24.Left = 0.02637795F;
            this.line24.LineWeight = 1F;
            this.line24.Name = "line24";
            this.line24.Top = 4.989764F;
            this.line24.Width = 4.640945F;
            this.line24.X1 = 0.02637795F;
            this.line24.X2 = 4.667323F;
            this.line24.Y1 = 4.989764F;
            this.line24.Y2 = 4.989764F;
            // 
            // line25
            // 
            this.line25.Height = 0F;
            this.line25.Left = 0.02637795F;
            this.line25.LineWeight = 1F;
            this.line25.Name = "line25";
            this.line25.Top = 5.322835F;
            this.line25.Width = 4.640945F;
            this.line25.X1 = 0.02637795F;
            this.line25.X2 = 4.667323F;
            this.line25.Y1 = 5.322835F;
            this.line25.Y2 = 5.322835F;
            // 
            // line26
            // 
            this.line26.Height = 0F;
            this.line26.Left = 0.02637795F;
            this.line26.LineWeight = 1F;
            this.line26.Name = "line26";
            this.line26.Top = 5.6563F;
            this.line26.Width = 4.640945F;
            this.line26.X1 = 0.02637795F;
            this.line26.X2 = 4.667323F;
            this.line26.Y1 = 5.6563F;
            this.line26.Y2 = 5.6563F;
            // 
            // line27
            // 
            this.line27.Height = 0F;
            this.line27.Left = 0.02637795F;
            this.line27.LineWeight = 1F;
            this.line27.Name = "line27";
            this.line27.Top = 3.008268F;
            this.line27.Width = 4.640945F;
            this.line27.X1 = 0.02637795F;
            this.line27.X2 = 4.667323F;
            this.line27.Y1 = 3.008268F;
            this.line27.Y2 = 3.008268F;
            // 
            // line28
            // 
            this.line28.Height = 0F;
            this.line28.Left = 4.667323F;
            this.line28.LineWeight = 1F;
            this.line28.Name = "line28";
            this.line28.Top = 0.3311024F;
            this.line28.Width = 2.498032F;
            this.line28.X1 = 4.667323F;
            this.line28.X2 = 7.165355F;
            this.line28.Y1 = 0.3311024F;
            this.line28.Y2 = 0.3311024F;
            // 
            // line29
            // 
            this.line29.Height = 3.658662F;
            this.line29.Left = 4.923573F;
            this.line29.LineWeight = 1F;
            this.line29.Name = "line29";
            this.line29.Top = 0.3311024F;
            this.line29.Width = 5.00679E-05F;
            this.line29.X1 = 4.923623F;
            this.line29.X2 = 4.923573F;
            this.line29.Y1 = 0.3311024F;
            this.line29.Y2 = 3.989764F;
            // 
            // line30
            // 
            this.line30.Height = 0F;
            this.line30.Left = 4.923623F;
            this.line30.LineWeight = 1F;
            this.line30.Name = "line30";
            this.line30.Top = 0.6669292F;
            this.line30.Width = 2.241732F;
            this.line30.X1 = 4.923623F;
            this.line30.X2 = 7.165355F;
            this.line30.Y1 = 0.6669292F;
            this.line30.Y2 = 0.6669292F;
            // 
            // line31
            // 
            this.line31.Height = 0F;
            this.line31.Left = 4.923229F;
            this.line31.LineWeight = 1F;
            this.line31.Name = "line31";
            this.line31.Top = 1F;
            this.line31.Width = 2.242126F;
            this.line31.X1 = 4.923229F;
            this.line31.X2 = 7.165355F;
            this.line31.Y1 = 1F;
            this.line31.Y2 = 1F;
            // 
            // line32
            // 
            this.line32.Height = 0F;
            this.line32.Left = 4.923229F;
            this.line32.LineWeight = 1F;
            this.line32.Name = "line32";
            this.line32.Top = 1.333465F;
            this.line32.Width = 2.242126F;
            this.line32.X1 = 4.923229F;
            this.line32.X2 = 7.165355F;
            this.line32.Y1 = 1.333465F;
            this.line32.Y2 = 1.333465F;
            // 
            // line33
            // 
            this.line33.Height = 0F;
            this.line33.Left = 4.923229F;
            this.line33.LineWeight = 1F;
            this.line33.Name = "line33";
            this.line33.Top = 1.666929F;
            this.line33.Width = 2.242126F;
            this.line33.X1 = 4.923229F;
            this.line33.X2 = 7.165355F;
            this.line33.Y1 = 1.666929F;
            this.line33.Y2 = 1.666929F;
            // 
            // line34
            // 
            this.line34.Height = 0F;
            this.line34.Left = 4.923229F;
            this.line34.LineWeight = 1F;
            this.line34.Name = "line34";
            this.line34.Top = 2F;
            this.line34.Width = 2.242126F;
            this.line34.X1 = 4.923229F;
            this.line34.X2 = 7.165355F;
            this.line34.Y1 = 2F;
            this.line34.Y2 = 2F;
            // 
            // line35
            // 
            this.line35.Height = 0F;
            this.line35.Left = 4.923229F;
            this.line35.LineWeight = 1F;
            this.line35.Name = "line35";
            this.line35.Top = 2.333465F;
            this.line35.Width = 2.242126F;
            this.line35.X1 = 4.923229F;
            this.line35.X2 = 7.165355F;
            this.line35.Y1 = 2.333465F;
            this.line35.Y2 = 2.333465F;
            // 
            // line36
            // 
            this.line36.Height = 0F;
            this.line36.Left = 4.923229F;
            this.line36.LineWeight = 1F;
            this.line36.Name = "line36";
            this.line36.Top = 2.666929F;
            this.line36.Width = 2.242126F;
            this.line36.X1 = 4.923229F;
            this.line36.X2 = 7.165355F;
            this.line36.Y1 = 2.666929F;
            this.line36.Y2 = 2.666929F;
            // 
            // line37
            // 
            this.line37.Height = 0F;
            this.line37.Left = 4.923229F;
            this.line37.LineWeight = 1F;
            this.line37.Name = "line37";
            this.line37.Top = 3.989764F;
            this.line37.Width = 2.242126F;
            this.line37.X1 = 4.923229F;
            this.line37.X2 = 7.165355F;
            this.line37.Y1 = 3.989764F;
            this.line37.Y2 = 3.989764F;
            // 
            // line38
            // 
            this.line38.Height = 0F;
            this.line38.Left = 4.667717F;
            this.line38.LineWeight = 1F;
            this.line38.Name = "line38";
            this.line38.Top = 4.323229F;
            this.line38.Width = 2.497638F;
            this.line38.X1 = 4.667717F;
            this.line38.X2 = 7.165355F;
            this.line38.Y1 = 4.323229F;
            this.line38.Y2 = 4.323229F;
            // 
            // line39
            // 
            this.line39.Height = 0F;
            this.line39.Left = 0.02637795F;
            this.line39.LineWeight = 1F;
            this.line39.Name = "line39";
            this.line39.Top = 6F;
            this.line39.Width = 7.064173F;
            this.line39.X1 = 0.02637795F;
            this.line39.X2 = 7.090551F;
            this.line39.Y1 = 6F;
            this.line39.Y2 = 6F;
            // 
            // line40
            // 
            this.line40.Height = 0.3311024F;
            this.line40.Left = 5.820079F;
            this.line40.LineWeight = 1F;
            this.line40.Name = "line40";
            this.line40.Top = 0F;
            this.line40.Width = 9.536743E-07F;
            this.line40.X1 = 5.820079F;
            this.line40.X2 = 5.82008F;
            this.line40.Y1 = 0F;
            this.line40.Y2 = 0.3311024F;
            // 
            // line41
            // 
            this.line41.Height = 3.994094F;
            this.line41.Left = 5.919292F;
            this.line41.LineWeight = 1F;
            this.line41.Name = "line41";
            this.line41.Top = 0.3291337F;
            this.line41.Width = 0F;
            this.line41.X1 = 5.919292F;
            this.line41.X2 = 5.919292F;
            this.line41.Y1 = 0.3291337F;
            this.line41.Y2 = 4.323228F;
            // 
            // txtTekiyoKoza
            // 
            this.txtTekiyoKoza.CanGrow = false;
            this.txtTekiyoKoza.DataField = "ITEM121";
            this.txtTekiyoKoza.Height = 0.21875F;
            this.txtTekiyoKoza.Left = 4.76378F;
            this.txtTekiyoKoza.Name = "txtTekiyoKoza";
            this.txtTekiyoKoza.Style = resources.GetString("txtTekiyoKoza.Style");
            this.txtTekiyoKoza.Tag = "";
            this.txtTekiyoKoza.Text = "ITEM121\r\n";
            this.txtTekiyoKoza.Top = 5.592914F;
            this.txtTekiyoKoza.Width = 2.259843F;
            // 
            // textBox11
            // 
            this.textBox11.DataField = "ITEM98";
            this.textBox11.Height = 0.21875F;
            this.textBox11.Left = 5.937008F;
            this.textBox11.Name = "textBox11";
            this.textBox11.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.textBox11.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox11.Tag = "";
            this.textBox11.Text = "ITEM98";
            this.textBox11.Top = 0.3929134F;
            this.textBox11.Width = 1.122047F;
            // 
            // textBox53
            // 
            this.textBox53.DataField = "ITEM108";
            this.textBox53.Height = 0.21875F;
            this.textBox53.Left = 5.937008F;
            this.textBox53.Name = "textBox53";
            this.textBox53.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.textBox53.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox53.Tag = "";
            this.textBox53.Text = "ITEM108";
            this.textBox53.Top = 3.706693F;
            this.textBox53.Width = 1.122441F;
            // 
            // line46
            // 
            this.line46.Height = 0F;
            this.line46.Left = 4.923229F;
            this.line46.LineWeight = 1F;
            this.line46.Name = "line46";
            this.line46.Top = 3.008268F;
            this.line46.Width = 2.242126F;
            this.line46.X1 = 4.923229F;
            this.line46.X2 = 7.165355F;
            this.line46.Y1 = 3.008268F;
            this.line46.Y2 = 3.008268F;
            // 
            // line47
            // 
            this.line47.Height = 0F;
            this.line47.Left = 4.923229F;
            this.line47.LineWeight = 1F;
            this.line47.Name = "line47";
            this.line47.Top = 3.341732F;
            this.line47.Width = 2.242126F;
            this.line47.X1 = 4.923229F;
            this.line47.X2 = 7.165355F;
            this.line47.Y1 = 3.341732F;
            this.line47.Y2 = 3.341732F;
            // 
            // line48
            // 
            this.line48.Height = 0F;
            this.line48.Left = 4.923228F;
            this.line48.LineWeight = 1F;
            this.line48.Name = "line48";
            this.line48.Top = 3.667323F;
            this.line48.Width = 2.242127F;
            this.line48.X1 = 4.923228F;
            this.line48.X2 = 7.165355F;
            this.line48.Y1 = 3.667323F;
            this.line48.Y2 = 3.667323F;
            // 
            // textBox7
            // 
            this.textBox7.DataField = "ITEM104";
            this.textBox7.Height = 0.21875F;
            this.textBox7.Left = 5.937008F;
            this.textBox7.Name = "textBox7";
            this.textBox7.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.textBox7.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox7.Tag = "";
            this.textBox7.Text = "ITEM104";
            this.textBox7.Top = 2.393701F;
            this.textBox7.Width = 1.122441F;
            // 
            // textBox74
            // 
            this.textBox74.DataField = "ITEM105";
            this.textBox74.Height = 0.21875F;
            this.textBox74.Left = 5.937008F;
            this.textBox74.Name = "textBox74";
            this.textBox74.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.textBox74.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox74.Tag = "";
            this.textBox74.Text = "ITEM105";
            this.textBox74.Top = 2.725197F;
            this.textBox74.Width = 1.122441F;
            // 
            // textBox81
            // 
            this.textBox81.DataField = "ITEM106";
            this.textBox81.Height = 0.21875F;
            this.textBox81.Left = 5.937008F;
            this.textBox81.Name = "textBox81";
            this.textBox81.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.textBox81.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox81.Tag = "";
            this.textBox81.Text = "ITEM106";
            this.textBox81.Top = 3.058662F;
            this.textBox81.Width = 1.122441F;
            // 
            // textBox87
            // 
            this.textBox87.DataField = "ITEM107";
            this.textBox87.Height = 0.21875F;
            this.textBox87.Left = 5.937008F;
            this.textBox87.Name = "textBox87";
            this.textBox87.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.textBox87.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; vertical-align: middle; ddo-char-set: 1";
            this.textBox87.Tag = "";
            this.textBox87.Text = "ITEM107";
            this.textBox87.Top = 3.373229F;
            this.textBox87.Width = 1.122441F;
            // 
            // textBox88
            // 
            this.textBox88.DataField = "ITEM116";
            this.textBox88.Height = 0.21875F;
            this.textBox88.Left = 5.037008F;
            this.textBox88.Name = "textBox88";
            this.textBox88.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: left; vertical-align: middle; ddo-char-set: 1";
            this.textBox88.Tag = "";
            this.textBox88.Text = "控除項目７";
            this.textBox88.Top = 2.393701F;
            this.textBox88.Width = 0.8342521F;
            // 
            // textBox94
            // 
            this.textBox94.DataField = "ITEM117";
            this.textBox94.Height = 0.21875F;
            this.textBox94.Left = 5.037008F;
            this.textBox94.Name = "textBox94";
            this.textBox94.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: left; vertical-align: middle; ddo-char-set: 1";
            this.textBox94.Tag = "";
            this.textBox94.Text = "控除項目８";
            this.textBox94.Top = 2.725197F;
            this.textBox94.Width = 0.8342521F;
            // 
            // textBox95
            // 
            this.textBox95.DataField = "ITEM118";
            this.textBox95.Height = 0.21875F;
            this.textBox95.Left = 5.037008F;
            this.textBox95.Name = "textBox95";
            this.textBox95.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: left; vertical-align: middle; ddo-char-set: 1";
            this.textBox95.Tag = "";
            this.textBox95.Text = "控除項目９";
            this.textBox95.Top = 3.058662F;
            this.textBox95.Width = 0.7897637F;
            // 
            // textBox101
            // 
            this.textBox101.DataField = "ITEM119";
            this.textBox101.Height = 0.21875F;
            this.textBox101.Left = 5.037008F;
            this.textBox101.Name = "textBox101";
            this.textBox101.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: left; vertical-align: middle; ddo-char-set: 1";
            this.textBox101.Tag = "";
            this.textBox101.Text = "控除項目10";
            this.textBox101.Top = 3.373229F;
            this.textBox101.Width = 0.8342521F;
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            // 
            // groupHeader1
            // 
            this.groupHeader1.DataField = "ITEM04";
            this.groupHeader1.Height = 0F;
            this.groupHeader1.Name = "groupHeader1";
            this.groupHeader1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
            // 
            // groupFooter1
            // 
            this.groupFooter1.Height = 0F;
            this.groupFooter1.Name = "groupFooter1";
            // 
            // groupHeader2
            // 
            this.groupHeader2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.label16,
            this.label4,
            this.label5,
            this.textBox1,
            this.textBox6,
            this.label1,
            this.label2,
            this.label3,
            this.line1,
            this.line2,
            this.label19,
            this.textBox52,
            this.label20,
            this.textBox80,
            this.line10,
            this.crossSectionBox1,
            this.textBox5,
            this.lblInfo,
            this.line42,
            this.line43,
            this.line44,
            this.line45,
            this.label6,
            this.textBox60,
            this.label21,
            this.label22,
            this.textBox102,
            this.ラベル6});
            this.groupHeader2.DataField = "ITEM06";
            this.groupHeader2.Height = 2.18819F;
            this.groupHeader2.Name = "groupHeader2";
            this.groupHeader2.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
            // 
            // label16
            // 
            this.label16.Height = 0.1979167F;
            this.label16.HyperLink = null;
            this.label16.Left = 5.714961F;
            this.label16.Name = "label16";
            this.label16.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; ddo-char-set: 128";
            this.label16.Text = "(            )";
            this.label16.Top = 1.423622F;
            this.label16.Width = 1.092126F;
            // 
            // label4
            // 
            this.label4.Height = 0.3334646F;
            this.label4.HyperLink = null;
            this.label4.Left = 2.761811F;
            this.label4.Name = "label4";
            this.label4.Style = "background-color: Navy; color: #E0E0E0; font-family: ＭＳ 明朝; font-size: 12pt; text" +
    "-align: center; vertical-align: middle";
            this.label4.Text = "単 価";
            this.label4.Top = 1.854725F;
            this.label4.Width = 0.8165357F;
            // 
            // label5
            // 
            this.label5.Height = 0.3334646F;
            this.label5.HyperLink = null;
            this.label5.Left = 3.578347F;
            this.label5.Name = "label5";
            this.label5.Style = "background-color: Navy; color: #E0E0E0; font-family: ＭＳ 明朝; font-size: 12pt; text" +
    "-align: center; vertical-align: middle";
            this.label5.Text = "金  額";
            this.label5.Top = 1.854725F;
            this.label5.Width = 1.088976F;
            // 
            // textBox1
            // 
            this.textBox1.DataField = "ITEM05";
            this.textBox1.Height = 0.21875F;
            this.textBox1.Left = 0.361811F;
            this.textBox1.Name = "textBox1";
            this.textBox1.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: left; ddo-char-set: 1";
            this.textBox1.Tag = "";
            this.textBox1.Text = "ITEM05";
            this.textBox1.Top = 1.402756F;
            this.textBox1.Width = 2.0563F;
            // 
            // textBox6
            // 
            this.textBox6.DataField = "ITEM01";
            this.textBox6.Height = 0.1875F;
            this.textBox6.Left = 5.426772F;
            this.textBox6.Name = "textBox6";
            this.textBox6.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: center; ddo-char-set: 1";
            this.textBox6.Tag = "";
            this.textBox6.Text = "ITEM01";
            this.textBox6.Top = 1.231102F;
            this.textBox6.Width = 1.531496F;
            // 
            // label1
            // 
            this.label1.Height = 0.3334646F;
            this.label1.HyperLink = null;
            this.label1.Left = 0.02519685F;
            this.label1.Name = "label1";
            this.label1.Style = "background-color: Navy; color: #E0E0E0; font-family: ＭＳ 明朝; font-size: 12pt; text" +
    "-align: center; vertical-align: middle";
            this.label1.Text = "ｺｰﾄﾞ";
            this.label1.Top = 1.854725F;
            this.label1.Width = 0.4287402F;
            // 
            // label2
            // 
            this.label2.Height = 0.3334646F;
            this.label2.HyperLink = null;
            this.label2.Left = 0.4409449F;
            this.label2.Name = "label2";
            this.label2.Style = "background-color: Navy; color: #E0E0E0; font-family: ＭＳ 明朝; font-size: 12pt; text" +
    "-align: center; vertical-align: middle";
            this.label2.Text = "魚        種";
            this.label2.Top = 1.854725F;
            this.label2.Width = 1.601969F;
            // 
            // label3
            // 
            this.label3.Height = 0.3334646F;
            this.label3.HyperLink = null;
            this.label3.Left = 2.032677F;
            this.label3.Name = "label3";
            this.label3.Style = "background-color: Navy; color: #E0E0E0; font-family: ＭＳ 明朝; font-size: 12pt; text" +
    "-align: center; vertical-align: middle";
            this.label3.Text = "数量";
            this.label3.Top = 1.854725F;
            this.label3.Width = 0.7291338F;
            // 
            // line1
            // 
            this.line1.Height = 1.072884E-06F;
            this.line1.Left = 0.3582677F;
            this.line1.LineWeight = 1F;
            this.line1.Name = "line1";
            this.line1.Top = 1.633071F;
            this.line1.Width = 2.059842F;
            this.line1.X1 = 0.3582677F;
            this.line1.X2 = 2.41811F;
            this.line1.Y1 = 1.633072F;
            this.line1.Y2 = 1.633071F;
            // 
            // line2
            // 
            this.line2.Height = 9.536743E-07F;
            this.line2.Left = 0.3582677F;
            this.line2.LineWeight = 1F;
            this.line2.Name = "line2";
            this.line2.Top = 1.662598F;
            this.line2.Width = 2.059842F;
            this.line2.X1 = 0.3582677F;
            this.line2.X2 = 2.41811F;
            this.line2.Y1 = 1.662599F;
            this.line2.Y2 = 1.662598F;
            // 
            // label19
            // 
            this.label19.Height = 0.3125F;
            this.label19.HyperLink = null;
            this.label19.Left = 2.479134F;
            this.label19.Name = "label19";
            this.label19.Style = "font-family: ＭＳ 明朝; font-size: 18pt; font-weight: bold; text-align: justify; text" +
    "-justify: distribute-all-lines";
            this.label19.Text = "水揚明細書";
            this.label19.Top = 0.2374016F;
            this.label19.Width = 2.07792F;
            // 
            // textBox52
            // 
            this.textBox52.Height = 0.1740158F;
            this.textBox52.Left = 6.23752F;
            this.textBox52.Name = "textBox52";
            this.textBox52.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.textBox52.SummaryGroup = "groupHeader1";
            this.textBox52.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox52.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.textBox52.Text = "page";
            this.textBox52.Top = 1.013025F;
            this.textBox52.Width = 0.28125F;
            // 
            // label20
            // 
            this.label20.Height = 0.1740158F;
            this.label20.HyperLink = null;
            this.label20.Left = 6.497756F;
            this.label20.Name = "label20";
            this.label20.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; ddo-char-set: 1";
            this.label20.Text = "/";
            this.label20.Top = 1.013025F;
            this.label20.Width = 0.1295276F;
            // 
            // textBox80
            // 
            this.textBox80.Height = 0.1740158F;
            this.textBox80.Left = 6.627284F;
            this.textBox80.Name = "textBox80";
            this.textBox80.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; ddo-char-set: 1";
            this.textBox80.SummaryGroup = "groupHeader1";
            this.textBox80.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.textBox80.Text = "page";
            this.textBox80.Top = 1.013025F;
            this.textBox80.Width = 0.28125F;
            // 
            // line10
            // 
            this.line10.Height = 0.333316F;
            this.line10.Left = 2.761811F;
            this.line10.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.line10.LineWeight = 1F;
            this.line10.Name = "line10";
            this.line10.Top = 1.854725F;
            this.line10.Width = 0F;
            this.line10.X1 = 2.761811F;
            this.line10.X2 = 2.761811F;
            this.line10.Y1 = 1.854725F;
            this.line10.Y2 = 2.188041F;
            // 
            // crossSectionBox1
            // 
            this.crossSectionBox1.Bottom = 2.19562F;
            this.crossSectionBox1.Left = 0F;
            this.crossSectionBox1.LineWeight = 1F;
            this.crossSectionBox1.Name = "crossSectionBox1";
            this.crossSectionBox1.Right = 7.072918F;
            this.crossSectionBox1.Top = 0.549787F;
            // 
            // textBox5
            // 
            this.textBox5.DataField = "ITEM120";
            this.textBox5.Height = 0.1682251F;
            this.textBox5.Left = 5.937009F;
            this.textBox5.Name = "textBox5";
            this.textBox5.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: right; ddo-char-set: 128";
            this.textBox5.Tag = "";
            this.textBox5.Text = "ITEM120";
            this.textBox5.Top = 1.438468F;
            this.textBox5.Width = 0.685039F;
            // 
            // lblInfo
            // 
            this.lblInfo.Height = 0.1682251F;
            this.lblInfo.HyperLink = null;
            this.lblInfo.Left = 5.826772F;
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; ddo-char-set: 128";
            this.lblInfo.Text = "旧";
            this.lblInfo.Top = 1.438468F;
            this.lblInfo.Width = 0.2362204F;
            // 
            // line42
            // 
            this.line42.Height = 0.333315F;
            this.line42.Left = 4.667717F;
            this.line42.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.line42.LineWeight = 1F;
            this.line42.Name = "line42";
            this.line42.Top = 1.854725F;
            this.line42.Width = 0F;
            this.line42.X1 = 4.667717F;
            this.line42.X2 = 4.667717F;
            this.line42.Y1 = 1.854725F;
            this.line42.Y2 = 2.18804F;
            // 
            // line43
            // 
            this.line43.Height = 0.333315F;
            this.line43.Left = 2.032677F;
            this.line43.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.line43.LineWeight = 1F;
            this.line43.Name = "line43";
            this.line43.Top = 1.854725F;
            this.line43.Width = 0F;
            this.line43.X1 = 2.032677F;
            this.line43.X2 = 2.032677F;
            this.line43.Y1 = 1.854725F;
            this.line43.Y2 = 2.18804F;
            // 
            // line44
            // 
            this.line44.Height = 0.333315F;
            this.line44.Left = 0.4409449F;
            this.line44.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.line44.LineWeight = 1F;
            this.line44.Name = "line44";
            this.line44.Top = 1.854725F;
            this.line44.Width = 0F;
            this.line44.X1 = 0.4409449F;
            this.line44.X2 = 0.4409449F;
            this.line44.Y1 = 1.854725F;
            this.line44.Y2 = 2.18804F;
            // 
            // line45
            // 
            this.line45.Height = 0.333315F;
            this.line45.Left = 3.578347F;
            this.line45.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.line45.LineWeight = 1F;
            this.line45.Name = "line45";
            this.line45.Top = 1.854725F;
            this.line45.Width = 0F;
            this.line45.X1 = 3.578347F;
            this.line45.X2 = 3.578347F;
            this.line45.Y1 = 1.854725F;
            this.line45.Y2 = 2.18804F;
            // 
            // label6
            // 
            this.label6.Height = 0.3334646F;
            this.label6.HyperLink = null;
            this.label6.Left = 4.667323F;
            this.label6.Name = "label6";
            this.label6.Style = "background-color: Navy; color: #E0E0E0; font-family: ＭＳ 明朝; font-size: 12pt; text" +
    "-align: center; vertical-align: middle";
            this.label6.Text = "控  除  内  訳";
            this.label6.Top = 1.854725F;
            this.label6.Width = 2.430709F;
            // 
            // textBox60
            // 
            this.textBox60.DataField = "ITEM04";
            this.textBox60.Height = 0.21875F;
            this.textBox60.Left = 0.1362205F;
            this.textBox60.Name = "textBox60";
            this.textBox60.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; ddo-char-set: 128";
            this.textBox60.Tag = "";
            this.textBox60.Text = "ITEM04";
            this.textBox60.Top = 1.175591F;
            this.textBox60.Width = 0.7156007F;
            // 
            // label21
            // 
            this.label21.Height = 0.1979167F;
            this.label21.HyperLink = null;
            this.label21.Left = 4.667323F;
            this.label21.Name = "label21";
            this.label21.Style = "";
            this.label21.Text = "セリ日";
            this.label21.Top = 1.225591F;
            this.label21.Width = 0.5417323F;
            // 
            // label22
            // 
            this.label22.Height = 0.1979167F;
            this.label22.HyperLink = null;
            this.label22.Left = 4.667323F;
            this.label22.Name = "label22";
            this.label22.Style = "";
            this.label22.Text = "支払予定日";
            this.label22.Top = 1.604725F;
            this.label22.Width = 1F;
            // 
            // textBox102
            // 
            this.textBox102.DataField = "ITEM127";
            this.textBox102.Height = 0.2F;
            this.textBox102.Left = 5.554331F;
            this.textBox102.Name = "textBox102";
            this.textBox102.Style = "font-family: ＭＳ 明朝; font-size: 11.25pt";
            this.textBox102.Text = "ITEM127";
            this.textBox102.Top = 1.602756F;
            this.textBox102.Width = 1.380315F;
            // 
            // groupFooter2
            // 
            this.groupFooter2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.label8,
            this.label9,
            this.label10,
            this.label11,
            this.label12,
            this.label13,
            this.label14,
            this.label15,
            this.label18,
            this.label17,
            this.picture1});
            this.groupFooter2.Height = 1.657426F;
            this.groupFooter2.Name = "groupFooter2";
            // 
            // label8
            // 
            this.label8.Height = 0.2186352F;
            this.label8.HyperLink = null;
            this.label8.Left = 0.2395833F;
            this.label8.Name = "label8";
            this.label8.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; ddo-char-set: 1";
            this.label8.Text = "本日の鮮魚代金の明細は上記のとおりです。";
            this.label8.Top = 0.08333334F;
            this.label8.Width = 2.989551F;
            // 
            // label9
            // 
            this.label9.CharacterSpacing = 1F;
            this.label9.DataField = "ITEM122";
            this.label9.Height = 0.1981628F;
            this.label9.HyperLink = null;
            this.label9.Left = 3.869291F;
            this.label9.Name = "label9";
            this.label9.Style = "font-family: ＭＳ 明朝; font-size: 14.25pt; text-align: justify; text-justify: distri" +
    "bute-all-lines; ddo-char-set: 1";
            this.label9.Text = "名護漁業協同組合";
            this.label9.Top = 0.1811024F;
            this.label9.Width = 2.531102F;
            // 
            // label10
            // 
            this.label10.Height = 0.1770833F;
            this.label10.HyperLink = null;
            this.label10.Left = 3.900394F;
            this.label10.Name = "label10";
            this.label10.Style = "font-family: ＭＳ 明朝; font-size: 9pt; text-align: justify; text-justify: distribute" +
    "-all-lines; vertical-align: middle; ddo-char-set: 1";
            this.label10.Text = "住 所";
            this.label10.Top = 0.6692914F;
            this.label10.Width = 0.4136322F;
            // 
            // label11
            // 
            this.label11.DataField = "ITEM123";
            this.label11.Height = 0.1770833F;
            this.label11.HyperLink = null;
            this.label11.Left = 4.370867F;
            this.label11.Name = "label11";
            this.label11.Style = "font-family: ＭＳ 明朝; font-size: 9pt; vertical-align: middle; ddo-char-set: 1";
            this.label11.Text = "沖縄県名護市城三丁目１番１号";
            this.label11.Top = 0.6692914F;
            this.label11.Width = 2.409843F;
            // 
            // label12
            // 
            this.label12.Height = 0.1770833F;
            this.label12.HyperLink = null;
            this.label12.Left = 3.900394F;
            this.label12.Name = "label12";
            this.label12.Style = "font-family: ＭＳ 明朝; font-size: 9pt; text-align: justify; text-justify: distribute" +
    "-all-lines; vertical-align: middle; ddo-char-set: 1";
            this.label12.Text = "TEL";
            this.label12.Top = 0.8464568F;
            this.label12.Width = 0.4136322F;
            // 
            // label13
            // 
            this.label13.Height = 0.1770833F;
            this.label13.HyperLink = null;
            this.label13.Left = 3.900394F;
            this.label13.Name = "label13";
            this.label13.Style = "font-family: ＭＳ 明朝; font-size: 9pt; text-align: justify; text-justify: distribute" +
    "-all-lines; vertical-align: middle; ddo-char-set: 1";
            this.label13.Text = "F A X";
            this.label13.Top = 1.023622F;
            this.label13.Width = 0.4136322F;
            // 
            // label14
            // 
            this.label14.DataField = "ITEM124";
            this.label14.Height = 0.1770833F;
            this.label14.HyperLink = null;
            this.label14.Left = 4.314174F;
            this.label14.Name = "label14";
            this.label14.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; vertical-align: middle; ddo-char-set: 1";
            this.label14.Text = "（０９８０）５２－２８１２";
            this.label14.Top = 0.8464568F;
            this.label14.Width = 2.466536F;
            // 
            // label15
            // 
            this.label15.Height = 0.1770833F;
            this.label15.HyperLink = null;
            this.label15.Left = 4.314174F;
            this.label15.Name = "label15";
            this.label15.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; vertical-align: middle; ddo-char-set: 1";
            this.label15.Text = "０９８―８６１－０８１９";
            this.label15.Top = 1.035433F;
            this.label15.Width = 2.466536F;
            // 
            // label18
            // 
            this.label18.DataField = "ITEM125";
            this.label18.Height = 0.1770833F;
            this.label18.HyperLink = null;
            this.label18.Left = 3.900394F;
            this.label18.Name = "label18";
            this.label18.Style = "font-family: ＭＳ 明朝; font-size: 12pt; text-align: justify; text-justify: auto; ver" +
    "tical-align: middle; ddo-char-set: 128";
            this.label18.Text = "本所";
            this.label18.Top = 0.45F;
            this.label18.Visible = false;
            this.label18.Width = 1.760236F;
            // 
            // label17
            // 
            this.label17.DataField = "ITEM126";
            this.label17.Height = 0.8161418F;
            this.label17.HyperLink = null;
            this.label17.Left = 0.2397637F;
            this.label17.Name = "label17";
            this.label17.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; ddo-char-set: 1";
            this.label17.Text = "消費税率( 8%) \\9,999円 消費税率(10%) \\9,999円";
            this.label17.Top = 0.3964567F;
            this.label17.Visible = false;
            this.label17.Width = 1.80315F;
            // 
            // picture1
            // 
            this.picture1.Height = 0.9578743F;
            this.picture1.HyperLink = null;
            this.picture1.ImageData = ((System.IO.Stream)(resources.GetObject("picture1.ImageData")));
            this.picture1.Left = 6.080316F;
            this.picture1.Name = "picture1";
            this.picture1.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Stretch;
            this.picture1.Top = 0.1811024F;
            this.picture1.Width = 0.9984257F;
            // 
            // ラベル6
            // 
            this.ラベル6.Height = 0.2291338F;
            this.ラベル6.HyperLink = null;
            this.ラベル6.Left = 2.41811F;
            this.ラベル6.Name = "ラベル6";
            this.ラベル6.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 12.75pt; font-weight: bold; text-ali" +
    "gn: left; ddo-char-set: 128";
            this.ラベル6.Tag = "";
            this.ラベル6.Text = "様";
            this.ラベル6.Top = 1.394488F;
            this.ラベル6.Width = 0.305118F;
            // 
            // HNDR1021R
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.3937008F;
            this.PageSettings.Margins.Left = 0.5905512F;
            this.PageSettings.Margins.Right = 0.1968504F;
            this.PageSettings.Margins.Top = 0.7874016F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Portrait;
            this.PageSettings.PaperHeight = 11.69291F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.267716F;
            this.PrintWidth = 7.099295F;
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.groupHeader1);
            this.Sections.Add(this.groupHeader2);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.groupFooter2);
            this.Sections.Add(this.groupFooter1);
            this.Sections.Add(this.pageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.テキスト10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox54)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox55)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox56)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox57)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox58)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox59)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox61)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox62)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox63)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox64)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox65)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox66)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox67)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox68)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox69)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox70)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox71)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox72)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox73)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox75)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox76)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox77)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox78)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox79)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox82)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox83)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox84)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox85)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox86)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox89)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox90)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox91)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox92)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox93)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox96)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox97)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox98)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox99)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox100)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox103)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox104)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox105)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox106)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox107)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox110)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox111)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox112)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox113)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox114)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox117)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox118)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox119)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox120)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox121)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTekiyoKoza)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox74)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox81)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox87)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox88)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox94)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox95)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox101)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox80)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox60)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox102)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader groupHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter groupFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader groupHeader2;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter groupFooter2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト13;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト14;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト17;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト18;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox13;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox14;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox15;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox16;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox17;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox18;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox19;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox20;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox21;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox22;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox23;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox24;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox25;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox26;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox27;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox28;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox29;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox30;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox31;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox32;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox33;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox34;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox35;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox36;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox37;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox38;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox39;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox40;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox41;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox42;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox43;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox44;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox45;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox46;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox47;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox48;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox49;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox50;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox51;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox53;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox54;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox55;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox56;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox57;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox58;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox59;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox61;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox62;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox63;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox64;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox65;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox66;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox67;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox68;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox69;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox70;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox71;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox72;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox73;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox75;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox76;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox77;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox78;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox79;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox82;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox83;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox84;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox85;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox86;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox89;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox90;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox91;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox92;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox93;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox96;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox97;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox98;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox99;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox100;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox103;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox104;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox105;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox106;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox107;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox110;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox111;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox112;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox113;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox114;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox117;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox118;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox119;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox120;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox121;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label2;
        private GrapeCity.ActiveReports.SectionReportModel.Label label3;
        private GrapeCity.ActiveReports.SectionReportModel.Label label4;
        private GrapeCity.ActiveReports.SectionReportModel.Label label5;
        private GrapeCity.ActiveReports.SectionReportModel.Label label6;
        private GrapeCity.ActiveReports.SectionReportModel.Label label7;
        private GrapeCity.ActiveReports.SectionReportModel.Label label8;
        private GrapeCity.ActiveReports.SectionReportModel.Label label9;
        private GrapeCity.ActiveReports.SectionReportModel.Label label10;
        private GrapeCity.ActiveReports.SectionReportModel.Label label11;
        private GrapeCity.ActiveReports.SectionReportModel.Label label12;
        private GrapeCity.ActiveReports.SectionReportModel.Label label13;
        private GrapeCity.ActiveReports.SectionReportModel.Label label14;
        private GrapeCity.ActiveReports.SectionReportModel.Label label15;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
        private GrapeCity.ActiveReports.SectionReportModel.Line line2;
        private GrapeCity.ActiveReports.SectionReportModel.Label label19;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox52;
        private GrapeCity.ActiveReports.SectionReportModel.Label label20;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox80;
        private GrapeCity.ActiveReports.SectionReportModel.Line line3;
        private GrapeCity.ActiveReports.SectionReportModel.Line line4;
        private GrapeCity.ActiveReports.SectionReportModel.Line line5;
        private GrapeCity.ActiveReports.SectionReportModel.Line line6;
        private GrapeCity.ActiveReports.SectionReportModel.Line line7;
        private GrapeCity.ActiveReports.SectionReportModel.Line line8;
        private GrapeCity.ActiveReports.SectionReportModel.Line line9;
        private GrapeCity.ActiveReports.SectionReportModel.Line line10;
        private GrapeCity.ActiveReports.SectionReportModel.Line line11;
        private GrapeCity.ActiveReports.SectionReportModel.Line line12;
        private GrapeCity.ActiveReports.SectionReportModel.Line line13;
        private GrapeCity.ActiveReports.SectionReportModel.Line line14;
        private GrapeCity.ActiveReports.SectionReportModel.Line line15;
        private GrapeCity.ActiveReports.SectionReportModel.Line line16;
        private GrapeCity.ActiveReports.SectionReportModel.Line line17;
        private GrapeCity.ActiveReports.SectionReportModel.Line line18;
        private GrapeCity.ActiveReports.SectionReportModel.Line line19;
        private GrapeCity.ActiveReports.SectionReportModel.Line line20;
        private GrapeCity.ActiveReports.SectionReportModel.Line line21;
        private GrapeCity.ActiveReports.SectionReportModel.Line line22;
        private GrapeCity.ActiveReports.SectionReportModel.Line line23;
        private GrapeCity.ActiveReports.SectionReportModel.Line line24;
        private GrapeCity.ActiveReports.SectionReportModel.Line line25;
        private GrapeCity.ActiveReports.SectionReportModel.Line line26;
        private GrapeCity.ActiveReports.SectionReportModel.Line line27;
        private GrapeCity.ActiveReports.SectionReportModel.Line line28;
        private GrapeCity.ActiveReports.SectionReportModel.Line line29;
        private GrapeCity.ActiveReports.SectionReportModel.Line line30;
        private GrapeCity.ActiveReports.SectionReportModel.Line line31;
        private GrapeCity.ActiveReports.SectionReportModel.Line line32;
        private GrapeCity.ActiveReports.SectionReportModel.Line line33;
        private GrapeCity.ActiveReports.SectionReportModel.Line line34;
        private GrapeCity.ActiveReports.SectionReportModel.Line line35;
        private GrapeCity.ActiveReports.SectionReportModel.Line line36;
        private GrapeCity.ActiveReports.SectionReportModel.Line line37;
        private GrapeCity.ActiveReports.SectionReportModel.Line line38;
        private GrapeCity.ActiveReports.SectionReportModel.Line line39;
        private GrapeCity.ActiveReports.SectionReportModel.Line line40;
        private GrapeCity.ActiveReports.SectionReportModel.Line line41;
        private GrapeCity.ActiveReports.SectionReportModel.CrossSectionBox crossSectionBox1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox60;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTekiyoKoza;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox5;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblInfo;
        private GrapeCity.ActiveReports.SectionReportModel.Label label16;
        private GrapeCity.ActiveReports.SectionReportModel.Line line42;
        private GrapeCity.ActiveReports.SectionReportModel.Line line43;
        private GrapeCity.ActiveReports.SectionReportModel.Line line44;
        private GrapeCity.ActiveReports.SectionReportModel.Line line45;
        private GrapeCity.ActiveReports.SectionReportModel.Line line46;
        private GrapeCity.ActiveReports.SectionReportModel.Line line47;
        private GrapeCity.ActiveReports.SectionReportModel.Line line48;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox74;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox81;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox87;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox88;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox94;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox95;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox101;
        private GrapeCity.ActiveReports.SectionReportModel.Label label18;
        private GrapeCity.ActiveReports.SectionReportModel.Label label17;
        private GrapeCity.ActiveReports.SectionReportModel.Label label21;
        private GrapeCity.ActiveReports.SectionReportModel.Label label22;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox102;
        private GrapeCity.ActiveReports.SectionReportModel.Picture picture1;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル6;
    }
}
