﻿using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;


namespace jp.co.fsi.hn.hnmr1091
{
    /// <summary>
    /// 集計表の設定(HNMR1094)
    /// </summary>
    public partial class HNMR1094 : BasePgForm
    {
        #region プロパティ
        /// <summary>
        /// 画面上最後となるフォーカスのEnterボタン押下時処理用変数
        /// </summary>
        private bool _dtFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._dtFlg;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HNMR1094()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.InitForm();は呼び出さなくて構いません。
        /// また、このメソッド内の処理を外出しでこのクラス内にメソッド化するのは構いませんが、
        /// 原則、独自で起動時のイベント処理を実装することは禁じます。
        /// </remarks>
        protected override void InitForm()
        {
            // タイトルの表示非表示を設定
            this.lblTitle.Visible = false;
            // ボタンの表示非表示を設定
            this.btnF1.Visible = false;
            this.btnF2.Visible = false;
            this.btnF3.Visible = true;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF6.Visible = true;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;
            this.btnEnter.Visible = false;
            // ボタンの活性非活性を設定
            this.btnF1.Enabled = false;
            this.btnF2.Enabled = false;
            this.btnF3.Enabled = false;
            this.btnF4.Enabled = false;
            this.btnF5.Enabled = false;
            this.btnF6.Enabled = true;
            this.btnF7.Enabled = false;
            this.btnF8.Enabled = false;
            this.btnF9.Enabled = false;
            this.btnF10.Enabled = false;
            this.btnF11.Enabled = false;
            this.btnF12.Enabled = false;
            this.btnEnter.Enabled = false;

            this.btnF3.Location = this.btnEnter.Location;
            this.btnF6.Location = this.btnF1.Location;

            this.txtShukeihyoCd.Text = "0";

            // Enter処理を無効化
            this._dtFlg = false;

            // 集計表コードにフォーカス
            this.txtShukeihyoCd.Focus();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            switch (this.ActiveCtlNm)
            {
                default:
                    break;
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F3キー押下時処理
        /// </summary>
        public override void PressF3()
        {
            if(this.btnF3.Enabled != false)
            {
                // 全項目を再度入力値チェック
                if (!ValidateAll())
                {
                    // エラーありの場合ここで処理終了
                    return;
                }

                string msg = "削除しますか？";
                if (Msg.ConfYesNo(msg) == DialogResult.No)
                {
                    // 「いいえ」を押されたら処理終了
                    return;
                }
                this.DeleteTB_HN_SHUKEIHYO_SETTEI();
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        public override void PressF6()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            string msg;
            if (this.btnF3.Enabled == false)
            {
                msg = "登録しますか？";
            }
            else
            {
                msg = "更新しますか？";
            }

            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            if (this.btnF3.Enabled == false)
            {
                // 登録処理
                this.InsertTB_HN_SHUKEIHYO_SETTEI();
            }
            else
            {
                // 更新処理
                this.UpdateTB_HN_SHUKEIHYO_SETTEI();
            }

            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 集計表コードの値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShukeihyo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShukeihyoCd())
            {
                this.btnF3.Enabled = false;
                e.Cancel = true;
                this.txtShukeihyoCd.SelectAll();
            }
            else
            {
                // 集計コードに一致するデータがあるかを確認
                DataTable dtResult = this.GetShukeihyoSetteiData();
                if (dtResult.Rows.Count == 0)
                {
                    this.txtShukeihyoTitle.Text = "";
                    this.btnF3.Enabled = false;
                }
                else
                {
                    this.txtShukeihyoTitle.Text = dtResult.Rows[0]["Title"].ToString();
                    this.btnF3.Enabled = true;
                }
                this.txtShukeihyoTitle.Focus();
            }
        }

        /// <summary>
        /// タイトルの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShukeihyoTitle_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidtxtShukeihyoTitle())
            {
                e.Cancel = true;
                this.txtShukeihyoTitle.SelectAll();
            }
        }

        /// <summary>
        /// タイトルのEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShukeihyoTitle_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.Flg)
            {
                // 全項目を再度入力値チェック
                if (!ValidateAll())
                {
                    // エラーありの場合ここで処理終了
                    return;
                }

                string msg;
                if (this.btnF3.Enabled == false)
                {
                    msg = "登録しますか？";
                }
                else
                {
                    msg = "更新しますか？";
                }

                if (Msg.ConfYesNo(msg) == DialogResult.No)
                {
                    // 「いいえ」を押されたら処理終了
                    return;
                }

                if (this.btnF3.Enabled == false)
                {
                    // 登録処理
                    this.InsertTB_HN_SHUKEIHYO_SETTEI();
                }
                else
                {
                    // 更新処理
                    this.UpdateTB_HN_SHUKEIHYO_SETTEI();
                }

                this.DialogResult = DialogResult.OK;
                this.Close();
            }

        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 集計表コードの入力チェック
        /// </summary>
        /// <returns>true:OK/false:NG</returns>
        private bool IsValidShukeihyoCd()
        {
            // 空の場合、エラーメッセージを表示し、フォーカスを移動しない
            if (ValChk.IsEmpty(this.txtShukeihyoCd.Text))
            {
                Msg.Notice("数値を入力してください。");
                return false;
            }

            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtShukeihyoCd.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                return false;
            }

            // 0の場合、エラーメッセージを表示し、フォーカスを移動しない
            if (this.txtShukeihyoCd.Text == "0")
            {
                Msg.Notice("0より大きい数値を入力してください。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// タイトルの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidtxtShukeihyoTitle()
        {
            // 指定文字数を超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtShukeihyoTitle.Text, this.txtShukeihyoTitle.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }


        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 集計表コードのチェック
            if (!IsValidShukeihyoCd())
            {
                this.txtShukeihyoCd.Focus();
                this.txtShukeihyoCd.SelectAll();
                return false;
            }

            // タイトルのチェック
            if (!IsValidtxtShukeihyoTitle())
            {
                this.txtShukeihyoTitle.Focus();
                this.txtShukeihyoTitle.SelectAll();
                return false;
            }

            return true;
        }

        /// <summary>
        /// 集計表設定データの有無を取得
        /// </summary>
        /// <returns>集計表設定の取得したデータ</returns>
        private DataTable GetShukeihyoSetteiData()
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT");
            sql.Append(" RETSU_NM AS TITLE ");
            sql.Append("FROM");
            sql.Append(" TB_HN_RPT_RETSU_KBN ");
            sql.Append("WHERE");
            sql.Append(" KAISHA_CD = @KAISHA_CD");
            sql.Append(" AND RPT_TYPE = @RPT_TYPE");
            sql.Append(" AND RPT_NO = @RPT_NO");
            sql.Append(" AND RETSU_NO = @RETSU_NO ");
            sql.Append("ORDER BY");
            sql.Append(" RETSU_NO ASC ");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@RPT_TYPE", SqlDbType.Decimal, 1, this.Par3);
            dpc.SetParam("@RPT_NO", SqlDbType.Decimal, 1, this.InData);
            dpc.SetParam("@RETSU_NO", SqlDbType.Decimal, 4, this.txtShukeihyoCd.Text);

            DataTable dtResult = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);

            return dtResult;
        }

        /// <summary>
        /// 集計表設定のデータを削除
        /// </summary>
        private void DeleteTB_HN_SHUKEIHYO_SETTEI()
        {
            try
            {
                // トランザクション開始
                this.Dba.BeginTransaction();

                DbParamCollection whereParam = new DbParamCollection();

                // TB_集計表設定削除
                whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                whereParam.SetParam("@RPT_TYPE", SqlDbType.Decimal, 1, this.Par3);
                whereParam.SetParam("@RPT_NO", SqlDbType.Decimal, 1, this.InData);
                whereParam.SetParam("@RETSU_NO", SqlDbType.Decimal, 4, this.txtShukeihyoCd.Text);
                this.Dba.Delete("TB_HN_RPT_RETSU_KBN",
                    "KAISHA_CD = @KAISHA_CD AND RPT_TYPE = @RPT_TYPE AND RPT_NO = @RPT_NO AND RETSU_NO = @RETSU_NO",
                    whereParam);
                // TB_集計表設定明細削除
                this.Dba.Delete("TB_HN_RPT_RETSU_MEISAI",
                    "KAISHA_CD = @KAISHA_CD AND RPT_TYPE = @RPT_TYPE AND RPT_NO = @RPT_NO AND RETSU_NO = @RETSU_NO",
                    whereParam);

                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }
        }

        /// <summary>
        /// 集計表設定のデータを登録
        /// </summary>
        private void InsertTB_HN_SHUKEIHYO_SETTEI()
        {
            try
            {
                // トランザクション開始
                this.Dba.BeginTransaction();

                DbParamCollection updParam = new DbParamCollection();

                updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                updParam.SetParam("@RPT_TYPE", SqlDbType.Decimal, 1, this.Par3);
                updParam.SetParam("@RPT_NO", SqlDbType.Decimal, 1, this.InData);
                updParam.SetParam("@RETSU_NO", SqlDbType.Decimal, 4, this.txtShukeihyoCd.Text);
                updParam.SetParam("@RETSU_NM", SqlDbType.VarChar, 20, this.txtShukeihyoTitle.Text);

                this.Dba.Insert("TB_HN_RPT_RETSU_KBN", updParam);

                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }
        }

        /// <summary>
        /// 集計表設定のデータを更新
        /// </summary>
        private void UpdateTB_HN_SHUKEIHYO_SETTEI()
        {
            try
            {
                // トランザクション開始
                this.Dba.BeginTransaction();

                DbParamCollection updParam = new DbParamCollection();
                DbParamCollection whereParam = new DbParamCollection();
                string where = "";

                updParam.SetParam("@RETSU_NM", SqlDbType.VarChar, 20, this.txtShukeihyoTitle.Text);

                whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                whereParam.SetParam("@RPT_TYPE", SqlDbType.Decimal, 1, this.Par3);
                whereParam.SetParam("@RPT_NO", SqlDbType.Decimal, 1, this.InData);
                whereParam.SetParam("@RETSU_NO", SqlDbType.Decimal, 4, this.txtShukeihyoCd.Text);

                where += "KAISHA_CD = @KAISHA_CD AND ";
                where += "RPT_TYPE = @RPT_TYPE AND ";
                where += "RPT_NO = @RPT_NO AND ";
                where += "RETSU_NO = @RETSU_NO ";

                this.Dba.Update("TB_HN_RPT_RETSU_KBN", updParam, where, whereParam);

                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }
        }
        #endregion

    }
}
