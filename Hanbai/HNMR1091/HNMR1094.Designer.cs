﻿namespace jp.co.fsi.hn.hnmr1091
{
    partial class HNMR1094
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnEnter = new System.Windows.Forms.Button();
            this.txtShukeihyoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShukeihyoCd = new System.Windows.Forms.Label();
            this.gbxCondition = new System.Windows.Forms.GroupBox();
            this.txtShukeihyoTitle = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShukeihyoTitle = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.gbxCondition.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Size = new System.Drawing.Size(254, 23);
            this.lblTitle.Text = "売上集計表検索";
            // 
            // btnEsc
            // 
            this.btnEsc.Location = new System.Drawing.Point(3, 49);
            // 
            // btnF1
            // 
            this.btnF1.Location = new System.Drawing.Point(132, 49);
            this.btnF1.Text = "F1";
            this.btnF1.Visible = false;
            // 
            // btnF2
            // 
            this.btnF2.Location = new System.Drawing.Point(132, 49);
            // 
            // btnF3
            // 
            this.btnF3.Location = new System.Drawing.Point(196, 49);
            this.btnF3.Text = "F3\r\n\r\n削除";
            this.btnF3.Visible = false;
            // 
            // btnF4
            // 
            this.btnF4.Location = new System.Drawing.Point(196, 49);
            this.btnF4.Visible = false;
            // 
            // btnF5
            // 
            this.btnF5.Location = new System.Drawing.Point(322, 49);
            this.btnF5.Text = "F5";
            this.btnF5.Visible = false;
            // 
            // btnF7
            // 
            this.btnF7.Location = new System.Drawing.Point(452, 49);
            this.btnF7.Visible = false;
            // 
            // btnF6
            // 
            this.btnF6.Location = new System.Drawing.Point(387, 49);
            this.btnF6.Text = "F6\r\n\r\n登録";
            this.btnF6.Visible = false;
            // 
            // btnF8
            // 
            this.btnF8.Location = new System.Drawing.Point(516, 49);
            this.btnF8.Visible = false;
            // 
            // btnF9
            // 
            this.btnF9.Location = new System.Drawing.Point(580, 49);
            this.btnF9.Visible = false;
            // 
            // btnF12
            // 
            this.btnF12.Location = new System.Drawing.Point(772, 49);
            this.btnF12.Text = "F12";
            this.btnF12.Visible = false;
            // 
            // btnF11
            // 
            this.btnF11.Location = new System.Drawing.Point(708, 49);
            // 
            // btnF10
            // 
            this.btnF10.Location = new System.Drawing.Point(644, 49);
            this.btnF10.Visible = false;
            // 
            // pnlDebug
            // 
            this.pnlDebug.Controls.Add(this.btnEnter);
            this.pnlDebug.Location = new System.Drawing.Point(6, 67);
            this.pnlDebug.Size = new System.Drawing.Size(262, 100);
            this.pnlDebug.Controls.SetChildIndex(this.btnEnter, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF5, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF6, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF7, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF8, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF4, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF9, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF3, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF10, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF2, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF11, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF1, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF12, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnEsc, 0);
            // 
            // btnEnter
            // 
            this.btnEnter.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnEnter.Location = new System.Drawing.Point(67, 49);
            this.btnEnter.Name = "btnEnter";
            this.btnEnter.Size = new System.Drawing.Size(65, 45);
            this.btnEnter.TabIndex = 905;
            this.btnEnter.TabStop = false;
            this.btnEnter.Text = "Enter\r\n\r\n決定";
            this.btnEnter.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnEnter.UseVisualStyleBackColor = true;
            // 
            // txtShukeihyoCd
            // 
            this.txtShukeihyoCd.AutoSizeFromLength = true;
            this.txtShukeihyoCd.DisplayLength = null;
            this.txtShukeihyoCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShukeihyoCd.Location = new System.Drawing.Point(114, 9);
            this.txtShukeihyoCd.MaxLength = 4;
            this.txtShukeihyoCd.Name = "txtShukeihyoCd";
            this.txtShukeihyoCd.Size = new System.Drawing.Size(48, 20);
            this.txtShukeihyoCd.TabIndex = 1;
            this.txtShukeihyoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShukeihyoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtShukeihyo_Validating);
            // 
            // lblShukeihyoCd
            // 
            this.lblShukeihyoCd.BackColor = System.Drawing.Color.Silver;
            this.lblShukeihyoCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShukeihyoCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShukeihyoCd.Location = new System.Drawing.Point(12, 9);
            this.lblShukeihyoCd.Name = "lblShukeihyoCd";
            this.lblShukeihyoCd.Size = new System.Drawing.Size(100, 20);
            this.lblShukeihyoCd.TabIndex = 0;
            this.lblShukeihyoCd.Text = "設定コード";
            this.lblShukeihyoCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // gbxCondition
            // 
            this.gbxCondition.Controls.Add(this.txtShukeihyoTitle);
            this.gbxCondition.Controls.Add(this.lblShukeihyoTitle);
            this.gbxCondition.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxCondition.Location = new System.Drawing.Point(11, 31);
            this.gbxCondition.Name = "gbxCondition";
            this.gbxCondition.Size = new System.Drawing.Size(256, 71);
            this.gbxCondition.TabIndex = 2;
            this.gbxCondition.TabStop = false;
            // 
            // txtShukeihyoTitle
            // 
            this.txtShukeihyoTitle.AutoSizeFromLength = true;
            this.txtShukeihyoTitle.DisplayLength = null;
            this.txtShukeihyoTitle.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShukeihyoTitle.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtShukeihyoTitle.Location = new System.Drawing.Point(102, 16);
            this.txtShukeihyoTitle.MaxLength = 20;
            this.txtShukeihyoTitle.Name = "txtShukeihyoTitle";
            this.txtShukeihyoTitle.Size = new System.Drawing.Size(146, 20);
            this.txtShukeihyoTitle.TabIndex = 1;
            this.txtShukeihyoTitle.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtShukeihyoTitle_KeyDown);
            this.txtShukeihyoTitle.Validating += new System.ComponentModel.CancelEventHandler(this.txtShukeihyoTitle_Validating);
            // 
            // lblShukeihyoTitle
            // 
            this.lblShukeihyoTitle.BackColor = System.Drawing.Color.Silver;
            this.lblShukeihyoTitle.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShukeihyoTitle.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShukeihyoTitle.Location = new System.Drawing.Point(10, 16);
            this.lblShukeihyoTitle.Name = "lblShukeihyoTitle";
            this.lblShukeihyoTitle.Size = new System.Drawing.Size(90, 20);
            this.lblShukeihyoTitle.TabIndex = 0;
            this.lblShukeihyoTitle.Text = "タ イ ト ル";
            this.lblShukeihyoTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // HNMR1094
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(279, 168);
            this.Controls.Add(this.gbxCondition);
            this.Controls.Add(this.txtShukeihyoCd);
            this.Controls.Add(this.lblShukeihyoCd);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "HNMR1094";
            this.ShowFButton = true;
            this.Text = "項目タイトルの設定";
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblShukeihyoCd, 0);
            this.Controls.SetChildIndex(this.txtShukeihyoCd, 0);
            this.Controls.SetChildIndex(this.gbxCondition, 0);
            this.pnlDebug.ResumeLayout(false);
            this.gbxCondition.ResumeLayout(false);
            this.gbxCondition.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        protected System.Windows.Forms.Button btnEnter;
        private common.controls.FsiTextBox txtShukeihyoCd;
        private System.Windows.Forms.Label lblShukeihyoCd;
        private System.Windows.Forms.GroupBox gbxCondition;
        private common.controls.FsiTextBox txtShukeihyoTitle;
        private System.Windows.Forms.Label lblShukeihyoTitle;

    }
}