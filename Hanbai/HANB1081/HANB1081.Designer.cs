﻿namespace jp.co.fsi.han.hanb1081
{
    partial class HANB1081
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbxDate = new System.Windows.Forms.GroupBox();
            this.lblDateDay = new System.Windows.Forms.Label();
            this.lblDateMonth = new System.Windows.Forms.Label();
            this.labelDateYear = new System.Windows.Forms.Label();
            this.txtDateDay = new jp.co.fsi.common.controls.SosTextBox();
            this.txtDateYear = new jp.co.fsi.common.controls.SosTextBox();
            this.txtDateMonth = new jp.co.fsi.common.controls.SosTextBox();
            this.lblDateGengo = new System.Windows.Forms.Label();
            this.lblDate = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.gbxDate.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Text = "";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Size = new System.Drawing.Size(847, 100);
            // 
            // gbxDate
            // 
            this.gbxDate.Controls.Add(this.lblDateDay);
            this.gbxDate.Controls.Add(this.lblDateMonth);
            this.gbxDate.Controls.Add(this.labelDateYear);
            this.gbxDate.Controls.Add(this.txtDateDay);
            this.gbxDate.Controls.Add(this.txtDateYear);
            this.gbxDate.Controls.Add(this.txtDateMonth);
            this.gbxDate.Controls.Add(this.lblDateGengo);
            this.gbxDate.Controls.Add(this.lblDate);
            this.gbxDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxDate.ForeColor = System.Drawing.SystemColors.ControlText;
            this.gbxDate.Location = new System.Drawing.Point(21, 46);
            this.gbxDate.Name = "gbxDate";
            this.gbxDate.Size = new System.Drawing.Size(439, 91);
            this.gbxDate.TabIndex = 3;
            this.gbxDate.TabStop = false;
            this.gbxDate.Text = "セ リ 日";
            // 
            // lblDateDay
            // 
            this.lblDateDay.BackColor = System.Drawing.Color.Silver;
            this.lblDateDay.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateDay.Location = new System.Drawing.Point(262, 37);
            this.lblDateDay.Name = "lblDateDay";
            this.lblDateDay.Size = new System.Drawing.Size(20, 18);
            this.lblDateDay.TabIndex = 7;
            this.lblDateDay.Text = "日";
            this.lblDateDay.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateMonth
            // 
            this.lblDateMonth.BackColor = System.Drawing.Color.Silver;
            this.lblDateMonth.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateMonth.Location = new System.Drawing.Point(212, 38);
            this.lblDateMonth.Name = "lblDateMonth";
            this.lblDateMonth.Size = new System.Drawing.Size(15, 19);
            this.lblDateMonth.TabIndex = 5;
            this.lblDateMonth.Text = "月";
            this.lblDateMonth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelDateYear
            // 
            this.labelDateYear.BackColor = System.Drawing.Color.Silver;
            this.labelDateYear.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.labelDateYear.Location = new System.Drawing.Point(162, 36);
            this.labelDateYear.Name = "labelDateYear";
            this.labelDateYear.Size = new System.Drawing.Size(17, 21);
            this.labelDateYear.TabIndex = 3;
            this.labelDateYear.Text = "年";
            this.labelDateYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDateDay
            // 
            this.txtDateDay.AutoSizeFromLength = false;
            this.txtDateDay.DisplayLength = null;
            this.txtDateDay.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateDay.Location = new System.Drawing.Point(230, 36);
            this.txtDateDay.MaxLength = 2;
            this.txtDateDay.Name = "txtDateDay";
            this.txtDateDay.Size = new System.Drawing.Size(30, 20);
            this.txtDateDay.TabIndex = 6;
            this.txtDateDay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateDay.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateDay_Validating);
            // 
            // txtDateYear
            // 
            this.txtDateYear.AutoSizeFromLength = false;
            this.txtDateYear.DisplayLength = null;
            this.txtDateYear.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateYear.Location = new System.Drawing.Point(130, 37);
            this.txtDateYear.MaxLength = 2;
            this.txtDateYear.Name = "txtDateYear";
            this.txtDateYear.Size = new System.Drawing.Size(30, 20);
            this.txtDateYear.TabIndex = 2;
            this.txtDateYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateYear.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateYear_Validating);
            // 
            // txtDateMonth
            // 
            this.txtDateMonth.AutoSizeFromLength = false;
            this.txtDateMonth.DisplayLength = null;
            this.txtDateMonth.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateMonth.Location = new System.Drawing.Point(180, 37);
            this.txtDateMonth.MaxLength = 2;
            this.txtDateMonth.Name = "txtDateMonth";
            this.txtDateMonth.Size = new System.Drawing.Size(30, 20);
            this.txtDateMonth.TabIndex = 4;
            this.txtDateMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateMonth.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateMonth_Validating);
            // 
            // lblDateGengo
            // 
            this.lblDateGengo.BackColor = System.Drawing.Color.Silver;
            this.lblDateGengo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDateGengo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateGengo.Location = new System.Drawing.Point(87, 35);
            this.lblDateGengo.Name = "lblDateGengo";
            this.lblDateGengo.Size = new System.Drawing.Size(41, 23);
            this.lblDateGengo.TabIndex = 1;
            this.lblDateGengo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDate
            // 
            this.lblDate.BackColor = System.Drawing.Color.Silver;
            this.lblDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDate.Location = new System.Drawing.Point(23, 33);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(264, 27);
            this.lblDate.TabIndex = 0;
            this.lblDate.Text = "セ リ 日";
            this.lblDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // HANB1081
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 638);
            this.Controls.Add(this.gbxDate);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "HANB1081";
            this.Text = "";
            this.Controls.SetChildIndex(this.gbxDate, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.pnlDebug.ResumeLayout(false);
            this.gbxDate.ResumeLayout(false);
            this.gbxDate.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbxDate;
        private jp.co.fsi.common.controls.SosTextBox txtDateYear;
        private System.Windows.Forms.Label lblDateGengo;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.Label lblDateDay;
        private System.Windows.Forms.Label lblDateMonth;
        private System.Windows.Forms.Label labelDateYear;
        private jp.co.fsi.common.controls.SosTextBox txtDateDay;
        private jp.co.fsi.common.controls.SosTextBox txtDateMonth;

    }
}