﻿using System;
using System.ComponentModel;
using System.Data;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Collections.Generic;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;
using jp.co.fsi.common.constants;
using System.Diagnostics;
using System.Security.Policy;
using System.Net;

namespace jp.co.fsi.han.hanb1081
{
    /// <summary>
    /// データ連携　読込(HANB1081)
    /// </summary>
    public partial class HANB1081 : BasePgForm
    {
        #region 構造体
        /// <summary>
        /// 数量・金額情報
        /// </summary>
        private struct Summary
        {
            public decimal KingakuKei;
            public decimal KojoKomoku1;
            public decimal KojoKomoku2;
            public decimal GyogyoTesuryo;
            public decimal Hakodai;

            /// <summary>
            /// 金額をクリア
            /// </summary>
            public void Clear()
            {
                KingakuKei = 0;
                KojoKomoku1 = 0;
                KojoKomoku2 = 0;
                GyogyoTesuryo = 0;
                Hakodai = 0;
            }
        }
        #endregion

        #region 定数
        private DateTime today = DateTime.Today; // 本日日付
        private decimal consumptionTax = 1; // 消費税率初期値（バグがわかりやすいように1%を設定している
        // DB登録項目での固定値
        protected const int DUMMY_DENPYO_KUBUN = 3;     // 伝票区分
        #endregion

        #region プロパティ
        /// <summary>
        /// 画面上最後となるフォーカスのEnterボタン押下時処理用変数
        /// </summary>
        private bool _dtFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._dtFlg;
            }
        }

        private System.Net.NetworkCredential ftpCredential;
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HANB1081()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            string[] jpDate = Util.ConvJpDate(DateTime.Now, this.Dba);
            lblDateGengo.Text = jpDate[0];
            txtDateYear.Text = jpDate[2];
            txtDateMonth.Text = jpDate[3];
            txtDateDay.Text = jpDate[4];

            // 初期フォーカス
            txtDateYear.Focus();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 日付、船主コード１・２に
            // フォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtDateYear":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;

            switch (this.ActiveCtlNm)
            {
                case "txtDateYear":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom("COMC9011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.com.comc9011.COMC9011");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblDateGengo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.lblDateGengo.Text = result[1];

                                // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
                                DateTime tmpDate = Util.ConvAdDate(this.lblDateGengo.Text, this.txtDateYear.Text,
                                    this.txtDateMonth.Text, "1", this.Dba);
                                int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);
                                if (Util.ToInt(this.txtDateDay.Text) > lastDayInMonth)
                                {
                                    this.txtDateDay.Text = Util.ToString(lastDayInMonth);
                                }

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                string[] arrJpDate =
                                    Util.FixJpDate(this.lblDateGengo.Text,
                                        this.txtDateYear.Text,
                                        this.txtDateMonth.Text,
                                        this.txtDateDay.Text,
                                        this.Dba);
                                this.lblDateGengo.Text = arrJpDate[0];
                                this.txtDateYear.Text = arrJpDate[2];
                                this.txtDateMonth.Text = arrJpDate[3];
                                this.txtDateDay.Text = arrJpDate[4];
                            }
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF5();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF5()
        {
            // 会計年度の凍結処理チェック 凍結されていた場合はアラートを表示し処理を行なわない
            if (Util.GetKaikeiNendoFixedFlg(this.UInfo.KaikeiNendo, this.Dba))
            {
                Msg.Error("この会計年度は凍結されています。");
                return;
            }

            // 入力日付に該当する会計年度が選択会計年度と一致するかチェック
            DateTime DENPYO_DATE = Util.ConvAdDate(this.lblDateGengo.Text, this.txtDateYear.Text,
                    this.txtDateMonth.Text, this.txtDateDay.Text, this.Dba);
            if (Util.GetKaikeiNendo(DENPYO_DATE, this.Dba) != this.UInfo.KaikeiNendo)
            {
                Msg.Error("入力日付が選択会計年度の範囲外です。");
                return;
            }

            // 選択会計年度が今年度かチェック
            DateTime today = DateTime.Today;
            if (Util.GetKaikeiNendo(today, this.Dba) != this.UInfo.KaikeiNendo)
            {
                if (Msg.ConfNmYesNo("データ連携（更新）", "選択会計年度が今年度ではありませんが、宜しいですか？") == DialogResult.No)
                {
                    return;
                }
            }

            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("データ連携（更新）", "実行しますか？") == DialogResult.Yes)
            {
                // TMP_SHIKIRI_DATA ・ TMP_SHIKIRI_MEISAI
                DataConvert();
            }

            this.txtDateDay.Focus();
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF6();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF6()
        {
            // 会計年度の凍結処理チェック 凍結されていた場合はアラートを表示し処理を行なわない
            if (Util.GetKaikeiNendoFixedFlg(this.UInfo.KaikeiNendo, this.Dba))
            {
                Msg.Error("この会計年度は凍結されています。");
                return;
            }

            DateTime DENPYO_DATE = Util.ConvAdDate(this.lblDateGengo.Text, this.txtDateYear.Text,
                    this.txtDateMonth.Text, this.txtDateDay.Text, this.Dba);
            // 会計年度の凍結処理チェック 凍結されていた場合はアラートを表示し処理を行なわない
            if (Util.GetKaikeiNendo(DENPYO_DATE, this.Dba) != this.UInfo.KaikeiNendo)
            {
                Msg.Error("入力日付が選択会計年度の範囲外です。");
                return;
            }

            DateTime today = DateTime.Today;
            // 会計年度の凍結処理チェック 凍結されていた場合はアラートを表示し処理を行なわない
            if (Util.GetKaikeiNendo(today, this.Dba) != this.UInfo.KaikeiNendo)
            {
                if (Msg.ConfNmYesNo("データ連携（読込）", "選択会計年度が今年度ではありませんが、宜しいですか？") == DialogResult.No)
                {
                    return;
                }
            }

            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("データ連携（読込）", "実行しますか？") == DialogResult.Yes)
            {
                //ネットワークドライブ割り当て
                if (!NetUseStart())
                {
                    this.txtDateDay.Focus();
                    return;
                }
                // テキストファイル読込処理
                if(!ReadData())
                {
                    this.txtDateDay.Focus();
                    return;
                }

                // ネットワークドライブ切断
                if (!NetUseDelete())
                {
                    this.txtDateDay.Focus();
                    return;
                }
            }

            this.txtDateDay.Focus();
        }

        /// <summary>
        /// F7キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF7();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF7()
        {
            // 会計年度の凍結処理チェック 凍結されていた場合はアラートを表示し処理を行なわない
            if (Util.GetKaikeiNendoFixedFlg(this.UInfo.KaikeiNendo, this.Dba))
            {
                Msg.Error("この会計年度は凍結されています。");
                return;
            }

            DateTime DENPYO_DATE = Util.ConvAdDate(this.lblDateGengo.Text, this.txtDateYear.Text,
                    this.txtDateMonth.Text, this.txtDateDay.Text, this.Dba);
            // 会計年度の凍結処理チェック 凍結されていた場合はアラートを表示し処理を行なわない
            if (Util.GetKaikeiNendo(DENPYO_DATE, this.Dba) != this.UInfo.KaikeiNendo)
            {
                Msg.Error("入力日付が選択会計年度の範囲外です。");
                return;
            }

            DateTime today = DateTime.Today;
            // 会計年度の凍結処理チェック 凍結されていた場合はアラートを表示し処理を行なわない
            if (Util.GetKaikeiNendo(today, this.Dba) != this.UInfo.KaikeiNendo)
            {
                if (Msg.ConfNmYesNo("データ連携（読込）", "選択会計年度が今年度ではありませんが、宜しいですか？") == DialogResult.No)
                {
                    return;
                }
            }

            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("データ連携（読込）", "実行しますか？") == DialogResult.Yes)
            {
                // テキストファイル読込処理
                if (!ReadDataFtp())
                {
                    this.txtDateDay.Focus();
                    return;
                }
            }

            this.txtDateDay.Focus();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 和暦(年)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateYear_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidDateYear())
            {
                e.Cancel = true;
                this.txtDateYear.SelectAll();
            }
            else
            {
                CheckDate();
                SetDate();
            }
        }

        /// <summary>
        /// 和暦(月)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateMonth_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidDateMonth())
            {
                e.Cancel = true;
                this.txtDateMonth.SelectAll();
            }
            else
            {
                CheckDate();
                SetDate();
            }
        }

        /// <summary>
        /// 和暦(日)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateDay_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidDateDay())
            {
                e.Cancel = true;
                this.txtDateDay.SelectAll();
            }
            else
            {
                CheckDate();
                SetDate();
            }
        }

        /// <summary>
        /// 船主コード(至)のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFunanushiCdTo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.Flg)
            {
                // Enter処理を無効化
                this._dtFlg = false;

                // 全項目を再度入力値チェック
                if (!ValidateAll())
                {
                    // エラーありの場合ここで処理終了
                    return;
                }

                // テキストファイル読込処理
                ReadData();
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 年(自)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidDateYear()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtDateYear.Text))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }
            // 空の場合、0年として処理
            if (ValChk.IsEmpty(this.txtDateYear.Text))
            {
                this.txtDateYear.Text = "0";
            }

            return true;
        }

        /// <summary>
        /// 月(自)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidDateMonth()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtDateMonth.Text))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }
            // 空の場合、1月として処理
            if (ValChk.IsEmpty(this.txtDateMonth.Text))
            {
                this.txtDateMonth.Text = "1";
            }
            else
            {
                // 12を超える月が入力された場合、12月として処理
                if (Util.ToInt(this.txtDateMonth.Text) > 12)
                {
                    this.txtDateMonth.Text = "12";
                }
                // 1より小さい月が入力された場合、1月として処理
                else if (Util.ToInt(this.txtDateMonth.Text) < 1)
                {
                    this.txtDateMonth.Text = "1";
                }
            }

            return true;
        }

        /// <summary>
        /// 日(自)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidDateDay()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtDateDay.Text))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }

            if (ValChk.IsEmpty(this.txtDateDay.Text))
            {
                // 空の場合、1日として処理
                this.txtDateDay.Text = "1";
            }
            else
            {
                // 1より小さい日が入力された場合、1日として処理
                if (Util.ToInt(this.txtDateDay.Text) < 1)
                {
                    this.txtDateDay.Text = "1";
                }
            }

            return true;
        }

        /// <summary>
        /// 年月日(自)の月末入力チェック
        /// </summary>
        private void CheckDate()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblDateGengo.Text, this.txtDateYear.Text,
                this.txtDateMonth.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDateDay.Text) > lastDayInMonth)
            {
                this.txtDateDay.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(自)の正しい和暦への変換処理
        /// </summary>
        private void SetDate()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDate(Util.FixJpDate(this.lblDateGengo.Text, this.txtDateYear.Text,
                this.txtDateMonth.Text, this.txtDateDay.Text, this.Dba));
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 年(自)のチェック
            if (!IsValidDateYear())
            {
                this.txtDateYear.Focus();
                this.txtDateYear.SelectAll();
                return false;
            }
            // 月(自)のチェック
            if (!IsValidDateMonth())
            {
                this.txtDateMonth.Focus();
                this.txtDateMonth.SelectAll();
                return false;
            }
            // 日(自)のチェック
            if (!IsValidDateDay())
            {
                this.txtDateDay.Focus();
                this.txtDateDay.SelectAll();
                return false;
            }
            // 年月日(自)の月末入力チェック処理
            CheckDate();
            // 年月日(自)の正しい和暦への変換処理
            SetDate();

            return true;
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpDate(string[] arrJpDate)
        {
            this.lblDateGengo.Text = arrJpDate[0];
            this.txtDateYear.Text = arrJpDate[2];
            this.txtDateMonth.Text = arrJpDate[3];
            this.txtDateDay.Text = arrJpDate[4];
        }

        /// <summary>
        /// テキストファイルのデータを読み込み、データベースにインポート
        /// </summary>
        private Boolean ReadData()
        {
            #region 変数宣言
            string Url; 
            Uri dirPath1;
            Uri dirPath2;
            string stBuffer;
            string[] stArrayData;
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder sql = new StringBuilder();
            #endregion

            #region データ取得準備
            // 日付を西暦にして取得
            DateTime tmpDate = Getdate();

            Url = this.Config.LoadPgConfig(Constants.SubSys.Han, "HANB1081", "Setting", "Url");
            dirPath1 = new Uri(Url); // 基準となるパス
            dirPath2 = new Uri(dirPath1, ".\\" + tmpDate.ToString("yyyyMMdd") + "SHIKIRI.txt"); // 相対パス

            // 保存先の存在チェック
            if (!Directory.Exists(dirPath1.LocalPath))
            {
                Msg.Info("保存先フォルダが見つかりません。");
                return false;
            }
            // ファイルの存在チェック
            if (!File.Exists(dirPath2.LocalPath))
            {
                Msg.Info("txtファイルが見つかりません。");
                return false;
            }
            // データの重複チェック（同じセリ日はエラーで返す）
            // StreamReader の新しいインスタンスを生成する
            System.IO.StreamReader cReader = (new System.IO.StreamReader(dirPath2.LocalPath, System.Text.Encoding.Default));
            // ファイルを 1 行読み込む
            stBuffer = cReader.ReadLine();
            // 文字列をタブごとに区切る
            stArrayData = stBuffer.Split(' ');
            // 
            DataTable dt = GetWbSeriDate(Util.ToDate(stArrayData[1]));
            cReader.Close();
            #endregion

            try
            {
                // トランザクションの開始
                this.Dba.BeginTransaction();

                if (dt.Rows.Count > 0)
                {
                    Msg.Notice("既に精算済みです。");
                    if (Msg.ConfYesNo("指定されたセリ日のセリデータを削除します。よろしいですか？") == DialogResult.No)
                    {
                        return false;
                    }

                    #region セリデータ削除処理
                    dpc = new DbParamCollection();
                    sql = new StringBuilder();
                    sql.Append("DELETE ");
                    sql.Append("  FROM TB_HN_WB_SHIKIRIS");
                    sql.Append("  WHERE");
                    sql.Append("   KAISHA_CD = @KAISHA_CD AND");
                    sql.Append("   SERIBI = @SERIBI ");

                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                    // 検索する日付をセット
                    dpc.SetParam("@SERIBI", SqlDbType.DateTime, Util.ToDate(stArrayData[1]));
                    this.Dba.ModifyBySql(Util.ToString(sql), dpc);
                    #endregion
                }

                //テキストファイルからデータを取得
                dpc = new DbParamCollection();
                sql = new StringBuilder();
                sql.Append(" BULK INSERT TB_HN_WB_SHIKIRIS ");
                sql.Append(" FROM '" + dirPath2.LocalPath + "'");
                sql.Append(" WITH ");
                sql.Append(" ( ");
                sql.Append("   ROWTERMINATOR = '\n' ");
                sql.Append(" );  ");
                this.Dba.ModifyBySql(Util.ToString(sql), dpc);

                // トランザクションをコミット
                this.Dba.Commit();
                Msg.Info("取り込み処理が正常に終了しました。");

                // txtファイルを移動
                Microsoft.VisualBasic.FileIO.FileSystem.MoveFile(
                dirPath2.LocalPath, this.Config.LoadPgConfig(Constants.SubSys.Han, "HANB1081", "Setting", "backup") + Path.GetFileName(dirPath2.LocalPath),
                Microsoft.VisualBasic.FileIO.UIOption.AllDialogs, Microsoft.VisualBasic.FileIO.UICancelOption.DoNothing);
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            return true;
        }

        /// <summary>
        /// テキストファイルのデータを読み込み、データベースにインポート
        /// </summary>
        private Boolean ReadDataFtp()
        {
            #region 変数宣言
            string Url;
            Uri dirPath1;
            Uri dirPath2;
            string stBuffer;
            string[] stArrayData;
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder sql = new StringBuilder();
            #endregion

            #region データ取得準備
            // 日付を西暦にして取得
            DateTime tmpDate = Getdate();

            Url = this.Config.LoadPgConfig(Constants.SubSys.Han, "HANB1081", "Setting", "backup");
            dirPath1 = new Uri(Url); // 基準となるパス
            dirPath2 = new Uri(dirPath1, ".\\" + tmpDate.ToString("yyyyMMdd") + "SHIKIRI.txt"); // 相対パス

            // FTPを使用してデータ取得
            string fileName = tmpDate.ToString("yyyyMMdd") + "SHIKIRI.txt";

            // ダウンロードするファイルのURI
            Uri downloadUrl = new Uri("ftp://wx21.wadax.ne.jp/data/shikiri/" + fileName);
            // アップロードするファイルのURI
            Uri uploadUrl = new Uri("ftp://wx21.wadax.ne.jp/data/shikiri_bk/" + fileName);
            // ログインユーザー名とパスワードを設定
            ftpCredential = new System.Net.NetworkCredential("jf-nago-jp", "c4EqXtWa");

            // 保存先の存在チェック
            if (!Directory.Exists(dirPath1.LocalPath))
            {
                Msg.Info("保存先フォルダが見つかりません。");
                return false;
            }

            // FTPサーバーからダウンロード
            try
            {
                DownloadFtpFile(dirPath2.LocalPath, downloadUrl);
            }
            catch (WebException e)
            {
                if (e.Status == WebExceptionStatus.ProtocolError)
                {
                    Msg.Info("txtファイルが見つかりません。");
                    return false;
                }
            }

            // データの重複チェック（同じセリ日はエラーで返す）
            // StreamReader の新しいインスタンスを生成する
            System.IO.StreamReader cReader = (new System.IO.StreamReader(dirPath2.LocalPath, System.Text.Encoding.Default));
            // ファイルを 1 行読み込む
            stBuffer = cReader.ReadLine();
            // 文字列をタブごとに区切る
            stArrayData = stBuffer.Split(' ');

            DataTable dt = GetWbSeriDate(Util.ToDate(stArrayData[1]));
            cReader.Close();
            #endregion
            
            try
            {
                // トランザクションの開始
                this.Dba.BeginTransaction();

                if (dt.Rows.Count > 0)
                {
                    Msg.Notice("既に精算済みです。");
                    if (Msg.ConfYesNo("指定されたセリ日のセリデータを削除します。よろしいですか？") == DialogResult.No)
                    {
                        return false;
                    }

                    #region セリデータ削除処理
                    dpc = new DbParamCollection();
                    sql = new StringBuilder();
                    sql.Append("DELETE ");
                    sql.Append("  FROM TB_HN_WB_SHIKIRIS");
                    sql.Append("  WHERE");
                    sql.Append("   KAISHA_CD = @KAISHA_CD AND");
                    sql.Append("   SERIBI = @SERIBI ");

                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                    // 検索する日付をセット
                    dpc.SetParam("@SERIBI", SqlDbType.DateTime, Util.ToDate(stArrayData[1]));
                    this.Dba.ModifyBySql(Util.ToString(sql), dpc);
                    #endregion
                }

                // StreamReader の新しいインスタンスを生成する
                System.IO.StreamReader cReader2 = (new System.IO.StreamReader(dirPath2.LocalPath, System.Text.Encoding.Default));

                while (!cReader2.EndOfStream)
                {
                    // ファイルを 1 行読み込む
                    stBuffer = cReader2.ReadLine();
                    // 文字列をタブごとに区切る
                    stArrayData = stBuffer.Split(' ');

                    //テキストファイルからデータを取得
                    dpc = new DbParamCollection();
                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, Util.ToDecimal(stArrayData[0]));
                    dpc.SetParam("@SERIBI", SqlDbType.DateTime, stArrayData[1]);
                    dpc.SetParam("@SERI_NO", SqlDbType.Decimal, 9, Util.ToDecimal(stArrayData[2]));
                    dpc.SetParam("@SERI_TANTO_CD", SqlDbType.Decimal, 4, Util.ToDecimal(stArrayData[3]));
                    dpc.SetParam("@SERI_AREA", SqlDbType.Decimal, 4, Util.ToDecimal(stArrayData[4]));
                    dpc.SetParam("@YAMA_NO", SqlDbType.Decimal, 6, Util.ToDecimal(stArrayData[5]));
                    dpc.SetParam("@YAMA_EDA_NO", SqlDbType.Decimal, 4, Util.ToDecimal(stArrayData[6]));
                    dpc.SetParam("@SENSHU_CD", SqlDbType.Decimal, 9, Util.ToDecimal(stArrayData[7]));
                    dpc.SetParam("@GYOHO_CD", SqlDbType.Decimal, 9, Util.ToDecimal(stArrayData[8]));
                    dpc.SetParam("@JOTAI_CD", SqlDbType.VarChar, 30, stArrayData[9]);
                    dpc.SetParam("@GYOSHU_CD", SqlDbType.Decimal, 9, Util.ToDecimal(stArrayData[10]));
                    dpc.SetParam("@SURYO", SqlDbType.Decimal, 9,2, Util.ToDecimal(stArrayData[11]));
                    dpc.SetParam("@TANKA", SqlDbType.Decimal, 9,2, Util.ToDecimal(stArrayData[12]));
                    dpc.SetParam("@NAKAGAININ_CD", SqlDbType.Decimal, 9, Util.ToDecimal(stArrayData[13]));
                    dpc.SetParam("@HAKO_FLG", SqlDbType.Decimal, 4, Util.ToDecimal(stArrayData[14]));
                    dpc.SetParam("@HONSU", SqlDbType.Decimal, 9,2, Util.ToDecimal(stArrayData[15]));
                    dpc.SetParam("@SHORI_FLG", SqlDbType.Decimal, 4, Util.ToDecimal(stArrayData[16]));
                    dpc.SetParam("@PARENT_SERI_NO", SqlDbType.Decimal, 9, Util.ToDecimal(stArrayData[17]));
                    dpc.SetParam("@CHILD_COUNT", SqlDbType.Decimal, 2, Util.ToDecimal(stArrayData[18]));
                    dpc.SetParam("@SHORI_YMD", SqlDbType.DateTime, stArrayData[19]);

                    // 処理実行
                    this.Dba.Insert("TB_HN_WB_SHIKIRIS", dpc);
                }

                cReader2.Close();

                // トランザクションをコミット
                this.Dba.Commit();
                Msg.Info("取り込み処理が正常に終了しました。");

                // FTPサーバーから削除
                DeleteFtpFile(downloadUrl.ToString());
                // FTPサーバーにアップロード
                UploadFtpFile(dirPath2.LocalPath, uploadUrl);
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }
            
            return true;
        }

        /// <summary>
        /// FTPサーバーからファイルをダウンロードする
        /// </summary>
        /// <param name="downFile">ダウンロードしたファイルの保存先</param>
        /// <param name="u">ダウンロードするファイルのURI</param>
        private void DownloadFtpFile(string downFile, Uri u)
        {
            //FtpWebRequestの作成
            System.Net.FtpWebRequest ftpReq = (System.Net.FtpWebRequest)System.Net.WebRequest.Create(u);

            if (this.ftpCredential != null)
                ftpReq.Credentials = this.ftpCredential;

            //MethodにWebRequestMethods.Ftp.DownloadFile("RETR")を設定
            ftpReq.Method = System.Net.WebRequestMethods.Ftp.DownloadFile;
            //要求の完了後に接続を閉じる
            ftpReq.KeepAlive = true;

            //FtpWebResponseを取得
            System.Net.FtpWebResponse ftpRes = (System.Net.FtpWebResponse)ftpReq.GetResponse();
            //ファイルをダウンロードするためのStreamを取得
            System.IO.Stream resStrm = ftpRes.GetResponseStream();
            //ダウンロードしたファイルを書き込むためのFileStreamを作成
            System.IO.FileStream fs = new System.IO.FileStream(
                downFile, System.IO.FileMode.Create, System.IO.FileAccess.Write);
            //ダウンロードしたデータを書き込む
            byte[] buffer = new byte[1024];
            while (true)
            {
                int readSize = resStrm.Read(buffer, 0, buffer.Length);
                if (readSize == 0)
                    break;
                fs.Write(buffer, 0, readSize);
            }
            fs.Close();
            resStrm.Close();

            //閉じる
            ftpRes.Close();
        }

        /// <summary>
        /// FTPサーバーにファイルをアップロードする
        /// </summary>
        /// <param name="downFile">アップロードするファイル</param>
        /// <param name="u">アップロード先のURI</param>
        private void UploadFtpFile(string upFile, Uri u)
        {
            //FtpWebRequestの作成
            System.Net.FtpWebRequest ftpReq = (System.Net.FtpWebRequest)System.Net.WebRequest.Create(u);

            if (this.ftpCredential != null)
                ftpReq.Credentials = this.ftpCredential;


            //MethodにWebRequestMethods.Ftp.UploadFile("STOR")を設定
            ftpReq.Method = System.Net.WebRequestMethods.Ftp.UploadFile;

            //FtpWebResponseを取得
            System.Net.FtpWebResponse ftpRes = (System.Net.FtpWebResponse)ftpReq.GetResponse();

            //ファイルをアップロードするためのStreamを取得
            System.IO.Stream reqStrm = ftpReq.GetRequestStream();
            //アップロードするファイルを開く
            System.IO.FileStream fs = new System.IO.FileStream(
                upFile, System.IO.FileMode.Open, System.IO.FileAccess.Read);
            //アップロードStreamに書き込む
            byte[] buffer = new byte[1024];
            while (true)
            {
                int readSize = fs.Read(buffer, 0, buffer.Length);
                if (readSize == 0)
                    break;
                reqStrm.Write(buffer, 0, readSize);
            }
            fs.Close();
            reqStrm.Close();

            //閉じる
            ftpRes.Close();
        }

        /// <summary>
        /// FTPサーバーからファイルを削除する
        /// </summary>
        /// <param name="uri">削除するファイルのURI</param>
        private void DeleteFtpFile(string uri)
        {
            //FtpWebRequestの作成
            System.Net.FtpWebRequest ftpReq = (System.Net.FtpWebRequest)System.Net.WebRequest.Create(uri);

            if (this.ftpCredential != null)
                ftpReq.Credentials = this.ftpCredential;

            //MethodにWebRequestMethods.Ftp.DeleteFile(DELE)を設定
            ftpReq.Method = System.Net.WebRequestMethods.Ftp.DeleteFile;

            //FtpWebResponseを取得
            System.Net.FtpWebResponse ftpRes = (System.Net.FtpWebResponse)ftpReq.GetResponse();

            //閉じる
            ftpRes.Close();
        }

        /// <summary>
        /// テキストファイルのデータを読み込み、データベースにインポート
        /// </summary>
        private void DataConvert()
        {
            int denpyoBango = 0;
            decimal area_cd = 0;
            decimal funanushi_cd = 0;
            decimal Gokeisuryo = 0;
            decimal zeinukiGokeiDenpyo = 0;
            decimal zeinukiGokeiMeisai = 0;
            decimal zeikomiGokei = 0;
            decimal shohizei = 0;
            decimal hakoflg = 0;
            int count = 0;
            int lastRow = 0;
            int flg = 0;
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder sql = new StringBuilder();

            try
            {
                // 日付を西暦にして取得
                DateTime tmpDate = Getdate();
                // webセリ情報取得
                DataTable dt = GetWbSeriJoho(tmpDate);
                if(dt.Rows.Count == 0)
                {
                    return;
                }
                // 伝票番号取得
                denpyoBango = this.Dba.GetHNDenpyoNo(this.UInfo, DUMMY_DENPYO_KUBUN, 0);
                // 最後の行を取得
                lastRow = dt.Rows.Count;
                // トランザクションの開始
                this.Dba.BeginTransaction();

                #region セリデータ削除処理
                dpc = new DbParamCollection();
                sql = new StringBuilder();
                sql.Append("DELETE ");
                sql.Append("  FROM TB_HN_TMP_SHIKIRI_DATA");
                sql.Append("  WHERE");
                sql.Append("   KAISHA_CD = @KAISHA_CD ");

                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                this.Dba.ModifyBySql(Util.ToString(sql), dpc);


                dpc = new DbParamCollection();
                sql = new StringBuilder();
                sql.Append("DELETE ");
                sql.Append("  FROM TB_HN_TMP_SHIKIRI_MEISAI");
                sql.Append("  WHERE");
                sql.Append("   KAISHA_CD = @KAISHA_CD ");

                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                this.Dba.ModifyBySql(Util.ToString(sql), dpc);

                #endregion

                // 最初のエリアコードを取得
                area_cd = Util.ToInt(dt.Rows[0]["SERI_AREA"]);
                // 最初の船主コードを取得
                funanushi_cd = Util.ToInt(dt.Rows[0]["SENSHU_CD"]);
                int gyo_no = 1;

                for (int i = 0; dt.Rows.Count > i; i++)
                {
                    DbParamCollection insParm;
                    #region TB_HN_TMP_SHIKIRI_MEISAIに更新
                    insParm = new DbParamCollection();
                    insParm.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                    insParm.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo); // セリ日から計算してINSERTした方がいいのか...
                    insParm.SetParam("@DENPYO_BANGO", SqlDbType.VarChar, 9, denpyoBango);
                    insParm.SetParam("@DENPYO_MEISAI_BANGO", SqlDbType.VarChar, 9, gyo_no); gyo_no++;//GYO_NOカウント
                    insParm.SetParam("@SERIBI", SqlDbType.DateTime, Util.ToDate(dt.Rows[i]["SERIBI"]));
                    insParm.SetParam("@YAMA_NO", SqlDbType.VarChar, 9, dt.Rows[i]["YAMA_NO"]);
                    insParm.SetParam("@GYOSHU_CD", SqlDbType.VarChar, 9, dt.Rows[i]["GYOSHU_CD"]);
                    insParm.SetParam("@SURYO", SqlDbType.VarChar, 9, dt.Rows[i]["SURYO"]);
                    // 合計数量（仕切データ）
                    Gokeisuryo += Util.ToDecimal(dt.Rows[i]["SURYO"]);
                    insParm.SetParam("@TANKA", SqlDbType.VarChar, 9, dt.Rows[i]["TANKA"]);
                    // 税抜き水揚金額計算
                    zeinukiGokeiMeisai = Util.ToDecimal(dt.Rows[i]["SURYO"]) * Util.ToDecimal(dt.Rows[i]["TANKA"]);
                    zeinukiGokeiDenpyo += zeinukiGokeiMeisai;
                    insParm.SetParam("@KINGAKU", SqlDbType.VarChar, 9, zeinukiGokeiMeisai);
                    // 消費税額計算（明細）
                    shohizei = calcDetailTax(zeinukiGokeiMeisai, Util.ToDate(dt.Rows[i]["SERIBI"]));
                    insParm.SetParam("@SHOHIZEIGAKU", SqlDbType.VarChar, 9, shohizei);
                    insParm.SetParam("@NAKAGAININ_CD", SqlDbType.VarChar, 9, dt.Rows[i]["NAKAGAININ_CD"]);
                    insParm.SetParam("@PAYAO_NO", SqlDbType.VarChar, 9, 0);//0固定？
                    if(Util.ToDecimal(dt.Rows[i]["HAKO_FLG"]) == 1)
                    {
                        hakoflg += Util.ToDecimal(dt.Rows[i]["HAKO_FLG"]);
                    }

                    // 処理実行
                    this.Dba.Insert("TB_HN_TMP_SHIKIRI_MEISAI", insParm);
                    #endregion

                    // 最後の行の場合flgを立てる
                    if (Util.ToInt(count) == lastRow - 1)
                    {
                        flg = 1;
                    }
                    // 次の行の船主コードが違っていたら
                    else if (Util.ToDecimal(dt.Rows[i]["SENSHU_CD"]) != Util.ToDecimal(dt.Rows[i + 1]["SENSHU_CD"]))
                    {
                        flg = 1;
                    }
                    // 次の行のエリアコードが違っていたら
                    else if (Util.ToDecimal(dt.Rows[i]["SERI_AREA"]) != Util.ToDecimal(dt.Rows[i + 1]["SERI_AREA"]))
                    {
                        flg = 1;
                    }

                    #region TB_HN_TMP_SHIKIRI_DATAに更新
                    // 船主コードが変わった又は最後の行は仕切データにINSERT
                    if (flg == 1)
                    {
                        insParm = new DbParamCollection();
                        insParm.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                        insParm.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo); // セリ日から計算してINSERTした方がいいのか...
                        insParm.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 12, denpyoBango);
                        insParm.SetParam("@SERIBI", SqlDbType.DateTime, Util.ToDate(dt.Rows[i]["SERIBI"]));
                        insParm.SetParam("@SEISAN_BANGO", SqlDbType.Decimal, 12, 0);// 0固定
                        insParm.SetParam("@SENSHU_CD", SqlDbType.Decimal, 12, dt.Rows[i]["SENSHU_CD"]);
                        insParm.SetParam("@SEISAN_KUBUN", SqlDbType.Decimal, 12, 3);// 3固定
                        insParm.SetParam("@GYOHO_CD", SqlDbType.Decimal, 12, dt.Rows[i]["GYOHO_CD"]);
                        insParm.SetParam("@GYOKYO_TESURYO", SqlDbType.Decimal, 12, 5);// DBから取得(今は固定で5）
                        insParm.SetParam("@NIUKENIN_CD", SqlDbType.Decimal, 12, 0);// 0固定
                        insParm.SetParam("@SHIJO_CD", SqlDbType.Decimal, 12, 0);// 0固定？
                        insParm.SetParam("@MIZUAGE_SURYO", SqlDbType.Decimal, 12, Gokeisuryo);
                        insParm.SetParam("@ZEINUKI_MIZUAGE_KINGAKU", SqlDbType.Decimal, 12, zeinukiGokeiDenpyo);
                        // 消費税額計算（伝票）
                        shohizei = calcDetailTax(zeinukiGokeiDenpyo, Util.ToDate(dt.Rows[i]["SERIBI"]));
                        insParm.SetParam("@SHOHIZEIGAKU", SqlDbType.Decimal, 12, shohizei);
                        // 税込み水揚金額計算
                        zeikomiGokei = zeinukiGokeiDenpyo + shohizei;
                        insParm.SetParam("@ZEIKOMI_MIZUAGE_KINGAKU", SqlDbType.Decimal, 12, zeikomiGokei);
                        insParm.SetParam("@SEISAN_TAISHO_KUBUN", SqlDbType.Decimal, 12, 0);// 0固定
                        insParm.SetParam("@MIZUAGE_SHISHO_KUBUN", SqlDbType.Decimal, 12, 1);// 1固定
                        insParm.SetParam("@KEIRI_RENKEI_KUBUN", SqlDbType.Decimal, 12, 0);// 0固定
                        insParm.SetParam("@KEIJO_NENGETSU", SqlDbType.Decimal, 12, 0);// 0固定
                        insParm.SetParam("@KEIJO_NENBI", SqlDbType.DateTime, today);// 今日の日付
                        insParm.SetParam("@NAGO_DENPYO_BANGO", SqlDbType.Decimal, 12, 1);//0固定？
                        insParm.SetParam("@HAKOSU", SqlDbType.Decimal, 12, hakoflg);
                        // 処理実行
                        this.Dba.Insert("TB_HN_TMP_SHIKIRI_DATA", insParm);

                        denpyoBango++;
                        // 税抜・税込金額・消費税額の値をクリア
                        Gokeisuryo = 0;
                        zeinukiGokeiDenpyo = 0;
                        zeikomiGokei = 0;
                        hakoflg = 0;
                        shohizei = 0;
                        flg = 0;
                        gyo_no = 1;
                    }
                    #endregion

                    area_cd = Util.ToInt(dt.Rows[i]["SERI_AREA"]);
                    funanushi_cd = Util.ToInt(dt.Rows[i]["SENSHU_CD"]);
                    count++;// カウントアップ

                }
                // トランザクションをコミット
                this.Dba.Commit();
                Msg.Info("更新処理が正常に終了しました。");
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }
        }

        /// <summary>
        /// ネットワークドライブ接続
        /// <summary>
        /// <param name="seribi">セリ日</param>
        /// <returns>webセリ情報テーブル</returns>
        private Boolean NetUseStart()
        {
            // 変数宣言
            string Url;
            Uri dirPath;

            Url = this.Config.LoadPgConfig(Constants.SubSys.Han, "HANB1081", "Setting", "Start");
            dirPath = new Uri(Url); // 基準となるパス

            // 保存先の存在チェック
            if (!File.Exists(dirPath.LocalPath))
            {
                Msg.Info("ネットワークドライブ割り当てに失敗しました");
                return false;
            }

            ProcessStartInfo psInfo = new ProcessStartInfo();
            psInfo.FileName = Util.ToString(dirPath.LocalPath); // 実行するファイル
            psInfo.CreateNoWindow = true; // コンソール・ウィンドウを開かない
            psInfo.UseShellExecute = false; // シェル機能を使用しない

            Process.Start(psInfo);

            return true;
        }

        /// <summary>
        /// ネットワークドライブ切断
        /// <summary>
        /// <param name="seribi">セリ日</param>
        /// <returns>webセリ情報テーブル</returns>
        private Boolean NetUseDelete()
        {
            // 変数宣言
            string Url;
            Uri dirPath;

            Url = this.Config.LoadPgConfig(Constants.SubSys.Han, "HANB1081", "Setting", "Delete");
            dirPath = new Uri(Url); // 基準となるパス

            // 保存先の存在チェック
            if (!File.Exists(dirPath.LocalPath))
            {
                Msg.Info("ネットワークドライブ切断に失敗しました");
                return false;
            }

            ProcessStartInfo psInfo = new ProcessStartInfo();
            psInfo.FileName = Util.ToString(dirPath.LocalPath); // 実行するファイル
            psInfo.CreateNoWindow = true; // コンソール・ウィンドウを開かない
            psInfo.UseShellExecute = false; // シェル機能を使用しない

            Process.Start(psInfo);

            return true;
        }

        /// <summary>
        /// webセリ情報取得
        /// <summary>
        /// <param name="seribi">セリ日</param>
        /// <returns>webセリ情報テーブル</returns>
        private DataTable GetWbSeriJoho(DateTime seribi)
        {
            // webセリ仕切データ取得(TB_HN_WB_SHIKIRIS)
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder sql = new StringBuilder();
            sql.Append(" SELECT");
            sql.Append(" * ");
            sql.Append(" FROM");
            sql.Append("     TB_HN_WB_SHIKIRIS");
            sql.Append(" WHERE");
            sql.Append("     KAISHA_CD = @KAISHA_CD AND");
            sql.Append("     SERIBI = @SERIBI AND ");
            sql.Append("     NAKAGAININ_CD > 0 AND ");
            sql.Append("     TANKA > 0 ");
            sql.Append(" ORDER BY ");
            sql.Append("    YAMA_NO");

            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SERIBI", SqlDbType.DateTime, seribi);
            dpc.SetParam("@TANKA", SqlDbType.Decimal, 0);
            dpc.SetParam("@NAKAGAININ", SqlDbType.Decimal, 0);
            DataTable dtSerijoho = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);

            return dtSerijoho;
        }

        /// <summary>
        /// セリ日存在チェック
        /// <summary>
        /// <param name="seribi">セリ日</param>
        /// <returns>webセリ情報テーブル</returns>
        private DataTable GetWbSeriDate(DateTime seribi)
        {
            // webセリ仕切データ取得(TB_HN_WB_SHIKIRIS)
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder sql = new StringBuilder();
            sql.Append(" SELECT");
            sql.Append(" * ");
            sql.Append(" FROM");
            sql.Append("     TB_HN_WB_SHIKIRIS");
            sql.Append(" WHERE");
            sql.Append("     KAISHA_CD = @KAISHA_CD AND");
            sql.Append("     SERIBI = @SERIBI ");

            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SERIBI", SqlDbType.DateTime, seribi);
            DataTable dtSeridate = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);

            return dtSeridate;
        }

        /// <summary>
        ///  日付を西暦にして取得
        /// <summary>
        /// <returns>西暦の日付</returns>
        private DateTime Getdate()
        {
            // 日付を西暦にして取得
            DateTime tmpDate = Util.ConvAdDate(this.lblDateGengo.Text, this.txtDateYear.Text,
                    this.txtDateMonth.Text, this.txtDateDay.Text, this.Dba);

            return tmpDate;
        }

        /// <summary>
        /// 消費税額算出
        /// </summary>
        /// <param name="mizuageKingaku">水揚金額</param>
        /// <param name="seribi">セリ日</param>
        /// <returns>decimal 消費税額</returns>
        private decimal calcDetailTax(decimal mizuageKingaku, DateTime seribi)
        {
            //消費税率取得
            this.consumptionTax = this.GetZeiritsu(seribi);
            return Util.Round(mizuageKingaku * (this.consumptionTax / 100), 0);
            // 明細の消費税額（小数点第二位を四捨五入後、小数点第一位を五捨六入する）
            // 例１：水揚金額が 10円 の場合は 消費税額 0.50円 → 0円
            // 例２：水揚金額が 11円 の場合は 消費税額 0.55円 → 1円
            // return Math.Floor(Decimal.Add((decimal)0.45, (decimal)mizuageKingaku * (this.consumptionTax / 100)));

        }
        #endregion
    }
}
