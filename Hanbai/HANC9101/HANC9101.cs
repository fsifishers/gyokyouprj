﻿using System;
using System.Collections;
using System.ComponentModel;
using System.IO;
using System.Data;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

using GrapeCity.ActiveReports;

using jp.co.fsi.common.access;
using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.han.hanc9101
{
    /// <summary>
    /// 船主マスタ一覧(HANC9101)
    /// </summary>
    public partial class HANC9101 : BasePgForm
    {
        #region プロパティ
        /// <summary>
        /// 画面上最後となるフォーカスのEnterボタン押下時処理用変数
        /// </summary>
        private bool _dtFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._dtFlg;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HANC9101()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region メソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // ボタンの位置調整
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF1.Location = this.btnF2.Location;
            this.btnF5.Location = this.btnF4.Location;
            this.btnF4.Location = this.btnF3.Location;

            // 年度をセット
            string[] nendo = Util.ConvJpDate(new DateTime(this.UInfo.KaikeiNendo, 4, 1), this.Dba);
            this.lblJpNengo.Text = nendo[0];
            this.txtJpYear.Text = nendo[2];
            
            // フォーカス設定
            this.txtSeijunKubunCd.Focus();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 船名CDと年度にフォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtJpYear":
                case "txtFunanushiCdFr":
                case "txtFunanushiCdTo":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;

            switch (this.ActiveCtlNm)
            {
                case "txtJpYear":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom("COMC9011.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.com.comc9011.COMC9011");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblJpNengo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.lblJpNengo.Text = result[1];
                            }
                        }
                    }
                    break;

                case "txtFunanushiCdFr":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom("KOBC9021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.kob.kobc9021.KOBC9021");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtFunanushiCdFr.Text = outData[0];
                                this.lblFunanushiCdFr.Text = outData[1];
                            }
                        }
                    }
                    break;

                case "txtFunanushiCdTo":
                    // アセンブリのロード
                    asm = Assembly.LoadFrom("KOBC9021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.kob.kobc9021.KOBC9021");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtFunanushiCdTo.Text = outData[0];
                                this.lblFunanushiCdTo.Text = outData[1];
                            }
                        }
                    }
                    break;

                default:
                    break;
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
            {
                // プレビュー処理
                DoPrint(true);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("印刷", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }

        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            // 設定画面の起動
            // MEMO:原則としてここで渡す帳票IDの設定はReport.csvに保持していることが前提ですが、
            // 保持していない場合は、設定画面での保存(F6)時に新規に設定が保持されます。
            PrintSettingForm psForm = new PrintSettingForm(new string[1] { "HANC9101R" });
            psForm.ShowDialog();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 正準区分の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSeijunKubunCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidSeijunKubunCd())
            {
                e.Cancel = true;
                this.txtSeijunKubunCd.SelectAll();
            }
        }

        /// <summary>
        /// 年度の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtJpYear_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidJpYear())
            {
                e.Cancel = true;
                this.txtJpYear.SelectAll();
            }
        }

        /// <summary>
        /// 船主コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFunanushiCdFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidFunanushiCdFr())
            {
                e.Cancel = true;
                this.txtFunanushiCdFr.SelectAll();
            }
        }

        /// <summary>
        /// 船主コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFunanushiCdTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidFunanushiCdTo())
            {
                e.Cancel = true;
                this.txtFunanushiCdTo.SelectAll();

                // Enter処理を無効化
                this._dtFlg = false;
            }
            else
            {
                // Enter処理を有効化
                this._dtFlg = true;
            }
        }

        /// <summary>
        /// 船主コード(至)のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFunanushiCdTo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.Flg)
            {
                // Enter処理を無効化
                this._dtFlg = false;

                // 全項目を再度入力値チェック
                if (!ValidateAll())
                {
                    // エラーありの場合ここで処理終了
                    return;
                }

                if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
                {
                    // ﾌﾟﾚﾋﾞｭｰ処理
                    DoPrint(true);
                }
                else
                {
                    this.txtFunanushiCdTo.Focus();
                }
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 正準区分の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidSeijunKubunCd()
        {
            // 未入力の場合「0」を表示
            if (ValChk.IsEmpty(this.txtSeijunKubunCd.Text))
            {
                this.txtSeijunKubunCd.Text = "0";
                return true;
            }

            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtSeijunKubunCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 0～3のみの入力を許可
            if (!(Util.ToDecimal(this.txtSeijunKubunCd.Text) >= 0 && Util.ToDecimal(this.txtSeijunKubunCd.Text) <= 3))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 年の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidJpYear()
        {
            // 数字以外が入力されたらエラーメッセージ
            if (!ValChk.IsNumber(this.txtJpYear.Text))
            {
                Msg.Error("数値のみで入力してください。");
                return false;
            }

            // 空の場合、0年として処理
            if (ValChk.IsEmpty(this.txtJpYear.Text))
            {
                this.txtJpYear.Text = "0";
            }

            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDate(Util.FixJpDate(this.lblJpNengo.Text, this.txtJpYear.Text,
                "1", "1", this.Dba));

            return true;
        }

        /// <summary>
        /// 船主コードの入力チェック(先頭）
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidFunanushiCdFr()
        {
            // 未入力の場合「先頭」を表示
            if (ValChk.IsEmpty(this.txtFunanushiCdFr.Text))
            {
                this.lblFunanushiCdFr.Text = "先　頭";
                return true;
            }

            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtFunanushiCdFr.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 名称を表示(存在しないコードを入力されたら空白表示)
            string name = this.Dba.GetName(this.UInfo, "VI_HN_FUNANUSHI", this.txtFunanushiCdFr.Text);
            this.lblFunanushiCdFr.Text = name;

            return true;
        }

        /// <summary>
        /// 船主コードの入力チェック(最後）
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidFunanushiCdTo()
        {
            // 未入力の場合、「最後」を表示
            if (ValChk.IsEmpty(this.txtFunanushiCdTo.Text))
            {
                this.lblFunanushiCdTo.Text = "最　後";
                return true;
            }

            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtFunanushiCdTo.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 名称を表示(存在しないコードを入力されたら空白表示)
            string name = this.Dba.GetName(this.UInfo, "VI_HN_FUNANUSHI", this.txtFunanushiCdTo.Text);
            this.lblFunanushiCdTo.Text = name;

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 正準区分のチェック
            if (!IsValidSeijunKubunCd())
            {
                this.txtSeijunKubunCd.Focus();
                this.txtSeijunKubunCd.SelectAll();
                return false;
            }

            // 年のチェック
            if (!IsValidJpYear())
            {
                this.txtJpYear.Focus();
                this.txtJpYear.SelectAll();
                return false;
            }

            // 船主コードのチェック
            if (!IsValidFunanushiCdFr())
            {
                this.txtFunanushiCdFr.Focus();
                this.txtFunanushiCdFr.SelectAll();
                return false;
            }

            // 船主コードのチェック
            if (!IsValidFunanushiCdTo())
            {
                this.txtFunanushiCdTo.Focus();
                this.txtFunanushiCdTo.SelectAll();
                return false;
            }
            return true;
        }

        /// <summary>
        /// 配列に格納された和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpDate(string[] arrJpDate)
        {
            this.lblJpNengo.Text = arrJpDate[0];
            this.txtJpYear.Text = arrJpDate[2];
        }

        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview)
        {
            try
            {
                this.Dba.BeginTransaction();

                // 帳票出力用にワークテーブルにデータを作成
                bool dataFlag = MakeWkData();

                // 帳票出力
                if (dataFlag)
                {
                    // 取得列の定義
                    StringBuilder cols = new StringBuilder();
                    cols.Append("  ITEM01");
                    cols.Append(" ,ITEM02");
                    cols.Append(" ,ITEM03");
                    cols.Append(" ,ITEM04");
                    cols.Append(" ,ITEM05");
                    cols.Append(" ,ITEM06");
                    cols.Append(" ,ITEM07");
                    cols.Append(" ,ITEM08");
                    cols.Append(" ,ITEM09");
                    cols.Append(" ,ITEM10");
                    cols.Append(" ,ITEM11");
                    cols.Append(" ,ITEM12");
                    cols.Append(" ,ITEM13");
                    cols.Append(" ,ITEM14");
                    cols.Append(" ,ITEM15");
                    cols.Append(" ,ITEM16");
                    cols.Append(" ,ITEM17");
                    cols.Append(" ,ITEM18");
                    cols.Append(" ,ITEM19");
                    cols.Append(" ,ITEM20");
                    cols.Append(" ,ITEM21");
                    cols.Append(" ,ITEM22");
                    cols.Append(" ,ITEM23");
                    cols.Append(" ,ITEM24");
                    cols.Append(" ,ITEM25");

                    // バインドパラメータの設定
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);

                    // データの取得
                    DataTable dtOutput = this.Dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_HN_TBL", "GUID = @GUID", "SORT ASC", dpc);

                    HANC9101R rpt = new HANC9101R(dtOutput);
                    if (isPreview)
                    {
                        // 帳票オブジェクトをインスタンス化
                        PreviewForm pFrm = new PreviewForm(rpt, this.UnqId);
                        // プレビュー画面表示
                        pFrm.Show();
                    }
                    else
                    {
                        // 帳票オブジェクトをインスタンス化
                        // 直接印刷
                        rpt.Run(false);
                        rpt.Document.Print(true, true, false);
                    }
                }
            }
            finally
            {
                this.Dba.Rollback();
            }
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData()
        {
            #region データ取得の準備

            // 船主コード設定
            string FUNANUSHI_CD_FR;
            string FUNANUSHI_CD_TO;

            string FUNANUSHI_NM_FR;
            string FUNANUSHI_NM_TO;
            if (Util.ToDecimal(txtFunanushiCdFr.Text) > 0)
            {
                FUNANUSHI_CD_FR = txtFunanushiCdFr.Text;
            }
            else
            {
                FUNANUSHI_CD_FR = "0";
            }
            if (Util.ToDecimal(txtFunanushiCdTo.Text) > 0)
            {
                FUNANUSHI_CD_TO = txtFunanushiCdTo.Text;
            }
            else
            {
                FUNANUSHI_CD_TO = "9999";
            }
            FUNANUSHI_NM_FR = lblFunanushiCdFr.Text;
            FUNANUSHI_NM_TO = lblFunanushiCdTo.Text;
            int i = 0; // ループ用カウント変数
            int dbSORT = 1;
            string FunanushiJusho;
            #endregion

            #region メインデータ取得
            // 入力された情報を元にワークテーブルに更新をする
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder Sql = new StringBuilder();
            //VI_HN_TORIHIKISAKI_JOHOからデータ取得
            Sql.Append("SELECT");
            Sql.Append(" * ");
            Sql.Append("FROM ");
            Sql.Append(" VI_HN_TORIHIKISAKI_JOHO ");
            Sql.Append("WHERE");
            Sql.Append(" KAISHA_CD = @KAISHA_CD AND");
            Sql.Append(" TORIHIKISAKI_CD BETWEEN @FUNANUSHI_CD_FR AND @FUNANUSHI_CD_TO AND");
            Sql.Append(" TORIHIKISAKI_KUBUN1 = 1 AND");
            Sql.Append(" DATTAIBI IS NULL ");
            // 正準区分が選択されている場合のみ設定
            if (this.txtSeijunKubunCd.Text != "0")
            {
                Sql.Append(" AND SEIJUN_KUBUN = @SEIJUN_KUBUN ");
                dpc.SetParam("@SEIJUN_KUBUN", SqlDbType.Decimal, 1, this.txtSeijunKubunCd.Text);
            }
            Sql.Append("ORDER BY ");
            Sql.Append(" KAISHA_CD,");
            Sql.Append(" SEIJUN_KUBUN,");
            Sql.Append(" TORIHIKISAKI_CD");
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@FUNANUSHI_CD_FR", SqlDbType.Decimal, 6, FUNANUSHI_CD_FR);
            dpc.SetParam("@FUNANUSHI_CD_TO", SqlDbType.Decimal, 6, FUNANUSHI_CD_TO);
            DataTable dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
            #endregion

            if (dtMainLoop.Rows.Count == 0)
            {
                Msg.Info("該当データがありません。");
                return false;
            }
            else
            {
                while (dtMainLoop.Rows.Count > i)
                {
                    #region インサートテーブル
                    Sql = new StringBuilder();
                    dpc = new DbParamCollection();
                    Sql.Append("INSERT INTO PR_HN_TBL(");
                    Sql.Append("  GUID");
                    Sql.Append(" ,SORT");
                    Sql.Append(" ,ITEM01");
                    Sql.Append(" ,ITEM02");
                    Sql.Append(" ,ITEM03");
                    Sql.Append(" ,ITEM04");
                    Sql.Append(" ,ITEM05");
                    Sql.Append(" ,ITEM06");
                    Sql.Append(" ,ITEM07");
                    Sql.Append(" ,ITEM08");
                    Sql.Append(" ,ITEM09");
                    Sql.Append(" ,ITEM10");
                    Sql.Append(" ,ITEM11");
                    Sql.Append(" ,ITEM12");
                    Sql.Append(" ,ITEM13");
                    Sql.Append(" ,ITEM14");
                    Sql.Append(" ,ITEM15");
                    Sql.Append(" ,ITEM16");
                    Sql.Append(" ,ITEM17");
                    Sql.Append(" ,ITEM18");
                    Sql.Append(" ,ITEM19");
                    Sql.Append(" ,ITEM20");
                    Sql.Append(" ,ITEM21");
                    Sql.Append(" ,ITEM22");
                    Sql.Append(" ,ITEM23");
                    Sql.Append(" ,ITEM24");
                    Sql.Append(" ,ITEM25");
                    Sql.Append(") ");
                    Sql.Append("VALUES(");
                    Sql.Append("  @GUID");
                    Sql.Append(" ,@SORT");
                    Sql.Append(" ,@ITEM01");
                    Sql.Append(" ,@ITEM02");
                    Sql.Append(" ,@ITEM03");
                    Sql.Append(" ,@ITEM04");
                    Sql.Append(" ,@ITEM05");
                    Sql.Append(" ,@ITEM06");
                    Sql.Append(" ,@ITEM07");
                    Sql.Append(" ,@ITEM08");
                    Sql.Append(" ,@ITEM09");
                    Sql.Append(" ,@ITEM10");
                    Sql.Append(" ,@ITEM11");
                    Sql.Append(" ,@ITEM12");
                    Sql.Append(" ,@ITEM13");
                    Sql.Append(" ,@ITEM14");
                    Sql.Append(" ,@ITEM15");
                    Sql.Append(" ,@ITEM16");
                    Sql.Append(" ,@ITEM17");
                    Sql.Append(" ,@ITEM18");
                    Sql.Append(" ,@ITEM19");
                    Sql.Append(" ,@ITEM20");
                    Sql.Append(" ,@ITEM21");
                    Sql.Append(" ,@ITEM22");
                    Sql.Append(" ,@ITEM23");
                    Sql.Append(" ,@ITEM24");
                    Sql.Append(" ,@ITEM25");
                    Sql.Append(") ");
                    #endregion

                    #region データ登録
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                    dbSORT++;
                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, FUNANUSHI_CD_FR); // 船主コード（先頭）
                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, FUNANUSHI_CD_TO); // 船主コード（最後）
                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, FUNANUSHI_NM_FR); // 船主名（先頭）
                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, FUNANUSHI_NM_TO); // 船主名（最後）
                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["TORIHIKISAKI_CD"]); // 船主コード
                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["TORIHIKISAKI_NM"]); // 正式船主名
                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["RYAKUSHO"]); // 略称船主名
                    dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["YUBIN_BANGO1"]); // 郵便番号1
                    dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["YUBIN_BANGO2"]); // 郵便番号2
                    FunanushiJusho = Util.ToString(dtMainLoop.Rows[i]["JUSHO1"]) + "" + Util.ToString(dtMainLoop.Rows[i]["JUSHO2"]);
                    dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, FunanushiJusho); // 住所
                    dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["FUNE_NM_CD"]); // 船主CD
                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["FUNE_NM"]); // 船名
                    dpc.SetParam("@ITEM13", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["GYOHO_CD"]); // 漁法CD
                    dpc.SetParam("@ITEM14", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["GYOHO_NM"]); // 漁法名
                    dpc.SetParam("@ITEM15", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["KUMIAI_TESURYO_RITSU"], 2)); // 組合手数料率
                    dpc.SetParam("@ITEM16", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["FUTSU_KOZA_BANGO"]); // 普通口座番号
                    // 普通口座区分
                    if (dtMainLoop.Rows[i]["FUTSU_KOZA_KUBUN"].ToString() =="1")
                    {
                        dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, "普通");
                    }
                    else
                    {
                        dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, "");
                    }
                    // 積立区分
                    if (dtMainLoop.Rows[i]["TSUMITATE_KUBUN"].ToString() == "1")
                    {
                        dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, "積立有り");
                    }
                    else
                    {
                        dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, "積立無し");
                    }
                    dpc.SetParam("@ITEM19", SqlDbType.VarChar, 200, Util.FormatNum(dtMainLoop.Rows[i]["TSUMITATE_RITSU"], 2)); // 積立率
                    dpc.SetParam("@ITEM20", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["TSUMITATE_KOZA_BANGO"]); // 積立口座番号
                    // 積立口座区分
                    if (dtMainLoop.Rows[i]["TSUMITATE_KOZA_KUBUN"].ToString() == "1")
                    {
                        dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, "普通");
                    }
                    else
                    {
                        dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, "");
                    }
                    dpc.SetParam("@ITEM22", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["DENWA_BANGO"]); // 電話番号
                    dpc.SetParam("@ITEM23", SqlDbType.VarChar, 200, dtMainLoop.Rows[i]["FAX_BANGO"]); // FAX番号
                    // 支所区分
                    if (dtMainLoop.Rows[i]["SHISHO_KUBUN"].ToString() == "1")
                    {
                        dpc.SetParam("@ITEM24", SqlDbType.VarChar, 200, "本社"); 
                    }
                    else
                    {
                        dpc.SetParam("@ITEM24", SqlDbType.VarChar, 200, "支社"); 
                    }
                    // 正準区分
                    if (dtMainLoop.Rows[i]["SEIJUN_KUBUN"].ToString() == "1")
                    {
                        dpc.SetParam("@ITEM25", SqlDbType.VarChar, 200,"正組合員");
                    }
                    else if (dtMainLoop.Rows[i]["SEIJUN_KUBUN"].ToString() == "2")
                    {
                        dpc.SetParam("@ITEM25", SqlDbType.VarChar, 200,"準組合員");
                    }
                    else
                    {
                        dpc.SetParam("@ITEM25", SqlDbType.VarChar, 200,"組合員以外");
                    }

                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                    #endregion

                    i++;
                }
            }
            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
                "SORT",
                "PR_HN_TBL",
                "GUID = @GUID",
                dpc);

            bool dataFlag;
            if (tmpdtPR_HN_TBL.Rows.Count > 0)
            {
                dataFlag = true;
            }
            else
            {
                dataFlag = false;
            }

            return dataFlag;
        }
        #endregion
    }
}

