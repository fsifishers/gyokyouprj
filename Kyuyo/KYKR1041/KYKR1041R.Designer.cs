﻿namespace jp.co.fsi.ky.kykr1041
{
    /// <summary>
    /// KYKR1041R の帳票
    /// </summary>
    partial class KYKR1041R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(KYKR1041R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.ボックス0 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ITEM002 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.shape1 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ITEM001 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.直線1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.ラベル4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ITEM005 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ITEM006 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtToday = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.直線16 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線17 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線18 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線19 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線21 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線22 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線23 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線24 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線25 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線26 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線28 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線29 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.ラベル31 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル32 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル33 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル34 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル35 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル36 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル37 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル38 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル39 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル40 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル41 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.直線88 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線90 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線91 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線92 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線93 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線94 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線95 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線96 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線97 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線98 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線99 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.ITEM011 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM012 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM013 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM014 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM015 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM016 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM017 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM018 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM019 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM020 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM022 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM021 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.groupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.ITEM010 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.shape4 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.shape3 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.textBox3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.総レコード数 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM002)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM001)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM005)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM006)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtToday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM011)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM012)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM013)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM014)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM015)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM016)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM017)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM018)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM019)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM020)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM022)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM021)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.総レコード数)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.ボックス0,
            this.ITEM002,
            this.shape1,
            this.ITEM001,
            this.ラベル2,
            this.直線1,
            this.直線3,
            this.ラベル4,
            this.ラベル6,
            this.ITEM005,
            this.ラベル8,
            this.ラベル9,
            this.ITEM006,
            this.txtToday,
            this.テキスト13,
            this.ラベル15,
            this.直線16,
            this.直線17,
            this.直線18,
            this.直線19,
            this.直線21,
            this.直線22,
            this.直線23,
            this.直線24,
            this.直線25,
            this.直線26,
            this.直線28,
            this.直線29,
            this.ラベル31,
            this.ラベル32,
            this.ラベル33,
            this.ラベル34,
            this.ラベル35,
            this.ラベル36,
            this.ラベル37,
            this.ラベル38,
            this.ラベル39,
            this.ラベル40,
            this.ラベル41,
            this.textBox1,
            this.textBox2});
            this.pageHeader.Height = 1.102362F;
            this.pageHeader.Name = "pageHeader";
            this.pageHeader.Format += new System.EventHandler(this.pageHeader_Format);
            // 
            // ボックス0
            // 
            this.ボックス0.BackColor = System.Drawing.Color.White;
            this.ボックス0.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス0.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス0.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス0.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス0.Height = 0.4340278F;
            this.ボックス0.Left = 0.002777815F;
            this.ボックス0.Name = "ボックス0";
            this.ボックス0.RoundingRadius = 9.999999F;
            this.ボックス0.Tag = "";
            this.ボックス0.Top = 0.2405512F;
            this.ボックス0.Width = 1.053128F;
            // 
            // ITEM002
            // 
            this.ITEM002.DataField = "ITEM002";
            this.ITEM002.Height = 0.1706857F;
            this.ITEM002.Left = 0.5354331F;
            this.ITEM002.Name = "ITEM002";
            this.ITEM002.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.ITEM002.Tag = "";
            this.ITEM002.Text = "ITEM002";
            this.ITEM002.Top = 0.4929134F;
            this.ITEM002.Width = 0.4952756F;
            // 
            // shape1
            // 
            this.shape1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.shape1.Height = 0.3149606F;
            this.shape1.Left = 0F;
            this.shape1.Name = "shape1";
            this.shape1.RoundingRadius = 9.999999F;
            this.shape1.Top = 0.7874016F;
            this.shape1.Width = 10.62992F;
            // 
            // ITEM001
            // 
            this.ITEM001.DataField = "ITEM001";
            this.ITEM001.Height = 0.1706857F;
            this.ITEM001.Left = 0.01181102F;
            this.ITEM001.Name = "ITEM001";
            this.ITEM001.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.ITEM001.Tag = "";
            this.ITEM001.Text = "ITEM001";
            this.ITEM001.Top = 0.4929134F;
            this.ITEM001.Width = 0.4952756F;
            // 
            // ラベル2
            // 
            this.ラベル2.Height = 0.1722933F;
            this.ラベル2.HyperLink = null;
            this.ラベル2.Left = 0.02361107F;
            this.ラベル2.Name = "ラベル2";
            this.ラベル2.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-weight: normal; text-alig" +
    "n: center; ddo-char-set: 1";
            this.ラベル2.Tag = "";
            this.ラベル2.Text = "委 託 コ ー ド";
            this.ラベル2.Top = 0.2718012F;
            this.ラベル2.Width = 1.005917F;
            // 
            // 直線1
            // 
            this.直線1.Height = 0.0001805127F;
            this.直線1.Left = 0F;
            this.直線1.LineWeight = 0F;
            this.直線1.Name = "直線1";
            this.直線1.Tag = "";
            this.直線1.Top = 0.4801345F;
            this.直線1.Width = 1.049103F;
            this.直線1.X1 = 0F;
            this.直線1.X2 = 1.049103F;
            this.直線1.Y1 = 0.480315F;
            this.直線1.Y2 = 0.4801345F;
            // 
            // 直線3
            // 
            this.直線3.Height = 0.1993055F;
            this.直線3.Left = 0.5246063F;
            this.直線3.LineWeight = 0F;
            this.直線3.Name = "直線3";
            this.直線3.Tag = "";
            this.直線3.Top = 0.480315F;
            this.直線3.Width = 0.0001969337F;
            this.直線3.X1 = 0.5246063F;
            this.直線3.X2 = 0.5248032F;
            this.直線3.Y1 = 0.480315F;
            this.直線3.Y2 = 0.6796205F;
            // 
            // ラベル4
            // 
            this.ラベル4.Height = 0.2352526F;
            this.ラベル4.HyperLink = null;
            this.ラベル4.Left = 3.815278F;
            this.ラベル4.Name = "ラベル4";
            this.ラベル4.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 15pt; font-weight: bold; text-align:" +
    " center; ddo-char-set: 1";
            this.ラベル4.Tag = "";
            this.ラベル4.Text = "総 合 振 替 明 細 書";
            this.ラベル4.Top = 0.0009678751F;
            this.ラベル4.Width = 2.641415F;
            // 
            // ラベル6
            // 
            this.ラベル6.Height = 0.15625F;
            this.ラベル6.HyperLink = null;
            this.ラベル6.Left = 3.304861F;
            this.ラベル6.Name = "ラベル6";
            this.ラベル6.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.ラベル6.Tag = "";
            this.ラベル6.Text = "御 中";
            this.ラベル6.Top = 0.3551345F;
            this.ラベル6.Width = 0.3959266F;
            // 
            // ITEM005
            // 
            this.ITEM005.DataField = "ITEM005";
            this.ITEM005.Height = 0.15625F;
            this.ITEM005.Left = 4.211111F;
            this.ITEM005.Name = "ITEM005";
            this.ITEM005.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.ITEM005.Tag = "";
            this.ITEM005.Text = "ITEM005";
            this.ITEM005.Top = 0.399579F;
            this.ITEM005.Width = 1.420833F;
            // 
            // ラベル8
            // 
            this.ラベル8.Height = 0.15625F;
            this.ラベル8.HyperLink = null;
            this.ラベル8.Left = 5.669292F;
            this.ラベル8.Name = "ラベル8";
            this.ラベル8.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.ラベル8.Tag = "";
            this.ラベル8.Text = "振替分";
            this.ラベル8.Top = 0.3996063F;
            this.ラベル8.Width = 0.4270833F;
            // 
            // ラベル9
            // 
            this.ラベル9.Height = 0.1458333F;
            this.ラベル9.HyperLink = null;
            this.ラベル9.Left = 6.655555F;
            this.ラベル9.Name = "ラベル9";
            this.ラベル9.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.ラベル9.Tag = "";
            this.ラベル9.Text = "委 託 先 名";
            this.ラベル9.Top = 0.3134679F;
            this.ラベル9.Width = 0.6666667F;
            // 
            // ITEM006
            // 
            this.ITEM006.DataField = "ITEM006";
            this.ITEM006.Height = 0.15625F;
            this.ITEM006.Left = 6.659029F;
            this.ITEM006.Name = "ITEM006";
            this.ITEM006.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.ITEM006.Tag = "";
            this.ITEM006.Text = "ITEM006";
            this.ITEM006.Top = 0.462079F;
            this.ITEM006.Width = 2.952083F;
            // 
            // txtToday
            // 
            this.txtToday.DataField = "ITEM025";
            this.txtToday.Height = 0.15625F;
            this.txtToday.Left = 8.818898F;
            this.txtToday.Name = "txtToday";
            this.txtToday.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.txtToday.Tag = "";
            this.txtToday.Text = null;
            this.txtToday.Top = 0.04251969F;
            this.txtToday.Width = 1.26122F;
            // 
            // テキスト13
            // 
            this.テキスト13.Height = 0.15625F;
            this.テキスト13.Left = 10.07874F;
            this.テキスト13.Name = "テキスト13";
            this.テキスト13.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト13.SummaryFunc = GrapeCity.ActiveReports.SectionReportModel.SummaryFunc.Count;
            this.テキスト13.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト13.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.テキスト13.Tag = "";
            this.テキスト13.Text = null;
            this.テキスト13.Top = 0.04263455F;
            this.テキスト13.Width = 0.3583336F;
            // 
            // ラベル15
            // 
            this.ラベル15.Height = 0.15625F;
            this.ラベル15.HyperLink = null;
            this.ラベル15.Left = 10.42986F;
            this.ラベル15.Name = "ラベル15";
            this.ラベル15.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.ラベル15.Tag = "";
            this.ラベル15.Text = "頁";
            this.ラベル15.Top = 0.04263455F;
            this.ラベル15.Width = 0.1770833F;
            // 
            // 直線16
            // 
            this.直線16.Height = 0.3149604F;
            this.直線16.Left = 0F;
            this.直線16.LineWeight = 1F;
            this.直線16.Name = "直線16";
            this.直線16.Tag = "";
            this.直線16.Top = 0.7874016F;
            this.直線16.Width = 0F;
            this.直線16.X1 = 0F;
            this.直線16.X2 = 0F;
            this.直線16.Y1 = 0.7874016F;
            this.直線16.Y2 = 1.102362F;
            // 
            // 直線17
            // 
            this.直線17.Height = 0F;
            this.直線17.Left = 0F;
            this.直線17.LineWeight = 1F;
            this.直線17.Name = "直線17";
            this.直線17.Tag = "";
            this.直線17.Top = 0.7877734F;
            this.直線17.Width = 10.63194F;
            this.直線17.X1 = 0F;
            this.直線17.X2 = 10.63194F;
            this.直線17.Y1 = 0.7877734F;
            this.直線17.Y2 = 0.7877734F;
            // 
            // 直線18
            // 
            this.直線18.Height = 0F;
            this.直線18.Left = 0F;
            this.直線18.LineWeight = 1F;
            this.直線18.Name = "直線18";
            this.直線18.Tag = "";
            this.直線18.Top = 1.102362F;
            this.直線18.Width = 10.625F;
            this.直線18.X1 = 0F;
            this.直線18.X2 = 10.625F;
            this.直線18.Y1 = 1.102362F;
            this.直線18.Y2 = 1.102362F;
            // 
            // 直線19
            // 
            this.直線19.Height = 0.3149604F;
            this.直線19.Left = 10.62778F;
            this.直線19.LineWeight = 1F;
            this.直線19.Name = "直線19";
            this.直線19.Tag = "";
            this.直線19.Top = 0.7874016F;
            this.直線19.Width = 0F;
            this.直線19.X1 = 10.62778F;
            this.直線19.X2 = 10.62778F;
            this.直線19.Y1 = 0.7874016F;
            this.直線19.Y2 = 1.102362F;
            // 
            // 直線21
            // 
            this.直線21.Height = 0.3149604F;
            this.直線21.Left = 0.9448819F;
            this.直線21.LineWeight = 0F;
            this.直線21.Name = "直線21";
            this.直線21.Tag = "";
            this.直線21.Top = 0.7874016F;
            this.直線21.Width = 0F;
            this.直線21.X1 = 0.9448819F;
            this.直線21.X2 = 0.9448819F;
            this.直線21.Y1 = 0.7874016F;
            this.直線21.Y2 = 1.102362F;
            // 
            // 直線22
            // 
            this.直線22.Height = 0.3149604F;
            this.直線22.Left = 3.815278F;
            this.直線22.LineWeight = 0F;
            this.直線22.Name = "直線22";
            this.直線22.Tag = "";
            this.直線22.Top = 0.7874016F;
            this.直線22.Width = 0F;
            this.直線22.X1 = 3.815278F;
            this.直線22.X2 = 3.815278F;
            this.直線22.Y1 = 0.7874016F;
            this.直線22.Y2 = 1.102362F;
            // 
            // 直線23
            // 
            this.直線23.Height = 0.3149604F;
            this.直線23.Left = 4.336111F;
            this.直線23.LineWeight = 0F;
            this.直線23.Name = "直線23";
            this.直線23.Tag = "";
            this.直線23.Top = 0.7874016F;
            this.直線23.Width = 0F;
            this.直線23.X1 = 4.336111F;
            this.直線23.X2 = 4.336111F;
            this.直線23.Y1 = 0.7874016F;
            this.直線23.Y2 = 1.102362F;
            // 
            // 直線24
            // 
            this.直線24.Height = 0.3149604F;
            this.直線24.Left = 4.877778F;
            this.直線24.LineWeight = 0F;
            this.直線24.Name = "直線24";
            this.直線24.Tag = "";
            this.直線24.Top = 0.7874016F;
            this.直線24.Width = 0F;
            this.直線24.X1 = 4.877778F;
            this.直線24.X2 = 4.877778F;
            this.直線24.Y1 = 0.7874016F;
            this.直線24.Y2 = 1.102362F;
            // 
            // 直線25
            // 
            this.直線25.Height = 0.3149604F;
            this.直線25.Left = 5.117361F;
            this.直線25.LineWeight = 0F;
            this.直線25.Name = "直線25";
            this.直線25.Tag = "";
            this.直線25.Top = 0.7874016F;
            this.直線25.Width = 0F;
            this.直線25.X1 = 5.117361F;
            this.直線25.X2 = 5.117361F;
            this.直線25.Y1 = 0.7874016F;
            this.直線25.Y2 = 1.102362F;
            // 
            // 直線26
            // 
            this.直線26.Height = 0.3149604F;
            this.直線26.Left = 5.909028F;
            this.直線26.LineWeight = 0F;
            this.直線26.Name = "直線26";
            this.直線26.Tag = "";
            this.直線26.Top = 0.7874016F;
            this.直線26.Width = 0F;
            this.直線26.X1 = 5.909028F;
            this.直線26.X2 = 5.909028F;
            this.直線26.Y1 = 0.7874016F;
            this.直線26.Y2 = 1.102362F;
            // 
            // 直線28
            // 
            this.直線28.Height = 0.3149604F;
            this.直線28.Left = 6.811023F;
            this.直線28.LineWeight = 0F;
            this.直線28.Name = "直線28";
            this.直線28.Tag = "";
            this.直線28.Top = 0.7874016F;
            this.直線28.Width = 0F;
            this.直線28.X1 = 6.811023F;
            this.直線28.X2 = 6.811023F;
            this.直線28.Y1 = 0.7874016F;
            this.直線28.Y2 = 1.102362F;
            // 
            // 直線29
            // 
            this.直線29.Height = 0.3149604F;
            this.直線29.Left = 8.385827F;
            this.直線29.LineWeight = 0F;
            this.直線29.Name = "直線29";
            this.直線29.Tag = "";
            this.直線29.Top = 0.7874016F;
            this.直線29.Width = 0F;
            this.直線29.X1 = 8.385827F;
            this.直線29.X2 = 8.385827F;
            this.直線29.Y1 = 0.7874016F;
            this.直線29.Y2 = 1.102362F;
            // 
            // ラベル31
            // 
            this.ラベル31.Height = 0.15625F;
            this.ラベル31.HyperLink = null;
            this.ラベル31.Left = 0.03700788F;
            this.ラベル31.Name = "ラベル31";
            this.ラベル31.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.ラベル31.Tag = "";
            this.ラベル31.Text = "契 約 者 番 号";
            this.ラベル31.Top = 0.8779528F;
            this.ラベル31.Width = 0.9314961F;
            // 
            // ラベル32
            // 
            this.ラベル32.Height = 0.15625F;
            this.ラベル32.HyperLink = null;
            this.ラベル32.Left = 1.742361F;
            this.ラベル32.Name = "ラベル32";
            this.ラベル32.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.ラベル32.Tag = "";
            this.ラベル32.Text = "口　座　名　義　人";
            this.ラベル32.Top = 0.8779528F;
            this.ラベル32.Width = 1.177083F;
            // 
            // ラベル33
            // 
            this.ラベル33.Height = 0.1165355F;
            this.ラベル33.HyperLink = null;
            this.ラベル33.Left = 3.87874F;
            this.ラベル33.Name = "ラベル33";
            this.ラベル33.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.ラベル33.Tag = "";
            this.ラベル33.Text = "銀   行";
            this.ラベル33.Top = 0.818504F;
            this.ラベル33.Width = 0.4270833F;
            // 
            // ラベル34
            // 
            this.ラベル34.Height = 0.1480315F;
            this.ラベル34.HyperLink = null;
            this.ラベル34.Left = 3.87874F;
            this.ラベル34.Name = "ラベル34";
            this.ラベル34.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.ラベル34.Tag = "";
            this.ラベル34.Text = "コード";
            this.ラベル34.Top = 0.9350393F;
            this.ラベル34.Width = 0.4270833F;
            // 
            // ラベル35
            // 
            this.ラベル35.Height = 0.15625F;
            this.ラベル35.HyperLink = null;
            this.ラベル35.Left = 4.461111F;
            this.ラベル35.Name = "ラベル35";
            this.ラベル35.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.ラベル35.Tag = "";
            this.ラベル35.Text = "店番";
            this.ラベル35.Top = 0.8779528F;
            this.ラベル35.Width = 0.3020833F;
            // 
            // ラベル36
            // 
            this.ラベル36.Height = 0.28125F;
            this.ラベル36.HyperLink = null;
            this.ラベル36.Left = 4.920866F;
            this.ラベル36.Name = "ラベル36";
            this.ラベル36.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.ラベル36.Tag = "";
            this.ラベル36.Text = "種目";
            this.ラベル36.Top = 0.8149607F;
            this.ラベル36.Width = 0.1770833F;
            // 
            // ラベル37
            // 
            this.ラベル37.Height = 0.15625F;
            this.ラベル37.HyperLink = null;
            this.ラベル37.Left = 5.236805F;
            this.ラベル37.Name = "ラベル37";
            this.ラベル37.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.ラベル37.Tag = "";
            this.ラベル37.Text = "口座番号";
            this.ラベル37.Top = 0.8779528F;
            this.ラベル37.Width = 0.5520833F;
            // 
            // ラベル38
            // 
            this.ラベル38.Height = 0.15625F;
            this.ラベル38.HyperLink = null;
            this.ラベル38.Left = 5.929861F;
            this.ラベル38.Name = "ラベル38";
            this.ラベル38.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.ラベル38.Tag = "";
            this.ラベル38.Text = "振 替 金 額";
            this.ラベル38.Top = 0.8779528F;
            this.ラベル38.Width = 0.7395833F;
            // 
            // ラベル39
            // 
            this.ラベル39.Height = 0.1244095F;
            this.ラベル39.HyperLink = null;
            this.ラベル39.Left = 7.086615F;
            this.ラベル39.Name = "ラベル39";
            this.ラベル39.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.ラベル39.Tag = "";
            this.ラベル39.Text = "銀　　　行　　　名";
            this.ラベル39.Top = 0.818504F;
            this.ラベル39.Width = 1.104894F;
            // 
            // ラベル40
            // 
            this.ラベル40.Height = 0.1244587F;
            this.ラベル40.HyperLink = null;
            this.ラベル40.Left = 7.086615F;
            this.ラベル40.Name = "ラベル40";
            this.ラベル40.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.ラベル40.Tag = "";
            this.ラベル40.Text = "店　　  舗　　  名";
            this.ラベル40.Top = 0.9586614F;
            this.ラベル40.Width = 1.104894F;
            // 
            // ラベル41
            // 
            this.ラベル41.Height = 0.15625F;
            this.ラベル41.HyperLink = null;
            this.ラベル41.Left = 8.815277F;
            this.ラベル41.Name = "ラベル41";
            this.ラベル41.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.ラベル41.Tag = "";
            this.ラベル41.Text = "摘　　　　　　　要";
            this.ラベル41.Top = 0.8779528F;
            this.ラベル41.Width = 1.791667F;
            // 
            // textBox1
            // 
            this.textBox1.DataField = "ITEM003";
            this.textBox1.Height = 0.1562992F;
            this.textBox1.Left = 1.956693F;
            this.textBox1.Name = "textBox1";
            this.textBox1.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-" +
    "weight: normal; text-align: left; ddo-char-set: 1";
            this.textBox1.Tag = "";
            this.textBox1.Text = "ITEM003";
            this.textBox1.Top = 0.3425197F;
            this.textBox1.Width = 1.287008F;
            // 
            // textBox2
            // 
            this.textBox2.DataField = "ITEM004";
            this.textBox2.Height = 0.1562992F;
            this.textBox2.Left = 1.956693F;
            this.textBox2.Name = "textBox2";
            this.textBox2.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-" +
    "weight: normal; text-align: left; ddo-char-set: 1";
            this.textBox2.Tag = "";
            this.textBox2.Text = "ITEM004";
            this.textBox2.Top = 0.5338582F;
            this.textBox2.Visible = false;
            this.textBox2.Width = 1.287008F;
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.直線88,
            this.直線90,
            this.直線91,
            this.直線92,
            this.直線93,
            this.直線94,
            this.直線95,
            this.直線96,
            this.直線97,
            this.直線98,
            this.直線99,
            this.ITEM011,
            this.ITEM012,
            this.ITEM013,
            this.ITEM014,
            this.ITEM015,
            this.ITEM016,
            this.ITEM017,
            this.ITEM018,
            this.ITEM019,
            this.ITEM020,
            this.ITEM022,
            this.ITEM021});
            this.detail.Height = 0.3612751F;
            this.detail.Name = "detail";
            this.detail.Format += new System.EventHandler(this.detail_Format);
            // 
            // 直線88
            // 
            this.直線88.Height = 0.3543307F;
            this.直線88.Left = 0F;
            this.直線88.LineWeight = 1F;
            this.直線88.Name = "直線88";
            this.直線88.Tag = "";
            this.直線88.Top = 0F;
            this.直線88.Width = 0F;
            this.直線88.X1 = 0F;
            this.直線88.X2 = 0F;
            this.直線88.Y1 = 0F;
            this.直線88.Y2 = 0.3543307F;
            // 
            // 直線90
            // 
            this.直線90.Height = 0F;
            this.直線90.Left = 0F;
            this.直線90.LineWeight = 0F;
            this.直線90.Name = "直線90";
            this.直線90.Tag = "";
            this.直線90.Top = 0.3543307F;
            this.直線90.Width = 10.62992F;
            this.直線90.X1 = 0F;
            this.直線90.X2 = 10.62992F;
            this.直線90.Y1 = 0.3543307F;
            this.直線90.Y2 = 0.3543307F;
            // 
            // 直線91
            // 
            this.直線91.Height = 0.3543307F;
            this.直線91.Left = 10.62778F;
            this.直線91.LineWeight = 1F;
            this.直線91.Name = "直線91";
            this.直線91.Tag = "";
            this.直線91.Top = 0F;
            this.直線91.Width = 0F;
            this.直線91.X1 = 10.62778F;
            this.直線91.X2 = 10.62778F;
            this.直線91.Y1 = 0F;
            this.直線91.Y2 = 0.3543307F;
            // 
            // 直線92
            // 
            this.直線92.Height = 0.3543307F;
            this.直線92.Left = 0.9448819F;
            this.直線92.LineWeight = 0F;
            this.直線92.Name = "直線92";
            this.直線92.Tag = "";
            this.直線92.Top = 0F;
            this.直線92.Width = 0F;
            this.直線92.X1 = 0.9448819F;
            this.直線92.X2 = 0.9448819F;
            this.直線92.Y1 = 0F;
            this.直線92.Y2 = 0.3543307F;
            // 
            // 直線93
            // 
            this.直線93.Height = 0.3543307F;
            this.直線93.Left = 3.815279F;
            this.直線93.LineWeight = 0F;
            this.直線93.Name = "直線93";
            this.直線93.Tag = "";
            this.直線93.Top = 0F;
            this.直線93.Width = 0F;
            this.直線93.X1 = 3.815279F;
            this.直線93.X2 = 3.815279F;
            this.直線93.Y1 = 0F;
            this.直線93.Y2 = 0.3543307F;
            // 
            // 直線94
            // 
            this.直線94.Height = 0.3543307F;
            this.直線94.Left = 4.330709F;
            this.直線94.LineWeight = 0F;
            this.直線94.Name = "直線94";
            this.直線94.Tag = "";
            this.直線94.Top = 0F;
            this.直線94.Width = 0F;
            this.直線94.X1 = 4.330709F;
            this.直線94.X2 = 4.330709F;
            this.直線94.Y1 = 0F;
            this.直線94.Y2 = 0.3543307F;
            // 
            // 直線95
            // 
            this.直線95.Height = 0.3543307F;
            this.直線95.Left = 4.877779F;
            this.直線95.LineWeight = 0F;
            this.直線95.Name = "直線95";
            this.直線95.Tag = "";
            this.直線95.Top = 0F;
            this.直線95.Width = 0F;
            this.直線95.X1 = 4.877779F;
            this.直線95.X2 = 4.877779F;
            this.直線95.Y1 = 0F;
            this.直線95.Y2 = 0.3543307F;
            // 
            // 直線96
            // 
            this.直線96.Height = 0.3543307F;
            this.直線96.Left = 5.12014F;
            this.直線96.LineWeight = 0F;
            this.直線96.Name = "直線96";
            this.直線96.Tag = "";
            this.直線96.Top = 0F;
            this.直線96.Width = 0F;
            this.直線96.X1 = 5.12014F;
            this.直線96.X2 = 5.12014F;
            this.直線96.Y1 = 0F;
            this.直線96.Y2 = 0.3543307F;
            // 
            // 直線97
            // 
            this.直線97.Height = 0.3543307F;
            this.直線97.Left = 5.909029F;
            this.直線97.LineWeight = 0F;
            this.直線97.Name = "直線97";
            this.直線97.Tag = "";
            this.直線97.Top = 0F;
            this.直線97.Width = 0F;
            this.直線97.X1 = 5.909029F;
            this.直線97.X2 = 5.909029F;
            this.直線97.Y1 = 0F;
            this.直線97.Y2 = 0.3543307F;
            // 
            // 直線98
            // 
            this.直線98.Height = 0.3543307F;
            this.直線98.Left = 6.811023F;
            this.直線98.LineWeight = 0F;
            this.直線98.Name = "直線98";
            this.直線98.Tag = "";
            this.直線98.Top = 0F;
            this.直線98.Width = 0F;
            this.直線98.X1 = 6.811023F;
            this.直線98.X2 = 6.811023F;
            this.直線98.Y1 = 0F;
            this.直線98.Y2 = 0.3543307F;
            // 
            // 直線99
            // 
            this.直線99.Height = 0.3543307F;
            this.直線99.Left = 8.385827F;
            this.直線99.LineWeight = 0F;
            this.直線99.Name = "直線99";
            this.直線99.Tag = "";
            this.直線99.Top = 0F;
            this.直線99.Width = 0F;
            this.直線99.X1 = 8.385827F;
            this.直線99.X2 = 8.385827F;
            this.直線99.Y1 = 0F;
            this.直線99.Y2 = 0.3543307F;
            // 
            // ITEM011
            // 
            this.ITEM011.DataField = "ITEM011";
            this.ITEM011.Height = 0.1980315F;
            this.ITEM011.Left = 0.002617623F;
            this.ITEM011.Name = "ITEM011";
            this.ITEM011.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: left; ddo-char-set: 1";
            this.ITEM011.Tag = "";
            this.ITEM011.Text = "ITEM011";
            this.ITEM011.Top = 0.03818898F;
            this.ITEM011.Width = 0.9422644F;
            // 
            // ITEM012
            // 
            this.ITEM012.DataField = "ITEM012";
            this.ITEM012.Height = 0.1980315F;
            this.ITEM012.Left = 0.9929135F;
            this.ITEM012.Name = "ITEM012";
            this.ITEM012.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: left; ddo-char-set: 1";
            this.ITEM012.Tag = "";
            this.ITEM012.Text = "ITEM012";
            this.ITEM012.Top = 0.03818898F;
            this.ITEM012.Width = 1.129167F;
            // 
            // ITEM013
            // 
            this.ITEM013.DataField = "ITEM013";
            this.ITEM013.Height = 0.1980315F;
            this.ITEM013.Left = 2.141339F;
            this.ITEM013.Name = "ITEM013";
            this.ITEM013.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: left; ddo-char-set: 1";
            this.ITEM013.Tag = "";
            this.ITEM013.Text = "ITEM013";
            this.ITEM013.Top = 0.03818898F;
            this.ITEM013.Width = 1.220975F;
            // 
            // ITEM014
            // 
            this.ITEM014.DataField = "ITEM014";
            this.ITEM014.Height = 0.1980315F;
            this.ITEM014.Left = 3.385827F;
            this.ITEM014.Name = "ITEM014";
            this.ITEM014.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.ITEM014.Tag = "";
            this.ITEM014.Text = "1001\r\n\r\n";
            this.ITEM014.Top = 0.03818898F;
            this.ITEM014.Width = 0.4126475F;
            // 
            // ITEM015
            // 
            this.ITEM015.DataField = "ITEM015";
            this.ITEM015.Height = 0.1980315F;
            this.ITEM015.Left = 3.867185F;
            this.ITEM015.Name = "ITEM015";
            this.ITEM015.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.ITEM015.Tag = "";
            this.ITEM015.Text = "ITEM015";
            this.ITEM015.Top = 0.03818898F;
            this.ITEM015.Width = 0.43125F;
            // 
            // ITEM016
            // 
            this.ITEM016.DataField = "ITEM016";
            this.ITEM016.Height = 0.1980315F;
            this.ITEM016.Left = 4.377815F;
            this.ITEM016.Name = "ITEM016";
            this.ITEM016.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.ITEM016.Tag = "";
            this.ITEM016.Text = "ITEM016";
            this.ITEM016.Top = 0.03818898F;
            this.ITEM016.Width = 0.4729167F;
            // 
            // ITEM017
            // 
            this.ITEM017.DataField = "ITEM017";
            this.ITEM017.Height = 0.1980315F;
            this.ITEM017.Left = 4.919308F;
            this.ITEM017.Name = "ITEM017";
            this.ITEM017.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: center; ddo-char-set: 1";
            this.ITEM017.Tag = "";
            this.ITEM017.Text = "ITEM017";
            this.ITEM017.Top = 0.03818898F;
            this.ITEM017.Width = 0.1708333F;
            // 
            // ITEM018
            // 
            this.ITEM018.DataField = "ITEM018";
            this.ITEM018.Height = 0.1980315F;
            this.ITEM018.Left = 5.157502F;
            this.ITEM018.Name = "ITEM018";
            this.ITEM018.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.ITEM018.Tag = "";
            this.ITEM018.Text = "ITEM018";
            this.ITEM018.Top = 0.03818898F;
            this.ITEM018.Width = 0.7020833F;
            // 
            // ITEM019
            // 
            this.ITEM019.DataField = "ITEM019";
            this.ITEM019.Height = 0.1980315F;
            this.ITEM019.Left = 5.917323F;
            this.ITEM019.Name = "ITEM019";
            this.ITEM019.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.ITEM019.Tag = "";
            this.ITEM019.Text = "ITEM019";
            this.ITEM019.Top = 0.03818898F;
            this.ITEM019.Width = 0.8700789F;
            // 
            // ITEM020
            // 
            this.ITEM020.CanGrow = false;
            this.ITEM020.DataField = "ITEM020";
            this.ITEM020.Height = 0.153937F;
            this.ITEM020.Left = 6.842126F;
            this.ITEM020.Name = "ITEM020";
            this.ITEM020.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: left; ddo-char-set: 1";
            this.ITEM020.Tag = "";
            this.ITEM020.Text = "ITEM020";
            this.ITEM020.Top = 0.01456693F;
            this.ITEM020.Width = 1.504167F;
            // 
            // ITEM022
            // 
            this.ITEM022.DataField = "ITEM022";
            this.ITEM022.Height = 0.2767717F;
            this.ITEM022.Left = 8.425198F;
            this.ITEM022.Name = "ITEM022";
            this.ITEM022.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: left; ddo-char-set: 1";
            this.ITEM022.Tag = "";
            this.ITEM022.Text = "ITEM022";
            this.ITEM022.Top = 0.03818898F;
            this.ITEM022.Width = 2.123278F;
            // 
            // ITEM021
            // 
            this.ITEM021.CanGrow = false;
            this.ITEM021.DataField = "ITEM021";
            this.ITEM021.Height = 0.1618111F;
            this.ITEM021.Left = 6.842126F;
            this.ITEM021.Name = "ITEM021";
            this.ITEM021.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: left; ddo-char-set: 1";
            this.ITEM021.Tag = "";
            this.ITEM021.Text = "ITEM021";
            this.ITEM021.Top = 0.1653543F;
            this.ITEM021.Width = 1.504167F;
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            // 
            // groupHeader1
            // 
            this.groupHeader1.DataField = "ITEM010";
            this.groupHeader1.Height = 0F;
            this.groupHeader1.Name = "groupHeader1";
            // 
            // ITEM010
            // 
            this.ITEM010.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.shape4,
            this.label6,
            this.label5,
            this.label4,
            this.shape3,
            this.line4,
            this.line5,
            this.line6,
            this.textBox3,
            this.textBox4,
            this.label2,
            this.label3,
            this.textBox5,
            this.textBox6,
            this.line7,
            this.line8,
            this.line9,
            this.label1,
            this.label7,
            this.総レコード数});
            this.ITEM010.Height = 0.8282316F;
            this.ITEM010.Name = "ITEM010";
            this.ITEM010.Format += new System.EventHandler(this.ITEM010_Format);
            // 
            // shape4
            // 
            this.shape4.Height = 0.7874016F;
            this.shape4.Left = 8.385827F;
            this.shape4.Name = "shape4";
            this.shape4.RoundingRadius = 9.999999F;
            this.shape4.Top = 0F;
            this.shape4.Width = 2.244095F;
            // 
            // label6
            // 
            this.label6.Height = 0.3070866F;
            this.label6.HyperLink = null;
            this.label6.Left = 9.87008F;
            this.label6.Name = "label6";
            this.label6.Style = "font-family: ＭＳ 明朝; font-size: 11pt; text-align: center; ddo-char-set: 1";
            this.label6.Text = "係　印";
            this.label6.Top = 0.03149607F;
            this.label6.Width = 0.7874007F;
            // 
            // label5
            // 
            this.label5.Height = 0.3070866F;
            this.label5.HyperLink = null;
            this.label5.Left = 9.098426F;
            this.label5.Name = "label5";
            this.label5.Style = "font-family: ＭＳ 明朝; font-size: 11pt; text-align: center; ddo-char-set: 1";
            this.label5.Text = "照　査";
            this.label5.Top = 0.03149607F;
            this.label5.Width = 0.7874007F;
            // 
            // label4
            // 
            this.label4.Height = 0.3070866F;
            this.label4.HyperLink = null;
            this.label4.Left = 8.326773F;
            this.label4.Name = "label4";
            this.label4.Style = "font-family: ＭＳ 明朝; font-size: 11pt; text-align: center; ddo-char-set: 1";
            this.label4.Text = "検　印";
            this.label4.Top = 0.03149607F;
            this.label4.Width = 0.7874008F;
            // 
            // shape3
            // 
            this.shape3.Height = 0.5905512F;
            this.shape3.Left = 4.330709F;
            this.shape3.Name = "shape3";
            this.shape3.RoundingRadius = 9.999999F;
            this.shape3.Top = 0F;
            this.shape3.Width = 2.480315F;
            // 
            // line4
            // 
            this.line4.Height = 0F;
            this.line4.Left = 4.330709F;
            this.line4.LineWeight = 1F;
            this.line4.Name = "line4";
            this.line4.Top = 0.2952756F;
            this.line4.Width = 2.480314F;
            this.line4.X1 = 4.330709F;
            this.line4.X2 = 6.811023F;
            this.line4.Y1 = 0.2952756F;
            this.line4.Y2 = 0.2952756F;
            // 
            // line5
            // 
            this.line5.Height = 0.5905512F;
            this.line5.Left = 5.118111F;
            this.line5.LineWeight = 1F;
            this.line5.Name = "line5";
            this.line5.Top = 0F;
            this.line5.Width = 0F;
            this.line5.X1 = 5.118111F;
            this.line5.X2 = 5.118111F;
            this.line5.Y1 = 0F;
            this.line5.Y2 = 0.5905512F;
            // 
            // line6
            // 
            this.line6.Height = 0.5905512F;
            this.line6.Left = 5.905513F;
            this.line6.LineWeight = 1F;
            this.line6.Name = "line6";
            this.line6.Top = 0F;
            this.line6.Width = 0F;
            this.line6.X1 = 5.905513F;
            this.line6.X2 = 5.905513F;
            this.line6.Y1 = 0F;
            this.line6.Y2 = 0.5905512F;
            // 
            // textBox3
            // 
            this.textBox3.DataField = "ITEM023";
            this.textBox3.Height = 0.1771654F;
            this.textBox3.Left = 5.157481F;
            this.textBox3.Name = "textBox3";
            this.textBox3.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.textBox3.SummaryGroup = "groupHeader1";
            this.textBox3.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox3.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox3.Tag = "";
            this.textBox3.Text = "ITEM023";
            this.textBox3.Top = 0.05118111F;
            this.textBox3.Width = 0.5641734F;
            // 
            // textBox4
            // 
            this.textBox4.DataField = "ITEM019";
            this.textBox4.Height = 0.173622F;
            this.textBox4.Left = 5.917323F;
            this.textBox4.Name = "textBox4";
            this.textBox4.OutputFormat = resources.GetString("textBox4.OutputFormat");
            this.textBox4.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.textBox4.SummaryGroup = "groupHeader1";
            this.textBox4.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox4.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox4.Tag = "";
            this.textBox4.Text = "ITEM019";
            this.textBox4.Top = 0.05118111F;
            this.textBox4.Width = 0.8700789F;
            // 
            // label2
            // 
            this.label2.Height = 0.1562828F;
            this.label2.HyperLink = null;
            this.label2.Left = 4.488189F;
            this.label2.Name = "label2";
            this.label2.Style = "font-family: ＭＳ 明朝; ddo-char-set: 1";
            this.label2.Text = "頁　計";
            this.label2.Top = 0.05118111F;
            this.label2.Width = 0.4680934F;
            // 
            // label3
            // 
            this.label3.Height = 0.1562828F;
            this.label3.HyperLink = null;
            this.label3.Left = 4.488189F;
            this.label3.Name = "label3";
            this.label3.Style = "font-family: ＭＳ 明朝; ddo-char-set: 1";
            this.label3.Text = "合　計\r\n";
            this.label3.Top = 0.3779528F;
            this.label3.Width = 0.4602199F;
            // 
            // textBox5
            // 
            this.textBox5.DataField = "ITEM023";
            this.textBox5.Height = 0.169685F;
            this.textBox5.Left = 5.153543F;
            this.textBox5.Name = "textBox5";
            this.textBox5.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.textBox5.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.textBox5.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.textBox5.Tag = "";
            this.textBox5.Text = "ITEM023";
            this.textBox5.Top = 0.3622048F;
            this.textBox5.Visible = false;
            this.textBox5.Width = 0.5641734F;
            // 
            // textBox6
            // 
            this.textBox6.DataField = "ITEM019";
            this.textBox6.Height = 0.173622F;
            this.textBox6.Left = 5.917323F;
            this.textBox6.Name = "textBox6";
            this.textBox6.OutputFormat = resources.GetString("textBox6.OutputFormat");
            this.textBox6.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.textBox6.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.textBox6.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.textBox6.Tag = "";
            this.textBox6.Text = "ITEM019";
            this.textBox6.Top = 0.3622048F;
            this.textBox6.Visible = false;
            this.textBox6.Width = 0.8700789F;
            // 
            // line7
            // 
            this.line7.Height = 0.7874016F;
            this.line7.Left = 9.094488F;
            this.line7.LineWeight = 1F;
            this.line7.Name = "line7";
            this.line7.Top = 0F;
            this.line7.Width = 0F;
            this.line7.X1 = 9.094488F;
            this.line7.X2 = 9.094488F;
            this.line7.Y1 = 0F;
            this.line7.Y2 = 0.7874016F;
            // 
            // line8
            // 
            this.line8.Height = 0.7874016F;
            this.line8.Left = 9.88189F;
            this.line8.LineWeight = 1F;
            this.line8.Name = "line8";
            this.line8.Top = 0F;
            this.line8.Width = 0F;
            this.line8.X1 = 9.88189F;
            this.line8.X2 = 9.88189F;
            this.line8.Y1 = 0F;
            this.line8.Y2 = 0.7874016F;
            // 
            // line9
            // 
            this.line9.Height = 0F;
            this.line9.Left = 8.385827F;
            this.line9.LineWeight = 1F;
            this.line9.Name = "line9";
            this.line9.Top = 0.2362205F;
            this.line9.Width = 2.244093F;
            this.line9.X1 = 8.385827F;
            this.line9.X2 = 10.62992F;
            this.line9.Y1 = 0.2362205F;
            this.line9.Y2 = 0.2362205F;
            // 
            // label1
            // 
            this.label1.Height = 0.1653543F;
            this.label1.HyperLink = null;
            this.label1.Left = 5.720473F;
            this.label1.Name = "label1";
            this.label1.Style = "font-family: ＭＳ 明朝; font-size: 10pt; ddo-char-set: 1";
            this.label1.Text = "件";
            this.label1.Top = 0.05905512F;
            this.label1.Width = 0.1736393F;
            // 
            // label7
            // 
            this.label7.Height = 0.1653543F;
            this.label7.HyperLink = null;
            this.label7.Left = 5.720473F;
            this.label7.Name = "label7";
            this.label7.Style = "font-family: ＭＳ 明朝; font-size: 10pt; ddo-char-set: 1";
            this.label7.Text = "件";
            this.label7.Top = 0.3748032F;
            this.label7.Width = 0.1736393F;
            // 
            // 総レコード数
            // 
            this.総レコード数.DataField = "ITEM024";
            this.総レコード数.Height = 0.1574803F;
            this.総レコード数.Left = 2.362205F;
            this.総レコード数.Name = "総レコード数";
            this.総レコード数.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.総レコード数.Tag = "";
            this.総レコード数.Text = "総レコード数";
            this.総レコード数.Top = 0.3149607F;
            this.総レコード数.Visible = false;
            this.総レコード数.Width = 1.023622F;
            // 
            // KYKR1041R
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.1968504F;
            this.PageSettings.Margins.Left = 0.3937007F;
            this.PageSettings.Margins.Right = 0.3937007F;
            this.PageSettings.Margins.Top = 0.5905512F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
            this.PageSettings.PaperHeight = 11.69291F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.267716F;
            this.PrintWidth = 10.70866F;
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.groupHeader1);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.ITEM010);
            this.Sections.Add(this.pageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.ITEM002)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM001)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM005)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM006)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtToday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM011)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM012)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM013)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM014)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM015)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM016)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM017)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM018)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM019)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM020)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM022)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM021)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.総レコード数)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.Line 直線88;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線90;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線91;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線92;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線93;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線94;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線95;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線96;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線97;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線98;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線99;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM011;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM012;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM013;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM014;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM015;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM016;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM017;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM018;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM019;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM020;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM022;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM021;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス0;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM001;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM002;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル2;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線1;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線3;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル4;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM005;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル8;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM006;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtToday;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト13;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル15;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線16;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線17;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線18;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線19;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線21;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線22;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線23;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線24;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線25;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線26;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線28;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線29;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル31;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル32;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル33;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル34;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル35;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル36;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル37;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル38;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル39;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル40;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル41;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape1;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader groupHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter ITEM010;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape3;
        private GrapeCity.ActiveReports.SectionReportModel.Line line4;
        private GrapeCity.ActiveReports.SectionReportModel.Line line5;
        private GrapeCity.ActiveReports.SectionReportModel.Line line6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox4;
        private GrapeCity.ActiveReports.SectionReportModel.Label label2;
        private GrapeCity.ActiveReports.SectionReportModel.Label label3;
        private GrapeCity.ActiveReports.SectionReportModel.Label label6;
        private GrapeCity.ActiveReports.SectionReportModel.Label label5;
        private GrapeCity.ActiveReports.SectionReportModel.Label label4;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox6;
        private GrapeCity.ActiveReports.SectionReportModel.Line line7;
        private GrapeCity.ActiveReports.SectionReportModel.Line line8;
        private GrapeCity.ActiveReports.SectionReportModel.Line line9;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 総レコード数;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox2;

    }
}
