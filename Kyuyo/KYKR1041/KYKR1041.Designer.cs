﻿namespace jp.co.fsi.ky.kykr1041
{
    partial class KYKR1041
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpNengetsu = new System.Windows.Forms.GroupBox();
            this.lblMonthNengetsu = new System.Windows.Forms.Label();
            this.lblYearNengetsu = new System.Windows.Forms.Label();
            this.txtMonthNengetsu = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblGengoNengetsu = new System.Windows.Forms.Label();
            this.txtGengoYearNengetsu = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMizuageShisho = new System.Windows.Forms.Label();
            this.grpOrder = new System.Windows.Forms.GroupBox();
            this.rdoOrder2 = new System.Windows.Forms.RadioButton();
            this.rdoOrder1 = new System.Windows.Forms.RadioButton();
            this.grpFurikomi = new System.Windows.Forms.GroupBox();
            this.lblDayFurikomi = new System.Windows.Forms.Label();
            this.txtDayFurikomi = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMonthFurikomi = new System.Windows.Forms.Label();
            this.lblYearFurikomi = new System.Windows.Forms.Label();
            this.txtMonthFurikomi = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblGengoFurikomi = new System.Windows.Forms.Label();
            this.txtGengoYearFurikomi = new jp.co.fsi.common.controls.FsiTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.grpNengetsu.SuspendLayout();
            this.grpOrder.SuspendLayout();
            this.grpFurikomi.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Text = "銀行振込一覧表";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Size = new System.Drawing.Size(847, 100);
            // 
            // grpNengetsu
            // 
            this.grpNengetsu.Controls.Add(this.lblMonthNengetsu);
            this.grpNengetsu.Controls.Add(this.lblYearNengetsu);
            this.grpNengetsu.Controls.Add(this.txtMonthNengetsu);
            this.grpNengetsu.Controls.Add(this.lblGengoNengetsu);
            this.grpNengetsu.Controls.Add(this.txtGengoYearNengetsu);
            this.grpNengetsu.Controls.Add(this.lblMizuageShisho);
            this.grpNengetsu.Font = new System.Drawing.Font("ＭＳ ゴシック", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.grpNengetsu.Location = new System.Drawing.Point(12, 53);
            this.grpNengetsu.Name = "grpNengetsu";
            this.grpNengetsu.Size = new System.Drawing.Size(215, 67);
            this.grpNengetsu.TabIndex = 1;
            this.grpNengetsu.TabStop = false;
            this.grpNengetsu.Text = "支給年月";
            // 
            // lblMonthNengetsu
            // 
            this.lblMonthNengetsu.BackColor = System.Drawing.Color.Silver;
            this.lblMonthNengetsu.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMonthNengetsu.Location = new System.Drawing.Point(164, 29);
            this.lblMonthNengetsu.Name = "lblMonthNengetsu";
            this.lblMonthNengetsu.Size = new System.Drawing.Size(23, 20);
            this.lblMonthNengetsu.TabIndex = 4;
            this.lblMonthNengetsu.Text = "月";
            // 
            // lblYearNengetsu
            // 
            this.lblYearNengetsu.BackColor = System.Drawing.Color.Silver;
            this.lblYearNengetsu.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblYearNengetsu.Location = new System.Drawing.Point(96, 29);
            this.lblYearNengetsu.Name = "lblYearNengetsu";
            this.lblYearNengetsu.Size = new System.Drawing.Size(24, 20);
            this.lblYearNengetsu.TabIndex = 2;
            this.lblYearNengetsu.Text = "年";
            // 
            // txtMonthNengetsu
            // 
            this.txtMonthNengetsu.AutoSizeFromLength = false;
            this.txtMonthNengetsu.DisplayLength = null;
            this.txtMonthNengetsu.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMonthNengetsu.Location = new System.Drawing.Point(124, 27);
            this.txtMonthNengetsu.MaxLength = 2;
            this.txtMonthNengetsu.Name = "txtMonthNengetsu";
            this.txtMonthNengetsu.Size = new System.Drawing.Size(34, 23);
            this.txtMonthNengetsu.TabIndex = 3;
            this.txtMonthNengetsu.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonthNengetsu.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthNengetsu_Validating);
            // 
            // lblGengoNengetsu
            // 
            this.lblGengoNengetsu.BackColor = System.Drawing.Color.Silver;
            this.lblGengoNengetsu.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGengoNengetsu.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGengoNengetsu.Location = new System.Drawing.Point(18, 29);
            this.lblGengoNengetsu.Name = "lblGengoNengetsu";
            this.lblGengoNengetsu.Size = new System.Drawing.Size(42, 20);
            this.lblGengoNengetsu.TabIndex = 0;
            this.lblGengoNengetsu.Text = "平成";
            // 
            // txtGengoYearNengetsu
            // 
            this.txtGengoYearNengetsu.AutoSizeFromLength = false;
            this.txtGengoYearNengetsu.DisplayLength = null;
            this.txtGengoYearNengetsu.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGengoYearNengetsu.Location = new System.Drawing.Point(61, 27);
            this.txtGengoYearNengetsu.MaxLength = 2;
            this.txtGengoYearNengetsu.Name = "txtGengoYearNengetsu";
            this.txtGengoYearNengetsu.Size = new System.Drawing.Size(34, 23);
            this.txtGengoYearNengetsu.TabIndex = 1;
            this.txtGengoYearNengetsu.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGengoYearNengetsu.Validating += new System.ComponentModel.CancelEventHandler(this.txtGengoYearNengetsu_Validating);
            // 
            // lblMizuageShisho
            // 
            this.lblMizuageShisho.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShisho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShisho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblMizuageShisho.Location = new System.Drawing.Point(14, 25);
            this.lblMizuageShisho.Name = "lblMizuageShisho";
            this.lblMizuageShisho.Size = new System.Drawing.Size(175, 28);
            this.lblMizuageShisho.TabIndex = 8;
            this.lblMizuageShisho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // grpOrder
            // 
            this.grpOrder.Controls.Add(this.rdoOrder2);
            this.grpOrder.Controls.Add(this.rdoOrder1);
            this.grpOrder.Font = new System.Drawing.Font("ＭＳ ゴシック", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.grpOrder.Location = new System.Drawing.Point(12, 209);
            this.grpOrder.Name = "grpOrder";
            this.grpOrder.Size = new System.Drawing.Size(204, 53);
            this.grpOrder.TabIndex = 3;
            this.grpOrder.TabStop = false;
            this.grpOrder.Text = "出力順";
            // 
            // rdoOrder2
            // 
            this.rdoOrder2.AutoSize = true;
            this.rdoOrder2.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoOrder2.Location = new System.Drawing.Point(106, 20);
            this.rdoOrder2.Name = "rdoOrder2";
            this.rdoOrder2.Size = new System.Drawing.Size(74, 20);
            this.rdoOrder2.TabIndex = 1;
            this.rdoOrder2.Text = "部門順";
            this.rdoOrder2.UseVisualStyleBackColor = true;
            this.rdoOrder2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rdoOrder2_KeyDown);
            // 
            // rdoOrder1
            // 
            this.rdoOrder1.AutoSize = true;
            this.rdoOrder1.Checked = true;
            this.rdoOrder1.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoOrder1.Location = new System.Drawing.Point(23, 20);
            this.rdoOrder1.Name = "rdoOrder1";
            this.rdoOrder1.Size = new System.Drawing.Size(74, 20);
            this.rdoOrder1.TabIndex = 0;
            this.rdoOrder1.TabStop = true;
            this.rdoOrder1.Text = "社員順";
            this.rdoOrder1.UseVisualStyleBackColor = true;
            this.rdoOrder1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rdoOrder1_KeyDown);
            // 
            // grpFurikomi
            // 
            this.grpFurikomi.Controls.Add(this.lblDayFurikomi);
            this.grpFurikomi.Controls.Add(this.txtDayFurikomi);
            this.grpFurikomi.Controls.Add(this.lblMonthFurikomi);
            this.grpFurikomi.Controls.Add(this.lblYearFurikomi);
            this.grpFurikomi.Controls.Add(this.txtMonthFurikomi);
            this.grpFurikomi.Controls.Add(this.lblGengoFurikomi);
            this.grpFurikomi.Controls.Add(this.txtGengoYearFurikomi);
            this.grpFurikomi.Controls.Add(this.label1);
            this.grpFurikomi.Font = new System.Drawing.Font("ＭＳ ゴシック", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.grpFurikomi.Location = new System.Drawing.Point(12, 126);
            this.grpFurikomi.Name = "grpFurikomi";
            this.grpFurikomi.Size = new System.Drawing.Size(279, 77);
            this.grpFurikomi.TabIndex = 2;
            this.grpFurikomi.TabStop = false;
            this.grpFurikomi.Text = "振込日";
            // 
            // lblDayFurikomi
            // 
            this.lblDayFurikomi.BackColor = System.Drawing.Color.Silver;
            this.lblDayFurikomi.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDayFurikomi.Location = new System.Drawing.Point(228, 33);
            this.lblDayFurikomi.Name = "lblDayFurikomi";
            this.lblDayFurikomi.Size = new System.Drawing.Size(24, 16);
            this.lblDayFurikomi.TabIndex = 6;
            this.lblDayFurikomi.Text = "日";
            // 
            // txtDayFurikomi
            // 
            this.txtDayFurikomi.AutoSizeFromLength = false;
            this.txtDayFurikomi.DisplayLength = null;
            this.txtDayFurikomi.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDayFurikomi.Location = new System.Drawing.Point(188, 30);
            this.txtDayFurikomi.MaxLength = 2;
            this.txtDayFurikomi.Name = "txtDayFurikomi";
            this.txtDayFurikomi.Size = new System.Drawing.Size(34, 23);
            this.txtDayFurikomi.TabIndex = 5;
            this.txtDayFurikomi.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDayFurikomi.Validating += new System.ComponentModel.CancelEventHandler(this.txtDayFurikomi_Validating);
            // 
            // lblMonthFurikomi
            // 
            this.lblMonthFurikomi.BackColor = System.Drawing.Color.Silver;
            this.lblMonthFurikomi.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMonthFurikomi.Location = new System.Drawing.Point(164, 33);
            this.lblMonthFurikomi.Name = "lblMonthFurikomi";
            this.lblMonthFurikomi.Size = new System.Drawing.Size(24, 16);
            this.lblMonthFurikomi.TabIndex = 4;
            this.lblMonthFurikomi.Text = "月";
            // 
            // lblYearFurikomi
            // 
            this.lblYearFurikomi.BackColor = System.Drawing.Color.Silver;
            this.lblYearFurikomi.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblYearFurikomi.Location = new System.Drawing.Point(96, 33);
            this.lblYearFurikomi.Name = "lblYearFurikomi";
            this.lblYearFurikomi.Size = new System.Drawing.Size(24, 16);
            this.lblYearFurikomi.TabIndex = 2;
            this.lblYearFurikomi.Text = "年";
            // 
            // txtMonthFurikomi
            // 
            this.txtMonthFurikomi.AutoSizeFromLength = false;
            this.txtMonthFurikomi.DisplayLength = null;
            this.txtMonthFurikomi.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMonthFurikomi.Location = new System.Drawing.Point(124, 30);
            this.txtMonthFurikomi.MaxLength = 2;
            this.txtMonthFurikomi.Name = "txtMonthFurikomi";
            this.txtMonthFurikomi.Size = new System.Drawing.Size(34, 23);
            this.txtMonthFurikomi.TabIndex = 3;
            this.txtMonthFurikomi.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonthFurikomi.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthFurikomi_Validating);
            // 
            // lblGengoFurikomi
            // 
            this.lblGengoFurikomi.BackColor = System.Drawing.Color.Silver;
            this.lblGengoFurikomi.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGengoFurikomi.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGengoFurikomi.Location = new System.Drawing.Point(17, 32);
            this.lblGengoFurikomi.Name = "lblGengoFurikomi";
            this.lblGengoFurikomi.Size = new System.Drawing.Size(42, 20);
            this.lblGengoFurikomi.TabIndex = 0;
            this.lblGengoFurikomi.Text = "平成";
            // 
            // txtGengoYearFurikomi
            // 
            this.txtGengoYearFurikomi.AutoSizeFromLength = false;
            this.txtGengoYearFurikomi.DisplayLength = null;
            this.txtGengoYearFurikomi.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGengoYearFurikomi.Location = new System.Drawing.Point(61, 30);
            this.txtGengoYearFurikomi.MaxLength = 2;
            this.txtGengoYearFurikomi.Name = "txtGengoYearFurikomi";
            this.txtGengoYearFurikomi.Size = new System.Drawing.Size(34, 23);
            this.txtGengoYearFurikomi.TabIndex = 1;
            this.txtGengoYearFurikomi.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGengoYearFurikomi.Validating += new System.ComponentModel.CancelEventHandler(this.txtGengoYearFurikomi_Validating);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Silver;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.label1.Location = new System.Drawing.Point(13, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(242, 30);
            this.label1.TabIndex = 8;
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // KYKR1041
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 638);
            this.Controls.Add(this.grpFurikomi);
            this.Controls.Add(this.grpOrder);
            this.Controls.Add(this.grpNengetsu);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "KYKR1041";
            this.Par1 = "1";
            this.Text = "銀行振込一覧表";
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.grpNengetsu, 0);
            this.Controls.SetChildIndex(this.grpOrder, 0);
            this.Controls.SetChildIndex(this.grpFurikomi, 0);
            this.pnlDebug.ResumeLayout(false);
            this.grpNengetsu.ResumeLayout(false);
            this.grpNengetsu.PerformLayout();
            this.grpOrder.ResumeLayout(false);
            this.grpOrder.PerformLayout();
            this.grpFurikomi.ResumeLayout(false);
            this.grpFurikomi.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpNengetsu;
        private System.Windows.Forms.Label lblMonthNengetsu;
        private System.Windows.Forms.Label lblYearNengetsu;
        private jp.co.fsi.common.controls.FsiTextBox txtMonthNengetsu;
        private System.Windows.Forms.Label lblGengoNengetsu;
        private jp.co.fsi.common.controls.FsiTextBox txtGengoYearNengetsu;
        private System.Windows.Forms.GroupBox grpOrder;
        private System.Windows.Forms.RadioButton rdoOrder2;
        private System.Windows.Forms.RadioButton rdoOrder1;
        private System.Windows.Forms.GroupBox grpFurikomi;
        private System.Windows.Forms.Label lblDayFurikomi;
        private common.controls.FsiTextBox txtDayFurikomi;
        private System.Windows.Forms.Label lblMonthFurikomi;
        private System.Windows.Forms.Label lblYearFurikomi;
        private common.controls.FsiTextBox txtMonthFurikomi;
        private System.Windows.Forms.Label lblGengoFurikomi;
        private common.controls.FsiTextBox txtGengoYearFurikomi;
        private System.Windows.Forms.Label lblMizuageShisho;
        private System.Windows.Forms.Label label1;
    }
}