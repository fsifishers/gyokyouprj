﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.access;
using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;
using GrapeCity.ActiveReports;

namespace jp.co.fsi.ky.kykr1021
{
    /// <summary>
    /// 勤怠項目一覧表(KYKR1021)
    /// </summary>
    public partial class KYKR1021 : BasePgForm
    {
        #region 定数
        public const string KYUYO = "1";    // 給与
        public const string SHOYO = "2";    // 賞与

        /// <summary>
        /// 明細が２行の場合のフィールド数
        /// </summary>
        public const int rptCols1 = 94;

        /// <summary>
        /// 明細が３行になる場合のフィールド数
        /// </summary>
        public const int rptCols2 = 106;

        /// <summary>
        /// 明細が４行になる場合のフィールド数
        /// </summary>
        public const int rptCols3 = 118;
        #endregion

        #region プロパティ
        private int _rowCnt = 24;
        public int RowCnt
        {
            get
            {
                return _rowCnt;
            }
            set
            {
                _rowCnt = value;
            }
        }

        /// <summary>
        /// 印刷用COLUMN定数
        /// </summary>
        public int rptCols;

        public DataTable dtRptSti;
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KYKR1021()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 支給年月の初期値取得
            string[] jpDate = getShikyuDate();
            // 支給月(自)
            lblGengoNengetsuFr.Text     = jpDate[0];
            txtGengoYearNengetsuFr.Text = jpDate[2];
            txtMonthNengetsuFr.Text     = jpDate[3];
            // 支給月(至)
            lblGengoNengetsuTo.Text     = jpDate[0];
            txtGengoYearNengetsuTo.Text = jpDate[2];
            txtMonthNengetsuTo.Text     = jpDate[3];

            // 帳票タイトル、項目数取得
            getReportInfo(this.Par1, this.Par2);

            // 初期フォーカス
            this.txtGengoYearNengetsuFr.Focus();
            this.txtGengoYearNengetsuFr.SelectAll();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 元号年とコード項目に
            // フォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtGengoYearNengetsuFr":
                case "txtGengoYearNengetsuTo":
                case "txtShainCdFr":
                case "txtShainCdTo":
                case "txtBumonCdFr":
                case "txtBumonCdTo":
                case "txtBukaCdFr":
                case "txtBukaCdTo":
                    this.btnF1.Enabled = true;
                    break;
                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        public override void PressF1()
        {
            Assembly asm;
            Type t;

            //MEMO:現状アクティブなコントロールごとに処理を実装してください。
            switch (this.ActiveCtlNm)
            {
                case "txtShainCdFr":
                case "txtShainCdTo":
                    #region 社員検索
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom("KYCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.ky.kycm1021.KYCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            if (this.ActiveCtlNm == "txtShainCdFr")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.Par1 = "1";                     // ダイアログ起動パラメータ
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.txtShainCdFr.Text = result[0];
                                    this.lblShainCdFr.Text = result[1];
                                }
                            }
                            else if (this.ActiveCtlNm == "txtShainCdTo")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.Par1 = "1";                     // ダイアログ起動パラメータ
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.txtShainCdTo.Text = result[0];
                                    this.lblShainCdTo.Text = result[1];
                                }
                            }
                        }
                    }
                    #endregion
                    break;
                case "txtBumonCdFr":
                case "txtBumonCdTo":
                    #region 部門検索
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom("KYCM1031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.ky.kycm1031.KYCM1031");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            if (this.ActiveCtlNm == "txtBumonCdFr")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.Par1 = "1";
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.txtBumonCdFr.Text = result[0];
                                    this.lblBumonCdFr.Text = result[1];
                                    this.txtBukaCdFr.Text = "";
                                    this.lblBukaCdFr.Text = "先　頭";
                                }
                            }
                            else if (this.ActiveCtlNm == "txtBumonCdTo")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.Par1 = "1";
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.txtBumonCdTo.Text = result[0];
                                    this.lblBumonCdTo.Text = result[1];
                                    this.txtBukaCdTo.Text = "";
                                    this.lblBukaCdTo.Text = "最　後";
                                }
                            }
                        }
                    }
                    #endregion
                    break;
                case "txtBukaCdFr":
                case "txtBukaCdTo":
                    #region 部課検索
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom("KYCM1041.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.ky.kycm1041.KYCM1041");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            if (this.ActiveCtlNm == "txtBukaCdFr"
                                && !ValChk.IsEmpty(this.txtBumonCdFr.Text))
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.Par1 = "1";                         // 検索ダイアログ起動パラメータ
                                frm.InData = this.txtBumonCdFr.Text;    // (string)部門コード
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.txtBukaCdFr.Text = result[0];
                                    this.lblBukaCdFr.Text = result[1];
                                }
                            }
                            else if (this.ActiveCtlNm == "txtBukaCdTo"
                                && !ValChk.IsEmpty(this.txtBumonCdTo.Text))
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.Par1 = "1";                         // 検索ダイアログ起動パラメータ
                                frm.InData = this.txtBumonCdTo.Text;    // (string)部門コード
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.txtBukaCdTo.Text = result[0];
                                    this.lblBukaCdTo.Text = result[1];
                                }
                            }
                        }
                    }
                    #endregion
                    break;
                case "txtGengoYearNengetsuFr":
                case "txtGengoYearNengetsuTo":
                    #region 元号検索
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom("CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            if (this.ActiveCtlNm == "txtGengoYearNengetsuFr")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.InData = this.lblGengoNengetsuFr.Text;
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.lblGengoNengetsuFr.Text = result[1];

                                    // 存在しない日付の場合、補正して存在する日付に戻す
                                    string[] arrJpDate =
                                        Util.FixJpDate(this.lblGengoNengetsuFr.Text,
                                            this.txtGengoYearNengetsuFr.Text,
                                            this.txtMonthNengetsuFr.Text,
                                            "1",
                                            this.Dba);
                                    this.lblGengoNengetsuFr.Text = arrJpDate[0];
                                    this.txtGengoYearNengetsuFr.Text = arrJpDate[2];
                                    this.txtMonthNengetsuFr.Text = arrJpDate[3];
                                }
                            }
                            else if (this.ActiveCtlNm == "txtGengoYearNengetsuTo")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.InData = this.lblGengoNengetsuTo.Text;
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.lblGengoNengetsuTo.Text = result[1];

                                    // 存在しない日付の場合、補正して存在する日付に戻す
                                    string[] arrJpDate =
                                        Util.FixJpDate(this.lblGengoNengetsuTo.Text,
                                            this.txtGengoYearNengetsuTo.Text,
                                            this.txtMonthNengetsuTo.Text,
                                            "1",
                                            this.Dba);
                                    this.lblGengoNengetsuTo.Text = arrJpDate[0];
                                    this.txtGengoYearNengetsuTo.Text = arrJpDate[2];
                                    this.txtMonthNengetsuTo.Text = arrJpDate[3];
                                }
                            }
                        }
                        
                    }
                    #endregion
                    break;
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            if (Msg.ConfYesNo("実行しますか？") == DialogResult.Yes)
            {
                // プレビュー処理
                DoPrint(true);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            if (Msg.ConfYesNo("実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }

        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            // 設定画面の起動
            // MEMO:原則としてここで渡す帳票IDの設定はReport.csvに保持していることが前提ですが、
            // 保持していない場合は、設定画面での保存(F6)時に新規に設定が保持されます。
            //PrintSettingForm psForm = new PrintSettingForm(new string[1] { "KYKR1021R" });
            string[] reportId = reportInfo();
            PrintSettingForm psForm = new PrintSettingForm(reportId);
            psForm.ShowDialog();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 社員コード(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShainCdFr_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtShainCdFr.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtShainCdFr.SelectAll();
                e.Cancel = true;
                return;
            }

            // コードを元に名称を取得する
            // 取得された場合、名称をラベルに反映する
            if (this.txtShainCdFr.Text.Length > 0)
            {
                this.lblShainCdFr.Text = this.Dba.GetName(this.UInfo, "TB_KY_SHAIN_JOHO", " ", this.txtShainCdFr.Text);
            }
            else
            {
                this.lblShainCdFr.Text = "先　頭";
            }
        }

        /// <summary>
        /// 社員コード(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShainCdTo_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtShainCdTo.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtShainCdTo.SelectAll();
                e.Cancel = true;
                return;
            }

            // コードを元に名称を取得する
            // 取得された場合、名称をラベルに反映する
            if (this.txtShainCdTo.Text.Length > 0)
            {
                this.lblShainCdTo.Text = this.Dba.GetName(this.UInfo, "TB_KY_SHAIN_JOHO", " ", this.txtShainCdTo.Text);
            }
            else
            {
                this.lblShainCdTo.Text = "最　後";
            }
        }

        /// <summary>
        /// 部門コード(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtBumonCdFr_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtBumonCdFr.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtBumonCdFr.SelectAll();
                e.Cancel = true;
                return;
            }

            // コードを元に名称を取得する
            // 取得された場合、名称をラベルに反映する
            if (this.txtBumonCdFr.Text.Length > 0)
            {
                this.lblBumonCdFr.Text = this.Dba.GetName(this.UInfo, "TB_KY_BUMON", " ", this.txtBumonCdFr.Text);
            }
            else
            {
                this.lblBumonCdFr.Text = "先　頭";
            }
        }

        /// <summary>
        /// 部門コード(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtBumonCdTo_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtBumonCdTo.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtBumonCdTo.SelectAll();
                e.Cancel = true;
                return;
            }

            // コードを元に名称を取得する
            // 取得された場合、名称をラベルに反映する
            if (this.txtBumonCdTo.Text.Length > 0)
            {
                this.lblBumonCdTo.Text = this.Dba.GetName(this.UInfo, "TB_KY_BUMON", " ", this.txtBumonCdTo.Text);
            }
            else
            {
                this.lblBumonCdTo.Text = "最　後";
            }
        }

        /// <summary>
        /// 部課コード(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtBukaCdFr_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtBukaCdFr.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtBukaCdFr.SelectAll();
                e.Cancel = true;
                return;
            }

            // コードを元に名称を取得する
            // 取得された場合、名称をラベルに反映する
            if (this.txtBukaCdFr.Text.Length > 0 && this.txtBumonCdFr.Text.Length > 0)
            {
                this.lblBukaCdFr.Text = this.Dba.GetBukaNm(this.UInfo, this.txtBumonCdFr.Text, this.txtBukaCdFr.Text);
            }
            else
            {
                this.lblBukaCdFr.Text = "先　頭";
            }
        }

        /// <summary>
        /// 部課コード(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtBukaCdTo_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtBukaCdTo.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtBukaCdTo.SelectAll();
                e.Cancel = true;
                return;
            }

            // コードを元に名称を取得する
            // 取得された場合、名称をラベルに反映する
            if (this.txtBukaCdTo.Text.Length > 0 && this.txtBumonCdTo.Text.Length > 0)
            {
                this.lblBukaCdTo.Text = this.Dba.GetBukaNm(this.UInfo,this.txtBumonCdTo.Text, this.txtBukaCdTo.Text);
            }
            else
            {
                this.lblBukaCdTo.Text = "最　後";
            }
        }

        /// <summary>
        /// 部課CD(至)のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtBukaCdTo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
                {
                    // ﾌﾟﾚﾋﾞｭｰ処理
                    DoPrint(true);
                }
                else
                {
                    this.txtBukaCdTo.Focus();
                }
            }
        }

        /// <summary>
        /// 支給月自(年)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGengoYearNengetsuFr_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtGengoYearNengetsuFr.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtGengoYearNengetsuFr.SelectAll();
                e.Cancel = true;
                return;
            }

            // 空の場合、0年として処理
            if (ValChk.IsEmpty(this.txtGengoYearNengetsuFr.Text))
            {
                this.txtGengoYearNengetsuFr.Text = "0";
            }

            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            this.SetJpDateNengetsu(Util.FixJpDate(this.lblGengoNengetsuFr.Text, this.txtGengoYearNengetsuFr.Text,
                this.txtMonthNengetsuFr.Text, "1", this.Dba));
        }

        /// <summary>
        /// 支給月自(月)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMonthNengetsuFr_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtMonthNengetsuFr.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtMonthNengetsuFr.SelectAll();
                e.Cancel = true;
                return;
            }

            if (ValChk.IsEmpty(this.txtMonthNengetsuFr.Text) || Util.ToInt(this.txtMonthNengetsuFr.Text) == 0)
            {
                // 空の場合、1月として処理
                this.txtMonthNengetsuFr.Text = "1";
            }
            else
            {
                // 12を超える月が入力された場合、12月として処理
                if (Util.ToInt(this.txtMonthNengetsuFr.Text) > 12)
                {
                    this.txtMonthNengetsuFr.Text = "12";
                }
            }

            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            this.SetJpDateNengetsu(Util.FixJpDate(this.lblGengoNengetsuFr.Text, this.txtGengoYearNengetsuFr.Text,
                this.txtMonthNengetsuFr.Text, "1", this.Dba));
        }

        /// <summary>
        /// 支給月至(年)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGengoYearNengetsuTo_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtGengoYearNengetsuTo.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtGengoYearNengetsuTo.SelectAll();
                e.Cancel = true;
                return;
            }

            // 空の場合、0年として処理
            if (ValChk.IsEmpty(this.txtGengoYearNengetsuTo.Text))
            {
                this.txtGengoYearNengetsuTo.Text = "0";
            }

            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            this.SetJpDateTaisho(Util.FixJpDate(this.lblGengoNengetsuTo.Text, this.txtGengoYearNengetsuTo.Text,
                this.txtMonthNengetsuTo.Text, "1", this.Dba));
        }

        /// <summary>
        /// 支給月至(月)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMonthNengetsuTo_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtMonthNengetsuTo.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtMonthNengetsuTo.SelectAll();
                e.Cancel = true;
                return;
            }

            if (ValChk.IsEmpty(this.txtMonthNengetsuTo.Text) || Util.ToInt(this.txtMonthNengetsuTo.Text) == 0)
            {
                // 空の場合、1月として処理
                this.txtMonthNengetsuTo.Text = "1";
            }
            else
            {
                // 12を超える月が入力された場合、12月として処理
                if (Util.ToInt(this.txtMonthNengetsuTo.Text) > 12)
                {
                    this.txtMonthNengetsuTo.Text = "12";
                }
            }

            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            this.SetJpDateTaisho(Util.FixJpDate(this.lblGengoNengetsuTo.Text, this.txtGengoYearNengetsuTo.Text,
                this.txtMonthNengetsuTo.Text, "1", this.Dba));
        }

        /// <summary>
        ///  出力方法ラジオボタン変更時の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rdoHoho_CheckedChanged(object sender, EventArgs e)
        {
            if (rdoHoho1.Checked)   
            {
                // 出力方法：社員別の時は、並び順：社員順は利用可能
                rdoOrder1.Enabled = true;
            }
            else
            {
                // 出力方法：部課別の時は、並び順：社員順は利用不可
                rdoOrder2.Checked = true;
                rdoOrder1.Enabled = false;
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview)
        {
            try
            {
                bool dataFlag;

#if DEBUG
                this.Dba.DeleteWork("PR_KY_TBL", this.UnqId);
#endif
                this.Dba.BeginTransaction();

                // 帳票出力用にワークテーブルにデータを作成
                //dataFlag = MakeWkData();
                dataFlag = MakeWkData00();

                // 帳票出力
                if (dataFlag)
                {
                    // 取得列の定義
                    StringBuilder cols = ColsArray(rptCols, "");  //new StringBuilder();

                    // バインドパラメータの設定
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                    string orderby = "";
                    if (this.rdoOrder2.Checked == false)
                    {
                        orderby = "SORT ASC";
                    }
                    else
                    {
                        //orderby = "ITEM005 ASC , SORT ASC";
                        orderby = "ITEM005, ITEM006, ITEM004";
                    }
                    // データの取得
                    DataTable dtOutput = this.Dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_KY_TBL", "GUID = @GUID", orderby, dpc);

                    // 帳票オブジェクトをインスタンス化
                    string[] reportId = reportInfo();
                    //KYKR1021R rpt = new KYKR1021R(dtOutput);
                    // 最大項目数で出力レポート、出力カラム数を設定する。
                    jp.co.fsi.common.report.BaseReport rpt;
                    switch (rptCols)
                    {
                        case rptCols1:
                            rpt = new KYKR1021R(dtOutput, reportId[0]);
                            break;
                        case rptCols2:
                            rpt = new KYKR1022R(dtOutput, reportId[0]);
                            break;
                        case rptCols3:
                            rpt = new KYKR1023R(dtOutput, reportId[0]);
                            break;
                        default:
                            rpt = new KYKR1021R(dtOutput, reportId[0]);
                            break;
                    }

                    if (isPreview)
                    {
                        // プレビュー画面表示
                        PreviewForm pFrm = new PreviewForm(rpt, this.UnqId);
                        pFrm.WindowState = FormWindowState.Maximized;
                        pFrm.Show();
                    }
                    else
                    {
                        // 直接印刷
                        rpt.Run(false);
                        rpt.Document.Print(true, true, false);
                    }
                }
                else
                {
                    Msg.Info("該当データが存在しません。");
                    return;
                }

            }
            finally
            {
#if DEBUG
                this.Dba.Commit();
#else
                this.Dba.Rollback();
#endif
            }
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData()
        {
            #region データ取得前準備
            // 集計期間を西暦にして保持
            DateTime dateFr = Util.ConvAdDate(this.lblGengoNengetsuFr.Text, this.txtGengoYearNengetsuFr.Text,
                    this.txtMonthNengetsuFr.Text, "1", this.Dba);
            DateTime dateTo = Util.ConvAdDate(this.lblGengoNengetsuTo.Text, this.txtGengoYearNengetsuTo.Text,
                    this.txtMonthNengetsuTo.Text, "1", this.Dba);
            // 集計期間を和暦で保持
            string[] jpDateFr = Util.ConvJpDate(dateFr, this.Dba);
            string[] jpDateTo = Util.ConvJpDate(dateTo, this.Dba);
            // 社員コード設定
            string shainCdFr;
            string shainCdTo;
            if (Util.ToDecimal(txtShainCdFr.Text) > 0) 
            {
                shainCdFr = txtShainCdFr.Text;
            }
            else
            {
                shainCdFr = "0";
            }
            if (Util.ToDecimal(txtShainCdTo.Text) > 0) 
            {
                shainCdTo = txtShainCdTo.Text;
            }
            else
            {
                shainCdTo = "999999";
            }
            // 部門コード設定
            string bumonCdFr;
            string bumonCdTo;
            if (Util.ToDecimal(txtBumonCdFr.Text) > 0)
            {
                bumonCdFr = txtBumonCdFr.Text;
            }
            else
            {
                bumonCdFr = "0";
            }
            if (Util.ToDecimal(txtBumonCdTo.Text) > 0)
            {
                bumonCdTo = txtBumonCdTo.Text;
            }
            else
            {
                bumonCdTo = "9999";
            }
            // 部課コード設定
            string bukaCdFr;
            string bukaCdTo;
            if (Util.ToDecimal(txtBukaCdFr.Text) > 0)
            {
                bukaCdFr = txtBukaCdFr.Text;
            }
            else
            {
                bukaCdFr = "0";
            }
            if (Util.ToDecimal(txtBukaCdTo.Text) > 0)
            {
                bukaCdTo = txtBukaCdTo.Text;
            }
            else
            {
                bukaCdTo = "9999";
            }
#endregion
            DbParamCollection dpc = new DbParamCollection();

            // VI_帳票項目設定
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 2, this.UInfo.KaishaCd);
            dpc.SetParam("@KYUYO_SHOYO", SqlDbType.VarChar, 2, this.Par1);
            dpc.SetParam("@CHOHYO_BANGO", SqlDbType.VarChar, 2, this.Par2);
            string strwhr = "KAISHA_CD = @KAISHA_CD AND CHOHYO_BANGO = @CHOHYO_BANGO AND KYUYO_SHOYO = @KYUYO_SHOYO";
            string strOdr = "KAISHA_CD ASC, KYUYO_SHOYO ASC, INJI_ICHI ASC";
            DataTable dtSti = this.Dba.GetDataTableByConditionWithParams("*","VI_KY_CHOHYO_KOMOKU_SETTEI",strwhr, strOdr, dpc);
            if (dtSti.Rows.Count == 0)
            {
                return false;
            }
            
            // 給与明細勤怠データ
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 2, this.UInfo.KaishaCd);
            dpc.SetParam("@NENGETSU_FR", SqlDbType.VarChar, 10, dateFr.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@NENGETSU_TO", SqlDbType.VarChar, 10, dateTo.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@SHAIN_CD_FR", SqlDbType.VarChar, 6, shainCdFr);
            dpc.SetParam("@SHAIN_CD_TO", SqlDbType.VarChar, 6, shainCdTo);
            dpc.SetParam("@BUMON_CD_FR", SqlDbType.VarChar, 4, bumonCdFr);
            dpc.SetParam("@BUMON_CD_TO", SqlDbType.VarChar, 4, bumonCdTo);
            dpc.SetParam("@BUKA_CD_FR", SqlDbType.VarChar, 4, bukaCdFr);
            dpc.SetParam("@BUKA_CD_TO", SqlDbType.VarChar, 4, bukaCdTo);

            // 出力ワークの明細データ作成方法を判定
            DataTable dtMainLoop = new DataTable();
            if (rdoHoho1.Checked)
            {
                // 社員別
                dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(GetSyainbetsuSQL(), dpc);
            }
            else
            {
                if (rdoSex1.Checked)
                {
                    // 部課男女別
                    dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(GetBukaSeibetsuSQL(), dpc);
                }
                else
                {
                    // 部課別
                    dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(GetBukabetsuSQL(), dpc);
                }
            }
            if (dtMainLoop.Rows.Count == 0)
            {
                return false;
            }
            else
            {
                ArrayList alParamsPrKyTbl;      // ワークテーブルINSERT用パラメータ
                int sort = 0;                   // ソートフィールドカウンタ

                foreach (DataRow dr in dtMainLoop.Rows)
                {
                    sort++;     // カウントアップ

                    // データ登録
                    alParamsPrKyTbl = SetPrKyTblParams(sort, jpDateFr, jpDateTo, dtSti, dr);
                    this.Dba.Insert("PR_KY_TBL", (DbParamCollection)alParamsPrKyTbl[0]);
                }

                // 小計レコードの追加
                DataTable dt = new DataTable();
                // 出力方法：社員別出力のとき
                if (rdoHoho1.Checked)
                {
#region 社員別出力時
                    // 並び順：部課順の時
                    if (rdoOrder2.Checked)          
                    {
                        /// 男女別出力が指定された時
                        if (rdoSex1.Checked)
                        {   
                            // 出力方法：部課別出力のとき
                            if (rdoHoho2.Checked)
                            {
                                // 部課別男女合計レコード
                                dt = this.Dba.GetDataTableFromSqlWithParams(GetBukaGenderSubtotalSQL(), dpc);
                                foreach (DataRow dr in dt.Rows)
                                {
                                    sort++;
                                    alParamsPrKyTbl = SetPrKyTblParams(sort, jpDateFr, jpDateTo, dtSti, dr);
                                    this.Dba.Insert("PR_KY_TBL", (DbParamCollection)alParamsPrKyTbl[0]);
                                }
                            }
                            // 並び順：部門順の時
                            if (rdoOrder2.Checked)
                            {
                                // 部門別男女合計レコード
                                dt = this.Dba.GetDataTableFromSqlWithParams(GetBumonGenderSubtotalSQL(), dpc);
                                foreach (DataRow dr in dt.Rows)
                                {
                                    sort++;
                                    alParamsPrKyTbl = SetPrKyTblParams(sort, jpDateFr, jpDateTo, dtSti, dr);
                                    this.Dba.Insert("PR_KY_TBL", (DbParamCollection)alParamsPrKyTbl[0]);
                                }
                            }
                        }
                        // 部課別レコード
                        dt = this.Dba.GetDataTableFromSqlWithParams(GetBukaSubtotalSQL(), dpc);
                        foreach (DataRow dr in dt.Rows)
                        {
                            sort++;
                            alParamsPrKyTbl = SetPrKyTblParams(sort, jpDateFr, jpDateTo, dtSti, dr);
                            this.Dba.Insert("PR_KY_TBL", (DbParamCollection)alParamsPrKyTbl[0]);
                            string ninzu = Util.ToString(dr[8]);
                        }
                        // 部門別レコード
                        dt = this.Dba.GetDataTableFromSqlWithParams(GetBumonSubtotalSQL(), dpc);
                        foreach (DataRow dr in dt.Rows)
                        {
                            sort++;
                            alParamsPrKyTbl = SetPrKyTblParams(sort, jpDateFr, jpDateTo, dtSti, dr);
                            this.Dba.Insert("PR_KY_TBL", (DbParamCollection)alParamsPrKyTbl[0]);
                        }
                    }
#endregion
                }
                // 出力方法：部課別の時
                else
                {
#region 部課別出力時
                    /// 男女別出力が指定された時
                    if (rdoSex1.Checked)
                    {
                        // 出力方法：部課別出力のとき
                        if (rdoHoho2.Checked)
                        {
                            // 部課合計レコードの追加
                            dt = this.Dba.GetDataTableFromSqlWithParams(GetBukaSubtotalSQL(), dpc);
                            foreach (DataRow dr in dt.Rows)
                            {
                                sort++;
                                alParamsPrKyTbl = SetPrKyTblParams(sort, jpDateFr, jpDateTo, dtSti, dr);
                                this.Dba.Insert("PR_KY_TBL", (DbParamCollection)alParamsPrKyTbl[0]);
                            }
                        }
                        // 並び順：部門順の時
                        if (rdoOrder2.Checked)
                        {
                            // 部門男女別合計レコードの追加
                            dt = this.Dba.GetDataTableFromSqlWithParams(GetBumonGenderSubtotalSQL(), dpc);
                            foreach (DataRow dr in dt.Rows)
                            {
                                sort++;
                                alParamsPrKyTbl = SetPrKyTblParams(sort, jpDateFr, jpDateTo, dtSti, dr);
                                this.Dba.Insert("PR_KY_TBL", (DbParamCollection)alParamsPrKyTbl[0]);
                            }
                        }
                    }
                    // 部門合計レコードの追加
                    dt = this.Dba.GetDataTableFromSqlWithParams(GetBumonSubtotalSQL(), dpc);
                    foreach (DataRow dr in dt.Rows)
                    {
                        sort++;
                        alParamsPrKyTbl = SetPrKyTblParams(sort, jpDateFr, jpDateTo, dtSti, dr);
                        this.Dba.Insert("PR_KY_TBL", (DbParamCollection)alParamsPrKyTbl[0]);
                    }
#endregion
                }
                // 合計レコードの追加
                dt = this.Dba.GetDataTableFromSqlWithParams(GetTotalSQL(), dpc);
                foreach (DataRow dr in dt.Rows)
                {
                    sort++;
                    alParamsPrKyTbl = SetPrKyTblParams(sort, jpDateFr, jpDateTo, dtSti, dr);
                    this.Dba.Insert("PR_KY_TBL", (DbParamCollection)alParamsPrKyTbl[0]);
                }
                // 男女合計レコードの追加
                if (rdoSex1.Checked)
                {
                    dt = this.Dba.GetDataTableFromSqlWithParams(GetGenderSubtotalSQL(), dpc);
                    foreach (DataRow dr in dt.Rows)
                    {
                        sort++;
                        alParamsPrKyTbl = SetPrKyTblParams(sort, jpDateFr, jpDateTo, dtSti, dr);
                        this.Dba.Insert("PR_KY_TBL", (DbParamCollection)alParamsPrKyTbl[0]);
                    }
                }
            }

            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
                "SORT",
                "PR_KY_TBL",
                "GUID = @GUID",
                dpc);
            bool dataFlag;
            if (tmpdtPR_HN_TBL.Rows.Count > 0)
            {
                dataFlag = true;
            }
            else
            {
                dataFlag = false;
            }

            return dataFlag;
        }

        private bool MakeWkData00()
        {
            #region
            // 集計期間を西暦にして保持
            DateTime dateFr = Util.ConvAdDate(this.lblGengoNengetsuFr.Text, this.txtGengoYearNengetsuFr.Text,
                    this.txtMonthNengetsuFr.Text, "1", this.Dba);
            DateTime dateTo = Util.ConvAdDate(this.lblGengoNengetsuTo.Text, this.txtGengoYearNengetsuTo.Text,
                    this.txtMonthNengetsuTo.Text, "1", this.Dba);
            // 集計期間を和暦で保持
            string[] jpDateFr = Util.ConvJpDate(dateFr, this.Dba);
            string[] jpDateTo = Util.ConvJpDate(dateTo, this.Dba);
            // 社員コード設定
            string shainCdFr;
            string shainCdTo;
            if (Util.ToDecimal(txtShainCdFr.Text) > 0)
            {
                shainCdFr = txtShainCdFr.Text;
            }
            else
            {
                shainCdFr = "0";
            }
            if (Util.ToDecimal(txtShainCdTo.Text) > 0)
            {
                shainCdTo = txtShainCdTo.Text;
            }
            else
            {
                shainCdTo = "999999";
            }
            // 部門コード設定
            string bumonCdFr;
            string bumonCdTo;
            if (Util.ToDecimal(txtBumonCdFr.Text) > 0)
            {
                bumonCdFr = txtBumonCdFr.Text;
            }
            else
            {
                bumonCdFr = "0";
            }
            if (Util.ToDecimal(txtBumonCdTo.Text) > 0)
            {
                bumonCdTo = txtBumonCdTo.Text;
            }
            else
            {
                bumonCdTo = "9999";
            }
            // 部課コード設定
            string bukaCdFr;
            string bukaCdTo;
            if (Util.ToDecimal(txtBukaCdFr.Text) > 0)
            {
                bukaCdFr = txtBukaCdFr.Text;
            }
            else
            {
                bukaCdFr = "0";
            }
            if (Util.ToDecimal(txtBukaCdTo.Text) > 0)
            {
                bukaCdTo = txtBukaCdTo.Text;
            }
            else
            {
                bukaCdTo = "9999";
            }
            // 出力順 0:社員順、1:部門順
            int order = (this.rdoOrder1.Checked == true) ? 0 : 1;
            #endregion
            DbParamCollection dpc = new DbParamCollection();

            #region 帳票項目取得
            // VI_帳票項目設定
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 2, this.UInfo.KaishaCd);
            dpc.SetParam("@KYUYO_SHOYO", SqlDbType.VarChar, 2, this.Par1);          //給与賞与区分
            dpc.SetParam("@CHOHYO_BANGO", SqlDbType.VarChar, 2, this.Par2);         //帳票番号
            string strwhr = "KAISHA_CD = @KAISHA_CD AND CHOHYO_BANGO = @CHOHYO_BANGO AND KYUYO_SHOYO = @KYUYO_SHOYO";
            string strOdr = "KAISHA_CD ASC, KYUYO_SHOYO ASC, INJI_ICHI ASC";
            //DataTable dtSti = this.Dba.GetDataTableByConditionWithParams("*", "VI_KY_CHOHYO_KOMOKU_SETTEI", strwhr, strOdr, dpc);
            //if (dtSti.Rows.Count == 0)
            dtRptSti = this.Dba.GetDataTableByConditionWithParams("*", "VI_KY_CHOHYO_KOMOKU_SETTEI", strwhr, strOdr, dpc);
            if (dtRptSti.Rows.Count == 0)
            {
                Msg.Info("帳票項目設定がされていません。\n帳票項目設定を行ってください。");
                return false;
            }
            #endregion

            #region 集計データ取得
            // 給与明細勤怠データ
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 2, this.UInfo.KaishaCd);
            dpc.SetParam("@NENGETSU_FR", SqlDbType.VarChar, 10, dateFr.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@NENGETSU_TO", SqlDbType.VarChar, 10, dateTo.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@SHAIN_CD_FR", SqlDbType.VarChar, 6, shainCdFr);
            dpc.SetParam("@SHAIN_CD_TO", SqlDbType.VarChar, 6, shainCdTo);
            dpc.SetParam("@BUMON_CD_FR", SqlDbType.VarChar, 4, bumonCdFr);
            dpc.SetParam("@BUMON_CD_TO", SqlDbType.VarChar, 4, bumonCdTo);
            dpc.SetParam("@BUKA_CD_FR", SqlDbType.VarChar, 4, bukaCdFr);
            dpc.SetParam("@BUKA_CD_TO", SqlDbType.VarChar, 4, bukaCdTo);

            // 出力ワークの明細データ作成方法を判定
            DataTable dtMainLoop = new DataTable();
            if (rdoHoho1.Checked)
            {
                // 社員別
                dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(GetSyainbetsuSQLA(), dpc);
            }
            else
            {
                if (rdoSex1.Checked)
                {
                    // 部課男女別
                    dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(GetBukaSeibetsuSQLA(), dpc);
                }
                else
                {
                    // 部課別
                    dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(GetBukabetsuSQLA(), dpc);
                }
            }
            #endregion
            if (dtMainLoop.Rows.Count == 0)
            {
                return false;
            }
            else
            {
                ArrayList alParamsPrKyTbl;      // ワークテーブルINSERT用パラメータ
                int sort = 0;                   // ソートフィールドカウンタ

                foreach (DataRow dr in dtMainLoop.Rows)
                {
                    sort++;     // カウントアップ

                    // データ登録
                    alParamsPrKyTbl = SetPrKyTblParamsA(sort, jpDateFr, jpDateTo, dtRptSti, dr);
                    this.Dba.Insert("PR_KY_TBL", (DbParamCollection)alParamsPrKyTbl[0]);
                }
                // 小計レコードの追加
                DataTable dt = new DataTable();
                // 出力方法：社員別出力のとき
                if (rdoHoho1.Checked)
                {
                    #region 社員別出力時
                    // 並び順：部課順の時
                    if (rdoOrder2.Checked)
                    {
                        /// 男女別出力が指定された時
                        if (rdoSex1.Checked)
                        {
                            // 部課別男女合計レコード
                            dt = this.Dba.GetDataTableFromSqlWithParams(GetBukaGenderSubtotalSQLA(), dpc);
                            foreach (DataRow dr in dt.Rows)
                            {
                                sort++;
                                alParamsPrKyTbl = SetPrKyTblParamsA(sort, jpDateFr, jpDateTo, dtRptSti, dr);
                                this.Dba.Insert("PR_KY_TBL", (DbParamCollection)alParamsPrKyTbl[0]);
                            }
                            // 部門別男女合計レコード
                            dt = this.Dba.GetDataTableFromSqlWithParams(GetBumonGenderSubtotalSQLA(), dpc);
                            foreach (DataRow dr in dt.Rows)
                            {
                                sort++;
                                alParamsPrKyTbl = SetPrKyTblParamsA(sort, jpDateFr, jpDateTo, dtRptSti, dr);
                                this.Dba.Insert("PR_KY_TBL", (DbParamCollection)alParamsPrKyTbl[0]);
                            }
                            //// 出力方法：部課別出力のとき
                            //if (rdoHoho2.Checked)
                            //{
                            //    // 部課別男女合計レコード
                            //    dt = this.Dba.GetDataTableFromSqlWithParams(GetBukaGenderSubtotalSQLA(), dpc);
                            //    foreach (DataRow dr in dt.Rows)
                            //    {
                            //        sort++;
                            //        alParamsPrKyTbl = SetPrKyTblParamsA(sort, jpDateFr, jpDateTo, dtRptSti, dr);
                            //        this.Dba.Insert("PR_KY_TBL", (DbParamCollection)alParamsPrKyTbl[0]);
                            //    }
                            //}
                            //// 並び順：部門順の時
                            //if (rdoOrder2.Checked)
                            //{
                            //    // 部門別男女合計レコード
                            //    dt = this.Dba.GetDataTableFromSqlWithParams(GetBumonGenderSubtotalSQLA(), dpc);
                            //    foreach (DataRow dr in dt.Rows)
                            //    {
                            //        sort++;
                            //        alParamsPrKyTbl = SetPrKyTblParamsA(sort, jpDateFr, jpDateTo, dtRptSti, dr);
                            //        this.Dba.Insert("PR_KY_TBL", (DbParamCollection)alParamsPrKyTbl[0]);
                            //    }
                            //}
                        }
                        // 部課別レコード
                        dt = this.Dba.GetDataTableFromSqlWithParams(GetBukaSubtotalSQLA(), dpc);
                        foreach (DataRow dr in dt.Rows)
                        {
                            sort++;
                            alParamsPrKyTbl = SetPrKyTblParamsA(sort, jpDateFr, jpDateTo, dtRptSti, dr);
                            this.Dba.Insert("PR_KY_TBL", (DbParamCollection)alParamsPrKyTbl[0]);
                            string ninzu = Util.ToString(dr[8]);
                        }
                        // 部門別レコード
                        dt = this.Dba.GetDataTableFromSqlWithParams(GetBumonSubtotalSQLA(), dpc);
                        foreach (DataRow dr in dt.Rows)
                        {
                            sort++;
                            alParamsPrKyTbl = SetPrKyTblParamsA(sort, jpDateFr, jpDateTo, dtRptSti, dr);
                            this.Dba.Insert("PR_KY_TBL", (DbParamCollection)alParamsPrKyTbl[0]);
                        }
                    }
                    #endregion
                }
                // 出力方法：部課別の時
                else
                {
                    #region 部課別出力時
                    /// 男女別出力が指定された時
                    if (rdoSex1.Checked)
                        {
                        // 出力方法：部課別出力のとき
                        if (rdoHoho2.Checked)
                        {
                            // 部課合計レコードの追加
                            dt = this.Dba.GetDataTableFromSqlWithParams(GetBukaSubtotalSQLA(), dpc);
                            foreach (DataRow dr in dt.Rows)
                                {
                                sort++;
                                alParamsPrKyTbl = SetPrKyTblParamsA(sort, jpDateFr, jpDateTo, dtRptSti, dr);
                                this.Dba.Insert("PR_KY_TBL", (DbParamCollection)alParamsPrKyTbl[0]);
                                }
                        }
                        // 並び順：部門順の時
                        if (rdoOrder2.Checked)
                        {
                            // 部門男女別合計レコードの追加
                            dt = this.Dba.GetDataTableFromSqlWithParams(GetBumonGenderSubtotalSQLA(), dpc);
                            foreach (DataRow dr in dt.Rows)
                            {
                                sort++;
                                alParamsPrKyTbl = SetPrKyTblParamsA(sort, jpDateFr, jpDateTo, dtRptSti, dr);
                                this.Dba.Insert("PR_KY_TBL", (DbParamCollection)alParamsPrKyTbl[0]);
                            }
                        }
                    }
                    // 部門合計レコードの追加
                    dt = this.Dba.GetDataTableFromSqlWithParams(GetBumonSubtotalSQLA(), dpc);
                    foreach (DataRow dr in dt.Rows)
                        {
                        sort++;
                        alParamsPrKyTbl = SetPrKyTblParamsA(sort, jpDateFr, jpDateTo, dtRptSti, dr);
                        this.Dba.Insert("PR_KY_TBL", (DbParamCollection)alParamsPrKyTbl[0]);
                        }
                    #endregion
                }
                // 男女合計レコードの追加
                if (rdoSex1.Checked)
                {
                    dt = this.Dba.GetDataTableFromSqlWithParams(GetGenderSubtotalSQLA(), dpc);
                    foreach (DataRow dr in dt.Rows)
                    {
                        sort++;
                        alParamsPrKyTbl = SetPrKyTblParamsA(sort, jpDateFr, jpDateTo, dtRptSti, dr);
                        this.Dba.Insert("PR_KY_TBL", (DbParamCollection)alParamsPrKyTbl[0]);
                    }
                }
                // 合計レコードの追加
                dt = this.Dba.GetDataTableFromSqlWithParams(GetTotalSQLA(), dpc);
                foreach (DataRow dr in dt.Rows)
                {
                    sort++;
                    alParamsPrKyTbl = SetPrKyTblParamsA(sort, jpDateFr, jpDateTo, dtRptSti, dr);
                    this.Dba.Insert("PR_KY_TBL", (DbParamCollection)alParamsPrKyTbl[0]);
                }
            }

            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
                "SORT",
                "PR_KY_TBL",
                "GUID = @GUID",
                dpc);
            bool dataFlag;
            if (tmpdtPR_HN_TBL.Rows.Count > 0)
            {
                dataFlag = true;
            }
            else
            {
                dataFlag = false;
            }

            return dataFlag;
        }

        /// <summary>
        /// 社員別レコード取得SELECT文
        /// </summary>
        private string GetSyainbetsuSQL()
        {
            StringBuilder sql = new StringBuilder();
            // SELECT句
            sql.Append("SELECT K1.KAISHA_CD AS KAISHA_CD");
            sql.Append(", K1.SHAIN_CD AS SHAIN_CD");
            sql.Append(", M1.BUMON_CD AS BUMON_CD");
            sql.Append(", M1.BUKA_CD AS BUKA_CD");
            sql.Append(", MAX(M1.SEIBETSU) AS SEIBETSU");
            sql.Append(", MAX(M1.SHAIN_CD) AS DISP_CD");                    // 出力用コード
            sql.Append(", MAX(M1.SHAIN_NM) AS DISP_NM");                    // 出力用名称 
            sql.Append(", ' ' AS SEX");                                     // 出力用性別
            sql.Append(", ' ' AS TOTAL");                                   // 出力用人数
            // SELECT句続き～WHERE句(共通)
            sql.Append(GetSQLWithoutGroupingPart());
            // GROUP句
            sql.Append(" GROUP BY K1.KAISHA_CD, K1.SHAIN_CD, M1.BUMON_CD, M1.BUKA_CD");
            // ORDER句
            sql.Append(" ORDER BY KAISHA_CD ASC,SHAIN_CD ASC");

            return Util.ToString(sql);
        }
        /// <summary>
        /// 社員別レコード取得SELECT文
        /// </summary>
        private string GetSyainbetsuSQLA()
        {
            StringBuilder sql = new StringBuilder();
            // SELECT句
            sql.Append("SELECT K1.KAISHA_CD AS KAISHA_CD");
            sql.Append(", K1.SHAIN_CD AS SHAIN_CD");
            sql.Append(", M1.BUMON_CD AS BUMON_CD");
            sql.Append(", M1.BUKA_CD AS BUKA_CD");
            sql.Append(", MAX(M1.SEIBETSU) AS SEIBETSU");
            sql.Append(", MAX(M1.SHAIN_CD) AS DISP_CD");                    // 出力用コード
            sql.Append(", MAX(M1.SHAIN_NM) AS DISP_NM");                    // 出力用名称 
            sql.Append(", ' ' AS SEX");                                     // 出力用性別
            sql.Append(", ' ' AS TOTAL");                                   // 出力用人数
            // SELECT句続き～WHERE句(共通)
            sql.Append(GetSQLColsPart());
            sql.Append(GetSQLFromWherePart());
            // GROUP句
            sql.Append(" GROUP BY K1.KAISHA_CD, K1.SHAIN_CD, M1.BUMON_CD, M1.BUKA_CD");
            // ORDER句
            sql.Append(" ORDER BY KAISHA_CD ASC,");
            if (this.rdoOrder1.Checked)
            {
                sql.Append(" K1.SHAIN_CD ASC");
            }
            else
            {
                sql.Append(" M1.BUMON_CD ASC, M1.BUKA_CD ASC, K1.SHAIN_CD ASC");
            }
            return Util.ToString(sql);
        }

        /// <summary>
        /// 部課別レコード取得SELECT文
        /// </summary>
        private string GetBukabetsuSQL()
        {
            StringBuilder sql = new StringBuilder();
            // SELECT句
            sql.Append("SELECT K1.KAISHA_CD  AS KAISHA_CD");
            sql.Append(", 1000000 AS SHAIN_CD");                                        // 仮社員コード
            sql.Append(", M1.BUMON_CD AS BUMON_CD");
            sql.Append(", M1.BUKA_CD AS BUKA_CD");
            sql.Append(", 9 AS SEIBETSU");                                              // 仮性別
            sql.Append(", MAX(M1.BUKA_CD) AS DISP_CD");                                 // 出力用コード
            sql.Append(", MAX(M1.BUKA_NM) AS DISP_NM");                                 // 出力用名称
            sql.Append(", ' ' AS SEX");                                                 // 出力用性別
            sql.Append(", STR(COUNT(DISTINCT M1.SHAIN_CD)) + '人' AS TOTAL");            // 出力用人数
            // SELECT句続き～WHERE句(共通)
            sql.Append(GetSQLWithoutGroupingPart());
            // GROUP句
            sql.Append(" GROUP BY K1.KAISHA_CD, M1.BUMON_CD, M1.BUKA_CD");
            // ORDER句
            sql.Append(" ORDER BY KAISHA_CD ASC,BUMON_CD ASC,BUKA_CD ASC");

            return Util.ToString(sql);
        }
        /// <summary>
        /// 部課別レコード取得SELECT文
        /// </summary>
        private string GetBukabetsuSQLA()
        {
            StringBuilder sql = new StringBuilder();
            // SELECT句
            sql.Append("SELECT K1.KAISHA_CD  AS KAISHA_CD");
            sql.Append(", 1000000 AS SHAIN_CD");                                        // 仮社員コード
            sql.Append(", M1.BUMON_CD AS BUMON_CD");
            sql.Append(", M1.BUKA_CD AS BUKA_CD");
            sql.Append(", 9 AS SEIBETSU");                                              // 仮性別
            sql.Append(", MAX(M1.BUKA_CD) AS DISP_CD");                                 // 出力用コード
            sql.Append(", MAX(M1.BUKA_NM) AS DISP_NM");                                 // 出力用名称
            sql.Append(", ' ' AS SEX");                                                 // 出力用性別
            sql.Append(", STR(COUNT(DISTINCT M1.SHAIN_CD)) + '人' AS TOTAL");            // 出力用人数
            // SELECT句続き～WHERE句(共通)
            sql.Append(GetSQLColsPart());
            sql.Append(GetSQLFromWherePart());
            // GROUP句
            sql.Append(" GROUP BY K1.KAISHA_CD, M1.BUMON_CD, M1.BUKA_CD");
            // ORDER句
            sql.Append(" ORDER BY KAISHA_CD ASC,BUMON_CD ASC,BUKA_CD ASC");

            return Util.ToString(sql);
        }

        /// <summary>
        /// 部課男女別レコード取得SELECT文
        /// </summary>
        private string GetBukaSeibetsuSQL()
        {
            StringBuilder sql = new StringBuilder();
            // SELECT句
            sql.Append("SELECT K1.KAISHA_CD  AS KAISHA_CD");
            sql.Append(", 1000000 AS SHAIN_CD");                                        // 仮社員コード
            sql.Append(", M1.BUMON_CD AS BUMON_CD");
            sql.Append(", M1.BUKA_CD AS BUKA_CD");
            sql.Append(", M1.SEIBETSU AS SEIBETSU");
            sql.Append(", MAX(M1.BUKA_CD) AS DISP_CD");                                 // 出力用コード
            sql.Append(", MAX(M1.BUKA_NM) AS DISP_NM");                                 // 出力用名称
            sql.Append(", CASE WHEN M1.SEIBETSU=1 THEN '男' ELSE '女' END AS SEX");      // 出力用性別
            sql.Append(", STR(COUNT(DISTINCT M1.SHAIN_CD)) + '人' AS TOTAL");            // 出力用人数
            // SELECT句続き～WHERE句(共通)
            sql.Append(GetSQLWithoutGroupingPart());
            // GROUP句
            sql.Append(" GROUP BY K1.KAISHA_CD, M1.BUMON_CD, M1.BUKA_CD, M1.SEIBETSU");
            // ORDER句
            sql.Append(" ORDER BY KAISHA_CD ASC,BUMON_CD ASC,BUKA_CD ASC, SEIBETSU ASC");

            return Util.ToString(sql);
        }
        /// <summary>
        /// 部課男女別レコード取得SELECT文
        /// </summary>
        private string GetBukaSeibetsuSQLA()
        {
            StringBuilder sql = new StringBuilder();
            // SELECT句
            sql.Append("SELECT K1.KAISHA_CD  AS KAISHA_CD");
            sql.Append(", 1000000 AS SHAIN_CD");                                        // 仮社員コード
            sql.Append(", M1.BUMON_CD AS BUMON_CD");
            sql.Append(", M1.BUKA_CD AS BUKA_CD");
            sql.Append(", M1.SEIBETSU AS SEIBETSU");
            sql.Append(", MAX(M1.BUKA_CD) AS DISP_CD");                                 // 出力用コード
            sql.Append(", MAX(M1.BUKA_NM) AS DISP_NM");                                 // 出力用名称
            sql.Append(", CASE WHEN M1.SEIBETSU=1 THEN '男' ELSE '女' END AS SEX");      // 出力用性別
            sql.Append(", STR(COUNT(DISTINCT M1.SHAIN_CD)) + '人' AS TOTAL");            // 出力用人数
            // SELECT句続き～WHERE句(共通)
            sql.Append(GetSQLColsPart());
            sql.Append(GetSQLFromWherePart());
            // GROUP句
            sql.Append(" GROUP BY K1.KAISHA_CD, M1.BUMON_CD, M1.BUKA_CD, M1.SEIBETSU");
            // ORDER句
            sql.Append(" ORDER BY KAISHA_CD ASC,BUMON_CD ASC,BUKA_CD ASC, SEIBETSU ASC");

            return Util.ToString(sql);
        }

        /// <summary>
        /// 部課男女別小計レコード取得SELECT文
        /// </summary>
        private string GetBukaGenderSubtotalSQL()
        {
            StringBuilder sql = new StringBuilder();
            // SELECT句
            sql.Append("SELECT K1.KAISHA_CD  AS KAISHA_CD");
            sql.Append(", 1000000 AS SHAIN_CD");                                        // 仮社員コード
            sql.Append(", M1.BUMON_CD AS BUMON_CD");
            sql.Append(", M1.BUKA_CD AS BUKA_CD");
            sql.Append(", M1.SEIBETSU AS SEIBETSU");
            sql.Append(", ' ' AS DISP_CD");                                             // 出力用コード
            sql.Append(", CASE WHEN M1.SEIBETSU=1 THEN '【男子計】' ELSE '【女子計】' END AS DISP_NM");// 出力用名称
            sql.Append(", ' ' AS SEX");                                                 // 出力用性別
            sql.Append(", STR(COUNT(DISTINCT M1.SHAIN_CD)) + '人' AS TOTAL");            // 出力用人数
            // SELECT句続き～WHERE句(共通)
            sql.Append(GetSQLWithoutGroupingPart());
            // GROUP句
            sql.Append(" GROUP BY K1.KAISHA_CD, M1.BUMON_CD, M1.BUKA_CD, M1.SEIBETSU");
            // ORDER句
            sql.Append(" ORDER BY KAISHA_CD ASC,BUMON_CD ASC,BUKA_CD ASC,SEIBETSU ASC");

            return Util.ToString(sql);
        }
        /// <summary>
        /// 部課男女別小計レコード取得SELECT文
        /// </summary>
        private string GetBukaGenderSubtotalSQLA()
        {
            StringBuilder sql = new StringBuilder();
            // SELECT句
            sql.Append("SELECT K1.KAISHA_CD  AS KAISHA_CD");
            sql.Append(", 1000004 AS SHAIN_CD");                                        // 仮社員コード
            sql.Append(", M1.BUMON_CD AS BUMON_CD");
            sql.Append(", M1.BUKA_CD AS BUKA_CD");
            sql.Append(", M1.SEIBETSU AS SEIBETSU");
            sql.Append(", ' ' AS DISP_CD");                                             // 出力用コード
            sql.Append(", CASE WHEN M1.SEIBETSU=1 THEN '【男子計】' ELSE '【女子計】' END AS DISP_NM");// 出力用名称
            sql.Append(", ' ' AS SEX");                                                 // 出力用性別
            sql.Append(", STR(COUNT(DISTINCT M1.SHAIN_CD)) + '人' AS TOTAL");            // 出力用人数
            // SELECT句続き～WHERE句(共通)
            sql.Append(GetSQLColsPart());
            sql.Append(GetSQLFromWherePart());
            // GROUP句
            sql.Append(" GROUP BY K1.KAISHA_CD, M1.BUMON_CD, M1.BUKA_CD, M1.SEIBETSU");
            // ORDER句
            sql.Append(" ORDER BY KAISHA_CD ASC, M1.BUMON_CD ASC, M1.BUKA_CD ASC");

            return Util.ToString(sql);
        }

        /// <summary>
        /// 部課別小計レコード取得SELECT文
        /// </summary>
        private string GetBukaSubtotalSQL()
        {
            StringBuilder sql = new StringBuilder();
            // SELECT句
            sql.Append("SELECT K1.KAISHA_CD  AS KAISHA_CD");
            sql.Append(", 1000000 AS SHAIN_CD");                                        // 仮社員コード
            sql.Append(", M1.BUMON_CD AS BUMON_CD");
            sql.Append(", M1.BUKA_CD AS BUKA_CD");
            sql.Append(", 9 AS SEIBETSU");                                              // 仮性別コード
            sql.Append(", MAX(M1.BUMON_CD) AS DISP_CD");                                // 出力用コード
            sql.Append(", '【' + MAX(ISNULL(M1.BUKA_NM, '部課計')) + '】' AS DISP_NM");                   // 出力用名称
            sql.Append(", ' ' AS SEX");                                                 // 出力用性別
            sql.Append(", STR(COUNT(DISTINCT M1.SHAIN_CD)) + '人' AS TOTAL");            // 出力用人数
            // SELECT句続き～WHERE句(共通)
            sql.Append(GetSQLWithoutGroupingPart());
            // GROUP句
            sql.Append(" GROUP BY K1.KAISHA_CD, M1.BUMON_CD, M1.BUKA_CD");
            // ORDER句
            sql.Append(" ORDER BY KAISHA_CD ASC,BUMON_CD ASC,BUKA_CD ASC");

            return Util.ToString(sql);
        }
        /// <summary>
        /// 部課別小計レコード取得SELECT文
        /// </summary>
        private string GetBukaSubtotalSQLA()
        {
            StringBuilder sql = new StringBuilder();
            // SELECT句
            sql.Append("SELECT K1.KAISHA_CD  AS KAISHA_CD");
            sql.Append(", 1000006 AS SHAIN_CD");                                        // 仮社員コード
            sql.Append(", M1.BUMON_CD AS BUMON_CD");
            sql.Append(", M1.BUKA_CD AS BUKA_CD");
            sql.Append(", 9 AS SEIBETSU");                                              // 仮性別コード
            sql.Append(", MAX(M1.BUMON_CD) AS DISP_CD");                                // 出力用コード
            sql.Append(", '【' + MAX(ISNULL(M1.BUKA_NM, '部課計')) + '】' AS DISP_NM");                   // 出力用名称
            sql.Append(", ' ' AS SEX");                                                 // 出力用性別
            sql.Append(", STR(COUNT(DISTINCT M1.SHAIN_CD)) + '人' AS TOTAL");            // 出力用人数
            // SELECT句続き～WHERE句(共通)
            sql.Append(GetSQLColsPart());
            sql.Append(GetSQLFromWherePart());
            // GROUP句
            sql.Append(" GROUP BY K1.KAISHA_CD, M1.BUMON_CD, M1.BUKA_CD");
            // ORDER句
            sql.Append(" ORDER BY KAISHA_CD ASC,BUMON_CD ASC,BUKA_CD ASC");

            return Util.ToString(sql);
        }

        /// <summary>
        /// 部門男女別小計レコード取得SELECT文
        /// </summary>
        private string GetBumonGenderSubtotalSQL()
        {
            StringBuilder sql = new StringBuilder();
            // SELECT句
            sql.Append("SELECT K1.KAISHA_CD  AS KAISHA_CD");
            sql.Append(", 1000000 AS SHAIN_CD");                                        // 仮社員コード
            sql.Append(", M1.BUMON_CD AS BUMON_CD");
            sql.Append(", 10000 AS BUKA_CD");                                           // 仮部課コード
            sql.Append(", M1.SEIBETSU AS SEIBETSU");
            sql.Append(", ' ' AS DISP_CD");                                             // 出力用コード
            sql.Append(", CASE WHEN M1.SEIBETSU=1 THEN '【男子計】' ELSE '【女子計】' END AS DISP_NM");// 出力用名称
            sql.Append(", ' ' AS SEX");                                                 // 出力用性別
            sql.Append(", STR(COUNT(DISTINCT M1.SHAIN_CD)) + '人' AS TOTAL");            // 出力用人数
            // SELECT句続き～WHERE句(共通)
            sql.Append(GetSQLWithoutGroupingPart());
            // GROUP句
            sql.Append(" GROUP BY K1.KAISHA_CD, M1.BUMON_CD, M1.SEIBETSU");
            // ORDER句
            sql.Append(" ORDER BY KAISHA_CD ASC,BUMON_CD ASC, SEIBETSU ASC");

            return Util.ToString(sql);
        }
        /// <summary>
        /// 部門男女別小計レコード取得SELECT文
        /// </summary>
        private string GetBumonGenderSubtotalSQLA()
        {
            StringBuilder sql = new StringBuilder();
            // SELECT句
            sql.Append("SELECT K1.KAISHA_CD  AS KAISHA_CD");
            sql.Append(", 1000006 AS SHAIN_CD");                                        // 仮社員コード
            sql.Append(", M1.BUMON_CD AS BUMON_CD");
            sql.Append(", 10000 AS BUKA_CD");                                           // 仮部課コード
            sql.Append(", M1.SEIBETSU AS SEIBETSU");
            sql.Append(", ' ' AS DISP_CD");                                             // 出力用コード
            sql.Append(", CASE WHEN M1.SEIBETSU=1 THEN '【男子計】' ELSE '【女子計】' END AS DISP_NM");// 出力用名称
            sql.Append(", ' ' AS SEX");                                                 // 出力用性別
            sql.Append(", STR(COUNT(DISTINCT M1.SHAIN_CD)) + '人' AS TOTAL");            // 出力用人数
            // SELECT句続き～WHERE句(共通)
            sql.Append(GetSQLColsPart());
            sql.Append(GetSQLFromWherePart());
            // GROUP句
            sql.Append(" GROUP BY K1.KAISHA_CD, M1.BUMON_CD, M1.SEIBETSU");
            // ORDER句
            sql.Append(" ORDER BY KAISHA_CD ASC,BUMON_CD ASC, SEIBETSU ASC");

            return Util.ToString(sql);
        }

        /// <summary>
        /// 部門別小計レコード取得SELECT文
        /// </summary>
        private string GetBumonSubtotalSQL()
        {
            StringBuilder sql = new StringBuilder();
            // SELECT句
            sql.Append("SELECT K1.KAISHA_CD  AS KAISHA_CD");
            sql.Append(", 1000000 AS SHAIN_CD");                                            // 仮社員コード
            sql.Append(", M1.BUMON_CD AS BUMON_CD");
            sql.Append(", 10000 AS BUKA_CD");                                               // 仮部課コード
            sql.Append(", 9 AS SEIBETSU");                                                  // 仮性別コード
            sql.Append(", MAX(M1.BUMON_CD) AS DISP_CD");                                    // 出力用コード
            sql.Append(", '【' + MAX(M1.BUMON_NM) + '】' AS DISP_NM");                      // 出力用名称
            sql.Append(", ' ' AS SEX");                                                     // 出力用性別
            sql.Append(", STR(COUNT(DISTINCT M1.SHAIN_CD)) + '人' AS TOTAL");                // 出力用人数
            // SELECT句続き～WHERE句(共通)
            sql.Append(GetSQLWithoutGroupingPart());
            // GROUP句
            sql.Append(" GROUP BY K1.KAISHA_CD, M1.BUMON_CD");
            // ORDER句
            sql.Append(" ORDER BY KAISHA_CD ASC,BUMON_CD ASC");

            return Util.ToString(sql);
        }
        /// <summary>
        /// 部門別小計レコード取得SELECT文
        /// </summary>
        private string GetBumonSubtotalSQLA()
        {
            StringBuilder sql = new StringBuilder();
            // SELECT句
            sql.Append("SELECT K1.KAISHA_CD  AS KAISHA_CD");
            sql.Append(", 1000007 AS SHAIN_CD");                                            // 仮社員コード
            sql.Append(", M1.BUMON_CD AS BUMON_CD");
            sql.Append(", 10000 AS BUKA_CD");                                               // 仮部課コード
            sql.Append(", 9 AS SEIBETSU");                                                  // 仮性別コード
            sql.Append(", MAX(M1.BUMON_CD) AS DISP_CD");                                    // 出力用コード
            sql.Append(", '【' + MAX(M1.BUMON_NM) + '】' AS DISP_NM");                      // 出力用名称
            sql.Append(", ' ' AS SEX");                                                     // 出力用性別
            sql.Append(", STR(COUNT(DISTINCT M1.SHAIN_CD)) + '人' AS TOTAL");                // 出力用人数
            // SELECT句続き～WHERE句(共通)
            sql.Append(GetSQLColsPart());
            sql.Append(GetSQLFromWherePart());
            // GROUP句
            sql.Append(" GROUP BY K1.KAISHA_CD, M1.BUMON_CD");
            // ORDER句
            sql.Append(" ORDER BY KAISHA_CD ASC,BUMON_CD ASC");

            return Util.ToString(sql);
        }

        /// <summary>
        /// 男女合計レコード取得SELECT文
        /// </summary>
        private string GetGenderSubtotalSQL()
        {
            StringBuilder sql = new StringBuilder();
            // SELECT句
            sql.Append("SELECT K1.KAISHA_CD  AS KAISHA_CD");
            sql.Append(", 1000000 AS SHAIN_CD");                                        // 仮社員コード
            sql.Append(", 10000 AS BUMON_CD");                                          // 仮部門コード
            sql.Append(", 10000 AS BUKA_CD");                                           // 仮部課コード
            sql.Append(", M1.SEIBETSU AS SEIBETSU");
            sql.Append(", ' ' AS DISP_CD");                                             // 出力用コード
            sql.Append(", CASE WHEN M1.SEIBETSU=1 THEN '【男子計】' ELSE '【女子計】' END AS DISP_NM");// 出力用名称
            sql.Append(", ' ' AS SEX");                                                 // 出力用性別
            sql.Append(", STR(COUNT(DISTINCT M1.SHAIN_CD)) + '人' AS TOTAL");            // 出力用人数
            // SELECT句続き～WHERE句(共通)
            sql.Append(GetSQLWithoutGroupingPart());
            // GROUP句
            sql.Append(" GROUP BY K1.KAISHA_CD, M1.BUMON_CD, M1.SEIBETSU");
            // ORDER句
            sql.Append(" ORDER BY KAISHA_CD ASC,BUMON_CD ASC, SEIBETSU ASC");

            return Util.ToString(sql);
        }
        /// <summary>
        /// 男女合計レコード取得SELECT文
        /// </summary>
        private string GetGenderSubtotalSQLA()
        {
            StringBuilder sql = new StringBuilder();
            // SELECT句
            sql.Append("SELECT K1.KAISHA_CD  AS KAISHA_CD");
            sql.Append(", 1000008 AS SHAIN_CD");                                        // 仮社員コード
            sql.Append(", 10008 AS BUMON_CD");                                          // 仮部門コード
            sql.Append(", 10008 AS BUKA_CD");                                           // 仮部課コード
            sql.Append(", M1.SEIBETSU AS SEIBETSU");
            sql.Append(", ' ' AS DISP_CD");                                             // 出力用コード
            sql.Append(", CASE WHEN M1.SEIBETSU=1 THEN '【男子計】' ELSE '【女子計】' END AS DISP_NM");// 出力用名称
            sql.Append(", ' ' AS SEX");                                                 // 出力用性別
            sql.Append(", STR(COUNT(DISTINCT M1.SHAIN_CD)) + '人' AS TOTAL");            // 出力用人数
            // SELECT句続き～WHERE句(共通)
            sql.Append(GetSQLColsPart());
            sql.Append(GetSQLFromWherePart());
            // GROUP句
            //sql.Append(" GROUP BY K1.KAISHA_CD, M1.BUMON_CD, M1.SEIBETSU");
            sql.Append(" GROUP BY K1.KAISHA_CD, M1.SEIBETSU");
            // ORDER句
            //sql.Append(" ORDER BY KAISHA_CD ASC,BUMON_CD ASC, SEIBETSU ASC");
            sql.Append(" ORDER BY KAISHA_CD ASC, SEIBETSU ASC");

            return Util.ToString(sql);
        }

        /// <summary>
        /// 合計レコード取得SELECT文
        /// </summary>
        private string GetTotalSQL()
        {
            StringBuilder sql = new StringBuilder();
            // SELECT句
            sql.Append("SELECT K1.KAISHA_CD  AS KAISHA_CD");
            sql.Append(", 1000000 AS SHAIN_CD");                                        // 仮社員コード
            sql.Append(", 10000 AS BUMON_CD");                                          // 仮部門コード
            sql.Append(", 10000 AS BUKA_CD");                                           // 仮部課コード
            sql.Append(", 9 AS SEIBETSU");                                              // 仮性別コード
            sql.Append(", ' ' AS DISP_CD");                                             // 出力用コード
            sql.Append(", '【合　計】' AS DISP_NM");                                     // 出力用名称
            sql.Append(", ' ' AS SEX");                                                 // 出力用性別
            sql.Append(", STR(COUNT(DISTINCT M1.SHAIN_CD)) + '人' AS TOTAL");            // 出力用人数
            // SELECT句続き～WHERE句(共通)
            sql.Append(GetSQLWithoutGroupingPart());
            // GROUP句
            sql.Append(" GROUP BY K1.KAISHA_CD");
            // ORDER句
            sql.Append(" ORDER BY KAISHA_CD ASC");

            return Util.ToString(sql);
        }
        /// <summary>
        /// 合計レコード取得SELECT文
        /// </summary>
        private string GetTotalSQLA()
        {
            StringBuilder sql = new StringBuilder();
            // SELECT句
            sql.Append("SELECT K1.KAISHA_CD  AS KAISHA_CD");
            sql.Append(", 1000009 AS SHAIN_CD");                                        // 仮社員コード
            sql.Append(", 10009 AS BUMON_CD");                                          // 仮部門コード
            sql.Append(", 10009 AS BUKA_CD");                                           // 仮部課コード
            sql.Append(", 9 AS SEIBETSU");                                              // 仮性別コード
            sql.Append(", ' ' AS DISP_CD");                                             // 出力用コード
            sql.Append(", '【合　計】' AS DISP_NM");                                     // 出力用名称
            sql.Append(", ' ' AS SEX");                                                 // 出力用性別
            sql.Append(", STR(COUNT(DISTINCT M1.SHAIN_CD)) + '人' AS TOTAL");            // 出力用人数
            // SELECT句続き～WHERE句(共通)
            sql.Append(GetSQLColsPart());
            sql.Append(GetSQLFromWherePart());
            // GROUP句
            sql.Append(" GROUP BY K1.KAISHA_CD");
            // ORDER句
            sql.Append(" ORDER BY KAISHA_CD ASC");

            return Util.ToString(sql);
        }

        /// <summary>
        /// グループ化部分を含まない共通SQL文
        /// </summary>
        private string GetSQLWithoutGroupingPart()
        {
            StringBuilder sql = new StringBuilder();

            // 給与明細データ
            sql.Append(", SUM(K1.KOMOKU1) AS KOMOKU1");
            sql.Append(", SUM(K1.KOMOKU2) AS KOMOKU2");
            sql.Append(", SUM(K1.KOMOKU3) AS KOMOKU3");
            sql.Append(", SUM(K1.KOMOKU4) AS KOMOKU4");
            sql.Append(", SUM(K1.KOMOKU5) AS KOMOKU5");
            sql.Append(", SUM(K1.KOMOKU6) AS KOMOKU6");
            sql.Append(", SUM(K1.KOMOKU7) AS KOMOKU7");
            sql.Append(", SUM(K1.KOMOKU8) AS KOMOKU8");
            sql.Append(", SUM(K1.KOMOKU9) AS KOMOKU9");
            sql.Append(", SUM(K1.KOMOKU10) AS KOMOKU10");
            sql.Append(", SUM(K1.KOMOKU11) AS KOMOKU11");
            sql.Append(", SUM(K1.KOMOKU12) AS KOMOKU12");
            sql.Append(", SUM(K1.KOMOKU13) AS KOMOKU13");
            sql.Append(", SUM(K1.KOMOKU14) AS KOMOKU14");
            sql.Append(", SUM(K1.KOMOKU15) AS KOMOKU15");
            sql.Append(", SUM(K1.KOMOKU16) AS KOMOKU16");
            sql.Append(", SUM(K1.KOMOKU17) AS KOMOKU17");
            sql.Append(", SUM(K1.KOMOKU18) AS KOMOKU18");
            sql.Append(", SUM(K1.KOMOKU19) AS KOMOKU19");
            sql.Append(", SUM(K1.KOMOKU20) AS KOMOKU20");
            sql.Append(" FROM TB_KY_KYUYO_MEISAI_KINTAI AS K1");
            sql.Append(" LEFT OUTER JOIN TB_KY_KYUYO_MEISAI_SHIKYU AS K2");
            sql.Append(" ON (K1.KAISHA_CD = K2.KAISHA_CD)");
            sql.Append(" AND (K1.SHAIN_CD = K2.SHAIN_CD)");
            sql.Append(" AND (K1.NENGETSU = K2.NENGETSU)");
            sql.Append(" LEFT OUTER JOIN TB_KY_KYUYO_MEISAI_KOJO AS K3");
            sql.Append(" ON (K1.KAISHA_CD = K3.KAISHA_CD)");
            sql.Append(" AND (K1.SHAIN_CD = K3.SHAIN_CD)");
            sql.Append(" AND (K1.NENGETSU = K3.NENGETSU)");
            sql.Append(" LEFT OUTER JOIN TB_KY_KYUYO_MEISAI_GOKEI AS K4");
            sql.Append(" ON (K1.KAISHA_CD = K4.KAISHA_CD)");
            sql.Append(" AND (K1.SHAIN_CD = K4.SHAIN_CD)");
            sql.Append(" AND (K1.NENGETSU = K4.NENGETSU)");
            sql.Append(" LEFT OUTER JOIN VI_KY_SHAIN_JOHO AS M1");
            sql.Append(" ON (K1.KAISHA_CD = M1.KAISHA_CD)");
            sql.Append(" AND (K1.SHAIN_CD = M1.SHAIN_CD) ");
            sql.Append(" WHERE K1.KAISHA_CD = @KAISHA_CD");
            sql.Append(" AND K1.NENGETSU BETWEEN @NENGETSU_FR AND @NENGETSU_TO");
            sql.Append(" AND K1.SHAIN_CD BETWEEN @SHAIN_CD_FR AND @SHAIN_CD_TO");
            sql.Append(" AND M1.BUMON_CD BETWEEN @BUMON_CD_FR AND @BUMON_CD_TO");
            sql.Append(" AND M1.BUKA_CD BETWEEN @BUKA_CD_FR AND @BUKA_CD_TO");

            return Util.ToString(sql);
        }

        /// <summary>
        /// グループ化部分を含まない共通COLUMNSQL文
        /// </summary>
        private string GetSQLColsPart()
        {
            StringBuilder sql = new StringBuilder();

            foreach(DataRow dr in dtRptSti.Rows)
            {
                string COL_NUMBER = Util.ToString(dr["KOMOKU_BANGO"]); 
                string ApndName = "";
                switch (Util.ToInt(dr["KOMOKU_KUBUN"]))
                {
                    case 1:
                        ApndName = "K1.KOMOKU" + COL_NUMBER;
                        break;
                    case 2:
                        ApndName = "K2.KOMOKU" + COL_NUMBER;
                        break;
                    case 3:
                        ApndName = "K3.KOMOKU" + COL_NUMBER;
                        break;
                    case 4:
                        ApndName = "K4.KOMOKU" + COL_NUMBER;
                        break;
                    case 5:
                        ApndName = Util.ToString(dr["KEISANSHIKI"]);
                        break;
                    default:
                        break;
                    
                }
                COL_NUMBER = Util.ToString(dr["INJI_ICHI"]);
                sql.Append(",SUM(" + ApndName + ") AS KOMOKU" + COL_NUMBER);

            }
            return Util.ToString(sql);
        }

        /// <summary>
        /// グループ化部分を含まない共通SQL文
        /// </summary>
        private string GetSQLFromWherePart()
        {
            StringBuilder sql = new StringBuilder();


            // 給与明細データ
            string[] table1;  
            if (this.Par1.Equals(KYUYO))
            {
                table1 = new string[4]{ "TB_KY_KYUYO_MEISAI_KINTAI", "TB_KY_KYUYO_MEISAI_SHIKYU", "TB_KY_KYUYO_MEISAI_KOJO", "TB_KY_KYUYO_MEISAI_GOKEI" };
            }
            else
            {
                table1 = new string[4]{ "TB_KY_SHOYO_MEISAI_KINTAI", "TB_KY_SHOYO_MEISAI_SHIKYU", "TB_KY_SHOYO_MEISAI_KOJO", "TB_KY_SHOYO_MEISAI_GOKEI" };
            }
            sql.Append(" FROM " + table1[0] + " AS K1");
            sql.Append(" LEFT OUTER JOIN " + table1[1] + " AS K2");
            sql.Append(" ON (K1.KAISHA_CD = K2.KAISHA_CD)");
            sql.Append(" AND (K1.SHAIN_CD = K2.SHAIN_CD)");
            sql.Append(" AND (K1.NENGETSU = K2.NENGETSU)");
            sql.Append(" LEFT OUTER JOIN " + table1[2] + " AS K3");
            sql.Append(" ON (K1.KAISHA_CD = K3.KAISHA_CD)");
            sql.Append(" AND (K1.SHAIN_CD = K3.SHAIN_CD)");
            sql.Append(" AND (K1.NENGETSU = K3.NENGETSU)");
            sql.Append(" LEFT OUTER JOIN " + table1[2] + " AS K4");
            sql.Append(" ON (K1.KAISHA_CD = K4.KAISHA_CD)");
            sql.Append(" AND (K1.SHAIN_CD = K4.SHAIN_CD)");
            sql.Append(" AND (K1.NENGETSU = K4.NENGETSU)");
            sql.Append(" LEFT OUTER JOIN VI_KY_SHAIN_JOHO AS M1");
            sql.Append(" ON (K1.KAISHA_CD = M1.KAISHA_CD)");
            sql.Append(" AND (K1.SHAIN_CD = M1.SHAIN_CD) ");
            sql.Append(" WHERE ");
            sql.Append("     K1.KAISHA_CD = @KAISHA_CD");
            sql.Append(" AND K1.NENGETSU BETWEEN @NENGETSU_FR AND @NENGETSU_TO");
            sql.Append(" AND K1.SHAIN_CD BETWEEN @SHAIN_CD_FR AND @SHAIN_CD_TO");
            sql.Append(" AND M1.BUMON_CD BETWEEN @BUMON_CD_FR AND @BUMON_CD_TO");
            sql.Append(" AND M1.BUKA_CD BETWEEN @BUKA_CD_FR AND @BUKA_CD_TO");

            return Util.ToString(sql);
        }

        /// <summary>
        /// PR_KY_TBLに更新するためのパラメータ設定をします。
        /// </summary>
        /// <param name="sort">ソートフィールド値</param>
        /// <param name="jpDateFr">集計期間自</param>
        /// <param name="jpDateTo">集計期間至</param>
        /// <param name="dtSti">帳票設定データ</param>
        /// <param name="dr">給与明細勤怠データ行</param>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// </returns>
        private ArrayList SetPrKyTblParams(int sort, string[] jpDateFr, string[] jpDateTo, DataTable dtSti, DataRow dr)
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            // ヘッダ情報
            updParam.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            updParam.SetParam("@SORT", SqlDbType.Int, sort);
            updParam.SetParam("@ITEM001", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);             // 会社名
            updParam.SetParam("@ITEM002", SqlDbType.VarChar, 200, string.Format("{0}{2}年{3}月", jpDateFr)); // 年月自
            updParam.SetParam("@ITEM003", SqlDbType.VarChar, 200, string.Format("{0}{2}年{3}月", jpDateTo)); // 年月至
            
            // 並び順指定フィールド
            // ※各DB定義値＋１桁(合計行用)の桁そろえを行う
            updParam.SetParam("@ITEM004", SqlDbType.VarChar, 200, 
                            string.Format("{0, 7}", Util.ToString(dr["SHAIN_CD"])));                // 社員コード
            updParam.SetParam("@ITEM005", SqlDbType.VarChar, 200, 
                            string.Format("{0, 5}", Util.ToString(dr["BUMON_CD"])));                // 部門コード
            updParam.SetParam("@ITEM006", SqlDbType.VarChar, 200, 
                            string.Format("{0, 5}", Util.ToString(dr["BUKA_CD"])));                 // 部課コード
            updParam.SetParam("@ITEM007", SqlDbType.VarChar, 200, Util.ToString(dr["SEIBETSU"]));   // 性別
            
            // 出力表項目
            updParam.SetParam("@ITEM041", SqlDbType.VarChar, 200, Util.ToString(dr["DISP_CD"]));    // 表示コード
            updParam.SetParam("@ITEM042", SqlDbType.VarChar, 200, Util.ToString(dr["DISP_NM"]));    // 表示名称
            updParam.SetParam("@ITEM043", SqlDbType.VarChar, 200, Util.ToString(dr["SEX"]));        // 表示性別
            updParam.SetParam("@ITEM044", SqlDbType.VarChar, 200, Util.ToString(dr["TOTAL"]));      // 表示人数
            // 勤怠項目
            foreach (DataRow drSti in dtSti.Rows)
            {
                string wfld1 = "";
                string wfld2 = "";
                string sfld = "";
                // 見出し：ワークフィールド名(ITEM011-024)
                if (Util.ToInt(drSti["INJI_ICHI"]) <= 24)
                {
                    wfld1 = "ITEM" + string.Format("{0:D3}",
                                        Util.ToInt(10 + Util.ToInt(drSti["INJI_ICHI"])));
                }
                else
                {
                    wfld1 = "ITEM034";
                }
                // 項目値：ワークフィールド名(ITEM051-074)
                wfld2 = "ITEM" + string.Format("{0:D3}",
                                    Util.ToInt(50 + Util.ToInt(drSti["INJI_ICHI"])));
                // 項目値：参照フィールド名
                sfld = "KOMOKU" + Util.ToString(drSti["KOMOKU_BANGO"]);


                // 見出し
                updParam.SetParam("@" + wfld1, SqlDbType.VarChar, 200, Util.ToString(drSti["KOMOKU_MEISHO"]));

                // 少数利用フラグ(少数0あり・1なし) 
                bool UseShosu = Util.ToString(drSti["KOMOKU_KUBUN2"]).Equals("0") ? true : false;
                if (UseShosu)  // 少数あり⇒0.00　少数なし⇒#,##0 の書式
                {
                    updParam.SetParam("@" + wfld2, SqlDbType.VarChar, 200, Util.FormatNum(dr[sfld], 2));                                 
                }
                else
                {
                    updParam.SetParam("@" + wfld2, SqlDbType.VarChar, 200, Util.FormatNum(dr[sfld]));                                 
                }
            }

            alParams.Add(updParam);

            return alParams;
        }

        /// <summary>
        /// PR_KY_TBLに更新するためのパラメータ設定をします。
        /// </summary>
        /// <param name="sort">ソートフィールド値</param>
        /// <param name="jpDateFr">集計期間自</param>
        /// <param name="jpDateTo">集計期間至</param>
        /// <param name="dtSti">帳票設定データ</param>
        /// <param name="dr">給与明細勤怠データ行</param>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// </returns>
        private ArrayList SetPrKyTblParamsA(int sort, string[] jpDateFr, string[] jpDateTo, DataTable dtSti, DataRow dr)
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            // ヘッダ情報
            updParam.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            updParam.SetParam("@SORT", SqlDbType.Int, sort);
            updParam.SetParam("@ITEM001", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);             // 会社名
            updParam.SetParam("@ITEM002", SqlDbType.VarChar, 200, string.Format("{0}{2}年{3}月", jpDateFr)); // 年月自
            updParam.SetParam("@ITEM003", SqlDbType.VarChar, 200, string.Format("{0}{2}年{3}月", jpDateTo)); // 年月至

            // 並び順指定フィールド
            // ※各DB定義値＋１桁(合計行用)の桁そろえを行う
            updParam.SetParam("@ITEM004", SqlDbType.VarChar, 200,
                            string.Format("{0, 7}", Util.ToString(dr["SHAIN_CD"])));                // 社員コード
            updParam.SetParam("@ITEM005", SqlDbType.VarChar, 200,
                            string.Format("{0, 5}", Util.ToString(dr["BUMON_CD"])));                // 部門コード
            updParam.SetParam("@ITEM006", SqlDbType.VarChar, 200,
                            string.Format("{0, 5}", Util.ToString(dr["BUKA_CD"])));                 // 部課コード
            updParam.SetParam("@ITEM007", SqlDbType.VarChar, 200, Util.ToString(dr["SEIBETSU"]));   // 性別
            updParam.SetParam("@ITEM008", SqlDbType.VarChar, 200, Util.ToString(dr["DISP_NM"]));    // 表示名称
            updParam.SetParam("@ITEM009", SqlDbType.VarChar, 200, this.lblTitle.Text);      // レポートタイトル

            // 出力表項目
            updParam.SetParam("@ITEM010", SqlDbType.VarChar, 200, Util.ToString(dr["DISP_CD"]));    // 表示コード
            updParam.SetParam("@ITEM011", SqlDbType.VarChar, 200, Util.ToString(dr["SEX"]));        // 表示性別
            updParam.SetParam("@ITEM012", SqlDbType.VarChar, 200, Util.ToString(dr["TOTAL"]));      // 表示人数

            // 勤怠項目
            foreach (DataRow drSti in dtSti.Rows)
            {
                string wfld1 = "";
                string wfld2 = "";
                string sfld = "";
                // 見出し：ワークフィールド名(ITEM011-024)
                //if (Util.ToInt(drSti["INJI_ICHI"]) <= 24)
                if (Util.ToInt(drSti["INJI_ICHI"]) <= RowCnt)
                {
                    wfld1 = "ITEM" + string.Format("{0:D3}",
                                        Util.ToInt(20 + Util.ToInt(drSti["INJI_ICHI"])));
                }
                else
                {
                    wfld1 = "ITEM" + string.Format("{0:D3}",
                                        Util.ToInt(20 + (RowCnt + 1)));
                }
                // 項目値：ワークフィールド名(ITEM051-074)
                wfld2 = "ITEM" + string.Format("{0:D3}",
                                    Util.ToInt(70 + Util.ToInt(drSti["INJI_ICHI"])));
                // 項目値：参照フィールド名
                //sfld = "KOMOKU" + Util.ToString(drSti["KOMOKU_BANGO"]);
                sfld = "KOMOKU" + Util.ToString(drSti["INJI_ICHI"]);

                // 見出し
                updParam.SetParam("@" + wfld1, SqlDbType.VarChar, 200, Util.ToString(drSti["KOMOKU_MEISHO"]));

                bool UseShosu = false;
                // 勤怠項目の場合は少数フラグをチェックする
                if (Util.ToString(drSti["KOMOKU_KUBUN"]).Equals("1"))
                {
                    // 少数利用フラグ(少数0あり・1なし) 
                    UseShosu = Util.ToString(drSti["KOMOKU_KUBUN2"]).Equals("0") ? true : false;
                }
                if (UseShosu)  // 少数あり⇒0.00　少数なし⇒#,##0 の書式
                {
                    updParam.SetParam("@" + wfld2, SqlDbType.VarChar, 200, Util.FormatNum(dr[sfld], 2));
                }
                else
                {
                    updParam.SetParam("@" + wfld2, SqlDbType.VarChar, 200, Util.FormatNum(dr[sfld]));
                }
            }

            alParams.Add(updParam);

            return alParams;
        }

        /// <summary>
        /// 配列に格納された和暦支給月を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpDateNengetsu(string[] arrJpDate)
        {
            this.lblGengoNengetsuFr.Text = arrJpDate[0];
            this.txtGengoYearNengetsuFr.Text = arrJpDate[2];
            this.txtMonthNengetsuFr.Text = arrJpDate[3];
        }

        /// <summary>
        /// 配列に格納された和暦対象月を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpDateTaisho(string[] arrJpDate)
        {
            this.lblGengoNengetsuTo.Text = arrJpDate[0];
            this.txtGengoYearNengetsuTo.Text = arrJpDate[2];
            this.txtMonthNengetsuTo.Text = arrJpDate[3];
        }

        private string[] getShikyuDate()
        {
            string[] retDate;

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            string sql = "SELECT MAX(NENGETSU) AS MAX_NENGETSU";
            sql += " FROM TB_KY_KYUYO_MEISAI_KINTAI WHERE KAISHA_CD = @KAISHA_CD";
            DataTable dtShikyuInfo =
                this.Dba.GetDataTableFromSqlWithParams(sql, dpc);
            if (dtShikyuInfo.Rows.Count == 0 || ValChk.IsEmpty(dtShikyuInfo.Rows[0]["MAX_NENGETSU"]))
            {
                // 該当支給データ存在しない場合はシステム日付をセット
                retDate = Util.ConvJpDate(DateTime.Now, this.Dba);
            }
            else
            {
                retDate = Util.ConvJpDate(Util.ToDate(dtShikyuInfo.Rows[0]["MAX_NENGETSU"]), this.Dba);
            }

            return retDate;
        }

        /// <summary>
        /// 帳票タイトル項目数の取得
        /// </summary>
        /// <param name="rptType">給与か賞与かを指定する。　1:給与、2:賞与</param>
        /// <param name="rptNo">帳票番号を指定</param>
        /// <returns></returns>
        private bool getReportInfo(string rptType, string rptNo)
        {
            StringBuilder sql = new StringBuilder();
            DbParamCollection dpc = new DbParamCollection();

            //帳票タイトル取得
            sql.Append(" SELECT CHOHYO_NM, MAX(INJI_ICHI) AS ROW_CNT FROM TB_KY_CHOHYO_KOMOKU_SETTEI ");
            sql.Append(" WHERE ");
            sql.Append(" KAISHA_CD = @KAISHA_CD AND ");
            sql.Append(" KYUYO_SHOYO = @KYUYO_SHOYO AND ");
            sql.Append(" CHOHYO_BANGO = @CHOHYO_BANGO ");
            sql.Append(" GROUP BY");
            sql.Append(" CHOHYO_NM");

            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@KYUYO_SHOYO", SqlDbType.Decimal, 1, rptType);
            dpc.SetParam("@CHOHYO_BANGO", SqlDbType.Decimal, 2, rptNo);
            DataTable dtRptInfo = this.Dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);
            if (dtRptInfo.Rows.Count == 0)
            {
                Msg.Info("帳票設定がされていません、帳票設定を行ってください。");
                return false;
            }
            else
            {
                DataRow dr = dtRptInfo.Rows[0];
                // 帳票タイトル取得
                if (Util.ToString(dr["CHOHYO_NM"]) != "")
                {
                    this.lblTitle.Text = Util.ToString(dr["CHOHYO_NM"]);
                    this.Text = this.lblTitle.Text;
                }
                // レポート項目数取得
                RowCnt = (Util.ToInt(dr["ROW_CNT"]) == 0) ? _rowCnt : Util.ToInt(dr["ROW_CNT"]);
                if (RowCnt < 25)
                {
                    rptCols = rptCols1;
                }
                else if (RowCnt < 37)
                {
                    rptCols = rptCols2;
                }
                else if (RowCnt < 49)
                {
                    rptCols = rptCols3;
                }
                else
                {
                    Msg.Info("帳票項目設定に誤りがあります。\n帳票項目設定を確認してください。");
                    return false;
                }

                return true;
            }
        }

        /// <summary>
        /// 印刷用使用列名文字配列の作成
        /// </summary>
        /// <param name="colsCnt">列数</param>
        /// <param name="Prefix">接頭詞</param>
        /// <returns>印刷テーブル使用列名文字列</returns>
        private StringBuilder ColsArray(int colsCnt, string Prefix)
        {
            StringBuilder cols = new StringBuilder();
            string itemNm = "ITEM";
            string wk;
            for (int i = 1; i < colsCnt + 1; i++)
            {
                if (i == 1)
                {
                    wk = "  " + Prefix + itemNm + Util.PadZero(i, 3);
                }
                else
                {
                    wk = " ," + Prefix + itemNm + Util.PadZero(i, 3);
                }
                cols.Append(wk);
            }
            return cols;
        }

        private string[] reportInfo()
        {
            string[] ret;
            StringBuilder sql = new StringBuilder();
            DbParamCollection dpc = new DbParamCollection();
            sql.Append(" SELECT");
            sql.Append(" CHOHYO_NM AS NAME");
            sql.Append(" FROM TB_KY_CHOHYO_KOMOKU_SETTEI");
            sql.Append(" WHERE");
            sql.Append(" KYUYO_SHOYO = @KYUYO_SHOYO AND ");
            sql.Append(" CHOHYO_BANGO = @CHOHYO_BANGO ");
            sql.Append(" GROUP BY");
            sql.Append(" CHOHYO_NM");
            dpc.SetParam("@KYUYO_SHOYO", SqlDbType.VarChar, 2, this.Par1);
            dpc.SetParam("@CHOHYO_BANGO", SqlDbType.VarChar, 2, this.Par2);
            DataTable repInfoTbl = this.Dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);
            if (repInfoTbl.Rows.Count == 0)
            {
                Msg.Notice("帳票項目設定がされていません。\n帳票項目設定を行ってください。");
                return null;
            }
            else
            {
                ret = new string[repInfoTbl.Rows.Count];
                int i = 0;
                foreach (DataRow dr in repInfoTbl.Rows)
                {
                    ret[i] = Util.ToString(dr["NAME"]);
                }
                return ret;
            }
        }
        #endregion
    }
}