﻿namespace jp.co.fsi.ky.kykr1021
{
    /// <summary>
    /// KYKR1023R の帳票
    /// </summary>
    partial class KYKR1023R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(KYKR1023R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.textBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle08 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle07 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle06 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle05 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle04 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle21 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle09 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtTitle02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtTitle10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle22 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle23 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle24 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle25 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle26 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line19 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtCompanyName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtPageCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtToday = new GrapeCity.ActiveReports.SectionReportModel.ReportInfo();
            this.txtHyojiDateFr = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtHyojiDateTo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox54 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.textBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox21 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox22 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox23 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox24 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox25 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line27 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line21 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line22 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line23 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line24 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line25 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line26 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtVal10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtVal22 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtVal21 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtVal09 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtVal08 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtVal20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtVal19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtVal07 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtVal06 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtVal18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtVal17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtVal05 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtVal23 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtVal11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtVal02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtVal01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line28 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtVal12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtVal24 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtVal13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtVal25 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtVal14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtVal26 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtVal15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtVal27 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtVal16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtVal28 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line36 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtVal03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox74 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line40 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.textBox26 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox27 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox28 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox29 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox30 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox31 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox32 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox33 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox34 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox35 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox36 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox37 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox38 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox39 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox40 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox41 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox42 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox43 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox44 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox45 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox46 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox47 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox48 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox49 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line32 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line33 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line34 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line35 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line37 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line31 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line30 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line29 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line18 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line39 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line38 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line20 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line15 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line16 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle08)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle07)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle06)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle05)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle04)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle09)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtToday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHyojiDateFr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHyojiDateTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox54)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVal10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVal22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVal21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVal09)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVal08)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVal20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVal19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVal07)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVal06)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVal18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVal17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVal05)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVal23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVal11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVal02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVal01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVal12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVal24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVal13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVal25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVal14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVal26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVal15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVal27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVal16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVal28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVal03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox74)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.textBox1,
            this.txtTitle08,
            this.txtTitle20,
            this.txtTitle19,
            this.txtTitle07,
            this.txtTitle06,
            this.txtTitle18,
            this.txtTitle17,
            this.txtTitle05,
            this.txtTitle04,
            this.txtTitle16,
            this.txtTitle15,
            this.txtTitle03,
            this.txtTitle21,
            this.txtTitle09,
            this.lblTitle,
            this.txtTitle02,
            this.txtTitle01,
            this.line2,
            this.txtTitle10,
            this.txtTitle22,
            this.txtTitle11,
            this.txtTitle23,
            this.txtTitle12,
            this.txtTitle24,
            this.txtTitle13,
            this.txtTitle25,
            this.txtTitle14,
            this.txtTitle26,
            this.line19,
            this.txtCompanyName,
            this.lblPage,
            this.txtPageCount,
            this.txtToday,
            this.txtHyojiDateFr,
            this.txtHyojiDateTo,
            this.textBox54,
            this.line1,
            this.textBox2,
            this.textBox3,
            this.textBox4,
            this.textBox5,
            this.textBox6,
            this.textBox7,
            this.textBox8,
            this.textBox9,
            this.textBox10,
            this.textBox11,
            this.textBox12,
            this.textBox13,
            this.textBox14,
            this.textBox15,
            this.textBox16,
            this.textBox17,
            this.textBox18,
            this.textBox19,
            this.textBox20,
            this.textBox21,
            this.textBox22,
            this.textBox23,
            this.textBox24,
            this.textBox25,
            this.line7,
            this.line6,
            this.line27,
            this.line8,
            this.line5,
            this.line4,
            this.line3,
            this.line21,
            this.line22,
            this.line23,
            this.line24,
            this.line25,
            this.line26,
            this.line10,
            this.line13,
            this.line14,
            this.line9});
            this.pageHeader.Height = 1.351165F;
            this.pageHeader.Name = "pageHeader";
            // 
            // textBox1
            // 
            this.textBox1.Height = 0.6299213F;
            this.textBox1.Left = 0F;
            this.textBox1.MultiLine = false;
            this.textBox1.Name = "textBox1";
            this.textBox1.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold;" +
    " text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox1.Text = null;
            this.textBox1.Top = 0.7086615F;
            this.textBox1.Width = 10.90551F;
            // 
            // txtTitle08
            // 
            this.txtTitle08.DataField = "ITEM026";
            this.txtTitle08.Height = 0.1574803F;
            this.txtTitle08.Left = 5.669291F;
            this.txtTitle08.MultiLine = false;
            this.txtTitle08.Name = "txtTitle08";
            this.txtTitle08.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: bold; text-align: center; ver" +
    "tical-align: bottom; ddo-char-set: 128";
            this.txtTitle08.Text = "ITEM026";
            this.txtTitle08.Top = 0.7055119F;
            this.txtTitle08.Width = 0.7480315F;
            // 
            // txtTitle20
            // 
            this.txtTitle20.DataField = "ITEM038";
            this.txtTitle20.Height = 0.1574803F;
            this.txtTitle20.Left = 5.669291F;
            this.txtTitle20.MultiLine = false;
            this.txtTitle20.Name = "txtTitle20";
            this.txtTitle20.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: bold; text-align: center; ver" +
    "tical-align: bottom; ddo-char-set: 128";
            this.txtTitle20.Text = "ITEM038";
            this.txtTitle20.Top = 0.8661418F;
            this.txtTitle20.Width = 0.7480315F;
            // 
            // txtTitle19
            // 
            this.txtTitle19.DataField = "ITEM037";
            this.txtTitle19.Height = 0.1574803F;
            this.txtTitle19.Left = 4.92126F;
            this.txtTitle19.MultiLine = false;
            this.txtTitle19.Name = "txtTitle19";
            this.txtTitle19.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: bold; text-align: center; ver" +
    "tical-align: bottom; ddo-char-set: 128";
            this.txtTitle19.Text = "ITEM037";
            this.txtTitle19.Top = 0.8661418F;
            this.txtTitle19.Width = 0.7480315F;
            // 
            // txtTitle07
            // 
            this.txtTitle07.DataField = "ITEM025";
            this.txtTitle07.Height = 0.1574803F;
            this.txtTitle07.Left = 4.92126F;
            this.txtTitle07.MultiLine = false;
            this.txtTitle07.Name = "txtTitle07";
            this.txtTitle07.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: bold; text-align: center; ver" +
    "tical-align: bottom; ddo-char-set: 128";
            this.txtTitle07.Text = "ITEM025";
            this.txtTitle07.Top = 0.7055119F;
            this.txtTitle07.Width = 0.7480315F;
            // 
            // txtTitle06
            // 
            this.txtTitle06.DataField = "ITEM024";
            this.txtTitle06.Height = 0.1574803F;
            this.txtTitle06.Left = 4.151181F;
            this.txtTitle06.MultiLine = false;
            this.txtTitle06.Name = "txtTitle06";
            this.txtTitle06.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: bold; text-align: center; ver" +
    "tical-align: bottom; ddo-char-set: 128";
            this.txtTitle06.Text = "ITEM024";
            this.txtTitle06.Top = 0.7055119F;
            this.txtTitle06.Width = 0.7480315F;
            // 
            // txtTitle18
            // 
            this.txtTitle18.DataField = "ITEM036";
            this.txtTitle18.Height = 0.1574803F;
            this.txtTitle18.Left = 4.151181F;
            this.txtTitle18.MultiLine = false;
            this.txtTitle18.Name = "txtTitle18";
            this.txtTitle18.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: bold; text-align: center; ver" +
    "tical-align: bottom; ddo-char-set: 128";
            this.txtTitle18.Text = "ITEM036";
            this.txtTitle18.Top = 0.8661418F;
            this.txtTitle18.Width = 0.7480315F;
            // 
            // txtTitle17
            // 
            this.txtTitle17.DataField = "ITEM035";
            this.txtTitle17.Height = 0.1574803F;
            this.txtTitle17.Left = 3.40315F;
            this.txtTitle17.MultiLine = false;
            this.txtTitle17.Name = "txtTitle17";
            this.txtTitle17.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: bold; text-align: center; ver" +
    "tical-align: bottom; ddo-char-set: 128";
            this.txtTitle17.Text = "ITEM035";
            this.txtTitle17.Top = 0.8661418F;
            this.txtTitle17.Width = 0.7480315F;
            // 
            // txtTitle05
            // 
            this.txtTitle05.DataField = "ITEM023";
            this.txtTitle05.Height = 0.1574803F;
            this.txtTitle05.Left = 3.40315F;
            this.txtTitle05.MultiLine = false;
            this.txtTitle05.Name = "txtTitle05";
            this.txtTitle05.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: bold; text-align: center; ver" +
    "tical-align: bottom; ddo-char-set: 128";
            this.txtTitle05.Text = "ITEM023";
            this.txtTitle05.Top = 0.7086616F;
            this.txtTitle05.Width = 0.7480315F;
            // 
            // txtTitle04
            // 
            this.txtTitle04.DataField = "ITEM022";
            this.txtTitle04.Height = 0.1574803F;
            this.txtTitle04.Left = 2.655118F;
            this.txtTitle04.MultiLine = false;
            this.txtTitle04.Name = "txtTitle04";
            this.txtTitle04.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: bold; text-align: center; ver" +
    "tical-align: bottom; ddo-char-set: 128";
            this.txtTitle04.Text = "ITEM022";
            this.txtTitle04.Top = 0.7055119F;
            this.txtTitle04.Width = 0.7480315F;
            // 
            // txtTitle16
            // 
            this.txtTitle16.DataField = "ITEM034";
            this.txtTitle16.Height = 0.1574803F;
            this.txtTitle16.Left = 2.655118F;
            this.txtTitle16.MultiLine = false;
            this.txtTitle16.Name = "txtTitle16";
            this.txtTitle16.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: bold; text-align: center; ver" +
    "tical-align: bottom; ddo-char-set: 128";
            this.txtTitle16.Text = "ITEM034";
            this.txtTitle16.Top = 0.8661418F;
            this.txtTitle16.Width = 0.7480315F;
            // 
            // txtTitle15
            // 
            this.txtTitle15.DataField = "ITEM033";
            this.txtTitle15.Height = 0.1574803F;
            this.txtTitle15.Left = 1.907087F;
            this.txtTitle15.MultiLine = false;
            this.txtTitle15.Name = "txtTitle15";
            this.txtTitle15.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: bold; text-align: center; ver" +
    "tical-align: bottom; ddo-char-set: 128";
            this.txtTitle15.Text = "ITEM033";
            this.txtTitle15.Top = 0.8661418F;
            this.txtTitle15.Width = 0.7480315F;
            // 
            // txtTitle03
            // 
            this.txtTitle03.DataField = "ITEM021";
            this.txtTitle03.Height = 0.1574804F;
            this.txtTitle03.Left = 1.907087F;
            this.txtTitle03.MultiLine = false;
            this.txtTitle03.Name = "txtTitle03";
            this.txtTitle03.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: bold; text-align: center; ver" +
    "tical-align: bottom; ddo-char-set: 128";
            this.txtTitle03.Text = "ITEM021";
            this.txtTitle03.Top = 0.7086615F;
            this.txtTitle03.Width = 0.7480315F;
            // 
            // txtTitle21
            // 
            this.txtTitle21.DataField = "ITEM039";
            this.txtTitle21.Height = 0.1574803F;
            this.txtTitle21.Left = 6.417323F;
            this.txtTitle21.MultiLine = false;
            this.txtTitle21.Name = "txtTitle21";
            this.txtTitle21.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: bold; text-align: center; ver" +
    "tical-align: bottom; ddo-char-set: 128";
            this.txtTitle21.Text = "ITEM039";
            this.txtTitle21.Top = 0.8661418F;
            this.txtTitle21.Width = 0.7480315F;
            // 
            // txtTitle09
            // 
            this.txtTitle09.DataField = "ITEM027";
            this.txtTitle09.Height = 0.1574803F;
            this.txtTitle09.Left = 6.417323F;
            this.txtTitle09.MultiLine = false;
            this.txtTitle09.Name = "txtTitle09";
            this.txtTitle09.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: bold; text-align: center; ver" +
    "tical-align: bottom; ddo-char-set: 128";
            this.txtTitle09.Text = "ITEM027";
            this.txtTitle09.Top = 0.7055119F;
            this.txtTitle09.Width = 0.7480315F;
            // 
            // lblTitle
            // 
            this.lblTitle.DataField = "ITEM009";
            this.lblTitle.Height = 0.3149607F;
            this.lblTitle.HyperLink = null;
            this.lblTitle.Left = 3.681101F;
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Style = "font-family: ＭＳ 明朝; font-size: 18pt; font-weight: bold; text-align: justify; text" +
    "-justify: distribute-all-lines; vertical-align: middle; ddo-char-set: 128";
            this.lblTitle.Text = "勤 怠 項 目 一 覧 表";
            this.lblTitle.Top = 0F;
            this.lblTitle.Width = 3.543307F;
            // 
            // txtTitle02
            // 
            this.txtTitle02.Height = 0.3149606F;
            this.txtTitle02.Left = 0.4582677F;
            this.txtTitle02.MultiLine = false;
            this.txtTitle02.Name = "txtTitle02";
            this.txtTitle02.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; ver" +
    "tical-align: middle; ddo-char-set: 1";
            this.txtTitle02.Text = "氏   名";
            this.txtTitle02.Top = 0.7086615F;
            this.txtTitle02.Width = 1.431496F;
            // 
            // txtTitle01
            // 
            this.txtTitle01.Height = 0.3118111F;
            this.txtTitle01.Left = 0F;
            this.txtTitle01.MultiLine = false;
            this.txtTitle01.Name = "txtTitle01";
            this.txtTitle01.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; ver" +
    "tical-align: middle; ddo-char-set: 1";
            this.txtTitle01.Text = "コード";
            this.txtTitle01.Top = 0.7118111F;
            this.txtTitle01.Width = 0.4582677F;
            // 
            // line2
            // 
            this.line2.Height = 0.3149607F;
            this.line2.Left = 0F;
            this.line2.LineWeight = 1F;
            this.line2.Name = "line2";
            this.line2.Top = 0.7086614F;
            this.line2.Width = 9.536743E-07F;
            this.line2.X1 = 9.536743E-07F;
            this.line2.X2 = 0F;
            this.line2.Y1 = 0.7086614F;
            this.line2.Y2 = 1.023622F;
            // 
            // txtTitle10
            // 
            this.txtTitle10.DataField = "ITEM028";
            this.txtTitle10.Height = 0.1574803F;
            this.txtTitle10.Left = 7.165356F;
            this.txtTitle10.MultiLine = false;
            this.txtTitle10.Name = "txtTitle10";
            this.txtTitle10.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: bold; text-align: center; ver" +
    "tical-align: bottom; ddo-char-set: 128";
            this.txtTitle10.Text = "ITEM028";
            this.txtTitle10.Top = 0.7055119F;
            this.txtTitle10.Width = 0.7480315F;
            // 
            // txtTitle22
            // 
            this.txtTitle22.DataField = "ITEM040";
            this.txtTitle22.Height = 0.1574803F;
            this.txtTitle22.Left = 7.165356F;
            this.txtTitle22.MultiLine = false;
            this.txtTitle22.Name = "txtTitle22";
            this.txtTitle22.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: bold; text-align: center; ver" +
    "tical-align: bottom; ddo-char-set: 128";
            this.txtTitle22.Text = "ITEM040";
            this.txtTitle22.Top = 0.8661418F;
            this.txtTitle22.Width = 0.7480315F;
            // 
            // txtTitle11
            // 
            this.txtTitle11.DataField = "ITEM029";
            this.txtTitle11.Height = 0.1574803F;
            this.txtTitle11.Left = 7.913386F;
            this.txtTitle11.MultiLine = false;
            this.txtTitle11.Name = "txtTitle11";
            this.txtTitle11.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: bold; text-align: center; ver" +
    "tical-align: bottom; ddo-char-set: 128";
            this.txtTitle11.Text = "ITEM029";
            this.txtTitle11.Top = 0.7055119F;
            this.txtTitle11.Width = 0.7480315F;
            // 
            // txtTitle23
            // 
            this.txtTitle23.DataField = "ITEM041";
            this.txtTitle23.Height = 0.1574803F;
            this.txtTitle23.Left = 7.913386F;
            this.txtTitle23.MultiLine = false;
            this.txtTitle23.Name = "txtTitle23";
            this.txtTitle23.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: bold; text-align: center; ver" +
    "tical-align: bottom; ddo-char-set: 128";
            this.txtTitle23.Text = "ITEM041";
            this.txtTitle23.Top = 0.8661418F;
            this.txtTitle23.Width = 0.7480315F;
            // 
            // txtTitle12
            // 
            this.txtTitle12.DataField = "ITEM030";
            this.txtTitle12.Height = 0.1574803F;
            this.txtTitle12.Left = 8.661417F;
            this.txtTitle12.MultiLine = false;
            this.txtTitle12.Name = "txtTitle12";
            this.txtTitle12.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: bold; text-align: center; ver" +
    "tical-align: bottom; ddo-char-set: 128";
            this.txtTitle12.Text = "ITEM030";
            this.txtTitle12.Top = 0.7055119F;
            this.txtTitle12.Width = 0.7480315F;
            // 
            // txtTitle24
            // 
            this.txtTitle24.DataField = "ITEM042";
            this.txtTitle24.Height = 0.1574803F;
            this.txtTitle24.Left = 8.661417F;
            this.txtTitle24.MultiLine = false;
            this.txtTitle24.Name = "txtTitle24";
            this.txtTitle24.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: bold; text-align: center; ver" +
    "tical-align: bottom; ddo-char-set: 128";
            this.txtTitle24.Text = "ITEM042";
            this.txtTitle24.Top = 0.8661418F;
            this.txtTitle24.Width = 0.7480315F;
            // 
            // txtTitle13
            // 
            this.txtTitle13.DataField = "ITEM031";
            this.txtTitle13.Height = 0.1574803F;
            this.txtTitle13.Left = 9.409449F;
            this.txtTitle13.MultiLine = false;
            this.txtTitle13.Name = "txtTitle13";
            this.txtTitle13.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: bold; text-align: center; ver" +
    "tical-align: bottom; ddo-char-set: 128";
            this.txtTitle13.Text = "ITEM031";
            this.txtTitle13.Top = 0.7055119F;
            this.txtTitle13.Width = 0.7480315F;
            // 
            // txtTitle25
            // 
            this.txtTitle25.DataField = "ITEM043";
            this.txtTitle25.Height = 0.1574803F;
            this.txtTitle25.Left = 9.409449F;
            this.txtTitle25.MultiLine = false;
            this.txtTitle25.Name = "txtTitle25";
            this.txtTitle25.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: bold; text-align: center; ver" +
    "tical-align: bottom; ddo-char-set: 128";
            this.txtTitle25.Text = "ITEM043";
            this.txtTitle25.Top = 0.8661418F;
            this.txtTitle25.Width = 0.7480315F;
            // 
            // txtTitle14
            // 
            this.txtTitle14.DataField = "ITEM032";
            this.txtTitle14.Height = 0.1574803F;
            this.txtTitle14.Left = 10.15748F;
            this.txtTitle14.MultiLine = false;
            this.txtTitle14.Name = "txtTitle14";
            this.txtTitle14.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: bold; text-align: center; ver" +
    "tical-align: bottom; ddo-char-set: 128";
            this.txtTitle14.Text = "ITEM032";
            this.txtTitle14.Top = 0.7055119F;
            this.txtTitle14.Width = 0.7480315F;
            // 
            // txtTitle26
            // 
            this.txtTitle26.DataField = "ITEM044";
            this.txtTitle26.Height = 0.1574803F;
            this.txtTitle26.Left = 10.15748F;
            this.txtTitle26.MultiLine = false;
            this.txtTitle26.Name = "txtTitle26";
            this.txtTitle26.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: bold; text-align: center; ver" +
    "tical-align: bottom; ddo-char-set: 128";
            this.txtTitle26.Text = "ITEM044";
            this.txtTitle26.Top = 0.8661418F;
            this.txtTitle26.Width = 0.7480315F;
            // 
            // line19
            // 
            this.line19.Height = 0F;
            this.line19.Left = 1.889764F;
            this.line19.LineWeight = 1F;
            this.line19.Name = "line19";
            this.line19.Top = 0.8629922F;
            this.line19.Width = 9.011816F;
            this.line19.X1 = 1.889764F;
            this.line19.X2 = 10.90158F;
            this.line19.Y1 = 0.8629922F;
            this.line19.Y2 = 0.8629922F;
            // 
            // txtCompanyName
            // 
            this.txtCompanyName.DataField = "ITEM001";
            this.txtCompanyName.Height = 0.2F;
            this.txtCompanyName.Left = 0F;
            this.txtCompanyName.Name = "txtCompanyName";
            this.txtCompanyName.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: bottom; ddo-char-set: 1";
            this.txtCompanyName.Text = "1";
            this.txtCompanyName.Top = 0.3937008F;
            this.txtCompanyName.Width = 2.740158F;
            // 
            // lblPage
            // 
            this.lblPage.Height = 0.2F;
            this.lblPage.HyperLink = null;
            this.lblPage.Left = 10.55118F;
            this.lblPage.Name = "lblPage";
            this.lblPage.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
            this.lblPage.Text = "頁";
            this.lblPage.Top = 0.3149607F;
            this.lblPage.Width = 0.2212596F;
            // 
            // txtPageCount
            // 
            this.txtPageCount.Height = 0.2F;
            this.txtPageCount.Left = 10.30788F;
            this.txtPageCount.MultiLine = false;
            this.txtPageCount.Name = "txtPageCount";
            this.txtPageCount.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
            this.txtPageCount.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtPageCount.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.txtPageCount.Text = "999";
            this.txtPageCount.Top = 0.3149607F;
            this.txtPageCount.Width = 0.2433071F;
            // 
            // txtToday
            // 
            this.txtToday.FormatString = "{RunDateTime:yyyy/MM/dd}";
            this.txtToday.Height = 0.2F;
            this.txtToday.Left = 8.740158F;
            this.txtToday.Name = "txtToday";
            this.txtToday.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: top; ddo-char-set: 1";
            this.txtToday.Top = 0.3149607F;
            this.txtToday.Width = 1.394487F;
            // 
            // txtHyojiDateFr
            // 
            this.txtHyojiDateFr.DataField = "ITEM002";
            this.txtHyojiDateFr.Height = 0.1968504F;
            this.txtHyojiDateFr.Left = 4.151181F;
            this.txtHyojiDateFr.MultiLine = false;
            this.txtHyojiDateFr.Name = "txtHyojiDateFr";
            this.txtHyojiDateFr.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
            this.txtHyojiDateFr.Text = "02平成27年12月20日";
            this.txtHyojiDateFr.Top = 0.3181102F;
            this.txtHyojiDateFr.Width = 1.201181F;
            // 
            // txtHyojiDateTo
            // 
            this.txtHyojiDateTo.DataField = "ITEM003";
            this.txtHyojiDateTo.Height = 0.1968504F;
            this.txtHyojiDateTo.Left = 5.53189F;
            this.txtHyojiDateTo.MultiLine = false;
            this.txtHyojiDateTo.Name = "txtHyojiDateTo";
            this.txtHyojiDateTo.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 1";
            this.txtHyojiDateTo.Text = "03平成27年12月20日";
            this.txtHyojiDateTo.Top = 0.3149607F;
            this.txtHyojiDateTo.Width = 1.161024F;
            // 
            // textBox54
            // 
            this.textBox54.Height = 0.2F;
            this.textBox54.Left = 5.352363F;
            this.textBox54.MultiLine = false;
            this.textBox54.Name = "textBox54";
            this.textBox54.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
            this.textBox54.Text = "～";
            this.textBox54.Top = 0.3181103F;
            this.textBox54.Width = 0.1795273F;
            // 
            // line1
            // 
            this.line1.Height = 0F;
            this.line1.Left = 0F;
            this.line1.LineWeight = 1F;
            this.line1.Name = "line1";
            this.line1.Top = 0.7118111F;
            this.line1.Width = 10.90158F;
            this.line1.X1 = 0F;
            this.line1.X2 = 10.90158F;
            this.line1.Y1 = 0.7118111F;
            this.line1.Y2 = 0.7118111F;
            // 
            // textBox2
            // 
            this.textBox2.DataField = "ITEM050";
            this.textBox2.Height = 0.1574803F;
            this.textBox2.Left = 5.66929F;
            this.textBox2.MultiLine = false;
            this.textBox2.Name = "textBox2";
            this.textBox2.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: bold; text-align: center; ver" +
    "tical-align: bottom; ddo-char-set: 128";
            this.textBox2.Text = "ITEM050";
            this.textBox2.Top = 1.023622F;
            this.textBox2.Width = 0.7480313F;
            // 
            // textBox3
            // 
            this.textBox3.DataField = "ITEM062";
            this.textBox3.Height = 0.1574803F;
            this.textBox3.Left = 5.66929F;
            this.textBox3.MultiLine = false;
            this.textBox3.Name = "textBox3";
            this.textBox3.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: bold; text-align: center; ver" +
    "tical-align: bottom; ddo-char-set: 128";
            this.textBox3.Text = "ITEM062";
            this.textBox3.Top = 1.184252F;
            this.textBox3.Width = 0.7480313F;
            // 
            // textBox4
            // 
            this.textBox4.DataField = "ITEM061";
            this.textBox4.Height = 0.1574803F;
            this.textBox4.Left = 4.921258F;
            this.textBox4.MultiLine = false;
            this.textBox4.Name = "textBox4";
            this.textBox4.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: bold; text-align: center; ver" +
    "tical-align: bottom; ddo-char-set: 128";
            this.textBox4.Text = "ITEM061";
            this.textBox4.Top = 1.184252F;
            this.textBox4.Width = 0.7480313F;
            // 
            // textBox5
            // 
            this.textBox5.DataField = "ITEM049";
            this.textBox5.Height = 0.1574803F;
            this.textBox5.Left = 4.921258F;
            this.textBox5.MultiLine = false;
            this.textBox5.Name = "textBox5";
            this.textBox5.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: bold; text-align: center; ver" +
    "tical-align: bottom; ddo-char-set: 128";
            this.textBox5.Text = "ITEM049";
            this.textBox5.Top = 1.023622F;
            this.textBox5.Width = 0.7480313F;
            // 
            // textBox6
            // 
            this.textBox6.DataField = "ITEM048";
            this.textBox6.Height = 0.1574803F;
            this.textBox6.Left = 4.15118F;
            this.textBox6.MultiLine = false;
            this.textBox6.Name = "textBox6";
            this.textBox6.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: bold; text-align: center; ver" +
    "tical-align: bottom; ddo-char-set: 128";
            this.textBox6.Text = "ITEM048";
            this.textBox6.Top = 1.023622F;
            this.textBox6.Width = 0.7480313F;
            // 
            // textBox7
            // 
            this.textBox7.DataField = "ITEM060";
            this.textBox7.Height = 0.1574803F;
            this.textBox7.Left = 4.15118F;
            this.textBox7.MultiLine = false;
            this.textBox7.Name = "textBox7";
            this.textBox7.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: bold; text-align: center; ver" +
    "tical-align: bottom; ddo-char-set: 128";
            this.textBox7.Text = "ITEM060";
            this.textBox7.Top = 1.184252F;
            this.textBox7.Width = 0.7480313F;
            // 
            // textBox8
            // 
            this.textBox8.DataField = "ITEM059";
            this.textBox8.Height = 0.1574803F;
            this.textBox8.Left = 3.403149F;
            this.textBox8.MultiLine = false;
            this.textBox8.Name = "textBox8";
            this.textBox8.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: bold; text-align: center; ver" +
    "tical-align: bottom; ddo-char-set: 128";
            this.textBox8.Text = "ITEM059";
            this.textBox8.Top = 1.184252F;
            this.textBox8.Width = 0.7480313F;
            // 
            // textBox9
            // 
            this.textBox9.DataField = "ITEM047";
            this.textBox9.Height = 0.1574803F;
            this.textBox9.Left = 3.403149F;
            this.textBox9.MultiLine = false;
            this.textBox9.Name = "textBox9";
            this.textBox9.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: bold; text-align: center; ver" +
    "tical-align: bottom; ddo-char-set: 128";
            this.textBox9.Text = "ITEM047";
            this.textBox9.Top = 1.026772F;
            this.textBox9.Width = 0.7480313F;
            // 
            // textBox10
            // 
            this.textBox10.DataField = "ITEM046";
            this.textBox10.Height = 0.1574803F;
            this.textBox10.Left = 2.655118F;
            this.textBox10.MultiLine = false;
            this.textBox10.Name = "textBox10";
            this.textBox10.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: bold; text-align: center; ver" +
    "tical-align: bottom; ddo-char-set: 128";
            this.textBox10.Text = "ITEM046";
            this.textBox10.Top = 1.023622F;
            this.textBox10.Width = 0.7480313F;
            // 
            // textBox11
            // 
            this.textBox11.DataField = "ITEM058";
            this.textBox11.Height = 0.1574803F;
            this.textBox11.Left = 2.655118F;
            this.textBox11.MultiLine = false;
            this.textBox11.Name = "textBox11";
            this.textBox11.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: bold; text-align: center; ver" +
    "tical-align: bottom; ddo-char-set: 128";
            this.textBox11.Text = "ITEM058";
            this.textBox11.Top = 1.184252F;
            this.textBox11.Width = 0.7480313F;
            // 
            // textBox12
            // 
            this.textBox12.DataField = "ITEM057";
            this.textBox12.Height = 0.1574803F;
            this.textBox12.Left = 1.907087F;
            this.textBox12.MultiLine = false;
            this.textBox12.Name = "textBox12";
            this.textBox12.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: bold; text-align: center; ver" +
    "tical-align: bottom; ddo-char-set: 128";
            this.textBox12.Text = "ITEM057";
            this.textBox12.Top = 1.184252F;
            this.textBox12.Width = 0.7480313F;
            // 
            // textBox13
            // 
            this.textBox13.DataField = "ITEM045";
            this.textBox13.Height = 0.1574804F;
            this.textBox13.Left = 1.907087F;
            this.textBox13.MultiLine = false;
            this.textBox13.Name = "textBox13";
            this.textBox13.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: bold; text-align: center; ver" +
    "tical-align: bottom; ddo-char-set: 128";
            this.textBox13.Text = "ITEM045";
            this.textBox13.Top = 1.026772F;
            this.textBox13.Width = 0.7480313F;
            // 
            // textBox14
            // 
            this.textBox14.DataField = "ITEM063";
            this.textBox14.Height = 0.1574803F;
            this.textBox14.Left = 6.417321F;
            this.textBox14.MultiLine = false;
            this.textBox14.Name = "textBox14";
            this.textBox14.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: bold; text-align: center; ver" +
    "tical-align: bottom; ddo-char-set: 128";
            this.textBox14.Text = "ITEM063";
            this.textBox14.Top = 1.184252F;
            this.textBox14.Width = 0.7480313F;
            // 
            // textBox15
            // 
            this.textBox15.DataField = "ITEM051";
            this.textBox15.Height = 0.1574803F;
            this.textBox15.Left = 6.417321F;
            this.textBox15.MultiLine = false;
            this.textBox15.Name = "textBox15";
            this.textBox15.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: bold; text-align: center; ver" +
    "tical-align: bottom; ddo-char-set: 128";
            this.textBox15.Text = "ITEM051";
            this.textBox15.Top = 1.023622F;
            this.textBox15.Width = 0.7480313F;
            // 
            // textBox16
            // 
            this.textBox16.DataField = "ITEM052";
            this.textBox16.Height = 0.1574803F;
            this.textBox16.Left = 7.165353F;
            this.textBox16.MultiLine = false;
            this.textBox16.Name = "textBox16";
            this.textBox16.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: bold; text-align: center; ver" +
    "tical-align: bottom; ddo-char-set: 128";
            this.textBox16.Text = "ITEM052";
            this.textBox16.Top = 1.023622F;
            this.textBox16.Width = 0.7480313F;
            // 
            // textBox17
            // 
            this.textBox17.DataField = "ITEM064";
            this.textBox17.Height = 0.1574803F;
            this.textBox17.Left = 7.165353F;
            this.textBox17.MultiLine = false;
            this.textBox17.Name = "textBox17";
            this.textBox17.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: bold; text-align: center; ver" +
    "tical-align: bottom; ddo-char-set: 128";
            this.textBox17.Text = "ITEM064";
            this.textBox17.Top = 1.184252F;
            this.textBox17.Width = 0.7480313F;
            // 
            // textBox18
            // 
            this.textBox18.DataField = "ITEM053";
            this.textBox18.Height = 0.1574803F;
            this.textBox18.Left = 7.913387F;
            this.textBox18.MultiLine = false;
            this.textBox18.Name = "textBox18";
            this.textBox18.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: bold; text-align: center; ver" +
    "tical-align: bottom; ddo-char-set: 128";
            this.textBox18.Text = "ITEM053";
            this.textBox18.Top = 1.023622F;
            this.textBox18.Width = 0.7480313F;
            // 
            // textBox19
            // 
            this.textBox19.DataField = "ITEM065";
            this.textBox19.Height = 0.1574803F;
            this.textBox19.Left = 7.913387F;
            this.textBox19.MultiLine = false;
            this.textBox19.Name = "textBox19";
            this.textBox19.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: bold; text-align: center; ver" +
    "tical-align: bottom; ddo-char-set: 128";
            this.textBox19.Text = "ITEM065";
            this.textBox19.Top = 1.184252F;
            this.textBox19.Width = 0.7480313F;
            // 
            // textBox20
            // 
            this.textBox20.DataField = "ITEM054";
            this.textBox20.Height = 0.1574803F;
            this.textBox20.Left = 8.661415F;
            this.textBox20.MultiLine = false;
            this.textBox20.Name = "textBox20";
            this.textBox20.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: bold; text-align: center; ver" +
    "tical-align: bottom; ddo-char-set: 128";
            this.textBox20.Text = "ITEM054";
            this.textBox20.Top = 1.023622F;
            this.textBox20.Width = 0.7480313F;
            // 
            // textBox21
            // 
            this.textBox21.DataField = "ITEM066";
            this.textBox21.Height = 0.1574803F;
            this.textBox21.Left = 8.661415F;
            this.textBox21.MultiLine = false;
            this.textBox21.Name = "textBox21";
            this.textBox21.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: bold; text-align: center; ver" +
    "tical-align: bottom; ddo-char-set: 128";
            this.textBox21.Text = "ITEM066";
            this.textBox21.Top = 1.184252F;
            this.textBox21.Width = 0.7480313F;
            // 
            // textBox22
            // 
            this.textBox22.DataField = "ITEM055";
            this.textBox22.Height = 0.1574803F;
            this.textBox22.Left = 9.409452F;
            this.textBox22.MultiLine = false;
            this.textBox22.Name = "textBox22";
            this.textBox22.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: bold; text-align: center; ver" +
    "tical-align: bottom; ddo-char-set: 128";
            this.textBox22.Text = "ITEM055";
            this.textBox22.Top = 1.023622F;
            this.textBox22.Width = 0.7480313F;
            // 
            // textBox23
            // 
            this.textBox23.DataField = "ITEM067";
            this.textBox23.Height = 0.1574803F;
            this.textBox23.Left = 9.409452F;
            this.textBox23.MultiLine = false;
            this.textBox23.Name = "textBox23";
            this.textBox23.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: bold; text-align: center; ver" +
    "tical-align: bottom; ddo-char-set: 128";
            this.textBox23.Text = "ITEM067";
            this.textBox23.Top = 1.184252F;
            this.textBox23.Width = 0.7480313F;
            // 
            // textBox24
            // 
            this.textBox24.DataField = "ITEM056";
            this.textBox24.Height = 0.1574803F;
            this.textBox24.Left = 10.15748F;
            this.textBox24.MultiLine = false;
            this.textBox24.Name = "textBox24";
            this.textBox24.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: bold; text-align: center; ver" +
    "tical-align: bottom; ddo-char-set: 128";
            this.textBox24.Text = "ITEM056";
            this.textBox24.Top = 1.023622F;
            this.textBox24.Width = 0.7480313F;
            // 
            // textBox25
            // 
            this.textBox25.DataField = "ITEM068";
            this.textBox25.Height = 0.1574803F;
            this.textBox25.Left = 10.15748F;
            this.textBox25.MultiLine = false;
            this.textBox25.Name = "textBox25";
            this.textBox25.Style = "font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: bold; text-align: center; ver" +
    "tical-align: bottom; ddo-char-set: 128";
            this.textBox25.Text = "ITEM068";
            this.textBox25.Top = 1.184252F;
            this.textBox25.Width = 0.7480313F;
            // 
            // line7
            // 
            this.line7.Height = 0.6267719F;
            this.line7.Left = 0.4582677F;
            this.line7.LineWeight = 1F;
            this.line7.Name = "line7";
            this.line7.Top = 0.7118111F;
            this.line7.Width = 0F;
            this.line7.X1 = 0.4582677F;
            this.line7.X2 = 0.4582677F;
            this.line7.Y1 = 0.7118111F;
            this.line7.Y2 = 1.338583F;
            // 
            // line6
            // 
            this.line6.Height = 0.6267719F;
            this.line6.Left = 1.889764F;
            this.line6.LineWeight = 1F;
            this.line6.Name = "line6";
            this.line6.Top = 0.7118111F;
            this.line6.Width = 0F;
            this.line6.X1 = 1.889764F;
            this.line6.X2 = 1.889764F;
            this.line6.Y1 = 0.7118111F;
            this.line6.Y2 = 1.338583F;
            // 
            // line27
            // 
            this.line27.Height = 0.6267719F;
            this.line27.Left = 2.655117F;
            this.line27.LineWeight = 1F;
            this.line27.Name = "line27";
            this.line27.Top = 0.7118111F;
            this.line27.Width = 9.536743E-07F;
            this.line27.X1 = 2.655118F;
            this.line27.X2 = 2.655117F;
            this.line27.Y1 = 0.7118111F;
            this.line27.Y2 = 1.338583F;
            // 
            // line8
            // 
            this.line8.Height = 0.6267719F;
            this.line8.Left = 3.403142F;
            this.line8.LineWeight = 1F;
            this.line8.Name = "line8";
            this.line8.Top = 0.7118111F;
            this.line8.Width = 8.106232E-06F;
            this.line8.X1 = 3.40315F;
            this.line8.X2 = 3.403142F;
            this.line8.Y1 = 0.7118111F;
            this.line8.Y2 = 1.338583F;
            // 
            // line5
            // 
            this.line5.Height = 0.6267719F;
            this.line5.Left = 4.151181F;
            this.line5.LineWeight = 1F;
            this.line5.Name = "line5";
            this.line5.Top = 0.7118111F;
            this.line5.Width = 0F;
            this.line5.X1 = 4.151181F;
            this.line5.X2 = 4.151181F;
            this.line5.Y1 = 0.7118111F;
            this.line5.Y2 = 1.338583F;
            // 
            // line4
            // 
            this.line4.Height = 0.6267719F;
            this.line4.Left = 4.92126F;
            this.line4.LineWeight = 1F;
            this.line4.Name = "line4";
            this.line4.Top = 0.7118111F;
            this.line4.Width = 0F;
            this.line4.X1 = 4.92126F;
            this.line4.X2 = 4.92126F;
            this.line4.Y1 = 0.7118111F;
            this.line4.Y2 = 1.338583F;
            // 
            // line3
            // 
            this.line3.Height = 0.6267719F;
            this.line3.Left = 5.669291F;
            this.line3.LineWeight = 1F;
            this.line3.Name = "line3";
            this.line3.Top = 0.7118111F;
            this.line3.Width = 9.536743E-07F;
            this.line3.X1 = 5.669292F;
            this.line3.X2 = 5.669291F;
            this.line3.Y1 = 0.7118111F;
            this.line3.Y2 = 1.338583F;
            // 
            // line21
            // 
            this.line21.Height = 0.6267719F;
            this.line21.Left = 6.417323F;
            this.line21.LineWeight = 1F;
            this.line21.Name = "line21";
            this.line21.Top = 0.7118111F;
            this.line21.Width = 0F;
            this.line21.X1 = 6.417323F;
            this.line21.X2 = 6.417323F;
            this.line21.Y1 = 0.7118111F;
            this.line21.Y2 = 1.338583F;
            // 
            // line22
            // 
            this.line22.Height = 0.6267719F;
            this.line22.Left = 7.165355F;
            this.line22.LineWeight = 1F;
            this.line22.Name = "line22";
            this.line22.Top = 0.7118111F;
            this.line22.Width = 0F;
            this.line22.X1 = 7.165355F;
            this.line22.X2 = 7.165355F;
            this.line22.Y1 = 0.7118111F;
            this.line22.Y2 = 1.338583F;
            // 
            // line23
            // 
            this.line23.Height = 0.6267719F;
            this.line23.Left = 7.913386F;
            this.line23.LineWeight = 1F;
            this.line23.Name = "line23";
            this.line23.Top = 0.7118111F;
            this.line23.Width = 0F;
            this.line23.X1 = 7.913386F;
            this.line23.X2 = 7.913386F;
            this.line23.Y1 = 0.7118111F;
            this.line23.Y2 = 1.338583F;
            // 
            // line24
            // 
            this.line24.Height = 0.6267719F;
            this.line24.Left = 8.661416F;
            this.line24.LineWeight = 1F;
            this.line24.Name = "line24";
            this.line24.Top = 0.7118111F;
            this.line24.Width = 1.907349E-06F;
            this.line24.X1 = 8.661418F;
            this.line24.X2 = 8.661416F;
            this.line24.Y1 = 0.7118111F;
            this.line24.Y2 = 1.338583F;
            // 
            // line25
            // 
            this.line25.Height = 0.6267719F;
            this.line25.Left = 9.409449F;
            this.line25.LineWeight = 1F;
            this.line25.Name = "line25";
            this.line25.Top = 0.7118111F;
            this.line25.Width = 9.536743E-07F;
            this.line25.X1 = 9.40945F;
            this.line25.X2 = 9.409449F;
            this.line25.Y1 = 0.7118111F;
            this.line25.Y2 = 1.338583F;
            // 
            // line26
            // 
            this.line26.Height = 0.6267719F;
            this.line26.Left = 10.15748F;
            this.line26.LineWeight = 1F;
            this.line26.Name = "line26";
            this.line26.Top = 0.7118111F;
            this.line26.Width = 0F;
            this.line26.X1 = 10.15748F;
            this.line26.X2 = 10.15748F;
            this.line26.Y1 = 0.7118111F;
            this.line26.Y2 = 1.338583F;
            // 
            // line10
            // 
            this.line10.Height = 0.6299216F;
            this.line10.Left = 10.90158F;
            this.line10.LineWeight = 1F;
            this.line10.Name = "line10";
            this.line10.Top = 0.7086614F;
            this.line10.Width = 0F;
            this.line10.X1 = 10.90158F;
            this.line10.X2 = 10.90158F;
            this.line10.Y1 = 0.7086614F;
            this.line10.Y2 = 1.338583F;
            // 
            // line13
            // 
            this.line13.Height = 0F;
            this.line13.Left = 1.889764F;
            this.line13.LineWeight = 1F;
            this.line13.Name = "line13";
            this.line13.Top = 1.023622F;
            this.line13.Width = 9.011816F;
            this.line13.X1 = 1.889764F;
            this.line13.X2 = 10.90158F;
            this.line13.Y1 = 1.023622F;
            this.line13.Y2 = 1.023622F;
            // 
            // line14
            // 
            this.line14.Height = 0F;
            this.line14.Left = 1.889764F;
            this.line14.LineWeight = 1F;
            this.line14.Name = "line14";
            this.line14.Top = 1.184252F;
            this.line14.Width = 9.011816F;
            this.line14.X1 = 1.889764F;
            this.line14.X2 = 10.90158F;
            this.line14.Y1 = 1.184252F;
            this.line14.Y2 = 1.184252F;
            // 
            // line9
            // 
            this.line9.Height = 0F;
            this.line9.Left = 0F;
            this.line9.LineWeight = 1F;
            this.line9.Name = "line9";
            this.line9.Top = 1.338583F;
            this.line9.Width = 10.90158F;
            this.line9.X1 = 0F;
            this.line9.X2 = 10.90158F;
            this.line9.Y1 = 1.338583F;
            this.line9.Y2 = 1.338583F;
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtVal10,
            this.txtVal22,
            this.txtVal21,
            this.txtVal09,
            this.txtVal08,
            this.txtVal20,
            this.txtVal19,
            this.txtVal07,
            this.txtVal06,
            this.txtVal18,
            this.txtVal17,
            this.txtVal05,
            this.txtVal23,
            this.txtVal11,
            this.txtVal02,
            this.txtVal01,
            this.line28,
            this.txtVal12,
            this.txtVal24,
            this.txtVal13,
            this.txtVal25,
            this.txtVal14,
            this.txtVal26,
            this.txtVal15,
            this.txtVal27,
            this.txtVal16,
            this.txtVal28,
            this.line36,
            this.line11,
            this.txtVal03,
            this.textBox74,
            this.line40,
            this.textBox26,
            this.textBox27,
            this.textBox28,
            this.textBox29,
            this.textBox30,
            this.textBox31,
            this.textBox32,
            this.textBox33,
            this.textBox34,
            this.textBox35,
            this.textBox36,
            this.textBox37,
            this.textBox38,
            this.textBox39,
            this.textBox40,
            this.textBox41,
            this.textBox42,
            this.textBox43,
            this.textBox44,
            this.textBox45,
            this.textBox46,
            this.textBox47,
            this.textBox48,
            this.textBox49,
            this.line12,
            this.line32,
            this.line33,
            this.line34,
            this.line35,
            this.line37,
            this.line31,
            this.line30,
            this.line29,
            this.line18,
            this.line39,
            this.line38,
            this.line20,
            this.line15,
            this.line16});
            this.detail.Height = 0.6393701F;
            this.detail.Name = "detail";
            this.detail.Format += new System.EventHandler(this.detail_Format);
            // 
            // txtVal10
            // 
            this.txtVal10.DataField = "ITEM076";
            this.txtVal10.Height = 0.1574803F;
            this.txtVal10.Left = 5.669295F;
            this.txtVal10.MultiLine = false;
            this.txtVal10.Name = "txtVal10";
            this.txtVal10.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: right; verti" +
    "cal-align: bottom; ddo-char-set: 128";
            this.txtVal10.Text = "ITEM076";
            this.txtVal10.Top = 0F;
            this.txtVal10.Width = 0.7480315F;
            // 
            // txtVal22
            // 
            this.txtVal22.DataField = "ITEM088";
            this.txtVal22.Height = 0.1574803F;
            this.txtVal22.Left = 5.669292F;
            this.txtVal22.MultiLine = false;
            this.txtVal22.Name = "txtVal22";
            this.txtVal22.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: right; verti" +
    "cal-align: bottom; ddo-char-set: 128";
            this.txtVal22.Text = "ITEM088";
            this.txtVal22.Top = 0.1606306F;
            this.txtVal22.Width = 0.7480315F;
            // 
            // txtVal21
            // 
            this.txtVal21.DataField = "ITEM087";
            this.txtVal21.Height = 0.1574803F;
            this.txtVal21.Left = 4.92126F;
            this.txtVal21.MultiLine = false;
            this.txtVal21.Name = "txtVal21";
            this.txtVal21.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: right; verti" +
    "cal-align: bottom; ddo-char-set: 128";
            this.txtVal21.Text = "ITEM087";
            this.txtVal21.Top = 0.1606306F;
            this.txtVal21.Width = 0.7480315F;
            // 
            // txtVal09
            // 
            this.txtVal09.DataField = "ITEM075";
            this.txtVal09.Height = 0.1574803F;
            this.txtVal09.Left = 4.92126F;
            this.txtVal09.MultiLine = false;
            this.txtVal09.Name = "txtVal09";
            this.txtVal09.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: right; verti" +
    "cal-align: bottom; ddo-char-set: 128";
            this.txtVal09.Text = "ITEM075";
            this.txtVal09.Top = 0F;
            this.txtVal09.Width = 0.7480315F;
            // 
            // txtVal08
            // 
            this.txtVal08.DataField = "ITEM074";
            this.txtVal08.Height = 0.1574803F;
            this.txtVal08.Left = 4.151181F;
            this.txtVal08.MultiLine = false;
            this.txtVal08.Name = "txtVal08";
            this.txtVal08.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: right; verti" +
    "cal-align: bottom; ddo-char-set: 128";
            this.txtVal08.Text = "ITEM074";
            this.txtVal08.Top = 0F;
            this.txtVal08.Width = 0.7480315F;
            // 
            // txtVal20
            // 
            this.txtVal20.DataField = "ITEM086";
            this.txtVal20.Height = 0.1574803F;
            this.txtVal20.Left = 4.151181F;
            this.txtVal20.MultiLine = false;
            this.txtVal20.Name = "txtVal20";
            this.txtVal20.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: right; verti" +
    "cal-align: bottom; ddo-char-set: 128";
            this.txtVal20.Text = "ITEM086";
            this.txtVal20.Top = 0.1606306F;
            this.txtVal20.Width = 0.7480315F;
            // 
            // txtVal19
            // 
            this.txtVal19.DataField = "ITEM085";
            this.txtVal19.Height = 0.1574803F;
            this.txtVal19.Left = 3.40315F;
            this.txtVal19.MultiLine = false;
            this.txtVal19.Name = "txtVal19";
            this.txtVal19.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: right; verti" +
    "cal-align: bottom; ddo-char-set: 128";
            this.txtVal19.Text = "ITEM085";
            this.txtVal19.Top = 0.1574806F;
            this.txtVal19.Width = 0.7480315F;
            // 
            // txtVal07
            // 
            this.txtVal07.DataField = "ITEM073";
            this.txtVal07.Height = 0.1574803F;
            this.txtVal07.Left = 3.40315F;
            this.txtVal07.MultiLine = false;
            this.txtVal07.Name = "txtVal07";
            this.txtVal07.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: right; verti" +
    "cal-align: bottom; ddo-char-set: 128";
            this.txtVal07.Text = "ITEM073";
            this.txtVal07.Top = 0F;
            this.txtVal07.Width = 0.7480315F;
            // 
            // txtVal06
            // 
            this.txtVal06.DataField = "ITEM072";
            this.txtVal06.Height = 0.1574803F;
            this.txtVal06.Left = 2.637795F;
            this.txtVal06.MultiLine = false;
            this.txtVal06.Name = "txtVal06";
            this.txtVal06.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: right; verti" +
    "cal-align: bottom; ddo-char-set: 128";
            this.txtVal06.Text = "ITEM072";
            this.txtVal06.Top = 0F;
            this.txtVal06.Width = 0.7480315F;
            // 
            // txtVal18
            // 
            this.txtVal18.DataField = "ITEM084";
            this.txtVal18.Height = 0.1574803F;
            this.txtVal18.Left = 2.637796F;
            this.txtVal18.MultiLine = false;
            this.txtVal18.Name = "txtVal18";
            this.txtVal18.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: right; verti" +
    "cal-align: bottom; ddo-char-set: 128";
            this.txtVal18.Text = "ITEM084";
            this.txtVal18.Top = 0.1606306F;
            this.txtVal18.Width = 0.7480315F;
            // 
            // txtVal17
            // 
            this.txtVal17.DataField = "ITEM083";
            this.txtVal17.Height = 0.1574803F;
            this.txtVal17.Left = 1.889764F;
            this.txtVal17.MultiLine = false;
            this.txtVal17.Name = "txtVal17";
            this.txtVal17.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: right; verti" +
    "cal-align: bottom; ddo-char-set: 128";
            this.txtVal17.Text = "ITEM083";
            this.txtVal17.Top = 0.1574803F;
            this.txtVal17.Width = 0.7480315F;
            // 
            // txtVal05
            // 
            this.txtVal05.DataField = "ITEM071";
            this.txtVal05.Height = 0.1574803F;
            this.txtVal05.Left = 1.889764F;
            this.txtVal05.MultiLine = false;
            this.txtVal05.Name = "txtVal05";
            this.txtVal05.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: right; verti" +
    "cal-align: bottom; ddo-char-set: 128";
            this.txtVal05.Text = "ITEM071";
            this.txtVal05.Top = 0F;
            this.txtVal05.Width = 0.7480315F;
            // 
            // txtVal23
            // 
            this.txtVal23.DataField = "ITEM089";
            this.txtVal23.Height = 0.1574803F;
            this.txtVal23.Left = 6.417323F;
            this.txtVal23.MultiLine = false;
            this.txtVal23.Name = "txtVal23";
            this.txtVal23.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: right; verti" +
    "cal-align: bottom; ddo-char-set: 128";
            this.txtVal23.Text = "ITEM089";
            this.txtVal23.Top = 0.1606306F;
            this.txtVal23.Width = 0.7480315F;
            // 
            // txtVal11
            // 
            this.txtVal11.DataField = "ITEM077";
            this.txtVal11.Height = 0.1574803F;
            this.txtVal11.Left = 6.417326F;
            this.txtVal11.MultiLine = false;
            this.txtVal11.Name = "txtVal11";
            this.txtVal11.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: right; verti" +
    "cal-align: bottom; ddo-char-set: 128";
            this.txtVal11.Text = "ITEM077";
            this.txtVal11.Top = 0F;
            this.txtVal11.Width = 0.7480315F;
            // 
            // txtVal02
            // 
            this.txtVal02.DataField = "ITEM008";
            this.txtVal02.Height = 0.3905515F;
            this.txtVal02.Left = 0.4582677F;
            this.txtVal02.MultiLine = false;
            this.txtVal02.Name = "txtVal02";
            this.txtVal02.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: left; vertic" +
    "al-align: middle; ddo-char-set: 128";
            this.txtVal02.Text = "ITEM008";
            this.txtVal02.Top = 0.003149331F;
            this.txtVal02.Width = 1.431496F;
            // 
            // txtVal01
            // 
            this.txtVal01.DataField = "ITEM010";
            this.txtVal01.Height = 0.626772F;
            this.txtVal01.Left = 0F;
            this.txtVal01.MultiLine = false;
            this.txtVal01.Name = "txtVal01";
            this.txtVal01.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: right; verti" +
    "cal-align: middle; ddo-char-set: 128";
            this.txtVal01.Text = "ITEM010";
            this.txtVal01.Top = 0.003149331F;
            this.txtVal01.Width = 0.4582677F;
            // 
            // line28
            // 
            this.line28.Height = 0.6299213F;
            this.line28.Left = 0.4582677F;
            this.line28.LineWeight = 1F;
            this.line28.Name = "line28";
            this.line28.Top = 0F;
            this.line28.Width = 0F;
            this.line28.X1 = 0.4582677F;
            this.line28.X2 = 0.4582677F;
            this.line28.Y1 = 0F;
            this.line28.Y2 = 0.6299213F;
            // 
            // txtVal12
            // 
            this.txtVal12.DataField = "ITEM078";
            this.txtVal12.Height = 0.1574803F;
            this.txtVal12.Left = 7.165358F;
            this.txtVal12.MultiLine = false;
            this.txtVal12.Name = "txtVal12";
            this.txtVal12.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: right; verti" +
    "cal-align: bottom; ddo-char-set: 128";
            this.txtVal12.Text = "ITEM078";
            this.txtVal12.Top = 0F;
            this.txtVal12.Width = 0.7480315F;
            // 
            // txtVal24
            // 
            this.txtVal24.DataField = "ITEM090";
            this.txtVal24.Height = 0.1574803F;
            this.txtVal24.Left = 7.165355F;
            this.txtVal24.MultiLine = false;
            this.txtVal24.Name = "txtVal24";
            this.txtVal24.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: right; verti" +
    "cal-align: bottom; ddo-char-set: 128";
            this.txtVal24.Text = "ITEM090";
            this.txtVal24.Top = 0.1606306F;
            this.txtVal24.Width = 0.7480315F;
            // 
            // txtVal13
            // 
            this.txtVal13.DataField = "ITEM079";
            this.txtVal13.Height = 0.1574803F;
            this.txtVal13.Left = 7.913389F;
            this.txtVal13.MultiLine = false;
            this.txtVal13.Name = "txtVal13";
            this.txtVal13.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: right; verti" +
    "cal-align: bottom; ddo-char-set: 128";
            this.txtVal13.Text = "ITEM079";
            this.txtVal13.Top = 0F;
            this.txtVal13.Width = 0.7480315F;
            // 
            // txtVal25
            // 
            this.txtVal25.DataField = "ITEM091";
            this.txtVal25.Height = 0.1574803F;
            this.txtVal25.Left = 7.913386F;
            this.txtVal25.MultiLine = false;
            this.txtVal25.Name = "txtVal25";
            this.txtVal25.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: right; verti" +
    "cal-align: bottom; ddo-char-set: 128";
            this.txtVal25.Text = "ITEM091";
            this.txtVal25.Top = 0.1606306F;
            this.txtVal25.Width = 0.7480315F;
            // 
            // txtVal14
            // 
            this.txtVal14.DataField = "ITEM080";
            this.txtVal14.Height = 0.1574803F;
            this.txtVal14.Left = 8.661421F;
            this.txtVal14.MultiLine = false;
            this.txtVal14.Name = "txtVal14";
            this.txtVal14.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: right; verti" +
    "cal-align: bottom; ddo-char-set: 128";
            this.txtVal14.Text = "ITEM080";
            this.txtVal14.Top = 0F;
            this.txtVal14.Width = 0.7480315F;
            // 
            // txtVal26
            // 
            this.txtVal26.DataField = "ITEM092";
            this.txtVal26.Height = 0.1574803F;
            this.txtVal26.Left = 8.661418F;
            this.txtVal26.MultiLine = false;
            this.txtVal26.Name = "txtVal26";
            this.txtVal26.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: right; verti" +
    "cal-align: bottom; ddo-char-set: 128";
            this.txtVal26.Text = "ITEM092";
            this.txtVal26.Top = 0.1606306F;
            this.txtVal26.Width = 0.7480315F;
            // 
            // txtVal15
            // 
            this.txtVal15.DataField = "ITEM081";
            this.txtVal15.Height = 0.1574803F;
            this.txtVal15.Left = 9.409449F;
            this.txtVal15.MultiLine = false;
            this.txtVal15.Name = "txtVal15";
            this.txtVal15.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: right; verti" +
    "cal-align: bottom; ddo-char-set: 128";
            this.txtVal15.Text = "ITEM081";
            this.txtVal15.Top = 0F;
            this.txtVal15.Width = 0.7480315F;
            // 
            // txtVal27
            // 
            this.txtVal27.DataField = "ITEM093";
            this.txtVal27.Height = 0.1574803F;
            this.txtVal27.Left = 9.40945F;
            this.txtVal27.MultiLine = false;
            this.txtVal27.Name = "txtVal27";
            this.txtVal27.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: right; verti" +
    "cal-align: bottom; ddo-char-set: 128";
            this.txtVal27.Text = "ITEM093";
            this.txtVal27.Top = 0.1606306F;
            this.txtVal27.Width = 0.7480315F;
            // 
            // txtVal16
            // 
            this.txtVal16.DataField = "ITEM082";
            this.txtVal16.Height = 0.1574803F;
            this.txtVal16.Left = 10.15748F;
            this.txtVal16.MultiLine = false;
            this.txtVal16.Name = "txtVal16";
            this.txtVal16.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: right; verti" +
    "cal-align: bottom; ddo-char-set: 128";
            this.txtVal16.Text = "ITEM082";
            this.txtVal16.Top = 0F;
            this.txtVal16.Width = 0.7480315F;
            // 
            // txtVal28
            // 
            this.txtVal28.DataField = "ITEM094";
            this.txtVal28.Height = 0.1574803F;
            this.txtVal28.Left = 10.15748F;
            this.txtVal28.MultiLine = false;
            this.txtVal28.Name = "txtVal28";
            this.txtVal28.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: right; verti" +
    "cal-align: bottom; ddo-char-set: 128";
            this.txtVal28.Text = "ITEM094";
            this.txtVal28.Top = 0.1606306F;
            this.txtVal28.Width = 0.7480315F;
            // 
            // line36
            // 
            this.line36.Height = 0F;
            this.line36.Left = 1.889764F;
            this.line36.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.line36.LineWeight = 1F;
            this.line36.Name = "line36";
            this.line36.Top = 0.1574803F;
            this.line36.Width = 9.011816F;
            this.line36.X1 = 1.889764F;
            this.line36.X2 = 10.90158F;
            this.line36.Y1 = 0.1574803F;
            this.line36.Y2 = 0.1574803F;
            // 
            // line11
            // 
            this.line11.Height = 0.6299213F;
            this.line11.Left = 0F;
            this.line11.LineWeight = 1F;
            this.line11.Name = "line11";
            this.line11.Top = 0F;
            this.line11.Width = 9.536743E-07F;
            this.line11.X1 = 9.536743E-07F;
            this.line11.X2 = 0F;
            this.line11.Y1 = 0F;
            this.line11.Y2 = 0.6299213F;
            // 
            // txtVal03
            // 
            this.txtVal03.DataField = "ITEM011";
            this.txtVal03.Height = 0.1574803F;
            this.txtVal03.Left = 0.8137796F;
            this.txtVal03.MultiLine = false;
            this.txtVal03.Name = "txtVal03";
            this.txtVal03.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: center; vert" +
    "ical-align: middle; ddo-char-set: 128";
            this.txtVal03.Text = "ITEM011";
            this.txtVal03.Top = 0.472441F;
            this.txtVal03.Width = 0.4862205F;
            // 
            // textBox74
            // 
            this.textBox74.DataField = "ITEM012";
            this.textBox74.Height = 0.1574803F;
            this.textBox74.Left = 1.027559F;
            this.textBox74.MultiLine = false;
            this.textBox74.Name = "textBox74";
            this.textBox74.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: left; vertic" +
    "al-align: middle; ddo-char-set: 128";
            this.textBox74.Text = "ITEM012";
            this.textBox74.Top = 0.472441F;
            this.textBox74.Width = 0.7874017F;
            // 
            // line40
            // 
            this.line40.Height = 0F;
            this.line40.Left = 0F;
            this.line40.LineWeight = 1F;
            this.line40.Name = "line40";
            this.line40.Top = 0.6299213F;
            this.line40.Width = 10.90158F;
            this.line40.X1 = 0F;
            this.line40.X2 = 10.90158F;
            this.line40.Y1 = 0.6299213F;
            this.line40.Y2 = 0.6299213F;
            // 
            // textBox26
            // 
            this.textBox26.DataField = "ITEM100";
            this.textBox26.Height = 0.1574803F;
            this.textBox26.Left = 5.669296F;
            this.textBox26.MultiLine = false;
            this.textBox26.Name = "textBox26";
            this.textBox26.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: right; verti" +
    "cal-align: bottom; ddo-char-set: 128";
            this.textBox26.Text = "ITEM100";
            this.textBox26.Top = 0.3181103F;
            this.textBox26.Width = 0.7480313F;
            // 
            // textBox27
            // 
            this.textBox27.DataField = "ITEM112";
            this.textBox27.Height = 0.1574803F;
            this.textBox27.Left = 5.669292F;
            this.textBox27.MultiLine = false;
            this.textBox27.Name = "textBox27";
            this.textBox27.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: right; verti" +
    "cal-align: bottom; ddo-char-set: 128";
            this.textBox27.Text = "ITEM112";
            this.textBox27.Top = 0.4787409F;
            this.textBox27.Width = 0.7480313F;
            // 
            // textBox28
            // 
            this.textBox28.DataField = "ITEM111";
            this.textBox28.Height = 0.1574803F;
            this.textBox28.Left = 4.92126F;
            this.textBox28.MultiLine = false;
            this.textBox28.Name = "textBox28";
            this.textBox28.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: right; verti" +
    "cal-align: bottom; ddo-char-set: 128";
            this.textBox28.Text = "ITEM111";
            this.textBox28.Top = 0.4787409F;
            this.textBox28.Width = 0.7480313F;
            // 
            // textBox29
            // 
            this.textBox29.DataField = "ITEM099";
            this.textBox29.Height = 0.1574803F;
            this.textBox29.Left = 4.92126F;
            this.textBox29.MultiLine = false;
            this.textBox29.Name = "textBox29";
            this.textBox29.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: right; verti" +
    "cal-align: bottom; ddo-char-set: 128";
            this.textBox29.Text = "ITEM099";
            this.textBox29.Top = 0.3181103F;
            this.textBox29.Width = 0.7480313F;
            // 
            // textBox30
            // 
            this.textBox30.DataField = "ITEM098";
            this.textBox30.Height = 0.1574803F;
            this.textBox30.Left = 4.151181F;
            this.textBox30.MultiLine = false;
            this.textBox30.Name = "textBox30";
            this.textBox30.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: right; verti" +
    "cal-align: bottom; ddo-char-set: 128";
            this.textBox30.Text = "ITEM098";
            this.textBox30.Top = 0.3212591F;
            this.textBox30.Width = 0.7480313F;
            // 
            // textBox31
            // 
            this.textBox31.DataField = "ITEM110";
            this.textBox31.Height = 0.1574803F;
            this.textBox31.Left = 4.151181F;
            this.textBox31.MultiLine = false;
            this.textBox31.Name = "textBox31";
            this.textBox31.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: right; verti" +
    "cal-align: bottom; ddo-char-set: 128";
            this.textBox31.Text = "ITEM110";
            this.textBox31.Top = 0.4818898F;
            this.textBox31.Width = 0.7480313F;
            // 
            // textBox32
            // 
            this.textBox32.DataField = "ITEM109";
            this.textBox32.Height = 0.1574803F;
            this.textBox32.Left = 3.40315F;
            this.textBox32.MultiLine = false;
            this.textBox32.Name = "textBox32";
            this.textBox32.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: right; verti" +
    "cal-align: bottom; ddo-char-set: 128";
            this.textBox32.Text = "ITEM109";
            this.textBox32.Top = 0.4787398F;
            this.textBox32.Width = 0.7480313F;
            // 
            // textBox33
            // 
            this.textBox33.DataField = "ITEM097";
            this.textBox33.Height = 0.1574803F;
            this.textBox33.Left = 3.40315F;
            this.textBox33.MultiLine = false;
            this.textBox33.Name = "textBox33";
            this.textBox33.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: right; verti" +
    "cal-align: bottom; ddo-char-set: 128";
            this.textBox33.Text = "ITEM097";
            this.textBox33.Top = 0.3212591F;
            this.textBox33.Width = 0.7480313F;
            // 
            // textBox34
            // 
            this.textBox34.DataField = "ITEM096";
            this.textBox34.Height = 0.1574803F;
            this.textBox34.Left = 2.637795F;
            this.textBox34.MultiLine = false;
            this.textBox34.Name = "textBox34";
            this.textBox34.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: right; verti" +
    "cal-align: bottom; ddo-char-set: 128";
            this.textBox34.Text = "ITEM096";
            this.textBox34.Top = 0.3212591F;
            this.textBox34.Width = 0.7480313F;
            // 
            // textBox35
            // 
            this.textBox35.DataField = "ITEM108";
            this.textBox35.Height = 0.1574803F;
            this.textBox35.Left = 2.637796F;
            this.textBox35.MultiLine = false;
            this.textBox35.Name = "textBox35";
            this.textBox35.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: right; verti" +
    "cal-align: bottom; ddo-char-set: 128";
            this.textBox35.Text = "ITEM108";
            this.textBox35.Top = 0.4818898F;
            this.textBox35.Width = 0.7480313F;
            // 
            // textBox36
            // 
            this.textBox36.DataField = "ITEM107";
            this.textBox36.Height = 0.1574803F;
            this.textBox36.Left = 1.889764F;
            this.textBox36.MultiLine = false;
            this.textBox36.Name = "textBox36";
            this.textBox36.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: right; verti" +
    "cal-align: bottom; ddo-char-set: 128";
            this.textBox36.Text = "ITEM107";
            this.textBox36.Top = 0.4787394F;
            this.textBox36.Width = 0.7480313F;
            // 
            // textBox37
            // 
            this.textBox37.DataField = "ITEM095";
            this.textBox37.Height = 0.1574803F;
            this.textBox37.Left = 1.889764F;
            this.textBox37.MultiLine = false;
            this.textBox37.Name = "textBox37";
            this.textBox37.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: right; verti" +
    "cal-align: bottom; ddo-char-set: 128";
            this.textBox37.Text = "ITEM095";
            this.textBox37.Top = 0.3212591F;
            this.textBox37.Width = 0.7480313F;
            // 
            // textBox38
            // 
            this.textBox38.DataField = "ITEM113";
            this.textBox38.Height = 0.1574803F;
            this.textBox38.Left = 6.417324F;
            this.textBox38.MultiLine = false;
            this.textBox38.Name = "textBox38";
            this.textBox38.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: right; verti" +
    "cal-align: bottom; ddo-char-set: 128";
            this.textBox38.Text = "ITEM113";
            this.textBox38.Top = 0.4787409F;
            this.textBox38.Width = 0.7480313F;
            // 
            // textBox39
            // 
            this.textBox39.DataField = "ITEM101";
            this.textBox39.Height = 0.1574803F;
            this.textBox39.Left = 6.417327F;
            this.textBox39.MultiLine = false;
            this.textBox39.Name = "textBox39";
            this.textBox39.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: right; verti" +
    "cal-align: bottom; ddo-char-set: 128";
            this.textBox39.Text = "ITEM101";
            this.textBox39.Top = 0.3181103F;
            this.textBox39.Width = 0.7480313F;
            // 
            // textBox40
            // 
            this.textBox40.DataField = "ITEM102";
            this.textBox40.Height = 0.1574803F;
            this.textBox40.Left = 7.165362F;
            this.textBox40.MultiLine = false;
            this.textBox40.Name = "textBox40";
            this.textBox40.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: right; verti" +
    "cal-align: bottom; ddo-char-set: 128";
            this.textBox40.Text = "ITEM102";
            this.textBox40.Top = 0.3181103F;
            this.textBox40.Width = 0.7480313F;
            // 
            // textBox41
            // 
            this.textBox41.DataField = "ITEM114";
            this.textBox41.Height = 0.1574803F;
            this.textBox41.Left = 7.165355F;
            this.textBox41.MultiLine = false;
            this.textBox41.Name = "textBox41";
            this.textBox41.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: right; verti" +
    "cal-align: bottom; ddo-char-set: 128";
            this.textBox41.Text = "ITEM114";
            this.textBox41.Top = 0.4787409F;
            this.textBox41.Width = 0.7480313F;
            // 
            // textBox42
            // 
            this.textBox42.DataField = "ITEM103";
            this.textBox42.Height = 0.1574803F;
            this.textBox42.Left = 7.91339F;
            this.textBox42.MultiLine = false;
            this.textBox42.Name = "textBox42";
            this.textBox42.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: right; verti" +
    "cal-align: bottom; ddo-char-set: 128";
            this.textBox42.Text = "ITEM103";
            this.textBox42.Top = 0.3181103F;
            this.textBox42.Width = 0.7480313F;
            // 
            // textBox43
            // 
            this.textBox43.DataField = "ITEM115";
            this.textBox43.Height = 0.1574803F;
            this.textBox43.Left = 7.91339F;
            this.textBox43.MultiLine = false;
            this.textBox43.Name = "textBox43";
            this.textBox43.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: right; verti" +
    "cal-align: bottom; ddo-char-set: 128";
            this.textBox43.Text = "ITEM115";
            this.textBox43.Top = 0.4787409F;
            this.textBox43.Width = 0.7480313F;
            // 
            // textBox44
            // 
            this.textBox44.DataField = "ITEM104";
            this.textBox44.Height = 0.1574803F;
            this.textBox44.Left = 8.661425F;
            this.textBox44.MultiLine = false;
            this.textBox44.Name = "textBox44";
            this.textBox44.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: right; verti" +
    "cal-align: bottom; ddo-char-set: 128";
            this.textBox44.Text = "ITEM104";
            this.textBox44.Top = 0.3181103F;
            this.textBox44.Width = 0.7480313F;
            // 
            // textBox45
            // 
            this.textBox45.DataField = "ITEM116";
            this.textBox45.Height = 0.1574803F;
            this.textBox45.Left = 8.661418F;
            this.textBox45.MultiLine = false;
            this.textBox45.Name = "textBox45";
            this.textBox45.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: right; verti" +
    "cal-align: bottom; ddo-char-set: 128";
            this.textBox45.Text = "ITEM116";
            this.textBox45.Top = 0.4787409F;
            this.textBox45.Width = 0.7480313F;
            // 
            // textBox46
            // 
            this.textBox46.DataField = "ITEM105";
            this.textBox46.Height = 0.1574803F;
            this.textBox46.Left = 9.409452F;
            this.textBox46.MultiLine = false;
            this.textBox46.Name = "textBox46";
            this.textBox46.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: right; verti" +
    "cal-align: bottom; ddo-char-set: 128";
            this.textBox46.Text = "ITEM105";
            this.textBox46.Top = 0.3181103F;
            this.textBox46.Width = 0.7480313F;
            // 
            // textBox47
            // 
            this.textBox47.DataField = "ITEM117";
            this.textBox47.Height = 0.1574803F;
            this.textBox47.Left = 9.409452F;
            this.textBox47.MultiLine = false;
            this.textBox47.Name = "textBox47";
            this.textBox47.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: right; verti" +
    "cal-align: bottom; ddo-char-set: 128";
            this.textBox47.Text = "ITEM117";
            this.textBox47.Top = 0.4787409F;
            this.textBox47.Width = 0.7480313F;
            // 
            // textBox48
            // 
            this.textBox48.DataField = "ITEM106";
            this.textBox48.Height = 0.1574803F;
            this.textBox48.Left = 10.15748F;
            this.textBox48.MultiLine = false;
            this.textBox48.Name = "textBox48";
            this.textBox48.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: right; verti" +
    "cal-align: bottom; ddo-char-set: 128";
            this.textBox48.Text = "ITEM106";
            this.textBox48.Top = 0.3181103F;
            this.textBox48.Width = 0.7480313F;
            // 
            // textBox49
            // 
            this.textBox49.DataField = "ITEM118";
            this.textBox49.Height = 0.1574803F;
            this.textBox49.Left = 10.15748F;
            this.textBox49.MultiLine = false;
            this.textBox49.Name = "textBox49";
            this.textBox49.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: right; verti" +
    "cal-align: bottom; ddo-char-set: 128";
            this.textBox49.Text = "ITEM118";
            this.textBox49.Top = 0.4787409F;
            this.textBox49.Width = 0.7480313F;
            // 
            // line12
            // 
            this.line12.Height = 0.6299213F;
            this.line12.Left = 10.90158F;
            this.line12.LineWeight = 2F;
            this.line12.Name = "line12";
            this.line12.Top = 0F;
            this.line12.Width = 0F;
            this.line12.X1 = 10.90158F;
            this.line12.X2 = 10.90158F;
            this.line12.Y1 = 0F;
            this.line12.Y2 = 0.6299213F;
            // 
            // line32
            // 
            this.line32.Height = 0.6299213F;
            this.line32.Left = 10.15748F;
            this.line32.LineWeight = 1F;
            this.line32.Name = "line32";
            this.line32.Top = 0F;
            this.line32.Width = 0F;
            this.line32.X1 = 10.15748F;
            this.line32.X2 = 10.15748F;
            this.line32.Y1 = 0F;
            this.line32.Y2 = 0.6299213F;
            // 
            // line33
            // 
            this.line33.Height = 0.6299213F;
            this.line33.Left = 9.409447F;
            this.line33.LineWeight = 1F;
            this.line33.Name = "line33";
            this.line33.Top = 0F;
            this.line33.Width = 3.814697E-06F;
            this.line33.X1 = 9.409447F;
            this.line33.X2 = 9.409451F;
            this.line33.Y1 = 0F;
            this.line33.Y2 = 0.6299213F;
            // 
            // line34
            // 
            this.line34.Height = 0.6299213F;
            this.line34.Left = 8.661413F;
            this.line34.LineWeight = 1F;
            this.line34.Name = "line34";
            this.line34.Top = 0F;
            this.line34.Width = 3.814697E-06F;
            this.line34.X1 = 8.661413F;
            this.line34.X2 = 8.661417F;
            this.line34.Y1 = 0F;
            this.line34.Y2 = 0.6299213F;
            // 
            // line35
            // 
            this.line35.Height = 0.6299213F;
            this.line35.Left = 7.913385F;
            this.line35.LineWeight = 1F;
            this.line35.Name = "line35";
            this.line35.Top = 0F;
            this.line35.Width = 4.291534E-06F;
            this.line35.X1 = 7.913385F;
            this.line35.X2 = 7.913389F;
            this.line35.Y1 = 0F;
            this.line35.Y2 = 0.6299213F;
            // 
            // line37
            // 
            this.line37.Height = 0.6299213F;
            this.line37.Left = 7.16535F;
            this.line37.LineWeight = 1F;
            this.line37.Name = "line37";
            this.line37.Top = 0F;
            this.line37.Width = 3.814697E-06F;
            this.line37.X1 = 7.16535F;
            this.line37.X2 = 7.165354F;
            this.line37.Y1 = 0F;
            this.line37.Y2 = 0.6299213F;
            // 
            // line31
            // 
            this.line31.Height = 0.6299213F;
            this.line31.Left = 6.417319F;
            this.line31.LineWeight = 1F;
            this.line31.Name = "line31";
            this.line31.Top = 0F;
            this.line31.Width = 4.291534E-06F;
            this.line31.X1 = 6.417319F;
            this.line31.X2 = 6.417323F;
            this.line31.Y1 = 0F;
            this.line31.Y2 = 0.6299213F;
            // 
            // line30
            // 
            this.line30.Height = 0.6299213F;
            this.line30.Left = 5.669287F;
            this.line30.LineWeight = 1F;
            this.line30.Name = "line30";
            this.line30.Top = 0F;
            this.line30.Width = 3.814697E-06F;
            this.line30.X1 = 5.669287F;
            this.line30.X2 = 5.669291F;
            this.line30.Y1 = 0F;
            this.line30.Y2 = 0.6299213F;
            // 
            // line29
            // 
            this.line29.Height = 0.6299213F;
            this.line29.Left = 4.921256F;
            this.line29.LineWeight = 1F;
            this.line29.Name = "line29";
            this.line29.Top = 0F;
            this.line29.Width = 3.814697E-06F;
            this.line29.X1 = 4.921256F;
            this.line29.X2 = 4.92126F;
            this.line29.Y1 = 0F;
            this.line29.Y2 = 0.6299213F;
            // 
            // line18
            // 
            this.line18.Height = 0.6299213F;
            this.line18.Left = 4.151177F;
            this.line18.LineWeight = 1F;
            this.line18.Name = "line18";
            this.line18.Top = 0F;
            this.line18.Width = 4.291534E-06F;
            this.line18.X1 = 4.151177F;
            this.line18.X2 = 4.151181F;
            this.line18.Y1 = 0F;
            this.line18.Y2 = 0.6299213F;
            // 
            // line39
            // 
            this.line39.Height = 0.6299213F;
            this.line39.Left = 3.403142F;
            this.line39.LineWeight = 1F;
            this.line39.Name = "line39";
            this.line39.Top = 0F;
            this.line39.Width = 4.053116E-06F;
            this.line39.X1 = 3.403146F;
            this.line39.X2 = 3.403142F;
            this.line39.Y1 = 0F;
            this.line39.Y2 = 0.6299213F;
            // 
            // line38
            // 
            this.line38.Height = 0.6299213F;
            this.line38.Left = 2.655114F;
            this.line38.LineWeight = 1F;
            this.line38.Name = "line38";
            this.line38.Top = 0F;
            this.line38.Width = 3.099442E-06F;
            this.line38.X1 = 2.655114F;
            this.line38.X2 = 2.655117F;
            this.line38.Y1 = 0F;
            this.line38.Y2 = 0.6299213F;
            // 
            // line20
            // 
            this.line20.Height = 0.6299213F;
            this.line20.Left = 1.88976F;
            this.line20.LineWeight = 1F;
            this.line20.Name = "line20";
            this.line20.Top = 0F;
            this.line20.Width = 3.933907E-06F;
            this.line20.X1 = 1.88976F;
            this.line20.X2 = 1.889764F;
            this.line20.Y1 = 0F;
            this.line20.Y2 = 0.6299213F;
            // 
            // line15
            // 
            this.line15.Height = 0F;
            this.line15.Left = 1.889764F;
            this.line15.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.line15.LineWeight = 1F;
            this.line15.Name = "line15";
            this.line15.Top = 0.3212599F;
            this.line15.Width = 9.011816F;
            this.line15.X1 = 1.889764F;
            this.line15.X2 = 10.90158F;
            this.line15.Y1 = 0.3212599F;
            this.line15.Y2 = 0.3212599F;
            // 
            // line16
            // 
            this.line16.Height = 0F;
            this.line16.Left = 1.889764F;
            this.line16.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.line16.LineWeight = 1F;
            this.line16.Name = "line16";
            this.line16.Top = 0.4755906F;
            this.line16.Width = 9.011816F;
            this.line16.X1 = 1.889764F;
            this.line16.X2 = 10.90158F;
            this.line16.Y1 = 0.4755906F;
            this.line16.Y2 = 0.4755906F;
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0.01041667F;
            this.pageFooter.Name = "pageFooter";
            // 
            // KYKR1023R
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.7874016F;
            this.PageSettings.Margins.Left = 0.3937008F;
            this.PageSettings.Margins.Right = 0.3937008F;
            this.PageSettings.Margins.Top = 0.5905512F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
            this.PageSettings.PaperHeight = 11.69291F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.267716F;
            this.PrintWidth = 10.90551F;
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.pageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle08)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle07)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle06)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle05)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle04)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle09)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtToday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHyojiDateFr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHyojiDateTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox54)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVal10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVal22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVal21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVal09)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVal08)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVal20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVal19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVal07)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVal06)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVal18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVal17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVal05)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVal23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVal11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVal02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVal01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVal12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVal24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVal13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVal25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVal14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVal26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVal15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVal27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVal16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVal28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVal03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox74)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHyojiDateFr;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCompanyName;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPageCount;
        private GrapeCity.ActiveReports.SectionReportModel.ReportInfo txtToday;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHyojiDateTo;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle02;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle05;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle04;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle17;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle18;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle16;
        private GrapeCity.ActiveReports.SectionReportModel.Line line19;
        private GrapeCity.ActiveReports.SectionReportModel.Line line2;
        private GrapeCity.ActiveReports.SectionReportModel.Line line3;
        private GrapeCity.ActiveReports.SectionReportModel.Line line4;
        private GrapeCity.ActiveReports.SectionReportModel.Line line5;
        private GrapeCity.ActiveReports.SectionReportModel.Line line6;
        private GrapeCity.ActiveReports.SectionReportModel.Line line7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle13;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle14;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle23;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle25;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle24;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle07;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle09;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle08;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle19;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle21;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle22;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle20;
        private GrapeCity.ActiveReports.SectionReportModel.Line line21;
        private GrapeCity.ActiveReports.SectionReportModel.Line line22;
        private GrapeCity.ActiveReports.SectionReportModel.Line line23;
        private GrapeCity.ActiveReports.SectionReportModel.Line line24;
        private GrapeCity.ActiveReports.SectionReportModel.Line line25;
        private GrapeCity.ActiveReports.SectionReportModel.Line line26;
        private GrapeCity.ActiveReports.SectionReportModel.Line line27;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle06;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle15;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle03;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle26;
        private GrapeCity.ActiveReports.SectionReportModel.Line line8;
        private GrapeCity.ActiveReports.SectionReportModel.Line line9;
        private GrapeCity.ActiveReports.SectionReportModel.Line line10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtVal10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtVal22;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtVal21;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtVal09;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtVal08;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtVal20;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtVal19;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtVal07;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtVal06;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtVal18;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtVal17;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtVal05;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtVal23;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtVal11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtVal02;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtVal01;
        private GrapeCity.ActiveReports.SectionReportModel.Line line18;
        private GrapeCity.ActiveReports.SectionReportModel.Line line20;
        private GrapeCity.ActiveReports.SectionReportModel.Line line28;
        private GrapeCity.ActiveReports.SectionReportModel.Line line29;
        private GrapeCity.ActiveReports.SectionReportModel.Line line30;
        private GrapeCity.ActiveReports.SectionReportModel.Line line31;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtVal12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtVal24;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtVal13;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtVal25;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtVal14;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtVal26;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtVal15;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtVal27;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtVal16;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtVal28;
        private GrapeCity.ActiveReports.SectionReportModel.Line line32;
        private GrapeCity.ActiveReports.SectionReportModel.Line line33;
        private GrapeCity.ActiveReports.SectionReportModel.Line line34;
        private GrapeCity.ActiveReports.SectionReportModel.Line line35;
        private GrapeCity.ActiveReports.SectionReportModel.Line line36;
        private GrapeCity.ActiveReports.SectionReportModel.Line line37;
        private GrapeCity.ActiveReports.SectionReportModel.Line line38;
        private GrapeCity.ActiveReports.SectionReportModel.Line line39;
        private GrapeCity.ActiveReports.SectionReportModel.Line line40;
        private GrapeCity.ActiveReports.SectionReportModel.Line line11;
        private GrapeCity.ActiveReports.SectionReportModel.Line line12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtVal03;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox54;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox74;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox13;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox14;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox15;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox16;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox17;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox18;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox19;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox20;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox21;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox22;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox23;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox24;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox25;
        private GrapeCity.ActiveReports.SectionReportModel.Line line13;
        private GrapeCity.ActiveReports.SectionReportModel.Line line14;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox26;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox27;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox28;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox29;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox30;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox31;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox32;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox33;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox34;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox35;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox36;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox37;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox38;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox39;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox40;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox41;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox42;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox43;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox44;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox45;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox46;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox47;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox48;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox49;
        private GrapeCity.ActiveReports.SectionReportModel.Line line15;
        private GrapeCity.ActiveReports.SectionReportModel.Line line16;
    }
}
