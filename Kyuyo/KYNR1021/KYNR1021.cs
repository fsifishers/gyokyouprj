﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Reflection;

using GrapeCity.ActiveReports;

using jp.co.fsi.common.access;
using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;
using jp.co.fsi.common.mynumber;

namespace jp.co.fsi.ky.kynr1021
{
    /// <summary>
    /// 源泉徴収票(KYNR1021)
    /// </summary>
    public partial class KYNR1021 : BasePgForm
    {
        #region 変数
        int _sort;                                          // 出力ワークSORTフィールド
        decimal[] _shainGokei;                              // 社員別支給実績合計用
        DataTable _dtKyuSti;                                // 給与項目設定データ
        DataTable _dtShoSti;                                // 賞与項目設定データ
        #endregion

        #region プロパティ
        /// <summary>
        /// 画面上最後となるフォーカスのEnterボタン押下時処理用変数
        /// </summary>
        private bool _dtFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._dtFlg;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KYNR1021()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)

        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 年度初期値
            if (Util.ToInt(Util.ToString(this.txtGengoYearNendo.Text)) == 0)
            {
                string[] nendo = Util.ConvJpDate(new DateTime(this.UInfo.KaikeiNendo, 4, 1), this.Dba);
                this.lblGengoNendo.Text = nendo[0];
                this.txtGengoYearNendo.Text = nendo[2];
            }

            // オプション初期値
            this.rdoTaisho1.Checked = true;
            this.rdoOrder2.Checked = true;

            // 初期フォーカス
            this.txtGengoYearNendo.Focus();
            this.txtGengoYearNendo.SelectAll();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 元号年とコード項目に
            // フォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtGengoYearNendo":
                case "txtShainCdFr":
                case "txtShainCdTo":
                case "txtBumonCdFr":
                case "txtBumonCdTo":
                case "txtBukaCdFr":
                case "txtBukaCdTo":
                    this.btnF1.Enabled = true;
                    break;
                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        public override void PressF1()
        {
            Assembly asm;
            Type t;

            switch (this.ActiveCtlNm)
            {
                case "txtGengoYearNendo":
                    #region 元号検索
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom("CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            if (this.ActiveCtlNm == "txtGengoYearNendo")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.InData = this.lblGengoNendo.Text;
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.lblGengoNendo.Text = result[1];

                                    // 和暦年未入力時の初期値
                                    if (Util.ToInt(Util.ToString(this.txtGengoYearNendo.Text)) == 0)
                                    {
                                        string[] nendo = Util.ConvJpDate(new DateTime(this.UInfo.KaikeiNendo, 4, 1), this.Dba);
                                        this.lblGengoNendo.Text = nendo[0];
                                        this.txtGengoYearNendo.Text = nendo[2];
                                    }

                                    // 和暦年補正
                                    DateTime adDate =
                                        Util.ConvAdDate(this.lblGengoNendo.Text, this.txtGengoYearNendo.Text, "4", "1", this.Dba);
                                    string[] jpDate = Util.ConvJpDate(adDate, this.Dba);
                                    this.lblGengoNendo.Text = jpDate[0];
                                    this.txtGengoYearNendo.Text = jpDate[2];
                                }
                            }
                        }

                    }
                    #endregion
                    break;
                case "txtShainCdFr":
                case "txtShainCdTo":
                    #region 社員検索
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom("KYCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.ky.kycm1021.KYCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            if (this.ActiveCtlNm == "txtShainCdFr")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.Par1 = "1";                     // ダイアログ起動パラメータ
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.txtShainCdFr.Text = result[0];
                                    this.lblShainCdFr.Text = result[1];
                                }
                            }
                            else if (this.ActiveCtlNm == "txtShainCdTo")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.Par1 = "1";                     // ダイアログ起動パラメータ
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.txtShainCdTo.Text = result[0];
                                    this.lblShainCdTo.Text = result[1];
                                }
                            }
                        }
                    }
                    #endregion
                    break;
                case "txtBumonCdFr":
                case "txtBumonCdTo":
                    #region 部門検索
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom("KYCM1031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.ky.kycm1031.KYCM1031");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            if (this.ActiveCtlNm == "txtBumonCdFr")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.Par1 = "1";
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.txtBumonCdFr.Text = result[0];
                                    this.lblBumonCdFr.Text = result[1];
                                    this.txtBukaCdFr.Text = "";
                                    this.lblBukaCdFr.Text = "先　頭";
                                }
                            }
                            else if (this.ActiveCtlNm == "txtBumonCdTo")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.Par1 = "1";
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.txtBumonCdTo.Text = result[0];
                                    this.lblBumonCdTo.Text = result[1];
                                    this.txtBukaCdTo.Text = "";
                                    this.lblBukaCdTo.Text = "最　後";
                                }
                            }
                        }
                    }
                    #endregion
                    break;
                case "txtBukaCdFr":
                case "txtBukaCdTo":
                    #region 部課検索
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom("KYCM1041.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.ky.kycm1041.KYCM1041");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            if (this.ActiveCtlNm == "txtBukaCdFr"
                                && !ValChk.IsEmpty(this.txtBumonCdFr.Text))
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.Par1 = "1";                         // 検索ダイアログ起動パラメータ
                                frm.InData = this.txtBumonCdFr.Text;    // (string)部門コード
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.txtBukaCdFr.Text = result[0];
                                    this.lblBukaCdFr.Text = result[1];
                                }
                            }
                            else if (this.ActiveCtlNm == "txtBukaCdTo"
                                && !ValChk.IsEmpty(this.txtBumonCdTo.Text))
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.Par1 = "1";                         // 検索ダイアログ起動パラメータ
                                frm.InData = this.txtBumonCdTo.Text;    // (string)部門コード
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.txtBukaCdTo.Text = result[0];
                                    this.lblBukaCdTo.Text = result[1];
                                }
                            }
                        }
                    }
                    #endregion
                    break;
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
            {
                // プレビュー処理
                DoPrint(true);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            if (Msg.ConfNmYesNo("印刷", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }

        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            // 設定画面の起動
            // MEMO:原則としてここで渡す帳票IDの設定はReport.csvに保持していることが前提ですが、
            // 保持していない場合は、設定画面での保存(F6)時に新規に設定が保持されます。
            //PrintSettingForm psForm = new PrintSettingForm(new string[3] { "KYNR1021R", "KYNR1022R", "KYNR1023R" });
            PrintSettingForm psForm = new PrintSettingForm(new string[2] { "KYNR1022R", "KYNR1023R" });
            psForm.ShowDialog();
        }
        #endregion

        #region イベント

        /// <summary>
        /// 社員コード(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShainCdFr_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtShainCdFr.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtShainCdFr.SelectAll();
                e.Cancel = true;
                return;
            }

            // コードを元に名称を取得する
            // 取得された場合、名称をラベルに反映する
            if (this.txtShainCdFr.Text.Length > 0)
            {
                this.lblShainCdFr.Text = this.Dba.GetName(this.UInfo, "TB_KY_SHAIN_JOHO"," ", this.txtShainCdFr.Text);
            }
            else
            {
                this.lblShainCdFr.Text = "先　頭";
            }
        }

        /// <summary>
        /// 社員コード(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShainCdTo_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtShainCdTo.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtShainCdTo.SelectAll();
                e.Cancel = true;
                return;
            }

            // コードを元に名称を取得する
            // 取得された場合、名称をラベルに反映する
            if (this.txtShainCdTo.Text.Length > 0)
            {
                this.lblShainCdTo.Text = this.Dba.GetName(this.UInfo, "TB_KY_SHAIN_JOHO", " ", this.txtShainCdTo.Text);
            }
            else
            {
                this.lblShainCdTo.Text = "最　後";
            }
        }

        /// <summary>
        /// 部門コード(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtBumonCdFr_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtBumonCdFr.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtBumonCdFr.SelectAll();
                e.Cancel = true;
                return;
            }

            // コードを元に名称を取得する
            // 取得された場合、名称をラベルに反映する
            if (this.txtBumonCdFr.Text.Length > 0)
            {
                this.lblBumonCdFr.Text = this.Dba.GetName(this.UInfo, "TB_KY_BUMON", " ", this.txtBumonCdFr.Text);
            }
            else
            {
                this.lblBumonCdFr.Text = "先　頭";
            }
        }

        /// <summary>
        /// 部門コード(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtBumonCdTo_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtBumonCdTo.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtBumonCdTo.SelectAll();
                e.Cancel = true;
                return;
            }

            // コードを元に名称を取得する
            // 取得された場合、名称をラベルに反映する
            if (this.txtBumonCdTo.Text.Length > 0)
            {
                this.lblBumonCdTo.Text = this.Dba.GetName(this.UInfo, "TB_KY_BUMON", " ", this.txtBumonCdTo.Text);
            }
            else
            {
                this.lblBumonCdTo.Text = "最　後";
            }
        }

        /// <summary>
        /// 部課コード(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtBukaCdFr_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtBukaCdFr.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtBukaCdFr.SelectAll();
                e.Cancel = true;
                return;
            }

            // コードを元に名称を取得する
            // 取得された場合、名称をラベルに反映する
            if (this.txtBukaCdFr.Text.Length > 0)
            {
                this.lblBukaCdFr.Text = this.Dba.GetBukaNm(this.UInfo, this.txtBumonCdFr.Text, this.txtBukaCdFr.Text);
            }
            else
            {
                this.lblBukaCdFr.Text = "先　頭";
            }
        }

        /// <summary>
        /// 部課コード(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtBukaCdTo_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtBukaCdTo.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtBukaCdTo.SelectAll();
                e.Cancel = true;
                // Enter処理を無効化
                this._dtFlg = false;
                return;
            }

            // コードを元に名称を取得する
            // 取得された場合、名称をラベルに反映する
            if (this.txtBukaCdTo.Text.Length > 0)
            {
                this.lblBukaCdTo.Text = this.Dba.GetBukaNm(this.UInfo, this.txtBumonCdTo.Text, this.txtBukaCdTo.Text);
            }
            else
            {
                this.lblBukaCdTo.Text = "最　後";
            }
            // Enter処理を有効化
            this._dtFlg = true;
        }

        /// <summary>
        /// 部課コード(至)のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtBukaCdTo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.Flg)
            {
                // Enter処理を無効化
                this._dtFlg = false;

                if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
                {
                    // ﾌﾟﾚﾋﾞｭｰ処理
                    DoPrint(true);
                }
                else
                {
                    this.txtBukaCdTo.Focus();
                }
            }
        }
        /// <summary>
        /// 年度(年)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGengoYearNendo_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtGengoYearNendo.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtGengoYearNendo.SelectAll();
                e.Cancel = true;
                return;
            }

            string[] nendo;

            // 年度の取得
            if (Util.ToInt(Util.ToString(this.txtGengoYearNendo.Text)) == 0)
            {
                // 無効年入力の時
                nendo = Util.ConvJpDate(new DateTime(this.UInfo.KaikeiNendo, 1, 1), this.Dba);
            }
            else
            {
                DateTime adNendo =
                    Util.ConvAdDate(this.lblGengoNendo.Text, this.txtGengoYearNendo.Text, "4", "1", this.Dba);
                nendo = Util.ConvJpDate(adNendo, this.Dba);
            }
            this.lblGengoNendo.Text = nendo[0];
            this.txtGengoYearNendo.Text = nendo[2];
        }

        #endregion

        #region privateメソッド
        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview)
        {
            try
            {
                bool dataFlag;
                this.Dba.BeginTransaction();

                // 帳票出力用にワークテーブルにデータを作成
                dataFlag = MakeWkData();

                // 帳票出力
                if (dataFlag)
                {
                    #region ITEM001～ITEM153
                    // 取得列の定義
                    StringBuilder cols = new StringBuilder();
                    cols.Append("  ITEM001");
                    cols.Append(" ,ITEM002");
                    cols.Append(" ,ITEM003");
                    cols.Append(" ,ITEM004");
                    cols.Append(" ,ITEM005");
                    cols.Append(" ,ITEM006");
                    cols.Append(" ,ITEM007");
                    cols.Append(" ,ITEM008");
                    cols.Append(" ,ITEM009");
                    cols.Append(" ,ITEM010");
                    cols.Append(" ,ITEM011");
                    cols.Append(" ,ITEM012");
                    cols.Append(" ,ITEM013");
                    cols.Append(" ,ITEM014");
                    cols.Append(" ,ITEM015");
                    cols.Append(" ,ITEM016");
                    cols.Append(" ,ITEM017");
                    cols.Append(" ,ITEM018");
                    cols.Append(" ,ITEM019");
                    cols.Append(" ,ITEM020");
                    cols.Append(" ,ITEM021");
                    cols.Append(" ,ITEM022");
                    cols.Append(" ,ITEM023");
                    cols.Append(" ,ITEM024");
                    cols.Append(" ,ITEM025");
                    cols.Append(" ,ITEM026");
                    cols.Append(" ,ITEM027");
                    cols.Append(" ,ITEM028");
                    cols.Append(" ,ITEM029");
                    cols.Append(" ,ITEM030");
                    cols.Append(" ,ITEM031");
                    cols.Append(" ,ITEM032");
                    cols.Append(" ,ITEM033");
                    cols.Append(" ,ITEM034");
                    cols.Append(" ,ITEM035");
                    cols.Append(" ,ITEM036");
                    cols.Append(" ,ITEM037");
                    cols.Append(" ,ITEM038");
                    cols.Append(" ,ITEM039");
                    cols.Append(" ,ITEM040");
                    cols.Append(" ,ITEM041");
                    cols.Append(" ,ITEM042");
                    cols.Append(" ,ITEM043");
                    cols.Append(" ,ITEM044");
                    cols.Append(" ,ITEM045");
                    cols.Append(" ,ITEM046");
                    cols.Append(" ,ITEM047");
                    cols.Append(" ,ITEM048");
                    cols.Append(" ,ITEM049");
                    cols.Append(" ,ITEM050");
                    cols.Append(" ,ITEM051");
                    cols.Append(" ,ITEM052");
                    cols.Append(" ,ITEM053");
                    cols.Append(" ,ITEM054");
                    cols.Append(" ,ITEM055");
                    cols.Append(" ,ITEM056");
                    cols.Append(" ,ITEM057");
                    cols.Append(" ,ITEM058");
                    cols.Append(" ,ITEM059");
                    cols.Append(" ,ITEM060");
                    cols.Append(" ,ITEM061");
                    cols.Append(" ,ITEM062");
                    cols.Append(" ,ITEM063");
                    cols.Append(" ,ITEM064");
                    cols.Append(" ,ITEM065");
                    cols.Append(" ,ITEM066");
                    cols.Append(" ,ITEM067");
                    cols.Append(" ,ITEM068");
                    cols.Append(" ,ITEM069");
                    cols.Append(" ,ITEM070");
                    cols.Append(" ,ITEM071");
                    cols.Append(" ,ITEM072");
                    cols.Append(" ,ITEM073");
                    cols.Append(" ,ITEM074");
                    cols.Append(" ,ITEM075");
                    cols.Append(" ,ITEM076");
                    cols.Append(" ,ITEM077");
                    cols.Append(" ,ITEM078");
                    cols.Append(" ,ITEM079");
                    cols.Append(" ,ITEM080");
                    cols.Append(" ,ITEM081");
                    cols.Append(" ,ITEM082");
                    cols.Append(" ,ITEM083");
                    cols.Append(" ,ITEM084");
                    cols.Append(" ,ITEM085");
                    cols.Append(" ,ITEM086");
                    cols.Append(" ,ITEM087");
                    cols.Append(" ,ITEM088");
                    cols.Append(" ,ITEM089");
                    cols.Append(" ,ITEM090");
                    cols.Append(" ,ITEM091");
                    cols.Append(" ,ITEM092");
                    cols.Append(" ,ITEM093");
                    cols.Append(" ,ITEM094");
                    cols.Append(" ,ITEM095");
                    cols.Append(" ,ITEM096");
                    cols.Append(" ,ITEM097");
                    cols.Append(" ,ITEM098");
                    cols.Append(" ,ITEM099");
                    cols.Append(" ,ITEM100");
                    cols.Append(" ,ITEM101");
                    cols.Append(" ,ITEM102");
                    cols.Append(" ,ITEM103");
                    cols.Append(" ,ITEM104");
                    cols.Append(" ,ITEM105");
                    cols.Append(" ,ITEM106");
                    cols.Append(" ,ITEM107");
                    cols.Append(" ,ITEM108");
                    cols.Append(" ,ITEM109");
                    cols.Append(" ,ITEM110");
                    cols.Append(" ,ITEM111");
                    cols.Append(" ,ITEM112");
                    cols.Append(" ,ITEM113");
                    cols.Append(" ,ITEM114");
                    cols.Append(" ,ITEM115");
                    cols.Append(" ,ITEM116");
                    cols.Append(" ,ITEM117");
                    cols.Append(" ,ITEM118");
                    cols.Append(" ,ITEM119");
                    cols.Append(" ,ITEM120");
                    cols.Append(" ,ITEM121");
                    cols.Append(" ,ITEM122");
                    cols.Append(" ,ITEM123");
                    cols.Append(" ,ITEM124");
                    cols.Append(" ,ITEM125");
                    cols.Append(" ,ITEM126");
                    cols.Append(" ,ITEM127");
                    cols.Append(" ,ITEM128");
                    cols.Append(" ,ITEM129");
                    cols.Append(" ,ITEM130");
                    cols.Append(" ,ITEM131");
                    cols.Append(" ,ITEM132");
                    cols.Append(" ,ITEM133");
                    cols.Append(" ,ITEM134");
                    cols.Append(" ,ITEM135");
                    cols.Append(" ,ITEM136");
                    cols.Append(" ,ITEM137");
                    cols.Append(" ,ITEM138");
                    cols.Append(" ,ITEM139");
                    cols.Append(" ,ITEM140");
                    cols.Append(" ,ITEM141");
                    cols.Append(" ,ITEM142");
                    cols.Append(" ,ITEM143");
                    cols.Append(" ,ITEM144");
                    cols.Append(" ,ITEM145");
                    cols.Append(" ,ITEM146");
                    cols.Append(" ,ITEM147");
                    cols.Append(" ,ITEM148");
                    cols.Append(" ,ITEM149");
                    cols.Append(" ,ITEM150");
                    cols.Append(" ,ITEM151");
                    cols.Append(" ,ITEM152");
                    cols.Append(" ,ITEM153");

                    #endregion

                    // バインドパラメータの設定
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);

                    // データの取得
                    DataTable dtOutput = this.Dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_KY_TBL", "GUID = @GUID", "SORT ASC", dpc);

                    #region 27年度分
                    //// 帳票オブジェクトをインスタンス化
                    //KYNR1021R rpt = new KYNR1021R(dtOutput);

                    //if (isPreview)
                    //{
                    //    // プレビュー画面表示　
                    //    PreviewForm pFrm = new PreviewForm(rpt, this.UnqId);
                    //    pFrm.WindowState = FormWindowState.Maximized;
                    //    pFrm.Show();
                    //}
                    //else
                    //{
                    //    // 直接印刷
                    //    rpt.Run(false);
                    //    rpt.Document.Print(true, true, false);
                    //}
                    #endregion 

                    #region 28年度分
                    // 帳票オブジェクトをインスタンス化
                    KYNR1022R rpt = new KYNR1022R(dtOutput);
                    // 帳票オブジェクトをインスタンス化
                    KYNR1023R rpt1 = new KYNR1023R(dtOutput);

                    if (isPreview)
                    {
                        // プレビュー画面表示　税務署・受給者交付
                        PreviewForm pFrm = new PreviewForm(rpt, this.UnqId);
                        pFrm.WindowState = FormWindowState.Maximized;
                        pFrm.Show();
                        // プレビュー画面表示　市町村提出用
                        PreviewForm pFrm1 = new PreviewForm(rpt1, this.UnqId);
                        pFrm1.WindowState = FormWindowState.Maximized;
                        pFrm1.Show();
                    }
                    else
                    {
                        // 直接印刷
                        rpt.Run(false);
                        rpt.Document.Print(true, true, false);
                        // 直接印刷
                        rpt1.Run(false);
                        rpt1.Document.Print(true, true, false);
                    }
                    #endregion 
                }
                else
                {
                    Msg.Info("該当データが存在しません。");
                    return;
                }
            }
            finally
            {
                this.Dba.Rollback();
            }
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData()
        {
            // 年度を西暦・和暦で保持
            DateTime nendo = Util.ConvAdDate(this.lblGengoNendo.Text, this.txtGengoYearNendo.Text, "1", "1", this.Dba);
            string[] jpNendo = Util.ConvJpDate(nendo, this.Dba);

            #region 画面上での指定値
            // 社員コード設定
            string shainCdFr;
            string shainCdTo;
            if (Util.ToDecimal(txtShainCdFr.Text) > 0)
            {
                shainCdFr = txtShainCdFr.Text;
            }
            else
            {
                shainCdFr = "0";
            }
            if (Util.ToDecimal(txtShainCdTo.Text) > 0)
            {
                shainCdTo = txtShainCdTo.Text;
            }
            else
            {
                shainCdTo = "999999";
            }
            // 部門コード設定
            string bumonCdFr;
            string bumonCdTo;
            if (Util.ToDecimal(txtBumonCdFr.Text) > 0)
            {
                bumonCdFr = txtBumonCdFr.Text;
            }
            else
            {
                bumonCdFr = "0";
            }
            if (Util.ToDecimal(txtBumonCdTo.Text) > 0)
            {
                bumonCdTo = txtBumonCdTo.Text;
            }
            else
            {
                bumonCdTo = "9999";
            }
            // 部課コード設定
            string bukaCdFr;
            string bukaCdTo;
            if (Util.ToDecimal(txtBukaCdFr.Text) > 0)
            {
                bukaCdFr = txtBukaCdFr.Text;
            }
            else
            {
                bukaCdFr = "0";
            }
            if (Util.ToDecimal(txtBukaCdTo.Text) > 0)
            {
                bukaCdTo = txtBukaCdTo.Text;
            }
            else
            {
                bukaCdTo = "9999";
            }
            #endregion

            DbParamCollection dpc;
            StringBuilder sql;
            DataTable dt;
            ArrayList alParams;
            //ArrayList alParams2;

            #region 会社情報データ
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 2, this.UInfo.KaishaCd);
            // TB会社情報
            DataTable dtKaishaJoho = this.Dba.GetDataTableByConditionWithParams(
                "*",
                "TB_CM_KAISHA_JOHO",
                "KAISHA_CD = @KAISHA_CD",
                dpc);
            if (dtKaishaJoho.Rows.Count == 0)
            {
                return false;
            }
            #endregion

            #region 給与/賞与項目設定データ
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 2, this.UInfo.KaishaCd);
            // VI給与項目設定
            _dtKyuSti = this.Dba.GetDataTableByConditionWithParams(
                "*",
                "VI_KY_KYUYO_KOMOKU_STI",
                "KAISHA_CD = @KAISHA_CD AND INJI_KUBUN <> '0'",
                "KOMOKU_SHUBETSU, KOMOKU_BANGO",
                dpc);
            if (_dtKyuSti.Rows.Count == 0)
            {
                return false;
            }
            // VI賞与項目設定
            _dtShoSti = this.Dba.GetDataTableByConditionWithParams(
                "*",
                "VI_KY_SHOYO_KOMOKU_SETTEI",
                "KAISHA_CD = @KAISHA_CD AND INJI_KUBUN <> '0'",
                "KOMOKU_SHUBETSU, KOMOKU_BANGO",
                dpc);
            if (_dtShoSti.Rows.Count == 0)
            {
                return false;
            }
            #endregion

            #region dtMainLoop = VI社員情報(VI_KY_SHAIN_JOHO)
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 2, this.UInfo.KaishaCd);
            dpc.SetParam("@SHAIN_CD_FR", SqlDbType.VarChar, 6, shainCdFr);
            dpc.SetParam("@SHAIN_CD_TO", SqlDbType.VarChar, 6, shainCdTo);
            dpc.SetParam("@BUMON_CD_FR", SqlDbType.VarChar, 4, bumonCdFr);
            dpc.SetParam("@BUMON_CD_TO", SqlDbType.VarChar, 4, bumonCdTo);
            dpc.SetParam("@BUKA_CD_FR", SqlDbType.VarChar, 4, bukaCdFr);
            dpc.SetParam("@BUKA_CD_TO", SqlDbType.VarChar, 4, bukaCdTo);
            sql = new StringBuilder();
            sql.Append("SELECT * FROM VI_KY_SHAIN_JOHO");
            sql.Append(" WHERE KAISHA_CD = @KAISHA_CD");
            sql.Append(" AND SHAIN_CD BETWEEN @SHAIN_CD_FR AND @SHAIN_CD_TO");
            sql.Append(" AND BUMON_CD BETWEEN @BUMON_CD_FR AND @BUMON_CD_TO");
            sql.Append(" AND BUKA_CD BETWEEN @BUKA_CD_FR AND @BUKA_CD_TO");
            if (this.rdoTaisho1.Checked)
            {
                sql.Append(" AND TAISHOKU_NENGAPPI IS NULL");
            }
            else
            {
                sql.Append(" AND TAISHOKU_NENGAPPI IS NOT NULL");
            }
            // ORDER句
            if (this.rdoOrder1.Checked)
            {
                sql.Append(" ORDER BY KAISHA_CD ASC,SHAIN_CD ASC");                            // 社員順
            }
            else
            {
                sql.Append(" ORDER BY KAISHA_CD ASC,BUMON_CD ASC,BUKA_CD ASC,SHAIN_CD ASC");   // 部門順
            }
            #endregion

            DataTable dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);
            if (dtMainLoop.Rows.Count == 0)
            {
                return false;
            }
            else
            {
                _sort = 0;                                                                  // SORTフィールドカウンタ初期化

                string kyuyoMeisaiSql = GetKyKyuShoMeisaiSql(1);                            // 給与明細集計用SQL文字列
                string shoyoMeisaiSql = GetKyKyuShoMeisaiSql(2);                            // 賞与明細集計用SQL文字列
                string kaishaNm;

                string[] shain = new string[4];                                             // 社員情報
                decimal[] kingaku = new decimal[24];                                        // 金額情報
                string[] kubun = new string[29];                                            // 区分情報

                //※アイテム153まで拡張
                string[] item = new string[153];

                foreach (DataRow drS in dtMainLoop.Rows)
                {
                    // 配列値初期化
                    for (int i = 0; i < 153; i++) item[i] = "";

                    #region 基本情報をセット
                    item[0] = string.Format("{0:D6}", Util.ToInt(drS["SHAIN_CD"]));                         // 社員コード
                    item[10] = string.Format("{0} {2}年度分", jpNendo);                                         // 票年度
                    item[11] = string.Format("{0}{2}年度分", Util.ConvJpDate(nendo.AddYears(1), this.Dba));     // 給報年度
                    #endregion

                    #region 社員情報をセット
                    item[21] = (!ValChk.IsEmpty(drS["YUBIN_BANGO1"])) ?                                         // 郵便番号
                            "〒" + Util.ToString(drS["YUBIN_BANGO1"]) + "－" + Util.ToString(drS["YUBIN_BANGO2"]) : "";
                    item[22] = Util.ToString(drS["JUSHO1"]);                                                    // 住所１ 
                    item[23] = Util.ToString(drS["JUSHO2"]);                                                    // 住所２
                    item[24] = Util.ToString(drS["SHAIN_CD"]);                                                  // 受給者番号(社員CD)
                    item[25] = Util.ToString(drS["SHAIN_KANA_NM"]);                                             // フリガナ
                    item[26] = Util.ToString(drS["YAKUSHOKU_NM"]);                                              // 役職名
                    item[27] = Util.ToString(drS["SHAIN_NM"]);                                                  // 氏名
                    item[28] = Util.ToString(drS["KYUYO_SHUBETSU"]);                                            // 種別
                    if (!ValChk.IsEmpty(drS["NYUSHA_NENGAPPI"]))                                                // 中途就職
                    {
                        if (Util.ToDate(drS["NYUSHA_NENGAPPI"]).Year == nendo.Year)
                        {
                            item[80] = "○";
                            string[] jpDate = Util.ConvJpDate(Util.ToString(drS["NYUSHA_NENGAPPI"]), this.Dba);
                            item[81] = jpDate[2];
                            item[82] = jpDate[3];
                            item[83] = jpDate[4];
                        }
                    }
                    if (!ValChk.IsEmpty(drS["TAISHOKU_NENGAPPI"]))                                              // 中途退職
                    {
                        if (Util.ToDate(drS["TAISHOKU_NENGAPPI"]).Year == nendo.Year)
                        {
                            string[] jpDate = Util.ConvJpDate(Util.ToString(drS["TAISHOKU_NENGAPPI"]), this.Dba);
                            // 年内の中途入社の場合は摘要欄に記載。
                            if (item[80] ==  "○")
                            {
                                kaishaNm = Util.ToString(dtKaishaJoho.Rows[0]["KAISHA_NM"]);
                                kaishaNm = kaishaNm.Replace(" ", "").Replace("　", "");
                                item[152] = kaishaNm + "    " + this.lblGengoNendo.Text + jpDate[2] + "年" + jpDate[3] + "月" + jpDate[4] + "日退職";
                            }
                            // それ以外はそのまま表示
                            else
                            {
                                item[84] = "○";
                                item[85] = jpDate[2];
                                item[86] = jpDate[3];
                                item[87] = jpDate[4];
                            }
                        }
                    }
                    if (!ValChk.IsEmpty(drS["SEINENGAPPI"]))
                    {
                        string[] jpDate = Util.ConvJpDate(Util.ToString(drS["SEINENGAPPI"]), this.Dba);
                        switch (Util.ToString(jpDate[1]))
                        {
                            case "M":
                                item[88] = "○";
                                break;
                            case "T":
                                item[89] = "○";
                                break;
                            case "S":
                                item[90] = "○";
                                break;
                            case "H":
                                item[91] = "○";
                                break;
                        }
                        item[92] = jpDate[2];
                        item[93] = jpDate[3];
                        item[94] = jpDate[4];
                    }
                    #endregion

                    #region 会社情報をセット
                    item[95] = Util.ToString(dtKaishaJoho.Rows[0]["JUSHO1"]);
                    item[96] = Util.ToString(dtKaishaJoho.Rows[0]["KAISHA_NM"]);
                    item[97] = "(電話) " + Util.ToString(dtKaishaJoho.Rows[0]["DENWA_BANGO"]);

                    #region 平成２８年度法改正で源泉徴収に関わってくるコード（マイナンバー対応）
                    // 法人番号をセット
                    string kaisaNo = Mynumber.GetmyNumber(Util.ToInt(this.UInfo.KaishaCd), this.Dba);
                    if (kaisaNo != "0000000000000")
                    {
                        item[99] = kaisaNo;
                    }
                    else
                    {
                        item[99] = "";
                    }
                    //※　マイナンバー摘要後（源泉徴収は社員のみを想定しているので固定で区分は１を設定）
                    string myno = Mynumber.GetmyNumber(Util.ToInt(drS["SHAIN_CD"]), 1, 0, this.Dba);
                    if (myno != "000000000000")
                    {
                        item[100] = myno;//　ITEM101
                    }
                    else
                    {
                        item[100] = "";
                    }
                    // 101~start
                    item[101] = Util.ToString(drS["FUYO_HAIGUSHA_KANA_NM"]);　　　　　　　　　　　　//控除対象配偶者カナ名//ITEM102
                    item[102] = Util.ToString(drS["FUYO_HAIGUSHA_NM"]);　　　　　　　　　　　　//控除対象配偶者名//ITEM103
                    item[103] = (Util.ToInt(drS["HAIGUSHA_KUBUN"]) == 1) ? "○" : ""; 　　　　//控除対象配偶者住居//ITEM104
                    item[104] = Mynumber.GetmyNumber(Util.ToInt(drS["SHAIN_CD"]), 1, 9, this.Dba);　　　　　//控除対象配偶者マイナンバー//ITEM105
                    if (item[104] != "000000000000")
                    {
                        item[104] = Mynumber.GetmyNumber(Util.ToInt(drS["SHAIN_CD"]), 1, 9, this.Dba);　　　　　//控除対象配偶者マイナンバー//ITEM105
                    }
                    else
                    {
                        item[104] = "";　　　　　//控除対象配偶者マイナンバー
                    }

                    item[105] = Util.ToString(drS["FUYO1_KANA_NM"]);　　　　　　　　　　　　//控除対象カナ名1
                    item[106] = Util.ToString(drS["FUYO1_NM"]);　　　　　　　　　//控除対象名1
                    item[107] = (Util.ToInt(drS["FUYO1_JUKYO"]) == 1) ? "○" : ""; 　　　　//控除対象住居1
                    item[108] = Mynumber.GetmyNumber(Util.ToInt(drS["SHAIN_CD"]), 1, 1, this.Dba);　　　　　//控除対象マイナンバー1
                    if (item[108] != "000000000000")
                    {
                        item[108] = Mynumber.GetmyNumber(Util.ToInt(drS["SHAIN_CD"]), 1, 1, this.Dba);　　　　　//控除対象マイナンバー1
                    }
                    else
                    {
                        item[108] = "";　　　　　//控除対象配偶者マイナンバー
                    }

                    item[109] = Util.ToString(drS["FUYO2_KANA_NM"]);　　　　　　　　　　　　//控除対象カナ名2
                    item[110] = Util.ToString(drS["FUYO2_NM"]);　　　　　　　　　//控除対象名2
                    item[111] = (Util.ToInt(drS["FUYO2_JUKYO"]) == 1) ? "○" : ""; 　　　　//控除対象住居2
                    item[112] = Mynumber.GetmyNumber(Util.ToInt(drS["SHAIN_CD"]), 1, 2, this.Dba);　　　　　//控除対象マイナンバー2
                    if (item[112] != "000000000000")
                    {
                        item[112] = Mynumber.GetmyNumber(Util.ToInt(drS["SHAIN_CD"]), 1, 2, this.Dba);　　　　　//控除対象マイナンバー2
                    }
                    else
                    {
                        item[112] = "";　　　　　//控除対象配偶者マイナンバー
                    }

                    item[113] = Util.ToString(drS["FUYO3_KANA_NM"]);　　　　　　　　　　　　//控除対象カナ名3
                    item[114] = Util.ToString(drS["FUYO3_NM"]);　　　　　　　　　//控除対象名3
                    item[115] = (Util.ToInt(drS["FUYO3_JUKYO"]) == 1) ? "○" : ""; 　　　　//控除対象住居3
                    item[116] = Mynumber.GetmyNumber(Util.ToInt(drS["SHAIN_CD"]), 1, 3, this.Dba);　　　　　//控除対象マイナンバー3
                    if (item[116] != "000000000000")
                    {
                        item[116] = Mynumber.GetmyNumber(Util.ToInt(drS["SHAIN_CD"]), 1, 3, this.Dba);　　　　　//控除対象マイナンバー3　
                    }
                    else
                    {
                        item[116] = "";　　　　　//控除対象配偶者マイナンバー
                    }

                    item[117] = Util.ToString(drS["FUYO4_KANA_NM"]);　　　　　　　　　　　　//控除対象カナ名4
                    item[118] = Util.ToString(drS["FUYO4_NM"]);　　　　　　　　　//控除対象名4
                    item[119] = (Util.ToInt(drS["FUYO4_JUKYO"]) == 1) ? "○" : ""; 　　　　//控除対象住居4
                    item[120] = Mynumber.GetmyNumber(Util.ToInt(drS["SHAIN_CD"]), 1, 4, this.Dba);　　　　　//控除対象マイナンバー4　
                    if (item[120] != "000000000000")
                    {
                        item[120] = Mynumber.GetmyNumber(Util.ToInt(drS["SHAIN_CD"]), 1, 4, this.Dba);　　　　　//控除対象マイナンバー4　
                    }
                    else
                    {
                        item[120] = "";　　　　　//控除対象配偶者マイナンバー
                    }

                    /*　16歳未満の控除対象配偶者　*/
                    item[121] = Util.ToString(drS["FUYO1_MIMAN_KANA_NM"]);　　　　　　　　　　　　//控除対象カナ名1
                    item[122] = Util.ToString(drS["FUYO1_MIMAN_NM"]);　　　　　　　　　//控除対象名1
                    item[123] = (Util.ToInt(drS["FUYO1_MIMAN_JUKYO"]) == 1) ? "○" : ""; 　　　　//控除対象住居1
                    item[124] = Mynumber.GetmyNumber(Util.ToInt(drS["SHAIN_CD"]), 1, 5, this.Dba);　　　　　//控除対象マイナンバー1　
                    if (item[124] != "000000000000")
                    {
                        item[124] = Mynumber.GetmyNumber(Util.ToInt(drS["SHAIN_CD"]), 1, 5, this.Dba);　　　　　//控除対象マイナンバー1　
                    }
                    else
                    {
                        item[124] = "";　　　　　//控除対象配偶者マイナンバー
                    }

                    item[125] = Util.ToString(drS["FUYO2_MIMAN_KANA_NM"]);　　　　　　　　　　　　//控除対象カナ名2
                    item[126] = Util.ToString(drS["FUYO2_MIMAN_NM"]);　　　　　　　　　//控除対象名2
                    item[127] = (Util.ToInt(drS["FUYO2_MIMAN_JUKYO"]) == 1) ? "○" : ""; 　　　　//控除対象住居2
                    item[128] = Mynumber.GetmyNumber(Util.ToInt(drS["SHAIN_CD"]), 1, 6, this.Dba);　　　　　//控除対象マイナンバー2
                    if (item[128] != "000000000000")
                    {
                        item[128] = Mynumber.GetmyNumber(Util.ToInt(drS["SHAIN_CD"]), 1, 6, this.Dba);　　　　　//控除対象マイナンバー2
                    }
                    else
                    {
                        item[128] = "";　　　　　//控除対象配偶者マイナンバー
                    }

                    item[129] = Util.ToString(drS["FUYO3_MIMAN_KANA_NM"]);　　　　　　　　　　　　//控除対象カナ名3
                    item[130] = Util.ToString(drS["FUYO3_MIMAN_NM"]);　　　　　　　　　//控除対象名3
                    item[131] = (Util.ToInt(drS["FUYO3_MIMAN_JUKYO"]) == 1) ? "○" : ""; 　　　　//控除対象住居3
                    item[132] = Mynumber.GetmyNumber(Util.ToInt(drS["SHAIN_CD"]), 1, 7, this.Dba);　　　　　//控除対象マイナンバー3　
                    if (item[132] != "000000000000")
                    {
                        item[132] = Mynumber.GetmyNumber(Util.ToInt(drS["SHAIN_CD"]), 1, 7, this.Dba);　　　　　//控除対象マイナンバー3　
                    }
                    else
                    {
                        item[132] = "";　　　　　//控除対象配偶者マイナンバー
                    }

                    item[133] = Util.ToString(drS["FUYO4_MIMAN_KANA_NM"]);　　　　　　　　　　　　//控除対象カナ名4
                    item[134] = Util.ToString(drS["FUYO4_MIMAN_NM"]);　　　　　　　　　//控除対象名4
                    item[135] = (Util.ToInt(drS["FUYO4_MIMAN_JUKYO"]) == 1) ? "○" : ""; 　　　　//控除対象住居4
                    item[136] = Mynumber.GetmyNumber(Util.ToInt(drS["SHAIN_CD"]), 1, 8, this.Dba);　　　　　//控除対象マイナンバー4
                    if (item[136] != "000000000000")
                    {
                        item[136] = Mynumber.GetmyNumber(Util.ToInt(drS["SHAIN_CD"]), 1, 8, this.Dba);　　　　　//控除対象マイナンバー4
                    }
                    else
                    {
                        item[136] = "";　　　　　//控除対象配偶者マイナンバー
                    }
                            // 140~
                    item[140] = Util.ToString(drS["FUYO_BIKOU"]);　　　　　　　　　//5人目以降の扶養親族備考欄
                    item[141] = Util.ToString(drS["FUYO_MIMAN_BIKOU"]);　　　　　　　　　　　　//5人目以降の16歳未満扶養親族備考欄
                    #endregion

                    #endregion

                    // TB年末調整データ取得
                    dpc = new DbParamCollection();
                    dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 2, this.UInfo.KaishaCd);
                    dpc.SetParam("@NENDO", SqlDbType.DateTime, nendo);
                    dpc.SetParam("@SHAIN_CD", SqlDbType.Decimal, 6, 0, Util.ToDecimal(drS["SHAIN_CD"]));
                    sql = new StringBuilder();
                    sql.Append("SELECT * FROM TB_KY_NENMATSU_CHOSEI");
                    sql.Append(" WHERE KAISHA_CD = @KAISHA_CD");
                    sql.Append(" AND NENDO = @NENDO");
                    sql.Append(" AND SHAIN_CD = @SHAIN_CD");
                    DataTable dtNenmatsuChosei = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);
                    // 年調入力、処理もしていない方
                    if (dtNenmatsuChosei.Rows.Count == 0)
                    {
                        #region 年調未済の場合　TB給与/賞与明細データを出力(支給実績がある場合)

                        // 給与賞与明細データ抽出パラメータ
                        dpc = new DbParamCollection();
                        dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 2, this.UInfo.KaishaCd);
                        dpc.SetParam("@SHAIN_CD", SqlDbType.Decimal, 6, 0, Util.ToDecimal(drS["SHAIN_CD"]));
                        dpc.SetParam("@NENGETSU_FR", SqlDbType.DateTime, nendo);
                        dpc.SetParam("@NENGETSU_TO", SqlDbType.DateTime, nendo.AddMonths(11));

                        decimal kazeigaku = 0;
                        decimal shakaihokenryo = 0;
                        decimal shotokuzei = 0;

                        // 給与明細集計
                        dt = this.Dba.GetDataTableFromSqlWithParams(kyuyoMeisaiSql, dpc);
                        if (dt.Rows.Count > 0)
                        {
                            kazeigaku = Util.ToDecimal(dt.Rows[0]["KAZEIGAKU"]);                               // 給与等金額
                            shakaihokenryo = Util.ToDecimal(dt.Rows[0]["SHAKAIHOKENRYO"]);                          // 社会保険(給与)
                            shotokuzei = Util.ToDecimal(dt.Rows[0]["SHOTOKUZEI"]);                             // 徴収税額
                        }
                        // 賞与明細集計
                        dt = this.Dba.GetDataTableFromSqlWithParams(shoyoMeisaiSql, dpc);
                        if (dt.Rows.Count > 0)
                        {
                            kazeigaku += Util.ToDecimal(dt.Rows[0]["KAZEIGAKU"]);                               // 給与等金額
                            shakaihokenryo += Util.ToDecimal(dt.Rows[0]["SHAKAIHOKENRYO"]);                         // 社会保険(給与)
                            shotokuzei += Util.ToDecimal(dt.Rows[0]["SHOTOKUZEI"]);                             // 徴収税額
                        }

                        // 支給実績がある場合、ワークへ出力
                        if (kazeigaku != 0 || shakaihokenryo != 0 || shotokuzei != 0)
                        {
                            // TB社員情報から区分情報を取得
                            item[29] = Util.FormatNum(kazeigaku);                                               // 給与等金額

                            item[30] = Util.FormatNum(0);                              // 給与所得控除後の金額
                            item[31] = Util.FormatNum(0);                                          // 所得控除の額の合計額
                            item[32] = Util.FormatNum(shotokuzei);                                              // 源泉徴収額
                            item[33] = (Util.ToInt(drS["HAIGUSHA_KUBUN"]) == 1) ? "○" : "";                    // 配偶者有
                            item[34] = (Util.ToInt(drS["HAIGUSHA_KUBUN"]) == 0) ? "○" : "";                    // 配偶者無
                            item[35] = (Util.ToInt(drS["HAIGUSHA_KUBUN"]) == 2) ? "○" : "";                    // 配偶者老人
                            //item[36] = // 配偶者特別控除の額
                            item[37] = this.FormatNum(Util.ToInt(drS["FUYO_TOKUTEI"]));                         // 扶養・特定
                            item[38] = this.FormatNum(Util.ToInt(drS["FUYO_ROJIN_DOKYO_ROSHINTO"]));            // 扶養・老人・同居
                            item[39] = this.FormatNum(Util.ToInt(drS["FUYO_ROJIN_DOKYO_ROSHINTO"])              // 扶養・老人・計
                                                        + Util.ToInt(drS["FUYO_ROJIN_DOKYO_ROSHINTO_IGAI"]));
                            item[40] = this.FormatNum(Util.ToInt(drS["FUYO_IPPAN"])                             // 扶養・その他
                                                    - Util.ToInt(drS["FUYO_TOKUTEI"])
                                                    - Util.ToInt(drS["FUYO_ROJIN_DOKYO_ROSHINTO"])
                                                    - Util.ToInt(drS["FUYO_ROJIN_DOKYO_ROSHINTO_IGAI"]));
                            item[41] = this.FormatNum(Util.ToInt(drS["FUYO_DOKYO_TOKUSHO_IPPAN"]));             // 障害・特別・同居
                            item[42] = this.FormatNum(Util.ToInt(drS["SHOGAISHA_TOKUBETSU"])                    // 障害・特別・計
                                                        + Util.ToInt(drS["FUYO_DOKYO_TOKUSHO_IPPAN"]));
                            item[43] = this.FormatNum(Util.ToInt(drS["SHOGAISHA_IPPAN"]));                      // 障害・その他
                            item[44] = this.FormatNum(shakaihokenryo);                                          // 社会保険料の金額
                            //item[45] = // 社会保険料内書
                            //item[46] = // 生命保険料の控除額
                            //item[47] = // 地震保険料の控除額
                            //item[48] = // 住宅借入金等特別控除額(実控除額)
                            item[49] = "";                                                 // 住宅借入金等特別控除額(控除可能額)
                            item[50] = "";                                                 // 国民年金保険料等の金額
                            item[52] = Util.ToString(drS["TEKIYO1"]);                                           // 摘要１
                            item[53] = Util.ToString(drS["TEKIYO2"]);                                           // 摘要２
                            //    item[54] = "中途収入金額";
                            //    item[55] = "中途社会保険";
                            //    item[56] = "中途所得税";
                            //    item[57] = // 中途収入金額
                            //    item[58] = // 中途社会保険
                            //    item[59] = // 中途所得税
                            //    item[142] = // 前職名称等、備考欄
                            //    item[143] = // 前職退職年月日
                            //item[60] = // 配偶者の合計所得
                            //item[61] = // 新生命保険料の金額
                            //item[62] = // 旧生命保険料の金額
                            //item[63] = // 介護医療保険料の金額
                            //item[64] = // 新個人年金保険料の金額
                            //item[65] = // 旧個人年金保険料の金額
                            //item[66] = // 旧長期損害保険料の金額
                            item[67] = this.FormatNum(Util.ToInt(drS["SAI16_MIMAN"]));                           // 16歳未満
                            item[137] = this.FormatNum(Util.ToInt(drS["HIJUKYO_SHINZOKU"]));                           // 非住居親族数
                            item[68] = (Util.ToInt(drS["MISEINENSHA"]) == 1) ? "○" : "";                        // 本人未成年者
                            item[69] = (Util.ToInt(drS["GAIKOKUJIN"]) == 1) ? "○" : "";                         // 本人外国人
                            item[70] = (Util.ToInt(drS["SHIBO_TAISHOKU"]) == 1) ? "○" : "";                     // 本人死亡退職
                            item[71] = (Util.ToInt(drS["SAIGAISHA"]) == 1) ? "○" : "";                          // 本人災害者
                            item[72] = (Util.ToInt(drS["ZEIGAKU_HYO"]) == 2) ? "○" : "";                        // 本人乙欄
                            item[73] = (Util.ToInt(drS["HONNIN_KUBUN2"]) == 2) ? "○" : "";                      // 本人障害者特別
                            item[74] = (Util.ToInt(drS["HONNIN_KUBUN2"]) == 1) ? "○" : "";                      // 本人障害者その他
                            item[75] = (Util.ToInt(drS["HONNIN_KUBUN1"]) == 2) ? "○" : "";                      // 本人寡婦一般
                            item[76] = (Util.ToInt(drS["HONNIN_KUBUN1"]) == 3) ? "○" : "";                      // 本人寡婦特別
                            item[77] = (Util.ToInt(drS["HONNIN_KUBUN1"]) == 4) ? "○" : "";                      // 本人寡夫
                            item[78] = (Util.ToInt(drS["HONNIN_KUBUN3"]) == 1) ? "○" : "";                      // 本人勤労学生

                            // ワークレコード追加
                            alParams = SetPrKyTblParams(item);                                               // 票レコード
                            this.Dba.Insert("PR_KY_TBL", (DbParamCollection)alParams[0]);
                            //// トランザクションをコミット
                            //this.Dba.Commit();
                        }

                        #endregion
                    }
                    else
                    {
                        DataRow drN = dtNenmatsuChosei.Rows[0];
                        // 年末調整を行う人
                        if ((Util.ToDecimal(drS["NENMATSU_CHOSEI"]) == 1 && Util.ToString(drS["TAISHOKU_NENGAPPI"]) == ""))// || Util.ToDecimal(drN["KEISAN_F"]) == 1)
                        {
                            #region 年調済みの場合　TB年末調整データを出力

                            // TB年末調整データからセット
                            item[29] = Util.FormatNum(                                                          // 給与等金額
                                        Util.ToDecimal(drN["KYUYOTO_KAZEI_KINGAKU"])
                                        + Util.ToDecimal(drN["SHOYOTO_KAZEI_KINGAKU"])
                                        + Util.ToDecimal(drN["ZENSHOKUBUN_KAZEI_KYUYOGAKU"])
                                        + Util.ToDecimal(drN["CHOSEI_KYUYO_KAZEI_KYUYOGAKU"])
                                        + Util.ToDecimal(drN["CHOSEI_SHOYO_KAZEI_KYUYOGAKU"]));
                            item[30] = Util.FormatNum(                                                          // 給与所得控除後の金額
                                        Util.ToDecimal(drN["KYUYO_SHOTOKU_KOJOGO_NO_KNGK"]));
                            item[31] = Util.FormatNum(                                                          // 所得控除の額の合計額
                                        Util.ToDecimal(drN["SHAKAI_HOKENRYO_KJGK_KYUYOTO"])
                                        + Util.ToDecimal(drN["CHOSEI_KYUYO_SHAKAI_HOKENRYO"])
                                        + Util.ToDecimal(drN["CHOSEI_SHOYO_SHAKAI_HOKENRYO"])
                                        + Util.ToDecimal(drN["ZENSHOKUBUN_SHAKAI_HOKENRYO"])
                                        + Util.ToDecimal(drN["SHAKAI_HOKENRYO_KJGK_SNKKBN"])
                                        + Util.ToDecimal(drN["SKB_KIGYO_KYOSAI_KAKEKIN_KJGK"])
                                        + Util.ToDecimal(drN["SEIMEIHOKENRYO_KOJOGAKU"])
                                        + Util.ToDecimal(drN["SONGAIHOKENRYO_KOJOGAKU"])
                                        + Util.ToDecimal(drN["HAIGUSHA_TOKUBETSU_KOJOGAKU"])
                                        + Util.ToDecimal(drN["HAIGUSHA_KOJOGAKU"])
                                        + Util.ToDecimal(drN["FUYO_KOJOGAKU"])
                                        + Util.ToDecimal(drN["KISO_KOJOGAKU"])
                                        + Util.ToDecimal(drN["SHOGAISHA_KOJOGAKU"])
                                        + Util.ToDecimal(drN["RONENSHA_KAFU_KOJOGAKU"])
                                        + Util.ToDecimal(drN["KINRO_GAKUSEI_KOJOGAKU"]));
                            item[32] = Util.FormatNum(                                                          // 源泉徴収額
                                        Util.ToDecimal(drN["NENZEIGAKU"]));
                            item[33] = (Util.ToInt(drN["HAIGUSHA_KUBUN"]) == 1) ? "○" : "";                    // 配偶者有
                            item[34] = (Util.ToInt(drN["HAIGUSHA_KUBUN"]) == 0) ? "○" : "";                    // 配偶者無
                            item[35] = (Util.ToInt(drN["HAIGUSHA_KUBUN"]) == 2) ? "○" : "";                    // 配偶者老人
                            item[36] = this.FormatNum(                                                          // 配偶者特別控除の額
                                        Util.ToDecimal(drN["HAIGUSHA_TOKUBETSU_KOJOGAKU"]));
                            item[37] = this.FormatNum(Util.ToInt(drN["FUYO_TOKUTEI"]));                         // 扶養・特定
                            item[38] = this.FormatNum(Util.ToInt(drN["FUYO_ROZIN_DOKYO_ROSHINTO"]));            // 扶養・老人・同居
                            item[39] = this.FormatNum(Util.ToInt(drN["FUYO_ROZIN_DOKYO_ROSHINTO"])              // 扶養・老人・計
                                                        + Util.ToInt(drN["FUYO_ROZIN_DOKYO_ROSHINTO_IGAI"]));
                            item[40] = this.FormatNum(Util.ToInt(drN["FUYO_IPPAN"])                             // 扶養・その他
                                                        - Util.ToInt(drN["FUYO_TOKUTEI"])
                                                        - Util.ToInt(drN["FUYO_ROZIN_DOKYO_ROSHINTO"])
                                                        - Util.ToInt(drN["FUYO_ROZIN_DOKYO_ROSHINTO_IGAI"]));
                            item[41] = this.FormatNum(Util.ToInt(drN["FUYO_DOKYO_TOKUSHO_IPPAN"]));             // 障害・特別・同居
                            item[42] = this.FormatNum(Util.ToInt(drN["SHOGAISHA_TOKUBETSU"])                    // 障害・特別・計
                                                        + Util.ToInt(drN["FUYO_DOKYO_TOKUSHO_IPPAN"]));
                            item[43] = this.FormatNum(Util.ToInt(drN["SHOGAISHA_IPPAN"]));                      // 障害・その他
                            item[44] = this.FormatNum(                                                          // 社会保険料の金額
                                        Util.ToDecimal(drN["SHAKAI_HOKENRYO_KJGK_KYUYOTO"])
                                        + Util.ToDecimal(drN["CHOSEI_KYUYO_SHAKAI_HOKENRYO"])
                                        + Util.ToDecimal(drN["CHOSEI_SHOYO_SHAKAI_HOKENRYO"])
                                        + Util.ToDecimal(drN["ZENSHOKUBUN_SHAKAI_HOKENRYO"])
                                        + Util.ToDecimal(drN["SHAKAI_HOKENRYO_KJGK_SNKKBN"])
                                        + Util.ToDecimal(drN["SKB_KIGYO_KYOSAI_KAKEKIN_KJGK"]));
                            item[45] = (Util.ToDecimal(drN["SKB_KIGYO_KYOSAI_KAKEKIN_KJGK"]) != 0) ?           // 社会保険料内書
                                        "(" + this.FormatNum(
                                                    Util.ToDecimal(drN["SKB_KIGYO_KYOSAI_KAKEKIN_KJGK"])).PadLeft(9) + ")" : "";
                            item[46] = this.FormatNum(Util.ToDecimal(drN["SEIMEIHOKENRYO_KOJOGAKU"]));         // 生命保険料の控除額
                            item[47] = this.FormatNum(Util.ToDecimal(drN["SONGAIHOKENRYO_KOJOGAKU"]));         // 地震保険料の控除額
                            item[48] = this.FormatNum(                                              // 住宅借入金等特別控除額(実控除額)
                                                    Util.ToDecimal(drN["SANSHUTSU_NENZEIGAKU"])
                                                        - Util.ToDecimal(drN["NENCHO_NENZEIGAKU"]));
                            if (Util.ToDecimal(drN["JUTAKU_SHUTOKUTO_TKBT_KOJOGAKU"]) > Util.ToDecimal(drN["SANSHUTSU_NENZEIGAKU"]))
                            {
                                item[49] = this.FormatNum(                                          // 住宅借入金等特別控除額(控除可能額)
                                                    Util.ToDecimal(drN["JUTAKU_SHUTOKUTO_TKBT_KOJOGAKU"]));
                            }
                            else
                            {
                                item[49] = "";
                            }
                            item[50] = this.FormatNum(                                              // 国民年金保険料等の金額
                                                    Util.ToDecimal(drN["KOKUMIN_NENKIN_HOKENRYO_SHRIGK"]));
                            if (!ValChk.IsEmpty(drN["IJU_KAISHI_DATE1"]))
                            {
                                string[] jpDate = Util.ConvJpDate(Util.ToString(drN["IJU_KAISHI_DATE1"]), this.Dba);
                                item[51] = jpDate[2];
                                item[138] = jpDate[3];
                                item[139] = jpDate[4];
                                // if (Util.ToDate(drS["TAISHOKU_NENGAPPI"]).Year == nendo.Year)
                                //item[51] = string.Format("{0}{2}年{3}月{4}日"                  // 居住開始年月日
                                //                        , Util.ConvJpDate(Util.ToDate(drN["IJU_KAISHI_DATE"]), this.Dba));
                            }
                            if (!ValChk.IsEmpty(drN["IJU_KAISHI_DATE2"]))
                            {
                                string[] jpDate = Util.ConvJpDate(Util.ToString(drN["IJU_KAISHI_DATE2"]), this.Dba);
                                item[144] = jpDate[2];// ITEM145
                                item[145] = jpDate[3];
                                item[146] = jpDate[4];
                            }
                            item[147] = Util.ToString(drN["JUTAKU_KARIIRETO_TKBT_KOJO_KUBUN1"]);　　// 住宅借入金等特別控除区分(1回目)
                            item[148] = Util.ToString(drN["JUTAKU_KARIIRETO_TKBT_KOJO_KUBUN2"]);　　// 住宅借入金等特別控除区分(2回目)
                            if (Util.ToInt(drN["JUTAKU_KARIIRETO_NENMATSUZAN1"]) != 0)
                            {
                                item[149] = this.FormatNum(drN["JUTAKU_KARIIRETO_NENMATSUZAN1"]);　　　// 住宅借入金等年末残高(1回目)
                            }
                            else
                            {
                                item[149] = "";
                            }
                            if (Util.ToInt(drN["JUTAKU_KARIIRETO_NENMATSUZAN2"]) != 0)
                            {
                                item[150] = this.FormatNum(drN["JUTAKU_KARIIRETO_NENMATSUZAN2"]);　　　// 住宅借入金等年末残高(2回目)
                            }
                            else
                            {
                                item[150] = "";
                            }
                            if (Util.ToInt(drN["JUTAKU_KARIIRETO_TKBT_KOJOSU"]) != 0)
                            {
                                item[151] = Util.ToString(drN["JUTAKU_KARIIRETO_TKBT_KOJOSU"]); 　　　// 住宅借入金等特別控除適用数
                            }
                            else
                            {
                                item[151] = ""; 　　　// ITEM152
                            }


                            item[52] = Util.ToString(drN["TEKIYO1"]);                                           // 摘要１
                            item[53] = Util.ToString(drN["TEKIYO2"]);                                           // 摘要２
                            if (Util.ToDecimal(drN["ZENSHOKUBUN_KAZEI_KYUYOGAKU"]) != 0
                                || Util.ToDecimal(drN["ZENSHOKUBUN_SHAKAI_HOKENRYO"]) != 0
                                || Util.ToDecimal(drN["ZENSHOKUBUN_SHOTOKUZEIGAKU"]) != 0)
                            {
                                item[54] = "中途収入金額";
                                item[55] = "中途社会保険";
                                item[56] = "中途所得税";
                                item[57] = this.FormatNum(                                                      // 中途収入金額
                                                Util.ToDecimal(drN["ZENSHOKUBUN_KAZEI_KYUYOGAKU"])) + "円";
                                item[58] = this.FormatNum(                                                      // 中途社会保険
                                                Util.ToDecimal(drN["ZENSHOKUBUN_SHAKAI_HOKENRYO"])) + "円";
                                item[59] = this.FormatNum(                                                      // 中途所得税
                                                Util.ToDecimal(drN["ZENSHOKUBUN_SHOTOKUZEIGAKU"])) + "円";

                            }
                            item[142] = Util.ToString(drS["ZENSHOKU_BIKOU"]);　　　　　　　　　　　　//前職名称等、備考欄
                            if (!ValChk.IsEmpty(drN["ZENTAISHOKU_DATE"]))
                            {
                                item[143] = string.Format("前職退職年月日　{0}{2}年{3}月{4}日"                  // 前職退職年月日
                                                        , Util.ConvJpDate(Util.ToDate(drN["ZENTAISHOKU_DATE"]), this.Dba));
                            }
                            else
                            {
                                item[143] = "";
                            }

                            item[60] = this.FormatNum(Util.ToInt(drN["HAIGUSHA_GOKEI_SHOTOKU"])) + "　";         // 配偶者の合計所得
                            item[61] = this.FormatNum(Util.ToInt(drN["SHIN_SEIMEI_HOKENRYO"])) + "　";           // 新生命保険料の金額
                            item[62] = this.FormatNum(Util.ToInt(drN["KYU_SEIMEI_HOKENRYO"])) + "　";            // 旧生命保険料の金額
                            item[63] = this.FormatNum(Util.ToInt(drN["KAIGO_IRYO_HOKENRYO"])) + "　";            // 介護医療保険料の金額
                            item[64] = this.FormatNum(Util.ToInt(drN["SHIN_KOJIN_NENKIN_HOKENRYO"])) + "　";     // 新個人年金保険料の金額
                            item[65] = this.FormatNum(Util.ToInt(drN["KOJIN_NENKIN_HOKENRYO_SHRIGK"])) + "　";   // 旧個人年金保険料の金額
                            item[66] = this.FormatNum(Util.ToInt(drN["CHOKI_SONGAI_HOKENRYO_SHRIGK"])) + "　";   // 旧長期損害保険料の金額

                            item[67] = this.FormatNum(Util.ToInt(drN["SAI16_MIMAN"]));                           // 16歳未満
                            item[137] = this.FormatNum(Util.ToInt(drS["HIJUKYO_SHINZOKU"]));                           // 非住居親族数
                            item[68] = (Util.ToInt(drN["MISEINENSHA"]) == 1) ? "○" : "";                        // 本人未成年者
                            item[69] = (Util.ToInt(drN["GAIKOKUJIN"]) == 1) ? "○" : "";                         // 本人外国人
                            item[70] = (Util.ToInt(drN["SHIBO_TAISHOKU"]) == 1) ? "○" : "";                     // 本人死亡退職
                            item[71] = (Util.ToInt(drN["SAIGAISHA"]) == 1) ? "○" : "";                          // 本人災害者
                            item[72] = (Util.ToInt(drN["ZEIGAKU_HYO"]) == 2) ? "○" : "";                        // 本人乙欄
                            item[73] = (Util.ToInt(drN["HONNIN_KUBUN2"]) == 2) ? "○" : "";                      // 本人障害者特別
                            item[74] = (Util.ToInt(drN["HONNIN_KUBUN2"]) == 1) ? "○" : "";                      // 本人障害者その他
                            item[75] = (Util.ToInt(drN["HONNIN_KUBUN1"]) == 2) ? "○" : "";                      // 本人寡婦一般
                            item[76] = (Util.ToInt(drN["HONNIN_KUBUN1"]) == 3) ? "○" : "";                      // 本人寡婦特別
                            item[77] = (Util.ToInt(drN["HONNIN_KUBUN1"]) == 4) ? "○" : "";                      // 本人寡夫
                            item[78] = (Util.ToInt(drN["HONNIN_KUBUN3"]) == 1) ? "○" : "";                      // 本人勤労学生

                            // ワークレコード追加
                            alParams = SetPrKyTblParams(item);
                            this.Dba.Insert("PR_KY_TBL", (DbParamCollection)alParams[0]);
                            //// トランザクションをコミット
                            //this.Dba.Commit();

                            #endregion
                        }
                        // 年末調整を行わない人
                        else
                        {
                            #region 年調未済の場合　TB給与/賞与明細データを出力(支給実績がある場合)

                            // 給与賞与明細データ抽出パラメータ
                            dpc = new DbParamCollection();
                            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 2, this.UInfo.KaishaCd);
                            dpc.SetParam("@SHAIN_CD", SqlDbType.Decimal, 6, 0, Util.ToDecimal(drS["SHAIN_CD"]));
                            dpc.SetParam("@NENGETSU_FR", SqlDbType.DateTime, nendo);
                            dpc.SetParam("@NENGETSU_TO", SqlDbType.DateTime, nendo.AddMonths(11));

                            decimal kazeigaku = 0;
                            decimal shakaihokenryo = 0;
                            decimal shotokuzei = 0;

                            // 給与明細集計
                            dt = this.Dba.GetDataTableFromSqlWithParams(kyuyoMeisaiSql, dpc);
                            if (dt.Rows.Count > 0)
                            {
                                kazeigaku = Util.ToDecimal(dt.Rows[0]["KAZEIGAKU"]);                               // 給与等金額
                                shakaihokenryo = Util.ToDecimal(dt.Rows[0]["SHAKAIHOKENRYO"]);                          // 社会保険(給与)
                                shotokuzei = Util.ToDecimal(dt.Rows[0]["SHOTOKUZEI"]);                             // 徴収税額
                            }
                            shotokuzei += Util.ToDecimal(dtNenmatsuChosei.Rows[0]["CHOSEI_KYUYO_SHOTOKUZEIGAKU"]);
                            // 賞与明細集計
                            dt = this.Dba.GetDataTableFromSqlWithParams(shoyoMeisaiSql, dpc);
                            if (dt.Rows.Count > 0)
                            {
                                kazeigaku += Util.ToDecimal(dt.Rows[0]["KAZEIGAKU"]);                               // 給与等金額
                                shakaihokenryo += Util.ToDecimal(dt.Rows[0]["SHAKAIHOKENRYO"]);                         // 社会保険(給与)
                                shotokuzei += Util.ToDecimal(dt.Rows[0]["SHOTOKUZEI"]);                             // 徴収税額
                            }

                            // 支給実績がある場合、ワークへ出力
                            if (kazeigaku != 0 || shakaihokenryo != 0 || shotokuzei != 0)
                            {
                                // TB社員情報から区分情報を取得
                                item[29] = Util.FormatNum(kazeigaku);                                               // 給与等金額

                                item[30] = Util.FormatNum(0);                              // 給与所得控除後の金額
                                item[31] = Util.FormatNum(shakaihokenryo);                                          // 所得控除の額の合計額
                                item[32] = Util.FormatNum(shotokuzei);                                              // 源泉徴収額
                                item[33] = (Util.ToInt(drS["HAIGUSHA_KUBUN"]) == 1) ? "○" : "";                    // 配偶者有
                                item[34] = (Util.ToInt(drS["HAIGUSHA_KUBUN"]) == 0) ? "○" : "";                    // 配偶者無
                                item[35] = (Util.ToInt(drS["HAIGUSHA_KUBUN"]) == 2) ? "○" : "";                    // 配偶者老人
                                //item[36] = // 配偶者特別控除の額
                                item[37] = this.FormatNum(Util.ToInt(drS["FUYO_TOKUTEI"]));                         // 扶養・特定
                                item[38] = this.FormatNum(Util.ToInt(drS["FUYO_ROJIN_DOKYO_ROSHINTO"]));            // 扶養・老人・同居
                                item[39] = this.FormatNum(Util.ToInt(drS["FUYO_ROJIN_DOKYO_ROSHINTO"])              // 扶養・老人・計
                                                            + Util.ToInt(drS["FUYO_ROJIN_DOKYO_ROSHINTO_IGAI"]));
                                item[40] = this.FormatNum(Util.ToInt(drS["FUYO_IPPAN"])                             // 扶養・その他
                                                        - Util.ToInt(drS["FUYO_TOKUTEI"])
                                                        - Util.ToInt(drS["FUYO_ROJIN_DOKYO_ROSHINTO"])
                                                        - Util.ToInt(drS["FUYO_ROJIN_DOKYO_ROSHINTO_IGAI"]));
                                item[41] = this.FormatNum(Util.ToInt(drS["FUYO_DOKYO_TOKUSHO_IPPAN"]));             // 障害・特別・同居
                                item[42] = this.FormatNum(Util.ToInt(drS["SHOGAISHA_TOKUBETSU"])                    // 障害・特別・計
                                                            + Util.ToInt(drS["FUYO_DOKYO_TOKUSHO_IPPAN"]));
                                item[43] = this.FormatNum(Util.ToInt(drS["SHOGAISHA_IPPAN"]));                      // 障害・その他
                                item[44] = this.FormatNum(shakaihokenryo);                                          // 社会保険料の金額
                                //item[45] = // 社会保険料内書
                                //item[46] = // 生命保険料の控除額
                                //item[47] = // 地震保険料の控除額
                                //item[48] = // 住宅借入金等特別控除額(実控除額)
                                item[49] = "";                                                 // 住宅借入金等特別控除額(控除可能額)
                                item[50] = "";                                                 // 国民年金保険料等の金額
                                item[52] = Util.ToString(drS["TEKIYO1"]);                                           // 摘要１
                                item[53] = Util.ToString(drS["TEKIYO2"]);                                           // 摘要２
                                //    item[54] = "中途収入金額";
                                //    item[55] = "中途社会保険";
                                //    item[56] = "中途所得税";
                                //    item[57] = // 中途収入金額
                                //    item[58] = // 中途社会保険
                                //    item[59] = // 中途所得税
                                //    item[142] = // 前職名称等、備考欄
                                //    item[143] = // 前職退職年月日
                                //item[60] = // 配偶者の合計所得
                                //item[61] = // 新生命保険料の金額
                                //item[62] = // 旧生命保険料の金額
                                //item[63] = // 介護医療保険料の金額
                                //item[64] = // 新個人年金保険料の金額
                                //item[65] = // 旧個人年金保険料の金額
                                //item[66] = // 旧長期損害保険料の金額
                                item[67] = this.FormatNum(Util.ToInt(drS["SAI16_MIMAN"]));                           // 16歳未満
                                item[137] = this.FormatNum(Util.ToInt(drS["HIJUKYO_SHINZOKU"]));                           // 非住居親族数
                                item[68] = (Util.ToInt(drS["MISEINENSHA"]) == 1) ? "○" : "";                        // 本人未成年者
                                item[69] = (Util.ToInt(drS["GAIKOKUJIN"]) == 1) ? "○" : "";                         // 本人外国人
                                item[70] = (Util.ToInt(drS["SHIBO_TAISHOKU"]) == 1) ? "○" : "";                     // 本人死亡退職
                                item[71] = (Util.ToInt(drS["SAIGAISHA"]) == 1) ? "○" : "";                          // 本人災害者
                                item[72] = (Util.ToInt(drS["ZEIGAKU_HYO"]) == 2) ? "○" : "";                        // 本人乙欄
                                item[73] = (Util.ToInt(drS["HONNIN_KUBUN2"]) == 2) ? "○" : "";                      // 本人障害者特別
                                item[74] = (Util.ToInt(drS["HONNIN_KUBUN2"]) == 1) ? "○" : "";                      // 本人障害者その他
                                item[75] = (Util.ToInt(drS["HONNIN_KUBUN1"]) == 2) ? "○" : "";                      // 本人寡婦一般
                                item[76] = (Util.ToInt(drS["HONNIN_KUBUN1"]) == 3) ? "○" : "";                      // 本人寡婦特別
                                item[77] = (Util.ToInt(drS["HONNIN_KUBUN1"]) == 4) ? "○" : "";                      // 本人寡夫
                                item[78] = (Util.ToInt(drS["HONNIN_KUBUN3"]) == 1) ? "○" : "";                      // 本人勤労学生

                                // ワークレコード追加
                                alParams = SetPrKyTblParams(item);                                               // 票レコード
                                this.Dba.Insert("PR_KY_TBL", (DbParamCollection)alParams[0]);
                                //// トランザクションをコミット
                                //this.Dba.Commit();
                            }

                            #endregion
                        }
                    }
                }
            }

            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
                "SORT",
                "PR_KY_TBL",
                "GUID = @GUID",
                dpc);
            bool dataFlag;
            if (tmpdtPR_HN_TBL.Rows.Count > 0)
            {
                dataFlag = true;
            }
            else
            {
                dataFlag = false;
            }

            return dataFlag;
        }

        /// <summary>
        /// 給与/賞与明細データ照会SQLの取得
        /// </summary>
        /// <param name="type">1給与 2賞与</param>
        /// <returns></returns>
        private string GetKyKyuShoMeisaiSql(int type)
        {
            string kyuSho = (type == 1) ? "KYUYO" : "SHOYO";

            DataTable dtSti = (type == 1) ? _dtKyuSti : _dtShoSti;

            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT K2.SHAIN_CD AS SHAIN_CD");

            DataRow[] foundRows;
            string filter;
            string order;

            // 所得税課税額
            filter = "KOMOKU_SHUBETSU = 2 AND KOMOKU_KUBUN1 = 1";
            order = "KOMOKU_BANGO";
            foundRows = dtSti.Select(filter, order);
            sql.Append(", SUM(0");
            foreach (DataRow dr in foundRows)
            {
                sql.Append(" + ISNULL(K2.KOMOKU" + Util.ToString(dr["KOMOKU_BANGO"]) + " , 0)");
            }
            sql.Append(") AS KAZEIGAKU");
            // 社会保険料等
            filter = "KOMOKU_SHUBETSU = 3 AND KOMOKU_KUBUN3 >= 31 AND KOMOKU_KUBUN3 <= 33";
            order = "KOMOKU_BANGO";
            foundRows = dtSti.Select(filter, order);
            sql.Append(", SUM(0");
            foreach (DataRow dr in foundRows)
            {
                sql.Append(" + ISNULL(K3.KOMOKU" + Util.ToString(dr["KOMOKU_BANGO"]) + " , 0)");
            }
            sql.Append(") AS SHAKAIHOKENRYO");
            // 所得税
            filter = "KOMOKU_SHUBETSU = 3 AND (KOMOKU_KUBUN3 = 34 OR KOMOKU_KUBUN3 = 36)";               // 調整所得税を含む
            order = "KOMOKU_BANGO";
            foundRows = dtSti.Select(filter, order);
            sql.Append(", SUM(0");
            foreach (DataRow dr in foundRows)
            {
                sql.Append(" + ISNULL(K3.KOMOKU" + Util.ToString(dr["KOMOKU_BANGO"]) + " , 0)");
            }
            sql.Append(") AS SHOTOKUZEI");

            sql.Append(" FROM");
            sql.Append(" TB_KY_" + kyuSho + "_MEISAI_SHIKYU AS K2");
            sql.Append(" LEFT OUTER JOIN TB_KY_" + kyuSho + "_MEISAI_KOJO AS K3");
            sql.Append(" ON (K2.KAISHA_CD = K3.KAISHA_CD)");
            sql.Append(" AND (K2.SHAIN_CD = K3.SHAIN_CD)");
            sql.Append(" AND (K2.NENGETSU = K3.NENGETSU)");
            sql.Append(" WHERE K2.KAISHA_CD = @KAISHA_CD");
            sql.Append(" AND K2.SHAIN_CD = @SHAIN_CD");
            sql.Append(" AND K2.NENGETSU BETWEEN @NENGETSU_FR AND @NENGETSU_TO");
            sql.Append(" GROUP BY K2.SHAIN_CD");

            return Util.ToString(sql);
        }

        /// <summary>
        /// PR_KY_TBLに更新するためのパラメータ設定をします。
        /// </summary>
        /// <param name="jpNendo">和暦年度</param>
        /// <param name="shain">社員情報</param>
        /// <param name="kingaku">金額情報</param>
        /// <param name="kubun">区分情報</param>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// </returns>
        private ArrayList SetPrKyTblParams(string[] item)
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            ++_sort;

            // 制御情報
            updParam.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            updParam.SetParam("@SORT", SqlDbType.Int, _sort);

            // ITEM010-021
            updParam.SetParam("@ITEM001", SqlDbType.VarChar, 200, item[0]);
            updParam.SetParam("@ITEM011", SqlDbType.VarChar, 200, item[10]);
            updParam.SetParam("@ITEM012", SqlDbType.VarChar, 200, item[11]);
            updParam.SetParam("@ITEM021", SqlDbType.VarChar, 200, "");
            updParam.SetParam("@ITEM099", SqlDbType.VarChar, 200, "※区分");


            // ITEM022-098
            for (int i = 22; i <= 98; i++)
            {
                updParam.SetParam("@ITEM0" + Util.ToString(i), SqlDbType.VarChar, 200, item[i - 1]);
            }

            // ITEM100-152 
            for (int i = 100; i <= 153; i++)
            {
                updParam.SetParam("@ITEM" + Util.ToString(i), SqlDbType.VarChar, 200, item[i - 1]);
            }
            //マイナンバー対応時　変更
            //updParam.SetParam("@ITEM100", SqlDbType.VarChar, 200, item[99]);// 法人番号
            //updParam.SetParam("@ITEM101", SqlDbType.VarChar, 200, item[100]);// 各社員のマイナンバー


            alParams.Add(updParam);

            return alParams;
        }
        /// <summary>
        /// 数値フォーマット(０値を表示しない)
        /// </summary>
        private string FormatNum(object num)
        {
            string ret = "";
            if (num != null) ret = Util.FormatNum(num);
            if (ret == "0") ret = "";
            return ret;
        }
        #endregion

    }
}