﻿using System.Data;
using jp.co.fsi.common.report;

namespace jp.co.fsi.ky.kynr1021
{
    /// <summary>
    /// KYNR1022R の帳票
    /// </summary>
    public partial class KYNR1022R : BaseReport
    {
        public KYNR1022R(DataTable tgtData): base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }
    }
}
