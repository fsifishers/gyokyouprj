﻿using System.Data;
using jp.co.fsi.common.report;
using jp.co.fsi.common.util;

namespace jp.co.fsi.ky.kynr1021
{
    /// <summary>
    /// KYNR1023R の帳票
    /// </summary>
    public partial class KYNR1023R : BaseReport
    {
        public KYNR1023R(DataTable tgtData): base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();

        }
    }
}
