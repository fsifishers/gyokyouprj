﻿namespace jp.co.fsi.ky.kynr1021
{
    /// <summary>
    /// KYNR1022R の帳票
    /// </summary>
    partial class KYNR1022R
    {
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(KYNR1022R));
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.テキスト3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label91 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox105 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox192 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox21 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox22 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox23 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox24 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox53 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox54 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox66 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox72 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox78 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox81 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox82 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox83 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox85 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox86 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox88 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox87 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox89 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM025 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト61 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM034 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト63 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.aaa = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト65 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM036 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト71 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM037 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル177 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.テキスト39 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox25 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト124 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox26 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox27 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox28 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト138 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox30 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox31 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox32 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox34 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox35 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox36 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox37 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox38 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox90 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox91 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox92 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox93 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox94 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox95 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox96 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox97 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox98 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox99 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox100 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox101 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox102 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox104 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox106 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox107 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox108 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox109 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox110 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox111 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox112 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox113 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox114 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox115 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox116 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox117 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox118 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox119 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox120 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox121 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox122 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox123 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox124 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox125 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox126 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox128 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox129 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox130 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox131 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox132 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox133 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox134 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox135 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox136 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox170 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox171 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox172 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox80 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox39 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox40 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox42 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox44 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox165 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox45 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox46 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox47 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox49 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox50 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox51 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox103 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox137 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox138 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label83 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox55 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox58 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox59 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox61 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox63 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox64 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox65 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox67 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox70 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox71 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox73 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox75 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox76 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox77 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox144 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox145 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox146 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox147 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox150 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox151 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox152 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox153 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox156 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox157 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox158 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label25 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label45 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox257 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label46 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label47 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label48 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label49 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label50 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label51 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox259 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox264 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox268 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox269 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox273 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox274 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox278 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox279 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox283 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox286 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox287 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox288 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox289 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox290 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox291 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox292 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox293 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox294 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox296 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox297 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox298 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox299 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox300 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox301 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox302 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox303 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox160 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox161 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox162 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox163 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox164 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox166 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox167 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox168 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox169 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox304 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト163 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト167 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト168 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label56 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox305 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox306 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox316 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label57 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox317 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label58 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox318 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label59 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox319 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label60 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox320 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label61 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line15 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line16 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line17 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line18 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line19 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line20 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line21 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line22 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line23 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line24 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line25 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line26 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line27 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line28 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line29 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line30 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line31 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line32 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line33 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line34 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line35 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line36 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line37 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line38 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line39 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line40 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line41 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line42 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line43 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line44 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line45 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line46 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line47 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line48 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line49 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line50 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line51 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line73 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line96 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line97 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line98 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line99 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line100 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line101 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line113 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line114 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line115 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line116 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line117 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line118 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line119 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line120 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line121 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line122 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line123 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.textBox322 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox323 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox324 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox325 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox326 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox142 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox141 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox56 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox311 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox57 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox69 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox143 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox139 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox62 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox313 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox68 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox310 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox74 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox307 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox276 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox281 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox266 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox271 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox260 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox261 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox262 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox263 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox270 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox315 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox265 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox312 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox275 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox309 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox280 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox308 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line124 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.label26 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox29 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox48 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox41 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox43 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox33 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox127 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox173 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox174 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox175 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox176 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox178 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox179 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox180 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox181 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox182 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox183 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox184 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox185 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox186 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox187 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox188 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox189 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox190 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox191 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label27 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label28 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label29 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox193 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox197 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox198 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox199 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox200 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox201 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox202 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox203 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox204 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label30 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox205 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox206 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox207 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox208 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox209 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox210 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox211 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox212 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox213 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox214 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox215 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox216 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox217 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox218 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox219 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox220 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label31 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label32 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label33 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox221 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox222 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox223 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox224 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox225 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label34 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label35 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox226 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox227 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox228 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox229 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label36 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label37 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label38 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label39 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label40 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox230 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox231 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox232 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox233 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox234 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox235 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox236 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox237 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox238 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox239 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label41 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox241 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line135 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.textBox242 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox243 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label42 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox244 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox245 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox246 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox247 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox248 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox250 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox251 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox252 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox253 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox254 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox255 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox256 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox327 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox328 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox329 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label43 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox330 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label64 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label65 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label66 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label67 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label68 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label69 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox331 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox332 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label70 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label71 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label72 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label73 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label74 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label75 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label76 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label77 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox333 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label78 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox334 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label79 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox335 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label80 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox336 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label81 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox337 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label82 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox338 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label84 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox339 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox340 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox341 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox342 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox343 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox344 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox345 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox347 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label85 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox348 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox349 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label86 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox351 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox353 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox354 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label87 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label88 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox355 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox356 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox358 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox359 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox360 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox362 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox363 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox365 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox366 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox367 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox369 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox370 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox371 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox373 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox374 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox376 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox377 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox379 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox380 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox382 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox383 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox384 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox385 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox386 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox387 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox388 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox389 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox390 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox391 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox392 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox393 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox394 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox395 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox396 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox397 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox398 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox400 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox402 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox403 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox404 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox405 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox406 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox407 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox408 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox409 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox410 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox411 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox412 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox413 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox414 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox415 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox416 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox417 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox418 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox419 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox420 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox421 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox422 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox423 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox424 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox425 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox426 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox427 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox428 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox429 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox430 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox431 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox432 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox433 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox434 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox435 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox438 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox439 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox440 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox441 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox442 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox443 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox444 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox445 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox446 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox447 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox448 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox449 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox450 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox451 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox452 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox453 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox454 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox455 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox456 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox457 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox458 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox459 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox460 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox461 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox462 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox463 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox464 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox465 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox466 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox467 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox468 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox469 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox470 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox471 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox472 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox473 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox474 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox475 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox476 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox477 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox478 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox479 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox480 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox481 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox482 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox483 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox484 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox485 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox486 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox487 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox488 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox489 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox490 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox491 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox492 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox352 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox401 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox346 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line52 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line53 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line54 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line55 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.textBox267 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox295 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox350 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line56 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line57 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line58 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line59 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line60 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line61 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line62 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line63 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line64 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line65 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line66 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.textBox84 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox79 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line112 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line110 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line109 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line111 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line108 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line107 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line106 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line105 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line104 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line103 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line102 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.textBox258 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox140 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox52 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox60 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox314 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox284 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox285 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox149 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox148 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label63 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox159 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label62 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox155 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox154 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label44 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line67 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.label54 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label55 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label52 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label53 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox195 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox196 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label89 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label90 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label92 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox177 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox240 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox321 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox249 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.textBox194 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox272 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label91)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox105)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox192)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox54)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox66)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox72)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox78)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox81)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox82)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox83)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox85)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox86)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox88)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox87)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox89)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM025)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト61)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM034)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト63)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aaa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト65)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM036)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト71)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM037)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル177)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト124)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト138)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox90)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox91)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox92)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox93)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox94)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox95)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox96)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox97)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox98)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox99)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox100)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox101)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox102)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox104)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox106)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox107)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox108)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox109)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox110)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox111)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox112)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox113)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox114)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox115)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox116)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox117)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox118)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox119)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox120)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox121)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox122)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox123)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox124)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox125)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox126)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox128)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox129)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox130)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox131)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox132)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox133)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox134)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox135)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox136)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox170)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox171)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox172)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox80)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox165)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox103)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox137)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox138)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label83)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox55)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox58)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox59)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox61)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox63)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox64)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox65)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox67)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox70)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox71)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox73)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox75)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox76)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox77)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox144)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox145)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox146)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox147)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox150)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox151)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox152)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox153)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox156)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox157)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox158)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox257)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox259)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox264)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox268)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox269)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox273)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox274)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox278)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox279)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox283)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox286)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox287)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox288)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox289)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox290)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox291)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox292)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox293)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox294)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox296)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox297)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox298)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox299)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox300)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox301)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox302)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox303)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox160)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox161)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox162)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox163)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox164)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox166)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox167)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox168)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox169)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox304)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト163)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト167)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト168)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label56)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox305)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox306)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox316)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label57)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox317)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label58)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox318)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label59)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox319)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label60)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox320)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label61)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox322)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox323)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox324)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox325)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox326)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox142)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox141)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox56)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox311)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox57)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox69)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox143)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox139)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox62)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox313)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox68)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox310)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox74)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox307)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox276)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox281)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox266)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox271)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox260)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox261)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox262)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox263)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox270)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox315)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox265)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox312)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox275)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox309)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox280)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox308)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox127)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox173)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox174)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox175)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox176)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox178)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox179)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox180)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox181)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox182)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox183)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox184)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox185)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox186)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox187)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox188)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox189)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox190)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox191)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox193)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox197)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox198)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox199)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox200)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox201)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox202)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox203)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox204)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox205)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox206)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox207)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox208)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox209)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox210)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox211)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox212)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox213)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox214)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox215)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox216)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox217)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox218)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox219)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox220)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox221)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox222)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox223)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox224)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox225)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox226)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox227)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox228)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox229)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox230)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox231)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox232)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox233)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox234)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox235)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox236)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox237)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox238)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox239)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox241)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox242)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox243)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox244)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox245)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox246)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox247)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox248)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox250)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox251)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox252)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox253)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox254)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox255)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox256)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox327)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox328)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox329)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox330)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label64)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label65)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label66)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label67)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label68)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label69)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox331)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox332)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label70)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label71)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label72)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label73)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label74)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label75)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label76)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label77)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox333)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label78)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox334)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label79)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox335)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label80)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox336)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label81)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox337)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label82)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox338)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label84)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox339)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox340)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox341)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox342)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox343)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox344)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox345)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox347)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label85)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox348)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox349)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label86)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox351)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox353)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox354)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label87)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label88)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox355)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox356)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox358)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox359)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox360)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox362)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox363)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox365)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox366)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox367)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox369)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox370)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox371)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox373)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox374)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox376)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox377)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox379)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox380)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox382)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox383)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox384)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox385)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox386)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox387)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox388)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox389)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox390)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox391)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox392)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox393)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox394)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox395)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox396)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox397)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox398)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox400)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox402)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox403)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox404)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox405)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox406)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox407)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox408)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox409)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox410)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox411)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox412)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox413)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox414)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox415)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox416)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox417)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox418)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox419)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox420)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox421)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox422)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox423)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox424)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox425)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox426)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox427)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox428)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox429)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox430)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox431)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox432)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox433)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox434)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox435)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox438)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox439)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox440)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox441)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox442)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox443)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox444)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox445)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox446)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox447)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox448)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox449)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox450)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox451)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox452)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox453)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox454)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox455)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox456)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox457)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox458)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox459)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox460)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox461)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox462)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox463)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox464)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox465)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox466)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox467)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox468)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox469)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox470)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox471)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox472)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox473)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox474)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox475)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox476)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox477)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox478)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox479)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox480)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox481)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox482)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox483)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox484)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox485)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox486)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox487)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox488)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox489)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox490)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox491)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox492)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox352)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox401)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox346)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox267)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox295)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox350)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox84)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox79)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox258)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox140)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox60)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox314)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox284)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox285)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox149)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox148)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label63)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox159)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label62)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox155)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox154)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label54)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label55)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox195)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox196)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label89)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label90)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label92)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox177)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox240)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox321)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox249)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox194)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox272)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // detail
            // 
            this.detail.CanShrink = true;
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.テキスト3,
            this.label91,
            this.textBox105,
            this.textBox192,
            this.label12,
            this.textBox3,
            this.textBox4,
            this.textBox5,
            this.textBox6,
            this.textBox7,
            this.textBox8,
            this.textBox11,
            this.textBox12,
            this.textBox13,
            this.textBox14,
            this.textBox15,
            this.textBox16,
            this.textBox17,
            this.textBox18,
            this.textBox19,
            this.textBox20,
            this.textBox21,
            this.textBox22,
            this.textBox23,
            this.label1,
            this.textBox24,
            this.textBox53,
            this.textBox54,
            this.textBox66,
            this.textBox72,
            this.textBox78,
            this.textBox81,
            this.textBox82,
            this.textBox83,
            this.textBox85,
            this.textBox86,
            this.label2,
            this.label3,
            this.label4,
            this.label5,
            this.textBox88,
            this.textBox87,
            this.textBox89,
            this.ITEM025,
            this.テキスト61,
            this.ITEM034,
            this.テキスト63,
            this.aaa,
            this.テキスト65,
            this.ITEM036,
            this.テキスト71,
            this.ITEM037,
            this.ラベル177,
            this.テキスト39,
            this.textBox25,
            this.テキスト124,
            this.textBox26,
            this.textBox27,
            this.textBox28,
            this.テキスト138,
            this.textBox30,
            this.textBox31,
            this.textBox32,
            this.textBox34,
            this.textBox35,
            this.textBox36,
            this.textBox37,
            this.textBox38,
            this.textBox90,
            this.textBox91,
            this.textBox92,
            this.textBox93,
            this.textBox94,
            this.textBox95,
            this.textBox96,
            this.textBox97,
            this.textBox98,
            this.textBox99,
            this.textBox100,
            this.textBox101,
            this.textBox102,
            this.textBox104,
            this.textBox106,
            this.textBox107,
            this.textBox108,
            this.textBox109,
            this.textBox110,
            this.textBox111,
            this.textBox112,
            this.textBox113,
            this.textBox114,
            this.textBox115,
            this.textBox116,
            this.textBox117,
            this.textBox118,
            this.textBox119,
            this.textBox120,
            this.textBox121,
            this.textBox122,
            this.textBox123,
            this.textBox124,
            this.textBox125,
            this.textBox126,
            this.textBox128,
            this.textBox129,
            this.textBox130,
            this.textBox131,
            this.textBox132,
            this.textBox133,
            this.textBox134,
            this.textBox135,
            this.textBox136,
            this.textBox170,
            this.textBox171,
            this.textBox172,
            this.textBox80,
            this.textBox2,
            this.textBox39,
            this.textBox40,
            this.textBox42,
            this.textBox44,
            this.textBox165,
            this.label17,
            this.label18,
            this.label19,
            this.textBox45,
            this.textBox46,
            this.textBox47,
            this.textBox49,
            this.textBox50,
            this.label14,
            this.label16,
            this.textBox51,
            this.textBox103,
            this.textBox137,
            this.textBox138,
            this.label9,
            this.label10,
            this.label13,
            this.label11,
            this.label83,
            this.textBox55,
            this.textBox58,
            this.textBox59,
            this.textBox61,
            this.textBox63,
            this.textBox64,
            this.textBox65,
            this.textBox67,
            this.textBox70,
            this.textBox71,
            this.textBox73,
            this.textBox75,
            this.textBox76,
            this.textBox77,
            this.textBox144,
            this.textBox145,
            this.textBox146,
            this.textBox147,
            this.textBox150,
            this.textBox151,
            this.textBox152,
            this.textBox153,
            this.textBox156,
            this.textBox157,
            this.label7,
            this.textBox158,
            this.label20,
            this.label22,
            this.label23,
            this.label24,
            this.label25,
            this.label45,
            this.textBox257,
            this.label46,
            this.label47,
            this.label48,
            this.label49,
            this.label50,
            this.label51,
            this.textBox259,
            this.textBox264,
            this.textBox268,
            this.textBox269,
            this.textBox273,
            this.textBox274,
            this.textBox278,
            this.textBox279,
            this.textBox283,
            this.textBox286,
            this.textBox287,
            this.textBox288,
            this.textBox289,
            this.textBox290,
            this.textBox291,
            this.textBox292,
            this.textBox293,
            this.textBox294,
            this.textBox296,
            this.textBox297,
            this.textBox298,
            this.textBox299,
            this.textBox300,
            this.textBox301,
            this.textBox302,
            this.textBox303,
            this.textBox160,
            this.textBox161,
            this.textBox162,
            this.textBox163,
            this.textBox164,
            this.textBox166,
            this.textBox167,
            this.textBox168,
            this.textBox169,
            this.textBox304,
            this.テキスト163,
            this.テキスト167,
            this.テキスト168,
            this.label56,
            this.textBox305,
            this.textBox306,
            this.label6,
            this.textBox316,
            this.label57,
            this.textBox317,
            this.label58,
            this.textBox318,
            this.label59,
            this.textBox319,
            this.label60,
            this.textBox320,
            this.label61,
            this.textBox10,
            this.line2,
            this.line3,
            this.line4,
            this.line5,
            this.line6,
            this.line7,
            this.line8,
            this.line9,
            this.line10,
            this.line11,
            this.line12,
            this.line13,
            this.line14,
            this.line15,
            this.line16,
            this.line17,
            this.line18,
            this.line19,
            this.line20,
            this.line21,
            this.line22,
            this.line23,
            this.line24,
            this.line25,
            this.line26,
            this.line27,
            this.line28,
            this.line29,
            this.line30,
            this.line31,
            this.line32,
            this.line33,
            this.line34,
            this.line35,
            this.line36,
            this.line37,
            this.line38,
            this.line39,
            this.line40,
            this.line41,
            this.line42,
            this.line43,
            this.line44,
            this.line45,
            this.line46,
            this.line47,
            this.line48,
            this.line49,
            this.line50,
            this.line51,
            this.line73,
            this.line96,
            this.line97,
            this.line98,
            this.line99,
            this.line100,
            this.line101,
            this.line113,
            this.line114,
            this.line115,
            this.line116,
            this.line117,
            this.line118,
            this.line119,
            this.line120,
            this.line121,
            this.line122,
            this.line123,
            this.textBox322,
            this.textBox323,
            this.textBox324,
            this.textBox325,
            this.textBox326,
            this.textBox142,
            this.textBox141,
            this.textBox56,
            this.textBox311,
            this.textBox57,
            this.textBox69,
            this.textBox143,
            this.textBox139,
            this.textBox62,
            this.textBox313,
            this.textBox68,
            this.textBox310,
            this.textBox74,
            this.textBox307,
            this.textBox276,
            this.textBox281,
            this.textBox266,
            this.textBox271,
            this.textBox260,
            this.textBox261,
            this.textBox262,
            this.textBox263,
            this.textBox270,
            this.textBox315,
            this.textBox265,
            this.textBox312,
            this.textBox275,
            this.textBox309,
            this.textBox280,
            this.textBox308,
            this.line124,
            this.label26,
            this.textBox29,
            this.textBox48,
            this.label15,
            this.textBox41,
            this.textBox43,
            this.textBox33,
            this.textBox1,
            this.textBox127,
            this.textBox173,
            this.textBox174,
            this.textBox175,
            this.textBox176,
            this.textBox178,
            this.textBox179,
            this.textBox180,
            this.textBox181,
            this.textBox182,
            this.textBox183,
            this.textBox184,
            this.textBox185,
            this.textBox186,
            this.textBox187,
            this.textBox188,
            this.textBox189,
            this.textBox190,
            this.textBox191,
            this.label21,
            this.label27,
            this.label28,
            this.label29,
            this.textBox193,
            this.textBox197,
            this.textBox198,
            this.textBox199,
            this.textBox200,
            this.textBox201,
            this.textBox202,
            this.textBox203,
            this.textBox204,
            this.label30,
            this.textBox205,
            this.textBox206,
            this.textBox207,
            this.textBox208,
            this.textBox209,
            this.textBox210,
            this.textBox211,
            this.textBox212,
            this.textBox213,
            this.textBox214,
            this.textBox215,
            this.textBox216,
            this.textBox217,
            this.textBox218,
            this.textBox219,
            this.textBox220,
            this.label31,
            this.label32,
            this.label33,
            this.textBox221,
            this.textBox222,
            this.textBox223,
            this.textBox224,
            this.textBox225,
            this.label34,
            this.label35,
            this.textBox226,
            this.textBox227,
            this.textBox228,
            this.textBox229,
            this.label36,
            this.label37,
            this.label38,
            this.label39,
            this.label40,
            this.textBox230,
            this.textBox231,
            this.textBox232,
            this.textBox233,
            this.textBox234,
            this.textBox235,
            this.textBox236,
            this.textBox237,
            this.textBox238,
            this.textBox239,
            this.label41,
            this.textBox241,
            this.line135,
            this.textBox242,
            this.textBox243,
            this.label42,
            this.textBox244,
            this.textBox245,
            this.textBox246,
            this.textBox247,
            this.textBox248,
            this.textBox250,
            this.textBox251,
            this.textBox252,
            this.textBox253,
            this.textBox254,
            this.textBox255,
            this.textBox256,
            this.textBox327,
            this.textBox328,
            this.textBox329,
            this.label43,
            this.textBox330,
            this.label64,
            this.label65,
            this.label66,
            this.label67,
            this.label68,
            this.label69,
            this.textBox331,
            this.textBox332,
            this.label70,
            this.label71,
            this.label72,
            this.label73,
            this.label74,
            this.label75,
            this.label76,
            this.label77,
            this.textBox333,
            this.label78,
            this.textBox334,
            this.label79,
            this.textBox335,
            this.label80,
            this.textBox336,
            this.label81,
            this.textBox337,
            this.label82,
            this.textBox338,
            this.label84,
            this.textBox339,
            this.textBox340,
            this.textBox341,
            this.textBox342,
            this.textBox343,
            this.textBox344,
            this.textBox345,
            this.textBox347,
            this.label85,
            this.textBox348,
            this.textBox349,
            this.label86,
            this.textBox351,
            this.textBox353,
            this.textBox354,
            this.label87,
            this.label88,
            this.textBox355,
            this.textBox356,
            this.textBox358,
            this.textBox359,
            this.textBox360,
            this.textBox362,
            this.textBox363,
            this.textBox365,
            this.textBox366,
            this.textBox367,
            this.textBox369,
            this.textBox370,
            this.textBox371,
            this.textBox373,
            this.textBox374,
            this.textBox376,
            this.textBox377,
            this.textBox379,
            this.textBox380,
            this.textBox382,
            this.textBox383,
            this.textBox384,
            this.textBox385,
            this.textBox386,
            this.textBox387,
            this.textBox388,
            this.textBox389,
            this.textBox390,
            this.textBox391,
            this.textBox392,
            this.textBox393,
            this.textBox394,
            this.textBox395,
            this.textBox396,
            this.textBox397,
            this.textBox398,
            this.textBox400,
            this.textBox402,
            this.textBox403,
            this.textBox404,
            this.textBox405,
            this.textBox406,
            this.textBox407,
            this.textBox408,
            this.textBox409,
            this.textBox410,
            this.textBox411,
            this.textBox412,
            this.textBox413,
            this.textBox414,
            this.textBox415,
            this.textBox416,
            this.textBox417,
            this.textBox418,
            this.textBox419,
            this.textBox420,
            this.textBox421,
            this.textBox422,
            this.textBox423,
            this.textBox424,
            this.textBox425,
            this.textBox426,
            this.textBox427,
            this.textBox428,
            this.textBox429,
            this.textBox430,
            this.textBox431,
            this.textBox432,
            this.textBox433,
            this.textBox434,
            this.textBox435,
            this.textBox438,
            this.textBox439,
            this.textBox440,
            this.textBox441,
            this.textBox442,
            this.textBox443,
            this.textBox444,
            this.textBox445,
            this.textBox446,
            this.textBox447,
            this.textBox448,
            this.textBox449,
            this.textBox450,
            this.textBox451,
            this.textBox452,
            this.textBox453,
            this.textBox454,
            this.textBox455,
            this.textBox456,
            this.textBox457,
            this.textBox458,
            this.textBox459,
            this.textBox460,
            this.textBox461,
            this.textBox462,
            this.textBox463,
            this.textBox464,
            this.textBox465,
            this.textBox466,
            this.textBox467,
            this.textBox468,
            this.textBox469,
            this.textBox470,
            this.textBox471,
            this.textBox472,
            this.textBox473,
            this.textBox474,
            this.textBox475,
            this.textBox476,
            this.textBox477,
            this.textBox478,
            this.textBox479,
            this.textBox480,
            this.textBox481,
            this.textBox482,
            this.textBox483,
            this.textBox484,
            this.textBox485,
            this.textBox486,
            this.textBox487,
            this.textBox488,
            this.textBox489,
            this.textBox490,
            this.textBox491,
            this.textBox492,
            this.textBox352,
            this.textBox401,
            this.textBox346,
            this.line52,
            this.line53,
            this.line54,
            this.line55,
            this.textBox267,
            this.textBox295,
            this.textBox350,
            this.line56,
            this.line57,
            this.line58,
            this.line59,
            this.line60,
            this.line61,
            this.line62,
            this.line63,
            this.line64,
            this.line65,
            this.line66,
            this.textBox84,
            this.textBox79,
            this.line112,
            this.line110,
            this.line109,
            this.line111,
            this.line108,
            this.line107,
            this.line106,
            this.line105,
            this.line104,
            this.line103,
            this.line102,
            this.textBox258,
            this.textBox140,
            this.textBox52,
            this.textBox60,
            this.textBox314,
            this.textBox284,
            this.textBox285,
            this.textBox149,
            this.textBox148,
            this.label63,
            this.label8,
            this.textBox159,
            this.label62,
            this.textBox155,
            this.textBox154,
            this.label44,
            this.line67,
            this.label54,
            this.label55,
            this.label52,
            this.label53,
            this.textBox195,
            this.textBox196,
            this.label89,
            this.label90,
            this.label92,
            this.textBox177,
            this.textBox240,
            this.textBox321,
            this.textBox9,
            this.textBox249,
            this.line1,
            this.textBox194,
            this.textBox272});
            this.detail.Height = 7.878582F;
            this.detail.Name = "detail";
            // 
            // テキスト3
            // 
            this.テキスト3.Height = 0.7884623F;
            this.テキスト3.HyperLink = null;
            this.テキスト3.Left = 0F;
            this.テキスト3.Name = "テキスト3";
            this.テキスト3.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 6.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128; ddo-font-vertical: true";
            this.テキスト3.Tag = "";
            this.テキスト3.Text = "(税務署提出用)";
            this.テキスト3.Top = 6.850394F;
            this.テキスト3.Width = 0.1506944F;
            // 
            // label91
            // 
            this.label91.Height = 0.7884625F;
            this.label91.HyperLink = null;
            this.label91.Left = 5.703938F;
            this.label91.Name = "label91";
            this.label91.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 6.75pt; font-weight: normal; text-al" +
    "ign: left; ddo-char-set: 128; ddo-font-vertical: true";
            this.label91.Tag = "";
            this.label91.Text = "(受給者交付用)";
            this.label91.Top = 6.850394F;
            this.label91.Width = 0.1506944F;
            // 
            // textBox105
            // 
            this.textBox105.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox105.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox105.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox105.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox105.Height = 0.4665354F;
            this.textBox105.Left = 0.3405512F;
            this.textBox105.Name = "textBox105";
            this.textBox105.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox105.Tag = "";
            this.textBox105.Text = "外\r\n国\r\n人";
            this.textBox105.Top = 6.226378F;
            this.textBox105.Width = 0.2133858F;
            // 
            // textBox192
            // 
            this.textBox192.DataField = "ITEM011";
            this.textBox192.Height = 0.1576389F;
            this.textBox192.Left = 6.369686F;
            this.textBox192.Name = "textBox192";
            this.textBox192.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.textBox192.Tag = "";
            this.textBox192.Text = "ITEM011";
            this.textBox192.Top = 0.06732356F;
            this.textBox192.Width = 0.8182299F;
            // 
            // label12
            // 
            this.label12.Height = 0.1611111F;
            this.label12.HyperLink = null;
            this.label12.Left = 7.057994F;
            this.label12.Name = "label12";
            this.label12.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-alig" +
    "n: center; ddo-char-set: 128";
            this.label12.Tag = "";
            this.label12.Text = "給与所得の源泉徴収票";
            this.label12.Top = 0.06378019F;
            this.label12.Width = 2.819854F;
            // 
            // textBox3
            // 
            this.textBox3.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox3.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox3.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox3.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox3.Height = 0.6885816F;
            this.textBox3.Left = 0.1275573F;
            this.textBox3.Name = "textBox3";
            this.textBox3.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox3.Tag = "";
            this.textBox3.Text = "支払を\r\n受ける者";
            this.textBox3.Top = 0.2563004F;
            this.textBox3.Width = 0.3448837F;
            // 
            // textBox4
            // 
            this.textBox4.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox4.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox4.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox4.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox4.Height = 0.6889765F;
            this.textBox4.Left = 0.472441F;
            this.textBox4.Name = "textBox4";
            this.textBox4.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox4.Tag = "";
            this.textBox4.Text = "住所又は居所";
            this.textBox4.Top = 0.2559055F;
            this.textBox4.Width = 0.2047244F;
            // 
            // textBox5
            // 
            this.textBox5.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox5.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox5.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox5.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox5.Height = 0.2759842F;
            this.textBox5.Left = 3.299213F;
            this.textBox5.Name = "textBox5";
            this.textBox5.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox5.Tag = "";
            this.textBox5.Text = "氏名";
            this.textBox5.Top = 0.668504F;
            this.textBox5.Width = 0.2444882F;
            // 
            // textBox6
            // 
            this.textBox6.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox6.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox6.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox6.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox6.DataField = "ITEM022";
            this.textBox6.Height = 0.2295276F;
            this.textBox6.Left = 0.6771654F;
            this.textBox6.Name = "textBox6";
            this.textBox6.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: left; vertical-align: middle; ddo-char-set: 1";
            this.textBox6.Tag = "";
            this.textBox6.Text = "ITEM022";
            this.textBox6.Top = 0.2562992F;
            this.textBox6.Width = 2.622047F;
            // 
            // textBox7
            // 
            this.textBox7.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox7.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox7.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox7.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox7.DataField = "ITEM023";
            this.textBox7.Height = 0.2295276F;
            this.textBox7.Left = 0.6771654F;
            this.textBox7.Name = "textBox7";
            this.textBox7.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: left; vertical-align: middle; ddo-char-set: 1";
            this.textBox7.Tag = "";
            this.textBox7.Text = "ITEM023";
            this.textBox7.Top = 0.4854331F;
            this.textBox7.Width = 2.622047F;
            // 
            // textBox8
            // 
            this.textBox8.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox8.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox8.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox8.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox8.DataField = "ITEM024";
            this.textBox8.Height = 0.2295276F;
            this.textBox8.Left = 0.6771654F;
            this.textBox8.Name = "textBox8";
            this.textBox8.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: left; vertical-align: middle; ddo-char-set: 1";
            this.textBox8.Tag = "";
            this.textBox8.Text = "ITEM024";
            this.textBox8.Top = 0.7149607F;
            this.textBox8.Width = 2.622047F;
            // 
            // textBox11
            // 
            this.textBox11.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox11.DataField = "ITEM028";
            this.textBox11.Height = 0.138189F;
            this.textBox11.Left = 3.592126F;
            this.textBox11.Name = "textBox11";
            this.textBox11.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.textBox11.Tag = "";
            this.textBox11.Text = "ITEM028";
            this.textBox11.Top = 0.806693F;
            this.textBox11.Width = 1.955512F;
            // 
            // textBox12
            // 
            this.textBox12.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox12.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox12.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox12.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox12.Height = 0.1377953F;
            this.textBox12.Left = 3.299607F;
            this.textBox12.Name = "textBox12";
            this.textBox12.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: left; vertical-align: middle; ddo-char-set: 1";
            this.textBox12.Tag = "";
            this.textBox12.Text = "(個人番号)";
            this.textBox12.Top = 0.3929133F;
            this.textBox12.Width = 2.248032F;
            // 
            // textBox13
            // 
            this.textBox13.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox13.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox13.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox13.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox13.DataField = "ITEM030";
            this.textBox13.Height = 0.3152778F;
            this.textBox13.Left = 1.172833F;
            this.textBox13.Name = "textBox13";
            this.textBox13.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; vertical-align: bottom; ddo-char-set: 1";
            this.textBox13.Tag = "";
            this.textBox13.Text = "ITEM030";
            this.textBox13.Top = 1.23071F;
            this.textBox13.Width = 1.063385F;
            // 
            // textBox14
            // 
            this.textBox14.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox14.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox14.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox14.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox14.Height = 0.2858279F;
            this.textBox14.Left = 0.1275573F;
            this.textBox14.Name = "textBox14";
            this.textBox14.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox14.Tag = "";
            this.textBox14.Text = "種　　　別";
            this.textBox14.Top = 0.944882F;
            this.textBox14.Width = 1.045276F;
            // 
            // textBox15
            // 
            this.textBox15.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox15.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox15.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox15.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox15.DataField = "ITEM029";
            this.textBox15.Height = 0.3152778F;
            this.textBox15.Left = 0.1275573F;
            this.textBox15.Name = "textBox15";
            this.textBox15.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: left; vertical-align: bottom; ddo-char-set: 1";
            this.textBox15.Tag = "";
            this.textBox15.Text = "ITEM029";
            this.textBox15.Top = 1.23071F;
            this.textBox15.Width = 1.045276F;
            // 
            // textBox16
            // 
            this.textBox16.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox16.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox16.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox16.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox16.Height = 0.2858279F;
            this.textBox16.Left = 1.172835F;
            this.textBox16.Name = "textBox16";
            this.textBox16.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox16.Tag = "";
            this.textBox16.Text = "支　払　金　額";
            this.textBox16.Top = 0.944882F;
            this.textBox16.Width = 1.063194F;
            // 
            // textBox17
            // 
            this.textBox17.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox17.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox17.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox17.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox17.DataField = "ITEM031";
            this.textBox17.Height = 0.3152778F;
            this.textBox17.Left = 2.236219F;
            this.textBox17.Name = "textBox17";
            this.textBox17.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; vertical-align: bottom; ddo-char-set: 1";
            this.textBox17.Tag = "";
            this.textBox17.Text = "ITEM031";
            this.textBox17.Top = 1.23071F;
            this.textBox17.Width = 1.063385F;
            // 
            // textBox18
            // 
            this.textBox18.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox18.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox18.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox18.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox18.Height = 0.2858279F;
            this.textBox18.Left = 2.236219F;
            this.textBox18.Name = "textBox18";
            this.textBox18.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox18.Tag = "";
            this.textBox18.Text = "給与所得控除後の金額";
            this.textBox18.Top = 0.944882F;
            this.textBox18.Width = 1.063194F;
            // 
            // textBox19
            // 
            this.textBox19.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox19.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox19.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox19.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox19.DataField = "ITEM032";
            this.textBox19.Height = 0.3152778F;
            this.textBox19.Left = 3.299601F;
            this.textBox19.Name = "textBox19";
            this.textBox19.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; vertical-align: bottom; ddo-char-set: 1";
            this.textBox19.Tag = "";
            this.textBox19.Text = "ITEM032";
            this.textBox19.Top = 1.23071F;
            this.textBox19.Width = 1.063385F;
            // 
            // textBox20
            // 
            this.textBox20.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox20.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox20.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox20.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox20.Height = 0.2858279F;
            this.textBox20.Left = 3.299601F;
            this.textBox20.Name = "textBox20";
            this.textBox20.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox20.Tag = "";
            this.textBox20.Text = "所得控除の額の合計額";
            this.textBox20.Top = 0.944882F;
            this.textBox20.Width = 1.063194F;
            // 
            // textBox21
            // 
            this.textBox21.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox21.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox21.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox21.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox21.DataField = "ITEM033";
            this.textBox21.Height = 0.3152778F;
            this.textBox21.Left = 4.362991F;
            this.textBox21.Name = "textBox21";
            this.textBox21.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; vertical-align: bottom; ddo-char-set: 1";
            this.textBox21.Tag = "";
            this.textBox21.Text = "ITEM033";
            this.textBox21.Top = 1.23071F;
            this.textBox21.Width = 1.184648F;
            // 
            // textBox22
            // 
            this.textBox22.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox22.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox22.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox22.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox22.Height = 0.2858279F;
            this.textBox22.Left = 4.362991F;
            this.textBox22.Name = "textBox22";
            this.textBox22.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox22.Tag = "";
            this.textBox22.Text = "源 泉 徴 収 税 額";
            this.textBox22.Top = 0.944882F;
            this.textBox22.Width = 1.184648F;
            // 
            // textBox23
            // 
            this.textBox23.DataField = "ITEM011";
            this.textBox23.Height = 0.1576389F;
            this.textBox23.Left = 0.472441F;
            this.textBox23.Name = "textBox23";
            this.textBox23.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.textBox23.Tag = "";
            this.textBox23.Text = "ITEM011";
            this.textBox23.Top = 0.06732407F;
            this.textBox23.Width = 1.022953F;
            // 
            // label1
            // 
            this.label1.Height = 0.1611111F;
            this.label1.HyperLink = null;
            this.label1.Left = 1.365472F;
            this.label1.Name = "label1";
            this.label1.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-alig" +
    "n: center; ddo-char-set: 128";
            this.label1.Tag = "";
            this.label1.Text = "給与所得の源泉徴収票";
            this.label1.Top = 0.0637807F;
            this.label1.Width = 2.819854F;
            // 
            // textBox24
            // 
            this.textBox24.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox24.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox24.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox24.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox24.Height = 0.3152788F;
            this.textBox24.Left = 0.1275573F;
            this.textBox24.Name = "textBox24";
            this.textBox24.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: left; vertical-align: middle; ddo-char-set: 1";
            this.textBox24.Tag = "";
            this.textBox24.Text = "控除対象配偶者の有無等";
            this.textBox24.Top = 1.546063F;
            this.textBox24.Width = 0.974805F;
            // 
            // textBox53
            // 
            this.textBox53.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox53.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox53.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox53.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox53.Height = 1.651969F;
            this.textBox53.Left = 0.1275591F;
            this.textBox53.Name = "textBox53";
            this.textBox53.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1; ddo-" +
    "font-vertical: true";
            this.textBox53.Tag = "";
            this.textBox53.Text = "控除対象\r\n扶養親族";
            this.textBox53.Top = 4.56811F;
            this.textBox53.Width = 0.1874007F;
            // 
            // textBox54
            // 
            this.textBox54.Height = 0.2362202F;
            this.textBox54.Left = 0.4712599F;
            this.textBox54.Name = "textBox54";
            this.textBox54.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox54.Tag = "";
            this.textBox54.Text = "氏名";
            this.textBox54.Top = 4.173229F;
            this.textBox54.Width = 0.4751969F;
            // 
            // textBox66
            // 
            this.textBox66.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox66.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox66.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox66.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox66.Height = 0.1559055F;
            this.textBox66.Left = 0.472441F;
            this.textBox66.Name = "textBox66";
            this.textBox66.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox66.Tag = "";
            this.textBox66.Text = "個人番号";
            this.textBox66.Top = 4.413385F;
            this.textBox66.Width = 0.4740157F;
            // 
            // textBox72
            // 
            this.textBox72.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox72.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox72.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox72.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox72.DataField = "ITEM105";
            this.textBox72.Height = 0.1555119F;
            this.textBox72.Left = 0.9464568F;
            this.textBox72.Name = "textBox72";
            this.textBox72.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: justify; text-justify: distribute-all-lines; vertical" +
    "-align: middle; ddo-char-set: 1";
            this.textBox72.Tag = "";
            this.textBox72.Text = "123456789012";
            this.textBox72.Top = 4.413385F;
            this.textBox72.Width = 1.654331F;
            // 
            // textBox78
            // 
            this.textBox78.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox78.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox78.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox78.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox78.Height = 0.7720475F;
            this.textBox78.Left = 0.1275591F;
            this.textBox78.Name = "textBox78";
            this.textBox78.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: left; vertical-align: top; ddo-char-set: 1";
            this.textBox78.Tag = "";
            this.textBox78.Text = "(摘要)";
            this.textBox78.Top = 2.859449F;
            this.textBox78.Width = 5.420079F;
            // 
            // textBox81
            // 
            this.textBox81.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox81.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox81.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox81.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox81.Height = 0.2362205F;
            this.textBox81.Left = 0.3937026F;
            this.textBox81.Name = "textBox81";
            this.textBox81.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox81.Tag = "";
            this.textBox81.Text = "氏 名 又 は\r\n名       称";
            this.textBox81.Top = 7.324016F;
            this.textBox81.Width = 0.8870079F;
            // 
            // textBox82
            // 
            this.textBox82.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox82.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox82.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox82.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox82.DataField = "ITEM096";
            this.textBox82.Height = 0.2362205F;
            this.textBox82.Left = 1.280711F;
            this.textBox82.Name = "textBox82";
            this.textBox82.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: left; ddo-char-set: 1";
            this.textBox82.Tag = "";
            this.textBox82.Text = "ITEM096";
            this.textBox82.Top = 7.087796F;
            this.textBox82.Width = 4.26693F;
            // 
            // textBox83
            // 
            this.textBox83.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox83.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox83.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox83.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox83.CanGrow = false;
            this.textBox83.DataField = "ITEM097";
            this.textBox83.Height = 0.2362205F;
            this.textBox83.Left = 1.280711F;
            this.textBox83.Name = "textBox83";
            this.textBox83.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: left; vertical-align: middle; ddo-char-set: 1";
            this.textBox83.Tag = "";
            this.textBox83.Text = "ITEM097";
            this.textBox83.Top = 7.324016F;
            this.textBox83.Width = 4.26693F;
            // 
            // textBox85
            // 
            this.textBox85.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox85.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox85.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox85.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox85.CanGrow = false;
            this.textBox85.DataField = "ITEM100";
            this.textBox85.Height = 0.2362205F;
            this.textBox85.Left = 1.280711F;
            this.textBox85.Name = "textBox85";
            this.textBox85.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: justify; text-justify: distribute-all-lines; vertica" +
    "l-align: middle; ddo-char-set: 1";
            this.textBox85.Tag = "";
            this.textBox85.Text = "1234567890123";
            this.textBox85.Top = 6.851575F;
            this.textBox85.Width = 1.868895F;
            // 
            // textBox86
            // 
            this.textBox86.DataField = "ITEM098";
            this.textBox86.Height = 0.1576389F;
            this.textBox86.Left = 3.793701F;
            this.textBox86.Name = "textBox86";
            this.textBox86.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: left; vertical-align: bottom; ddo-char-set: 1";
            this.textBox86.Tag = "";
            this.textBox86.Text = "ITEM098(電話)";
            this.textBox86.Top = 7.375985F;
            this.textBox86.Width = 1.7374F;
            // 
            // label2
            // 
            this.label2.Height = 0.08611111F;
            this.label2.HyperLink = null;
            this.label2.Left = 2.069684F;
            this.label2.Name = "label2";
            this.label2.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label2.Tag = "";
            this.label2.Text = "円";
            this.label2.Top = 1.246458F;
            this.label2.Width = 0.1666667F;
            // 
            // label3
            // 
            this.label3.Height = 0.08611111F;
            this.label3.HyperLink = null;
            this.label3.Left = 4.185435F;
            this.label3.Name = "label3";
            this.label3.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label3.Tag = "";
            this.label3.Text = "円";
            this.label3.Top = 1.246458F;
            this.label3.Width = 0.1666667F;
            // 
            // label4
            // 
            this.label4.Height = 0.08611111F;
            this.label4.HyperLink = null;
            this.label4.Left = 3.132677F;
            this.label4.Name = "label4";
            this.label4.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label4.Tag = "";
            this.label4.Text = "円";
            this.label4.Top = 1.246458F;
            this.label4.Width = 0.1666667F;
            // 
            // label5
            // 
            this.label5.Height = 0.08611111F;
            this.label5.HyperLink = null;
            this.label5.Left = 5.388186F;
            this.label5.Name = "label5";
            this.label5.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label5.Tag = "";
            this.label5.Text = "円";
            this.label5.Top = 1.246458F;
            this.label5.Width = 0.1595823F;
            // 
            // textBox88
            // 
            this.textBox88.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox88.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox88.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox88.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox88.Height = 0.1377953F;
            this.textBox88.Left = 3.3F;
            this.textBox88.Name = "textBox88";
            this.textBox88.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.textBox88.Tag = "";
            this.textBox88.Text = "(役職名)";
            this.textBox88.Top = 0.5307087F;
            this.textBox88.Width = 2.247637F;
            // 
            // textBox87
            // 
            this.textBox87.CanGrow = false;
            this.textBox87.DataField = "ITEM101";
            this.textBox87.Height = 0.1299213F;
            this.textBox87.Left = 3.920473F;
            this.textBox87.Name = "textBox87";
            this.textBox87.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: justify; text-justify: distribute-all-lines; vertical" +
    "-align: bottom; ddo-char-set: 1";
            this.textBox87.Tag = "";
            this.textBox87.Text = "123456789012";
            this.textBox87.Top = 0.3929134F;
            this.textBox87.Width = 1.610629F;
            // 
            // textBox89
            // 
            this.textBox89.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox89.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox89.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox89.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox89.Height = 0.1377953F;
            this.textBox89.Left = 3.299608F;
            this.textBox89.Name = "textBox89";
            this.textBox89.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: left; vertical-align: bottom; ddo-char-set: 1";
            this.textBox89.Tag = "";
            this.textBox89.Text = "(受給者番号)";
            this.textBox89.Top = 0.2562993F;
            this.textBox89.Width = 2.248031F;
            // 
            // ITEM025
            // 
            this.ITEM025.CanGrow = false;
            this.ITEM025.DataField = "ITEM025";
            this.ITEM025.Height = 0.1082677F;
            this.ITEM025.Left = 4.007874F;
            this.ITEM025.Name = "ITEM025";
            this.ITEM025.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.ITEM025.Tag = "";
            this.ITEM025.Text = "ITEM025";
            this.ITEM025.Top = 0.2677166F;
            this.ITEM025.Width = 1.523228F;
            // 
            // テキスト61
            // 
            this.テキスト61.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト61.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト61.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト61.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト61.Height = 0.1072454F;
            this.テキスト61.Left = 0.1275592F;
            this.テキスト61.Name = "テキスト61";
            this.テキスト61.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト61.Tag = "";
            this.テキスト61.Text = "　有";
            this.テキスト61.Top = 1.861259F;
            this.テキスト61.Width = 0.3448818F;
            // 
            // ITEM034
            // 
            this.ITEM034.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM034.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM034.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM034.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM034.DataField = "ITEM034";
            this.ITEM034.Height = 0.2196845F;
            this.ITEM034.Left = 0.1275592F;
            this.ITEM034.Name = "ITEM034";
            this.ITEM034.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.ITEM034.Tag = "";
            this.ITEM034.Text = "ITEM034";
            this.ITEM034.Top = 1.968504F;
            this.ITEM034.Width = 0.3448818F;
            // 
            // テキスト63
            // 
            this.テキスト63.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト63.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト63.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト63.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト63.Height = 0.1070866F;
            this.テキスト63.Left = 0.472441F;
            this.テキスト63.Name = "テキスト63";
            this.テキスト63.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト63.Tag = "";
            this.テキスト63.Text = "従有";
            this.テキスト63.Top = 1.861417F;
            this.テキスト63.Width = 0.2944883F;
            // 
            // aaa
            // 
            this.aaa.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.aaa.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.aaa.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.aaa.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.aaa.Height = 0.2196846F;
            this.aaa.Left = 0.472441F;
            this.aaa.Name = "aaa";
            this.aaa.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: bold; text-align: center; ddo-char-set: 1";
            this.aaa.Tag = "";
            this.aaa.Text = null;
            this.aaa.Top = 1.968504F;
            this.aaa.Width = 0.2948819F;
            // 
            // テキスト65
            // 
            this.テキスト65.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト65.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト65.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト65.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト65.CanGrow = false;
            this.テキスト65.Height = 0.1576389F;
            this.テキスト65.Left = 0.7669292F;
            this.テキスト65.Name = "テキスト65";
            this.テキスト65.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト65.Tag = "";
            this.テキスト65.Text = "老人";
            this.テキスト65.Top = 1.70362F;
            this.テキスト65.Width = 0.3360731F;
            // 
            // ITEM036
            // 
            this.ITEM036.DataField = "ITEM036";
            this.ITEM036.Height = 0.2082679F;
            this.ITEM036.Left = 0.7874016F;
            this.ITEM036.Name = "ITEM036";
            this.ITEM036.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.ITEM036.Tag = "";
            this.ITEM036.Text = "ITEM036";
            this.ITEM036.Top = 1.968504F;
            this.ITEM036.Width = 0.3149607F;
            // 
            // テキスト71
            // 
            this.テキスト71.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト71.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト71.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト71.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト71.Height = 0.3152788F;
            this.テキスト71.Left = 1.102362F;
            this.テキスト71.Name = "テキスト71";
            this.テキスト71.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.テキスト71.Tag = "";
            this.テキスト71.Text = "配偶者特別控除の額";
            this.テキスト71.Top = 1.546062F;
            this.テキスト71.Width = 0.5909723F;
            // 
            // ITEM037
            // 
            this.ITEM037.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM037.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM037.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM037.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM037.DataField = "ITEM037";
            this.ITEM037.Height = 0.326847F;
            this.ITEM037.Left = 1.102362F;
            this.ITEM037.Name = "ITEM037";
            this.ITEM037.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.ITEM037.Tag = "";
            this.ITEM037.Text = "ITEM037";
            this.ITEM037.Top = 1.861342F;
            this.ITEM037.Width = 0.5909723F;
            // 
            // ラベル177
            // 
            this.ラベル177.Height = 0.08611111F;
            this.ラベル177.HyperLink = null;
            this.ラベル177.Left = 1.521806F;
            this.ラベル177.Name = "ラベル177";
            this.ラベル177.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.ラベル177.Tag = "";
            this.ラベル177.Text = "円";
            this.ラベル177.Top = 1.86273F;
            this.ラベル177.Width = 0.1666667F;
            // 
            // テキスト39
            // 
            this.テキスト39.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト39.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト39.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト39.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト39.Height = 0.3149616F;
            this.テキスト39.Left = 1.693307F;
            this.テキスト39.Name = "テキスト39";
            this.テキスト39.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.テキスト39.Tag = "";
            this.テキスト39.Text = "控 除 対 象 扶 養 親 族 の 数\r\n（ 配 偶 者 を 除 く 。）";
            this.テキスト39.Top = 1.546457F;
            this.テキスト39.Width = 2.116142F;
            // 
            // textBox25
            // 
            this.textBox25.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox25.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox25.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox25.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox25.Height = 0.3153553F;
            this.textBox25.Left = 4.29567F;
            this.textBox25.Name = "textBox25";
            this.textBox25.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox25.Tag = "";
            this.textBox25.Text = "障害者の数\r\n(本人を除く)";
            this.textBox25.Top = 1.546063F;
            this.textBox25.Width = 0.7877955F;
            // 
            // テキスト124
            // 
            this.テキスト124.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト124.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト124.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト124.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト124.Height = 0.107032F;
            this.テキスト124.Left = 1.693307F;
            this.テキスト124.Name = "テキスト124";
            this.テキスト124.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト124.Tag = "";
            this.テキスト124.Text = "特 定";
            this.テキスト124.Top = 1.861417F;
            this.テキスト124.Width = 0.5429134F;
            // 
            // textBox26
            // 
            this.textBox26.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox26.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox26.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox26.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox26.Height = 0.107032F;
            this.textBox26.Left = 2.232284F;
            this.textBox26.Name = "textBox26";
            this.textBox26.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox26.Tag = "";
            this.textBox26.Text = "老　人";
            this.textBox26.Top = 1.861417F;
            this.textBox26.Width = 0.996063F;
            // 
            // textBox27
            // 
            this.textBox27.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox27.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox27.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox27.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox27.Height = 0.1059056F;
            this.textBox27.Left = 3.228347F;
            this.textBox27.Name = "textBox27";
            this.textBox27.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox27.Tag = "";
            this.textBox27.Text = "その他";
            this.textBox27.Top = 1.862599F;
            this.textBox27.Width = 0.5811022F;
            // 
            // textBox28
            // 
            this.textBox28.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox28.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox28.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox28.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox28.Height = 0.107032F;
            this.textBox28.Left = 4.294882F;
            this.textBox28.Name = "textBox28";
            this.textBox28.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox28.Tag = "";
            this.textBox28.Text = "特別";
            this.textBox28.Top = 1.862599F;
            this.textBox28.Width = 0.3940945F;
            // 
            // テキスト138
            // 
            this.テキスト138.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト138.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト138.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト138.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト138.Height = 0.1070867F;
            this.テキスト138.Left = 4.693701F;
            this.テキスト138.Name = "テキスト138";
            this.テキスト138.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト138.Tag = "";
            this.テキスト138.Text = "その他";
            this.テキスト138.Top = 1.862599F;
            this.テキスト138.Width = 0.3897636F;
            // 
            // textBox30
            // 
            this.textBox30.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox30.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox30.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox30.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox30.CanGrow = false;
            this.textBox30.Height = 0.4224421F;
            this.textBox30.Left = 3.809449F;
            this.textBox30.Name = "textBox30";
            this.textBox30.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox30.Tag = "";
            this.textBox30.Text = "16歳未満\r\n扶養親族の数";
            this.textBox30.Top = 1.546063F;
            this.textBox30.Width = 0.4854331F;
            // 
            // textBox31
            // 
            this.textBox31.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox31.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox31.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox31.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox31.DataField = "ITEM138";
            this.textBox31.Height = 0.2082678F;
            this.textBox31.Left = 5.083465F;
            this.textBox31.Name = "textBox31";
            this.textBox31.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox31.Tag = "";
            this.textBox31.Text = "ITEM138";
            this.textBox31.Top = 1.968504F;
            this.textBox31.Width = 0.4641754F;
            // 
            // textBox32
            // 
            this.textBox32.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox32.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox32.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox32.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox32.CanGrow = false;
            this.textBox32.Height = 0.4224409F;
            this.textBox32.Left = 5.083465F;
            this.textBox32.Name = "textBox32";
            this.textBox32.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox32.Tag = "";
            this.textBox32.Text = "非住居者である親族の数";
            this.textBox32.Top = 1.546063F;
            this.textBox32.Width = 0.4641754F;
            // 
            // textBox34
            // 
            this.textBox34.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox34.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox34.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox34.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox34.DataField = "ITEM069";
            this.textBox34.Height = 0.1576389F;
            this.textBox34.Left = 0.1275591F;
            this.textBox34.Name = "textBox34";
            this.textBox34.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox34.Tag = "";
            this.textBox34.Text = "ITEM069";
            this.textBox34.Top = 6.692914F;
            this.textBox34.Width = 0.2131944F;
            // 
            // textBox35
            // 
            this.textBox35.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox35.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox35.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox35.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox35.DataField = "ITEM070";
            this.textBox35.Height = 0.1576389F;
            this.textBox35.Left = 0.3409449F;
            this.textBox35.Name = "textBox35";
            this.textBox35.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox35.Tag = "";
            this.textBox35.Text = "ITEM070";
            this.textBox35.Top = 6.692914F;
            this.textBox35.Width = 0.2131944F;
            // 
            // textBox36
            // 
            this.textBox36.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox36.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox36.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox36.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox36.DataField = "ITEM071";
            this.textBox36.Height = 0.1576389F;
            this.textBox36.Left = 0.5541383F;
            this.textBox36.Name = "textBox36";
            this.textBox36.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox36.Tag = "";
            this.textBox36.Text = "ITEM071";
            this.textBox36.Top = 6.692914F;
            this.textBox36.Width = 0.2127909F;
            // 
            // textBox37
            // 
            this.textBox37.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox37.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox37.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox37.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox37.DataField = "ITEM072";
            this.textBox37.Height = 0.1576389F;
            this.textBox37.Left = 0.7669292F;
            this.textBox37.Name = "textBox37";
            this.textBox37.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox37.Tag = "";
            this.textBox37.Text = "ITEM072";
            this.textBox37.Top = 6.692914F;
            this.textBox37.Width = 0.2131944F;
            // 
            // textBox38
            // 
            this.textBox38.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox38.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox38.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox38.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox38.DataField = "ITEM073";
            this.textBox38.Height = 0.1576389F;
            this.textBox38.Left = 0.9805286F;
            this.textBox38.Name = "textBox38";
            this.textBox38.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox38.Tag = "";
            this.textBox38.Text = "ITEM073";
            this.textBox38.Top = 6.694095F;
            this.textBox38.Width = 0.2131944F;
            // 
            // textBox90
            // 
            this.textBox90.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox90.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox90.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox90.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox90.DataField = "ITEM074";
            this.textBox90.Height = 0.1576389F;
            this.textBox90.Left = 1.193701F;
            this.textBox90.Name = "textBox90";
            this.textBox90.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox90.Tag = "";
            this.textBox90.Text = "ITEM074";
            this.textBox90.Top = 6.694095F;
            this.textBox90.Width = 0.2334646F;
            // 
            // textBox91
            // 
            this.textBox91.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox91.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox91.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox91.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox91.DataField = "ITEM075";
            this.textBox91.Height = 0.1576389F;
            this.textBox91.Left = 1.427166F;
            this.textBox91.Name = "textBox91";
            this.textBox91.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox91.Tag = "";
            this.textBox91.Text = "ITEM075";
            this.textBox91.Top = 6.694095F;
            this.textBox91.Width = 0.236029F;
            // 
            // textBox92
            // 
            this.textBox92.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox92.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox92.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox92.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox92.DataField = "ITEM076";
            this.textBox92.Height = 0.1574803F;
            this.textBox92.Left = 1.666142F;
            this.textBox92.Name = "textBox92";
            this.textBox92.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox92.Tag = "";
            this.textBox92.Text = "ITEM076";
            this.textBox92.Top = 6.694095F;
            this.textBox92.Width = 0.2362205F;
            // 
            // textBox93
            // 
            this.textBox93.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox93.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox93.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox93.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox93.DataField = "ITEM077";
            this.textBox93.Height = 0.1574803F;
            this.textBox93.Left = 1.902362F;
            this.textBox93.Name = "textBox93";
            this.textBox93.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox93.Tag = "";
            this.textBox93.Text = "ITEM077";
            this.textBox93.Top = 6.694095F;
            this.textBox93.Width = 0.2362205F;
            // 
            // textBox94
            // 
            this.textBox94.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox94.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox94.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox94.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox94.DataField = "ITEM078";
            this.textBox94.Height = 0.1576389F;
            this.textBox94.Left = 2.138977F;
            this.textBox94.Name = "textBox94";
            this.textBox94.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox94.Tag = "";
            this.textBox94.Text = "ITEM078";
            this.textBox94.Top = 6.694095F;
            this.textBox94.Width = 0.2131944F;
            // 
            // textBox95
            // 
            this.textBox95.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox95.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox95.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox95.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox95.DataField = "ITEM079";
            this.textBox95.Height = 0.156299F;
            this.textBox95.Left = 2.352362F;
            this.textBox95.Name = "textBox95";
            this.textBox95.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox95.Tag = "";
            this.textBox95.Text = "ITEM079";
            this.textBox95.Top = 6.695276F;
            this.textBox95.Width = 0.2131944F;
            // 
            // textBox96
            // 
            this.textBox96.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox96.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox96.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox96.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox96.DataField = "ITEM081";
            this.textBox96.Height = 0.3877954F;
            this.textBox96.Left = 2.565355F;
            this.textBox96.Name = "textBox96";
            this.textBox96.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox96.Tag = "";
            this.textBox96.Text = "ITEM081";
            this.textBox96.Top = 6.463781F;
            this.textBox96.Width = 0.2478399F;
            // 
            // textBox97
            // 
            this.textBox97.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox97.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox97.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox97.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox97.DataField = "ITEM085";
            this.textBox97.Height = 0.3877953F;
            this.textBox97.Left = 2.813386F;
            this.textBox97.Name = "textBox97";
            this.textBox97.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox97.Tag = "";
            this.textBox97.Text = "ITEM085";
            this.textBox97.Top = 6.463781F;
            this.textBox97.Width = 0.2480315F;
            // 
            // textBox98
            // 
            this.textBox98.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox98.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox98.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox98.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox98.DataField = "ITEM086";
            this.textBox98.Height = 0.3877953F;
            this.textBox98.Left = 3.061418F;
            this.textBox98.Name = "textBox98";
            this.textBox98.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox98.Tag = "";
            this.textBox98.Text = "ITEM086";
            this.textBox98.Top = 6.463781F;
            this.textBox98.Width = 0.2480315F;
            // 
            // textBox99
            // 
            this.textBox99.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox99.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox99.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox99.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox99.DataField = "ITEM087";
            this.textBox99.Height = 0.3877953F;
            this.textBox99.Left = 3.309449F;
            this.textBox99.Name = "textBox99";
            this.textBox99.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox99.Tag = "";
            this.textBox99.Text = "ITEM087";
            this.textBox99.Top = 6.463781F;
            this.textBox99.Width = 0.2480315F;
            // 
            // textBox100
            // 
            this.textBox100.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox100.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox100.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox100.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox100.DataField = "ITEM088";
            this.textBox100.Height = 0.3877953F;
            this.textBox100.Left = 3.557481F;
            this.textBox100.Name = "textBox100";
            this.textBox100.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox100.Tag = "";
            this.textBox100.Text = "ITEM088";
            this.textBox100.Top = 6.463781F;
            this.textBox100.Width = 0.2480315F;
            // 
            // textBox101
            // 
            this.textBox101.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox101.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox101.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox101.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox101.Height = 0.1180556F;
            this.textBox101.Left = 2.565355F;
            this.textBox101.Name = "textBox101";
            this.textBox101.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox101.Tag = "";
            this.textBox101.Text = "中 途 就・退 職";
            this.textBox101.Top = 6.225198F;
            this.textBox101.Width = 1.244094F;
            // 
            // textBox102
            // 
            this.textBox102.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox102.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox102.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox102.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox102.Height = 0.1181102F;
            this.textBox102.Left = 2.565355F;
            this.textBox102.Name = "textBox102";
            this.textBox102.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox102.Tag = "";
            this.textBox102.Text = "就職";
            this.textBox102.Top = 6.34567F;
            this.textBox102.Width = 0.2480315F;
            // 
            // textBox104
            // 
            this.textBox104.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox104.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox104.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox104.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox104.Height = 0.4665354F;
            this.textBox104.Left = 0.1275591F;
            this.textBox104.Name = "textBox104";
            this.textBox104.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox104.Tag = "";
            this.textBox104.Text = "未\r\n成\r\n年\r\n者";
            this.textBox104.Top = 6.226378F;
            this.textBox104.Width = 0.2133858F;
            // 
            // textBox106
            // 
            this.textBox106.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox106.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox106.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox106.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox106.Height = 0.4665354F;
            this.textBox106.Left = 0.5537558F;
            this.textBox106.Name = "textBox106";
            this.textBox106.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox106.Tag = "";
            this.textBox106.Text = "死\r\n亡\r\n退\r\n職";
            this.textBox106.Top = 6.226182F;
            this.textBox106.Width = 0.2133858F;
            // 
            // textBox107
            // 
            this.textBox107.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox107.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox107.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox107.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox107.Height = 0.4665354F;
            this.textBox107.Left = 0.7669511F;
            this.textBox107.Name = "textBox107";
            this.textBox107.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox107.Tag = "";
            this.textBox107.Text = "災\r\n害\r\n者";
            this.textBox107.Top = 6.226182F;
            this.textBox107.Width = 0.2133858F;
            // 
            // textBox108
            // 
            this.textBox108.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox108.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox108.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox108.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox108.Height = 0.4665354F;
            this.textBox108.Left = 0.9801458F;
            this.textBox108.Name = "textBox108";
            this.textBox108.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox108.Tag = "";
            this.textBox108.Text = "乙\r\n\r\n欄";
            this.textBox108.Top = 6.226182F;
            this.textBox108.Width = 0.2133858F;
            // 
            // textBox109
            // 
            this.textBox109.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox109.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox109.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox109.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox109.Height = 0.1180556F;
            this.textBox109.Left = 1.193701F;
            this.textBox109.Name = "textBox109";
            this.textBox109.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox109.Tag = "";
            this.textBox109.Text = "本人が障害者";
            this.textBox109.Top = 6.226378F;
            this.textBox109.Width = 0.472441F;
            // 
            // textBox110
            // 
            this.textBox110.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox110.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox110.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox110.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox110.Height = 0.348425F;
            this.textBox110.Left = 1.193701F;
            this.textBox110.Name = "textBox110";
            this.textBox110.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox110.Tag = "";
            this.textBox110.Text = "特\r\n\r\n別";
            this.textBox110.Top = 6.344489F;
            this.textBox110.Width = 0.2362205F;
            // 
            // textBox111
            // 
            this.textBox111.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox111.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox111.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox111.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox111.CanGrow = false;
            this.textBox111.Height = 0.348425F;
            this.textBox111.Left = 1.427166F;
            this.textBox111.Name = "textBox111";
            this.textBox111.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox111.Tag = "";
            this.textBox111.Text = "そ\r\nの\r\n他";
            this.textBox111.Top = 6.344489F;
            this.textBox111.Width = 0.2362205F;
            // 
            // textBox112
            // 
            this.textBox112.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox112.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox112.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox112.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox112.Height = 0.1180556F;
            this.textBox112.Left = 1.666142F;
            this.textBox112.Name = "textBox112";
            this.textBox112.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox112.Tag = "";
            this.textBox112.Text = "寡　婦";
            this.textBox112.Top = 6.226378F;
            this.textBox112.Width = 0.4724409F;
            // 
            // textBox113
            // 
            this.textBox113.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox113.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox113.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox113.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox113.Height = 0.3484252F;
            this.textBox113.Left = 1.666142F;
            this.textBox113.Name = "textBox113";
            this.textBox113.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox113.Tag = "";
            this.textBox113.Text = "一\r\n般";
            this.textBox113.Top = 6.344489F;
            this.textBox113.Width = 0.2362205F;
            // 
            // textBox114
            // 
            this.textBox114.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox114.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox114.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox114.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox114.Height = 0.3484252F;
            this.textBox114.Left = 1.902172F;
            this.textBox114.Name = "textBox114";
            this.textBox114.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox114.Tag = "";
            this.textBox114.Text = "特\r\n別";
            this.textBox114.Top = 6.344489F;
            this.textBox114.Width = 0.2362205F;
            // 
            // textBox115
            // 
            this.textBox115.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox115.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox115.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox115.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox115.Height = 0.4665354F;
            this.textBox115.Left = 2.138583F;
            this.textBox115.Name = "textBox115";
            this.textBox115.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox115.Tag = "";
            this.textBox115.Text = "寡　夫";
            this.textBox115.Top = 6.226378F;
            this.textBox115.Width = 0.2133858F;
            // 
            // textBox116
            // 
            this.textBox116.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox116.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox116.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox116.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox116.CanGrow = false;
            this.textBox116.Height = 0.4665354F;
            this.textBox116.Left = 2.351969F;
            this.textBox116.Name = "textBox116";
            this.textBox116.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1; ddo-font-vertical: true";
            this.textBox116.Tag = "";
            this.textBox116.Text = "勤\r\n労\r\n学\r\n生";
            this.textBox116.Top = 6.226378F;
            this.textBox116.Width = 0.2133858F;
            // 
            // textBox117
            // 
            this.textBox117.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox117.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox117.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox117.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox117.Height = 0.1181102F;
            this.textBox117.Left = 2.813386F;
            this.textBox117.Name = "textBox117";
            this.textBox117.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox117.Tag = "";
            this.textBox117.Text = "退職";
            this.textBox117.Top = 6.34567F;
            this.textBox117.Width = 0.2480315F;
            // 
            // textBox118
            // 
            this.textBox118.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox118.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox118.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox118.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox118.Height = 0.1181102F;
            this.textBox118.Left = 3.061418F;
            this.textBox118.Name = "textBox118";
            this.textBox118.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox118.Tag = "";
            this.textBox118.Text = "年";
            this.textBox118.Top = 6.34567F;
            this.textBox118.Width = 0.2480315F;
            // 
            // textBox119
            // 
            this.textBox119.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox119.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox119.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox119.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox119.Height = 0.1181102F;
            this.textBox119.Left = 3.309449F;
            this.textBox119.Name = "textBox119";
            this.textBox119.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox119.Tag = "";
            this.textBox119.Text = "月";
            this.textBox119.Top = 6.34567F;
            this.textBox119.Width = 0.2480315F;
            // 
            // textBox120
            // 
            this.textBox120.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox120.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox120.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox120.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox120.Height = 0.1181102F;
            this.textBox120.Left = 3.557481F;
            this.textBox120.Name = "textBox120";
            this.textBox120.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox120.Tag = "";
            this.textBox120.Text = "日";
            this.textBox120.Top = 6.34567F;
            this.textBox120.Width = 0.2480315F;
            // 
            // textBox121
            // 
            this.textBox121.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox121.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox121.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox121.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox121.DataField = "ITEM089";
            this.textBox121.Height = 0.3877953F;
            this.textBox121.Left = 3.805512F;
            this.textBox121.Name = "textBox121";
            this.textBox121.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox121.Tag = "";
            this.textBox121.Text = "ITEM089";
            this.textBox121.Top = 6.463781F;
            this.textBox121.Width = 0.2480315F;
            // 
            // textBox122
            // 
            this.textBox122.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox122.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox122.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox122.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox122.DataField = "ITEM090";
            this.textBox122.Height = 0.3877953F;
            this.textBox122.Left = 4.053544F;
            this.textBox122.Name = "textBox122";
            this.textBox122.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox122.Tag = "";
            this.textBox122.Text = "ITEM090";
            this.textBox122.Top = 6.463781F;
            this.textBox122.Width = 0.2480315F;
            // 
            // textBox123
            // 
            this.textBox123.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox123.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox123.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox123.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox123.DataField = "ITEM091";
            this.textBox123.Height = 0.3877953F;
            this.textBox123.Left = 4.301575F;
            this.textBox123.Name = "textBox123";
            this.textBox123.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox123.Tag = "";
            this.textBox123.Text = "ITEM091";
            this.textBox123.Top = 6.463781F;
            this.textBox123.Width = 0.2480315F;
            // 
            // textBox124
            // 
            this.textBox124.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox124.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox124.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox124.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox124.DataField = "ITEM092";
            this.textBox124.Height = 0.3877953F;
            this.textBox124.Left = 4.549607F;
            this.textBox124.Name = "textBox124";
            this.textBox124.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox124.Tag = "";
            this.textBox124.Text = "ITEM092";
            this.textBox124.Top = 6.463781F;
            this.textBox124.Width = 0.2480315F;
            // 
            // textBox125
            // 
            this.textBox125.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox125.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox125.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox125.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox125.DataField = "ITEM093";
            this.textBox125.Height = 0.3877953F;
            this.textBox125.Left = 4.797638F;
            this.textBox125.Name = "textBox125";
            this.textBox125.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox125.Tag = "";
            this.textBox125.Text = "ITEM093";
            this.textBox125.Top = 6.463781F;
            this.textBox125.Width = 0.2480315F;
            // 
            // textBox126
            // 
            this.textBox126.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox126.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox126.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox126.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox126.Height = 0.1180556F;
            this.textBox126.Left = 3.805512F;
            this.textBox126.Name = "textBox126";
            this.textBox126.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox126.Tag = "";
            this.textBox126.Text = "受　給　者　生　年　月　日";
            this.textBox126.Top = 6.225198F;
            this.textBox126.Width = 1.742269F;
            // 
            // textBox128
            // 
            this.textBox128.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox128.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox128.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox128.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox128.Height = 0.1181102F;
            this.textBox128.Left = 3.805512F;
            this.textBox128.Name = "textBox128";
            this.textBox128.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox128.Tag = "";
            this.textBox128.Text = "明";
            this.textBox128.Top = 6.34567F;
            this.textBox128.Width = 0.2480315F;
            // 
            // textBox129
            // 
            this.textBox129.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox129.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox129.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox129.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox129.Height = 0.1181102F;
            this.textBox129.Left = 4.053544F;
            this.textBox129.Name = "textBox129";
            this.textBox129.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox129.Tag = "";
            this.textBox129.Text = "大";
            this.textBox129.Top = 6.34567F;
            this.textBox129.Width = 0.2480315F;
            // 
            // textBox130
            // 
            this.textBox130.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox130.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox130.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox130.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox130.Height = 0.1181102F;
            this.textBox130.Left = 4.301575F;
            this.textBox130.Name = "textBox130";
            this.textBox130.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox130.Tag = "";
            this.textBox130.Text = "昭";
            this.textBox130.Top = 6.34567F;
            this.textBox130.Width = 0.2480315F;
            // 
            // textBox131
            // 
            this.textBox131.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox131.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox131.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox131.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox131.Height = 0.1181102F;
            this.textBox131.Left = 4.549607F;
            this.textBox131.Name = "textBox131";
            this.textBox131.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox131.Tag = "";
            this.textBox131.Text = "平";
            this.textBox131.Top = 6.34567F;
            this.textBox131.Width = 0.2480315F;
            // 
            // textBox132
            // 
            this.textBox132.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox132.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox132.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox132.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox132.Height = 0.1181102F;
            this.textBox132.Left = 4.797638F;
            this.textBox132.Name = "textBox132";
            this.textBox132.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox132.Tag = "";
            this.textBox132.Text = "年";
            this.textBox132.Top = 6.34567F;
            this.textBox132.Width = 0.2480315F;
            // 
            // textBox133
            // 
            this.textBox133.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox133.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox133.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox133.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox133.DataField = "ITEM094";
            this.textBox133.Height = 0.3877953F;
            this.textBox133.Left = 5.04567F;
            this.textBox133.Name = "textBox133";
            this.textBox133.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox133.Tag = "";
            this.textBox133.Text = "ITEM094";
            this.textBox133.Top = 6.463781F;
            this.textBox133.Width = 0.2480315F;
            // 
            // textBox134
            // 
            this.textBox134.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox134.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox134.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox134.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox134.Height = 0.1181102F;
            this.textBox134.Left = 5.04567F;
            this.textBox134.Name = "textBox134";
            this.textBox134.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox134.Tag = "";
            this.textBox134.Text = "月";
            this.textBox134.Top = 6.34567F;
            this.textBox134.Width = 0.2480315F;
            // 
            // textBox135
            // 
            this.textBox135.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox135.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox135.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox135.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox135.DataField = "ITEM095";
            this.textBox135.Height = 0.3877953F;
            this.textBox135.Left = 5.293701F;
            this.textBox135.Name = "textBox135";
            this.textBox135.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox135.Tag = "";
            this.textBox135.Text = "ITEM095";
            this.textBox135.Top = 6.463781F;
            this.textBox135.Width = 0.2539372F;
            // 
            // textBox136
            // 
            this.textBox136.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox136.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox136.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox136.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox136.Height = 0.1181102F;
            this.textBox136.Left = 5.293701F;
            this.textBox136.Name = "textBox136";
            this.textBox136.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox136.Tag = "";
            this.textBox136.Text = "日";
            this.textBox136.Top = 6.34567F;
            this.textBox136.Width = 0.2539372F;
            // 
            // textBox170
            // 
            this.textBox170.DataField = "ITEM082";
            this.textBox170.Height = 0.1381944F;
            this.textBox170.Left = 3.061418F;
            this.textBox170.Name = "textBox170";
            this.textBox170.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.textBox170.Tag = "";
            this.textBox170.Text = "ITEM082";
            this.textBox170.Top = 6.463781F;
            this.textBox170.Width = 0.2478399F;
            // 
            // textBox171
            // 
            this.textBox171.DataField = "ITEM083";
            this.textBox171.Height = 0.1381944F;
            this.textBox171.Left = 3.330315F;
            this.textBox171.Name = "textBox171";
            this.textBox171.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.textBox171.Tag = "";
            this.textBox171.Text = "ITEM083";
            this.textBox171.Top = 6.463781F;
            this.textBox171.Width = 0.2131944F;
            // 
            // textBox172
            // 
            this.textBox172.DataField = "ITEM084";
            this.textBox172.Height = 0.1381944F;
            this.textBox172.Left = 3.587402F;
            this.textBox172.Name = "textBox172";
            this.textBox172.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.textBox172.Tag = "";
            this.textBox172.Text = "ITEM084";
            this.textBox172.Top = 6.463781F;
            this.textBox172.Width = 0.2131944F;
            // 
            // textBox80
            // 
            this.textBox80.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox80.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox80.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox80.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox80.Height = 0.2362205F;
            this.textBox80.Left = 0.3937026F;
            this.textBox80.Name = "textBox80";
            this.textBox80.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox80.Tag = "";
            this.textBox80.Text = "住所（居所）\r\n又 は 所在地";
            this.textBox80.Top = 7.087796F;
            this.textBox80.Width = 0.8870079F;
            // 
            // textBox2
            // 
            this.textBox2.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox2.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox2.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox2.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox2.DataField = "ITEM045";
            this.textBox2.Height = 0.3541667F;
            this.textBox2.Left = 0.127559F;
            this.textBox2.Name = "textBox2";
            this.textBox2.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: right; vertical-align: bottom; ddo-char-set: 1";
            this.textBox2.Tag = "";
            this.textBox2.Text = "ITEM045";
            this.textBox2.Top = 2.505118F;
            this.textBox2.Width = 1.367717F;
            // 
            // textBox39
            // 
            this.textBox39.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox39.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox39.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox39.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox39.DataField = "ITEM049";
            this.textBox39.Height = 0.3541667F;
            this.textBox39.Left = 4.198032F;
            this.textBox39.Name = "textBox39";
            this.textBox39.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: right; vertical-align: bottom; ddo-char-set: 1";
            this.textBox39.Tag = "";
            this.textBox39.Text = "ITEM049";
            this.textBox39.Top = 2.505118F;
            this.textBox39.Width = 1.349607F;
            // 
            // textBox40
            // 
            this.textBox40.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox40.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox40.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox40.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox40.Height = 0.3232284F;
            this.textBox40.Left = 0.1275591F;
            this.textBox40.Name = "textBox40";
            this.textBox40.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox40.Tag = "";
            this.textBox40.Text = "社会保険料等 の 金 額";
            this.textBox40.Top = 2.18189F;
            this.textBox40.Width = 1.358662F;
            // 
            // textBox42
            // 
            this.textBox42.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox42.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox42.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox42.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox42.DataField = "ITEM048";
            this.textBox42.Height = 0.3541667F;
            this.textBox42.Left = 2.841339F;
            this.textBox42.Name = "textBox42";
            this.textBox42.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: right; vertical-align: bottom; ddo-char-set: 1";
            this.textBox42.Tag = "";
            this.textBox42.Text = "ITEM048";
            this.textBox42.Top = 2.505118F;
            this.textBox42.Width = 1.356693F;
            // 
            // textBox44
            // 
            this.textBox44.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox44.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox44.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox44.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox44.DataField = "ITEM047";
            this.textBox44.Height = 0.3541667F;
            this.textBox44.Left = 1.486221F;
            this.textBox44.Name = "textBox44";
            this.textBox44.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: right; vertical-align: bottom; ddo-char-set: 1";
            this.textBox44.Tag = "";
            this.textBox44.Text = "ITEM047";
            this.textBox44.Top = 2.505118F;
            this.textBox44.Width = 1.362626F;
            // 
            // textBox165
            // 
            this.textBox165.DataField = "ITEM046";
            this.textBox165.Height = 0.09582743F;
            this.textBox165.Left = 0.1551181F;
            this.textBox165.Name = "textBox165";
            this.textBox165.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox165.Tag = "";
            this.textBox165.Text = "ITEM046";
            this.textBox165.Top = 2.546457F;
            this.textBox165.Width = 1.262102F;
            // 
            // label17
            // 
            this.label17.Height = 0.08622047F;
            this.label17.HyperLink = null;
            this.label17.Left = 2.696851F;
            this.label17.Name = "label17";
            this.label17.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label17.Tag = "";
            this.label17.Text = "円";
            this.label17.Top = 2.516929F;
            this.label17.Width = 0.1240157F;
            // 
            // label18
            // 
            this.label18.Height = 0.08622047F;
            this.label18.HyperLink = null;
            this.label18.Left = 4.053544F;
            this.label18.Name = "label18";
            this.label18.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label18.Tag = "";
            this.label18.Text = "円";
            this.label18.Top = 2.516929F;
            this.label18.Width = 0.1240157F;
            // 
            // label19
            // 
            this.label19.Height = 0.08611111F;
            this.label19.HyperLink = null;
            this.label19.Left = 5.413386F;
            this.label19.Name = "label19";
            this.label19.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label19.Tag = "";
            this.label19.Text = "円";
            this.label19.Top = 2.516929F;
            this.label19.Width = 0.1241202F;
            // 
            // textBox45
            // 
            this.textBox45.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox45.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox45.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox45.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox45.Height = 0.214961F;
            this.textBox45.Left = 2.897638F;
            this.textBox45.Name = "textBox45";
            this.textBox45.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: bold; tex" +
    "t-align: center; ddo-char-set: 1";
            this.textBox45.Tag = "";
            this.textBox45.Text = null;
            this.textBox45.Top = 1.968898F;
            this.textBox45.Width = 0.3307087F;
            // 
            // textBox46
            // 
            this.textBox46.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox46.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox46.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox46.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox46.DataField = "ITEM041";
            this.textBox46.Height = 0.214961F;
            this.textBox46.Left = 3.228347F;
            this.textBox46.Name = "textBox46";
            this.textBox46.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox46.Tag = "";
            this.textBox46.Text = "ITEM041";
            this.textBox46.Top = 1.966929F;
            this.textBox46.Width = 0.3149602F;
            // 
            // textBox47
            // 
            this.textBox47.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox47.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox47.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox47.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox47.Height = 0.214961F;
            this.textBox47.Left = 3.517323F;
            this.textBox47.Name = "textBox47";
            this.textBox47.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: bold; tex" +
    "t-align: center; ddo-char-set: 1";
            this.textBox47.Tag = "";
            this.textBox47.Text = null;
            this.textBox47.Top = 1.966929F;
            this.textBox47.Width = 0.2921261F;
            // 
            // textBox49
            // 
            this.textBox49.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox49.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox49.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox49.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox49.DataField = "ITEM043";
            this.textBox49.Height = 0.203937F;
            this.textBox49.Left = 4.505907F;
            this.textBox49.Name = "textBox49";
            this.textBox49.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox49.Tag = "";
            this.textBox49.Text = "ITEM043";
            this.textBox49.Top = 1.974016F;
            this.textBox49.Width = 0.1968504F;
            // 
            // textBox50
            // 
            this.textBox50.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox50.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox50.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox50.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox50.DataField = "ITEM044";
            this.textBox50.Height = 0.2070867F;
            this.textBox50.Left = 4.693701F;
            this.textBox50.Name = "textBox50";
            this.textBox50.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox50.Tag = "";
            this.textBox50.Text = "ITEM044";
            this.textBox50.Top = 1.971654F;
            this.textBox50.Width = 0.3896542F;
            // 
            // label14
            // 
            this.label14.Height = 0.08611111F;
            this.label14.HyperLink = null;
            this.label14.Left = 3.355512F;
            this.label14.Name = "label14";
            this.label14.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label14.Tag = "";
            this.label14.Text = "人";
            this.label14.Top = 1.970866F;
            this.label14.Width = 0.15625F;
            // 
            // label16
            // 
            this.label16.Height = 0.08740143F;
            this.label16.HyperLink = null;
            this.label16.Left = 4.767323F;
            this.label16.Name = "label16";
            this.label16.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label16.Tag = "";
            this.label16.Text = "人";
            this.label16.Top = 1.974016F;
            this.label16.Width = 0.2896156F;
            // 
            // textBox51
            // 
            this.textBox51.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox51.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox51.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox51.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox51.DataField = "ITEM038";
            this.textBox51.Height = 0.214961F;
            this.textBox51.Left = 1.693503F;
            this.textBox51.Name = "textBox51";
            this.textBox51.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox51.Tag = "";
            this.textBox51.Text = "ITEM038";
            this.textBox51.Top = 1.968504F;
            this.textBox51.Width = 0.2677166F;
            // 
            // textBox103
            // 
            this.textBox103.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox103.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox103.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox103.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox103.Height = 0.2149609F;
            this.textBox103.Left = 1.964567F;
            this.textBox103.Name = "textBox103";
            this.textBox103.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: bold; tex" +
    "t-align: center; ddo-char-set: 1";
            this.textBox103.Tag = "";
            this.textBox103.Text = null;
            this.textBox103.Top = 1.968504F;
            this.textBox103.Width = 0.2677166F;
            // 
            // textBox137
            // 
            this.textBox137.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox137.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox137.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox137.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox137.DataField = "ITEM040";
            this.textBox137.Height = 0.2137799F;
            this.textBox137.Left = 2.566536F;
            this.textBox137.Name = "textBox137";
            this.textBox137.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox137.Tag = "";
            this.textBox137.Text = "ITEM040";
            this.textBox137.Top = 1.968504F;
            this.textBox137.Width = 0.3307087F;
            // 
            // textBox138
            // 
            this.textBox138.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox138.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox138.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox138.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox138.DataField = "ITEM039";
            this.textBox138.Height = 0.2137799F;
            this.textBox138.Left = 2.232284F;
            this.textBox138.Name = "textBox138";
            this.textBox138.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox138.Tag = "";
            this.textBox138.Text = "ITEM039";
            this.textBox138.Top = 1.968504F;
            this.textBox138.Width = 0.3348432F;
            // 
            // label9
            // 
            this.label9.Height = 0.08611111F;
            this.label9.HyperLink = null;
            this.label9.Left = 1.693307F;
            this.label9.Name = "label9";
            this.label9.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label9.Tag = "";
            this.label9.Text = "人";
            this.label9.Top = 1.974016F;
            this.label9.Width = 0.2751969F;
            // 
            // label10
            // 
            this.label10.Height = 0.08611111F;
            this.label10.HyperLink = null;
            this.label10.Left = 2.237795F;
            this.label10.Name = "label10";
            this.label10.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.label10.Tag = "";
            this.label10.Text = "内              人";
            this.label10.Top = 1.974016F;
            this.label10.Width = 0.6614062F;
            // 
            // label13
            // 
            this.label13.Height = 0.08611111F;
            this.label13.HyperLink = null;
            this.label13.Left = 1.964567F;
            this.label13.Name = "label13";
            this.label13.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 4.5pt; font-weight: normal; text-ali" +
    "gn: center; ddo-char-set: 1";
            this.label13.Tag = "";
            this.label13.Text = "従人";
            this.label13.Top = 1.980315F;
            this.label13.Width = 0.2716537F;
            // 
            // label11
            // 
            this.label11.Height = 0.08611111F;
            this.label11.HyperLink = null;
            this.label11.Left = 2.944488F;
            this.label11.Name = "label11";
            this.label11.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 4.5pt; font-weight: normal; text-ali" +
    "gn: center; ddo-char-set: 1";
            this.label11.Tag = "";
            this.label11.Text = "従人";
            this.label11.Top = 1.974016F;
            this.label11.Width = 0.2523623F;
            // 
            // label83
            // 
            this.label83.Height = 0.08611111F;
            this.label83.HyperLink = null;
            this.label83.Left = 3.543701F;
            this.label83.Name = "label83";
            this.label83.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 4.5pt; font-weight: normal; text-ali" +
    "gn: center; ddo-char-set: 1";
            this.label83.Tag = "";
            this.label83.Text = "従人";
            this.label83.Top = 1.970866F;
            this.label83.Width = 0.2657483F;
            // 
            // textBox55
            // 
            this.textBox55.Height = 0.2362205F;
            this.textBox55.Left = 0.4712599F;
            this.textBox55.Name = "textBox55";
            this.textBox55.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox55.Tag = "";
            this.textBox55.Text = "氏名";
            this.textBox55.Top = 4.98504F;
            this.textBox55.Width = 0.4751969F;
            // 
            // textBox58
            // 
            this.textBox58.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox58.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox58.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox58.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox58.DataField = "ITEM113";
            this.textBox58.Height = 0.1555119F;
            this.textBox58.Left = 0.9464568F;
            this.textBox58.Name = "textBox58";
            this.textBox58.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: justify; text-justify: distribute-all-lines; vertical" +
    "-align: middle; ddo-char-set: 1";
            this.textBox58.Tag = "";
            this.textBox58.Text = "1 2 3 4 5 6 7 8 9 1 2";
            this.textBox58.Top = 5.239371F;
            this.textBox58.Width = 1.651968F;
            // 
            // textBox59
            // 
            this.textBox59.Height = 0.1299213F;
            this.textBox59.Left = 0.4712599F;
            this.textBox59.Name = "textBox59";
            this.textBox59.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5.6pt; font" +
    "-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox59.Tag = "";
            this.textBox59.Text = "(フリガナ)";
            this.textBox59.Top = 4.981496F;
            this.textBox59.Width = 0.4751969F;
            // 
            // textBox61
            // 
            this.textBox61.Height = 0.1590554F;
            this.textBox61.Left = 0.472441F;
            this.textBox61.Name = "textBox61";
            this.textBox61.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox61.Tag = "";
            this.textBox61.Text = "氏名";
            this.textBox61.Top = 4.665355F;
            this.textBox61.Width = 0.4740158F;
            // 
            // textBox63
            // 
            this.textBox63.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox63.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox63.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox63.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox63.Height = 0.1559055F;
            this.textBox63.Left = 0.472441F;
            this.textBox63.Name = "textBox63";
            this.textBox63.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox63.Tag = "";
            this.textBox63.Text = "個人番号";
            this.textBox63.Top = 4.826772F;
            this.textBox63.Width = 0.4740157F;
            // 
            // textBox64
            // 
            this.textBox64.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox64.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox64.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox64.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox64.DataField = "ITEM109";
            this.textBox64.Height = 0.1555119F;
            this.textBox64.Left = 0.9464568F;
            this.textBox64.Name = "textBox64";
            this.textBox64.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: justify; text-justify: distribute-all-lines; vertical" +
    "-align: middle; ddo-char-set: 1";
            this.textBox64.Tag = "";
            this.textBox64.Text = "1 2 3 4 5 6 7 8 9 1 2";
            this.textBox64.Top = 4.827165F;
            this.textBox64.Width = 1.651181F;
            // 
            // textBox65
            // 
            this.textBox65.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox65.Height = 0.1299213F;
            this.textBox65.Left = 0.472441F;
            this.textBox65.Name = "textBox65";
            this.textBox65.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5.6pt; font" +
    "-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox65.Tag = "";
            this.textBox65.Text = "(フリガナ)";
            this.textBox65.Top = 4.57126F;
            this.textBox65.Width = 0.4740158F;
            // 
            // textBox67
            // 
            this.textBox67.Height = 0.2362205F;
            this.textBox67.Left = 0.4712599F;
            this.textBox67.Name = "textBox67";
            this.textBox67.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox67.Tag = "";
            this.textBox67.Text = "氏名";
            this.textBox67.Top = 5.420866F;
            this.textBox67.Width = 0.4751969F;
            // 
            // textBox70
            // 
            this.textBox70.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox70.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox70.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox70.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox70.DataField = "ITEM117";
            this.textBox70.Height = 0.1555119F;
            this.textBox70.Left = 0.9488189F;
            this.textBox70.Name = "textBox70";
            this.textBox70.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: justify; text-justify: distribute-all-lines; vertical" +
    "-align: middle; ddo-char-set: 1";
            this.textBox70.Tag = "";
            this.textBox70.Text = "1 2 3 4 5 6 7 8 9 1 2";
            this.textBox70.Top = 5.653936F;
            this.textBox70.Width = 1.651969F;
            // 
            // textBox71
            // 
            this.textBox71.Height = 0.1220472F;
            this.textBox71.Left = 0.4712599F;
            this.textBox71.Name = "textBox71";
            this.textBox71.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5.6pt; font" +
    "-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox71.Tag = "";
            this.textBox71.Text = "(フリガナ)";
            this.textBox71.Top = 5.399607F;
            this.textBox71.Width = 0.4751969F;
            // 
            // textBox73
            // 
            this.textBox73.Height = 0.2208658F;
            this.textBox73.Left = 0.4712599F;
            this.textBox73.Name = "textBox73";
            this.textBox73.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox73.Tag = "";
            this.textBox73.Text = "氏名";
            this.textBox73.Top = 5.830709F;
            this.textBox73.Width = 0.4751969F;
            // 
            // textBox75
            // 
            this.textBox75.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox75.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox75.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox75.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox75.Height = 0.1559055F;
            this.textBox75.Left = 0.4712599F;
            this.textBox75.Name = "textBox75";
            this.textBox75.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox75.Tag = "";
            this.textBox75.Text = "個人番号";
            this.textBox75.Top = 6.062993F;
            this.textBox75.Width = 0.4751968F;
            // 
            // textBox76
            // 
            this.textBox76.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox76.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox76.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox76.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox76.DataField = "ITEM121";
            this.textBox76.Height = 0.1555119F;
            this.textBox76.Left = 0.9496064F;
            this.textBox76.Name = "textBox76";
            this.textBox76.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: justify; text-justify: distribute-all-lines; vertical" +
    "-align: middle; ddo-char-set: 1";
            this.textBox76.Tag = "";
            this.textBox76.Text = "1 2 3 4 5 6 7 8 9 1 2";
            this.textBox76.Top = 6.065748F;
            this.textBox76.Width = 1.651181F;
            // 
            // textBox77
            // 
            this.textBox77.Height = 0.1299213F;
            this.textBox77.Left = 0.4712599F;
            this.textBox77.Name = "textBox77";
            this.textBox77.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5.6pt; font" +
    "-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox77.Tag = "";
            this.textBox77.Text = "(フリガナ)";
            this.textBox77.Top = 5.809449F;
            this.textBox77.Width = 0.4751969F;
            // 
            // textBox144
            // 
            this.textBox144.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox144.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox144.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox144.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox144.CanGrow = false;
            this.textBox144.DataField = "ITEM064";
            this.textBox144.Height = 0.215748F;
            this.textBox144.Left = 3.094488F;
            this.textBox144.Name = "textBox144";
            this.textBox144.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox144.Tag = "";
            this.textBox144.Text = "ITEM064";
            this.textBox144.Top = 3.627559F;
            this.textBox144.Width = 0.4937008F;
            // 
            // textBox145
            // 
            this.textBox145.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox145.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox145.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox145.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox145.Height = 0.215748F;
            this.textBox145.Left = 2.600789F;
            this.textBox145.Name = "textBox145";
            this.textBox145.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 4.6pt; font" +
    "-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox145.Tag = "";
            this.textBox145.Text = "介護医療保険料\r\nの金額";
            this.textBox145.Top = 3.627559F;
            this.textBox145.Width = 0.4937008F;
            // 
            // textBox146
            // 
            this.textBox146.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox146.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox146.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox146.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox146.CanGrow = false;
            this.textBox146.DataField = "ITEM065";
            this.textBox146.Height = 0.215748F;
            this.textBox146.Left = 4.081891F;
            this.textBox146.Name = "textBox146";
            this.textBox146.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox146.Tag = "";
            this.textBox146.Text = "ITEM065";
            this.textBox146.Top = 3.627559F;
            this.textBox146.Width = 0.4937008F;
            // 
            // textBox147
            // 
            this.textBox147.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox147.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox147.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox147.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox147.Height = 0.215748F;
            this.textBox147.Left = 3.588189F;
            this.textBox147.Name = "textBox147";
            this.textBox147.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 4.7pt; font" +
    "-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox147.Tag = "";
            this.textBox147.Text = "新個人年金保険料の金額";
            this.textBox147.Top = 3.627559F;
            this.textBox147.Width = 0.4937008F;
            // 
            // textBox150
            // 
            this.textBox150.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox150.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox150.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox150.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox150.CanGrow = false;
            this.textBox150.DataField = "ITEM066";
            this.textBox150.Height = 0.215748F;
            this.textBox150.Left = 5.028347F;
            this.textBox150.Name = "textBox150";
            this.textBox150.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox150.Tag = "";
            this.textBox150.Text = "ITEM066";
            this.textBox150.Top = 3.62874F;
            this.textBox150.Width = 0.5192914F;
            // 
            // textBox151
            // 
            this.textBox151.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox151.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox151.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox151.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox151.Height = 0.215748F;
            this.textBox151.Left = 4.562994F;
            this.textBox151.Name = "textBox151";
            this.textBox151.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 4.7pt; font" +
    "-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox151.Tag = "";
            this.textBox151.Text = "旧個人年金\r\n保険料の金額";
            this.textBox151.Top = 3.62874F;
            this.textBox151.Width = 0.4681099F;
            // 
            // textBox152
            // 
            this.textBox152.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox152.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox152.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox152.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox152.CanGrow = false;
            this.textBox152.DataField = "ITEM062";
            this.textBox152.Height = 0.215748F;
            this.textBox152.Left = 1.118505F;
            this.textBox152.Name = "textBox152";
            this.textBox152.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox152.Tag = "";
            this.textBox152.Text = "ITEM062";
            this.textBox152.Top = 3.627559F;
            this.textBox152.Width = 0.4937008F;
            // 
            // textBox153
            // 
            this.textBox153.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox153.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox153.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox153.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox153.Height = 0.215748F;
            this.textBox153.Left = 0.6248037F;
            this.textBox153.Name = "textBox153";
            this.textBox153.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 4.7pt; font" +
    "-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox153.Tag = "";
            this.textBox153.Text = "新生命保険料\r\nの金額\r\n";
            this.textBox153.Top = 3.627559F;
            this.textBox153.Width = 0.4937008F;
            // 
            // textBox156
            // 
            this.textBox156.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox156.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox156.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox156.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox156.CanGrow = false;
            this.textBox156.DataField = "ITEM063";
            this.textBox156.Height = 0.215748F;
            this.textBox156.Left = 2.105906F;
            this.textBox156.Name = "textBox156";
            this.textBox156.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox156.Tag = "";
            this.textBox156.Text = "ITEM063";
            this.textBox156.Top = 3.627559F;
            this.textBox156.Width = 0.4937008F;
            // 
            // textBox157
            // 
            this.textBox157.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox157.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox157.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox157.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox157.Height = 0.215748F;
            this.textBox157.Left = 1.612206F;
            this.textBox157.Name = "textBox157";
            this.textBox157.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 4.7pt; font" +
    "-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox157.Tag = "";
            this.textBox157.Text = "旧生命保険料\r\nの金額";
            this.textBox157.Top = 3.627559F;
            this.textBox157.Width = 0.4937008F;
            // 
            // label7
            // 
            this.label7.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label7.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label7.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label7.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label7.Height = 0.1555118F;
            this.label7.HyperLink = null;
            this.label7.Left = 0.6248037F;
            this.label7.Name = "label7";
            this.label7.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 4.6pt; font-weight: normal; text-ali" +
    "gn: center; vertical-align: middle; ddo-char-set: 1";
            this.label7.Tag = "";
            this.label7.Text = "住宅借入金等\r\n特別控除適用数";
            this.label7.Top = 3.844489F;
            this.label7.Width = 0.4937009F;
            // 
            // textBox158
            // 
            this.textBox158.DataField = "ITEM050";
            this.textBox158.Height = 0.1401575F;
            this.textBox158.Left = 1.115748F;
            this.textBox158.Name = "textBox158";
            this.textBox158.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox158.Tag = "";
            this.textBox158.Text = "ITEM050";
            this.textBox158.Top = 4.015749F;
            this.textBox158.Width = 0.4527559F;
            // 
            // label20
            // 
            this.label20.Height = 0.1185041F;
            this.label20.HyperLink = null;
            this.label20.Left = 3.421666F;
            this.label20.Name = "label20";
            this.label20.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label20.Tag = "";
            this.label20.Text = "円";
            this.label20.Top = 4.156971F;
            this.label20.Width = 0.1666665F;
            // 
            // label22
            // 
            this.label22.Height = 0.08611111F;
            this.label22.HyperLink = null;
            this.label22.Left = 1.427166F;
            this.label22.Name = "label22";
            this.label22.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label22.Tag = "";
            this.label22.Text = "円";
            this.label22.Top = 3.631496F;
            this.label22.Width = 0.1666665F;
            // 
            // label23
            // 
            this.label23.Height = 0.08899908F;
            this.label23.HyperLink = null;
            this.label23.Left = 2.417323F;
            this.label23.Name = "label23";
            this.label23.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label23.Tag = "";
            this.label23.Text = "円";
            this.label23.Top = 3.631496F;
            this.label23.Width = 0.1666665F;
            // 
            // label24
            // 
            this.label24.Height = 0.08611111F;
            this.label24.HyperLink = null;
            this.label24.Left = 4.396457F;
            this.label24.Name = "label24";
            this.label24.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label24.Tag = "";
            this.label24.Text = "円";
            this.label24.Top = 3.634252F;
            this.label24.Width = 0.166667F;
            // 
            // label25
            // 
            this.label25.Height = 0.08611111F;
            this.label25.HyperLink = null;
            this.label25.Left = 5.381103F;
            this.label25.Name = "label25";
            this.label25.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label25.Tag = "";
            this.label25.Text = "円";
            this.label25.Top = 3.634252F;
            this.label25.Width = 0.166667F;
            // 
            // label45
            // 
            this.label45.Height = 0.08611111F;
            this.label45.HyperLink = null;
            this.label45.Left = 3.390945F;
            this.label45.Name = "label45";
            this.label45.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label45.Tag = "";
            this.label45.Text = "円";
            this.label45.Top = 3.634252F;
            this.label45.Width = 0.166667F;
            // 
            // textBox257
            // 
            this.textBox257.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox257.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox257.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox257.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox257.Height = 0.215748F;
            this.textBox257.Left = 0.1275591F;
            this.textBox257.Name = "textBox257";
            this.textBox257.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 4.5pt; font" +
    "-weight: normal; text-align: center; ddo-char-set: 1";
            this.textBox257.Tag = "";
            this.textBox257.Text = "生命保険料\r\nの金額の内\r\n訳";
            this.textBox257.Top = 3.627559F;
            this.textBox257.Width = 0.4972446F;
            // 
            // label46
            // 
            this.label46.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label46.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label46.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label46.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label46.Height = 0.1555118F;
            this.label46.HyperLink = null;
            this.label46.Left = 0.6248037F;
            this.label46.Name = "label46";
            this.label46.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 4.6pt; font-weight: normal; text-ali" +
    "gn: center; vertical-align: middle; ddo-char-set: 1";
            this.label46.Tag = "";
            this.label46.Text = "住宅借入金等\r\n特別控除可能額";
            this.label46.Top = 4.000393F;
            this.label46.Width = 0.4937011F;
            // 
            // label47
            // 
            this.label47.Height = 0.08611111F;
            this.label47.HyperLink = null;
            this.label47.Left = 1.445669F;
            this.label47.Name = "label47";
            this.label47.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label47.Tag = "";
            this.label47.Text = "円";
            this.label47.Top = 4F;
            this.label47.Width = 0.1666665F;
            // 
            // label48
            // 
            this.label48.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label48.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label48.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label48.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label48.Height = 0.1555118F;
            this.label48.HyperLink = null;
            this.label48.Left = 1.612206F;
            this.label48.Name = "label48";
            this.label48.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 4.6pt; font-weight: normal; text-ali" +
    "gn: center; vertical-align: middle; ddo-char-set: 1";
            this.label48.Tag = "";
            this.label48.Text = "住居開始年月日\r\n(1回目)";
            this.label48.Top = 3.844489F;
            this.label48.Width = 0.4937009F;
            // 
            // label49
            // 
            this.label49.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label49.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label49.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label49.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label49.Height = 0.1555118F;
            this.label49.HyperLink = null;
            this.label49.Left = 1.612206F;
            this.label49.Name = "label49";
            this.label49.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 4.6pt; font-weight: normal; text-ali" +
    "gn: center; vertical-align: middle; ddo-char-set: 1";
            this.label49.Tag = "";
            this.label49.Text = "住居開始年月日\r\n(2回目)";
            this.label49.Top = 4.000393F;
            this.label49.Width = 0.4937009F;
            // 
            // label50
            // 
            this.label50.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label50.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label50.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label50.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label50.Height = 0.1555118F;
            this.label50.HyperLink = null;
            this.label50.Left = 3.094488F;
            this.label50.Name = "label50";
            this.label50.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 4.6pt; font-weight: normal; text-ali" +
    "gn: center; vertical-align: middle; ddo-char-set: 1";
            this.label50.Tag = "";
            this.label50.Text = "住宅借入金等特別控除区分(1回目)";
            this.label50.Top = 3.844882F;
            this.label50.Width = 0.5866145F;
            // 
            // label51
            // 
            this.label51.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label51.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label51.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label51.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label51.Height = 0.1555118F;
            this.label51.HyperLink = null;
            this.label51.Left = 3.094488F;
            this.label51.Name = "label51";
            this.label51.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 4.6pt; font-weight: normal; text-ali" +
    "gn: center; vertical-align: middle; ddo-char-set: 1";
            this.label51.Tag = "";
            this.label51.Text = "住宅借入金等特別控除区分(2回目)";
            this.label51.Top = 4.000393F;
            this.label51.Width = 0.5866145F;
            // 
            // textBox259
            // 
            this.textBox259.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox259.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox259.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox259.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox259.Height = 1.651575F;
            this.textBox259.Left = 2.600788F;
            this.textBox259.Name = "textBox259";
            this.textBox259.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1; ddo-" +
    "font-vertical: true";
            this.textBox259.Tag = "";
            this.textBox259.Text = "1\r\n6歳未満の扶養親族";
            this.textBox259.Top = 4.569685F;
            this.textBox259.Width = 0.1874008F;
            // 
            // textBox264
            // 
            this.textBox264.Height = 0.2381894F;
            this.textBox264.Left = 2.944489F;
            this.textBox264.Name = "textBox264";
            this.textBox264.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox264.Tag = "";
            this.textBox264.Text = "氏名";
            this.textBox264.Top = 5.004331F;
            this.textBox264.Width = 0.4751969F;
            // 
            // textBox268
            // 
            this.textBox268.Height = 0.09842521F;
            this.textBox268.Left = 2.944488F;
            this.textBox268.Name = "textBox268";
            this.textBox268.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5.6pt; font" +
    "-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox268.Tag = "";
            this.textBox268.Text = "(フリガナ)";
            this.textBox268.Top = 5.004331F;
            this.textBox268.Width = 0.4751967F;
            // 
            // textBox269
            // 
            this.textBox269.Height = 0.2381894F;
            this.textBox269.Left = 2.944489F;
            this.textBox269.Name = "textBox269";
            this.textBox269.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox269.Tag = "";
            this.textBox269.Text = "氏名";
            this.textBox269.Top = 4.592124F;
            this.textBox269.Width = 0.4751969F;
            // 
            // textBox273
            // 
            this.textBox273.Height = 0.09842521F;
            this.textBox273.Left = 2.946457F;
            this.textBox273.Name = "textBox273";
            this.textBox273.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5.6pt; font" +
    "-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox273.Tag = "";
            this.textBox273.Text = "(フリガナ)";
            this.textBox273.Top = 4.592126F;
            this.textBox273.Width = 0.4751967F;
            // 
            // textBox274
            // 
            this.textBox274.Height = 0.2381894F;
            this.textBox274.Left = 2.944489F;
            this.textBox274.Name = "textBox274";
            this.textBox274.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox274.Tag = "";
            this.textBox274.Text = "氏名";
            this.textBox274.Top = 5.418897F;
            this.textBox274.Width = 0.4751969F;
            // 
            // textBox278
            // 
            this.textBox278.Height = 0.09842521F;
            this.textBox278.Left = 2.944488F;
            this.textBox278.Name = "textBox278";
            this.textBox278.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5.6pt; font" +
    "-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox278.Tag = "";
            this.textBox278.Text = "(フリガナ)";
            this.textBox278.Top = 5.418898F;
            this.textBox278.Width = 0.4751967F;
            // 
            // textBox279
            // 
            this.textBox279.Height = 0.2381894F;
            this.textBox279.Left = 2.944488F;
            this.textBox279.Name = "textBox279";
            this.textBox279.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox279.Tag = "";
            this.textBox279.Text = "氏名";
            this.textBox279.Top = 5.830708F;
            this.textBox279.Width = 0.4751969F;
            // 
            // textBox283
            // 
            this.textBox283.Height = 0.09842521F;
            this.textBox283.Left = 2.944488F;
            this.textBox283.Name = "textBox283";
            this.textBox283.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5.6pt; font" +
    "-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox283.Tag = "";
            this.textBox283.Text = "(フリガナ)";
            this.textBox283.Top = 5.826772F;
            this.textBox283.Width = 0.4751967F;
            // 
            // textBox286
            // 
            this.textBox286.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox286.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox286.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox286.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox286.Height = 0.2578742F;
            this.textBox286.Left = 2.204725F;
            this.textBox286.Name = "textBox286";
            this.textBox286.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox286.Tag = "";
            this.textBox286.Text = "区分";
            this.textBox286.Top = 4.569685F;
            this.textBox286.Width = 0.1181103F;
            // 
            // textBox287
            // 
            this.textBox287.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox287.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox287.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox287.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox287.DataField = "ITEM108";
            this.textBox287.Height = 0.2578741F;
            this.textBox287.Left = 2.322835F;
            this.textBox287.Name = "textBox287";
            this.textBox287.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox287.Tag = "";
            this.textBox287.Text = "ITEM108";
            this.textBox287.Top = 4.569685F;
            this.textBox287.Width = 0.2779529F;
            // 
            // textBox288
            // 
            this.textBox288.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox288.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox288.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox288.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox288.Height = 0.2559072F;
            this.textBox288.Left = 2.204725F;
            this.textBox288.Name = "textBox288";
            this.textBox288.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox288.Tag = "";
            this.textBox288.Text = "区分";
            this.textBox288.Top = 4.98307F;
            this.textBox288.Width = 0.1181103F;
            // 
            // textBox289
            // 
            this.textBox289.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox289.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox289.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox289.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox289.DataField = "ITEM112";
            this.textBox289.Height = 0.2578755F;
            this.textBox289.Left = 2.322835F;
            this.textBox289.Name = "textBox289";
            this.textBox289.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox289.Tag = "";
            this.textBox289.Text = "ITEM112";
            this.textBox289.Top = 4.981496F;
            this.textBox289.Width = 0.2779529F;
            // 
            // textBox290
            // 
            this.textBox290.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox290.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox290.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox290.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox290.Height = 0.2551181F;
            this.textBox290.Left = 2.204725F;
            this.textBox290.Name = "textBox290";
            this.textBox290.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox290.Tag = "";
            this.textBox290.Text = "区分";
            this.textBox290.Top = 5.398426F;
            this.textBox290.Width = 0.1181104F;
            // 
            // textBox291
            // 
            this.textBox291.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox291.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox291.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox291.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox291.DataField = "ITEM116";
            this.textBox291.Height = 0.2590548F;
            this.textBox291.Left = 2.322835F;
            this.textBox291.Name = "textBox291";
            this.textBox291.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox291.Tag = "";
            this.textBox291.Text = "ITEM116";
            this.textBox291.Top = 5.396063F;
            this.textBox291.Width = 0.2779529F;
            // 
            // textBox292
            // 
            this.textBox292.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox292.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox292.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox292.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox292.Height = 0.2559058F;
            this.textBox292.Left = 2.204725F;
            this.textBox292.Name = "textBox292";
            this.textBox292.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox292.Tag = "";
            this.textBox292.Text = "区分";
            this.textBox292.Top = 5.809449F;
            this.textBox292.Width = 0.1181103F;
            // 
            // textBox293
            // 
            this.textBox293.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox293.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox293.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox293.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox293.DataField = "ITEM120";
            this.textBox293.Height = 0.2559057F;
            this.textBox293.Left = 2.322835F;
            this.textBox293.Name = "textBox293";
            this.textBox293.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox293.Tag = "";
            this.textBox293.Text = "ITEM120";
            this.textBox293.Top = 5.809449F;
            this.textBox293.Width = 0.2779529F;
            // 
            // textBox294
            // 
            this.textBox294.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox294.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox294.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox294.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox294.Height = 0.2637798F;
            this.textBox294.Left = 4.677953F;
            this.textBox294.Name = "textBox294";
            this.textBox294.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox294.Tag = "";
            this.textBox294.Text = "区分";
            this.textBox294.Top = 4.564961F;
            this.textBox294.Width = 0.1181103F;
            // 
            // textBox296
            // 
            this.textBox296.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox296.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox296.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox296.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox296.Height = 0.2578742F;
            this.textBox296.Left = 4.677953F;
            this.textBox296.Name = "textBox296";
            this.textBox296.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox296.Tag = "";
            this.textBox296.Text = "区分";
            this.textBox296.Top = 4.984251F;
            this.textBox296.Width = 0.1181103F;
            // 
            // textBox297
            // 
            this.textBox297.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox297.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox297.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox297.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox297.DataField = "ITEM128";
            this.textBox297.Height = 0.2578741F;
            this.textBox297.Left = 4.796063F;
            this.textBox297.Name = "textBox297";
            this.textBox297.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox297.Tag = "";
            this.textBox297.Text = "ITEM128";
            this.textBox297.Top = 4.984251F;
            this.textBox297.Width = 0.2779524F;
            // 
            // textBox298
            // 
            this.textBox298.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox298.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox298.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox298.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox298.Height = 0.2578742F;
            this.textBox298.Left = 4.677953F;
            this.textBox298.Name = "textBox298";
            this.textBox298.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox298.Tag = "";
            this.textBox298.Text = "区分";
            this.textBox298.Top = 5.399213F;
            this.textBox298.Width = 0.1181103F;
            // 
            // textBox299
            // 
            this.textBox299.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox299.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox299.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox299.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox299.DataField = "ITEM132";
            this.textBox299.Height = 0.2578741F;
            this.textBox299.Left = 4.796063F;
            this.textBox299.Name = "textBox299";
            this.textBox299.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox299.Tag = "";
            this.textBox299.Text = "ITEM132";
            this.textBox299.Top = 5.399213F;
            this.textBox299.Width = 0.2779524F;
            // 
            // textBox300
            // 
            this.textBox300.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox300.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox300.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox300.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox300.Height = 0.2578742F;
            this.textBox300.Left = 4.677953F;
            this.textBox300.Name = "textBox300";
            this.textBox300.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox300.Tag = "";
            this.textBox300.Text = "区分";
            this.textBox300.Top = 5.809449F;
            this.textBox300.Width = 0.1181103F;
            // 
            // textBox301
            // 
            this.textBox301.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox301.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox301.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox301.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox301.DataField = "ITEM136";
            this.textBox301.Height = 0.2578741F;
            this.textBox301.Left = 4.796063F;
            this.textBox301.Name = "textBox301";
            this.textBox301.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox301.Tag = "";
            this.textBox301.Text = "ITEM136";
            this.textBox301.Top = 5.809449F;
            this.textBox301.Width = 0.2779524F;
            // 
            // textBox302
            // 
            this.textBox302.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox302.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox302.DataField = "ITEM141";
            this.textBox302.Height = 0.8157484F;
            this.textBox302.Left = 5.074016F;
            this.textBox302.Name = "textBox302";
            this.textBox302.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5.5pt; font" +
    "-weight: normal; text-align: left; vertical-align: top; ddo-char-set: 1";
            this.textBox302.Tag = "";
            this.textBox302.Text = "ITEM141";
            this.textBox302.Top = 4.662205F;
            this.textBox302.Width = 0.4736223F;
            // 
            // textBox303
            // 
            this.textBox303.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox303.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox303.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox303.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox303.Height = 0.7452748F;
            this.textBox303.Left = 5.074016F;
            this.textBox303.Name = "textBox303";
            this.textBox303.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5.5pt; font" +
    "-weight: normal; text-align: left; vertical-align: top; ddo-char-set: 1";
            this.textBox303.Tag = "";
            this.textBox303.Text = null;
            this.textBox303.Top = 5.475985F;
            this.textBox303.Width = 0.4736223F;
            // 
            // textBox160
            // 
            this.textBox160.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox160.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox160.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox160.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox160.DataField = "ITEM052";
            this.textBox160.Height = 0.1555117F;
            this.textBox160.Left = 2.111023F;
            this.textBox160.Name = "textBox160";
            this.textBox160.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox160.Tag = "";
            this.textBox160.Text = "ITEM052";
            this.textBox160.Top = 3.844882F;
            this.textBox160.Width = 0.3295276F;
            // 
            // textBox161
            // 
            this.textBox161.DataField = "ITEM053";
            this.textBox161.Height = 0.1104167F;
            this.textBox161.Left = 0.2362205F;
            this.textBox161.Name = "textBox161";
            this.textBox161.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6.5pt; font" +
    "-weight: normal; text-align: left; ddo-char-set: 1";
            this.textBox161.Tag = "";
            this.textBox161.Text = "ITEM053";
            this.textBox161.Top = 3.017717F;
            this.textBox161.Width = 2.519685F;
            // 
            // textBox162
            // 
            this.textBox162.DataField = "ITEM054";
            this.textBox162.Height = 0.1104167F;
            this.textBox162.Left = 0.2361058F;
            this.textBox162.Name = "textBox162";
            this.textBox162.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6.5pt; font" +
    "-weight: normal; text-align: left; ddo-char-set: 1";
            this.textBox162.Tag = "";
            this.textBox162.Text = "ITEM054";
            this.textBox162.Top = 3.137326F;
            this.textBox162.Width = 2.519685F;
            // 
            // textBox163
            // 
            this.textBox163.DataField = "ITEM055";
            this.textBox163.Height = 0.1377953F;
            this.textBox163.Left = 2.834646F;
            this.textBox163.Name = "textBox163";
            this.textBox163.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.textBox163.Tag = "";
            this.textBox163.Text = "ITEM055";
            this.textBox163.Top = 2.992126F;
            this.textBox163.Width = 0.9055118F;
            // 
            // textBox164
            // 
            this.textBox164.DataField = "ITEM058";
            this.textBox164.Height = 0.1377953F;
            this.textBox164.Left = 2.834646F;
            this.textBox164.Name = "textBox164";
            this.textBox164.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.textBox164.Tag = "";
            this.textBox164.Text = "ITEM058";
            this.textBox164.Top = 3.149606F;
            this.textBox164.Width = 0.9055118F;
            // 
            // textBox166
            // 
            this.textBox166.DataField = "ITEM056";
            this.textBox166.Height = 0.1377953F;
            this.textBox166.Left = 3.770473F;
            this.textBox166.Name = "textBox166";
            this.textBox166.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.textBox166.Tag = "";
            this.textBox166.Text = "ITEM056";
            this.textBox166.Top = 2.992126F;
            this.textBox166.Width = 0.7791339F;
            // 
            // textBox167
            // 
            this.textBox167.DataField = "ITEM059";
            this.textBox167.Height = 0.1377953F;
            this.textBox167.Left = 3.770473F;
            this.textBox167.Name = "textBox167";
            this.textBox167.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.textBox167.Tag = "";
            this.textBox167.Text = "ITEM059";
            this.textBox167.Top = 3.149606F;
            this.textBox167.Width = 0.7791339F;
            // 
            // textBox168
            // 
            this.textBox168.DataField = "ITEM057";
            this.textBox168.Height = 0.1377953F;
            this.textBox168.Left = 4.575591F;
            this.textBox168.Name = "textBox168";
            this.textBox168.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.textBox168.Tag = "";
            this.textBox168.Text = "ITEM057";
            this.textBox168.Top = 2.992126F;
            this.textBox168.Width = 0.8181103F;
            // 
            // textBox169
            // 
            this.textBox169.DataField = "ITEM060";
            this.textBox169.Height = 0.1377953F;
            this.textBox169.Left = 4.575591F;
            this.textBox169.Name = "textBox169";
            this.textBox169.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.textBox169.Tag = "";
            this.textBox169.Text = "ITEM060";
            this.textBox169.Top = 3.149606F;
            this.textBox169.Width = 0.8181103F;
            // 
            // textBox304
            // 
            this.textBox304.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox304.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox304.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox304.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox304.Height = 0.2362205F;
            this.textBox304.Left = 3.149606F;
            this.textBox304.Name = "textBox304";
            this.textBox304.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: left; vertical-align: middle; ddo-char-set: 1";
            this.textBox304.Tag = "";
            this.textBox304.Text = "(右詰めで記載してください。)";
            this.textBox304.Top = 6.851575F;
            this.textBox304.Width = 2.398032F;
            // 
            // テキスト163
            // 
            this.テキスト163.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト163.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト163.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト163.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト163.Height = 0.1576389F;
            this.テキスト163.Left = 0.1275591F;
            this.テキスト163.Name = "テキスト163";
            this.テキスト163.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: bold; text-align: left; ddo-char-set: 1";
            this.テキスト163.Tag = "";
            this.テキスト163.Text = "整理欄";
            this.テキスト163.Top = 7.559449F;
            this.テキスト163.Width = 0.6318296F;
            // 
            // テキスト167
            // 
            this.テキスト167.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト167.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト167.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト167.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト167.Height = 0.1576389F;
            this.テキスト167.Left = 0.7593876F;
            this.テキスト167.Name = "テキスト167";
            this.テキスト167.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: bold; text-align: center; ddo-char-set: 1";
            this.テキスト167.Tag = "";
            this.テキスト167.Text = null;
            this.テキスト167.Top = 7.559449F;
            this.テキスト167.Width = 1.602817F;
            // 
            // テキスト168
            // 
            this.テキスト168.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト168.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト168.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト168.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト168.Height = 0.1576389F;
            this.テキスト168.Left = 2.362205F;
            this.テキスト168.Name = "テキスト168";
            this.テキスト168.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: bold; text-align: center; ddo-char-set: 1";
            this.テキスト168.Tag = "";
            this.テキスト168.Text = null;
            this.テキスト168.Top = 7.560237F;
            this.テキスト168.Width = 1.438583F;
            // 
            // label56
            // 
            this.label56.Height = 0.08611111F;
            this.label56.HyperLink = null;
            this.label56.Left = 2.327559F;
            this.label56.Name = "label56";
            this.label56.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label56.Tag = "";
            this.label56.Text = "年";
            this.label56.Top = 3.844882F;
            this.label56.Width = 0.09461927F;
            // 
            // textBox305
            // 
            this.textBox305.DataField = "ITEM144";
            this.textBox305.Height = 0.1104167F;
            this.textBox305.Left = 3.094488F;
            this.textBox305.Name = "textBox305";
            this.textBox305.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6.5pt; font" +
    "-weight: normal; text-align: left; ddo-char-set: 1";
            this.textBox305.Tag = "";
            this.textBox305.Text = "ITEM144";
            this.textBox305.Top = 3.443307F;
            this.textBox305.Width = 2.283465F;
            // 
            // textBox306
            // 
            this.textBox306.DataField = "ITEM143";
            this.textBox306.Height = 0.1104167F;
            this.textBox306.Left = 3.081103F;
            this.textBox306.Name = "textBox306";
            this.textBox306.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6.5pt; font" +
    "-weight: normal; text-align: left; ddo-char-set: 1";
            this.textBox306.Tag = "";
            this.textBox306.Text = "ITEM143";
            this.textBox306.Top = 3.307087F;
            this.textBox306.Width = 2.283465F;
            // 
            // label6
            // 
            this.label6.Height = 0.08611111F;
            this.label6.HyperLink = null;
            this.label6.Left = 0.155221F;
            this.label6.Name = "label6";
            this.label6.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.label6.Tag = "";
            this.label6.Text = "内　　　　　　　　　　　　　　　 　円";
            this.label6.Top = 2.516924F;
            this.label6.Width = 1.335313F;
            // 
            // textBox316
            // 
            this.textBox316.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox316.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox316.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox316.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox316.DataField = "ITEM139";
            this.textBox316.Height = 0.1555117F;
            this.textBox316.Left = 2.440945F;
            this.textBox316.Name = "textBox316";
            this.textBox316.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox316.Tag = "";
            this.textBox316.Text = "ITEM139";
            this.textBox316.Top = 3.844882F;
            this.textBox316.Width = 0.3295276F;
            // 
            // label57
            // 
            this.label57.Height = 0.08611111F;
            this.label57.HyperLink = null;
            this.label57.Left = 2.65748F;
            this.label57.Name = "label57";
            this.label57.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label57.Tag = "";
            this.label57.Text = "月";
            this.label57.Top = 3.844882F;
            this.label57.Width = 0.0946193F;
            // 
            // textBox317
            // 
            this.textBox317.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox317.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox317.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox317.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox317.DataField = "ITEM140";
            this.textBox317.Height = 0.1555117F;
            this.textBox317.Left = 2.764961F;
            this.textBox317.Name = "textBox317";
            this.textBox317.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox317.Tag = "";
            this.textBox317.Text = "ITEM140";
            this.textBox317.Top = 3.844882F;
            this.textBox317.Width = 0.3295276F;
            // 
            // label58
            // 
            this.label58.Height = 0.08611111F;
            this.label58.HyperLink = null;
            this.label58.Left = 2.987008F;
            this.label58.Name = "label58";
            this.label58.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label58.Tag = "";
            this.label58.Text = "日";
            this.label58.Top = 3.844882F;
            this.label58.Width = 0.0946193F;
            // 
            // textBox318
            // 
            this.textBox318.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox318.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox318.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox318.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox318.DataField = "ITEM145";
            this.textBox318.Height = 0.1555117F;
            this.textBox318.Left = 2.110631F;
            this.textBox318.Name = "textBox318";
            this.textBox318.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox318.Tag = "";
            this.textBox318.Text = "ITEM145";
            this.textBox318.Top = 4.002362F;
            this.textBox318.Width = 0.3295276F;
            // 
            // label59
            // 
            this.label59.Height = 0.08611111F;
            this.label59.HyperLink = null;
            this.label59.Left = 2.327166F;
            this.label59.Name = "label59";
            this.label59.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label59.Tag = "";
            this.label59.Text = "年";
            this.label59.Top = 4.002362F;
            this.label59.Width = 0.0946193F;
            // 
            // textBox319
            // 
            this.textBox319.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox319.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox319.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox319.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox319.DataField = "ITEM146";
            this.textBox319.Height = 0.1555117F;
            this.textBox319.Left = 2.440552F;
            this.textBox319.Name = "textBox319";
            this.textBox319.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox319.Tag = "";
            this.textBox319.Text = "ITEM146";
            this.textBox319.Top = 4.002362F;
            this.textBox319.Width = 0.3295276F;
            // 
            // label60
            // 
            this.label60.Height = 0.08611111F;
            this.label60.HyperLink = null;
            this.label60.Left = 2.657086F;
            this.label60.Name = "label60";
            this.label60.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label60.Tag = "";
            this.label60.Text = "月";
            this.label60.Top = 4.002362F;
            this.label60.Width = 0.0946193F;
            // 
            // textBox320
            // 
            this.textBox320.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox320.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox320.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox320.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox320.DataField = "ITEM147";
            this.textBox320.Height = 0.1555117F;
            this.textBox320.Left = 2.764567F;
            this.textBox320.Name = "textBox320";
            this.textBox320.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox320.Tag = "";
            this.textBox320.Text = "ITEM147";
            this.textBox320.Top = 4.002362F;
            this.textBox320.Width = 0.3295276F;
            // 
            // label61
            // 
            this.label61.Height = 0.08611111F;
            this.label61.HyperLink = null;
            this.label61.Left = 2.986614F;
            this.label61.Name = "label61";
            this.label61.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label61.Tag = "";
            this.label61.Text = "日";
            this.label61.Top = 4.002362F;
            this.label61.Width = 0.0946193F;
            // 
            // textBox10
            // 
            this.textBox10.DataField = "ITEM027";
            this.textBox10.Height = 0.1023622F;
            this.textBox10.Left = 3.809449F;
            this.textBox10.Name = "textBox10";
            this.textBox10.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.textBox10.Tag = "";
            this.textBox10.Text = "ITEM027";
            this.textBox10.Top = 0.5511811F;
            this.textBox10.Width = 1.6F;
            // 
            // line2
            // 
            this.line2.Height = 0.1555123F;
            this.line2.Left = 1.074803F;
            this.line2.LineWeight = 1F;
            this.line2.Name = "line2";
            this.line2.Top = 4.413386F;
            this.line2.Width = 0F;
            this.line2.X1 = 1.074803F;
            this.line2.X2 = 1.074803F;
            this.line2.Y1 = 4.568898F;
            this.line2.Y2 = 4.413386F;
            // 
            // line3
            // 
            this.line3.Height = 0.1555123F;
            this.line3.Left = 1.220472F;
            this.line3.LineWeight = 1F;
            this.line3.Name = "line3";
            this.line3.Top = 4.413386F;
            this.line3.Width = 0F;
            this.line3.X1 = 1.220472F;
            this.line3.X2 = 1.220472F;
            this.line3.Y1 = 4.568898F;
            this.line3.Y2 = 4.413386F;
            // 
            // line4
            // 
            this.line4.Height = 0.1555123F;
            this.line4.Left = 1.358268F;
            this.line4.LineWeight = 1F;
            this.line4.Name = "line4";
            this.line4.Top = 4.413386F;
            this.line4.Width = 0F;
            this.line4.X1 = 1.358268F;
            this.line4.X2 = 1.358268F;
            this.line4.Y1 = 4.568898F;
            this.line4.Y2 = 4.413386F;
            // 
            // line5
            // 
            this.line5.Height = 0.1555123F;
            this.line5.Left = 1.496063F;
            this.line5.LineWeight = 1F;
            this.line5.Name = "line5";
            this.line5.Top = 4.413386F;
            this.line5.Width = 0F;
            this.line5.X1 = 1.496063F;
            this.line5.X2 = 1.496063F;
            this.line5.Y1 = 4.568898F;
            this.line5.Y2 = 4.413386F;
            // 
            // line6
            // 
            this.line6.Height = 0.1555123F;
            this.line6.Left = 1.633858F;
            this.line6.LineWeight = 1F;
            this.line6.Name = "line6";
            this.line6.Top = 4.413386F;
            this.line6.Width = 0F;
            this.line6.X1 = 1.633858F;
            this.line6.X2 = 1.633858F;
            this.line6.Y1 = 4.568898F;
            this.line6.Y2 = 4.413386F;
            // 
            // line7
            // 
            this.line7.Height = 0.1555123F;
            this.line7.Left = 1.771654F;
            this.line7.LineWeight = 1F;
            this.line7.Name = "line7";
            this.line7.Top = 4.413386F;
            this.line7.Width = 0F;
            this.line7.X1 = 1.771654F;
            this.line7.X2 = 1.771654F;
            this.line7.Y1 = 4.568898F;
            this.line7.Y2 = 4.413386F;
            // 
            // line8
            // 
            this.line8.Height = 0.1555123F;
            this.line8.Left = 1.909449F;
            this.line8.LineWeight = 1F;
            this.line8.Name = "line8";
            this.line8.Top = 4.413386F;
            this.line8.Width = 0F;
            this.line8.X1 = 1.909449F;
            this.line8.X2 = 1.909449F;
            this.line8.Y1 = 4.568898F;
            this.line8.Y2 = 4.413386F;
            // 
            // line9
            // 
            this.line9.Height = 0.1555123F;
            this.line9.Left = 2.047244F;
            this.line9.LineWeight = 1F;
            this.line9.Name = "line9";
            this.line9.Top = 4.413386F;
            this.line9.Width = 0F;
            this.line9.X1 = 2.047244F;
            this.line9.X2 = 2.047244F;
            this.line9.Y1 = 4.568898F;
            this.line9.Y2 = 4.413386F;
            // 
            // line10
            // 
            this.line10.Height = 0.1555123F;
            this.line10.Left = 2.346457F;
            this.line10.LineWeight = 1F;
            this.line10.Name = "line10";
            this.line10.Top = 4.413386F;
            this.line10.Width = 0F;
            this.line10.X1 = 2.346457F;
            this.line10.X2 = 2.346457F;
            this.line10.Y1 = 4.568898F;
            this.line10.Y2 = 4.413386F;
            // 
            // line11
            // 
            this.line11.Height = 0.1555123F;
            this.line11.Left = 2.480315F;
            this.line11.LineWeight = 1F;
            this.line11.Name = "line11";
            this.line11.Top = 4.413386F;
            this.line11.Width = 0F;
            this.line11.X1 = 2.480315F;
            this.line11.X2 = 2.480315F;
            this.line11.Y1 = 4.568898F;
            this.line11.Y2 = 4.413386F;
            // 
            // line12
            // 
            this.line12.Height = 0.153543F;
            this.line12.Left = 1.074803F;
            this.line12.LineWeight = 1F;
            this.line12.Name = "line12";
            this.line12.Top = 4.829134F;
            this.line12.Width = 0F;
            this.line12.X1 = 1.074803F;
            this.line12.X2 = 1.074803F;
            this.line12.Y1 = 4.982677F;
            this.line12.Y2 = 4.829134F;
            // 
            // line13
            // 
            this.line13.Height = 0.153543F;
            this.line13.Left = 1.220472F;
            this.line13.LineWeight = 1F;
            this.line13.Name = "line13";
            this.line13.Top = 4.829134F;
            this.line13.Width = 1.072884E-06F;
            this.line13.X1 = 1.220473F;
            this.line13.X2 = 1.220472F;
            this.line13.Y1 = 4.982677F;
            this.line13.Y2 = 4.829134F;
            // 
            // line14
            // 
            this.line14.Height = 0.153543F;
            this.line14.Left = 1.358267F;
            this.line14.LineWeight = 1F;
            this.line14.Name = "line14";
            this.line14.Top = 4.829134F;
            this.line14.Width = 1.072884E-06F;
            this.line14.X1 = 1.358268F;
            this.line14.X2 = 1.358267F;
            this.line14.Y1 = 4.982677F;
            this.line14.Y2 = 4.829134F;
            // 
            // line15
            // 
            this.line15.Height = 0.153543F;
            this.line15.Left = 1.496063F;
            this.line15.LineWeight = 1F;
            this.line15.Name = "line15";
            this.line15.Top = 4.829134F;
            this.line15.Width = 0F;
            this.line15.X1 = 1.496063F;
            this.line15.X2 = 1.496063F;
            this.line15.Y1 = 4.982677F;
            this.line15.Y2 = 4.829134F;
            // 
            // line16
            // 
            this.line16.Height = 0.153543F;
            this.line16.Left = 1.633858F;
            this.line16.LineWeight = 1F;
            this.line16.Name = "line16";
            this.line16.Top = 4.829134F;
            this.line16.Width = 0F;
            this.line16.X1 = 1.633858F;
            this.line16.X2 = 1.633858F;
            this.line16.Y1 = 4.982677F;
            this.line16.Y2 = 4.829134F;
            // 
            // line17
            // 
            this.line17.Height = 0.153543F;
            this.line17.Left = 1.771653F;
            this.line17.LineWeight = 1F;
            this.line17.Name = "line17";
            this.line17.Top = 4.829134F;
            this.line17.Width = 9.536743E-07F;
            this.line17.X1 = 1.771654F;
            this.line17.X2 = 1.771653F;
            this.line17.Y1 = 4.982677F;
            this.line17.Y2 = 4.829134F;
            // 
            // line18
            // 
            this.line18.Height = 0.153543F;
            this.line18.Left = 1.909449F;
            this.line18.LineWeight = 1F;
            this.line18.Name = "line18";
            this.line18.Top = 4.829134F;
            this.line18.Width = 0F;
            this.line18.X1 = 1.909449F;
            this.line18.X2 = 1.909449F;
            this.line18.Y1 = 4.982677F;
            this.line18.Y2 = 4.829134F;
            // 
            // line19
            // 
            this.line19.Height = 0.153543F;
            this.line19.Left = 2.047244F;
            this.line19.LineWeight = 1F;
            this.line19.Name = "line19";
            this.line19.Top = 4.829134F;
            this.line19.Width = 0F;
            this.line19.X1 = 2.047244F;
            this.line19.X2 = 2.047244F;
            this.line19.Y1 = 4.982677F;
            this.line19.Y2 = 4.829134F;
            // 
            // line20
            // 
            this.line20.Height = 0.153543F;
            this.line20.Left = 2.204724F;
            this.line20.LineWeight = 1F;
            this.line20.Name = "line20";
            this.line20.Top = 4.829134F;
            this.line20.Width = 9.536743E-07F;
            this.line20.X1 = 2.204725F;
            this.line20.X2 = 2.204724F;
            this.line20.Y1 = 4.982677F;
            this.line20.Y2 = 4.829134F;
            // 
            // line21
            // 
            this.line21.Height = 0.153543F;
            this.line21.Left = 2.480315F;
            this.line21.LineWeight = 1F;
            this.line21.Name = "line21";
            this.line21.Top = 4.829134F;
            this.line21.Width = 0F;
            this.line21.X1 = 2.480315F;
            this.line21.X2 = 2.480315F;
            this.line21.Y1 = 4.982677F;
            this.line21.Y2 = 4.829134F;
            // 
            // line22
            // 
            this.line22.Height = 0.1555123F;
            this.line22.Left = 1.074803F;
            this.line22.LineWeight = 1F;
            this.line22.Name = "line22";
            this.line22.Top = 5.23937F;
            this.line22.Width = 0F;
            this.line22.X1 = 1.074803F;
            this.line22.X2 = 1.074803F;
            this.line22.Y1 = 5.394882F;
            this.line22.Y2 = 5.23937F;
            // 
            // line23
            // 
            this.line23.Height = 0.1555123F;
            this.line23.Left = 1.220473F;
            this.line23.LineWeight = 1F;
            this.line23.Name = "line23";
            this.line23.Top = 5.23937F;
            this.line23.Width = 0F;
            this.line23.X1 = 1.220473F;
            this.line23.X2 = 1.220473F;
            this.line23.Y1 = 5.394882F;
            this.line23.Y2 = 5.23937F;
            // 
            // line24
            // 
            this.line24.Height = 0.1555123F;
            this.line24.Left = 1.358268F;
            this.line24.LineWeight = 1F;
            this.line24.Name = "line24";
            this.line24.Top = 5.23937F;
            this.line24.Width = 0F;
            this.line24.X1 = 1.358268F;
            this.line24.X2 = 1.358268F;
            this.line24.Y1 = 5.394882F;
            this.line24.Y2 = 5.23937F;
            // 
            // line25
            // 
            this.line25.Height = 0.1555123F;
            this.line25.Left = 1.496063F;
            this.line25.LineWeight = 1F;
            this.line25.Name = "line25";
            this.line25.Top = 5.23937F;
            this.line25.Width = 0F;
            this.line25.X1 = 1.496063F;
            this.line25.X2 = 1.496063F;
            this.line25.Y1 = 5.394882F;
            this.line25.Y2 = 5.23937F;
            // 
            // line26
            // 
            this.line26.Height = 0.1555123F;
            this.line26.Left = 1.633858F;
            this.line26.LineWeight = 1F;
            this.line26.Name = "line26";
            this.line26.Top = 5.23937F;
            this.line26.Width = 0F;
            this.line26.X1 = 1.633858F;
            this.line26.X2 = 1.633858F;
            this.line26.Y1 = 5.394882F;
            this.line26.Y2 = 5.23937F;
            // 
            // line27
            // 
            this.line27.Height = 0.1555123F;
            this.line27.Left = 1.771654F;
            this.line27.LineWeight = 1F;
            this.line27.Name = "line27";
            this.line27.Top = 5.23937F;
            this.line27.Width = 0F;
            this.line27.X1 = 1.771654F;
            this.line27.X2 = 1.771654F;
            this.line27.Y1 = 5.394882F;
            this.line27.Y2 = 5.23937F;
            // 
            // line28
            // 
            this.line28.Height = 0.1555123F;
            this.line28.Left = 1.909449F;
            this.line28.LineWeight = 1F;
            this.line28.Name = "line28";
            this.line28.Top = 5.23937F;
            this.line28.Width = 0F;
            this.line28.X1 = 1.909449F;
            this.line28.X2 = 1.909449F;
            this.line28.Y1 = 5.394882F;
            this.line28.Y2 = 5.23937F;
            // 
            // line29
            // 
            this.line29.Height = 0.1555123F;
            this.line29.Left = 2.047244F;
            this.line29.LineWeight = 1F;
            this.line29.Name = "line29";
            this.line29.Top = 5.23937F;
            this.line29.Width = 0F;
            this.line29.X1 = 2.047244F;
            this.line29.X2 = 2.047244F;
            this.line29.Y1 = 5.394882F;
            this.line29.Y2 = 5.23937F;
            // 
            // line30
            // 
            this.line30.Height = 0.1555123F;
            this.line30.Left = 2.204725F;
            this.line30.LineWeight = 1F;
            this.line30.Name = "line30";
            this.line30.Top = 5.23937F;
            this.line30.Width = 0F;
            this.line30.X1 = 2.204725F;
            this.line30.X2 = 2.204725F;
            this.line30.Y1 = 5.394882F;
            this.line30.Y2 = 5.23937F;
            // 
            // line31
            // 
            this.line31.Height = 0.1555123F;
            this.line31.Left = 2.480315F;
            this.line31.LineWeight = 1F;
            this.line31.Name = "line31";
            this.line31.Top = 5.23937F;
            this.line31.Width = 0F;
            this.line31.X1 = 2.480315F;
            this.line31.X2 = 2.480315F;
            this.line31.Y1 = 5.394882F;
            this.line31.Y2 = 5.23937F;
            // 
            // line32
            // 
            this.line32.Height = 0.1555119F;
            this.line32.Left = 1.074803F;
            this.line32.LineWeight = 1F;
            this.line32.Name = "line32";
            this.line32.Top = 5.653543F;
            this.line32.Width = 0F;
            this.line32.X1 = 1.074803F;
            this.line32.X2 = 1.074803F;
            this.line32.Y1 = 5.809055F;
            this.line32.Y2 = 5.653543F;
            // 
            // line33
            // 
            this.line33.Height = 0.1555119F;
            this.line33.Left = 1.220473F;
            this.line33.LineWeight = 1F;
            this.line33.Name = "line33";
            this.line33.Top = 5.653543F;
            this.line33.Width = 0F;
            this.line33.X1 = 1.220473F;
            this.line33.X2 = 1.220473F;
            this.line33.Y1 = 5.809055F;
            this.line33.Y2 = 5.653543F;
            // 
            // line34
            // 
            this.line34.Height = 0.1555119F;
            this.line34.Left = 1.358268F;
            this.line34.LineWeight = 1F;
            this.line34.Name = "line34";
            this.line34.Top = 5.653543F;
            this.line34.Width = 0F;
            this.line34.X1 = 1.358268F;
            this.line34.X2 = 1.358268F;
            this.line34.Y1 = 5.809055F;
            this.line34.Y2 = 5.653543F;
            // 
            // line35
            // 
            this.line35.Height = 0.1555119F;
            this.line35.Left = 1.496063F;
            this.line35.LineWeight = 1F;
            this.line35.Name = "line35";
            this.line35.Top = 5.653543F;
            this.line35.Width = 0F;
            this.line35.X1 = 1.496063F;
            this.line35.X2 = 1.496063F;
            this.line35.Y1 = 5.809055F;
            this.line35.Y2 = 5.653543F;
            // 
            // line36
            // 
            this.line36.Height = 0.1555119F;
            this.line36.Left = 1.633858F;
            this.line36.LineWeight = 1F;
            this.line36.Name = "line36";
            this.line36.Top = 5.653543F;
            this.line36.Width = 0F;
            this.line36.X1 = 1.633858F;
            this.line36.X2 = 1.633858F;
            this.line36.Y1 = 5.809055F;
            this.line36.Y2 = 5.653543F;
            // 
            // line37
            // 
            this.line37.Height = 0.1555119F;
            this.line37.Left = 1.771654F;
            this.line37.LineWeight = 1F;
            this.line37.Name = "line37";
            this.line37.Top = 5.653543F;
            this.line37.Width = 0F;
            this.line37.X1 = 1.771654F;
            this.line37.X2 = 1.771654F;
            this.line37.Y1 = 5.809055F;
            this.line37.Y2 = 5.653543F;
            // 
            // line38
            // 
            this.line38.Height = 0.1555119F;
            this.line38.Left = 1.909449F;
            this.line38.LineWeight = 1F;
            this.line38.Name = "line38";
            this.line38.Top = 5.653543F;
            this.line38.Width = 0F;
            this.line38.X1 = 1.909449F;
            this.line38.X2 = 1.909449F;
            this.line38.Y1 = 5.809055F;
            this.line38.Y2 = 5.653543F;
            // 
            // line39
            // 
            this.line39.Height = 0.1555119F;
            this.line39.Left = 2.047244F;
            this.line39.LineWeight = 1F;
            this.line39.Name = "line39";
            this.line39.Top = 5.653543F;
            this.line39.Width = 0F;
            this.line39.X1 = 2.047244F;
            this.line39.X2 = 2.047244F;
            this.line39.Y1 = 5.809055F;
            this.line39.Y2 = 5.653543F;
            // 
            // line40
            // 
            this.line40.Height = 0.1555119F;
            this.line40.Left = 2.204725F;
            this.line40.LineWeight = 1F;
            this.line40.Name = "line40";
            this.line40.Top = 5.653543F;
            this.line40.Width = 0F;
            this.line40.X1 = 2.204725F;
            this.line40.X2 = 2.204725F;
            this.line40.Y1 = 5.809055F;
            this.line40.Y2 = 5.653543F;
            // 
            // line41
            // 
            this.line41.Height = 0.1555119F;
            this.line41.Left = 2.480315F;
            this.line41.LineWeight = 1F;
            this.line41.Name = "line41";
            this.line41.Top = 5.653543F;
            this.line41.Width = 0F;
            this.line41.X1 = 2.480315F;
            this.line41.X2 = 2.480315F;
            this.line41.Y1 = 5.809055F;
            this.line41.Y2 = 5.653543F;
            // 
            // line42
            // 
            this.line42.Height = 0.1555119F;
            this.line42.Left = 1.074803F;
            this.line42.LineWeight = 1F;
            this.line42.Name = "line42";
            this.line42.Top = 6.067717F;
            this.line42.Width = 0F;
            this.line42.X1 = 1.074803F;
            this.line42.X2 = 1.074803F;
            this.line42.Y1 = 6.223229F;
            this.line42.Y2 = 6.067717F;
            // 
            // line43
            // 
            this.line43.Height = 0.1555123F;
            this.line43.Left = 1.220473F;
            this.line43.LineWeight = 1F;
            this.line43.Name = "line43";
            this.line43.Top = 6.066929F;
            this.line43.Width = 0F;
            this.line43.X1 = 1.220473F;
            this.line43.X2 = 1.220473F;
            this.line43.Y1 = 6.222441F;
            this.line43.Y2 = 6.066929F;
            // 
            // line44
            // 
            this.line44.Height = 0.1555123F;
            this.line44.Left = 1.358268F;
            this.line44.LineWeight = 1F;
            this.line44.Name = "line44";
            this.line44.Top = 6.066929F;
            this.line44.Width = 0F;
            this.line44.X1 = 1.358268F;
            this.line44.X2 = 1.358268F;
            this.line44.Y1 = 6.222441F;
            this.line44.Y2 = 6.066929F;
            // 
            // line45
            // 
            this.line45.Height = 0.1555123F;
            this.line45.Left = 1.496063F;
            this.line45.LineWeight = 1F;
            this.line45.Name = "line45";
            this.line45.Top = 6.066929F;
            this.line45.Width = 0F;
            this.line45.X1 = 1.496063F;
            this.line45.X2 = 1.496063F;
            this.line45.Y1 = 6.222441F;
            this.line45.Y2 = 6.066929F;
            // 
            // line46
            // 
            this.line46.Height = 0.1555123F;
            this.line46.Left = 1.633858F;
            this.line46.LineWeight = 1F;
            this.line46.Name = "line46";
            this.line46.Top = 6.066929F;
            this.line46.Width = 0F;
            this.line46.X1 = 1.633858F;
            this.line46.X2 = 1.633858F;
            this.line46.Y1 = 6.222441F;
            this.line46.Y2 = 6.066929F;
            // 
            // line47
            // 
            this.line47.Height = 0.1555123F;
            this.line47.Left = 1.771654F;
            this.line47.LineWeight = 1F;
            this.line47.Name = "line47";
            this.line47.Top = 6.066929F;
            this.line47.Width = 0F;
            this.line47.X1 = 1.771654F;
            this.line47.X2 = 1.771654F;
            this.line47.Y1 = 6.222441F;
            this.line47.Y2 = 6.066929F;
            // 
            // line48
            // 
            this.line48.Height = 0.1555123F;
            this.line48.Left = 1.909449F;
            this.line48.LineWeight = 1F;
            this.line48.Name = "line48";
            this.line48.Top = 6.066929F;
            this.line48.Width = 0F;
            this.line48.X1 = 1.909449F;
            this.line48.X2 = 1.909449F;
            this.line48.Y1 = 6.222441F;
            this.line48.Y2 = 6.066929F;
            // 
            // line49
            // 
            this.line49.Height = 0.1555123F;
            this.line49.Left = 2.047244F;
            this.line49.LineWeight = 1F;
            this.line49.Name = "line49";
            this.line49.Top = 6.066929F;
            this.line49.Width = 0F;
            this.line49.X1 = 2.047244F;
            this.line49.X2 = 2.047244F;
            this.line49.Y1 = 6.222441F;
            this.line49.Y2 = 6.066929F;
            // 
            // line50
            // 
            this.line50.Height = 0.1555123F;
            this.line50.Left = 2.204725F;
            this.line50.LineWeight = 1F;
            this.line50.Name = "line50";
            this.line50.Top = 6.066929F;
            this.line50.Width = 0F;
            this.line50.X1 = 2.204725F;
            this.line50.X2 = 2.204725F;
            this.line50.Y1 = 6.222441F;
            this.line50.Y2 = 6.066929F;
            // 
            // line51
            // 
            this.line51.Height = 0.1555123F;
            this.line51.Left = 2.480315F;
            this.line51.LineWeight = 1F;
            this.line51.Name = "line51";
            this.line51.Top = 6.066929F;
            this.line51.Width = 0F;
            this.line51.X1 = 2.480315F;
            this.line51.X2 = 2.480315F;
            this.line51.Y1 = 6.222441F;
            this.line51.Y2 = 6.066929F;
            // 
            // line73
            // 
            this.line73.Height = 0.1480322F;
            this.line73.Left = 3.419685F;
            this.line73.LineWeight = 1F;
            this.line73.Name = "line73";
            this.line73.Top = 4.831496F;
            this.line73.Width = 1.637402F;
            this.line73.X1 = 3.419685F;
            this.line73.X2 = 5.057087F;
            this.line73.Y1 = 4.979528F;
            this.line73.Y2 = 4.831496F;
            // 
            // line96
            // 
            this.line96.Height = 0.1555123F;
            this.line96.Left = 2.204725F;
            this.line96.LineWeight = 1F;
            this.line96.Name = "line96";
            this.line96.Top = 4.413386F;
            this.line96.Width = 0F;
            this.line96.X1 = 2.204725F;
            this.line96.X2 = 2.204725F;
            this.line96.Y1 = 4.568898F;
            this.line96.Y2 = 4.413386F;
            // 
            // line97
            // 
            this.line97.Height = 0.1555109F;
            this.line97.Left = 2.346457F;
            this.line97.LineWeight = 1F;
            this.line97.Name = "line97";
            this.line97.Top = 4.828741F;
            this.line97.Width = 0F;
            this.line97.X1 = 2.346457F;
            this.line97.X2 = 2.346457F;
            this.line97.Y1 = 4.984252F;
            this.line97.Y2 = 4.828741F;
            // 
            // line98
            // 
            this.line98.Height = 0.1555123F;
            this.line98.Left = 2.346457F;
            this.line98.LineWeight = 1F;
            this.line98.Name = "line98";
            this.line98.Top = 5.240945F;
            this.line98.Width = 0F;
            this.line98.X1 = 2.346457F;
            this.line98.X2 = 2.346457F;
            this.line98.Y1 = 5.396457F;
            this.line98.Y2 = 5.240945F;
            // 
            // line99
            // 
            this.line99.Height = 0.1555123F;
            this.line99.Left = 2.346457F;
            this.line99.LineWeight = 1F;
            this.line99.Name = "line99";
            this.line99.Top = 5.652362F;
            this.line99.Width = 0F;
            this.line99.X1 = 2.346457F;
            this.line99.X2 = 2.346457F;
            this.line99.Y1 = 5.807874F;
            this.line99.Y2 = 5.652362F;
            // 
            // line100
            // 
            this.line100.Height = 0.1555119F;
            this.line100.Left = 2.346457F;
            this.line100.LineWeight = 1F;
            this.line100.Name = "line100";
            this.line100.Top = 6.064961F;
            this.line100.Width = 0F;
            this.line100.X1 = 2.346457F;
            this.line100.X2 = 2.346457F;
            this.line100.Y1 = 6.220473F;
            this.line100.Y2 = 6.064961F;
            // 
            // line101
            // 
            this.line101.Height = 0.2362199F;
            this.line101.Left = 1.413386F;
            this.line101.LineWeight = 1F;
            this.line101.Name = "line101";
            this.line101.Top = 6.851575F;
            this.line101.Width = 0F;
            this.line101.X1 = 1.413386F;
            this.line101.X2 = 1.413386F;
            this.line101.Y1 = 7.087795F;
            this.line101.Y2 = 6.851575F;
            // 
            // line113
            // 
            this.line113.Height = 0.1350394F;
            this.line113.Left = 4.007874F;
            this.line113.LineWeight = 1F;
            this.line113.Name = "line113";
            this.line113.Top = 0.4003937F;
            this.line113.Width = 0F;
            this.line113.X1 = 4.007874F;
            this.line113.X2 = 4.007874F;
            this.line113.Y1 = 0.5354331F;
            this.line113.Y2 = 0.4003937F;
            // 
            // line114
            // 
            this.line114.Height = 0.1350394F;
            this.line114.Left = 4.153543F;
            this.line114.LineWeight = 1F;
            this.line114.Name = "line114";
            this.line114.Top = 0.4003937F;
            this.line114.Width = 0F;
            this.line114.X1 = 4.153543F;
            this.line114.X2 = 4.153543F;
            this.line114.Y1 = 0.5354331F;
            this.line114.Y2 = 0.4003937F;
            // 
            // line115
            // 
            this.line115.Height = 0.1350394F;
            this.line115.Left = 4.291339F;
            this.line115.LineWeight = 1F;
            this.line115.Name = "line115";
            this.line115.Top = 0.4003937F;
            this.line115.Width = 0F;
            this.line115.X1 = 4.291339F;
            this.line115.X2 = 4.291339F;
            this.line115.Y1 = 0.5354331F;
            this.line115.Y2 = 0.4003937F;
            // 
            // line116
            // 
            this.line116.Height = 0.1350394F;
            this.line116.Left = 4.429134F;
            this.line116.LineWeight = 1F;
            this.line116.Name = "line116";
            this.line116.Top = 0.4003937F;
            this.line116.Width = 0F;
            this.line116.X1 = 4.429134F;
            this.line116.X2 = 4.429134F;
            this.line116.Y1 = 0.5354331F;
            this.line116.Y2 = 0.4003937F;
            // 
            // line117
            // 
            this.line117.Height = 0.1350394F;
            this.line117.Left = 4.56693F;
            this.line117.LineWeight = 1F;
            this.line117.Name = "line117";
            this.line117.Top = 0.4003937F;
            this.line117.Width = 0F;
            this.line117.X1 = 4.56693F;
            this.line117.X2 = 4.56693F;
            this.line117.Y1 = 0.5354331F;
            this.line117.Y2 = 0.4003937F;
            // 
            // line118
            // 
            this.line118.Height = 0.1350394F;
            this.line118.Left = 4.84252F;
            this.line118.LineWeight = 1F;
            this.line118.Name = "line118";
            this.line118.Top = 0.4003937F;
            this.line118.Width = 0F;
            this.line118.X1 = 4.84252F;
            this.line118.X2 = 4.84252F;
            this.line118.Y1 = 0.5354331F;
            this.line118.Y2 = 0.4003937F;
            // 
            // line119
            // 
            this.line119.Height = 0.1350394F;
            this.line119.Left = 4.980315F;
            this.line119.LineWeight = 1F;
            this.line119.Name = "line119";
            this.line119.Top = 0.4003937F;
            this.line119.Width = 0F;
            this.line119.X1 = 4.980315F;
            this.line119.X2 = 4.980315F;
            this.line119.Y1 = 0.5354331F;
            this.line119.Y2 = 0.4003937F;
            // 
            // line120
            // 
            this.line120.Height = 0.1350394F;
            this.line120.Left = 5.279529F;
            this.line120.LineWeight = 1F;
            this.line120.Name = "line120";
            this.line120.Top = 0.4003937F;
            this.line120.Width = 0F;
            this.line120.X1 = 5.279529F;
            this.line120.X2 = 5.279529F;
            this.line120.Y1 = 0.5354331F;
            this.line120.Y2 = 0.4003937F;
            // 
            // line121
            // 
            this.line121.Height = 0.1350394F;
            this.line121.Left = 5.413386F;
            this.line121.LineWeight = 1F;
            this.line121.Name = "line121";
            this.line121.Top = 0.4003937F;
            this.line121.Width = 0F;
            this.line121.X1 = 5.413386F;
            this.line121.X2 = 5.413386F;
            this.line121.Y1 = 0.5354331F;
            this.line121.Y2 = 0.4003937F;
            // 
            // line122
            // 
            this.line122.Height = 0.1350394F;
            this.line122.Left = 5.137795F;
            this.line122.LineWeight = 1F;
            this.line122.Name = "line122";
            this.line122.Top = 0.4003937F;
            this.line122.Width = 0F;
            this.line122.X1 = 5.137795F;
            this.line122.X2 = 5.137795F;
            this.line122.Y1 = 0.5354331F;
            this.line122.Y2 = 0.4003937F;
            // 
            // line123
            // 
            this.line123.Height = 0.1350394F;
            this.line123.Left = 4.72441F;
            this.line123.LineWeight = 1F;
            this.line123.Name = "line123";
            this.line123.Top = 0.4003937F;
            this.line123.Width = 0F;
            this.line123.X1 = 4.72441F;
            this.line123.X2 = 4.72441F;
            this.line123.Y1 = 0.5354331F;
            this.line123.Y2 = 0.4003937F;
            // 
            // textBox322
            // 
            this.textBox322.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox322.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox322.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox322.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox322.DataField = "ITEM152";
            this.textBox322.Height = 0.1555118F;
            this.textBox322.Left = 1.118504F;
            this.textBox322.Name = "textBox322";
            this.textBox322.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: right; vertical-align: bottom; ddo-char-set: 1";
            this.textBox322.Tag = "";
            this.textBox322.Text = "ITEM152";
            this.textBox322.Top = 3.844882F;
            this.textBox322.Width = 0.4937007F;
            // 
            // textBox323
            // 
            this.textBox323.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox323.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox323.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox323.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox323.DataField = "ITEM148";
            this.textBox323.Height = 0.1555118F;
            this.textBox323.Left = 3.681103F;
            this.textBox323.Name = "textBox323";
            this.textBox323.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox323.Tag = "";
            this.textBox323.Text = "ITEM148";
            this.textBox323.Top = 3.844488F;
            this.textBox323.Width = 0.5244095F;
            // 
            // textBox324
            // 
            this.textBox324.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox324.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox324.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox324.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox324.DataField = "ITEM149";
            this.textBox324.Height = 0.1555118F;
            this.textBox324.Left = 3.681103F;
            this.textBox324.Name = "textBox324";
            this.textBox324.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox324.Tag = "";
            this.textBox324.Text = "ITEM149";
            this.textBox324.Top = 4.000394F;
            this.textBox324.Width = 0.5244095F;
            // 
            // textBox325
            // 
            this.textBox325.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox325.DataField = "ITEM150";
            this.textBox325.Height = 0.140551F;
            this.textBox325.Left = 4.786614F;
            this.textBox325.Name = "textBox325";
            this.textBox325.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox325.Tag = "";
            this.textBox325.Text = "ITEM150";
            this.textBox325.Top = 3.859843F;
            this.textBox325.Width = 0.7610239F;
            // 
            // textBox326
            // 
            this.textBox326.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox326.DataField = "ITEM151";
            this.textBox326.Height = 0.140551F;
            this.textBox326.Left = 4.78189F;
            this.textBox326.Name = "textBox326";
            this.textBox326.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox326.Tag = "";
            this.textBox326.Text = "ITEM151";
            this.textBox326.Top = 4.016536F;
            this.textBox326.Width = 0.7492127F;
            // 
            // textBox142
            // 
            this.textBox142.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox142.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox142.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox142.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox142.Height = 0.4133858F;
            this.textBox142.Left = 0.3149607F;
            this.textBox142.Name = "textBox142";
            this.textBox142.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1; ddo-" +
    "font-vertical: true";
            this.textBox142.Tag = "";
            this.textBox142.Text = "3";
            this.textBox142.Top = 5.396062F;
            this.textBox142.Width = 0.1574803F;
            // 
            // textBox141
            // 
            this.textBox141.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox141.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox141.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox141.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox141.Height = 0.4133858F;
            this.textBox141.Left = 0.3149607F;
            this.textBox141.Name = "textBox141";
            this.textBox141.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1; ddo-" +
    "font-vertical: true";
            this.textBox141.Tag = "";
            this.textBox141.Text = "2";
            this.textBox141.Top = 4.982677F;
            this.textBox141.Width = 0.1574803F;
            // 
            // textBox56
            // 
            this.textBox56.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox56.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox56.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox56.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox56.DataField = "ITEM111";
            this.textBox56.Height = 0.2559048F;
            this.textBox56.Left = 0.9464568F;
            this.textBox56.Name = "textBox56";
            this.textBox56.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5.5pt; font" +
    "-weight: normal; text-align: left; vertical-align: bottom; ddo-char-set: 1";
            this.textBox56.Tag = "";
            this.textBox56.Text = "ITEM111";
            this.textBox56.Top = 4.983072F;
            this.textBox56.Width = 1.258268F;
            // 
            // textBox311
            // 
            this.textBox311.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox311.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox311.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox311.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox311.DataField = "ITEM110";
            this.textBox311.Height = 0.09645708F;
            this.textBox311.Left = 0.9496064F;
            this.textBox311.Name = "textBox311";
            this.textBox311.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5.5pt; font" +
    "-weight: normal; text-align: left; vertical-align: top; ddo-char-set: 1";
            this.textBox311.Tag = "";
            this.textBox311.Text = "ITEM110";
            this.textBox311.Top = 4.983071F;
            this.textBox311.Width = 1.255118F;
            // 
            // textBox57
            // 
            this.textBox57.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox57.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox57.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox57.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox57.Height = 0.1559055F;
            this.textBox57.Left = 0.472441F;
            this.textBox57.Name = "textBox57";
            this.textBox57.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox57.Tag = "";
            this.textBox57.Text = "個人番号";
            this.textBox57.Top = 5.238977F;
            this.textBox57.Width = 0.4740157F;
            // 
            // textBox69
            // 
            this.textBox69.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox69.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox69.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox69.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox69.Height = 0.1559055F;
            this.textBox69.Left = 0.4712599F;
            this.textBox69.Name = "textBox69";
            this.textBox69.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox69.Tag = "";
            this.textBox69.Text = "個人番号";
            this.textBox69.Top = 5.653543F;
            this.textBox69.Width = 0.4751968F;
            // 
            // textBox143
            // 
            this.textBox143.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox143.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox143.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox143.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox143.Height = 0.4133858F;
            this.textBox143.Left = 0.3149607F;
            this.textBox143.Name = "textBox143";
            this.textBox143.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1; ddo-" +
    "font-vertical: true";
            this.textBox143.Tag = "";
            this.textBox143.Text = "4";
            this.textBox143.Top = 5.807874F;
            this.textBox143.Width = 0.1574803F;
            // 
            // textBox139
            // 
            this.textBox139.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox139.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox139.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox139.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox139.Height = 0.4133858F;
            this.textBox139.Left = 0.3149607F;
            this.textBox139.Name = "textBox139";
            this.textBox139.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1; ddo-" +
    "font-vertical: true";
            this.textBox139.Tag = "";
            this.textBox139.Text = "1";
            this.textBox139.Top = 4.56811F;
            this.textBox139.Width = 0.1574803F;
            // 
            // textBox62
            // 
            this.textBox62.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox62.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox62.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox62.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox62.DataField = "ITEM107";
            this.textBox62.Height = 0.2625984F;
            this.textBox62.Left = 0.9464567F;
            this.textBox62.Name = "textBox62";
            this.textBox62.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5.5pt; font" +
    "-weight: normal; text-align: left; vertical-align: bottom; ddo-char-set: 1";
            this.textBox62.Tag = "";
            this.textBox62.Text = "ITEM107";
            this.textBox62.Top = 4.568898F;
            this.textBox62.Width = 1.258268F;
            // 
            // textBox313
            // 
            this.textBox313.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox313.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox313.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox313.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox313.DataField = "ITEM106";
            this.textBox313.Height = 0.09645708F;
            this.textBox313.Left = 0.9464567F;
            this.textBox313.Name = "textBox313";
            this.textBox313.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5.5pt; font" +
    "-weight: normal; text-align: left; vertical-align: top; ddo-char-set: 1";
            this.textBox313.Tag = "";
            this.textBox313.Text = "ITEM106";
            this.textBox313.Top = 4.568898F;
            this.textBox313.Width = 1.258268F;
            // 
            // textBox68
            // 
            this.textBox68.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox68.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox68.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox68.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox68.DataField = "ITEM115";
            this.textBox68.Height = 0.2578741F;
            this.textBox68.Left = 0.9464568F;
            this.textBox68.Name = "textBox68";
            this.textBox68.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5.5pt; font" +
    "-weight: normal; text-align: left; text-justify: distribute-all-lines; vertical-" +
    "align: bottom; ddo-char-set: 1";
            this.textBox68.Tag = "";
            this.textBox68.Text = "ITEM115";
            this.textBox68.Top = 5.397636F;
            this.textBox68.Width = 1.258268F;
            // 
            // textBox310
            // 
            this.textBox310.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox310.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox310.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox310.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox310.DataField = "ITEM114";
            this.textBox310.Height = 0.09645708F;
            this.textBox310.Left = 0.9496064F;
            this.textBox310.Name = "textBox310";
            this.textBox310.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5.5pt; font" +
    "-weight: normal; text-align: left; vertical-align: top; ddo-char-set: 1";
            this.textBox310.Tag = "";
            this.textBox310.Text = "ITEM114";
            this.textBox310.Top = 5.399213F;
            this.textBox310.Width = 1.255118F;
            // 
            // textBox74
            // 
            this.textBox74.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox74.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox74.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox74.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox74.DataField = "ITEM119";
            this.textBox74.Height = 0.2559057F;
            this.textBox74.Left = 0.9464568F;
            this.textBox74.Name = "textBox74";
            this.textBox74.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5.5pt; font" +
    "-weight: normal; text-align: left; vertical-align: bottom; ddo-char-set: 1";
            this.textBox74.Tag = "";
            this.textBox74.Text = "ITEM119";
            this.textBox74.Top = 5.809449F;
            this.textBox74.Width = 1.258268F;
            // 
            // textBox307
            // 
            this.textBox307.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox307.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox307.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox307.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox307.DataField = "ITEM118";
            this.textBox307.Height = 0.09645706F;
            this.textBox307.Left = 0.9496064F;
            this.textBox307.Name = "textBox307";
            this.textBox307.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5.5pt; font" +
    "-weight: normal; text-align: left; vertical-align: top; ddo-char-set: 1";
            this.textBox307.Tag = "";
            this.textBox307.Text = "ITEM118";
            this.textBox307.Top = 5.809449F;
            this.textBox307.Width = 1.255118F;
            // 
            // textBox276
            // 
            this.textBox276.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox276.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox276.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox276.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox276.Height = 0.1559055F;
            this.textBox276.Left = 2.944489F;
            this.textBox276.Name = "textBox276";
            this.textBox276.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox276.Tag = "";
            this.textBox276.Text = "個人番号";
            this.textBox276.Top = 5.655118F;
            this.textBox276.Width = 0.4751968F;
            // 
            // textBox281
            // 
            this.textBox281.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox281.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox281.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox281.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox281.Height = 0.1559055F;
            this.textBox281.Left = 2.944489F;
            this.textBox281.Name = "textBox281";
            this.textBox281.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox281.Tag = "";
            this.textBox281.Text = "個人番号";
            this.textBox281.Top = 6.06693F;
            this.textBox281.Width = 0.4751968F;
            // 
            // textBox266
            // 
            this.textBox266.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox266.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox266.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox266.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox266.Height = 0.1559055F;
            this.textBox266.Left = 2.944489F;
            this.textBox266.Name = "textBox266";
            this.textBox266.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox266.Tag = "";
            this.textBox266.Text = "個人番号";
            this.textBox266.Top = 5.240551F;
            this.textBox266.Width = 0.4751968F;
            // 
            // textBox271
            // 
            this.textBox271.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox271.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox271.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox271.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox271.Height = 0.1559055F;
            this.textBox271.Left = 2.944489F;
            this.textBox271.Name = "textBox271";
            this.textBox271.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox271.Tag = "";
            this.textBox271.Text = "個人番号";
            this.textBox271.Top = 4.828347F;
            this.textBox271.Width = 0.4751968F;
            // 
            // textBox260
            // 
            this.textBox260.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox260.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox260.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox260.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox260.Height = 0.4133858F;
            this.textBox260.Left = 2.788189F;
            this.textBox260.Name = "textBox260";
            this.textBox260.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1; ddo-" +
    "font-vertical: true";
            this.textBox260.Tag = "";
            this.textBox260.Text = "1";
            this.textBox260.Top = 4.571259F;
            this.textBox260.Width = 0.1574803F;
            // 
            // textBox261
            // 
            this.textBox261.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox261.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox261.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox261.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox261.Height = 0.4133858F;
            this.textBox261.Left = 2.788189F;
            this.textBox261.Name = "textBox261";
            this.textBox261.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1; ddo-" +
    "font-vertical: true";
            this.textBox261.Tag = "";
            this.textBox261.Text = "2";
            this.textBox261.Top = 4.984251F;
            this.textBox261.Width = 0.1574803F;
            // 
            // textBox262
            // 
            this.textBox262.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox262.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox262.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox262.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox262.Height = 0.4133858F;
            this.textBox262.Left = 2.788189F;
            this.textBox262.Name = "textBox262";
            this.textBox262.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1; ddo-" +
    "font-vertical: true";
            this.textBox262.Tag = "";
            this.textBox262.Text = "3";
            this.textBox262.Top = 5.397637F;
            this.textBox262.Width = 0.1574803F;
            // 
            // textBox263
            // 
            this.textBox263.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox263.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox263.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox263.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox263.Height = 0.4133858F;
            this.textBox263.Left = 2.788189F;
            this.textBox263.Name = "textBox263";
            this.textBox263.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1; ddo-" +
    "font-vertical: true";
            this.textBox263.Tag = "";
            this.textBox263.Text = "4";
            this.textBox263.Top = 5.809449F;
            this.textBox263.Width = 0.1574803F;
            // 
            // textBox270
            // 
            this.textBox270.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox270.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox270.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox270.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox270.DataField = "ITEM123";
            this.textBox270.Height = 0.2625982F;
            this.textBox270.Left = 3.419685F;
            this.textBox270.Name = "textBox270";
            this.textBox270.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5.5pt; font" +
    "-weight: normal; text-align: left; vertical-align: bottom; ddo-char-set: 1";
            this.textBox270.Tag = "";
            this.textBox270.Text = "ITEM123";
            this.textBox270.Top = 4.566142F;
            this.textBox270.Width = 1.258268F;
            // 
            // textBox315
            // 
            this.textBox315.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox315.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox315.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox315.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox315.DataField = "ITEM122";
            this.textBox315.Height = 0.09645708F;
            this.textBox315.Left = 3.421654F;
            this.textBox315.Name = "textBox315";
            this.textBox315.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5.5pt; font" +
    "-weight: normal; text-align: left; vertical-align: top; ddo-char-set: 1";
            this.textBox315.Tag = "";
            this.textBox315.Text = "ITEM122";
            this.textBox315.Top = 4.567717F;
            this.textBox315.Width = 1.256299F;
            // 
            // textBox265
            // 
            this.textBox265.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox265.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox265.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox265.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox265.DataField = "ITEM127";
            this.textBox265.Height = 0.2559048F;
            this.textBox265.Left = 3.419685F;
            this.textBox265.Name = "textBox265";
            this.textBox265.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5.5pt; font" +
    "-weight: normal; text-align: left; vertical-align: bottom; ddo-char-set: 1";
            this.textBox265.Tag = "";
            this.textBox265.Text = "ITEM127";
            this.textBox265.Top = 4.984645F;
            this.textBox265.Width = 1.258268F;
            // 
            // textBox312
            // 
            this.textBox312.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox312.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox312.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox312.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox312.DataField = "ITEM126";
            this.textBox312.Height = 0.09645708F;
            this.textBox312.Left = 3.419685F;
            this.textBox312.Name = "textBox312";
            this.textBox312.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5.5pt; font" +
    "-weight: normal; text-align: left; vertical-align: top; ddo-char-set: 1";
            this.textBox312.Tag = "";
            this.textBox312.Text = "ITEM126";
            this.textBox312.Top = 4.984252F;
            this.textBox312.Width = 1.258268F;
            // 
            // textBox275
            // 
            this.textBox275.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox275.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox275.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox275.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox275.DataField = "ITEM131";
            this.textBox275.Height = 0.2578741F;
            this.textBox275.Left = 3.419685F;
            this.textBox275.Name = "textBox275";
            this.textBox275.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5.5pt; font" +
    "-weight: normal; text-align: left; vertical-align: bottom; ddo-char-set: 1";
            this.textBox275.Tag = "";
            this.textBox275.Text = "ITEM131";
            this.textBox275.Top = 5.399211F;
            this.textBox275.Width = 1.258268F;
            // 
            // textBox309
            // 
            this.textBox309.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox309.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox309.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox309.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox309.DataField = "ITEM130";
            this.textBox309.Height = 0.09645708F;
            this.textBox309.Left = 3.419685F;
            this.textBox309.Name = "textBox309";
            this.textBox309.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5.5pt; font" +
    "-weight: normal; text-align: left; vertical-align: top; ddo-char-set: 1";
            this.textBox309.Tag = "";
            this.textBox309.Text = "ITEM130";
            this.textBox309.Top = 5.399607F;
            this.textBox309.Width = 1.258268F;
            // 
            // textBox280
            // 
            this.textBox280.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox280.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox280.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox280.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox280.DataField = "ITEM135";
            this.textBox280.Height = 0.2559057F;
            this.textBox280.Left = 3.419685F;
            this.textBox280.Name = "textBox280";
            this.textBox280.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5.5pt; font" +
    "-weight: normal; text-align: left; vertical-align: bottom; ddo-char-set: 1";
            this.textBox280.Tag = "";
            this.textBox280.Text = "ITEM135";
            this.textBox280.Top = 5.809843F;
            this.textBox280.Width = 1.258267F;
            // 
            // textBox308
            // 
            this.textBox308.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox308.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox308.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox308.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox308.DataField = "ITEM134";
            this.textBox308.Height = 0.09645708F;
            this.textBox308.Left = 3.419685F;
            this.textBox308.Name = "textBox308";
            this.textBox308.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5.5pt; font" +
    "-weight: normal; text-align: left; vertical-align: top; ddo-char-set: 1";
            this.textBox308.Tag = "";
            this.textBox308.Text = "ITEM134";
            this.textBox308.Top = 5.811418F;
            this.textBox308.Width = 1.258268F;
            // 
            // line124
            // 
            this.line124.Height = 0.1350394F;
            this.line124.Left = 3.893701F;
            this.line124.LineWeight = 1F;
            this.line124.Name = "line124";
            this.line124.Top = 0.3956693F;
            this.line124.Width = 0F;
            this.line124.X1 = 3.893701F;
            this.line124.X2 = 3.893701F;
            this.line124.Y1 = 0.5307087F;
            this.line124.Y2 = 0.3956693F;
            // 
            // label26
            // 
            this.label26.Height = 0.08611111F;
            this.label26.HyperLink = null;
            this.label26.Left = 5.370866F;
            this.label26.Name = "label26";
            this.label26.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label26.Tag = "";
            this.label26.Text = "円";
            this.label26.Top = 4.16811F;
            this.label26.Width = 0.166667F;
            // 
            // textBox29
            // 
            this.textBox29.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox29.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox29.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox29.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox29.DataField = "ITEM068";
            this.textBox29.Height = 0.2082678F;
            this.textBox29.Left = 3.809449F;
            this.textBox29.Name = "textBox29";
            this.textBox29.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox29.Tag = "";
            this.textBox29.Text = "ITEM068";
            this.textBox29.Top = 1.968505F;
            this.textBox29.Width = 0.4854331F;
            // 
            // textBox48
            // 
            this.textBox48.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox48.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox48.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox48.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox48.DataField = "ITEM042";
            this.textBox48.Height = 0.203937F;
            this.textBox48.Left = 4.294882F;
            this.textBox48.Name = "textBox48";
            this.textBox48.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox48.Tag = "";
            this.textBox48.Text = "ITEM042";
            this.textBox48.Top = 1.974016F;
            this.textBox48.Width = 0.2110233F;
            // 
            // label15
            // 
            this.label15.Height = 0.08611111F;
            this.label15.HyperLink = null;
            this.label15.Left = 4.316929F;
            this.label15.Name = "label15";
            this.label15.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.label15.Tag = "";
            this.label15.Text = "内人";
            this.label15.Top = 1.974016F;
            this.label15.Width = 0.3857827F;
            // 
            // textBox41
            // 
            this.textBox41.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox41.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox41.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox41.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox41.Height = 0.3232284F;
            this.textBox41.Left = 2.848819F;
            this.textBox41.Name = "textBox41";
            this.textBox41.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox41.Tag = "";
            this.textBox41.Text = "地震保険料の控除額";
            this.textBox41.Top = 2.18189F;
            this.textBox41.Width = 1.356693F;
            // 
            // textBox43
            // 
            this.textBox43.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox43.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox43.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox43.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox43.Height = 0.3232284F;
            this.textBox43.Left = 1.486221F;
            this.textBox43.Name = "textBox43";
            this.textBox43.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox43.Tag = "";
            this.textBox43.Text = "生命保険料の 控 除 額";
            this.textBox43.Top = 2.18189F;
            this.textBox43.Width = 1.362598F;
            // 
            // textBox33
            // 
            this.textBox33.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox33.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox33.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox33.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox33.Height = 0.3232283F;
            this.textBox33.Left = 4.198032F;
            this.textBox33.Name = "textBox33";
            this.textBox33.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox33.Tag = "";
            this.textBox33.Text = "住宅借入金等特別控除の額";
            this.textBox33.Top = 2.18189F;
            this.textBox33.Width = 1.349606F;
            // 
            // textBox1
            // 
            this.textBox1.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox1.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox1.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox1.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox1.Height = 0.6885816F;
            this.textBox1.Left = 5.827164F;
            this.textBox1.Name = "textBox1";
            this.textBox1.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox1.Tag = "";
            this.textBox1.Text = "支払を\r\n受ける者";
            this.textBox1.Top = 0.2570881F;
            this.textBox1.Width = 0.3448837F;
            // 
            // textBox127
            // 
            this.textBox127.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox127.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox127.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox127.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox127.Height = 0.6889765F;
            this.textBox127.Left = 6.172048F;
            this.textBox127.Name = "textBox127";
            this.textBox127.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox127.Tag = "";
            this.textBox127.Text = "住所又は居所";
            this.textBox127.Top = 0.2566933F;
            this.textBox127.Width = 0.2047244F;
            // 
            // textBox173
            // 
            this.textBox173.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox173.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox173.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox173.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox173.Height = 0.2759842F;
            this.textBox173.Left = 8.998819F;
            this.textBox173.Name = "textBox173";
            this.textBox173.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox173.Tag = "";
            this.textBox173.Text = "氏名";
            this.textBox173.Top = 0.6692919F;
            this.textBox173.Width = 0.2444882F;
            // 
            // textBox174
            // 
            this.textBox174.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox174.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox174.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox174.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox174.DataField = "ITEM022";
            this.textBox174.Height = 0.2295276F;
            this.textBox174.Left = 6.376772F;
            this.textBox174.Name = "textBox174";
            this.textBox174.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: left; vertical-align: middle; ddo-char-set: 1";
            this.textBox174.Tag = "";
            this.textBox174.Text = "ITEM022";
            this.textBox174.Top = 0.2570869F;
            this.textBox174.Width = 2.622047F;
            // 
            // textBox175
            // 
            this.textBox175.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox175.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox175.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox175.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox175.DataField = "ITEM023";
            this.textBox175.Height = 0.2295276F;
            this.textBox175.Left = 6.376772F;
            this.textBox175.Name = "textBox175";
            this.textBox175.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: left; vertical-align: middle; ddo-char-set: 1";
            this.textBox175.Tag = "";
            this.textBox175.Text = "ITEM023";
            this.textBox175.Top = 0.486221F;
            this.textBox175.Width = 2.622047F;
            // 
            // textBox176
            // 
            this.textBox176.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox176.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox176.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox176.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox176.DataField = "ITEM024";
            this.textBox176.Height = 0.2295276F;
            this.textBox176.Left = 6.376772F;
            this.textBox176.Name = "textBox176";
            this.textBox176.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: left; vertical-align: middle; ddo-char-set: 1";
            this.textBox176.Tag = "";
            this.textBox176.Text = "ITEM024";
            this.textBox176.Top = 0.7157482F;
            this.textBox176.Width = 2.622047F;
            // 
            // textBox178
            // 
            this.textBox178.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox178.DataField = "ITEM028";
            this.textBox178.Height = 0.1374016F;
            this.textBox178.Left = 9.287796F;
            this.textBox178.Name = "textBox178";
            this.textBox178.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.textBox178.Tag = "";
            this.textBox178.Text = "ITEM028";
            this.textBox178.Top = 0.8074804F;
            this.textBox178.Width = 1.959449F;
            // 
            // textBox179
            // 
            this.textBox179.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox179.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox179.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox179.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox179.Height = 0.1377953F;
            this.textBox179.Left = 8.999212F;
            this.textBox179.Name = "textBox179";
            this.textBox179.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: left; vertical-align: middle; ddo-char-set: 1";
            this.textBox179.Tag = "";
            this.textBox179.Text = "(個人番号)";
            this.textBox179.Top = 0.3937012F;
            this.textBox179.Width = 2.248033F;
            // 
            // textBox180
            // 
            this.textBox180.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox180.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox180.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox180.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox180.DataField = "ITEM030";
            this.textBox180.Height = 0.3152778F;
            this.textBox180.Left = 6.87244F;
            this.textBox180.Name = "textBox180";
            this.textBox180.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; vertical-align: bottom; ddo-char-set: 1";
            this.textBox180.Tag = "";
            this.textBox180.Text = "ITEM030";
            this.textBox180.Top = 1.231498F;
            this.textBox180.Width = 1.063385F;
            // 
            // textBox181
            // 
            this.textBox181.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox181.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox181.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox181.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox181.Height = 0.2858279F;
            this.textBox181.Left = 5.827164F;
            this.textBox181.Name = "textBox181";
            this.textBox181.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox181.Tag = "";
            this.textBox181.Text = "種　　　別";
            this.textBox181.Top = 0.9456699F;
            this.textBox181.Width = 1.045276F;
            // 
            // textBox182
            // 
            this.textBox182.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox182.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox182.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox182.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox182.DataField = "ITEM029";
            this.textBox182.Height = 0.3152778F;
            this.textBox182.Left = 5.827164F;
            this.textBox182.Name = "textBox182";
            this.textBox182.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: left; vertical-align: bottom; ddo-char-set: 1";
            this.textBox182.Tag = "";
            this.textBox182.Text = "ITEM029";
            this.textBox182.Top = 1.231498F;
            this.textBox182.Width = 1.045276F;
            // 
            // textBox183
            // 
            this.textBox183.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox183.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox183.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox183.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox183.Height = 0.2858279F;
            this.textBox183.Left = 6.872441F;
            this.textBox183.Name = "textBox183";
            this.textBox183.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox183.Tag = "";
            this.textBox183.Text = "支　払　金　額";
            this.textBox183.Top = 0.9456699F;
            this.textBox183.Width = 1.063194F;
            // 
            // textBox184
            // 
            this.textBox184.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox184.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox184.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox184.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox184.DataField = "ITEM031";
            this.textBox184.Height = 0.3152778F;
            this.textBox184.Left = 7.935825F;
            this.textBox184.Name = "textBox184";
            this.textBox184.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; vertical-align: bottom; ddo-char-set: 1";
            this.textBox184.Tag = "";
            this.textBox184.Text = "ITEM031";
            this.textBox184.Top = 1.231498F;
            this.textBox184.Width = 1.063385F;
            // 
            // textBox185
            // 
            this.textBox185.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox185.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox185.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox185.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox185.Height = 0.2858279F;
            this.textBox185.Left = 7.935825F;
            this.textBox185.Name = "textBox185";
            this.textBox185.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox185.Tag = "";
            this.textBox185.Text = "給与所得控除後の金額";
            this.textBox185.Top = 0.9456699F;
            this.textBox185.Width = 1.063194F;
            // 
            // textBox186
            // 
            this.textBox186.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox186.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox186.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox186.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox186.DataField = "ITEM032";
            this.textBox186.Height = 0.3152778F;
            this.textBox186.Left = 8.999207F;
            this.textBox186.Name = "textBox186";
            this.textBox186.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; vertical-align: bottom; ddo-char-set: 1";
            this.textBox186.Tag = "";
            this.textBox186.Text = "ITEM032";
            this.textBox186.Top = 1.231498F;
            this.textBox186.Width = 1.063385F;
            // 
            // textBox187
            // 
            this.textBox187.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox187.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox187.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox187.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox187.Height = 0.2858279F;
            this.textBox187.Left = 8.999207F;
            this.textBox187.Name = "textBox187";
            this.textBox187.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox187.Tag = "";
            this.textBox187.Text = "所得控除の額の合計額";
            this.textBox187.Top = 0.9456699F;
            this.textBox187.Width = 1.063194F;
            // 
            // textBox188
            // 
            this.textBox188.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox188.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox188.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox188.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox188.DataField = "ITEM033";
            this.textBox188.Height = 0.3152778F;
            this.textBox188.Left = 10.0626F;
            this.textBox188.Name = "textBox188";
            this.textBox188.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; vertical-align: bottom; ddo-char-set: 1";
            this.textBox188.Tag = "";
            this.textBox188.Text = "ITEM033";
            this.textBox188.Top = 1.231498F;
            this.textBox188.Width = 1.184648F;
            // 
            // textBox189
            // 
            this.textBox189.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox189.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox189.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox189.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox189.Height = 0.2858279F;
            this.textBox189.Left = 10.0626F;
            this.textBox189.Name = "textBox189";
            this.textBox189.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox189.Tag = "";
            this.textBox189.Text = "源 泉 徴 収 税 額";
            this.textBox189.Top = 0.9456699F;
            this.textBox189.Width = 1.184648F;
            // 
            // textBox190
            // 
            this.textBox190.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox190.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox190.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox190.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox190.Height = 0.3152788F;
            this.textBox190.Left = 5.827164F;
            this.textBox190.Name = "textBox190";
            this.textBox190.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: left; vertical-align: middle; ddo-char-set: 1";
            this.textBox190.Tag = "";
            this.textBox190.Text = "控除対象配偶者の有無等";
            this.textBox190.Top = 1.546851F;
            this.textBox190.Width = 0.9748049F;
            // 
            // textBox191
            // 
            this.textBox191.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox191.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox191.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox191.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox191.Height = 0.7720473F;
            this.textBox191.Left = 5.827166F;
            this.textBox191.Name = "textBox191";
            this.textBox191.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: left; vertical-align: top; ddo-char-set: 1";
            this.textBox191.Tag = "";
            this.textBox191.Text = "(摘要)";
            this.textBox191.Top = 2.860236F;
            this.textBox191.Width = 5.420079F;
            // 
            // label21
            // 
            this.label21.Height = 0.08611111F;
            this.label21.HyperLink = null;
            this.label21.Left = 7.769291F;
            this.label21.Name = "label21";
            this.label21.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label21.Tag = "";
            this.label21.Text = "円";
            this.label21.Top = 1.247246F;
            this.label21.Width = 0.1666667F;
            // 
            // label27
            // 
            this.label27.Height = 0.08611111F;
            this.label27.HyperLink = null;
            this.label27.Left = 9.88504F;
            this.label27.Name = "label27";
            this.label27.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label27.Tag = "";
            this.label27.Text = "円";
            this.label27.Top = 1.247246F;
            this.label27.Width = 0.1666667F;
            // 
            // label28
            // 
            this.label28.Height = 0.08611111F;
            this.label28.HyperLink = null;
            this.label28.Left = 8.832283F;
            this.label28.Name = "label28";
            this.label28.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label28.Tag = "";
            this.label28.Text = "円";
            this.label28.Top = 1.247246F;
            this.label28.Width = 0.1666667F;
            // 
            // label29
            // 
            this.label29.Height = 0.08611111F;
            this.label29.HyperLink = null;
            this.label29.Left = 11.08779F;
            this.label29.Name = "label29";
            this.label29.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label29.Tag = "";
            this.label29.Text = "円";
            this.label29.Top = 1.247246F;
            this.label29.Width = 0.1595824F;
            // 
            // textBox193
            // 
            this.textBox193.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox193.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox193.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox193.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox193.Height = 0.1377953F;
            this.textBox193.Left = 8.999605F;
            this.textBox193.Name = "textBox193";
            this.textBox193.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.textBox193.Tag = "";
            this.textBox193.Text = "(役職名)";
            this.textBox193.Top = 0.5314964F;
            this.textBox193.Width = 2.247638F;
            // 
            // textBox197
            // 
            this.textBox197.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox197.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox197.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox197.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox197.Height = 0.1072454F;
            this.textBox197.Left = 5.827166F;
            this.textBox197.Name = "textBox197";
            this.textBox197.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox197.Tag = "";
            this.textBox197.Text = "　有";
            this.textBox197.Top = 1.862047F;
            this.textBox197.Width = 0.3448818F;
            // 
            // textBox198
            // 
            this.textBox198.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox198.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox198.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox198.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox198.DataField = "ITEM034";
            this.textBox198.Height = 0.2196845F;
            this.textBox198.Left = 5.827166F;
            this.textBox198.Name = "textBox198";
            this.textBox198.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox198.Tag = "";
            this.textBox198.Text = "ITEM034";
            this.textBox198.Top = 1.969292F;
            this.textBox198.Width = 0.3448818F;
            // 
            // textBox199
            // 
            this.textBox199.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox199.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox199.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox199.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox199.Height = 0.1070866F;
            this.textBox199.Left = 6.172048F;
            this.textBox199.Name = "textBox199";
            this.textBox199.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox199.Tag = "";
            this.textBox199.Text = "従有";
            this.textBox199.Top = 1.862205F;
            this.textBox199.Width = 0.2944883F;
            // 
            // textBox200
            // 
            this.textBox200.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox200.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox200.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox200.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox200.Height = 0.2196846F;
            this.textBox200.Left = 6.172048F;
            this.textBox200.Name = "textBox200";
            this.textBox200.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: bold; text-align: center; ddo-char-set: 1";
            this.textBox200.Tag = "";
            this.textBox200.Text = null;
            this.textBox200.Top = 1.969292F;
            this.textBox200.Width = 0.2948819F;
            // 
            // textBox201
            // 
            this.textBox201.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox201.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox201.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox201.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox201.CanGrow = false;
            this.textBox201.Height = 0.1576389F;
            this.textBox201.Left = 6.466536F;
            this.textBox201.Name = "textBox201";
            this.textBox201.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox201.Tag = "";
            this.textBox201.Text = "老人";
            this.textBox201.Top = 1.704408F;
            this.textBox201.Width = 0.3360731F;
            // 
            // textBox202
            // 
            this.textBox202.DataField = "ITEM036";
            this.textBox202.Height = 0.2082679F;
            this.textBox202.Left = 6.487008F;
            this.textBox202.Name = "textBox202";
            this.textBox202.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox202.Tag = "";
            this.textBox202.Text = "ITEM036";
            this.textBox202.Top = 1.969292F;
            this.textBox202.Width = 0.3149607F;
            // 
            // textBox203
            // 
            this.textBox203.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox203.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox203.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox203.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox203.Height = 0.3152788F;
            this.textBox203.Left = 6.801969F;
            this.textBox203.Name = "textBox203";
            this.textBox203.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox203.Tag = "";
            this.textBox203.Text = "配偶者特別控除の額";
            this.textBox203.Top = 1.54685F;
            this.textBox203.Width = 0.5909723F;
            // 
            // textBox204
            // 
            this.textBox204.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox204.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox204.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox204.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox204.DataField = "ITEM037";
            this.textBox204.Height = 0.326847F;
            this.textBox204.Left = 6.801969F;
            this.textBox204.Name = "textBox204";
            this.textBox204.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox204.Tag = "";
            this.textBox204.Text = "ITEM037";
            this.textBox204.Top = 1.86213F;
            this.textBox204.Width = 0.5909723F;
            // 
            // label30
            // 
            this.label30.Height = 0.08611111F;
            this.label30.HyperLink = null;
            this.label30.Left = 7.221413F;
            this.label30.Name = "label30";
            this.label30.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label30.Tag = "";
            this.label30.Text = "円";
            this.label30.Top = 1.863518F;
            this.label30.Width = 0.1666667F;
            // 
            // textBox205
            // 
            this.textBox205.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox205.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox205.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox205.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox205.Height = 0.3149616F;
            this.textBox205.Left = 7.392914F;
            this.textBox205.Name = "textBox205";
            this.textBox205.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox205.Tag = "";
            this.textBox205.Text = "控 除 対 象 扶 養 親 族 の 数\r\n（ 配 偶 者 を 除 く 。）";
            this.textBox205.Top = 1.547245F;
            this.textBox205.Width = 2.116143F;
            // 
            // textBox206
            // 
            this.textBox206.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox206.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox206.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox206.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox206.Height = 0.3153553F;
            this.textBox206.Left = 9.995275F;
            this.textBox206.Name = "textBox206";
            this.textBox206.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox206.Tag = "";
            this.textBox206.Text = "障害者の数\r\n(本人を除く)";
            this.textBox206.Top = 1.546851F;
            this.textBox206.Width = 0.7877958F;
            // 
            // textBox207
            // 
            this.textBox207.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox207.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox207.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox207.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox207.Height = 0.107032F;
            this.textBox207.Left = 7.392914F;
            this.textBox207.Name = "textBox207";
            this.textBox207.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox207.Tag = "";
            this.textBox207.Text = "特 定";
            this.textBox207.Top = 1.862205F;
            this.textBox207.Width = 0.5429134F;
            // 
            // textBox208
            // 
            this.textBox208.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox208.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox208.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox208.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox208.Height = 0.107032F;
            this.textBox208.Left = 7.931891F;
            this.textBox208.Name = "textBox208";
            this.textBox208.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox208.Tag = "";
            this.textBox208.Text = "老　人";
            this.textBox208.Top = 1.862205F;
            this.textBox208.Width = 0.9960632F;
            // 
            // textBox209
            // 
            this.textBox209.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox209.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox209.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox209.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox209.Height = 0.1059056F;
            this.textBox209.Left = 8.927952F;
            this.textBox209.Name = "textBox209";
            this.textBox209.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox209.Tag = "";
            this.textBox209.Text = "その他";
            this.textBox209.Top = 1.863387F;
            this.textBox209.Width = 0.5811022F;
            // 
            // textBox210
            // 
            this.textBox210.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox210.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox210.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox210.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox210.Height = 0.107032F;
            this.textBox210.Left = 9.994488F;
            this.textBox210.Name = "textBox210";
            this.textBox210.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox210.Tag = "";
            this.textBox210.Text = "特別";
            this.textBox210.Top = 1.863387F;
            this.textBox210.Width = 0.3940945F;
            // 
            // textBox211
            // 
            this.textBox211.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox211.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox211.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox211.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox211.Height = 0.1070867F;
            this.textBox211.Left = 10.39331F;
            this.textBox211.Name = "textBox211";
            this.textBox211.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox211.Tag = "";
            this.textBox211.Text = "その他";
            this.textBox211.Top = 1.863387F;
            this.textBox211.Width = 0.3897636F;
            // 
            // textBox212
            // 
            this.textBox212.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox212.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox212.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox212.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox212.CanGrow = false;
            this.textBox212.Height = 0.4224422F;
            this.textBox212.Left = 9.509054F;
            this.textBox212.Name = "textBox212";
            this.textBox212.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox212.Tag = "";
            this.textBox212.Text = "16歳未満\r\n扶養親族の数";
            this.textBox212.Top = 1.546851F;
            this.textBox212.Width = 0.4854331F;
            // 
            // textBox213
            // 
            this.textBox213.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox213.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox213.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox213.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox213.DataField = "ITEM138";
            this.textBox213.Height = 0.2082678F;
            this.textBox213.Left = 10.78307F;
            this.textBox213.Name = "textBox213";
            this.textBox213.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox213.Tag = "";
            this.textBox213.Text = "ITEM138";
            this.textBox213.Top = 1.973622F;
            this.textBox213.Width = 0.4641754F;
            // 
            // textBox214
            // 
            this.textBox214.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox214.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox214.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox214.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox214.CanGrow = false;
            this.textBox214.Height = 0.4224409F;
            this.textBox214.Left = 10.78307F;
            this.textBox214.Name = "textBox214";
            this.textBox214.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox214.Tag = "";
            this.textBox214.Text = "非住居者である親族の数";
            this.textBox214.Top = 1.546851F;
            this.textBox214.Width = 0.4641754F;
            // 
            // textBox215
            // 
            this.textBox215.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox215.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox215.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox215.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox215.DataField = "ITEM045";
            this.textBox215.Height = 0.3541667F;
            this.textBox215.Left = 5.827166F;
            this.textBox215.Name = "textBox215";
            this.textBox215.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: right; vertical-align: bottom; ddo-char-set: 1";
            this.textBox215.Tag = "";
            this.textBox215.Text = "ITEM045";
            this.textBox215.Top = 2.505906F;
            this.textBox215.Width = 1.367717F;
            // 
            // textBox216
            // 
            this.textBox216.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox216.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox216.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox216.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox216.DataField = "ITEM049";
            this.textBox216.Height = 0.3541667F;
            this.textBox216.Left = 9.897638F;
            this.textBox216.Name = "textBox216";
            this.textBox216.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: right; vertical-align: bottom; ddo-char-set: 1";
            this.textBox216.Tag = "";
            this.textBox216.Text = "ITEM049";
            this.textBox216.Top = 2.505906F;
            this.textBox216.Width = 1.349606F;
            // 
            // textBox217
            // 
            this.textBox217.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox217.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox217.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox217.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox217.Height = 0.3232284F;
            this.textBox217.Left = 5.827166F;
            this.textBox217.Name = "textBox217";
            this.textBox217.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox217.Tag = "";
            this.textBox217.Text = "社会保険料等 の 金 額";
            this.textBox217.Top = 2.182678F;
            this.textBox217.Width = 1.358662F;
            // 
            // textBox218
            // 
            this.textBox218.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox218.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox218.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox218.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox218.DataField = "ITEM048";
            this.textBox218.Height = 0.3541667F;
            this.textBox218.Left = 8.548424F;
            this.textBox218.Name = "textBox218";
            this.textBox218.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: right; vertical-align: bottom; ddo-char-set: 1";
            this.textBox218.Tag = "";
            this.textBox218.Text = "ITEM048";
            this.textBox218.Top = 2.505906F;
            this.textBox218.Width = 1.349214F;
            // 
            // textBox219
            // 
            this.textBox219.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox219.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox219.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox219.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox219.DataField = "ITEM047";
            this.textBox219.Height = 0.3541667F;
            this.textBox219.Left = 7.185828F;
            this.textBox219.Name = "textBox219";
            this.textBox219.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: right; vertical-align: bottom; ddo-char-set: 1";
            this.textBox219.Tag = "";
            this.textBox219.Text = "ITEM047";
            this.textBox219.Top = 2.505906F;
            this.textBox219.Width = 1.362626F;
            // 
            // textBox220
            // 
            this.textBox220.DataField = "ITEM046";
            this.textBox220.Height = 0.09582743F;
            this.textBox220.Left = 5.854725F;
            this.textBox220.Name = "textBox220";
            this.textBox220.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox220.Tag = "";
            this.textBox220.Text = "ITEM046";
            this.textBox220.Top = 2.547245F;
            this.textBox220.Width = 1.262102F;
            // 
            // label31
            // 
            this.label31.Height = 0.08611111F;
            this.label31.HyperLink = null;
            this.label31.Left = 8.378313F;
            this.label31.Name = "label31";
            this.label31.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label31.Tag = "";
            this.label31.Text = "円";
            this.label31.Top = 2.517711F;
            this.label31.Width = 0.1666665F;
            // 
            // label32
            // 
            this.label32.Height = 0.08611111F;
            this.label32.HyperLink = null;
            this.label32.Left = 9.731102F;
            this.label32.Name = "label32";
            this.label32.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label32.Tag = "";
            this.label32.Text = "円";
            this.label32.Top = 2.517717F;
            this.label32.Width = 0.1666665F;
            // 
            // label33
            // 
            this.label33.Height = 0.08611111F;
            this.label33.HyperLink = null;
            this.label33.Left = 11.07756F;
            this.label33.Name = "label33";
            this.label33.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label33.Tag = "";
            this.label33.Text = "円";
            this.label33.Top = 2.517717F;
            this.label33.Width = 0.1595535F;
            // 
            // textBox221
            // 
            this.textBox221.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox221.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox221.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox221.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox221.Height = 0.2149611F;
            this.textBox221.Left = 8.597244F;
            this.textBox221.Name = "textBox221";
            this.textBox221.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: bold; tex" +
    "t-align: center; ddo-char-set: 1";
            this.textBox221.Tag = "";
            this.textBox221.Text = null;
            this.textBox221.Top = 1.969686F;
            this.textBox221.Width = 0.3307088F;
            // 
            // textBox222
            // 
            this.textBox222.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox222.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox222.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox222.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox222.DataField = "ITEM041";
            this.textBox222.Height = 0.2149611F;
            this.textBox222.Left = 8.927952F;
            this.textBox222.Name = "textBox222";
            this.textBox222.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox222.Tag = "";
            this.textBox222.Text = "ITEM041";
            this.textBox222.Top = 1.967717F;
            this.textBox222.Width = 0.3149602F;
            // 
            // textBox223
            // 
            this.textBox223.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox223.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox223.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox223.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox223.Height = 0.2149611F;
            this.textBox223.Left = 9.216928F;
            this.textBox223.Name = "textBox223";
            this.textBox223.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: bold; tex" +
    "t-align: center; ddo-char-set: 1";
            this.textBox223.Tag = "";
            this.textBox223.Text = null;
            this.textBox223.Top = 1.967717F;
            this.textBox223.Width = 0.2921261F;
            // 
            // textBox224
            // 
            this.textBox224.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox224.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox224.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox224.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox224.DataField = "ITEM043";
            this.textBox224.Height = 0.203937F;
            this.textBox224.Left = 10.20551F;
            this.textBox224.Name = "textBox224";
            this.textBox224.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox224.Tag = "";
            this.textBox224.Text = "ITEM043";
            this.textBox224.Top = 1.977953F;
            this.textBox224.Width = 0.1968504F;
            // 
            // textBox225
            // 
            this.textBox225.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox225.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox225.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox225.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox225.DataField = "ITEM044";
            this.textBox225.Height = 0.2070867F;
            this.textBox225.Left = 10.39331F;
            this.textBox225.Name = "textBox225";
            this.textBox225.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox225.Tag = "";
            this.textBox225.Text = "ITEM044";
            this.textBox225.Top = 1.975197F;
            this.textBox225.Width = 0.3896542F;
            // 
            // label34
            // 
            this.label34.Height = 0.08611111F;
            this.label34.HyperLink = null;
            this.label34.Left = 9.055119F;
            this.label34.Name = "label34";
            this.label34.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label34.Tag = "";
            this.label34.Text = "人";
            this.label34.Top = 1.971654F;
            this.label34.Width = 0.15625F;
            // 
            // label35
            // 
            this.label35.Height = 0.08740146F;
            this.label35.HyperLink = null;
            this.label35.Left = 10.46693F;
            this.label35.Name = "label35";
            this.label35.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label35.Tag = "";
            this.label35.Text = "人";
            this.label35.Top = 1.974804F;
            this.label35.Width = 0.2896156F;
            // 
            // textBox226
            // 
            this.textBox226.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox226.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox226.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox226.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox226.DataField = "ITEM038";
            this.textBox226.Height = 0.2149611F;
            this.textBox226.Left = 7.396457F;
            this.textBox226.Name = "textBox226";
            this.textBox226.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox226.Tag = "";
            this.textBox226.Text = "ITEM038";
            this.textBox226.Top = 1.969291F;
            this.textBox226.Width = 0.2677166F;
            // 
            // textBox227
            // 
            this.textBox227.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox227.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox227.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox227.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox227.Height = 0.2149609F;
            this.textBox227.Left = 7.664174F;
            this.textBox227.Name = "textBox227";
            this.textBox227.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: bold; tex" +
    "t-align: center; ddo-char-set: 1";
            this.textBox227.Tag = "";
            this.textBox227.Text = null;
            this.textBox227.Top = 1.969292F;
            this.textBox227.Width = 0.2677166F;
            // 
            // textBox228
            // 
            this.textBox228.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox228.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox228.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox228.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox228.DataField = "ITEM040";
            this.textBox228.Height = 0.2137799F;
            this.textBox228.Left = 8.266142F;
            this.textBox228.Name = "textBox228";
            this.textBox228.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox228.Tag = "";
            this.textBox228.Text = "ITEM040";
            this.textBox228.Top = 1.969292F;
            this.textBox228.Width = 0.3307088F;
            // 
            // textBox229
            // 
            this.textBox229.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox229.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox229.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox229.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox229.DataField = "ITEM039";
            this.textBox229.Height = 0.2137799F;
            this.textBox229.Left = 7.931891F;
            this.textBox229.Name = "textBox229";
            this.textBox229.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox229.Tag = "";
            this.textBox229.Text = "ITEM039";
            this.textBox229.Top = 1.969292F;
            this.textBox229.Width = 0.3348431F;
            // 
            // label36
            // 
            this.label36.Height = 0.08611111F;
            this.label36.HyperLink = null;
            this.label36.Left = 7.392914F;
            this.label36.Name = "label36";
            this.label36.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label36.Tag = "";
            this.label36.Text = "人";
            this.label36.Top = 1.974804F;
            this.label36.Width = 0.2751969F;
            // 
            // label37
            // 
            this.label37.Height = 0.08611111F;
            this.label37.HyperLink = null;
            this.label37.Left = 7.937402F;
            this.label37.Name = "label37";
            this.label37.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.label37.Tag = "";
            this.label37.Text = "内              人";
            this.label37.Top = 1.974804F;
            this.label37.Width = 0.6614062F;
            // 
            // label38
            // 
            this.label38.Height = 0.08611111F;
            this.label38.HyperLink = null;
            this.label38.Left = 7.664174F;
            this.label38.Name = "label38";
            this.label38.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 4.5pt; font-weight: normal; text-ali" +
    "gn: center; ddo-char-set: 1";
            this.label38.Tag = "";
            this.label38.Text = "従人";
            this.label38.Top = 1.981103F;
            this.label38.Width = 0.2716537F;
            // 
            // label39
            // 
            this.label39.Height = 0.08611111F;
            this.label39.HyperLink = null;
            this.label39.Left = 8.596849F;
            this.label39.Name = "label39";
            this.label39.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 4.5pt; font-weight: normal; text-ali" +
    "gn: center; ddo-char-set: 1";
            this.label39.Tag = "";
            this.label39.Text = "従人";
            this.label39.Top = 1.974804F;
            this.label39.Width = 0.3007894F;
            // 
            // label40
            // 
            this.label40.Height = 0.08611111F;
            this.label40.HyperLink = null;
            this.label40.Left = 9.242914F;
            this.label40.Name = "label40";
            this.label40.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 4.5pt; font-weight: normal; text-ali" +
    "gn: center; ddo-char-set: 1";
            this.label40.Tag = "";
            this.label40.Text = "従人";
            this.label40.Top = 1.971654F;
            this.label40.Width = 0.2661409F;
            // 
            // textBox230
            // 
            this.textBox230.DataField = "ITEM053";
            this.textBox230.Height = 0.1104167F;
            this.textBox230.Left = 5.935827F;
            this.textBox230.Name = "textBox230";
            this.textBox230.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6.5pt; font" +
    "-weight: normal; text-align: left; ddo-char-set: 1";
            this.textBox230.Tag = "";
            this.textBox230.Text = "ITEM053";
            this.textBox230.Top = 3.018505F;
            this.textBox230.Width = 2.519685F;
            // 
            // textBox231
            // 
            this.textBox231.DataField = "ITEM054";
            this.textBox231.Height = 0.1104167F;
            this.textBox231.Left = 5.935713F;
            this.textBox231.Name = "textBox231";
            this.textBox231.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6.5pt; font" +
    "-weight: normal; text-align: left; ddo-char-set: 1";
            this.textBox231.Tag = "";
            this.textBox231.Text = "ITEM054";
            this.textBox231.Top = 3.138114F;
            this.textBox231.Width = 2.519685F;
            // 
            // textBox232
            // 
            this.textBox232.DataField = "ITEM055";
            this.textBox232.Height = 0.1377953F;
            this.textBox232.Left = 8.534252F;
            this.textBox232.Name = "textBox232";
            this.textBox232.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.textBox232.Tag = "";
            this.textBox232.Text = "ITEM055";
            this.textBox232.Top = 2.992914F;
            this.textBox232.Width = 0.9055118F;
            // 
            // textBox233
            // 
            this.textBox233.DataField = "ITEM058";
            this.textBox233.Height = 0.1377953F;
            this.textBox233.Left = 8.534252F;
            this.textBox233.Name = "textBox233";
            this.textBox233.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.textBox233.Tag = "";
            this.textBox233.Text = "ITEM058";
            this.textBox233.Top = 3.150394F;
            this.textBox233.Width = 0.9055118F;
            // 
            // textBox234
            // 
            this.textBox234.DataField = "ITEM056";
            this.textBox234.Height = 0.1377953F;
            this.textBox234.Left = 9.470079F;
            this.textBox234.Name = "textBox234";
            this.textBox234.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.textBox234.Tag = "";
            this.textBox234.Text = "ITEM056";
            this.textBox234.Top = 2.992914F;
            this.textBox234.Width = 0.779134F;
            // 
            // textBox235
            // 
            this.textBox235.DataField = "ITEM059";
            this.textBox235.Height = 0.1377953F;
            this.textBox235.Left = 9.470079F;
            this.textBox235.Name = "textBox235";
            this.textBox235.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.textBox235.Tag = "";
            this.textBox235.Text = "ITEM059";
            this.textBox235.Top = 3.150394F;
            this.textBox235.Width = 0.779134F;
            // 
            // textBox236
            // 
            this.textBox236.DataField = "ITEM057";
            this.textBox236.Height = 0.1377953F;
            this.textBox236.Left = 10.2752F;
            this.textBox236.Name = "textBox236";
            this.textBox236.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.textBox236.Tag = "";
            this.textBox236.Text = "ITEM057";
            this.textBox236.Top = 2.992914F;
            this.textBox236.Width = 0.8181104F;
            // 
            // textBox237
            // 
            this.textBox237.DataField = "ITEM060";
            this.textBox237.Height = 0.1377953F;
            this.textBox237.Left = 10.2752F;
            this.textBox237.Name = "textBox237";
            this.textBox237.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.textBox237.Tag = "";
            this.textBox237.Text = "ITEM060";
            this.textBox237.Top = 3.150394F;
            this.textBox237.Width = 0.8181104F;
            // 
            // textBox238
            // 
            this.textBox238.DataField = "ITEM144";
            this.textBox238.Height = 0.1104167F;
            this.textBox238.Left = 8.794094F;
            this.textBox238.Name = "textBox238";
            this.textBox238.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6.5pt; font" +
    "-weight: normal; text-align: left; ddo-char-set: 1";
            this.textBox238.Tag = "";
            this.textBox238.Text = "ITEM144";
            this.textBox238.Top = 3.444095F;
            this.textBox238.Width = 2.283465F;
            // 
            // textBox239
            // 
            this.textBox239.DataField = "ITEM143";
            this.textBox239.Height = 0.1104167F;
            this.textBox239.Left = 8.780708F;
            this.textBox239.Name = "textBox239";
            this.textBox239.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6.5pt; font" +
    "-weight: normal; text-align: left; ddo-char-set: 1";
            this.textBox239.Tag = "";
            this.textBox239.Text = "ITEM143";
            this.textBox239.Top = 3.307875F;
            this.textBox239.Width = 2.283465F;
            // 
            // label41
            // 
            this.label41.Height = 0.08611111F;
            this.label41.HyperLink = null;
            this.label41.Left = 5.854828F;
            this.label41.Name = "label41";
            this.label41.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.label41.Tag = "";
            this.label41.Text = "内　　　　　　　　　　　　　　　 　円";
            this.label41.Top = 2.517711F;
            this.label41.Width = 1.335313F;
            // 
            // textBox241
            // 
            this.textBox241.DataField = "ITEM027";
            this.textBox241.Height = 0.1023622F;
            this.textBox241.Left = 9.509054F;
            this.textBox241.Name = "textBox241";
            this.textBox241.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.textBox241.Tag = "";
            this.textBox241.Text = "ITEM027";
            this.textBox241.Top = 0.5519687F;
            this.textBox241.Width = 1.6F;
            // 
            // line135
            // 
            this.line135.Height = 0.1350393F;
            this.line135.Left = 9.593307F;
            this.line135.LineWeight = 1F;
            this.line135.Name = "line135";
            this.line135.Top = 0.3964571F;
            this.line135.Width = 0F;
            this.line135.X1 = 9.593307F;
            this.line135.X2 = 9.593307F;
            this.line135.Y1 = 0.5314964F;
            this.line135.Y2 = 0.3964571F;
            // 
            // textBox242
            // 
            this.textBox242.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox242.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox242.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox242.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox242.DataField = "ITEM068";
            this.textBox242.Height = 0.2082678F;
            this.textBox242.Left = 9.509056F;
            this.textBox242.Name = "textBox242";
            this.textBox242.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox242.Tag = "";
            this.textBox242.Text = "ITEM068";
            this.textBox242.Top = 1.970473F;
            this.textBox242.Width = 0.4854331F;
            // 
            // textBox243
            // 
            this.textBox243.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox243.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox243.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox243.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox243.DataField = "ITEM042";
            this.textBox243.Height = 0.203937F;
            this.textBox243.Left = 9.994489F;
            this.textBox243.Name = "textBox243";
            this.textBox243.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox243.Tag = "";
            this.textBox243.Text = "ITEM042";
            this.textBox243.Top = 1.977953F;
            this.textBox243.Width = 0.2110233F;
            // 
            // label42
            // 
            this.label42.Height = 0.08611111F;
            this.label42.HyperLink = null;
            this.label42.Left = 10.01653F;
            this.label42.Name = "label42";
            this.label42.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.label42.Tag = "";
            this.label42.Text = "内人";
            this.label42.Top = 1.974804F;
            this.label42.Width = 0.3857827F;
            // 
            // textBox244
            // 
            this.textBox244.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox244.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox244.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox244.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox244.Height = 0.3232284F;
            this.textBox244.Left = 8.548424F;
            this.textBox244.Name = "textBox244";
            this.textBox244.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox244.Tag = "";
            this.textBox244.Text = "地震保険料の控除額";
            this.textBox244.Top = 2.182678F;
            this.textBox244.Width = 1.356693F;
            // 
            // textBox245
            // 
            this.textBox245.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox245.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox245.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox245.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox245.Height = 0.3232284F;
            this.textBox245.Left = 7.185828F;
            this.textBox245.Name = "textBox245";
            this.textBox245.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox245.Tag = "";
            this.textBox245.Text = "生命保険料の 控 除 額";
            this.textBox245.Top = 2.182678F;
            this.textBox245.Width = 1.362598F;
            // 
            // textBox246
            // 
            this.textBox246.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox246.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox246.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox246.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox246.Height = 0.32126F;
            this.textBox246.Left = 9.897638F;
            this.textBox246.Name = "textBox246";
            this.textBox246.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox246.Tag = "";
            this.textBox246.Text = "住宅借入金等特別控除の額";
            this.textBox246.Top = 2.184645F;
            this.textBox246.Width = 1.349607F;
            // 
            // textBox247
            // 
            this.textBox247.Height = 0.2362202F;
            this.textBox247.Left = 6.170867F;
            this.textBox247.Name = "textBox247";
            this.textBox247.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox247.Tag = "";
            this.textBox247.Text = "氏名";
            this.textBox247.Top = 4.168898F;
            this.textBox247.Width = 0.475197F;
            // 
            // textBox248
            // 
            this.textBox248.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox248.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox248.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox248.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox248.Height = 0.1559055F;
            this.textBox248.Left = 6.172048F;
            this.textBox248.Name = "textBox248";
            this.textBox248.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox248.Tag = "";
            this.textBox248.Text = "個人番号";
            this.textBox248.Top = 4.409053F;
            this.textBox248.Width = 0.4740157F;
            // 
            // textBox250
            // 
            this.textBox250.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox250.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox250.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox250.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox250.CanGrow = false;
            this.textBox250.DataField = "ITEM064";
            this.textBox250.Height = 0.215748F;
            this.textBox250.Left = 8.794095F;
            this.textBox250.Name = "textBox250";
            this.textBox250.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox250.Tag = "";
            this.textBox250.Text = "ITEM064";
            this.textBox250.Top = 3.623229F;
            this.textBox250.Width = 0.4937007F;
            // 
            // textBox251
            // 
            this.textBox251.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox251.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox251.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox251.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox251.Height = 0.215748F;
            this.textBox251.Left = 8.300396F;
            this.textBox251.Name = "textBox251";
            this.textBox251.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 4.6pt; font" +
    "-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox251.Tag = "";
            this.textBox251.Text = "介護医療保険料\r\nの金額";
            this.textBox251.Top = 3.623229F;
            this.textBox251.Width = 0.4937007F;
            // 
            // textBox252
            // 
            this.textBox252.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox252.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox252.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox252.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox252.CanGrow = false;
            this.textBox252.DataField = "ITEM065";
            this.textBox252.Height = 0.215748F;
            this.textBox252.Left = 9.781498F;
            this.textBox252.Name = "textBox252";
            this.textBox252.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox252.Tag = "";
            this.textBox252.Text = "ITEM065";
            this.textBox252.Top = 3.623229F;
            this.textBox252.Width = 0.4937007F;
            // 
            // textBox253
            // 
            this.textBox253.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox253.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox253.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox253.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox253.Height = 0.215748F;
            this.textBox253.Left = 9.287796F;
            this.textBox253.Name = "textBox253";
            this.textBox253.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 4.7pt; font" +
    "-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox253.Tag = "";
            this.textBox253.Text = "新個人年金保険料の金額";
            this.textBox253.Top = 3.623229F;
            this.textBox253.Width = 0.4937007F;
            // 
            // textBox254
            // 
            this.textBox254.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox254.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox254.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox254.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox254.CanGrow = false;
            this.textBox254.DataField = "ITEM066";
            this.textBox254.Height = 0.215748F;
            this.textBox254.Left = 10.72795F;
            this.textBox254.Name = "textBox254";
            this.textBox254.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox254.Tag = "";
            this.textBox254.Text = "ITEM066";
            this.textBox254.Top = 3.62441F;
            this.textBox254.Width = 0.5192914F;
            // 
            // textBox255
            // 
            this.textBox255.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox255.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox255.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox255.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox255.Height = 0.215748F;
            this.textBox255.Left = 10.2626F;
            this.textBox255.Name = "textBox255";
            this.textBox255.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 4.7pt; font" +
    "-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox255.Tag = "";
            this.textBox255.Text = "旧個人年金\r\n保険料の金額";
            this.textBox255.Top = 3.62441F;
            this.textBox255.Width = 0.4681098F;
            // 
            // textBox256
            // 
            this.textBox256.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox256.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox256.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox256.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox256.CanGrow = false;
            this.textBox256.DataField = "ITEM062";
            this.textBox256.Height = 0.215748F;
            this.textBox256.Left = 6.818112F;
            this.textBox256.Name = "textBox256";
            this.textBox256.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox256.Tag = "";
            this.textBox256.Text = "ITEM062";
            this.textBox256.Top = 3.623229F;
            this.textBox256.Width = 0.4937007F;
            // 
            // textBox327
            // 
            this.textBox327.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox327.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox327.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox327.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox327.Height = 0.215748F;
            this.textBox327.Left = 6.32441F;
            this.textBox327.Name = "textBox327";
            this.textBox327.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 4.7pt; font" +
    "-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox327.Tag = "";
            this.textBox327.Text = "新生命保険料\r\nの金額\r\n";
            this.textBox327.Top = 3.623229F;
            this.textBox327.Width = 0.4937007F;
            // 
            // textBox328
            // 
            this.textBox328.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox328.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox328.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox328.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox328.CanGrow = false;
            this.textBox328.DataField = "ITEM063";
            this.textBox328.Height = 0.215748F;
            this.textBox328.Left = 7.805513F;
            this.textBox328.Name = "textBox328";
            this.textBox328.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox328.Tag = "";
            this.textBox328.Text = "ITEM063";
            this.textBox328.Top = 3.623229F;
            this.textBox328.Width = 0.4937007F;
            // 
            // textBox329
            // 
            this.textBox329.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox329.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox329.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox329.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox329.Height = 0.215748F;
            this.textBox329.Left = 7.311813F;
            this.textBox329.Name = "textBox329";
            this.textBox329.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 4.7pt; font" +
    "-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox329.Tag = "";
            this.textBox329.Text = "旧生命保険料\r\nの金額";
            this.textBox329.Top = 3.623229F;
            this.textBox329.Width = 0.4937007F;
            // 
            // label43
            // 
            this.label43.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label43.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label43.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label43.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label43.Height = 0.1555118F;
            this.label43.HyperLink = null;
            this.label43.Left = 6.32441F;
            this.label43.Name = "label43";
            this.label43.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 4.6pt; font-weight: normal; text-ali" +
    "gn: center; vertical-align: middle; ddo-char-set: 1";
            this.label43.Tag = "";
            this.label43.Text = "住宅借入金等\r\n特別控除適用数";
            this.label43.Top = 3.840158F;
            this.label43.Width = 0.4937009F;
            // 
            // textBox330
            // 
            this.textBox330.DataField = "ITEM050";
            this.textBox330.Height = 0.1401575F;
            this.textBox330.Left = 6.815355F;
            this.textBox330.Name = "textBox330";
            this.textBox330.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox330.Tag = "";
            this.textBox330.Text = "ITEM050";
            this.textBox330.Top = 4.011418F;
            this.textBox330.Width = 0.4964566F;
            // 
            // label64
            // 
            this.label64.Height = 0.1185041F;
            this.label64.HyperLink = null;
            this.label64.Left = 9.121273F;
            this.label64.Name = "label64";
            this.label64.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label64.Tag = "";
            this.label64.Text = "円";
            this.label64.Top = 4.15264F;
            this.label64.Width = 0.1666665F;
            // 
            // label65
            // 
            this.label65.Height = 0.08611111F;
            this.label65.HyperLink = null;
            this.label65.Left = 7.126773F;
            this.label65.Name = "label65";
            this.label65.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label65.Tag = "";
            this.label65.Text = "円";
            this.label65.Top = 3.627165F;
            this.label65.Width = 0.1666665F;
            // 
            // label66
            // 
            this.label66.Height = 0.0889991F;
            this.label66.HyperLink = null;
            this.label66.Left = 8.11693F;
            this.label66.Name = "label66";
            this.label66.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label66.Tag = "";
            this.label66.Text = "円";
            this.label66.Top = 3.627166F;
            this.label66.Width = 0.1666665F;
            // 
            // label67
            // 
            this.label67.Height = 0.08611111F;
            this.label67.HyperLink = null;
            this.label67.Left = 10.09606F;
            this.label67.Name = "label67";
            this.label67.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label67.Tag = "";
            this.label67.Text = "円";
            this.label67.Top = 3.629922F;
            this.label67.Width = 0.166667F;
            // 
            // label68
            // 
            this.label68.Height = 0.08611111F;
            this.label68.HyperLink = null;
            this.label68.Left = 11.08071F;
            this.label68.Name = "label68";
            this.label68.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label68.Tag = "";
            this.label68.Text = "円";
            this.label68.Top = 3.629922F;
            this.label68.Width = 0.166667F;
            // 
            // label69
            // 
            this.label69.Height = 0.08611111F;
            this.label69.HyperLink = null;
            this.label69.Left = 9.090552F;
            this.label69.Name = "label69";
            this.label69.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label69.Tag = "";
            this.label69.Text = "円";
            this.label69.Top = 3.629922F;
            this.label69.Width = 0.166667F;
            // 
            // textBox331
            // 
            this.textBox331.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox331.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox331.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox331.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox331.Height = 0.215748F;
            this.textBox331.Left = 5.827166F;
            this.textBox331.Name = "textBox331";
            this.textBox331.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 4.5pt; font" +
    "-weight: normal; text-align: center; ddo-char-set: 1";
            this.textBox331.Tag = "";
            this.textBox331.Text = "生命保険料\r\nの金額の内\r\n訳";
            this.textBox331.Top = 3.623229F;
            this.textBox331.Width = 0.4972446F;
            // 
            // textBox332
            // 
            this.textBox332.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox332.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox332.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox332.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox332.Height = 0.3125986F;
            this.textBox332.Left = 5.827166F;
            this.textBox332.Name = "textBox332";
            this.textBox332.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox332.Tag = "";
            this.textBox332.Text = "住宅借入\r\n金等特別\r\n控除の額\r\nの内訳";
            this.textBox332.Top = 3.838977F;
            this.textBox332.Width = 0.4972441F;
            // 
            // label70
            // 
            this.label70.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label70.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label70.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label70.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label70.Height = 0.1555118F;
            this.label70.HyperLink = null;
            this.label70.Left = 6.32441F;
            this.label70.Name = "label70";
            this.label70.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 4.6pt; font-weight: normal; text-ali" +
    "gn: center; vertical-align: middle; ddo-char-set: 1";
            this.label70.Tag = "";
            this.label70.Text = "住宅借入金等\r\n特別控除可能額";
            this.label70.Top = 3.996062F;
            this.label70.Width = 0.4937011F;
            // 
            // label71
            // 
            this.label71.Height = 0.08611111F;
            this.label71.HyperLink = null;
            this.label71.Left = 7.145276F;
            this.label71.Name = "label71";
            this.label71.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label71.Tag = "";
            this.label71.Text = "円";
            this.label71.Top = 3.995669F;
            this.label71.Width = 0.1666665F;
            // 
            // label72
            // 
            this.label72.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label72.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label72.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label72.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label72.Height = 0.1555118F;
            this.label72.HyperLink = null;
            this.label72.Left = 7.311813F;
            this.label72.Name = "label72";
            this.label72.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 4.6pt; font-weight: normal; text-ali" +
    "gn: center; vertical-align: middle; ddo-char-set: 1";
            this.label72.Tag = "";
            this.label72.Text = "住居開始年月日\r\n(1回目)";
            this.label72.Top = 3.840158F;
            this.label72.Width = 0.4937009F;
            // 
            // label73
            // 
            this.label73.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label73.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label73.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label73.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label73.Height = 0.1555118F;
            this.label73.HyperLink = null;
            this.label73.Left = 7.311813F;
            this.label73.Name = "label73";
            this.label73.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 4.6pt; font-weight: normal; text-ali" +
    "gn: center; vertical-align: middle; ddo-char-set: 1";
            this.label73.Tag = "";
            this.label73.Text = "住居開始年月日\r\n(2回目)";
            this.label73.Top = 3.996062F;
            this.label73.Width = 0.4937009F;
            // 
            // label74
            // 
            this.label74.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label74.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label74.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label74.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label74.Height = 0.1555118F;
            this.label74.HyperLink = null;
            this.label74.Left = 8.794095F;
            this.label74.Name = "label74";
            this.label74.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 4.6pt; font-weight: normal; text-ali" +
    "gn: center; vertical-align: middle; ddo-char-set: 1";
            this.label74.Tag = "";
            this.label74.Text = "住宅借入金等特別控除区分(1回目)";
            this.label74.Top = 3.840551F;
            this.label74.Width = 0.5866145F;
            // 
            // label75
            // 
            this.label75.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label75.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label75.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label75.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label75.Height = 0.1555118F;
            this.label75.HyperLink = null;
            this.label75.Left = 8.794095F;
            this.label75.Name = "label75";
            this.label75.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 4.6pt; font-weight: normal; text-ali" +
    "gn: center; vertical-align: middle; ddo-char-set: 1";
            this.label75.Tag = "";
            this.label75.Text = "住宅借入金等特別控除区分(2回目)";
            this.label75.Top = 3.996062F;
            this.label75.Width = 0.5866145F;
            // 
            // label76
            // 
            this.label76.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label76.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label76.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label76.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label76.Height = 0.1555118F;
            this.label76.HyperLink = null;
            this.label76.Left = 9.90512F;
            this.label76.Name = "label76";
            this.label76.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 4.6pt; font-weight: normal; text-ali" +
    "gn: center; vertical-align: middle; ddo-char-set: 1";
            this.label76.Tag = "";
            this.label76.Text = "住宅借入金等\r\n年末残高(1回目)";
            this.label76.Top = 3.840551F;
            this.label76.Width = 0.5763763F;
            // 
            // label77
            // 
            this.label77.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label77.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label77.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label77.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label77.Height = 0.1555118F;
            this.label77.HyperLink = null;
            this.label77.Left = 9.90512F;
            this.label77.Name = "label77";
            this.label77.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 4.6pt; font-weight: normal; text-ali" +
    "gn: center; vertical-align: middle; ddo-char-set: 1";
            this.label77.Tag = "";
            this.label77.Text = "住宅借入金等\r\n年末残高(2回目)";
            this.label77.Top = 3.996061F;
            this.label77.Width = 0.5763763F;
            // 
            // textBox333
            // 
            this.textBox333.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox333.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox333.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox333.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox333.DataField = "ITEM052";
            this.textBox333.Height = 0.1555117F;
            this.textBox333.Left = 7.805906F;
            this.textBox333.Name = "textBox333";
            this.textBox333.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox333.Tag = "";
            this.textBox333.Text = "ITEM052";
            this.textBox333.Top = 3.840551F;
            this.textBox333.Width = 0.3295276F;
            // 
            // label78
            // 
            this.label78.Height = 0.08611111F;
            this.label78.HyperLink = null;
            this.label78.Left = 8.022442F;
            this.label78.Name = "label78";
            this.label78.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label78.Tag = "";
            this.label78.Text = "年";
            this.label78.Top = 3.840551F;
            this.label78.Width = 0.09461923F;
            // 
            // textBox334
            // 
            this.textBox334.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox334.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox334.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox334.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox334.DataField = "ITEM139";
            this.textBox334.Height = 0.1555117F;
            this.textBox334.Left = 8.140552F;
            this.textBox334.Name = "textBox334";
            this.textBox334.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox334.Tag = "";
            this.textBox334.Text = "ITEM139";
            this.textBox334.Top = 3.840551F;
            this.textBox334.Width = 0.3295276F;
            // 
            // label79
            // 
            this.label79.Height = 0.08611111F;
            this.label79.HyperLink = null;
            this.label79.Left = 8.357087F;
            this.label79.Name = "label79";
            this.label79.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label79.Tag = "";
            this.label79.Text = "月";
            this.label79.Top = 3.840551F;
            this.label79.Width = 0.0946193F;
            // 
            // textBox335
            // 
            this.textBox335.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox335.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox335.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox335.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox335.DataField = "ITEM140";
            this.textBox335.Height = 0.1555117F;
            this.textBox335.Left = 8.464568F;
            this.textBox335.Name = "textBox335";
            this.textBox335.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox335.Tag = "";
            this.textBox335.Text = "ITEM140";
            this.textBox335.Top = 3.840551F;
            this.textBox335.Width = 0.3295276F;
            // 
            // label80
            // 
            this.label80.Height = 0.08611111F;
            this.label80.HyperLink = null;
            this.label80.Left = 8.686615F;
            this.label80.Name = "label80";
            this.label80.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label80.Tag = "";
            this.label80.Text = "日";
            this.label80.Top = 3.840551F;
            this.label80.Width = 0.0946193F;
            // 
            // textBox336
            // 
            this.textBox336.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox336.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox336.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox336.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox336.DataField = "ITEM145";
            this.textBox336.Height = 0.1555117F;
            this.textBox336.Left = 7.805513F;
            this.textBox336.Name = "textBox336";
            this.textBox336.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox336.Tag = "";
            this.textBox336.Text = "ITEM145";
            this.textBox336.Top = 3.998031F;
            this.textBox336.Width = 0.3295276F;
            // 
            // label81
            // 
            this.label81.Height = 0.08611111F;
            this.label81.HyperLink = null;
            this.label81.Left = 8.022048F;
            this.label81.Name = "label81";
            this.label81.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label81.Tag = "";
            this.label81.Text = "年";
            this.label81.Top = 3.998031F;
            this.label81.Width = 0.0946193F;
            // 
            // textBox337
            // 
            this.textBox337.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox337.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox337.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox337.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox337.DataField = "ITEM146";
            this.textBox337.Height = 0.1555117F;
            this.textBox337.Left = 8.140159F;
            this.textBox337.Name = "textBox337";
            this.textBox337.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox337.Tag = "";
            this.textBox337.Text = "ITEM146";
            this.textBox337.Top = 3.998031F;
            this.textBox337.Width = 0.3295276F;
            // 
            // label82
            // 
            this.label82.Height = 0.08611111F;
            this.label82.HyperLink = null;
            this.label82.Left = 8.356693F;
            this.label82.Name = "label82";
            this.label82.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label82.Tag = "";
            this.label82.Text = "月";
            this.label82.Top = 3.998031F;
            this.label82.Width = 0.0946193F;
            // 
            // textBox338
            // 
            this.textBox338.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox338.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox338.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox338.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox338.DataField = "ITEM147";
            this.textBox338.Height = 0.1555117F;
            this.textBox338.Left = 8.464173F;
            this.textBox338.Name = "textBox338";
            this.textBox338.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox338.Tag = "";
            this.textBox338.Text = "ITEM147";
            this.textBox338.Top = 3.998031F;
            this.textBox338.Width = 0.3295276F;
            // 
            // label84
            // 
            this.label84.Height = 0.08611111F;
            this.label84.HyperLink = null;
            this.label84.Left = 8.686221F;
            this.label84.Name = "label84";
            this.label84.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label84.Tag = "";
            this.label84.Text = "日";
            this.label84.Top = 3.998031F;
            this.label84.Width = 0.0946193F;
            // 
            // textBox339
            // 
            this.textBox339.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox339.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox339.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox339.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox339.DataField = "ITEM152";
            this.textBox339.Height = 0.1555118F;
            this.textBox339.Left = 6.818111F;
            this.textBox339.Name = "textBox339";
            this.textBox339.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: right; vertical-align: bottom; ddo-char-set: 1";
            this.textBox339.Tag = "";
            this.textBox339.Text = "ITEM152";
            this.textBox339.Top = 3.840551F;
            this.textBox339.Width = 0.4937007F;
            // 
            // textBox340
            // 
            this.textBox340.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox340.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox340.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox340.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox340.DataField = "ITEM148";
            this.textBox340.Height = 0.1555118F;
            this.textBox340.Left = 9.38071F;
            this.textBox340.Name = "textBox340";
            this.textBox340.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox340.Tag = "";
            this.textBox340.Text = "ITEM148";
            this.textBox340.Top = 3.840158F;
            this.textBox340.Width = 0.5244095F;
            // 
            // textBox341
            // 
            this.textBox341.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox341.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox341.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox341.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox341.DataField = "ITEM149";
            this.textBox341.Height = 0.1555118F;
            this.textBox341.Left = 9.38071F;
            this.textBox341.Name = "textBox341";
            this.textBox341.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox341.Tag = "";
            this.textBox341.Text = "ITEM149";
            this.textBox341.Top = 3.996063F;
            this.textBox341.Width = 0.5244095F;
            // 
            // textBox342
            // 
            this.textBox342.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox342.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox342.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox342.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox342.DataField = "ITEM150";
            this.textBox342.Height = 0.1555118F;
            this.textBox342.Left = 10.4815F;
            this.textBox342.Name = "textBox342";
            this.textBox342.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox342.Tag = "";
            this.textBox342.Text = "ITEM150";
            this.textBox342.Top = 3.838976F;
            this.textBox342.Width = 0.7657486F;
            // 
            // textBox343
            // 
            this.textBox343.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox343.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox343.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox343.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox343.DataField = "ITEM151";
            this.textBox343.Height = 0.1555118F;
            this.textBox343.Left = 10.4815F;
            this.textBox343.Name = "textBox343";
            this.textBox343.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox343.Tag = "";
            this.textBox343.Text = "ITEM151";
            this.textBox343.Top = 3.996063F;
            this.textBox343.Width = 0.7657486F;
            // 
            // textBox344
            // 
            this.textBox344.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox344.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox344.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox344.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox344.Height = 0.2578743F;
            this.textBox344.Left = 7.904332F;
            this.textBox344.Name = "textBox344";
            this.textBox344.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox344.Tag = "";
            this.textBox344.Text = "区分";
            this.textBox344.Top = 4.151576F;
            this.textBox344.Width = 0.1181102F;
            // 
            // textBox345
            // 
            this.textBox345.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox345.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox345.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox345.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox345.DataField = "ITEM104";
            this.textBox345.Height = 0.2578741F;
            this.textBox345.Left = 8.022442F;
            this.textBox345.Name = "textBox345";
            this.textBox345.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox345.Tag = "";
            this.textBox345.Text = "ITEM104";
            this.textBox345.Top = 4.151576F;
            this.textBox345.Width = 0.2779529F;
            // 
            // textBox347
            // 
            this.textBox347.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox347.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox347.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox347.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox347.CanGrow = false;
            this.textBox347.DataField = "ITEM061";
            this.textBox347.Height = 0.4118109F;
            this.textBox347.Left = 8.794095F;
            this.textBox347.Name = "textBox347";
            this.textBox347.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox347.Tag = "";
            this.textBox347.Text = "ITEM061";
            this.textBox347.Top = 4.152754F;
            this.textBox347.Width = 0.4937007F;
            // 
            // label85
            // 
            this.label85.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label85.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label85.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label85.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label85.Height = 0.411811F;
            this.label85.HyperLink = null;
            this.label85.Left = 9.287796F;
            this.label85.Name = "label85";
            this.label85.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 4.7pt; font-weight: normal; text-ali" +
    "gn: center; vertical-align: middle; ddo-char-set: 1";
            this.label85.Tag = "";
            this.label85.Text = "国民年金保険\r\n料等の金額";
            this.label85.Top = 4.151575F;
            this.label85.Width = 0.4937007F;
            // 
            // textBox348
            // 
            this.textBox348.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox348.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox348.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox348.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox348.CanGrow = false;
            this.textBox348.DataField = "ITEM051";
            this.textBox348.Height = 0.411811F;
            this.textBox348.Left = 9.781497F;
            this.textBox348.Name = "textBox348";
            this.textBox348.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox348.Tag = "";
            this.textBox348.Text = "ITEM051";
            this.textBox348.Top = 4.151575F;
            this.textBox348.Width = 0.4937007F;
            // 
            // textBox349
            // 
            this.textBox349.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox349.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox349.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox349.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox349.Height = 0.411811F;
            this.textBox349.Left = 10.2752F;
            this.textBox349.Name = "textBox349";
            this.textBox349.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 4.7pt; font" +
    "-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox349.Tag = "";
            this.textBox349.Text = "旧長期損害保険料の金額";
            this.textBox349.Top = 4.151576F;
            this.textBox349.Width = 0.4649604F;
            // 
            // label86
            // 
            this.label86.Height = 0.08611111F;
            this.label86.HyperLink = null;
            this.label86.Left = 11.07047F;
            this.label86.Name = "label86";
            this.label86.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label86.Tag = "";
            this.label86.Text = "円";
            this.label86.Top = 4.16378F;
            this.label86.Width = 0.166667F;
            // 
            // textBox351
            // 
            this.textBox351.Height = 0.1299212F;
            this.textBox351.Left = 6.170867F;
            this.textBox351.Name = "textBox351";
            this.textBox351.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5.6pt; font" +
    "-weight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox351.Tag = "";
            this.textBox351.Text = "(フリガナ)";
            this.textBox351.Top = 4.16378F;
            this.textBox351.Width = 0.475197F;
            // 
            // textBox353
            // 
            this.textBox353.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox353.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox353.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox353.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox353.DataField = "ITEM103";
            this.textBox353.Height = 0.2578741F;
            this.textBox353.Left = 6.646064F;
            this.textBox353.Name = "textBox353";
            this.textBox353.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5.5pt; font" +
    "-weight: normal; text-align: left; vertical-align: bottom; ddo-char-set: 1";
            this.textBox353.Tag = "";
            this.textBox353.Text = "ITEM103";
            this.textBox353.Top = 4.151576F;
            this.textBox353.Width = 1.258268F;
            // 
            // textBox354
            // 
            this.textBox354.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox354.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox354.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox354.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox354.DataField = "ITEM102";
            this.textBox354.Height = 0.09645708F;
            this.textBox354.Left = 6.646064F;
            this.textBox354.Name = "textBox354";
            this.textBox354.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5.5pt; font" +
    "-weight: normal; text-align: left; vertical-align: top; ddo-char-set: 1";
            this.textBox354.Tag = "";
            this.textBox354.Text = "ITEM102item[101]";
            this.textBox354.Top = 4.153543F;
            this.textBox354.Width = 1.258268F;
            // 
            // label87
            // 
            this.label87.Height = 0.08611111F;
            this.label87.HyperLink = null;
            this.label87.Left = 10.09606F;
            this.label87.Name = "label87";
            this.label87.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label87.Tag = "";
            this.label87.Text = "円";
            this.label87.Top = 4.168898F;
            this.label87.Width = 0.166667F;
            // 
            // label88
            // 
            this.label88.Height = 0.08611111F;
            this.label88.HyperLink = null;
            this.label88.Left = 9.090552F;
            this.label88.Name = "label88";
            this.label88.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label88.Tag = "";
            this.label88.Text = "円";
            this.label88.Top = 4.168898F;
            this.label88.Width = 0.166667F;
            // 
            // textBox355
            // 
            this.textBox355.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox355.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox355.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox355.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox355.Height = 1.651969F;
            this.textBox355.Left = 5.827167F;
            this.textBox355.Name = "textBox355";
            this.textBox355.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1; ddo-" +
    "font-vertical: true";
            this.textBox355.Tag = "";
            this.textBox355.Text = "控除対象\r\n扶養親族";
            this.textBox355.Top = 4.567322F;
            this.textBox355.Width = 0.1874007F;
            // 
            // textBox356
            // 
            this.textBox356.Height = 0.2362205F;
            this.textBox356.Left = 6.170867F;
            this.textBox356.Name = "textBox356";
            this.textBox356.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox356.Tag = "";
            this.textBox356.Text = "氏名";
            this.textBox356.Top = 4.983071F;
            this.textBox356.Width = 0.475197F;
            // 
            // textBox358
            // 
            this.textBox358.Height = 0.1299212F;
            this.textBox358.Left = 6.170867F;
            this.textBox358.Name = "textBox358";
            this.textBox358.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5.6pt; font" +
    "-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox358.Tag = "";
            this.textBox358.Text = "(フリガナ)";
            this.textBox358.Top = 4.979527F;
            this.textBox358.Width = 0.475197F;
            // 
            // textBox359
            // 
            this.textBox359.Height = 0.1590554F;
            this.textBox359.Left = 6.172048F;
            this.textBox359.Name = "textBox359";
            this.textBox359.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox359.Tag = "";
            this.textBox359.Text = "氏名";
            this.textBox359.Top = 4.663386F;
            this.textBox359.Width = 0.4740159F;
            // 
            // textBox360
            // 
            this.textBox360.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox360.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox360.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox360.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox360.Height = 0.1559055F;
            this.textBox360.Left = 6.172048F;
            this.textBox360.Name = "textBox360";
            this.textBox360.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox360.Tag = "";
            this.textBox360.Text = "個人番号";
            this.textBox360.Top = 4.826378F;
            this.textBox360.Width = 0.4740157F;
            // 
            // textBox362
            // 
            this.textBox362.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox362.Height = 0.1299212F;
            this.textBox362.Left = 6.175197F;
            this.textBox362.Name = "textBox362";
            this.textBox362.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5.6pt; font" +
    "-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox362.Tag = "";
            this.textBox362.Text = "(フリガナ)";
            this.textBox362.Top = 4.56811F;
            this.textBox362.Width = 0.4740159F;
            // 
            // textBox363
            // 
            this.textBox363.Height = 0.2362205F;
            this.textBox363.Left = 6.170867F;
            this.textBox363.Name = "textBox363";
            this.textBox363.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox363.Tag = "";
            this.textBox363.Text = "氏名";
            this.textBox363.Top = 5.418898F;
            this.textBox363.Width = 0.475197F;
            // 
            // textBox365
            // 
            this.textBox365.Height = 0.1220472F;
            this.textBox365.Left = 6.170867F;
            this.textBox365.Name = "textBox365";
            this.textBox365.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5.6pt; font" +
    "-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox365.Tag = "";
            this.textBox365.Text = "(フリガナ)";
            this.textBox365.Top = 5.397639F;
            this.textBox365.Width = 0.475197F;
            // 
            // textBox366
            // 
            this.textBox366.Height = 0.2208658F;
            this.textBox366.Left = 6.170867F;
            this.textBox366.Name = "textBox366";
            this.textBox366.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox366.Tag = "";
            this.textBox366.Text = "氏名";
            this.textBox366.Top = 5.828741F;
            this.textBox366.Width = 0.475197F;
            // 
            // textBox367
            // 
            this.textBox367.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox367.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox367.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox367.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox367.Height = 0.1559055F;
            this.textBox367.Left = 6.170867F;
            this.textBox367.Name = "textBox367";
            this.textBox367.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox367.Tag = "";
            this.textBox367.Text = "個人番号";
            this.textBox367.Top = 6.061024F;
            this.textBox367.Width = 0.4751968F;
            // 
            // textBox369
            // 
            this.textBox369.Height = 0.1299212F;
            this.textBox369.Left = 6.170867F;
            this.textBox369.Name = "textBox369";
            this.textBox369.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5.6pt; font" +
    "-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox369.Tag = "";
            this.textBox369.Text = "(フリガナ)";
            this.textBox369.Top = 5.80748F;
            this.textBox369.Width = 0.475197F;
            // 
            // textBox370
            // 
            this.textBox370.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox370.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox370.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox370.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox370.Height = 1.651575F;
            this.textBox370.Left = 8.300395F;
            this.textBox370.Name = "textBox370";
            this.textBox370.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1; ddo-" +
    "font-vertical: true";
            this.textBox370.Tag = "";
            this.textBox370.Text = "1\r\n6歳未満の扶養親族";
            this.textBox370.Top = 4.567717F;
            this.textBox370.Width = 0.1874008F;
            // 
            // textBox371
            // 
            this.textBox371.Height = 0.2381894F;
            this.textBox371.Left = 8.644095F;
            this.textBox371.Name = "textBox371";
            this.textBox371.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox371.Tag = "";
            this.textBox371.Text = "氏名";
            this.textBox371.Top = 5.002363F;
            this.textBox371.Width = 0.4751969F;
            // 
            // textBox373
            // 
            this.textBox373.Height = 0.09842521F;
            this.textBox373.Left = 8.644095F;
            this.textBox373.Name = "textBox373";
            this.textBox373.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5.6pt; font" +
    "-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox373.Tag = "";
            this.textBox373.Text = "(フリガナ)";
            this.textBox373.Top = 5.002363F;
            this.textBox373.Width = 0.4751967F;
            // 
            // textBox374
            // 
            this.textBox374.Height = 0.2381894F;
            this.textBox374.Left = 8.644095F;
            this.textBox374.Name = "textBox374";
            this.textBox374.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox374.Tag = "";
            this.textBox374.Text = "氏名";
            this.textBox374.Top = 4.590156F;
            this.textBox374.Width = 0.4751969F;
            // 
            // textBox376
            // 
            this.textBox376.Height = 0.09842521F;
            this.textBox376.Left = 8.646064F;
            this.textBox376.Name = "textBox376";
            this.textBox376.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5.6pt; font" +
    "-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox376.Tag = "";
            this.textBox376.Text = "(フリガナ)";
            this.textBox376.Top = 4.590158F;
            this.textBox376.Width = 0.4751967F;
            // 
            // textBox377
            // 
            this.textBox377.Height = 0.2381894F;
            this.textBox377.Left = 8.644095F;
            this.textBox377.Name = "textBox377";
            this.textBox377.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox377.Tag = "";
            this.textBox377.Text = "氏名";
            this.textBox377.Top = 5.416929F;
            this.textBox377.Width = 0.4751969F;
            // 
            // textBox379
            // 
            this.textBox379.Height = 0.09842521F;
            this.textBox379.Left = 8.644095F;
            this.textBox379.Name = "textBox379";
            this.textBox379.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5.6pt; font" +
    "-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox379.Tag = "";
            this.textBox379.Text = "(フリガナ)";
            this.textBox379.Top = 5.416929F;
            this.textBox379.Width = 0.4751967F;
            // 
            // textBox380
            // 
            this.textBox380.Height = 0.2381894F;
            this.textBox380.Left = 8.644095F;
            this.textBox380.Name = "textBox380";
            this.textBox380.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox380.Tag = "";
            this.textBox380.Text = "氏名";
            this.textBox380.Top = 5.82874F;
            this.textBox380.Width = 0.4751969F;
            // 
            // textBox382
            // 
            this.textBox382.Height = 0.09842521F;
            this.textBox382.Left = 8.644095F;
            this.textBox382.Name = "textBox382";
            this.textBox382.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5.6pt; font" +
    "-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox382.Tag = "";
            this.textBox382.Text = "(フリガナ)";
            this.textBox382.Top = 5.824804F;
            this.textBox382.Width = 0.4751967F;
            // 
            // textBox383
            // 
            this.textBox383.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox383.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox383.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox383.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox383.Height = 0.2578742F;
            this.textBox383.Left = 7.904333F;
            this.textBox383.Name = "textBox383";
            this.textBox383.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox383.Tag = "";
            this.textBox383.Text = "区分";
            this.textBox383.Top = 4.567717F;
            this.textBox383.Width = 0.1181103F;
            // 
            // textBox384
            // 
            this.textBox384.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox384.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox384.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox384.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox384.DataField = "ITEM108";
            this.textBox384.Height = 0.2578741F;
            this.textBox384.Left = 8.022442F;
            this.textBox384.Name = "textBox384";
            this.textBox384.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox384.Tag = "";
            this.textBox384.Text = "ITEM108";
            this.textBox384.Top = 4.567717F;
            this.textBox384.Width = 0.2779529F;
            // 
            // textBox385
            // 
            this.textBox385.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox385.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox385.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox385.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox385.Height = 0.2559072F;
            this.textBox385.Left = 7.904333F;
            this.textBox385.Name = "textBox385";
            this.textBox385.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox385.Tag = "";
            this.textBox385.Text = "区分";
            this.textBox385.Top = 4.981102F;
            this.textBox385.Width = 0.1181103F;
            // 
            // textBox386
            // 
            this.textBox386.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox386.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox386.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox386.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox386.DataField = "ITEM112";
            this.textBox386.Height = 0.2578755F;
            this.textBox386.Left = 8.022442F;
            this.textBox386.Name = "textBox386";
            this.textBox386.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox386.Tag = "";
            this.textBox386.Text = "ITEM112";
            this.textBox386.Top = 4.979527F;
            this.textBox386.Width = 0.2779529F;
            // 
            // textBox387
            // 
            this.textBox387.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox387.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox387.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox387.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox387.Height = 0.2551181F;
            this.textBox387.Left = 7.904333F;
            this.textBox387.Name = "textBox387";
            this.textBox387.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox387.Tag = "";
            this.textBox387.Text = "区分";
            this.textBox387.Top = 5.396457F;
            this.textBox387.Width = 0.1181104F;
            // 
            // textBox388
            // 
            this.textBox388.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox388.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox388.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox388.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox388.DataField = "ITEM116";
            this.textBox388.Height = 0.2590548F;
            this.textBox388.Left = 8.022442F;
            this.textBox388.Name = "textBox388";
            this.textBox388.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox388.Tag = "";
            this.textBox388.Text = "ITEM116";
            this.textBox388.Top = 5.394095F;
            this.textBox388.Width = 0.2779529F;
            // 
            // textBox389
            // 
            this.textBox389.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox389.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox389.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox389.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox389.Height = 0.2559058F;
            this.textBox389.Left = 7.904333F;
            this.textBox389.Name = "textBox389";
            this.textBox389.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox389.Tag = "";
            this.textBox389.Text = "区分";
            this.textBox389.Top = 5.80748F;
            this.textBox389.Width = 0.1181103F;
            // 
            // textBox390
            // 
            this.textBox390.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox390.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox390.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox390.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox390.DataField = "ITEM120";
            this.textBox390.Height = 0.2559057F;
            this.textBox390.Left = 8.022442F;
            this.textBox390.Name = "textBox390";
            this.textBox390.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox390.Tag = "";
            this.textBox390.Text = "ITEM120";
            this.textBox390.Top = 5.80748F;
            this.textBox390.Width = 0.2779529F;
            // 
            // textBox391
            // 
            this.textBox391.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox391.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox391.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox391.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox391.Height = 0.2637798F;
            this.textBox391.Left = 10.37756F;
            this.textBox391.Name = "textBox391";
            this.textBox391.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox391.Tag = "";
            this.textBox391.Text = "区分";
            this.textBox391.Top = 4.562993F;
            this.textBox391.Width = 0.1181103F;
            // 
            // textBox392
            // 
            this.textBox392.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox392.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox392.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox392.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox392.DataField = "ITEM124";
            this.textBox392.Height = 0.2637797F;
            this.textBox392.Left = 10.48622F;
            this.textBox392.Name = "textBox392";
            this.textBox392.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox392.Tag = "";
            this.textBox392.Text = "ITEM124";
            this.textBox392.Top = 4.562993F;
            this.textBox392.Width = 0.2874019F;
            // 
            // textBox393
            // 
            this.textBox393.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox393.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox393.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox393.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox393.Height = 0.2578742F;
            this.textBox393.Left = 10.37756F;
            this.textBox393.Name = "textBox393";
            this.textBox393.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox393.Tag = "";
            this.textBox393.Text = "区分";
            this.textBox393.Top = 4.982282F;
            this.textBox393.Width = 0.1181103F;
            // 
            // textBox394
            // 
            this.textBox394.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox394.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox394.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox394.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox394.DataField = "ITEM128";
            this.textBox394.Height = 0.2578741F;
            this.textBox394.Left = 10.49567F;
            this.textBox394.Name = "textBox394";
            this.textBox394.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox394.Tag = "";
            this.textBox394.Text = "ITEM128";
            this.textBox394.Top = 4.982282F;
            this.textBox394.Width = 0.2779524F;
            // 
            // textBox395
            // 
            this.textBox395.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox395.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox395.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox395.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox395.Height = 0.2578742F;
            this.textBox395.Left = 10.37756F;
            this.textBox395.Name = "textBox395";
            this.textBox395.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox395.Tag = "";
            this.textBox395.Text = "区分";
            this.textBox395.Top = 5.397245F;
            this.textBox395.Width = 0.1181103F;
            // 
            // textBox396
            // 
            this.textBox396.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox396.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox396.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox396.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox396.DataField = "ITEM132";
            this.textBox396.Height = 0.2578741F;
            this.textBox396.Left = 10.49567F;
            this.textBox396.Name = "textBox396";
            this.textBox396.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox396.Tag = "";
            this.textBox396.Text = "ITEM132";
            this.textBox396.Top = 5.397245F;
            this.textBox396.Width = 0.2779524F;
            // 
            // textBox397
            // 
            this.textBox397.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox397.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox397.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox397.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox397.Height = 0.2578742F;
            this.textBox397.Left = 10.37756F;
            this.textBox397.Name = "textBox397";
            this.textBox397.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox397.Tag = "";
            this.textBox397.Text = "区分";
            this.textBox397.Top = 5.80748F;
            this.textBox397.Width = 0.1181103F;
            // 
            // textBox398
            // 
            this.textBox398.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox398.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox398.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox398.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox398.DataField = "ITEM136";
            this.textBox398.Height = 0.2578741F;
            this.textBox398.Left = 10.49567F;
            this.textBox398.Name = "textBox398";
            this.textBox398.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox398.Tag = "";
            this.textBox398.Text = "ITEM136";
            this.textBox398.Top = 5.80748F;
            this.textBox398.Width = 0.2779524F;
            // 
            // textBox400
            // 
            this.textBox400.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox400.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox400.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox400.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox400.Height = 1.652362F;
            this.textBox400.Left = 10.77362F;
            this.textBox400.Name = "textBox400";
            this.textBox400.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5.5pt; font" +
    "-weight: normal; text-align: left; vertical-align: top; ddo-char-set: 1";
            this.textBox400.Tag = "";
            this.textBox400.Text = null;
            this.textBox400.Top = 4.566929F;
            this.textBox400.Width = 0.4736223F;
            // 
            // textBox402
            // 
            this.textBox402.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox402.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox402.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox402.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox402.Height = 0.4133858F;
            this.textBox402.Left = 6.014568F;
            this.textBox402.Name = "textBox402";
            this.textBox402.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1; ddo-" +
    "font-vertical: true";
            this.textBox402.Tag = "";
            this.textBox402.Text = "2";
            this.textBox402.Top = 4.980709F;
            this.textBox402.Width = 0.1574803F;
            // 
            // textBox403
            // 
            this.textBox403.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox403.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox403.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox403.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox403.DataField = "ITEM111";
            this.textBox403.Height = 0.2559048F;
            this.textBox403.Left = 6.646064F;
            this.textBox403.Name = "textBox403";
            this.textBox403.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5.5pt; font" +
    "-weight: normal; text-align: left; vertical-align: bottom; ddo-char-set: 1";
            this.textBox403.Tag = "";
            this.textBox403.Text = "ITEM111";
            this.textBox403.Top = 4.981104F;
            this.textBox403.Width = 1.258268F;
            // 
            // textBox404
            // 
            this.textBox404.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox404.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox404.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox404.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox404.DataField = "ITEM110";
            this.textBox404.Height = 0.09645708F;
            this.textBox404.Left = 6.649214F;
            this.textBox404.Name = "textBox404";
            this.textBox404.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5.5pt; font" +
    "-weight: normal; text-align: left; vertical-align: top; ddo-char-set: 1";
            this.textBox404.Tag = "";
            this.textBox404.Text = "ITEM110";
            this.textBox404.Top = 4.981102F;
            this.textBox404.Width = 1.255118F;
            // 
            // textBox405
            // 
            this.textBox405.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox405.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox405.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox405.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox405.Height = 0.1559055F;
            this.textBox405.Left = 6.172049F;
            this.textBox405.Name = "textBox405";
            this.textBox405.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox405.Tag = "";
            this.textBox405.Text = "個人番号";
            this.textBox405.Top = 5.237009F;
            this.textBox405.Width = 0.4740157F;
            // 
            // textBox406
            // 
            this.textBox406.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox406.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox406.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox406.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox406.Height = 0.1559055F;
            this.textBox406.Left = 6.170867F;
            this.textBox406.Name = "textBox406";
            this.textBox406.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox406.Tag = "";
            this.textBox406.Text = "個人番号";
            this.textBox406.Top = 5.651575F;
            this.textBox406.Width = 0.4751968F;
            // 
            // textBox407
            // 
            this.textBox407.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox407.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox407.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox407.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox407.Height = 0.4133858F;
            this.textBox407.Left = 6.014568F;
            this.textBox407.Name = "textBox407";
            this.textBox407.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1; ddo-" +
    "font-vertical: true";
            this.textBox407.Tag = "";
            this.textBox407.Text = "4";
            this.textBox407.Top = 5.805906F;
            this.textBox407.Width = 0.1574803F;
            // 
            // textBox408
            // 
            this.textBox408.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox408.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox408.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox408.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox408.Height = 0.4133858F;
            this.textBox408.Left = 6.014568F;
            this.textBox408.Name = "textBox408";
            this.textBox408.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1; ddo-" +
    "font-vertical: true";
            this.textBox408.Tag = "";
            this.textBox408.Text = "1";
            this.textBox408.Top = 4.567717F;
            this.textBox408.Width = 0.1574803F;
            // 
            // textBox409
            // 
            this.textBox409.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox409.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox409.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox409.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox409.DataField = "ITEM107";
            this.textBox409.Height = 0.2625984F;
            this.textBox409.Left = 6.646064F;
            this.textBox409.Name = "textBox409";
            this.textBox409.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5.5pt; font" +
    "-weight: normal; text-align: left; vertical-align: bottom; ddo-char-set: 1";
            this.textBox409.Tag = "";
            this.textBox409.Text = "ITEM107";
            this.textBox409.Top = 4.566929F;
            this.textBox409.Width = 1.258268F;
            // 
            // textBox410
            // 
            this.textBox410.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox410.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox410.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox410.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox410.DataField = "ITEM106";
            this.textBox410.Height = 0.09645708F;
            this.textBox410.Left = 6.646064F;
            this.textBox410.Name = "textBox410";
            this.textBox410.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5.5pt; font" +
    "-weight: normal; text-align: left; vertical-align: top; ddo-char-set: 1";
            this.textBox410.Tag = "";
            this.textBox410.Text = "ITEM106";
            this.textBox410.Top = 4.566929F;
            this.textBox410.Width = 1.258268F;
            // 
            // textBox411
            // 
            this.textBox411.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox411.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox411.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox411.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox411.DataField = "ITEM115";
            this.textBox411.Height = 0.2578741F;
            this.textBox411.Left = 6.646064F;
            this.textBox411.Name = "textBox411";
            this.textBox411.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5.5pt; font" +
    "-weight: normal; text-align: left; text-justify: distribute-all-lines; vertical-" +
    "align: bottom; ddo-char-set: 1";
            this.textBox411.Tag = "";
            this.textBox411.Text = "ITEM115";
            this.textBox411.Top = 5.395668F;
            this.textBox411.Width = 1.258268F;
            // 
            // textBox412
            // 
            this.textBox412.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox412.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox412.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox412.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox412.DataField = "ITEM114";
            this.textBox412.Height = 0.09645708F;
            this.textBox412.Left = 6.649214F;
            this.textBox412.Name = "textBox412";
            this.textBox412.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5.5pt; font" +
    "-weight: normal; text-align: left; vertical-align: top; ddo-char-set: 1";
            this.textBox412.Tag = "";
            this.textBox412.Text = "ITEM114";
            this.textBox412.Top = 5.397245F;
            this.textBox412.Width = 1.255118F;
            // 
            // textBox413
            // 
            this.textBox413.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox413.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox413.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox413.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox413.DataField = "ITEM119";
            this.textBox413.Height = 0.2559057F;
            this.textBox413.Left = 6.646064F;
            this.textBox413.Name = "textBox413";
            this.textBox413.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5.5pt; font" +
    "-weight: normal; text-align: left; vertical-align: bottom; ddo-char-set: 1";
            this.textBox413.Tag = "";
            this.textBox413.Text = "ITEM119";
            this.textBox413.Top = 5.80748F;
            this.textBox413.Width = 1.258268F;
            // 
            // textBox414
            // 
            this.textBox414.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox414.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox414.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox414.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox414.DataField = "ITEM118";
            this.textBox414.Height = 0.09645708F;
            this.textBox414.Left = 6.649214F;
            this.textBox414.Name = "textBox414";
            this.textBox414.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5.5pt; font" +
    "-weight: normal; text-align: left; vertical-align: top; ddo-char-set: 1";
            this.textBox414.Tag = "";
            this.textBox414.Text = "ITEM118";
            this.textBox414.Top = 5.80748F;
            this.textBox414.Width = 1.255118F;
            // 
            // textBox415
            // 
            this.textBox415.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox415.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox415.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox415.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox415.Height = 0.1559055F;
            this.textBox415.Left = 8.644095F;
            this.textBox415.Name = "textBox415";
            this.textBox415.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox415.Tag = "";
            this.textBox415.Text = "個人番号";
            this.textBox415.Top = 5.65315F;
            this.textBox415.Width = 0.4751968F;
            // 
            // textBox416
            // 
            this.textBox416.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox416.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox416.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox416.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox416.Height = 0.1559055F;
            this.textBox416.Left = 8.644095F;
            this.textBox416.Name = "textBox416";
            this.textBox416.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox416.Tag = "";
            this.textBox416.Text = "個人番号";
            this.textBox416.Top = 6.064961F;
            this.textBox416.Width = 0.4751968F;
            // 
            // textBox417
            // 
            this.textBox417.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox417.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox417.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox417.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox417.Height = 0.1559055F;
            this.textBox417.Left = 8.644095F;
            this.textBox417.Name = "textBox417";
            this.textBox417.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox417.Tag = "";
            this.textBox417.Text = "個人番号";
            this.textBox417.Top = 5.238583F;
            this.textBox417.Width = 0.4751968F;
            // 
            // textBox418
            // 
            this.textBox418.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox418.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox418.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox418.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox418.Height = 0.1559055F;
            this.textBox418.Left = 8.644095F;
            this.textBox418.Name = "textBox418";
            this.textBox418.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox418.Tag = "";
            this.textBox418.Text = "個人番号";
            this.textBox418.Top = 4.826379F;
            this.textBox418.Width = 0.4751968F;
            // 
            // textBox419
            // 
            this.textBox419.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox419.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox419.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox419.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox419.Height = 0.4133858F;
            this.textBox419.Left = 8.487796F;
            this.textBox419.Name = "textBox419";
            this.textBox419.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1; ddo-" +
    "font-vertical: true";
            this.textBox419.Tag = "";
            this.textBox419.Text = "1";
            this.textBox419.Top = 4.56693F;
            this.textBox419.Width = 0.1574803F;
            // 
            // textBox420
            // 
            this.textBox420.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox420.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox420.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox420.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox420.Height = 0.4133858F;
            this.textBox420.Left = 8.487796F;
            this.textBox420.Name = "textBox420";
            this.textBox420.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1; ddo-" +
    "font-vertical: true";
            this.textBox420.Tag = "";
            this.textBox420.Text = "2";
            this.textBox420.Top = 4.982282F;
            this.textBox420.Width = 0.1574803F;
            // 
            // textBox421
            // 
            this.textBox421.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox421.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox421.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox421.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox421.Height = 0.4133858F;
            this.textBox421.Left = 8.487796F;
            this.textBox421.Name = "textBox421";
            this.textBox421.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1; ddo-" +
    "font-vertical: true";
            this.textBox421.Tag = "";
            this.textBox421.Text = "3";
            this.textBox421.Top = 5.395669F;
            this.textBox421.Width = 0.1574803F;
            // 
            // textBox422
            // 
            this.textBox422.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox422.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox422.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox422.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox422.Height = 0.4133858F;
            this.textBox422.Left = 8.487796F;
            this.textBox422.Name = "textBox422";
            this.textBox422.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1; ddo-" +
    "font-vertical: true";
            this.textBox422.Tag = "";
            this.textBox422.Text = "4";
            this.textBox422.Top = 5.80748F;
            this.textBox422.Width = 0.1574803F;
            // 
            // textBox423
            // 
            this.textBox423.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox423.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox423.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox423.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox423.DataField = "ITEM123";
            this.textBox423.Height = 0.2625982F;
            this.textBox423.Left = 9.119292F;
            this.textBox423.Name = "textBox423";
            this.textBox423.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5.5pt; font" +
    "-weight: normal; text-align: left; vertical-align: bottom; ddo-char-set: 1";
            this.textBox423.Tag = "";
            this.textBox423.Text = "ITEM123";
            this.textBox423.Top = 4.564174F;
            this.textBox423.Width = 1.258268F;
            // 
            // textBox424
            // 
            this.textBox424.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox424.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox424.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox424.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox424.DataField = "ITEM122";
            this.textBox424.Height = 0.09645708F;
            this.textBox424.Left = 9.121262F;
            this.textBox424.Name = "textBox424";
            this.textBox424.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5.5pt; font" +
    "-weight: normal; text-align: left; vertical-align: top; ddo-char-set: 1";
            this.textBox424.Tag = "";
            this.textBox424.Text = "ITEM122";
            this.textBox424.Top = 4.565749F;
            this.textBox424.Width = 1.256299F;
            // 
            // textBox425
            // 
            this.textBox425.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox425.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox425.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox425.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox425.DataField = "ITEM127";
            this.textBox425.Height = 0.2559048F;
            this.textBox425.Left = 9.119292F;
            this.textBox425.Name = "textBox425";
            this.textBox425.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5.5pt; font" +
    "-weight: normal; text-align: left; vertical-align: bottom; ddo-char-set: 1";
            this.textBox425.Tag = "";
            this.textBox425.Text = "ITEM127";
            this.textBox425.Top = 4.982677F;
            this.textBox425.Width = 1.258268F;
            // 
            // textBox426
            // 
            this.textBox426.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox426.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox426.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox426.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox426.DataField = "ITEM126";
            this.textBox426.Height = 0.09645708F;
            this.textBox426.Left = 9.119292F;
            this.textBox426.Name = "textBox426";
            this.textBox426.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5.5pt; font" +
    "-weight: normal; text-align: left; vertical-align: top; ddo-char-set: 1";
            this.textBox426.Tag = "";
            this.textBox426.Text = "ITEM126";
            this.textBox426.Top = 4.982284F;
            this.textBox426.Width = 1.258268F;
            // 
            // textBox427
            // 
            this.textBox427.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox427.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox427.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox427.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox427.DataField = "ITEM131";
            this.textBox427.Height = 0.2578741F;
            this.textBox427.Left = 9.119292F;
            this.textBox427.Name = "textBox427";
            this.textBox427.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5.5pt; font" +
    "-weight: normal; text-align: left; vertical-align: bottom; ddo-char-set: 1";
            this.textBox427.Tag = "";
            this.textBox427.Text = "ITEM131";
            this.textBox427.Top = 5.397243F;
            this.textBox427.Width = 1.258268F;
            // 
            // textBox428
            // 
            this.textBox428.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox428.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox428.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox428.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox428.DataField = "ITEM130";
            this.textBox428.Height = 0.09645708F;
            this.textBox428.Left = 9.119292F;
            this.textBox428.Name = "textBox428";
            this.textBox428.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5.5pt; font" +
    "-weight: normal; text-align: left; vertical-align: top; ddo-char-set: 1";
            this.textBox428.Tag = "";
            this.textBox428.Text = "ITEM130";
            this.textBox428.Top = 5.397639F;
            this.textBox428.Width = 1.258268F;
            // 
            // textBox429
            // 
            this.textBox429.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox429.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox429.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox429.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox429.DataField = "ITEM135";
            this.textBox429.Height = 0.2559057F;
            this.textBox429.Left = 9.119292F;
            this.textBox429.Name = "textBox429";
            this.textBox429.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5.5pt; font" +
    "-weight: normal; text-align: left; vertical-align: bottom; ddo-char-set: 1";
            this.textBox429.Tag = "";
            this.textBox429.Text = "ITEM135";
            this.textBox429.Top = 5.807875F;
            this.textBox429.Width = 1.258267F;
            // 
            // textBox430
            // 
            this.textBox430.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox430.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox430.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox430.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox430.DataField = "ITEM134";
            this.textBox430.Height = 0.09645708F;
            this.textBox430.Left = 9.119292F;
            this.textBox430.Name = "textBox430";
            this.textBox430.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5.5pt; font" +
    "-weight: normal; text-align: left; vertical-align: top; ddo-char-set: 1";
            this.textBox430.Tag = "";
            this.textBox430.Text = "ITEM134";
            this.textBox430.Top = 5.80945F;
            this.textBox430.Width = 1.258268F;
            // 
            // textBox431
            // 
            this.textBox431.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox431.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox431.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox431.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox431.Height = 0.4665354F;
            this.textBox431.Left = 6.040945F;
            this.textBox431.Name = "textBox431";
            this.textBox431.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox431.Tag = "";
            this.textBox431.Text = "外\r\n国\r\n人";
            this.textBox431.Top = 6.225196F;
            this.textBox431.Width = 0.2133858F;
            // 
            // textBox432
            // 
            this.textBox432.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox432.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox432.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox432.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox432.Height = 0.707872F;
            this.textBox432.Left = 5.827953F;
            this.textBox432.Name = "textBox432";
            this.textBox432.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8.5pt; font" +
    "-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox432.Tag = "";
            this.textBox432.Text = "支 　　払 　 者";
            this.textBox432.Top = 6.850394F;
            this.textBox432.Width = 0.2661438F;
            // 
            // textBox433
            // 
            this.textBox433.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox433.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox433.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox433.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox433.Height = 0.2362205F;
            this.textBox433.Left = 6.094096F;
            this.textBox433.Name = "textBox433";
            this.textBox433.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox433.Tag = "";
            this.textBox433.Text = "氏 名 又 は\r\n名       称";
            this.textBox433.Top = 7.322832F;
            this.textBox433.Width = 0.8870077F;
            // 
            // textBox434
            // 
            this.textBox434.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox434.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox434.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox434.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox434.DataField = "ITEM096";
            this.textBox434.Height = 0.2362205F;
            this.textBox434.Left = 6.981105F;
            this.textBox434.Name = "textBox434";
            this.textBox434.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: left; ddo-char-set: 1";
            this.textBox434.Tag = "";
            this.textBox434.Text = "ITEM096";
            this.textBox434.Top = 7.086617F;
            this.textBox434.Width = 4.26693F;
            // 
            // textBox435
            // 
            this.textBox435.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox435.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox435.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox435.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox435.CanGrow = false;
            this.textBox435.DataField = "ITEM097";
            this.textBox435.Height = 0.2362205F;
            this.textBox435.Left = 6.981105F;
            this.textBox435.Name = "textBox435";
            this.textBox435.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: left; vertical-align: middle; ddo-char-set: 1";
            this.textBox435.Tag = "";
            this.textBox435.Text = "ITEM097";
            this.textBox435.Top = 7.322832F;
            this.textBox435.Width = 4.26693F;
            // 
            // textBox438
            // 
            this.textBox438.DataField = "ITEM098";
            this.textBox438.Height = 0.1576389F;
            this.textBox438.Left = 9.494094F;
            this.textBox438.Name = "textBox438";
            this.textBox438.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: left; vertical-align: bottom; ddo-char-set: 1";
            this.textBox438.Tag = "";
            this.textBox438.Text = "ITEM098(電話)";
            this.textBox438.Top = 7.374804F;
            this.textBox438.Width = 1.7374F;
            // 
            // textBox439
            // 
            this.textBox439.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox439.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox439.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox439.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox439.DataField = "ITEM069";
            this.textBox439.Height = 0.1576389F;
            this.textBox439.Left = 5.827953F;
            this.textBox439.Name = "textBox439";
            this.textBox439.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox439.Tag = "";
            this.textBox439.Text = "ITEM069";
            this.textBox439.Top = 6.692914F;
            this.textBox439.Width = 0.2131944F;
            // 
            // textBox440
            // 
            this.textBox440.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox440.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox440.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox440.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox440.DataField = "ITEM070";
            this.textBox440.Height = 0.1576389F;
            this.textBox440.Left = 6.041338F;
            this.textBox440.Name = "textBox440";
            this.textBox440.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox440.Tag = "";
            this.textBox440.Text = "ITEM070";
            this.textBox440.Top = 6.692914F;
            this.textBox440.Width = 0.2131944F;
            // 
            // textBox441
            // 
            this.textBox441.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox441.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox441.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox441.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox441.DataField = "ITEM071";
            this.textBox441.Height = 0.1576389F;
            this.textBox441.Left = 6.254532F;
            this.textBox441.Name = "textBox441";
            this.textBox441.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox441.Tag = "";
            this.textBox441.Text = "ITEM071";
            this.textBox441.Top = 6.692914F;
            this.textBox441.Width = 0.2127909F;
            // 
            // textBox442
            // 
            this.textBox442.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox442.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox442.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox442.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox442.DataField = "ITEM072";
            this.textBox442.Height = 0.1576389F;
            this.textBox442.Left = 6.467323F;
            this.textBox442.Name = "textBox442";
            this.textBox442.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox442.Tag = "";
            this.textBox442.Text = "ITEM072";
            this.textBox442.Top = 6.692914F;
            this.textBox442.Width = 0.2131944F;
            // 
            // textBox443
            // 
            this.textBox443.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox443.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox443.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox443.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox443.DataField = "ITEM073";
            this.textBox443.Height = 0.1576389F;
            this.textBox443.Left = 6.680923F;
            this.textBox443.Name = "textBox443";
            this.textBox443.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox443.Tag = "";
            this.textBox443.Text = "ITEM073";
            this.textBox443.Top = 6.692914F;
            this.textBox443.Width = 0.2131944F;
            // 
            // textBox444
            // 
            this.textBox444.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox444.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox444.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox444.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox444.DataField = "ITEM074";
            this.textBox444.Height = 0.1576389F;
            this.textBox444.Left = 6.894094F;
            this.textBox444.Name = "textBox444";
            this.textBox444.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox444.Tag = "";
            this.textBox444.Text = "ITEM074";
            this.textBox444.Top = 6.692914F;
            this.textBox444.Width = 0.2334646F;
            // 
            // textBox445
            // 
            this.textBox445.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox445.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox445.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox445.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox445.DataField = "ITEM075";
            this.textBox445.Height = 0.1576389F;
            this.textBox445.Left = 7.130314F;
            this.textBox445.Name = "textBox445";
            this.textBox445.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox445.Tag = "";
            this.textBox445.Text = "ITEM075";
            this.textBox445.Top = 6.692914F;
            this.textBox445.Width = 0.236029F;
            // 
            // textBox446
            // 
            this.textBox446.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox446.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox446.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox446.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox446.DataField = "ITEM076";
            this.textBox446.Height = 0.1574803F;
            this.textBox446.Left = 7.366725F;
            this.textBox446.Name = "textBox446";
            this.textBox446.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox446.Tag = "";
            this.textBox446.Text = "ITEM076";
            this.textBox446.Top = 6.692914F;
            this.textBox446.Width = 0.2362205F;
            // 
            // textBox447
            // 
            this.textBox447.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox447.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox447.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox447.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox447.DataField = "ITEM077";
            this.textBox447.Height = 0.1574803F;
            this.textBox447.Left = 7.602756F;
            this.textBox447.Name = "textBox447";
            this.textBox447.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox447.Tag = "";
            this.textBox447.Text = "ITEM077";
            this.textBox447.Top = 6.692914F;
            this.textBox447.Width = 0.2362205F;
            // 
            // textBox448
            // 
            this.textBox448.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox448.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox448.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox448.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox448.DataField = "ITEM078";
            this.textBox448.Height = 0.1576389F;
            this.textBox448.Left = 7.839371F;
            this.textBox448.Name = "textBox448";
            this.textBox448.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox448.Tag = "";
            this.textBox448.Text = "ITEM078";
            this.textBox448.Top = 6.692914F;
            this.textBox448.Width = 0.2131944F;
            // 
            // textBox449
            // 
            this.textBox449.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox449.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox449.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox449.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox449.DataField = "ITEM079";
            this.textBox449.Height = 0.1576389F;
            this.textBox449.Left = 8.052565F;
            this.textBox449.Name = "textBox449";
            this.textBox449.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox449.Tag = "";
            this.textBox449.Text = "ITEM079";
            this.textBox449.Top = 6.692914F;
            this.textBox449.Width = 0.2131944F;
            // 
            // textBox450
            // 
            this.textBox450.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox450.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox450.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox450.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox450.DataField = "ITEM081";
            this.textBox450.Height = 0.3877954F;
            this.textBox450.Left = 8.265748F;
            this.textBox450.Name = "textBox450";
            this.textBox450.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox450.Tag = "";
            this.textBox450.Text = "ITEM081";
            this.textBox450.Top = 6.462599F;
            this.textBox450.Width = 0.2478399F;
            // 
            // textBox451
            // 
            this.textBox451.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox451.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox451.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox451.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox451.DataField = "ITEM085";
            this.textBox451.Height = 0.3877953F;
            this.textBox451.Left = 8.51378F;
            this.textBox451.Name = "textBox451";
            this.textBox451.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox451.Tag = "";
            this.textBox451.Text = "ITEM085";
            this.textBox451.Top = 6.462599F;
            this.textBox451.Width = 0.2480315F;
            // 
            // textBox452
            // 
            this.textBox452.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox452.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox452.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox452.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox452.DataField = "ITEM086";
            this.textBox452.Height = 0.3877953F;
            this.textBox452.Left = 8.761811F;
            this.textBox452.Name = "textBox452";
            this.textBox452.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox452.Tag = "";
            this.textBox452.Text = "ITEM086";
            this.textBox452.Top = 6.462599F;
            this.textBox452.Width = 0.2480315F;
            // 
            // textBox453
            // 
            this.textBox453.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox453.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox453.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox453.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox453.DataField = "ITEM087";
            this.textBox453.Height = 0.3877953F;
            this.textBox453.Left = 9.009843F;
            this.textBox453.Name = "textBox453";
            this.textBox453.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox453.Tag = "";
            this.textBox453.Text = "ITEM087";
            this.textBox453.Top = 6.462599F;
            this.textBox453.Width = 0.2480315F;
            // 
            // textBox454
            // 
            this.textBox454.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox454.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox454.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox454.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox454.DataField = "ITEM088";
            this.textBox454.Height = 0.3877953F;
            this.textBox454.Left = 9.257874F;
            this.textBox454.Name = "textBox454";
            this.textBox454.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox454.Tag = "";
            this.textBox454.Text = "ITEM088";
            this.textBox454.Top = 6.462599F;
            this.textBox454.Width = 0.2480315F;
            // 
            // textBox455
            // 
            this.textBox455.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox455.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox455.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox455.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox455.Height = 0.1180556F;
            this.textBox455.Left = 8.265748F;
            this.textBox455.Name = "textBox455";
            this.textBox455.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox455.Tag = "";
            this.textBox455.Text = "中 途 就・退 職";
            this.textBox455.Top = 6.224016F;
            this.textBox455.Width = 1.244094F;
            // 
            // textBox456
            // 
            this.textBox456.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox456.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox456.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox456.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox456.Height = 0.1181102F;
            this.textBox456.Left = 8.265748F;
            this.textBox456.Name = "textBox456";
            this.textBox456.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox456.Tag = "";
            this.textBox456.Text = "就職";
            this.textBox456.Top = 6.344488F;
            this.textBox456.Width = 0.2480315F;
            // 
            // textBox457
            // 
            this.textBox457.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox457.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox457.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox457.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox457.Height = 0.4665354F;
            this.textBox457.Left = 5.827953F;
            this.textBox457.Name = "textBox457";
            this.textBox457.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox457.Tag = "";
            this.textBox457.Text = "未\r\n成\r\n年\r\n者";
            this.textBox457.Top = 6.225196F;
            this.textBox457.Width = 0.2133858F;
            // 
            // textBox458
            // 
            this.textBox458.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox458.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox458.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox458.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox458.Height = 0.4665354F;
            this.textBox458.Left = 6.254149F;
            this.textBox458.Name = "textBox458";
            this.textBox458.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox458.Tag = "";
            this.textBox458.Text = "死\r\n亡\r\n退\r\n職";
            this.textBox458.Top = 6.225F;
            this.textBox458.Width = 0.2133858F;
            // 
            // textBox459
            // 
            this.textBox459.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox459.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox459.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox459.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox459.Height = 0.4665354F;
            this.textBox459.Left = 6.467345F;
            this.textBox459.Name = "textBox459";
            this.textBox459.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox459.Tag = "";
            this.textBox459.Text = "災\r\n害\r\n者";
            this.textBox459.Top = 6.225F;
            this.textBox459.Width = 0.2133858F;
            // 
            // textBox460
            // 
            this.textBox460.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox460.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox460.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox460.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox460.Height = 0.4665354F;
            this.textBox460.Left = 6.68054F;
            this.textBox460.Name = "textBox460";
            this.textBox460.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox460.Tag = "";
            this.textBox460.Text = "乙\r\n\r\n欄";
            this.textBox460.Top = 6.225F;
            this.textBox460.Width = 0.2133858F;
            // 
            // textBox461
            // 
            this.textBox461.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox461.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox461.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox461.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox461.Height = 0.1180556F;
            this.textBox461.Left = 6.894094F;
            this.textBox461.Name = "textBox461";
            this.textBox461.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox461.Tag = "";
            this.textBox461.Text = "本人が障害者";
            this.textBox461.Top = 6.225196F;
            this.textBox461.Width = 0.472441F;
            // 
            // textBox462
            // 
            this.textBox462.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox462.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox462.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox462.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox462.Height = 0.348425F;
            this.textBox462.Left = 6.894094F;
            this.textBox462.Name = "textBox462";
            this.textBox462.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox462.Tag = "";
            this.textBox462.Text = "特\r\n\r\n別";
            this.textBox462.Top = 6.343307F;
            this.textBox462.Width = 0.2362205F;
            // 
            // textBox463
            // 
            this.textBox463.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox463.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox463.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox463.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox463.CanGrow = false;
            this.textBox463.Height = 0.348425F;
            this.textBox463.Left = 7.130314F;
            this.textBox463.Name = "textBox463";
            this.textBox463.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox463.Tag = "";
            this.textBox463.Text = "そ\r\nの\r\n他";
            this.textBox463.Top = 6.343307F;
            this.textBox463.Width = 0.2362205F;
            // 
            // textBox464
            // 
            this.textBox464.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox464.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox464.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox464.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox464.Height = 0.1180556F;
            this.textBox464.Left = 7.366535F;
            this.textBox464.Name = "textBox464";
            this.textBox464.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox464.Tag = "";
            this.textBox464.Text = "寡　婦";
            this.textBox464.Top = 6.225196F;
            this.textBox464.Width = 0.4724409F;
            // 
            // textBox465
            // 
            this.textBox465.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox465.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox465.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox465.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox465.Height = 0.3484252F;
            this.textBox465.Left = 7.366535F;
            this.textBox465.Name = "textBox465";
            this.textBox465.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox465.Tag = "";
            this.textBox465.Text = "一\r\n般";
            this.textBox465.Top = 6.343307F;
            this.textBox465.Width = 0.2362205F;
            // 
            // textBox466
            // 
            this.textBox466.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox466.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox466.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox466.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox466.Height = 0.3484252F;
            this.textBox466.Left = 7.602566F;
            this.textBox466.Name = "textBox466";
            this.textBox466.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox466.Tag = "";
            this.textBox466.Text = "特\r\n別";
            this.textBox466.Top = 6.343307F;
            this.textBox466.Width = 0.2362205F;
            // 
            // textBox467
            // 
            this.textBox467.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox467.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox467.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox467.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox467.Height = 0.4665354F;
            this.textBox467.Left = 7.838976F;
            this.textBox467.Name = "textBox467";
            this.textBox467.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox467.Tag = "";
            this.textBox467.Text = "寡　夫";
            this.textBox467.Top = 6.225196F;
            this.textBox467.Width = 0.2133858F;
            // 
            // textBox468
            // 
            this.textBox468.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox468.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox468.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox468.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox468.CanGrow = false;
            this.textBox468.Height = 0.4665354F;
            this.textBox468.Left = 8.052362F;
            this.textBox468.Name = "textBox468";
            this.textBox468.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1; ddo-font-vertical: true";
            this.textBox468.Tag = "";
            this.textBox468.Text = "勤\r\n労\r\n学\r\n生";
            this.textBox468.Top = 6.225196F;
            this.textBox468.Width = 0.2133858F;
            // 
            // textBox469
            // 
            this.textBox469.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox469.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox469.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox469.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox469.Height = 0.1181102F;
            this.textBox469.Left = 8.51378F;
            this.textBox469.Name = "textBox469";
            this.textBox469.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox469.Tag = "";
            this.textBox469.Text = "退職";
            this.textBox469.Top = 6.344488F;
            this.textBox469.Width = 0.2480315F;
            // 
            // textBox470
            // 
            this.textBox470.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox470.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox470.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox470.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox470.Height = 0.1181102F;
            this.textBox470.Left = 8.761811F;
            this.textBox470.Name = "textBox470";
            this.textBox470.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox470.Tag = "";
            this.textBox470.Text = "年";
            this.textBox470.Top = 6.344488F;
            this.textBox470.Width = 0.2480315F;
            // 
            // textBox471
            // 
            this.textBox471.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox471.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox471.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox471.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox471.Height = 0.1181102F;
            this.textBox471.Left = 9.009843F;
            this.textBox471.Name = "textBox471";
            this.textBox471.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox471.Tag = "";
            this.textBox471.Text = "月";
            this.textBox471.Top = 6.344488F;
            this.textBox471.Width = 0.2480315F;
            // 
            // textBox472
            // 
            this.textBox472.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox472.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox472.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox472.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox472.Height = 0.1181102F;
            this.textBox472.Left = 9.257874F;
            this.textBox472.Name = "textBox472";
            this.textBox472.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox472.Tag = "";
            this.textBox472.Text = "日";
            this.textBox472.Top = 6.344488F;
            this.textBox472.Width = 0.2480315F;
            // 
            // textBox473
            // 
            this.textBox473.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox473.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox473.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox473.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox473.DataField = "ITEM089";
            this.textBox473.Height = 0.3877953F;
            this.textBox473.Left = 9.505906F;
            this.textBox473.Name = "textBox473";
            this.textBox473.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox473.Tag = "";
            this.textBox473.Text = "ITEM089";
            this.textBox473.Top = 6.462599F;
            this.textBox473.Width = 0.2480315F;
            // 
            // textBox474
            // 
            this.textBox474.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox474.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox474.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox474.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox474.DataField = "ITEM090";
            this.textBox474.Height = 0.3877953F;
            this.textBox474.Left = 9.753938F;
            this.textBox474.Name = "textBox474";
            this.textBox474.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox474.Tag = "";
            this.textBox474.Text = "ITEM090";
            this.textBox474.Top = 6.462599F;
            this.textBox474.Width = 0.2480315F;
            // 
            // textBox475
            // 
            this.textBox475.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox475.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox475.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox475.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox475.DataField = "ITEM091";
            this.textBox475.Height = 0.3877953F;
            this.textBox475.Left = 10.00197F;
            this.textBox475.Name = "textBox475";
            this.textBox475.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox475.Tag = "";
            this.textBox475.Text = "ITEM091";
            this.textBox475.Top = 6.462599F;
            this.textBox475.Width = 0.2480315F;
            // 
            // textBox476
            // 
            this.textBox476.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox476.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox476.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox476.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox476.DataField = "ITEM092";
            this.textBox476.Height = 0.3877953F;
            this.textBox476.Left = 10.25F;
            this.textBox476.Name = "textBox476";
            this.textBox476.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox476.Tag = "";
            this.textBox476.Text = "ITEM092";
            this.textBox476.Top = 6.462599F;
            this.textBox476.Width = 0.2480315F;
            // 
            // textBox477
            // 
            this.textBox477.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox477.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox477.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox477.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox477.DataField = "ITEM093";
            this.textBox477.Height = 0.3877953F;
            this.textBox477.Left = 10.49803F;
            this.textBox477.Name = "textBox477";
            this.textBox477.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox477.Tag = "";
            this.textBox477.Text = "ITEM093";
            this.textBox477.Top = 6.462599F;
            this.textBox477.Width = 0.2480315F;
            // 
            // textBox478
            // 
            this.textBox478.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox478.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox478.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox478.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox478.Height = 0.1180556F;
            this.textBox478.Left = 9.505906F;
            this.textBox478.Name = "textBox478";
            this.textBox478.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox478.Tag = "";
            this.textBox478.Text = "受　給　者　生　年　月　日";
            this.textBox478.Top = 6.224016F;
            this.textBox478.Width = 1.742269F;
            // 
            // textBox479
            // 
            this.textBox479.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox479.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox479.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox479.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox479.Height = 0.1181102F;
            this.textBox479.Left = 9.505906F;
            this.textBox479.Name = "textBox479";
            this.textBox479.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox479.Tag = "";
            this.textBox479.Text = "明";
            this.textBox479.Top = 6.344488F;
            this.textBox479.Width = 0.2480315F;
            // 
            // textBox480
            // 
            this.textBox480.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox480.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox480.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox480.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox480.Height = 0.1181102F;
            this.textBox480.Left = 9.753938F;
            this.textBox480.Name = "textBox480";
            this.textBox480.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox480.Tag = "";
            this.textBox480.Text = "大";
            this.textBox480.Top = 6.344488F;
            this.textBox480.Width = 0.2480315F;
            // 
            // textBox481
            // 
            this.textBox481.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox481.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox481.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox481.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox481.Height = 0.1181102F;
            this.textBox481.Left = 10.00197F;
            this.textBox481.Name = "textBox481";
            this.textBox481.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox481.Tag = "";
            this.textBox481.Text = "昭";
            this.textBox481.Top = 6.344488F;
            this.textBox481.Width = 0.2480315F;
            // 
            // textBox482
            // 
            this.textBox482.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox482.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox482.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox482.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox482.Height = 0.1181102F;
            this.textBox482.Left = 10.25F;
            this.textBox482.Name = "textBox482";
            this.textBox482.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox482.Tag = "";
            this.textBox482.Text = "平";
            this.textBox482.Top = 6.344488F;
            this.textBox482.Width = 0.2480315F;
            // 
            // textBox483
            // 
            this.textBox483.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox483.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox483.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox483.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox483.Height = 0.1181102F;
            this.textBox483.Left = 10.49803F;
            this.textBox483.Name = "textBox483";
            this.textBox483.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox483.Tag = "";
            this.textBox483.Text = "年";
            this.textBox483.Top = 6.344488F;
            this.textBox483.Width = 0.2480315F;
            // 
            // textBox484
            // 
            this.textBox484.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox484.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox484.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox484.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox484.DataField = "ITEM094";
            this.textBox484.Height = 0.3877953F;
            this.textBox484.Left = 10.74606F;
            this.textBox484.Name = "textBox484";
            this.textBox484.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox484.Tag = "";
            this.textBox484.Text = "ITEM094";
            this.textBox484.Top = 6.462599F;
            this.textBox484.Width = 0.2480315F;
            // 
            // textBox485
            // 
            this.textBox485.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox485.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox485.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox485.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox485.Height = 0.1181102F;
            this.textBox485.Left = 10.74606F;
            this.textBox485.Name = "textBox485";
            this.textBox485.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox485.Tag = "";
            this.textBox485.Text = "月";
            this.textBox485.Top = 6.344488F;
            this.textBox485.Width = 0.2480315F;
            // 
            // textBox486
            // 
            this.textBox486.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox486.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox486.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox486.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox486.DataField = "ITEM095";
            this.textBox486.Height = 0.3877953F;
            this.textBox486.Left = 10.9941F;
            this.textBox486.Name = "textBox486";
            this.textBox486.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox486.Tag = "";
            this.textBox486.Text = "ITEM095";
            this.textBox486.Top = 6.462599F;
            this.textBox486.Width = 0.2539372F;
            // 
            // textBox487
            // 
            this.textBox487.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox487.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox487.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox487.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox487.Height = 0.1181102F;
            this.textBox487.Left = 10.9941F;
            this.textBox487.Name = "textBox487";
            this.textBox487.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox487.Tag = "";
            this.textBox487.Text = "日";
            this.textBox487.Top = 6.344488F;
            this.textBox487.Width = 0.2539372F;
            // 
            // textBox488
            // 
            this.textBox488.DataField = "ITEM082";
            this.textBox488.Height = 0.1381944F;
            this.textBox488.Left = 8.761811F;
            this.textBox488.Name = "textBox488";
            this.textBox488.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.textBox488.Tag = "";
            this.textBox488.Text = "ITEM082";
            this.textBox488.Top = 6.462599F;
            this.textBox488.Width = 0.2478399F;
            // 
            // textBox489
            // 
            this.textBox489.DataField = "ITEM083";
            this.textBox489.Height = 0.1381944F;
            this.textBox489.Left = 9.030709F;
            this.textBox489.Name = "textBox489";
            this.textBox489.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.textBox489.Tag = "";
            this.textBox489.Text = "ITEM083";
            this.textBox489.Top = 6.462599F;
            this.textBox489.Width = 0.2131944F;
            // 
            // textBox490
            // 
            this.textBox490.DataField = "ITEM084";
            this.textBox490.Height = 0.1381944F;
            this.textBox490.Left = 9.287795F;
            this.textBox490.Name = "textBox490";
            this.textBox490.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.textBox490.Tag = "";
            this.textBox490.Text = "ITEM084";
            this.textBox490.Top = 6.462599F;
            this.textBox490.Width = 0.2131944F;
            // 
            // textBox491
            // 
            this.textBox491.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox491.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox491.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox491.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox491.Height = 0.2362205F;
            this.textBox491.Left = 6.094096F;
            this.textBox491.Name = "textBox491";
            this.textBox491.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox491.Tag = "";
            this.textBox491.Text = "住所（居所）\r\n又 は 所在地";
            this.textBox491.Top = 7.086617F;
            this.textBox491.Width = 0.8870077F;
            // 
            // textBox492
            // 
            this.textBox492.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox492.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox492.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox492.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox492.Height = 0.2362205F;
            this.textBox492.Left = 6.094095F;
            this.textBox492.Name = "textBox492";
            this.textBox492.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: left; vertical-align: middle; ddo-char-set: 1";
            this.textBox492.Tag = "";
            this.textBox492.Text = null;
            this.textBox492.Top = 6.851575F;
            this.textBox492.Width = 5.153938F;
            // 
            // textBox352
            // 
            this.textBox352.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox352.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox352.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox352.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox352.Height = 0.4133857F;
            this.textBox352.Left = 5.827166F;
            this.textBox352.Name = "textBox352";
            this.textBox352.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox352.Tag = "";
            this.textBox352.Text = "控除対象\r\n配偶者";
            this.textBox352.Top = 4.152756F;
            this.textBox352.Width = 0.3448819F;
            // 
            // textBox401
            // 
            this.textBox401.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox401.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox401.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox401.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox401.Height = 0.4133858F;
            this.textBox401.Left = 6.014568F;
            this.textBox401.Name = "textBox401";
            this.textBox401.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1; ddo-" +
    "font-vertical: true";
            this.textBox401.Tag = "";
            this.textBox401.Text = "3";
            this.textBox401.Top = 5.394094F;
            this.textBox401.Width = 0.1574803F;
            // 
            // textBox346
            // 
            this.textBox346.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox346.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox346.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox346.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox346.Height = 0.4118109F;
            this.textBox346.Left = 8.300406F;
            this.textBox346.Name = "textBox346";
            this.textBox346.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 4.7pt; font" +
    "-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox346.Tag = "";
            this.textBox346.Text = "配偶者の\r\n合計所得";
            this.textBox346.Top = 4.15264F;
            this.textBox346.Width = 0.4937007F;
            // 
            // line52
            // 
            this.line52.Height = 0.1480317F;
            this.line52.Left = 3.433465F;
            this.line52.LineWeight = 1F;
            this.line52.Name = "line52";
            this.line52.Top = 5.248031F;
            this.line52.Width = 1.637402F;
            this.line52.X1 = 3.433465F;
            this.line52.X2 = 5.070867F;
            this.line52.Y1 = 5.396063F;
            this.line52.Y2 = 5.248031F;
            // 
            // line53
            // 
            this.line53.Height = 0.1480317F;
            this.line53.Left = 3.421654F;
            this.line53.LineWeight = 1F;
            this.line53.Name = "line53";
            this.line53.Top = 5.661023F;
            this.line53.Width = 1.637401F;
            this.line53.X1 = 3.421654F;
            this.line53.X2 = 5.059055F;
            this.line53.Y1 = 5.809055F;
            this.line53.Y2 = 5.661023F;
            // 
            // line54
            // 
            this.line54.Height = 0.1480331F;
            this.line54.Left = 3.421654F;
            this.line54.LineWeight = 1F;
            this.line54.Name = "line54";
            this.line54.Top = 6.06496F;
            this.line54.Width = 1.637401F;
            this.line54.X1 = 3.421654F;
            this.line54.X2 = 5.059055F;
            this.line54.Y1 = 6.212993F;
            this.line54.Y2 = 6.06496F;
            // 
            // line55
            // 
            this.line55.Height = 0.7425199F;
            this.line55.Left = 5.083465F;
            this.line55.LineWeight = 1F;
            this.line55.Name = "line55";
            this.line55.Top = 5.477953F;
            this.line55.Width = 0.4641728F;
            this.line55.X1 = 5.083465F;
            this.line55.X2 = 5.547638F;
            this.line55.Y1 = 6.220473F;
            this.line55.Y2 = 5.477953F;
            // 
            // textBox267
            // 
            this.textBox267.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox267.Height = 0.09724441F;
            this.textBox267.Left = 5.070867F;
            this.textBox267.Name = "textBox267";
            this.textBox267.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 4.7pt; font" +
    "-weight: normal; text-align: left; vertical-align: middle; ddo-char-set: 1";
            this.textBox267.Tag = "";
            this.textBox267.Text = "(備考)";
            this.textBox267.Top = 4.56811F;
            this.textBox267.Width = 0.4767714F;
            // 
            // textBox295
            // 
            this.textBox295.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox295.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox295.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox295.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox295.DataField = "ITEM124";
            this.textBox295.Height = 0.2637797F;
            this.textBox295.Left = 4.786614F;
            this.textBox295.Name = "textBox295";
            this.textBox295.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox295.Tag = "";
            this.textBox295.Text = "ITEM124";
            this.textBox295.Top = 4.564961F;
            this.textBox295.Width = 0.2874019F;
            // 
            // textBox350
            // 
            this.textBox350.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox350.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox350.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox350.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox350.CanGrow = false;
            this.textBox350.DataField = "ITEM067";
            this.textBox350.Height = 0.411811F;
            this.textBox350.Left = 10.72795F;
            this.textBox350.Name = "textBox350";
            this.textBox350.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox350.Tag = "";
            this.textBox350.Text = "ITEM067";
            this.textBox350.Top = 4.151576F;
            this.textBox350.Width = 0.5192914F;
            // 
            // line56
            // 
            this.line56.Height = 1.65F;
            this.line56.Left = 10.78307F;
            this.line56.LineWeight = 1F;
            this.line56.Name = "line56";
            this.line56.Top = 4.562993F;
            this.line56.Width = 0.45473F;
            this.line56.X1 = 10.78307F;
            this.line56.X2 = 11.2378F;
            this.line56.Y1 = 6.212993F;
            this.line56.Y2 = 4.562993F;
            // 
            // line57
            // 
            this.line57.Height = 0.1480322F;
            this.line57.Left = 9.119292F;
            this.line57.LineWeight = 1F;
            this.line57.Name = "line57";
            this.line57.Top = 4.837008F;
            this.line57.Width = 1.637398F;
            this.line57.X1 = 9.119292F;
            this.line57.X2 = 10.75669F;
            this.line57.Y1 = 4.98504F;
            this.line57.Y2 = 4.837008F;
            // 
            // line58
            // 
            this.line58.Height = 0.1480322F;
            this.line58.Left = 9.131103F;
            this.line58.LineWeight = 1F;
            this.line58.Name = "line58";
            this.line58.Top = 5.251575F;
            this.line58.Width = 1.637398F;
            this.line58.X1 = 9.131103F;
            this.line58.X2 = 10.7685F;
            this.line58.Y1 = 5.399607F;
            this.line58.Y2 = 5.251575F;
            // 
            // line59
            // 
            this.line59.Height = 0.1480322F;
            this.line59.Left = 9.119292F;
            this.line59.LineWeight = 1F;
            this.line59.Name = "line59";
            this.line59.Top = 5.663779F;
            this.line59.Width = 1.637398F;
            this.line59.X1 = 9.119292F;
            this.line59.X2 = 10.75669F;
            this.line59.Y1 = 5.811811F;
            this.line59.Y2 = 5.663779F;
            // 
            // line60
            // 
            this.line60.Height = 0.1480322F;
            this.line60.Left = 9.119293F;
            this.line60.LineWeight = 1F;
            this.line60.Name = "line60";
            this.line60.Top = 6.075984F;
            this.line60.Width = 1.637397F;
            this.line60.X1 = 9.119293F;
            this.line60.X2 = 10.75669F;
            this.line60.Y1 = 6.224016F;
            this.line60.Y2 = 6.075984F;
            // 
            // line61
            // 
            this.line61.Height = 0.1480331F;
            this.line61.Left = 6.651969F;
            this.line61.LineWeight = 1F;
            this.line61.Name = "line61";
            this.line61.Top = 4.83307F;
            this.line61.Width = 1.637403F;
            this.line61.X1 = 6.651969F;
            this.line61.X2 = 8.289371F;
            this.line61.Y1 = 4.981103F;
            this.line61.Y2 = 4.83307F;
            // 
            // line62
            // 
            this.line62.Height = 0.1480322F;
            this.line62.Left = 6.66378F;
            this.line62.LineWeight = 1F;
            this.line62.Name = "line62";
            this.line62.Top = 5.250787F;
            this.line62.Width = 1.637403F;
            this.line62.X1 = 6.66378F;
            this.line62.X2 = 8.301184F;
            this.line62.Y1 = 5.398819F;
            this.line62.Y2 = 5.250787F;
            // 
            // line63
            // 
            this.line63.Height = 0.1480317F;
            this.line63.Left = 6.65197F;
            this.line63.LineWeight = 1F;
            this.line63.Name = "line63";
            this.line63.Top = 5.66378F;
            this.line63.Width = 1.637401F;
            this.line63.X1 = 6.65197F;
            this.line63.X2 = 8.289371F;
            this.line63.Y1 = 5.811812F;
            this.line63.Y2 = 5.66378F;
            // 
            // line64
            // 
            this.line64.Height = 0.1480317F;
            this.line64.Left = 6.66378F;
            this.line64.LineWeight = 1F;
            this.line64.Name = "line64";
            this.line64.Top = 6.067716F;
            this.line64.Width = 1.637402F;
            this.line64.X1 = 6.66378F;
            this.line64.X2 = 8.301182F;
            this.line64.Y1 = 6.215748F;
            this.line64.Y2 = 6.067716F;
            // 
            // line65
            // 
            this.line65.Height = 0.1480308F;
            this.line65.Left = 6.649213F;
            this.line65.LineWeight = 1F;
            this.line65.Name = "line65";
            this.line65.Top = 4.418898F;
            this.line65.Width = 1.637403F;
            this.line65.X1 = 6.649213F;
            this.line65.X2 = 8.286615F;
            this.line65.Y1 = 4.566929F;
            this.line65.Y2 = 4.418898F;
            // 
            // line66
            // 
            this.line66.Height = 0.2350402F;
            this.line66.Left = 6.094095F;
            this.line66.LineWeight = 1F;
            this.line66.Name = "line66";
            this.line66.Top = 6.851575F;
            this.line66.Width = 5.153934F;
            this.line66.X1 = 6.094095F;
            this.line66.X2 = 11.24803F;
            this.line66.Y1 = 7.086615F;
            this.line66.Y2 = 6.851575F;
            // 
            // textBox84
            // 
            this.textBox84.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox84.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox84.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox84.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox84.Height = 0.2362205F;
            this.textBox84.Left = 0.3937026F;
            this.textBox84.Name = "textBox84";
            this.textBox84.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox84.Tag = "";
            this.textBox84.Text = "個人番号又は\r\n法 人 番 号";
            this.textBox84.Top = 6.851575F;
            this.textBox84.Width = 0.8870079F;
            // 
            // textBox79
            // 
            this.textBox79.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox79.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox79.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox79.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox79.Height = 0.7078731F;
            this.textBox79.Left = 0.1275591F;
            this.textBox79.Name = "textBox79";
            this.textBox79.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8.5pt; font" +
    "-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox79.Tag = "";
            this.textBox79.Text = "支 　　払 　 者";
            this.textBox79.Top = 6.851575F;
            this.textBox79.Width = 0.2661436F;
            // 
            // line112
            // 
            this.line112.Height = 0.2362218F;
            this.line112.Left = 3.011811F;
            this.line112.LineWeight = 1F;
            this.line112.Name = "line112";
            this.line112.Top = 6.851573F;
            this.line112.Width = 0F;
            this.line112.X1 = 3.011811F;
            this.line112.X2 = 3.011811F;
            this.line112.Y1 = 7.087795F;
            this.line112.Y2 = 6.851573F;
            // 
            // line110
            // 
            this.line110.Height = 0.2362199F;
            this.line110.Left = 2.850394F;
            this.line110.LineWeight = 1F;
            this.line110.Name = "line110";
            this.line110.Top = 6.851575F;
            this.line110.Width = 0F;
            this.line110.X1 = 2.850394F;
            this.line110.X2 = 2.850394F;
            this.line110.Y1 = 7.087795F;
            this.line110.Y2 = 6.851575F;
            // 
            // line109
            // 
            this.line109.Height = 0.2362199F;
            this.line109.Left = 2.69685F;
            this.line109.LineWeight = 1F;
            this.line109.Name = "line109";
            this.line109.Top = 6.851575F;
            this.line109.Width = 0F;
            this.line109.X1 = 2.69685F;
            this.line109.X2 = 2.69685F;
            this.line109.Y1 = 7.087795F;
            this.line109.Y2 = 6.851575F;
            // 
            // line111
            // 
            this.line111.Height = 0.2362199F;
            this.line111.Left = 2.559055F;
            this.line111.LineWeight = 1F;
            this.line111.Name = "line111";
            this.line111.Top = 6.851575F;
            this.line111.Width = 0F;
            this.line111.X1 = 2.559055F;
            this.line111.X2 = 2.559055F;
            this.line111.Y1 = 7.087795F;
            this.line111.Y2 = 6.851575F;
            // 
            // line108
            // 
            this.line108.Height = 0.2362161F;
            this.line108.Left = 2.440945F;
            this.line108.LineWeight = 1F;
            this.line108.Name = "line108";
            this.line108.Top = 6.85158F;
            this.line108.Width = 0F;
            this.line108.X1 = 2.440945F;
            this.line108.X2 = 2.440945F;
            this.line108.Y1 = 7.087796F;
            this.line108.Y2 = 6.85158F;
            // 
            // line107
            // 
            this.line107.Height = 0.2362199F;
            this.line107.Left = 2.30315F;
            this.line107.LineWeight = 1F;
            this.line107.Name = "line107";
            this.line107.Top = 6.851575F;
            this.line107.Width = 0F;
            this.line107.X1 = 2.30315F;
            this.line107.X2 = 2.30315F;
            this.line107.Y1 = 7.087795F;
            this.line107.Y2 = 6.851575F;
            // 
            // line106
            // 
            this.line106.Height = 0.2362199F;
            this.line106.Left = 2.145669F;
            this.line106.LineWeight = 1F;
            this.line106.Name = "line106";
            this.line106.Top = 6.851575F;
            this.line106.Width = 0F;
            this.line106.X1 = 2.145669F;
            this.line106.X2 = 2.145669F;
            this.line106.Y1 = 7.087795F;
            this.line106.Y2 = 6.851575F;
            // 
            // line105
            // 
            this.line105.Height = 0.2362199F;
            this.line105.Left = 2F;
            this.line105.LineWeight = 1F;
            this.line105.Name = "line105";
            this.line105.Top = 6.851575F;
            this.line105.Width = 0F;
            this.line105.X1 = 2F;
            this.line105.X2 = 2F;
            this.line105.Y1 = 7.087795F;
            this.line105.Y2 = 6.851575F;
            // 
            // line104
            // 
            this.line104.Height = 0.2362199F;
            this.line104.Left = 1.850394F;
            this.line104.LineWeight = 1F;
            this.line104.Name = "line104";
            this.line104.Top = 6.851575F;
            this.line104.Width = 0F;
            this.line104.X1 = 1.850394F;
            this.line104.X2 = 1.850394F;
            this.line104.Y1 = 7.087795F;
            this.line104.Y2 = 6.851575F;
            // 
            // line103
            // 
            this.line103.Height = 0.2362199F;
            this.line103.Left = 1.692914F;
            this.line103.LineWeight = 1F;
            this.line103.Name = "line103";
            this.line103.Top = 6.851575F;
            this.line103.Width = 0F;
            this.line103.X1 = 1.692914F;
            this.line103.X2 = 1.692914F;
            this.line103.Y1 = 7.087795F;
            this.line103.Y2 = 6.851575F;
            // 
            // line102
            // 
            this.line102.Height = 0.2362199F;
            this.line102.Left = 1.574803F;
            this.line102.LineWeight = 1F;
            this.line102.Name = "line102";
            this.line102.Top = 6.851575F;
            this.line102.Width = 0F;
            this.line102.X1 = 1.574803F;
            this.line102.X2 = 1.574803F;
            this.line102.Y1 = 7.087795F;
            this.line102.Y2 = 6.851575F;
            // 
            // textBox258
            // 
            this.textBox258.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox258.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox258.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox258.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox258.Height = 0.3125986F;
            this.textBox258.Left = 0.1275591F;
            this.textBox258.Name = "textBox258";
            this.textBox258.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox258.Tag = "";
            this.textBox258.Text = "住宅借入\r\n金等特別\r\n控除の額\r\nの内訳";
            this.textBox258.Top = 3.843307F;
            this.textBox258.Width = 0.4972441F;
            // 
            // textBox140
            // 
            this.textBox140.Height = 0.1299213F;
            this.textBox140.Left = 0.4712599F;
            this.textBox140.Name = "textBox140";
            this.textBox140.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5.6pt; font" +
    "-weight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox140.Tag = "";
            this.textBox140.Text = "(フリガナ)";
            this.textBox140.Top = 4.16811F;
            this.textBox140.Width = 0.4751969F;
            // 
            // textBox52
            // 
            this.textBox52.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox52.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox52.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox52.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox52.Height = 0.4133856F;
            this.textBox52.Left = 0.1275591F;
            this.textBox52.Name = "textBox52";
            this.textBox52.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox52.Tag = "";
            this.textBox52.Text = "控除対象\r\n配偶者";
            this.textBox52.Top = 4.155906F;
            this.textBox52.Width = 0.3448819F;
            // 
            // textBox60
            // 
            this.textBox60.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox60.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox60.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox60.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox60.DataField = "ITEM103";
            this.textBox60.Height = 0.2578741F;
            this.textBox60.Left = 0.9464568F;
            this.textBox60.Name = "textBox60";
            this.textBox60.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5.5pt; font" +
    "-weight: normal; text-align: left; vertical-align: bottom; ddo-char-set: 1";
            this.textBox60.Tag = "";
            this.textBox60.Text = "ITEM103";
            this.textBox60.Top = 4.155906F;
            this.textBox60.Width = 1.258268F;
            // 
            // textBox314
            // 
            this.textBox314.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox314.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox314.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox314.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox314.DataField = "ITEM102";
            this.textBox314.Height = 0.09645708F;
            this.textBox314.Left = 0.9464567F;
            this.textBox314.Name = "textBox314";
            this.textBox314.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5.5pt; font" +
    "-weight: normal; text-align: left; vertical-align: top; ddo-char-set: 1";
            this.textBox314.Tag = "";
            this.textBox314.Text = "ITEM102item[101]";
            this.textBox314.Top = 4.157874F;
            this.textBox314.Width = 1.258268F;
            // 
            // textBox284
            // 
            this.textBox284.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox284.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox284.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox284.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox284.Height = 0.2578743F;
            this.textBox284.Left = 2.204725F;
            this.textBox284.Name = "textBox284";
            this.textBox284.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox284.Tag = "";
            this.textBox284.Text = "区分";
            this.textBox284.Top = 4.155906F;
            this.textBox284.Width = 0.1181102F;
            // 
            // textBox285
            // 
            this.textBox285.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox285.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox285.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox285.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox285.DataField = "ITEM104";
            this.textBox285.Height = 0.2578741F;
            this.textBox285.Left = 2.322835F;
            this.textBox285.Name = "textBox285";
            this.textBox285.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox285.Tag = "";
            this.textBox285.Text = "ITEM104";
            this.textBox285.Top = 4.155906F;
            this.textBox285.Width = 0.2779529F;
            // 
            // textBox149
            // 
            this.textBox149.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox149.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox149.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox149.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox149.Height = 0.4118109F;
            this.textBox149.Left = 2.6008F;
            this.textBox149.Name = "textBox149";
            this.textBox149.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 4.7pt; font" +
    "-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox149.Tag = "";
            this.textBox149.Text = "配偶者の\r\n合計所得";
            this.textBox149.Top = 4.156971F;
            this.textBox149.Width = 0.4937008F;
            // 
            // textBox148
            // 
            this.textBox148.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox148.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox148.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox148.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox148.CanGrow = false;
            this.textBox148.DataField = "ITEM061";
            this.textBox148.Height = 0.4118109F;
            this.textBox148.Left = 3.094488F;
            this.textBox148.Name = "textBox148";
            this.textBox148.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox148.Tag = "";
            this.textBox148.Text = "ITEM061";
            this.textBox148.Top = 4.157085F;
            this.textBox148.Width = 0.4937008F;
            // 
            // label63
            // 
            this.label63.Height = 0.08611111F;
            this.label63.HyperLink = null;
            this.label63.Left = 3.390945F;
            this.label63.Name = "label63";
            this.label63.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label63.Tag = "";
            this.label63.Text = "円";
            this.label63.Top = 4.173229F;
            this.label63.Width = 0.166667F;
            // 
            // label8
            // 
            this.label8.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label8.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label8.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label8.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label8.Height = 0.411811F;
            this.label8.HyperLink = null;
            this.label8.Left = 3.588189F;
            this.label8.Name = "label8";
            this.label8.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 4.7pt; font-weight: normal; text-ali" +
    "gn: center; vertical-align: middle; ddo-char-set: 1";
            this.label8.Tag = "";
            this.label8.Text = "国民年金保険\r\n料等の金額";
            this.label8.Top = 4.157874F;
            this.label8.Width = 0.4937008F;
            // 
            // textBox159
            // 
            this.textBox159.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox159.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox159.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox159.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox159.CanGrow = false;
            this.textBox159.DataField = "ITEM051";
            this.textBox159.Height = 0.411811F;
            this.textBox159.Left = 4.08189F;
            this.textBox159.Name = "textBox159";
            this.textBox159.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox159.Tag = "";
            this.textBox159.Text = "ITEM051";
            this.textBox159.Top = 4.157087F;
            this.textBox159.Width = 0.4937008F;
            // 
            // label62
            // 
            this.label62.Height = 0.08611111F;
            this.label62.HyperLink = null;
            this.label62.Left = 4.396457F;
            this.label62.Name = "label62";
            this.label62.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label62.Tag = "";
            this.label62.Text = "円";
            this.label62.Top = 4.173229F;
            this.label62.Width = 0.166667F;
            // 
            // textBox155
            // 
            this.textBox155.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox155.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox155.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox155.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox155.Height = 0.411811F;
            this.textBox155.Left = 4.575591F;
            this.textBox155.Name = "textBox155";
            this.textBox155.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 4.7pt; font" +
    "-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox155.Tag = "";
            this.textBox155.Text = "旧長期損害保険料の金額";
            this.textBox155.Top = 4.155906F;
            this.textBox155.Width = 0.4649604F;
            // 
            // textBox154
            // 
            this.textBox154.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox154.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox154.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox154.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox154.CanGrow = false;
            this.textBox154.DataField = "ITEM067";
            this.textBox154.Height = 0.4114176F;
            this.textBox154.Left = 5.028347F;
            this.textBox154.Name = "textBox154";
            this.textBox154.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox154.Tag = "";
            this.textBox154.Text = "ITEM067";
            this.textBox154.Top = 4.157874F;
            this.textBox154.Width = 0.5192914F;
            // 
            // label44
            // 
            this.label44.Height = 0.08611111F;
            this.label44.HyperLink = null;
            this.label44.Left = 5.364567F;
            this.label44.Name = "label44";
            this.label44.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label44.Tag = "";
            this.label44.Text = "円";
            this.label44.Top = 4.16811F;
            this.label44.Width = 0.166667F;
            // 
            // line67
            // 
            this.line67.Height = 0.312598F;
            this.line67.Left = 5.547638F;
            this.line67.LineWeight = 1F;
            this.line67.Name = "line67";
            this.line67.Top = 3.838977F;
            this.line67.Width = 0F;
            this.line67.X1 = 5.547638F;
            this.line67.X2 = 5.547638F;
            this.line67.Y1 = 4.151575F;
            this.line67.Y2 = 3.838977F;
            // 
            // label54
            // 
            this.label54.Height = 0.08611111F;
            this.label54.HyperLink = null;
            this.label54.Left = 5.433071F;
            this.label54.Name = "label54";
            this.label54.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 4.5pt; font-weight: normal; text-ali" +
    "gn: right; ddo-char-set: 1";
            this.label54.Tag = "";
            this.label54.Text = "円";
            this.label54.Top = 3.858268F;
            this.label54.Width = 0.09816313F;
            // 
            // label55
            // 
            this.label55.Height = 0.08622047F;
            this.label55.HyperLink = null;
            this.label55.Left = 5.433071F;
            this.label55.Name = "label55";
            this.label55.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 4.5pt; font-weight: normal; text-ali" +
    "gn: right; ddo-char-set: 1";
            this.label55.Tag = "";
            this.label55.Text = "円";
            this.label55.Top = 4.011417F;
            this.label55.Width = 0.0980315F;
            // 
            // label52
            // 
            this.label52.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label52.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label52.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label52.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label52.Height = 0.1555118F;
            this.label52.HyperLink = null;
            this.label52.Left = 4.205512F;
            this.label52.Name = "label52";
            this.label52.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 4.6pt; font-weight: normal; text-ali" +
    "gn: center; vertical-align: middle; ddo-char-set: 1";
            this.label52.Tag = "";
            this.label52.Text = "住宅借入金等\r\n年末残高(1回目)";
            this.label52.Top = 3.844882F;
            this.label52.Width = 0.5763763F;
            // 
            // label53
            // 
            this.label53.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label53.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label53.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label53.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.label53.Height = 0.1555118F;
            this.label53.HyperLink = null;
            this.label53.Left = 4.205512F;
            this.label53.Name = "label53";
            this.label53.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 4.6pt; font-weight: normal; text-ali" +
    "gn: center; vertical-align: middle; ddo-char-set: 1";
            this.label53.Tag = "";
            this.label53.Text = "住宅借入金等\r\n年末残高(2回目)";
            this.label53.Top = 4.000392F;
            this.label53.Width = 0.5763763F;
            // 
            // textBox195
            // 
            this.textBox195.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox195.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox195.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox195.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox195.Height = 0.1377953F;
            this.textBox195.Left = 8.999212F;
            this.textBox195.Name = "textBox195";
            this.textBox195.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: left; vertical-align: bottom; ddo-char-set: 1";
            this.textBox195.Tag = "";
            this.textBox195.Text = "(受給者番号)";
            this.textBox195.Top = 0.2559058F;
            this.textBox195.Width = 2.248031F;
            // 
            // textBox196
            // 
            this.textBox196.CanGrow = false;
            this.textBox196.DataField = "ITEM025";
            this.textBox196.Height = 0.1082677F;
            this.textBox196.Left = 9.70748F;
            this.textBox196.Name = "textBox196";
            this.textBox196.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox196.Tag = "";
            this.textBox196.Text = "ITEM025";
            this.textBox196.Top = 0.2677166F;
            this.textBox196.Width = 1.523228F;
            // 
            // label89
            // 
            this.label89.Height = 0.08611111F;
            this.label89.HyperLink = null;
            this.label89.Left = 11.07126F;
            this.label89.Name = "label89";
            this.label89.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label89.Tag = "";
            this.label89.Text = "円";
            this.label89.Top = 3.844488F;
            this.label89.Width = 0.166667F;
            // 
            // label90
            // 
            this.label90.Height = 0.08611111F;
            this.label90.HyperLink = null;
            this.label90.Left = 11.07047F;
            this.label90.Name = "label90";
            this.label90.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label90.Tag = "";
            this.label90.Text = "円";
            this.label90.Top = 4.000394F;
            this.label90.Width = 0.166667F;
            // 
            // label92
            // 
            this.label92.Height = 0.08611111F;
            this.label92.HyperLink = null;
            this.label92.Left = 11.06417F;
            this.label92.Name = "label92";
            this.label92.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label92.Tag = "";
            this.label92.Text = "円";
            this.label92.Top = 4.168898F;
            this.label92.Width = 0.166667F;
            // 
            // textBox177
            // 
            this.textBox177.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox177.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox177.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox177.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox177.Height = 0.1374011F;
            this.textBox177.Left = 9.243308F;
            this.textBox177.Name = "textBox177";
            this.textBox177.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.textBox177.Tag = "";
            this.textBox177.Text = "(フリガナ)";
            this.textBox177.Top = 0.6708662F;
            this.textBox177.Width = 2.003936F;
            // 
            // textBox240
            // 
            this.textBox240.CanGrow = false;
            this.textBox240.DataField = "ITEM026";
            this.textBox240.Height = 0.0968502F;
            this.textBox240.Left = 9.731103F;
            this.textBox240.Name = "textBox240";
            this.textBox240.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: left; text-justify: auto; vertical-align: middle; ddo" +
    "-char-set: 1";
            this.textBox240.Tag = "";
            this.textBox240.Text = "ITEM026";
            this.textBox240.Top = 0.6905512F;
            this.textBox240.Width = 1.499606F;
            // 
            // textBox321
            // 
            this.textBox321.CanGrow = false;
            this.textBox321.DataField = "ITEM026";
            this.textBox321.Height = 0.1169291F;
            this.textBox321.Left = 4.031497F;
            this.textBox321.Name = "textBox321";
            this.textBox321.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: left; text-justify: auto; vertical-align: middle; ddo" +
    "-char-set: 1";
            this.textBox321.Tag = "";
            this.textBox321.Text = "ITEM026";
            this.textBox321.Top = 0.6897638F;
            this.textBox321.Width = 1.499606F;
            // 
            // textBox9
            // 
            this.textBox9.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox9.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox9.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox9.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox9.Height = 0.1389764F;
            this.textBox9.Left = 3.543701F;
            this.textBox9.Name = "textBox9";
            this.textBox9.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.textBox9.Tag = "";
            this.textBox9.Text = "(フリガナ)";
            this.textBox9.Top = 0.6708662F;
            this.textBox9.Width = 2.003936F;
            // 
            // textBox249
            // 
            this.textBox249.CanGrow = false;
            this.textBox249.DataField = "ITEM026";
            this.textBox249.Height = 0.09763776F;
            this.textBox249.Left = 4.031497F;
            this.textBox249.Name = "textBox249";
            this.textBox249.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: left; text-justify: auto; vertical-align: middle; ddo" +
    "-char-set: 1";
            this.textBox249.Tag = "";
            this.textBox249.Text = "ITEM026";
            this.textBox249.Top = 0.6897638F;
            this.textBox249.Width = 1.499606F;
            // 
            // line1
            // 
            this.line1.Height = 0.122441F;
            this.line1.Left = 9.593307F;
            this.line1.LineWeight = 1F;
            this.line1.Name = "line1";
            this.line1.Top = 0.4003937F;
            this.line1.Width = 1.654723F;
            this.line1.X1 = 9.593307F;
            this.line1.X2 = 11.24803F;
            this.line1.Y1 = 0.5228347F;
            this.line1.Y2 = 0.4003937F;
            // 
            // textBox194
            // 
            this.textBox194.DataField = "ITEM153";
            this.textBox194.Height = 0.1104167F;
            this.textBox194.Left = 0.2362205F;
            this.textBox194.Name = "textBox194";
            this.textBox194.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6.5pt; font" +
    "-weight: normal; text-align: left; ddo-char-set: 1";
            this.textBox194.Tag = "";
            this.textBox194.Text = "ITEM153";
            this.textBox194.Top = 3.248425F;
            this.textBox194.Width = 2.519685F;
            // 
            // textBox272
            // 
            this.textBox272.DataField = "ITEM153";
            this.textBox272.Height = 0.1104167F;
            this.textBox272.Left = 5.935827F;
            this.textBox272.Name = "textBox272";
            this.textBox272.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6.5pt; font" +
    "-weight: normal; text-align: left; ddo-char-set: 1";
            this.textBox272.Tag = "";
            this.textBox272.Text = "ITEM153";
            this.textBox272.Top = 3.248425F;
            this.textBox272.Width = 2.519685F;
            // 
            // KYUR3042R
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.1181102F;
            this.PageSettings.Margins.Left = 0.1968504F;
            this.PageSettings.Margins.Right = 0F;
            this.PageSettings.Margins.Top = 0.2755905F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
            this.PageSettings.PaperHeight = 11.69291F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.267716F;
            this.PrintWidth = 11.29528F;
            this.Sections.Add(this.detail);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.テキスト3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label91)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox105)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox192)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox54)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox66)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox72)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox78)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox81)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox82)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox83)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox85)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox86)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox88)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox87)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox89)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM025)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト61)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM034)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト63)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aaa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト65)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM036)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト71)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM037)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル177)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト124)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト138)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox90)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox91)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox92)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox93)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox94)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox95)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox96)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox97)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox98)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox99)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox100)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox101)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox102)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox104)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox106)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox107)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox108)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox109)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox110)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox111)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox112)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox113)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox114)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox115)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox116)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox117)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox118)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox119)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox120)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox121)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox122)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox123)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox124)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox125)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox126)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox128)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox129)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox130)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox131)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox132)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox133)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox134)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox135)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox136)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox170)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox171)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox172)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox80)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox165)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox103)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox137)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox138)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label83)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox55)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox58)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox59)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox61)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox63)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox64)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox65)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox67)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox70)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox71)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox73)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox75)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox76)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox77)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox144)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox145)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox146)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox147)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox150)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox151)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox152)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox153)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox156)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox157)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox158)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox257)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox259)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox264)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox268)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox269)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox273)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox274)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox278)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox279)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox283)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox286)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox287)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox288)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox289)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox290)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox291)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox292)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox293)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox294)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox296)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox297)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox298)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox299)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox300)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox301)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox302)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox303)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox160)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox161)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox162)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox163)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox164)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox166)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox167)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox168)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox169)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox304)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト163)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト167)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト168)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label56)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox305)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox306)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox316)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label57)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox317)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label58)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox318)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label59)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox319)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label60)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox320)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label61)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox322)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox323)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox324)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox325)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox326)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox142)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox141)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox56)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox311)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox57)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox69)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox143)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox139)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox62)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox313)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox68)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox310)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox74)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox307)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox276)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox281)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox266)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox271)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox260)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox261)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox262)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox263)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox270)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox315)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox265)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox312)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox275)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox309)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox280)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox308)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox127)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox173)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox174)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox175)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox176)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox178)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox179)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox180)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox181)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox182)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox183)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox184)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox185)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox186)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox187)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox188)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox189)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox190)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox191)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox193)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox197)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox198)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox199)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox200)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox201)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox202)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox203)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox204)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox205)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox206)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox207)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox208)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox209)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox210)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox211)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox212)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox213)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox214)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox215)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox216)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox217)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox218)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox219)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox220)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox221)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox222)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox223)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox224)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox225)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox226)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox227)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox228)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox229)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox230)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox231)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox232)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox233)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox234)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox235)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox236)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox237)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox238)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox239)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox241)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox242)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox243)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox244)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox245)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox246)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox247)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox248)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox250)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox251)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox252)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox253)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox254)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox255)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox256)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox327)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox328)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox329)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox330)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label64)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label65)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label66)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label67)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label68)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label69)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox331)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox332)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label70)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label71)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label72)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label73)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label74)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label75)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label76)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label77)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox333)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label78)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox334)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label79)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox335)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label80)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox336)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label81)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox337)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label82)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox338)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label84)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox339)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox340)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox341)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox342)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox343)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox344)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox345)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox347)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label85)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox348)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox349)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label86)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox351)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox353)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox354)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label87)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label88)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox355)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox356)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox358)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox359)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox360)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox362)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox363)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox365)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox366)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox367)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox369)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox370)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox371)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox373)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox374)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox376)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox377)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox379)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox380)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox382)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox383)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox384)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox385)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox386)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox387)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox388)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox389)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox390)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox391)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox392)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox393)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox394)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox395)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox396)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox397)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox398)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox400)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox402)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox403)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox404)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox405)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox406)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox407)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox408)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox409)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox410)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox411)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox412)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox413)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox414)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox415)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox416)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox417)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox418)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox419)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox420)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox421)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox422)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox423)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox424)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox425)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox426)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox427)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox428)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox429)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox430)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox431)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox432)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox433)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox434)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox435)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox438)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox439)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox440)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox441)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox442)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox443)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox444)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox445)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox446)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox447)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox448)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox449)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox450)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox451)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox452)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox453)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox454)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox455)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox456)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox457)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox458)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox459)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox460)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox461)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox462)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox463)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox464)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox465)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox466)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox467)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox468)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox469)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox470)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox471)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox472)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox473)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox474)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox475)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox476)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox477)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox478)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox479)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox480)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox481)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox482)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox483)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox484)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox485)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox486)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox487)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox488)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox489)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox490)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox491)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox492)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox352)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox401)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox346)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox267)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox295)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox350)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox84)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox79)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox258)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox140)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox60)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox314)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox284)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox285)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox149)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox148)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label63)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox159)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label62)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox155)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox154)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label54)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label55)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox195)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox196)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label89)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label90)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label92)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox177)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox240)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox321)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox249)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox194)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox272)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox192;
        private GrapeCity.ActiveReports.SectionReportModel.Label label12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox13;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox14;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox15;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox16;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox17;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox18;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox19;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox20;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox21;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox22;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox23;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox24;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox52;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox53;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox54;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox60;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox66;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox72;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox78;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox79;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox80;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox81;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox82;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox83;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox84;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox85;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox86;
        private GrapeCity.ActiveReports.SectionReportModel.Label label2;
        private GrapeCity.ActiveReports.SectionReportModel.Label label3;
        private GrapeCity.ActiveReports.SectionReportModel.Label label4;
        private GrapeCity.ActiveReports.SectionReportModel.Label label5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox87;
        private GrapeCity.ActiveReports.SectionReportModel.Label テキスト3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox88;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox89;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM025;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト61;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM034;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト63;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox aaa;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト65;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM036;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト71;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM037;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル177;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト39;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox25;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト124;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox26;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox27;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox28;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト138;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox29;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox30;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox31;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox32;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox34;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox35;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox36;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox37;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox38;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox90;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox91;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox92;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox93;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox94;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox95;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox96;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox97;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox98;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox99;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox100;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox101;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox102;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox104;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox105;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox106;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox107;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox108;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox109;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox110;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox111;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox112;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox113;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox114;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox115;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox116;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox117;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox118;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox119;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox120;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox121;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox122;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox123;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox124;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox125;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox126;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox128;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox129;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox130;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox131;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox132;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox133;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox134;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox135;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox136;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox170;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox171;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox172;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox2;
        private GrapeCity.ActiveReports.SectionReportModel.Label label6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox33;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox39;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox40;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox41;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox42;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox43;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox44;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox165;
        private GrapeCity.ActiveReports.SectionReportModel.Label label17;
        private GrapeCity.ActiveReports.SectionReportModel.Label label18;
        private GrapeCity.ActiveReports.SectionReportModel.Label label19;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox45;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox46;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox47;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox49;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox50;
        private GrapeCity.ActiveReports.SectionReportModel.Label label14;
        private GrapeCity.ActiveReports.SectionReportModel.Label label15;
        private GrapeCity.ActiveReports.SectionReportModel.Label label16;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox51;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox103;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox137;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox138;
        private GrapeCity.ActiveReports.SectionReportModel.Label label9;
        private GrapeCity.ActiveReports.SectionReportModel.Label label10;
        private GrapeCity.ActiveReports.SectionReportModel.Label label13;
        private GrapeCity.ActiveReports.SectionReportModel.Label label11;
        private GrapeCity.ActiveReports.SectionReportModel.Label label83;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox139;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox140;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox141;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox142;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox143;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox55;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox56;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox57;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox58;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox59;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox61;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox62;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox63;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox64;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox65;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox67;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox68;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox69;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox70;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox71;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox73;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox74;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox75;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox76;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox77;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox144;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox145;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox146;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox147;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox148;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox149;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox150;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox151;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox152;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox153;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox154;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox155;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox156;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox157;
        private GrapeCity.ActiveReports.SectionReportModel.Label label7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox158;
        private GrapeCity.ActiveReports.SectionReportModel.Label label8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox159;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox160;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox161;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox162;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox163;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox164;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox166;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox167;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox168;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox169;
        private GrapeCity.ActiveReports.SectionReportModel.Label label20;
        private GrapeCity.ActiveReports.SectionReportModel.Label label22;
        private GrapeCity.ActiveReports.SectionReportModel.Label label23;
        private GrapeCity.ActiveReports.SectionReportModel.Label label24;
        private GrapeCity.ActiveReports.SectionReportModel.Label label25;
        private GrapeCity.ActiveReports.SectionReportModel.Label label26;
        private GrapeCity.ActiveReports.SectionReportModel.Label label45;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox257;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox258;
        private GrapeCity.ActiveReports.SectionReportModel.Label label46;
        private GrapeCity.ActiveReports.SectionReportModel.Label label47;
        private GrapeCity.ActiveReports.SectionReportModel.Label label48;
        private GrapeCity.ActiveReports.SectionReportModel.Label label49;
        private GrapeCity.ActiveReports.SectionReportModel.Label label50;
        private GrapeCity.ActiveReports.SectionReportModel.Label label51;
        private GrapeCity.ActiveReports.SectionReportModel.Label label52;
        private GrapeCity.ActiveReports.SectionReportModel.Label label53;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox259;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox260;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox261;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox262;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox263;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox264;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox265;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox266;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox268;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox269;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox270;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox271;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox273;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox274;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox275;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox276;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox278;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox279;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox280;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox281;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox283;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox284;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox285;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox286;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox287;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox288;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox289;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox290;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox291;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox292;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox293;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox294;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox295;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox296;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox297;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox298;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox299;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox300;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox301;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox302;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox304;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト163;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト167;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト168;
        private GrapeCity.ActiveReports.SectionReportModel.Label label56;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox305;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox306;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox307;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox308;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox309;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox310;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox311;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox312;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox313;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox314;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox315;
        private GrapeCity.ActiveReports.SectionReportModel.Line line2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox316;
        private GrapeCity.ActiveReports.SectionReportModel.Label label57;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox317;
        private GrapeCity.ActiveReports.SectionReportModel.Label label58;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox318;
        private GrapeCity.ActiveReports.SectionReportModel.Label label59;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox319;
        private GrapeCity.ActiveReports.SectionReportModel.Label label60;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox320;
        private GrapeCity.ActiveReports.SectionReportModel.Label label61;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox321;
        private GrapeCity.ActiveReports.SectionReportModel.Line line3;
        private GrapeCity.ActiveReports.SectionReportModel.Line line4;
        private GrapeCity.ActiveReports.SectionReportModel.Line line5;
        private GrapeCity.ActiveReports.SectionReportModel.Line line6;
        private GrapeCity.ActiveReports.SectionReportModel.Line line7;
        private GrapeCity.ActiveReports.SectionReportModel.Line line8;
        private GrapeCity.ActiveReports.SectionReportModel.Line line9;
        private GrapeCity.ActiveReports.SectionReportModel.Line line10;
        private GrapeCity.ActiveReports.SectionReportModel.Line line11;
        private GrapeCity.ActiveReports.SectionReportModel.Line line12;
        private GrapeCity.ActiveReports.SectionReportModel.Line line13;
        private GrapeCity.ActiveReports.SectionReportModel.Line line14;
        private GrapeCity.ActiveReports.SectionReportModel.Line line15;
        private GrapeCity.ActiveReports.SectionReportModel.Line line16;
        private GrapeCity.ActiveReports.SectionReportModel.Line line17;
        private GrapeCity.ActiveReports.SectionReportModel.Line line18;
        private GrapeCity.ActiveReports.SectionReportModel.Line line19;
        private GrapeCity.ActiveReports.SectionReportModel.Line line20;
        private GrapeCity.ActiveReports.SectionReportModel.Line line21;
        private GrapeCity.ActiveReports.SectionReportModel.Line line22;
        private GrapeCity.ActiveReports.SectionReportModel.Line line23;
        private GrapeCity.ActiveReports.SectionReportModel.Line line24;
        private GrapeCity.ActiveReports.SectionReportModel.Line line25;
        private GrapeCity.ActiveReports.SectionReportModel.Line line26;
        private GrapeCity.ActiveReports.SectionReportModel.Line line27;
        private GrapeCity.ActiveReports.SectionReportModel.Line line28;
        private GrapeCity.ActiveReports.SectionReportModel.Line line29;
        private GrapeCity.ActiveReports.SectionReportModel.Line line30;
        private GrapeCity.ActiveReports.SectionReportModel.Line line31;
        private GrapeCity.ActiveReports.SectionReportModel.Line line32;
        private GrapeCity.ActiveReports.SectionReportModel.Line line33;
        private GrapeCity.ActiveReports.SectionReportModel.Line line34;
        private GrapeCity.ActiveReports.SectionReportModel.Line line35;
        private GrapeCity.ActiveReports.SectionReportModel.Line line36;
        private GrapeCity.ActiveReports.SectionReportModel.Line line37;
        private GrapeCity.ActiveReports.SectionReportModel.Line line38;
        private GrapeCity.ActiveReports.SectionReportModel.Line line39;
        private GrapeCity.ActiveReports.SectionReportModel.Line line40;
        private GrapeCity.ActiveReports.SectionReportModel.Line line41;
        private GrapeCity.ActiveReports.SectionReportModel.Line line42;
        private GrapeCity.ActiveReports.SectionReportModel.Line line43;
        private GrapeCity.ActiveReports.SectionReportModel.Line line44;
        private GrapeCity.ActiveReports.SectionReportModel.Line line45;
        private GrapeCity.ActiveReports.SectionReportModel.Line line46;
        private GrapeCity.ActiveReports.SectionReportModel.Line line47;
        private GrapeCity.ActiveReports.SectionReportModel.Line line48;
        private GrapeCity.ActiveReports.SectionReportModel.Line line49;
        private GrapeCity.ActiveReports.SectionReportModel.Line line50;
        private GrapeCity.ActiveReports.SectionReportModel.Line line51;
        private GrapeCity.ActiveReports.SectionReportModel.Line line73;
        private GrapeCity.ActiveReports.SectionReportModel.Line line96;
        private GrapeCity.ActiveReports.SectionReportModel.Line line97;
        private GrapeCity.ActiveReports.SectionReportModel.Line line98;
        private GrapeCity.ActiveReports.SectionReportModel.Line line99;
        private GrapeCity.ActiveReports.SectionReportModel.Line line100;
        private GrapeCity.ActiveReports.SectionReportModel.Line line101;
        private GrapeCity.ActiveReports.SectionReportModel.Line line102;
        private GrapeCity.ActiveReports.SectionReportModel.Line line103;
        private GrapeCity.ActiveReports.SectionReportModel.Line line104;
        private GrapeCity.ActiveReports.SectionReportModel.Line line105;
        private GrapeCity.ActiveReports.SectionReportModel.Line line106;
        private GrapeCity.ActiveReports.SectionReportModel.Line line107;
        private GrapeCity.ActiveReports.SectionReportModel.Line line109;
        private GrapeCity.ActiveReports.SectionReportModel.Line line110;
        private GrapeCity.ActiveReports.SectionReportModel.Line line111;
        private GrapeCity.ActiveReports.SectionReportModel.Line line112;
        private GrapeCity.ActiveReports.SectionReportModel.Line line113;
        private GrapeCity.ActiveReports.SectionReportModel.Line line114;
        private GrapeCity.ActiveReports.SectionReportModel.Line line115;
        private GrapeCity.ActiveReports.SectionReportModel.Line line116;
        private GrapeCity.ActiveReports.SectionReportModel.Line line117;
        private GrapeCity.ActiveReports.SectionReportModel.Line line118;
        private GrapeCity.ActiveReports.SectionReportModel.Line line119;
        private GrapeCity.ActiveReports.SectionReportModel.Line line120;
        private GrapeCity.ActiveReports.SectionReportModel.Line line121;
        private GrapeCity.ActiveReports.SectionReportModel.Line line122;
        private GrapeCity.ActiveReports.SectionReportModel.Line line123;
        private GrapeCity.ActiveReports.SectionReportModel.Line line108;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox322;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox323;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox324;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox325;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox326;
        private GrapeCity.ActiveReports.SectionReportModel.Line line124;
        private GrapeCity.ActiveReports.SectionReportModel.Label label62;
        private GrapeCity.ActiveReports.SectionReportModel.Label label63;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox127;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox173;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox174;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox175;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox176;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox177;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox178;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox179;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox180;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox181;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox182;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox183;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox184;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox185;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox186;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox187;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox188;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox189;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox190;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox191;
        private GrapeCity.ActiveReports.SectionReportModel.Label label21;
        private GrapeCity.ActiveReports.SectionReportModel.Label label27;
        private GrapeCity.ActiveReports.SectionReportModel.Label label28;
        private GrapeCity.ActiveReports.SectionReportModel.Label label29;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox193;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox195;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox196;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox197;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox198;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox199;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox200;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox201;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox202;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox203;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox204;
        private GrapeCity.ActiveReports.SectionReportModel.Label label30;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox205;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox206;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox207;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox208;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox209;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox210;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox211;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox212;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox213;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox214;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox215;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox216;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox217;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox218;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox219;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox220;
        private GrapeCity.ActiveReports.SectionReportModel.Label label31;
        private GrapeCity.ActiveReports.SectionReportModel.Label label32;
        private GrapeCity.ActiveReports.SectionReportModel.Label label33;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox221;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox222;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox223;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox224;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox225;
        private GrapeCity.ActiveReports.SectionReportModel.Label label34;
        private GrapeCity.ActiveReports.SectionReportModel.Label label35;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox226;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox227;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox228;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox229;
        private GrapeCity.ActiveReports.SectionReportModel.Label label36;
        private GrapeCity.ActiveReports.SectionReportModel.Label label37;
        private GrapeCity.ActiveReports.SectionReportModel.Label label38;
        private GrapeCity.ActiveReports.SectionReportModel.Label label39;
        private GrapeCity.ActiveReports.SectionReportModel.Label label40;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox230;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox231;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox232;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox233;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox234;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox235;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox236;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox237;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox238;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox239;
        private GrapeCity.ActiveReports.SectionReportModel.Label label41;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox240;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox241;
        private GrapeCity.ActiveReports.SectionReportModel.Line line135;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox242;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox243;
        private GrapeCity.ActiveReports.SectionReportModel.Label label42;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox244;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox245;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox246;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox247;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox248;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox250;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox251;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox252;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox253;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox254;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox255;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox256;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox327;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox328;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox329;
        private GrapeCity.ActiveReports.SectionReportModel.Label label43;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox330;
        private GrapeCity.ActiveReports.SectionReportModel.Label label64;
        private GrapeCity.ActiveReports.SectionReportModel.Label label65;
        private GrapeCity.ActiveReports.SectionReportModel.Label label66;
        private GrapeCity.ActiveReports.SectionReportModel.Label label67;
        private GrapeCity.ActiveReports.SectionReportModel.Label label68;
        private GrapeCity.ActiveReports.SectionReportModel.Label label69;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox331;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox332;
        private GrapeCity.ActiveReports.SectionReportModel.Label label70;
        private GrapeCity.ActiveReports.SectionReportModel.Label label71;
        private GrapeCity.ActiveReports.SectionReportModel.Label label72;
        private GrapeCity.ActiveReports.SectionReportModel.Label label73;
        private GrapeCity.ActiveReports.SectionReportModel.Label label74;
        private GrapeCity.ActiveReports.SectionReportModel.Label label75;
        private GrapeCity.ActiveReports.SectionReportModel.Label label76;
        private GrapeCity.ActiveReports.SectionReportModel.Label label77;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox333;
        private GrapeCity.ActiveReports.SectionReportModel.Label label78;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox334;
        private GrapeCity.ActiveReports.SectionReportModel.Label label79;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox335;
        private GrapeCity.ActiveReports.SectionReportModel.Label label80;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox336;
        private GrapeCity.ActiveReports.SectionReportModel.Label label81;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox337;
        private GrapeCity.ActiveReports.SectionReportModel.Label label82;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox338;
        private GrapeCity.ActiveReports.SectionReportModel.Label label84;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox339;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox340;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox341;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox342;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox343;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox344;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox345;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox347;
        private GrapeCity.ActiveReports.SectionReportModel.Label label85;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox348;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox349;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox350;
        private GrapeCity.ActiveReports.SectionReportModel.Label label86;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox351;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox353;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox354;
        private GrapeCity.ActiveReports.SectionReportModel.Label label87;
        private GrapeCity.ActiveReports.SectionReportModel.Label label88;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox355;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox356;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox358;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox359;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox360;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox362;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox363;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox365;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox366;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox367;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox369;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox370;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox371;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox373;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox374;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox376;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox377;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox379;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox380;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox382;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox383;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox384;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox385;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox386;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox387;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox388;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox389;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox390;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox391;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox392;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox393;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox394;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox395;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox396;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox397;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox398;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox400;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox402;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox403;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox404;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox405;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox406;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox407;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox408;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox409;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox410;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox411;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox412;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox413;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox414;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox415;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox416;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox417;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox418;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox419;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox420;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox421;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox422;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox423;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox424;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox425;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox426;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox427;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox428;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox429;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox430;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox431;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox432;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox433;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox434;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox435;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox438;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox439;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox440;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox441;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox442;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox443;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox444;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox445;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox446;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox447;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox448;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox449;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox450;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox451;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox452;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox453;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox454;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox455;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox456;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox457;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox458;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox459;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox460;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox461;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox462;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox463;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox464;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox465;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox466;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox467;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox468;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox469;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox470;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox471;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox472;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox473;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox474;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox475;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox476;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox477;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox478;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox479;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox480;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox481;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox482;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox483;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox484;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox485;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox486;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox487;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox488;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox489;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox490;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox491;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox352;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox401;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox346;
        private GrapeCity.ActiveReports.SectionReportModel.Label label91;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox303;
        private GrapeCity.ActiveReports.SectionReportModel.Line line52;
        private GrapeCity.ActiveReports.SectionReportModel.Line line53;
        private GrapeCity.ActiveReports.SectionReportModel.Line line54;
        private GrapeCity.ActiveReports.SectionReportModel.Line line55;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox492;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox267;
        private GrapeCity.ActiveReports.SectionReportModel.Line line56;
        private GrapeCity.ActiveReports.SectionReportModel.Line line57;
        private GrapeCity.ActiveReports.SectionReportModel.Line line58;
        private GrapeCity.ActiveReports.SectionReportModel.Line line59;
        private GrapeCity.ActiveReports.SectionReportModel.Line line60;
        private GrapeCity.ActiveReports.SectionReportModel.Line line61;
        private GrapeCity.ActiveReports.SectionReportModel.Line line62;
        private GrapeCity.ActiveReports.SectionReportModel.Line line63;
        private GrapeCity.ActiveReports.SectionReportModel.Line line64;
        private GrapeCity.ActiveReports.SectionReportModel.Line line65;
        private GrapeCity.ActiveReports.SectionReportModel.Line line66;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox48;
        private GrapeCity.ActiveReports.SectionReportModel.Label label44;
        private GrapeCity.ActiveReports.SectionReportModel.Line line67;
        private GrapeCity.ActiveReports.SectionReportModel.Label label54;
        private GrapeCity.ActiveReports.SectionReportModel.Label label55;
        private GrapeCity.ActiveReports.SectionReportModel.Label label89;
        private GrapeCity.ActiveReports.SectionReportModel.Label label90;
        private GrapeCity.ActiveReports.SectionReportModel.Label label92;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox249;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox194;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox272;

    }
}
