﻿namespace jp.co.fsi.ky.kynr1021
{
    /// <summary>
    /// KYNR1021R の帳票
    /// </summary>
    partial class KYNR1021R
    {
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(KYNR1021R));
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.textBox218 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox226 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox227 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox228 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox189 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox188 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox187 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox186 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox185 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト54 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM011 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト28 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.テキスト3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ITEM011CP = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル138 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル139 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.テキスト156 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト158 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト159 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト160 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト161 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト162 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト163 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト164 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト165 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト166 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト167 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト168 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト169 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト170 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト171 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM045 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル176 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ITEM068 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM069 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM070 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM071 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM072 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM073 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM074 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM075 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM076 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM077 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM078 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM079 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM081 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM085 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM086 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM087 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM088 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト27 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト30 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト32 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト34 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM030 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト39 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト47 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト48 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM096 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM097 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト52 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM022 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM023 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM024 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト58 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM029 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト60 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト61 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト62 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト63 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト64 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト65 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM034 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM035 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.aaa = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.bbb = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM036 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト71 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM037 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト73 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト74 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト75 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト76 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト77 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト78 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト79 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト80 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト81 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト82 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト83 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト84 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト85 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト86 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト87 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト88 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト89 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト90 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト91 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM089 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM090 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM091 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM092 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM093 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト97 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト98 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト99 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト100 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト101 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト102 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM094 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト104 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM095 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト106 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト107 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM031 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM032 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM033 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM049 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM048 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト121 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM047 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト124 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM038 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.a = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM040 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM039 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM041 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM042 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM043 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM044 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト138 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM064 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM065 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM061 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM066 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM062 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM067 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト153 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM063 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト155 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト157 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox21 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM098 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト40 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ITEM050 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル159 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ITEM051 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM052 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM053 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM054 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM055 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM058 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM056 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM059 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM057 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM060 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト35 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル171 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル172 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル173 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ITEM046 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル177 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル178 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル182 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル183 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル184 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル185 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル186 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル187 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル188 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル189 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル190 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル191 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル192 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル193 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル194 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル195 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ITEM025 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM026 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM027 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM028 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM082 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM083 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM084 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox22 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox23 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox24 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox25 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox26 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox27 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox28 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox29 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox30 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox31 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox32 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox33 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox34 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox35 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox36 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox37 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox38 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox39 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox40 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox41 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox42 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox43 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox44 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox45 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox46 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox47 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox48 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox49 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox50 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox52 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox53 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox54 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox55 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox56 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox57 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox58 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox59 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox60 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox61 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox62 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox63 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox64 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox65 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox66 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox67 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox68 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox69 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox70 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox71 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox72 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox73 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox74 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox75 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox76 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox77 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox78 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox79 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox80 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox81 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox82 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox83 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox84 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox85 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox86 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox87 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox88 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox89 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox90 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox91 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox92 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox93 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox94 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox95 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox96 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox97 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox98 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox99 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox100 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox101 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox102 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox103 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox104 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox105 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox106 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox107 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox108 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox109 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox110 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox111 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox112 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox113 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox114 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox115 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox116 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox117 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox118 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox119 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox120 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox121 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox124 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox126 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox128 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox129 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox130 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox131 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox132 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox133 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox134 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox135 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox136 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox137 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox138 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox139 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox140 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox141 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox142 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox143 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox144 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox145 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox146 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox147 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox148 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox149 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox150 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox151 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox152 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox153 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox154 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox155 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox156 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox157 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox158 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox159 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox160 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox161 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox162 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox163 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox164 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox165 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label25 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label26 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox166 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox167 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox168 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox169 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox170 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox171 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox172 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.shape1 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.shape2 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.textBox173 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox174 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox175 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox176 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox177 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox178 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox179 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox180 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox181 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox182 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox183 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox184 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox190 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox191 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox192 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox193 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox194 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox195 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox196 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox197 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label27 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox198 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label28 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox199 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox200 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox201 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox202 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox203 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox204 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox205 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox206 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox207 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox208 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox209 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox210 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox211 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox212 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox213 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox214 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox215 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox216 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox217 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox219 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox220 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox221 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox222 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox223 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox224 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox225 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox229 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox230 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox231 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox232 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox233 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox234 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox235 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox236 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox237 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox238 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox239 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox240 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox241 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox242 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox243 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox244 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox245 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox246 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox247 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox248 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox249 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox250 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox251 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox252 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox253 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox254 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox255 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox256 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox257 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox258 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox259 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox260 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox261 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox262 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox263 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox264 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox265 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox266 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox267 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox268 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox269 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox270 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox271 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox272 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox273 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox274 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox275 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox276 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox277 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox278 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox279 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox280 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox281 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox282 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox283 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox284 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox285 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox286 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox287 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox288 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox289 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox290 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox291 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox292 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox293 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox294 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox295 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox296 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox297 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox298 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox299 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox300 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox301 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox302 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox303 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox304 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox305 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox306 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox307 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox308 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox309 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox310 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox311 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox312 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox313 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox314 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox315 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox316 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox317 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox318 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox319 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox320 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox321 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox322 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox323 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox474 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox475 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox476 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox477 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox478 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox479 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label29 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox480 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label30 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox481 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox482 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox483 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox484 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox485 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox486 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox487 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox488 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox489 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox490 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label31 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label32 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label33 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label34 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox491 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label35 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label36 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label40 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label41 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label42 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label44 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label45 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label46 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label47 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label48 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label49 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label50 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label51 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label52 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label79 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox492 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox493 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox494 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox495 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox496 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox497 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox498 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.shape3 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.shape5 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.shape6 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.shape9 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.shape11 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.shape12 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.shape15 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.shape17 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.shape19 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.shape21 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.shape26 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.shape27 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.shape28 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.label80 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox51 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox122 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox123 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox125 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label81 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label82 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label83 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label56 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label86 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label37 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label87 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label38 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label39 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label88 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label89 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label43 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label90 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label91 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label92 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label93 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.shape20 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.label94 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label95 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox127 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox324 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox325 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox326 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox327 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox328 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox329 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox330 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox331 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox332 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox333 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox334 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox335 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox336 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox337 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox338 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox339 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox340 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox341 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox342 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox343 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox344 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox345 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox346 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox347 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox348 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox349 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox350 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label53 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox351 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label54 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox352 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox353 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox354 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox355 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox356 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox357 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox358 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox359 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox360 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox361 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox362 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox363 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox364 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox365 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox366 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox367 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox368 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox369 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox370 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox371 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox372 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox373 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox374 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox375 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox376 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox377 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox378 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox379 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox380 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox381 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox382 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox383 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox384 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox385 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox386 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox387 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox388 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox389 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox390 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox391 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox392 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox393 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox394 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox395 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox396 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox397 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox398 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox399 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox400 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox401 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox402 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox403 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox404 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox405 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox406 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox407 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox408 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox409 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox410 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox411 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox412 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox413 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox414 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox415 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox416 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox417 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox418 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox419 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox420 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox421 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox422 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox423 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox424 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox425 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox426 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox427 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox428 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox429 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox430 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox431 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox432 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox433 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox434 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox435 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox436 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox437 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox438 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox439 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox440 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox441 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox442 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox443 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox444 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox445 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox446 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox447 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox448 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox449 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox450 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox451 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox452 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox453 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox454 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox455 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox456 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox457 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox458 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox459 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox460 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox461 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox462 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox463 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox464 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox465 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox466 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox467 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox468 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox469 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox470 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox471 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox472 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox473 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox499 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox500 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox501 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox502 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox503 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label55 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox504 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label57 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox505 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox506 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox507 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox508 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox509 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox510 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox511 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox512 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox513 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox514 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label58 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label59 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label60 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label61 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox515 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label62 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label63 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label64 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label65 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label66 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label67 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label68 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label69 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label70 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label71 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label72 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label73 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label74 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label75 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label76 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox516 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox517 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox518 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox519 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox520 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox521 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox522 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.shape4 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.shape7 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.shape8 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.shape10 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.shape13 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.shape14 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.shape16 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.shape18 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.shape22 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.shape23 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.shape24 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.shape25 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.shape29 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.label77 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label78 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label84 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label85 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label96 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label97 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label98 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label99 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label100 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label101 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label102 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label103 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label104 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label105 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.shape30 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.label106 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label107 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            ((System.ComponentModel.ISupportInitialize)(this.textBox218)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox226)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox227)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox228)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox189)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox188)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox187)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox186)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox185)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト54)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM011)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM011CP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル138)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル139)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト156)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト158)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト159)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト160)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト161)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト162)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト163)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト164)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト165)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト166)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト167)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト168)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト169)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト170)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト171)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM045)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル176)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM068)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM069)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM070)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM071)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM072)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM073)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM074)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM075)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM076)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM077)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM078)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM079)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM081)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM085)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM086)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM087)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM088)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM030)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM096)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM097)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM022)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM023)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM024)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト58)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM029)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト60)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト61)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト62)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト63)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト64)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト65)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM034)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM035)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aaa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bbb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM036)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト71)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM037)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト73)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト74)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト75)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト76)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト77)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト78)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト79)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト80)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト81)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト82)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト83)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト84)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト85)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト86)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト87)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト88)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト89)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト90)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト91)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM089)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM090)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM091)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM092)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM093)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト97)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト98)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト99)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト100)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト101)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト102)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM094)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト104)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM095)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト106)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト107)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM031)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM032)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM033)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM049)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM048)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト121)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM047)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト124)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM038)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.a)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM040)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM039)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM041)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM042)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM043)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM044)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト138)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM064)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM065)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM061)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM066)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM062)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM067)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト153)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM063)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト155)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト157)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM098)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM050)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル159)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM051)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM052)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM053)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM054)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM055)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM058)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM056)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM059)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM057)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM060)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル171)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル172)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル173)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM046)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル177)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル178)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル182)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル183)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル184)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル185)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル186)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル187)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル188)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル189)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル190)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル191)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル192)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル193)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル194)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル195)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM025)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM026)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM027)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM028)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM082)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM083)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM084)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox54)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox55)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox56)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox57)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox58)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox59)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox60)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox61)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox62)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox63)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox64)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox65)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox66)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox67)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox68)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox69)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox70)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox71)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox72)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox73)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox74)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox75)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox76)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox77)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox78)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox79)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox80)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox81)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox82)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox83)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox84)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox85)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox86)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox87)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox88)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox89)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox90)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox91)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox92)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox93)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox94)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox95)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox96)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox97)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox98)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox99)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox100)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox101)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox102)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox103)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox104)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox105)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox106)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox107)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox108)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox109)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox110)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox111)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox112)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox113)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox114)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox115)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox116)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox117)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox118)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox119)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox120)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox121)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox124)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox126)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox128)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox129)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox130)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox131)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox132)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox133)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox134)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox135)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox136)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox137)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox138)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox139)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox140)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox141)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox142)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox143)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox144)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox145)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox146)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox147)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox148)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox149)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox150)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox151)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox152)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox153)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox154)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox155)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox156)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox157)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox158)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox159)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox160)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox161)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox162)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox163)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox164)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox165)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox166)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox167)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox168)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox169)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox170)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox171)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox172)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox173)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox174)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox175)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox176)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox177)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox178)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox179)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox180)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox181)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox182)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox183)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox184)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox190)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox191)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox192)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox193)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox194)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox195)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox196)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox197)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox198)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox199)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox200)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox201)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox202)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox203)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox204)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox205)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox206)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox207)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox208)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox209)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox210)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox211)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox212)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox213)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox214)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox215)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox216)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox217)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox219)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox220)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox221)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox222)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox223)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox224)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox225)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox229)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox230)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox231)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox232)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox233)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox234)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox235)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox236)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox237)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox238)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox239)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox240)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox241)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox242)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox243)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox244)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox245)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox246)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox247)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox248)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox249)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox250)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox251)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox252)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox253)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox254)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox255)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox256)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox257)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox258)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox259)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox260)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox261)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox262)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox263)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox264)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox265)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox266)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox267)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox268)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox269)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox270)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox271)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox272)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox273)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox274)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox275)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox276)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox277)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox278)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox279)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox280)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox281)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox282)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox283)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox284)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox285)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox286)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox287)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox288)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox289)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox290)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox291)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox292)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox293)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox294)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox295)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox296)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox297)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox298)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox299)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox300)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox301)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox302)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox303)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox304)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox305)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox306)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox307)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox308)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox309)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox310)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox311)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox312)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox313)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox314)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox315)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox316)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox317)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox318)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox319)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox320)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox321)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox322)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox323)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox474)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox475)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox476)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox477)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox478)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox479)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox480)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox481)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox482)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox483)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox484)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox485)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox486)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox487)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox488)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox489)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox490)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox491)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label79)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox492)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox493)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox494)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox495)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox496)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox497)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox498)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label80)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox122)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox123)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox125)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label81)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label82)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label83)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label56)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label86)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label87)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label88)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label89)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label90)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label91)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label92)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label93)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label94)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label95)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox127)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox324)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox325)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox326)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox327)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox328)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox329)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox330)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox331)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox332)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox333)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox334)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox335)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox336)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox337)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox338)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox339)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox340)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox341)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox342)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox343)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox344)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox345)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox346)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox347)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox348)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox349)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox350)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox351)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label54)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox352)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox353)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox354)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox355)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox356)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox357)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox358)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox359)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox360)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox361)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox362)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox363)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox364)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox365)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox366)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox367)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox368)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox369)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox370)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox371)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox372)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox373)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox374)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox375)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox376)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox377)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox378)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox379)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox380)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox381)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox382)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox383)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox384)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox385)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox386)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox387)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox388)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox389)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox390)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox391)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox392)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox393)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox394)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox395)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox396)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox397)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox398)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox399)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox400)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox401)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox402)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox403)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox404)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox405)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox406)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox407)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox408)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox409)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox410)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox411)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox412)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox413)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox414)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox415)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox416)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox417)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox418)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox419)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox420)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox421)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox422)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox423)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox424)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox425)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox426)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox427)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox428)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox429)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox430)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox431)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox432)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox433)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox434)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox435)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox436)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox437)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox438)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox439)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox440)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox441)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox442)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox443)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox444)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox445)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox446)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox447)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox448)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox449)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox450)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox451)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox452)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox453)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox454)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox455)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox456)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox457)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox458)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox459)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox460)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox461)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox462)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox463)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox464)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox465)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox466)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox467)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox468)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox469)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox470)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox471)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox472)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox473)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox499)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox500)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox501)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox502)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox503)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label55)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox504)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label57)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox505)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox506)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox507)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox508)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox509)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox510)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox511)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox512)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox513)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox514)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label58)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label59)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label60)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label61)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox515)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label62)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label63)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label64)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label65)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label66)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label67)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label68)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label69)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label70)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label71)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label72)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label73)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label74)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label75)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label76)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox516)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox517)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox518)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox519)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox520)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox521)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox522)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label77)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label78)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label84)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label85)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label96)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label97)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label98)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label99)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label100)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label101)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label102)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label103)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label104)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label105)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label106)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label107)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // detail
            // 
            this.detail.CanShrink = true;
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.textBox218,
            this.textBox226,
            this.textBox227,
            this.textBox228,
            this.textBox189,
            this.textBox188,
            this.textBox187,
            this.textBox186,
            this.textBox185,
            this.テキスト54,
            this.ITEM011,
            this.テキスト28,
            this.テキスト3,
            this.ITEM011CP,
            this.ラベル138,
            this.ラベル139,
            this.テキスト156,
            this.テキスト158,
            this.テキスト159,
            this.テキスト160,
            this.テキスト161,
            this.テキスト162,
            this.テキスト163,
            this.テキスト164,
            this.テキスト165,
            this.テキスト166,
            this.テキスト167,
            this.テキスト168,
            this.テキスト169,
            this.テキスト170,
            this.テキスト171,
            this.ITEM045,
            this.ラベル176,
            this.ITEM068,
            this.ITEM069,
            this.ITEM070,
            this.ITEM071,
            this.ITEM072,
            this.ITEM073,
            this.ITEM074,
            this.ITEM075,
            this.ITEM076,
            this.ITEM077,
            this.ITEM078,
            this.ITEM079,
            this.ITEM081,
            this.ITEM085,
            this.ITEM086,
            this.ITEM087,
            this.ITEM088,
            this.テキスト27,
            this.テキスト30,
            this.テキスト32,
            this.テキスト34,
            this.ITEM030,
            this.テキスト39,
            this.テキスト47,
            this.テキスト48,
            this.ITEM096,
            this.ITEM097,
            this.テキスト52,
            this.ITEM022,
            this.ITEM023,
            this.ITEM024,
            this.テキスト58,
            this.ITEM029,
            this.テキスト60,
            this.テキスト61,
            this.テキスト62,
            this.テキスト63,
            this.テキスト64,
            this.テキスト65,
            this.ITEM034,
            this.ITEM035,
            this.aaa,
            this.bbb,
            this.ITEM036,
            this.テキスト71,
            this.ITEM037,
            this.テキスト73,
            this.テキスト74,
            this.テキスト75,
            this.テキスト76,
            this.テキスト77,
            this.テキスト78,
            this.テキスト79,
            this.テキスト80,
            this.テキスト81,
            this.テキスト82,
            this.テキスト83,
            this.テキスト84,
            this.テキスト85,
            this.テキスト86,
            this.テキスト87,
            this.テキスト88,
            this.テキスト89,
            this.テキスト90,
            this.テキスト91,
            this.ITEM089,
            this.ITEM090,
            this.ITEM091,
            this.ITEM092,
            this.ITEM093,
            this.テキスト97,
            this.テキスト98,
            this.テキスト99,
            this.テキスト100,
            this.テキスト101,
            this.テキスト102,
            this.ITEM094,
            this.テキスト104,
            this.ITEM095,
            this.テキスト106,
            this.テキスト107,
            this.ITEM031,
            this.textBox2,
            this.ITEM032,
            this.textBox3,
            this.ITEM033,
            this.textBox4,
            this.textBox5,
            this.textBox6,
            this.ITEM049,
            this.textBox7,
            this.textBox8,
            this.ITEM048,
            this.テキスト121,
            this.ITEM047,
            this.textBox9,
            this.テキスト124,
            this.ITEM038,
            this.a,
            this.textBox10,
            this.ITEM040,
            this.textBox11,
            this.ITEM039,
            this.textBox12,
            this.ITEM041,
            this.textBox13,
            this.textBox14,
            this.ITEM042,
            this.ITEM043,
            this.ITEM044,
            this.テキスト138,
            this.ITEM064,
            this.textBox15,
            this.ITEM065,
            this.textBox16,
            this.ITEM061,
            this.textBox17,
            this.ITEM066,
            this.textBox18,
            this.ITEM062,
            this.textBox19,
            this.ITEM067,
            this.テキスト153,
            this.ITEM063,
            this.テキスト155,
            this.textBox20,
            this.テキスト157,
            this.textBox21,
            this.ITEM098,
            this.テキスト40,
            this.ITEM050,
            this.ラベル159,
            this.ITEM051,
            this.ITEM052,
            this.ITEM053,
            this.ITEM054,
            this.ITEM055,
            this.ITEM058,
            this.ITEM056,
            this.ITEM059,
            this.ITEM057,
            this.ITEM060,
            this.テキスト35,
            this.ラベル171,
            this.ラベル172,
            this.ラベル173,
            this.ITEM046,
            this.ラベル177,
            this.ラベル178,
            this.ラベル182,
            this.ラベル183,
            this.ラベル184,
            this.ラベル185,
            this.ラベル186,
            this.ラベル187,
            this.ラベル188,
            this.ラベル189,
            this.ラベル190,
            this.ラベル191,
            this.ラベル192,
            this.ラベル193,
            this.ラベル194,
            this.ラベル195,
            this.ITEM025,
            this.ITEM026,
            this.ITEM027,
            this.ITEM028,
            this.ITEM082,
            this.ITEM083,
            this.ITEM084,
            this.textBox22,
            this.label1,
            this.textBox23,
            this.textBox24,
            this.textBox25,
            this.textBox26,
            this.textBox27,
            this.textBox28,
            this.textBox29,
            this.textBox30,
            this.textBox31,
            this.textBox32,
            this.textBox33,
            this.textBox34,
            this.textBox35,
            this.textBox36,
            this.textBox37,
            this.textBox38,
            this.textBox39,
            this.textBox40,
            this.textBox41,
            this.textBox42,
            this.textBox43,
            this.textBox44,
            this.textBox45,
            this.textBox46,
            this.textBox47,
            this.textBox48,
            this.textBox49,
            this.textBox50,
            this.textBox52,
            this.textBox53,
            this.textBox54,
            this.textBox55,
            this.textBox56,
            this.textBox57,
            this.textBox58,
            this.textBox59,
            this.textBox60,
            this.textBox61,
            this.textBox62,
            this.textBox63,
            this.textBox64,
            this.textBox65,
            this.textBox66,
            this.textBox67,
            this.textBox68,
            this.textBox69,
            this.textBox70,
            this.textBox71,
            this.textBox72,
            this.textBox73,
            this.textBox74,
            this.textBox75,
            this.textBox76,
            this.textBox77,
            this.textBox78,
            this.textBox79,
            this.textBox80,
            this.textBox81,
            this.textBox82,
            this.textBox83,
            this.textBox84,
            this.textBox85,
            this.textBox86,
            this.textBox87,
            this.textBox88,
            this.textBox89,
            this.textBox90,
            this.textBox91,
            this.textBox92,
            this.textBox93,
            this.textBox94,
            this.textBox95,
            this.textBox96,
            this.textBox97,
            this.textBox98,
            this.textBox99,
            this.textBox100,
            this.textBox101,
            this.textBox102,
            this.textBox103,
            this.textBox104,
            this.textBox105,
            this.textBox106,
            this.textBox107,
            this.textBox108,
            this.textBox109,
            this.textBox110,
            this.textBox111,
            this.textBox112,
            this.textBox113,
            this.textBox114,
            this.textBox115,
            this.textBox116,
            this.textBox117,
            this.textBox118,
            this.textBox119,
            this.textBox120,
            this.textBox121,
            this.textBox124,
            this.textBox126,
            this.textBox128,
            this.textBox129,
            this.textBox130,
            this.textBox131,
            this.textBox132,
            this.textBox133,
            this.textBox134,
            this.textBox135,
            this.textBox136,
            this.textBox137,
            this.textBox138,
            this.textBox139,
            this.textBox140,
            this.textBox141,
            this.textBox142,
            this.textBox143,
            this.textBox144,
            this.textBox145,
            this.textBox146,
            this.textBox147,
            this.textBox148,
            this.textBox149,
            this.textBox150,
            this.textBox151,
            this.textBox152,
            this.textBox153,
            this.label2,
            this.textBox154,
            this.label3,
            this.textBox155,
            this.textBox156,
            this.textBox157,
            this.textBox158,
            this.textBox159,
            this.textBox160,
            this.textBox161,
            this.textBox162,
            this.textBox163,
            this.textBox164,
            this.label4,
            this.label5,
            this.label6,
            this.label7,
            this.textBox165,
            this.label8,
            this.label14,
            this.label15,
            this.label16,
            this.label17,
            this.label18,
            this.label19,
            this.label20,
            this.label21,
            this.label22,
            this.label23,
            this.label24,
            this.label25,
            this.label26,
            this.textBox166,
            this.textBox167,
            this.textBox168,
            this.textBox169,
            this.textBox170,
            this.textBox171,
            this.textBox172,
            this.shape1,
            this.shape2,
            this.textBox173,
            this.textBox174,
            this.textBox175,
            this.textBox176,
            this.textBox177,
            this.textBox178,
            this.textBox179,
            this.textBox180,
            this.textBox181,
            this.textBox182,
            this.textBox183,
            this.textBox184,
            this.textBox190,
            this.textBox191,
            this.textBox192,
            this.textBox193,
            this.textBox194,
            this.textBox195,
            this.textBox196,
            this.textBox197,
            this.label27,
            this.textBox198,
            this.label28,
            this.textBox199,
            this.textBox200,
            this.textBox201,
            this.textBox202,
            this.textBox203,
            this.textBox204,
            this.textBox205,
            this.textBox206,
            this.textBox207,
            this.textBox208,
            this.textBox209,
            this.textBox210,
            this.textBox211,
            this.textBox212,
            this.textBox213,
            this.textBox214,
            this.textBox215,
            this.textBox216,
            this.textBox217,
            this.textBox219,
            this.textBox220,
            this.textBox221,
            this.textBox222,
            this.textBox223,
            this.textBox224,
            this.textBox225,
            this.textBox229,
            this.textBox230,
            this.textBox231,
            this.textBox232,
            this.textBox233,
            this.textBox234,
            this.textBox235,
            this.textBox236,
            this.textBox237,
            this.textBox238,
            this.textBox239,
            this.textBox240,
            this.textBox241,
            this.textBox242,
            this.textBox243,
            this.textBox244,
            this.textBox245,
            this.textBox246,
            this.textBox247,
            this.textBox248,
            this.textBox249,
            this.textBox250,
            this.textBox251,
            this.textBox252,
            this.textBox253,
            this.textBox254,
            this.textBox255,
            this.textBox256,
            this.textBox257,
            this.textBox258,
            this.textBox259,
            this.textBox260,
            this.textBox261,
            this.textBox262,
            this.textBox263,
            this.textBox264,
            this.textBox265,
            this.textBox266,
            this.textBox267,
            this.textBox268,
            this.textBox269,
            this.textBox270,
            this.textBox271,
            this.textBox272,
            this.textBox273,
            this.textBox274,
            this.textBox275,
            this.textBox276,
            this.textBox277,
            this.textBox278,
            this.textBox279,
            this.textBox280,
            this.textBox281,
            this.textBox282,
            this.textBox283,
            this.textBox284,
            this.textBox285,
            this.textBox286,
            this.textBox287,
            this.textBox288,
            this.textBox289,
            this.textBox290,
            this.textBox291,
            this.textBox292,
            this.textBox293,
            this.textBox294,
            this.textBox295,
            this.textBox296,
            this.textBox297,
            this.textBox298,
            this.textBox299,
            this.textBox300,
            this.textBox301,
            this.textBox302,
            this.textBox303,
            this.textBox304,
            this.textBox305,
            this.textBox306,
            this.textBox307,
            this.textBox308,
            this.textBox309,
            this.textBox310,
            this.textBox311,
            this.textBox312,
            this.textBox313,
            this.textBox314,
            this.textBox315,
            this.textBox316,
            this.textBox317,
            this.textBox318,
            this.textBox319,
            this.textBox320,
            this.textBox321,
            this.textBox322,
            this.textBox323,
            this.textBox474,
            this.textBox475,
            this.textBox476,
            this.textBox477,
            this.textBox478,
            this.textBox479,
            this.label29,
            this.textBox480,
            this.label30,
            this.textBox481,
            this.textBox482,
            this.textBox483,
            this.textBox484,
            this.textBox485,
            this.textBox486,
            this.textBox487,
            this.textBox488,
            this.textBox489,
            this.textBox490,
            this.label31,
            this.label32,
            this.label33,
            this.label34,
            this.textBox491,
            this.label35,
            this.label36,
            this.label40,
            this.label41,
            this.label42,
            this.label44,
            this.label45,
            this.label46,
            this.label47,
            this.label48,
            this.label49,
            this.label50,
            this.label51,
            this.label52,
            this.label79,
            this.textBox492,
            this.textBox493,
            this.textBox494,
            this.textBox495,
            this.textBox496,
            this.textBox497,
            this.textBox498,
            this.shape3,
            this.shape5,
            this.shape6,
            this.shape9,
            this.shape11,
            this.shape12,
            this.shape15,
            this.shape17,
            this.shape19,
            this.shape21,
            this.shape26,
            this.shape27,
            this.shape28,
            this.label80,
            this.textBox51,
            this.textBox122,
            this.textBox123,
            this.textBox125,
            this.label9,
            this.label10,
            this.label13,
            this.label81,
            this.label82,
            this.label11,
            this.label83,
            this.label56,
            this.label12,
            this.label86,
            this.label37,
            this.label87,
            this.label38,
            this.label39,
            this.label88,
            this.label89,
            this.label43,
            this.label90,
            this.label91,
            this.label92,
            this.label93,
            this.shape20,
            this.label94,
            this.label95,
            this.textBox1,
            this.textBox127,
            this.textBox324,
            this.textBox325,
            this.textBox326,
            this.textBox327,
            this.textBox328,
            this.textBox329,
            this.textBox330,
            this.textBox331,
            this.textBox332,
            this.textBox333,
            this.textBox334,
            this.textBox335,
            this.textBox336,
            this.textBox337,
            this.textBox338,
            this.textBox339,
            this.textBox340,
            this.textBox341,
            this.textBox342,
            this.textBox343,
            this.textBox344,
            this.textBox345,
            this.textBox346,
            this.textBox347,
            this.textBox348,
            this.textBox349,
            this.textBox350,
            this.label53,
            this.textBox351,
            this.label54,
            this.textBox352,
            this.textBox353,
            this.textBox354,
            this.textBox355,
            this.textBox356,
            this.textBox357,
            this.textBox358,
            this.textBox359,
            this.textBox360,
            this.textBox361,
            this.textBox362,
            this.textBox363,
            this.textBox364,
            this.textBox365,
            this.textBox366,
            this.textBox367,
            this.textBox368,
            this.textBox369,
            this.textBox370,
            this.textBox371,
            this.textBox372,
            this.textBox373,
            this.textBox374,
            this.textBox375,
            this.textBox376,
            this.textBox377,
            this.textBox378,
            this.textBox379,
            this.textBox380,
            this.textBox381,
            this.textBox382,
            this.textBox383,
            this.textBox384,
            this.textBox385,
            this.textBox386,
            this.textBox387,
            this.textBox388,
            this.textBox389,
            this.textBox390,
            this.textBox391,
            this.textBox392,
            this.textBox393,
            this.textBox394,
            this.textBox395,
            this.textBox396,
            this.textBox397,
            this.textBox398,
            this.textBox399,
            this.textBox400,
            this.textBox401,
            this.textBox402,
            this.textBox403,
            this.textBox404,
            this.textBox405,
            this.textBox406,
            this.textBox407,
            this.textBox408,
            this.textBox409,
            this.textBox410,
            this.textBox411,
            this.textBox412,
            this.textBox413,
            this.textBox414,
            this.textBox415,
            this.textBox416,
            this.textBox417,
            this.textBox418,
            this.textBox419,
            this.textBox420,
            this.textBox421,
            this.textBox422,
            this.textBox423,
            this.textBox424,
            this.textBox425,
            this.textBox426,
            this.textBox427,
            this.textBox428,
            this.textBox429,
            this.textBox430,
            this.textBox431,
            this.textBox432,
            this.textBox433,
            this.textBox434,
            this.textBox435,
            this.textBox436,
            this.textBox437,
            this.textBox438,
            this.textBox439,
            this.textBox440,
            this.textBox441,
            this.textBox442,
            this.textBox443,
            this.textBox444,
            this.textBox445,
            this.textBox446,
            this.textBox447,
            this.textBox448,
            this.textBox449,
            this.textBox450,
            this.textBox451,
            this.textBox452,
            this.textBox453,
            this.textBox454,
            this.textBox455,
            this.textBox456,
            this.textBox457,
            this.textBox458,
            this.textBox459,
            this.textBox460,
            this.textBox461,
            this.textBox462,
            this.textBox463,
            this.textBox464,
            this.textBox465,
            this.textBox466,
            this.textBox467,
            this.textBox468,
            this.textBox469,
            this.textBox470,
            this.textBox471,
            this.textBox472,
            this.textBox473,
            this.textBox499,
            this.textBox500,
            this.textBox501,
            this.textBox502,
            this.textBox503,
            this.label55,
            this.textBox504,
            this.label57,
            this.textBox505,
            this.textBox506,
            this.textBox507,
            this.textBox508,
            this.textBox509,
            this.textBox510,
            this.textBox511,
            this.textBox512,
            this.textBox513,
            this.textBox514,
            this.label58,
            this.label59,
            this.label60,
            this.label61,
            this.textBox515,
            this.label62,
            this.label63,
            this.label64,
            this.label65,
            this.label66,
            this.label67,
            this.label68,
            this.label69,
            this.label70,
            this.label71,
            this.label72,
            this.label73,
            this.label74,
            this.label75,
            this.label76,
            this.textBox516,
            this.textBox517,
            this.textBox518,
            this.textBox519,
            this.textBox520,
            this.textBox521,
            this.textBox522,
            this.shape4,
            this.shape7,
            this.shape8,
            this.shape10,
            this.shape13,
            this.shape14,
            this.shape16,
            this.shape18,
            this.shape22,
            this.shape23,
            this.shape24,
            this.shape25,
            this.shape29,
            this.label77,
            this.label78,
            this.label84,
            this.label85,
            this.label96,
            this.label97,
            this.label98,
            this.label99,
            this.label100,
            this.label101,
            this.label102,
            this.label103,
            this.label104,
            this.label105,
            this.shape30,
            this.label106,
            this.label107});
            this.detail.Height = 8.015744F;
            this.detail.Name = "detail";
            // 
            // textBox218
            // 
            this.textBox218.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox218.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox218.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox218.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox218.Height = 0.7520834F;
            this.textBox218.Left = 5.941339F;
            this.textBox218.Name = "textBox218";
            this.textBox218.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7.5pt; font" +
    "-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox218.Tag = "";
            this.textBox218.Text = "支　払 を受け　る　者";
            this.textBox218.Top = 0.4362155F;
            this.textBox218.Width = 0.357874F;
            // 
            // textBox226
            // 
            this.textBox226.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox226.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox226.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox226.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox226.Height = 0.7520834F;
            this.textBox226.Left = 6.29567F;
            this.textBox226.Name = "textBox226";
            this.textBox226.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox226.Tag = "";
            this.textBox226.Text = "住所又は居所";
            this.textBox226.Top = 0.4362205F;
            this.textBox226.Width = 0.1576389F;
            // 
            // textBox227
            // 
            this.textBox227.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox227.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox227.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox227.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox227.DataField = "ITEM099";
            this.textBox227.Height = 0.2011812F;
            this.textBox227.Left = 6.45315F;
            this.textBox227.Name = "textBox227";
            this.textBox227.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: left; vertical-align: middle; ddo-char-set: 1";
            this.textBox227.Tag = "";
            this.textBox227.Text = "ITEM099";
            this.textBox227.Top = 0.4350394F;
            this.textBox227.Width = 0.3937064F;
            // 
            // textBox228
            // 
            this.textBox228.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox228.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox228.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox228.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox228.Height = 0.1972222F;
            this.textBox228.Left = 6.846851F;
            this.textBox228.Name = "textBox228";
            this.textBox228.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: bold; text-align: left; ddo-char-set: 1";
            this.textBox228.Tag = "";
            this.textBox228.Text = null;
            this.textBox228.Top = 0.4350394F;
            this.textBox228.Width = 2.086806F;
            // 
            // textBox189
            // 
            this.textBox189.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox189.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox189.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox189.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox189.Height = 0.1576389F;
            this.textBox189.Left = 8.463551F;
            this.textBox189.Name = "textBox189";
            this.textBox189.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: left; ddo-char-set: 1";
            this.textBox189.Tag = "";
            this.textBox189.Text = null;
            this.textBox189.Top = 0.2845908F;
            this.textBox189.Width = 0.1576389F;
            // 
            // textBox188
            // 
            this.textBox188.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox188.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox188.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox188.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox188.Height = 0.1576389F;
            this.textBox188.Left = 8.305912F;
            this.textBox188.Name = "textBox188";
            this.textBox188.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: left; ddo-char-set: 1";
            this.textBox188.Tag = "";
            this.textBox188.Text = null;
            this.textBox188.Top = 0.2845908F;
            this.textBox188.Width = 0.1576389F;
            // 
            // textBox187
            // 
            this.textBox187.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox187.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox187.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox187.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox187.Height = 0.1576389F;
            this.textBox187.Left = 8.148281F;
            this.textBox187.Name = "textBox187";
            this.textBox187.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: left; ddo-char-set: 1";
            this.textBox187.Tag = "";
            this.textBox187.Text = null;
            this.textBox187.Top = 0.2845908F;
            this.textBox187.Width = 0.1576389F;
            // 
            // textBox186
            // 
            this.textBox186.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox186.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox186.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox186.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox186.Height = 0.1576389F;
            this.textBox186.Left = 7.990641F;
            this.textBox186.Name = "textBox186";
            this.textBox186.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: left; ddo-char-set: 1";
            this.textBox186.Tag = "";
            this.textBox186.Text = null;
            this.textBox186.Top = 0.2845908F;
            this.textBox186.Width = 0.1576389F;
            // 
            // textBox185
            // 
            this.textBox185.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox185.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox185.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox185.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox185.Height = 0.1576389F;
            this.textBox185.Left = 7.833072F;
            this.textBox185.Name = "textBox185";
            this.textBox185.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: left; ddo-char-set: 1";
            this.textBox185.Tag = "";
            this.textBox185.Text = null;
            this.textBox185.Top = 0.2845908F;
            this.textBox185.Width = 0.1576389F;
            // 
            // テキスト54
            // 
            this.テキスト54.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト54.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト54.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト54.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト54.Height = 0.1972222F;
            this.テキスト54.Left = 0.7952756F;
            this.テキスト54.Name = "テキスト54";
            this.テキスト54.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: bold; text-align: left; ddo-char-set: 1";
            this.テキスト54.Tag = "";
            this.テキスト54.Text = null;
            this.テキスト54.Top = 0.2754492F;
            this.テキスト54.Width = 2.480677F;
            // 
            // ITEM011
            // 
            this.ITEM011.DataField = "ITEM011";
            this.ITEM011.Height = 0.1576389F;
            this.ITEM011.Left = 0.8051181F;
            this.ITEM011.Name = "ITEM011";
            this.ITEM011.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.ITEM011.Tag = "";
            this.ITEM011.Text = "ITEM011";
            this.ITEM011.Top = 0.0984252F;
            this.ITEM011.Width = 0.8182297F;
            // 
            // テキスト28
            // 
            this.テキスト28.Height = 0.1611111F;
            this.テキスト28.HyperLink = null;
            this.テキスト28.Left = 1.493427F;
            this.テキスト28.Name = "テキスト28";
            this.テキスト28.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-alig" +
    "n: center; ddo-char-set: 128";
            this.テキスト28.Tag = "";
            this.テキスト28.Text = "給与所得の源泉徴収票";
            this.テキスト28.Top = 0.09488189F;
            this.テキスト28.Width = 2.819854F;
            // 
            // テキスト3
            // 
            this.テキスト3.Height = 0.7097222F;
            this.テキスト3.HyperLink = null;
            this.テキスト3.Left = 0.1299987F;
            this.テキスト3.Name = "テキスト3";
            this.テキスト3.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: bold; text-align: " +
    "left; ddo-char-set: 1";
            this.テキスト3.Tag = "";
            this.テキスト3.Text = "税務署提出用";
            this.テキスト3.Top = 2.937555F;
            this.テキスト3.Width = 0.1506944F;
            // 
            // ITEM011CP
            // 
            this.ITEM011CP.DataField = "ITEM011";
            this.ITEM011CP.Height = 0.1576389F;
            this.ITEM011CP.Left = 0.8049986F;
            this.ITEM011CP.Name = "ITEM011CP";
            this.ITEM011CP.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.ITEM011CP.Tag = "";
            this.ITEM011CP.Text = "ITEM011";
            this.ITEM011CP.Top = 4.081977F;
            this.ITEM011CP.Width = 0.8225606F;
            // 
            // ラベル138
            // 
            this.ラベル138.Height = 0.1611111F;
            this.ラベル138.HyperLink = null;
            this.ラベル138.Left = 1.505118F;
            this.ラベル138.Name = "ラベル138";
            this.ラベル138.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-alig" +
    "n: center; ddo-char-set: 128";
            this.ラベル138.Tag = "";
            this.ラベル138.Text = "給与所得の源泉徴収票";
            this.ラベル138.Top = 4.078484F;
            this.ラベル138.Width = 2.732525F;
            // 
            // ラベル139
            // 
            this.ラベル139.Height = 0.5951389F;
            this.ラベル139.HyperLink = null;
            this.ラベル139.Left = 0.1299987F;
            this.ラベル139.Name = "ラベル139";
            this.ラベル139.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: bold; text-align: " +
    "left; ddo-char-set: 1";
            this.ラベル139.Tag = "";
            this.ラベル139.Text = "受給者交付用";
            this.ラベル139.Top = 7.005589F;
            this.ラベル139.Width = 0.1715278F;
            // 
            // テキスト156
            // 
            this.テキスト156.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト156.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト156.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト156.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト156.Height = 0.1576389F;
            this.テキスト156.Left = 0.2834709F;
            this.テキスト156.Name = "テキスト156";
            this.テキスト156.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: bold; text-align: center; ddo-char-set: 1";
            this.テキスト156.Tag = "";
            this.テキスト156.Text = "署番号";
            this.テキスト156.Top = 3.706999F;
            this.テキスト156.Width = 0.4722222F;
            // 
            // テキスト158
            // 
            this.テキスト158.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト158.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト158.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト158.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト158.Height = 0.1576389F;
            this.テキスト158.Left = 0.7556931F;
            this.テキスト158.Name = "テキスト158";
            this.テキスト158.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: bold; text-align: center; ddo-char-set: 1";
            this.テキスト158.Tag = "";
            this.テキスト158.Text = null;
            this.テキスト158.Top = 3.706999F;
            this.テキスト158.Width = 0.1576389F;
            // 
            // テキスト159
            // 
            this.テキスト159.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト159.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト159.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト159.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト159.Height = 0.1576389F;
            this.テキスト159.Left = 0.913332F;
            this.テキスト159.Name = "テキスト159";
            this.テキスト159.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: bold; text-align: center; ddo-char-set: 1";
            this.テキスト159.Tag = "";
            this.テキスト159.Text = null;
            this.テキスト159.Top = 3.706999F;
            this.テキスト159.Width = 0.1576389F;
            // 
            // テキスト160
            // 
            this.テキスト160.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト160.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト160.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト160.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト160.Height = 0.1576389F;
            this.テキスト160.Left = 1.070971F;
            this.テキスト160.Name = "テキスト160";
            this.テキスト160.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: bold; text-align: center; ddo-char-set: 1";
            this.テキスト160.Tag = "";
            this.テキスト160.Text = null;
            this.テキスト160.Top = 3.706999F;
            this.テキスト160.Width = 0.1576389F;
            // 
            // テキスト161
            // 
            this.テキスト161.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト161.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト161.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト161.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト161.Height = 0.1576389F;
            this.テキスト161.Left = 1.22861F;
            this.テキスト161.Name = "テキスト161";
            this.テキスト161.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: bold; text-align: center; ddo-char-set: 1";
            this.テキスト161.Tag = "";
            this.テキスト161.Text = null;
            this.テキスト161.Top = 3.706999F;
            this.テキスト161.Width = 0.1576389F;
            // 
            // テキスト162
            // 
            this.テキスト162.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト162.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト162.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト162.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト162.Height = 0.1576389F;
            this.テキスト162.Left = 1.386249F;
            this.テキスト162.Name = "テキスト162";
            this.テキスト162.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: bold; text-align: center; ddo-char-set: 1";
            this.テキスト162.Tag = "";
            this.テキスト162.Text = null;
            this.テキスト162.Top = 3.706999F;
            this.テキスト162.Width = 0.1576389F;
            // 
            // テキスト163
            // 
            this.テキスト163.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト163.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト163.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト163.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト163.Height = 0.1576389F;
            this.テキスト163.Left = 1.543888F;
            this.テキスト163.Name = "テキスト163";
            this.テキスト163.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: bold; text-align: center; ddo-char-set: 1";
            this.テキスト163.Tag = "";
            this.テキスト163.Text = "整理番号";
            this.テキスト163.Top = 3.706999F;
            this.テキスト163.Width = 0.6298611F;
            // 
            // テキスト164
            // 
            this.テキスト164.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト164.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト164.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト164.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト164.Height = 0.1576389F;
            this.テキスト164.Left = 2.804304F;
            this.テキスト164.Name = "テキスト164";
            this.テキスト164.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: bold; text-align: center; ddo-char-set: 1";
            this.テキスト164.Tag = "";
            this.テキスト164.Text = null;
            this.テキスト164.Top = 3.706999F;
            this.テキスト164.Width = 0.1576389F;
            // 
            // テキスト165
            // 
            this.テキスト165.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト165.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト165.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト165.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト165.Height = 0.1576389F;
            this.テキスト165.Left = 2.961943F;
            this.テキスト165.Name = "テキスト165";
            this.テキスト165.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: bold; text-align: center; ddo-char-set: 1";
            this.テキスト165.Tag = "";
            this.テキスト165.Text = null;
            this.テキスト165.Top = 3.706999F;
            this.テキスト165.Width = 0.1576389F;
            // 
            // テキスト166
            // 
            this.テキスト166.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト166.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト166.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト166.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト166.Height = 0.1576389F;
            this.テキスト166.Left = 3.119582F;
            this.テキスト166.Name = "テキスト166";
            this.テキスト166.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: bold; text-align: center; ddo-char-set: 1";
            this.テキスト166.Tag = "";
            this.テキスト166.Text = null;
            this.テキスト166.Top = 3.706999F;
            this.テキスト166.Width = 0.1576389F;
            // 
            // テキスト167
            // 
            this.テキスト167.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト167.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト167.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト167.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト167.Height = 0.1576389F;
            this.テキスト167.Left = 2.173749F;
            this.テキスト167.Name = "テキスト167";
            this.テキスト167.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: bold; text-align: center; ddo-char-set: 1";
            this.テキスト167.Tag = "";
            this.テキスト167.Text = null;
            this.テキスト167.Top = 3.706999F;
            this.テキスト167.Width = 0.1576389F;
            // 
            // テキスト168
            // 
            this.テキスト168.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト168.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト168.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト168.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト168.Height = 0.1576389F;
            this.テキスト168.Left = 2.331388F;
            this.テキスト168.Name = "テキスト168";
            this.テキスト168.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: bold; text-align: center; ddo-char-set: 1";
            this.テキスト168.Tag = "";
            this.テキスト168.Text = null;
            this.テキスト168.Top = 3.706999F;
            this.テキスト168.Width = 0.1576389F;
            // 
            // テキスト169
            // 
            this.テキスト169.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト169.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト169.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト169.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト169.Height = 0.1576389F;
            this.テキスト169.Left = 2.489027F;
            this.テキスト169.Name = "テキスト169";
            this.テキスト169.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: bold; text-align: center; ddo-char-set: 1";
            this.テキスト169.Tag = "";
            this.テキスト169.Text = null;
            this.テキスト169.Top = 3.706999F;
            this.テキスト169.Width = 0.1576389F;
            // 
            // テキスト170
            // 
            this.テキスト170.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト170.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト170.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト170.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト170.Height = 0.1576389F;
            this.テキスト170.Left = 2.646665F;
            this.テキスト170.Name = "テキスト170";
            this.テキスト170.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: bold; text-align: center; ddo-char-set: 1";
            this.テキスト170.Tag = "";
            this.テキスト170.Text = null;
            this.テキスト170.Top = 3.706999F;
            this.テキスト170.Width = 0.1576389F;
            // 
            // テキスト171
            // 
            this.テキスト171.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト171.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト171.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト171.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト171.Height = 0.1576389F;
            this.テキスト171.Left = 3.277221F;
            this.テキスト171.Name = "テキスト171";
            this.テキスト171.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: bold; text-align: center; ddo-char-set: 1";
            this.テキスト171.Tag = "";
            this.テキスト171.Text = null;
            this.テキスト171.Top = 3.706999F;
            this.テキスト171.Width = 0.1576389F;
            // 
            // ITEM045
            // 
            this.ITEM045.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM045.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM045.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM045.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM045.DataField = "ITEM045";
            this.ITEM045.Height = 0.3541667F;
            this.ITEM045.Left = 3.039147F;
            this.ITEM045.Name = "ITEM045";
            this.ITEM045.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: right; vertical-align: bottom; ddo-char-set: 1";
            this.ITEM045.Tag = "";
            this.ITEM045.Text = "ITEM045";
            this.ITEM045.Top = 1.815727F;
            this.ITEM045.Width = 0.7090278F;
            // 
            // ラベル176
            // 
            this.ラベル176.Height = 0.08611111F;
            this.ラベル176.HyperLink = null;
            this.ラベル176.Left = 3.064841F;
            this.ラベル176.Name = "ラベル176";
            this.ラベル176.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.ラベル176.Tag = "";
            this.ラベル176.Text = "内　　　　　　　円";
            this.ラベル176.Top = 1.827533F;
            this.ラベル176.Width = 0.6785924F;
            // 
            // ITEM068
            // 
            this.ITEM068.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM068.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM068.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM068.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM068.DataField = "ITEM068";
            this.ITEM068.Height = 0.1576389F;
            this.ITEM068.Left = 0.2835906F;
            this.ITEM068.Name = "ITEM068";
            this.ITEM068.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.ITEM068.Tag = "";
            this.ITEM068.Text = "ITEM068";
            this.ITEM068.Top = 3.154616F;
            this.ITEM068.Width = 0.2131944F;
            // 
            // ITEM069
            // 
            this.ITEM069.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM069.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM069.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM069.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM069.DataField = "ITEM069";
            this.ITEM069.Height = 0.1576389F;
            this.ITEM069.Left = 0.4967849F;
            this.ITEM069.Name = "ITEM069";
            this.ITEM069.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.ITEM069.Tag = "";
            this.ITEM069.Text = "ITEM069";
            this.ITEM069.Top = 3.154616F;
            this.ITEM069.Width = 0.2131944F;
            // 
            // ITEM070
            // 
            this.ITEM070.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM070.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM070.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM070.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM070.DataField = "ITEM070";
            this.ITEM070.Height = 0.1576389F;
            this.ITEM070.Left = 0.7099795F;
            this.ITEM070.Name = "ITEM070";
            this.ITEM070.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.ITEM070.Tag = "";
            this.ITEM070.Text = "ITEM070";
            this.ITEM070.Top = 3.154616F;
            this.ITEM070.Width = 0.2131944F;
            // 
            // ITEM071
            // 
            this.ITEM071.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM071.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM071.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM071.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM071.DataField = "ITEM071";
            this.ITEM071.Height = 0.1576389F;
            this.ITEM071.Left = 0.9231737F;
            this.ITEM071.Name = "ITEM071";
            this.ITEM071.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.ITEM071.Tag = "";
            this.ITEM071.Text = "ITEM071";
            this.ITEM071.Top = 3.154616F;
            this.ITEM071.Width = 0.2131944F;
            // 
            // ITEM072
            // 
            this.ITEM072.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM072.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM072.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM072.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM072.DataField = "ITEM072";
            this.ITEM072.Height = 0.1576389F;
            this.ITEM072.Left = 1.136369F;
            this.ITEM072.Name = "ITEM072";
            this.ITEM072.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.ITEM072.Tag = "";
            this.ITEM072.Text = "ITEM072";
            this.ITEM072.Top = 3.154616F;
            this.ITEM072.Width = 0.2131944F;
            // 
            // ITEM073
            // 
            this.ITEM073.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM073.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM073.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM073.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM073.DataField = "ITEM073";
            this.ITEM073.Height = 0.1576389F;
            this.ITEM073.Left = 1.349563F;
            this.ITEM073.Name = "ITEM073";
            this.ITEM073.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.ITEM073.Tag = "";
            this.ITEM073.Text = "ITEM073";
            this.ITEM073.Top = 3.154616F;
            this.ITEM073.Width = 0.2131944F;
            // 
            // ITEM074
            // 
            this.ITEM074.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM074.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM074.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM074.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM074.DataField = "ITEM074";
            this.ITEM074.Height = 0.1576389F;
            this.ITEM074.Left = 1.562758F;
            this.ITEM074.Name = "ITEM074";
            this.ITEM074.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.ITEM074.Tag = "";
            this.ITEM074.Text = "ITEM074";
            this.ITEM074.Top = 3.154616F;
            this.ITEM074.Width = 0.2131944F;
            // 
            // ITEM075
            // 
            this.ITEM075.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM075.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM075.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM075.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM075.DataField = "ITEM075";
            this.ITEM075.Height = 0.1576389F;
            this.ITEM075.Left = 1.775952F;
            this.ITEM075.Name = "ITEM075";
            this.ITEM075.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.ITEM075.Tag = "";
            this.ITEM075.Text = "ITEM075";
            this.ITEM075.Top = 3.154616F;
            this.ITEM075.Width = 0.2131944F;
            // 
            // ITEM076
            // 
            this.ITEM076.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM076.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM076.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM076.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM076.DataField = "ITEM076";
            this.ITEM076.Height = 0.1576389F;
            this.ITEM076.Left = 1.989146F;
            this.ITEM076.Name = "ITEM076";
            this.ITEM076.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.ITEM076.Tag = "";
            this.ITEM076.Text = "ITEM076";
            this.ITEM076.Top = 3.154616F;
            this.ITEM076.Width = 0.2131944F;
            // 
            // ITEM077
            // 
            this.ITEM077.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM077.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM077.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM077.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM077.DataField = "ITEM077";
            this.ITEM077.Height = 0.1576389F;
            this.ITEM077.Left = 2.202341F;
            this.ITEM077.Name = "ITEM077";
            this.ITEM077.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.ITEM077.Tag = "";
            this.ITEM077.Text = "ITEM077";
            this.ITEM077.Top = 3.154616F;
            this.ITEM077.Width = 0.2131944F;
            // 
            // ITEM078
            // 
            this.ITEM078.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM078.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM078.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM078.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM078.DataField = "ITEM078";
            this.ITEM078.Height = 0.1576389F;
            this.ITEM078.Left = 2.415534F;
            this.ITEM078.Name = "ITEM078";
            this.ITEM078.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.ITEM078.Tag = "";
            this.ITEM078.Text = "ITEM078";
            this.ITEM078.Top = 3.154616F;
            this.ITEM078.Width = 0.2131944F;
            // 
            // ITEM079
            // 
            this.ITEM079.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM079.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM079.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM079.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM079.DataField = "ITEM079";
            this.ITEM079.Height = 0.1576389F;
            this.ITEM079.Left = 2.62873F;
            this.ITEM079.Name = "ITEM079";
            this.ITEM079.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.ITEM079.Tag = "";
            this.ITEM079.Text = "ITEM079";
            this.ITEM079.Top = 3.154616F;
            this.ITEM079.Width = 0.2131944F;
            // 
            // ITEM081
            // 
            this.ITEM081.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM081.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM081.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM081.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM081.DataField = "ITEM081";
            this.ITEM081.Height = 0.2756944F;
            this.ITEM081.Left = 2.841924F;
            this.ITEM081.Name = "ITEM081";
            this.ITEM081.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.ITEM081.Tag = "";
            this.ITEM081.Text = "ITEM081";
            this.ITEM081.Top = 3.036561F;
            this.ITEM081.Width = 0.2131944F;
            // 
            // ITEM085
            // 
            this.ITEM085.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM085.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM085.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM085.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM085.DataField = "ITEM085";
            this.ITEM085.Height = 0.2756944F;
            this.ITEM085.Left = 3.055118F;
            this.ITEM085.Name = "ITEM085";
            this.ITEM085.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.ITEM085.Tag = "";
            this.ITEM085.Text = "ITEM085";
            this.ITEM085.Top = 3.036561F;
            this.ITEM085.Width = 0.2131944F;
            // 
            // ITEM086
            // 
            this.ITEM086.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM086.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM086.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM086.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM086.DataField = "ITEM086";
            this.ITEM086.Height = 0.2756944F;
            this.ITEM086.Left = 3.268313F;
            this.ITEM086.Name = "ITEM086";
            this.ITEM086.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.ITEM086.Tag = "";
            this.ITEM086.Text = "ITEM086";
            this.ITEM086.Top = 3.036561F;
            this.ITEM086.Width = 0.2131944F;
            // 
            // ITEM087
            // 
            this.ITEM087.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM087.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM087.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM087.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM087.DataField = "ITEM087";
            this.ITEM087.Height = 0.2756944F;
            this.ITEM087.Left = 3.481507F;
            this.ITEM087.Name = "ITEM087";
            this.ITEM087.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.ITEM087.Tag = "";
            this.ITEM087.Text = "ITEM087";
            this.ITEM087.Top = 3.036561F;
            this.ITEM087.Width = 0.2131944F;
            // 
            // ITEM088
            // 
            this.ITEM088.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM088.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM088.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM088.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM088.DataField = "ITEM088";
            this.ITEM088.Height = 0.2756944F;
            this.ITEM088.Left = 3.694702F;
            this.ITEM088.Name = "ITEM088";
            this.ITEM088.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.ITEM088.Tag = "";
            this.ITEM088.Text = "ITEM088";
            this.ITEM088.Top = 3.036561F;
            this.ITEM088.Width = 0.2131944F;
            // 
            // テキスト27
            // 
            this.テキスト27.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト27.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト27.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト27.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト27.Height = 0.1972222F;
            this.テキスト27.Left = 3.433591F;
            this.テキスト27.Name = "テキスト27";
            this.テキスト27.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.テキスト27.Tag = "";
            this.テキスト27.Text = "(受給者番号)";
            this.テキスト27.Top = 0.2754492F;
            this.テキスト27.Width = 1.96875F;
            // 
            // テキスト30
            // 
            this.テキスト30.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト30.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト30.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト30.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト30.Height = 0.3583333F;
            this.テキスト30.Left = 3.433591F;
            this.テキスト30.Name = "テキスト30";
            this.テキスト30.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.テキスト30.Tag = "";
            this.テキスト30.Text = "(役職名)";
            this.テキスト30.Top = 0.6691991F;
            this.テキスト30.Width = 1.96875F;
            // 
            // テキスト32
            // 
            this.テキスト32.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト32.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト32.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト32.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト32.Height = 0.7520834F;
            this.テキスト32.Left = 0.2835906F;
            this.テキスト32.Name = "テキスト32";
            this.テキスト32.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7.5pt; font" +
    "-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.テキスト32.Tag = "";
            this.テキスト32.Text = "支　払 を受け　る　者";
            this.テキスト32.Top = 0.2754492F;
            this.テキスト32.Width = 0.357874F;
            // 
            // テキスト34
            // 
            this.テキスト34.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト34.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト34.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト34.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト34.Height = 0.1972222F;
            this.テキスト34.Left = 3.433591F;
            this.テキスト34.Name = "テキスト34";
            this.テキスト34.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.テキスト34.Tag = "";
            this.テキスト34.Text = "(フリガナ)";
            this.テキスト34.Top = 0.4726714F;
            this.テキスト34.Width = 1.96875F;
            // 
            // ITEM030
            // 
            this.ITEM030.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM030.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM030.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM030.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM030.DataField = "ITEM030";
            this.ITEM030.Height = 0.3152778F;
            this.ITEM030.Left = 1.149563F;
            this.ITEM030.Name = "ITEM030";
            this.ITEM030.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; vertical-align: bottom; ddo-char-set: 1";
            this.ITEM030.Tag = "";
            this.ITEM030.Text = "ITEM030";
            this.ITEM030.Top = 1.185171F;
            this.ITEM030.Width = 1.063194F;
            // 
            // テキスト39
            // 
            this.テキスト39.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト39.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト39.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト39.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト39.Height = 0.1972222F;
            this.テキスト39.Left = 1.464841F;
            this.テキスト39.Name = "テキスト39";
            this.テキスト39.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト39.Tag = "";
            this.テキスト39.Text = "控除対象扶養親族の数\r\n(配偶者を除く)";
            this.テキスト39.Top = 1.500449F;
            this.テキスト39.Width = 1.023611F;
            // 
            // テキスト47
            // 
            this.テキスト47.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト47.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト47.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト47.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト47.Height = 0.1180556F;
            this.テキスト47.Left = 2.841924F;
            this.テキスト47.Name = "テキスト47";
            this.テキスト47.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト47.Tag = "";
            this.テキスト47.Text = "中 途 就・退 職";
            this.テキスト47.Top = 2.800449F;
            this.テキスト47.Width = 1.067361F;
            // 
            // テキスト48
            // 
            this.テキスト48.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト48.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト48.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト48.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト48.Height = 0.1180556F;
            this.テキスト48.Left = 2.841924F;
            this.テキスト48.Name = "テキスト48";
            this.テキスト48.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト48.Tag = "";
            this.テキスト48.Text = "就職";
            this.テキスト48.Top = 2.918504F;
            this.テキスト48.Width = 0.2131944F;
            // 
            // ITEM096
            // 
            this.ITEM096.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM096.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM096.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM096.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM096.DataField = "ITEM096";
            this.ITEM096.Height = 0.1972222F;
            this.ITEM096.Left = 1.134285F;
            this.ITEM096.Name = "ITEM096";
            this.ITEM096.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: left; ddo-char-set: 1";
            this.ITEM096.Tag = "";
            this.ITEM096.Text = "ITEM096";
            this.ITEM096.Top = 3.312255F;
            this.ITEM096.Width = 4.26875F;
            // 
            // ITEM097
            // 
            this.ITEM097.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM097.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM097.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM097.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM097.CanGrow = false;
            this.ITEM097.DataField = "ITEM097";
            this.ITEM097.Height = 0.1972222F;
            this.ITEM097.Left = 1.134285F;
            this.ITEM097.Name = "ITEM097";
            this.ITEM097.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: left; ddo-char-set: 1";
            this.ITEM097.Tag = "";
            this.ITEM097.Text = "ITEM097";
            this.ITEM097.Top = 3.509477F;
            this.ITEM097.Width = 4.26875F;
            // 
            // テキスト52
            // 
            this.テキスト52.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト52.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト52.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト52.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト52.Height = 0.7520834F;
            this.テキスト52.Left = 0.6377571F;
            this.テキスト52.Name = "テキスト52";
            this.テキスト52.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト52.Tag = "";
            this.テキスト52.Text = "住所又は居所";
            this.テキスト52.Top = 0.2754492F;
            this.テキスト52.Width = 0.1576389F;
            // 
            // ITEM022
            // 
            this.ITEM022.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM022.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM022.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM022.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM022.DataField = "ITEM022";
            this.ITEM022.Height = 0.1576389F;
            this.ITEM022.Left = 0.7953961F;
            this.ITEM022.Name = "ITEM022";
            this.ITEM022.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: left; ddo-char-set: 1";
            this.ITEM022.Tag = "";
            this.ITEM022.Text = "ITEM022";
            this.ITEM022.Top = 0.476838F;
            this.ITEM022.Width = 2.480556F;
            // 
            // ITEM023
            // 
            this.ITEM023.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM023.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM023.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM023.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM023.DataField = "ITEM023";
            this.ITEM023.Height = 0.1972222F;
            this.ITEM023.Left = 0.7953961F;
            this.ITEM023.Name = "ITEM023";
            this.ITEM023.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: left; ddo-char-set: 1";
            this.ITEM023.Tag = "";
            this.ITEM023.Text = "ITEM023";
            this.ITEM023.Top = 0.634477F;
            this.ITEM023.Width = 2.480556F;
            // 
            // ITEM024
            // 
            this.ITEM024.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM024.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM024.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM024.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM024.DataField = "ITEM024";
            this.ITEM024.Height = 0.1972222F;
            this.ITEM024.Left = 0.7953961F;
            this.ITEM024.Name = "ITEM024";
            this.ITEM024.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: left; ddo-char-set: 1";
            this.ITEM024.Tag = "";
            this.ITEM024.Text = "ITEM024";
            this.ITEM024.Top = 0.831699F;
            this.ITEM024.Width = 2.480556F;
            // 
            // テキスト58
            // 
            this.テキスト58.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト58.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト58.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト58.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト58.Height = 0.1576389F;
            this.テキスト58.Left = 0.2835906F;
            this.テキスト58.Name = "テキスト58";
            this.テキスト58.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト58.Tag = "";
            this.テキスト58.Text = "種　　　別";
            this.テキスト58.Top = 1.027532F;
            this.テキスト58.Width = 0.8659722F;
            // 
            // ITEM029
            // 
            this.ITEM029.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM029.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM029.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM029.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM029.DataField = "ITEM029";
            this.ITEM029.Height = 0.3152778F;
            this.ITEM029.Left = 0.2835906F;
            this.ITEM029.Name = "ITEM029";
            this.ITEM029.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: left; vertical-align: bottom; ddo-char-set: 1";
            this.ITEM029.Tag = "";
            this.ITEM029.Text = "ITEM029";
            this.ITEM029.Top = 1.185171F;
            this.ITEM029.Width = 0.8659722F;
            // 
            // テキスト60
            // 
            this.テキスト60.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト60.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト60.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト60.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト60.Height = 0.3153544F;
            this.テキスト60.Left = 0.2835906F;
            this.テキスト60.Name = "テキスト60";
            this.テキスト60.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: left; vertical-align: middle; ddo-char-set: 1";
            this.テキスト60.Tag = "";
            this.テキスト60.Text = "控除対象配偶\r\n\r\n者の有無等";
            this.テキスト60.Top = 1.500253F;
            this.テキスト60.Width = 0.5909723F;
            // 
            // テキスト61
            // 
            this.テキスト61.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト61.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト61.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト61.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト61.Height = 0.1576389F;
            this.テキスト61.Left = 0.2835906F;
            this.テキスト61.Name = "テキスト61";
            this.テキスト61.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト61.Tag = "";
            this.テキスト61.Text = "　有";
            this.テキスト61.Top = 1.815727F;
            this.テキスト61.Width = 0.1180556F;
            // 
            // テキスト62
            // 
            this.テキスト62.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト62.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト62.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト62.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト62.Height = 0.1576389F;
            this.テキスト62.Left = 0.4016461F;
            this.テキスト62.Name = "テキスト62";
            this.テキスト62.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト62.Tag = "";
            this.テキスト62.Text = "　無";
            this.テキスト62.Top = 1.815727F;
            this.テキスト62.Width = 0.1180556F;
            // 
            // テキスト63
            // 
            this.テキスト63.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト63.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト63.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト63.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト63.Height = 0.1576389F;
            this.テキスト63.Left = 0.5197017F;
            this.テキスト63.Name = "テキスト63";
            this.テキスト63.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト63.Tag = "";
            this.テキスト63.Text = "従有";
            this.テキスト63.Top = 1.815727F;
            this.テキスト63.Width = 0.1180556F;
            // 
            // テキスト64
            // 
            this.テキスト64.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト64.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト64.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト64.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト64.Height = 0.1576389F;
            this.テキスト64.Left = 0.6377571F;
            this.テキスト64.Name = "テキスト64";
            this.テキスト64.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト64.Tag = "";
            this.テキスト64.Text = "従無";
            this.テキスト64.Top = 1.815727F;
            this.テキスト64.Width = 0.1180556F;
            // 
            // テキスト65
            // 
            this.テキスト65.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト65.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト65.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト65.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト65.CanGrow = false;
            this.テキスト65.Height = 0.1576389F;
            this.テキスト65.Left = 0.7565074F;
            this.テキスト65.Name = "テキスト65";
            this.テキスト65.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト65.Tag = "";
            this.テキスト65.Text = "老人";
            this.テキスト65.Top = 1.658088F;
            this.テキスト65.Width = 0.1180556F;
            // 
            // ITEM034
            // 
            this.ITEM034.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM034.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM034.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM034.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM034.DataField = "ITEM034";
            this.ITEM034.Height = 0.1972222F;
            this.ITEM034.Left = 0.2835906F;
            this.ITEM034.Name = "ITEM034";
            this.ITEM034.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.ITEM034.Tag = "";
            this.ITEM034.Text = "ITEM034";
            this.ITEM034.Top = 1.973366F;
            this.ITEM034.Width = 0.1180556F;
            // 
            // ITEM035
            // 
            this.ITEM035.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM035.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM035.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM035.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM035.DataField = "ITEM035";
            this.ITEM035.Height = 0.1972222F;
            this.ITEM035.Left = 0.4016461F;
            this.ITEM035.Name = "ITEM035";
            this.ITEM035.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.ITEM035.Tag = "";
            this.ITEM035.Text = "ITEM035";
            this.ITEM035.Top = 1.973366F;
            this.ITEM035.Width = 0.1180556F;
            // 
            // aaa
            // 
            this.aaa.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.aaa.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.aaa.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.aaa.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.aaa.Height = 0.1972222F;
            this.aaa.Left = 0.5197017F;
            this.aaa.Name = "aaa";
            this.aaa.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: bold; text-align: center; ddo-char-set: 1";
            this.aaa.Tag = "";
            this.aaa.Text = null;
            this.aaa.Top = 1.973366F;
            this.aaa.Width = 0.1180556F;
            // 
            // bbb
            // 
            this.bbb.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.bbb.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.bbb.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.bbb.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.bbb.Height = 0.1972222F;
            this.bbb.Left = 0.6377571F;
            this.bbb.Name = "bbb";
            this.bbb.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: bold; text-align: center; ddo-char-set: 1";
            this.bbb.Tag = "";
            this.bbb.Text = null;
            this.bbb.Top = 1.973366F;
            this.bbb.Width = 0.1180556F;
            // 
            // ITEM036
            // 
            this.ITEM036.DataField = "ITEM036";
            this.ITEM036.Height = 0.1972222F;
            this.ITEM036.Left = 0.7558129F;
            this.ITEM036.Name = "ITEM036";
            this.ITEM036.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.ITEM036.Tag = "";
            this.ITEM036.Text = "ITEM036";
            this.ITEM036.Top = 1.973366F;
            this.ITEM036.Width = 0.1180556F;
            // 
            // テキスト71
            // 
            this.テキスト71.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト71.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト71.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト71.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト71.Height = 0.3152778F;
            this.テキスト71.Left = 0.874563F;
            this.テキスト71.Name = "テキスト71";
            this.テキスト71.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.テキスト71.Tag = "";
            this.テキスト71.Text = "配偶者特別控除の額";
            this.テキスト71.Top = 1.500449F;
            this.テキスト71.Width = 0.5909723F;
            // 
            // ITEM037
            // 
            this.ITEM037.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM037.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM037.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM037.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM037.DataField = "ITEM037";
            this.ITEM037.Height = 0.3541667F;
            this.ITEM037.Left = 0.874563F;
            this.ITEM037.Name = "ITEM037";
            this.ITEM037.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.ITEM037.Tag = "";
            this.ITEM037.Text = "ITEM037";
            this.ITEM037.Top = 1.815727F;
            this.ITEM037.Width = 0.5909723F;
            // 
            // テキスト73
            // 
            this.テキスト73.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト73.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト73.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト73.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト73.Height = 0.6298611F;
            this.テキスト73.Left = 0.2835906F;
            this.テキスト73.Name = "テキスト73";
            this.テキスト73.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.テキスト73.Tag = "";
            this.テキスト73.Text = "(摘要)";
            this.テキスト73.Top = 2.170588F;
            this.テキスト73.Width = 5.11875F;
            // 
            // テキスト74
            // 
            this.テキスト74.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト74.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト74.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト74.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト74.CanGrow = false;
            this.テキスト74.Height = 0.3541667F;
            this.テキスト74.Left = 0.2835906F;
            this.テキスト74.Name = "テキスト74";
            this.テキスト74.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5.5pt; font" +
    "-weight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト74.Tag = "";
            this.テキスト74.Text = "扶16養歳親未族満";
            this.テキスト74.Top = 2.800449F;
            this.テキスト74.Width = 0.2131944F;
            // 
            // テキスト75
            // 
            this.テキスト75.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト75.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト75.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト75.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト75.Height = 0.3541667F;
            this.テキスト75.Left = 0.4967849F;
            this.テキスト75.Name = "テキスト75";
            this.テキスト75.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト75.Tag = "";
            this.テキスト75.Text = "未\r\n成\r\n年";
            this.テキスト75.Top = 2.800449F;
            this.テキスト75.Width = 0.2131944F;
            // 
            // テキスト76
            // 
            this.テキスト76.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト76.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト76.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト76.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト76.Height = 0.3541667F;
            this.テキスト76.Left = 0.7099795F;
            this.テキスト76.Name = "テキスト76";
            this.テキスト76.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト76.Tag = "";
            this.テキスト76.Text = "外\r\n国\r\n人";
            this.テキスト76.Top = 2.800449F;
            this.テキスト76.Width = 0.2131944F;
            // 
            // テキスト77
            // 
            this.テキスト77.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト77.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト77.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト77.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト77.Height = 0.3541667F;
            this.テキスト77.Left = 0.9231737F;
            this.テキスト77.Name = "テキスト77";
            this.テキスト77.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト77.Tag = "";
            this.テキスト77.Text = "死\r\n亡\r\n退";
            this.テキスト77.Top = 2.800449F;
            this.テキスト77.Width = 0.2131944F;
            // 
            // テキスト78
            // 
            this.テキスト78.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト78.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト78.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト78.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト78.Height = 0.3541667F;
            this.テキスト78.Left = 1.136369F;
            this.テキスト78.Name = "テキスト78";
            this.テキスト78.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト78.Tag = "";
            this.テキスト78.Text = "災\r\n害\r\n者";
            this.テキスト78.Top = 2.800449F;
            this.テキスト78.Width = 0.2131944F;
            // 
            // テキスト79
            // 
            this.テキスト79.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト79.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト79.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト79.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト79.Height = 0.3541667F;
            this.テキスト79.Left = 1.349563F;
            this.テキスト79.Name = "テキスト79";
            this.テキスト79.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト79.Tag = "";
            this.テキスト79.Text = "乙\r\n　\r\n欄";
            this.テキスト79.Top = 2.800449F;
            this.テキスト79.Width = 0.2131944F;
            // 
            // テキスト80
            // 
            this.テキスト80.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト80.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト80.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト80.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト80.Height = 0.1180556F;
            this.テキスト80.Left = 1.562758F;
            this.テキスト80.Name = "テキスト80";
            this.テキスト80.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 4pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト80.Tag = "";
            this.テキスト80.Text = "本人が障害者";
            this.テキスト80.Top = 2.800449F;
            this.テキスト80.Width = 0.4256943F;
            // 
            // テキスト81
            // 
            this.テキスト81.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト81.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト81.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト81.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト81.Height = 0.2361111F;
            this.テキスト81.Left = 1.562758F;
            this.テキスト81.Name = "テキスト81";
            this.テキスト81.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト81.Tag = "";
            this.テキスト81.Text = "特\r\n別";
            this.テキスト81.Top = 2.918504F;
            this.テキスト81.Width = 0.2131944F;
            // 
            // テキスト82
            // 
            this.テキスト82.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト82.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト82.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト82.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト82.CanGrow = false;
            this.テキスト82.Height = 0.2361111F;
            this.テキスト82.Left = 1.775952F;
            this.テキスト82.Name = "テキスト82";
            this.テキスト82.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト82.Tag = "";
            this.テキスト82.Text = "そ\r\nの\r\n他";
            this.テキスト82.Top = 2.918504F;
            this.テキスト82.Width = 0.2131944F;
            // 
            // テキスト83
            // 
            this.テキスト83.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト83.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト83.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト83.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト83.Height = 0.1180556F;
            this.テキスト83.Left = 1.988452F;
            this.テキスト83.Name = "テキスト83";
            this.テキスト83.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト83.Tag = "";
            this.テキスト83.Text = "寡　婦";
            this.テキスト83.Top = 2.800449F;
            this.テキスト83.Width = 0.4256943F;
            // 
            // テキスト84
            // 
            this.テキスト84.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト84.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト84.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト84.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト84.Height = 0.2361111F;
            this.テキスト84.Left = 1.989146F;
            this.テキスト84.Name = "テキスト84";
            this.テキスト84.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト84.Tag = "";
            this.テキスト84.Text = "一\r\n般";
            this.テキスト84.Top = 2.918504F;
            this.テキスト84.Width = 0.2131944F;
            // 
            // テキスト85
            // 
            this.テキスト85.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト85.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト85.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト85.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト85.Height = 0.2361111F;
            this.テキスト85.Left = 2.202341F;
            this.テキスト85.Name = "テキスト85";
            this.テキスト85.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト85.Tag = "";
            this.テキスト85.Text = "特\r\n別";
            this.テキスト85.Top = 2.918504F;
            this.テキスト85.Width = 0.2131944F;
            // 
            // テキスト86
            // 
            this.テキスト86.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト86.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト86.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト86.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト86.Height = 0.3582677F;
            this.テキスト86.Left = 2.415354F;
            this.テキスト86.Name = "テキスト86";
            this.テキスト86.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト86.Tag = "";
            this.テキスト86.Text = "寡　夫";
            this.テキスト86.Top = 2.796457F;
            this.テキスト86.Width = 0.2131944F;
            // 
            // テキスト87
            // 
            this.テキスト87.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト87.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト87.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト87.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト87.CanGrow = false;
            this.テキスト87.Height = 0.3582677F;
            this.テキスト87.Left = 2.628561F;
            this.テキスト87.Name = "テキスト87";
            this.テキスト87.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1; ddo-font-vertical: true";
            this.テキスト87.Tag = "";
            this.テキスト87.Text = "勤\r\n労\r\n学\r\n生";
            this.テキスト87.Top = 2.797976F;
            this.テキスト87.Width = 0.2131944F;
            // 
            // テキスト88
            // 
            this.テキスト88.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト88.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト88.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト88.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト88.Height = 0.1180556F;
            this.テキスト88.Left = 3.055118F;
            this.テキスト88.Name = "テキスト88";
            this.テキスト88.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト88.Tag = "";
            this.テキスト88.Text = "退職";
            this.テキスト88.Top = 2.918504F;
            this.テキスト88.Width = 0.2131944F;
            // 
            // テキスト89
            // 
            this.テキスト89.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト89.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト89.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト89.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト89.Height = 0.1180556F;
            this.テキスト89.Left = 3.268313F;
            this.テキスト89.Name = "テキスト89";
            this.テキスト89.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト89.Tag = "";
            this.テキスト89.Text = "年";
            this.テキスト89.Top = 2.918504F;
            this.テキスト89.Width = 0.2131944F;
            // 
            // テキスト90
            // 
            this.テキスト90.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト90.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト90.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト90.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト90.Height = 0.1180556F;
            this.テキスト90.Left = 3.481507F;
            this.テキスト90.Name = "テキスト90";
            this.テキスト90.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト90.Tag = "";
            this.テキスト90.Text = "月";
            this.テキスト90.Top = 2.918504F;
            this.テキスト90.Width = 0.2131944F;
            // 
            // テキスト91
            // 
            this.テキスト91.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト91.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト91.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト91.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト91.Height = 0.1180556F;
            this.テキスト91.Left = 3.694702F;
            this.テキスト91.Name = "テキスト91";
            this.テキスト91.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト91.Tag = "";
            this.テキスト91.Text = "日";
            this.テキスト91.Top = 2.918504F;
            this.テキスト91.Width = 0.2131944F;
            // 
            // ITEM089
            // 
            this.ITEM089.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM089.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM089.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM089.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM089.DataField = "ITEM089";
            this.ITEM089.Height = 0.2756944F;
            this.ITEM089.Left = 3.907895F;
            this.ITEM089.Name = "ITEM089";
            this.ITEM089.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.ITEM089.Tag = "";
            this.ITEM089.Text = "ITEM089";
            this.ITEM089.Top = 3.036561F;
            this.ITEM089.Width = 0.2131944F;
            // 
            // ITEM090
            // 
            this.ITEM090.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM090.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM090.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM090.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM090.DataField = "ITEM090";
            this.ITEM090.Height = 0.2756944F;
            this.ITEM090.Left = 4.121091F;
            this.ITEM090.Name = "ITEM090";
            this.ITEM090.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.ITEM090.Tag = "";
            this.ITEM090.Text = "ITEM090";
            this.ITEM090.Top = 3.036561F;
            this.ITEM090.Width = 0.2131944F;
            // 
            // ITEM091
            // 
            this.ITEM091.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM091.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM091.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM091.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM091.DataField = "ITEM091";
            this.ITEM091.Height = 0.2756944F;
            this.ITEM091.Left = 4.334285F;
            this.ITEM091.Name = "ITEM091";
            this.ITEM091.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.ITEM091.Tag = "";
            this.ITEM091.Text = "ITEM091";
            this.ITEM091.Top = 3.036561F;
            this.ITEM091.Width = 0.2131944F;
            // 
            // ITEM092
            // 
            this.ITEM092.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM092.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM092.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM092.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM092.DataField = "ITEM092";
            this.ITEM092.Height = 0.2756944F;
            this.ITEM092.Left = 4.54748F;
            this.ITEM092.Name = "ITEM092";
            this.ITEM092.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.ITEM092.Tag = "";
            this.ITEM092.Text = "ITEM092";
            this.ITEM092.Top = 3.036561F;
            this.ITEM092.Width = 0.2131944F;
            // 
            // ITEM093
            // 
            this.ITEM093.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM093.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM093.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM093.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM093.DataField = "ITEM093";
            this.ITEM093.Height = 0.2756944F;
            this.ITEM093.Left = 4.760674F;
            this.ITEM093.Name = "ITEM093";
            this.ITEM093.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.ITEM093.Tag = "";
            this.ITEM093.Text = "ITEM093";
            this.ITEM093.Top = 3.036561F;
            this.ITEM093.Width = 0.2131944F;
            // 
            // テキスト97
            // 
            this.テキスト97.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト97.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト97.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト97.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト97.Height = 0.1180556F;
            this.テキスト97.Left = 3.907895F;
            this.テキスト97.Name = "テキスト97";
            this.テキスト97.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト97.Tag = "";
            this.テキスト97.Text = "受　給　者　生　年　月　日";
            this.テキスト97.Top = 2.800449F;
            this.テキスト97.Width = 1.493056F;
            // 
            // テキスト98
            // 
            this.テキスト98.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト98.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト98.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト98.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト98.Height = 0.1180556F;
            this.テキスト98.Left = 3.907895F;
            this.テキスト98.Name = "テキスト98";
            this.テキスト98.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト98.Tag = "";
            this.テキスト98.Text = "明";
            this.テキスト98.Top = 2.918504F;
            this.テキスト98.Width = 0.2131944F;
            // 
            // テキスト99
            // 
            this.テキスト99.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト99.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト99.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト99.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト99.Height = 0.1180556F;
            this.テキスト99.Left = 4.121091F;
            this.テキスト99.Name = "テキスト99";
            this.テキスト99.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト99.Tag = "";
            this.テキスト99.Text = "大";
            this.テキスト99.Top = 2.918504F;
            this.テキスト99.Width = 0.2131944F;
            // 
            // テキスト100
            // 
            this.テキスト100.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト100.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト100.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト100.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト100.Height = 0.1180556F;
            this.テキスト100.Left = 4.334285F;
            this.テキスト100.Name = "テキスト100";
            this.テキスト100.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト100.Tag = "";
            this.テキスト100.Text = "昭";
            this.テキスト100.Top = 2.918504F;
            this.テキスト100.Width = 0.2131944F;
            // 
            // テキスト101
            // 
            this.テキスト101.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト101.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト101.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト101.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト101.Height = 0.1180556F;
            this.テキスト101.Left = 4.54748F;
            this.テキスト101.Name = "テキスト101";
            this.テキスト101.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト101.Tag = "";
            this.テキスト101.Text = "平";
            this.テキスト101.Top = 2.918504F;
            this.テキスト101.Width = 0.2131944F;
            // 
            // テキスト102
            // 
            this.テキスト102.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト102.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト102.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト102.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト102.Height = 0.1180556F;
            this.テキスト102.Left = 4.760674F;
            this.テキスト102.Name = "テキスト102";
            this.テキスト102.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト102.Tag = "";
            this.テキスト102.Text = "年";
            this.テキスト102.Top = 2.918504F;
            this.テキスト102.Width = 0.2131944F;
            // 
            // ITEM094
            // 
            this.ITEM094.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM094.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM094.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM094.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM094.DataField = "ITEM094";
            this.ITEM094.Height = 0.2756944F;
            this.ITEM094.Left = 4.973868F;
            this.ITEM094.Name = "ITEM094";
            this.ITEM094.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.ITEM094.Tag = "";
            this.ITEM094.Text = "ITEM094";
            this.ITEM094.Top = 3.036561F;
            this.ITEM094.Width = 0.2131944F;
            // 
            // テキスト104
            // 
            this.テキスト104.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト104.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト104.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト104.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト104.Height = 0.1180556F;
            this.テキスト104.Left = 4.973868F;
            this.テキスト104.Name = "テキスト104";
            this.テキスト104.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト104.Tag = "";
            this.テキスト104.Text = "月";
            this.テキスト104.Top = 2.918504F;
            this.テキスト104.Width = 0.2131944F;
            // 
            // ITEM095
            // 
            this.ITEM095.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM095.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM095.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM095.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM095.DataField = "ITEM095";
            this.ITEM095.Height = 0.2756944F;
            this.ITEM095.Left = 5.187063F;
            this.ITEM095.Name = "ITEM095";
            this.ITEM095.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.ITEM095.Tag = "";
            this.ITEM095.Text = "ITEM095";
            this.ITEM095.Top = 3.036561F;
            this.ITEM095.Width = 0.2138889F;
            // 
            // テキスト106
            // 
            this.テキスト106.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト106.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト106.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト106.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト106.Height = 0.1180556F;
            this.テキスト106.Left = 5.187063F;
            this.テキスト106.Name = "テキスト106";
            this.テキスト106.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト106.Tag = "";
            this.テキスト106.Text = "日";
            this.テキスト106.Top = 2.918504F;
            this.テキスト106.Width = 0.2138889F;
            // 
            // テキスト107
            // 
            this.テキスト107.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト107.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト107.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト107.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト107.Height = 0.1576389F;
            this.テキスト107.Left = 1.149563F;
            this.テキスト107.Name = "テキスト107";
            this.テキスト107.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト107.Tag = "";
            this.テキスト107.Text = "支　払　金　額";
            this.テキスト107.Top = 1.027532F;
            this.テキスト107.Width = 1.063194F;
            // 
            // ITEM031
            // 
            this.ITEM031.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM031.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM031.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM031.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM031.DataField = "ITEM031";
            this.ITEM031.Height = 0.3152778F;
            this.ITEM031.Left = 2.212757F;
            this.ITEM031.Name = "ITEM031";
            this.ITEM031.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; vertical-align: bottom; ddo-char-set: 1";
            this.ITEM031.Tag = "";
            this.ITEM031.Text = "ITEM031";
            this.ITEM031.Top = 1.185171F;
            this.ITEM031.Width = 1.063194F;
            // 
            // textBox2
            // 
            this.textBox2.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox2.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox2.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox2.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox2.Height = 0.1576389F;
            this.textBox2.Left = 2.212757F;
            this.textBox2.Name = "textBox2";
            this.textBox2.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox2.Tag = "";
            this.textBox2.Text = "給与所得控除後の金額";
            this.textBox2.Top = 1.027532F;
            this.textBox2.Width = 1.063194F;
            // 
            // ITEM032
            // 
            this.ITEM032.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM032.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM032.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM032.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM032.DataField = "ITEM032";
            this.ITEM032.Height = 0.3152778F;
            this.ITEM032.Left = 3.275952F;
            this.ITEM032.Name = "ITEM032";
            this.ITEM032.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; vertical-align: bottom; ddo-char-set: 1";
            this.ITEM032.Tag = "";
            this.ITEM032.Text = "ITEM032";
            this.ITEM032.Top = 1.185171F;
            this.ITEM032.Width = 1.063194F;
            // 
            // textBox3
            // 
            this.textBox3.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox3.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox3.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox3.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox3.Height = 0.1576389F;
            this.textBox3.Left = 3.275952F;
            this.textBox3.Name = "textBox3";
            this.textBox3.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox3.Tag = "";
            this.textBox3.Text = "所得控除の額の合計額";
            this.textBox3.Top = 1.027532F;
            this.textBox3.Width = 1.063194F;
            // 
            // ITEM033
            // 
            this.ITEM033.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM033.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM033.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM033.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM033.DataField = "ITEM033";
            this.ITEM033.Height = 0.3152778F;
            this.ITEM033.Left = 4.339146F;
            this.ITEM033.Name = "ITEM033";
            this.ITEM033.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; vertical-align: bottom; ddo-char-set: 1";
            this.ITEM033.Tag = "";
            this.ITEM033.Text = "ITEM033";
            this.ITEM033.Top = 1.185171F;
            this.ITEM033.Width = 1.063194F;
            // 
            // textBox4
            // 
            this.textBox4.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox4.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox4.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox4.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox4.Height = 0.1576389F;
            this.textBox4.Left = 4.339146F;
            this.textBox4.Name = "textBox4";
            this.textBox4.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox4.Tag = "";
            this.textBox4.Text = "源泉徴収税額";
            this.textBox4.Top = 1.027532F;
            this.textBox4.Width = 1.063194F;
            // 
            // textBox5
            // 
            this.textBox5.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox5.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox5.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox5.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox5.Height = 0.7519687F;
            this.textBox5.Left = 3.27611F;
            this.textBox5.Name = "textBox5";
            this.textBox5.Style = "background-color: White; font-family: ＭＳ 明朝; font-size: 8pt; font-weight: normal;" +
    " text-align: center; ddo-char-set: 1";
            this.textBox5.Tag = "";
            this.textBox5.Text = " 氏　\r\n\r\n\r\n名";
            this.textBox5.Top = 0.2754492F;
            this.textBox5.Width = 0.1576389F;
            // 
            // textBox6
            // 
            this.textBox6.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox6.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox6.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox6.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox6.Height = 0.3152778F;
            this.textBox6.Left = 4.811368F;
            this.textBox6.Name = "textBox6";
            this.textBox6.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox6.Tag = "";
            this.textBox6.Text = "住宅借入金等特別控除の額";
            this.textBox6.Top = 1.500449F;
            this.textBox6.Width = 0.5909723F;
            // 
            // ITEM049
            // 
            this.ITEM049.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM049.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM049.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM049.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM049.DataField = "ITEM049";
            this.ITEM049.Height = 0.3541667F;
            this.ITEM049.Left = 4.811368F;
            this.ITEM049.Name = "ITEM049";
            this.ITEM049.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: right; vertical-align: bottom; ddo-char-set: 1";
            this.ITEM049.Tag = "";
            this.ITEM049.Text = "ITEM049";
            this.ITEM049.Top = 1.815727F;
            this.ITEM049.Width = 0.5909723F;
            // 
            // textBox7
            // 
            this.textBox7.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox7.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox7.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox7.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox7.Height = 0.3152778F;
            this.textBox7.Left = 3.039147F;
            this.textBox7.Name = "textBox7";
            this.textBox7.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox7.Tag = "";
            this.textBox7.Text = "社会保険料等 の 金 額";
            this.textBox7.Top = 1.500449F;
            this.textBox7.Width = 0.7090278F;
            // 
            // textBox8
            // 
            this.textBox8.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox8.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox8.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox8.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox8.Height = 0.3152778F;
            this.textBox8.Left = 4.339146F;
            this.textBox8.Name = "textBox8";
            this.textBox8.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox8.Tag = "";
            this.textBox8.Text = "地震保険料の控除額";
            this.textBox8.Top = 1.500449F;
            this.textBox8.Width = 0.4722222F;
            // 
            // ITEM048
            // 
            this.ITEM048.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM048.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM048.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM048.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM048.DataField = "ITEM048";
            this.ITEM048.Height = 0.3541667F;
            this.ITEM048.Left = 4.339146F;
            this.ITEM048.Name = "ITEM048";
            this.ITEM048.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: right; vertical-align: bottom; ddo-char-set: 1";
            this.ITEM048.Tag = "";
            this.ITEM048.Text = "ITEM048";
            this.ITEM048.Top = 1.815727F;
            this.ITEM048.Width = 0.4722222F;
            // 
            // テキスト121
            // 
            this.テキスト121.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト121.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト121.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト121.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト121.Height = 0.3152778F;
            this.テキスト121.Left = 3.748174F;
            this.テキスト121.Name = "テキスト121";
            this.テキスト121.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.テキスト121.Tag = "";
            this.テキスト121.Text = "生命保険料の 控 除 額";
            this.テキスト121.Top = 1.500449F;
            this.テキスト121.Width = 0.5909723F;
            // 
            // ITEM047
            // 
            this.ITEM047.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM047.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM047.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM047.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM047.DataField = "ITEM047";
            this.ITEM047.Height = 0.3541667F;
            this.ITEM047.Left = 3.748174F;
            this.ITEM047.Name = "ITEM047";
            this.ITEM047.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: right; vertical-align: bottom; ddo-char-set: 1";
            this.ITEM047.Tag = "";
            this.ITEM047.Text = "ITEM047";
            this.ITEM047.Top = 1.815727F;
            this.ITEM047.Width = 0.5909723F;
            // 
            // textBox9
            // 
            this.textBox9.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox9.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox9.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox9.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox9.Height = 0.1972222F;
            this.textBox9.Left = 2.488452F;
            this.textBox9.Name = "textBox9";
            this.textBox9.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox9.Tag = "";
            this.textBox9.Text = "障害者の数(本人を除く)";
            this.textBox9.Top = 1.500449F;
            this.textBox9.Width = 0.5513888F;
            // 
            // テキスト124
            // 
            this.テキスト124.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト124.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト124.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト124.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト124.Height = 0.1180556F;
            this.テキスト124.Left = 1.464841F;
            this.テキスト124.Name = "テキスト124";
            this.テキスト124.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト124.Tag = "";
            this.テキスト124.Text = "特定";
            this.テキスト124.Top = 1.697671F;
            this.テキスト124.Width = 0.2756944F;
            // 
            // ITEM038
            // 
            this.ITEM038.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM038.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM038.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM038.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM038.DataField = "ITEM038";
            this.ITEM038.Height = 0.3541667F;
            this.ITEM038.Left = 1.465534F;
            this.ITEM038.Name = "ITEM038";
            this.ITEM038.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.ITEM038.Tag = "";
            this.ITEM038.Text = "ITEM038";
            this.ITEM038.Top = 1.815727F;
            this.ITEM038.Width = 0.1301349F;
            // 
            // a
            // 
            this.a.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.a.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.a.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.a.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.a.Height = 0.3541667F;
            this.a.Left = 1.599213F;
            this.a.Name = "a";
            this.a.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: bold; tex" +
    "t-align: center; ddo-char-set: 1";
            this.a.Tag = "";
            this.a.Text = null;
            this.a.Top = 1.815748F;
            this.a.Width = 0.1417323F;
            // 
            // textBox10
            // 
            this.textBox10.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox10.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox10.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox10.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox10.Height = 0.1180556F;
            this.textBox10.Left = 1.740535F;
            this.textBox10.Name = "textBox10";
            this.textBox10.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox10.Tag = "";
            this.textBox10.Text = "老　人";
            this.textBox10.Top = 1.697671F;
            this.textBox10.Width = 0.4333332F;
            // 
            // ITEM040
            // 
            this.ITEM040.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM040.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM040.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM040.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM040.DataField = "ITEM040";
            this.ITEM040.Height = 0.3541667F;
            this.ITEM040.Left = 1.878347F;
            this.ITEM040.Name = "ITEM040";
            this.ITEM040.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.ITEM040.Tag = "";
            this.ITEM040.Text = "ITEM040";
            this.ITEM040.Top = 1.815748F;
            this.ITEM040.Width = 0.1576389F;
            // 
            // textBox11
            // 
            this.textBox11.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox11.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox11.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox11.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox11.Height = 0.3541667F;
            this.textBox11.Left = 2.038583F;
            this.textBox11.Name = "textBox11";
            this.textBox11.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: bold; tex" +
    "t-align: center; ddo-char-set: 1";
            this.textBox11.Tag = "";
            this.textBox11.Text = null;
            this.textBox11.Top = 1.815748F;
            this.textBox11.Width = 0.1339515F;
            // 
            // ITEM039
            // 
            this.ITEM039.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM039.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM039.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM039.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM039.DataField = "ITEM039";
            this.ITEM039.Height = 0.3541667F;
            this.ITEM039.Left = 1.740535F;
            this.ITEM039.Name = "ITEM039";
            this.ITEM039.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.ITEM039.Tag = "";
            this.ITEM039.Text = "ITEM039";
            this.ITEM039.Top = 1.815727F;
            this.ITEM039.Width = 0.141749F;
            // 
            // textBox12
            // 
            this.textBox12.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox12.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox12.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox12.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox12.Height = 0.1180556F;
            this.textBox12.Left = 2.173868F;
            this.textBox12.Name = "textBox12";
            this.textBox12.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox12.Tag = "";
            this.textBox12.Text = "その他";
            this.textBox12.Top = 1.697671F;
            this.textBox12.Width = 0.3152778F;
            // 
            // ITEM041
            // 
            this.ITEM041.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM041.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM041.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM041.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM041.DataField = "ITEM041";
            this.ITEM041.Height = 0.3541667F;
            this.ITEM041.Left = 2.173868F;
            this.ITEM041.Name = "ITEM041";
            this.ITEM041.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.ITEM041.Tag = "";
            this.ITEM041.Text = "ITEM041";
            this.ITEM041.Top = 1.815727F;
            this.ITEM041.Width = 0.1576389F;
            // 
            // textBox13
            // 
            this.textBox13.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox13.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox13.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox13.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox13.Height = 0.3541667F;
            this.textBox13.Left = 2.331507F;
            this.textBox13.Name = "textBox13";
            this.textBox13.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: bold; tex" +
    "t-align: center; ddo-char-set: 1";
            this.textBox13.Tag = "";
            this.textBox13.Text = null;
            this.textBox13.Top = 1.815727F;
            this.textBox13.Width = 0.1576389F;
            // 
            // textBox14
            // 
            this.textBox14.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox14.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox14.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox14.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox14.Height = 0.1180556F;
            this.textBox14.Left = 2.489146F;
            this.textBox14.Name = "textBox14";
            this.textBox14.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox14.Tag = "";
            this.textBox14.Text = "特別";
            this.textBox14.Top = 1.697671F;
            this.textBox14.Width = 0.3152778F;
            // 
            // ITEM042
            // 
            this.ITEM042.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM042.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM042.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM042.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM042.DataField = "ITEM042";
            this.ITEM042.Height = 0.3541667F;
            this.ITEM042.Left = 2.489146F;
            this.ITEM042.Name = "ITEM042";
            this.ITEM042.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.ITEM042.Tag = "";
            this.ITEM042.Text = "ITEM042";
            this.ITEM042.Top = 1.815727F;
            this.ITEM042.Width = 0.1576389F;
            // 
            // ITEM043
            // 
            this.ITEM043.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM043.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM043.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM043.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM043.DataField = "ITEM043";
            this.ITEM043.Height = 0.3541667F;
            this.ITEM043.Left = 2.646785F;
            this.ITEM043.Name = "ITEM043";
            this.ITEM043.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.ITEM043.Tag = "";
            this.ITEM043.Text = "ITEM043";
            this.ITEM043.Top = 1.815727F;
            this.ITEM043.Width = 0.1576389F;
            // 
            // ITEM044
            // 
            this.ITEM044.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM044.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM044.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM044.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM044.DataField = "ITEM044";
            this.ITEM044.Height = 0.3541667F;
            this.ITEM044.Left = 2.804424F;
            this.ITEM044.Name = "ITEM044";
            this.ITEM044.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.ITEM044.Tag = "";
            this.ITEM044.Text = "ITEM044";
            this.ITEM044.Top = 1.815727F;
            this.ITEM044.Width = 0.2361111F;
            // 
            // テキスト138
            // 
            this.テキスト138.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト138.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト138.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト138.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト138.Height = 0.1180556F;
            this.テキスト138.Left = 2.804457F;
            this.テキスト138.Name = "テキスト138";
            this.テキスト138.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 4.5pt; font" +
    "-weight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト138.Tag = "";
            this.テキスト138.Text = "その他";
            this.テキスト138.Top = 1.697496F;
            this.テキスト138.Width = 0.2322833F;
            // 
            // ITEM064
            // 
            this.ITEM064.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM064.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM064.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM064.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM064.CanGrow = false;
            this.ITEM064.DataField = "ITEM064";
            this.ITEM064.Height = 0.1576389F;
            this.ITEM064.Left = 4.693313F;
            this.ITEM064.Name = "ITEM064";
            this.ITEM064.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; ddo-char-set: 1";
            this.ITEM064.Tag = "";
            this.ITEM064.Text = "ITEM064";
            this.ITEM064.Top = 2.169893F;
            this.ITEM064.Width = 0.7090278F;
            // 
            // textBox15
            // 
            this.textBox15.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox15.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox15.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox15.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox15.Height = 0.1576389F;
            this.textBox15.Left = 3.86623F;
            this.textBox15.Name = "textBox15";
            this.textBox15.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox15.Tag = "";
            this.textBox15.Text = "介護医療保険料の金額";
            this.textBox15.Top = 2.169893F;
            this.textBox15.Width = 0.8270833F;
            // 
            // ITEM065
            // 
            this.ITEM065.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM065.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM065.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM065.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM065.CanGrow = false;
            this.ITEM065.DataField = "ITEM065";
            this.ITEM065.Height = 0.1576389F;
            this.ITEM065.Left = 4.693313F;
            this.ITEM065.Name = "ITEM065";
            this.ITEM065.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; ddo-char-set: 1";
            this.ITEM065.Tag = "";
            this.ITEM065.Text = "ITEM065";
            this.ITEM065.Top = 2.327532F;
            this.ITEM065.Width = 0.7090278F;
            // 
            // textBox16
            // 
            this.textBox16.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox16.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox16.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox16.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox16.Height = 0.1576389F;
            this.textBox16.Left = 3.86623F;
            this.textBox16.Name = "textBox16";
            this.textBox16.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox16.Tag = "";
            this.textBox16.Text = "新個人年金保険料の金額";
            this.textBox16.Top = 2.327532F;
            this.textBox16.Width = 0.8270833F;
            // 
            // ITEM061
            // 
            this.ITEM061.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM061.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM061.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM061.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM061.CanGrow = false;
            this.ITEM061.DataField = "ITEM061";
            this.ITEM061.Height = 0.1576389F;
            this.ITEM061.Left = 3.157202F;
            this.ITEM061.Name = "ITEM061";
            this.ITEM061.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; ddo-char-set: 1";
            this.ITEM061.Tag = "";
            this.ITEM061.Text = "ITEM061";
            this.ITEM061.Top = 2.327532F;
            this.ITEM061.Width = 0.7090278F;
            // 
            // textBox17
            // 
            this.textBox17.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox17.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox17.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox17.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox17.Height = 0.1576389F;
            this.textBox17.Left = 2.419007F;
            this.textBox17.Name = "textBox17";
            this.textBox17.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox17.Tag = "";
            this.textBox17.Text = "配偶者の合計所得";
            this.textBox17.Top = 2.327532F;
            this.textBox17.Width = 0.7375F;
            // 
            // ITEM066
            // 
            this.ITEM066.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM066.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM066.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM066.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM066.CanGrow = false;
            this.ITEM066.DataField = "ITEM066";
            this.ITEM066.Height = 0.1576389F;
            this.ITEM066.Left = 4.693313F;
            this.ITEM066.Name = "ITEM066";
            this.ITEM066.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; ddo-char-set: 1";
            this.ITEM066.Tag = "";
            this.ITEM066.Text = "ITEM066";
            this.ITEM066.Top = 2.485172F;
            this.ITEM066.Width = 0.7090278F;
            // 
            // textBox18
            // 
            this.textBox18.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox18.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox18.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox18.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox18.Height = 0.1576389F;
            this.textBox18.Left = 3.86623F;
            this.textBox18.Name = "textBox18";
            this.textBox18.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox18.Tag = "";
            this.textBox18.Text = "旧個人年金保険料の金額";
            this.textBox18.Top = 2.485172F;
            this.textBox18.Width = 0.8270833F;
            // 
            // ITEM062
            // 
            this.ITEM062.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM062.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM062.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM062.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM062.CanGrow = false;
            this.ITEM062.DataField = "ITEM062";
            this.ITEM062.Height = 0.1576389F;
            this.ITEM062.Left = 3.157202F;
            this.ITEM062.Name = "ITEM062";
            this.ITEM062.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; ddo-char-set: 1";
            this.ITEM062.Tag = "";
            this.ITEM062.Text = "ITEM062";
            this.ITEM062.Top = 2.485172F;
            this.ITEM062.Width = 0.7090278F;
            // 
            // textBox19
            // 
            this.textBox19.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox19.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox19.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox19.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox19.Height = 0.1576389F;
            this.textBox19.Left = 2.419007F;
            this.textBox19.Name = "textBox19";
            this.textBox19.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox19.Tag = "";
            this.textBox19.Text = "新生命保険料の金額\r\n";
            this.textBox19.Top = 2.485172F;
            this.textBox19.Width = 0.7375F;
            // 
            // ITEM067
            // 
            this.ITEM067.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM067.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM067.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM067.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM067.CanGrow = false;
            this.ITEM067.DataField = "ITEM067";
            this.ITEM067.Height = 0.1576389F;
            this.ITEM067.Left = 4.693313F;
            this.ITEM067.Name = "ITEM067";
            this.ITEM067.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; ddo-char-set: 1";
            this.ITEM067.Tag = "";
            this.ITEM067.Text = "ITEM067";
            this.ITEM067.Top = 2.64281F;
            this.ITEM067.Width = 0.7090278F;
            // 
            // テキスト153
            // 
            this.テキスト153.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト153.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト153.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト153.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト153.Height = 0.1576389F;
            this.テキスト153.Left = 3.86623F;
            this.テキスト153.Name = "テキスト153";
            this.テキスト153.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト153.Tag = "";
            this.テキスト153.Text = "旧長期損害保険料の金額";
            this.テキスト153.Top = 2.64281F;
            this.テキスト153.Width = 0.8270833F;
            // 
            // ITEM063
            // 
            this.ITEM063.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM063.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM063.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM063.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM063.CanGrow = false;
            this.ITEM063.DataField = "ITEM063";
            this.ITEM063.Height = 0.1576389F;
            this.ITEM063.Left = 3.157202F;
            this.ITEM063.Name = "ITEM063";
            this.ITEM063.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; ddo-char-set: 1";
            this.ITEM063.Tag = "";
            this.ITEM063.Text = "ITEM063";
            this.ITEM063.Top = 2.64281F;
            this.ITEM063.Width = 0.7090278F;
            // 
            // テキスト155
            // 
            this.テキスト155.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト155.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト155.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト155.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト155.Height = 0.1576389F;
            this.テキスト155.Left = 2.419007F;
            this.テキスト155.Name = "テキスト155";
            this.テキスト155.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト155.Tag = "";
            this.テキスト155.Text = "旧生命保険料の金額";
            this.テキスト155.Top = 2.64281F;
            this.テキスト155.Width = 0.7375F;
            // 
            // textBox20
            // 
            this.textBox20.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox20.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox20.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox20.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox20.Height = 0.3944443F;
            this.textBox20.Left = 0.2835906F;
            this.textBox20.Name = "textBox20";
            this.textBox20.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox20.Tag = "";
            this.textBox20.Text = "支払者";
            this.textBox20.Top = 3.312255F;
            this.textBox20.Width = 0.2131944F;
            // 
            // テキスト157
            // 
            this.テキスト157.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト157.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト157.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト157.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.テキスト157.Height = 0.1972222F;
            this.テキスト157.Left = 0.4967849F;
            this.テキスト157.Name = "テキスト157";
            this.テキスト157.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.テキスト157.Tag = "";
            this.テキスト157.Text = "住所(居所)\r\n又は所在地 ";
            this.テキスト157.Top = 3.312255F;
            this.テキスト157.Width = 0.6375F;
            // 
            // textBox21
            // 
            this.textBox21.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox21.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox21.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox21.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox21.CanGrow = false;
            this.textBox21.Height = 0.1972222F;
            this.textBox21.Left = 0.4967849F;
            this.textBox21.Name = "textBox21";
            this.textBox21.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox21.Tag = "";
            this.textBox21.Text = "氏 名 又 は\r\n名　　　 称";
            this.textBox21.Top = 3.509477F;
            this.textBox21.Width = 0.6375F;
            // 
            // ITEM098
            // 
            this.ITEM098.DataField = "ITEM098";
            this.ITEM098.Height = 0.1576389F;
            this.ITEM098.Left = 4.023174F;
            this.ITEM098.Name = "ITEM098";
            this.ITEM098.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.ITEM098.Tag = "";
            this.ITEM098.Text = "ITEM098";
            this.ITEM098.Top = 3.525449F;
            this.ITEM098.Width = 1.358333F;
            // 
            // テキスト40
            // 
            this.テキスト40.Height = 0.1180556F;
            this.テキスト40.HyperLink = null;
            this.テキスト40.Left = 0.585674F;
            this.テキスト40.Name = "テキスト40";
            this.テキスト40.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.テキスト40.Tag = "";
            this.テキスト40.Text = "住宅借入金等特別控除可能額";
            this.テキスト40.Top = 2.181699F;
            this.テキスト40.Width = 1.141667F;
            // 
            // ITEM050
            // 
            this.ITEM050.DataField = "ITEM050";
            this.ITEM050.Height = 0.1180556F;
            this.ITEM050.Left = 1.72734F;
            this.ITEM050.Name = "ITEM050";
            this.ITEM050.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.ITEM050.Tag = "";
            this.ITEM050.Text = "ITEM050";
            this.ITEM050.Top = 2.181699F;
            this.ITEM050.Width = 0.527778F;
            // 
            // ラベル159
            // 
            this.ラベル159.Height = 0.1180556F;
            this.ラベル159.HyperLink = null;
            this.ラベル159.Left = 2.318313F;
            this.ラベル159.Name = "ラベル159";
            this.ラベル159.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.ラベル159.Tag = "";
            this.ラベル159.Text = "国民年金保険料等の金額";
            this.ラベル159.Top = 2.181699F;
            this.ラベル159.Width = 0.9451389F;
            // 
            // ITEM051
            // 
            this.ITEM051.DataField = "ITEM051";
            this.ITEM051.Height = 0.1180556F;
            this.ITEM051.Left = 3.263451F;
            this.ITEM051.Name = "ITEM051";
            this.ITEM051.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.ITEM051.Tag = "";
            this.ITEM051.Text = "ITEM051";
            this.ITEM051.Top = 2.181699F;
            this.ITEM051.Width = 0.5909723F;
            // 
            // ITEM052
            // 
            this.ITEM052.DataField = "ITEM052";
            this.ITEM052.Height = 0.1416667F;
            this.ITEM052.Left = 0.585674F;
            this.ITEM052.Name = "ITEM052";
            this.ITEM052.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.ITEM052.Tag = "";
            this.ITEM052.Text = "ITEM052";
            this.ITEM052.Top = 2.299756F;
            this.ITEM052.Width = 1.784028F;
            // 
            // ITEM053
            // 
            this.ITEM053.DataField = "ITEM053";
            this.ITEM053.Height = 0.1104167F;
            this.ITEM053.Left = 0.585674F;
            this.ITEM053.Name = "ITEM053";
            this.ITEM053.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.ITEM053.Tag = "";
            this.ITEM053.Text = "ITEM053";
            this.ITEM053.Top = 2.410172F;
            this.ITEM053.Width = 1.784028F;
            // 
            // ITEM054
            // 
            this.ITEM054.DataField = "ITEM054";
            this.ITEM054.Height = 0.1104167F;
            this.ITEM054.Left = 0.5858268F;
            this.ITEM054.Name = "ITEM054";
            this.ITEM054.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.ITEM054.Tag = "";
            this.ITEM054.Text = "ITEM054";
            this.ITEM054.Top = 2.522048F;
            this.ITEM054.Width = 1.784028F;
            // 
            // ITEM055
            // 
            this.ITEM055.DataField = "ITEM055";
            this.ITEM055.Height = 0.07847222F;
            this.ITEM055.Left = 0.3231738F;
            this.ITEM055.Name = "ITEM055";
            this.ITEM055.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.ITEM055.Tag = "";
            this.ITEM055.Text = "ITEM055";
            this.ITEM055.Top = 2.631006F;
            this.ITEM055.Width = 0.6694444F;
            // 
            // ITEM058
            // 
            this.ITEM058.DataField = "ITEM058";
            this.ITEM058.Height = 0.07847222F;
            this.ITEM058.Left = 0.3231738F;
            this.ITEM058.Name = "ITEM058";
            this.ITEM058.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.ITEM058.Tag = "";
            this.ITEM058.Text = "ITEM058";
            this.ITEM058.Top = 2.709477F;
            this.ITEM058.Width = 0.6694444F;
            // 
            // ITEM056
            // 
            this.ITEM056.DataField = "ITEM056";
            this.ITEM056.Height = 0.07847222F;
            this.ITEM056.Left = 1.023174F;
            this.ITEM056.Name = "ITEM056";
            this.ITEM056.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.ITEM056.Tag = "";
            this.ITEM056.Text = "ITEM056";
            this.ITEM056.Top = 2.629616F;
            this.ITEM056.Width = 0.6694444F;
            // 
            // ITEM059
            // 
            this.ITEM059.DataField = "ITEM059";
            this.ITEM059.Height = 0.07847222F;
            this.ITEM059.Left = 1.023174F;
            this.ITEM059.Name = "ITEM059";
            this.ITEM059.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.ITEM059.Tag = "";
            this.ITEM059.Text = "ITEM059";
            this.ITEM059.Top = 2.708088F;
            this.ITEM059.Width = 0.6694444F;
            // 
            // ITEM057
            // 
            this.ITEM057.DataField = "ITEM057";
            this.ITEM057.Height = 0.07847222F;
            this.ITEM057.Left = 1.721091F;
            this.ITEM057.Name = "ITEM057";
            this.ITEM057.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.ITEM057.Tag = "";
            this.ITEM057.Text = "ITEM057";
            this.ITEM057.Top = 2.629616F;
            this.ITEM057.Width = 0.6694444F;
            // 
            // ITEM060
            // 
            this.ITEM060.DataField = "ITEM060";
            this.ITEM060.Height = 0.07847222F;
            this.ITEM060.Left = 1.721091F;
            this.ITEM060.Name = "ITEM060";
            this.ITEM060.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.ITEM060.Tag = "";
            this.ITEM060.Text = "ITEM060";
            this.ITEM060.Top = 2.708088F;
            this.ITEM060.Width = 0.6694444F;
            // 
            // テキスト35
            // 
            this.テキスト35.Height = 0.08611111F;
            this.テキスト35.HyperLink = null;
            this.テキスト35.Left = 1.158661F;
            this.テキスト35.Name = "テキスト35";
            this.テキスト35.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.テキスト35.Tag = "";
            this.テキスト35.Text = "内　　　　　　　　　　　 　円";
            this.テキスト35.Top = 1.192913F;
            this.テキスト35.Width = 1.039441F;
            // 
            // ラベル171
            // 
            this.ラベル171.Height = 0.08611111F;
            this.ラベル171.HyperLink = null;
            this.ラベル171.Left = 4.356578F;
            this.ラベル171.Name = "ラベル171";
            this.ラベル171.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.ラベル171.Tag = "";
            this.ラベル171.Text = "内　　　　　　　　　　　 　円";
            this.ラベル171.Top = 1.192913F;
            this.ラベル171.Width = 1.035225F;
            // 
            // ラベル172
            // 
            this.ラベル172.Height = 0.08611111F;
            this.ラベル172.HyperLink = null;
            this.ラベル172.Left = 4.148245F;
            this.ラベル172.Name = "ラベル172";
            this.ラベル172.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.ラベル172.Tag = "";
            this.ラベル172.Text = "円";
            this.ラベル172.Top = 1.192913F;
            this.ラベル172.Width = 0.1666667F;
            // 
            // ラベル173
            // 
            this.ラベル173.Height = 0.08611111F;
            this.ラベル173.HyperLink = null;
            this.ラベル173.Left = 3.085745F;
            this.ラベル173.Name = "ラベル173";
            this.ラベル173.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.ラベル173.Tag = "";
            this.ラベル173.Text = "円";
            this.ラベル173.Top = 1.192913F;
            this.ラベル173.Width = 0.1666667F;
            // 
            // ITEM046
            // 
            this.ITEM046.DataField = "ITEM046";
            this.ITEM046.Height = 0.09582744F;
            this.ITEM046.Left = 3.064841F;
            this.ITEM046.Name = "ITEM046";
            this.ITEM046.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.ITEM046.Tag = "";
            this.ITEM046.Text = "ITEM046";
            this.ITEM046.Top = 1.910866F;
            this.ITEM046.Width = 0.6673612F;
            // 
            // ラベル177
            // 
            this.ラベル177.Height = 0.08611111F;
            this.ラベル177.HyperLink = null;
            this.ラベル177.Left = 1.294007F;
            this.ラベル177.Name = "ラベル177";
            this.ラベル177.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.ラベル177.Tag = "";
            this.ラベル177.Text = "円";
            this.ラベル177.Top = 1.817116F;
            this.ラベル177.Width = 0.1666667F;
            // 
            // ラベル178
            // 
            this.ラベル178.Height = 0.08611111F;
            this.ラベル178.HyperLink = null;
            this.ラベル178.Left = 1.471091F;
            this.ラベル178.Name = "ラベル178";
            this.ラベル178.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.ラベル178.Tag = "";
            this.ラベル178.Text = "人";
            this.ラベル178.Top = 1.817116F;
            this.ラベル178.Width = 0.1245788F;
            // 
            // ラベル182
            // 
            this.ラベル182.Height = 0.08611111F;
            this.ラベル182.HyperLink = null;
            this.ラベル182.Left = 1.741924F;
            this.ラベル182.Name = "ラベル182";
            this.ラベル182.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.ラベル182.Tag = "";
            this.ラベル182.Text = "内    人";
            this.ラベル182.Top = 1.817116F;
            this.ラベル182.Width = 0.3139814F;
            // 
            // ラベル183
            // 
            this.ラベル183.Height = 0.08611111F;
            this.ラベル183.HyperLink = null;
            this.ラベル183.Left = 2.179424F;
            this.ラベル183.Name = "ラベル183";
            this.ラベル183.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.ラベル183.Tag = "";
            this.ラベル183.Text = "人";
            this.ラベル183.Top = 1.817116F;
            this.ラベル183.Width = 0.15625F;
            // 
            // ラベル184
            // 
            this.ラベル184.Height = 0.08611111F;
            this.ラベル184.HyperLink = null;
            this.ラベル184.Left = 2.502341F;
            this.ラベル184.Name = "ラベル184";
            this.ラベル184.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.ラベル184.Tag = "";
            this.ラベル184.Text = "内   人";
            this.ラベル184.Top = 1.827533F;
            this.ラベル184.Width = 0.2916667F;
            // 
            // ラベル185
            // 
            this.ラベル185.Height = 0.08611111F;
            this.ラベル185.HyperLink = null;
            this.ラベル185.Left = 2.866924F;
            this.ラベル185.Name = "ラベル185";
            this.ラベル185.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.ラベル185.Tag = "";
            this.ラベル185.Text = "人";
            this.ラベル185.Top = 1.827533F;
            this.ラベル185.Width = 0.15625F;
            // 
            // ラベル186
            // 
            this.ラベル186.Height = 0.08611111F;
            this.ラベル186.HyperLink = null;
            this.ラベル186.Left = 4.169007F;
            this.ラベル186.Name = "ラベル186";
            this.ラベル186.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.ラベル186.Tag = "";
            this.ラベル186.Text = "円";
            this.ラベル186.Top = 1.827533F;
            this.ラベル186.Width = 0.1666667F;
            // 
            // ラベル187
            // 
            this.ラベル187.Height = 0.08611111F;
            this.ラベル187.HyperLink = null;
            this.ラベル187.Left = 4.637757F;
            this.ラベル187.Name = "ラベル187";
            this.ラベル187.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.ラベル187.Tag = "";
            this.ラベル187.Text = "円";
            this.ラベル187.Top = 1.827533F;
            this.ラベル187.Width = 0.1666667F;
            // 
            // ラベル188
            // 
            this.ラベル188.Height = 0.08611111F;
            this.ラベル188.HyperLink = null;
            this.ラベル188.Left = 5.22109F;
            this.ラベル188.Name = "ラベル188";
            this.ラベル188.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.ラベル188.Tag = "";
            this.ラベル188.Text = "円";
            this.ラベル188.Top = 1.827533F;
            this.ラベル188.Width = 0.1666667F;
            // 
            // ラベル189
            // 
            this.ラベル189.Height = 0.08611111F;
            this.ラベル189.HyperLink = null;
            this.ラベル189.Left = 3.700257F;
            this.ラベル189.Name = "ラベル189";
            this.ラベル189.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.ラベル189.Tag = "";
            this.ラベル189.Text = "円";
            this.ラベル189.Top = 2.327532F;
            this.ラベル189.Width = 0.1666667F;
            // 
            // ラベル190
            // 
            this.ラベル190.Height = 0.08611111F;
            this.ラベル190.HyperLink = null;
            this.ラベル190.Left = 3.700257F;
            this.ラベル190.Name = "ラベル190";
            this.ラベル190.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.ラベル190.Tag = "";
            this.ラベル190.Text = "円";
            this.ラベル190.Top = 2.483782F;
            this.ラベル190.Width = 0.1666667F;
            // 
            // ラベル191
            // 
            this.ラベル191.Height = 0.08611111F;
            this.ラベル191.HyperLink = null;
            this.ラベル191.Left = 3.700257F;
            this.ラベル191.Name = "ラベル191";
            this.ラベル191.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.ラベル191.Tag = "";
            this.ラベル191.Text = "円";
            this.ラベル191.Top = 2.640032F;
            this.ラベル191.Width = 0.1666667F;
            // 
            // ラベル192
            // 
            this.ラベル192.Height = 0.08611111F;
            this.ラベル192.HyperLink = null;
            this.ラベル192.Left = 5.231507F;
            this.ラベル192.Name = "ラベル192";
            this.ラベル192.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.ラベル192.Tag = "";
            this.ラベル192.Text = "円";
            this.ラベル192.Top = 2.327532F;
            this.ラベル192.Width = 0.1666667F;
            // 
            // ラベル193
            // 
            this.ラベル193.Height = 0.08611111F;
            this.ラベル193.HyperLink = null;
            this.ラベル193.Left = 5.231507F;
            this.ラベル193.Name = "ラベル193";
            this.ラベル193.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.ラベル193.Tag = "";
            this.ラベル193.Text = "円";
            this.ラベル193.Top = 2.483782F;
            this.ラベル193.Width = 0.1666667F;
            // 
            // ラベル194
            // 
            this.ラベル194.Height = 0.08611111F;
            this.ラベル194.HyperLink = null;
            this.ラベル194.Left = 5.231507F;
            this.ラベル194.Name = "ラベル194";
            this.ラベル194.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.ラベル194.Tag = "";
            this.ラベル194.Text = "円";
            this.ラベル194.Top = 2.640032F;
            this.ラベル194.Width = 0.1666667F;
            // 
            // ラベル195
            // 
            this.ラベル195.Height = 0.08611111F;
            this.ラベル195.HyperLink = null;
            this.ラベル195.Left = 5.231507F;
            this.ラベル195.Name = "ラベル195";
            this.ラベル195.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.ラベル195.Tag = "";
            this.ラベル195.Text = "円";
            this.ラベル195.Top = 2.171283F;
            this.ラベル195.Width = 0.1666667F;
            // 
            // ITEM025
            // 
            this.ITEM025.CanGrow = false;
            this.ITEM025.DataField = "ITEM025";
            this.ITEM025.Height = 0.1472222F;
            this.ITEM025.Left = 4.012757F;
            this.ITEM025.Name = "ITEM025";
            this.ITEM025.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: left; ddo-char-set: 1";
            this.ITEM025.Tag = "";
            this.ITEM025.Text = "ITEM025";
            this.ITEM025.Top = 0.3066992F;
            this.ITEM025.Width = 1.364583F;
            // 
            // ITEM026
            // 
            this.ITEM026.DataField = "ITEM026";
            this.ITEM026.Height = 0.1576389F;
            this.ITEM026.Left = 3.898174F;
            this.ITEM026.Name = "ITEM026";
            this.ITEM026.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: left; ddo-char-set: 1";
            this.ITEM026.Tag = "";
            this.ITEM026.Text = "ITEM026";
            this.ITEM026.Top = 0.4941992F;
            this.ITEM026.Width = 1.479167F;
            // 
            // ITEM027
            // 
            this.ITEM027.DataField = "ITEM027";
            this.ITEM027.Height = 0.1368055F;
            this.ITEM027.Left = 3.898174F;
            this.ITEM027.Name = "ITEM027";
            this.ITEM027.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.ITEM027.Tag = "";
            this.ITEM027.Text = "ITEM027";
            this.ITEM027.Top = 0.6921159F;
            this.ITEM027.Width = 1.479167F;
            // 
            // ITEM028
            // 
            this.ITEM028.DataField = "ITEM028";
            this.ITEM028.Height = 0.1576389F;
            this.ITEM028.Left = 3.898174F;
            this.ITEM028.Name = "ITEM028";
            this.ITEM028.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: left; ddo-char-set: 1";
            this.ITEM028.Tag = "";
            this.ITEM028.Text = "ITEM028";
            this.ITEM028.Top = 0.8483659F;
            this.ITEM028.Width = 1.479167F;
            // 
            // ITEM082
            // 
            this.ITEM082.DataField = "ITEM082";
            this.ITEM082.Height = 0.1381944F;
            this.ITEM082.Left = 3.273174F;
            this.ITEM082.Name = "ITEM082";
            this.ITEM082.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": center; vertical-align: bottom; ddo-char-set: 1";
            this.ITEM082.Tag = "";
            this.ITEM082.Text = "ITEM082";
            this.ITEM082.Top = 3.025449F;
            this.ITEM082.Width = 0.2131944F;
            // 
            // ITEM083
            // 
            this.ITEM083.DataField = "ITEM083";
            this.ITEM083.Height = 0.1381944F;
            this.ITEM083.Left = 3.486368F;
            this.ITEM083.Name = "ITEM083";
            this.ITEM083.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.ITEM083.Tag = "";
            this.ITEM083.Text = "ITEM083";
            this.ITEM083.Top = 3.025449F;
            this.ITEM083.Width = 0.2131944F;
            // 
            // ITEM084
            // 
            this.ITEM084.DataField = "ITEM084";
            this.ITEM084.Height = 0.1381944F;
            this.ITEM084.Left = 3.699563F;
            this.ITEM084.Name = "ITEM084";
            this.ITEM084.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.ITEM084.Tag = "";
            this.ITEM084.Text = "ITEM084";
            this.ITEM084.Top = 3.025449F;
            this.ITEM084.Width = 0.2131944F;
            // 
            // textBox22
            // 
            this.textBox22.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox22.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox22.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox22.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox22.DataField = "ITEM045";
            this.textBox22.Height = 0.3541667F;
            this.textBox22.Left = 3.039415F;
            this.textBox22.Name = "textBox22";
            this.textBox22.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: right; vertical-align: bottom; ddo-char-set: 1";
            this.textBox22.Tag = "";
            this.textBox22.Text = "ITEM045";
            this.textBox22.Top = 5.795927F;
            this.textBox22.Width = 0.7090278F;
            // 
            // label1
            // 
            this.label1.Height = 0.08611111F;
            this.label1.HyperLink = null;
            this.label1.Left = 3.065108F;
            this.label1.Name = "label1";
            this.label1.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.label1.Tag = "";
            this.label1.Text = "内　　　　　　　円";
            this.label1.Top = 5.807733F;
            this.label1.Width = 0.6785922F;
            // 
            // textBox23
            // 
            this.textBox23.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox23.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox23.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox23.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox23.DataField = "ITEM068";
            this.textBox23.Height = 0.1576389F;
            this.textBox23.Left = 0.2838583F;
            this.textBox23.Name = "textBox23";
            this.textBox23.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox23.Tag = "";
            this.textBox23.Text = "ITEM068";
            this.textBox23.Top = 7.134816F;
            this.textBox23.Width = 0.2131944F;
            // 
            // textBox24
            // 
            this.textBox24.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox24.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox24.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox24.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox24.DataField = "ITEM069";
            this.textBox24.Height = 0.1576389F;
            this.textBox24.Left = 0.4970525F;
            this.textBox24.Name = "textBox24";
            this.textBox24.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox24.Tag = "";
            this.textBox24.Text = "ITEM069";
            this.textBox24.Top = 7.134816F;
            this.textBox24.Width = 0.2131944F;
            // 
            // textBox25
            // 
            this.textBox25.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox25.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox25.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox25.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox25.DataField = "ITEM070";
            this.textBox25.Height = 0.1576389F;
            this.textBox25.Left = 0.710247F;
            this.textBox25.Name = "textBox25";
            this.textBox25.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox25.Tag = "";
            this.textBox25.Text = "ITEM070";
            this.textBox25.Top = 7.134816F;
            this.textBox25.Width = 0.2131944F;
            // 
            // textBox26
            // 
            this.textBox26.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox26.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox26.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox26.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox26.DataField = "ITEM071";
            this.textBox26.Height = 0.1576389F;
            this.textBox26.Left = 0.9234412F;
            this.textBox26.Name = "textBox26";
            this.textBox26.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox26.Tag = "";
            this.textBox26.Text = "ITEM071";
            this.textBox26.Top = 7.134816F;
            this.textBox26.Width = 0.2131944F;
            // 
            // textBox27
            // 
            this.textBox27.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox27.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox27.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox27.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox27.DataField = "ITEM072";
            this.textBox27.Height = 0.1576389F;
            this.textBox27.Left = 1.136636F;
            this.textBox27.Name = "textBox27";
            this.textBox27.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox27.Tag = "";
            this.textBox27.Text = "ITEM072";
            this.textBox27.Top = 7.134816F;
            this.textBox27.Width = 0.2131944F;
            // 
            // textBox28
            // 
            this.textBox28.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox28.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox28.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox28.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox28.DataField = "ITEM073";
            this.textBox28.Height = 0.1576389F;
            this.textBox28.Left = 1.349831F;
            this.textBox28.Name = "textBox28";
            this.textBox28.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox28.Tag = "";
            this.textBox28.Text = "ITEM073";
            this.textBox28.Top = 7.134816F;
            this.textBox28.Width = 0.2131944F;
            // 
            // textBox29
            // 
            this.textBox29.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox29.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox29.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox29.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox29.DataField = "ITEM074";
            this.textBox29.Height = 0.1576389F;
            this.textBox29.Left = 1.563025F;
            this.textBox29.Name = "textBox29";
            this.textBox29.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox29.Tag = "";
            this.textBox29.Text = "ITEM074";
            this.textBox29.Top = 7.134816F;
            this.textBox29.Width = 0.2131944F;
            // 
            // textBox30
            // 
            this.textBox30.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox30.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox30.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox30.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox30.DataField = "ITEM075";
            this.textBox30.Height = 0.1576389F;
            this.textBox30.Left = 1.776219F;
            this.textBox30.Name = "textBox30";
            this.textBox30.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox30.Tag = "";
            this.textBox30.Text = "ITEM075";
            this.textBox30.Top = 7.134816F;
            this.textBox30.Width = 0.2131944F;
            // 
            // textBox31
            // 
            this.textBox31.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox31.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox31.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox31.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox31.DataField = "ITEM076";
            this.textBox31.Height = 0.1576389F;
            this.textBox31.Left = 1.989413F;
            this.textBox31.Name = "textBox31";
            this.textBox31.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox31.Tag = "";
            this.textBox31.Text = "ITEM076";
            this.textBox31.Top = 7.134816F;
            this.textBox31.Width = 0.2131944F;
            // 
            // textBox32
            // 
            this.textBox32.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox32.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox32.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox32.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox32.DataField = "ITEM077";
            this.textBox32.Height = 0.1576389F;
            this.textBox32.Left = 2.202609F;
            this.textBox32.Name = "textBox32";
            this.textBox32.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox32.Tag = "";
            this.textBox32.Text = "ITEM077";
            this.textBox32.Top = 7.134816F;
            this.textBox32.Width = 0.2131944F;
            // 
            // textBox33
            // 
            this.textBox33.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox33.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox33.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox33.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox33.DataField = "ITEM078";
            this.textBox33.Height = 0.1576389F;
            this.textBox33.Left = 2.415802F;
            this.textBox33.Name = "textBox33";
            this.textBox33.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox33.Tag = "";
            this.textBox33.Text = "ITEM078";
            this.textBox33.Top = 7.134816F;
            this.textBox33.Width = 0.2131944F;
            // 
            // textBox34
            // 
            this.textBox34.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox34.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox34.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox34.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox34.DataField = "ITEM079";
            this.textBox34.Height = 0.1576389F;
            this.textBox34.Left = 2.628997F;
            this.textBox34.Name = "textBox34";
            this.textBox34.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox34.Tag = "";
            this.textBox34.Text = "ITEM079";
            this.textBox34.Top = 7.134816F;
            this.textBox34.Width = 0.2131944F;
            // 
            // textBox35
            // 
            this.textBox35.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox35.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox35.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox35.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox35.DataField = "ITEM081";
            this.textBox35.Height = 0.2756944F;
            this.textBox35.Left = 2.842192F;
            this.textBox35.Name = "textBox35";
            this.textBox35.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox35.Tag = "";
            this.textBox35.Text = "ITEM081";
            this.textBox35.Top = 7.01676F;
            this.textBox35.Width = 0.2131944F;
            // 
            // textBox36
            // 
            this.textBox36.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox36.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox36.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox36.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox36.DataField = "ITEM085";
            this.textBox36.Height = 0.2756944F;
            this.textBox36.Left = 3.055386F;
            this.textBox36.Name = "textBox36";
            this.textBox36.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox36.Tag = "";
            this.textBox36.Text = "ITEM085";
            this.textBox36.Top = 7.01676F;
            this.textBox36.Width = 0.2131944F;
            // 
            // textBox37
            // 
            this.textBox37.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox37.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox37.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox37.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox37.DataField = "ITEM086";
            this.textBox37.Height = 0.2756944F;
            this.textBox37.Left = 3.26858F;
            this.textBox37.Name = "textBox37";
            this.textBox37.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox37.Tag = "";
            this.textBox37.Text = "ITEM086";
            this.textBox37.Top = 7.01676F;
            this.textBox37.Width = 0.2131944F;
            // 
            // textBox38
            // 
            this.textBox38.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox38.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox38.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox38.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox38.DataField = "ITEM087";
            this.textBox38.Height = 0.2756944F;
            this.textBox38.Left = 3.481775F;
            this.textBox38.Name = "textBox38";
            this.textBox38.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox38.Tag = "";
            this.textBox38.Text = "ITEM087";
            this.textBox38.Top = 7.01676F;
            this.textBox38.Width = 0.2131944F;
            // 
            // textBox39
            // 
            this.textBox39.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox39.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox39.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox39.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox39.DataField = "ITEM088";
            this.textBox39.Height = 0.2756944F;
            this.textBox39.Left = 3.69497F;
            this.textBox39.Name = "textBox39";
            this.textBox39.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox39.Tag = "";
            this.textBox39.Text = "ITEM088";
            this.textBox39.Top = 7.01676F;
            this.textBox39.Width = 0.2131944F;
            // 
            // textBox40
            // 
            this.textBox40.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox40.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox40.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox40.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox40.Height = 0.1972222F;
            this.textBox40.Left = 3.433858F;
            this.textBox40.Name = "textBox40";
            this.textBox40.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.textBox40.Tag = "";
            this.textBox40.Text = "(受給者番号)";
            this.textBox40.Top = 4.255649F;
            this.textBox40.Width = 1.96875F;
            // 
            // textBox41
            // 
            this.textBox41.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox41.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox41.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox41.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox41.Height = 0.3583333F;
            this.textBox41.Left = 3.433858F;
            this.textBox41.Name = "textBox41";
            this.textBox41.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.textBox41.Tag = "";
            this.textBox41.Text = "(役職名)";
            this.textBox41.Top = 4.649399F;
            this.textBox41.Width = 1.96875F;
            // 
            // textBox42
            // 
            this.textBox42.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox42.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox42.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox42.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox42.Height = 0.7520834F;
            this.textBox42.Left = 0.2838583F;
            this.textBox42.Name = "textBox42";
            this.textBox42.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7.5pt; font" +
    "-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox42.Tag = "";
            this.textBox42.Text = "支　払 を受け　る　者";
            this.textBox42.Top = 4.255649F;
            this.textBox42.Width = 0.3578739F;
            // 
            // textBox43
            // 
            this.textBox43.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox43.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox43.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox43.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox43.Height = 0.1972222F;
            this.textBox43.Left = 3.433858F;
            this.textBox43.Name = "textBox43";
            this.textBox43.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.textBox43.Tag = "";
            this.textBox43.Text = "(フリガナ)";
            this.textBox43.Top = 4.452871F;
            this.textBox43.Width = 1.96875F;
            // 
            // textBox44
            // 
            this.textBox44.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox44.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox44.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox44.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox44.DataField = "ITEM030";
            this.textBox44.Height = 0.3152778F;
            this.textBox44.Left = 1.14983F;
            this.textBox44.Name = "textBox44";
            this.textBox44.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; vertical-align: bottom; ddo-char-set: 1";
            this.textBox44.Tag = "";
            this.textBox44.Text = "ITEM030";
            this.textBox44.Top = 5.165371F;
            this.textBox44.Width = 1.063194F;
            // 
            // textBox45
            // 
            this.textBox45.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox45.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox45.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox45.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox45.Height = 0.1972222F;
            this.textBox45.Left = 1.465108F;
            this.textBox45.Name = "textBox45";
            this.textBox45.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox45.Tag = "";
            this.textBox45.Text = "控除対象扶養親族の数\r\n(配偶者を除く)";
            this.textBox45.Top = 5.480649F;
            this.textBox45.Width = 1.023611F;
            // 
            // textBox46
            // 
            this.textBox46.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox46.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox46.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox46.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox46.Height = 0.1180556F;
            this.textBox46.Left = 2.842192F;
            this.textBox46.Name = "textBox46";
            this.textBox46.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox46.Tag = "";
            this.textBox46.Text = "中 途 就・退 職";
            this.textBox46.Top = 6.780649F;
            this.textBox46.Width = 1.067361F;
            // 
            // textBox47
            // 
            this.textBox47.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox47.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox47.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox47.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox47.Height = 0.1180556F;
            this.textBox47.Left = 2.842192F;
            this.textBox47.Name = "textBox47";
            this.textBox47.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox47.Tag = "";
            this.textBox47.Text = "就職";
            this.textBox47.Top = 6.898704F;
            this.textBox47.Width = 0.2131944F;
            // 
            // textBox48
            // 
            this.textBox48.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox48.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox48.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox48.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox48.DataField = "ITEM096";
            this.textBox48.Height = 0.1972222F;
            this.textBox48.Left = 1.134552F;
            this.textBox48.Name = "textBox48";
            this.textBox48.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: left; ddo-char-set: 1";
            this.textBox48.Tag = "";
            this.textBox48.Text = "ITEM096";
            this.textBox48.Top = 7.292455F;
            this.textBox48.Width = 4.26875F;
            // 
            // textBox49
            // 
            this.textBox49.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox49.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox49.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox49.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox49.CanGrow = false;
            this.textBox49.DataField = "ITEM097";
            this.textBox49.Height = 0.1972222F;
            this.textBox49.Left = 1.134552F;
            this.textBox49.Name = "textBox49";
            this.textBox49.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: left; ddo-char-set: 1";
            this.textBox49.Tag = "";
            this.textBox49.Text = "ITEM097";
            this.textBox49.Top = 7.489677F;
            this.textBox49.Width = 4.26875F;
            // 
            // textBox50
            // 
            this.textBox50.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox50.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox50.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox50.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox50.Height = 0.7520834F;
            this.textBox50.Left = 0.6380246F;
            this.textBox50.Name = "textBox50";
            this.textBox50.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox50.Tag = "";
            this.textBox50.Text = "住所又は居所";
            this.textBox50.Top = 4.255649F;
            this.textBox50.Width = 0.1576388F;
            // 
            // textBox52
            // 
            this.textBox52.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox52.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox52.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox52.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox52.Height = 0.1972222F;
            this.textBox52.Left = 0.7952756F;
            this.textBox52.Name = "textBox52";
            this.textBox52.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: bold; text-align: left; ddo-char-set: 1";
            this.textBox52.Tag = "";
            this.textBox52.Text = null;
            this.textBox52.Top = 4.255649F;
            this.textBox52.Width = 2.480945F;
            // 
            // textBox53
            // 
            this.textBox53.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox53.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox53.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox53.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox53.DataField = "ITEM022";
            this.textBox53.Height = 0.1576389F;
            this.textBox53.Left = 0.7956636F;
            this.textBox53.Name = "textBox53";
            this.textBox53.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: left; ddo-char-set: 1";
            this.textBox53.Tag = "";
            this.textBox53.Text = "ITEM022";
            this.textBox53.Top = 4.457038F;
            this.textBox53.Width = 2.480556F;
            // 
            // textBox54
            // 
            this.textBox54.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox54.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox54.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox54.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox54.DataField = "ITEM023";
            this.textBox54.Height = 0.1972222F;
            this.textBox54.Left = 0.7956636F;
            this.textBox54.Name = "textBox54";
            this.textBox54.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: left; ddo-char-set: 1";
            this.textBox54.Tag = "";
            this.textBox54.Text = "ITEM023";
            this.textBox54.Top = 4.614677F;
            this.textBox54.Width = 2.480556F;
            // 
            // textBox55
            // 
            this.textBox55.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox55.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox55.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox55.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox55.DataField = "ITEM024";
            this.textBox55.Height = 0.1972222F;
            this.textBox55.Left = 0.7956636F;
            this.textBox55.Name = "textBox55";
            this.textBox55.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: left; ddo-char-set: 1";
            this.textBox55.Tag = "";
            this.textBox55.Text = "ITEM024";
            this.textBox55.Top = 4.811899F;
            this.textBox55.Width = 2.480556F;
            // 
            // textBox56
            // 
            this.textBox56.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox56.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox56.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox56.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox56.Height = 0.1576389F;
            this.textBox56.Left = 0.2838583F;
            this.textBox56.Name = "textBox56";
            this.textBox56.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox56.Tag = "";
            this.textBox56.Text = "種　　　別";
            this.textBox56.Top = 5.007732F;
            this.textBox56.Width = 0.865972F;
            // 
            // textBox57
            // 
            this.textBox57.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox57.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox57.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox57.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox57.DataField = "ITEM029";
            this.textBox57.Height = 0.3152778F;
            this.textBox57.Left = 0.2838583F;
            this.textBox57.Name = "textBox57";
            this.textBox57.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: left; vertical-align: bottom; ddo-char-set: 1";
            this.textBox57.Tag = "";
            this.textBox57.Text = "ITEM029";
            this.textBox57.Top = 5.165371F;
            this.textBox57.Width = 0.865972F;
            // 
            // textBox58
            // 
            this.textBox58.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox58.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox58.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox58.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox58.Height = 0.3153544F;
            this.textBox58.Left = 0.2838583F;
            this.textBox58.Name = "textBox58";
            this.textBox58.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: left; vertical-align: middle; ddo-char-set: 1";
            this.textBox58.Tag = "";
            this.textBox58.Text = "控除対象配偶\r\n\r\n者の有無等";
            this.textBox58.Top = 5.480453F;
            this.textBox58.Width = 0.5909724F;
            // 
            // textBox59
            // 
            this.textBox59.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox59.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox59.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox59.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox59.Height = 0.1576389F;
            this.textBox59.Left = 0.2838583F;
            this.textBox59.Name = "textBox59";
            this.textBox59.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox59.Tag = "";
            this.textBox59.Text = "　有";
            this.textBox59.Top = 5.795927F;
            this.textBox59.Width = 0.1180556F;
            // 
            // textBox60
            // 
            this.textBox60.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox60.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox60.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox60.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox60.Height = 0.1576389F;
            this.textBox60.Left = 0.4019139F;
            this.textBox60.Name = "textBox60";
            this.textBox60.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox60.Tag = "";
            this.textBox60.Text = "　無";
            this.textBox60.Top = 5.795927F;
            this.textBox60.Width = 0.1180556F;
            // 
            // textBox61
            // 
            this.textBox61.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox61.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox61.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox61.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox61.Height = 0.1576389F;
            this.textBox61.Left = 0.5199692F;
            this.textBox61.Name = "textBox61";
            this.textBox61.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox61.Tag = "";
            this.textBox61.Text = "従有";
            this.textBox61.Top = 5.795927F;
            this.textBox61.Width = 0.1180556F;
            // 
            // textBox62
            // 
            this.textBox62.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox62.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox62.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox62.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox62.Height = 0.1576389F;
            this.textBox62.Left = 0.6380246F;
            this.textBox62.Name = "textBox62";
            this.textBox62.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox62.Tag = "";
            this.textBox62.Text = "従無";
            this.textBox62.Top = 5.795927F;
            this.textBox62.Width = 0.1180556F;
            // 
            // textBox63
            // 
            this.textBox63.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox63.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox63.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox63.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox63.CanGrow = false;
            this.textBox63.Height = 0.1576389F;
            this.textBox63.Left = 0.7567749F;
            this.textBox63.Name = "textBox63";
            this.textBox63.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox63.Tag = "";
            this.textBox63.Text = "老人";
            this.textBox63.Top = 5.638288F;
            this.textBox63.Width = 0.1180556F;
            // 
            // textBox64
            // 
            this.textBox64.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox64.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox64.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox64.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox64.DataField = "ITEM034";
            this.textBox64.Height = 0.1972222F;
            this.textBox64.Left = 0.2838583F;
            this.textBox64.Name = "textBox64";
            this.textBox64.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox64.Tag = "";
            this.textBox64.Text = "ITEM034";
            this.textBox64.Top = 5.953566F;
            this.textBox64.Width = 0.1180556F;
            // 
            // textBox65
            // 
            this.textBox65.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox65.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox65.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox65.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox65.DataField = "ITEM035";
            this.textBox65.Height = 0.1972222F;
            this.textBox65.Left = 0.4019139F;
            this.textBox65.Name = "textBox65";
            this.textBox65.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox65.Tag = "";
            this.textBox65.Text = "ITEM035";
            this.textBox65.Top = 5.953566F;
            this.textBox65.Width = 0.1180556F;
            // 
            // textBox66
            // 
            this.textBox66.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox66.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox66.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox66.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox66.Height = 0.1972222F;
            this.textBox66.Left = 0.5199692F;
            this.textBox66.Name = "textBox66";
            this.textBox66.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: bold; text-align: center; ddo-char-set: 1";
            this.textBox66.Tag = "";
            this.textBox66.Text = null;
            this.textBox66.Top = 5.953566F;
            this.textBox66.Width = 0.1180556F;
            // 
            // textBox67
            // 
            this.textBox67.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox67.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox67.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox67.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox67.Height = 0.1972222F;
            this.textBox67.Left = 0.6380246F;
            this.textBox67.Name = "textBox67";
            this.textBox67.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: bold; text-align: center; ddo-char-set: 1";
            this.textBox67.Tag = "";
            this.textBox67.Text = null;
            this.textBox67.Top = 5.953566F;
            this.textBox67.Width = 0.1180556F;
            // 
            // textBox68
            // 
            this.textBox68.DataField = "ITEM036";
            this.textBox68.Height = 0.1972222F;
            this.textBox68.Left = 0.7560804F;
            this.textBox68.Name = "textBox68";
            this.textBox68.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox68.Tag = "";
            this.textBox68.Text = "ITEM036";
            this.textBox68.Top = 5.953566F;
            this.textBox68.Width = 0.1180556F;
            // 
            // textBox69
            // 
            this.textBox69.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox69.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox69.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox69.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox69.Height = 0.3152778F;
            this.textBox69.Left = 0.8748305F;
            this.textBox69.Name = "textBox69";
            this.textBox69.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox69.Tag = "";
            this.textBox69.Text = "配偶者特別控除の額";
            this.textBox69.Top = 5.480649F;
            this.textBox69.Width = 0.5909724F;
            // 
            // textBox70
            // 
            this.textBox70.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox70.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox70.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox70.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox70.DataField = "ITEM037";
            this.textBox70.Height = 0.3541667F;
            this.textBox70.Left = 0.8748305F;
            this.textBox70.Name = "textBox70";
            this.textBox70.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox70.Tag = "";
            this.textBox70.Text = "ITEM037";
            this.textBox70.Top = 5.795927F;
            this.textBox70.Width = 0.5909724F;
            // 
            // textBox71
            // 
            this.textBox71.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox71.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox71.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox71.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox71.Height = 0.6298611F;
            this.textBox71.Left = 0.2838583F;
            this.textBox71.Name = "textBox71";
            this.textBox71.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.textBox71.Tag = "";
            this.textBox71.Text = "(摘要)";
            this.textBox71.Top = 6.150788F;
            this.textBox71.Width = 5.11875F;
            // 
            // textBox72
            // 
            this.textBox72.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox72.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox72.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox72.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox72.CanGrow = false;
            this.textBox72.Height = 0.3541667F;
            this.textBox72.Left = 0.2838583F;
            this.textBox72.Name = "textBox72";
            this.textBox72.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5.5pt; font" +
    "-weight: normal; text-align: center; ddo-char-set: 1";
            this.textBox72.Tag = "";
            this.textBox72.Text = "扶16養歳親未族満";
            this.textBox72.Top = 6.780649F;
            this.textBox72.Width = 0.2131944F;
            // 
            // textBox73
            // 
            this.textBox73.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox73.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox73.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox73.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox73.Height = 0.3541667F;
            this.textBox73.Left = 0.4970525F;
            this.textBox73.Name = "textBox73";
            this.textBox73.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox73.Tag = "";
            this.textBox73.Text = "未\r\n成\r\n年";
            this.textBox73.Top = 6.780649F;
            this.textBox73.Width = 0.2131944F;
            // 
            // textBox74
            // 
            this.textBox74.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox74.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox74.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox74.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox74.Height = 0.3541667F;
            this.textBox74.Left = 0.7102363F;
            this.textBox74.Name = "textBox74";
            this.textBox74.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox74.Tag = "";
            this.textBox74.Text = "外\r\n国\r\n人";
            this.textBox74.Top = 6.780846F;
            this.textBox74.Width = 0.2131944F;
            // 
            // textBox75
            // 
            this.textBox75.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox75.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox75.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox75.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox75.Height = 0.3541667F;
            this.textBox75.Left = 0.9234412F;
            this.textBox75.Name = "textBox75";
            this.textBox75.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox75.Tag = "";
            this.textBox75.Text = "死\r\n亡\r\n退";
            this.textBox75.Top = 6.780649F;
            this.textBox75.Width = 0.2131944F;
            // 
            // textBox76
            // 
            this.textBox76.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox76.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox76.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox76.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox76.Height = 0.3541667F;
            this.textBox76.Left = 1.136636F;
            this.textBox76.Name = "textBox76";
            this.textBox76.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox76.Tag = "";
            this.textBox76.Text = "災\r\n害\r\n者";
            this.textBox76.Top = 6.780649F;
            this.textBox76.Width = 0.2131944F;
            // 
            // textBox77
            // 
            this.textBox77.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox77.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox77.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox77.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox77.Height = 0.3541667F;
            this.textBox77.Left = 1.349831F;
            this.textBox77.Name = "textBox77";
            this.textBox77.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox77.Tag = "";
            this.textBox77.Text = "乙\r\n\r\n欄";
            this.textBox77.Top = 6.780649F;
            this.textBox77.Width = 0.2131944F;
            // 
            // textBox78
            // 
            this.textBox78.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox78.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox78.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox78.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox78.Height = 0.1180556F;
            this.textBox78.Left = 1.563025F;
            this.textBox78.Name = "textBox78";
            this.textBox78.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 4pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox78.Tag = "";
            this.textBox78.Text = "本人が障害者";
            this.textBox78.Top = 6.780649F;
            this.textBox78.Width = 0.4256945F;
            // 
            // textBox79
            // 
            this.textBox79.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox79.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox79.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox79.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox79.Height = 0.2361111F;
            this.textBox79.Left = 1.563025F;
            this.textBox79.Name = "textBox79";
            this.textBox79.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox79.Tag = "";
            this.textBox79.Text = "特\r\n別";
            this.textBox79.Top = 6.898704F;
            this.textBox79.Width = 0.2131944F;
            // 
            // textBox80
            // 
            this.textBox80.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox80.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox80.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox80.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox80.CanGrow = false;
            this.textBox80.Height = 0.2361111F;
            this.textBox80.Left = 1.776219F;
            this.textBox80.Name = "textBox80";
            this.textBox80.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox80.Tag = "";
            this.textBox80.Text = "そ\r\nの\r\n他";
            this.textBox80.Top = 6.898704F;
            this.textBox80.Width = 0.2131944F;
            // 
            // textBox81
            // 
            this.textBox81.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox81.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox81.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox81.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox81.Height = 0.1180556F;
            this.textBox81.Left = 1.988719F;
            this.textBox81.Name = "textBox81";
            this.textBox81.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox81.Tag = "";
            this.textBox81.Text = "寡　婦";
            this.textBox81.Top = 6.780649F;
            this.textBox81.Width = 0.4256945F;
            // 
            // textBox82
            // 
            this.textBox82.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox82.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox82.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox82.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox82.Height = 0.2361111F;
            this.textBox82.Left = 1.989413F;
            this.textBox82.Name = "textBox82";
            this.textBox82.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox82.Tag = "";
            this.textBox82.Text = "一\r\n般";
            this.textBox82.Top = 6.898704F;
            this.textBox82.Width = 0.2131944F;
            // 
            // textBox83
            // 
            this.textBox83.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox83.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox83.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox83.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox83.Height = 0.2361111F;
            this.textBox83.Left = 2.202609F;
            this.textBox83.Name = "textBox83";
            this.textBox83.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox83.Tag = "";
            this.textBox83.Text = "特\r\n別";
            this.textBox83.Top = 6.898704F;
            this.textBox83.Width = 0.2131944F;
            // 
            // textBox84
            // 
            this.textBox84.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox84.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox84.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox84.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox84.Height = 0.3622052F;
            this.textBox84.Left = 2.415748F;
            this.textBox84.Name = "textBox84";
            this.textBox84.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox84.Tag = "";
            this.textBox84.Text = "寡　夫";
            this.textBox84.Top = 6.772578F;
            this.textBox84.Width = 0.2131944F;
            // 
            // textBox85
            // 
            this.textBox85.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox85.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox85.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox85.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox85.CanGrow = false;
            this.textBox85.Height = 0.3622052F;
            this.textBox85.Left = 2.628943F;
            this.textBox85.Name = "textBox85";
            this.textBox85.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1; ddo-font-vertical: true";
            this.textBox85.Tag = "";
            this.textBox85.Text = "勤\r\n労\r\n学\r\n生";
            this.textBox85.Top = 6.772578F;
            this.textBox85.Width = 0.2131944F;
            // 
            // textBox86
            // 
            this.textBox86.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox86.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox86.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox86.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox86.Height = 0.1180556F;
            this.textBox86.Left = 3.055386F;
            this.textBox86.Name = "textBox86";
            this.textBox86.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox86.Tag = "";
            this.textBox86.Text = "退職";
            this.textBox86.Top = 6.898704F;
            this.textBox86.Width = 0.2131944F;
            // 
            // textBox87
            // 
            this.textBox87.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox87.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox87.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox87.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox87.Height = 0.1180556F;
            this.textBox87.Left = 3.26858F;
            this.textBox87.Name = "textBox87";
            this.textBox87.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox87.Tag = "";
            this.textBox87.Text = "年";
            this.textBox87.Top = 6.898704F;
            this.textBox87.Width = 0.2131944F;
            // 
            // textBox88
            // 
            this.textBox88.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox88.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox88.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox88.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox88.Height = 0.1180556F;
            this.textBox88.Left = 3.481775F;
            this.textBox88.Name = "textBox88";
            this.textBox88.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox88.Tag = "";
            this.textBox88.Text = "月";
            this.textBox88.Top = 6.898704F;
            this.textBox88.Width = 0.2131944F;
            // 
            // textBox89
            // 
            this.textBox89.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox89.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox89.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox89.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox89.Height = 0.1180556F;
            this.textBox89.Left = 3.69497F;
            this.textBox89.Name = "textBox89";
            this.textBox89.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox89.Tag = "";
            this.textBox89.Text = "日";
            this.textBox89.Top = 6.898704F;
            this.textBox89.Width = 0.2131944F;
            // 
            // textBox90
            // 
            this.textBox90.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox90.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox90.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox90.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox90.DataField = "ITEM089";
            this.textBox90.Height = 0.2756944F;
            this.textBox90.Left = 3.908163F;
            this.textBox90.Name = "textBox90";
            this.textBox90.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox90.Tag = "";
            this.textBox90.Text = "ITEM089";
            this.textBox90.Top = 7.01676F;
            this.textBox90.Width = 0.2131944F;
            // 
            // textBox91
            // 
            this.textBox91.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox91.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox91.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox91.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox91.DataField = "ITEM090";
            this.textBox91.Height = 0.2756944F;
            this.textBox91.Left = 4.121357F;
            this.textBox91.Name = "textBox91";
            this.textBox91.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox91.Tag = "";
            this.textBox91.Text = "ITEM090";
            this.textBox91.Top = 7.01676F;
            this.textBox91.Width = 0.2131944F;
            // 
            // textBox92
            // 
            this.textBox92.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox92.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox92.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox92.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox92.DataField = "ITEM091";
            this.textBox92.Height = 0.2756944F;
            this.textBox92.Left = 4.334551F;
            this.textBox92.Name = "textBox92";
            this.textBox92.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox92.Tag = "";
            this.textBox92.Text = "ITEM091";
            this.textBox92.Top = 7.01676F;
            this.textBox92.Width = 0.2131944F;
            // 
            // textBox93
            // 
            this.textBox93.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox93.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox93.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox93.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox93.DataField = "ITEM092";
            this.textBox93.Height = 0.2756944F;
            this.textBox93.Left = 4.547746F;
            this.textBox93.Name = "textBox93";
            this.textBox93.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox93.Tag = "";
            this.textBox93.Text = "ITEM092";
            this.textBox93.Top = 7.01676F;
            this.textBox93.Width = 0.2131944F;
            // 
            // textBox94
            // 
            this.textBox94.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox94.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox94.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox94.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox94.DataField = "ITEM093";
            this.textBox94.Height = 0.2756944F;
            this.textBox94.Left = 4.76094F;
            this.textBox94.Name = "textBox94";
            this.textBox94.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox94.Tag = "";
            this.textBox94.Text = "ITEM093";
            this.textBox94.Top = 7.01676F;
            this.textBox94.Width = 0.2131944F;
            // 
            // textBox95
            // 
            this.textBox95.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox95.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox95.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox95.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox95.Height = 0.1180556F;
            this.textBox95.Left = 3.908163F;
            this.textBox95.Name = "textBox95";
            this.textBox95.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox95.Tag = "";
            this.textBox95.Text = "受　給　者　生　年　月　日";
            this.textBox95.Top = 6.780649F;
            this.textBox95.Width = 1.493056F;
            // 
            // textBox96
            // 
            this.textBox96.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox96.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox96.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox96.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox96.Height = 0.1180556F;
            this.textBox96.Left = 3.908163F;
            this.textBox96.Name = "textBox96";
            this.textBox96.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox96.Tag = "";
            this.textBox96.Text = "明";
            this.textBox96.Top = 6.898704F;
            this.textBox96.Width = 0.2131944F;
            // 
            // textBox97
            // 
            this.textBox97.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox97.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox97.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox97.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox97.Height = 0.1180556F;
            this.textBox97.Left = 4.121357F;
            this.textBox97.Name = "textBox97";
            this.textBox97.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox97.Tag = "";
            this.textBox97.Text = "大";
            this.textBox97.Top = 6.898704F;
            this.textBox97.Width = 0.2131944F;
            // 
            // textBox98
            // 
            this.textBox98.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox98.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox98.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox98.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox98.Height = 0.1180556F;
            this.textBox98.Left = 4.334551F;
            this.textBox98.Name = "textBox98";
            this.textBox98.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox98.Tag = "";
            this.textBox98.Text = "昭";
            this.textBox98.Top = 6.898704F;
            this.textBox98.Width = 0.2131944F;
            // 
            // textBox99
            // 
            this.textBox99.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox99.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox99.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox99.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox99.Height = 0.1180556F;
            this.textBox99.Left = 4.547746F;
            this.textBox99.Name = "textBox99";
            this.textBox99.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox99.Tag = "";
            this.textBox99.Text = "平";
            this.textBox99.Top = 6.898704F;
            this.textBox99.Width = 0.2131944F;
            // 
            // textBox100
            // 
            this.textBox100.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox100.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox100.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox100.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox100.Height = 0.1180556F;
            this.textBox100.Left = 4.76094F;
            this.textBox100.Name = "textBox100";
            this.textBox100.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox100.Tag = "";
            this.textBox100.Text = "年";
            this.textBox100.Top = 6.898704F;
            this.textBox100.Width = 0.2131944F;
            // 
            // textBox101
            // 
            this.textBox101.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox101.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox101.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox101.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox101.DataField = "ITEM094";
            this.textBox101.Height = 0.2756944F;
            this.textBox101.Left = 4.974135F;
            this.textBox101.Name = "textBox101";
            this.textBox101.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox101.Tag = "";
            this.textBox101.Text = "ITEM094";
            this.textBox101.Top = 7.01676F;
            this.textBox101.Width = 0.2131948F;
            // 
            // textBox102
            // 
            this.textBox102.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox102.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox102.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox102.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox102.Height = 0.1180556F;
            this.textBox102.Left = 4.974135F;
            this.textBox102.Name = "textBox102";
            this.textBox102.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox102.Tag = "";
            this.textBox102.Text = "月";
            this.textBox102.Top = 6.898704F;
            this.textBox102.Width = 0.2131948F;
            // 
            // textBox103
            // 
            this.textBox103.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox103.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox103.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox103.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox103.DataField = "ITEM095";
            this.textBox103.Height = 0.2756944F;
            this.textBox103.Left = 5.18733F;
            this.textBox103.Name = "textBox103";
            this.textBox103.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox103.Tag = "";
            this.textBox103.Text = "ITEM095";
            this.textBox103.Top = 7.01676F;
            this.textBox103.Width = 0.2138891F;
            // 
            // textBox104
            // 
            this.textBox104.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox104.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox104.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox104.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox104.Height = 0.1180556F;
            this.textBox104.Left = 5.18733F;
            this.textBox104.Name = "textBox104";
            this.textBox104.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox104.Tag = "";
            this.textBox104.Text = "日";
            this.textBox104.Top = 6.898704F;
            this.textBox104.Width = 0.2138891F;
            // 
            // textBox105
            // 
            this.textBox105.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox105.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox105.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox105.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox105.Height = 0.1576389F;
            this.textBox105.Left = 1.14983F;
            this.textBox105.Name = "textBox105";
            this.textBox105.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox105.Tag = "";
            this.textBox105.Text = "支　払　金　額";
            this.textBox105.Top = 5.007732F;
            this.textBox105.Width = 1.063194F;
            // 
            // textBox106
            // 
            this.textBox106.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox106.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox106.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox106.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox106.DataField = "ITEM031";
            this.textBox106.Height = 0.3152778F;
            this.textBox106.Left = 2.213025F;
            this.textBox106.Name = "textBox106";
            this.textBox106.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; vertical-align: bottom; ddo-char-set: 1";
            this.textBox106.Tag = "";
            this.textBox106.Text = "ITEM031";
            this.textBox106.Top = 5.165371F;
            this.textBox106.Width = 1.063194F;
            // 
            // textBox107
            // 
            this.textBox107.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox107.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox107.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox107.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox107.Height = 0.1576389F;
            this.textBox107.Left = 2.213025F;
            this.textBox107.Name = "textBox107";
            this.textBox107.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox107.Tag = "";
            this.textBox107.Text = "給与所得控除後の金額";
            this.textBox107.Top = 5.007732F;
            this.textBox107.Width = 1.063194F;
            // 
            // textBox108
            // 
            this.textBox108.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox108.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox108.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox108.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox108.DataField = "ITEM032";
            this.textBox108.Height = 0.3152778F;
            this.textBox108.Left = 3.276219F;
            this.textBox108.Name = "textBox108";
            this.textBox108.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; vertical-align: bottom; ddo-char-set: 1";
            this.textBox108.Tag = "";
            this.textBox108.Text = "ITEM032";
            this.textBox108.Top = 5.165371F;
            this.textBox108.Width = 1.063194F;
            // 
            // textBox109
            // 
            this.textBox109.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox109.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox109.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox109.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox109.Height = 0.1576389F;
            this.textBox109.Left = 3.276219F;
            this.textBox109.Name = "textBox109";
            this.textBox109.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox109.Tag = "";
            this.textBox109.Text = "所得控除の額の合計額";
            this.textBox109.Top = 5.007732F;
            this.textBox109.Width = 1.063194F;
            // 
            // textBox110
            // 
            this.textBox110.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox110.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox110.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox110.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox110.DataField = "ITEM033";
            this.textBox110.Height = 0.3152778F;
            this.textBox110.Left = 4.339412F;
            this.textBox110.Name = "textBox110";
            this.textBox110.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; vertical-align: bottom; ddo-char-set: 1";
            this.textBox110.Tag = "";
            this.textBox110.Text = "ITEM033";
            this.textBox110.Top = 5.165371F;
            this.textBox110.Width = 1.063194F;
            // 
            // textBox111
            // 
            this.textBox111.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox111.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox111.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox111.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox111.Height = 0.1576389F;
            this.textBox111.Left = 4.339412F;
            this.textBox111.Name = "textBox111";
            this.textBox111.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox111.Tag = "";
            this.textBox111.Text = "源泉徴収税額";
            this.textBox111.Top = 5.007732F;
            this.textBox111.Width = 1.063194F;
            // 
            // textBox112
            // 
            this.textBox112.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox112.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox112.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox112.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox112.Height = 0.7519687F;
            this.textBox112.Left = 3.276378F;
            this.textBox112.Name = "textBox112";
            this.textBox112.Style = "background-color: White; font-family: ＭＳ 明朝; font-size: 8pt; font-weight: normal;" +
    " text-align: center; ddo-char-set: 1";
            this.textBox112.Tag = "";
            this.textBox112.Text = " 氏　\r\n\r\n\r\n名";
            this.textBox112.Top = 4.255649F;
            this.textBox112.Width = 0.157639F;
            // 
            // textBox113
            // 
            this.textBox113.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox113.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox113.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox113.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox113.Height = 0.3152778F;
            this.textBox113.Left = 4.811635F;
            this.textBox113.Name = "textBox113";
            this.textBox113.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox113.Tag = "";
            this.textBox113.Text = "住宅借入金等特別控除の額";
            this.textBox113.Top = 5.480649F;
            this.textBox113.Width = 0.5909724F;
            // 
            // textBox114
            // 
            this.textBox114.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox114.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox114.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox114.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox114.DataField = "ITEM049";
            this.textBox114.Height = 0.3541667F;
            this.textBox114.Left = 4.811635F;
            this.textBox114.Name = "textBox114";
            this.textBox114.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: right; vertical-align: bottom; ddo-char-set: 1";
            this.textBox114.Tag = "";
            this.textBox114.Text = "ITEM049";
            this.textBox114.Top = 5.795927F;
            this.textBox114.Width = 0.5909724F;
            // 
            // textBox115
            // 
            this.textBox115.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox115.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox115.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox115.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox115.Height = 0.3152778F;
            this.textBox115.Left = 3.039415F;
            this.textBox115.Name = "textBox115";
            this.textBox115.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox115.Tag = "";
            this.textBox115.Text = "社会保険料等 の 金 額";
            this.textBox115.Top = 5.480649F;
            this.textBox115.Width = 0.7090278F;
            // 
            // textBox116
            // 
            this.textBox116.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox116.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox116.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox116.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox116.Height = 0.3152778F;
            this.textBox116.Left = 4.339412F;
            this.textBox116.Name = "textBox116";
            this.textBox116.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox116.Tag = "";
            this.textBox116.Text = "地震保険料の控除額";
            this.textBox116.Top = 5.480649F;
            this.textBox116.Width = 0.4722223F;
            // 
            // textBox117
            // 
            this.textBox117.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox117.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox117.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox117.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox117.DataField = "ITEM048";
            this.textBox117.Height = 0.3541667F;
            this.textBox117.Left = 4.339412F;
            this.textBox117.Name = "textBox117";
            this.textBox117.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: right; vertical-align: bottom; ddo-char-set: 1";
            this.textBox117.Tag = "";
            this.textBox117.Text = "ITEM048";
            this.textBox117.Top = 5.795927F;
            this.textBox117.Width = 0.4722223F;
            // 
            // textBox118
            // 
            this.textBox118.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox118.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox118.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox118.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox118.Height = 0.3152778F;
            this.textBox118.Left = 3.748442F;
            this.textBox118.Name = "textBox118";
            this.textBox118.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox118.Tag = "";
            this.textBox118.Text = "生命保険料の 控 除 額";
            this.textBox118.Top = 5.480649F;
            this.textBox118.Width = 0.5909724F;
            // 
            // textBox119
            // 
            this.textBox119.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox119.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox119.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox119.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox119.DataField = "ITEM047";
            this.textBox119.Height = 0.3541667F;
            this.textBox119.Left = 3.748442F;
            this.textBox119.Name = "textBox119";
            this.textBox119.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: right; vertical-align: bottom; ddo-char-set: 1";
            this.textBox119.Tag = "";
            this.textBox119.Text = "ITEM047";
            this.textBox119.Top = 5.795927F;
            this.textBox119.Width = 0.5909724F;
            // 
            // textBox120
            // 
            this.textBox120.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox120.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox120.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox120.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox120.Height = 0.1972222F;
            this.textBox120.Left = 2.488719F;
            this.textBox120.Name = "textBox120";
            this.textBox120.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox120.Tag = "";
            this.textBox120.Text = "障害者の数(本人を除く)";
            this.textBox120.Top = 5.480649F;
            this.textBox120.Width = 0.5513887F;
            // 
            // textBox121
            // 
            this.textBox121.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox121.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox121.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox121.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox121.Height = 0.1180556F;
            this.textBox121.Left = 1.465108F;
            this.textBox121.Name = "textBox121";
            this.textBox121.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox121.Tag = "";
            this.textBox121.Text = "特定";
            this.textBox121.Top = 5.677871F;
            this.textBox121.Width = 0.2756944F;
            // 
            // textBox124
            // 
            this.textBox124.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox124.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox124.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox124.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox124.Height = 0.1180556F;
            this.textBox124.Left = 1.740802F;
            this.textBox124.Name = "textBox124";
            this.textBox124.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox124.Tag = "";
            this.textBox124.Text = "老　人";
            this.textBox124.Top = 5.677871F;
            this.textBox124.Width = 0.4333334F;
            // 
            // textBox126
            // 
            this.textBox126.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox126.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox126.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox126.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox126.Height = 0.3541667F;
            this.textBox126.Left = 2.023622F;
            this.textBox126.Name = "textBox126";
            this.textBox126.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: bold; tex" +
    "t-align: center; ddo-char-set: 1";
            this.textBox126.Tag = "";
            this.textBox126.Text = null;
            this.textBox126.Top = 5.795806F;
            this.textBox126.Width = 0.1496062F;
            // 
            // textBox128
            // 
            this.textBox128.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox128.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox128.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox128.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox128.Height = 0.1180556F;
            this.textBox128.Left = 2.174136F;
            this.textBox128.Name = "textBox128";
            this.textBox128.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox128.Tag = "";
            this.textBox128.Text = "その他";
            this.textBox128.Top = 5.677871F;
            this.textBox128.Width = 0.3152781F;
            // 
            // textBox129
            // 
            this.textBox129.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox129.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox129.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox129.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox129.DataField = "ITEM041";
            this.textBox129.Height = 0.3541667F;
            this.textBox129.Left = 2.174136F;
            this.textBox129.Name = "textBox129";
            this.textBox129.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox129.Tag = "";
            this.textBox129.Text = "ITEM041";
            this.textBox129.Top = 5.795927F;
            this.textBox129.Width = 0.157639F;
            // 
            // textBox130
            // 
            this.textBox130.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox130.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox130.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox130.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox130.Height = 0.3541667F;
            this.textBox130.Left = 2.331775F;
            this.textBox130.Name = "textBox130";
            this.textBox130.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: bold; tex" +
    "t-align: center; ddo-char-set: 1";
            this.textBox130.Tag = "";
            this.textBox130.Text = null;
            this.textBox130.Top = 5.795927F;
            this.textBox130.Width = 0.157639F;
            // 
            // textBox131
            // 
            this.textBox131.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox131.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox131.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox131.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox131.Height = 0.1180556F;
            this.textBox131.Left = 2.489413F;
            this.textBox131.Name = "textBox131";
            this.textBox131.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox131.Tag = "";
            this.textBox131.Text = "特別";
            this.textBox131.Top = 5.677871F;
            this.textBox131.Width = 0.3152781F;
            // 
            // textBox132
            // 
            this.textBox132.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox132.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox132.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox132.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox132.DataField = "ITEM042";
            this.textBox132.Height = 0.3541667F;
            this.textBox132.Left = 2.489413F;
            this.textBox132.Name = "textBox132";
            this.textBox132.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox132.Tag = "";
            this.textBox132.Text = "ITEM042";
            this.textBox132.Top = 5.795927F;
            this.textBox132.Width = 0.157639F;
            // 
            // textBox133
            // 
            this.textBox133.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox133.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox133.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox133.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox133.DataField = "ITEM043";
            this.textBox133.Height = 0.3541667F;
            this.textBox133.Left = 2.647052F;
            this.textBox133.Name = "textBox133";
            this.textBox133.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox133.Tag = "";
            this.textBox133.Text = "ITEM043";
            this.textBox133.Top = 5.795927F;
            this.textBox133.Width = 0.157639F;
            // 
            // textBox134
            // 
            this.textBox134.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox134.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox134.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox134.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox134.DataField = "ITEM044";
            this.textBox134.Height = 0.3541667F;
            this.textBox134.Left = 2.804692F;
            this.textBox134.Name = "textBox134";
            this.textBox134.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox134.Tag = "";
            this.textBox134.Text = "ITEM044";
            this.textBox134.Top = 5.795927F;
            this.textBox134.Width = 0.2361112F;
            // 
            // textBox135
            // 
            this.textBox135.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox135.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox135.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox135.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox135.Height = 0.1180556F;
            this.textBox135.Left = 2.804725F;
            this.textBox135.Name = "textBox135";
            this.textBox135.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 4.5pt; font" +
    "-weight: normal; text-align: center; ddo-char-set: 1";
            this.textBox135.Tag = "";
            this.textBox135.Text = "その他";
            this.textBox135.Top = 5.677696F;
            this.textBox135.Width = 0.2322831F;
            // 
            // textBox136
            // 
            this.textBox136.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox136.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox136.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox136.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox136.CanGrow = false;
            this.textBox136.DataField = "ITEM064";
            this.textBox136.Height = 0.1576389F;
            this.textBox136.Left = 4.693579F;
            this.textBox136.Name = "textBox136";
            this.textBox136.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; ddo-char-set: 1";
            this.textBox136.Tag = "";
            this.textBox136.Text = "ITEM064";
            this.textBox136.Top = 6.150093F;
            this.textBox136.Width = 0.7090278F;
            // 
            // textBox137
            // 
            this.textBox137.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox137.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox137.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox137.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox137.Height = 0.1576389F;
            this.textBox137.Left = 3.866498F;
            this.textBox137.Name = "textBox137";
            this.textBox137.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox137.Tag = "";
            this.textBox137.Text = "介護医療保険料の金額";
            this.textBox137.Top = 6.150093F;
            this.textBox137.Width = 0.8270836F;
            // 
            // textBox138
            // 
            this.textBox138.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox138.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox138.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox138.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox138.CanGrow = false;
            this.textBox138.DataField = "ITEM065";
            this.textBox138.Height = 0.1576389F;
            this.textBox138.Left = 4.693579F;
            this.textBox138.Name = "textBox138";
            this.textBox138.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; ddo-char-set: 1";
            this.textBox138.Tag = "";
            this.textBox138.Text = "ITEM065";
            this.textBox138.Top = 6.307732F;
            this.textBox138.Width = 0.7090278F;
            // 
            // textBox139
            // 
            this.textBox139.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox139.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox139.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox139.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox139.Height = 0.1576389F;
            this.textBox139.Left = 3.866498F;
            this.textBox139.Name = "textBox139";
            this.textBox139.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox139.Tag = "";
            this.textBox139.Text = "新個人年金保険料の金額";
            this.textBox139.Top = 6.307732F;
            this.textBox139.Width = 0.8270836F;
            // 
            // textBox140
            // 
            this.textBox140.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox140.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox140.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox140.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox140.CanGrow = false;
            this.textBox140.DataField = "ITEM061";
            this.textBox140.Height = 0.1576389F;
            this.textBox140.Left = 3.157469F;
            this.textBox140.Name = "textBox140";
            this.textBox140.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; ddo-char-set: 1";
            this.textBox140.Tag = "";
            this.textBox140.Text = "ITEM061";
            this.textBox140.Top = 6.307732F;
            this.textBox140.Width = 0.7090278F;
            // 
            // textBox141
            // 
            this.textBox141.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox141.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox141.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox141.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox141.Height = 0.1576389F;
            this.textBox141.Left = 2.419275F;
            this.textBox141.Name = "textBox141";
            this.textBox141.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox141.Tag = "";
            this.textBox141.Text = "配偶者の合計所得";
            this.textBox141.Top = 6.307732F;
            this.textBox141.Width = 0.7375002F;
            // 
            // textBox142
            // 
            this.textBox142.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox142.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox142.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox142.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox142.CanGrow = false;
            this.textBox142.DataField = "ITEM066";
            this.textBox142.Height = 0.1576389F;
            this.textBox142.Left = 4.693579F;
            this.textBox142.Name = "textBox142";
            this.textBox142.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; ddo-char-set: 1";
            this.textBox142.Tag = "";
            this.textBox142.Text = "ITEM066";
            this.textBox142.Top = 6.465372F;
            this.textBox142.Width = 0.7090278F;
            // 
            // textBox143
            // 
            this.textBox143.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox143.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox143.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox143.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox143.Height = 0.1576389F;
            this.textBox143.Left = 3.866498F;
            this.textBox143.Name = "textBox143";
            this.textBox143.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox143.Tag = "";
            this.textBox143.Text = "旧個人年金保険料の金額";
            this.textBox143.Top = 6.465372F;
            this.textBox143.Width = 0.8270836F;
            // 
            // textBox144
            // 
            this.textBox144.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox144.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox144.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox144.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox144.CanGrow = false;
            this.textBox144.DataField = "ITEM062";
            this.textBox144.Height = 0.1576389F;
            this.textBox144.Left = 3.157469F;
            this.textBox144.Name = "textBox144";
            this.textBox144.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; ddo-char-set: 1";
            this.textBox144.Tag = "";
            this.textBox144.Text = "ITEM062";
            this.textBox144.Top = 6.465372F;
            this.textBox144.Width = 0.7090278F;
            // 
            // textBox145
            // 
            this.textBox145.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox145.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox145.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox145.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox145.Height = 0.1576389F;
            this.textBox145.Left = 2.419275F;
            this.textBox145.Name = "textBox145";
            this.textBox145.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox145.Tag = "";
            this.textBox145.Text = "新生命保険料の金額\r\n";
            this.textBox145.Top = 6.465372F;
            this.textBox145.Width = 0.7375002F;
            // 
            // textBox146
            // 
            this.textBox146.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox146.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox146.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox146.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox146.CanGrow = false;
            this.textBox146.DataField = "ITEM067";
            this.textBox146.Height = 0.1576389F;
            this.textBox146.Left = 4.693579F;
            this.textBox146.Name = "textBox146";
            this.textBox146.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; ddo-char-set: 1";
            this.textBox146.Tag = "";
            this.textBox146.Text = "ITEM067";
            this.textBox146.Top = 6.62301F;
            this.textBox146.Width = 0.7090278F;
            // 
            // textBox147
            // 
            this.textBox147.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox147.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox147.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox147.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox147.Height = 0.1576389F;
            this.textBox147.Left = 3.866498F;
            this.textBox147.Name = "textBox147";
            this.textBox147.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox147.Tag = "";
            this.textBox147.Text = "旧長期損害保険料の金額";
            this.textBox147.Top = 6.62301F;
            this.textBox147.Width = 0.8270836F;
            // 
            // textBox148
            // 
            this.textBox148.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox148.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox148.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox148.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox148.CanGrow = false;
            this.textBox148.DataField = "ITEM063";
            this.textBox148.Height = 0.1576389F;
            this.textBox148.Left = 3.157469F;
            this.textBox148.Name = "textBox148";
            this.textBox148.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; ddo-char-set: 1";
            this.textBox148.Tag = "";
            this.textBox148.Text = "ITEM063";
            this.textBox148.Top = 6.62301F;
            this.textBox148.Width = 0.7090278F;
            // 
            // textBox149
            // 
            this.textBox149.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox149.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox149.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox149.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox149.Height = 0.1576389F;
            this.textBox149.Left = 2.419275F;
            this.textBox149.Name = "textBox149";
            this.textBox149.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox149.Tag = "";
            this.textBox149.Text = "旧生命保険料の金額";
            this.textBox149.Top = 6.62301F;
            this.textBox149.Width = 0.7375002F;
            // 
            // textBox150
            // 
            this.textBox150.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox150.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox150.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox150.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox150.Height = 0.3944443F;
            this.textBox150.Left = 0.2838583F;
            this.textBox150.Name = "textBox150";
            this.textBox150.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox150.Tag = "";
            this.textBox150.Text = "支払者";
            this.textBox150.Top = 7.292455F;
            this.textBox150.Width = 0.2131944F;
            // 
            // textBox151
            // 
            this.textBox151.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox151.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox151.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox151.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox151.Height = 0.1972222F;
            this.textBox151.Left = 0.4970525F;
            this.textBox151.Name = "textBox151";
            this.textBox151.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox151.Tag = "";
            this.textBox151.Text = "住所(居所)\r\n又は所在地 ";
            this.textBox151.Top = 7.292455F;
            this.textBox151.Width = 0.6374998F;
            // 
            // textBox152
            // 
            this.textBox152.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox152.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox152.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox152.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox152.CanGrow = false;
            this.textBox152.Height = 0.1972222F;
            this.textBox152.Left = 0.4970525F;
            this.textBox152.Name = "textBox152";
            this.textBox152.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox152.Tag = "";
            this.textBox152.Text = "氏 名 又 は\r\n名　　　 称";
            this.textBox152.Top = 7.489677F;
            this.textBox152.Width = 0.6374998F;
            // 
            // textBox153
            // 
            this.textBox153.DataField = "ITEM098";
            this.textBox153.Height = 0.1576389F;
            this.textBox153.Left = 4.023441F;
            this.textBox153.Name = "textBox153";
            this.textBox153.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.textBox153.Tag = "";
            this.textBox153.Text = "ITEM098";
            this.textBox153.Top = 7.505649F;
            this.textBox153.Width = 1.358334F;
            // 
            // label2
            // 
            this.label2.Height = 0.1180556F;
            this.label2.HyperLink = null;
            this.label2.Left = 0.5859416F;
            this.label2.Name = "label2";
            this.label2.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.label2.Tag = "";
            this.label2.Text = "住宅借入金等特別控除可能額";
            this.label2.Top = 6.161899F;
            this.label2.Width = 1.141666F;
            // 
            // textBox154
            // 
            this.textBox154.DataField = "ITEM050";
            this.textBox154.Height = 0.1180556F;
            this.textBox154.Left = 1.727608F;
            this.textBox154.Name = "textBox154";
            this.textBox154.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.textBox154.Tag = "";
            this.textBox154.Text = "ITEM050";
            this.textBox154.Top = 6.161899F;
            this.textBox154.Width = 0.527778F;
            // 
            // label3
            // 
            this.label3.Height = 0.1180556F;
            this.label3.HyperLink = null;
            this.label3.Left = 2.34252F;
            this.label3.Name = "label3";
            this.label3.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.label3.Tag = "";
            this.label3.Text = "国民年金保険料等の金額";
            this.label3.Top = 6.161948F;
            this.label3.Width = 0.9451389F;
            // 
            // textBox155
            // 
            this.textBox155.DataField = "ITEM051";
            this.textBox155.Height = 0.1180556F;
            this.textBox155.Left = 3.287008F;
            this.textBox155.Name = "textBox155";
            this.textBox155.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.textBox155.Tag = "";
            this.textBox155.Text = "ITEM051";
            this.textBox155.Top = 6.161948F;
            this.textBox155.Width = 0.5673835F;
            // 
            // textBox156
            // 
            this.textBox156.DataField = "ITEM052";
            this.textBox156.Height = 0.1416667F;
            this.textBox156.Left = 0.5859416F;
            this.textBox156.Name = "textBox156";
            this.textBox156.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.textBox156.Tag = "";
            this.textBox156.Text = "ITEM052";
            this.textBox156.Top = 6.279955F;
            this.textBox156.Width = 1.784028F;
            // 
            // textBox157
            // 
            this.textBox157.DataField = "ITEM053";
            this.textBox157.Height = 0.1104167F;
            this.textBox157.Left = 0.5859416F;
            this.textBox157.Name = "textBox157";
            this.textBox157.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.textBox157.Tag = "";
            this.textBox157.Text = "ITEM053";
            this.textBox157.Top = 6.390371F;
            this.textBox157.Width = 1.784028F;
            // 
            // textBox158
            // 
            this.textBox158.DataField = "ITEM054";
            this.textBox158.Height = 0.1104167F;
            this.textBox158.Left = 0.5858268F;
            this.textBox158.Name = "textBox158";
            this.textBox158.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.textBox158.Tag = "";
            this.textBox158.Text = "ITEM054";
            this.textBox158.Top = 6.50998F;
            this.textBox158.Width = 1.784028F;
            // 
            // textBox159
            // 
            this.textBox159.DataField = "ITEM055";
            this.textBox159.Height = 0.07847222F;
            this.textBox159.Left = 0.3234415F;
            this.textBox159.Name = "textBox159";
            this.textBox159.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.textBox159.Tag = "";
            this.textBox159.Text = "ITEM055";
            this.textBox159.Top = 6.611206F;
            this.textBox159.Width = 0.6694443F;
            // 
            // textBox160
            // 
            this.textBox160.DataField = "ITEM058";
            this.textBox160.Height = 0.07847222F;
            this.textBox160.Left = 0.3234415F;
            this.textBox160.Name = "textBox160";
            this.textBox160.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.textBox160.Tag = "";
            this.textBox160.Text = "ITEM058";
            this.textBox160.Top = 6.689677F;
            this.textBox160.Width = 0.6694443F;
            // 
            // textBox161
            // 
            this.textBox161.DataField = "ITEM056";
            this.textBox161.Height = 0.07847222F;
            this.textBox161.Left = 1.023442F;
            this.textBox161.Name = "textBox161";
            this.textBox161.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.textBox161.Tag = "";
            this.textBox161.Text = "ITEM056";
            this.textBox161.Top = 6.609816F;
            this.textBox161.Width = 0.6694446F;
            // 
            // textBox162
            // 
            this.textBox162.DataField = "ITEM059";
            this.textBox162.Height = 0.07847222F;
            this.textBox162.Left = 1.023442F;
            this.textBox162.Name = "textBox162";
            this.textBox162.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.textBox162.Tag = "";
            this.textBox162.Text = "ITEM059";
            this.textBox162.Top = 6.688288F;
            this.textBox162.Width = 0.6694446F;
            // 
            // textBox163
            // 
            this.textBox163.DataField = "ITEM057";
            this.textBox163.Height = 0.07847222F;
            this.textBox163.Left = 1.721358F;
            this.textBox163.Name = "textBox163";
            this.textBox163.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.textBox163.Tag = "";
            this.textBox163.Text = "ITEM057";
            this.textBox163.Top = 6.609816F;
            this.textBox163.Width = 0.6694446F;
            // 
            // textBox164
            // 
            this.textBox164.DataField = "ITEM060";
            this.textBox164.Height = 0.07847222F;
            this.textBox164.Left = 1.721358F;
            this.textBox164.Name = "textBox164";
            this.textBox164.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.textBox164.Tag = "";
            this.textBox164.Text = "ITEM060";
            this.textBox164.Top = 6.688288F;
            this.textBox164.Width = 0.6694446F;
            // 
            // label4
            // 
            this.label4.Height = 0.08611111F;
            this.label4.HyperLink = null;
            this.label4.Left = 1.158778F;
            this.label4.Name = "label4";
            this.label4.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.label4.Tag = "";
            this.label4.Text = "内　　　　　　　　　　　 　円";
            this.label4.Top = 5.169428F;
            this.label4.Width = 1.054921F;
            // 
            // label5
            // 
            this.label5.Height = 0.08611111F;
            this.label5.HyperLink = null;
            this.label5.Left = 4.356693F;
            this.label5.Name = "label5";
            this.label5.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.label5.Tag = "";
            this.label5.Text = "内　　　　　　　　　　　 　円";
            this.label5.Top = 5.169428F;
            this.label5.Width = 1.035353F;
            // 
            // label6
            // 
            this.label6.Height = 0.08611111F;
            this.label6.HyperLink = null;
            this.label6.Left = 4.14836F;
            this.label6.Name = "label6";
            this.label6.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label6.Tag = "";
            this.label6.Text = "円";
            this.label6.Top = 5.169428F;
            this.label6.Width = 0.1666665F;
            // 
            // label7
            // 
            this.label7.Height = 0.08611111F;
            this.label7.HyperLink = null;
            this.label7.Left = 3.085861F;
            this.label7.Name = "label7";
            this.label7.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label7.Tag = "";
            this.label7.Text = "円";
            this.label7.Top = 5.169428F;
            this.label7.Width = 0.1666665F;
            // 
            // textBox165
            // 
            this.textBox165.DataField = "ITEM046";
            this.textBox165.Height = 0.09582744F;
            this.textBox165.Left = 3.065108F;
            this.textBox165.Name = "textBox165";
            this.textBox165.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox165.Tag = "";
            this.textBox165.Text = "ITEM046";
            this.textBox165.Top = 5.891066F;
            this.textBox165.Width = 0.6673613F;
            // 
            // label8
            // 
            this.label8.Height = 0.08611111F;
            this.label8.HyperLink = null;
            this.label8.Left = 1.294275F;
            this.label8.Name = "label8";
            this.label8.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label8.Tag = "";
            this.label8.Text = "円";
            this.label8.Top = 5.797316F;
            this.label8.Width = 0.1666665F;
            // 
            // label14
            // 
            this.label14.Height = 0.08611111F;
            this.label14.HyperLink = null;
            this.label14.Left = 2.179692F;
            this.label14.Name = "label14";
            this.label14.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label14.Tag = "";
            this.label14.Text = "人";
            this.label14.Top = 5.797316F;
            this.label14.Width = 0.15625F;
            // 
            // label15
            // 
            this.label15.Height = 0.08611111F;
            this.label15.HyperLink = null;
            this.label15.Left = 2.502608F;
            this.label15.Name = "label15";
            this.label15.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.label15.Tag = "";
            this.label15.Text = "内人";
            this.label15.Top = 5.807733F;
            this.label15.Width = 0.2916665F;
            // 
            // label16
            // 
            this.label16.Height = 0.08611111F;
            this.label16.HyperLink = null;
            this.label16.Left = 2.867192F;
            this.label16.Name = "label16";
            this.label16.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label16.Tag = "";
            this.label16.Text = "人";
            this.label16.Top = 5.807733F;
            this.label16.Width = 0.15625F;
            // 
            // label17
            // 
            this.label17.Height = 0.08611111F;
            this.label17.HyperLink = null;
            this.label17.Left = 4.169274F;
            this.label17.Name = "label17";
            this.label17.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label17.Tag = "";
            this.label17.Text = "円";
            this.label17.Top = 5.807733F;
            this.label17.Width = 0.1666665F;
            // 
            // label18
            // 
            this.label18.Height = 0.08611111F;
            this.label18.HyperLink = null;
            this.label18.Left = 4.638024F;
            this.label18.Name = "label18";
            this.label18.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label18.Tag = "";
            this.label18.Text = "円";
            this.label18.Top = 5.807733F;
            this.label18.Width = 0.1666665F;
            // 
            // label19
            // 
            this.label19.Height = 0.08611111F;
            this.label19.HyperLink = null;
            this.label19.Left = 5.221357F;
            this.label19.Name = "label19";
            this.label19.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label19.Tag = "";
            this.label19.Text = "円";
            this.label19.Top = 5.807733F;
            this.label19.Width = 0.166667F;
            // 
            // label20
            // 
            this.label20.Height = 0.08611111F;
            this.label20.HyperLink = null;
            this.label20.Left = 3.700525F;
            this.label20.Name = "label20";
            this.label20.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label20.Tag = "";
            this.label20.Text = "円";
            this.label20.Top = 6.307732F;
            this.label20.Width = 0.1666665F;
            // 
            // label21
            // 
            this.label21.Height = 0.08611111F;
            this.label21.HyperLink = null;
            this.label21.Left = 3.700525F;
            this.label21.Name = "label21";
            this.label21.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label21.Tag = "";
            this.label21.Text = "円";
            this.label21.Top = 6.463982F;
            this.label21.Width = 0.1666665F;
            // 
            // label22
            // 
            this.label22.Height = 0.08611111F;
            this.label22.HyperLink = null;
            this.label22.Left = 3.700525F;
            this.label22.Name = "label22";
            this.label22.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label22.Tag = "";
            this.label22.Text = "円";
            this.label22.Top = 6.620232F;
            this.label22.Width = 0.1666665F;
            // 
            // label23
            // 
            this.label23.Height = 0.08611111F;
            this.label23.HyperLink = null;
            this.label23.Left = 5.231774F;
            this.label23.Name = "label23";
            this.label23.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label23.Tag = "";
            this.label23.Text = "円";
            this.label23.Top = 6.307732F;
            this.label23.Width = 0.166667F;
            // 
            // label24
            // 
            this.label24.Height = 0.08611111F;
            this.label24.HyperLink = null;
            this.label24.Left = 5.231774F;
            this.label24.Name = "label24";
            this.label24.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label24.Tag = "";
            this.label24.Text = "円";
            this.label24.Top = 6.463982F;
            this.label24.Width = 0.166667F;
            // 
            // label25
            // 
            this.label25.Height = 0.08611111F;
            this.label25.HyperLink = null;
            this.label25.Left = 5.231774F;
            this.label25.Name = "label25";
            this.label25.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label25.Tag = "";
            this.label25.Text = "円";
            this.label25.Top = 6.620232F;
            this.label25.Width = 0.166667F;
            // 
            // label26
            // 
            this.label26.Height = 0.08611111F;
            this.label26.HyperLink = null;
            this.label26.Left = 5.231774F;
            this.label26.Name = "label26";
            this.label26.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label26.Tag = "";
            this.label26.Text = "円";
            this.label26.Top = 6.151483F;
            this.label26.Width = 0.166667F;
            // 
            // textBox166
            // 
            this.textBox166.CanGrow = false;
            this.textBox166.DataField = "ITEM025";
            this.textBox166.Height = 0.1472222F;
            this.textBox166.Left = 4.013024F;
            this.textBox166.Name = "textBox166";
            this.textBox166.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: left; ddo-char-set: 1";
            this.textBox166.Tag = "";
            this.textBox166.Text = "ITEM025";
            this.textBox166.Top = 4.286899F;
            this.textBox166.Width = 1.364583F;
            // 
            // textBox167
            // 
            this.textBox167.DataField = "ITEM026";
            this.textBox167.Height = 0.1576389F;
            this.textBox167.Left = 3.898442F;
            this.textBox167.Name = "textBox167";
            this.textBox167.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: left; ddo-char-set: 1";
            this.textBox167.Tag = "";
            this.textBox167.Text = "ITEM026";
            this.textBox167.Top = 4.474399F;
            this.textBox167.Width = 1.479167F;
            // 
            // textBox168
            // 
            this.textBox168.DataField = "ITEM027";
            this.textBox168.Height = 0.1368055F;
            this.textBox168.Left = 3.898442F;
            this.textBox168.Name = "textBox168";
            this.textBox168.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.textBox168.Tag = "";
            this.textBox168.Text = "ITEM027";
            this.textBox168.Top = 4.672316F;
            this.textBox168.Width = 1.479167F;
            // 
            // textBox169
            // 
            this.textBox169.DataField = "ITEM028";
            this.textBox169.Height = 0.1576389F;
            this.textBox169.Left = 3.898442F;
            this.textBox169.Name = "textBox169";
            this.textBox169.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: left; ddo-char-set: 1";
            this.textBox169.Tag = "";
            this.textBox169.Text = "ITEM028";
            this.textBox169.Top = 4.828566F;
            this.textBox169.Width = 1.479167F;
            // 
            // textBox170
            // 
            this.textBox170.DataField = "ITEM082";
            this.textBox170.Height = 0.1381944F;
            this.textBox170.Left = 3.273442F;
            this.textBox170.Name = "textBox170";
            this.textBox170.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.textBox170.Tag = "";
            this.textBox170.Text = "ITEM082";
            this.textBox170.Top = 7.005649F;
            this.textBox170.Width = 0.2131944F;
            // 
            // textBox171
            // 
            this.textBox171.DataField = "ITEM083";
            this.textBox171.Height = 0.1381944F;
            this.textBox171.Left = 3.486636F;
            this.textBox171.Name = "textBox171";
            this.textBox171.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.textBox171.Tag = "";
            this.textBox171.Text = "ITEM083";
            this.textBox171.Top = 7.005649F;
            this.textBox171.Width = 0.2131944F;
            // 
            // textBox172
            // 
            this.textBox172.DataField = "ITEM084";
            this.textBox172.Height = 0.1381944F;
            this.textBox172.Left = 3.699831F;
            this.textBox172.Name = "textBox172";
            this.textBox172.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.textBox172.Tag = "";
            this.textBox172.Text = "ITEM084";
            this.textBox172.Top = 7.005649F;
            this.textBox172.Width = 0.2131944F;
            // 
            // shape1
            // 
            this.shape1.Height = 0.1976378F;
            this.shape1.Left = 3.431496F;
            this.shape1.LineWeight = 3F;
            this.shape1.Name = "shape1";
            this.shape1.RoundingRadius = 9.999999F;
            this.shape1.Top = 0.4716536F;
            this.shape1.Width = 1.969685F;
            // 
            // shape2
            // 
            this.shape2.Height = 0.1976378F;
            this.shape2.Left = 3.432677F;
            this.shape2.LineWeight = 3F;
            this.shape2.Name = "shape2";
            this.shape2.RoundingRadius = 9.999999F;
            this.shape2.Top = 4.451712F;
            this.shape2.Width = 1.969685F;
            // 
            // textBox173
            // 
            this.textBox173.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox173.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox173.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox173.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox173.Height = 0.1576389F;
            this.textBox173.Left = 5.941339F;
            this.textBox173.Name = "textBox173";
            this.textBox173.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: left; ddo-char-set: 1";
            this.textBox173.Tag = "";
            this.textBox173.Text = null;
            this.textBox173.Top = 0.2845908F;
            this.textBox173.Width = 0.1576389F;
            // 
            // textBox174
            // 
            this.textBox174.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox174.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox174.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox174.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox174.Height = 0.1576389F;
            this.textBox174.Left = 6.098978F;
            this.textBox174.Name = "textBox174";
            this.textBox174.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: left; ddo-char-set: 1";
            this.textBox174.Tag = "";
            this.textBox174.Text = null;
            this.textBox174.Top = 0.2845908F;
            this.textBox174.Width = 0.3152778F;
            // 
            // textBox175
            // 
            this.textBox175.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox175.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox175.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox175.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox175.Height = 0.1576389F;
            this.textBox175.Left = 6.414256F;
            this.textBox175.Name = "textBox175";
            this.textBox175.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: left; ddo-char-set: 1";
            this.textBox175.Tag = "";
            this.textBox175.Text = null;
            this.textBox175.Top = 0.2845908F;
            this.textBox175.Width = 0.1576389F;
            // 
            // textBox176
            // 
            this.textBox176.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox176.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox176.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox176.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox176.Height = 0.1576389F;
            this.textBox176.Left = 6.571895F;
            this.textBox176.Name = "textBox176";
            this.textBox176.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: left; ddo-char-set: 1";
            this.textBox176.Tag = "";
            this.textBox176.Text = null;
            this.textBox176.Top = 0.2845908F;
            this.textBox176.Width = 0.1576389F;
            // 
            // textBox177
            // 
            this.textBox177.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox177.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox177.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox177.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox177.Height = 0.1576389F;
            this.textBox177.Left = 6.729534F;
            this.textBox177.Name = "textBox177";
            this.textBox177.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: left; ddo-char-set: 1";
            this.textBox177.Tag = "";
            this.textBox177.Text = null;
            this.textBox177.Top = 0.2845908F;
            this.textBox177.Width = 0.1576389F;
            // 
            // textBox178
            // 
            this.textBox178.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox178.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox178.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox178.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox178.Height = 0.1576389F;
            this.textBox178.Left = 6.887174F;
            this.textBox178.Name = "textBox178";
            this.textBox178.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: left; ddo-char-set: 1";
            this.textBox178.Tag = "";
            this.textBox178.Text = null;
            this.textBox178.Top = 0.2845908F;
            this.textBox178.Width = 0.1576389F;
            // 
            // textBox179
            // 
            this.textBox179.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox179.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox179.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox179.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox179.Height = 0.1576389F;
            this.textBox179.Left = 7.044811F;
            this.textBox179.Name = "textBox179";
            this.textBox179.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: left; ddo-char-set: 1";
            this.textBox179.Tag = "";
            this.textBox179.Text = null;
            this.textBox179.Top = 0.2845908F;
            this.textBox179.Width = 0.1576389F;
            // 
            // textBox180
            // 
            this.textBox180.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox180.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox180.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox180.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox180.Height = 0.1576389F;
            this.textBox180.Left = 7.202447F;
            this.textBox180.Name = "textBox180";
            this.textBox180.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: left; ddo-char-set: 1";
            this.textBox180.Tag = "";
            this.textBox180.Text = null;
            this.textBox180.Top = 0.2845908F;
            this.textBox180.Width = 0.1576389F;
            // 
            // textBox181
            // 
            this.textBox181.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox181.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox181.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox181.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox181.Height = 0.1576389F;
            this.textBox181.Left = 7.360086F;
            this.textBox181.Name = "textBox181";
            this.textBox181.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: left; ddo-char-set: 1";
            this.textBox181.Tag = "";
            this.textBox181.Text = null;
            this.textBox181.Top = 0.2845908F;
            this.textBox181.Width = 0.1576389F;
            // 
            // textBox182
            // 
            this.textBox182.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox182.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox182.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox182.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox182.Height = 0.1576389F;
            this.textBox182.Left = 7.517725F;
            this.textBox182.Name = "textBox182";
            this.textBox182.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: left; ddo-char-set: 1";
            this.textBox182.Tag = "";
            this.textBox182.Text = null;
            this.textBox182.Top = 0.2845908F;
            this.textBox182.Width = 0.1576389F;
            // 
            // textBox183
            // 
            this.textBox183.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox183.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox183.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox183.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox183.Height = 0.1576389F;
            this.textBox183.Left = 7.675371F;
            this.textBox183.Name = "textBox183";
            this.textBox183.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: left; ddo-char-set: 1";
            this.textBox183.Tag = "";
            this.textBox183.Text = null;
            this.textBox183.Top = 0.2845908F;
            this.textBox183.Width = 0.1576389F;
            // 
            // textBox184
            // 
            this.textBox184.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox184.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox184.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox184.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox184.Height = 0.1180556F;
            this.textBox184.Left = 5.941339F;
            this.textBox184.Name = "textBox184";
            this.textBox184.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: bold; text-align: left; ddo-char-set: 1";
            this.textBox184.Tag = "";
            this.textBox184.Text = "※";
            this.textBox184.Top = 0.1665355F;
            this.textBox184.Width = 2.677778F;
            // 
            // textBox190
            // 
            this.textBox190.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox190.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox190.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox190.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox190.Height = 0.1576389F;
            this.textBox190.Left = 8.619106F;
            this.textBox190.Name = "textBox190";
            this.textBox190.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: left; ddo-char-set: 1";
            this.textBox190.Tag = "";
            this.textBox190.Text = null;
            this.textBox190.Top = 0.2845908F;
            this.textBox190.Width = 0.7090278F;
            // 
            // textBox191
            // 
            this.textBox191.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox191.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox191.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox191.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox191.Height = 0.1180556F;
            this.textBox191.Left = 8.619106F;
            this.textBox191.Name = "textBox191";
            this.textBox191.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: bold; text-align: left; ddo-char-set: 1";
            this.textBox191.Tag = "";
            this.textBox191.Text = "※  種 別";
            this.textBox191.Top = 0.1665355F;
            this.textBox191.Width = 0.7090278F;
            // 
            // textBox192
            // 
            this.textBox192.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox192.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox192.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox192.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox192.Height = 0.1576389F;
            this.textBox192.Left = 9.328135F;
            this.textBox192.Name = "textBox192";
            this.textBox192.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: left; ddo-char-set: 1";
            this.textBox192.Tag = "";
            this.textBox192.Text = null;
            this.textBox192.Top = 0.2845908F;
            this.textBox192.Width = 0.8659722F;
            // 
            // textBox193
            // 
            this.textBox193.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox193.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox193.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox193.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox193.Height = 0.1180556F;
            this.textBox193.Left = 9.328135F;
            this.textBox193.Name = "textBox193";
            this.textBox193.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: bold; text-align: left; ddo-char-set: 1";
            this.textBox193.Tag = "";
            this.textBox193.Text = "※  整 理 番 号";
            this.textBox193.Top = 0.1665355F;
            this.textBox193.Width = 0.8659722F;
            // 
            // textBox194
            // 
            this.textBox194.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox194.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox194.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox194.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox194.Height = 0.1576389F;
            this.textBox194.Left = 10.19411F;
            this.textBox194.Name = "textBox194";
            this.textBox194.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: left; ddo-char-set: 1";
            this.textBox194.Tag = "";
            this.textBox194.Text = null;
            this.textBox194.Top = 0.2845908F;
            this.textBox194.Width = 0.8659722F;
            // 
            // textBox195
            // 
            this.textBox195.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox195.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox195.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox195.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox195.Height = 0.1180556F;
            this.textBox195.Left = 10.19411F;
            this.textBox195.Name = "textBox195";
            this.textBox195.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: bold; text-align: left; ddo-char-set: 1";
            this.textBox195.Tag = "";
            this.textBox195.Text = "※";
            this.textBox195.Top = 0.1665355F;
            this.textBox195.Width = 0.8659722F;
            // 
            // textBox196
            // 
            this.textBox196.DataField = "ITEM012";
            this.textBox196.Height = 0.7618055F;
            this.textBox196.Left = 5.765645F;
            this.textBox196.Name = "textBox196";
            this.textBox196.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.textBox196.Tag = "";
            this.textBox196.Text = "ITEM012";
            this.textBox196.Top = 0.1977854F;
            this.textBox196.Width = 0.1604167F;
            // 
            // textBox197
            // 
            this.textBox197.Height = 2.126389F;
            this.textBox197.Left = 5.724016F;
            this.textBox197.Name = "textBox197";
            this.textBox197.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: bold; text-align: left; ddo-char-set: 128";
            this.textBox197.Tag = "";
            this.textBox197.Text = "給与支払報告書\r\n　個人別明細書";
            this.textBox197.Top = 0.979134F;
            this.textBox197.Width = 0.1980316F;
            // 
            // label27
            // 
            this.label27.Height = 0.7174979F;
            this.label27.HyperLink = null;
            this.label27.Left = 5.776062F;
            this.label27.Name = "label27";
            this.label27.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: bold; text-align: " +
    "left; ddo-char-set: 1";
            this.label27.Tag = "";
            this.label27.Text = "市区町村提出用";
            this.label27.Top = 3.15612F;
            this.label27.Width = 0.1715278F;
            // 
            // textBox198
            // 
            this.textBox198.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox198.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox198.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox198.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox198.DataField = "ITEM045";
            this.textBox198.Height = 0.3541667F;
            this.textBox198.Left = 8.696899F;
            this.textBox198.Name = "textBox198";
            this.textBox198.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: right; vertical-align: bottom; ddo-char-set: 1";
            this.textBox198.Tag = "";
            this.textBox198.Text = "ITEM045";
            this.textBox198.Top = 1.976493F;
            this.textBox198.Width = 0.7090278F;
            // 
            // label28
            // 
            this.label28.Height = 0.08611111F;
            this.label28.HyperLink = null;
            this.label28.Left = 8.722586F;
            this.label28.Name = "label28";
            this.label28.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.label28.Tag = "";
            this.label28.Text = "内　　　　　　　円";
            this.label28.Top = 1.988299F;
            this.label28.Width = 0.6785923F;
            // 
            // textBox199
            // 
            this.textBox199.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox199.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox199.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox199.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox199.DataField = "ITEM068";
            this.textBox199.Height = 0.1576389F;
            this.textBox199.Left = 5.941339F;
            this.textBox199.Name = "textBox199";
            this.textBox199.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox199.Tag = "";
            this.textBox199.Text = "ITEM068";
            this.textBox199.Top = 3.315385F;
            this.textBox199.Width = 0.2131944F;
            // 
            // textBox200
            // 
            this.textBox200.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox200.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox200.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox200.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox200.DataField = "ITEM069";
            this.textBox200.Height = 0.1576389F;
            this.textBox200.Left = 6.154534F;
            this.textBox200.Name = "textBox200";
            this.textBox200.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox200.Tag = "";
            this.textBox200.Text = "ITEM069";
            this.textBox200.Top = 3.315385F;
            this.textBox200.Width = 0.2131944F;
            // 
            // textBox201
            // 
            this.textBox201.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox201.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox201.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox201.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox201.DataField = "ITEM070";
            this.textBox201.Height = 0.1576389F;
            this.textBox201.Left = 6.367728F;
            this.textBox201.Name = "textBox201";
            this.textBox201.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox201.Tag = "";
            this.textBox201.Text = "ITEM070";
            this.textBox201.Top = 3.315385F;
            this.textBox201.Width = 0.2131944F;
            // 
            // textBox202
            // 
            this.textBox202.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox202.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox202.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox202.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox202.DataField = "ITEM071";
            this.textBox202.Height = 0.1576389F;
            this.textBox202.Left = 6.580922F;
            this.textBox202.Name = "textBox202";
            this.textBox202.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox202.Tag = "";
            this.textBox202.Text = "ITEM071";
            this.textBox202.Top = 3.315385F;
            this.textBox202.Width = 0.2131944F;
            // 
            // textBox203
            // 
            this.textBox203.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox203.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox203.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox203.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox203.DataField = "ITEM072";
            this.textBox203.Height = 0.1576389F;
            this.textBox203.Left = 6.794118F;
            this.textBox203.Name = "textBox203";
            this.textBox203.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox203.Tag = "";
            this.textBox203.Text = "ITEM072";
            this.textBox203.Top = 3.315385F;
            this.textBox203.Width = 0.2131944F;
            // 
            // textBox204
            // 
            this.textBox204.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox204.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox204.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox204.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox204.DataField = "ITEM073";
            this.textBox204.Height = 0.1576389F;
            this.textBox204.Left = 7.007311F;
            this.textBox204.Name = "textBox204";
            this.textBox204.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox204.Tag = "";
            this.textBox204.Text = "ITEM073";
            this.textBox204.Top = 3.315385F;
            this.textBox204.Width = 0.2131944F;
            // 
            // textBox205
            // 
            this.textBox205.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox205.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox205.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox205.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox205.DataField = "ITEM074";
            this.textBox205.Height = 0.1576389F;
            this.textBox205.Left = 7.220502F;
            this.textBox205.Name = "textBox205";
            this.textBox205.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox205.Tag = "";
            this.textBox205.Text = "ITEM074";
            this.textBox205.Top = 3.315385F;
            this.textBox205.Width = 0.2131944F;
            // 
            // textBox206
            // 
            this.textBox206.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox206.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox206.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox206.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox206.DataField = "ITEM075";
            this.textBox206.Height = 0.1576389F;
            this.textBox206.Left = 7.433697F;
            this.textBox206.Name = "textBox206";
            this.textBox206.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox206.Tag = "";
            this.textBox206.Text = "ITEM075";
            this.textBox206.Top = 3.315385F;
            this.textBox206.Width = 0.2131944F;
            // 
            // textBox207
            // 
            this.textBox207.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox207.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox207.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox207.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox207.DataField = "ITEM076";
            this.textBox207.Height = 0.1576389F;
            this.textBox207.Left = 7.646891F;
            this.textBox207.Name = "textBox207";
            this.textBox207.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox207.Tag = "";
            this.textBox207.Text = "ITEM076";
            this.textBox207.Top = 3.315385F;
            this.textBox207.Width = 0.2131944F;
            // 
            // textBox208
            // 
            this.textBox208.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox208.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox208.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox208.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox208.DataField = "ITEM077";
            this.textBox208.Height = 0.1576389F;
            this.textBox208.Left = 7.860086F;
            this.textBox208.Name = "textBox208";
            this.textBox208.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox208.Tag = "";
            this.textBox208.Text = "ITEM077";
            this.textBox208.Top = 3.315385F;
            this.textBox208.Width = 0.2131944F;
            // 
            // textBox209
            // 
            this.textBox209.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox209.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox209.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox209.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox209.DataField = "ITEM078";
            this.textBox209.Height = 0.1576389F;
            this.textBox209.Left = 8.07328F;
            this.textBox209.Name = "textBox209";
            this.textBox209.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox209.Tag = "";
            this.textBox209.Text = "ITEM078";
            this.textBox209.Top = 3.315385F;
            this.textBox209.Width = 0.2131944F;
            // 
            // textBox210
            // 
            this.textBox210.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox210.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox210.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox210.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox210.DataField = "ITEM079";
            this.textBox210.Height = 0.1576389F;
            this.textBox210.Left = 8.286474F;
            this.textBox210.Name = "textBox210";
            this.textBox210.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox210.Tag = "";
            this.textBox210.Text = "ITEM079";
            this.textBox210.Top = 3.315385F;
            this.textBox210.Width = 0.2131944F;
            // 
            // textBox211
            // 
            this.textBox211.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox211.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox211.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox211.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox211.DataField = "ITEM081";
            this.textBox211.Height = 0.2756944F;
            this.textBox211.Left = 8.49967F;
            this.textBox211.Name = "textBox211";
            this.textBox211.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox211.Tag = "";
            this.textBox211.Text = "ITEM081";
            this.textBox211.Top = 3.197327F;
            this.textBox211.Width = 0.2131944F;
            // 
            // textBox212
            // 
            this.textBox212.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox212.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox212.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox212.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox212.DataField = "ITEM085";
            this.textBox212.Height = 0.2756944F;
            this.textBox212.Left = 8.712864F;
            this.textBox212.Name = "textBox212";
            this.textBox212.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox212.Tag = "";
            this.textBox212.Text = "ITEM085";
            this.textBox212.Top = 3.197327F;
            this.textBox212.Width = 0.2131944F;
            // 
            // textBox213
            // 
            this.textBox213.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox213.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox213.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox213.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox213.DataField = "ITEM086";
            this.textBox213.Height = 0.2756944F;
            this.textBox213.Left = 8.926058F;
            this.textBox213.Name = "textBox213";
            this.textBox213.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox213.Tag = "";
            this.textBox213.Text = "ITEM086";
            this.textBox213.Top = 3.197327F;
            this.textBox213.Width = 0.2131944F;
            // 
            // textBox214
            // 
            this.textBox214.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox214.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox214.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox214.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox214.DataField = "ITEM087";
            this.textBox214.Height = 0.2756944F;
            this.textBox214.Left = 9.139252F;
            this.textBox214.Name = "textBox214";
            this.textBox214.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox214.Tag = "";
            this.textBox214.Text = "ITEM087";
            this.textBox214.Top = 3.197327F;
            this.textBox214.Width = 0.2131944F;
            // 
            // textBox215
            // 
            this.textBox215.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox215.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox215.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox215.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox215.DataField = "ITEM088";
            this.textBox215.Height = 0.2756944F;
            this.textBox215.Left = 9.352448F;
            this.textBox215.Name = "textBox215";
            this.textBox215.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox215.Tag = "";
            this.textBox215.Text = "ITEM088";
            this.textBox215.Top = 3.197327F;
            this.textBox215.Width = 0.2131944F;
            // 
            // textBox216
            // 
            this.textBox216.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox216.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox216.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox216.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox216.Height = 0.1972222F;
            this.textBox216.Left = 9.091335F;
            this.textBox216.Name = "textBox216";
            this.textBox216.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.textBox216.Tag = "";
            this.textBox216.Text = "(受給者番号)";
            this.textBox216.Top = 0.4362155F;
            this.textBox216.Width = 1.96875F;
            // 
            // textBox217
            // 
            this.textBox217.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox217.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox217.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox217.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox217.Height = 0.3583333F;
            this.textBox217.Left = 9.091335F;
            this.textBox217.Name = "textBox217";
            this.textBox217.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.textBox217.Tag = "";
            this.textBox217.Text = "(役職名)";
            this.textBox217.Top = 0.8299657F;
            this.textBox217.Width = 1.96875F;
            // 
            // textBox219
            // 
            this.textBox219.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox219.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox219.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox219.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox219.Height = 0.1972222F;
            this.textBox219.Left = 9.091335F;
            this.textBox219.Name = "textBox219";
            this.textBox219.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.textBox219.Tag = "";
            this.textBox219.Text = "(フリガナ)";
            this.textBox219.Top = 0.6334378F;
            this.textBox219.Width = 1.96875F;
            // 
            // textBox220
            // 
            this.textBox220.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox220.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox220.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox220.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox220.DataField = "ITEM030";
            this.textBox220.Height = 0.3152778F;
            this.textBox220.Left = 6.807311F;
            this.textBox220.Name = "textBox220";
            this.textBox220.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; vertical-align: bottom; ddo-char-set: 1";
            this.textBox220.Tag = "";
            this.textBox220.Text = "ITEM030";
            this.textBox220.Top = 1.345938F;
            this.textBox220.Width = 1.063194F;
            // 
            // textBox221
            // 
            this.textBox221.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox221.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox221.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox221.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox221.Height = 0.1972222F;
            this.textBox221.Left = 7.122593F;
            this.textBox221.Name = "textBox221";
            this.textBox221.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox221.Tag = "";
            this.textBox221.Text = "控除対象扶養親族の数\r\n(配偶者を除く)";
            this.textBox221.Top = 1.661215F;
            this.textBox221.Width = 1.023611F;
            // 
            // textBox222
            // 
            this.textBox222.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox222.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox222.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox222.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox222.Height = 0.1180556F;
            this.textBox222.Left = 8.49967F;
            this.textBox222.Name = "textBox222";
            this.textBox222.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox222.Tag = "";
            this.textBox222.Text = "中 途 就・退 職";
            this.textBox222.Top = 2.961216F;
            this.textBox222.Width = 1.067361F;
            // 
            // textBox223
            // 
            this.textBox223.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox223.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox223.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox223.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox223.Height = 0.1180556F;
            this.textBox223.Left = 8.49967F;
            this.textBox223.Name = "textBox223";
            this.textBox223.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox223.Tag = "";
            this.textBox223.Text = "就職";
            this.textBox223.Top = 3.079271F;
            this.textBox223.Width = 0.2131944F;
            // 
            // textBox224
            // 
            this.textBox224.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox224.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox224.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox224.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox224.DataField = "ITEM096";
            this.textBox224.Height = 0.1972222F;
            this.textBox224.Left = 6.792033F;
            this.textBox224.Name = "textBox224";
            this.textBox224.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: left; ddo-char-set: 1";
            this.textBox224.Tag = "";
            this.textBox224.Text = "ITEM096";
            this.textBox224.Top = 3.473024F;
            this.textBox224.Width = 4.26875F;
            // 
            // textBox225
            // 
            this.textBox225.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox225.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox225.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox225.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox225.CanGrow = false;
            this.textBox225.DataField = "ITEM097";
            this.textBox225.Height = 0.1972222F;
            this.textBox225.Left = 6.792033F;
            this.textBox225.Name = "textBox225";
            this.textBox225.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: left; ddo-char-set: 1";
            this.textBox225.Tag = "";
            this.textBox225.Text = "ITEM097";
            this.textBox225.Top = 3.670246F;
            this.textBox225.Width = 4.26875F;
            // 
            // textBox229
            // 
            this.textBox229.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox229.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox229.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox229.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox229.DataField = "ITEM022";
            this.textBox229.Height = 0.1576389F;
            this.textBox229.Left = 6.453145F;
            this.textBox229.Name = "textBox229";
            this.textBox229.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: left; ddo-char-set: 1";
            this.textBox229.Tag = "";
            this.textBox229.Text = "ITEM022";
            this.textBox229.Top = 0.6376044F;
            this.textBox229.Width = 2.480556F;
            // 
            // textBox230
            // 
            this.textBox230.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox230.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox230.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox230.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox230.DataField = "ITEM023";
            this.textBox230.Height = 0.1972222F;
            this.textBox230.Left = 6.453145F;
            this.textBox230.Name = "textBox230";
            this.textBox230.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: left; ddo-char-set: 1";
            this.textBox230.Tag = "";
            this.textBox230.Text = "ITEM023";
            this.textBox230.Top = 0.7952434F;
            this.textBox230.Width = 2.480556F;
            // 
            // textBox231
            // 
            this.textBox231.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox231.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox231.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox231.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox231.DataField = "ITEM024";
            this.textBox231.Height = 0.1972222F;
            this.textBox231.Left = 6.453145F;
            this.textBox231.Name = "textBox231";
            this.textBox231.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: left; ddo-char-set: 1";
            this.textBox231.Tag = "";
            this.textBox231.Text = "ITEM024";
            this.textBox231.Top = 0.9924656F;
            this.textBox231.Width = 2.480556F;
            // 
            // textBox232
            // 
            this.textBox232.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox232.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox232.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox232.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox232.Height = 0.1576389F;
            this.textBox232.Left = 5.941339F;
            this.textBox232.Name = "textBox232";
            this.textBox232.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox232.Tag = "";
            this.textBox232.Text = "種　　　別";
            this.textBox232.Top = 1.188299F;
            this.textBox232.Width = 0.8659722F;
            // 
            // textBox233
            // 
            this.textBox233.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox233.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox233.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox233.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox233.DataField = "ITEM029";
            this.textBox233.Height = 0.3152778F;
            this.textBox233.Left = 5.941339F;
            this.textBox233.Name = "textBox233";
            this.textBox233.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: left; vertical-align: bottom; ddo-char-set: 1";
            this.textBox233.Tag = "";
            this.textBox233.Text = "ITEM029";
            this.textBox233.Top = 1.345938F;
            this.textBox233.Width = 0.8659722F;
            // 
            // textBox234
            // 
            this.textBox234.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox234.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox234.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox234.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox234.Height = 0.3153544F;
            this.textBox234.Left = 5.941339F;
            this.textBox234.Name = "textBox234";
            this.textBox234.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: left; vertical-align: middle; ddo-char-set: 1";
            this.textBox234.Tag = "";
            this.textBox234.Text = "控除対象配偶\r\n\r\n者の有無等";
            this.textBox234.Top = 1.661019F;
            this.textBox234.Width = 0.5909723F;
            // 
            // textBox235
            // 
            this.textBox235.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox235.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox235.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox235.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox235.Height = 0.1576389F;
            this.textBox235.Left = 5.941339F;
            this.textBox235.Name = "textBox235";
            this.textBox235.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox235.Tag = "";
            this.textBox235.Text = "C有";
            this.textBox235.Top = 1.976493F;
            this.textBox235.Width = 0.1180556F;
            // 
            // textBox236
            // 
            this.textBox236.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox236.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox236.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox236.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox236.Height = 0.1576389F;
            this.textBox236.Left = 6.059395F;
            this.textBox236.Name = "textBox236";
            this.textBox236.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox236.Tag = "";
            this.textBox236.Text = "D無";
            this.textBox236.Top = 1.976493F;
            this.textBox236.Width = 0.1180556F;
            // 
            // textBox237
            // 
            this.textBox237.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox237.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox237.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox237.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox237.Height = 0.1576389F;
            this.textBox237.Left = 6.17745F;
            this.textBox237.Name = "textBox237";
            this.textBox237.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox237.Tag = "";
            this.textBox237.Text = "従有";
            this.textBox237.Top = 1.976493F;
            this.textBox237.Width = 0.1180556F;
            // 
            // textBox238
            // 
            this.textBox238.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox238.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox238.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox238.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox238.Height = 0.1576389F;
            this.textBox238.Left = 6.295506F;
            this.textBox238.Name = "textBox238";
            this.textBox238.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox238.Tag = "";
            this.textBox238.Text = "従無";
            this.textBox238.Top = 1.976493F;
            this.textBox238.Width = 0.1180556F;
            // 
            // textBox239
            // 
            this.textBox239.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox239.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox239.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox239.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox239.CanGrow = false;
            this.textBox239.Height = 0.1576389F;
            this.textBox239.Left = 6.414256F;
            this.textBox239.Name = "textBox239";
            this.textBox239.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox239.Tag = "";
            this.textBox239.Text = "老人";
            this.textBox239.Top = 1.818854F;
            this.textBox239.Width = 0.1180556F;
            // 
            // textBox240
            // 
            this.textBox240.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox240.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox240.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox240.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox240.DataField = "ITEM034";
            this.textBox240.Height = 0.1972222F;
            this.textBox240.Left = 5.941339F;
            this.textBox240.Name = "textBox240";
            this.textBox240.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox240.Tag = "";
            this.textBox240.Text = "ITEM034";
            this.textBox240.Top = 2.134132F;
            this.textBox240.Width = 0.1180556F;
            // 
            // textBox241
            // 
            this.textBox241.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox241.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox241.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox241.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox241.DataField = "ITEM035";
            this.textBox241.Height = 0.1972222F;
            this.textBox241.Left = 6.059395F;
            this.textBox241.Name = "textBox241";
            this.textBox241.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox241.Tag = "";
            this.textBox241.Text = "ITEM035";
            this.textBox241.Top = 2.134132F;
            this.textBox241.Width = 0.1180556F;
            // 
            // textBox242
            // 
            this.textBox242.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox242.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox242.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox242.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox242.Height = 0.1972222F;
            this.textBox242.Left = 6.17745F;
            this.textBox242.Name = "textBox242";
            this.textBox242.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: bold; text-align: center; ddo-char-set: 1";
            this.textBox242.Tag = "";
            this.textBox242.Text = null;
            this.textBox242.Top = 2.134132F;
            this.textBox242.Width = 0.1180556F;
            // 
            // textBox243
            // 
            this.textBox243.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox243.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox243.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox243.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox243.Height = 0.1972222F;
            this.textBox243.Left = 6.295506F;
            this.textBox243.Name = "textBox243";
            this.textBox243.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: bold; text-align: center; ddo-char-set: 1";
            this.textBox243.Tag = "";
            this.textBox243.Text = null;
            this.textBox243.Top = 2.134132F;
            this.textBox243.Width = 0.1180556F;
            // 
            // textBox244
            // 
            this.textBox244.DataField = "ITEM036";
            this.textBox244.Height = 0.1972222F;
            this.textBox244.Left = 6.413561F;
            this.textBox244.Name = "textBox244";
            this.textBox244.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox244.Tag = "";
            this.textBox244.Text = "ITEM036";
            this.textBox244.Top = 2.134132F;
            this.textBox244.Width = 0.1180556F;
            // 
            // textBox245
            // 
            this.textBox245.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox245.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox245.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox245.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox245.Height = 0.3152778F;
            this.textBox245.Left = 6.532311F;
            this.textBox245.Name = "textBox245";
            this.textBox245.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox245.Tag = "";
            this.textBox245.Text = "配偶者特別控除の額";
            this.textBox245.Top = 1.661215F;
            this.textBox245.Width = 0.5909723F;
            // 
            // textBox246
            // 
            this.textBox246.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox246.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox246.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox246.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox246.DataField = "ITEM037";
            this.textBox246.Height = 0.3541667F;
            this.textBox246.Left = 6.532311F;
            this.textBox246.Name = "textBox246";
            this.textBox246.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox246.Tag = "";
            this.textBox246.Text = "ITEM037";
            this.textBox246.Top = 1.976493F;
            this.textBox246.Width = 0.5909723F;
            // 
            // textBox247
            // 
            this.textBox247.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox247.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox247.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox247.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox247.Height = 0.6298611F;
            this.textBox247.Left = 5.941339F;
            this.textBox247.Name = "textBox247";
            this.textBox247.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.textBox247.Tag = "";
            this.textBox247.Text = "(摘要)";
            this.textBox247.Top = 2.331354F;
            this.textBox247.Width = 5.11875F;
            // 
            // textBox248
            // 
            this.textBox248.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox248.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox248.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox248.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox248.CanGrow = false;
            this.textBox248.Height = 0.3541667F;
            this.textBox248.Left = 5.941339F;
            this.textBox248.Name = "textBox248";
            this.textBox248.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5.5pt; font" +
    "-weight: normal; text-align: center; ddo-char-set: 1";
            this.textBox248.Tag = "";
            this.textBox248.Text = "扶16養歳親未族満";
            this.textBox248.Top = 2.961216F;
            this.textBox248.Width = 0.2131944F;
            // 
            // textBox249
            // 
            this.textBox249.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox249.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox249.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox249.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox249.Height = 0.3541667F;
            this.textBox249.Left = 6.154534F;
            this.textBox249.Name = "textBox249";
            this.textBox249.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.textBox249.Tag = "";
            this.textBox249.Text = "未\r\n成\r\n年\r\n";
            this.textBox249.Top = 2.961216F;
            this.textBox249.Width = 0.2131944F;
            // 
            // textBox250
            // 
            this.textBox250.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox250.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox250.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox250.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox250.Height = 0.3541667F;
            this.textBox250.Left = 6.367728F;
            this.textBox250.Name = "textBox250";
            this.textBox250.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox250.Tag = "";
            this.textBox250.Text = "外\r\n国\r\n人";
            this.textBox250.Top = 2.961216F;
            this.textBox250.Width = 0.2131944F;
            // 
            // textBox251
            // 
            this.textBox251.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox251.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox251.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox251.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox251.Height = 0.3541667F;
            this.textBox251.Left = 6.580922F;
            this.textBox251.Name = "textBox251";
            this.textBox251.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox251.Tag = "";
            this.textBox251.Text = "死\r\n亡\r\n退\r\n";
            this.textBox251.Top = 2.961216F;
            this.textBox251.Width = 0.2131944F;
            // 
            // textBox252
            // 
            this.textBox252.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox252.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox252.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox252.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox252.Height = 0.3541667F;
            this.textBox252.Left = 6.794118F;
            this.textBox252.Name = "textBox252";
            this.textBox252.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox252.Tag = "";
            this.textBox252.Text = "災\r\n害\r\n者";
            this.textBox252.Top = 2.961216F;
            this.textBox252.Width = 0.2131944F;
            // 
            // textBox253
            // 
            this.textBox253.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox253.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox253.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox253.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox253.Height = 0.3541667F;
            this.textBox253.Left = 7.007311F;
            this.textBox253.Name = "textBox253";
            this.textBox253.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox253.Tag = "";
            this.textBox253.Text = "乙\r\n\r\n欄";
            this.textBox253.Top = 2.961216F;
            this.textBox253.Width = 0.2131944F;
            // 
            // textBox254
            // 
            this.textBox254.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox254.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox254.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox254.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox254.Height = 0.1180556F;
            this.textBox254.Left = 7.220502F;
            this.textBox254.Name = "textBox254";
            this.textBox254.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 4pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox254.Tag = "";
            this.textBox254.Text = "本人が障害者";
            this.textBox254.Top = 2.961216F;
            this.textBox254.Width = 0.4256943F;
            // 
            // textBox255
            // 
            this.textBox255.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox255.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox255.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox255.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox255.Height = 0.2361111F;
            this.textBox255.Left = 7.220502F;
            this.textBox255.Name = "textBox255";
            this.textBox255.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.textBox255.Tag = "";
            this.textBox255.Text = "特\r\n別";
            this.textBox255.Top = 3.079271F;
            this.textBox255.Width = 0.2131944F;
            // 
            // textBox256
            // 
            this.textBox256.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox256.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox256.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox256.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox256.CanGrow = false;
            this.textBox256.Height = 0.2361111F;
            this.textBox256.Left = 7.433697F;
            this.textBox256.Name = "textBox256";
            this.textBox256.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.textBox256.Tag = "";
            this.textBox256.Text = "そ\r\nの\r\n他";
            this.textBox256.Top = 3.079271F;
            this.textBox256.Width = 0.2131944F;
            // 
            // textBox257
            // 
            this.textBox257.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox257.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox257.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox257.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox257.Height = 0.1180556F;
            this.textBox257.Left = 7.646204F;
            this.textBox257.Name = "textBox257";
            this.textBox257.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox257.Tag = "";
            this.textBox257.Text = "寡　婦";
            this.textBox257.Top = 2.961216F;
            this.textBox257.Width = 0.4256943F;
            // 
            // textBox258
            // 
            this.textBox258.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox258.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox258.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox258.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox258.Height = 0.2361111F;
            this.textBox258.Left = 7.646891F;
            this.textBox258.Name = "textBox258";
            this.textBox258.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.textBox258.Tag = "";
            this.textBox258.Text = "一\r\n般";
            this.textBox258.Top = 3.079271F;
            this.textBox258.Width = 0.2131944F;
            // 
            // textBox259
            // 
            this.textBox259.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox259.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox259.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox259.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox259.Height = 0.2361111F;
            this.textBox259.Left = 7.860086F;
            this.textBox259.Name = "textBox259";
            this.textBox259.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.textBox259.Tag = "";
            this.textBox259.Text = "特\r\n別";
            this.textBox259.Top = 3.079271F;
            this.textBox259.Width = 0.2131944F;
            // 
            // textBox260
            // 
            this.textBox260.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox260.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox260.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox260.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox260.Height = 0.3541667F;
            this.textBox260.Left = 8.073233F;
            this.textBox260.Name = "textBox260";
            this.textBox260.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.textBox260.Tag = "";
            this.textBox260.Text = "寡　夫";
            this.textBox260.Top = 2.961019F;
            this.textBox260.Width = 0.2131944F;
            // 
            // textBox261
            // 
            this.textBox261.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox261.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox261.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox261.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox261.CanGrow = false;
            this.textBox261.Height = 0.3541667F;
            this.textBox261.Left = 8.286419F;
            this.textBox261.Name = "textBox261";
            this.textBox261.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1; ddo-font-vertical: true";
            this.textBox261.Tag = "";
            this.textBox261.Text = "勤\r\n労\r\n学\r\n生";
            this.textBox261.Top = 2.961019F;
            this.textBox261.Width = 0.2131944F;
            // 
            // textBox262
            // 
            this.textBox262.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox262.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox262.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox262.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox262.Height = 0.1180556F;
            this.textBox262.Left = 8.712864F;
            this.textBox262.Name = "textBox262";
            this.textBox262.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox262.Tag = "";
            this.textBox262.Text = "退職";
            this.textBox262.Top = 3.079271F;
            this.textBox262.Width = 0.2131944F;
            // 
            // textBox263
            // 
            this.textBox263.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox263.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox263.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox263.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox263.Height = 0.1180556F;
            this.textBox263.Left = 8.926058F;
            this.textBox263.Name = "textBox263";
            this.textBox263.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox263.Tag = "";
            this.textBox263.Text = "年";
            this.textBox263.Top = 3.079271F;
            this.textBox263.Width = 0.2131944F;
            // 
            // textBox264
            // 
            this.textBox264.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox264.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox264.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox264.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox264.Height = 0.1180556F;
            this.textBox264.Left = 9.139252F;
            this.textBox264.Name = "textBox264";
            this.textBox264.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox264.Tag = "";
            this.textBox264.Text = "月";
            this.textBox264.Top = 3.079271F;
            this.textBox264.Width = 0.2131944F;
            // 
            // textBox265
            // 
            this.textBox265.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox265.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox265.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox265.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox265.Height = 0.1180556F;
            this.textBox265.Left = 9.352448F;
            this.textBox265.Name = "textBox265";
            this.textBox265.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox265.Tag = "";
            this.textBox265.Text = "日";
            this.textBox265.Top = 3.079271F;
            this.textBox265.Width = 0.2131944F;
            // 
            // textBox266
            // 
            this.textBox266.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox266.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox266.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox266.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox266.DataField = "ITEM089";
            this.textBox266.Height = 0.2756944F;
            this.textBox266.Left = 9.565641F;
            this.textBox266.Name = "textBox266";
            this.textBox266.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox266.Tag = "";
            this.textBox266.Text = "ITEM089";
            this.textBox266.Top = 3.197327F;
            this.textBox266.Width = 0.2131944F;
            // 
            // textBox267
            // 
            this.textBox267.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox267.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox267.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox267.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox267.DataField = "ITEM090";
            this.textBox267.Height = 0.2756944F;
            this.textBox267.Left = 9.778835F;
            this.textBox267.Name = "textBox267";
            this.textBox267.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox267.Tag = "";
            this.textBox267.Text = "ITEM090";
            this.textBox267.Top = 3.197327F;
            this.textBox267.Width = 0.2131944F;
            // 
            // textBox268
            // 
            this.textBox268.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox268.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox268.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox268.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox268.DataField = "ITEM091";
            this.textBox268.Height = 0.2756944F;
            this.textBox268.Left = 9.992031F;
            this.textBox268.Name = "textBox268";
            this.textBox268.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox268.Tag = "";
            this.textBox268.Text = "ITEM091";
            this.textBox268.Top = 3.197327F;
            this.textBox268.Width = 0.2131944F;
            // 
            // textBox269
            // 
            this.textBox269.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox269.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox269.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox269.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox269.DataField = "ITEM092";
            this.textBox269.Height = 0.2756944F;
            this.textBox269.Left = 10.20522F;
            this.textBox269.Name = "textBox269";
            this.textBox269.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox269.Tag = "";
            this.textBox269.Text = "ITEM092";
            this.textBox269.Top = 3.197327F;
            this.textBox269.Width = 0.2131944F;
            // 
            // textBox270
            // 
            this.textBox270.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox270.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox270.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox270.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox270.DataField = "ITEM093";
            this.textBox270.Height = 0.2756944F;
            this.textBox270.Left = 10.41842F;
            this.textBox270.Name = "textBox270";
            this.textBox270.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox270.Tag = "";
            this.textBox270.Text = "ITEM093";
            this.textBox270.Top = 3.197327F;
            this.textBox270.Width = 0.2131944F;
            // 
            // textBox271
            // 
            this.textBox271.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox271.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox271.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox271.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox271.Height = 0.1180556F;
            this.textBox271.Left = 9.565641F;
            this.textBox271.Name = "textBox271";
            this.textBox271.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox271.Tag = "";
            this.textBox271.Text = "受　給　者　生　年　月　日";
            this.textBox271.Top = 2.961216F;
            this.textBox271.Width = 1.493056F;
            // 
            // textBox272
            // 
            this.textBox272.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox272.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox272.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox272.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox272.Height = 0.1180556F;
            this.textBox272.Left = 9.565641F;
            this.textBox272.Name = "textBox272";
            this.textBox272.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox272.Tag = "";
            this.textBox272.Text = "明";
            this.textBox272.Top = 3.079271F;
            this.textBox272.Width = 0.2131944F;
            // 
            // textBox273
            // 
            this.textBox273.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox273.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox273.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox273.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox273.Height = 0.1180556F;
            this.textBox273.Left = 9.778835F;
            this.textBox273.Name = "textBox273";
            this.textBox273.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox273.Tag = "";
            this.textBox273.Text = "大";
            this.textBox273.Top = 3.079271F;
            this.textBox273.Width = 0.2131944F;
            // 
            // textBox274
            // 
            this.textBox274.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox274.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox274.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox274.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox274.Height = 0.1180556F;
            this.textBox274.Left = 9.992031F;
            this.textBox274.Name = "textBox274";
            this.textBox274.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox274.Tag = "";
            this.textBox274.Text = "昭";
            this.textBox274.Top = 3.079271F;
            this.textBox274.Width = 0.2131944F;
            // 
            // textBox275
            // 
            this.textBox275.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox275.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox275.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox275.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox275.Height = 0.1180556F;
            this.textBox275.Left = 10.20522F;
            this.textBox275.Name = "textBox275";
            this.textBox275.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox275.Tag = "";
            this.textBox275.Text = "平";
            this.textBox275.Top = 3.079271F;
            this.textBox275.Width = 0.2131944F;
            // 
            // textBox276
            // 
            this.textBox276.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox276.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox276.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox276.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox276.Height = 0.1180556F;
            this.textBox276.Left = 10.41842F;
            this.textBox276.Name = "textBox276";
            this.textBox276.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox276.Tag = "";
            this.textBox276.Text = "年";
            this.textBox276.Top = 3.079271F;
            this.textBox276.Width = 0.2131944F;
            // 
            // textBox277
            // 
            this.textBox277.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox277.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox277.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox277.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox277.DataField = "ITEM094";
            this.textBox277.Height = 0.2756944F;
            this.textBox277.Left = 10.63161F;
            this.textBox277.Name = "textBox277";
            this.textBox277.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox277.Tag = "";
            this.textBox277.Text = "ITEM094";
            this.textBox277.Top = 3.197327F;
            this.textBox277.Width = 0.2131944F;
            // 
            // textBox278
            // 
            this.textBox278.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox278.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox278.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox278.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox278.Height = 0.1180556F;
            this.textBox278.Left = 10.63161F;
            this.textBox278.Name = "textBox278";
            this.textBox278.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox278.Tag = "";
            this.textBox278.Text = "月";
            this.textBox278.Top = 3.079271F;
            this.textBox278.Width = 0.2131944F;
            // 
            // textBox279
            // 
            this.textBox279.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox279.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox279.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox279.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox279.DataField = "ITEM095";
            this.textBox279.Height = 0.2756944F;
            this.textBox279.Left = 10.84481F;
            this.textBox279.Name = "textBox279";
            this.textBox279.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox279.Tag = "";
            this.textBox279.Text = "ITEM095";
            this.textBox279.Top = 3.197327F;
            this.textBox279.Width = 0.2138889F;
            // 
            // textBox280
            // 
            this.textBox280.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox280.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox280.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox280.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox280.Height = 0.1180556F;
            this.textBox280.Left = 10.84481F;
            this.textBox280.Name = "textBox280";
            this.textBox280.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox280.Tag = "";
            this.textBox280.Text = "日";
            this.textBox280.Top = 3.079271F;
            this.textBox280.Width = 0.2138889F;
            // 
            // textBox281
            // 
            this.textBox281.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox281.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox281.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox281.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox281.Height = 0.1576389F;
            this.textBox281.Left = 6.807311F;
            this.textBox281.Name = "textBox281";
            this.textBox281.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox281.Tag = "";
            this.textBox281.Text = "支　払　金　額";
            this.textBox281.Top = 1.188299F;
            this.textBox281.Width = 1.063194F;
            // 
            // textBox282
            // 
            this.textBox282.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox282.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox282.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox282.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox282.DataField = "ITEM031";
            this.textBox282.Height = 0.3152778F;
            this.textBox282.Left = 7.870502F;
            this.textBox282.Name = "textBox282";
            this.textBox282.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; vertical-align: bottom; ddo-char-set: 1";
            this.textBox282.Tag = "";
            this.textBox282.Text = "ITEM031";
            this.textBox282.Top = 1.345938F;
            this.textBox282.Width = 1.063194F;
            // 
            // textBox283
            // 
            this.textBox283.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox283.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox283.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox283.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox283.Height = 0.1576389F;
            this.textBox283.Left = 7.870502F;
            this.textBox283.Name = "textBox283";
            this.textBox283.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox283.Tag = "";
            this.textBox283.Text = "給与所得控除後の金額";
            this.textBox283.Top = 1.188299F;
            this.textBox283.Width = 1.063194F;
            // 
            // textBox284
            // 
            this.textBox284.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox284.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox284.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox284.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox284.DataField = "ITEM032";
            this.textBox284.Height = 0.3152778F;
            this.textBox284.Left = 8.933697F;
            this.textBox284.Name = "textBox284";
            this.textBox284.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; vertical-align: bottom; ddo-char-set: 1";
            this.textBox284.Tag = "";
            this.textBox284.Text = "ITEM032";
            this.textBox284.Top = 1.345938F;
            this.textBox284.Width = 1.063194F;
            // 
            // textBox285
            // 
            this.textBox285.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox285.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox285.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox285.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox285.Height = 0.1576389F;
            this.textBox285.Left = 8.933697F;
            this.textBox285.Name = "textBox285";
            this.textBox285.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox285.Tag = "";
            this.textBox285.Text = "所得控除の額の合計額";
            this.textBox285.Top = 1.188299F;
            this.textBox285.Width = 1.063194F;
            // 
            // textBox286
            // 
            this.textBox286.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox286.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox286.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox286.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox286.DataField = "ITEM033";
            this.textBox286.Height = 0.3152778F;
            this.textBox286.Left = 9.996891F;
            this.textBox286.Name = "textBox286";
            this.textBox286.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; vertical-align: bottom; ddo-char-set: 1";
            this.textBox286.Tag = "";
            this.textBox286.Text = "ITEM033";
            this.textBox286.Top = 1.345938F;
            this.textBox286.Width = 1.063194F;
            // 
            // textBox287
            // 
            this.textBox287.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox287.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox287.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox287.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox287.Height = 0.1576389F;
            this.textBox287.Left = 9.996891F;
            this.textBox287.Name = "textBox287";
            this.textBox287.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox287.Tag = "";
            this.textBox287.Text = "源泉徴収税額";
            this.textBox287.Top = 1.188299F;
            this.textBox287.Width = 1.063194F;
            // 
            // textBox288
            // 
            this.textBox288.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox288.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox288.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox288.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox288.Height = 0.7519685F;
            this.textBox288.Left = 8.933859F;
            this.textBox288.Name = "textBox288";
            this.textBox288.Style = "background-color: White; font-family: ＭＳ 明朝; font-size: 8pt; font-weight: normal;" +
    " text-align: center; ddo-char-set: 1";
            this.textBox288.Tag = "";
            this.textBox288.Text = " 氏　\r\n\r\n\r\n名";
            this.textBox288.Top = 0.4362205F;
            this.textBox288.Width = 0.1576389F;
            // 
            // textBox289
            // 
            this.textBox289.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox289.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox289.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox289.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox289.Height = 0.3152778F;
            this.textBox289.Left = 10.46911F;
            this.textBox289.Name = "textBox289";
            this.textBox289.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox289.Tag = "";
            this.textBox289.Text = "住宅借入金等特別控除の額";
            this.textBox289.Top = 1.661215F;
            this.textBox289.Width = 0.5909723F;
            // 
            // textBox290
            // 
            this.textBox290.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox290.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox290.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox290.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox290.DataField = "ITEM049";
            this.textBox290.Height = 0.3541667F;
            this.textBox290.Left = 10.46911F;
            this.textBox290.Name = "textBox290";
            this.textBox290.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: right; vertical-align: bottom; ddo-char-set: 1";
            this.textBox290.Tag = "";
            this.textBox290.Text = "ITEM049";
            this.textBox290.Top = 1.976493F;
            this.textBox290.Width = 0.5909723F;
            // 
            // textBox291
            // 
            this.textBox291.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox291.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox291.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox291.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox291.Height = 0.3152778F;
            this.textBox291.Left = 8.696899F;
            this.textBox291.Name = "textBox291";
            this.textBox291.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox291.Tag = "";
            this.textBox291.Text = "社会保険料等 の 金 額";
            this.textBox291.Top = 1.661215F;
            this.textBox291.Width = 0.7090278F;
            // 
            // textBox292
            // 
            this.textBox292.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox292.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox292.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox292.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox292.Height = 0.3152778F;
            this.textBox292.Left = 9.996891F;
            this.textBox292.Name = "textBox292";
            this.textBox292.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox292.Tag = "";
            this.textBox292.Text = "地震保険料の控除額";
            this.textBox292.Top = 1.661215F;
            this.textBox292.Width = 0.4722222F;
            // 
            // textBox293
            // 
            this.textBox293.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox293.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox293.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox293.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox293.DataField = "ITEM048";
            this.textBox293.Height = 0.3541667F;
            this.textBox293.Left = 9.996891F;
            this.textBox293.Name = "textBox293";
            this.textBox293.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: right; vertical-align: bottom; ddo-char-set: 1";
            this.textBox293.Tag = "";
            this.textBox293.Text = "ITEM048";
            this.textBox293.Top = 1.976493F;
            this.textBox293.Width = 0.4722222F;
            // 
            // textBox294
            // 
            this.textBox294.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox294.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox294.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox294.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox294.Height = 0.3152778F;
            this.textBox294.Left = 9.40592F;
            this.textBox294.Name = "textBox294";
            this.textBox294.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox294.Tag = "";
            this.textBox294.Text = "生命保険料の 控 除 額";
            this.textBox294.Top = 1.661215F;
            this.textBox294.Width = 0.5909723F;
            // 
            // textBox295
            // 
            this.textBox295.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox295.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox295.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox295.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox295.DataField = "ITEM047";
            this.textBox295.Height = 0.3541667F;
            this.textBox295.Left = 9.40592F;
            this.textBox295.Name = "textBox295";
            this.textBox295.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: right; vertical-align: bottom; ddo-char-set: 1";
            this.textBox295.Tag = "";
            this.textBox295.Text = "ITEM047";
            this.textBox295.Top = 1.976493F;
            this.textBox295.Width = 0.5909723F;
            // 
            // textBox296
            // 
            this.textBox296.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox296.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox296.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox296.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox296.Height = 0.1972222F;
            this.textBox296.Left = 8.146196F;
            this.textBox296.Name = "textBox296";
            this.textBox296.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox296.Tag = "";
            this.textBox296.Text = "障害者の数(本人を除く)";
            this.textBox296.Top = 1.661215F;
            this.textBox296.Width = 0.5513888F;
            // 
            // textBox297
            // 
            this.textBox297.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox297.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox297.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox297.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox297.Height = 0.1180556F;
            this.textBox297.Left = 7.122593F;
            this.textBox297.Name = "textBox297";
            this.textBox297.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox297.Tag = "";
            this.textBox297.Text = "特定";
            this.textBox297.Top = 1.858438F;
            this.textBox297.Width = 0.2756944F;
            // 
            // textBox298
            // 
            this.textBox298.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox298.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox298.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox298.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox298.DataField = "ITEM038";
            this.textBox298.Height = 0.3541667F;
            this.textBox298.Left = 7.12328F;
            this.textBox298.Name = "textBox298";
            this.textBox298.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox298.Tag = "";
            this.textBox298.Text = "ITEM038";
            this.textBox298.Top = 1.976493F;
            this.textBox298.Width = 0.1377443F;
            // 
            // textBox299
            // 
            this.textBox299.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox299.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox299.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox299.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox299.Height = 0.3541667F;
            this.textBox299.Left = 7.257874F;
            this.textBox299.Name = "textBox299";
            this.textBox299.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: bold; tex" +
    "t-align: center; ddo-char-set: 1";
            this.textBox299.Tag = "";
            this.textBox299.Text = null;
            this.textBox299.Top = 1.976378F;
            this.textBox299.Width = 0.1338585F;
            // 
            // textBox300
            // 
            this.textBox300.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox300.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox300.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox300.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox300.Height = 0.1180556F;
            this.textBox300.Left = 7.39828F;
            this.textBox300.Name = "textBox300";
            this.textBox300.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox300.Tag = "";
            this.textBox300.Text = "老　人";
            this.textBox300.Top = 1.858438F;
            this.textBox300.Width = 0.4333332F;
            // 
            // textBox301
            // 
            this.textBox301.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox301.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox301.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox301.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox301.DataField = "ITEM040";
            this.textBox301.Height = 0.3541667F;
            this.textBox301.Left = 7.537402F;
            this.textBox301.Name = "textBox301";
            this.textBox301.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox301.Tag = "";
            this.textBox301.Text = "ITEM040";
            this.textBox301.Top = 1.976378F;
            this.textBox301.Width = 0.1496061F;
            // 
            // textBox302
            // 
            this.textBox302.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox302.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox302.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox302.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox302.Height = 0.3541667F;
            this.textBox302.Left = 7.691339F;
            this.textBox302.Name = "textBox302";
            this.textBox302.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: bold; tex" +
    "t-align: center; ddo-char-set: 1";
            this.textBox302.Tag = "";
            this.textBox302.Text = null;
            this.textBox302.Top = 1.976378F;
            this.textBox302.Width = 0.133858F;
            // 
            // textBox303
            // 
            this.textBox303.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox303.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox303.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox303.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox303.DataField = "ITEM039";
            this.textBox303.Height = 0.3541667F;
            this.textBox303.Left = 7.39828F;
            this.textBox303.Name = "textBox303";
            this.textBox303.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox303.Tag = "";
            this.textBox303.Text = "ITEM039";
            this.textBox303.Top = 1.976493F;
            this.textBox303.Width = 0.134004F;
            // 
            // textBox304
            // 
            this.textBox304.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox304.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox304.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox304.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox304.Height = 0.1180556F;
            this.textBox304.Left = 7.831614F;
            this.textBox304.Name = "textBox304";
            this.textBox304.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox304.Tag = "";
            this.textBox304.Text = "その他";
            this.textBox304.Top = 1.858438F;
            this.textBox304.Width = 0.3152778F;
            // 
            // textBox305
            // 
            this.textBox305.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox305.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox305.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox305.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox305.DataField = "ITEM041";
            this.textBox305.Height = 0.3541667F;
            this.textBox305.Left = 7.831614F;
            this.textBox305.Name = "textBox305";
            this.textBox305.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox305.Tag = "";
            this.textBox305.Text = "ITEM041";
            this.textBox305.Top = 1.976493F;
            this.textBox305.Width = 0.1576389F;
            // 
            // textBox306
            // 
            this.textBox306.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox306.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox306.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox306.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox306.Height = 0.3541667F;
            this.textBox306.Left = 7.989253F;
            this.textBox306.Name = "textBox306";
            this.textBox306.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: bold; tex" +
    "t-align: center; ddo-char-set: 1";
            this.textBox306.Tag = "";
            this.textBox306.Text = null;
            this.textBox306.Top = 1.976493F;
            this.textBox306.Width = 0.1576389F;
            // 
            // textBox307
            // 
            this.textBox307.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox307.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox307.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox307.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox307.Height = 0.1180556F;
            this.textBox307.Left = 8.146891F;
            this.textBox307.Name = "textBox307";
            this.textBox307.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox307.Tag = "";
            this.textBox307.Text = "特別";
            this.textBox307.Top = 1.858438F;
            this.textBox307.Width = 0.3152778F;
            // 
            // textBox308
            // 
            this.textBox308.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox308.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox308.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox308.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox308.DataField = "ITEM042";
            this.textBox308.Height = 0.3541667F;
            this.textBox308.Left = 8.146891F;
            this.textBox308.Name = "textBox308";
            this.textBox308.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox308.Tag = "";
            this.textBox308.Text = "ITEM042";
            this.textBox308.Top = 1.976493F;
            this.textBox308.Width = 0.1576389F;
            // 
            // textBox309
            // 
            this.textBox309.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox309.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox309.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox309.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox309.DataField = "ITEM043";
            this.textBox309.Height = 0.3541667F;
            this.textBox309.Left = 8.304531F;
            this.textBox309.Name = "textBox309";
            this.textBox309.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox309.Tag = "";
            this.textBox309.Text = "ITEM043";
            this.textBox309.Top = 1.976493F;
            this.textBox309.Width = 0.1576389F;
            // 
            // textBox310
            // 
            this.textBox310.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox310.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox310.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox310.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox310.DataField = "ITEM044";
            this.textBox310.Height = 0.3541667F;
            this.textBox310.Left = 8.46217F;
            this.textBox310.Name = "textBox310";
            this.textBox310.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox310.Tag = "";
            this.textBox310.Text = "ITEM044";
            this.textBox310.Top = 1.976493F;
            this.textBox310.Width = 0.2361111F;
            // 
            // textBox311
            // 
            this.textBox311.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox311.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox311.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox311.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox311.Height = 0.1180556F;
            this.textBox311.Left = 8.462204F;
            this.textBox311.Name = "textBox311";
            this.textBox311.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 4.5pt; font" +
    "-weight: normal; text-align: center; ddo-char-set: 1";
            this.textBox311.Tag = "";
            this.textBox311.Text = "その他";
            this.textBox311.Top = 1.858263F;
            this.textBox311.Width = 0.2322833F;
            // 
            // textBox312
            // 
            this.textBox312.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox312.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox312.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox312.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox312.CanGrow = false;
            this.textBox312.DataField = "ITEM064";
            this.textBox312.Height = 0.1576389F;
            this.textBox312.Left = 10.35106F;
            this.textBox312.Name = "textBox312";
            this.textBox312.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; ddo-char-set: 1";
            this.textBox312.Tag = "";
            this.textBox312.Text = "ITEM064";
            this.textBox312.Top = 2.330659F;
            this.textBox312.Width = 0.7090278F;
            // 
            // textBox313
            // 
            this.textBox313.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox313.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox313.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox313.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox313.Height = 0.1576389F;
            this.textBox313.Left = 9.523975F;
            this.textBox313.Name = "textBox313";
            this.textBox313.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox313.Tag = "";
            this.textBox313.Text = "介護医療保険料の金額";
            this.textBox313.Top = 2.330659F;
            this.textBox313.Width = 0.8270833F;
            // 
            // textBox314
            // 
            this.textBox314.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox314.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox314.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox314.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox314.CanGrow = false;
            this.textBox314.DataField = "ITEM065";
            this.textBox314.Height = 0.1576389F;
            this.textBox314.Left = 10.35106F;
            this.textBox314.Name = "textBox314";
            this.textBox314.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; ddo-char-set: 1";
            this.textBox314.Tag = "";
            this.textBox314.Text = "ITEM065";
            this.textBox314.Top = 2.488299F;
            this.textBox314.Width = 0.7090278F;
            // 
            // textBox315
            // 
            this.textBox315.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox315.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox315.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox315.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox315.Height = 0.1576389F;
            this.textBox315.Left = 9.523975F;
            this.textBox315.Name = "textBox315";
            this.textBox315.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox315.Tag = "";
            this.textBox315.Text = "新個人年金保険料の金額";
            this.textBox315.Top = 2.488299F;
            this.textBox315.Width = 0.8270833F;
            // 
            // textBox316
            // 
            this.textBox316.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox316.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox316.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox316.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox316.CanGrow = false;
            this.textBox316.DataField = "ITEM061";
            this.textBox316.Height = 0.1576389F;
            this.textBox316.Left = 8.814947F;
            this.textBox316.Name = "textBox316";
            this.textBox316.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; ddo-char-set: 1";
            this.textBox316.Tag = "";
            this.textBox316.Text = "ITEM061";
            this.textBox316.Top = 2.488299F;
            this.textBox316.Width = 0.7090278F;
            // 
            // textBox317
            // 
            this.textBox317.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox317.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox317.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox317.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox317.Height = 0.1576389F;
            this.textBox317.Left = 8.076754F;
            this.textBox317.Name = "textBox317";
            this.textBox317.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox317.Tag = "";
            this.textBox317.Text = "配偶者の合計所得";
            this.textBox317.Top = 2.488299F;
            this.textBox317.Width = 0.7375F;
            // 
            // textBox318
            // 
            this.textBox318.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox318.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox318.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox318.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox318.CanGrow = false;
            this.textBox318.DataField = "ITEM066";
            this.textBox318.Height = 0.1576389F;
            this.textBox318.Left = 10.35106F;
            this.textBox318.Name = "textBox318";
            this.textBox318.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; ddo-char-set: 1";
            this.textBox318.Tag = "";
            this.textBox318.Text = "ITEM066";
            this.textBox318.Top = 2.645938F;
            this.textBox318.Width = 0.7090278F;
            // 
            // textBox319
            // 
            this.textBox319.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox319.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox319.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox319.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox319.Height = 0.1576389F;
            this.textBox319.Left = 9.523975F;
            this.textBox319.Name = "textBox319";
            this.textBox319.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox319.Tag = "";
            this.textBox319.Text = "旧個人年金保険料の金額";
            this.textBox319.Top = 2.645938F;
            this.textBox319.Width = 0.8270833F;
            // 
            // textBox320
            // 
            this.textBox320.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox320.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox320.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox320.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox320.CanGrow = false;
            this.textBox320.DataField = "ITEM062";
            this.textBox320.Height = 0.1576389F;
            this.textBox320.Left = 8.814947F;
            this.textBox320.Name = "textBox320";
            this.textBox320.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; ddo-char-set: 1";
            this.textBox320.Tag = "";
            this.textBox320.Text = "ITEM062";
            this.textBox320.Top = 2.645938F;
            this.textBox320.Width = 0.7090278F;
            // 
            // textBox321
            // 
            this.textBox321.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox321.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox321.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox321.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox321.Height = 0.1576389F;
            this.textBox321.Left = 8.076754F;
            this.textBox321.Name = "textBox321";
            this.textBox321.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox321.Tag = "";
            this.textBox321.Text = "新生命保険料の金額\r\n";
            this.textBox321.Top = 2.645938F;
            this.textBox321.Width = 0.7375F;
            // 
            // textBox322
            // 
            this.textBox322.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox322.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox322.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox322.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox322.CanGrow = false;
            this.textBox322.DataField = "ITEM067";
            this.textBox322.Height = 0.1576389F;
            this.textBox322.Left = 10.35106F;
            this.textBox322.Name = "textBox322";
            this.textBox322.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; ddo-char-set: 1";
            this.textBox322.Tag = "";
            this.textBox322.Text = "ITEM067";
            this.textBox322.Top = 2.803576F;
            this.textBox322.Width = 0.7090278F;
            // 
            // textBox323
            // 
            this.textBox323.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox323.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox323.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox323.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox323.Height = 0.1576389F;
            this.textBox323.Left = 9.523975F;
            this.textBox323.Name = "textBox323";
            this.textBox323.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox323.Tag = "";
            this.textBox323.Text = "旧長期損害保険料の金額";
            this.textBox323.Top = 2.803576F;
            this.textBox323.Width = 0.8270833F;
            // 
            // textBox474
            // 
            this.textBox474.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox474.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox474.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox474.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox474.CanGrow = false;
            this.textBox474.DataField = "ITEM063";
            this.textBox474.Height = 0.1576389F;
            this.textBox474.Left = 8.814947F;
            this.textBox474.Name = "textBox474";
            this.textBox474.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; ddo-char-set: 1";
            this.textBox474.Tag = "";
            this.textBox474.Text = "ITEM063";
            this.textBox474.Top = 2.803576F;
            this.textBox474.Width = 0.7090278F;
            // 
            // textBox475
            // 
            this.textBox475.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox475.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox475.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox475.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox475.Height = 0.1576389F;
            this.textBox475.Left = 8.076754F;
            this.textBox475.Name = "textBox475";
            this.textBox475.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox475.Tag = "";
            this.textBox475.Text = "旧生命保険料の金額";
            this.textBox475.Top = 2.803576F;
            this.textBox475.Width = 0.7375F;
            // 
            // textBox476
            // 
            this.textBox476.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox476.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox476.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox476.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox476.Height = 0.3944443F;
            this.textBox476.Left = 5.941339F;
            this.textBox476.Name = "textBox476";
            this.textBox476.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox476.Tag = "";
            this.textBox476.Text = "支払者";
            this.textBox476.Top = 3.473024F;
            this.textBox476.Width = 0.2131944F;
            // 
            // textBox477
            // 
            this.textBox477.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox477.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox477.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox477.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox477.Height = 0.1972222F;
            this.textBox477.Left = 6.154534F;
            this.textBox477.Name = "textBox477";
            this.textBox477.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox477.Tag = "";
            this.textBox477.Text = "住所(居所)\r\n又は所在地 ";
            this.textBox477.Top = 3.473024F;
            this.textBox477.Width = 0.6375F;
            // 
            // textBox478
            // 
            this.textBox478.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox478.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox478.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox478.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox478.CanGrow = false;
            this.textBox478.Height = 0.1972222F;
            this.textBox478.Left = 6.154534F;
            this.textBox478.Name = "textBox478";
            this.textBox478.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox478.Tag = "";
            this.textBox478.Text = "氏 名 又 は\r\n名　　　 称";
            this.textBox478.Top = 3.670246F;
            this.textBox478.Width = 0.6375F;
            // 
            // textBox479
            // 
            this.textBox479.DataField = "ITEM098";
            this.textBox479.Height = 0.1576389F;
            this.textBox479.Left = 9.681103F;
            this.textBox479.Name = "textBox479";
            this.textBox479.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.textBox479.Tag = "";
            this.textBox479.Text = "ITEM098";
            this.textBox479.Top = 3.683071F;
            this.textBox479.Width = 1.358333F;
            // 
            // label29
            // 
            this.label29.Height = 0.1180556F;
            this.label29.HyperLink = null;
            this.label29.Left = 6.243423F;
            this.label29.Name = "label29";
            this.label29.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.label29.Tag = "";
            this.label29.Text = "住宅借入金等特別控除可能額";
            this.label29.Top = 2.342465F;
            this.label29.Width = 1.141667F;
            // 
            // textBox480
            // 
            this.textBox480.DataField = "ITEM050";
            this.textBox480.Height = 0.1180556F;
            this.textBox480.Left = 7.385086F;
            this.textBox480.Name = "textBox480";
            this.textBox480.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.textBox480.Tag = "";
            this.textBox480.Text = "ITEM050";
            this.textBox480.Top = 2.342465F;
            this.textBox480.Width = 0.5277781F;
            // 
            // label30
            // 
            this.label30.Height = 0.1180556F;
            this.label30.HyperLink = null;
            this.label30.Left = 7.976058F;
            this.label30.Name = "label30";
            this.label30.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.label30.Tag = "";
            this.label30.Text = "国民年金保険料等の金額";
            this.label30.Top = 2.342465F;
            this.label30.Width = 0.9451389F;
            // 
            // textBox481
            // 
            this.textBox481.DataField = "ITEM051";
            this.textBox481.Height = 0.1180556F;
            this.textBox481.Left = 8.921196F;
            this.textBox481.Name = "textBox481";
            this.textBox481.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.textBox481.Tag = "";
            this.textBox481.Text = "ITEM051";
            this.textBox481.Top = 2.342465F;
            this.textBox481.Width = 0.5909723F;
            // 
            // textBox482
            // 
            this.textBox482.DataField = "ITEM052";
            this.textBox482.Height = 0.1416667F;
            this.textBox482.Left = 6.243423F;
            this.textBox482.Name = "textBox482";
            this.textBox482.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.textBox482.Tag = "";
            this.textBox482.Text = "ITEM052";
            this.textBox482.Top = 2.460522F;
            this.textBox482.Width = 1.784028F;
            // 
            // textBox483
            // 
            this.textBox483.DataField = "ITEM053";
            this.textBox483.Height = 0.1104167F;
            this.textBox483.Left = 6.243423F;
            this.textBox483.Name = "textBox483";
            this.textBox483.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.textBox483.Tag = "";
            this.textBox483.Text = "ITEM053";
            this.textBox483.Top = 2.570937F;
            this.textBox483.Width = 1.784028F;
            // 
            // textBox484
            // 
            this.textBox484.DataField = "ITEM054";
            this.textBox484.Height = 0.1104167F;
            this.textBox484.Left = 6.243307F;
            this.textBox484.Name = "textBox484";
            this.textBox484.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.textBox484.Tag = "";
            this.textBox484.Text = "ITEM054";
            this.textBox484.Top = 2.693302F;
            this.textBox484.Width = 1.784028F;
            // 
            // textBox485
            // 
            this.textBox485.DataField = "ITEM055";
            this.textBox485.Height = 0.07847222F;
            this.textBox485.Left = 5.980923F;
            this.textBox485.Name = "textBox485";
            this.textBox485.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.textBox485.Tag = "";
            this.textBox485.Text = "ITEM055";
            this.textBox485.Top = 2.791772F;
            this.textBox485.Width = 0.6694444F;
            // 
            // textBox486
            // 
            this.textBox486.DataField = "ITEM058";
            this.textBox486.Height = 0.07847222F;
            this.textBox486.Left = 5.980923F;
            this.textBox486.Name = "textBox486";
            this.textBox486.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.textBox486.Tag = "";
            this.textBox486.Text = "ITEM058";
            this.textBox486.Top = 2.870243F;
            this.textBox486.Width = 0.6694444F;
            // 
            // textBox487
            // 
            this.textBox487.DataField = "ITEM056";
            this.textBox487.Height = 0.07847222F;
            this.textBox487.Left = 6.680923F;
            this.textBox487.Name = "textBox487";
            this.textBox487.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.textBox487.Tag = "";
            this.textBox487.Text = "ITEM056";
            this.textBox487.Top = 2.790383F;
            this.textBox487.Width = 0.6694444F;
            // 
            // textBox488
            // 
            this.textBox488.DataField = "ITEM059";
            this.textBox488.Height = 0.07847222F;
            this.textBox488.Left = 6.680923F;
            this.textBox488.Name = "textBox488";
            this.textBox488.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.textBox488.Tag = "";
            this.textBox488.Text = "ITEM059";
            this.textBox488.Top = 2.868855F;
            this.textBox488.Width = 0.6694444F;
            // 
            // textBox489
            // 
            this.textBox489.DataField = "ITEM057";
            this.textBox489.Height = 0.07847222F;
            this.textBox489.Left = 7.378843F;
            this.textBox489.Name = "textBox489";
            this.textBox489.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.textBox489.Tag = "";
            this.textBox489.Text = "ITEM057";
            this.textBox489.Top = 2.790383F;
            this.textBox489.Width = 0.6694444F;
            // 
            // textBox490
            // 
            this.textBox490.DataField = "ITEM060";
            this.textBox490.Height = 0.07847222F;
            this.textBox490.Left = 7.378843F;
            this.textBox490.Name = "textBox490";
            this.textBox490.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.textBox490.Tag = "";
            this.textBox490.Text = "ITEM060";
            this.textBox490.Top = 2.868855F;
            this.textBox490.Width = 0.6694444F;
            // 
            // label31
            // 
            this.label31.Height = 0.08611111F;
            this.label31.HyperLink = null;
            this.label31.Left = 6.816256F;
            this.label31.Name = "label31";
            this.label31.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.label31.Tag = "";
            this.label31.Text = "Ａ内　　　　　　　　　 　円";
            this.label31.Top = 1.3622F;
            this.label31.Width = 0.9719342F;
            // 
            // label32
            // 
            this.label32.Height = 0.08611111F;
            this.label32.HyperLink = null;
            this.label32.Left = 10.01418F;
            this.label32.Name = "label32";
            this.label32.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.label32.Tag = "";
            this.label32.Text = "内　　　　　　　　　　　 　円";
            this.label32.Top = 1.3622F;
            this.label32.Width = 1.035351F;
            // 
            // label33
            // 
            this.label33.Height = 0.08611111F;
            this.label33.HyperLink = null;
            this.label33.Left = 9.805836F;
            this.label33.Name = "label33";
            this.label33.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label33.Tag = "";
            this.label33.Text = "円";
            this.label33.Top = 1.3622F;
            this.label33.Width = 0.1666667F;
            // 
            // label34
            // 
            this.label34.Height = 0.08611111F;
            this.label34.HyperLink = null;
            this.label34.Left = 7.875985F;
            this.label34.Name = "label34";
            this.label34.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.label34.Tag = "";
            this.label34.Text = "Ｂ　　　　　　　　　　　　円";
            this.label34.Top = 1.3622F;
            this.label34.Width = 1.034017F;
            // 
            // textBox491
            // 
            this.textBox491.DataField = "ITEM046";
            this.textBox491.Height = 0.09582743F;
            this.textBox491.Left = 8.722586F;
            this.textBox491.Name = "textBox491";
            this.textBox491.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox491.Tag = "";
            this.textBox491.Text = "ITEM046";
            this.textBox491.Top = 2.071632F;
            this.textBox491.Width = 0.6673612F;
            // 
            // label35
            // 
            this.label35.Height = 0.08611111F;
            this.label35.HyperLink = null;
            this.label35.Left = 6.951756F;
            this.label35.Name = "label35";
            this.label35.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label35.Tag = "";
            this.label35.Text = "円";
            this.label35.Top = 1.977882F;
            this.label35.Width = 0.1666667F;
            // 
            // label36
            // 
            this.label36.Height = 0.08611111F;
            this.label36.HyperLink = null;
            this.label36.Left = 7.128836F;
            this.label36.Name = "label36";
            this.label36.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label36.Tag = "";
            this.label36.Text = "人";
            this.label36.Top = 1.977882F;
            this.label36.Width = 0.1286445F;
            // 
            // label40
            // 
            this.label40.Height = 0.08611111F;
            this.label40.HyperLink = null;
            this.label40.Left = 7.399669F;
            this.label40.Name = "label40";
            this.label40.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.label40.Tag = "";
            this.label40.Text = "内    人";
            this.label40.Top = 1.977882F;
            this.label40.Width = 0.3074185F;
            // 
            // label41
            // 
            this.label41.Height = 0.08611111F;
            this.label41.HyperLink = null;
            this.label41.Left = 7.837169F;
            this.label41.Name = "label41";
            this.label41.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label41.Tag = "";
            this.label41.Text = "人";
            this.label41.Top = 1.977882F;
            this.label41.Width = 0.15625F;
            // 
            // label42
            // 
            this.label42.Height = 0.08611111F;
            this.label42.HyperLink = null;
            this.label42.Left = 8.140158F;
            this.label42.Name = "label42";
            this.label42.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.label42.Tag = "";
            this.label42.Text = "Ｋ内 Ｌ人  Ｍ人";
            this.label42.Top = 1.988299F;
            this.label42.Width = 0.5543305F;
            // 
            // label44
            // 
            this.label44.Height = 0.08611111F;
            this.label44.HyperLink = null;
            this.label44.Left = 9.826752F;
            this.label44.Name = "label44";
            this.label44.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label44.Tag = "";
            this.label44.Text = "円";
            this.label44.Top = 1.988299F;
            this.label44.Width = 0.1666667F;
            // 
            // label45
            // 
            this.label45.Height = 0.08611111F;
            this.label45.HyperLink = null;
            this.label45.Left = 10.2955F;
            this.label45.Name = "label45";
            this.label45.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label45.Tag = "";
            this.label45.Text = "円";
            this.label45.Top = 1.993701F;
            this.label45.Width = 0.1666667F;
            // 
            // label46
            // 
            this.label46.Height = 0.08611111F;
            this.label46.HyperLink = null;
            this.label46.Left = 10.87884F;
            this.label46.Name = "label46";
            this.label46.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label46.Tag = "";
            this.label46.Text = "円";
            this.label46.Top = 1.993701F;
            this.label46.Width = 0.1666667F;
            // 
            // label47
            // 
            this.label47.Height = 0.08611111F;
            this.label47.HyperLink = null;
            this.label47.Left = 9.358002F;
            this.label47.Name = "label47";
            this.label47.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label47.Tag = "";
            this.label47.Text = "円";
            this.label47.Top = 2.488299F;
            this.label47.Width = 0.1666667F;
            // 
            // label48
            // 
            this.label48.Height = 0.08611111F;
            this.label48.HyperLink = null;
            this.label48.Left = 9.358002F;
            this.label48.Name = "label48";
            this.label48.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label48.Tag = "";
            this.label48.Text = "円";
            this.label48.Top = 2.644549F;
            this.label48.Width = 0.1666667F;
            // 
            // label49
            // 
            this.label49.Height = 0.08611111F;
            this.label49.HyperLink = null;
            this.label49.Left = 9.358002F;
            this.label49.Name = "label49";
            this.label49.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label49.Tag = "";
            this.label49.Text = "円";
            this.label49.Top = 2.800799F;
            this.label49.Width = 0.1666667F;
            // 
            // label50
            // 
            this.label50.Height = 0.08611111F;
            this.label50.HyperLink = null;
            this.label50.Left = 10.88925F;
            this.label50.Name = "label50";
            this.label50.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label50.Tag = "";
            this.label50.Text = "円";
            this.label50.Top = 2.488299F;
            this.label50.Width = 0.1666667F;
            // 
            // label51
            // 
            this.label51.Height = 0.08611111F;
            this.label51.HyperLink = null;
            this.label51.Left = 10.88925F;
            this.label51.Name = "label51";
            this.label51.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label51.Tag = "";
            this.label51.Text = "円";
            this.label51.Top = 2.644549F;
            this.label51.Width = 0.1666667F;
            // 
            // label52
            // 
            this.label52.Height = 0.08611111F;
            this.label52.HyperLink = null;
            this.label52.Left = 10.88925F;
            this.label52.Name = "label52";
            this.label52.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label52.Tag = "";
            this.label52.Text = "円";
            this.label52.Top = 2.800799F;
            this.label52.Width = 0.1666667F;
            // 
            // label79
            // 
            this.label79.Height = 0.08611111F;
            this.label79.HyperLink = null;
            this.label79.Left = 10.88925F;
            this.label79.Name = "label79";
            this.label79.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label79.Tag = "";
            this.label79.Text = "円";
            this.label79.Top = 2.332049F;
            this.label79.Width = 0.1666667F;
            // 
            // textBox492
            // 
            this.textBox492.CanGrow = false;
            this.textBox492.DataField = "ITEM025";
            this.textBox492.Height = 0.1472222F;
            this.textBox492.Left = 9.670502F;
            this.textBox492.Name = "textBox492";
            this.textBox492.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: left; ddo-char-set: 1";
            this.textBox492.Tag = "";
            this.textBox492.Text = "ITEM025";
            this.textBox492.Top = 0.4674655F;
            this.textBox492.Width = 1.364583F;
            // 
            // textBox493
            // 
            this.textBox493.DataField = "ITEM026";
            this.textBox493.Height = 0.1576389F;
            this.textBox493.Left = 9.55592F;
            this.textBox493.Name = "textBox493";
            this.textBox493.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: left; ddo-char-set: 1";
            this.textBox493.Tag = "";
            this.textBox493.Text = "ITEM026";
            this.textBox493.Top = 0.6549655F;
            this.textBox493.Width = 1.479167F;
            // 
            // textBox494
            // 
            this.textBox494.DataField = "ITEM027";
            this.textBox494.Height = 0.1368055F;
            this.textBox494.Left = 9.55592F;
            this.textBox494.Name = "textBox494";
            this.textBox494.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.textBox494.Tag = "";
            this.textBox494.Text = "ITEM027";
            this.textBox494.Top = 0.852883F;
            this.textBox494.Width = 1.479167F;
            // 
            // textBox495
            // 
            this.textBox495.DataField = "ITEM028";
            this.textBox495.Height = 0.1576389F;
            this.textBox495.Left = 9.55592F;
            this.textBox495.Name = "textBox495";
            this.textBox495.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: left; ddo-char-set: 1";
            this.textBox495.Tag = "";
            this.textBox495.Text = "ITEM028";
            this.textBox495.Top = 1.009133F;
            this.textBox495.Width = 1.479167F;
            // 
            // textBox496
            // 
            this.textBox496.DataField = "ITEM082";
            this.textBox496.Height = 0.1381944F;
            this.textBox496.Left = 8.93092F;
            this.textBox496.Name = "textBox496";
            this.textBox496.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.textBox496.Tag = "";
            this.textBox496.Text = "ITEM082";
            this.textBox496.Top = 3.186215F;
            this.textBox496.Width = 0.2131944F;
            // 
            // textBox497
            // 
            this.textBox497.DataField = "ITEM083";
            this.textBox497.Height = 0.1381944F;
            this.textBox497.Left = 9.144114F;
            this.textBox497.Name = "textBox497";
            this.textBox497.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.textBox497.Tag = "";
            this.textBox497.Text = "ITEM083";
            this.textBox497.Top = 3.186215F;
            this.textBox497.Width = 0.2131944F;
            // 
            // textBox498
            // 
            this.textBox498.DataField = "ITEM084";
            this.textBox498.Height = 0.1381944F;
            this.textBox498.Left = 9.357307F;
            this.textBox498.Name = "textBox498";
            this.textBox498.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.textBox498.Tag = "";
            this.textBox498.Text = "ITEM084";
            this.textBox498.Top = 3.186215F;
            this.textBox498.Width = 0.2131944F;
            // 
            // shape3
            // 
            this.shape3.Height = 0.1976378F;
            this.shape3.Left = 9.08819F;
            this.shape3.LineWeight = 3F;
            this.shape3.Name = "shape3";
            this.shape3.RoundingRadius = 9.999999F;
            this.shape3.Top = 0.6322786F;
            this.shape3.Width = 1.969685F;
            // 
            // shape5
            // 
            this.shape5.Height = 0.3153543F;
            this.shape5.Left = 6.80748F;
            this.shape5.LineWeight = 3F;
            this.shape5.Name = "shape5";
            this.shape5.RoundingRadius = 9.999999F;
            this.shape5.Top = 1.345669F;
            this.shape5.Width = 1.068504F;
            // 
            // shape6
            // 
            this.shape6.Height = 0.3153543F;
            this.shape6.Left = 7.861024F;
            this.shape6.LineWeight = 3F;
            this.shape6.Name = "shape6";
            this.shape6.RoundingRadius = 9.999999F;
            this.shape6.Top = 1.345669F;
            this.shape6.Width = 1.076378F;
            // 
            // shape9
            // 
            this.shape9.Height = 0.3767719F;
            this.shape9.Left = 5.941339F;
            this.shape9.LineWeight = 3F;
            this.shape9.Name = "shape9";
            this.shape9.RoundingRadius = 9.999999F;
            this.shape9.Top = 1.953932F;
            this.shape9.Width = 0.248031F;
            // 
            // shape11
            // 
            this.shape11.Height = 0.3767719F;
            this.shape11.Left = 6.40315F;
            this.shape11.LineWeight = 3F;
            this.shape11.Name = "shape11";
            this.shape11.RoundingRadius = 9.999999F;
            this.shape11.Top = 1.953937F;
            this.shape11.Width = 0.8578742F;
            // 
            // shape12
            // 
            this.shape12.Height = 0.3767719F;
            this.shape12.Left = 6.519292F;
            this.shape12.LineWeight = 3F;
            this.shape12.Name = "shape12";
            this.shape12.RoundingRadius = 9.999999F;
            this.shape12.Top = 1.953932F;
            this.shape12.Width = 0.6141732F;
            // 
            // shape15
            // 
            this.shape15.Height = 0.3767719F;
            this.shape15.Left = 7.385039F;
            this.shape15.LineWeight = 3F;
            this.shape15.Name = "shape15";
            this.shape15.RoundingRadius = 9.999999F;
            this.shape15.Top = 1.953937F;
            this.shape15.Width = 0.3137797F;
            // 
            // shape17
            // 
            this.shape17.Height = 0.3767719F;
            this.shape17.Left = 7.825984F;
            this.shape17.LineWeight = 3F;
            this.shape17.Name = "shape17";
            this.shape17.RoundingRadius = 9.999999F;
            this.shape17.Top = 1.953937F;
            this.shape17.Width = 0.1771653F;
            // 
            // shape19
            // 
            this.shape19.Height = 0.3728347F;
            this.shape19.Left = 8.140158F;
            this.shape19.LineWeight = 3F;
            this.shape19.Name = "shape19";
            this.shape19.RoundingRadius = 9.999999F;
            this.shape19.Top = 1.953937F;
            this.shape19.Width = 2.340551F;
            // 
            // shape21
            // 
            this.shape21.Height = 0.3767719F;
            this.shape21.Left = 9.401184F;
            this.shape21.LineWeight = 3F;
            this.shape21.Name = "shape21";
            this.shape21.RoundingRadius = 9.999999F;
            this.shape21.Top = 1.953932F;
            this.shape21.Width = 0.6066929F;
            // 
            // shape26
            // 
            this.shape26.Height = 0.6444888F;
            this.shape26.Left = 10.35039F;
            this.shape26.LineWeight = 3F;
            this.shape26.Name = "shape26";
            this.shape26.RoundingRadius = 9.999999F;
            this.shape26.Top = 2.326378F;
            this.shape26.Width = 0.7141743F;
            // 
            // shape27
            // 
            this.shape27.Height = 0.5255904F;
            this.shape27.Left = 5.940552F;
            this.shape27.LineWeight = 3F;
            this.shape27.Name = "shape27";
            this.shape27.RoundingRadius = 9.999999F;
            this.shape27.Top = 2.961024F;
            this.shape27.Width = 0.4409451F;
            // 
            // shape28
            // 
            this.shape28.Height = 0.529134F;
            this.shape28.Left = 7.202363F;
            this.shape28.LineWeight = 3F;
            this.shape28.Name = "shape28";
            this.shape28.RoundingRadius = 9.999999F;
            this.shape28.Top = 2.948819F;
            this.shape28.Width = 1.311024F;
            // 
            // label80
            // 
            this.label80.Height = 0.08611111F;
            this.label80.HyperLink = null;
            this.label80.Left = 1.60315F;
            this.label80.Name = "label80";
            this.label80.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 4.5pt; font-weight: normal; text-ali" +
    "gn: left; ddo-char-set: 1";
            this.label80.Tag = "";
            this.label80.Text = "従人";
            this.label80.Top = 1.817116F;
            this.label80.Width = 0.1562991F;
            // 
            // textBox51
            // 
            this.textBox51.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox51.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox51.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox51.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox51.DataField = "ITEM038";
            this.textBox51.Height = 0.3541667F;
            this.textBox51.Left = 1.465354F;
            this.textBox51.Name = "textBox51";
            this.textBox51.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox51.Tag = "";
            this.textBox51.Text = "ITEM038";
            this.textBox51.Top = 5.794232F;
            this.textBox51.Width = 0.1301349F;
            // 
            // textBox122
            // 
            this.textBox122.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox122.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox122.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox122.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox122.Height = 0.3541667F;
            this.textBox122.Left = 1.599032F;
            this.textBox122.Name = "textBox122";
            this.textBox122.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: bold; tex" +
    "t-align: center; ddo-char-set: 1";
            this.textBox122.Tag = "";
            this.textBox122.Text = null;
            this.textBox122.Top = 5.794253F;
            this.textBox122.Width = 0.1417323F;
            // 
            // textBox123
            // 
            this.textBox123.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox123.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox123.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox123.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox123.DataField = "ITEM040";
            this.textBox123.Height = 0.3541667F;
            this.textBox123.Left = 1.877953F;
            this.textBox123.Name = "textBox123";
            this.textBox123.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox123.Tag = "";
            this.textBox123.Text = "ITEM040";
            this.textBox123.Top = 5.794232F;
            this.textBox123.Width = 0.1456693F;
            // 
            // textBox125
            // 
            this.textBox125.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox125.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox125.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox125.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox125.DataField = "ITEM039";
            this.textBox125.Height = 0.3541667F;
            this.textBox125.Left = 1.740355F;
            this.textBox125.Name = "textBox125";
            this.textBox125.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox125.Tag = "";
            this.textBox125.Text = "ITEM039";
            this.textBox125.Top = 5.794232F;
            this.textBox125.Width = 0.1375984F;
            // 
            // label9
            // 
            this.label9.Height = 0.08611111F;
            this.label9.HyperLink = null;
            this.label9.Left = 1.47091F;
            this.label9.Name = "label9";
            this.label9.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label9.Tag = "";
            this.label9.Text = "人";
            this.label9.Top = 5.79562F;
            this.label9.Width = 0.1245788F;
            // 
            // label10
            // 
            this.label10.Height = 0.08611111F;
            this.label10.HyperLink = null;
            this.label10.Left = 1.741744F;
            this.label10.Name = "label10";
            this.label10.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.label10.Tag = "";
            this.label10.Text = "内    人";
            this.label10.Top = 5.79562F;
            this.label10.Width = 0.3137678F;
            // 
            // label13
            // 
            this.label13.Height = 0.08611111F;
            this.label13.HyperLink = null;
            this.label13.Left = 1.60297F;
            this.label13.Name = "label13";
            this.label13.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 4.5pt; font-weight: normal; text-ali" +
    "gn: left; ddo-char-set: 1";
            this.label13.Tag = "";
            this.label13.Text = "従人";
            this.label13.Top = 5.79562F;
            this.label13.Width = 0.1562991F;
            // 
            // label81
            // 
            this.label81.Height = 0.08611111F;
            this.label81.HyperLink = null;
            this.label81.Left = 2.038583F;
            this.label81.Name = "label81";
            this.label81.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 4.5pt; font-weight: normal; text-ali" +
    "gn: left; ddo-char-set: 1";
            this.label81.Tag = "";
            this.label81.Text = "従人";
            this.label81.Top = 1.815748F;
            this.label81.Width = 0.1562991F;
            // 
            // label82
            // 
            this.label82.Height = 0.08611111F;
            this.label82.HyperLink = null;
            this.label82.Left = 2.331496F;
            this.label82.Name = "label82";
            this.label82.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 4.5pt; font-weight: normal; text-ali" +
    "gn: left; ddo-char-set: 1";
            this.label82.Tag = "";
            this.label82.Text = "従人";
            this.label82.Top = 1.817116F;
            this.label82.Width = 0.1562991F;
            // 
            // label11
            // 
            this.label11.Height = 0.08611111F;
            this.label11.HyperLink = null;
            this.label11.Left = 2.023228F;
            this.label11.Name = "label11";
            this.label11.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 4.5pt; font-weight: normal; text-ali" +
    "gn: left; ddo-char-set: 1";
            this.label11.Tag = "";
            this.label11.Text = "従人";
            this.label11.Top = 5.794232F;
            this.label11.Width = 0.1562991F;
            // 
            // label83
            // 
            this.label83.Height = 0.08611111F;
            this.label83.HyperLink = null;
            this.label83.Left = 2.34252F;
            this.label83.Name = "label83";
            this.label83.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 4.5pt; font-weight: normal; text-ali" +
    "gn: left; ddo-char-set: 1";
            this.label83.Tag = "";
            this.label83.Text = "従人";
            this.label83.Top = 5.797381F;
            this.label83.Width = 0.1562991F;
            // 
            // label56
            // 
            this.label56.Height = 0.1041667F;
            this.label56.HyperLink = null;
            this.label56.Left = 6.425985F;
            this.label56.Name = "label56";
            this.label56.Style = "font-family: ＭＳ 明朝; font-size: 5.25pt; ddo-char-set: 128";
            this.label56.Text = "Ｅ  Ｆ";
            this.label56.Top = 1.984252F;
            this.label56.Width = 0.25F;
            // 
            // label12
            // 
            this.label12.Height = 0.1041667F;
            this.label12.HyperLink = null;
            this.label12.Left = 7.13504F;
            this.label12.Name = "label12";
            this.label12.Style = "font-family: ＭＳ 明朝; font-size: 5.25pt; ddo-char-set: 128";
            this.label12.Text = "G";
            this.label12.Top = 1.984252F;
            this.label12.Width = 0.06496096F;
            // 
            // label86
            // 
            this.label86.Height = 0.1041667F;
            this.label86.HyperLink = null;
            this.label86.Left = 7.410237F;
            this.label86.Name = "label86";
            this.label86.Style = "font-family: ＭＳ 明朝; font-size: 5.25pt; ddo-char-set: 128";
            this.label86.Text = "H\r\n";
            this.label86.Top = 2.053544F;
            this.label86.Width = 0.1074805F;
            // 
            // label37
            // 
            this.label37.Height = 0.08611111F;
            this.label37.HyperLink = null;
            this.label37.Left = 7.242126F;
            this.label37.Name = "label37";
            this.label37.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 4.5pt; font-weight: normal; text-ali" +
    "gn: left; ddo-char-set: 1";
            this.label37.Tag = "";
            this.label37.Text = "従人";
            this.label37.Top = 1.977953F;
            this.label37.Width = 0.1562991F;
            // 
            // label87
            // 
            this.label87.Height = 0.09212581F;
            this.label87.HyperLink = null;
            this.label87.Left = 10.01417F;
            this.label87.Name = "label87";
            this.label87.Style = "font-family: ＭＳ 明朝; font-size: 5.5pt; ddo-char-set: 1";
            this.label87.Text = "P";
            this.label87.Top = 1.993701F;
            this.label87.Width = 0.1377954F;
            // 
            // label38
            // 
            this.label38.Height = 0.08611111F;
            this.label38.HyperLink = null;
            this.label38.Left = 7.992126F;
            this.label38.Name = "label38";
            this.label38.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 4.5pt; font-weight: normal; text-ali" +
    "gn: left; ddo-char-set: 1";
            this.label38.Tag = "";
            this.label38.Text = "従人";
            this.label38.Top = 1.976378F;
            this.label38.Width = 0.1562991F;
            // 
            // label39
            // 
            this.label39.Height = 0.08611111F;
            this.label39.HyperLink = null;
            this.label39.Left = 7.687008F;
            this.label39.Name = "label39";
            this.label39.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 4.5pt; font-weight: normal; text-ali" +
    "gn: left; ddo-char-set: 1";
            this.label39.Tag = "";
            this.label39.Text = "従人";
            this.label39.Top = 1.976378F;
            this.label39.Width = 0.1562991F;
            // 
            // label88
            // 
            this.label88.Height = 0.1041667F;
            this.label88.HyperLink = null;
            this.label88.Left = 7.532284F;
            this.label88.Name = "label88";
            this.label88.Style = "font-family: ＭＳ 明朝; font-size: 5.25pt; ddo-char-set: 128";
            this.label88.Text = "I";
            this.label88.Top = 1.984252F;
            this.label88.Width = 0.1074806F;
            // 
            // label89
            // 
            this.label89.Height = 0.1041667F;
            this.label89.HyperLink = null;
            this.label89.Left = 7.837008F;
            this.label89.Name = "label89";
            this.label89.Style = "font-family: ＭＳ 明朝; font-size: 5.25pt; ddo-char-set: 128";
            this.label89.Text = "J";
            this.label89.Top = 1.984252F;
            this.label89.Width = 0.1074806F;
            // 
            // label43
            // 
            this.label43.Height = 0.08070866F;
            this.label43.HyperLink = null;
            this.label43.Left = 8.825985F;
            this.label43.Name = "label43";
            this.label43.Style = "font-family: ＭＳ 明朝; font-size: 5.5pt; ddo-char-set: 1";
            this.label43.Text = "N";
            this.label43.Top = 1.996063F;
            this.label43.Width = 0.1074806F;
            // 
            // label90
            // 
            this.label90.Height = 0.08070868F;
            this.label90.HyperLink = null;
            this.label90.Left = 9.448425F;
            this.label90.Name = "label90";
            this.label90.Style = "font-family: ＭＳ 明朝; font-size: 5.5pt; ddo-char-set: 1";
            this.label90.Text = "O";
            this.label90.Top = 1.993701F;
            this.label90.Width = 0.1074806F;
            // 
            // label91
            // 
            this.label91.Height = 0.1041667F;
            this.label91.HyperLink = null;
            this.label91.Left = 6.173623F;
            this.label91.Name = "label91";
            this.label91.Style = "font-family: ＭＳ 明朝; font-size: 5.25pt; ddo-char-set: 128";
            this.label91.Text = "チ";
            this.label91.Top = 2.973622F;
            this.label91.Width = 0.1311026F;
            // 
            // label92
            // 
            this.label92.Height = 0.1041667F;
            this.label92.HyperLink = null;
            this.label92.Left = 7.222442F;
            this.label92.Name = "label92";
            this.label92.Style = "font-family: ＭＳ 明朝; font-size: 5.25pt; ddo-char-set: 128";
            this.label92.Text = "リ　　ヌ　　ル　　ヲ";
            this.label92.Top = 3.093701F;
            this.label92.Width = 0.765748F;
            // 
            // label93
            // 
            this.label93.Height = 0.1041667F;
            this.label93.HyperLink = null;
            this.label93.Left = 8.076773F;
            this.label93.Name = "label93";
            this.label93.Style = "font-family: ＭＳ 明朝; font-size: 5.25pt; ddo-char-set: 128";
            this.label93.Text = "ワ　　カ";
            this.label93.Top = 2.97874F;
            this.label93.Width = 0.3854331F;
            // 
            // shape20
            // 
            this.shape20.Height = 0.5287399F;
            this.shape20.Left = 8.499607F;
            this.shape20.LineWeight = 3F;
            this.shape20.Name = "shape20";
            this.shape20.RoundingRadius = 9.999999F;
            this.shape20.Top = 2.949213F;
            this.shape20.Width = 1.07874F;
            // 
            // label94
            // 
            this.label94.Height = 0.6094489F;
            this.label94.HyperLink = null;
            this.label94.Left = 10.38662F;
            this.label94.Name = "label94";
            this.label94.Style = "font-family: ＭＳ 明朝; font-size: 5.25pt; ddo-char-set: 128";
            this.label94.Text = "ニ\r\n\r\nホ\r\n\r\nヘ\r\n\r\nト";
            this.label94.Top = 2.373229F;
            this.label94.Width = 0.1342515F;
            // 
            // label95
            // 
            this.label95.Height = 0.3909449F;
            this.label95.HyperLink = null;
            this.label95.Left = 8.822048F;
            this.label95.Name = "label95";
            this.label95.Style = "font-family: ＭＳ 明朝; font-size: 5.25pt; ddo-char-set: 128";
            this.label95.Text = "イ\r\n\r\nロ\r\n\r\nハ\r\n\r\n";
            this.label95.Top = 2.523622F;
            this.label95.Width = 0.1342515F;
            // 
            // textBox1
            // 
            this.textBox1.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox1.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox1.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox1.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox1.Height = 0.7520834F;
            this.textBox1.Left = 5.941339F;
            this.textBox1.Name = "textBox1";
            this.textBox1.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7.5pt; font" +
    "-weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox1.Tag = "";
            this.textBox1.Text = "支　払 を受け　る　者";
            this.textBox1.Top = 4.537402F;
            this.textBox1.Width = 0.357874F;
            // 
            // textBox127
            // 
            this.textBox127.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox127.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox127.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox127.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox127.Height = 0.7520834F;
            this.textBox127.Left = 6.29567F;
            this.textBox127.Name = "textBox127";
            this.textBox127.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox127.Tag = "";
            this.textBox127.Text = "住所又は居所";
            this.textBox127.Top = 4.537407F;
            this.textBox127.Width = 0.1576389F;
            // 
            // textBox324
            // 
            this.textBox324.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox324.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox324.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox324.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox324.DataField = "ITEM099";
            this.textBox324.Height = 0.2011812F;
            this.textBox324.Left = 6.45315F;
            this.textBox324.Name = "textBox324";
            this.textBox324.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: left; vertical-align: middle; ddo-char-set: 1";
            this.textBox324.Tag = "";
            this.textBox324.Text = "ITEM099";
            this.textBox324.Top = 4.536225F;
            this.textBox324.Width = 0.3937064F;
            // 
            // textBox325
            // 
            this.textBox325.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox325.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox325.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox325.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox325.Height = 0.1972222F;
            this.textBox325.Left = 6.846851F;
            this.textBox325.Name = "textBox325";
            this.textBox325.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: bold; text-align: left; ddo-char-set: 1";
            this.textBox325.Tag = "";
            this.textBox325.Text = null;
            this.textBox325.Top = 4.536225F;
            this.textBox325.Width = 2.086806F;
            // 
            // textBox326
            // 
            this.textBox326.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox326.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox326.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox326.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox326.Height = 0.1576389F;
            this.textBox326.Left = 8.463552F;
            this.textBox326.Name = "textBox326";
            this.textBox326.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: left; ddo-char-set: 1";
            this.textBox326.Tag = "";
            this.textBox326.Text = null;
            this.textBox326.Top = 4.385777F;
            this.textBox326.Width = 0.1576389F;
            // 
            // textBox327
            // 
            this.textBox327.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox327.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox327.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox327.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox327.Height = 0.1576389F;
            this.textBox327.Left = 8.305914F;
            this.textBox327.Name = "textBox327";
            this.textBox327.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: left; ddo-char-set: 1";
            this.textBox327.Tag = "";
            this.textBox327.Text = null;
            this.textBox327.Top = 4.385777F;
            this.textBox327.Width = 0.1576389F;
            // 
            // textBox328
            // 
            this.textBox328.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox328.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox328.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox328.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox328.Height = 0.1576389F;
            this.textBox328.Left = 8.148283F;
            this.textBox328.Name = "textBox328";
            this.textBox328.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: left; ddo-char-set: 1";
            this.textBox328.Tag = "";
            this.textBox328.Text = null;
            this.textBox328.Top = 4.385777F;
            this.textBox328.Width = 0.1576389F;
            // 
            // textBox329
            // 
            this.textBox329.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox329.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox329.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox329.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox329.Height = 0.1576389F;
            this.textBox329.Left = 7.990645F;
            this.textBox329.Name = "textBox329";
            this.textBox329.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: left; ddo-char-set: 1";
            this.textBox329.Tag = "";
            this.textBox329.Text = null;
            this.textBox329.Top = 4.385777F;
            this.textBox329.Width = 0.1576389F;
            // 
            // textBox330
            // 
            this.textBox330.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox330.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox330.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox330.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox330.Height = 0.1576389F;
            this.textBox330.Left = 7.833068F;
            this.textBox330.Name = "textBox330";
            this.textBox330.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: left; ddo-char-set: 1";
            this.textBox330.Tag = "";
            this.textBox330.Text = null;
            this.textBox330.Top = 4.385777F;
            this.textBox330.Width = 0.1576389F;
            // 
            // textBox331
            // 
            this.textBox331.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox331.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox331.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox331.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox331.Height = 0.1576389F;
            this.textBox331.Left = 5.941339F;
            this.textBox331.Name = "textBox331";
            this.textBox331.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: left; ddo-char-set: 1";
            this.textBox331.Tag = "";
            this.textBox331.Text = null;
            this.textBox331.Top = 4.385777F;
            this.textBox331.Width = 0.1576389F;
            // 
            // textBox332
            // 
            this.textBox332.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox332.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox332.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox332.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox332.Height = 0.1576389F;
            this.textBox332.Left = 6.098978F;
            this.textBox332.Name = "textBox332";
            this.textBox332.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: left; ddo-char-set: 1";
            this.textBox332.Tag = "";
            this.textBox332.Text = null;
            this.textBox332.Top = 4.385777F;
            this.textBox332.Width = 0.3152778F;
            // 
            // textBox333
            // 
            this.textBox333.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox333.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox333.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox333.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox333.Height = 0.1576389F;
            this.textBox333.Left = 6.414256F;
            this.textBox333.Name = "textBox333";
            this.textBox333.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: left; ddo-char-set: 1";
            this.textBox333.Tag = "";
            this.textBox333.Text = null;
            this.textBox333.Top = 4.385777F;
            this.textBox333.Width = 0.1576389F;
            // 
            // textBox334
            // 
            this.textBox334.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox334.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox334.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox334.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox334.Height = 0.1576389F;
            this.textBox334.Left = 6.571895F;
            this.textBox334.Name = "textBox334";
            this.textBox334.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: left; ddo-char-set: 1";
            this.textBox334.Tag = "";
            this.textBox334.Text = null;
            this.textBox334.Top = 4.385777F;
            this.textBox334.Width = 0.1576389F;
            // 
            // textBox335
            // 
            this.textBox335.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox335.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox335.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox335.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox335.Height = 0.1576389F;
            this.textBox335.Left = 6.729534F;
            this.textBox335.Name = "textBox335";
            this.textBox335.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: left; ddo-char-set: 1";
            this.textBox335.Tag = "";
            this.textBox335.Text = null;
            this.textBox335.Top = 4.385777F;
            this.textBox335.Width = 0.1576389F;
            // 
            // textBox336
            // 
            this.textBox336.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox336.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox336.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox336.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox336.Height = 0.1576389F;
            this.textBox336.Left = 6.887174F;
            this.textBox336.Name = "textBox336";
            this.textBox336.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: left; ddo-char-set: 1";
            this.textBox336.Tag = "";
            this.textBox336.Text = null;
            this.textBox336.Top = 4.385777F;
            this.textBox336.Width = 0.1576389F;
            // 
            // textBox337
            // 
            this.textBox337.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox337.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox337.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox337.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox337.Height = 0.1576389F;
            this.textBox337.Left = 7.044811F;
            this.textBox337.Name = "textBox337";
            this.textBox337.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: left; ddo-char-set: 1";
            this.textBox337.Tag = "";
            this.textBox337.Text = null;
            this.textBox337.Top = 4.385777F;
            this.textBox337.Width = 0.1576389F;
            // 
            // textBox338
            // 
            this.textBox338.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox338.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox338.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox338.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox338.Height = 0.1576389F;
            this.textBox338.Left = 7.20245F;
            this.textBox338.Name = "textBox338";
            this.textBox338.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: left; ddo-char-set: 1";
            this.textBox338.Tag = "";
            this.textBox338.Text = null;
            this.textBox338.Top = 4.385777F;
            this.textBox338.Width = 0.1576389F;
            // 
            // textBox339
            // 
            this.textBox339.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox339.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox339.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox339.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox339.Height = 0.1576389F;
            this.textBox339.Left = 7.360089F;
            this.textBox339.Name = "textBox339";
            this.textBox339.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: left; ddo-char-set: 1";
            this.textBox339.Tag = "";
            this.textBox339.Text = null;
            this.textBox339.Top = 4.385777F;
            this.textBox339.Width = 0.1576389F;
            // 
            // textBox340
            // 
            this.textBox340.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox340.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox340.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox340.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox340.Height = 0.1576389F;
            this.textBox340.Left = 7.517728F;
            this.textBox340.Name = "textBox340";
            this.textBox340.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: left; ddo-char-set: 1";
            this.textBox340.Tag = "";
            this.textBox340.Text = null;
            this.textBox340.Top = 4.385777F;
            this.textBox340.Width = 0.1576389F;
            // 
            // textBox341
            // 
            this.textBox341.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox341.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox341.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox341.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox341.Height = 0.1576389F;
            this.textBox341.Left = 7.675374F;
            this.textBox341.Name = "textBox341";
            this.textBox341.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: left; ddo-char-set: 1";
            this.textBox341.Tag = "";
            this.textBox341.Text = null;
            this.textBox341.Top = 4.385777F;
            this.textBox341.Width = 0.1576389F;
            // 
            // textBox342
            // 
            this.textBox342.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox342.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox342.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox342.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox342.Height = 0.1180556F;
            this.textBox342.Left = 5.941339F;
            this.textBox342.Name = "textBox342";
            this.textBox342.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: bold; text-align: left; ddo-char-set: 1";
            this.textBox342.Tag = "";
            this.textBox342.Text = "※";
            this.textBox342.Top = 4.267722F;
            this.textBox342.Width = 2.677778F;
            // 
            // textBox343
            // 
            this.textBox343.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox343.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox343.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox343.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox343.Height = 0.1576389F;
            this.textBox343.Left = 8.619108F;
            this.textBox343.Name = "textBox343";
            this.textBox343.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: left; ddo-char-set: 1";
            this.textBox343.Tag = "";
            this.textBox343.Text = null;
            this.textBox343.Top = 4.385777F;
            this.textBox343.Width = 0.7090278F;
            // 
            // textBox344
            // 
            this.textBox344.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox344.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox344.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox344.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox344.Height = 0.1180556F;
            this.textBox344.Left = 8.619108F;
            this.textBox344.Name = "textBox344";
            this.textBox344.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: bold; text-align: left; ddo-char-set: 1";
            this.textBox344.Tag = "";
            this.textBox344.Text = "※  種 別";
            this.textBox344.Top = 4.267722F;
            this.textBox344.Width = 0.7090278F;
            // 
            // textBox345
            // 
            this.textBox345.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox345.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox345.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox345.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox345.Height = 0.1576389F;
            this.textBox345.Left = 9.328136F;
            this.textBox345.Name = "textBox345";
            this.textBox345.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: left; ddo-char-set: 1";
            this.textBox345.Tag = "";
            this.textBox345.Text = null;
            this.textBox345.Top = 4.385777F;
            this.textBox345.Width = 0.8659722F;
            // 
            // textBox346
            // 
            this.textBox346.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox346.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox346.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox346.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox346.Height = 0.1180556F;
            this.textBox346.Left = 9.328136F;
            this.textBox346.Name = "textBox346";
            this.textBox346.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: bold; text-align: left; ddo-char-set: 1";
            this.textBox346.Tag = "";
            this.textBox346.Text = "※  整 理 番 号";
            this.textBox346.Top = 4.267722F;
            this.textBox346.Width = 0.8659722F;
            // 
            // textBox347
            // 
            this.textBox347.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox347.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox347.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox347.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox347.Height = 0.1576389F;
            this.textBox347.Left = 10.19412F;
            this.textBox347.Name = "textBox347";
            this.textBox347.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: bold; text-align: left; ddo-char-set: 1";
            this.textBox347.Tag = "";
            this.textBox347.Text = null;
            this.textBox347.Top = 4.385777F;
            this.textBox347.Width = 0.8659722F;
            // 
            // textBox348
            // 
            this.textBox348.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox348.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox348.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox348.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox348.Height = 0.1180556F;
            this.textBox348.Left = 10.19412F;
            this.textBox348.Name = "textBox348";
            this.textBox348.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: bold; text-align: left; ddo-char-set: 1";
            this.textBox348.Tag = "";
            this.textBox348.Text = "※";
            this.textBox348.Top = 4.267722F;
            this.textBox348.Width = 0.8659722F;
            // 
            // textBox349
            // 
            this.textBox349.DataField = "ITEM012";
            this.textBox349.Height = 0.7618055F;
            this.textBox349.Left = 5.765645F;
            this.textBox349.Name = "textBox349";
            this.textBox349.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.textBox349.Tag = "";
            this.textBox349.Text = "ITEM012";
            this.textBox349.Top = 4.298972F;
            this.textBox349.Width = 0.1604167F;
            // 
            // textBox350
            // 
            this.textBox350.Height = 2.126389F;
            this.textBox350.Left = 5.724016F;
            this.textBox350.Name = "textBox350";
            this.textBox350.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: bold; text-align: left; ddo-char-set: 128";
            this.textBox350.Tag = "";
            this.textBox350.Text = "給与支払報告書\r\n　個人別明細書";
            this.textBox350.Top = 5.08032F;
            this.textBox350.Width = 0.1980317F;
            // 
            // label53
            // 
            this.label53.Height = 0.7174979F;
            this.label53.HyperLink = null;
            this.label53.Left = 5.776062F;
            this.label53.Name = "label53";
            this.label53.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: bold; text-align: " +
    "left; ddo-char-set: 1";
            this.label53.Tag = "";
            this.label53.Text = "市区町村提出用";
            this.label53.Top = 7.257306F;
            this.label53.Width = 0.1715278F;
            // 
            // textBox351
            // 
            this.textBox351.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox351.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox351.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox351.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox351.DataField = "ITEM045";
            this.textBox351.Height = 0.3541667F;
            this.textBox351.Left = 8.696907F;
            this.textBox351.Name = "textBox351";
            this.textBox351.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: right; vertical-align: bottom; ddo-char-set: 1";
            this.textBox351.Tag = "";
            this.textBox351.Text = "ITEM045";
            this.textBox351.Top = 6.077679F;
            this.textBox351.Width = 0.7090278F;
            // 
            // label54
            // 
            this.label54.Height = 0.08611111F;
            this.label54.HyperLink = null;
            this.label54.Left = 8.722588F;
            this.label54.Name = "label54";
            this.label54.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.label54.Tag = "";
            this.label54.Text = "内　　　　　　　円";
            this.label54.Top = 6.089485F;
            this.label54.Width = 0.6785923F;
            // 
            // textBox352
            // 
            this.textBox352.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox352.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox352.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox352.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox352.DataField = "ITEM068";
            this.textBox352.Height = 0.1576389F;
            this.textBox352.Left = 5.941339F;
            this.textBox352.Name = "textBox352";
            this.textBox352.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox352.Tag = "";
            this.textBox352.Text = "ITEM068";
            this.textBox352.Top = 7.416571F;
            this.textBox352.Width = 0.2131944F;
            // 
            // textBox353
            // 
            this.textBox353.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox353.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox353.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox353.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox353.DataField = "ITEM069";
            this.textBox353.Height = 0.1576389F;
            this.textBox353.Left = 6.154534F;
            this.textBox353.Name = "textBox353";
            this.textBox353.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox353.Tag = "";
            this.textBox353.Text = "ITEM069";
            this.textBox353.Top = 7.416571F;
            this.textBox353.Width = 0.2131944F;
            // 
            // textBox354
            // 
            this.textBox354.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox354.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox354.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox354.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox354.DataField = "ITEM070";
            this.textBox354.Height = 0.1576389F;
            this.textBox354.Left = 6.367728F;
            this.textBox354.Name = "textBox354";
            this.textBox354.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox354.Tag = "";
            this.textBox354.Text = "ITEM070";
            this.textBox354.Top = 7.416571F;
            this.textBox354.Width = 0.2131944F;
            // 
            // textBox355
            // 
            this.textBox355.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox355.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox355.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox355.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox355.DataField = "ITEM071";
            this.textBox355.Height = 0.1576389F;
            this.textBox355.Left = 6.580922F;
            this.textBox355.Name = "textBox355";
            this.textBox355.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox355.Tag = "";
            this.textBox355.Text = "ITEM071";
            this.textBox355.Top = 7.416571F;
            this.textBox355.Width = 0.2131944F;
            // 
            // textBox356
            // 
            this.textBox356.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox356.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox356.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox356.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox356.DataField = "ITEM072";
            this.textBox356.Height = 0.1576389F;
            this.textBox356.Left = 6.794118F;
            this.textBox356.Name = "textBox356";
            this.textBox356.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox356.Tag = "";
            this.textBox356.Text = "ITEM072";
            this.textBox356.Top = 7.416571F;
            this.textBox356.Width = 0.2131944F;
            // 
            // textBox357
            // 
            this.textBox357.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox357.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox357.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox357.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox357.DataField = "ITEM073";
            this.textBox357.Height = 0.1576389F;
            this.textBox357.Left = 7.007311F;
            this.textBox357.Name = "textBox357";
            this.textBox357.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox357.Tag = "";
            this.textBox357.Text = "ITEM073";
            this.textBox357.Top = 7.416571F;
            this.textBox357.Width = 0.2131944F;
            // 
            // textBox358
            // 
            this.textBox358.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox358.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox358.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox358.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox358.DataField = "ITEM074";
            this.textBox358.Height = 0.1576389F;
            this.textBox358.Left = 7.220505F;
            this.textBox358.Name = "textBox358";
            this.textBox358.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox358.Tag = "";
            this.textBox358.Text = "ITEM074";
            this.textBox358.Top = 7.416571F;
            this.textBox358.Width = 0.2131944F;
            // 
            // textBox359
            // 
            this.textBox359.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox359.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox359.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox359.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox359.DataField = "ITEM075";
            this.textBox359.Height = 0.1576389F;
            this.textBox359.Left = 7.4337F;
            this.textBox359.Name = "textBox359";
            this.textBox359.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox359.Tag = "";
            this.textBox359.Text = "ITEM075";
            this.textBox359.Top = 7.416571F;
            this.textBox359.Width = 0.2131944F;
            // 
            // textBox360
            // 
            this.textBox360.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox360.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox360.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox360.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox360.DataField = "ITEM076";
            this.textBox360.Height = 0.1576389F;
            this.textBox360.Left = 7.646895F;
            this.textBox360.Name = "textBox360";
            this.textBox360.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox360.Tag = "";
            this.textBox360.Text = "ITEM076";
            this.textBox360.Top = 7.416571F;
            this.textBox360.Width = 0.2131944F;
            // 
            // textBox361
            // 
            this.textBox361.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox361.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox361.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox361.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox361.DataField = "ITEM077";
            this.textBox361.Height = 0.1576389F;
            this.textBox361.Left = 7.860089F;
            this.textBox361.Name = "textBox361";
            this.textBox361.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox361.Tag = "";
            this.textBox361.Text = "ITEM077";
            this.textBox361.Top = 7.416571F;
            this.textBox361.Width = 0.2131944F;
            // 
            // textBox362
            // 
            this.textBox362.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox362.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox362.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox362.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox362.DataField = "ITEM078";
            this.textBox362.Height = 0.1576389F;
            this.textBox362.Left = 8.073277F;
            this.textBox362.Name = "textBox362";
            this.textBox362.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox362.Tag = "";
            this.textBox362.Text = "ITEM078";
            this.textBox362.Top = 7.416571F;
            this.textBox362.Width = 0.2131944F;
            // 
            // textBox363
            // 
            this.textBox363.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox363.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox363.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox363.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox363.DataField = "ITEM079";
            this.textBox363.Height = 0.1576389F;
            this.textBox363.Left = 8.286476F;
            this.textBox363.Name = "textBox363";
            this.textBox363.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox363.Tag = "";
            this.textBox363.Text = "ITEM079";
            this.textBox363.Top = 7.416571F;
            this.textBox363.Width = 0.2131944F;
            // 
            // textBox364
            // 
            this.textBox364.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox364.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox364.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox364.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox364.DataField = "ITEM081";
            this.textBox364.Height = 0.2756944F;
            this.textBox364.Left = 8.499678F;
            this.textBox364.Name = "textBox364";
            this.textBox364.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox364.Tag = "";
            this.textBox364.Text = "ITEM081";
            this.textBox364.Top = 7.298513F;
            this.textBox364.Width = 0.2131944F;
            // 
            // textBox365
            // 
            this.textBox365.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox365.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox365.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox365.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox365.DataField = "ITEM085";
            this.textBox365.Height = 0.2756944F;
            this.textBox365.Left = 8.712873F;
            this.textBox365.Name = "textBox365";
            this.textBox365.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox365.Tag = "";
            this.textBox365.Text = "ITEM085";
            this.textBox365.Top = 7.298513F;
            this.textBox365.Width = 0.2131944F;
            // 
            // textBox366
            // 
            this.textBox366.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox366.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox366.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox366.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox366.DataField = "ITEM086";
            this.textBox366.Height = 0.2756944F;
            this.textBox366.Left = 8.92606F;
            this.textBox366.Name = "textBox366";
            this.textBox366.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox366.Tag = "";
            this.textBox366.Text = "ITEM086";
            this.textBox366.Top = 7.298513F;
            this.textBox366.Width = 0.2131944F;
            // 
            // textBox367
            // 
            this.textBox367.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox367.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox367.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox367.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox367.DataField = "ITEM087";
            this.textBox367.Height = 0.2756944F;
            this.textBox367.Left = 9.139256F;
            this.textBox367.Name = "textBox367";
            this.textBox367.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox367.Tag = "";
            this.textBox367.Text = "ITEM087";
            this.textBox367.Top = 7.298513F;
            this.textBox367.Width = 0.2131944F;
            // 
            // textBox368
            // 
            this.textBox368.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox368.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox368.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox368.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox368.DataField = "ITEM088";
            this.textBox368.Height = 0.2756944F;
            this.textBox368.Left = 9.352455F;
            this.textBox368.Name = "textBox368";
            this.textBox368.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox368.Tag = "";
            this.textBox368.Text = "ITEM088";
            this.textBox368.Top = 7.298513F;
            this.textBox368.Width = 0.2131944F;
            // 
            // textBox369
            // 
            this.textBox369.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox369.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox369.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox369.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox369.Height = 0.1972222F;
            this.textBox369.Left = 9.091339F;
            this.textBox369.Name = "textBox369";
            this.textBox369.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.textBox369.Tag = "";
            this.textBox369.Text = "(受給者番号)";
            this.textBox369.Top = 4.537402F;
            this.textBox369.Width = 1.96875F;
            // 
            // textBox370
            // 
            this.textBox370.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox370.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox370.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox370.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox370.Height = 0.3583333F;
            this.textBox370.Left = 9.091339F;
            this.textBox370.Name = "textBox370";
            this.textBox370.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.textBox370.Tag = "";
            this.textBox370.Text = "(役職名)";
            this.textBox370.Top = 4.931152F;
            this.textBox370.Width = 1.96875F;
            // 
            // textBox371
            // 
            this.textBox371.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox371.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox371.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox371.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox371.Height = 0.1972222F;
            this.textBox371.Left = 9.091339F;
            this.textBox371.Name = "textBox371";
            this.textBox371.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.textBox371.Tag = "";
            this.textBox371.Text = "(フリガナ)";
            this.textBox371.Top = 4.734624F;
            this.textBox371.Width = 1.96875F;
            // 
            // textBox372
            // 
            this.textBox372.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox372.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox372.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox372.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox372.DataField = "ITEM030";
            this.textBox372.Height = 0.3152778F;
            this.textBox372.Left = 6.807311F;
            this.textBox372.Name = "textBox372";
            this.textBox372.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; vertical-align: bottom; ddo-char-set: 1";
            this.textBox372.Tag = "";
            this.textBox372.Text = "ITEM030";
            this.textBox372.Top = 5.447124F;
            this.textBox372.Width = 1.063194F;
            // 
            // textBox373
            // 
            this.textBox373.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox373.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox373.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox373.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox373.Height = 0.1972222F;
            this.textBox373.Left = 7.122595F;
            this.textBox373.Name = "textBox373";
            this.textBox373.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox373.Tag = "";
            this.textBox373.Text = "控除対象扶養親族の数\r\n(配偶者を除く)";
            this.textBox373.Top = 5.762402F;
            this.textBox373.Width = 1.023611F;
            // 
            // textBox374
            // 
            this.textBox374.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox374.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox374.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox374.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox374.Height = 0.1180556F;
            this.textBox374.Left = 8.499678F;
            this.textBox374.Name = "textBox374";
            this.textBox374.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox374.Tag = "";
            this.textBox374.Text = "中 途 就・退 職";
            this.textBox374.Top = 7.062403F;
            this.textBox374.Width = 1.067361F;
            // 
            // textBox375
            // 
            this.textBox375.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox375.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox375.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox375.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox375.Height = 0.1180556F;
            this.textBox375.Left = 8.499678F;
            this.textBox375.Name = "textBox375";
            this.textBox375.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox375.Tag = "";
            this.textBox375.Text = "就職";
            this.textBox375.Top = 7.180457F;
            this.textBox375.Width = 0.2131944F;
            // 
            // textBox376
            // 
            this.textBox376.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox376.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox376.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox376.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox376.DataField = "ITEM096";
            this.textBox376.Height = 0.1972222F;
            this.textBox376.Left = 6.792033F;
            this.textBox376.Name = "textBox376";
            this.textBox376.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: left; ddo-char-set: 1";
            this.textBox376.Tag = "";
            this.textBox376.Text = "ITEM096";
            this.textBox376.Top = 7.574209F;
            this.textBox376.Width = 4.26875F;
            // 
            // textBox377
            // 
            this.textBox377.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox377.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox377.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox377.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox377.CanGrow = false;
            this.textBox377.DataField = "ITEM097";
            this.textBox377.Height = 0.1972222F;
            this.textBox377.Left = 6.792033F;
            this.textBox377.Name = "textBox377";
            this.textBox377.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: left; ddo-char-set: 1";
            this.textBox377.Tag = "";
            this.textBox377.Text = "ITEM097";
            this.textBox377.Top = 7.771432F;
            this.textBox377.Width = 4.26875F;
            // 
            // textBox378
            // 
            this.textBox378.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox378.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox378.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox378.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox378.DataField = "ITEM022";
            this.textBox378.Height = 0.1576389F;
            this.textBox378.Left = 6.453145F;
            this.textBox378.Name = "textBox378";
            this.textBox378.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: left; ddo-char-set: 1";
            this.textBox378.Tag = "";
            this.textBox378.Text = "ITEM022";
            this.textBox378.Top = 4.738791F;
            this.textBox378.Width = 2.480556F;
            // 
            // textBox379
            // 
            this.textBox379.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox379.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox379.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox379.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox379.DataField = "ITEM023";
            this.textBox379.Height = 0.1972222F;
            this.textBox379.Left = 6.453145F;
            this.textBox379.Name = "textBox379";
            this.textBox379.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: left; ddo-char-set: 1";
            this.textBox379.Tag = "";
            this.textBox379.Text = "ITEM023";
            this.textBox379.Top = 4.89643F;
            this.textBox379.Width = 2.480556F;
            // 
            // textBox380
            // 
            this.textBox380.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox380.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox380.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox380.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox380.DataField = "ITEM024";
            this.textBox380.Height = 0.1972222F;
            this.textBox380.Left = 6.453145F;
            this.textBox380.Name = "textBox380";
            this.textBox380.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: left; ddo-char-set: 1";
            this.textBox380.Tag = "";
            this.textBox380.Text = "ITEM024";
            this.textBox380.Top = 5.093652F;
            this.textBox380.Width = 2.480556F;
            // 
            // textBox381
            // 
            this.textBox381.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox381.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox381.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox381.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox381.Height = 0.1576389F;
            this.textBox381.Left = 5.941339F;
            this.textBox381.Name = "textBox381";
            this.textBox381.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox381.Tag = "";
            this.textBox381.Text = "種　　　別";
            this.textBox381.Top = 5.289485F;
            this.textBox381.Width = 0.8659722F;
            // 
            // textBox382
            // 
            this.textBox382.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox382.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox382.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox382.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox382.DataField = "ITEM029";
            this.textBox382.Height = 0.3152778F;
            this.textBox382.Left = 5.941339F;
            this.textBox382.Name = "textBox382";
            this.textBox382.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: left; vertical-align: bottom; ddo-char-set: 1";
            this.textBox382.Tag = "";
            this.textBox382.Text = "ITEM029";
            this.textBox382.Top = 5.447124F;
            this.textBox382.Width = 0.8659722F;
            // 
            // textBox383
            // 
            this.textBox383.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox383.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox383.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox383.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox383.Height = 0.3153544F;
            this.textBox383.Left = 5.941339F;
            this.textBox383.Name = "textBox383";
            this.textBox383.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: left; vertical-align: middle; ddo-char-set: 1";
            this.textBox383.Tag = "";
            this.textBox383.Text = "控除対象配偶\r\n\r\n者の有無等";
            this.textBox383.Top = 5.762205F;
            this.textBox383.Width = 0.5909723F;
            // 
            // textBox384
            // 
            this.textBox384.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox384.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox384.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox384.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox384.Height = 0.1576389F;
            this.textBox384.Left = 5.941339F;
            this.textBox384.Name = "textBox384";
            this.textBox384.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox384.Tag = "";
            this.textBox384.Text = "C有";
            this.textBox384.Top = 6.077679F;
            this.textBox384.Width = 0.1180556F;
            // 
            // textBox385
            // 
            this.textBox385.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox385.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox385.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox385.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox385.Height = 0.1576389F;
            this.textBox385.Left = 6.059395F;
            this.textBox385.Name = "textBox385";
            this.textBox385.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox385.Tag = "";
            this.textBox385.Text = "D無";
            this.textBox385.Top = 6.077679F;
            this.textBox385.Width = 0.1180556F;
            // 
            // textBox386
            // 
            this.textBox386.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox386.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox386.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox386.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox386.Height = 0.1576389F;
            this.textBox386.Left = 6.17745F;
            this.textBox386.Name = "textBox386";
            this.textBox386.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox386.Tag = "";
            this.textBox386.Text = "従有";
            this.textBox386.Top = 6.077679F;
            this.textBox386.Width = 0.1180556F;
            // 
            // textBox387
            // 
            this.textBox387.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox387.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox387.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox387.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox387.Height = 0.1576389F;
            this.textBox387.Left = 6.295506F;
            this.textBox387.Name = "textBox387";
            this.textBox387.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox387.Tag = "";
            this.textBox387.Text = "従無";
            this.textBox387.Top = 6.077679F;
            this.textBox387.Width = 0.1180556F;
            // 
            // textBox388
            // 
            this.textBox388.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox388.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox388.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox388.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox388.CanGrow = false;
            this.textBox388.Height = 0.1576389F;
            this.textBox388.Left = 6.414256F;
            this.textBox388.Name = "textBox388";
            this.textBox388.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox388.Tag = "";
            this.textBox388.Text = "老人";
            this.textBox388.Top = 5.920041F;
            this.textBox388.Width = 0.1180556F;
            // 
            // textBox389
            // 
            this.textBox389.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox389.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox389.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox389.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox389.DataField = "ITEM034";
            this.textBox389.Height = 0.1972222F;
            this.textBox389.Left = 5.941339F;
            this.textBox389.Name = "textBox389";
            this.textBox389.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox389.Tag = "";
            this.textBox389.Text = "ITEM034";
            this.textBox389.Top = 6.235318F;
            this.textBox389.Width = 0.1180556F;
            // 
            // textBox390
            // 
            this.textBox390.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox390.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox390.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox390.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox390.DataField = "ITEM035";
            this.textBox390.Height = 0.1972222F;
            this.textBox390.Left = 6.059395F;
            this.textBox390.Name = "textBox390";
            this.textBox390.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox390.Tag = "";
            this.textBox390.Text = "ITEM035";
            this.textBox390.Top = 6.235318F;
            this.textBox390.Width = 0.1180556F;
            // 
            // textBox391
            // 
            this.textBox391.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox391.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox391.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox391.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox391.Height = 0.1972222F;
            this.textBox391.Left = 6.17745F;
            this.textBox391.Name = "textBox391";
            this.textBox391.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: bold; text-align: center; ddo-char-set: 1";
            this.textBox391.Tag = "";
            this.textBox391.Text = null;
            this.textBox391.Top = 6.235318F;
            this.textBox391.Width = 0.1180556F;
            // 
            // textBox392
            // 
            this.textBox392.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox392.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox392.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox392.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox392.Height = 0.1972222F;
            this.textBox392.Left = 6.295506F;
            this.textBox392.Name = "textBox392";
            this.textBox392.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: bold; text-align: center; ddo-char-set: 1";
            this.textBox392.Tag = "";
            this.textBox392.Text = null;
            this.textBox392.Top = 6.235318F;
            this.textBox392.Width = 0.1180556F;
            // 
            // textBox393
            // 
            this.textBox393.DataField = "ITEM036";
            this.textBox393.Height = 0.1972222F;
            this.textBox393.Left = 6.413561F;
            this.textBox393.Name = "textBox393";
            this.textBox393.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox393.Tag = "";
            this.textBox393.Text = "ITEM036";
            this.textBox393.Top = 6.235318F;
            this.textBox393.Width = 0.1180556F;
            // 
            // textBox394
            // 
            this.textBox394.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox394.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox394.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox394.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox394.Height = 0.3152778F;
            this.textBox394.Left = 6.532311F;
            this.textBox394.Name = "textBox394";
            this.textBox394.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox394.Tag = "";
            this.textBox394.Text = "配偶者特別控除の額";
            this.textBox394.Top = 5.762402F;
            this.textBox394.Width = 0.5909723F;
            // 
            // textBox395
            // 
            this.textBox395.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox395.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox395.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox395.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox395.DataField = "ITEM037";
            this.textBox395.Height = 0.3541667F;
            this.textBox395.Left = 6.532311F;
            this.textBox395.Name = "textBox395";
            this.textBox395.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox395.Tag = "";
            this.textBox395.Text = "ITEM037";
            this.textBox395.Top = 6.077679F;
            this.textBox395.Width = 0.5909723F;
            // 
            // textBox396
            // 
            this.textBox396.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox396.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox396.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox396.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox396.Height = 0.6298611F;
            this.textBox396.Left = 5.941339F;
            this.textBox396.Name = "textBox396";
            this.textBox396.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.textBox396.Tag = "";
            this.textBox396.Text = "(摘要)";
            this.textBox396.Top = 6.432541F;
            this.textBox396.Width = 5.11875F;
            // 
            // textBox397
            // 
            this.textBox397.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox397.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox397.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox397.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox397.CanGrow = false;
            this.textBox397.Height = 0.3541667F;
            this.textBox397.Left = 5.941339F;
            this.textBox397.Name = "textBox397";
            this.textBox397.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5.5pt; font" +
    "-weight: normal; text-align: center; ddo-char-set: 1";
            this.textBox397.Tag = "";
            this.textBox397.Text = "扶16養歳親未族満";
            this.textBox397.Top = 7.062403F;
            this.textBox397.Width = 0.2131944F;
            // 
            // textBox398
            // 
            this.textBox398.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox398.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox398.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox398.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox398.Height = 0.3541667F;
            this.textBox398.Left = 6.154534F;
            this.textBox398.Name = "textBox398";
            this.textBox398.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.textBox398.Tag = "";
            this.textBox398.Text = "未\r\n成\r\n年\r\n";
            this.textBox398.Top = 7.062403F;
            this.textBox398.Width = 0.2131944F;
            // 
            // textBox399
            // 
            this.textBox399.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox399.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox399.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox399.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox399.Height = 0.3541667F;
            this.textBox399.Left = 6.367728F;
            this.textBox399.Name = "textBox399";
            this.textBox399.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox399.Tag = "";
            this.textBox399.Text = "外\r\n国\r\n人";
            this.textBox399.Top = 7.062403F;
            this.textBox399.Width = 0.2131944F;
            // 
            // textBox400
            // 
            this.textBox400.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox400.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox400.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox400.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox400.Height = 0.3541667F;
            this.textBox400.Left = 6.580922F;
            this.textBox400.Name = "textBox400";
            this.textBox400.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox400.Tag = "";
            this.textBox400.Text = "死\r\n亡\r\n退\r\n";
            this.textBox400.Top = 7.062403F;
            this.textBox400.Width = 0.2131944F;
            // 
            // textBox401
            // 
            this.textBox401.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox401.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox401.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox401.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox401.Height = 0.3541667F;
            this.textBox401.Left = 6.794118F;
            this.textBox401.Name = "textBox401";
            this.textBox401.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox401.Tag = "";
            this.textBox401.Text = "災\r\n害\r\n者";
            this.textBox401.Top = 7.062403F;
            this.textBox401.Width = 0.2131944F;
            // 
            // textBox402
            // 
            this.textBox402.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox402.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox402.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox402.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox402.Height = 0.3541667F;
            this.textBox402.Left = 7.007311F;
            this.textBox402.Name = "textBox402";
            this.textBox402.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox402.Tag = "";
            this.textBox402.Text = "乙\r\n\r\n欄";
            this.textBox402.Top = 7.062403F;
            this.textBox402.Width = 0.2131944F;
            // 
            // textBox403
            // 
            this.textBox403.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox403.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox403.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox403.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox403.Height = 0.1180556F;
            this.textBox403.Left = 7.220505F;
            this.textBox403.Name = "textBox403";
            this.textBox403.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 4pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox403.Tag = "";
            this.textBox403.Text = "本人が障害者";
            this.textBox403.Top = 7.062403F;
            this.textBox403.Width = 0.4256943F;
            // 
            // textBox404
            // 
            this.textBox404.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox404.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox404.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox404.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox404.Height = 0.2361111F;
            this.textBox404.Left = 7.220505F;
            this.textBox404.Name = "textBox404";
            this.textBox404.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.textBox404.Tag = "";
            this.textBox404.Text = "特\r\n別";
            this.textBox404.Top = 7.180457F;
            this.textBox404.Width = 0.2131944F;
            // 
            // textBox405
            // 
            this.textBox405.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox405.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox405.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox405.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox405.CanGrow = false;
            this.textBox405.Height = 0.2361111F;
            this.textBox405.Left = 7.4337F;
            this.textBox405.Name = "textBox405";
            this.textBox405.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.textBox405.Tag = "";
            this.textBox405.Text = "そ\r\nの\r\n他";
            this.textBox405.Top = 7.180457F;
            this.textBox405.Width = 0.2131944F;
            // 
            // textBox406
            // 
            this.textBox406.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox406.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox406.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox406.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox406.Height = 0.1180556F;
            this.textBox406.Left = 7.646207F;
            this.textBox406.Name = "textBox406";
            this.textBox406.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox406.Tag = "";
            this.textBox406.Text = "寡　婦";
            this.textBox406.Top = 7.062403F;
            this.textBox406.Width = 0.4256943F;
            // 
            // textBox407
            // 
            this.textBox407.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox407.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox407.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox407.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox407.Height = 0.2361111F;
            this.textBox407.Left = 7.646895F;
            this.textBox407.Name = "textBox407";
            this.textBox407.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.textBox407.Tag = "";
            this.textBox407.Text = "一\r\n般";
            this.textBox407.Top = 7.180457F;
            this.textBox407.Width = 0.2131944F;
            // 
            // textBox408
            // 
            this.textBox408.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox408.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox408.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox408.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox408.Height = 0.2361111F;
            this.textBox408.Left = 7.860089F;
            this.textBox408.Name = "textBox408";
            this.textBox408.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.textBox408.Tag = "";
            this.textBox408.Text = "特\r\n別";
            this.textBox408.Top = 7.180457F;
            this.textBox408.Width = 0.2131944F;
            // 
            // textBox409
            // 
            this.textBox409.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox409.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox409.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox409.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox409.Height = 0.3541667F;
            this.textBox409.Left = 8.073235F;
            this.textBox409.Name = "textBox409";
            this.textBox409.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.textBox409.Tag = "";
            this.textBox409.Text = "寡　夫";
            this.textBox409.Top = 7.062205F;
            this.textBox409.Width = 0.2131944F;
            // 
            // textBox410
            // 
            this.textBox410.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox410.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox410.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox410.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox410.CanGrow = false;
            this.textBox410.Height = 0.3541667F;
            this.textBox410.Left = 8.286421F;
            this.textBox410.Name = "textBox410";
            this.textBox410.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1; ddo-font-vertical: true";
            this.textBox410.Tag = "";
            this.textBox410.Text = "勤\r\n労\r\n学\r\n生";
            this.textBox410.Top = 7.062205F;
            this.textBox410.Width = 0.2131944F;
            // 
            // textBox411
            // 
            this.textBox411.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox411.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox411.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox411.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox411.Height = 0.1180556F;
            this.textBox411.Left = 8.712873F;
            this.textBox411.Name = "textBox411";
            this.textBox411.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox411.Tag = "";
            this.textBox411.Text = "退職";
            this.textBox411.Top = 7.180457F;
            this.textBox411.Width = 0.2131944F;
            // 
            // textBox412
            // 
            this.textBox412.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox412.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox412.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox412.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox412.Height = 0.1180556F;
            this.textBox412.Left = 8.92606F;
            this.textBox412.Name = "textBox412";
            this.textBox412.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox412.Tag = "";
            this.textBox412.Text = "年";
            this.textBox412.Top = 7.180457F;
            this.textBox412.Width = 0.2131944F;
            // 
            // textBox413
            // 
            this.textBox413.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox413.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox413.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox413.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox413.Height = 0.1180556F;
            this.textBox413.Left = 9.139256F;
            this.textBox413.Name = "textBox413";
            this.textBox413.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox413.Tag = "";
            this.textBox413.Text = "月";
            this.textBox413.Top = 7.180457F;
            this.textBox413.Width = 0.2131944F;
            // 
            // textBox414
            // 
            this.textBox414.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox414.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox414.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox414.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox414.Height = 0.1180556F;
            this.textBox414.Left = 9.352455F;
            this.textBox414.Name = "textBox414";
            this.textBox414.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox414.Tag = "";
            this.textBox414.Text = "日";
            this.textBox414.Top = 7.180457F;
            this.textBox414.Width = 0.2131944F;
            // 
            // textBox415
            // 
            this.textBox415.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox415.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox415.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox415.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox415.DataField = "ITEM089";
            this.textBox415.Height = 0.2756944F;
            this.textBox415.Left = 9.565643F;
            this.textBox415.Name = "textBox415";
            this.textBox415.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox415.Tag = "";
            this.textBox415.Text = "ITEM089";
            this.textBox415.Top = 7.298513F;
            this.textBox415.Width = 0.2131944F;
            // 
            // textBox416
            // 
            this.textBox416.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox416.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox416.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox416.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox416.DataField = "ITEM090";
            this.textBox416.Height = 0.2756944F;
            this.textBox416.Left = 9.778839F;
            this.textBox416.Name = "textBox416";
            this.textBox416.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox416.Tag = "";
            this.textBox416.Text = "ITEM090";
            this.textBox416.Top = 7.298513F;
            this.textBox416.Width = 0.2131944F;
            // 
            // textBox417
            // 
            this.textBox417.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox417.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox417.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox417.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox417.DataField = "ITEM091";
            this.textBox417.Height = 0.2756944F;
            this.textBox417.Left = 9.992039F;
            this.textBox417.Name = "textBox417";
            this.textBox417.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox417.Tag = "";
            this.textBox417.Text = "ITEM091";
            this.textBox417.Top = 7.298513F;
            this.textBox417.Width = 0.2131944F;
            // 
            // textBox418
            // 
            this.textBox418.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox418.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox418.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox418.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox418.DataField = "ITEM092";
            this.textBox418.Height = 0.2756944F;
            this.textBox418.Left = 10.20523F;
            this.textBox418.Name = "textBox418";
            this.textBox418.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox418.Tag = "";
            this.textBox418.Text = "ITEM092";
            this.textBox418.Top = 7.298513F;
            this.textBox418.Width = 0.2131944F;
            // 
            // textBox419
            // 
            this.textBox419.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox419.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox419.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox419.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox419.DataField = "ITEM093";
            this.textBox419.Height = 0.2756944F;
            this.textBox419.Left = 10.41842F;
            this.textBox419.Name = "textBox419";
            this.textBox419.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox419.Tag = "";
            this.textBox419.Text = "ITEM093";
            this.textBox419.Top = 7.298513F;
            this.textBox419.Width = 0.2131944F;
            // 
            // textBox420
            // 
            this.textBox420.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox420.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox420.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox420.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox420.Height = 0.1180556F;
            this.textBox420.Left = 9.565643F;
            this.textBox420.Name = "textBox420";
            this.textBox420.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox420.Tag = "";
            this.textBox420.Text = "受　給　者　生　年　月　日";
            this.textBox420.Top = 7.062403F;
            this.textBox420.Width = 1.493056F;
            // 
            // textBox421
            // 
            this.textBox421.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox421.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox421.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox421.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox421.Height = 0.1180556F;
            this.textBox421.Left = 9.565643F;
            this.textBox421.Name = "textBox421";
            this.textBox421.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox421.Tag = "";
            this.textBox421.Text = "明";
            this.textBox421.Top = 7.180457F;
            this.textBox421.Width = 0.2131944F;
            // 
            // textBox422
            // 
            this.textBox422.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox422.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox422.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox422.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox422.Height = 0.1180556F;
            this.textBox422.Left = 9.778839F;
            this.textBox422.Name = "textBox422";
            this.textBox422.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox422.Tag = "";
            this.textBox422.Text = "大";
            this.textBox422.Top = 7.180457F;
            this.textBox422.Width = 0.2131944F;
            // 
            // textBox423
            // 
            this.textBox423.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox423.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox423.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox423.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox423.Height = 0.1180556F;
            this.textBox423.Left = 9.992039F;
            this.textBox423.Name = "textBox423";
            this.textBox423.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox423.Tag = "";
            this.textBox423.Text = "昭";
            this.textBox423.Top = 7.180457F;
            this.textBox423.Width = 0.2131944F;
            // 
            // textBox424
            // 
            this.textBox424.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox424.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox424.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox424.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox424.Height = 0.1180556F;
            this.textBox424.Left = 10.20523F;
            this.textBox424.Name = "textBox424";
            this.textBox424.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox424.Tag = "";
            this.textBox424.Text = "平";
            this.textBox424.Top = 7.180457F;
            this.textBox424.Width = 0.2131944F;
            // 
            // textBox425
            // 
            this.textBox425.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox425.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox425.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox425.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox425.Height = 0.1180556F;
            this.textBox425.Left = 10.41842F;
            this.textBox425.Name = "textBox425";
            this.textBox425.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox425.Tag = "";
            this.textBox425.Text = "年";
            this.textBox425.Top = 7.180457F;
            this.textBox425.Width = 0.2131944F;
            // 
            // textBox426
            // 
            this.textBox426.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox426.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox426.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox426.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox426.DataField = "ITEM094";
            this.textBox426.Height = 0.2756944F;
            this.textBox426.Left = 10.63162F;
            this.textBox426.Name = "textBox426";
            this.textBox426.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox426.Tag = "";
            this.textBox426.Text = "ITEM094";
            this.textBox426.Top = 7.298513F;
            this.textBox426.Width = 0.2131944F;
            // 
            // textBox427
            // 
            this.textBox427.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox427.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox427.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox427.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox427.Height = 0.1180556F;
            this.textBox427.Left = 10.63162F;
            this.textBox427.Name = "textBox427";
            this.textBox427.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox427.Tag = "";
            this.textBox427.Text = "月";
            this.textBox427.Top = 7.180457F;
            this.textBox427.Width = 0.2131944F;
            // 
            // textBox428
            // 
            this.textBox428.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox428.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox428.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox428.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox428.DataField = "ITEM095";
            this.textBox428.Height = 0.2756944F;
            this.textBox428.Left = 10.84482F;
            this.textBox428.Name = "textBox428";
            this.textBox428.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox428.Tag = "";
            this.textBox428.Text = "ITEM095";
            this.textBox428.Top = 7.298513F;
            this.textBox428.Width = 0.2138889F;
            // 
            // textBox429
            // 
            this.textBox429.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox429.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox429.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox429.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox429.Height = 0.1180556F;
            this.textBox429.Left = 10.84482F;
            this.textBox429.Name = "textBox429";
            this.textBox429.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox429.Tag = "";
            this.textBox429.Text = "日";
            this.textBox429.Top = 7.180457F;
            this.textBox429.Width = 0.2138889F;
            // 
            // textBox430
            // 
            this.textBox430.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox430.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox430.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox430.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox430.Height = 0.1576389F;
            this.textBox430.Left = 6.807311F;
            this.textBox430.Name = "textBox430";
            this.textBox430.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox430.Tag = "";
            this.textBox430.Text = "支　払　金　額";
            this.textBox430.Top = 5.289485F;
            this.textBox430.Width = 1.063194F;
            // 
            // textBox431
            // 
            this.textBox431.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox431.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox431.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox431.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox431.DataField = "ITEM031";
            this.textBox431.Height = 0.3152778F;
            this.textBox431.Left = 7.870506F;
            this.textBox431.Name = "textBox431";
            this.textBox431.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; vertical-align: bottom; ddo-char-set: 1";
            this.textBox431.Tag = "";
            this.textBox431.Text = "ITEM031";
            this.textBox431.Top = 5.447124F;
            this.textBox431.Width = 1.063194F;
            // 
            // textBox432
            // 
            this.textBox432.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox432.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox432.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox432.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox432.Height = 0.1576389F;
            this.textBox432.Left = 7.870506F;
            this.textBox432.Name = "textBox432";
            this.textBox432.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox432.Tag = "";
            this.textBox432.Text = "給与所得控除後の金額";
            this.textBox432.Top = 5.289485F;
            this.textBox432.Width = 1.063194F;
            // 
            // textBox433
            // 
            this.textBox433.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox433.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox433.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox433.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox433.DataField = "ITEM032";
            this.textBox433.Height = 0.3152778F;
            this.textBox433.Left = 8.933699F;
            this.textBox433.Name = "textBox433";
            this.textBox433.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; vertical-align: bottom; ddo-char-set: 1";
            this.textBox433.Tag = "";
            this.textBox433.Text = "ITEM032";
            this.textBox433.Top = 5.447124F;
            this.textBox433.Width = 1.063194F;
            // 
            // textBox434
            // 
            this.textBox434.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox434.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox434.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox434.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox434.Height = 0.1576389F;
            this.textBox434.Left = 8.933699F;
            this.textBox434.Name = "textBox434";
            this.textBox434.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox434.Tag = "";
            this.textBox434.Text = "所得控除の額の合計額";
            this.textBox434.Top = 5.289485F;
            this.textBox434.Width = 1.063194F;
            // 
            // textBox435
            // 
            this.textBox435.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox435.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox435.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox435.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox435.DataField = "ITEM033";
            this.textBox435.Height = 0.3152778F;
            this.textBox435.Left = 9.996893F;
            this.textBox435.Name = "textBox435";
            this.textBox435.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: normal; text-align: right; vertical-align: bottom; ddo-char-set: 1";
            this.textBox435.Tag = "";
            this.textBox435.Text = "ITEM033";
            this.textBox435.Top = 5.447124F;
            this.textBox435.Width = 1.063194F;
            // 
            // textBox436
            // 
            this.textBox436.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox436.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox436.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox436.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox436.Height = 0.1576389F;
            this.textBox436.Left = 9.996893F;
            this.textBox436.Name = "textBox436";
            this.textBox436.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox436.Tag = "";
            this.textBox436.Text = "源泉徴収税額";
            this.textBox436.Top = 5.289485F;
            this.textBox436.Width = 1.063194F;
            // 
            // textBox437
            // 
            this.textBox437.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox437.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox437.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox437.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox437.Height = 0.7519687F;
            this.textBox437.Left = 8.933865F;
            this.textBox437.Name = "textBox437";
            this.textBox437.Style = "background-color: White; font-family: ＭＳ 明朝; font-size: 8pt; font-weight: normal;" +
    " text-align: center; ddo-char-set: 1";
            this.textBox437.Tag = "";
            this.textBox437.Text = " 氏　\r\n\r\n\r\n名";
            this.textBox437.Top = 4.537407F;
            this.textBox437.Width = 0.1576389F;
            // 
            // textBox438
            // 
            this.textBox438.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox438.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox438.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox438.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox438.Height = 0.3152778F;
            this.textBox438.Left = 10.46912F;
            this.textBox438.Name = "textBox438";
            this.textBox438.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox438.Tag = "";
            this.textBox438.Text = "住宅借入金等特別控除の額";
            this.textBox438.Top = 5.762402F;
            this.textBox438.Width = 0.5909723F;
            // 
            // textBox439
            // 
            this.textBox439.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox439.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox439.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox439.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox439.DataField = "ITEM049";
            this.textBox439.Height = 0.3541667F;
            this.textBox439.Left = 10.46912F;
            this.textBox439.Name = "textBox439";
            this.textBox439.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: right; vertical-align: bottom; ddo-char-set: 1";
            this.textBox439.Tag = "";
            this.textBox439.Text = "ITEM049";
            this.textBox439.Top = 6.077679F;
            this.textBox439.Width = 0.5909723F;
            // 
            // textBox440
            // 
            this.textBox440.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox440.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox440.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox440.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox440.Height = 0.3152778F;
            this.textBox440.Left = 8.696907F;
            this.textBox440.Name = "textBox440";
            this.textBox440.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox440.Tag = "";
            this.textBox440.Text = "社会保険料等 の 金 額";
            this.textBox440.Top = 5.762402F;
            this.textBox440.Width = 0.7090278F;
            // 
            // textBox441
            // 
            this.textBox441.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox441.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox441.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox441.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox441.Height = 0.3152778F;
            this.textBox441.Left = 9.996893F;
            this.textBox441.Name = "textBox441";
            this.textBox441.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox441.Tag = "";
            this.textBox441.Text = "地震保険料の控除額";
            this.textBox441.Top = 5.762402F;
            this.textBox441.Width = 0.4722222F;
            // 
            // textBox442
            // 
            this.textBox442.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox442.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox442.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox442.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox442.DataField = "ITEM048";
            this.textBox442.Height = 0.3541667F;
            this.textBox442.Left = 9.996893F;
            this.textBox442.Name = "textBox442";
            this.textBox442.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: right; vertical-align: bottom; ddo-char-set: 1";
            this.textBox442.Tag = "";
            this.textBox442.Text = "ITEM048";
            this.textBox442.Top = 6.077679F;
            this.textBox442.Width = 0.4722222F;
            // 
            // textBox443
            // 
            this.textBox443.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox443.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox443.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox443.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox443.Height = 0.3152778F;
            this.textBox443.Left = 9.405928F;
            this.textBox443.Name = "textBox443";
            this.textBox443.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox443.Tag = "";
            this.textBox443.Text = "生命保険料の 控 除 額";
            this.textBox443.Top = 5.762402F;
            this.textBox443.Width = 0.5909723F;
            // 
            // textBox444
            // 
            this.textBox444.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox444.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox444.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox444.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox444.DataField = "ITEM047";
            this.textBox444.Height = 0.3541667F;
            this.textBox444.Left = 9.405928F;
            this.textBox444.Name = "textBox444";
            this.textBox444.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: right; vertical-align: bottom; ddo-char-set: 1";
            this.textBox444.Tag = "";
            this.textBox444.Text = "ITEM047";
            this.textBox444.Top = 6.077679F;
            this.textBox444.Width = 0.5909723F;
            // 
            // textBox445
            // 
            this.textBox445.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox445.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox445.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox445.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox445.Height = 0.1972222F;
            this.textBox445.Left = 8.1462F;
            this.textBox445.Name = "textBox445";
            this.textBox445.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox445.Tag = "";
            this.textBox445.Text = "障害者の数(本人を除く)";
            this.textBox445.Top = 5.762402F;
            this.textBox445.Width = 0.5513888F;
            // 
            // textBox446
            // 
            this.textBox446.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox446.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox446.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox446.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox446.Height = 0.1180556F;
            this.textBox446.Left = 7.122595F;
            this.textBox446.Name = "textBox446";
            this.textBox446.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox446.Tag = "";
            this.textBox446.Text = "特定";
            this.textBox446.Top = 5.959624F;
            this.textBox446.Width = 0.2756944F;
            // 
            // textBox447
            // 
            this.textBox447.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox447.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox447.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox447.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox447.DataField = "ITEM038";
            this.textBox447.Height = 0.3541667F;
            this.textBox447.Left = 7.123284F;
            this.textBox447.Name = "textBox447";
            this.textBox447.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox447.Tag = "";
            this.textBox447.Text = "ITEM038";
            this.textBox447.Top = 6.077679F;
            this.textBox447.Width = 0.1377443F;
            // 
            // textBox448
            // 
            this.textBox448.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox448.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox448.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox448.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox448.Height = 0.3541667F;
            this.textBox448.Left = 7.257873F;
            this.textBox448.Name = "textBox448";
            this.textBox448.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: bold; tex" +
    "t-align: center; ddo-char-set: 1";
            this.textBox448.Tag = "";
            this.textBox448.Text = null;
            this.textBox448.Top = 6.077564F;
            this.textBox448.Width = 0.1338585F;
            // 
            // textBox449
            // 
            this.textBox449.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox449.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox449.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox449.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox449.Height = 0.1180556F;
            this.textBox449.Left = 7.398283F;
            this.textBox449.Name = "textBox449";
            this.textBox449.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox449.Tag = "";
            this.textBox449.Text = "老　人";
            this.textBox449.Top = 5.959624F;
            this.textBox449.Width = 0.4333332F;
            // 
            // textBox450
            // 
            this.textBox450.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox450.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox450.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox450.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox450.DataField = "ITEM040";
            this.textBox450.Height = 0.3541667F;
            this.textBox450.Left = 7.537402F;
            this.textBox450.Name = "textBox450";
            this.textBox450.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox450.Tag = "";
            this.textBox450.Text = "ITEM040";
            this.textBox450.Top = 6.077564F;
            this.textBox450.Width = 0.1496061F;
            // 
            // textBox451
            // 
            this.textBox451.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox451.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox451.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox451.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox451.Height = 0.3541667F;
            this.textBox451.Left = 7.691339F;
            this.textBox451.Name = "textBox451";
            this.textBox451.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: bold; tex" +
    "t-align: center; ddo-char-set: 1";
            this.textBox451.Tag = "";
            this.textBox451.Text = null;
            this.textBox451.Top = 6.077564F;
            this.textBox451.Width = 0.133858F;
            // 
            // textBox452
            // 
            this.textBox452.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox452.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox452.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox452.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox452.DataField = "ITEM039";
            this.textBox452.Height = 0.3541667F;
            this.textBox452.Left = 7.398283F;
            this.textBox452.Name = "textBox452";
            this.textBox452.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox452.Tag = "";
            this.textBox452.Text = "ITEM039";
            this.textBox452.Top = 6.077679F;
            this.textBox452.Width = 0.134004F;
            // 
            // textBox453
            // 
            this.textBox453.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox453.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox453.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox453.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox453.Height = 0.1180556F;
            this.textBox453.Left = 7.831617F;
            this.textBox453.Name = "textBox453";
            this.textBox453.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox453.Tag = "";
            this.textBox453.Text = "その他";
            this.textBox453.Top = 5.959624F;
            this.textBox453.Width = 0.3152778F;
            // 
            // textBox454
            // 
            this.textBox454.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox454.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox454.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox454.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox454.DataField = "ITEM041";
            this.textBox454.Height = 0.3541667F;
            this.textBox454.Left = 7.831617F;
            this.textBox454.Name = "textBox454";
            this.textBox454.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox454.Tag = "";
            this.textBox454.Text = "ITEM041";
            this.textBox454.Top = 6.077679F;
            this.textBox454.Width = 0.1576389F;
            // 
            // textBox455
            // 
            this.textBox455.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox455.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox455.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox455.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox455.Height = 0.3541667F;
            this.textBox455.Left = 7.989255F;
            this.textBox455.Name = "textBox455";
            this.textBox455.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: bold; tex" +
    "t-align: center; ddo-char-set: 1";
            this.textBox455.Tag = "";
            this.textBox455.Text = null;
            this.textBox455.Top = 6.077679F;
            this.textBox455.Width = 0.1576389F;
            // 
            // textBox456
            // 
            this.textBox456.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox456.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox456.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox456.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox456.Height = 0.1180556F;
            this.textBox456.Left = 8.146894F;
            this.textBox456.Name = "textBox456";
            this.textBox456.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox456.Tag = "";
            this.textBox456.Text = "特別";
            this.textBox456.Top = 5.959624F;
            this.textBox456.Width = 0.3152778F;
            // 
            // textBox457
            // 
            this.textBox457.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox457.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox457.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox457.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox457.DataField = "ITEM042";
            this.textBox457.Height = 0.3541667F;
            this.textBox457.Left = 8.146894F;
            this.textBox457.Name = "textBox457";
            this.textBox457.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox457.Tag = "";
            this.textBox457.Text = "ITEM042";
            this.textBox457.Top = 6.077679F;
            this.textBox457.Width = 0.1576389F;
            // 
            // textBox458
            // 
            this.textBox458.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox458.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox458.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox458.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox458.DataField = "ITEM043";
            this.textBox458.Height = 0.3541667F;
            this.textBox458.Left = 8.304539F;
            this.textBox458.Name = "textBox458";
            this.textBox458.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox458.Tag = "";
            this.textBox458.Text = "ITEM043";
            this.textBox458.Top = 6.077679F;
            this.textBox458.Width = 0.1576389F;
            // 
            // textBox459
            // 
            this.textBox459.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox459.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox459.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox459.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox459.DataField = "ITEM044";
            this.textBox459.Height = 0.3541667F;
            this.textBox459.Left = 8.462179F;
            this.textBox459.Name = "textBox459";
            this.textBox459.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox459.Tag = "";
            this.textBox459.Text = "ITEM044";
            this.textBox459.Top = 6.077679F;
            this.textBox459.Width = 0.2361111F;
            // 
            // textBox460
            // 
            this.textBox460.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox460.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox460.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox460.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox460.Height = 0.1180556F;
            this.textBox460.Left = 8.462214F;
            this.textBox460.Name = "textBox460";
            this.textBox460.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 4.5pt; font" +
    "-weight: normal; text-align: center; ddo-char-set: 1";
            this.textBox460.Tag = "";
            this.textBox460.Text = "その他";
            this.textBox460.Top = 5.959449F;
            this.textBox460.Width = 0.2322833F;
            // 
            // textBox461
            // 
            this.textBox461.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox461.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox461.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox461.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox461.CanGrow = false;
            this.textBox461.DataField = "ITEM064";
            this.textBox461.Height = 0.1576389F;
            this.textBox461.Left = 10.35107F;
            this.textBox461.Name = "textBox461";
            this.textBox461.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; ddo-char-set: 1";
            this.textBox461.Tag = "";
            this.textBox461.Text = "ITEM064";
            this.textBox461.Top = 6.431846F;
            this.textBox461.Width = 0.7090278F;
            // 
            // textBox462
            // 
            this.textBox462.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox462.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox462.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox462.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox462.Height = 0.1576389F;
            this.textBox462.Left = 9.523983F;
            this.textBox462.Name = "textBox462";
            this.textBox462.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox462.Tag = "";
            this.textBox462.Text = "介護医療保険料の金額";
            this.textBox462.Top = 6.431846F;
            this.textBox462.Width = 0.8270833F;
            // 
            // textBox463
            // 
            this.textBox463.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox463.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox463.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox463.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox463.CanGrow = false;
            this.textBox463.DataField = "ITEM065";
            this.textBox463.Height = 0.1576389F;
            this.textBox463.Left = 10.35107F;
            this.textBox463.Name = "textBox463";
            this.textBox463.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; ddo-char-set: 1";
            this.textBox463.Tag = "";
            this.textBox463.Text = "ITEM065";
            this.textBox463.Top = 6.589484F;
            this.textBox463.Width = 0.7090278F;
            // 
            // textBox464
            // 
            this.textBox464.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox464.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox464.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox464.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox464.Height = 0.1576389F;
            this.textBox464.Left = 9.523983F;
            this.textBox464.Name = "textBox464";
            this.textBox464.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox464.Tag = "";
            this.textBox464.Text = "新個人年金保険料の金額";
            this.textBox464.Top = 6.589484F;
            this.textBox464.Width = 0.8270833F;
            // 
            // textBox465
            // 
            this.textBox465.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox465.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox465.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox465.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox465.CanGrow = false;
            this.textBox465.DataField = "ITEM061";
            this.textBox465.Height = 0.1576389F;
            this.textBox465.Left = 8.814957F;
            this.textBox465.Name = "textBox465";
            this.textBox465.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; ddo-char-set: 1";
            this.textBox465.Tag = "";
            this.textBox465.Text = "ITEM061";
            this.textBox465.Top = 6.589484F;
            this.textBox465.Width = 0.7090278F;
            // 
            // textBox466
            // 
            this.textBox466.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox466.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox466.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox466.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox466.Height = 0.1576389F;
            this.textBox466.Left = 8.076756F;
            this.textBox466.Name = "textBox466";
            this.textBox466.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox466.Tag = "";
            this.textBox466.Text = "配偶者の合計所得";
            this.textBox466.Top = 6.589484F;
            this.textBox466.Width = 0.7375F;
            // 
            // textBox467
            // 
            this.textBox467.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox467.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox467.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox467.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox467.CanGrow = false;
            this.textBox467.DataField = "ITEM066";
            this.textBox467.Height = 0.1576389F;
            this.textBox467.Left = 10.35107F;
            this.textBox467.Name = "textBox467";
            this.textBox467.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; ddo-char-set: 1";
            this.textBox467.Tag = "";
            this.textBox467.Text = "ITEM066";
            this.textBox467.Top = 6.747124F;
            this.textBox467.Width = 0.7090278F;
            // 
            // textBox468
            // 
            this.textBox468.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox468.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox468.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox468.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox468.Height = 0.1576389F;
            this.textBox468.Left = 9.523983F;
            this.textBox468.Name = "textBox468";
            this.textBox468.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox468.Tag = "";
            this.textBox468.Text = "旧個人年金保険料の金額";
            this.textBox468.Top = 6.747124F;
            this.textBox468.Width = 0.8270833F;
            // 
            // textBox469
            // 
            this.textBox469.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox469.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox469.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox469.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox469.CanGrow = false;
            this.textBox469.DataField = "ITEM062";
            this.textBox469.Height = 0.1576389F;
            this.textBox469.Left = 8.814957F;
            this.textBox469.Name = "textBox469";
            this.textBox469.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; ddo-char-set: 1";
            this.textBox469.Tag = "";
            this.textBox469.Text = "ITEM062";
            this.textBox469.Top = 6.747124F;
            this.textBox469.Width = 0.7090278F;
            // 
            // textBox470
            // 
            this.textBox470.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox470.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox470.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox470.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox470.Height = 0.1576389F;
            this.textBox470.Left = 8.076756F;
            this.textBox470.Name = "textBox470";
            this.textBox470.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox470.Tag = "";
            this.textBox470.Text = "新生命保険料の金額\r\n";
            this.textBox470.Top = 6.747124F;
            this.textBox470.Width = 0.7375F;
            // 
            // textBox471
            // 
            this.textBox471.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox471.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox471.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox471.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox471.CanGrow = false;
            this.textBox471.DataField = "ITEM067";
            this.textBox471.Height = 0.1576389F;
            this.textBox471.Left = 10.35107F;
            this.textBox471.Name = "textBox471";
            this.textBox471.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; ddo-char-set: 1";
            this.textBox471.Tag = "";
            this.textBox471.Text = "ITEM067";
            this.textBox471.Top = 6.904762F;
            this.textBox471.Width = 0.7090278F;
            // 
            // textBox472
            // 
            this.textBox472.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox472.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox472.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox472.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox472.Height = 0.1576389F;
            this.textBox472.Left = 9.523983F;
            this.textBox472.Name = "textBox472";
            this.textBox472.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox472.Tag = "";
            this.textBox472.Text = "旧長期損害保険料の金額";
            this.textBox472.Top = 6.904762F;
            this.textBox472.Width = 0.8270833F;
            // 
            // textBox473
            // 
            this.textBox473.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox473.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox473.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox473.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox473.CanGrow = false;
            this.textBox473.DataField = "ITEM063";
            this.textBox473.Height = 0.1576389F;
            this.textBox473.Left = 8.814957F;
            this.textBox473.Name = "textBox473";
            this.textBox473.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: center; ddo-char-set: 1";
            this.textBox473.Tag = "";
            this.textBox473.Text = "ITEM063";
            this.textBox473.Top = 6.904762F;
            this.textBox473.Width = 0.7090278F;
            // 
            // textBox499
            // 
            this.textBox499.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox499.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox499.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox499.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox499.Height = 0.1576389F;
            this.textBox499.Left = 8.076756F;
            this.textBox499.Name = "textBox499";
            this.textBox499.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox499.Tag = "";
            this.textBox499.Text = "旧生命保険料の金額";
            this.textBox499.Top = 6.904762F;
            this.textBox499.Width = 0.7375F;
            // 
            // textBox500
            // 
            this.textBox500.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox500.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox500.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox500.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox500.Height = 0.3944443F;
            this.textBox500.Left = 5.941339F;
            this.textBox500.Name = "textBox500";
            this.textBox500.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox500.Tag = "";
            this.textBox500.Text = "支払者";
            this.textBox500.Top = 7.574209F;
            this.textBox500.Width = 0.2131944F;
            // 
            // textBox501
            // 
            this.textBox501.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox501.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox501.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox501.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox501.Height = 0.1972222F;
            this.textBox501.Left = 6.154534F;
            this.textBox501.Name = "textBox501";
            this.textBox501.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox501.Tag = "";
            this.textBox501.Text = "住所(居所)\r\n又は所在地 ";
            this.textBox501.Top = 7.574209F;
            this.textBox501.Width = 0.6375F;
            // 
            // textBox502
            // 
            this.textBox502.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox502.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox502.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox502.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox502.CanGrow = false;
            this.textBox502.Height = 0.1972222F;
            this.textBox502.Left = 6.154534F;
            this.textBox502.Name = "textBox502";
            this.textBox502.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox502.Tag = "";
            this.textBox502.Text = "氏 名 又 は\r\n名　　　 称";
            this.textBox502.Top = 7.771432F;
            this.textBox502.Width = 0.6375F;
            // 
            // textBox503
            // 
            this.textBox503.DataField = "ITEM098";
            this.textBox503.Height = 0.1576389F;
            this.textBox503.Left = 9.680929F;
            this.textBox503.Name = "textBox503";
            this.textBox503.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.textBox503.Tag = "";
            this.textBox503.Text = "ITEM098";
            this.textBox503.Top = 7.787404F;
            this.textBox503.Width = 1.358333F;
            // 
            // label55
            // 
            this.label55.Height = 0.1180556F;
            this.label55.HyperLink = null;
            this.label55.Left = 6.243423F;
            this.label55.Name = "label55";
            this.label55.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.label55.Tag = "";
            this.label55.Text = "住宅借入金等特別控除可能額";
            this.label55.Top = 6.443651F;
            this.label55.Width = 1.141667F;
            // 
            // textBox504
            // 
            this.textBox504.DataField = "ITEM050";
            this.textBox504.Height = 0.1180556F;
            this.textBox504.Left = 7.385089F;
            this.textBox504.Name = "textBox504";
            this.textBox504.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.textBox504.Tag = "";
            this.textBox504.Text = "ITEM050";
            this.textBox504.Top = 6.443651F;
            this.textBox504.Width = 0.5277781F;
            // 
            // label57
            // 
            this.label57.Height = 0.1180556F;
            this.label57.HyperLink = null;
            this.label57.Left = 7.976061F;
            this.label57.Name = "label57";
            this.label57.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.label57.Tag = "";
            this.label57.Text = "国民年金保険料等の金額";
            this.label57.Top = 6.443651F;
            this.label57.Width = 0.9451389F;
            // 
            // textBox505
            // 
            this.textBox505.DataField = "ITEM051";
            this.textBox505.Height = 0.1180556F;
            this.textBox505.Left = 8.9212F;
            this.textBox505.Name = "textBox505";
            this.textBox505.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.textBox505.Tag = "";
            this.textBox505.Text = "ITEM051";
            this.textBox505.Top = 6.443651F;
            this.textBox505.Width = 0.5909723F;
            // 
            // textBox506
            // 
            this.textBox506.DataField = "ITEM052";
            this.textBox506.Height = 0.1416667F;
            this.textBox506.Left = 6.243423F;
            this.textBox506.Name = "textBox506";
            this.textBox506.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.textBox506.Tag = "";
            this.textBox506.Text = "ITEM052";
            this.textBox506.Top = 6.561708F;
            this.textBox506.Width = 1.784028F;
            // 
            // textBox507
            // 
            this.textBox507.DataField = "ITEM053";
            this.textBox507.Height = 0.1104167F;
            this.textBox507.Left = 6.243423F;
            this.textBox507.Name = "textBox507";
            this.textBox507.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.textBox507.Tag = "";
            this.textBox507.Text = "ITEM053";
            this.textBox507.Top = 6.672124F;
            this.textBox507.Width = 1.784028F;
            // 
            // textBox508
            // 
            this.textBox508.DataField = "ITEM054";
            this.textBox508.Height = 0.1104167F;
            this.textBox508.Left = 6.243307F;
            this.textBox508.Name = "textBox508";
            this.textBox508.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.textBox508.Tag = "";
            this.textBox508.Text = "ITEM054";
            this.textBox508.Top = 6.794489F;
            this.textBox508.Width = 1.784028F;
            // 
            // textBox509
            // 
            this.textBox509.DataField = "ITEM055";
            this.textBox509.Height = 0.07847222F;
            this.textBox509.Left = 5.980923F;
            this.textBox509.Name = "textBox509";
            this.textBox509.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.textBox509.Tag = "";
            this.textBox509.Text = "ITEM055";
            this.textBox509.Top = 6.892959F;
            this.textBox509.Width = 0.6694444F;
            // 
            // textBox510
            // 
            this.textBox510.DataField = "ITEM058";
            this.textBox510.Height = 0.07847222F;
            this.textBox510.Left = 5.980923F;
            this.textBox510.Name = "textBox510";
            this.textBox510.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.textBox510.Tag = "";
            this.textBox510.Text = "ITEM058";
            this.textBox510.Top = 6.97143F;
            this.textBox510.Width = 0.6694444F;
            // 
            // textBox511
            // 
            this.textBox511.DataField = "ITEM056";
            this.textBox511.Height = 0.07847222F;
            this.textBox511.Left = 6.680923F;
            this.textBox511.Name = "textBox511";
            this.textBox511.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.textBox511.Tag = "";
            this.textBox511.Text = "ITEM056";
            this.textBox511.Top = 6.891568F;
            this.textBox511.Width = 0.6694444F;
            // 
            // textBox512
            // 
            this.textBox512.DataField = "ITEM059";
            this.textBox512.Height = 0.07847222F;
            this.textBox512.Left = 6.680923F;
            this.textBox512.Name = "textBox512";
            this.textBox512.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.textBox512.Tag = "";
            this.textBox512.Text = "ITEM059";
            this.textBox512.Top = 6.970041F;
            this.textBox512.Width = 0.6694444F;
            // 
            // textBox513
            // 
            this.textBox513.DataField = "ITEM057";
            this.textBox513.Height = 0.07847222F;
            this.textBox513.Left = 7.378846F;
            this.textBox513.Name = "textBox513";
            this.textBox513.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.textBox513.Tag = "";
            this.textBox513.Text = "ITEM057";
            this.textBox513.Top = 6.891568F;
            this.textBox513.Width = 0.6694444F;
            // 
            // textBox514
            // 
            this.textBox514.DataField = "ITEM060";
            this.textBox514.Height = 0.07847222F;
            this.textBox514.Left = 7.378846F;
            this.textBox514.Name = "textBox514";
            this.textBox514.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.textBox514.Tag = "";
            this.textBox514.Text = "ITEM060";
            this.textBox514.Top = 6.970041F;
            this.textBox514.Width = 0.6694444F;
            // 
            // label58
            // 
            this.label58.Height = 0.08611111F;
            this.label58.HyperLink = null;
            this.label58.Left = 6.816256F;
            this.label58.Name = "label58";
            this.label58.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.label58.Tag = "";
            this.label58.Text = "Ａ内　　　　　　　　　 　円";
            this.label58.Top = 5.463387F;
            this.label58.Width = 0.971934F;
            // 
            // label59
            // 
            this.label59.Height = 0.08611111F;
            this.label59.HyperLink = null;
            this.label59.Left = 10.01418F;
            this.label59.Name = "label59";
            this.label59.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.label59.Tag = "";
            this.label59.Text = "内　　　　　　　　　　　 　円";
            this.label59.Top = 5.463387F;
            this.label59.Width = 1.035351F;
            // 
            // label60
            // 
            this.label60.Height = 0.08611111F;
            this.label60.HyperLink = null;
            this.label60.Left = 9.805838F;
            this.label60.Name = "label60";
            this.label60.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label60.Tag = "";
            this.label60.Text = "円";
            this.label60.Top = 5.463387F;
            this.label60.Width = 0.1666667F;
            // 
            // label61
            // 
            this.label61.Height = 0.08611111F;
            this.label61.HyperLink = null;
            this.label61.Left = 7.875985F;
            this.label61.Name = "label61";
            this.label61.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.label61.Tag = "";
            this.label61.Text = "Ｂ　　　　　　　　　　　　円";
            this.label61.Top = 5.463387F;
            this.label61.Width = 1.034017F;
            // 
            // textBox515
            // 
            this.textBox515.DataField = "ITEM046";
            this.textBox515.Height = 0.09582743F;
            this.textBox515.Left = 8.722588F;
            this.textBox515.Name = "textBox515";
            this.textBox515.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-w" +
    "eight: normal; text-align: center; ddo-char-set: 1";
            this.textBox515.Tag = "";
            this.textBox515.Text = "ITEM046";
            this.textBox515.Top = 6.172818F;
            this.textBox515.Width = 0.6673612F;
            // 
            // label62
            // 
            this.label62.Height = 0.08611111F;
            this.label62.HyperLink = null;
            this.label62.Left = 6.951756F;
            this.label62.Name = "label62";
            this.label62.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label62.Tag = "";
            this.label62.Text = "円";
            this.label62.Top = 6.079068F;
            this.label62.Width = 0.1666667F;
            // 
            // label63
            // 
            this.label63.Height = 0.08611111F;
            this.label63.HyperLink = null;
            this.label63.Left = 7.128839F;
            this.label63.Name = "label63";
            this.label63.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label63.Tag = "";
            this.label63.Text = "人";
            this.label63.Top = 6.079068F;
            this.label63.Width = 0.1365156F;
            // 
            // label64
            // 
            this.label64.Height = 0.08611111F;
            this.label64.HyperLink = null;
            this.label64.Left = 7.399672F;
            this.label64.Name = "label64";
            this.label64.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.label64.Tag = "";
            this.label64.Text = "内    人";
            this.label64.Top = 6.079068F;
            this.label64.Width = 0.3074185F;
            // 
            // label65
            // 
            this.label65.Height = 0.08611111F;
            this.label65.HyperLink = null;
            this.label65.Left = 7.837172F;
            this.label65.Name = "label65";
            this.label65.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label65.Tag = "";
            this.label65.Text = "人";
            this.label65.Top = 6.079068F;
            this.label65.Width = 0.15625F;
            // 
            // label66
            // 
            this.label66.Height = 0.08611111F;
            this.label66.HyperLink = null;
            this.label66.Left = 8.140158F;
            this.label66.Name = "label66";
            this.label66.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.label66.Tag = "";
            this.label66.Text = "Ｋ内 Ｌ人  Ｍ人";
            this.label66.Top = 6.089375F;
            this.label66.Width = 0.5543305F;
            // 
            // label67
            // 
            this.label67.Height = 0.08611111F;
            this.label67.HyperLink = null;
            this.label67.Left = 9.826756F;
            this.label67.Name = "label67";
            this.label67.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label67.Tag = "";
            this.label67.Text = "円";
            this.label67.Top = 6.089485F;
            this.label67.Width = 0.1666667F;
            // 
            // label68
            // 
            this.label68.Height = 0.08611111F;
            this.label68.HyperLink = null;
            this.label68.Left = 10.29551F;
            this.label68.Name = "label68";
            this.label68.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label68.Tag = "";
            this.label68.Text = "円";
            this.label68.Top = 6.089485F;
            this.label68.Width = 0.1666667F;
            // 
            // label69
            // 
            this.label69.Height = 0.08611111F;
            this.label69.HyperLink = null;
            this.label69.Left = 10.87884F;
            this.label69.Name = "label69";
            this.label69.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label69.Tag = "";
            this.label69.Text = "円";
            this.label69.Top = 6.089485F;
            this.label69.Width = 0.1666667F;
            // 
            // label70
            // 
            this.label70.Height = 0.08611111F;
            this.label70.HyperLink = null;
            this.label70.Left = 9.358006F;
            this.label70.Name = "label70";
            this.label70.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label70.Tag = "";
            this.label70.Text = "円";
            this.label70.Top = 6.589484F;
            this.label70.Width = 0.1666667F;
            // 
            // label71
            // 
            this.label71.Height = 0.08611111F;
            this.label71.HyperLink = null;
            this.label71.Left = 9.358006F;
            this.label71.Name = "label71";
            this.label71.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label71.Tag = "";
            this.label71.Text = "円";
            this.label71.Top = 6.745734F;
            this.label71.Width = 0.1666667F;
            // 
            // label72
            // 
            this.label72.Height = 0.08611111F;
            this.label72.HyperLink = null;
            this.label72.Left = 9.358006F;
            this.label72.Name = "label72";
            this.label72.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label72.Tag = "";
            this.label72.Text = "円";
            this.label72.Top = 6.901984F;
            this.label72.Width = 0.1666667F;
            // 
            // label73
            // 
            this.label73.Height = 0.08611111F;
            this.label73.HyperLink = null;
            this.label73.Left = 10.88926F;
            this.label73.Name = "label73";
            this.label73.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label73.Tag = "";
            this.label73.Text = "円";
            this.label73.Top = 6.589484F;
            this.label73.Width = 0.1666667F;
            // 
            // label74
            // 
            this.label74.Height = 0.08611111F;
            this.label74.HyperLink = null;
            this.label74.Left = 10.88926F;
            this.label74.Name = "label74";
            this.label74.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label74.Tag = "";
            this.label74.Text = "円";
            this.label74.Top = 6.745734F;
            this.label74.Width = 0.1666667F;
            // 
            // label75
            // 
            this.label75.Height = 0.08611111F;
            this.label75.HyperLink = null;
            this.label75.Left = 10.88926F;
            this.label75.Name = "label75";
            this.label75.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label75.Tag = "";
            this.label75.Text = "円";
            this.label75.Top = 6.901984F;
            this.label75.Width = 0.1666667F;
            // 
            // label76
            // 
            this.label76.Height = 0.08611111F;
            this.label76.HyperLink = null;
            this.label76.Left = 10.88926F;
            this.label76.Name = "label76";
            this.label76.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.label76.Tag = "";
            this.label76.Text = "円";
            this.label76.Top = 6.433235F;
            this.label76.Width = 0.1666667F;
            // 
            // textBox516
            // 
            this.textBox516.CanGrow = false;
            this.textBox516.DataField = "ITEM025";
            this.textBox516.Height = 0.1472222F;
            this.textBox516.Left = 9.670506F;
            this.textBox516.Name = "textBox516";
            this.textBox516.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: left; ddo-char-set: 1";
            this.textBox516.Tag = "";
            this.textBox516.Text = "ITEM025";
            this.textBox516.Top = 4.568652F;
            this.textBox516.Width = 1.364583F;
            // 
            // textBox517
            // 
            this.textBox517.DataField = "ITEM026";
            this.textBox517.Height = 0.1576389F;
            this.textBox517.Left = 9.555929F;
            this.textBox517.Name = "textBox517";
            this.textBox517.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: left; ddo-char-set: 1";
            this.textBox517.Tag = "";
            this.textBox517.Text = "ITEM026";
            this.textBox517.Top = 4.756151F;
            this.textBox517.Width = 1.479167F;
            // 
            // textBox518
            // 
            this.textBox518.DataField = "ITEM027";
            this.textBox518.Height = 0.1368055F;
            this.textBox518.Left = 9.555929F;
            this.textBox518.Name = "textBox518";
            this.textBox518.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.textBox518.Tag = "";
            this.textBox518.Text = "ITEM027";
            this.textBox518.Top = 4.954069F;
            this.textBox518.Width = 1.479167F;
            // 
            // textBox519
            // 
            this.textBox519.DataField = "ITEM028";
            this.textBox519.Height = 0.1576389F;
            this.textBox519.Left = 9.555929F;
            this.textBox519.Name = "textBox519";
            this.textBox519.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-weight: normal; t" +
    "ext-align: left; ddo-char-set: 1";
            this.textBox519.Tag = "";
            this.textBox519.Text = "ITEM028";
            this.textBox519.Top = 5.110319F;
            this.textBox519.Width = 1.479167F;
            // 
            // textBox520
            // 
            this.textBox520.DataField = "ITEM082";
            this.textBox520.Height = 0.1381944F;
            this.textBox520.Left = 8.930929F;
            this.textBox520.Name = "textBox520";
            this.textBox520.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.textBox520.Tag = "";
            this.textBox520.Text = "ITEM082";
            this.textBox520.Top = 7.287401F;
            this.textBox520.Width = 0.2131944F;
            // 
            // textBox521
            // 
            this.textBox521.DataField = "ITEM083";
            this.textBox521.Height = 0.1381944F;
            this.textBox521.Left = 9.144115F;
            this.textBox521.Name = "textBox521";
            this.textBox521.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.textBox521.Tag = "";
            this.textBox521.Text = "ITEM083";
            this.textBox521.Top = 7.287401F;
            this.textBox521.Width = 0.2131944F;
            // 
            // textBox522
            // 
            this.textBox522.DataField = "ITEM084";
            this.textBox522.Height = 0.1381944F;
            this.textBox522.Left = 9.357311F;
            this.textBox522.Name = "textBox522";
            this.textBox522.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.textBox522.Tag = "";
            this.textBox522.Text = "ITEM084";
            this.textBox522.Top = 7.287401F;
            this.textBox522.Width = 0.2131944F;
            // 
            // shape4
            // 
            this.shape4.Height = 0.1976378F;
            this.shape4.Left = 9.0882F;
            this.shape4.LineWeight = 3F;
            this.shape4.Name = "shape4";
            this.shape4.RoundingRadius = 9.999999F;
            this.shape4.Top = 4.733465F;
            this.shape4.Width = 1.969685F;
            // 
            // shape7
            // 
            this.shape7.Height = 0.3153543F;
            this.shape7.Left = 6.80748F;
            this.shape7.LineWeight = 3F;
            this.shape7.Name = "shape7";
            this.shape7.RoundingRadius = 9.999999F;
            this.shape7.Top = 5.446856F;
            this.shape7.Width = 1.068504F;
            // 
            // shape8
            // 
            this.shape8.Height = 0.3153543F;
            this.shape8.Left = 7.861026F;
            this.shape8.LineWeight = 3F;
            this.shape8.Name = "shape8";
            this.shape8.RoundingRadius = 9.999999F;
            this.shape8.Top = 5.446856F;
            this.shape8.Width = 1.076378F;
            // 
            // shape10
            // 
            this.shape10.Height = 0.3767719F;
            this.shape10.Left = 5.941339F;
            this.shape10.LineWeight = 3F;
            this.shape10.Name = "shape10";
            this.shape10.RoundingRadius = 9.999999F;
            this.shape10.Top = 6.055119F;
            this.shape10.Width = 0.248031F;
            // 
            // shape13
            // 
            this.shape13.Height = 0.3767719F;
            this.shape13.Left = 6.40315F;
            this.shape13.LineWeight = 3F;
            this.shape13.Name = "shape13";
            this.shape13.RoundingRadius = 9.999999F;
            this.shape13.Top = 6.055118F;
            this.shape13.Width = 0.8578743F;
            // 
            // shape14
            // 
            this.shape14.Height = 0.3767719F;
            this.shape14.Left = 6.519292F;
            this.shape14.LineWeight = 3F;
            this.shape14.Name = "shape14";
            this.shape14.RoundingRadius = 9.999999F;
            this.shape14.Top = 6.055119F;
            this.shape14.Width = 0.6141732F;
            // 
            // shape16
            // 
            this.shape16.Height = 0.3767719F;
            this.shape16.Left = 7.385039F;
            this.shape16.LineWeight = 3F;
            this.shape16.Name = "shape16";
            this.shape16.RoundingRadius = 9.999999F;
            this.shape16.Top = 6.055118F;
            this.shape16.Width = 0.3137797F;
            // 
            // shape18
            // 
            this.shape18.Height = 0.3767719F;
            this.shape18.Left = 7.825984F;
            this.shape18.LineWeight = 3F;
            this.shape18.Name = "shape18";
            this.shape18.RoundingRadius = 9.999999F;
            this.shape18.Top = 6.055118F;
            this.shape18.Width = 0.1771653F;
            // 
            // shape22
            // 
            this.shape22.Height = 0.3728347F;
            this.shape22.Left = 8.140158F;
            this.shape22.LineWeight = 3F;
            this.shape22.Name = "shape22";
            this.shape22.RoundingRadius = 9.999999F;
            this.shape22.Top = 6.055118F;
            this.shape22.Width = 2.340551F;
            // 
            // shape23
            // 
            this.shape23.Height = 0.3767719F;
            this.shape23.Left = 9.401192F;
            this.shape23.LineWeight = 3F;
            this.shape23.Name = "shape23";
            this.shape23.RoundingRadius = 9.999999F;
            this.shape23.Top = 6.055119F;
            this.shape23.Width = 0.6066929F;
            // 
            // shape24
            // 
            this.shape24.Height = 0.6444888F;
            this.shape24.Left = 10.3504F;
            this.shape24.LineWeight = 3F;
            this.shape24.Name = "shape24";
            this.shape24.RoundingRadius = 9.999999F;
            this.shape24.Top = 6.427565F;
            this.shape24.Width = 0.7141743F;
            // 
            // shape25
            // 
            this.shape25.Height = 0.5255904F;
            this.shape25.Left = 5.940552F;
            this.shape25.LineWeight = 3F;
            this.shape25.Name = "shape25";
            this.shape25.RoundingRadius = 9.999999F;
            this.shape25.Top = 7.06221F;
            this.shape25.Width = 0.4409451F;
            // 
            // shape29
            // 
            this.shape29.Height = 0.529134F;
            this.shape29.Left = 7.20236F;
            this.shape29.LineWeight = 3F;
            this.shape29.Name = "shape29";
            this.shape29.RoundingRadius = 9.999999F;
            this.shape29.Top = 7.050005F;
            this.shape29.Width = 1.311024F;
            // 
            // label77
            // 
            this.label77.Height = 0.1041667F;
            this.label77.HyperLink = null;
            this.label77.Left = 6.425985F;
            this.label77.Name = "label77";
            this.label77.Style = "font-family: ＭＳ 明朝; font-size: 5.25pt; ddo-char-set: 128";
            this.label77.Text = "Ｅ  Ｆ";
            this.label77.Top = 6.085438F;
            this.label77.Width = 0.25F;
            // 
            // label78
            // 
            this.label78.Height = 0.1041667F;
            this.label78.HyperLink = null;
            this.label78.Left = 7.13504F;
            this.label78.Name = "label78";
            this.label78.Style = "font-family: ＭＳ 明朝; font-size: 5.25pt; ddo-char-set: 128";
            this.label78.Text = "G";
            this.label78.Top = 6.07914F;
            this.label78.Width = 0.06496096F;
            // 
            // label84
            // 
            this.label84.Height = 0.1041667F;
            this.label84.HyperLink = null;
            this.label84.Left = 7.410235F;
            this.label84.Name = "label84";
            this.label84.Style = "font-family: ＭＳ 明朝; font-size: 5.25pt; ddo-char-set: 128";
            this.label84.Text = "H\r\n";
            this.label84.Top = 6.15473F;
            this.label84.Width = 0.1074806F;
            // 
            // label85
            // 
            this.label85.Height = 0.08611111F;
            this.label85.HyperLink = null;
            this.label85.Left = 7.261024F;
            this.label85.Name = "label85";
            this.label85.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 4.5pt; font-weight: normal; text-ali" +
    "gn: left; ddo-char-set: 1";
            this.label85.Tag = "";
            this.label85.Text = "従人";
            this.label85.Top = 6.079134F;
            this.label85.Width = 0.1562991F;
            // 
            // label96
            // 
            this.label96.Height = 0.09212583F;
            this.label96.HyperLink = null;
            this.label96.Left = 10.01418F;
            this.label96.Name = "label96";
            this.label96.Style = "font-family: ＭＳ 明朝; font-size: 5.5pt; ddo-char-set: 1";
            this.label96.Text = "P";
            this.label96.Top = 6.089375F;
            this.label96.Width = 0.1377954F;
            // 
            // label97
            // 
            this.label97.Height = 0.08611111F;
            this.label97.HyperLink = null;
            this.label97.Left = 7.992124F;
            this.label97.Name = "label97";
            this.label97.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 4.5pt; font-weight: normal; text-ali" +
    "gn: left; ddo-char-set: 1";
            this.label97.Tag = "";
            this.label97.Text = "従人";
            this.label97.Top = 6.077564F;
            this.label97.Width = 0.1562991F;
            // 
            // label98
            // 
            this.label98.Height = 0.08611111F;
            this.label98.HyperLink = null;
            this.label98.Left = 7.687006F;
            this.label98.Name = "label98";
            this.label98.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 4.5pt; font-weight: normal; text-ali" +
    "gn: left; ddo-char-set: 1";
            this.label98.Tag = "";
            this.label98.Text = "従人";
            this.label98.Top = 6.077564F;
            this.label98.Width = 0.1562991F;
            // 
            // label99
            // 
            this.label99.Height = 0.1041667F;
            this.label99.HyperLink = null;
            this.label99.Left = 7.532283F;
            this.label99.Name = "label99";
            this.label99.Style = "font-family: ＭＳ 明朝; font-size: 5.25pt; ddo-char-set: 128";
            this.label99.Text = "I";
            this.label99.Top = 6.085438F;
            this.label99.Width = 0.1074806F;
            // 
            // label100
            // 
            this.label100.Height = 0.1041667F;
            this.label100.HyperLink = null;
            this.label100.Left = 7.837005F;
            this.label100.Name = "label100";
            this.label100.Style = "font-family: ＭＳ 明朝; font-size: 5.25pt; ddo-char-set: 128";
            this.label100.Text = "J";
            this.label100.Top = 6.077564F;
            this.label100.Width = 0.1074806F;
            // 
            // label101
            // 
            this.label101.Height = 0.08070868F;
            this.label101.HyperLink = null;
            this.label101.Left = 8.825985F;
            this.label101.Name = "label101";
            this.label101.Style = "font-family: ＭＳ 明朝; font-size: 5.5pt; ddo-char-set: 1";
            this.label101.Text = "N";
            this.label101.Top = 6.093307F;
            this.label101.Width = 0.1074806F;
            // 
            // label102
            // 
            this.label102.Height = 0.08070868F;
            this.label102.HyperLink = null;
            this.label102.Left = 9.448435F;
            this.label102.Name = "label102";
            this.label102.Style = "font-family: ＭＳ 明朝; font-size: 5.5pt; ddo-char-set: 1";
            this.label102.Text = "O";
            this.label102.Top = 6.094887F;
            this.label102.Width = 0.1074806F;
            // 
            // label103
            // 
            this.label103.Height = 0.1041667F;
            this.label103.HyperLink = null;
            this.label103.Left = 6.173623F;
            this.label103.Name = "label103";
            this.label103.Style = "font-family: ＭＳ 明朝; font-size: 5.25pt; ddo-char-set: 128";
            this.label103.Text = "チ";
            this.label103.Top = 7.074808F;
            this.label103.Width = 0.1311026F;
            // 
            // label104
            // 
            this.label104.Height = 0.1041667F;
            this.label104.HyperLink = null;
            this.label104.Left = 7.222443F;
            this.label104.Name = "label104";
            this.label104.Style = "font-family: ＭＳ 明朝; font-size: 5.25pt; ddo-char-set: 128";
            this.label104.Text = "リ　　ヌ　　ル　　ヲ";
            this.label104.Top = 7.194887F;
            this.label104.Width = 0.7657479F;
            // 
            // label105
            // 
            this.label105.Height = 0.1041667F;
            this.label105.HyperLink = null;
            this.label105.Left = 8.076769F;
            this.label105.Name = "label105";
            this.label105.Style = "font-family: ＭＳ 明朝; font-size: 5.25pt; ddo-char-set: 128";
            this.label105.Text = "ワ　　カ";
            this.label105.Top = 7.079926F;
            this.label105.Width = 0.3854331F;
            // 
            // shape30
            // 
            this.shape30.Height = 0.5287399F;
            this.shape30.Left = 8.499617F;
            this.shape30.LineWeight = 3F;
            this.shape30.Name = "shape30";
            this.shape30.RoundingRadius = 9.999999F;
            this.shape30.Top = 7.050399F;
            this.shape30.Width = 1.07874F;
            // 
            // label106
            // 
            this.label106.Height = 0.6094489F;
            this.label106.HyperLink = null;
            this.label106.Left = 10.38662F;
            this.label106.Name = "label106";
            this.label106.Style = "font-family: ＭＳ 明朝; font-size: 5.25pt; ddo-char-set: 128";
            this.label106.Text = "ニ\r\n\r\nホ\r\n\r\nヘ\r\n\r\nト";
            this.label106.Top = 6.474415F;
            this.label106.Width = 0.1342515F;
            // 
            // label107
            // 
            this.label107.Height = 0.3909448F;
            this.label107.HyperLink = null;
            this.label107.Left = 8.822054F;
            this.label107.Name = "label107";
            this.label107.Style = "font-family: ＭＳ 明朝; font-size: 5.25pt; ddo-char-set: 128";
            this.label107.Text = "イ\r\n\r\nロ\r\n\r\nハ\r\n\r\n";
            this.label107.Top = 6.624808F;
            this.label107.Width = 0.1342515F;
            // 
            // KYUR3041R
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0F;
            this.PageSettings.Margins.Left = 0.1968504F;
            this.PageSettings.Margins.Right = 0F;
            this.PageSettings.Margins.Top = 0.1968504F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
            this.PageSettings.PaperHeight = 11.69291F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.267716F;
            this.PrintWidth = 11.12254F;
            this.Sections.Add(this.detail);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.textBox218)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox226)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox227)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox228)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox189)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox188)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox187)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox186)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox185)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト54)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM011)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM011CP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル138)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル139)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト156)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト158)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト159)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト160)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト161)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト162)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト163)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト164)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト165)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト166)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト167)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト168)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト169)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト170)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト171)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM045)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル176)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM068)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM069)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM070)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM071)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM072)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM073)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM074)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM075)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM076)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM077)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM078)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM079)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM081)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM085)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM086)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM087)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM088)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM030)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM096)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM097)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM022)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM023)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM024)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト58)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM029)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト60)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト61)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト62)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト63)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト64)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト65)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM034)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM035)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aaa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bbb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM036)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト71)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM037)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト73)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト74)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト75)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト76)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト77)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト78)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト79)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト80)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト81)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト82)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト83)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト84)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト85)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト86)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト87)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト88)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト89)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト90)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト91)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM089)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM090)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM091)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM092)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM093)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト97)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト98)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト99)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト100)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト101)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト102)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM094)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト104)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM095)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト106)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト107)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM031)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM032)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM033)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM049)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM048)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト121)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM047)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト124)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM038)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.a)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM040)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM039)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM041)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM042)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM043)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM044)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト138)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM064)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM065)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM061)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM066)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM062)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM067)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト153)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM063)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト155)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト157)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM098)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM050)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル159)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM051)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM052)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM053)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM054)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM055)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM058)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM056)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM059)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM057)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM060)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル171)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル172)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル173)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM046)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル177)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル178)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル182)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル183)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル184)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル185)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル186)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル187)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル188)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル189)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル190)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル191)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル192)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル193)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル194)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル195)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM025)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM026)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM027)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM028)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM082)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM083)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM084)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox54)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox55)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox56)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox57)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox58)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox59)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox60)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox61)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox62)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox63)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox64)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox65)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox66)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox67)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox68)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox69)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox70)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox71)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox72)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox73)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox74)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox75)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox76)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox77)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox78)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox79)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox80)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox81)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox82)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox83)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox84)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox85)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox86)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox87)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox88)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox89)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox90)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox91)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox92)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox93)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox94)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox95)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox96)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox97)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox98)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox99)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox100)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox101)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox102)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox103)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox104)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox105)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox106)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox107)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox108)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox109)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox110)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox111)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox112)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox113)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox114)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox115)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox116)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox117)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox118)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox119)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox120)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox121)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox124)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox126)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox128)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox129)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox130)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox131)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox132)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox133)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox134)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox135)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox136)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox137)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox138)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox139)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox140)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox141)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox142)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox143)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox144)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox145)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox146)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox147)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox148)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox149)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox150)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox151)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox152)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox153)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox154)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox155)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox156)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox157)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox158)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox159)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox160)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox161)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox162)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox163)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox164)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox165)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox166)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox167)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox168)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox169)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox170)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox171)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox172)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox173)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox174)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox175)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox176)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox177)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox178)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox179)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox180)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox181)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox182)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox183)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox184)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox190)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox191)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox192)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox193)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox194)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox195)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox196)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox197)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox198)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox199)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox200)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox201)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox202)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox203)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox204)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox205)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox206)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox207)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox208)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox209)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox210)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox211)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox212)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox213)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox214)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox215)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox216)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox217)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox219)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox220)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox221)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox222)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox223)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox224)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox225)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox229)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox230)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox231)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox232)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox233)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox234)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox235)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox236)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox237)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox238)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox239)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox240)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox241)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox242)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox243)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox244)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox245)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox246)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox247)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox248)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox249)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox250)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox251)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox252)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox253)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox254)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox255)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox256)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox257)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox258)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox259)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox260)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox261)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox262)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox263)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox264)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox265)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox266)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox267)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox268)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox269)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox270)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox271)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox272)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox273)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox274)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox275)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox276)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox277)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox278)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox279)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox280)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox281)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox282)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox283)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox284)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox285)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox286)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox287)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox288)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox289)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox290)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox291)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox292)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox293)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox294)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox295)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox296)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox297)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox298)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox299)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox300)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox301)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox302)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox303)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox304)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox305)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox306)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox307)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox308)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox309)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox310)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox311)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox312)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox313)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox314)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox315)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox316)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox317)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox318)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox319)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox320)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox321)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox322)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox323)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox474)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox475)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox476)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox477)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox478)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox479)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox480)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox481)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox482)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox483)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox484)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox485)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox486)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox487)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox488)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox489)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox490)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox491)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label79)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox492)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox493)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox494)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox495)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox496)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox497)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox498)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label80)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox122)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox123)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox125)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label81)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label82)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label83)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label56)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label86)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label87)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label88)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label89)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label90)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label91)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label92)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label93)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label94)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label95)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox127)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox324)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox325)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox326)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox327)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox328)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox329)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox330)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox331)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox332)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox333)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox334)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox335)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox336)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox337)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox338)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox339)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox340)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox341)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox342)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox343)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox344)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox345)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox346)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox347)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox348)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox349)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox350)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox351)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label54)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox352)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox353)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox354)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox355)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox356)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox357)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox358)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox359)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox360)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox361)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox362)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox363)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox364)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox365)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox366)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox367)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox368)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox369)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox370)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox371)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox372)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox373)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox374)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox375)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox376)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox377)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox378)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox379)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox380)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox381)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox382)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox383)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox384)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox385)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox386)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox387)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox388)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox389)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox390)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox391)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox392)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox393)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox394)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox395)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox396)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox397)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox398)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox399)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox400)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox401)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox402)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox403)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox404)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox405)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox406)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox407)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox408)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox409)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox410)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox411)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox412)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox413)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox414)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox415)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox416)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox417)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox418)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox419)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox420)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox421)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox422)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox423)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox424)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox425)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox426)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox427)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox428)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox429)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox430)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox431)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox432)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox433)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox434)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox435)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox436)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox437)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox438)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox439)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox440)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox441)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox442)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox443)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox444)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox445)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox446)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox447)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox448)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox449)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox450)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox451)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox452)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox453)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox454)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox455)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox456)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox457)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox458)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox459)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox460)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox461)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox462)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox463)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox464)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox465)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox466)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox467)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox468)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox469)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox470)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox471)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox472)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox473)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox499)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox500)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox501)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox502)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox503)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label55)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox504)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label57)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox505)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox506)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox507)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox508)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox509)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox510)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox511)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox512)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox513)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox514)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label58)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label59)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label60)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label61)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox515)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label62)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label63)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label64)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label65)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label66)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label67)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label68)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label69)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label70)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label71)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label72)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label73)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label74)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label75)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label76)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox516)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox517)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox518)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox519)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox520)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox521)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox522)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label77)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label78)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label84)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label85)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label96)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label97)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label98)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label99)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label100)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label101)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label102)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label103)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label104)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label105)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label106)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label107)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM011;
        private GrapeCity.ActiveReports.SectionReportModel.Label テキスト28;
        private GrapeCity.ActiveReports.SectionReportModel.Label テキスト3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM011CP;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル138;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル139;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト156;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト158;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト159;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト160;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト161;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト162;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト163;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト164;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト165;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト166;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト167;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト168;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト169;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト170;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト171;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM045;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル176;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM068;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM069;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM070;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM071;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM072;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM073;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM074;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM075;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM076;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM077;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM078;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM079;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM081;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM085;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM086;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM087;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM088;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト27;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト30;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト32;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト34;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM030;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト39;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト47;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト48;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM097;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト52;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト54;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM022;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM023;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM024;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト58;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM029;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト60;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト61;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト62;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト63;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト64;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト65;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM034;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM035;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox aaa;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox bbb;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM036;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト71;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM037;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト73;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト74;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト75;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト76;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト77;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト78;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト79;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト80;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト81;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト82;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト83;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト84;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト85;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト86;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト87;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト88;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト89;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト90;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト91;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM089;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM090;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM091;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM092;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM093;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト97;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト98;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト99;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト100;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト101;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト102;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM094;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト104;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM095;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト106;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト107;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM031;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM032;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM033;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM049;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM048;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト121;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM047;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト124;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM038;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox a;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM040;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM039;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM041;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox13;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox14;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM042;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM043;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM044;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト138;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM064;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox15;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM065;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox16;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM061;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox17;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM066;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox18;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM062;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox19;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM067;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト153;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM063;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト155;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox20;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト157;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox21;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM098;
        private GrapeCity.ActiveReports.SectionReportModel.Label テキスト40;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM050;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル159;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM051;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM052;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM053;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM054;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM055;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM058;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM056;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM059;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM057;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM060;
        private GrapeCity.ActiveReports.SectionReportModel.Label テキスト35;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル171;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル172;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル173;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM046;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル177;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル178;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル182;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル183;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル184;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル185;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル186;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル187;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル188;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル189;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル190;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル191;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル192;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル193;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル194;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル195;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM025;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM026;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM027;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM028;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM082;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM083;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM084;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox22;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox23;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox24;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox25;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox26;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox27;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox28;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox29;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox30;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox31;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox32;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox33;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox34;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox35;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox36;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox37;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox38;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox39;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox40;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox41;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox42;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox43;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox44;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox45;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox46;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox47;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox48;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox49;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox50;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox52;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox53;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox54;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox55;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox56;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox57;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox58;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox59;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox60;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox61;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox62;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox63;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox64;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox65;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox66;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox67;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox68;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox69;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox70;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox71;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox72;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox73;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox74;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox75;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox76;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox77;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox78;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox79;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox80;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox81;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox82;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox83;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox86;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox87;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox88;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox89;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox90;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox91;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox92;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox93;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox94;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox95;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox96;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox97;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox98;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox99;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox100;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox101;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox102;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox103;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox104;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox105;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox106;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox107;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox108;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox109;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox110;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox111;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox112;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox113;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox114;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox115;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox116;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox117;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox118;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox119;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox120;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox121;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox124;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox126;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox128;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox129;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox130;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox131;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox132;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox133;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox134;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox135;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox136;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox137;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox138;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox139;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox140;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox141;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox142;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox143;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox144;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox145;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox146;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox147;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox148;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox149;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox150;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox151;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox152;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox153;
        private GrapeCity.ActiveReports.SectionReportModel.Label label2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox154;
        private GrapeCity.ActiveReports.SectionReportModel.Label label3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox155;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox156;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox157;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox158;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox159;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox160;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox161;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox162;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox163;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox164;
        private GrapeCity.ActiveReports.SectionReportModel.Label label4;
        private GrapeCity.ActiveReports.SectionReportModel.Label label5;
        private GrapeCity.ActiveReports.SectionReportModel.Label label6;
        private GrapeCity.ActiveReports.SectionReportModel.Label label7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox165;
        private GrapeCity.ActiveReports.SectionReportModel.Label label8;
        private GrapeCity.ActiveReports.SectionReportModel.Label label14;
        private GrapeCity.ActiveReports.SectionReportModel.Label label15;
        private GrapeCity.ActiveReports.SectionReportModel.Label label16;
        private GrapeCity.ActiveReports.SectionReportModel.Label label17;
        private GrapeCity.ActiveReports.SectionReportModel.Label label18;
        private GrapeCity.ActiveReports.SectionReportModel.Label label19;
        private GrapeCity.ActiveReports.SectionReportModel.Label label20;
        private GrapeCity.ActiveReports.SectionReportModel.Label label21;
        private GrapeCity.ActiveReports.SectionReportModel.Label label22;
        private GrapeCity.ActiveReports.SectionReportModel.Label label23;
        private GrapeCity.ActiveReports.SectionReportModel.Label label24;
        private GrapeCity.ActiveReports.SectionReportModel.Label label25;
        private GrapeCity.ActiveReports.SectionReportModel.Label label26;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox166;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox167;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox168;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox169;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox170;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox171;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox172;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM096;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox84;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox85;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape1;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox173;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox174;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox175;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox176;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox177;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox178;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox179;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox180;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox181;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox182;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox183;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox184;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox185;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox186;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox187;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox188;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox189;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox190;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox191;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox192;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox193;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox194;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox195;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox196;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox197;
        private GrapeCity.ActiveReports.SectionReportModel.Label label27;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox198;
        private GrapeCity.ActiveReports.SectionReportModel.Label label28;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox199;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox200;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox201;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox202;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox203;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox204;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox205;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox206;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox207;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox208;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox209;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox210;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox211;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox212;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox213;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox214;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox215;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox216;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox217;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox218;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox219;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox220;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox221;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox222;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox223;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox224;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox225;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox226;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox227;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox228;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox229;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox230;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox231;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox232;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox233;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox234;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox235;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox236;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox237;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox238;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox239;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox240;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox241;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox242;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox243;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox244;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox245;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox246;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox247;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox248;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox249;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox250;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox251;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox252;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox253;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox254;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox255;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox256;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox257;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox258;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox259;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox260;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox261;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox262;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox263;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox264;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox265;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox266;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox267;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox268;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox269;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox270;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox271;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox272;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox273;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox274;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox275;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox276;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox277;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox278;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox279;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox280;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox281;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox282;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox283;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox284;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox285;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox286;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox287;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox288;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox289;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox290;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox291;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox292;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox293;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox294;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox295;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox296;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox297;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox298;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox299;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox300;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox301;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox302;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox303;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox304;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox305;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox306;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox307;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox308;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox309;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox310;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox311;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox312;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox313;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox314;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox315;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox316;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox317;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox318;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox319;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox320;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox321;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox322;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox323;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox474;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox475;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox476;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox477;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox478;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox479;
        private GrapeCity.ActiveReports.SectionReportModel.Label label29;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox480;
        private GrapeCity.ActiveReports.SectionReportModel.Label label30;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox481;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox482;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox483;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox484;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox485;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox486;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox487;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox488;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox489;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox490;
        private GrapeCity.ActiveReports.SectionReportModel.Label label31;
        private GrapeCity.ActiveReports.SectionReportModel.Label label32;
        private GrapeCity.ActiveReports.SectionReportModel.Label label33;
        private GrapeCity.ActiveReports.SectionReportModel.Label label34;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox491;
        private GrapeCity.ActiveReports.SectionReportModel.Label label35;
        private GrapeCity.ActiveReports.SectionReportModel.Label label36;
        private GrapeCity.ActiveReports.SectionReportModel.Label label40;
        private GrapeCity.ActiveReports.SectionReportModel.Label label41;
        private GrapeCity.ActiveReports.SectionReportModel.Label label42;
        private GrapeCity.ActiveReports.SectionReportModel.Label label44;
        private GrapeCity.ActiveReports.SectionReportModel.Label label45;
        private GrapeCity.ActiveReports.SectionReportModel.Label label46;
        private GrapeCity.ActiveReports.SectionReportModel.Label label47;
        private GrapeCity.ActiveReports.SectionReportModel.Label label48;
        private GrapeCity.ActiveReports.SectionReportModel.Label label49;
        private GrapeCity.ActiveReports.SectionReportModel.Label label50;
        private GrapeCity.ActiveReports.SectionReportModel.Label label51;
        private GrapeCity.ActiveReports.SectionReportModel.Label label52;
        private GrapeCity.ActiveReports.SectionReportModel.Label label79;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox492;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox493;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox494;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox495;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox496;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox497;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox498;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape3;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape5;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape6;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape9;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape11;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape12;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape15;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape17;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape19;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape21;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape26;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape27;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape28;
        private GrapeCity.ActiveReports.SectionReportModel.Label label80;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox51;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox122;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox123;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox125;
        private GrapeCity.ActiveReports.SectionReportModel.Label label9;
        private GrapeCity.ActiveReports.SectionReportModel.Label label10;
        private GrapeCity.ActiveReports.SectionReportModel.Label label13;
        private GrapeCity.ActiveReports.SectionReportModel.Label label81;
        private GrapeCity.ActiveReports.SectionReportModel.Label label82;
        private GrapeCity.ActiveReports.SectionReportModel.Label label11;
        private GrapeCity.ActiveReports.SectionReportModel.Label label83;
        private GrapeCity.ActiveReports.SectionReportModel.Label label56;
        private GrapeCity.ActiveReports.SectionReportModel.Label label12;
        private GrapeCity.ActiveReports.SectionReportModel.Label label86;
        private GrapeCity.ActiveReports.SectionReportModel.Label label37;
        private GrapeCity.ActiveReports.SectionReportModel.Label label87;
        private GrapeCity.ActiveReports.SectionReportModel.Label label38;
        private GrapeCity.ActiveReports.SectionReportModel.Label label39;
        private GrapeCity.ActiveReports.SectionReportModel.Label label88;
        private GrapeCity.ActiveReports.SectionReportModel.Label label89;
        private GrapeCity.ActiveReports.SectionReportModel.Label label43;
        private GrapeCity.ActiveReports.SectionReportModel.Label label90;
        private GrapeCity.ActiveReports.SectionReportModel.Label label91;
        private GrapeCity.ActiveReports.SectionReportModel.Label label92;
        private GrapeCity.ActiveReports.SectionReportModel.Label label93;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape20;
        private GrapeCity.ActiveReports.SectionReportModel.Label label94;
        private GrapeCity.ActiveReports.SectionReportModel.Label label95;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox127;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox324;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox325;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox326;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox327;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox328;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox329;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox330;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox331;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox332;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox333;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox334;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox335;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox336;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox337;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox338;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox339;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox340;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox341;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox342;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox343;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox344;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox345;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox346;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox347;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox348;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox349;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox350;
        private GrapeCity.ActiveReports.SectionReportModel.Label label53;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox351;
        private GrapeCity.ActiveReports.SectionReportModel.Label label54;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox352;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox353;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox354;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox355;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox356;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox357;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox358;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox359;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox360;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox361;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox362;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox363;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox364;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox365;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox366;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox367;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox368;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox369;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox370;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox371;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox372;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox373;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox374;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox375;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox376;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox377;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox378;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox379;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox380;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox381;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox382;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox383;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox384;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox385;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox386;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox387;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox388;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox389;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox390;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox391;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox392;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox393;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox394;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox395;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox396;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox397;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox398;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox399;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox400;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox401;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox402;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox403;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox404;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox405;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox406;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox407;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox408;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox409;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox410;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox411;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox412;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox413;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox414;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox415;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox416;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox417;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox418;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox419;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox420;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox421;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox422;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox423;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox424;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox425;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox426;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox427;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox428;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox429;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox430;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox431;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox432;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox433;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox434;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox435;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox436;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox437;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox438;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox439;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox440;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox441;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox442;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox443;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox444;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox445;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox446;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox447;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox448;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox449;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox450;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox451;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox452;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox453;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox454;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox455;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox456;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox457;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox458;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox459;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox460;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox461;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox462;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox463;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox464;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox465;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox466;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox467;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox468;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox469;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox470;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox471;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox472;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox473;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox499;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox500;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox501;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox502;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox503;
        private GrapeCity.ActiveReports.SectionReportModel.Label label55;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox504;
        private GrapeCity.ActiveReports.SectionReportModel.Label label57;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox505;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox506;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox507;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox508;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox509;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox510;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox511;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox512;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox513;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox514;
        private GrapeCity.ActiveReports.SectionReportModel.Label label58;
        private GrapeCity.ActiveReports.SectionReportModel.Label label59;
        private GrapeCity.ActiveReports.SectionReportModel.Label label60;
        private GrapeCity.ActiveReports.SectionReportModel.Label label61;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox515;
        private GrapeCity.ActiveReports.SectionReportModel.Label label62;
        private GrapeCity.ActiveReports.SectionReportModel.Label label63;
        private GrapeCity.ActiveReports.SectionReportModel.Label label64;
        private GrapeCity.ActiveReports.SectionReportModel.Label label65;
        private GrapeCity.ActiveReports.SectionReportModel.Label label66;
        private GrapeCity.ActiveReports.SectionReportModel.Label label67;
        private GrapeCity.ActiveReports.SectionReportModel.Label label68;
        private GrapeCity.ActiveReports.SectionReportModel.Label label69;
        private GrapeCity.ActiveReports.SectionReportModel.Label label70;
        private GrapeCity.ActiveReports.SectionReportModel.Label label71;
        private GrapeCity.ActiveReports.SectionReportModel.Label label72;
        private GrapeCity.ActiveReports.SectionReportModel.Label label73;
        private GrapeCity.ActiveReports.SectionReportModel.Label label74;
        private GrapeCity.ActiveReports.SectionReportModel.Label label75;
        private GrapeCity.ActiveReports.SectionReportModel.Label label76;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox516;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox517;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox518;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox519;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox520;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox521;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox522;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape4;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape7;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape8;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape10;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape13;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape14;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape16;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape18;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape22;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape23;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape24;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape25;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape29;
        private GrapeCity.ActiveReports.SectionReportModel.Label label77;
        private GrapeCity.ActiveReports.SectionReportModel.Label label78;
        private GrapeCity.ActiveReports.SectionReportModel.Label label84;
        private GrapeCity.ActiveReports.SectionReportModel.Label label85;
        private GrapeCity.ActiveReports.SectionReportModel.Label label96;
        private GrapeCity.ActiveReports.SectionReportModel.Label label97;
        private GrapeCity.ActiveReports.SectionReportModel.Label label98;
        private GrapeCity.ActiveReports.SectionReportModel.Label label99;
        private GrapeCity.ActiveReports.SectionReportModel.Label label100;
        private GrapeCity.ActiveReports.SectionReportModel.Label label101;
        private GrapeCity.ActiveReports.SectionReportModel.Label label102;
        private GrapeCity.ActiveReports.SectionReportModel.Label label103;
        private GrapeCity.ActiveReports.SectionReportModel.Label label104;
        private GrapeCity.ActiveReports.SectionReportModel.Label label105;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape30;
        private GrapeCity.ActiveReports.SectionReportModel.Label label106;
        private GrapeCity.ActiveReports.SectionReportModel.Label label107;

    }
}
