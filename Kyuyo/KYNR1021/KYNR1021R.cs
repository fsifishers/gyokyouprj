﻿using System.Data;
using jp.co.fsi.common.report;

namespace jp.co.fsi.ky.kynr1021
{
    /// <summary>
    /// KYNR1021R の帳票
    /// </summary>
    public partial class KYNR1021R : BaseReport
    {
        public KYNR1021R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }
    }
}
