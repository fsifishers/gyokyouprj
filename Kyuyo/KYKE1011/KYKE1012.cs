﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.ky.kyke1011
{
    /// <summary>
    /// 給与明細入力(KYKE1012)
    /// </summary>
    public partial class KYKE1012 : BasePgForm
    {
        #region 定数
        private const string MODE_NEW  = "登録";
        private const string MODE_EDIT = "修正";
        #endregion

        #region 変数(private)
            private DateTime _nengetsu;
            private DateTime _shikyubi;
            private bool _enterAll;
            private bool _calculate;
            private int _kojoKomoku31;                                // 控除レコード項目31(扶養親族等の数)

            // クラス変数
            private DataTable _dtStiKintai;
            private DataTable _dtStiShikyu;
            private DataTable _dtStiKojo;
            private DataTable _dtStiGokei;
            private DataRow   _drKaisha;
            private DataRow   _drShain;
        #endregion

        #region プロパティ
        /// <summary>
        /// 支給月(日付は1日)
        /// </summary>
        public DateTime Nengetsu
        {
            get
            {
                return _nengetsu;
            }
            set
            {
                _nengetsu = Util.ToDate(value);
            }
        }

        /// <summary>
        /// 支給日
        /// </summary>
        public DateTime Shikyubi
        {
            get
            {
                return _shikyubi;
            }
            set
            {
                _shikyubi = Util.ToDate(value);
            }
        }
        /// <summary>
        /// 全て入力チェックボックス値
        /// </summary>
        public bool EnterAll
        {
            get
            {
                return _enterAll;
            }
            set
            {
                _enterAll = value;
            }
        }
        /// <summary>
        /// 更新時に計算チェックボックス値
        /// </summary>
        public bool Calculate
        {
            get
            {
                return _calculate;
            }
            set
            {
                _calculate = value;
            }
        }

        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KYKE1012()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)

        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 共通データ読込
            InitFormDataLoad();

            // 画面初期化
            InitDisp();

            // 支給月・支給日を表示
            SetJpDateNengetsu(Util.ConvJpDate(this.Nengetsu, this.Dba));
            SetJpDateShikyubi(Util.ConvJpDate(this.Shikyubi, this.Dba));

            // 社員コードにフォーカス
            this.txtShainCd.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtShainCd.Focus();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 社員コードに
            // フォーカス時のみF1・F12は有効、F4は無効
            // F3削除は給与データ読込結果で判定
            switch (this.ActiveCtlNm)
            {
                case "txtShainCd":
                    this.btnF1.Enabled = true;          // 検索
                    this.btnF4.Enabled = false;         // 計算
                    this.btnF12.Enabled = true;         // 日付変更
                    break;
                default:
                    this.btnF1.Enabled = false;       
                    this.btnF4.Enabled = true;       
                    this.btnF12.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // 社員データ・給与データのクリア
            ShainDataClear();
            KyuyoDataClear();

            // 社員コードにフォーカス
            this.txtShainCd.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtShainCd.Focus();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        public override void PressF1()
        {
            //アクティブコントロールごとの処理
            switch (this.ActiveCtlNm)
            {
                case "txtShainCd":
                    // アセンブリのロード
                    System.Reflection.Assembly asm = System.Reflection.Assembly.LoadFrom("KYCM1021.exe");
                    // フォーム作成
                    Type t = asm.GetType("jp.co.fsi.ky.kycm1021.KYCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";                     // ダイアログ起動パラメータ
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.txtShainCd.Text = result[0];

                                // 社員データ読込
                                ShainDataLoad(this.UInfo.KaishaCd, this.txtShainCd.Text);
                                // 給与データ読込
                                KyuyoDataLoad(this.UInfo.KaishaCd, this.txtShainCd.Text, this.Nengetsu);
                            }
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// F3キー押下時処理
        /// </summary>
        public override void PressF3()
        {
            // 社員コード未指定なら処理しない
            if (Util.ToDecimal(this.txtShainCd.Text) == 0)
            {
                return;
            }

            // 確認メッセージを表示
            string msg = "削除しますか？";
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            //// 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            // 削除用パラメータ
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
            dpc.SetParam("@SHAIN_CD", SqlDbType.Decimal, 6, this.txtShainCd.Text);
            dpc.SetParam("@NENGETSU", SqlDbType.DateTime, this.Nengetsu);
            string where = "KAISHA_CD = @KAISHA_CD";
            where += " AND SHAIN_CD = @SHAIN_CD";
            where += " AND NENGETSU = @NENGETSU";

            try
            {
                // トランザクション開始
                this.Dba.BeginTransaction();

                if (MODE_EDIT.Equals(this.lblMode.Text))
                {
                    // 給与明細データ削除
                    this.Dba.Delete("TB_KY_KYUYO_MEISAI_KINTAI", where, dpc);
                    this.Dba.Delete("TB_KY_KYUYO_MEISAI_SHIKYU", where, dpc);
                    this.Dba.Delete("TB_KY_KYUYO_MEISAI_KOJO", where, dpc);
                    this.Dba.Delete("TB_KY_KYUYO_MEISAI_GOKEI", where, dpc);
                }

                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // 編集内容クリア：ESC取消
            PressEsc();
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 社員コード未指定なら処理しない
            if (Util.ToDecimal(this.txtShainCd.Text) == 0)
            {
                return;
            }

            CalcKeisanshiki("txtKintai", _dtStiKintai, true);   // 勤怠項目の計算
            CalcKeisanshiki("txtShikyu", _dtStiShikyu, true);   // 支給項目の計算
            CalcTax();                                          // 控除項目の計算(税額)
            CalcKeisanshiki("txtKojo", _dtStiKojo, true);       // 控除項目の計算
            CalcTransferAmount();                               // 合計項目の計算(口座振込)
            CalcKeisanshiki("txtGokei", _dtStiGokei, true);     // 合計項目の計算
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        public override void PressF6()
        {
            // 社員コード未指定なら処理しない
            if (Util.ToDecimal(this.txtShainCd.Text) == 0)
            {
                return;
            }

            // 確認メッセージを表示
            string msg = (MODE_NEW.Equals(lblMode.Text) ? "登録" : "更新") + "しますか？";
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            // 更新時に計算するオプション
            if (this.Calculate)
            {
                CalcKeisanshiki("txtKintai", _dtStiKintai, true);   // 勤怠項目の計算
                CalcKeisanshiki("txtShikyu", _dtStiShikyu, true);   // 支給項目の計算
                CalcTax();                                          // 控除項目の計算(税額)
                CalcKeisanshiki("txtKojo", _dtStiKojo, true);       // 控除項目の計算
                CalcTransferAmount();                               // 合計項目の計算(口座振込)
                CalcKeisanshiki("txtGokei", _dtStiGokei, true);     // 合計項目の計算
            }

            // 入力値をバインドパラメータとしてセットする
            ArrayList alParamsKyKintai = SetKyKyuyoKintaiParams();
            ArrayList alParamsKyShikyu = SetKyKyuyoShikyuParams();
            ArrayList alParamsKyKojo   = SetKyKyuyoKojoParams();
            ArrayList alParamsKyGokei  = SetKyKyuyoGokeiParams();

            try
            {
                // トランザクション開始
                this.Dba.BeginTransaction();

                if (MODE_NEW.Equals(this.lblMode.Text))
                {
                    // 給与明細データ登録
                    this.Dba.Insert("TB_KY_KYUYO_MEISAI_KINTAI", (DbParamCollection)alParamsKyKintai[0]);   // 勤怠
                    this.Dba.Insert("TB_KY_KYUYO_MEISAI_SHIKYU", (DbParamCollection)alParamsKyShikyu[0]);   // 支給
                    this.Dba.Insert("TB_KY_KYUYO_MEISAI_KOJO", (DbParamCollection)alParamsKyKojo[0]);     // 控除
                    this.Dba.Insert("TB_KY_KYUYO_MEISAI_GOKEI", (DbParamCollection)alParamsKyGokei[0]);    // 合計
                }
                else if (MODE_EDIT.Equals(this.lblMode.Text))
                {
                    string where = "KAISHA_CD = @KAISHA_CD";
                    where += " AND SHAIN_CD = @SHAIN_CD";
                    where += " AND NENGETSU = @NENGETSU";

                    // 給与明細データ更新
                    this.Dba.Update("TB_KY_KYUYO_MEISAI_KINTAI",                                            // 勤怠
                                (DbParamCollection)alParamsKyKintai[1],
                                where,
                                (DbParamCollection)alParamsKyKintai[0]);
                    this.Dba.Update("TB_KY_KYUYO_MEISAI_SHIKYU", (DbParamCollection)alParamsKyShikyu[1],    // 支給
                                        where, (DbParamCollection)alParamsKyShikyu[0]);
                    this.Dba.Update("TB_KY_KYUYO_MEISAI_KOJO", (DbParamCollection)alParamsKyKojo[1],      // 控除
                                        where, (DbParamCollection)alParamsKyKojo[0]);
                    this.Dba.Update("TB_KY_KYUYO_MEISAI_GOKEI", (DbParamCollection)alParamsKyGokei[1],     // 合計
                                        where, (DbParamCollection)alParamsKyGokei[0]);
                }

                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // 編集内容クリア：ESC取消
            PressEsc();
        }

        /// <summary>
        /// F10キー押下時処理
        /// </summary>
        public override void PressF10()
        {
            this.Close();               // 終了
        }

        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            /// 支給情報指定フォームをダイアログ起動
            KYKE1011 frm;

            // 給与明細入力編集画面を起動
            frm = new KYKE1011();
            frm.Par1 = "1";
            frm.Nengetsu = this.Nengetsu;
            frm.Shikyubi = this.Shikyubi;
            frm.EnterAll = this.EnterAll;
            frm.Calculate = this.Calculate;
            frm.ShowDialog(this);
            if (frm.DialogResult == DialogResult.OK)
            {
                // 支給月・支給日を表示
                SetJpDateNengetsu(Util.ConvJpDate(this.Nengetsu, this.Dba));
                SetJpDateShikyubi(Util.ConvJpDate(this.Shikyubi, this.Dba));

                // 給与項目を初期化
                InitStiKyuyo();
            }
        }
        #endregion

        #region イベント
        /// <summary>
        /// 社員コードの検証
        /// </summary>
        private void txtShainCd_Validating(object sender, CancelEventArgs e)
        {
            if (IsValidShainCd())
            {
                // 給与データの読込
                if (this.txtShainCd.Modified)
                {
                    KyuyoDataLoad(this.UInfo.KaishaCd, this.txtShainCd.Text, this.Nengetsu);
                    this.txtShainCd.Modified = false;  // プロパティクリア
                }
            }
            else
            {
                e.Cancel = true;
                this.txtShainCd.Focus();
                this.txtShainCd.SelectAll();
            }
        }

        /// <summary>
        /// 勤怠項目の検証
        /// </summary>
        private void txtKintai_Validating(object sender, CancelEventArgs e)
        {
            // TextBoxの場合に実行
            if (sender is TextBox)
            {
                TextBox txtKintai = (TextBox)sender;

                // 勤怠項目の場合に実行
                if (txtKintai.Name.IndexOf("txtKintai") >= 0)
                {
                    if (IsValidKintai(txtKintai))
                    {
                        // 入力値をフォーマット(MaxLength==6で少数あり判定)
                        if (txtKintai.MaxLength == 6)
                        {
                            txtKintai.Text = Util.FormatNum(txtKintai.Text, 2);
                        }
                        else
                        {
                            txtKintai.Text = Util.FormatNum(txtKintai.Text, 0);
                        }
                    }
                    else
                    {
                        e.Cancel = true;
                        txtKintai.SelectAll();
                    }
                }
            }
        }

        /// <summary>
        /// 支給項目の検証
        /// </summary>
        private void txtShikyu_Validating(object sender, CancelEventArgs e)
        {
            // TextBoxの場合に実行
            if (sender is TextBox)
            {
                TextBox txtShikyu = (TextBox)sender;

                // 支給項目の場合に実行
                if (txtShikyu.Name.IndexOf("txtShikyu") >= 0)
                {
                    if (IsValidKintai(txtShikyu))
                    {
                        // 入力値をフォーマット
                        txtShikyu.Text = Util.FormatNum(txtShikyu.Text);

                        CalcTransferAmount();                               // 合計項目の計算(口座振込)
                        CalcKeisanshiki("txtGokei", _dtStiGokei, true);     // 合計項目の計算
                    }
                    else
                    {
                        e.Cancel = true;
                        txtShikyu.SelectAll();
                    }
                }
            }
        }

        /// <summary>
        /// 控除項目の検証
        /// </summary>
        private void txtKojo_Validating(object sender, CancelEventArgs e)
        {
            // TextBoxの場合に実行
            if (sender is TextBox)
            {
                TextBox txtKojo = (TextBox)sender;

                // 控除項目の場合に実行
                if (txtKojo.Name.IndexOf("txtKojo") >= 0)
                {
                    if (IsValidKintai(txtKojo))
                    {
                        // 入力値をフォーマット
                        txtKojo.Text = Util.FormatNum(txtKojo.Text);

                        CalcTransferAmount();                               // 合計項目の計算(口座振込)
                        CalcKeisanshiki("txtGokei", _dtStiGokei, true);     // 合計項目の計算
                    }
                    else
                    {
                        e.Cancel = true;
                        txtKojo.SelectAll();
                    }
                }
            }
        }

        /// <summary>
        /// 入力項目キーダウン処理
        /// 最終項目なら登録処理を実行する。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                jp.co.fsi.common.controls.FsiTextBox obj = (jp.co.fsi.common.controls.FsiTextBox)sender;
                if (!checkEnabled(obj.Name))
                {
                    // 最後のアクティブコントロールなら登録処理を実行する。
                    this.PressF6();
                }
            }
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 社員コードのチェック
            if (!IsValidShainCd())
            {
                this.txtShainCd.Focus();
                return false;
            }

            // 勤怠項目のチェック
            for (int i = 1; i <= 20; i++)
            {
                Control[] ctrl = this.Controls.Find("txtKintai" + Util.ToString(i), true);
                if (ctrl.Length > 0)
                {
                    if (((TextBox)ctrl[0]).Enabled)                 // 使用可能の時のみ
                    {
                        if (!IsValidKintai((TextBox)ctrl[0]))
                        {
                            ((TextBox)ctrl[0]).Focus();
                            return false;
                        }
                    }
                }
            }

            // 支給項目のチェック
            for (int i = 1; i <= 20; i++)
            {
                Control[] ctrl = this.Controls.Find("txtShikyu" + Util.ToString(i), true);
                if (ctrl.Length > 0)
                {
                    if (((TextBox)ctrl[0]).Enabled)                 // 使用可能の時のみ
                    {
                        if (!IsValidShikyu((TextBox)ctrl[0]))
                        {
                            ((TextBox)ctrl[0]).Focus();
                            return false;
                        }
                    }
                }
            }

            // 控除項目のチェック
            for (int i = 1; i <= 20; i++)
            {
                Control[] ctrl = this.Controls.Find("txtKojo" + Util.ToString(i), true);
                if (ctrl.Length > 0)
                {
                    if (((TextBox)ctrl[0]).Enabled)                 // 使用可能の時のみ
                    {
                        if (!IsValidKojo((TextBox)ctrl[0]))
                        {
                            ((TextBox)ctrl[0]).Focus();
                            return false;
                        }
                    }
                }
            }
            
            return true;
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 共通データ読込
        /// </summary>
        private void InitFormDataLoad()
        {
            // 給与明細項目設定データ
            {
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                StringBuilder cols = new StringBuilder();
                cols.Append("KOMOKU_BANGO");
                cols.Append(" ,KOMOKU_NM");
                cols.Append(" ,KOMOKU_KUBUN1");
                cols.Append(" ,KOMOKU_KUBUN2");
                cols.Append(" ,KOMOKU_KUBUN3");
                cols.Append(" ,KOMOKU_KUBUN4");
                cols.Append(" ,KOMOKU_KUBUN5");
                cols.Append(" ,NYURYOKU_KUBUN");
                cols.Append(" ,KEISAN_KUBUN");
                cols.Append(" ,KEISAN_JUNI");
                cols.Append(" ,KEISANSHIKI");
                StringBuilder where = new StringBuilder();
                where.Append("KAISHA_CD = @KAISHA_CD");
                StringBuilder order = new StringBuilder();
                order.Append("KOMOKU_BANGO");

                // 勤怠項目
                _dtStiKintai = this.Dba.GetDataTableByConditionWithParams(Util.ToString(cols)
                                , "TB_KY_KYUYO_KINTAI_KOMOKU_STI"
                                , Util.ToString(where), Util.ToString(order), dpc);
                // 支給項目
                _dtStiShikyu = this.Dba.GetDataTableByConditionWithParams(Util.ToString(cols)
                                , "TB_KY_KYUYO_SHIKYU_KOMOKU_STI"
                                , Util.ToString(where), Util.ToString(order), dpc);
                // 控除項目
                _dtStiKojo = this.Dba.GetDataTableByConditionWithParams(Util.ToString(cols)
                                , "TB_KY_KYUYO_KOJO_KOMOKU_SETTEI"
                                , Util.ToString(where), Util.ToString(order), dpc);
                // 合計項目
                _dtStiGokei = this.Dba.GetDataTableByConditionWithParams(Util.ToString(cols)
                                , "TB_KY_KYUYO_GOKEI_KOMOKU_STI"
                                , Util.ToString(where), Util.ToString(order), dpc);
            }

            // 会社情報(給与)
            {
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                StringBuilder cols = new StringBuilder();
                cols.Append("KAISHA_CD");
                cols.Append(" ,NENDO");
                cols.Append(" ,SHUGYO_NISSU");
                cols.Append(" ,SHUGYO_JIKAN");
                cols.Append(" ,FUTSU_ZANGYO_WARIMASHI_RITSU");
                cols.Append(" ,SHINYA_ZANGYO_WARIMASHI_RITSU");
                cols.Append(" ,KYUSHUTSU_FUTSU_ZNG_WRMS_RT");
                cols.Append(" ,KYUSHUTSU_SHINYA_ZNG_WRMS_RT");
                cols.Append(" ,HOTEI_FUTSU_ZNG_WRMS_RT");
                cols.Append(" ,HOTEI_SHINYA_ZNG_WRMS_RT");
                cols.Append(" ,SONOTA_ZANGYO_WARIMASHI_RITSU");
                cols.Append(" ,KEKKIN_KOJO_WARIMASHI_RITSU");
                cols.Append(" ,CHISO_KOJO_WARIMASHI_RITSU");
                cols.Append(" ,ZANGYO_TEATE_HASU_SHORI");
                cols.Append(" ,KEKKIN_KOJO_HASU_SHORI");
                cols.Append(" ,CHISO_KOJO_HASU_SHORI");
                cols.Append(" ,SHOTOKUZEI_KEISAN_HOHO");
                cols.Append(" ,KOYO_HOKEN_KEISAN_HOHO");
                cols.Append(" ,KOYO_HOKEN_GYOSHU");
                cols.Append(" ,KOYO_HOKEN_HIHOKENSHA_FTN_RT");
                cols.Append(" ,KOYO_HOKEN_HIHOKENSHA_FTN_KS");
                cols.Append(" ,KENKO_HOKEN_JIGYOSHO_SEIRI_KG");
                cols.Append(" ,KENKO_HOKEN_JIGYOSHO_BANGO");
                cols.Append(" ,KENKO_HOKEN_HIHOKENSHA_FTN_RT");
                cols.Append(" ,KENKO_HOKEN_HIHOKENSHA_FTN_KS");
                cols.Append(" ,KOSEI_NENKIN_JIGYOSHO_SEIRI_KG");
                cols.Append(" ,KOSEI_NENKIN_JIGYOSHO_BANGO");
                cols.Append(" ,KOSEI_NENKIN_HIHOKENSHA_FTN_RT");
                cols.Append(" ,KOSEI_NENKIN_HIHOKENSHA_FTN_KS");
                cols.Append(" ,GINKO_CD");
                cols.Append(" ,SHITEN_CD");
                cols.Append(" ,YOKIN_SHUMOKU");
                cols.Append(" ,KOZA_BANGO");
                cols.Append(" ,KOZA_MEIGININ_KANJI");
                cols.Append(" ,KOZA_MEIGININ_KANA");
                cols.Append(" ,ITAKU_CD");
                cols.Append(" ,SHUMOKU_CD");
                cols.Append(" ,REGIST_DATE");
                cols.Append(" ,UPDATE_DATE");
                cols.Append(" ,KNKHKN_HIHOKENSHA_FTN_RT_KIG");
                cols.Append(" ,KNKHKN_HIHOKENSHA_FTN_KS_KIG");
                StringBuilder where = new StringBuilder();
                where.Append("KAISHA_CD = @KAISHA_CD");

                // 会社情報
                DataTable dtKaisha = this.Dba.GetDataTableByConditionWithParams(Util.ToString(cols)
                                , "TB_KY_KAISHA_JOHO"
                                , Util.ToString(where), dpc);
                if ( dtKaisha.Rows.Count == 1)
                {
                    _drKaisha = dtKaisha.Rows[0];
                }
            }
        }

        /// <summary>
        /// 画面初期化
        /// </summary>
        private void InitDisp()
        {
            // 項目名・入力可否の初期値
            for (int i = 1; i <= 20; i++)
            {
                SetLblTextByNm("lblKintai" + Util.ToString(i), "");             // 項目名ラベル・勤怠
                SetLblTextByNm("lblShikyu" + Util.ToString(i), "");             // 項目名ラベル・支給
                SetLblTextByNm("lblKojo" + Util.ToString(i), "");               // 項目名ラベル・控除
                SetTxtEnabledByNm("txtKintai" + Util.ToString(i), false);       // 入力可否・勤怠
                SetTxtEnabledByNm("txtShikyu" + Util.ToString(i), false);       // 入力可否・支給
                SetTxtEnabledByNm("txtKojo" + Util.ToString(i), false);         // 入力可否・控除
                if (i <= 10)
                {
                    SetLblTextByNm("lblGokei" + Util.ToString(i), "");          // 項目名ラベル・合計
                    SetTxtEnabledByNm("txtGokei" + Util.ToString(i), false);    // 入力可否・合計
                }
            }
            
            // 給与項目の設定
            InitStiKyuyo();

            /// 入力項目クリア
            ShainDataClear();
            KyuyoDataClear(); 
        }

        /// <summary>
        /// 給与項目の初期化
        /// </summary>
        private void InitStiKyuyo()
        {
            // 項目名・入力可否の設定
            // (入力制限は全ての項目を入力オプションと項目設定テーブル.入力区分で判定する)
            foreach (DataRow dr in _dtStiKintai.Rows)
            {
                // 勤怠項目
                SetLblTextByNm("lblKintai" + Util.ToString(dr["KOMOKU_BANGO"]), Util.ToString(dr["KOMOKU_NM"]));
                SetTxtEnabledByNm("txtKintai" + Util.ToString(dr["KOMOKU_BANGO"]),
                                        this.EnterAll ? true : Util.ToString(dr["NYURYOKU_KUBUN"]).Equals("1"));
                SetTxtMaxLengthByNm("txtKintai" + Util.ToString(dr["KOMOKU_BANGO"]),
                                        Util.ToString(dr["KOMOKU_KUBUN2"]).Equals("0") ? 6 : 10);
            }
            foreach (DataRow dr in _dtStiShikyu.Rows)
            {
                // 支給項目
                SetLblTextByNm("lblShikyu" + Util.ToString(dr["KOMOKU_BANGO"]), Util.ToString(dr["KOMOKU_NM"]));
                SetTxtEnabledByNm("txtShikyu" + Util.ToString(dr["KOMOKU_BANGO"]),
                                        this.EnterAll ? true : Util.ToString(dr["NYURYOKU_KUBUN"]).Equals("1"));
            }
            foreach (DataRow dr in _dtStiKojo.Rows)
            {
                // 控除項目
                SetLblTextByNm("lblKojo" + Util.ToString(dr["KOMOKU_BANGO"]), Util.ToString(dr["KOMOKU_NM"]));
                SetTxtEnabledByNm("txtKojo" + Util.ToString(dr["KOMOKU_BANGO"]),
                                        this.EnterAll ? true : Util.ToString(dr["NYURYOKU_KUBUN"]).Equals("1"));
            }
            foreach (DataRow dr in _dtStiGokei.Rows)
            {
                // 合計項目
                SetLblTextByNm("lblGokei" + Util.ToString(dr["KOMOKU_BANGO"]), Util.ToString(dr["KOMOKU_NM"]));
                SetTxtEnabledByNm("txtGokei" + Util.ToString(dr["KOMOKU_BANGO"]), false);
            }
        }

        /// <summary>
        /// 社員データの読込
        /// </summary>
        /// <param name="kaishyaCd">会社コード</param>
        /// <param name="shainCd">社員コード</param>
        /// <returns>true/false</returns>
        private bool ShainDataLoad(string kaishyaCd, string shainCd)
        {
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, kaishyaCd);
            dpc.SetParam("@SHAIN_CD", SqlDbType.Decimal, 6, shainCd);

            StringBuilder cols = new StringBuilder();
            #region cols.Append(社員情報)
            cols.Append("KAISHA_CD");
            cols.Append(" ,SHAIN_CD");
            cols.Append(" ,SHAIN_NM");
            cols.Append(" ,SHAIN_KANA_NM");
            cols.Append(" ,SEIBETSU");
            cols.Append(" ,SEINENGAPPI");
            cols.Append(" ,YUBIN_BANGO1");
            cols.Append(" ,YUBIN_BANGO2");
            cols.Append(" ,JUSHO1");
            cols.Append(" ,JUSHO2");
            cols.Append(" ,DENWA_BANGO");
            cols.Append(" ,BUMON_CD");
            cols.Append(" ,BUMON_NM");
            cols.Append(" ,BUKA_CD");
            cols.Append(" ,BUKA_NM");
            cols.Append(" ,YAKUSHOKU_CD");
            cols.Append(" ,YAKUSHOKU_NM");
            cols.Append(" ,KYUYO_KEITAI");
            cols.Append(" ,KYUYO_KEITAI_NM");
            cols.Append(" ,NYUSHA_NENGAPPI");
            cols.Append(" ,TAISHOKU_NENGAPPI");
            cols.Append(" ,KENKO_HOKENSHO_BANGO");
            cols.Append(" ,KENKO_HOKEN_TOKYU");
            cols.Append(" ,KENKO_HOKEN_HYOJUN_HOSHU_GTGK");
            cols.Append(" ,KENKO_HOKENRYO");
            cols.Append(" ,KENKO_HOKEN_SHOYO_KEISAN_KUBUN");
            cols.Append(" ,KOSEI_NENKIN_BANGO");
            cols.Append(" ,KOSEI_NENKIN_TOKYU");
            cols.Append(" ,KOSEI_NENKIN_HYOJUN_HOSHU_GTGK");
            cols.Append(" ,KOSEI_NENKIN_HOKENRYO");
            cols.Append(" ,KOSEI_NENKIN_SHOYO_KEISAN_KBN");
            cols.Append(" ,KENKO_HOKEN_KANYU_KUBUN");
            cols.Append(" ,KOSEI_NENKIN_KANYU_KUBUN");
            cols.Append(" ,KOSEI_NENKIN_SHUBETSU");
            cols.Append(" ,KOSEI_NENKIN_SHUBETSU_NM");
            cols.Append(" ,SHAHO_JUZEN_KAITEI_TSUKI");
            cols.Append(" ,SHAHO_JUZEN_KAITEI_GENIN");
            cols.Append(" ,KOYO_HOKEN_KEISAN_KUBUN");
            cols.Append(" ,JUMINZEI_SHICHOSON_CD");
            cols.Append(" ,JUMINZEI_SHICHOSON_NM");
            cols.Append(" ,JUMINZEI_6GATSUBUN");
            cols.Append(" ,JUMINZEI_7GATSUBUN_IKO");
            cols.Append(" ,ZEIGAKU_HYO");
            cols.Append(" ,NENMATSU_CHOSEI");
            cols.Append(" ,HONNIN_KUBUN1");
            cols.Append(" ,HONNIN_KUBUN1_NM");
            cols.Append(" ,HONNIN_KUBUN2");
            cols.Append(" ,HONNIN_KUBUN2_NM");
            cols.Append(" ,HONNIN_KUBUN3");
            cols.Append(" ,HONNIN_KUBUN3_NM");
            cols.Append(" ,HAIGUSHA_KUBUN");
            cols.Append(" ,HAIGUSHA_KUBUN_NM");
            cols.Append(" ,FUYO_IPPAN");
            cols.Append(" ,FUYO_NENSHO");
            cols.Append(" ,FUYO_TOKUTEI");
            cols.Append(" ,FUYO_ROJIN_DOKYO_ROSHINTO_IGAI");
            cols.Append(" ,FUYO_ROJIN_DOKYO_ROSHINTO");
            cols.Append(" ,FUYO_DOKYO_TOKUSHO_IPPAN");
            cols.Append(" ,FUYO_DOKYO_TOKUSHO_NENSHO");
            cols.Append(" ,FUYO_DOKYO_TOKUSHO_TOKUTEI");
            cols.Append(" ,FUYO_DOKYO_TKS_DOKYO_RSNT_IGAI");
            cols.Append(" ,FUYO_DOKYO_TKS_DOKYO_RSNT");
            cols.Append(" ,SHOGAISHA_IPPAN");
            cols.Append(" ,SHOGAISHA_TOKUBETSU");
            cols.Append(" ,OTTO_ARI");
            cols.Append(" ,MISEINENSHA");
            cols.Append(" ,SHIBO_TAISHOKU");
            cols.Append(" ,SAIGAISHA");
            cols.Append(" ,GAIKOKUJIN");
            cols.Append(" ,TEKIYO1");
            cols.Append(" ,TEKIYO2");
            cols.Append(" ,SAI16_MIMAN");
            cols.Append(" ,KYUYO_SHUBETSU");
            cols.Append(" ,TEATE_SHIKYU_KOMOKU1");
            cols.Append(" ,TEATE_SHIKYU_KOMOKU2");
            cols.Append(" ,TEATE_SHIKYU_KOMOKU3");
            cols.Append(" ,TEATE_SHIKYU_KOMOKU4");
            cols.Append(" ,TEATE_SHIKYU_KOMOKU5");
            cols.Append(" ,TEATE_SHIKYU_KOMOKU6");
            cols.Append(" ,TEATE_SHIKYU_KOMOKU7");
            cols.Append(" ,TEATE_SHIKYU_KOMOKU8");
            cols.Append(" ,TEATE_SHIKYU_KOMOKU9");
            cols.Append(" ,TEATE_SHIKYU_KOMOKU10");
            cols.Append(" ,TEATE_SHIKYU_KOMOKU11");
            cols.Append(" ,TEATE_SHIKYU_KOMOKU12");
            cols.Append(" ,TEATE_SHIKYU_KOMOKU13");
            cols.Append(" ,TEATE_SHIKYU_KOMOKU14");
            cols.Append(" ,TEATE_SHIKYU_KOMOKU15");
            cols.Append(" ,TEATE_SHIKYU_KOMOKU16");
            cols.Append(" ,TEATE_SHIKYU_KOMOKU17");
            cols.Append(" ,TEATE_SHIKYU_KOMOKU18");
            cols.Append(" ,TEATE_SHIKYU_KOMOKU19");
            cols.Append(" ,TEATE_SHIKYU_KOMOKU20");
            cols.Append(" ,TEATE_SHIKYU_KOMOKU21");
            cols.Append(" ,TEATE_SHIKYU_KOMOKU22");
            cols.Append(" ,TEATE_SHIKYU_KOMOKU23");
            cols.Append(" ,TEATE_SHIKYU_KOMOKU24");
            cols.Append(" ,TEATE_SHIKYU_KOMOKU25");
            cols.Append(" ,TEATE_SHIKYU_KOMOKU26");
            cols.Append(" ,TEATE_SHIKYU_KOMOKU27");
            cols.Append(" ,TEATE_SHIKYU_KOMOKU28");
            cols.Append(" ,TEATE_SHIKYU_KOMOKU29");
            cols.Append(" ,TEATE_SHIKYU_KOMOKU30");
            cols.Append(" ,TEATE_KOJO_KOMOKU1");
            cols.Append(" ,TEATE_KOJO_KOMOKU2");
            cols.Append(" ,TEATE_KOJO_KOMOKU3");
            cols.Append(" ,TEATE_KOJO_KOMOKU4");
            cols.Append(" ,TEATE_KOJO_KOMOKU5");
            cols.Append(" ,TEATE_KOJO_KOMOKU6");
            cols.Append(" ,TEATE_KOJO_KOMOKU7");
            cols.Append(" ,TEATE_KOJO_KOMOKU8");
            cols.Append(" ,TEATE_KOJO_KOMOKU9");
            cols.Append(" ,TEATE_KOJO_KOMOKU10");
            cols.Append(" ,TEATE_KOJO_KOMOKU11");
            cols.Append(" ,TEATE_KOJO_KOMOKU12");
            cols.Append(" ,TEATE_KOJO_KOMOKU13");
            cols.Append(" ,TEATE_KOJO_KOMOKU14");
            cols.Append(" ,TEATE_KOJO_KOMOKU15");
            cols.Append(" ,TEATE_KOJO_KOMOKU16");
            cols.Append(" ,TEATE_KOJO_KOMOKU17");
            cols.Append(" ,TEATE_KOJO_KOMOKU18");
            cols.Append(" ,TEATE_KOJO_KOMOKU19");
            cols.Append(" ,TEATE_KOJO_KOMOKU20");
            cols.Append(" ,KYUYO_SHIKYU_HOHO1");
            cols.Append(" ,KYUYO_SHIKYU_KINGAKU1");
            cols.Append(" ,KYUYO_SHIKYU_RITSU1");
            cols.Append(" ,KYUYO_GINKO_CD1");
            cols.Append(" ,KYUYO_GINKO_NM1");
            cols.Append(" ,KYUYO_SHITEN_CD1");
            cols.Append(" ,KYUYO_SHITEN_NM1");
            cols.Append(" ,KYUYO_YOKIN_SHUMOKU1");
            cols.Append(" ,KYUYO_KOZA_BANGO1");
            cols.Append(" ,KYUYO_KOZA_MEIGININ_KANJI1");
            cols.Append(" ,KYUYO_KOZA_MEIGININ_KANA1");
            cols.Append(" ,KYUYO_KEIYAKUSHA_BANGO1");
            cols.Append(" ,KYUYO_SHIKYU_HOHO2");
            cols.Append(" ,KYUYO_SHIKYU_KINGAKU2");
            cols.Append(" ,KYUYO_SHIKYU_RITSU2");
            cols.Append(" ,KYUYO_GINKO_CD2");
            cols.Append(" ,KYUYO_GINKO_NM2");
            cols.Append(" ,KYUYO_SHITEN_CD2");
            cols.Append(" ,KYUYO_SHITEN_NM2");
            cols.Append(" ,KYUYO_YOKIN_SHUMOKU2");
            cols.Append(" ,KYUYO_KOZA_BANGO2");
            cols.Append(" ,KYUYO_KOZA_MEIGININ_KANJI2");
            cols.Append(" ,KYUYO_KOZA_MEIGININ_KANA2");
            cols.Append(" ,KYUYO_KEIYAKUSHA_BANGO2");
            cols.Append(" ,KAIGO_HOKEN_KUBUN");
            #endregion
            StringBuilder where = new StringBuilder("KAISHA_CD = @KAISHA_CD");
            where.Append(" AND SHAIN_CD = @SHAIN_CD");

            // 社員情報データ
            DataTable dtShain = this.Dba.GetDataTableByConditionWithParams(Util.ToString(cols)
                            , "VI_KY_SHAIN_JOHO"
                            , Util.ToString(where), dpc);

            // 該当レコードが１件の場合に正常
            if ( dtShain.Rows.Count == 1 )
            {

                // 社員情報データ
                _drShain = dtShain.Rows[0];

                // 在職・退職チェック
                if (_drShain["TAISHOKU_NENGAPPI"] != DBNull.Value)
                {
                    // 退職年月日を和暦に変換
                    string[] datetime = Util.ConvJpDate(Util.ToDate(_drShain["TAISHOKU_NENGAPPI"]), this.Dba);
                    string showDate = datetime[5];
                    // 確認メッセージを表示
                    string msg = "既に退職していますが宜しいですか？\n" +
                                    "\n氏      名：" + Util.ToString(_drShain["SHAIN_NM"]) +
                                    "\n退職年月日：" + showDate;
                    if (Msg.ConfYesNo(msg) == DialogResult.No)
                    {
                        // 「いいえ」を押されたら処理終了
                        return false;
                    }
                }

                // 社員情報を画面表示
                txtShainCd.Text = Util.ToString(Util.ToDecimal(txtShainCd.Text));
                lblShainNm.Text = Util.ToString(_drShain["SHAIN_NM"]);
                lblShyozoku.Text = Util.ToString(_drShain["BUMON_CD"]) + "-" + Util.ToString(_drShain["BUKA_CD"]);
                lblKyuyokeitai.Text = Util.ToString(_drShain["KYUYO_KEITAI_NM"]);
                
                // 戻り値
                return true;
            }
            else
            {
                // 戻り値
                return false;
            }
        }

        /// <summary>
        /// 給与データの読込
        /// </summary>
        /// <param name="kaishyaCd">会社コード</param>
        /// <param name="shainCd">社員コード</param>
        /// <param name="nengetsu">年月</param>
        private void KyuyoDataLoad(string kaishyaCd, string shainCd, DateTime nengetsu)
        {
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, kaishyaCd);
            dpc.SetParam("@SHAIN_CD", SqlDbType.Decimal, 6, shainCd);
            dpc.SetParam("@NENGETSU", SqlDbType.DateTime, nengetsu);
            StringBuilder cols = new StringBuilder();

            cols.Append("KAISHA_CD");
            cols.Append(" ,SHAIN_CD");
            cols.Append(" ,NENGETSU");
            cols.Append(" ,KAIKEI_NENDO");
            cols.Append(" ,KOMOKU1");
            cols.Append(" ,KOMOKU2");
            cols.Append(" ,KOMOKU3");
            cols.Append(" ,KOMOKU4");
            cols.Append(" ,KOMOKU5");
            cols.Append(" ,KOMOKU6");
            cols.Append(" ,KOMOKU7");
            cols.Append(" ,KOMOKU8");
            cols.Append(" ,KOMOKU9");
            cols.Append(" ,KOMOKU10");  // (※合計データは項目1～10の利用だが同様にする)
            cols.Append(" ,KOMOKU11");
            cols.Append(" ,KOMOKU12");
            cols.Append(" ,KOMOKU13");
            cols.Append(" ,KOMOKU14");
            cols.Append(" ,KOMOKU15");
            cols.Append(" ,KOMOKU16");
            cols.Append(" ,KOMOKU17");
            cols.Append(" ,KOMOKU18");
            cols.Append(" ,KOMOKU19");
            cols.Append(" ,KOMOKU20");  // 項目1～20までの参照を行う(項目21～35省略)
            cols.Append(" ,KOMOKU31");  // 明示されない更新フィールド(控除レコード.扶養親族等の数)
            cols.Append(" ,NYURYOKU_F");
            cols.Append(" ,KEISAN_F");
            cols.Append(" ,SHIKYUBI");
            cols.Append(" ,REGIST_DATE");
            cols.Append(" ,UPDATE_DATE");
            StringBuilder where = new StringBuilder("KAISHA_CD = @KAISHA_CD");
            where.Append(" AND SHAIN_CD = @SHAIN_CD");
            where.Append(" AND NENGETSU = @NENGETSU");

            // 給与明細勤怠データ
            DataTable dtKMKintai = this.Dba.GetDataTableByConditionWithParams(Util.ToString(cols)
                                    , "TB_KY_KYUYO_MEISAI_KINTAI"
                                    , Util.ToString(where), dpc);
            // 給与明細支給データ
            DataTable dtKMShikyu = this.Dba.GetDataTableByConditionWithParams(Util.ToString(cols)
                                    , "TB_KY_KYUYO_MEISAI_SHIKYU"
                                    , Util.ToString(where), dpc);
            // 給与明細控除データ
            DataTable dtKMKojo   = this.Dba.GetDataTableByConditionWithParams(Util.ToString(cols)
                                    , "TB_KY_KYUYO_MEISAI_KOJO"
                                    , Util.ToString(where), dpc);
            // 給与明細合計データ
            DataTable dtKMGokei  = this.Dba.GetDataTableByConditionWithParams(Util.ToString(cols)
                                    , "TB_KY_KYUYO_MEISAI_GOKEI"
                                    , Util.ToString(where), dpc);

            // 登録済データが各1レコード存在すれば画面上へ表示
            if (dtKMKintai.Rows.Count == 1 && dtKMShikyu.Rows.Count == 1 && dtKMKojo.Rows.Count == 1 && dtKMGokei.Rows.Count == 1)
            {
                // 登録データの修正
                lblMode.Text = MODE_EDIT;

                // ファンクションキー制御　F3削除使用可
                this.btnF3.Enabled = true;       
                
                // 項目設定が存在する保存値をセットする
                foreach (DataRow dr in _dtStiKintai.Rows)
                {
                    // 勤怠項目
                    SetTxtTextByNm("txtKintai" + Util.ToString(dr["KOMOKU_BANGO"]),
                            Util.ToString(dtKMKintai.Rows[0]["KOMOKU" + Util.ToString(dr["KOMOKU_BANGO"])]));

                }
                foreach (DataRow dr in _dtStiShikyu.Rows)
                {
                    // 支給項目
                    SetTxtTextByNm("txtShikyu" + Util.ToString(dr["KOMOKU_BANGO"]),
                            Util.ToString(dtKMShikyu.Rows[0]["KOMOKU" + Util.ToString(dr["KOMOKU_BANGO"])]));
                }
                foreach (DataRow dr in _dtStiKojo.Rows)
                {
                    // 控除項目
                    SetTxtTextByNm("txtKojo" + Util.ToString(dr["KOMOKU_BANGO"]),
                            Util.ToString(dtKMKojo.Rows[0]["KOMOKU" + Util.ToString(dr["KOMOKU_BANGO"])]));
                }
                foreach (DataRow dr in _dtStiGokei.Rows)
                {
                    // 合計項目
                    SetTxtTextByNm("txtGokei" + Util.ToString(dr["KOMOKU_BANGO"]),
                            Util.ToString(dtKMGokei.Rows[0]["KOMOKU" + Util.ToString(dr["KOMOKU_BANGO"])]));
                }

                // 支給日
                this.Shikyubi = Util.ToDate(dtKMGokei.Rows[0]["SHIKYUBI"]);
                SetJpDateShikyubi(Util.ConvJpDate(this.Shikyubi, this.Dba));

                // 控除レコード扶養親族等の数
                if (Util.ToInt(dtKMKojo.Rows[0]["KOMOKU31"]) > 0)
                {
                    _kojoKomoku31 = Util.ToInt(dtKMKojo.Rows[0]["KOMOKU31"]);
                }
                else
                {
                    _kojoKomoku31 = 0;
                }
            }
            else
            {
                // 新規データ登録
                lblMode.Text = MODE_NEW;

                // ファンクションキー制御　F3削除
                this.btnF3.Enabled = false;       

                // 各項目の入力初期値をセット
                KyuyoDataClear();

                // 固定項目の計算
                CalcKeisanshiki("txtKintai", _dtStiKintai, false);  // 勤怠項目の計算
                CalcKeisanshiki("txtShikyu", _dtStiShikyu, false);  // 支給項目の計算
                CalcTax();                                          // 控除項目の計算(税額)
                CalcKeisanshiki("txtKojo", _dtStiKojo, false);      // 控除項目の計算
                CalcTransferAmount();                               // 合計項目の計算(口座振込)
                CalcKeisanshiki("txtGokei", _dtStiGokei, true);     // 合計項目の計算
            }
        }

        /// <summary>
        /// 社員データのクリア
        /// </summary>
        private void ShainDataClear()
        {
            // 入力・表示項目クリア
            lblMode.Text = "";
            txtShainCd.Text = "0";
            lblShainNm.Text = "";
            lblShyozoku.Text = "";
            lblKyuyokeitai.Text = "";
            txtShainCd.SelectAll();

            // ファンクションキー制御　F3削除
            this.btnF3.Enabled = false;       
        }

        /// <summary>
        /// 給与データのクリア
        /// </summary>
        private void KyuyoDataClear()
        {
            // 各項目の入力初期値をセット
            foreach (DataRow dr in _dtStiKintai.Rows)
            {
                // 勤怠項目初期値：項目区分2(0少数あり　1少数なし)
                SetTxtTextByNm("txtKintai" + Util.ToString(dr["KOMOKU_BANGO"]),
                                        Util.ToString(dr["KOMOKU_KUBUN2"]).Equals("0") ? "0.00" : "0");
            }
            foreach (DataRow dr in _dtStiShikyu.Rows)
            {
                // 支給項目
                SetTxtTextByNm("txtShikyu" + Util.ToString(dr["KOMOKU_BANGO"]), "0");
            }
            foreach (DataRow dr in _dtStiKojo.Rows)
            {
                // 控除項目
                SetTxtTextByNm("txtKojo" + Util.ToString(dr["KOMOKU_BANGO"]), "0");
            }
            foreach (DataRow dr in _dtStiGokei.Rows)
            {
                // 合計項目
                SetTxtTextByNm("txtGokei" + Util.ToString(dr["KOMOKU_BANGO"]), "0");
            }
        }

        /// <summary>
        /// コントロール名でLabel.Text値の設定
        /// </summary>
        /// <param name="controlNm">コントロール名</param>
        /// <param name="value">値</param>
        private void SetLblTextByNm(string controlNm, string value)
        {
            Control[] ctrl = this.Controls.Find(controlNm, true);
            if (ctrl.Length > 0) ((Label)ctrl[0]).Text = value;
        }

        /// <summary>
        /// コントロール名指定でTextBox.Enabled値の設定
        /// </summary>
        /// <param name="controlNm">コントロール名</param>
        /// <param name="value">値</param>
        private void SetTxtEnabledByNm(string controlNm, bool value)
        {
            Control[] ctrl = this.Controls.Find(controlNm, true);
            if (ctrl.Length > 0)
            {
                ((TextBox)ctrl[0]).Enabled = value;
                ((TextBox)ctrl[0]).BackColor = Color.White;         // 背景色グレー回避のため
            }
        }

        /// <summary>
        /// コントロール名指定でTextBox.MaxLength値の設定
        /// </summary>
        /// <param name="controlNm">コントロール名</param>
        /// <param name="value">値</param>
        private void SetTxtMaxLengthByNm(string controlNm, int value)
        {
            Control[] ctrl = this.Controls.Find(controlNm, true);
            if (ctrl.Length > 0)
            {
                ((TextBox)ctrl[0]).MaxLength = value;
            }
        }

        /// <summary>
        /// コントロール名指定でTextBox.Text値の設定
        /// </summary>
        /// <param name="controlNm">コントロール名</param>
        /// <param name="value">値</param>
        private void SetTxtTextByNm(string controlNm, string value)
        {
            Control[] ctrl = this.Controls.Find(controlNm, true);
            if (ctrl.Length > 0)
            {
                if (((TextBox)ctrl[0]).MaxLength == 6)
                {
                    ((TextBox)ctrl[0]).Text = Util.FormatNum(value, 2);
                }
                else
                {
                    ((TextBox)ctrl[0]).Text = Util.FormatNum(value);
                }
            }
        }

        /// <summary>
        /// コントロール名指定でTextBox.Text値の取得
        /// </summary>
        /// <param name="controlNm">コントロール名</param>
        private string GetTxtTextByNm(string controlNm)
        {
            string ret = "";

            Control[] ctrl = this.Controls.Find(controlNm, true);
            if (ctrl.Length > 0)
            {
                ret = ((TextBox)ctrl[0]).Text;
            }
            return ret;
        }

        /// <summary>
        /// 配列に格納された支給日和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpDateShikyubi(string[] arrJpDate)
        {
            this.lblGengoShikyubi.Text = arrJpDate[0];
            this.lblGengoYearShikyubi.Text = arrJpDate[2];
            this.lblMonthShikyubi.Text = arrJpDate[3];
            this.lblDayShikyubi.Text = arrJpDate[4];
        }

        /// <summary>
        /// 配列に格納された支給月和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpDateNengetsu(string[] arrJpDate)
        {
            this.lblGengoNengetsu.Text = arrJpDate[0];
            this.lblGengoYearNengetsu.Text = arrJpDate[2];
            this.lblMonthNengetsu.Text = arrJpDate[3];
        }

        /// <summary>
        /// 社員コードの入力チェック
        /// </summary>
        /// <returns>true/false</returns>
        private bool IsValidShainCd()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtShainCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            //// ゼロ値入力は社員情報クリア
            //if (Util.ToDecimal(this.txtShainCd.Text) == 0)
            //{
            //    // 入力項目クリア
            //    ShainDataClear();
            //    KyuyoDataClear();

            //    return true;
            //}
            // 空 又は 0入力の場合
            if (ValChk.IsEmpty(this.txtShainCd.Text) || Equals(this.txtShainCd.Text, "0"))
            {
                // 水揚支所名称を表示する
                this.txtShainCd.Text = "0";
                // 入力項目クリア
                ShainDataClear();
                KyuyoDataClear();
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 存在しないコードを入力されたらエラー
            if (ShainDataLoad(this.UInfo.KaishaCd, this.txtShainCd.Text) )
            {
                return true;
            }
            else
            {
                Msg.Error("該当データはありません。");
                return false;
            }
        }

        /// <summary>
        /// 勤怠項目の入力チェック
        /// </summary>
        /// <param name="txtKintai">勤怠項目テキストボックス</param>
        /// <returns>true/false</returns>
        private bool IsValidKintai(TextBox txtKintai)
        {
            bool ret = true;

            // 少数部利用ありなし判定(MaxLength:6少数あり)
            if (txtKintai.MaxLength == 6)
            {
                if (!ValChk.IsDecNumWithinLength(txtKintai.Text, 3, 2, true)) 
                    ret = false;
            }
            else
            {
                if (!ValChk.IsDecNumWithinLength(txtKintai.Text, 8, 0, true))
                    ret = false;
            }

            if (!ret)
            {
                Msg.Error("入力に誤りがあります。");
            }

            return ret;
        }

        /// <summary>
        /// 支給項目の入力チェック
        /// </summary>
        /// <param name="txtShikyu">支給項目テキストボックス</param>
        /// <returns>true/false</returns>
        private bool IsValidShikyu(TextBox txtShikyu)
        {
            bool ret = true;

            if (!ValChk.IsDecNumWithinLength(txtShikyu.Text, 8, 0, true))
                ret = false;

            if (!ret)
            {
                Msg.Error("入力に誤りがあります。");
            }

            return ret;
        }

        /// <summary>
        /// 控除項目の入力チェック
        /// </summary>
        /// <param name="txtKojo">控除項目テキストボックス</param>
        /// <returns>true/false</returns>
        private bool IsValidKojo(TextBox txtKojo)
        {
            bool ret = true;

            if (!ValChk.IsDecNumWithinLength(txtKojo.Text, 8, 0, true))
                ret = false;

            if (!ret)
            {
                Msg.Error("入力に誤りがあります。");
            }

            return ret;
        }

        /// <summary>
        /// TB_KY_KYUYO_MEISAI_KINTAIに更新するためのパラメータ設定をします。
        /// </summary>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// 更新処理：DbParamCollection*2(Where句,Set句)
        /// </returns>
        private ArrayList SetKyKyuyoKintaiParams()
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            if (MODE_NEW.Equals(lblMode.Text))
            {
                // 更新パラメータ設定
                updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
                updParam.SetParam("@SHAIN_CD", SqlDbType.Decimal, 6, this.txtShainCd.Text);
                updParam.SetParam("@NENGETSU", SqlDbType.DateTime, this.Nengetsu);
                // 登録日
                updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWDATE");
            }
            else if (MODE_EDIT.Equals(lblMode.Text))
            {
                // Where句のパラメータに設定
                DbParamCollection whereParam = new DbParamCollection();
                whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
                whereParam.SetParam("@SHAIN_CD", SqlDbType.Decimal, 6, this.txtShainCd.Text);
                whereParam.SetParam("@NENGETSU", SqlDbType.DateTime, this.Nengetsu);
                alParams.Add(whereParam);
            }

            // 会計年度：年月の属する会計年度をセット
            updParam.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, GetKaikeiNendo(this.Nengetsu));
            
            // 項目1～20
            updParam.SetParam("@KOMOKU1", SqlDbType.Decimal, 11, 2, Util.ToDecimal(this.txtKintai1.Text));
            updParam.SetParam("@KOMOKU2", SqlDbType.Decimal, 11, 2, Util.ToDecimal(this.txtKintai2.Text));
            updParam.SetParam("@KOMOKU3", SqlDbType.Decimal, 11, 2, Util.ToDecimal(this.txtKintai3.Text));
            updParam.SetParam("@KOMOKU4", SqlDbType.Decimal, 11, 2, Util.ToDecimal(this.txtKintai4.Text));
            updParam.SetParam("@KOMOKU5", SqlDbType.Decimal, 11, 2, Util.ToDecimal(this.txtKintai5.Text));
            updParam.SetParam("@KOMOKU6", SqlDbType.Decimal, 11, 2, Util.ToDecimal(this.txtKintai6.Text));
            updParam.SetParam("@KOMOKU7", SqlDbType.Decimal, 11, 2, Util.ToDecimal(this.txtKintai7.Text));
            updParam.SetParam("@KOMOKU8", SqlDbType.Decimal, 11, 2, Util.ToDecimal(this.txtKintai8.Text));
            updParam.SetParam("@KOMOKU9", SqlDbType.Decimal, 11, 2, Util.ToDecimal(this.txtKintai9.Text));
            updParam.SetParam("@KOMOKU10", SqlDbType.Decimal, 11, 2, Util.ToDecimal(this.txtKintai10.Text));
            updParam.SetParam("@KOMOKU11", SqlDbType.Decimal, 11, 2, Util.ToDecimal(this.txtKintai11.Text));
            updParam.SetParam("@KOMOKU12", SqlDbType.Decimal, 11, 2, Util.ToDecimal(this.txtKintai12.Text));
            updParam.SetParam("@KOMOKU13", SqlDbType.Decimal, 11, 2, Util.ToDecimal(this.txtKintai13.Text));
            updParam.SetParam("@KOMOKU14", SqlDbType.Decimal, 11, 2, Util.ToDecimal(this.txtKintai14.Text));
            updParam.SetParam("@KOMOKU15", SqlDbType.Decimal, 11, 2, Util.ToDecimal(this.txtKintai15.Text));
            updParam.SetParam("@KOMOKU16", SqlDbType.Decimal, 11, 2, Util.ToDecimal(this.txtKintai16.Text));
            updParam.SetParam("@KOMOKU17", SqlDbType.Decimal, 11, 2, Util.ToDecimal(this.txtKintai17.Text));
            updParam.SetParam("@KOMOKU18", SqlDbType.Decimal, 11, 2, Util.ToDecimal(this.txtKintai18.Text));
            updParam.SetParam("@KOMOKU19", SqlDbType.Decimal, 11, 2, Util.ToDecimal(this.txtKintai19.Text));
            updParam.SetParam("@KOMOKU20", SqlDbType.Decimal, 11, 2, Util.ToDecimal(this.txtKintai20.Text));
            // 入力フラグ
            updParam.SetParam("@NYURYOKU_F", SqlDbType.Decimal, 1, this.EnterAll);
            // 計算フラグ
            updParam.SetParam("@KEISAN_F", SqlDbType.Decimal, 1, this.Calculate);
            // 支給日
            updParam.SetParam("@SHIKYUBI", SqlDbType.DateTime, this.Shikyubi);
            // 更新日
            updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWDATE");

            alParams.Add(updParam);

            return alParams;
        }

        /// <summary>
        /// TB_KY_KYUYO_MEISAI_SHIKYUに更新するためのパラメータ設定をします。
        /// </summary>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// 更新処理：DbParamCollection*2(Where句,Set句)
        /// </returns>
        private ArrayList SetKyKyuyoShikyuParams()
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            if (MODE_NEW.Equals(lblMode.Text))
            {
                // 更新パラメータ設定
                updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
                updParam.SetParam("@SHAIN_CD", SqlDbType.Decimal, 6, this.txtShainCd.Text);
                updParam.SetParam("@NENGETSU", SqlDbType.DateTime, this.Nengetsu);
                // 登録日
                updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWDATE");
            }
            else if (MODE_EDIT.Equals(lblMode.Text))
            {
                // Where句のパラメータに設定
                DbParamCollection whereParam = new DbParamCollection();
                whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
                whereParam.SetParam("@SHAIN_CD", SqlDbType.Decimal, 6, this.txtShainCd.Text);
                whereParam.SetParam("@NENGETSU", SqlDbType.DateTime, this.Nengetsu);
                alParams.Add(whereParam);
            }

            // 会計年度
            updParam.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, GetKaikeiNendo(this.Nengetsu));

            // 項目1～20
            updParam.SetParam("@KOMOKU1", SqlDbType.Decimal, 11, 2, Util.ToDecimal(this.txtShikyu1.Text));
            updParam.SetParam("@KOMOKU2", SqlDbType.Decimal, 11, 2, Util.ToDecimal(this.txtShikyu2.Text));
            updParam.SetParam("@KOMOKU3", SqlDbType.Decimal, 11, 2, Util.ToDecimal(this.txtShikyu3.Text));
            updParam.SetParam("@KOMOKU4", SqlDbType.Decimal, 11, 2, Util.ToDecimal(this.txtShikyu4.Text));
            updParam.SetParam("@KOMOKU5", SqlDbType.Decimal, 11, 2, Util.ToDecimal(this.txtShikyu5.Text));
            updParam.SetParam("@KOMOKU6", SqlDbType.Decimal, 11, 2, Util.ToDecimal(this.txtShikyu6.Text));
            updParam.SetParam("@KOMOKU7", SqlDbType.Decimal, 11, 2, Util.ToDecimal(this.txtShikyu7.Text));
            updParam.SetParam("@KOMOKU8", SqlDbType.Decimal, 11, 2, Util.ToDecimal(this.txtShikyu8.Text));
            updParam.SetParam("@KOMOKU9", SqlDbType.Decimal, 11, 2, Util.ToDecimal(this.txtShikyu9.Text));
            updParam.SetParam("@KOMOKU10", SqlDbType.Decimal, 11, 2, Util.ToDecimal(this.txtShikyu10.Text));
            updParam.SetParam("@KOMOKU11", SqlDbType.Decimal, 11, 2, Util.ToDecimal(this.txtShikyu11.Text));
            updParam.SetParam("@KOMOKU12", SqlDbType.Decimal, 11, 2, Util.ToDecimal(this.txtShikyu12.Text));
            updParam.SetParam("@KOMOKU13", SqlDbType.Decimal, 11, 2, Util.ToDecimal(this.txtShikyu13.Text));
            updParam.SetParam("@KOMOKU14", SqlDbType.Decimal, 11, 2, Util.ToDecimal(this.txtShikyu14.Text));
            updParam.SetParam("@KOMOKU15", SqlDbType.Decimal, 11, 2, Util.ToDecimal(this.txtShikyu15.Text));
            updParam.SetParam("@KOMOKU16", SqlDbType.Decimal, 11, 2, Util.ToDecimal(this.txtShikyu16.Text));
            updParam.SetParam("@KOMOKU17", SqlDbType.Decimal, 11, 2, Util.ToDecimal(this.txtShikyu17.Text));
            updParam.SetParam("@KOMOKU18", SqlDbType.Decimal, 11, 2, Util.ToDecimal(this.txtShikyu18.Text));
            updParam.SetParam("@KOMOKU19", SqlDbType.Decimal, 11, 2, Util.ToDecimal(this.txtShikyu19.Text));
            updParam.SetParam("@KOMOKU20", SqlDbType.Decimal, 11, 2, Util.ToDecimal(this.txtShikyu20.Text));
            // 入力フラグ
            updParam.SetParam("@NYURYOKU_F", SqlDbType.Decimal, 1, this.EnterAll);
            // 計算フラグ
            updParam.SetParam("@KEISAN_F", SqlDbType.Decimal, 1, this.Calculate);
            // 支給日
            updParam.SetParam("@SHIKYUBI", SqlDbType.DateTime, this.Shikyubi);
            // 更新日
            updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWDATE");

            alParams.Add(updParam);

            return alParams;
        }

        /// <summary>
        /// TB_KY_KYUYO_MEISAI_KOJOに更新するためのパラメータ設定をします。
        /// </summary>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// 更新処理：DbParamCollection*2(Where句,Set句)
        /// </returns>
        private ArrayList SetKyKyuyoKojoParams()
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            if (MODE_NEW.Equals(lblMode.Text))
            {
                // 更新パラメータ設定
                updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
                updParam.SetParam("@SHAIN_CD", SqlDbType.Decimal, 6, this.txtShainCd.Text);
                updParam.SetParam("@NENGETSU", SqlDbType.DateTime, this.Nengetsu);
                // 登録日
                updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWDATE");
            }
            else if (MODE_EDIT.Equals(lblMode.Text))
            {
                // Where句のパラメータに設定
                DbParamCollection whereParam = new DbParamCollection();
                whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
                whereParam.SetParam("@SHAIN_CD", SqlDbType.Decimal, 6, this.txtShainCd.Text);
                whereParam.SetParam("@NENGETSU", SqlDbType.DateTime, this.Nengetsu);
                alParams.Add(whereParam);
            }

            // 会計年度
            updParam.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, GetKaikeiNendo(this.Nengetsu));

            // 項目1～20
            updParam.SetParam("@KOMOKU1", SqlDbType.Decimal, 11, 2, Util.ToDecimal(this.txtKojo1.Text));
            updParam.SetParam("@KOMOKU2", SqlDbType.Decimal, 11, 2, Util.ToDecimal(this.txtKojo2.Text));
            updParam.SetParam("@KOMOKU3", SqlDbType.Decimal, 11, 2, Util.ToDecimal(this.txtKojo3.Text));
            updParam.SetParam("@KOMOKU4", SqlDbType.Decimal, 11, 2, Util.ToDecimal(this.txtKojo4.Text));
            updParam.SetParam("@KOMOKU5", SqlDbType.Decimal, 11, 2, Util.ToDecimal(this.txtKojo5.Text));
            updParam.SetParam("@KOMOKU6", SqlDbType.Decimal, 11, 2, Util.ToDecimal(this.txtKojo6.Text));
            updParam.SetParam("@KOMOKU7", SqlDbType.Decimal, 11, 2, Util.ToDecimal(this.txtKojo7.Text));
            updParam.SetParam("@KOMOKU8", SqlDbType.Decimal, 11, 2, Util.ToDecimal(this.txtKojo8.Text));
            updParam.SetParam("@KOMOKU9", SqlDbType.Decimal, 11, 2, Util.ToDecimal(this.txtKojo9.Text));
            updParam.SetParam("@KOMOKU10", SqlDbType.Decimal, 11, 2, Util.ToDecimal(this.txtKojo10.Text));
            updParam.SetParam("@KOMOKU11", SqlDbType.Decimal, 11, 2, Util.ToDecimal(this.txtKojo11.Text));
            updParam.SetParam("@KOMOKU12", SqlDbType.Decimal, 11, 2, Util.ToDecimal(this.txtKojo12.Text));
            updParam.SetParam("@KOMOKU13", SqlDbType.Decimal, 11, 2, Util.ToDecimal(this.txtKojo13.Text));
            updParam.SetParam("@KOMOKU14", SqlDbType.Decimal, 11, 2, Util.ToDecimal(this.txtKojo14.Text));
            updParam.SetParam("@KOMOKU15", SqlDbType.Decimal, 11, 2, Util.ToDecimal(this.txtKojo15.Text));
            updParam.SetParam("@KOMOKU16", SqlDbType.Decimal, 11, 2, Util.ToDecimal(this.txtKojo16.Text));
            updParam.SetParam("@KOMOKU17", SqlDbType.Decimal, 11, 2, Util.ToDecimal(this.txtKojo17.Text));
            updParam.SetParam("@KOMOKU18", SqlDbType.Decimal, 11, 2, Util.ToDecimal(this.txtKojo18.Text));
            updParam.SetParam("@KOMOKU19", SqlDbType.Decimal, 11, 2, Util.ToDecimal(this.txtKojo19.Text));
            updParam.SetParam("@KOMOKU20", SqlDbType.Decimal, 11, 2, Util.ToDecimal(this.txtKojo20.Text));

            // 非明示フィールド更新
            updParam.SetParam("@KOMOKU31", SqlDbType.Decimal, 11, 2, (_kojoKomoku31 > 0) ? _kojoKomoku31 : 0);      // 扶養親族等の数

            // 入力フラグ
            updParam.SetParam("@NYURYOKU_F", SqlDbType.Decimal, 1, this.EnterAll);
            // 計算フラグ
            updParam.SetParam("@KEISAN_F", SqlDbType.Decimal, 1, this.Calculate);
            // 支給日
            updParam.SetParam("@SHIKYUBI", SqlDbType.DateTime, this.Shikyubi);
            // 更新日
            updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWDATE");

            alParams.Add(updParam);

            return alParams;
        }

        /// <summary>
        /// TB_KY_KYUYO_MEISAI_GOKEIに更新するためのパラメータ設定をします。
        /// </summary>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// 更新処理：DbParamCollection*2(Where句,Set句)
        /// </returns>
        private ArrayList SetKyKyuyoGokeiParams()
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            if (MODE_NEW.Equals(lblMode.Text))
            {
                // 更新パラメータ設定
                updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
                updParam.SetParam("@SHAIN_CD", SqlDbType.Decimal, 6, this.txtShainCd.Text);
                updParam.SetParam("@NENGETSU", SqlDbType.DateTime, this.Nengetsu);
                // 登録日
                updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWDATE");
            }
            else if (MODE_EDIT.Equals(lblMode.Text))
            {
                // Where句のパラメータに設定
                DbParamCollection whereParam = new DbParamCollection();
                whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
                whereParam.SetParam("@SHAIN_CD", SqlDbType.Decimal, 6, this.txtShainCd.Text);
                whereParam.SetParam("@NENGETSU", SqlDbType.DateTime, this.Nengetsu);
                alParams.Add(whereParam);
            }

            // 会計年度
            updParam.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, GetKaikeiNendo(this.Nengetsu));

            // 項目1～20
            updParam.SetParam("@KOMOKU1", SqlDbType.Decimal, 11, 2, Util.ToDecimal(this.txtGokei1.Text));
            updParam.SetParam("@KOMOKU2", SqlDbType.Decimal, 11, 2, Util.ToDecimal(this.txtGokei2.Text));
            updParam.SetParam("@KOMOKU3", SqlDbType.Decimal, 11, 2, Util.ToDecimal(this.txtGokei3.Text));
            updParam.SetParam("@KOMOKU4", SqlDbType.Decimal, 11, 2, Util.ToDecimal(this.txtGokei4.Text));
            updParam.SetParam("@KOMOKU5", SqlDbType.Decimal, 11, 2, Util.ToDecimal(this.txtGokei5.Text));
            updParam.SetParam("@KOMOKU6", SqlDbType.Decimal, 11, 2, Util.ToDecimal(this.txtGokei6.Text));
            updParam.SetParam("@KOMOKU7", SqlDbType.Decimal, 11, 2, Util.ToDecimal(this.txtGokei7.Text));
            updParam.SetParam("@KOMOKU8", SqlDbType.Decimal, 11, 2, Util.ToDecimal(this.txtGokei8.Text));
            updParam.SetParam("@KOMOKU9", SqlDbType.Decimal, 11, 2, Util.ToDecimal(this.txtGokei9.Text));
            updParam.SetParam("@KOMOKU10", SqlDbType.Decimal, 11, 2, Util.ToDecimal(this.txtGokei10.Text));
            // 入力フラグ
            updParam.SetParam("@NYURYOKU_F", SqlDbType.Decimal, 1, this.EnterAll);
            // 計算フラグ
            updParam.SetParam("@KEISAN_F", SqlDbType.Decimal, 1, this.Calculate);
            // 支給日
            updParam.SetParam("@SHIKYUBI", SqlDbType.DateTime, this.Shikyubi);
            // 更新日
            updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWDATE");

            alParams.Add(updParam);

            return alParams;
        }

        /// <summary>
        /// 設定テーブルの計算式を実行
        /// </summary>
        /// <param name="controlsNm">コントロール名(番号なし)</param>
        /// <param name="dtSti">項目設定テーブルデータセット</param>
        /// <param name="opt">全計算項目指定オプションtrue(全て)/false(固定項目のみ)</param>
        private void CalcKeisanshiki(string controlsNm, DataTable dtSti, bool opt)
        {
            // 計算順位でソート
            DataView dvSti = new DataView(dtSti);
            dvSti.Sort = "KEISAN_JUNI ASC";
            
            // 項目設定が存在する保存値をセットする
            foreach (DataRowView dr in dvSti )
            {
                // 計算区分＝2計算項目のみ
                if (Util.ToString(dr["KEISAN_KUBUN"]).Equals("2"))
                {
                    // 計算対象を1固定項目のみとするか判定
                    if (opt || Util.ToString(dr["KOMOKU_KUBUN5"]).Equals("1"))
                    {
                        // 算出式が登録されている場合
                        if (!ValChk.IsEmpty(dr["KEISANSHIKI"]))
                        {
                            // 計算用文字列の取得
                            string keisanshiki = Util.ToString(dr["KEISANSHIKI"]);

                            // 計算実行
                            DataTable dtResult = Dba.GetDataTableFromSql(StiKeisanshikiToSQL(keisanshiki));
                            if (dtResult.Rows.Count > 0)
                            {
                                SetTxtTextByNm(controlsNm + Util.ToString(dr["KOMOKU_BANGO"]),
                                        Util.ToString(dtResult.Rows[0][0]));
                            }

                        }
                    }
                }
            }

        }

        /// <summary>
        /// 給与項目設定計算式のSQL文字列変換
        /// </summary>
        /// <param name="shiki">データ登録された計算式</param>
        /// <returns>変換後のSQL文字列(FROM句なしSELECT文：計算結果１カラムをDB照会)</returns>
        private string StiKeisanshikiToSQL(string shiki)
        {
            string sql;
            sql = "SELECT " + shiki;

            // VI_KY_SHAIN_JOHO　のフィールド名置換
            foreach (DataColumn co in _drShain.Table.Columns)
            {
                sql = sql.Replace(co.ColumnName, Util.ToString(_drShain[co.ColumnName]));
            }

            // 計算式で利用する画面項目名の置換
            // K1. 勤怠項目 (1-20)
            // K2. 支給項目 (1-20)
            // K3. 控除項目 (1-20)
            // K4. 合計項目 (1-10)
            for (int i = 20; i > 0; i--)
            {
                Control[] ctrl;
                ctrl = this.Controls.Find("txtKintai" + Util.ToString(i) , true);
                if (ctrl.Length > 0) sql = sql.Replace("K1.項目" + Util.ToString(i), 
                                                Util.ToString(Util.ToDecimal(((TextBox)ctrl[0]).Text)));
                ctrl = this.Controls.Find("txtShikyu" + Util.ToString(i), true);
                if (ctrl.Length > 0) sql = sql.Replace("K2.項目" + Util.ToString(i), 
                                                Util.ToString(Util.ToDecimal(((TextBox)ctrl[0]).Text)));
                ctrl = this.Controls.Find("txtKojo"   + Util.ToString(i), true);
                if (ctrl.Length > 0) sql = sql.Replace("K3.項目" + Util.ToString(i), 
                                                Util.ToString(Util.ToDecimal(((TextBox)ctrl[0]).Text)));
                if (i <= 10)
                {
                    ctrl = this.Controls.Find("txtGokei" + Util.ToString(i), true);
                    if (ctrl.Length > 0) sql = sql.Replace("K4.項目" + Util.ToString(i), 
                                                    Util.ToString(Util.ToDecimal(((TextBox)ctrl[0]).Text)));
                }
            }

            // エラー回避
            if (sql == "SELECT ")
            {
                sql = "SELECT 0";
            }

            return sql;
        }
      
        /// <summary>
        /// 税計算
        /// </summary>
        private void CalcTax()
        {
            // 控除項目設定テーブルを項目区分3順に
            // 定義値31健康保険・32厚生年金・33雇用保険・34所得税・35住民税別処理を行う
            DataView dvKojo = new DataView(_dtStiKojo);
            dvKojo.Sort = "KOMOKU_KUBUN3";
            foreach (DataRowView dr in dvKojo)
            {
                switch (Util.ToString(dr["KOMOKU_KUBUN3"]))
                {
                    case "31":  // 健康保険
                        // 社員情報よりセット
                        SetTxtTextByNm("txtKojo" + Util.ToString(dr["KOMOKU_BANGO"]), 
                            Util.ToString(_drShain["KENKO_HOKENRYO"]));
                        break;
                    case "32":  // 厚生年金
                        // 社員情報よりセット
                        SetTxtTextByNm("txtKojo" + Util.ToString(dr["KOMOKU_BANGO"]),
                            Util.ToString(_drShain["KOSEI_NENKIN_HOKENRYO"]));
                        break;
                    case "33":  // 雇用保険
                        // 雇用保険計算区分が1計算するの時のみ
                        if ( Util.ToString(_drShain["KOYO_HOKEN_KEISAN_KUBUN"]).Equals("1") )
                        {
                            // 総支給額
                            decimal shikyu = 0;
                            for (int i = 0; i <= 20; i++)
                            {
                                shikyu = shikyu + Util.ToDecimal(GetTxtTextByNm("txtShikyu" + Util.ToString(i)));
                            }

                            // 会社情報より被保険者負担率・基数を取得
                            decimal ftn_rt = Util.ToDecimal(_drKaisha["KOYO_HOKEN_HIHOKENSHA_FTN_RT"]); // 負担率
                            decimal ftn_ks = Util.ToDecimal(_drKaisha["KOYO_HOKEN_HIHOKENSHA_FTN_KS"]); // 負担基数
                        
                            // 雇用保険料計算
                            SetTxtTextByNm("txtKojo" + Util.ToString(dr["KOMOKU_BANGO"]),
                                Util.ToString(RoundUnemploymentInsuranceOnWithhold(shikyu * ftn_rt / ftn_ks)));
                        }
                        break;
                    case "34":  // 所得税

                        SetTxtTextByNm("txtKojo" + Util.ToString(dr["KOMOKU_BANGO"]),
                            Util.ToString(CalcTaxIncomeTax()));

                        break;
                    case "35":  // 住民税
                        // 社員情報よりセット
                        if (this.Nengetsu.Month == 6)       // 6月のみ
                        {
                            SetTxtTextByNm("txtKojo" + Util.ToString(dr["KOMOKU_BANGO"]),
                                Util.ToString(_drShain["JUMINZEI_6GATSUBUN"]));
                        }
                        else
                        {
                            SetTxtTextByNm("txtKojo" + Util.ToString(dr["KOMOKU_BANGO"]),
                                Util.ToString(_drShain["JUMINZEI_7GATSUBUN_IKO"]));
                        }
                        break;
                    default:    // 何もしない
                        break;
                }
            }
        }

        /// <summary>
        /// 源泉徴収所得税の算出
        /// </summary>
        private decimal CalcTaxIncomeTax()
        {
            decimal ret = 0;

            // 社員情報の所得税・税額表で「0なし」以外の時に実行
            if (!Util.ToString(_drShain["ZEIGAKU_HYO"]).Equals("0"))
            {
                // (A)給与等の支給額
                decimal kyuyoto = 0m;
                foreach (DataRow dr in _dtStiShikyu.Rows)
                {
                    // 支給項目の課税区分(項目区分1)が1課税の時のみ
                    if (Util.ToString(dr["KOMOKU_KUBUN1"]).Equals("1"))
                    {
                        kyuyoto += Util.ToDecimal(GetTxtTextByNm("txtShikyu" + Util.ToString(dr["KOMOKU_BANGO"])));
                    }
                }

                // (B)給与等から控除する社会保険料等
                decimal kojoto = 0m;
                foreach (DataRow dr in _dtStiKojo.Rows)
                {
                    // 控除項目の項目区分(項目区分3)で判定
                    switch (Util.ToString(dr["KOMOKU_KUBUN3"]))     
                    {
                        case "31":  // 健康保険
                        case "32":  // 厚生年金
                        case "33":  // 雇用保険
                            kojoto += Util.ToDecimal(GetTxtTextByNm("txtKojo" + Util.ToString(dr["KOMOKU_BANGO"])));
                            break;
                        default:
                            break;
                    }
                }

                // (X)扶養親族等の数
                int fuyo = 0;
                // ①控除対象配偶者(1一般　2老人)
                if (Util.ToInt(_drShain["HAIGUSHA_KUBUN"]) > 0) ++fuyo;  
                // ②控除対象扶養者数
                fuyo += Util.ToInt(_drShain["FUYO_IPPAN"]);
                // ③本人区分1(2一般寡婦　3特別寡婦　4寡夫)
                if (Util.ToInt(_drShain["HONNIN_KUBUN1"]) > 0) ++fuyo;
                // ④本人区分2(1一般障害者　2特別障害者)
                if (Util.ToInt(_drShain["HONNIN_KUBUN2"]) > 0) ++fuyo;
                // ⑤本人区分3(1勤労学生)
                if (Util.ToInt(_drShain["HONNIN_KUBUN3"]) > 0) ++fuyo;
                // ⑥扶養同居特障一般
                fuyo += Util.ToInt(_drShain["FUYO_DOKYO_TOKUSHO_IPPAN"]) * 2;   // 制度上のカウント方法
                // ⑦障害者一般
                fuyo += Util.ToInt(_drShain["SHOGAISHA_IPPAN"]);
                // ⑧障害者特別
                fuyo += Util.ToInt(_drShain["SHOGAISHA_TOKUBETSU"]);
                
                // クラス変数へセット
                _kojoKomoku31 = fuyo;

                // 税額表の参照
                string ko_otsu = Util.ToString(_drShain["ZEIGAKU_HYO"]);        // 甲乙の別
                decimal kazei = kyuyoto - kojoto;                               // 課税額　(A)-(B)
                switch (Util.ToString(_drKaisha["SHOTOKUZEI_KEISAN_HOHO"]))
                {
                    case "1":       // 1大蔵省(財務省)告示の計算
                        ret = CalcTaxIncomeTax_1(ko_otsu, kazei, fuyo);
                        break;
                    case "2":       // 2税額表
                        ret = CalcTaxIncomeTax_2(ko_otsu, kazei, fuyo);
                        break;
                    default:
                        break;
                }
            }
            return ret;
        }

        /// <summary>
        /// 所得税の計算(1財務省告示・電子計算機の特例)
        /// </summary>
        /// <param name="ko_otsu">0なし　1甲　2乙</param>
        /// <param name="kazei">給与等の支給額から給与等から控除する社会保険料等を引いた金額</param>
        /// <param name="fuyo">扶養親族等の数</param>
        private decimal CalcTaxIncomeTax_1(string ko_otsu, decimal kazei, int fuyo)
        {
            decimal ret = 0;

            // 2014.1現在は開発対象外としているので(2税額表)を仮利用
            ret = CalcTaxIncomeTax_2(ko_otsu, kazei, fuyo);

            return ret;
        }

        /// <summary>
        /// 所得税の計算(2税額表)
        /// </summary>
        /// <param name="ko_otsu">0なし　1甲　2乙</param>
        /// <param name="kazei">給与等の支給額から給与等から控除する社会保険料等を引いた金額</param>
        /// <param name="fuyo">扶養親族等の数</param>
        private decimal CalcTaxIncomeTax_2(string ko_otsu, decimal kazei, int fuyo)
        {
            decimal ret = 0;

            // 税額表(月額)の参照
            StringBuilder cols = new StringBuilder();
            cols.Append("SHOTOKU_KINGAKU_IJO");
            cols.Append(" ,SHOTOKU_KINGAKU_IKA");
            cols.Append(" ,KO_0NIN_ZEIGAKU");
            cols.Append(" ,KO_1NIN_ZEIGAKU");
            cols.Append(" ,KO_2NIN_ZEIGAKU");
            cols.Append(" ,KO_3NIN_ZEIGAKU");
            cols.Append(" ,KO_4NIN_ZEIGAKU");
            cols.Append(" ,KO_5NIN_ZEIGAKU");
            cols.Append(" ,KO_6NIN_ZEIGAKU");
            cols.Append(" ,KO_7NIN_ZEIGAKU");
            cols.Append(" ,OTSU_0NIN_ZEIGAKU");
            cols.Append(" ,KO_KEISANSHIKI");
            cols.Append(" ,OTSU_KEISANSHIKI");
            cols.Append(" ,KO_KOJOGAKU");
            cols.Append(" ,OTSU_KOJOGAKU");
            cols.Append(" ,REGIST_DATE");
            cols.Append(" ,UPDATE_DATE");
            StringBuilder where = new StringBuilder();
            where.Append("SHOTOKU_KINGAKU_IJO <= " + Util.ToString(kazei));
            where.Append(" AND SHOTOKU_KINGAKU_IKA >= " + Util.ToString(kazei));
            DataTable dt = Dba.GetDataTableByCondition(Util.ToString(cols), "TB_KY_F_GETSUGAKU_STKZIGK_H", Util.ToString(where));
            if (dt.Rows.Count == 1)
            {
                // 税額表参照すべき扶養人数等
                int xx = fuyo > 7 ? 7 : fuyo;

                // 税額計算用変数
                decimal z1 = 0;     // 税額表税額
                decimal z2 = 0;     // 税額表記載の計算式による調整額
                decimal z3 = 0;     // 扶養人数毎の控除額

                if (ko_otsu.Equals("1"))            // 甲欄
                {
                    // 税額表の税額
                    z1 = Util.ToDecimal(dt.Rows[0]["KO_" + Util.ToString(xx) + "NIN_ZEIGAKU"]);

                    // 甲欄計算式
                    if (!ValChk.IsEmpty(dt.Rows[0]["KO_KEISANSHIKI"]))
                    {
                        // 文字列：給与金額を金額値へ置換
                        string sql = "SELECT " + Util.ToString(dt.Rows[0]["KO_KEISANSHIKI"]) + " AS RET";
                        sql = sql.Replace("給与金額", Util.ToString(kazei));

                        // 計算実行
                        DataTable dtResult = Dba.GetDataTableFromSql(sql);
                        if (dtResult.Rows.Count > 0)
                        {
                            z2 = Util.ToDecimal(dtResult.Rows[0]["RET"]);
                        }
                    }

                    // 扶養控除
                    if (fuyo > xx)
                    {
                        z3 = (fuyo - xx) * Util.ToDecimal(dt.Rows[0]["KO_KOJOGAKU"]);
                    }
                }
                else if (ko_otsu.Equals("2"))       // 乙欄
                {
                    // 税額表の税額
                    z1 = Util.ToDecimal(dt.Rows[0]["OTSU_0NIN_ZEIGAKU"]);

                    // 甲欄計算式
                    if (!ValChk.IsEmpty(dt.Rows[0]["OTSU_KEISANSHIKI"]))
                    {
                        // 文字列：給与金額を金額値へ置換
                        string sql = "SELECT " + Util.ToString(dt.Rows[0]["KO_KEISANSHIKI"]) + " AS RET";
                        sql = sql.Replace("給与金額", Util.ToString(kazei));

                        // 計算実行
                        DataTable dtResult = Dba.GetDataTableFromSql(sql);
                        if (dtResult.Rows.Count > 0)
                        {
                            z2 = RoundIncomeTaxOnWithhold(Util.ToDecimal(dtResult.Rows[0]["RET"]));
                        }
                    }

                    // 扶養控除
                    if (fuyo > xx)
                    {
                        z3 = (fuyo - xx) * Util.ToDecimal(dt.Rows[0]["OTSU_KOJOGAKU"]);
                    }
                }
                // 源泉徴収所得税
                if (z1 + z2 - z3 < 0)
                {
                    ret = 0;
                }
                else
                {
                    ret = z1 + z2 - z3;
                }
            }

            return ret;
        }

        /// <summary>
        /// 源泉徴収時の雇用保険料端数処理
        /// </summary>
        /// <param name="gaku">金額</param>
        /// <returns>端数丸め後の金額</returns>
        private decimal RoundUnemploymentInsuranceOnWithhold(decimal gaku)
        {
            decimal ret = 0;

            // 源泉徴収の時：50銭以下の場合は切り捨て、50銭1厘以上の場合は切り上げ
            decimal hasu = gaku % 1.0m;
            if (hasu <= 0.5m)
            {
                ret = gaku - hasu;
            }
            else
            {
                ret = gaku - hasu + 1;
            }
            return ret;
        }

        /// <summary>
        /// 源泉徴収時の所得税端数処理
        /// </summary>
        /// <param name="gaku">金額</param>
        /// <returns>端数丸め後の金額</returns>
        private decimal RoundIncomeTaxOnWithhold(decimal gaku)
        {
            decimal ret = 0;

            // 源泉徴収の時：円未満は切り捨て
            decimal hasu = gaku % 1.0m;
            ret = gaku - hasu;
            return ret;
        }

        /// <summary>
        /// 給与振込項目の計算
        /// </summary>
        private void CalcTransferAmount()
        {
            // 画面上の差引支給額を取得
            decimal Gaku = 0;
            foreach (DataRow dr in _dtStiGokei.Rows)
            {
                if (Util.ToString(dr["KOMOKU_NM"]) == "差引支給額")
                {
                    Gaku = Util.ToDecimal(GetTxtTextByNm("txtGokei" + Util.ToString(dr["KOMOKU_BANGO"])));
                    break;
                }
            }

            #region 銀行振込の計算
            // 合計項目設定テーブルの定義値42給与振込１ 43給与振込２ それぞれ計算
            if (Gaku > 0)
            {
                int Sagaku_F = 0;
                decimal Ritsu = 0;
                decimal RitsuKingaku = 0;
                decimal furikomi_bango = 0;
                decimal furikomi_gaku1 = 0;
                decimal furikomi_gaku2 = 0;
                foreach (DataRow dr in _dtStiGokei.Rows)
                {
                    #region 【支給方法1】の支給方法が0:現金 又は 1:現金の場合
                    // 振込額1の値を固定値0として表示する
                    if (Util.ToString(dr["KOMOKU_KUBUN3"]).Equals("42")
                        && (Util.ToString(_drShain["KYUYO_SHIKYU_HOHO1"]).Equals("0") || Util.ToString(_drShain["KYUYO_SHIKYU_HOHO1"]).Equals("1")))
                    {
                        SetTxtTextByNm("txtGokei" + Util.ToString(dr["KOMOKU_BANGO"]), "0");
                        // 振込額1の値を保持
                        furikomi_gaku1 = 0;
                    }
                    #endregion

                    #region 【支給方法1】の支給方法が2:振込の場合
                    else if (Util.ToString(dr["KOMOKU_KUBUN3"]).Equals("42")
                        && Util.ToString(_drShain["KYUYO_SHIKYU_HOHO1"]).Equals("2"))
                    {
                        // 支給金額が固定値999999999の場合
                        if (Util.ToDecimal(_drShain["KYUYO_SHIKYU_KINGAKU1"]) == 999999999)
                        {
                            Ritsu = Util.ToDecimal(_drShain["KYUYO_SHIKYU_RITSU1"]);

                            // 画面上の差引支給額から率指定で算出
                            RitsuKingaku = Util.ToDecimal(Util.Round(Gaku * Ritsu / 100, 0));

                            SetTxtTextByNm("txtGokei" + Util.ToString(dr["KOMOKU_BANGO"]), Util.ToString(Gaku));

                            // 振込額1の値を保持
                            furikomi_gaku1 = Gaku;
                        }
                        // それ以外の場合
                        else
                        {
                            // 支給金額が1以上の場合、振込額1の値に支給金額を表示する
                            if (Util.ToDecimal(_drShain["KYUYO_SHIKYU_KINGAKU1"]) > 1)
                            {
                                SetTxtTextByNm("txtGokei" + Util.ToString(dr["KOMOKU_BANGO"]),
                                    Util.ToString(_drShain["KYUYO_SHIKYU_KINGAKU1"]));

                                // 振込額1の値を保持
                                furikomi_gaku1 = Util.ToDecimal(_drShain["KYUYO_SHIKYU_KINGAKU1"]);
                            }
                            // それ以外の場合、振込額1の値に税額を計算した金額を表示する
                            else
                            {
                                Ritsu = Util.ToDecimal(_drShain["KYUYO_SHIKYU_RITSU1"]);

                                // 画面上の差引支給額から率指定で算出
                                RitsuKingaku = Util.ToDecimal(Util.Round(Gaku * Ritsu / 100, 0));

                                SetTxtTextByNm("txtGokei" + Util.ToString(dr["KOMOKU_BANGO"]), Util.ToString(RitsuKingaku));

                                // 振込額1の値を保持
                                furikomi_gaku1 = RitsuKingaku;
                            }
                        }
                    }
                    #endregion

                    #region 【支給方法1】の支給金額1が999999999の場合
                    // 振込額1の値を固定値0として表示する
                    else if (Util.ToString(dr["KOMOKU_KUBUN3"]).Equals("43")
                        && Util.ToDecimal(_drShain["KYUYO_SHIKYU_KINGAKU1"]) == 999999999)
                    {
                        // 振込額を保持
                        furikomi_gaku2 = 0;

                        // 振込No.を保持
                        furikomi_bango = Util.ToDecimal(dr["KOMOKU_BANGO"]);
                    }
                    #endregion

                    #region 【支給方法1】の支給率1が100の場合
                    // 振込額2の値を固定値0として表示する
                    else if (Util.ToString(dr["KOMOKU_KUBUN3"]).Equals("43") && Util.ToDecimal(_drShain["KYUYO_SHIKYU_RITSU1"]) == 100)
                    {
                        // 振込額を保持
                        furikomi_gaku2 = 0;

                        // 振込No.を保持
                        furikomi_bango = Util.ToDecimal(dr["KOMOKU_BANGO"]);
                    }
                    #endregion

                    #region 【支給方法2】の支給方法が0:現金 又は 1:現金の場合
                    // 振込額1の値を固定値0として表示する
                    else if (Util.ToString(dr["KOMOKU_KUBUN3"]).Equals("43")
                        && (Util.ToString(_drShain["KYUYO_SHIKYU_HOHO2"]).Equals("0") || Util.ToString(_drShain["KYUYO_SHIKYU_HOHO2"]).Equals("1")))
                    {
                        // 振込額を保持
                        furikomi_gaku2 = 0;

                        // 振込No.を保持
                        furikomi_bango = Util.ToDecimal(dr["KOMOKU_BANGO"]);
                    }
                    #endregion

                    #region 【支給方法2】の支給方法が2:振込の場合
                    else if (Util.ToString(dr["KOMOKU_KUBUN3"]).Equals("43")
                        && Util.ToString(_drShain["KYUYO_SHIKYU_HOHO2"]).Equals("2"))
                    {
                        // 支給金額1が固定値999999999の場合
                        if (Util.ToDecimal(_drShain["KYUYO_SHIKYU_KINGAKU1"]) == 999999999)
                        {
                            furikomi_gaku2 = 0;
                        }
                        // それ以外の場合
                        else
                        {
                            // 支給金額2が固定値999999999の場合
                            if (Util.ToDecimal(_drShain["KYUYO_SHIKYU_KINGAKU2"]) == 999999999)
                            {
                                // 差額フラグを立てる
                                Sagaku_F = 1;
                            }
                            // 支給金額が1以上の場合、振込額2の値に支給金額を表示する
                            else if (Util.ToDecimal(_drShain["KYUYO_SHIKYU_KINGAKU2"]) > 1)
                            {
                                // 振込No.を保持
                                furikomi_bango = Util.ToDecimal(dr["KOMOKU_BANGO"]);
                                // 振込額を保持
                                furikomi_gaku2 = Util.ToDecimal(_drShain["KYUYO_SHIKYU_KINGAKU2"]);
                            }
                            // それ以外の場合、振込額1の値に税額を計算した金額を表示する
                            else
                            {
                                Ritsu = Util.ToDecimal(_drShain["KYUYO_SHIKYU_RITSU1"]);

                                // 画面上の差引支給額から率指定で算出
                                RitsuKingaku = Util.ToDecimal(Util.Round(Gaku * Ritsu / 100, 0));

                                SetTxtTextByNm("txtGokei" + Util.ToString(dr["KOMOKU_BANGO"]),
                                    Util.ToString(Gaku - RitsuKingaku));

                                // 差額フラグを立てる
                                Sagaku_F = 1;
                            }
                        }

                        // 振込No.を保持
                        furikomi_bango = Util.ToDecimal(dr["KOMOKU_BANGO"]);
                    }
                    #endregion
                }

                // 差額フラグが立っていた場合の処理
                if(Sagaku_F == 1)
                {
                    furikomi_gaku2 = Gaku - furikomi_gaku1;
                }

                // 振込額2を表示する
                SetTxtTextByNm("txtGokei" + furikomi_bango, Util.ToString(furikomi_gaku2));
            }
            #endregion
        }

        /// <summary>
        /// 会計年度の取得
        /// </summary>
        /// <param name="hizuke">日付</param>
        /// <returns>指定日付の属する年度</returns>
        private int GetKaikeiNendo(DateTime hizuke)
        {
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@HIDUKE", SqlDbType.DateTime, hizuke);
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT");
            sql.Append(" KAIKEI_NENDO");
            sql.Append(" FROM");
            sql.Append(" TB_ZM_KAISHA_JOHO");
            sql.Append(" WHERE");
            sql.Append(" KAIKEI_KIKAN_KAISHIBI <= @HIDUKE");
            sql.Append(" AND KAIKEI_KIKAN_SHURYOBI >= @HIDUKE");
            DataTable dt = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);
            if (dt.Rows.Count == 1)
            {
                return Util.ToInt(dt.Rows[0]["KAIKEI_NENDO"]);
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// 入力項目で最終コントロールであるかのチェック処理
        /// キーダウンから呼ばれて処理する。
        /// </summary>
        /// <param name="ctrlNm"></param>
        /// <returns>true: まだ入力項目がある、false:無い！最後</returns>
        private bool checkEnabled(string ctrlNm)
        {
            string[] ctrlName = new string[4] { "txtKintai", "txtShikyu", "txtKojo", "txtGokei" };
            int arrayIndex = 0;
            for (arrayIndex = 0; arrayIndex < ctrlName.Length ; arrayIndex++)
            {
                if (ctrlNm.IndexOf(ctrlName[arrayIndex]) >= 0)
                {
                    break;
                }
            }
            // チェックを始める項目番号＋１を取得（自分の次から）
            int ctrlNo = Util.ToInt(ctrlNm.Substring(ctrlName[arrayIndex].Length)) + 1 ;

            for (int i = arrayIndex; i < ctrlName.Length ; i++)
            {
                for (int j = ctrlNo; j < 21; j++)
                {
                    Control[] c = this.Controls.Find(ctrlName[i] + j.ToString(), true);
                    if (c.Length > 0)
                    {
                        if (((jp.co.fsi.common.controls.FsiTextBox)c[0]).Enabled)
                            return true;
                    }
                }
                // 次の項目のチェックを始める前に番号を初期化
                ctrlNo = 1;
            }
            return false;
        }
        #endregion
    }
}
