﻿namespace jp.co.fsi.ky.kyke1011
{
    partial class KYKE1011
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpNengetsu = new System.Windows.Forms.GroupBox();
            this.lblMonthShikyutsuki = new System.Windows.Forms.Label();
            this.lblYearShikyutsuki = new System.Windows.Forms.Label();
            this.txtMonthNengetsu = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblGengoNengetsu = new System.Windows.Forms.Label();
            this.txtGengoYearNengetsu = new jp.co.fsi.common.controls.FsiTextBox();
            this.grpShikyubi = new System.Windows.Forms.GroupBox();
            this.lblDayShikyubi = new System.Windows.Forms.Label();
            this.txtDayShikyubi = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMonthShikyubi = new System.Windows.Forms.Label();
            this.lblYearShikyubi = new System.Windows.Forms.Label();
            this.txtMonthShikyubi = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblGengoShikyubi = new System.Windows.Forms.Label();
            this.txtGengoYearShikyubi = new jp.co.fsi.common.controls.FsiTextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.chkCalculate = new System.Windows.Forms.CheckBox();
            this.chkEnterAll = new System.Windows.Forms.CheckBox();
            this.lblMizuageShisho = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.grpNengetsu.SuspendLayout();
            this.grpShikyubi.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Text = "給与明細入力";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Size = new System.Drawing.Size(847, 100);
            // 
            // grpNengetsu
            // 
            this.grpNengetsu.BackColor = System.Drawing.Color.White;
            this.grpNengetsu.Controls.Add(this.lblMonthShikyutsuki);
            this.grpNengetsu.Controls.Add(this.lblYearShikyutsuki);
            this.grpNengetsu.Controls.Add(this.txtMonthNengetsu);
            this.grpNengetsu.Controls.Add(this.lblGengoNengetsu);
            this.grpNengetsu.Controls.Add(this.txtGengoYearNengetsu);
            this.grpNengetsu.Controls.Add(this.lblMizuageShisho);
            this.grpNengetsu.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.grpNengetsu.Location = new System.Drawing.Point(12, 55);
            this.grpNengetsu.Name = "grpNengetsu";
            this.grpNengetsu.Size = new System.Drawing.Size(381, 57);
            this.grpNengetsu.TabIndex = 902;
            this.grpNengetsu.TabStop = false;
            // 
            // lblMonthShikyutsuki
            // 
            this.lblMonthShikyutsuki.AutoSize = true;
            this.lblMonthShikyutsuki.BackColor = System.Drawing.Color.Silver;
            this.lblMonthShikyutsuki.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMonthShikyutsuki.Location = new System.Drawing.Point(230, 27);
            this.lblMonthShikyutsuki.Name = "lblMonthShikyutsuki";
            this.lblMonthShikyutsuki.Size = new System.Drawing.Size(21, 13);
            this.lblMonthShikyutsuki.TabIndex = 4;
            this.lblMonthShikyutsuki.Text = "月";
            // 
            // lblYearShikyutsuki
            // 
            this.lblYearShikyutsuki.AutoSize = true;
            this.lblYearShikyutsuki.BackColor = System.Drawing.Color.Silver;
            this.lblYearShikyutsuki.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblYearShikyutsuki.Location = new System.Drawing.Point(171, 27);
            this.lblYearShikyutsuki.Name = "lblYearShikyutsuki";
            this.lblYearShikyutsuki.Size = new System.Drawing.Size(21, 13);
            this.lblYearShikyutsuki.TabIndex = 2;
            this.lblYearShikyutsuki.Text = "年";
            // 
            // txtMonthNengetsu
            // 
            this.txtMonthNengetsu.AutoSizeFromLength = false;
            this.txtMonthNengetsu.DisplayLength = null;
            this.txtMonthNengetsu.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMonthNengetsu.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtMonthNengetsu.Location = new System.Drawing.Point(193, 23);
            this.txtMonthNengetsu.MaxLength = 2;
            this.txtMonthNengetsu.Name = "txtMonthNengetsu";
            this.txtMonthNengetsu.Size = new System.Drawing.Size(34, 20);
            this.txtMonthNengetsu.TabIndex = 3;
            this.txtMonthNengetsu.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonthNengetsu.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthNengetsu_Validating);
            // 
            // lblGengoNengetsu
            // 
            this.lblGengoNengetsu.BackColor = System.Drawing.Color.Silver;
            this.lblGengoNengetsu.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGengoNengetsu.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGengoNengetsu.Location = new System.Drawing.Point(96, 23);
            this.lblGengoNengetsu.Name = "lblGengoNengetsu";
            this.lblGengoNengetsu.Size = new System.Drawing.Size(37, 20);
            this.lblGengoNengetsu.TabIndex = 0;
            this.lblGengoNengetsu.Text = "平成";
            this.lblGengoNengetsu.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtGengoYearNengetsu
            // 
            this.txtGengoYearNengetsu.AutoSizeFromLength = false;
            this.txtGengoYearNengetsu.DisplayLength = null;
            this.txtGengoYearNengetsu.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGengoYearNengetsu.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtGengoYearNengetsu.Location = new System.Drawing.Point(135, 23);
            this.txtGengoYearNengetsu.MaxLength = 2;
            this.txtGengoYearNengetsu.Name = "txtGengoYearNengetsu";
            this.txtGengoYearNengetsu.Size = new System.Drawing.Size(34, 20);
            this.txtGengoYearNengetsu.TabIndex = 1;
            this.txtGengoYearNengetsu.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGengoYearNengetsu.Validating += new System.ComponentModel.CancelEventHandler(this.txtGengoYearNengetsu_Validating);
            // 
            // grpShikyubi
            // 
            this.grpShikyubi.BackColor = System.Drawing.Color.White;
            this.grpShikyubi.Controls.Add(this.lblDayShikyubi);
            this.grpShikyubi.Controls.Add(this.txtDayShikyubi);
            this.grpShikyubi.Controls.Add(this.lblMonthShikyubi);
            this.grpShikyubi.Controls.Add(this.lblYearShikyubi);
            this.grpShikyubi.Controls.Add(this.txtMonthShikyubi);
            this.grpShikyubi.Controls.Add(this.lblGengoShikyubi);
            this.grpShikyubi.Controls.Add(this.txtGengoYearShikyubi);
            this.grpShikyubi.Controls.Add(this.label1);
            this.grpShikyubi.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.grpShikyubi.Location = new System.Drawing.Point(12, 118);
            this.grpShikyubi.Name = "grpShikyubi";
            this.grpShikyubi.Size = new System.Drawing.Size(381, 64);
            this.grpShikyubi.TabIndex = 903;
            this.grpShikyubi.TabStop = false;
            // 
            // lblDayShikyubi
            // 
            this.lblDayShikyubi.AutoSize = true;
            this.lblDayShikyubi.BackColor = System.Drawing.Color.Silver;
            this.lblDayShikyubi.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDayShikyubi.Location = new System.Drawing.Point(306, 27);
            this.lblDayShikyubi.Name = "lblDayShikyubi";
            this.lblDayShikyubi.Size = new System.Drawing.Size(21, 13);
            this.lblDayShikyubi.TabIndex = 7;
            this.lblDayShikyubi.Text = "日";
            // 
            // txtDayShikyubi
            // 
            this.txtDayShikyubi.AutoSizeFromLength = false;
            this.txtDayShikyubi.DisplayLength = null;
            this.txtDayShikyubi.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDayShikyubi.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtDayShikyubi.Location = new System.Drawing.Point(270, 23);
            this.txtDayShikyubi.MaxLength = 2;
            this.txtDayShikyubi.Name = "txtDayShikyubi";
            this.txtDayShikyubi.Size = new System.Drawing.Size(34, 20);
            this.txtDayShikyubi.TabIndex = 5;
            this.txtDayShikyubi.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDayShikyubi.Validating += new System.ComponentModel.CancelEventHandler(this.txtDayShikyubi_Validating);
            // 
            // lblMonthShikyubi
            // 
            this.lblMonthShikyubi.AutoSize = true;
            this.lblMonthShikyubi.BackColor = System.Drawing.Color.Silver;
            this.lblMonthShikyubi.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMonthShikyubi.Location = new System.Drawing.Point(236, 27);
            this.lblMonthShikyubi.Name = "lblMonthShikyubi";
            this.lblMonthShikyubi.Size = new System.Drawing.Size(21, 13);
            this.lblMonthShikyubi.TabIndex = 4;
            this.lblMonthShikyubi.Text = "月";
            // 
            // lblYearShikyubi
            // 
            this.lblYearShikyubi.AutoSize = true;
            this.lblYearShikyubi.BackColor = System.Drawing.Color.Silver;
            this.lblYearShikyubi.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblYearShikyubi.Location = new System.Drawing.Point(171, 27);
            this.lblYearShikyubi.Name = "lblYearShikyubi";
            this.lblYearShikyubi.Size = new System.Drawing.Size(21, 13);
            this.lblYearShikyubi.TabIndex = 2;
            this.lblYearShikyubi.Text = "年";
            // 
            // txtMonthShikyubi
            // 
            this.txtMonthShikyubi.AutoSizeFromLength = false;
            this.txtMonthShikyubi.DisplayLength = null;
            this.txtMonthShikyubi.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMonthShikyubi.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtMonthShikyubi.Location = new System.Drawing.Point(200, 23);
            this.txtMonthShikyubi.MaxLength = 2;
            this.txtMonthShikyubi.Name = "txtMonthShikyubi";
            this.txtMonthShikyubi.Size = new System.Drawing.Size(34, 20);
            this.txtMonthShikyubi.TabIndex = 3;
            this.txtMonthShikyubi.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonthShikyubi.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthShikyubi_Validating);
            // 
            // lblGengoShikyubi
            // 
            this.lblGengoShikyubi.BackColor = System.Drawing.Color.Silver;
            this.lblGengoShikyubi.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGengoShikyubi.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGengoShikyubi.Location = new System.Drawing.Point(93, 23);
            this.lblGengoShikyubi.Name = "lblGengoShikyubi";
            this.lblGengoShikyubi.Size = new System.Drawing.Size(40, 20);
            this.lblGengoShikyubi.TabIndex = 0;
            this.lblGengoShikyubi.Text = "平成";
            this.lblGengoShikyubi.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtGengoYearShikyubi
            // 
            this.txtGengoYearShikyubi.AutoSizeFromLength = false;
            this.txtGengoYearShikyubi.DisplayLength = null;
            this.txtGengoYearShikyubi.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGengoYearShikyubi.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtGengoYearShikyubi.Location = new System.Drawing.Point(135, 23);
            this.txtGengoYearShikyubi.MaxLength = 2;
            this.txtGengoYearShikyubi.Name = "txtGengoYearShikyubi";
            this.txtGengoYearShikyubi.Size = new System.Drawing.Size(34, 20);
            this.txtGengoYearShikyubi.TabIndex = 1;
            this.txtGengoYearShikyubi.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGengoYearShikyubi.Validating += new System.ComponentModel.CancelEventHandler(this.txtGengoYearShikyubi_Validating);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chkCalculate);
            this.groupBox1.Controls.Add(this.chkEnterAll);
            this.groupBox1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBox1.Location = new System.Drawing.Point(12, 188);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(381, 48);
            this.groupBox1.TabIndex = 904;
            this.groupBox1.TabStop = false;
            // 
            // chkCalculate
            // 
            this.chkCalculate.AutoSize = true;
            this.chkCalculate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.chkCalculate.Location = new System.Drawing.Point(207, 17);
            this.chkCalculate.Name = "chkCalculate";
            this.chkCalculate.Size = new System.Drawing.Size(138, 17);
            this.chkCalculate.TabIndex = 1;
            this.chkCalculate.Text = "更新時に計算する";
            this.chkCalculate.UseVisualStyleBackColor = true;
            this.chkCalculate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.chkCalculate_KeyDown);
            // 
            // chkEnterAll
            // 
            this.chkEnterAll.AutoSize = true;
            this.chkEnterAll.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.chkEnterAll.Location = new System.Drawing.Point(8, 18);
            this.chkEnterAll.Name = "chkEnterAll";
            this.chkEnterAll.Size = new System.Drawing.Size(166, 17);
            this.chkEnterAll.TabIndex = 0;
            this.chkEnterAll.Text = "全ての項目を入力する";
            this.chkEnterAll.UseVisualStyleBackColor = true;
            // 
            // lblMizuageShisho
            // 
            this.lblMizuageShisho.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShisho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShisho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblMizuageShisho.Location = new System.Drawing.Point(15, 20);
            this.lblMizuageShisho.Name = "lblMizuageShisho";
            this.lblMizuageShisho.Size = new System.Drawing.Size(240, 27);
            this.lblMizuageShisho.TabIndex = 6;
            this.lblMizuageShisho.Text = "支　給　月";
            this.lblMizuageShisho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Silver;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.label1.Location = new System.Drawing.Point(15, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(318, 27);
            this.label1.TabIndex = 8;
            this.label1.Text = "支　給　日";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // KYKE1011
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 638);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.grpShikyubi);
            this.Controls.Add(this.grpNengetsu);
            this.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.Name = "KYKE1011";
            this.Par1 = "";
            this.Text = "給与明細入力";
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.grpNengetsu, 0);
            this.Controls.SetChildIndex(this.grpShikyubi, 0);
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.pnlDebug.ResumeLayout(false);
            this.grpNengetsu.ResumeLayout(false);
            this.grpNengetsu.PerformLayout();
            this.grpShikyubi.ResumeLayout(false);
            this.grpShikyubi.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }
        #endregion

        private System.Windows.Forms.GroupBox grpNengetsu;
        private System.Windows.Forms.Label lblMonthShikyutsuki;
        private System.Windows.Forms.Label lblYearShikyutsuki;
        private common.controls.FsiTextBox txtMonthNengetsu;
        private System.Windows.Forms.Label lblGengoNengetsu;
        private common.controls.FsiTextBox txtGengoYearNengetsu;
        private System.Windows.Forms.GroupBox grpShikyubi;
        private System.Windows.Forms.Label lblDayShikyubi;
        private common.controls.FsiTextBox txtDayShikyubi;
        private System.Windows.Forms.Label lblMonthShikyubi;
        private System.Windows.Forms.Label lblYearShikyubi;
        private common.controls.FsiTextBox txtMonthShikyubi;
        private System.Windows.Forms.Label lblGengoShikyubi;
        private common.controls.FsiTextBox txtGengoYearShikyubi;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox chkCalculate;
        private System.Windows.Forms.CheckBox chkEnterAll;
        private System.Windows.Forms.Label lblMizuageShisho;
        private System.Windows.Forms.Label label1;
    }
}