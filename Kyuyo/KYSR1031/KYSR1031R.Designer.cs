﻿namespace jp.co.fsi.ky.kysr1031
{
    /// <summary>
    /// KYSR1031R の帳票
    /// </summary>
    partial class KYSR1031R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(KYSR1031R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.ボックス0 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ITEM002 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.shape1 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ITEM001 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.直線1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.ラベル4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ITEM005 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ITEM006 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.テキスト13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.直線16 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線17 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線18 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線19 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線21 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線22 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線23 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線24 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線25 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線26 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線28 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線29 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.ラベル31 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル32 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル33 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル34 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル35 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル36 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル37 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル38 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル39 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル40 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル41 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ITEM003 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM004 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtToday = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.直線88 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線90 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線91 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線92 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線93 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線94 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線95 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線96 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線97 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線98 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.直線99 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.ITEM011 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM012 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM013 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM014 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM015 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM016 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM017 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM018 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM019 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM020 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM022 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM021 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.groupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.groupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.shape2 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.shape5 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.textBox3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.総レコード数 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM002)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM001)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM005)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM006)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM003)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM004)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtToday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM011)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM012)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM013)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM014)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM015)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM016)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM017)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM018)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM019)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM020)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM022)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM021)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.総レコード数)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.ボックス0,
            this.ITEM002,
            this.shape1,
            this.ITEM001,
            this.ラベル2,
            this.直線1,
            this.直線3,
            this.ラベル4,
            this.ラベル6,
            this.ITEM005,
            this.ラベル8,
            this.ラベル9,
            this.ITEM006,
            this.テキスト13,
            this.ラベル15,
            this.直線16,
            this.直線17,
            this.直線18,
            this.直線19,
            this.直線21,
            this.直線22,
            this.直線23,
            this.直線24,
            this.直線25,
            this.直線26,
            this.直線28,
            this.直線29,
            this.ラベル31,
            this.ラベル32,
            this.ラベル33,
            this.ラベル34,
            this.ラベル35,
            this.ラベル36,
            this.ラベル37,
            this.ラベル38,
            this.ラベル39,
            this.ラベル40,
            this.ラベル41,
            this.ITEM003,
            this.ITEM004,
            this.txtToday});
            this.pageHeader.Height = 1.101394F;
            this.pageHeader.Name = "pageHeader";
            this.pageHeader.Format += new System.EventHandler(this.pageHeader_Format);
            // 
            // ボックス0
            // 
            this.ボックス0.BackColor = System.Drawing.Color.White;
            this.ボックス0.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス0.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス0.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス0.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス0.Height = 0.4340278F;
            this.ボックス0.Left = 0.002777815F;
            this.ボックス0.Name = "ボックス0";
            this.ボックス0.RoundingRadius = 9.999999F;
            this.ボックス0.Tag = "";
            this.ボックス0.Top = 0.2395833F;
            this.ボックス0.Width = 1.053128F;
            // 
            // ITEM002
            // 
            this.ITEM002.DataField = "ITEM002";
            this.ITEM002.Height = 0.1706857F;
            this.ITEM002.Left = 0.5354331F;
            this.ITEM002.Name = "ITEM002";
            this.ITEM002.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.ITEM002.Tag = "";
            this.ITEM002.Text = "ITEM002";
            this.ITEM002.Top = 0.4919455F;
            this.ITEM002.Width = 0.4952755F;
            // 
            // shape1
            // 
            this.shape1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.shape1.Height = 0.3149606F;
            this.shape1.Left = 0F;
            this.shape1.Name = "shape1";
            this.shape1.RoundingRadius = 9.999999F;
            this.shape1.Top = 0.7864336F;
            this.shape1.Width = 10.62992F;
            // 
            // ITEM001
            // 
            this.ITEM001.DataField = "ITEM001";
            this.ITEM001.Height = 0.1706857F;
            this.ITEM001.Left = 0.01181102F;
            this.ITEM001.Name = "ITEM001";
            this.ITEM001.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.ITEM001.Tag = "";
            this.ITEM001.Text = "ITEM001";
            this.ITEM001.Top = 0.4919455F;
            this.ITEM001.Width = 0.4952755F;
            // 
            // ラベル2
            // 
            this.ラベル2.Height = 0.1722933F;
            this.ラベル2.HyperLink = null;
            this.ラベル2.Left = 0.02361107F;
            this.ラベル2.Name = "ラベル2";
            this.ラベル2.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-weight: normal; text-alig" +
    "n: center; ddo-char-set: 1";
            this.ラベル2.Tag = "";
            this.ラベル2.Text = "委 託 コ ー ド";
            this.ラベル2.Top = 0.2708333F;
            this.ラベル2.Width = 1.005917F;
            // 
            // 直線1
            // 
            this.直線1.Height = 0.0001806021F;
            this.直線1.Left = 0F;
            this.直線1.LineWeight = 0F;
            this.直線1.Name = "直線1";
            this.直線1.Tag = "";
            this.直線1.Top = 0.4791666F;
            this.直線1.Width = 1.049103F;
            this.直線1.X1 = 0F;
            this.直線1.X2 = 1.049103F;
            this.直線1.Y1 = 0.4793472F;
            this.直線1.Y2 = 0.4791666F;
            // 
            // 直線3
            // 
            this.直線3.Height = 0.1993054F;
            this.直線3.Left = 0.5246062F;
            this.直線3.LineWeight = 0F;
            this.直線3.Name = "直線3";
            this.直線3.Tag = "";
            this.直線3.Top = 0.4793472F;
            this.直線3.Width = 0.0001969934F;
            this.直線3.X1 = 0.5246062F;
            this.直線3.X2 = 0.5248032F;
            this.直線3.Y1 = 0.4793472F;
            this.直線3.Y2 = 0.6786526F;
            // 
            // ラベル4
            // 
            this.ラベル4.Height = 0.2352526F;
            this.ラベル4.HyperLink = null;
            this.ラベル4.Left = 3.815278F;
            this.ラベル4.Name = "ラベル4";
            this.ラベル4.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 15pt; font-weight: bold; text-align:" +
    " center; ddo-char-set: 1";
            this.ラベル4.Tag = "";
            this.ラベル4.Text = "総 合 振 替 明 細 書";
            this.ラベル4.Top = 1.484295E-08F;
            this.ラベル4.Width = 2.641415F;
            // 
            // ラベル6
            // 
            this.ラベル6.Height = 0.15625F;
            this.ラベル6.HyperLink = null;
            this.ラベル6.Left = 3.304861F;
            this.ラベル6.Name = "ラベル6";
            this.ラベル6.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.ラベル6.Tag = "";
            this.ラベル6.Text = "御 中";
            this.ラベル6.Top = 0.3541667F;
            this.ラベル6.Width = 0.3959266F;
            // 
            // ITEM005
            // 
            this.ITEM005.DataField = "ITEM005";
            this.ITEM005.Height = 0.15625F;
            this.ITEM005.Left = 4.211024F;
            this.ITEM005.Name = "ITEM005";
            this.ITEM005.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.ITEM005.Tag = "";
            this.ITEM005.Text = "ITEM005";
            this.ITEM005.Top = 0.3996063F;
            this.ITEM005.Width = 1.420833F;
            // 
            // ラベル8
            // 
            this.ラベル8.Height = 0.15625F;
            this.ラベル8.HyperLink = null;
            this.ラベル8.Left = 5.669291F;
            this.ラベル8.Name = "ラベル8";
            this.ラベル8.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.ラベル8.Tag = "";
            this.ラベル8.Text = "振替分";
            this.ラベル8.Top = 0.3996063F;
            this.ラベル8.Width = 0.4270833F;
            // 
            // ラベル9
            // 
            this.ラベル9.Height = 0.1458333F;
            this.ラベル9.HyperLink = null;
            this.ラベル9.Left = 6.655555F;
            this.ラベル9.Name = "ラベル9";
            this.ラベル9.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.ラベル9.Tag = "";
            this.ラベル9.Text = "委 託 先 名";
            this.ラベル9.Top = 0.3125001F;
            this.ラベル9.Width = 0.6666667F;
            // 
            // ITEM006
            // 
            this.ITEM006.DataField = "ITEM006";
            this.ITEM006.Height = 0.15625F;
            this.ITEM006.Left = 6.659029F;
            this.ITEM006.Name = "ITEM006";
            this.ITEM006.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: left; ddo-char-set: 1";
            this.ITEM006.Tag = "";
            this.ITEM006.Text = "ITEM006";
            this.ITEM006.Top = 0.4611111F;
            this.ITEM006.Width = 2.952083F;
            // 
            // テキスト13
            // 
            this.テキスト13.Height = 0.15625F;
            this.テキスト13.Left = 10.07874F;
            this.テキスト13.Name = "テキスト13";
            this.テキスト13.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.テキスト13.SummaryFunc = GrapeCity.ActiveReports.SectionReportModel.SummaryFunc.Count;
            this.テキスト13.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.テキスト13.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.テキスト13.Tag = "";
            this.テキスト13.Text = null;
            this.テキスト13.Top = 0.04166669F;
            this.テキスト13.Width = 0.3583336F;
            // 
            // ラベル15
            // 
            this.ラベル15.Height = 0.15625F;
            this.ラベル15.HyperLink = null;
            this.ラベル15.Left = 10.42986F;
            this.ラベル15.Name = "ラベル15";
            this.ラベル15.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.ラベル15.Tag = "";
            this.ラベル15.Text = "頁";
            this.ラベル15.Top = 0.04166669F;
            this.ラベル15.Width = 0.1770833F;
            // 
            // 直線16
            // 
            this.直線16.Height = 0.3149605F;
            this.直線16.Left = 0F;
            this.直線16.LineWeight = 1F;
            this.直線16.Name = "直線16";
            this.直線16.Tag = "";
            this.直線16.Top = 0.7864336F;
            this.直線16.Width = 0F;
            this.直線16.X1 = 0F;
            this.直線16.X2 = 0F;
            this.直線16.Y1 = 0.7864336F;
            this.直線16.Y2 = 1.101394F;
            // 
            // 直線17
            // 
            this.直線17.Height = 0F;
            this.直線17.Left = 0F;
            this.直線17.LineWeight = 1F;
            this.直線17.Name = "直線17";
            this.直線17.Tag = "";
            this.直線17.Top = 0.7868058F;
            this.直線17.Width = 10.63194F;
            this.直線17.X1 = 0F;
            this.直線17.X2 = 10.63194F;
            this.直線17.Y1 = 0.7868058F;
            this.直線17.Y2 = 0.7868058F;
            // 
            // 直線18
            // 
            this.直線18.Height = 0F;
            this.直線18.Left = 0F;
            this.直線18.LineWeight = 1F;
            this.直線18.Name = "直線18";
            this.直線18.Tag = "";
            this.直線18.Top = 1.101394F;
            this.直線18.Width = 10.625F;
            this.直線18.X1 = 0F;
            this.直線18.X2 = 10.625F;
            this.直線18.Y1 = 1.101394F;
            this.直線18.Y2 = 1.101394F;
            // 
            // 直線19
            // 
            this.直線19.Height = 0.3149605F;
            this.直線19.Left = 10.62778F;
            this.直線19.LineWeight = 1F;
            this.直線19.Name = "直線19";
            this.直線19.Tag = "";
            this.直線19.Top = 0.7864336F;
            this.直線19.Width = 0F;
            this.直線19.X1 = 10.62778F;
            this.直線19.X2 = 10.62778F;
            this.直線19.Y1 = 0.7864336F;
            this.直線19.Y2 = 1.101394F;
            // 
            // 直線21
            // 
            this.直線21.Height = 0.3149605F;
            this.直線21.Left = 0.944882F;
            this.直線21.LineWeight = 0F;
            this.直線21.Name = "直線21";
            this.直線21.Tag = "";
            this.直線21.Top = 0.7864336F;
            this.直線21.Width = 0F;
            this.直線21.X1 = 0.944882F;
            this.直線21.X2 = 0.944882F;
            this.直線21.Y1 = 0.7864336F;
            this.直線21.Y2 = 1.101394F;
            // 
            // 直線22
            // 
            this.直線22.Height = 0.3149605F;
            this.直線22.Left = 3.815278F;
            this.直線22.LineWeight = 0F;
            this.直線22.Name = "直線22";
            this.直線22.Tag = "";
            this.直線22.Top = 0.7864336F;
            this.直線22.Width = 0F;
            this.直線22.X1 = 3.815278F;
            this.直線22.X2 = 3.815278F;
            this.直線22.Y1 = 0.7864336F;
            this.直線22.Y2 = 1.101394F;
            // 
            // 直線23
            // 
            this.直線23.Height = 0.3149605F;
            this.直線23.Left = 4.336111F;
            this.直線23.LineWeight = 0F;
            this.直線23.Name = "直線23";
            this.直線23.Tag = "";
            this.直線23.Top = 0.7864336F;
            this.直線23.Width = 0F;
            this.直線23.X1 = 4.336111F;
            this.直線23.X2 = 4.336111F;
            this.直線23.Y1 = 0.7864336F;
            this.直線23.Y2 = 1.101394F;
            // 
            // 直線24
            // 
            this.直線24.Height = 0.3149605F;
            this.直線24.Left = 4.877778F;
            this.直線24.LineWeight = 0F;
            this.直線24.Name = "直線24";
            this.直線24.Tag = "";
            this.直線24.Top = 0.7864336F;
            this.直線24.Width = 0F;
            this.直線24.X1 = 4.877778F;
            this.直線24.X2 = 4.877778F;
            this.直線24.Y1 = 0.7864336F;
            this.直線24.Y2 = 1.101394F;
            // 
            // 直線25
            // 
            this.直線25.Height = 0.3149605F;
            this.直線25.Left = 5.117361F;
            this.直線25.LineWeight = 0F;
            this.直線25.Name = "直線25";
            this.直線25.Tag = "";
            this.直線25.Top = 0.7864336F;
            this.直線25.Width = 0F;
            this.直線25.X1 = 5.117361F;
            this.直線25.X2 = 5.117361F;
            this.直線25.Y1 = 0.7864336F;
            this.直線25.Y2 = 1.101394F;
            // 
            // 直線26
            // 
            this.直線26.Height = 0.3149605F;
            this.直線26.Left = 5.909028F;
            this.直線26.LineWeight = 0F;
            this.直線26.Name = "直線26";
            this.直線26.Tag = "";
            this.直線26.Top = 0.7864336F;
            this.直線26.Width = 0F;
            this.直線26.X1 = 5.909028F;
            this.直線26.X2 = 5.909028F;
            this.直線26.Y1 = 0.7864336F;
            this.直線26.Y2 = 1.101394F;
            // 
            // 直線28
            // 
            this.直線28.Height = 0.3149605F;
            this.直線28.Left = 6.811023F;
            this.直線28.LineWeight = 0F;
            this.直線28.Name = "直線28";
            this.直線28.Tag = "";
            this.直線28.Top = 0.7864336F;
            this.直線28.Width = 0F;
            this.直線28.X1 = 6.811023F;
            this.直線28.X2 = 6.811023F;
            this.直線28.Y1 = 0.7864336F;
            this.直線28.Y2 = 1.101394F;
            // 
            // 直線29
            // 
            this.直線29.Height = 0.3149605F;
            this.直線29.Left = 8.385826F;
            this.直線29.LineWeight = 0F;
            this.直線29.Name = "直線29";
            this.直線29.Tag = "";
            this.直線29.Top = 0.7864336F;
            this.直線29.Width = 0F;
            this.直線29.X1 = 8.385826F;
            this.直線29.X2 = 8.385826F;
            this.直線29.Y1 = 0.7864336F;
            this.直線29.Y2 = 1.101394F;
            // 
            // ラベル31
            // 
            this.ラベル31.Height = 0.15625F;
            this.ラベル31.HyperLink = null;
            this.ラベル31.Left = 0.03700788F;
            this.ラベル31.Name = "ラベル31";
            this.ラベル31.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.ラベル31.Tag = "";
            this.ラベル31.Text = "契 約 者 番 号";
            this.ラベル31.Top = 0.8769849F;
            this.ラベル31.Width = 0.9314958F;
            // 
            // ラベル32
            // 
            this.ラベル32.Height = 0.15625F;
            this.ラベル32.HyperLink = null;
            this.ラベル32.Left = 1.742361F;
            this.ラベル32.Name = "ラベル32";
            this.ラベル32.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.ラベル32.Tag = "";
            this.ラベル32.Text = "口　座　名　義　人";
            this.ラベル32.Top = 0.8769849F;
            this.ラベル32.Width = 1.177083F;
            // 
            // ラベル33
            // 
            this.ラベル33.Height = 0.1165355F;
            this.ラベル33.HyperLink = null;
            this.ラベル33.Left = 3.878741F;
            this.ラベル33.Name = "ラベル33";
            this.ラベル33.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.ラベル33.Tag = "";
            this.ラベル33.Text = "銀   行";
            this.ラベル33.Top = 0.8175363F;
            this.ラベル33.Width = 0.4270833F;
            // 
            // ラベル34
            // 
            this.ラベル34.Height = 0.1480315F;
            this.ラベル34.HyperLink = null;
            this.ラベル34.Left = 3.878741F;
            this.ラベル34.Name = "ラベル34";
            this.ラベル34.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.ラベル34.Tag = "";
            this.ラベル34.Text = "コード";
            this.ラベル34.Top = 0.9340717F;
            this.ラベル34.Width = 0.4270833F;
            // 
            // ラベル35
            // 
            this.ラベル35.Height = 0.15625F;
            this.ラベル35.HyperLink = null;
            this.ラベル35.Left = 4.461111F;
            this.ラベル35.Name = "ラベル35";
            this.ラベル35.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.ラベル35.Tag = "";
            this.ラベル35.Text = "店番";
            this.ラベル35.Top = 0.8769849F;
            this.ラベル35.Width = 0.3020833F;
            // 
            // ラベル36
            // 
            this.ラベル36.Height = 0.28125F;
            this.ラベル36.HyperLink = null;
            this.ラベル36.Left = 4.920866F;
            this.ラベル36.Name = "ラベル36";
            this.ラベル36.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.ラベル36.Tag = "";
            this.ラベル36.Text = "種目";
            this.ラベル36.Top = 0.8139926F;
            this.ラベル36.Width = 0.1770833F;
            // 
            // ラベル37
            // 
            this.ラベル37.Height = 0.15625F;
            this.ラベル37.HyperLink = null;
            this.ラベル37.Left = 5.236805F;
            this.ラベル37.Name = "ラベル37";
            this.ラベル37.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.ラベル37.Tag = "";
            this.ラベル37.Text = "口座番号";
            this.ラベル37.Top = 0.8769849F;
            this.ラベル37.Width = 0.5520833F;
            // 
            // ラベル38
            // 
            this.ラベル38.Height = 0.15625F;
            this.ラベル38.HyperLink = null;
            this.ラベル38.Left = 5.929861F;
            this.ラベル38.Name = "ラベル38";
            this.ラベル38.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.ラベル38.Tag = "";
            this.ラベル38.Text = "振 替 金 額";
            this.ラベル38.Top = 0.8769849F;
            this.ラベル38.Width = 0.7395833F;
            // 
            // ラベル39
            // 
            this.ラベル39.Height = 0.1244095F;
            this.ラベル39.HyperLink = null;
            this.ラベル39.Left = 7.086618F;
            this.ラベル39.Name = "ラベル39";
            this.ラベル39.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.ラベル39.Tag = "";
            this.ラベル39.Text = "銀　　　行　　　名";
            this.ラベル39.Top = 0.8175363F;
            this.ラベル39.Width = 1.104894F;
            // 
            // ラベル40
            // 
            this.ラベル40.Height = 0.1244587F;
            this.ラベル40.HyperLink = null;
            this.ラベル40.Left = 7.086618F;
            this.ラベル40.Name = "ラベル40";
            this.ラベル40.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.ラベル40.Tag = "";
            this.ラベル40.Text = "店　　  舗　　  名";
            this.ラベル40.Top = 0.9576933F;
            this.ラベル40.Width = 1.104894F;
            // 
            // ラベル41
            // 
            this.ラベル41.Height = 0.15625F;
            this.ラベル41.HyperLink = null;
            this.ラベル41.Left = 8.815278F;
            this.ラベル41.Name = "ラベル41";
            this.ラベル41.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.ラベル41.Tag = "";
            this.ラベル41.Text = "摘　　　　　　　要";
            this.ラベル41.Top = 0.8769849F;
            this.ラベル41.Width = 1.791667F;
            // 
            // ITEM003
            // 
            this.ITEM003.DataField = "ITEM003";
            this.ITEM003.Height = 0.15625F;
            this.ITEM003.Left = 1.956693F;
            this.ITEM003.Name = "ITEM003";
            this.ITEM003.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-" +
    "weight: normal; text-align: left; ddo-char-set: 1";
            this.ITEM003.Tag = "";
            this.ITEM003.Text = "ITEM003";
            this.ITEM003.Top = 0.3425197F;
            this.ITEM003.Width = 1.287008F;
            // 
            // ITEM004
            // 
            this.ITEM004.DataField = "ITEM004";
            this.ITEM004.Height = 0.15625F;
            this.ITEM004.Left = 1.956693F;
            this.ITEM004.Name = "ITEM004";
            this.ITEM004.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-" +
    "weight: normal; text-align: left; ddo-char-set: 1";
            this.ITEM004.Tag = "";
            this.ITEM004.Text = "ITEM004";
            this.ITEM004.Top = 0.5337873F;
            this.ITEM004.Visible = false;
            this.ITEM004.Width = 1.287008F;
            // 
            // txtToday
            // 
            this.txtToday.Height = 0.15625F;
            this.txtToday.Left = 8.729135F;
            this.txtToday.Name = "txtToday";
            this.txtToday.Style = "font-family: ＭＳ 明朝; font-size: 9pt; ddo-char-set: 1";
            this.txtToday.Text = "textBox1";
            this.txtToday.Top = 0.04055119F;
            this.txtToday.Width = 1.270833F;
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.直線88,
            this.直線90,
            this.直線91,
            this.直線92,
            this.直線93,
            this.直線94,
            this.直線95,
            this.直線96,
            this.直線97,
            this.直線98,
            this.直線99,
            this.ITEM011,
            this.ITEM012,
            this.ITEM013,
            this.ITEM014,
            this.ITEM015,
            this.ITEM016,
            this.ITEM017,
            this.ITEM018,
            this.ITEM019,
            this.ITEM020,
            this.ITEM022,
            this.ITEM021});
            this.detail.Height = 0.3648185F;
            this.detail.Name = "detail";
            this.detail.Format += new System.EventHandler(this.detail_Format);
            // 
            // 直線88
            // 
            this.直線88.Height = 0.3543307F;
            this.直線88.Left = 0F;
            this.直線88.LineWeight = 1F;
            this.直線88.Name = "直線88";
            this.直線88.Tag = "";
            this.直線88.Top = 0.003543307F;
            this.直線88.Width = 0F;
            this.直線88.X1 = 0F;
            this.直線88.X2 = 0F;
            this.直線88.Y1 = 0.003543307F;
            this.直線88.Y2 = 0.357874F;
            // 
            // 直線90
            // 
            this.直線90.Height = 0F;
            this.直線90.Left = 0F;
            this.直線90.LineWeight = 0F;
            this.直線90.Name = "直線90";
            this.直線90.Tag = "";
            this.直線90.Top = 0.357874F;
            this.直線90.Width = 10.62992F;
            this.直線90.X1 = 0F;
            this.直線90.X2 = 10.62992F;
            this.直線90.Y1 = 0.357874F;
            this.直線90.Y2 = 0.357874F;
            // 
            // 直線91
            // 
            this.直線91.Height = 0.3543307F;
            this.直線91.Left = 10.62778F;
            this.直線91.LineWeight = 1F;
            this.直線91.Name = "直線91";
            this.直線91.Tag = "";
            this.直線91.Top = 0.003543307F;
            this.直線91.Width = 0F;
            this.直線91.X1 = 10.62778F;
            this.直線91.X2 = 10.62778F;
            this.直線91.Y1 = 0.003543307F;
            this.直線91.Y2 = 0.357874F;
            // 
            // 直線92
            // 
            this.直線92.Height = 0.3543307F;
            this.直線92.Left = 0.944882F;
            this.直線92.LineWeight = 0F;
            this.直線92.Name = "直線92";
            this.直線92.Tag = "";
            this.直線92.Top = 0.003543307F;
            this.直線92.Width = 0F;
            this.直線92.X1 = 0.944882F;
            this.直線92.X2 = 0.944882F;
            this.直線92.Y1 = 0.003543307F;
            this.直線92.Y2 = 0.357874F;
            // 
            // 直線93
            // 
            this.直線93.Height = 0.3543307F;
            this.直線93.Left = 3.815279F;
            this.直線93.LineWeight = 0F;
            this.直線93.Name = "直線93";
            this.直線93.Tag = "";
            this.直線93.Top = 0.003543307F;
            this.直線93.Width = 0F;
            this.直線93.X1 = 3.815279F;
            this.直線93.X2 = 3.815279F;
            this.直線93.Y1 = 0.003543307F;
            this.直線93.Y2 = 0.357874F;
            // 
            // 直線94
            // 
            this.直線94.Height = 0.3543307F;
            this.直線94.Left = 4.330709F;
            this.直線94.LineWeight = 0F;
            this.直線94.Name = "直線94";
            this.直線94.Tag = "";
            this.直線94.Top = 0.003543307F;
            this.直線94.Width = 0F;
            this.直線94.X1 = 4.330709F;
            this.直線94.X2 = 4.330709F;
            this.直線94.Y1 = 0.003543307F;
            this.直線94.Y2 = 0.357874F;
            // 
            // 直線95
            // 
            this.直線95.Height = 0.3543307F;
            this.直線95.Left = 4.877779F;
            this.直線95.LineWeight = 0F;
            this.直線95.Name = "直線95";
            this.直線95.Tag = "";
            this.直線95.Top = 0.003543307F;
            this.直線95.Width = 0F;
            this.直線95.X1 = 4.877779F;
            this.直線95.X2 = 4.877779F;
            this.直線95.Y1 = 0.003543307F;
            this.直線95.Y2 = 0.357874F;
            // 
            // 直線96
            // 
            this.直線96.Height = 0.3543307F;
            this.直線96.Left = 5.12014F;
            this.直線96.LineWeight = 0F;
            this.直線96.Name = "直線96";
            this.直線96.Tag = "";
            this.直線96.Top = 0.003543307F;
            this.直線96.Width = 0F;
            this.直線96.X1 = 5.12014F;
            this.直線96.X2 = 5.12014F;
            this.直線96.Y1 = 0.003543307F;
            this.直線96.Y2 = 0.357874F;
            // 
            // 直線97
            // 
            this.直線97.Height = 0.3543307F;
            this.直線97.Left = 5.909029F;
            this.直線97.LineWeight = 0F;
            this.直線97.Name = "直線97";
            this.直線97.Tag = "";
            this.直線97.Top = 0.003543307F;
            this.直線97.Width = 0F;
            this.直線97.X1 = 5.909029F;
            this.直線97.X2 = 5.909029F;
            this.直線97.Y1 = 0.003543307F;
            this.直線97.Y2 = 0.357874F;
            // 
            // 直線98
            // 
            this.直線98.Height = 0.3543307F;
            this.直線98.Left = 6.811023F;
            this.直線98.LineWeight = 0F;
            this.直線98.Name = "直線98";
            this.直線98.Tag = "";
            this.直線98.Top = 0.003543307F;
            this.直線98.Width = 0F;
            this.直線98.X1 = 6.811023F;
            this.直線98.X2 = 6.811023F;
            this.直線98.Y1 = 0.003543307F;
            this.直線98.Y2 = 0.357874F;
            // 
            // 直線99
            // 
            this.直線99.Height = 0.3543307F;
            this.直線99.Left = 8.385826F;
            this.直線99.LineWeight = 0F;
            this.直線99.Name = "直線99";
            this.直線99.Tag = "";
            this.直線99.Top = 0.003543307F;
            this.直線99.Width = 0F;
            this.直線99.X1 = 8.385826F;
            this.直線99.X2 = 8.385826F;
            this.直線99.Y1 = 0.003543307F;
            this.直線99.Y2 = 0.357874F;
            // 
            // ITEM011
            // 
            this.ITEM011.DataField = "ITEM011";
            this.ITEM011.Height = 0.1980315F;
            this.ITEM011.Left = 0.002617624F;
            this.ITEM011.Name = "ITEM011";
            this.ITEM011.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: left; ddo-char-set: 1";
            this.ITEM011.Tag = "";
            this.ITEM011.Text = "ITEM011";
            this.ITEM011.Top = 0.04173229F;
            this.ITEM011.Width = 0.9422646F;
            // 
            // ITEM012
            // 
            this.ITEM012.DataField = "ITEM012";
            this.ITEM012.Height = 0.1980315F;
            this.ITEM012.Left = 0.9929132F;
            this.ITEM012.Name = "ITEM012";
            this.ITEM012.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: left; ddo-char-set: 1";
            this.ITEM012.Tag = "";
            this.ITEM012.Text = "ITEM012";
            this.ITEM012.Top = 0.04173229F;
            this.ITEM012.Width = 1.129167F;
            // 
            // ITEM013
            // 
            this.ITEM013.DataField = "ITEM013";
            this.ITEM013.Height = 0.1980315F;
            this.ITEM013.Left = 2.141339F;
            this.ITEM013.Name = "ITEM013";
            this.ITEM013.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: left; ddo-char-set: 1";
            this.ITEM013.Tag = "";
            this.ITEM013.Text = "ITEM013";
            this.ITEM013.Top = 0.04173229F;
            this.ITEM013.Width = 1.220975F;
            // 
            // ITEM014
            // 
            this.ITEM014.DataField = "ITEM014";
            this.ITEM014.Height = 0.1980315F;
            this.ITEM014.Left = 3.385827F;
            this.ITEM014.Name = "ITEM014";
            this.ITEM014.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.ITEM014.Tag = "";
            this.ITEM014.Text = "1001\r\n\r\n";
            this.ITEM014.Top = 0.04173229F;
            this.ITEM014.Width = 0.4126475F;
            // 
            // ITEM015
            // 
            this.ITEM015.DataField = "ITEM015";
            this.ITEM015.Height = 0.1980315F;
            this.ITEM015.Left = 3.867185F;
            this.ITEM015.Name = "ITEM015";
            this.ITEM015.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.ITEM015.Tag = "";
            this.ITEM015.Text = "ITEM015";
            this.ITEM015.Top = 0.04173229F;
            this.ITEM015.Width = 0.43125F;
            // 
            // ITEM016
            // 
            this.ITEM016.DataField = "ITEM016";
            this.ITEM016.Height = 0.1980315F;
            this.ITEM016.Left = 4.377815F;
            this.ITEM016.Name = "ITEM016";
            this.ITEM016.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.ITEM016.Tag = "";
            this.ITEM016.Text = "ITEM016";
            this.ITEM016.Top = 0.04173229F;
            this.ITEM016.Width = 0.4729168F;
            // 
            // ITEM017
            // 
            this.ITEM017.DataField = "ITEM017";
            this.ITEM017.Height = 0.1980315F;
            this.ITEM017.Left = 4.919309F;
            this.ITEM017.Name = "ITEM017";
            this.ITEM017.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: center; ddo-char-set: 1";
            this.ITEM017.Tag = "";
            this.ITEM017.Text = "ITEM017";
            this.ITEM017.Top = 0.04173229F;
            this.ITEM017.Width = 0.1708333F;
            // 
            // ITEM018
            // 
            this.ITEM018.DataField = "ITEM018";
            this.ITEM018.Height = 0.1980315F;
            this.ITEM018.Left = 5.157502F;
            this.ITEM018.Name = "ITEM018";
            this.ITEM018.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.ITEM018.Tag = "";
            this.ITEM018.Text = "ITEM018";
            this.ITEM018.Top = 0.04173229F;
            this.ITEM018.Width = 0.7020833F;
            // 
            // ITEM019
            // 
            this.ITEM019.DataField = "ITEM019";
            this.ITEM019.Height = 0.1980315F;
            this.ITEM019.Left = 5.917323F;
            this.ITEM019.Name = "ITEM019";
            this.ITEM019.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.ITEM019.Tag = "";
            this.ITEM019.Text = "ITEM019";
            this.ITEM019.Top = 0.04173229F;
            this.ITEM019.Width = 0.8700791F;
            // 
            // ITEM020
            // 
            this.ITEM020.CanGrow = false;
            this.ITEM020.DataField = "ITEM020";
            this.ITEM020.Height = 0.153937F;
            this.ITEM020.Left = 6.842126F;
            this.ITEM020.Name = "ITEM020";
            this.ITEM020.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: left; ddo-char-set: 1";
            this.ITEM020.Tag = "";
            this.ITEM020.Text = "ITEM020";
            this.ITEM020.Top = 0.01811024F;
            this.ITEM020.Width = 1.504167F;
            // 
            // ITEM022
            // 
            this.ITEM022.DataField = "ITEM022";
            this.ITEM022.Height = 0.2767717F;
            this.ITEM022.Left = 8.425195F;
            this.ITEM022.Name = "ITEM022";
            this.ITEM022.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: left; ddo-char-set: 1";
            this.ITEM022.Tag = "";
            this.ITEM022.Text = "ITEM022";
            this.ITEM022.Top = 0.04173229F;
            this.ITEM022.Width = 2.123278F;
            // 
            // ITEM021
            // 
            this.ITEM021.CanGrow = false;
            this.ITEM021.DataField = "ITEM021";
            this.ITEM021.Height = 0.1618111F;
            this.ITEM021.Left = 6.842126F;
            this.ITEM021.Name = "ITEM021";
            this.ITEM021.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: left; ddo-char-set: 1";
            this.ITEM021.Tag = "";
            this.ITEM021.Text = "ITEM021";
            this.ITEM021.Top = 0.1688976F;
            this.ITEM021.Width = 1.504167F;
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            // 
            // groupHeader1
            // 
            this.groupHeader1.DataField = "ITEM010";
            this.groupHeader1.Height = 0F;
            this.groupHeader1.Name = "groupHeader1";
            // 
            // groupFooter1
            // 
            this.groupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.shape2,
            this.label8,
            this.label9,
            this.label10,
            this.shape5,
            this.line1,
            this.line2,
            this.line3,
            this.textBox3,
            this.textBox4,
            this.label11,
            this.label12,
            this.textBox5,
            this.textBox6,
            this.line10,
            this.line11,
            this.line12,
            this.label13,
            this.label7,
            this.総レコード数});
            this.groupFooter1.Height = 0.7874014F;
            this.groupFooter1.Name = "groupFooter1";
            this.groupFooter1.Format += new System.EventHandler(this.ITEM010_Format);
            // 
            // shape2
            // 
            this.shape2.Height = 0.7874014F;
            this.shape2.Left = 8.385824F;
            this.shape2.Name = "shape2";
            this.shape2.RoundingRadius = 9.999999F;
            this.shape2.Top = 0F;
            this.shape2.Width = 2.244095F;
            // 
            // label8
            // 
            this.label8.Height = 0.3070866F;
            this.label8.HyperLink = null;
            this.label8.Left = 9.870082F;
            this.label8.Name = "label8";
            this.label8.Style = "font-family: ＭＳ 明朝; font-size: 11pt; text-align: center; ddo-char-set: 1";
            this.label8.Text = "係　印";
            this.label8.Top = 0.03149607F;
            this.label8.Width = 0.7874007F;
            // 
            // label9
            // 
            this.label9.Height = 0.3070866F;
            this.label9.HyperLink = null;
            this.label9.Left = 9.098422F;
            this.label9.Name = "label9";
            this.label9.Style = "font-family: ＭＳ 明朝; font-size: 11pt; text-align: center; ddo-char-set: 1";
            this.label9.Text = "照　査";
            this.label9.Top = 0.03149607F;
            this.label9.Width = 0.7874007F;
            // 
            // label10
            // 
            this.label10.Height = 0.3070866F;
            this.label10.HyperLink = null;
            this.label10.Left = 8.326769F;
            this.label10.Name = "label10";
            this.label10.Style = "font-family: ＭＳ 明朝; font-size: 11pt; text-align: center; ddo-char-set: 1";
            this.label10.Text = "検　印";
            this.label10.Top = 0.03149607F;
            this.label10.Width = 0.7874007F;
            // 
            // shape5
            // 
            this.shape5.Height = 0.5905511F;
            this.shape5.Left = 4.330708F;
            this.shape5.Name = "shape5";
            this.shape5.RoundingRadius = 9.999999F;
            this.shape5.Top = 0F;
            this.shape5.Width = 2.480315F;
            // 
            // line1
            // 
            this.line1.Height = 0F;
            this.line1.Left = 4.330708F;
            this.line1.LineWeight = 1F;
            this.line1.Name = "line1";
            this.line1.Top = 0.2952756F;
            this.line1.Width = 2.480314F;
            this.line1.X1 = 4.330708F;
            this.line1.X2 = 6.811022F;
            this.line1.Y1 = 0.2952756F;
            this.line1.Y2 = 0.2952756F;
            // 
            // line2
            // 
            this.line2.Height = 0.5905511F;
            this.line2.Left = 5.118111F;
            this.line2.LineWeight = 1F;
            this.line2.Name = "line2";
            this.line2.Top = 0F;
            this.line2.Width = 0F;
            this.line2.X1 = 5.118111F;
            this.line2.X2 = 5.118111F;
            this.line2.Y1 = 0F;
            this.line2.Y2 = 0.5905511F;
            // 
            // line3
            // 
            this.line3.Height = 0.5905511F;
            this.line3.Left = 5.905511F;
            this.line3.LineWeight = 1F;
            this.line3.Name = "line3";
            this.line3.Top = 0F;
            this.line3.Width = 0F;
            this.line3.X1 = 5.905511F;
            this.line3.X2 = 5.905511F;
            this.line3.Y1 = 0F;
            this.line3.Y2 = 0.5905511F;
            // 
            // textBox3
            // 
            this.textBox3.DataField = "ITEM023";
            this.textBox3.Height = 0.1771654F;
            this.textBox3.Left = 5.157479F;
            this.textBox3.Name = "textBox3";
            this.textBox3.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.textBox3.SummaryGroup = "groupHeader1";
            this.textBox3.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox3.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox3.Tag = "";
            this.textBox3.Text = "ITEM023";
            this.textBox3.Top = 0.05118111F;
            this.textBox3.Width = 0.5641734F;
            // 
            // textBox4
            // 
            this.textBox4.DataField = "ITEM019";
            this.textBox4.Height = 0.173622F;
            this.textBox4.Left = 5.917321F;
            this.textBox4.Name = "textBox4";
            this.textBox4.OutputFormat = resources.GetString("textBox4.OutputFormat");
            this.textBox4.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.textBox4.SummaryGroup = "groupHeader1";
            this.textBox4.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox4.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox4.Tag = "";
            this.textBox4.Text = "ITEM019";
            this.textBox4.Top = 0.05118111F;
            this.textBox4.Width = 0.8700791F;
            // 
            // label11
            // 
            this.label11.Height = 0.1562828F;
            this.label11.HyperLink = null;
            this.label11.Left = 4.488189F;
            this.label11.Name = "label11";
            this.label11.Style = "font-family: ＭＳ 明朝; ddo-char-set: 1";
            this.label11.Text = "頁　計";
            this.label11.Top = 0.05118111F;
            this.label11.Width = 0.4680934F;
            // 
            // label12
            // 
            this.label12.Height = 0.1562828F;
            this.label12.HyperLink = null;
            this.label12.Left = 4.488189F;
            this.label12.Name = "label12";
            this.label12.Style = "font-family: ＭＳ 明朝; ddo-char-set: 1";
            this.label12.Text = "合　計\r\n";
            this.label12.Top = 0.3779528F;
            this.label12.Width = 0.4602199F;
            // 
            // textBox5
            // 
            this.textBox5.DataField = "ITEM023";
            this.textBox5.Height = 0.169685F;
            this.textBox5.Left = 5.153543F;
            this.textBox5.Name = "textBox5";
            this.textBox5.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.textBox5.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.textBox5.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.textBox5.Tag = "";
            this.textBox5.Text = "ITEM023";
            this.textBox5.Top = 0.3622048F;
            this.textBox5.Visible = false;
            this.textBox5.Width = 0.5641734F;
            // 
            // textBox6
            // 
            this.textBox6.DataField = "ITEM019";
            this.textBox6.Height = 0.173622F;
            this.textBox6.Left = 5.917321F;
            this.textBox6.Name = "textBox6";
            this.textBox6.OutputFormat = resources.GetString("textBox6.OutputFormat");
            this.textBox6.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.textBox6.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.textBox6.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.textBox6.Tag = "";
            this.textBox6.Text = "ITEM019";
            this.textBox6.Top = 0.3622048F;
            this.textBox6.Visible = false;
            this.textBox6.Width = 0.8700791F;
            // 
            // line10
            // 
            this.line10.Height = 0.7874014F;
            this.line10.Left = 9.094485F;
            this.line10.LineWeight = 1F;
            this.line10.Name = "line10";
            this.line10.Top = 0F;
            this.line10.Width = 0F;
            this.line10.X1 = 9.094485F;
            this.line10.X2 = 9.094485F;
            this.line10.Y1 = 0F;
            this.line10.Y2 = 0.7874014F;
            // 
            // line11
            // 
            this.line11.Height = 0.7874014F;
            this.line11.Left = 9.881886F;
            this.line11.LineWeight = 1F;
            this.line11.Name = "line11";
            this.line11.Top = 0F;
            this.line11.Width = 0F;
            this.line11.X1 = 9.881886F;
            this.line11.X2 = 9.881886F;
            this.line11.Y1 = 0F;
            this.line11.Y2 = 0.7874014F;
            // 
            // line12
            // 
            this.line12.Height = 0F;
            this.line12.Left = 8.385824F;
            this.line12.LineWeight = 1F;
            this.line12.Name = "line12";
            this.line12.Top = 0.2362205F;
            this.line12.Width = 2.244096F;
            this.line12.X1 = 8.385824F;
            this.line12.X2 = 10.62992F;
            this.line12.Y1 = 0.2362205F;
            this.line12.Y2 = 0.2362205F;
            // 
            // label13
            // 
            this.label13.Height = 0.1653543F;
            this.label13.HyperLink = null;
            this.label13.Left = 5.72047F;
            this.label13.Name = "label13";
            this.label13.Style = "font-family: ＭＳ 明朝; font-size: 10pt; ddo-char-set: 1";
            this.label13.Text = "件";
            this.label13.Top = 0.05905513F;
            this.label13.Width = 0.1736393F;
            // 
            // label7
            // 
            this.label7.Height = 0.1653543F;
            this.label7.HyperLink = null;
            this.label7.Left = 5.72047F;
            this.label7.Name = "label7";
            this.label7.Style = "font-family: ＭＳ 明朝; font-size: 10pt; ddo-char-set: 1";
            this.label7.Text = "件";
            this.label7.Top = 0.3748032F;
            this.label7.Width = 0.1736393F;
            // 
            // 総レコード数
            // 
            this.総レコード数.DataField = "ITEM024";
            this.総レコード数.Height = 0.1574803F;
            this.総レコード数.Left = 2.362204F;
            this.総レコード数.Name = "総レコード数";
            this.総レコード数.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.総レコード数.Tag = "";
            this.総レコード数.Text = "総レコード数";
            this.総レコード数.Top = 0.3149607F;
            this.総レコード数.Visible = false;
            this.総レコード数.Width = 1.023622F;
            // 
            // KYUR2051R
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.1968504F;
            this.PageSettings.Margins.Left = 0.3937008F;
            this.PageSettings.Margins.Right = 0.3937008F;
            this.PageSettings.Margins.Top = 0.5905512F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
            this.PageSettings.PaperHeight = 11.69291F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.267716F;
            this.PrintWidth = 10.70866F;
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.groupHeader1);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.groupFooter1);
            this.Sections.Add(this.pageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.ITEM002)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM001)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM005)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM006)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.テキスト13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM003)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM004)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtToday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM011)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM012)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM013)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM014)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM015)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM016)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM017)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM018)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM019)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM020)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM022)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM021)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.総レコード数)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス0;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM002;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM001;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル2;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線1;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線3;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル4;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM005;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル8;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM006;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox テキスト13;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル15;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線16;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線17;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線18;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線19;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線21;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線22;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線23;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線24;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線25;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線26;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線28;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線29;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル31;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル32;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル33;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル34;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル35;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル36;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル37;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル38;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル39;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル40;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル41;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM003;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM004;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線88;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線90;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線91;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線92;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線93;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線94;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線95;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線96;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線97;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線98;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線99;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM011;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM012;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM013;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM014;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM015;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM016;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM017;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM018;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM019;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM020;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM022;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM021;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader groupHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter groupFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape2;
        private GrapeCity.ActiveReports.SectionReportModel.Label label8;
        private GrapeCity.ActiveReports.SectionReportModel.Label label9;
        private GrapeCity.ActiveReports.SectionReportModel.Label label10;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape5;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
        private GrapeCity.ActiveReports.SectionReportModel.Line line2;
        private GrapeCity.ActiveReports.SectionReportModel.Line line3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox4;
        private GrapeCity.ActiveReports.SectionReportModel.Label label11;
        private GrapeCity.ActiveReports.SectionReportModel.Label label12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox6;
        private GrapeCity.ActiveReports.SectionReportModel.Line line10;
        private GrapeCity.ActiveReports.SectionReportModel.Line line11;
        private GrapeCity.ActiveReports.SectionReportModel.Line line12;
        private GrapeCity.ActiveReports.SectionReportModel.Label label13;
        private GrapeCity.ActiveReports.SectionReportModel.Label label7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox 総レコード数;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtToday;
    }
}
