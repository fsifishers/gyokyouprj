﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using jp.co.fsi.common.report;
using System.Data;
using jp.co.fsi.common.util;

namespace jp.co.fsi.ky.kysr1031
{
    /// <summary>
    /// KYSR1031R の帳票
    /// </summary>
    public partial class KYSR1031R : BaseReport
    {

        public KYSR1031R(DataTable tgtData)
            : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }

        private int i = 1; //レコードカウント用変数
        /// <summary>
        /// ページヘッダーの設定
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pageHeader_Format(object sender, EventArgs e)
        {
            //和暦でDataTimeを文字列に変換する
            System.Globalization.CultureInfo ci =
                new System.Globalization.CultureInfo("ja-JP", false);
            ci.DateTimeFormat.Calendar = new System.Globalization.JapaneseCalendar();

            this.txtToday.Text = DateTime.Now.ToString("gy年 M月 d日", ci);
        }


        private void detail_Format(object sender, EventArgs e)
        {
            // 5行目は太線に変更
            if (i == 5)
            {
                i = 0; // 初期化 
                this.直線90.LineWeight = 3;
            }
            if (i == 1)
            {
                this.直線90.LineWeight = 1;
            }
            i++;
        }

        private void ITEM010_Format(object sender, EventArgs e)
        {
            // 最後のグループフッタだけ合計を表示
            if (Util.ToInt(this.総レコード数.Value) == Util.ToInt(this.textBox5.Value))
            {
                this.label7.Visible = true;
                this.textBox5.Visible = true;
                this.textBox6.Visible = true;
            }
            else
            {
                this.label7.Visible = false;
                this.textBox5.Visible = false;
                this.textBox6.Visible = false;
            }
        }
    }
}
