﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Text;
using System.Windows.Forms;

using GrapeCity.ActiveReports;

using jp.co.fsi.common.access;
using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.ky.kysr1031
{
    /// <summary>
    /// 銀行振込一覧表(KYSR1031)
    /// </summary>
    public partial class KYSR1031 : BasePgForm
    {
        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KYSR1031()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 支給年月・振替日の初期値取得
            string[] jpDate;
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            string sql = "SELECT MAX(NENGETSU) AS MAX_NENGETSU";
            sql += " FROM TB_KY_SHOYO_MEISAI_KINTAI WHERE KAISHA_CD = @KAISHA_CD";
            DataTable dtShikyuInfo =
                this.Dba.GetDataTableFromSqlWithParams(sql, dpc);
            if (dtShikyuInfo.Rows.Count == 0 || ValChk.IsEmpty(dtShikyuInfo.Rows[0]["MAX_NENGETSU"]))
            {
                // 該当支給データ存在しない場合はシステム日付をセット
                jpDate = Util.ConvJpDate(DateTime.Now, this.Dba);
            }
            else
            {
                jpDate = Util.ConvJpDate(Util.ToDate(dtShikyuInfo.Rows[0]["MAX_NENGETSU"]), this.Dba);
            }
            lblGengoNengetsu.Text = jpDate[0];
            txtGengoYearNengetsu.Text = jpDate[2];
            txtMonthNengetsu.Text     = jpDate[3];
            lblGengoFurikomi.Text = jpDate[0];
            txtGengoYearFurikomi.Text = jpDate[2];
            txtMonthFurikomi.Text = jpDate[3];
            txtDayFurikomi.Text = jpDate[4];

            // 初期フォーカス
            this.txtGengoYearNengetsu.Focus();
            this.txtGengoYearNengetsu.SelectAll();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 元号年にフォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtGengoYearNengetsu":
                case "txtGengoYearFurikomi":
                    this.btnF1.Enabled = true;
                    break;
                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        public override void PressF1()
        {
            //MEMO:現状アクティブなコントロールごとに処理を実装してください。
            switch (this.ActiveCtlNm)
            {
                case "txtGengoYearNengetsu":
                case "txtGengoYearFurikomi":
                    #region 元号検索
                    // アセンブリのロード
                    System.Reflection.Assembly asm = System.Reflection.Assembly.LoadFrom("CMCM1021.exe");
                    // フォーム作成
                    Type t = asm.GetType("jp.co.fsi.com.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            if (this.ActiveCtlNm == "txtGengoYearNengetsu")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.InData = this.lblGengoNengetsu.Text;
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.lblGengoNengetsu.Text = result[1];

                                    // 存在しない日付の場合、補正して存在する日付に戻す
                                    string[] arrJpDate =
                                        Util.FixJpDate(this.lblGengoNengetsu.Text,
                                            this.txtGengoYearNengetsu.Text,
                                            this.txtMonthNengetsu.Text,
                                            "1",
                                            this.Dba);
                                    this.lblGengoNengetsu.Text = arrJpDate[0];
                                    this.txtGengoYearNengetsu.Text = arrJpDate[2];
                                    this.txtMonthNengetsu.Text = arrJpDate[3];
                                }
                            }
                            if (this.ActiveCtlNm == "txtGengoYearFurikomi")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.InData = this.lblGengoFurikomi.Text;
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.lblGengoFurikomi.Text = result[1];

                                    // 存在しない日付の場合、補正して存在する日付に戻す
                                    string[] arrJpDate =
                                        Util.FixJpDate(this.lblGengoFurikomi.Text,
                                            this.txtGengoYearFurikomi.Text,
                                            this.txtMonthFurikomi.Text,
                                            this.txtDayFurikomi.Text,
                                            this.Dba);
                                    this.lblGengoFurikomi.Text = arrJpDate[0];
                                    this.txtGengoYearFurikomi.Text = arrJpDate[2];
                                    this.txtMonthFurikomi.Text = arrJpDate[3];
                                    this.txtDayFurikomi.Text = arrJpDate[4];
                                }
                            }
                        }
                    }
                    break;
                    #endregion
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
            {
                // プレビュー処理
                DoPrint(true);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            if (Msg.ConfNmYesNo("印刷", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }

        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            // 設定画面の起動
            // MEMO:原則としてここで渡す帳票IDの設定はReport.csvに保持していることが前提ですが、
            // 保持していない場合は、設定画面での保存(F6)時に新規に設定が保持されます。
            PrintSettingForm psForm = new PrintSettingForm(new string[1] { "KYSR1031R" });
            psForm.ShowDialog();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 支給月(年)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGengoYearNengetsu_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtGengoYearNengetsu.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtGengoYearNengetsu.SelectAll();
                e.Cancel = true;
                return;
            }

            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            this.SetJpDateNengetsu(this.FixJpDate(this.lblGengoNengetsu.Text, this.txtGengoYearNengetsu.Text,
                this.txtMonthNengetsu.Text, "1", this.Dba));
        }

        /// <summary>
        /// 支給月(月)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMonthNengetsu_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtMonthNengetsu.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtMonthNengetsu.SelectAll();
                e.Cancel = true;
                return;
            }

            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            this.SetJpDateNengetsu(this.FixJpDate(this.lblGengoNengetsu.Text, this.txtGengoYearNengetsu.Text,
                this.txtMonthNengetsu.Text, "1", this.Dba));
        }

        /// <summary>
        /// 振替日(年)値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGengoYearFurikomi_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtGengoYearFurikomi.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtGengoYearFurikomi.SelectAll();
                e.Cancel = true;
                return;
            }

            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDateFurikomi(this.FixJpDate(this.lblGengoFurikomi.Text, this.txtGengoYearFurikomi.Text,
                this.txtMonthFurikomi.Text, this.txtDayFurikomi.Text, this.Dba));
        }

        /// <summary>
        /// 振替日(月)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMonthFurikomi_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtMonthFurikomi.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtMonthFurikomi.SelectAll();
                e.Cancel = true;
                return;
            }

            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDateFurikomi(this.FixJpDate(this.lblGengoFurikomi.Text, this.txtGengoYearFurikomi.Text,
                this.txtMonthFurikomi.Text, this.txtDayFurikomi.Text, this.Dba));
        }

        /// <summary>
        /// 振替日(日)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDayFurikomi_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtDayFurikomi.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtDayFurikomi.SelectAll();
                e.Cancel = true;
                return;
            }

            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            this.SetJpDateFurikomi(this.FixJpDate(this.lblGengoFurikomi.Text, this.txtGengoYearFurikomi.Text,
                this.txtMonthFurikomi.Text, this.txtDayFurikomi.Text, this.Dba));
        }

        /// <summary>
        /// 社員順のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rdoOrder1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
                {
                    // プレビュー処理
                    DoPrint(true);
                }
                else
                {
                    this.rdoOrder1.Focus();
                }
            }
        }
        /// <summary>
        /// 部門順のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rdoOrder2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
                {
                    // プレビュー処理
                    DoPrint(true);
                }
                else
                {
                    this.rdoOrder2.Focus();
                }
            }
        }

        #endregion

        #region privateメソッド
        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview)
        {
            try
            {
                bool dataFlag;

                this.Dba.BeginTransaction();

                // 帳票出力用にワークテーブルにデータを作成
                dataFlag = MakeWkData();

                #region KYUR2051出力
                // 帳票出力
                if (dataFlag)
                {
                    #region 取得列の定義
                    StringBuilder cols = new StringBuilder();
                    cols.Append("  ITEM001");
                    cols.Append(" ,ITEM002");
                    cols.Append(" ,ITEM003");
                    cols.Append(" ,ITEM004");
                    cols.Append(" ,ITEM005");
                    cols.Append(" ,ITEM006");
                    cols.Append(" ,ITEM007");
                    cols.Append(" ,ITEM008");
                    cols.Append(" ,ITEM009");
                    cols.Append(" ,ITEM010");
                    cols.Append(" ,ITEM011");
                    cols.Append(" ,ITEM012");
                    cols.Append(" ,ITEM013");
                    cols.Append(" ,ITEM014");
                    cols.Append(" ,ITEM015");
                    cols.Append(" ,ITEM016");
                    cols.Append(" ,ITEM017");
                    cols.Append(" ,ITEM018");
                    cols.Append(" ,ITEM019");
                    cols.Append(" ,ITEM020");
                    cols.Append(" ,ITEM021");
                    cols.Append(" ,ITEM022");
                    cols.Append(" ,ITEM023");
                    cols.Append(" ,ITEM024");
                    #endregion

                    // バインドパラメータの設定
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);

                    // データの取得
                    DataTable dtOutput = this.Dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_KY_TBL", "GUID = @GUID", "SORT ASC", dpc);

                    // 帳票オブジェクトをインスタンス化
                    KYSR1031R rpt = new KYSR1031R(dtOutput);

                    if (isPreview)
                    {
                        // プレビュー画面表示
                        PreviewForm pFrm = new PreviewForm(rpt, this.UnqId);
                        pFrm.WindowState = FormWindowState.Maximized;
                        pFrm.Show();
                    }
                    else
                    {
                        // 直接印刷
                        rpt.Run(false);
                        rpt.Document.Print(true, true, false);
                    }
                }
                #endregion
            }
            finally
            {
                this.Dba.Rollback();
            }
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData()
        {
            // 支給月・振替日を西暦にして保持
            DateTime nengetsu = Util.ConvAdDate(this.lblGengoNengetsu.Text, this.txtGengoYearNengetsu.Text,
                    this.txtMonthNengetsu.Text, "1", this.Dba);
            DateTime furikomi = Util.ConvAdDate(this.lblGengoFurikomi.Text, this.txtGengoYearFurikomi.Text,
                    this.txtMonthFurikomi.Text, this.txtDayFurikomi.Text, this.Dba);
            // 支給月・振替日を和暦で保持
            string[] jpNengetsu = Util.ConvJpDate(nengetsu, this.Dba);
            string[] jpFurikomi = Util.ConvJpDate(furikomi, this.Dba);

            #region メインループデータ取得

            DbParamCollection dpc = new DbParamCollection();
            StringBuilder sql = new StringBuilder();

            // 会社情報
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 2, this.UInfo.KaishaCd);
            DataTable dtKaisha = this.Dba.GetDataTableByConditionWithParams(
                "*",
                "VI_KY_KAISHA_JOHO",
                "KAISHA_CD = @KAISHA_CD", dpc);
            if (dtKaisha.Rows.Count == 0)
            {
                return false;
            }

            // 賞与明細項目設定・振込レコード
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 2, this.UInfo.KaishaCd);
            DataTable dtSti = this.Dba.GetDataTableByConditionWithParams(
                "*",
                "VI_KY_SHOYO_KOMOKU_SETTEI",
                "KAISHA_CD = @KAISHA_CD AND KOMOKU_SHUBETSU = 4"            // 合計レコード
                    + " AND (KOMOKU_KUBUN3 = 42 OR KOMOKU_KUBUN3 = 43)",    // 項目区分(42振込額１　43振込額２)
                dpc);
            if (dtSti.Rows.Count == 0)
            {
                return false;
            }

            // 各社員振込額データ
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 2, this.UInfo.KaishaCd);
            dpc.SetParam("@NENGETSU", SqlDbType.VarChar, 10, nengetsu.Date.ToString("yyyy/MM/dd"));
            // 振込額１・２フィールド名
            string wfld1 = "0";         // 設定データに定義ない場合は０値
            string wfld2 = "0";
            foreach (DataRow dr in dtSti.Rows)
            {
                if (Util.ToInt(dr["KOMOKU_KUBUN3"]) == 42) wfld1 = "K4.KOMOKU" + Util.ToString(dr["KOMOKU_BANGO"]);
                if (Util.ToInt(dr["KOMOKU_KUBUN3"]) == 43) wfld2 = "K4.KOMOKU" + Util.ToString(dr["KOMOKU_BANGO"]);
            }
            // SQL
            sql = new StringBuilder();
            sql.Append("SELECT K4.KAISHA_CD  AS KAISHA_CD");
            sql.Append(", K4.SHAIN_CD  AS SHAIN_CD");
            sql.Append(", M1.BUMON_CD  AS BUMON_CD");
            sql.Append(", M1.BUKA_CD  AS BUKA_CD");
            sql.Append(", M1.SHAIN_NM      AS SHAIN_NM");
            sql.Append(", M1.SHAIN_KANA_NM  AS SHAIN_KANA_NM");
            sql.Append(", " + wfld1 + " AS FURIKOMIGAKU1");
            sql.Append(", " + wfld2 + " AS FURIKOMIGAKU2");
            sql.Append(", M1.KYUYO_GINKO_CD1 AS GINKO_CD1");
            sql.Append(", M2.GINKO_NM AS GINKO_NM1");
            sql.Append(", M1.KYUYO_SHITEN_CD1 AS SHITEN_CD1");
            sql.Append(", M3.SHITEN_NM AS SHITEN_NM1");
            sql.Append(", M1.KYUYO_YOKIN_SHUMOKU1 AS YOKIN_SHUMOKU1");
            sql.Append(", M1.KYUYO_KOZA_BANGO1 AS KOZA_BANGO1");
            sql.Append(", M1.KYUYO_KOZA_MEIGININ_KANJI1 AS KOZA_MEIGININ_KANJI1");
            sql.Append(", M1.KYUYO_KOZA_MEIGININ_KANA1 AS KOZA_MEIGININ_KANA1");
            sql.Append(", M1.KYUYO_KEIYAKUSHA_BANGO1 AS KEIYAKUSHA_BANGO1");
            sql.Append(", M1.KYUYO_GINKO_CD2 AS GINKO_CD2");
            sql.Append(", M4.GINKO_NM AS GINKO_NM2");
            sql.Append(", M1.KYUYO_SHITEN_CD2 AS SHITEN_CD2");
            sql.Append(", M5.SHITEN_NM AS SHITEN_NM2");
            sql.Append(", M1.KYUYO_YOKIN_SHUMOKU2 AS YOKIN_SHUMOKU2");
            sql.Append(", M1.KYUYO_KOZA_BANGO2 AS KOZA_BANGO2");
            sql.Append(", M1.KYUYO_KOZA_MEIGININ_KANJI2 AS KOZA_MEIGININ_KANJI2");
            sql.Append(", M1.KYUYO_KOZA_MEIGININ_KANA2 AS KOZA_MEIGININ_KANA2");
            sql.Append(", M1.KYUYO_KEIYAKUSHA_BANGO2 AS KEIYAKUSHA_BANGO2");
            sql.Append("  FROM TB_KY_SHOYO_MEISAI_GOKEI AS K4");
            sql.Append("  LEFT  OUTER JOIN TB_KY_SHAIN_JOHO AS M1");
            sql.Append("  ON (K4.KAISHA_CD = M1.KAISHA_CD)");
            sql.Append("  AND (K4.SHAIN_CD = M1.SHAIN_CD)");
            sql.Append("  LEFT  OUTER JOIN TB_KY_GINKO AS M2");
            sql.Append("  ON (M1.KYUYO_GINKO_CD1 = M2.GINKO_CD)");
            sql.Append("  LEFT  OUTER JOIN TB_KY_GINKO_SHITEN AS M3");
            sql.Append("  ON (M1.KYUYO_GINKO_CD1 = M3.GINKO_CD)");
            sql.Append("  AND (M1.KYUYO_SHITEN_CD1 = M3.SHITEN_CD)");
            sql.Append("  LEFT  OUTER JOIN TB_KY_GINKO AS M4");
            sql.Append("  ON (M1.KYUYO_GINKO_CD2 = M4.GINKO_CD)");
            sql.Append("  LEFT  OUTER JOIN TB_KY_GINKO_SHITEN AS M5");
            sql.Append("  ON (M1.KYUYO_GINKO_CD2 = M5.GINKO_CD)");
            sql.Append("  AND (M1.KYUYO_SHITEN_CD2 = M5.SHITEN_CD)");
            sql.Append("  WHERE K4.KAISHA_CD = @KAISHA_CD");
            sql.Append("  AND K4.NENGETSU = @NENGETSU");
            sql.Append("  AND (" + wfld1 + " <> 0 OR " + wfld2 + " <> 0)");
            // ORDER句
            if (rdoOrder1.Checked)
            {
                sql.Append("  ORDER BY KAISHA_CD ASC, SHAIN_CD ASC");
            }
            else
            {
                sql.Append("  ORDER BY KAISHA_CD ASC, BUMON_CD ASC, BUKA_CD ASC, SHAIN_CD ASC");
            }

            #endregion

            DataTable dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);
            if (dtMainLoop.Rows.Count == 0)
            {
                return false;
            }
            else
            {
                ArrayList alParamsPrKyTbl;    　　　　　　　　  // ワークテーブルINSERT用パラメータ
                int sort = 0;               　　　　　　　　    // ソートフィールドカウンタ
                int count = dtMainLoop.Rows.Count;              // 総レコード数

                foreach (DataRow dr in dtMainLoop.Rows)
                {

                    // 振込１データ登録
                    if (Util.ToDecimal(dr["FURIKOMIGAKU1"]) > 0)
                    {
                        sort++;     // カウントアップ
                        alParamsPrKyTbl = SetPrKyTblParams(1, sort, jpFurikomi, dtKaisha.Rows[0], dr, count);
                        this.Dba.Insert("PR_KY_TBL", (DbParamCollection)alParamsPrKyTbl[0]);
                    }
                    // 振込２データ登録
                    if (Util.ToDecimal(dr["FURIKOMIGAKU2"]) > 0)
                    {
                        sort++;     // カウントアップ
                        count++;     // カウントアップ
                        alParamsPrKyTbl = SetPrKyTblParams(2, sort, jpFurikomi, dtKaisha.Rows[0], dr, count);
                        this.Dba.Insert("PR_KY_TBL", (DbParamCollection)alParamsPrKyTbl[0]);
                    }
                }
                // ブランク行を追加
                while (sort % 15 > 0)
                {
                    sort++;     // カウントアップ
                    alParamsPrKyTbl = SetPrKyTblParamsBlank(sort,count);
                    this.Dba.Insert("PR_KY_TBL", (DbParamCollection)alParamsPrKyTbl[0]);
                }
            }

            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
                "SORT",
                "PR_KY_TBL",
                "GUID = @GUID",
                dpc);
            bool dataFlag;
            if (tmpdtPR_HN_TBL.Rows.Count > 0)
            {
                dataFlag = true;
            }
            else
            {
                dataFlag = false;
            }

            return dataFlag;
        }

        /// <summary>
        /// PR_KY_TBLに更新するためのパラメータ設定をします。
        /// </summary>
        /// <param name="type">1:振込１　2:振込２</param>
        /// <param name="sort">ソートフィールド値</param>
        /// <param name="jpNengetsu">支給月</param>
        /// <param name="dr">会社情報データ行</param>
        /// <param name="dr">賞与明細合計データ行</param>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// </returns>
        private ArrayList SetPrKyTblParams(int type, int sort, string[] jpFurikomi, DataRow drK, DataRow dr ,int count)
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            // ヘッダ情報
            updParam.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            updParam.SetParam("@SORT", SqlDbType.Int, sort);
            updParam.SetParam("@ITEM001", SqlDbType.VarChar, 200,
                                            Util.ToString(drK["ITAKU_CD"]));                    // 委託コード
            updParam.SetParam("@ITEM002", SqlDbType.VarChar, 200,
                                            Util.ToString(drK["SHUMOKU_CD"]));                  // 種目コード
            updParam.SetParam("@ITEM003", SqlDbType.VarChar, 200,
                                            Util.ToString(dr["GINKO_NM1"]));                    // 銀行名
            //updParam.SetParam("@ITEM004", SqlDbType.VarChar, 200,
            //                                Util.ToString(dr["SHITEN_NM1"]));                   // 支店名 必要ない？
            updParam.SetParam("@ITEM005", SqlDbType.VarChar, 200, string.Format("{0} {2}年 {3}月 {4}日", jpFurikomi));   // 振込日付
            updParam.SetParam("@ITEM006", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);         // 会社名
            // 制御情報
            updParam.SetParam("@ITEM010", SqlDbType.VarChar, 200, 
                                    Util.ToString(Math.Floor(Util.ToDecimal((sort - 1) / 15)) + 1));  // ページ数
            // 振込情報
            if (type == 1)      
            {
                // 振込1
                updParam.SetParam("@ITEM011", SqlDbType.VarChar, 200,
                                                Util.ToString(dr["KEIYAKUSHA_BANGO1"]));        // 契約者番号
                updParam.SetParam("@ITEM012", SqlDbType.VarChar, 200,
                                                Util.ToString(dr["SHAIN_NM"]));                 // 口座名義人漢字
                updParam.SetParam("@ITEM013", SqlDbType.VarChar, 200,
                                                Util.ToString(dr["SHAIN_KANA_NM"]));            // 口座名義人カナ
                updParam.SetParam("@ITEM014", SqlDbType.VarChar, 200,
                                                Util.ToString(dr["SHAIN_CD"]));                 // 社員コード
                updParam.SetParam("@ITEM015", SqlDbType.VarChar, 200,
                                                Util.ToString(dr["GINKO_CD1"]));                // 銀行コード
                updParam.SetParam("@ITEM016", SqlDbType.VarChar, 200,
                                                Util.ToString(dr["SHITEN_CD1"]));               // 店番号
                updParam.SetParam("@ITEM017", SqlDbType.VarChar, 200,
                                                Util.ToString(dr["YOKIN_SHUMOKU1"]));           // 預金種目
                updParam.SetParam("@ITEM018", SqlDbType.VarChar, 200,
                            Util.ToString(dr["KOZA_BANGO1"]).PadLeft(7, Convert.ToChar("0")));    // 口座番号
                updParam.SetParam("@ITEM019", SqlDbType.VarChar, 200,
                                                Util.FormatNum(dr["FURIKOMIGAKU1"]));           // 振替金額
                updParam.SetParam("@ITEM020", SqlDbType.VarChar, 200,
                                                Util.ToString(dr["GINKO_NM1"]));                // 銀行名
                updParam.SetParam("@ITEM021", SqlDbType.VarChar, 200,
                                                Util.ToString(dr["SHITEN_NM1"]));               // 店舗名
                updParam.SetParam("@ITEM022", SqlDbType.VarChar, 200, "");                      // 摘要(社員名)
            }
            else if (type == 2)
            {
                // 振込2
                updParam.SetParam("@ITEM011", SqlDbType.VarChar, 200,
                                                Util.ToString(dr["KEIYAKUSHA_BANGO2"]));        // 契約者番号
                updParam.SetParam("@ITEM012", SqlDbType.VarChar, 200,
                                                Util.ToString(dr["SHAIN_NM"]));                 // 口座名義人漢字
                updParam.SetParam("@ITEM013", SqlDbType.VarChar, 200,
                                                Util.ToString(dr["SHAIN_KANA_NM"]));            // 口座名義人カナ
                updParam.SetParam("@ITEM014", SqlDbType.VarChar, 200,
                                                Util.ToString(dr["SHAIN_CD"]));                 // 社員コード
                updParam.SetParam("@ITEM015", SqlDbType.VarChar, 200,
                                                Util.ToString(dr["GINKO_CD2"]));                // 銀行コード
                updParam.SetParam("@ITEM016", SqlDbType.VarChar, 200,
                                                Util.ToString(dr["SHITEN_CD2"]));               // 店番号
                updParam.SetParam("@ITEM017", SqlDbType.VarChar, 200,
                                                Util.ToString(dr["YOKIN_SHUMOKU2"]));           // 預金種目
                updParam.SetParam("@ITEM018", SqlDbType.VarChar, 200,
                            Util.ToString(dr["KOZA_BANGO2"]).PadLeft(7, Convert.ToChar("0")));  // 口座番号
                updParam.SetParam("@ITEM019", SqlDbType.VarChar, 200,
                                                Util.FormatNum(dr["FURIKOMIGAKU2"]));           // 振替金額
                updParam.SetParam("@ITEM020", SqlDbType.VarChar, 200,
                                                Util.ToString(dr["GINKO_NM2"]));                // 銀行名
                updParam.SetParam("@ITEM021", SqlDbType.VarChar, 200,
                                                Util.ToString(dr["SHITEN_NM2"]));               // 店舗名
                updParam.SetParam("@ITEM022", SqlDbType.VarChar, 200, "");                      // 摘要(社員名)
            }
            updParam.SetParam("@ITEM023", SqlDbType.VarChar, 200, 1);                           // 計算用レコード数
            updParam.SetParam("@ITEM024", SqlDbType.VarChar, 200, count);                       // 計算用総レコード数
            alParams.Add(updParam);

            return alParams;
        }

        /// <summary>
        /// PR_KY_TBLにブランク行を更新するためのパラメータ設定をします。
        /// </summary>
        /// <param name="sort">ソートフィールド値</param>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// </returns>
        private ArrayList SetPrKyTblParamsBlank(int sort , int count)
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            // ヘッダ情報
            updParam.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            updParam.SetParam("@SORT", SqlDbType.Int, sort);
            // 制御情報
            updParam.SetParam("@ITEM010", SqlDbType.VarChar, 200,
                                    Util.ToString(Math.Floor(Util.ToDecimal((sort - 1) / 15)) + 1));  // ページ数
            // 演算対象フィールドにNULL値をセット
            updParam.SetParam("@ITEM014", SqlDbType.VarChar, 200, DBNull.Value);            // 社員コード
            updParam.SetParam("@ITEM019", SqlDbType.VarChar, 200, DBNull.Value);            // 振替金額
            updParam.SetParam("@ITEM023", SqlDbType.VarChar, 200, 0);                       // 計算用レコード数
            updParam.SetParam("@ITEM024", SqlDbType.VarChar, 200, count);                   // 計算用総レコード数
            alParams.Add(updParam);

            return alParams;
        }

        /// <summary>
        /// 和暦日付を正しい日付に補正します。(FixJpDate へ　和暦年・月・日の各値基本補正)
        /// </summary>
        /// <param name="gengo">元号(漢字名称。「平成」「昭和」など)</param>
        /// <param name="jpYear">年(和暦)</param>
        /// <param name="month">月</param>
        /// <param name="day">日</param>
        /// <param name="dba">データアクセスオブジェクト</param>
        /// <returns>
        /// 和暦にフォーマットされた日付
        /// 0:元号
        /// 1:元号を示す記号
        /// 2:年
        /// 3:月
        /// 4:日
        /// 5:全部一纏めにした日付
        /// </returns>
        private string[] FixJpDate(string gengo, string jpYear, string month, string day, DbAccess dba)
        {
            string fixJpYear = jpYear;
            string fixMonth = month;
            string fixDay = day;

            // 年値基本補正
            if (Util.ToInt(jpYear) < 1) fixJpYear = "0";
            // 月値基本補正
            if (Util.ToInt(month) < 1) fixMonth = "1";
            if (Util.ToInt(month) > 12) fixMonth = "12";
            // 日値基本補正
            if (Util.ToInt(day) < 1) fixDay = "1";
            DateTime getsumatsu = Util.ConvAdDate(gengo, fixJpYear, fixMonth, "1", dba);
            getsumatsu = getsumatsu.AddMonths(1).AddDays(-1);                           // 月末日
            if (Util.ToInt(fixDay) > getsumatsu.Day) fixDay = Util.ToString(getsumatsu.Day);

            // まず西暦に変換
            DateTime datAdDate = Util.ConvAdDate(gengo, fixJpYear, fixMonth, fixDay, dba);

            // それを和暦に変換して返却
            return Util.ConvJpDate(datAdDate, dba);
        }

        /// <summary>
        /// 配列に格納された和暦支給月を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpDateNengetsu(string[] arrJpDate)
        {
            this.lblGengoNengetsu.Text = arrJpDate[0];
            this.txtGengoYearNengetsu.Text = arrJpDate[2];
            this.txtMonthNengetsu.Text = arrJpDate[3];
        }

        /// <summary>
        /// 配列に格納された振替日和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpDateFurikomi(string[] arrJpDate)
        {
            this.lblGengoFurikomi.Text = arrJpDate[0];
            this.txtGengoYearFurikomi.Text = arrJpDate[2];
            this.txtMonthFurikomi.Text = arrJpDate[3];
            this.txtDayFurikomi.Text = arrJpDate[4];
        }
        #endregion
    }
}