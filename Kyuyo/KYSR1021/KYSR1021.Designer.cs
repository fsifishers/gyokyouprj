﻿namespace jp.co.fsi.ky.kysr1021
{
    partial class KYSR1021
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpNengetsu = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblMonthNengetsuTo = new System.Windows.Forms.Label();
            this.lblYearNengetsuTo = new System.Windows.Forms.Label();
            this.txtMonthNengetsuTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblGengoNengetsuTo = new System.Windows.Forms.Label();
            this.txtGengoYearNengetsuTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMonthNengetsuFr = new System.Windows.Forms.Label();
            this.lblYearNengetsuFr = new System.Windows.Forms.Label();
            this.txtMonthNengetsuFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblGengoNengetsuFr = new System.Windows.Forms.Label();
            this.txtGengoYearNengetsuFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.grpHoho = new System.Windows.Forms.GroupBox();
            this.rdoHoho2 = new System.Windows.Forms.RadioButton();
            this.rdoHoho1 = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.grpBukaCd = new System.Windows.Forms.GroupBox();
            this.lblBukaCdTo = new System.Windows.Forms.Label();
            this.txtBukaCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.lblBukaCdFr = new System.Windows.Forms.Label();
            this.txtBukaCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.grpBumonCd = new System.Windows.Forms.GroupBox();
            this.lblBumonCdTo = new System.Windows.Forms.Label();
            this.txtBumonCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lblBumonCdFr = new System.Windows.Forms.Label();
            this.txtBumonCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.grpShainCd = new System.Windows.Forms.GroupBox();
            this.lblShainCdTo = new System.Windows.Forms.Label();
            this.txtShainCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblCodeBet = new System.Windows.Forms.Label();
            this.lblShainCdFr = new System.Windows.Forms.Label();
            this.txtShainCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.grpOrder = new System.Windows.Forms.GroupBox();
            this.rdoOrder2 = new System.Windows.Forms.RadioButton();
            this.rdoOrder1 = new System.Windows.Forms.RadioButton();
            this.grpSex = new System.Windows.Forms.GroupBox();
            this.rdoSex2 = new System.Windows.Forms.RadioButton();
            this.rdoSex1 = new System.Windows.Forms.RadioButton();
            this.pnlDebug.SuspendLayout();
            this.grpNengetsu.SuspendLayout();
            this.grpHoho.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.grpBukaCd.SuspendLayout();
            this.grpBumonCd.SuspendLayout();
            this.grpShainCd.SuspendLayout();
            this.grpOrder.SuspendLayout();
            this.grpSex.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Text = "賞与控除項目一覧表";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Size = new System.Drawing.Size(847, 100);
            // 
            // grpNengetsu
            // 
            this.grpNengetsu.Controls.Add(this.label1);
            this.grpNengetsu.Controls.Add(this.lblMonthNengetsuTo);
            this.grpNengetsu.Controls.Add(this.lblYearNengetsuTo);
            this.grpNengetsu.Controls.Add(this.txtMonthNengetsuTo);
            this.grpNengetsu.Controls.Add(this.lblGengoNengetsuTo);
            this.grpNengetsu.Controls.Add(this.txtGengoYearNengetsuTo);
            this.grpNengetsu.Controls.Add(this.lblMonthNengetsuFr);
            this.grpNengetsu.Controls.Add(this.lblYearNengetsuFr);
            this.grpNengetsu.Controls.Add(this.txtMonthNengetsuFr);
            this.grpNengetsu.Controls.Add(this.lblGengoNengetsuFr);
            this.grpNengetsu.Controls.Add(this.txtGengoYearNengetsuFr);
            this.grpNengetsu.Font = new System.Drawing.Font("ＭＳ ゴシック", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.grpNengetsu.Location = new System.Drawing.Point(12, 57);
            this.grpNengetsu.Name = "grpNengetsu";
            this.grpNengetsu.Size = new System.Drawing.Size(398, 53);
            this.grpNengetsu.TabIndex = 0;
            this.grpNengetsu.TabStop = false;
            this.grpNengetsu.Text = "支給年月";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(188, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(24, 16);
            this.label1.TabIndex = 23;
            this.label1.Text = "～";
            // 
            // lblMonthNengetsuTo
            // 
            this.lblMonthNengetsuTo.AutoSize = true;
            this.lblMonthNengetsuTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMonthNengetsuTo.Location = new System.Drawing.Point(366, 20);
            this.lblMonthNengetsuTo.Name = "lblMonthNengetsuTo";
            this.lblMonthNengetsuTo.Size = new System.Drawing.Size(24, 16);
            this.lblMonthNengetsuTo.TabIndex = 22;
            this.lblMonthNengetsuTo.Text = "月";
            // 
            // lblYearNengetsuTo
            // 
            this.lblYearNengetsuTo.AutoSize = true;
            this.lblYearNengetsuTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblYearNengetsuTo.Location = new System.Drawing.Point(301, 20);
            this.lblYearNengetsuTo.Name = "lblYearNengetsuTo";
            this.lblYearNengetsuTo.Size = new System.Drawing.Size(24, 16);
            this.lblYearNengetsuTo.TabIndex = 20;
            this.lblYearNengetsuTo.Text = "年";
            // 
            // txtMonthNengetsuTo
            // 
            this.txtMonthNengetsuTo.AutoSizeFromLength = false;
            this.txtMonthNengetsuTo.DisplayLength = null;
            this.txtMonthNengetsuTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMonthNengetsuTo.Location = new System.Drawing.Point(326, 18);
            this.txtMonthNengetsuTo.MaxLength = 2;
            this.txtMonthNengetsuTo.Name = "txtMonthNengetsuTo";
            this.txtMonthNengetsuTo.Size = new System.Drawing.Size(34, 23);
            this.txtMonthNengetsuTo.TabIndex = 21;
            this.txtMonthNengetsuTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonthNengetsuTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthNengetsuTo_Validating);
            // 
            // lblGengoNengetsuTo
            // 
            this.lblGengoNengetsuTo.AutoSize = true;
            this.lblGengoNengetsuTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGengoNengetsuTo.Location = new System.Drawing.Point(218, 20);
            this.lblGengoNengetsuTo.Name = "lblGengoNengetsuTo";
            this.lblGengoNengetsuTo.Size = new System.Drawing.Size(40, 16);
            this.lblGengoNengetsuTo.TabIndex = 18;
            this.lblGengoNengetsuTo.Text = "平成";
            // 
            // txtGengoYearNengetsuTo
            // 
            this.txtGengoYearNengetsuTo.AutoSizeFromLength = false;
            this.txtGengoYearNengetsuTo.DisplayLength = null;
            this.txtGengoYearNengetsuTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGengoYearNengetsuTo.Location = new System.Drawing.Point(261, 18);
            this.txtGengoYearNengetsuTo.MaxLength = 2;
            this.txtGengoYearNengetsuTo.Name = "txtGengoYearNengetsuTo";
            this.txtGengoYearNengetsuTo.Size = new System.Drawing.Size(34, 23);
            this.txtGengoYearNengetsuTo.TabIndex = 19;
            this.txtGengoYearNengetsuTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGengoYearNengetsuTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtGengoYearNengetsuTo_Validating);
            // 
            // lblMonthNengetsuFr
            // 
            this.lblMonthNengetsuFr.AutoSize = true;
            this.lblMonthNengetsuFr.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMonthNengetsuFr.Location = new System.Drawing.Point(156, 20);
            this.lblMonthNengetsuFr.Name = "lblMonthNengetsuFr";
            this.lblMonthNengetsuFr.Size = new System.Drawing.Size(24, 16);
            this.lblMonthNengetsuFr.TabIndex = 4;
            this.lblMonthNengetsuFr.Text = "月";
            // 
            // lblYearNengetsuFr
            // 
            this.lblYearNengetsuFr.AutoSize = true;
            this.lblYearNengetsuFr.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblYearNengetsuFr.Location = new System.Drawing.Point(91, 20);
            this.lblYearNengetsuFr.Name = "lblYearNengetsuFr";
            this.lblYearNengetsuFr.Size = new System.Drawing.Size(24, 16);
            this.lblYearNengetsuFr.TabIndex = 2;
            this.lblYearNengetsuFr.Text = "年";
            // 
            // txtMonthNengetsuFr
            // 
            this.txtMonthNengetsuFr.AutoSizeFromLength = false;
            this.txtMonthNengetsuFr.DisplayLength = null;
            this.txtMonthNengetsuFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMonthNengetsuFr.Location = new System.Drawing.Point(116, 18);
            this.txtMonthNengetsuFr.MaxLength = 2;
            this.txtMonthNengetsuFr.Name = "txtMonthNengetsuFr";
            this.txtMonthNengetsuFr.Size = new System.Drawing.Size(34, 23);
            this.txtMonthNengetsuFr.TabIndex = 3;
            this.txtMonthNengetsuFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonthNengetsuFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthNengetsuFr_Validating);
            // 
            // lblGengoNengetsuFr
            // 
            this.lblGengoNengetsuFr.AutoSize = true;
            this.lblGengoNengetsuFr.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGengoNengetsuFr.Location = new System.Drawing.Point(8, 20);
            this.lblGengoNengetsuFr.Name = "lblGengoNengetsuFr";
            this.lblGengoNengetsuFr.Size = new System.Drawing.Size(40, 16);
            this.lblGengoNengetsuFr.TabIndex = 0;
            this.lblGengoNengetsuFr.Text = "平成";
            // 
            // txtGengoYearNengetsuFr
            // 
            this.txtGengoYearNengetsuFr.AutoSizeFromLength = false;
            this.txtGengoYearNengetsuFr.DisplayLength = null;
            this.txtGengoYearNengetsuFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGengoYearNengetsuFr.Location = new System.Drawing.Point(51, 18);
            this.txtGengoYearNengetsuFr.MaxLength = 2;
            this.txtGengoYearNengetsuFr.Name = "txtGengoYearNengetsuFr";
            this.txtGengoYearNengetsuFr.Size = new System.Drawing.Size(34, 23);
            this.txtGengoYearNengetsuFr.TabIndex = 1;
            this.txtGengoYearNengetsuFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGengoYearNengetsuFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtGengoYearNengetsuFr_Validating);
            // 
            // grpHoho
            // 
            this.grpHoho.Controls.Add(this.rdoHoho2);
            this.grpHoho.Controls.Add(this.rdoHoho1);
            this.grpHoho.Font = new System.Drawing.Font("ＭＳ ゴシック", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.grpHoho.Location = new System.Drawing.Point(12, 116);
            this.grpHoho.Name = "grpHoho";
            this.grpHoho.Size = new System.Drawing.Size(190, 53);
            this.grpHoho.TabIndex = 1;
            this.grpHoho.TabStop = false;
            this.grpHoho.Text = "出力方法";
            // 
            // rdoHoho2
            // 
            this.rdoHoho2.AutoSize = true;
            this.rdoHoho2.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoHoho2.Location = new System.Drawing.Point(94, 20);
            this.rdoHoho2.Name = "rdoHoho2";
            this.rdoHoho2.Size = new System.Drawing.Size(74, 20);
            this.rdoHoho2.TabIndex = 3;
            this.rdoHoho2.Text = "部課計";
            this.rdoHoho2.UseVisualStyleBackColor = true;
            this.rdoHoho2.CheckedChanged += new System.EventHandler(this.rdoHoho_CheckedChanged);
            // 
            // rdoHoho1
            // 
            this.rdoHoho1.AutoSize = true;
            this.rdoHoho1.Checked = true;
            this.rdoHoho1.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoHoho1.Location = new System.Drawing.Point(9, 20);
            this.rdoHoho1.Name = "rdoHoho1";
            this.rdoHoho1.Size = new System.Drawing.Size(74, 20);
            this.rdoHoho1.TabIndex = 2;
            this.rdoHoho1.TabStop = true;
            this.rdoHoho1.Text = "社員別";
            this.rdoHoho1.UseVisualStyleBackColor = true;
            this.rdoHoho1.CheckedChanged += new System.EventHandler(this.rdoHoho_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.grpBukaCd);
            this.groupBox1.Controls.Add(this.grpBumonCd);
            this.groupBox1.Controls.Add(this.grpShainCd);
            this.groupBox1.Font = new System.Drawing.Font("ＭＳ ゴシック", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBox1.Location = new System.Drawing.Point(12, 185);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(590, 206);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "出力範囲";
            // 
            // grpBukaCd
            // 
            this.grpBukaCd.Controls.Add(this.lblBukaCdTo);
            this.grpBukaCd.Controls.Add(this.txtBukaCdTo);
            this.grpBukaCd.Controls.Add(this.label5);
            this.grpBukaCd.Controls.Add(this.lblBukaCdFr);
            this.grpBukaCd.Controls.Add(this.txtBukaCdFr);
            this.grpBukaCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.grpBukaCd.Location = new System.Drawing.Point(5, 138);
            this.grpBukaCd.Name = "grpBukaCd";
            this.grpBukaCd.Size = new System.Drawing.Size(575, 53);
            this.grpBukaCd.TabIndex = 5;
            this.grpBukaCd.TabStop = false;
            this.grpBukaCd.Text = "部課コード";
            // 
            // lblBukaCdTo
            // 
            this.lblBukaCdTo.BackColor = System.Drawing.Color.Silver;
            this.lblBukaCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBukaCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBukaCdTo.Location = new System.Drawing.Point(365, 20);
            this.lblBukaCdTo.Name = "lblBukaCdTo";
            this.lblBukaCdTo.Size = new System.Drawing.Size(194, 20);
            this.lblBukaCdTo.TabIndex = 4;
            this.lblBukaCdTo.Text = "最　後";
            this.lblBukaCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtBukaCdTo
            // 
            this.txtBukaCdTo.AutoSizeFromLength = false;
            this.txtBukaCdTo.DisplayLength = null;
            this.txtBukaCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtBukaCdTo.Location = new System.Drawing.Point(303, 19);
            this.txtBukaCdTo.MaxLength = 4;
            this.txtBukaCdTo.Name = "txtBukaCdTo";
            this.txtBukaCdTo.Size = new System.Drawing.Size(55, 23);
            this.txtBukaCdTo.TabIndex = 3;
            this.txtBukaCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBukaCdTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtBukaCdTo_KeyDown);
            this.txtBukaCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtBukaCdTo_Validating);
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label5.Location = new System.Drawing.Point(276, 20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(18, 20);
            this.label5.TabIndex = 2;
            this.label5.Text = "～";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblBukaCdFr
            // 
            this.lblBukaCdFr.BackColor = System.Drawing.Color.Silver;
            this.lblBukaCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBukaCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBukaCdFr.Location = new System.Drawing.Point(76, 20);
            this.lblBukaCdFr.Name = "lblBukaCdFr";
            this.lblBukaCdFr.Size = new System.Drawing.Size(194, 20);
            this.lblBukaCdFr.TabIndex = 1;
            this.lblBukaCdFr.Text = "先　頭";
            this.lblBukaCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtBukaCdFr
            // 
            this.txtBukaCdFr.AutoSizeFromLength = false;
            this.txtBukaCdFr.DisplayLength = null;
            this.txtBukaCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtBukaCdFr.Location = new System.Drawing.Point(15, 19);
            this.txtBukaCdFr.MaxLength = 4;
            this.txtBukaCdFr.Name = "txtBukaCdFr";
            this.txtBukaCdFr.Size = new System.Drawing.Size(55, 23);
            this.txtBukaCdFr.TabIndex = 0;
            this.txtBukaCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBukaCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtBukaCdFr_Validating);
            // 
            // grpBumonCd
            // 
            this.grpBumonCd.Controls.Add(this.lblBumonCdTo);
            this.grpBumonCd.Controls.Add(this.txtBumonCdTo);
            this.grpBumonCd.Controls.Add(this.label2);
            this.grpBumonCd.Controls.Add(this.lblBumonCdFr);
            this.grpBumonCd.Controls.Add(this.txtBumonCdFr);
            this.grpBumonCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.grpBumonCd.Location = new System.Drawing.Point(6, 79);
            this.grpBumonCd.Name = "grpBumonCd";
            this.grpBumonCd.Size = new System.Drawing.Size(575, 53);
            this.grpBumonCd.TabIndex = 4;
            this.grpBumonCd.TabStop = false;
            this.grpBumonCd.Text = "部門コード";
            // 
            // lblBumonCdTo
            // 
            this.lblBumonCdTo.BackColor = System.Drawing.Color.Silver;
            this.lblBumonCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBumonCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBumonCdTo.Location = new System.Drawing.Point(365, 20);
            this.lblBumonCdTo.Name = "lblBumonCdTo";
            this.lblBumonCdTo.Size = new System.Drawing.Size(194, 20);
            this.lblBumonCdTo.TabIndex = 4;
            this.lblBumonCdTo.Text = "最　後";
            this.lblBumonCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtBumonCdTo
            // 
            this.txtBumonCdTo.AutoSizeFromLength = false;
            this.txtBumonCdTo.DisplayLength = null;
            this.txtBumonCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtBumonCdTo.Location = new System.Drawing.Point(303, 19);
            this.txtBumonCdTo.MaxLength = 4;
            this.txtBumonCdTo.Name = "txtBumonCdTo";
            this.txtBumonCdTo.Size = new System.Drawing.Size(55, 23);
            this.txtBumonCdTo.TabIndex = 3;
            this.txtBumonCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBumonCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtBumonCdTo_Validating);
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(276, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(18, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "～";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblBumonCdFr
            // 
            this.lblBumonCdFr.BackColor = System.Drawing.Color.Silver;
            this.lblBumonCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBumonCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBumonCdFr.Location = new System.Drawing.Point(76, 20);
            this.lblBumonCdFr.Name = "lblBumonCdFr";
            this.lblBumonCdFr.Size = new System.Drawing.Size(194, 20);
            this.lblBumonCdFr.TabIndex = 1;
            this.lblBumonCdFr.Text = "先　頭";
            this.lblBumonCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtBumonCdFr
            // 
            this.txtBumonCdFr.AutoSizeFromLength = false;
            this.txtBumonCdFr.DisplayLength = null;
            this.txtBumonCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtBumonCdFr.Location = new System.Drawing.Point(15, 19);
            this.txtBumonCdFr.MaxLength = 4;
            this.txtBumonCdFr.Name = "txtBumonCdFr";
            this.txtBumonCdFr.Size = new System.Drawing.Size(55, 23);
            this.txtBumonCdFr.TabIndex = 0;
            this.txtBumonCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBumonCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtBumonCdFr_Validating);
            // 
            // grpShainCd
            // 
            this.grpShainCd.Controls.Add(this.lblShainCdTo);
            this.grpShainCd.Controls.Add(this.txtShainCdTo);
            this.grpShainCd.Controls.Add(this.lblCodeBet);
            this.grpShainCd.Controls.Add(this.lblShainCdFr);
            this.grpShainCd.Controls.Add(this.txtShainCdFr);
            this.grpShainCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.grpShainCd.Location = new System.Drawing.Point(6, 20);
            this.grpShainCd.Name = "grpShainCd";
            this.grpShainCd.Size = new System.Drawing.Size(575, 53);
            this.grpShainCd.TabIndex = 3;
            this.grpShainCd.TabStop = false;
            this.grpShainCd.Text = "社員コード";
            // 
            // lblShainCdTo
            // 
            this.lblShainCdTo.BackColor = System.Drawing.Color.Silver;
            this.lblShainCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShainCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShainCdTo.Location = new System.Drawing.Point(365, 20);
            this.lblShainCdTo.Name = "lblShainCdTo";
            this.lblShainCdTo.Size = new System.Drawing.Size(194, 20);
            this.lblShainCdTo.TabIndex = 4;
            this.lblShainCdTo.Text = "最　後";
            this.lblShainCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShainCdTo
            // 
            this.txtShainCdTo.AutoSizeFromLength = false;
            this.txtShainCdTo.DisplayLength = null;
            this.txtShainCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShainCdTo.Location = new System.Drawing.Point(303, 19);
            this.txtShainCdTo.MaxLength = 4;
            this.txtShainCdTo.Name = "txtShainCdTo";
            this.txtShainCdTo.Size = new System.Drawing.Size(55, 23);
            this.txtShainCdTo.TabIndex = 3;
            this.txtShainCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShainCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtShainCdTo_Validating);
            // 
            // lblCodeBet
            // 
            this.lblCodeBet.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblCodeBet.Location = new System.Drawing.Point(276, 20);
            this.lblCodeBet.Name = "lblCodeBet";
            this.lblCodeBet.Size = new System.Drawing.Size(18, 20);
            this.lblCodeBet.TabIndex = 2;
            this.lblCodeBet.Text = "～";
            this.lblCodeBet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShainCdFr
            // 
            this.lblShainCdFr.BackColor = System.Drawing.Color.Silver;
            this.lblShainCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShainCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShainCdFr.Location = new System.Drawing.Point(76, 20);
            this.lblShainCdFr.Name = "lblShainCdFr";
            this.lblShainCdFr.Size = new System.Drawing.Size(194, 20);
            this.lblShainCdFr.TabIndex = 1;
            this.lblShainCdFr.Text = "先　頭";
            this.lblShainCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShainCdFr
            // 
            this.txtShainCdFr.AutoSizeFromLength = false;
            this.txtShainCdFr.DisplayLength = null;
            this.txtShainCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShainCdFr.Location = new System.Drawing.Point(15, 19);
            this.txtShainCdFr.MaxLength = 4;
            this.txtShainCdFr.Name = "txtShainCdFr";
            this.txtShainCdFr.Size = new System.Drawing.Size(55, 23);
            this.txtShainCdFr.TabIndex = 0;
            this.txtShainCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShainCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtShainCdFr_Validating);
            // 
            // grpOrder
            // 
            this.grpOrder.Controls.Add(this.rdoOrder2);
            this.grpOrder.Controls.Add(this.rdoOrder1);
            this.grpOrder.Font = new System.Drawing.Font("ＭＳ ゴシック", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.grpOrder.Location = new System.Drawing.Point(212, 116);
            this.grpOrder.Name = "grpOrder";
            this.grpOrder.Size = new System.Drawing.Size(190, 53);
            this.grpOrder.TabIndex = 2;
            this.grpOrder.TabStop = false;
            this.grpOrder.Text = "出力順";
            // 
            // rdoOrder2
            // 
            this.rdoOrder2.AutoSize = true;
            this.rdoOrder2.Checked = true;
            this.rdoOrder2.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoOrder2.Location = new System.Drawing.Point(94, 20);
            this.rdoOrder2.Name = "rdoOrder2";
            this.rdoOrder2.Size = new System.Drawing.Size(74, 20);
            this.rdoOrder2.TabIndex = 1;
            this.rdoOrder2.TabStop = true;
            this.rdoOrder2.Text = "部門順";
            this.rdoOrder2.UseVisualStyleBackColor = true;
            this.rdoOrder2.CheckedChanged += new System.EventHandler(this.rdoHoho_CheckedChanged);
            // 
            // rdoOrder1
            // 
            this.rdoOrder1.AutoSize = true;
            this.rdoOrder1.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoOrder1.Location = new System.Drawing.Point(9, 20);
            this.rdoOrder1.Name = "rdoOrder1";
            this.rdoOrder1.Size = new System.Drawing.Size(74, 20);
            this.rdoOrder1.TabIndex = 0;
            this.rdoOrder1.Text = "社員順";
            this.rdoOrder1.UseVisualStyleBackColor = true;
            this.rdoOrder1.CheckedChanged += new System.EventHandler(this.rdoHoho_CheckedChanged);
            // 
            // grpSex
            // 
            this.grpSex.Controls.Add(this.rdoSex2);
            this.grpSex.Controls.Add(this.rdoSex1);
            this.grpSex.Font = new System.Drawing.Font("ＭＳ ゴシック", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.grpSex.Location = new System.Drawing.Point(412, 116);
            this.grpSex.Name = "grpSex";
            this.grpSex.Size = new System.Drawing.Size(190, 53);
            this.grpSex.TabIndex = 3;
            this.grpSex.TabStop = false;
            this.grpSex.Text = "男女計印刷";
            // 
            // rdoSex2
            // 
            this.rdoSex2.AutoSize = true;
            this.rdoSex2.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoSex2.Location = new System.Drawing.Point(100, 20);
            this.rdoSex2.Name = "rdoSex2";
            this.rdoSex2.Size = new System.Drawing.Size(74, 20);
            this.rdoSex2.TabIndex = 3;
            this.rdoSex2.Text = "しない";
            this.rdoSex2.UseVisualStyleBackColor = true;
            // 
            // rdoSex1
            // 
            this.rdoSex1.AutoSize = true;
            this.rdoSex1.Checked = true;
            this.rdoSex1.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoSex1.Location = new System.Drawing.Point(15, 20);
            this.rdoSex1.Name = "rdoSex1";
            this.rdoSex1.Size = new System.Drawing.Size(58, 20);
            this.rdoSex1.TabIndex = 2;
            this.rdoSex1.TabStop = true;
            this.rdoSex1.Text = "する";
            this.rdoSex1.UseVisualStyleBackColor = true;
            // 
            // KYSR1021
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 638);
            this.Controls.Add(this.grpSex);
            this.Controls.Add(this.grpOrder);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.grpHoho);
            this.Controls.Add(this.grpNengetsu);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "KYSR1021";
            this.Text = "賞与控除項目一覧表";
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.grpNengetsu, 0);
            this.Controls.SetChildIndex(this.grpHoho, 0);
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.Controls.SetChildIndex(this.grpOrder, 0);
            this.Controls.SetChildIndex(this.grpSex, 0);
            this.pnlDebug.ResumeLayout(false);
            this.grpNengetsu.ResumeLayout(false);
            this.grpNengetsu.PerformLayout();
            this.grpHoho.ResumeLayout(false);
            this.grpHoho.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.grpBukaCd.ResumeLayout(false);
            this.grpBukaCd.PerformLayout();
            this.grpBumonCd.ResumeLayout(false);
            this.grpBumonCd.PerformLayout();
            this.grpShainCd.ResumeLayout(false);
            this.grpShainCd.PerformLayout();
            this.grpOrder.ResumeLayout(false);
            this.grpOrder.PerformLayout();
            this.grpSex.ResumeLayout(false);
            this.grpSex.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpNengetsu;
        private System.Windows.Forms.Label lblMonthNengetsuFr;
        private System.Windows.Forms.Label lblYearNengetsuFr;
        private jp.co.fsi.common.controls.FsiTextBox txtMonthNengetsuFr;
        private System.Windows.Forms.Label lblGengoNengetsuFr;
        private jp.co.fsi.common.controls.FsiTextBox txtGengoYearNengetsuFr;
        private System.Windows.Forms.GroupBox grpHoho;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox grpOrder;
        private System.Windows.Forms.RadioButton rdoOrder2;
        private System.Windows.Forms.RadioButton rdoOrder1;
        private System.Windows.Forms.GroupBox grpSex;
        private System.Windows.Forms.RadioButton rdoSex2;
        private System.Windows.Forms.RadioButton rdoSex1;
        private System.Windows.Forms.GroupBox grpBukaCd;
        private System.Windows.Forms.Label lblBukaCdTo;
        private common.controls.FsiTextBox txtBukaCdTo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblBukaCdFr;
        private common.controls.FsiTextBox txtBukaCdFr;
        private System.Windows.Forms.GroupBox grpBumonCd;
        private System.Windows.Forms.Label lblBumonCdTo;
        private common.controls.FsiTextBox txtBumonCdTo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblBumonCdFr;
        private common.controls.FsiTextBox txtBumonCdFr;
        private System.Windows.Forms.GroupBox grpShainCd;
        private System.Windows.Forms.Label lblShainCdTo;
        private common.controls.FsiTextBox txtShainCdTo;
        private System.Windows.Forms.Label lblCodeBet;
        private System.Windows.Forms.Label lblShainCdFr;
        private common.controls.FsiTextBox txtShainCdFr;
        private System.Windows.Forms.Label lblMonthNengetsuTo;
        private System.Windows.Forms.Label lblYearNengetsuTo;
        private common.controls.FsiTextBox txtMonthNengetsuTo;
        private System.Windows.Forms.Label lblGengoNengetsuTo;
        private common.controls.FsiTextBox txtGengoYearNengetsuTo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton rdoHoho2;
        private System.Windows.Forms.RadioButton rdoHoho1;
    }
}