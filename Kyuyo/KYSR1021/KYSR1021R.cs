﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using jp.co.fsi.common.report;
namespace jp.co.fsi.ky.kysr1021
{
    /// <summary>
    /// KYSR1021R の帳票
    /// </summary>
    public partial class KYSR1021R : BaseReport
    {

        public KYSR1021R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }

        private void detail_Format(object sender, EventArgs e)
        {
            // 先頭の文字列と一致するかどうかを判断する
            if (0 <= this.txtVal02.Text.IndexOf("【"))
            {
                detail.BackColor = System.Drawing.Color.PaleTurquoise;
            }
            else
            {
                detail.BackColor = System.Drawing.Color.Transparent;
            }
        }
    }
}
