﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.access;
using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;
using GrapeCity.ActiveReports;

namespace jp.co.fsi.ky.kysr1021
{
    /// <summary>
    /// 支給控除項目一覧表(KYSR1021)
    /// </summary>
    public partial class KYSR1021 : BasePgForm
    {
        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KYSR1021()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 支給年月の初期値取得
            string[] jpDate;
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            string sql = "SELECT MAX(NENGETSU) AS MAX_NENGETSU";
            sql += " FROM TB_KY_SHOYO_MEISAI_KINTAI WHERE KAISHA_CD = @KAISHA_CD";
            DataTable dtShikyuInfo =
                this.Dba.GetDataTableFromSqlWithParams(sql, dpc);
            if (dtShikyuInfo.Rows.Count == 0 || ValChk.IsEmpty(dtShikyuInfo.Rows[0]["MAX_NENGETSU"]))
            {
                // 該当支給データ存在しない場合はシステム日付をセット
                jpDate = Util.ConvJpDate(DateTime.Now, this.Dba);
            }
            else
            {
                jpDate = Util.ConvJpDate(Util.ToDate(dtShikyuInfo.Rows[0]["MAX_NENGETSU"]), this.Dba);
            }

            // 支給月(自)
            lblGengoNengetsuFr.Text     = jpDate[0];
            txtGengoYearNengetsuFr.Text = jpDate[2];
            txtMonthNengetsuFr.Text     = jpDate[3];
            // 支給月(至)
            lblGengoNengetsuTo.Text     = jpDate[0];
            txtGengoYearNengetsuTo.Text = jpDate[2];
            txtMonthNengetsuTo.Text     = jpDate[3];

            // 初期フォーカス
            this.txtGengoYearNengetsuFr.Focus();
            this.txtGengoYearNengetsuFr.SelectAll();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 元号年とコード項目に
            // フォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtGengoYearNengetsuFr":
                case "txtGengoYearNengetsuTo":
                case "txtShainCdFr":
                case "txtShainCdTo":
                case "txtBumonCdFr":
                case "txtBumonCdTo":
                case "txtBukaCdFr":
                case "txtBukaCdTo":
                    this.btnF1.Enabled = true;
                    break;
                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        public override void PressF1()
        {
            Assembly asm;
            Type t;

            //MEMO:現状アクティブなコントロールごとに処理を実装してください。
            switch (this.ActiveCtlNm)
            {
                case "txtShainCdFr":
                case "txtShainCdTo":
                    #region 社員検索
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom("KYCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.ky.kycm1021.KYCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            if (this.ActiveCtlNm == "txtShainCdFr")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.Par1 = "1";                     // ダイアログ起動パラメータ
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.txtShainCdFr.Text = result[0];
                                    this.lblShainCdFr.Text = result[1];
                                }
                            }
                            else if (this.ActiveCtlNm == "txtShainCdTo")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.Par1 = "1";                     // ダイアログ起動パラメータ
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.txtShainCdTo.Text = result[0];
                                    this.lblShainCdTo.Text = result[1];
                                }
                            }
                        }
                    }
                    #endregion
                    break;
                case "txtBumonCdFr":
                case "txtBumonCdTo":
                    #region 部門検索
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom("KYCM1031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.ky.kycm1031.KYCM1031");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            if (this.ActiveCtlNm == "txtBumonCdFr")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.Par1 = "1";
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.txtBumonCdFr.Text = result[0];
                                    this.lblBumonCdFr.Text = result[1];
                                    this.txtBukaCdFr.Text = "";
                                    this.lblBukaCdFr.Text = "先　頭";
                                }
                            }
                            else if (this.ActiveCtlNm == "txtBumonCdTo")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.Par1 = "1";
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.txtBumonCdTo.Text = result[0];
                                    this.lblBumonCdTo.Text = result[1];
                                    this.txtBukaCdTo.Text = "";
                                    this.lblBukaCdTo.Text = "最　後";
                                }
                            }
                        }
                    }
                    #endregion
                    break;
                case "txtBukaCdFr":
                case "txtBukaCdTo":
                    #region 部課検索
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom("KYCM1041.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.ky.kycm1041.KYCM1041");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            if (this.ActiveCtlNm == "txtBukaCdFr"
                                && !ValChk.IsEmpty(this.txtBumonCdFr.Text))
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.Par1 = "1";                         // 検索ダイアログ起動パラメータ
                                frm.InData = this.txtBumonCdFr.Text;    // (string)部門コード
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.txtBukaCdFr.Text = result[0];
                                    this.lblBukaCdFr.Text = result[1];
                                }
                            }
                            else if (this.ActiveCtlNm == "txtBukaCdTo"
                                && !ValChk.IsEmpty(this.txtBumonCdTo.Text))
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.Par1 = "1";                         // 検索ダイアログ起動パラメータ
                                frm.InData = this.txtBumonCdTo.Text;    // (string)部門コード
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.txtBukaCdTo.Text = result[0];
                                    this.lblBukaCdTo.Text = result[1];
                                }
                            }
                        }
                    }
                    #endregion
                    break;
                case "txtGengoYearNengetsuFr":
                case "txtGengoYearNengetsuTo":
                    #region 元号検索
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom("CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.com.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            if (this.ActiveCtlNm == "txtGengoYearNengetsuFr")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.InData = this.lblGengoNengetsuFr.Text;
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.lblGengoNengetsuFr.Text = result[1];

                                    // 存在しない日付の場合、補正して存在する日付に戻す
                                    string[] arrJpDate =
                                        Util.FixJpDate(this.lblGengoNengetsuFr.Text,
                                            this.txtGengoYearNengetsuFr.Text,
                                            this.txtMonthNengetsuFr.Text,
                                            "1",
                                            this.Dba);
                                    this.lblGengoNengetsuFr.Text = arrJpDate[0];
                                    this.txtGengoYearNengetsuFr.Text = arrJpDate[2];
                                    this.txtMonthNengetsuFr.Text = arrJpDate[3];
                                }
                            }
                            else if (this.ActiveCtlNm == "txtGengoYearNengetsuTo")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.InData = this.lblGengoNengetsuTo.Text;
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.lblGengoNengetsuTo.Text = result[1];

                                    // 存在しない日付の場合、補正して存在する日付に戻す
                                    string[] arrJpDate =
                                        Util.FixJpDate(this.lblGengoNengetsuTo.Text,
                                            this.txtGengoYearNengetsuTo.Text,
                                            this.txtMonthNengetsuTo.Text,
                                            "1",
                                            this.Dba);
                                    this.lblGengoNengetsuTo.Text = arrJpDate[0];
                                    this.txtGengoYearNengetsuTo.Text = arrJpDate[2];
                                    this.txtMonthNengetsuTo.Text = arrJpDate[3];
                                }
                            }
                        }
                        
                    }
                    #endregion
                    break;
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            if (Msg.ConfYesNo("実行しますか？") == DialogResult.Yes)
            {
                // プレビュー処理
                DoPrint(true);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            if (Msg.ConfYesNo("実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }

        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            // 設定画面の起動
            // MEMO:原則としてここで渡す帳票IDの設定はReport.csvに保持していることが前提ですが、
            // 保持していない場合は、設定画面での保存(F6)時に新規に設定が保持されます。
            PrintSettingForm psForm = new PrintSettingForm(new string[1] { "KYSR1021R" });
            psForm.ShowDialog();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 社員コード(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShainCdFr_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtShainCdFr.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtShainCdFr.SelectAll();
                e.Cancel = true;
                return;
            }

            // コードを元に名称を取得する
            // 取得された場合、名称をラベルに反映する
            if (this.txtShainCdFr.Text.Length > 0)
            {
                this.lblShainCdFr.Text = this.Dba.GetName(this.UInfo, "TB_KY_SHAIN_JOHO", " ", this.txtShainCdFr.Text);
            }
            else
            {
                this.lblShainCdFr.Text = "先　頭";
            }
        }

        /// <summary>
        /// 社員コード(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShainCdTo_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtShainCdTo.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtShainCdTo.SelectAll();
                e.Cancel = true;
                return;
            }

            // コードを元に名称を取得する
            // 取得された場合、名称をラベルに反映する
            if (this.txtShainCdTo.Text.Length > 0)
            {
                this.lblShainCdTo.Text = this.Dba.GetName(this.UInfo, "TB_KY_SHAIN_JOHO", " ", this.txtShainCdTo.Text);
            }
            else
            {
                this.lblShainCdTo.Text = "最　後";
            }
        }

        /// <summary>
        /// 部門コード(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtBumonCdFr_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtBumonCdFr.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtBumonCdFr.SelectAll();
                e.Cancel = true;
                return;
            }

            // コードを元に名称を取得する
            // 取得された場合、名称をラベルに反映する
            if (this.txtBumonCdFr.Text.Length > 0)
            {
                this.lblBumonCdFr.Text = this.Dba.GetName(this.UInfo, "TB_KY_BUMON", " ", this.txtBumonCdFr.Text);
            }
            else
            {
                this.lblBumonCdFr.Text = "先　頭";
            }
        }

        /// <summary>
        /// 部門コード(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtBumonCdTo_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtBumonCdTo.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtBumonCdTo.SelectAll();
                e.Cancel = true;
                return;
            }

            // コードを元に名称を取得する
            // 取得された場合、名称をラベルに反映する
            if (this.txtBumonCdTo.Text.Length > 0)
            {
                this.lblBumonCdTo.Text = this.Dba.GetName(this.UInfo, "TB_KY_BUMON", " ", this.txtBumonCdTo.Text);
            }
            else
            {
                this.lblBumonCdTo.Text = "最　後";
            }
        }

        /// <summary>
        /// 部課コード(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtBukaCdFr_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtBukaCdFr.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtBukaCdFr.SelectAll();
                e.Cancel = true;
                return;
            }

            // コードを元に名称を取得する
            // 取得された場合、名称をラベルに反映する
            if (this.txtBukaCdFr.Text.Length > 0)
            {
                this.lblBukaCdFr.Text = this.Dba.GetBukaNm(this.UInfo, this.txtBumonCdFr.Text, this.txtBukaCdFr.Text);
            }
            else
            {
                this.lblBukaCdFr.Text = "先　頭";
            }
        }

        /// <summary>
        /// 部課コード(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtBukaCdTo_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtBukaCdTo.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtBukaCdTo.SelectAll();
                e.Cancel = true;
                return;
            }

            // コードを元に名称を取得する
            // 取得された場合、名称をラベルに反映する
            if (this.txtBukaCdTo.Text.Length > 0)
            {
                this.lblBukaCdTo.Text = this.Dba.GetBukaNm(this.UInfo,this.txtBumonCdTo.Text, this.txtBukaCdTo.Text);
            }
            else
            {
                this.lblBukaCdTo.Text = "最　後";
            }
        }

        /// <summary>
        /// 部課CD(至)のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtBukaCdTo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
                {
                    // ﾌﾟﾚﾋﾞｭｰ処理
                    DoPrint(true);
                }
                else
                {
                    this.txtBukaCdTo.Focus();
                }
            }
        }
        /// <summary>
        /// 支給月自(年)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGengoYearNengetsuFr_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtGengoYearNengetsuFr.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtGengoYearNengetsuFr.SelectAll();
                e.Cancel = true;
                return;
            }

            // 空の場合、0年として処理
            if (ValChk.IsEmpty(this.txtGengoYearNengetsuFr.Text))
            {
                this.txtGengoYearNengetsuFr.Text = "0";
            }

            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            this.SetJpDateNengetsu(Util.FixJpDate(this.lblGengoNengetsuFr.Text, this.txtGengoYearNengetsuFr.Text,
                this.txtMonthNengetsuFr.Text, "1", this.Dba));
        }

        /// <summary>
        /// 支給月自(月)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMonthNengetsuFr_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtMonthNengetsuFr.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtMonthNengetsuFr.SelectAll();
                e.Cancel = true;
                return;
            }

            if (ValChk.IsEmpty(this.txtMonthNengetsuFr.Text) || Util.ToInt(this.txtMonthNengetsuFr.Text) == 0)
            {
                // 空の場合、1月として処理
                this.txtMonthNengetsuFr.Text = "1";
            }
            else
            {
                // 12を超える月が入力された場合、12月として処理
                if (Util.ToInt(this.txtMonthNengetsuFr.Text) > 12)
                {
                    this.txtMonthNengetsuFr.Text = "12";
                }
            }

            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            this.SetJpDateNengetsu(Util.FixJpDate(this.lblGengoNengetsuFr.Text, this.txtGengoYearNengetsuFr.Text,
                this.txtMonthNengetsuFr.Text, "1", this.Dba));
        }

        /// <summary>
        /// 支給月至(年)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGengoYearNengetsuTo_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtGengoYearNengetsuTo.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtGengoYearNengetsuTo.SelectAll();
                e.Cancel = true;
                return;
            }

            // 空の場合、0年として処理
            if (ValChk.IsEmpty(this.txtGengoYearNengetsuTo.Text))
            {
                this.txtGengoYearNengetsuTo.Text = "0";
            }

            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            this.SetJpDateTaisho(Util.FixJpDate(this.lblGengoNengetsuTo.Text, this.txtGengoYearNengetsuTo.Text,
                this.txtMonthNengetsuTo.Text, "1", this.Dba));
        }

        /// <summary>
        /// 支給月至(月)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMonthNengetsuTo_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtMonthNengetsuTo.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtMonthNengetsuTo.SelectAll();
                e.Cancel = true;
                return;
            }

            if (ValChk.IsEmpty(this.txtMonthNengetsuTo.Text) || Util.ToInt(this.txtMonthNengetsuTo.Text) == 0)
            {
                // 空の場合、1月として処理
                this.txtMonthNengetsuTo.Text = "1";
            }
            else
            {
                // 12を超える月が入力された場合、12月として処理
                if (Util.ToInt(this.txtMonthNengetsuTo.Text) > 12)
                {
                    this.txtMonthNengetsuTo.Text = "12";
                }
            }

            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            this.SetJpDateTaisho(Util.FixJpDate(this.lblGengoNengetsuTo.Text, this.txtGengoYearNengetsuTo.Text,
                this.txtMonthNengetsuTo.Text, "1", this.Dba));
        }

        /// <summary>
        ///  出力方法ラジオボタン変更時の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rdoHoho_CheckedChanged(object sender, EventArgs e)
        {
            if (rdoHoho1.Checked)   
            {
                // 出力方法：社員別の時は、並び順：社員順は利用可能
                rdoOrder1.Enabled = true;
            }
            else
            {
                // 出力方法：部課別の時は、並び順：社員順は利用不可
                rdoOrder2.Checked = true;
                rdoOrder1.Enabled = false;
            }
        }

        #endregion

        #region privateメソッド
        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview)
        {
            try
            {
                bool dataFlag;

                this.Dba.BeginTransaction();

                // 帳票出力用にワークテーブルにデータを作成
                dataFlag = MakeWkData();

                // 帳票出力
                if (dataFlag)
                {
                    #region ITEM001～074
                    // 取得列の定義
                    StringBuilder cols = new StringBuilder();
                    cols.Append("  ITEM001");
                    cols.Append(" ,ITEM002");
                    cols.Append(" ,ITEM003");
                    cols.Append(" ,ITEM004");
                    cols.Append(" ,ITEM005");
                    cols.Append(" ,ITEM006");
                    cols.Append(" ,ITEM007");
                    cols.Append(" ,ITEM008");
                    cols.Append(" ,ITEM009");
                    cols.Append(" ,ITEM010");
                    cols.Append(" ,ITEM011");
                    cols.Append(" ,ITEM012");
                    cols.Append(" ,ITEM013");
                    cols.Append(" ,ITEM014");
                    cols.Append(" ,ITEM015");
                    cols.Append(" ,ITEM016");
                    cols.Append(" ,ITEM017");
                    cols.Append(" ,ITEM018");
                    cols.Append(" ,ITEM019");
                    cols.Append(" ,ITEM020");
                    cols.Append(" ,ITEM021");
                    cols.Append(" ,ITEM022");
                    cols.Append(" ,ITEM023");
                    cols.Append(" ,ITEM024");
                    cols.Append(" ,ITEM025");
                    cols.Append(" ,ITEM026");
                    cols.Append(" ,ITEM027");
                    cols.Append(" ,ITEM028");
                    cols.Append(" ,ITEM029");
                    cols.Append(" ,ITEM030");
                    cols.Append(" ,ITEM031");
                    cols.Append(" ,ITEM032");
                    cols.Append(" ,ITEM033");
                    cols.Append(" ,ITEM034");
                    cols.Append(" ,ITEM035");
                    cols.Append(" ,ITEM036");
                    cols.Append(" ,ITEM037");
                    cols.Append(" ,ITEM038");
                    cols.Append(" ,ITEM039");
                    cols.Append(" ,ITEM040");
                    cols.Append(" ,ITEM041");
                    cols.Append(" ,ITEM042");
                    cols.Append(" ,ITEM043");
                    cols.Append(" ,ITEM044");
                    cols.Append(" ,ITEM045");
                    cols.Append(" ,ITEM046");
                    cols.Append(" ,ITEM047");
                    cols.Append(" ,ITEM048");
                    cols.Append(" ,ITEM049");
                    cols.Append(" ,ITEM050");
                    cols.Append(" ,ITEM051");
                    cols.Append(" ,ITEM052");
                    cols.Append(" ,ITEM053");
                    cols.Append(" ,ITEM054");
                    cols.Append(" ,ITEM055");
                    cols.Append(" ,ITEM056");
                    cols.Append(" ,ITEM057");
                    cols.Append(" ,ITEM058");
                    cols.Append(" ,ITEM059");
                    cols.Append(" ,ITEM060");
                    cols.Append(" ,ITEM061");
                    cols.Append(" ,ITEM062");
                    cols.Append(" ,ITEM063");
                    cols.Append(" ,ITEM064");
                    cols.Append(" ,ITEM065");
                    cols.Append(" ,ITEM066");
                    cols.Append(" ,ITEM067");
                    cols.Append(" ,ITEM068");
                    cols.Append(" ,ITEM069");
                    cols.Append(" ,ITEM070");
                    cols.Append(" ,ITEM071");
                    cols.Append(" ,ITEM072");
                    cols.Append(" ,ITEM073");
                    cols.Append(" ,ITEM074");
                    #endregion

                    // バインドパラメータの設定
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                    string orderby = "";
                    if (this.rdoOrder2.Checked == false)
                    {
                        orderby = "SORT ASC";
                    }
                    else
                    {
                        orderby = "ITEM005 ASC , SORT ASC";
                    }
                    // データの取得
                    DataTable dtOutput = this.Dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_KY_TBL", "GUID = @GUID", orderby, dpc);
                    // 帳票オブジェクトをインスタンス化
                    this.Dba.Commit();
                    KYSR1021R rpt = new KYSR1021R(dtOutput);
                    if (isPreview)
                    {
                        // プレビュー画面表示
                        PreviewForm pFrm = new PreviewForm(rpt, this.UnqId);
                        pFrm.WindowState = FormWindowState.Maximized;
                        pFrm.Show();
                    }
                    else
                    {
                        // 直接印刷
                        rpt.Run(false);
                        rpt.Document.Print(true, true, false);
                    }
                }
                else
                {
                    Msg.Info("該当データが存在しません。");
                    return;
                }
            }
            finally
            {
                this.Dba.Rollback();
            }

            #region ITEM001～074
            /*bool dataFlag;
            try
            {
                // 帳票出力用にワークテーブルにデータを作成
                dataFlag = MakeWkData();
                this.Dba.Commit();
            }
            finally
            {
                this.Dba.Rollback();
            }

            // 帳票出力
            if (dataFlag)
            {
                string rptNm = "";
                if ((rdoHoho1.Checked) && (rdoOrder2.Checked))
                {
                    rptNm = "R_KYUR2032";           // ***<VS1041>***
                }
                else
                {
                    rptNm = "R_KYUR2031";           // ***<VS1041>***
                }
                Report rpt = new Report();
                rpt.OutputReport(Path.Combine(Util.GetPath(), Constants.REP_DIR, "KYUR2031.mdb"), rptNm, this.UnqId, isPreview);
            }
            else
            {
                Msg.Info("該当データがありません。");
            }

            //////// ワークテーブルに作成したデータを削除
            try
            {
                // 帳票出力用に作成したデータを削除
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                this.Dba.Delete("PR_KY_TBL", "GUID = @GUID", dpc);
                this.Dba.Commit();
            }
            finally
            {
                this.Dba.Rollback();
            }*/
            #endregion
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData()
        {
            // 集計期間を西暦にして保持
            DateTime dateFr = Util.ConvAdDate(this.lblGengoNengetsuFr.Text, this.txtGengoYearNengetsuFr.Text,
                    this.txtMonthNengetsuFr.Text, "1", this.Dba);
            DateTime dateTo = Util.ConvAdDate(this.lblGengoNengetsuTo.Text, this.txtGengoYearNengetsuTo.Text,
                    this.txtMonthNengetsuTo.Text, "1", this.Dba);
            // 集計期間を和暦で保持
            string[] jpDateFr = Util.ConvJpDate(dateFr, this.Dba);
            string[] jpDateTo = Util.ConvJpDate(dateTo, this.Dba);
            // 社員コード設定
            string shainCdFr;
            string shainCdTo;
            if (Util.ToDecimal(txtShainCdFr.Text) > 0) 
            {
                shainCdFr = txtShainCdFr.Text;
            }
            else
            {
                shainCdFr = "0";
            }
            if (Util.ToDecimal(txtShainCdTo.Text) > 0) 
            {
                shainCdTo = txtShainCdTo.Text;
            }
            else
            {
                shainCdTo = "999999";
            }
            // 部門コード設定
            string bumonCdFr;
            string bumonCdTo;
            if (Util.ToDecimal(txtBumonCdFr.Text) > 0)
            {
                bumonCdFr = txtBumonCdFr.Text;
            }
            else
            {
                bumonCdFr = "0";
            }
            if (Util.ToDecimal(txtBumonCdTo.Text) > 0)
            {
                bumonCdTo = txtBumonCdTo.Text;
            }
            else
            {
                bumonCdTo = "9999";
            }
            // 部課コード設定
            string bukaCdFr;
            string bukaCdTo;
            if (Util.ToDecimal(txtBukaCdFr.Text) > 0)
            {
                bukaCdFr = txtBukaCdFr.Text;
            }
            else
            {
                bukaCdFr = "0";
            }
            if (Util.ToDecimal(txtBukaCdTo.Text) > 0)
            {
                bukaCdTo = txtBukaCdTo.Text;
            }
            else
            {
                bukaCdTo = "9999";
            }

            DbParamCollection dpc = new DbParamCollection();

            // VI_帳票項目設定
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 2, this.UInfo.KaishaCd);
            DataTable dtSti = this.Dba.GetDataTableByConditionWithParams(
                "*",
                "VI_KY_CHOHYO_KOMOKU_SETTEI",
                "KAISHA_CD = @KAISHA_CD AND CHOHYO_BANGO = 11 AND KYUYO_SHOYO = 2",     // ***<VS1041>***
                "KAISHA_CD ASC, KYUYO_SHOYO ASC, INJI_ICHI ASC",
                dpc);
            if (dtSti.Rows.Count == 0)
            {
                return false;
            }
            
            // 賞与明細支給控除データ
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 2, this.UInfo.KaishaCd);
            dpc.SetParam("@NENGETSU_FR", SqlDbType.VarChar, 10, dateFr.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@NENGETSU_TO", SqlDbType.VarChar, 10, dateTo.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@SHAIN_CD_FR", SqlDbType.VarChar, 6, shainCdFr);
            dpc.SetParam("@SHAIN_CD_TO", SqlDbType.VarChar, 6, shainCdTo);
            dpc.SetParam("@BUMON_CD_FR", SqlDbType.VarChar, 4, bumonCdFr);
            dpc.SetParam("@BUMON_CD_TO", SqlDbType.VarChar, 4, bumonCdTo);
            dpc.SetParam("@BUKA_CD_FR", SqlDbType.VarChar, 4, bukaCdFr);
            dpc.SetParam("@BUKA_CD_TO", SqlDbType.VarChar, 4, bukaCdTo);

            // 出力ワークの明細データ作成方法を判定
            DataTable dtMainLoop = new DataTable();
            if (rdoHoho1.Checked)
            {
                // 社員別
                dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(GetSyainbetsuSQL(), dpc);
            }
            else
            {
                if (rdoSex1.Checked)
                {
                    // 部課男女別
                    dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(GetBukaSeibetsuSQL(), dpc);
                }
                else
                {
                    // 部課別
                    dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(GetBukabetsuSQL(), dpc);
                }
            }
            if (dtMainLoop.Rows.Count == 0)
            {
                return false;
            }
            else
            {
                ArrayList alParamsPrKyTbl;      // ワークテーブルINSERT用パラメータ
                int sort = 0;                   // ソートフィールドカウンタ

                foreach (DataRow dr in dtMainLoop.Rows)
                {
                    sort++;     // カウントアップ

                    // データ登録
                    alParamsPrKyTbl = SetPrKyTblParams(sort, jpDateFr, jpDateTo, dtSti, dr);
                    this.Dba.Insert("PR_KY_TBL", (DbParamCollection)alParamsPrKyTbl[0]);
                }

                // 小計レコードの追加
                DataTable dt = new DataTable();
                // 出力方法：社員別出力のとき
                if (rdoHoho1.Checked)
                {
                    #region 社員別出力時
                    // 並び順：部課順の時
                    if (rdoOrder2.Checked)
                    {
                        /// 男女別出力が指定された時
                        if (rdoSex1.Checked)
                        {
                            // 出力方法：部課別出力のとき
                            if (rdoHoho2.Checked)
                            {
                                // 部課別男女合計レコード
                                dt = this.Dba.GetDataTableFromSqlWithParams(GetBukaGenderSubtotalSQL(), dpc);
                                foreach (DataRow dr in dt.Rows)
                                {
                                    sort++;
                                    alParamsPrKyTbl = SetPrKyTblParams(sort, jpDateFr, jpDateTo, dtSti, dr);
                                    this.Dba.Insert("PR_KY_TBL", (DbParamCollection)alParamsPrKyTbl[0]);
                                }
                            }
                            // 並び順：部門順の時
                            if (rdoOrder2.Checked)
                            {
                                // 部門別男女合計レコード
                                dt = this.Dba.GetDataTableFromSqlWithParams(GetBumonGenderSubtotalSQL(), dpc);
                                foreach (DataRow dr in dt.Rows)
                                {
                                    sort++;
                                    alParamsPrKyTbl = SetPrKyTblParams(sort, jpDateFr, jpDateTo, dtSti, dr);
                                    this.Dba.Insert("PR_KY_TBL", (DbParamCollection)alParamsPrKyTbl[0]);
                                }
                            }
                        }
                        // 部課別レコード
                        dt = this.Dba.GetDataTableFromSqlWithParams(GetBukaSubtotalSQL(),dpc);
                        foreach(DataRow dr in dt.Rows)
                        {
                            sort++;
                            alParamsPrKyTbl = SetPrKyTblParams(sort, jpDateFr, jpDateTo, dtSti, dr);
                            this.Dba.Insert("PR_KY_TBL", (DbParamCollection)alParamsPrKyTbl[0]);
                        }
                        // 部門別レコード
                        dt = this.Dba.GetDataTableFromSqlWithParams(GetBumonSubtotalSQL(), dpc);
                        foreach (DataRow dr in dt.Rows)
                        {
                            sort++;
                            alParamsPrKyTbl = SetPrKyTblParams(sort, jpDateFr, jpDateTo, dtSti, dr);
                            this.Dba.Insert("PR_KY_TBL", (DbParamCollection)alParamsPrKyTbl[0]);
                        }
                    }
                    #endregion
                }
                // 出力方法：部課別の時
                else
                {
                    #region 部課別出力時
                    /// 男女別出力が指定された時
                    if (rdoSex1.Checked)
                    {
                        // 出力方法：部課別出力のとき
                        if (rdoHoho2.Checked)
                        {
                            // 部課合計レコードの追加
                            dt = this.Dba.GetDataTableFromSqlWithParams(GetBukaSubtotalSQL(), dpc);
                            foreach (DataRow dr in dt.Rows)
                            {
                                sort++;
                                alParamsPrKyTbl = SetPrKyTblParams(sort, jpDateFr, jpDateTo, dtSti, dr);
                                this.Dba.Insert("PR_KY_TBL", (DbParamCollection)alParamsPrKyTbl[0]);
                            }
                        }
                        // 並び順：部門順の時
                        if (rdoOrder2.Checked)
                        {
                            // 部門男女別合計レコードの追加
                            dt = this.Dba.GetDataTableFromSqlWithParams(GetBumonGenderSubtotalSQL(), dpc);
                            foreach (DataRow dr in dt.Rows)
                            {
                                sort++;
                                alParamsPrKyTbl = SetPrKyTblParams(sort, jpDateFr, jpDateTo, dtSti, dr);
                                this.Dba.Insert("PR_KY_TBL", (DbParamCollection)alParamsPrKyTbl[0]);
                            }
                        }
                    }
                    // 部門合計レコードの追加
                    dt = this.Dba.GetDataTableFromSqlWithParams(GetBumonSubtotalSQL(), dpc);
                    foreach (DataRow dr in dt.Rows)
                    {
                        sort++;
                        alParamsPrKyTbl = SetPrKyTblParams(sort, jpDateFr, jpDateTo, dtSti, dr);
                        this.Dba.Insert("PR_KY_TBL", (DbParamCollection)alParamsPrKyTbl[0]);
                    }
                    #endregion
                }
                // 男女合計レコードの追加
                if (rdoSex1.Checked)
                {
                    dt = this.Dba.GetDataTableFromSqlWithParams(GetGenderSubtotalSQL(), dpc);
                    foreach (DataRow dr in dt.Rows)
                    {
                        sort++;
                        alParamsPrKyTbl = SetPrKyTblParams(sort, jpDateFr, jpDateTo, dtSti, dr);
                        this.Dba.Insert("PR_KY_TBL", (DbParamCollection)alParamsPrKyTbl[0]);
                    }
                }
                // 合計レコードの追加
                dt = this.Dba.GetDataTableFromSqlWithParams(GetTotalSQL(), dpc);
                foreach (DataRow dr in dt.Rows)
                {
                    sort++;
                    alParamsPrKyTbl = SetPrKyTblParams(sort, jpDateFr, jpDateTo, dtSti, dr);
                    this.Dba.Insert("PR_KY_TBL", (DbParamCollection)alParamsPrKyTbl[0]);
                }
            }


            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
                "SORT",
                "PR_KY_TBL",
                "GUID = @GUID",
                dpc);
            bool dataFlag;
            if (tmpdtPR_HN_TBL.Rows.Count > 0)
            {
                dataFlag = true;
            }
            else
            {
                dataFlag = false;
            }

            return dataFlag;
        }

        /// <summary>
        /// 社員別レコード取得SELECT文
        /// </summary>
        private string GetSyainbetsuSQL()
        {
            StringBuilder sql = new StringBuilder();
            // SELECT句
            sql.Append("SELECT K1.KAISHA_CD AS KAISHA_CD");
            sql.Append(", K1.SHAIN_CD AS SHAIN_CD");
            sql.Append(", M1.BUMON_CD AS BUMON_CD");
            sql.Append(", M1.BUKA_CD AS BUKA_CD");
            sql.Append(", MAX(M1.SEIBETSU) AS SEIBETSU");
            sql.Append(", MAX(M1.SHAIN_CD) AS DISP_CD");                    // 出力用コード
            sql.Append(", MAX(M1.SHAIN_NM) AS DISP_NM");                    // 出力用名称 
            sql.Append(", ' ' AS SEX");                                     // 出力用性別
            sql.Append(", ' ' AS TOTAL");                                   // 出力用人数
            // SELECT句続き～WHERE句(共通)
            sql.Append(GetSQLWithoutGroupingPart());
            // GROUP句
            sql.Append(" GROUP BY K1.KAISHA_CD, K1.SHAIN_CD, M1.BUMON_CD, M1.BUKA_CD");
            // ORDER句
            sql.Append(" ORDER BY KAISHA_CD ASC,SHAIN_CD ASC");

            return Util.ToString(sql);
        }
        /// <summary>
        /// 部課別レコード取得SELECT文
        /// </summary>
        private string GetBukabetsuSQL()
        {
            StringBuilder sql = new StringBuilder();
            // SELECT句
            sql.Append("SELECT K1.KAISHA_CD  AS KAISHA_CD");
            sql.Append(", 1000000 AS SHAIN_CD");                                    // 仮社員コード
            sql.Append(", M1.BUMON_CD AS BUMON_CD");
            sql.Append(", M1.BUKA_CD AS BUKA_CD");
            sql.Append(", 9 AS SEIBETSU");                                          // 仮性別
            sql.Append(", MAX(M1.BUKA_CD) AS DISP_CD");                             // 出力用コード
            sql.Append(", MAX(M1.BUKA_NM) AS DISP_NM");                             // 出力用名称
            sql.Append(", ' ' AS SEX");                                             // 出力用性別
            sql.Append(", STR(COUNT(DISTINCT M1.SHAIN_CD)) + '人' AS TOTAL");        // 出力用人数
            // SELECT句続き～WHERE句(共通)
            sql.Append(GetSQLWithoutGroupingPart());
            // GROUP句
            sql.Append(" GROUP BY K1.KAISHA_CD, M1.BUMON_CD, M1.BUKA_CD");
            // ORDER句
            sql.Append(" ORDER BY KAISHA_CD ASC,BUMON_CD ASC,BUKA_CD ASC");

            return Util.ToString(sql);
        }
        /// <summary>
        /// 部課男女別レコード取得SELECT文
        /// </summary>
        private string GetBukaSeibetsuSQL()
        {
            StringBuilder sql = new StringBuilder();
            // SELECT句
            sql.Append("SELECT K1.KAISHA_CD  AS KAISHA_CD");
            sql.Append(", 1000000 AS SHAIN_CD");                                    // 仮社員コード
            sql.Append(", M1.BUMON_CD AS BUMON_CD");
            sql.Append(", M1.BUKA_CD AS BUKA_CD");
            sql.Append(", M1.SEIBETSU AS SEIBETSU");
            sql.Append(", MAX(M1.BUKA_CD) AS DISP_CD");                             // 出力用コード
            sql.Append(", MAX(M1.BUKA_NM) AS DISP_NM");                             // 出力用名称
            sql.Append(", CASE WHEN M1.SEIBETSU=1 THEN '男' ELSE '女' END AS SEX");  // 出力用性別
            sql.Append(", STR(COUNT(DISTINCT M1.SHAIN_CD)) + '人' AS TOTAL");        // 出力用人数
            // SELECT句続き～WHERE句(共通)
            sql.Append(GetSQLWithoutGroupingPart());
            // GROUP句
            sql.Append(" GROUP BY K1.KAISHA_CD, M1.BUMON_CD, M1.BUKA_CD, M1.SEIBETSU");
            // ORDER句
            sql.Append(" ORDER BY KAISHA_CD ASC,BUMON_CD ASC,BUKA_CD ASC, SEIBETSU ASC");

            return Util.ToString(sql);
        }
        /// <summary>
        /// 部課男女別小計レコード取得SELECT文
        /// </summary>
        private string GetBukaGenderSubtotalSQL()
        {
            StringBuilder sql = new StringBuilder();
            // SELECT句
            sql.Append("SELECT K1.KAISHA_CD  AS KAISHA_CD");
            sql.Append(", 1000000 AS SHAIN_CD");                                // 仮社員コード
            sql.Append(", M1.BUMON_CD AS BUMON_CD");
            sql.Append(", M1.BUKA_CD AS BUKA_CD");
            sql.Append(", M1.SEIBETSU AS SEIBETSU");
            sql.Append(", ' ' AS DISP_CD");                                     // 出力用コード
            sql.Append(", CASE WHEN M1.SEIBETSU=1 THEN '【男子計】' ELSE '【女子計】' END AS DISP_NM");// 出力用名称
            sql.Append(", ' ' AS SEX");                                         // 出力用性別
            sql.Append(", STR(COUNT(DISTINCT M1.SHAIN_CD)) + '人' AS TOTAL");    // 出力用人数
            // SELECT句続き～WHERE句(共通)
            sql.Append(GetSQLWithoutGroupingPart());
            // GROUP句
            sql.Append(" GROUP BY K1.KAISHA_CD, M1.BUMON_CD, M1.BUKA_CD, M1.SEIBETSU");
            // ORDER句
            sql.Append(" ORDER BY KAISHA_CD ASC,BUMON_CD ASC,BUKA_CD ASC,SEIBETSU ASC");

            return Util.ToString(sql);
        }
        /// <summary>
        /// 部課別小計レコード取得SELECT文
        /// </summary>
        private string GetBukaSubtotalSQL()
        {
            StringBuilder sql = new StringBuilder();
            // SELECT句
            sql.Append("SELECT K1.KAISHA_CD  AS KAISHA_CD");
            sql.Append(", 1000000 AS SHAIN_CD");                                // 仮社員コード
            sql.Append(", M1.BUMON_CD AS BUMON_CD");
            sql.Append(", M1.BUKA_CD AS BUKA_CD");
            sql.Append(", 9 AS SEIBETSU");                                      // 仮性別コード
            sql.Append(", MAX(M1.BUMON_CD) AS DISP_CD");                        // 出力用コード
            sql.Append(", '【' + MAX(M1.BUKA_NM) + '】' AS DISP_NM");           // 出力用名称
            sql.Append(", ' ' AS SEX");                                         // 出力用性別
            sql.Append(", STR(COUNT(DISTINCT M1.SHAIN_CD)) + '人' AS TOTAL");    // 出力用人数
            // SELECT句続き～WHERE句(共通)
            sql.Append(GetSQLWithoutGroupingPart());
            // GROUP句
            sql.Append(" GROUP BY K1.KAISHA_CD, M1.BUMON_CD, M1.BUKA_CD");
            // ORDER句
            sql.Append(" ORDER BY KAISHA_CD ASC,BUMON_CD ASC,BUKA_CD ASC");

            return Util.ToString(sql);
        }
        /// <summary>
        /// 部門男女別小計レコード取得SELECT文
        /// </summary>
        private string GetBumonGenderSubtotalSQL()
        {
            StringBuilder sql = new StringBuilder();
            // SELECT句
            sql.Append("SELECT K1.KAISHA_CD  AS KAISHA_CD");
            sql.Append(", 1000000 AS SHAIN_CD");                            // 仮社員コード
            sql.Append(", M1.BUMON_CD AS BUMON_CD");
            sql.Append(", 10000 AS BUKA_CD");                               // 仮部課コード
            sql.Append(", M1.SEIBETSU AS SEIBETSU");
            sql.Append(", ' ' AS DISP_CD");                                 // 出力用コード
            sql.Append(", CASE WHEN M1.SEIBETSU=1 THEN '【男子計】' ELSE '【女子計】' END AS DISP_NM");// 出力用名称
            sql.Append(", ' ' AS SEX");                     // 出力用性別
            sql.Append(", STR(COUNT(DISTINCT M1.SHAIN_CD)) + '人' AS TOTAL");    // 出力用人数
            // SELECT句続き～WHERE句(共通)
            sql.Append(GetSQLWithoutGroupingPart());
            // GROUP句
            sql.Append(" GROUP BY K1.KAISHA_CD, M1.BUMON_CD, M1.SEIBETSU");
            // ORDER句
            sql.Append(" ORDER BY KAISHA_CD ASC,BUMON_CD ASC, SEIBETSU ASC");

            return Util.ToString(sql);
        }
        /// <summary>
        /// 部門別小計レコード取得SELECT文
        /// </summary>
        private string GetBumonSubtotalSQL()
        {
            StringBuilder sql = new StringBuilder();
            // SELECT句
            sql.Append("SELECT K1.KAISHA_CD  AS KAISHA_CD");
            sql.Append(", 1000000 AS SHAIN_CD");                            // 仮社員コード
            sql.Append(", M1.BUMON_CD AS BUMON_CD");
            sql.Append(", 10000 AS BUKA_CD");                               // 仮部課コード
            sql.Append(", 9 AS SEIBETSU");                                  // 仮性別コード
            sql.Append(", MAX(M1.BUMON_CD) AS DISP_CD");                    // 出力用コード
            sql.Append(", '【' + MAX(M1.BUMON_NM) + '】' AS DISP_NM");      // 出力用名称
            sql.Append(", ' ' AS SEX");                                     // 出力用性別
            sql.Append(", STR(COUNT(DISTINCT M1.SHAIN_CD)) + '人' AS TOTAL");　// 出力用人数
            // SELECT句続き～WHERE句(共通)
            sql.Append(GetSQLWithoutGroupingPart());
            // GROUP句
            sql.Append(" GROUP BY K1.KAISHA_CD, M1.BUMON_CD");
            // ORDER句
            sql.Append(" ORDER BY KAISHA_CD ASC,BUMON_CD ASC");

            return Util.ToString(sql);
        }
        /// <summary>
        /// 男女合計レコード取得SELECT文
        /// </summary>
        private string GetGenderSubtotalSQL()
        {
            StringBuilder sql = new StringBuilder();
            // SELECT句
            sql.Append("SELECT K1.KAISHA_CD  AS KAISHA_CD");
            sql.Append(", 1000000 AS SHAIN_CD");                            // 仮社員コード
            sql.Append(", 10000 AS BUMON_CD");                              // 仮部門コード
            sql.Append(", 10000 AS BUKA_CD");                               // 仮部課コード
            sql.Append(", M1.SEIBETSU AS SEIBETSU");
            sql.Append(", ' ' AS DISP_CD");                                 // 出力用コード
            sql.Append(", CASE WHEN M1.SEIBETSU=1 THEN '【男子計】' ELSE '【女子計】' END AS DISP_NM");// 出力用名称
            sql.Append(", ' ' AS SEX");                                     // 出力用性別
            sql.Append(", STR(COUNT(DISTINCT M1.SHAIN_CD)) + '名' AS TOTAL");   // 出力用人数
            // SELECT句続き～WHERE句(共通)
            sql.Append(GetSQLWithoutGroupingPart());
            // GROUP句
            sql.Append(" GROUP BY K1.KAISHA_CD, M1.BUMON_CD, M1.SEIBETSU");
            // ORDER句
            sql.Append(" ORDER BY KAISHA_CD ASC,BUMON_CD ASC, SEIBETSU ASC");

            return Util.ToString(sql);
        }
        /// <summary>
        /// 合計レコード取得SELECT文
        /// </summary>
        private string GetTotalSQL()
        {
            StringBuilder sql = new StringBuilder();
            // SELECT句
            sql.Append("SELECT K1.KAISHA_CD  AS KAISHA_CD");
            sql.Append(", 1000000 AS SHAIN_CD");                                // 仮社員コード
            sql.Append(", 10000 AS BUMON_CD");                                  // 仮部門コード
            sql.Append(", 10000 AS BUKA_CD");                                   // 仮部課コード
            sql.Append(", 9 AS SEIBETSU");                                      // 仮性別コード
            sql.Append(", ' ' AS DISP_CD");                                     // 出力用コード
            sql.Append(", '【合　計】' AS DISP_NM");                             // 出力用名称
            sql.Append(", ' ' AS SEX");                                         // 出力用性別
            sql.Append(", STR(COUNT(DISTINCT M1.SHAIN_CD)) + '人' AS TOTAL");    // 出力用人数
            // SELECT句続き～WHERE句(共通)
            sql.Append(GetSQLWithoutGroupingPart());
            // GROUP句
            sql.Append(" GROUP BY K1.KAISHA_CD");
            // ORDER句
            sql.Append(" ORDER BY KAISHA_CD ASC");

            return Util.ToString(sql);
        }
        /// <summary>
        /// グループ化部分を含まない共通SQL文
        /// </summary>
        private string GetSQLWithoutGroupingPart()
        {
            StringBuilder sql = new StringBuilder();

            // 賞与明細データ
            // ***<VS1041>***START
            // 勤怠項目(賞与基本情報)
            sql.Append(", SUM(K1.KOMOKU1) AS K1KOMOKU1");
            sql.Append(", SUM(K1.KOMOKU2) AS K1KOMOKU2");
            sql.Append(", SUM(K1.KOMOKU3) AS K1KOMOKU3");
            sql.Append(", SUM(K1.KOMOKU4) AS K1KOMOKU4");
            sql.Append(", SUM(K1.KOMOKU5) AS K1KOMOKU5");
            sql.Append(", SUM(K1.KOMOKU6) AS K1KOMOKU6");
            sql.Append(", SUM(K1.KOMOKU7) AS K1KOMOKU7");
            sql.Append(", SUM(K1.KOMOKU8) AS K1KOMOKU8");
            sql.Append(", SUM(K1.KOMOKU9) AS K1KOMOKU9");
            sql.Append(", SUM(K1.KOMOKU10) AS K1KOMOKU10");
            sql.Append(", SUM(K1.KOMOKU11) AS K1KOMOKU11");
            sql.Append(", SUM(K1.KOMOKU12) AS K1KOMOKU12");
            sql.Append(", SUM(K1.KOMOKU13) AS K1KOMOKU13");
            sql.Append(", SUM(K1.KOMOKU14) AS K1KOMOKU14");
            sql.Append(", SUM(K1.KOMOKU15) AS K1KOMOKU15");
            sql.Append(", SUM(K1.KOMOKU16) AS K1KOMOKU16");
            sql.Append(", SUM(K1.KOMOKU17) AS K1KOMOKU17");
            sql.Append(", SUM(K1.KOMOKU18) AS K1KOMOKU18");
            sql.Append(", SUM(K1.KOMOKU19) AS K1KOMOKU19");
            sql.Append(", SUM(K1.KOMOKU20) AS K1KOMOKU20");
            // 支給項目
            sql.Append(", SUM(K2.KOMOKU1) AS K2KOMOKU1");
            sql.Append(", SUM(K2.KOMOKU2) AS K2KOMOKU2");
            sql.Append(", SUM(K2.KOMOKU3) AS K2KOMOKU3");
            sql.Append(", SUM(K2.KOMOKU4) AS K2KOMOKU4");
            sql.Append(", SUM(K2.KOMOKU5) AS K2KOMOKU5");
            sql.Append(", SUM(K2.KOMOKU6) AS K2KOMOKU6");
            sql.Append(", SUM(K2.KOMOKU7) AS K2KOMOKU7");
            sql.Append(", SUM(K2.KOMOKU8) AS K2KOMOKU8");
            sql.Append(", SUM(K2.KOMOKU9) AS K2KOMOKU9");
            sql.Append(", SUM(K2.KOMOKU10) AS K2KOMOKU10");
            sql.Append(", SUM(K2.KOMOKU11) AS K2KOMOKU11");
            sql.Append(", SUM(K2.KOMOKU12) AS K2KOMOKU12");
            sql.Append(", SUM(K2.KOMOKU13) AS K2KOMOKU13");
            sql.Append(", SUM(K2.KOMOKU14) AS K2KOMOKU14");
            sql.Append(", SUM(K2.KOMOKU15) AS K2KOMOKU15");
            sql.Append(", SUM(K2.KOMOKU16) AS K2KOMOKU16");
            sql.Append(", SUM(K2.KOMOKU17) AS K2KOMOKU17");
            sql.Append(", SUM(K2.KOMOKU18) AS K2KOMOKU18");
            sql.Append(", SUM(K2.KOMOKU19) AS K2KOMOKU19");
            sql.Append(", SUM(K2.KOMOKU20) AS K2KOMOKU20");
            // 控除項目
            sql.Append(", SUM(K3.KOMOKU1) AS K3KOMOKU1");
            sql.Append(", SUM(K3.KOMOKU2) AS K3KOMOKU2");
            sql.Append(", SUM(K3.KOMOKU3) AS K3KOMOKU3");
            sql.Append(", SUM(K3.KOMOKU4) AS K3KOMOKU4");
            sql.Append(", SUM(K3.KOMOKU5) AS K3KOMOKU5");
            sql.Append(", SUM(K3.KOMOKU6) AS K3KOMOKU6");
            sql.Append(", SUM(K3.KOMOKU7) AS K3KOMOKU7");
            sql.Append(", SUM(K3.KOMOKU8) AS K3KOMOKU8");
            sql.Append(", SUM(K3.KOMOKU9) AS K3KOMOKU9");
            sql.Append(", SUM(K3.KOMOKU10) AS K3KOMOKU10");
            sql.Append(", SUM(K3.KOMOKU11) AS K3KOMOKU11");
            sql.Append(", SUM(K3.KOMOKU12) AS K3KOMOKU12");
            sql.Append(", SUM(K3.KOMOKU13) AS K3KOMOKU13");
            sql.Append(", SUM(K3.KOMOKU14) AS K3KOMOKU14");
            sql.Append(", SUM(K3.KOMOKU15) AS K3KOMOKU15");
            sql.Append(", SUM(K3.KOMOKU16) AS K3KOMOKU16");
            sql.Append(", SUM(K3.KOMOKU17) AS K3KOMOKU17");
            sql.Append(", SUM(K3.KOMOKU18) AS K3KOMOKU18");
            sql.Append(", SUM(K3.KOMOKU19) AS K3KOMOKU19");
            sql.Append(", SUM(K3.KOMOKU20) AS K3KOMOKU20");
            // 合計項目
            sql.Append(", SUM(K4.KOMOKU1) AS K4KOMOKU1");
            sql.Append(", SUM(K4.KOMOKU2) AS K4KOMOKU2");
            sql.Append(", SUM(K4.KOMOKU3) AS K4KOMOKU3");
            sql.Append(", SUM(K4.KOMOKU4) AS K4KOMOKU4");
            sql.Append(", SUM(K4.KOMOKU5) AS K4KOMOKU5");
            sql.Append(", SUM(K4.KOMOKU6) AS K4KOMOKU6");
            sql.Append(", SUM(K4.KOMOKU7) AS K4KOMOKU7");
            sql.Append(", SUM(K4.KOMOKU8) AS K4KOMOKU8");
            sql.Append(", SUM(K4.KOMOKU9) AS K4KOMOKU9");
            sql.Append(", SUM(K4.KOMOKU10) AS K4KOMOKU10");
            // ***<VS1041>***END
            sql.Append(" FROM TB_KY_SHOYO_MEISAI_KINTAI AS K1");
            sql.Append(" LEFT OUTER JOIN TB_KY_SHOYO_MEISAI_SHIKYU AS K2");
            sql.Append(" ON (K1.KAISHA_CD = K2.KAISHA_CD)");
            sql.Append(" AND (K1.SHAIN_CD = K2.SHAIN_CD)");
            sql.Append(" AND (K1.NENGETSU = K2.NENGETSU)");
            sql.Append(" LEFT OUTER JOIN TB_KY_SHOYO_MEISAI_KOJO AS K3");
            sql.Append(" ON (K1.KAISHA_CD = K3.KAISHA_CD)");
            sql.Append(" AND (K1.SHAIN_CD = K3.SHAIN_CD)");
            sql.Append(" AND (K1.NENGETSU = K3.NENGETSU)");
            sql.Append(" LEFT OUTER JOIN TB_KY_SHOYO_MEISAI_GOKEI AS K4");
            sql.Append(" ON (K1.KAISHA_CD = K4.KAISHA_CD)");
            sql.Append(" AND (K1.SHAIN_CD = K4.SHAIN_CD)");
            sql.Append(" AND (K1.NENGETSU = K4.NENGETSU)");
            sql.Append(" LEFT OUTER JOIN VI_KY_SHAIN_JOHO AS M1");
            sql.Append(" ON (K1.KAISHA_CD = M1.KAISHA_CD)");
            sql.Append(" AND (K1.SHAIN_CD = M1.SHAIN_CD) ");
            sql.Append(" WHERE K1.KAISHA_CD = @KAISHA_CD");
            sql.Append(" AND K1.NENGETSU BETWEEN @NENGETSU_FR AND @NENGETSU_TO");
            sql.Append(" AND K1.SHAIN_CD BETWEEN @SHAIN_CD_FR AND @SHAIN_CD_TO");
            sql.Append(" AND M1.BUMON_CD BETWEEN @BUMON_CD_FR AND @BUMON_CD_TO");
            sql.Append(" AND M1.BUKA_CD BETWEEN @BUKA_CD_FR AND @BUKA_CD_TO");

            return Util.ToString(sql);
        }

        /// <summary>
        /// PR_KY_TBLに更新するためのパラメータ設定をします。
        /// </summary>
        /// <param name="sort">ソートフィールド値</param>
        /// <param name="jpDateFr">集計期間自</param>
        /// <param name="jpDateTo">集計期間至</param>
        /// <param name="dtSti">帳票設定データ</param>
        /// <param name="dr">賞与明細支給控除合計データ行</param>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// </returns>
        private ArrayList SetPrKyTblParams(int sort, string[] jpDateFr, string[] jpDateTo, DataTable dtSti, DataRow dr)
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            // ヘッダ情報
            updParam.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            updParam.SetParam("@SORT", SqlDbType.Int, sort);
            updParam.SetParam("@ITEM001", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);             // 会社名
            updParam.SetParam("@ITEM002", SqlDbType.VarChar, 200, string.Format("{0}{2}年{3}月", jpDateFr)); // 年月自
            updParam.SetParam("@ITEM003", SqlDbType.VarChar, 200, string.Format("{0}{2}年{3}月", jpDateTo)); // 年月至
            
            // 並び順指定フィールド
            // ※各DB定義値＋１桁(合計行用)の桁そろえを行う
            updParam.SetParam("@ITEM004", SqlDbType.VarChar, 200, 
                            string.Format("{0, 7}", Util.ToString(dr["SHAIN_CD"])));                // 社員コード
            updParam.SetParam("@ITEM005", SqlDbType.VarChar, 200, 
                            string.Format("{0, 5}", Util.ToString(dr["BUMON_CD"])));                // 部門コード
            updParam.SetParam("@ITEM006", SqlDbType.VarChar, 200, 
                            string.Format("{0, 5}", Util.ToString(dr["BUKA_CD"])));                 // 部課コード
            updParam.SetParam("@ITEM007", SqlDbType.VarChar, 200, Util.ToString(dr["SEIBETSU"]));   // 性別
            
            // 出力表項目
            updParam.SetParam("@ITEM041", SqlDbType.VarChar, 200, Util.ToString(dr["DISP_CD"]));    // 表示コード
            updParam.SetParam("@ITEM042", SqlDbType.VarChar, 200, Util.ToString(dr["DISP_NM"]));    // 表示名称
            updParam.SetParam("@ITEM043", SqlDbType.VarChar, 200, Util.ToString(dr["SEX"]));        // 表示性別
            updParam.SetParam("@ITEM044", SqlDbType.VarChar, 200, Util.ToString(dr["TOTAL"]));      // 表示人数
            // 支給控除合計項目
            foreach (DataRow drSti in dtSti.Rows)
            {
                string wfld1 = "";
                string wfld2 = "";
                string sfld = "";
                // 見出し：ワークフィールド名(ITEM011-024)
                if (Util.ToInt(drSti["INJI_ICHI"]) <= 24)
                {
                    wfld1 = "ITEM" + string.Format("{0:D3}",
                                        Util.ToInt(10 + Util.ToInt(drSti["INJI_ICHI"])));
                }
                else
                {
                    wfld1 = "ITEM034";
                }
                // 項目値：ワークフィールド名(ITEM051-074)
                wfld2 = "ITEM" + string.Format("{0:D3}",
                                    Util.ToInt(50 + Util.ToInt(drSti["INJI_ICHI"])));
                // 項目値：参照フィールド名
                sfld = "K" + Util.ToString(drSti["KOMOKU_KUBUN"]) 
                        + "KOMOKU" + Util.ToString(drSti["KOMOKU_BANGO"]);      // ***<VS1041>***

                // 見出し
                updParam.SetParam("@" + wfld1, SqlDbType.VarChar, 200, Util.ToString(drSti["KOMOKU_MEISHO"]));

                // 項目区分＝1の時(勤怠項目)のみ少数表示判定
                if (Util.ToString(drSti["KOMOKU_KUBUN"]).Equals("1"))
                {
                    // 少数利用フラグ(少数0あり・1なし) 
                    bool UseShosu = Util.ToString(drSti["KOMOKU_KUBUN2"]).Equals("0") ? true : false;
                    if (UseShosu)  // 少数あり⇒0.00　少数なし⇒#,##0 の書式
                    {
                        updParam.SetParam("@" + wfld2, SqlDbType.VarChar, 200, Util.FormatNum(dr[sfld], 2));
                    }
                    else
                    {
                        updParam.SetParam("@" + wfld2, SqlDbType.VarChar, 200, Util.FormatNum(dr[sfld]));
                    }
                }
                else
                {
                    updParam.SetParam("@" + wfld2, SqlDbType.VarChar, 200, Util.FormatNum(dr[sfld]));
                }
            }

            alParams.Add(updParam);

            return alParams;
        }

        /// <summary>
        /// 配列に格納された和暦支給月を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpDateNengetsu(string[] arrJpDate)
        {
            this.lblGengoNengetsuFr.Text = arrJpDate[0];
            this.txtGengoYearNengetsuFr.Text = arrJpDate[2];
            this.txtMonthNengetsuFr.Text = arrJpDate[3];
        }

        /// <summary>
        /// 配列に格納された和暦対象月を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpDateTaisho(string[] arrJpDate)
        {
            this.lblGengoNengetsuTo.Text = arrJpDate[0];
            this.txtGengoYearNengetsuTo.Text = arrJpDate[2];
            this.txtMonthNengetsuTo.Text = arrJpDate[3];
        }

        #endregion

    }
}