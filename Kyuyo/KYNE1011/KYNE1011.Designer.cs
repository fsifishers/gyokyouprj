﻿namespace jp.co.fsi.ky.kyne1011
{
    partial class KYNE1011
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtShainCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShainCd = new System.Windows.Forms.Label();
            this.lblShainNm = new System.Windows.Forms.Label();
            this.pnl3 = new jp.co.fsi.common.FsiPanel();
            this.label26 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtShogaishaTokubetsu = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShogaishaTokubetsu = new System.Windows.Forms.Label();
            this.lblShogaisha = new System.Windows.Forms.Label();
            this.txtSai16Miman = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblSai16Miman = new System.Windows.Forms.Label();
            this.lblHaigushaKubunNm = new System.Windows.Forms.Label();
            this.txtHaigushaKubun = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblHaigushaKubun = new System.Windows.Forms.Label();
            this.lblGaikokujinNm = new System.Windows.Forms.Label();
            this.txtGaikokujin = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblGaikokujin = new System.Windows.Forms.Label();
            this.lblSaigaishaNm = new System.Windows.Forms.Label();
            this.txtSaigaisha = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblSaigaisha = new System.Windows.Forms.Label();
            this.lblShiboTaishokuNm = new System.Windows.Forms.Label();
            this.txtShiboTaishoku = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShiboTaishoku = new System.Windows.Forms.Label();
            this.lblMiseinenshaNm = new System.Windows.Forms.Label();
            this.txtMiseinensha = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMiseinensha = new System.Windows.Forms.Label();
            this.lblHonninKubun3Nm = new System.Windows.Forms.Label();
            this.txtHonninKubun3 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblHonninKubun3 = new System.Windows.Forms.Label();
            this.lblHonninKubun2Nm = new System.Windows.Forms.Label();
            this.txtHonninKubun2 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblHonninKubun2 = new System.Windows.Forms.Label();
            this.txtShogaishaIppan = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShogaishaIppan = new System.Windows.Forms.Label();
            this.txtFuyoDokyoTokushoIppan = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblFuyoDokyoTokushoIppan = new System.Windows.Forms.Label();
            this.lblFuyoRojin = new System.Windows.Forms.Label();
            this.lblZeigakuHyoNm = new System.Windows.Forms.Label();
            this.txtFuyoRojinDokyoRoshinto = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblFuyoRojinDokyoRoshinto = new System.Windows.Forms.Label();
            this.txtFuyoRojinDokyoRoshintoIgai = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblFuyoRojinDokyoRoshintoIgai = new System.Windows.Forms.Label();
            this.txtFuyoTokutei = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblFuyoTokutei = new System.Windows.Forms.Label();
            this.txtFuyoIppan = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblFuyoIppan = new System.Windows.Forms.Label();
            this.lblHonninKubun1Nm = new System.Windows.Forms.Label();
            this.txtHonninKubun1 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblHonninKubun1 = new System.Windows.Forms.Label();
            this.txtZeigakuHyo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblZeigakuHyo = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Sai16MimanNm = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblGengoNendo = new System.Windows.Forms.Label();
            this.panel1 = new jp.co.fsi.common.FsiPanel();
            this.txtGengoYearNendo = new jp.co.fsi.common.controls.FsiTextBox();
            this.pnl1 = new jp.co.fsi.common.FsiPanel();
            this.txtJutakuKariireToTkbtKJSu = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblJutakuKariireToTkbtKJSu = new System.Windows.Forms.Label();
            this.txtJutakuKariireToTkbtKJKubun1 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtJutakuKariireToTkbtKJKubun2 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtJutakuKariireToNenmatsuZan2 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKojoKubun2 = new System.Windows.Forms.Label();
            this.lblNenZan2 = new System.Windows.Forms.Label();
            this.txtJutakuKariireToNenmatsuZan1 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblNenZan1 = new System.Windows.Forms.Label();
            this.pnlTaishokuNengappi2 = new jp.co.fsi.common.FsiPanel();
            this.lblDayTaishokuNengappi2 = new System.Windows.Forms.Label();
            this.txtDayIjuKaishiDate2 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMonthTaishokuNengappi2 = new System.Windows.Forms.Label();
            this.lblGengoYearTaishokuNengappi2 = new System.Windows.Forms.Label();
            this.txtMonthIjuKaishiDate2 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblGengoIjuKaishiDate2 = new System.Windows.Forms.Label();
            this.txtGengoYearIjuKaishiDate2 = new jp.co.fsi.common.controls.FsiTextBox();
            this.panel3 = new jp.co.fsi.common.FsiPanel();
            this.label32 = new System.Windows.Forms.Label();
            this.txtDayZenTaishokuDate = new jp.co.fsi.common.controls.FsiTextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.txtMonthZenTaishokuDate = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblGengoZenTaishokuDate = new System.Windows.Forms.Label();
            this.txtGengoYearZenTaishokuDate = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtJutakuShutokutoTkbtKojogaku = new jp.co.fsi.common.controls.FsiTextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.txtZenshokubunShotokuzeigaku = new jp.co.fsi.common.controls.FsiTextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtZenshokubunShakaiHokenryo = new jp.co.fsi.common.controls.FsiTextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txtZenshokubunKazeiKyuyogaku = new jp.co.fsi.common.controls.FsiTextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.txtKokuminNenkinHokenryoShrigk = new jp.co.fsi.common.controls.FsiTextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.txtChokiSongaiHokenryoShrigk = new jp.co.fsi.common.controls.FsiTextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.txtKojinNenkinHokenryoShrigk = new jp.co.fsi.common.controls.FsiTextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.txtShinKojinNenkinHokenryo = new jp.co.fsi.common.controls.FsiTextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtKaigoIryoHokenryo = new jp.co.fsi.common.controls.FsiTextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtKyuSeimeiHokenryo = new jp.co.fsi.common.controls.FsiTextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtShinSeimeiHokenryo = new jp.co.fsi.common.controls.FsiTextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtHaigushaTokubetsuKojogaku = new jp.co.fsi.common.controls.FsiTextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtSongaihokenryoKojogaku = new jp.co.fsi.common.controls.FsiTextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtSeimeihokenryoKojogaku = new jp.co.fsi.common.controls.FsiTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtSkbKigyoKyosaiKakekinKjgk = new jp.co.fsi.common.controls.FsiTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtHaigushaGokeiShotoku = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtShakaiHokenryoKjgkSnkkbn = new jp.co.fsi.common.controls.FsiTextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.pnlTaishokuNengappi1 = new jp.co.fsi.common.FsiPanel();
            this.lblDayTaishokuNengappi1 = new System.Windows.Forms.Label();
            this.txtDayIjuKaishiDate1 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMonthTaishokuNengappi1 = new System.Windows.Forms.Label();
            this.lblGengoYearTaishokuNengappi1 = new System.Windows.Forms.Label();
            this.txtMonthIjuKaishiDate1 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblGengoIjuKaishiDate1 = new System.Windows.Forms.Label();
            this.txtGengoYearIjuKaishiDate1 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblTaishokuNengappi1 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.lblZenTaishokuNengappi = new System.Windows.Forms.Label();
            this.lblTaishokuNengappi2 = new System.Windows.Forms.Label();
            this.lblNyuryokuF = new System.Windows.Forms.Label();
            this.lblNenmatsuChosei = new System.Windows.Forms.Label();
            this.lblKeisanF = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.pnl2 = new jp.co.fsi.common.FsiPanel();
            this.txtChoseiShoyoShotokuzeigaku = new jp.co.fsi.common.controls.FsiTextBox();
            this.label45 = new System.Windows.Forms.Label();
            this.txtChoseiShoyoShakaiHokenryo = new jp.co.fsi.common.controls.FsiTextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.txtChoseiKyuyoShotokuzeigaku = new jp.co.fsi.common.controls.FsiTextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.txtChoseiKyuyoShakaiHokenryo = new jp.co.fsi.common.controls.FsiTextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.txtChoseiShoyoKazeiKyuyogaku = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtChoseiKyuyoKazeiKyuyogaku = new jp.co.fsi.common.controls.FsiTextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.panel2 = new jp.co.fsi.common.FsiPanel();
            this.txtTekiyo2 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtTekiyo1 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblTekiyo2 = new System.Windows.Forms.Label();
            this.lblTekiyo1 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.lblKojoKubun1 = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.pnl3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.pnl1.SuspendLayout();
            this.pnlTaishokuNengappi2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.pnlTaishokuNengappi1.SuspendLayout();
            this.pnl2.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Text = "年末調整入力";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(5, 549);
            this.pnlDebug.Size = new System.Drawing.Size(847, 91);
            // 
            // txtShainCd
            // 
            this.txtShainCd.AutoSizeFromLength = true;
            this.txtShainCd.DisplayLength = null;
            this.txtShainCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShainCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtShainCd.Location = new System.Drawing.Point(262, 41);
            this.txtShainCd.MaxLength = 6;
            this.txtShainCd.Name = "txtShainCd";
            this.txtShainCd.Size = new System.Drawing.Size(55, 23);
            this.txtShainCd.TabIndex = 1;
            this.txtShainCd.Text = "0";
            this.txtShainCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShainCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtShainCd_Validating);
            // 
            // lblShainCd
            // 
            this.lblShainCd.BackColor = System.Drawing.Color.Silver;
            this.lblShainCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShainCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShainCd.Location = new System.Drawing.Point(157, 40);
            this.lblShainCd.Name = "lblShainCd";
            this.lblShainCd.Size = new System.Drawing.Size(105, 25);
            this.lblShainCd.TabIndex = 1;
            this.lblShainCd.Text = "社員コード";
            this.lblShainCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShainNm
            // 
            this.lblShainNm.BackColor = System.Drawing.Color.Silver;
            this.lblShainNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShainNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShainNm.Location = new System.Drawing.Point(318, 40);
            this.lblShainNm.Name = "lblShainNm";
            this.lblShainNm.Size = new System.Drawing.Size(237, 25);
            this.lblShainNm.TabIndex = 903;
            this.lblShainNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnl3
            // 
            this.pnl3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.pnl3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnl3.Controls.Add(this.label26);
            this.pnl3.Controls.Add(this.label18);
            this.pnl3.Controls.Add(this.label17);
            this.pnl3.Controls.Add(this.label16);
            this.pnl3.Controls.Add(this.label11);
            this.pnl3.Controls.Add(this.label5);
            this.pnl3.Controls.Add(this.label4);
            this.pnl3.Controls.Add(this.label9);
            this.pnl3.Controls.Add(this.txtShogaishaTokubetsu);
            this.pnl3.Controls.Add(this.lblShogaishaTokubetsu);
            this.pnl3.Controls.Add(this.lblShogaisha);
            this.pnl3.Controls.Add(this.txtSai16Miman);
            this.pnl3.Controls.Add(this.lblSai16Miman);
            this.pnl3.Controls.Add(this.lblHaigushaKubunNm);
            this.pnl3.Controls.Add(this.txtHaigushaKubun);
            this.pnl3.Controls.Add(this.lblHaigushaKubun);
            this.pnl3.Controls.Add(this.lblGaikokujinNm);
            this.pnl3.Controls.Add(this.txtGaikokujin);
            this.pnl3.Controls.Add(this.lblGaikokujin);
            this.pnl3.Controls.Add(this.lblSaigaishaNm);
            this.pnl3.Controls.Add(this.txtSaigaisha);
            this.pnl3.Controls.Add(this.lblSaigaisha);
            this.pnl3.Controls.Add(this.lblShiboTaishokuNm);
            this.pnl3.Controls.Add(this.txtShiboTaishoku);
            this.pnl3.Controls.Add(this.lblShiboTaishoku);
            this.pnl3.Controls.Add(this.lblMiseinenshaNm);
            this.pnl3.Controls.Add(this.txtMiseinensha);
            this.pnl3.Controls.Add(this.lblMiseinensha);
            this.pnl3.Controls.Add(this.lblHonninKubun3Nm);
            this.pnl3.Controls.Add(this.txtHonninKubun3);
            this.pnl3.Controls.Add(this.lblHonninKubun3);
            this.pnl3.Controls.Add(this.lblHonninKubun2Nm);
            this.pnl3.Controls.Add(this.txtHonninKubun2);
            this.pnl3.Controls.Add(this.lblHonninKubun2);
            this.pnl3.Controls.Add(this.txtShogaishaIppan);
            this.pnl3.Controls.Add(this.lblShogaishaIppan);
            this.pnl3.Controls.Add(this.txtFuyoDokyoTokushoIppan);
            this.pnl3.Controls.Add(this.lblFuyoDokyoTokushoIppan);
            this.pnl3.Controls.Add(this.lblFuyoRojin);
            this.pnl3.Controls.Add(this.lblZeigakuHyoNm);
            this.pnl3.Controls.Add(this.txtFuyoRojinDokyoRoshinto);
            this.pnl3.Controls.Add(this.lblFuyoRojinDokyoRoshinto);
            this.pnl3.Controls.Add(this.txtFuyoRojinDokyoRoshintoIgai);
            this.pnl3.Controls.Add(this.lblFuyoRojinDokyoRoshintoIgai);
            this.pnl3.Controls.Add(this.txtFuyoTokutei);
            this.pnl3.Controls.Add(this.lblFuyoTokutei);
            this.pnl3.Controls.Add(this.txtFuyoIppan);
            this.pnl3.Controls.Add(this.lblFuyoIppan);
            this.pnl3.Controls.Add(this.lblHonninKubun1Nm);
            this.pnl3.Controls.Add(this.txtHonninKubun1);
            this.pnl3.Controls.Add(this.lblHonninKubun1);
            this.pnl3.Controls.Add(this.txtZeigakuHyo);
            this.pnl3.Controls.Add(this.lblZeigakuHyo);
            this.pnl3.Controls.Add(this.label3);
            this.pnl3.Controls.Add(this.Sai16MimanNm);
            this.pnl3.Location = new System.Drawing.Point(561, 68);
            this.pnl3.Name = "pnl3";
            this.pnl3.Size = new System.Drawing.Size(266, 463);
            this.pnl3.TabIndex = 4;
            // 
            // label26
            // 
            this.label26.BackColor = System.Drawing.Color.Silver;
            this.label26.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label26.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label26.Location = new System.Drawing.Point(225, 425);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(30, 23);
            this.label26.TabIndex = 109;
            this.label26.Text = "人";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label18
            // 
            this.label18.BackColor = System.Drawing.Color.Silver;
            this.label18.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label18.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label18.Location = new System.Drawing.Point(225, 402);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(30, 23);
            this.label18.TabIndex = 108;
            this.label18.Text = "人";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label17
            // 
            this.label17.BackColor = System.Drawing.Color.Silver;
            this.label17.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label17.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label17.Location = new System.Drawing.Point(225, 379);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(30, 23);
            this.label17.TabIndex = 107;
            this.label17.Text = "人";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label16
            // 
            this.label16.BackColor = System.Drawing.Color.Silver;
            this.label16.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label16.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label16.Location = new System.Drawing.Point(225, 354);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(30, 23);
            this.label16.TabIndex = 106;
            this.label16.Text = "人";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label11
            // 
            this.label11.BackColor = System.Drawing.Color.Silver;
            this.label11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label11.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label11.Location = new System.Drawing.Point(225, 331);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(30, 23);
            this.label11.TabIndex = 105;
            this.label11.Text = "人";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Silver;
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label5.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label5.Location = new System.Drawing.Point(225, 308);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(30, 23);
            this.label5.TabIndex = 104;
            this.label5.Text = "人";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Silver;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label4.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label4.Location = new System.Drawing.Point(225, 285);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(30, 23);
            this.label4.TabIndex = 103;
            this.label4.Text = "人";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.Gray;
            this.label9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label9.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(7, 261);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(24, 116);
            this.label9.TabIndex = 102;
            this.label9.Text = "扶養親族";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtShogaishaTokubetsu
            // 
            this.txtShogaishaTokubetsu.AutoSizeFromLength = true;
            this.txtShogaishaTokubetsu.DisplayLength = null;
            this.txtShogaishaTokubetsu.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShogaishaTokubetsu.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtShogaishaTokubetsu.Location = new System.Drawing.Point(193, 425);
            this.txtShogaishaTokubetsu.MaxLength = 2;
            this.txtShogaishaTokubetsu.Name = "txtShogaishaTokubetsu";
            this.txtShogaishaTokubetsu.Size = new System.Drawing.Size(32, 23);
            this.txtShogaishaTokubetsu.TabIndex = 16;
            this.txtShogaishaTokubetsu.Text = "0";
            this.txtShogaishaTokubetsu.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShogaishaTokubetsu.Validating += new System.ComponentModel.CancelEventHandler(this.txtShogaishaTokubetsu_Validating);
            // 
            // lblShogaishaTokubetsu
            // 
            this.lblShogaishaTokubetsu.BackColor = System.Drawing.Color.Silver;
            this.lblShogaishaTokubetsu.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShogaishaTokubetsu.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShogaishaTokubetsu.Location = new System.Drawing.Point(32, 425);
            this.lblShogaishaTokubetsu.Name = "lblShogaishaTokubetsu";
            this.lblShogaishaTokubetsu.Size = new System.Drawing.Size(161, 23);
            this.lblShogaishaTokubetsu.TabIndex = 96;
            this.lblShogaishaTokubetsu.Text = "特　　　　別";
            this.lblShogaishaTokubetsu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShogaisha
            // 
            this.lblShogaisha.BackColor = System.Drawing.Color.Gray;
            this.lblShogaisha.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShogaisha.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShogaisha.ForeColor = System.Drawing.Color.White;
            this.lblShogaisha.Location = new System.Drawing.Point(7, 379);
            this.lblShogaisha.Name = "lblShogaisha";
            this.lblShogaisha.Size = new System.Drawing.Size(24, 69);
            this.lblShogaisha.TabIndex = 94;
            this.lblShogaisha.Text = "障害者";
            this.lblShogaisha.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtSai16Miman
            // 
            this.txtSai16Miman.AutoSizeFromLength = true;
            this.txtSai16Miman.DisplayLength = null;
            this.txtSai16Miman.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtSai16Miman.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtSai16Miman.Location = new System.Drawing.Point(193, 354);
            this.txtSai16Miman.MaxLength = 2;
            this.txtSai16Miman.Name = "txtSai16Miman";
            this.txtSai16Miman.Size = new System.Drawing.Size(32, 23);
            this.txtSai16Miman.TabIndex = 13;
            this.txtSai16Miman.Text = "0";
            this.txtSai16Miman.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSai16Miman.Validating += new System.ComponentModel.CancelEventHandler(this.txtSai16Miman_Validating);
            // 
            // lblSai16Miman
            // 
            this.lblSai16Miman.BackColor = System.Drawing.Color.Silver;
            this.lblSai16Miman.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSai16Miman.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblSai16Miman.Location = new System.Drawing.Point(32, 354);
            this.lblSai16Miman.Name = "lblSai16Miman";
            this.lblSai16Miman.Size = new System.Drawing.Size(161, 23);
            this.lblSai16Miman.TabIndex = 84;
            this.lblSai16Miman.Text = "１６歳未満";
            this.lblSai16Miman.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblHaigushaKubunNm
            // 
            this.lblHaigushaKubunNm.BackColor = System.Drawing.Color.Silver;
            this.lblHaigushaKubunNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblHaigushaKubunNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblHaigushaKubunNm.Location = new System.Drawing.Point(132, 220);
            this.lblHaigushaKubunNm.Name = "lblHaigushaKubunNm";
            this.lblHaigushaKubunNm.Size = new System.Drawing.Size(123, 23);
            this.lblHaigushaKubunNm.TabIndex = 80;
            this.lblHaigushaKubunNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtHaigushaKubun
            // 
            this.txtHaigushaKubun.AutoSizeFromLength = true;
            this.txtHaigushaKubun.DisplayLength = null;
            this.txtHaigushaKubun.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtHaigushaKubun.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtHaigushaKubun.Location = new System.Drawing.Point(114, 220);
            this.txtHaigushaKubun.MaxLength = 1;
            this.txtHaigushaKubun.Name = "txtHaigushaKubun";
            this.txtHaigushaKubun.Size = new System.Drawing.Size(18, 23);
            this.txtHaigushaKubun.TabIndex = 8;
            this.txtHaigushaKubun.Text = "0";
            this.txtHaigushaKubun.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtHaigushaKubun.Validating += new System.ComponentModel.CancelEventHandler(this.txtHaigushaKubun_Validating);
            // 
            // lblHaigushaKubun
            // 
            this.lblHaigushaKubun.BackColor = System.Drawing.Color.Silver;
            this.lblHaigushaKubun.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblHaigushaKubun.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblHaigushaKubun.Location = new System.Drawing.Point(32, 220);
            this.lblHaigushaKubun.Name = "lblHaigushaKubun";
            this.lblHaigushaKubun.Size = new System.Drawing.Size(82, 23);
            this.lblHaigushaKubun.TabIndex = 78;
            this.lblHaigushaKubun.Text = "配偶者区分";
            this.lblHaigushaKubun.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblGaikokujinNm
            // 
            this.lblGaikokujinNm.BackColor = System.Drawing.Color.Silver;
            this.lblGaikokujinNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGaikokujinNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGaikokujinNm.Location = new System.Drawing.Point(132, 169);
            this.lblGaikokujinNm.Name = "lblGaikokujinNm";
            this.lblGaikokujinNm.Size = new System.Drawing.Size(123, 23);
            this.lblGaikokujinNm.TabIndex = 74;
            this.lblGaikokujinNm.Text = "0:対象外　1:対象";
            this.lblGaikokujinNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtGaikokujin
            // 
            this.txtGaikokujin.AutoSizeFromLength = true;
            this.txtGaikokujin.DisplayLength = null;
            this.txtGaikokujin.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGaikokujin.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtGaikokujin.Location = new System.Drawing.Point(114, 169);
            this.txtGaikokujin.MaxLength = 1;
            this.txtGaikokujin.Name = "txtGaikokujin";
            this.txtGaikokujin.Size = new System.Drawing.Size(18, 23);
            this.txtGaikokujin.TabIndex = 7;
            this.txtGaikokujin.Text = "0";
            this.txtGaikokujin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtGaikokujin.Validating += new System.ComponentModel.CancelEventHandler(this.txtGaikokujin_Validating);
            // 
            // lblGaikokujin
            // 
            this.lblGaikokujin.BackColor = System.Drawing.Color.Silver;
            this.lblGaikokujin.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGaikokujin.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGaikokujin.Location = new System.Drawing.Point(32, 169);
            this.lblGaikokujin.Name = "lblGaikokujin";
            this.lblGaikokujin.Size = new System.Drawing.Size(82, 23);
            this.lblGaikokujin.TabIndex = 72;
            this.lblGaikokujin.Text = "外 国 人";
            this.lblGaikokujin.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSaigaishaNm
            // 
            this.lblSaigaishaNm.BackColor = System.Drawing.Color.Silver;
            this.lblSaigaishaNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSaigaishaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblSaigaishaNm.Location = new System.Drawing.Point(132, 146);
            this.lblSaigaishaNm.Name = "lblSaigaishaNm";
            this.lblSaigaishaNm.Size = new System.Drawing.Size(123, 23);
            this.lblSaigaishaNm.TabIndex = 71;
            this.lblSaigaishaNm.Text = "0:対象外　1:対象";
            this.lblSaigaishaNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtSaigaisha
            // 
            this.txtSaigaisha.AutoSizeFromLength = true;
            this.txtSaigaisha.DisplayLength = null;
            this.txtSaigaisha.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtSaigaisha.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtSaigaisha.Location = new System.Drawing.Point(114, 146);
            this.txtSaigaisha.MaxLength = 1;
            this.txtSaigaisha.Name = "txtSaigaisha";
            this.txtSaigaisha.Size = new System.Drawing.Size(18, 23);
            this.txtSaigaisha.TabIndex = 6;
            this.txtSaigaisha.Text = "0";
            this.txtSaigaisha.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtSaigaisha.Validating += new System.ComponentModel.CancelEventHandler(this.txtSaigaisha_Validating);
            // 
            // lblSaigaisha
            // 
            this.lblSaigaisha.BackColor = System.Drawing.Color.Silver;
            this.lblSaigaisha.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSaigaisha.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblSaigaisha.Location = new System.Drawing.Point(32, 146);
            this.lblSaigaisha.Name = "lblSaigaisha";
            this.lblSaigaisha.Size = new System.Drawing.Size(82, 23);
            this.lblSaigaisha.TabIndex = 69;
            this.lblSaigaisha.Text = "災 害 者";
            this.lblSaigaisha.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShiboTaishokuNm
            // 
            this.lblShiboTaishokuNm.BackColor = System.Drawing.Color.Silver;
            this.lblShiboTaishokuNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShiboTaishokuNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShiboTaishokuNm.Location = new System.Drawing.Point(132, 123);
            this.lblShiboTaishokuNm.Name = "lblShiboTaishokuNm";
            this.lblShiboTaishokuNm.Size = new System.Drawing.Size(123, 23);
            this.lblShiboTaishokuNm.TabIndex = 68;
            this.lblShiboTaishokuNm.Text = "0:対象外　1:対象";
            this.lblShiboTaishokuNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShiboTaishoku
            // 
            this.txtShiboTaishoku.AutoSizeFromLength = true;
            this.txtShiboTaishoku.DisplayLength = null;
            this.txtShiboTaishoku.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShiboTaishoku.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtShiboTaishoku.Location = new System.Drawing.Point(114, 123);
            this.txtShiboTaishoku.MaxLength = 1;
            this.txtShiboTaishoku.Name = "txtShiboTaishoku";
            this.txtShiboTaishoku.Size = new System.Drawing.Size(18, 23);
            this.txtShiboTaishoku.TabIndex = 5;
            this.txtShiboTaishoku.Text = "0";
            this.txtShiboTaishoku.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtShiboTaishoku.Validating += new System.ComponentModel.CancelEventHandler(this.txtShiboTaishoku_Validating);
            // 
            // lblShiboTaishoku
            // 
            this.lblShiboTaishoku.BackColor = System.Drawing.Color.Silver;
            this.lblShiboTaishoku.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShiboTaishoku.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShiboTaishoku.Location = new System.Drawing.Point(32, 123);
            this.lblShiboTaishoku.Name = "lblShiboTaishoku";
            this.lblShiboTaishoku.Size = new System.Drawing.Size(82, 23);
            this.lblShiboTaishoku.TabIndex = 66;
            this.lblShiboTaishoku.Text = "死亡退職";
            this.lblShiboTaishoku.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMiseinenshaNm
            // 
            this.lblMiseinenshaNm.BackColor = System.Drawing.Color.Silver;
            this.lblMiseinenshaNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMiseinenshaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMiseinenshaNm.Location = new System.Drawing.Point(132, 100);
            this.lblMiseinenshaNm.Name = "lblMiseinenshaNm";
            this.lblMiseinenshaNm.Size = new System.Drawing.Size(123, 23);
            this.lblMiseinenshaNm.TabIndex = 65;
            this.lblMiseinenshaNm.Text = "0:対象外　1:対象";
            this.lblMiseinenshaNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtMiseinensha
            // 
            this.txtMiseinensha.AutoSizeFromLength = true;
            this.txtMiseinensha.DisplayLength = null;
            this.txtMiseinensha.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMiseinensha.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtMiseinensha.Location = new System.Drawing.Point(114, 100);
            this.txtMiseinensha.MaxLength = 1;
            this.txtMiseinensha.Name = "txtMiseinensha";
            this.txtMiseinensha.Size = new System.Drawing.Size(18, 23);
            this.txtMiseinensha.TabIndex = 4;
            this.txtMiseinensha.Text = "0";
            this.txtMiseinensha.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtMiseinensha.Validating += new System.ComponentModel.CancelEventHandler(this.txtMiseinensha_Validating);
            // 
            // lblMiseinensha
            // 
            this.lblMiseinensha.BackColor = System.Drawing.Color.Silver;
            this.lblMiseinensha.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMiseinensha.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMiseinensha.Location = new System.Drawing.Point(32, 100);
            this.lblMiseinensha.Name = "lblMiseinensha";
            this.lblMiseinensha.Size = new System.Drawing.Size(82, 23);
            this.lblMiseinensha.TabIndex = 63;
            this.lblMiseinensha.Text = "未成年者";
            this.lblMiseinensha.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblHonninKubun3Nm
            // 
            this.lblHonninKubun3Nm.BackColor = System.Drawing.Color.Silver;
            this.lblHonninKubun3Nm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblHonninKubun3Nm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblHonninKubun3Nm.Location = new System.Drawing.Point(132, 77);
            this.lblHonninKubun3Nm.Name = "lblHonninKubun3Nm";
            this.lblHonninKubun3Nm.Size = new System.Drawing.Size(123, 23);
            this.lblHonninKubun3Nm.TabIndex = 62;
            this.lblHonninKubun3Nm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtHonninKubun3
            // 
            this.txtHonninKubun3.AutoSizeFromLength = true;
            this.txtHonninKubun3.DisplayLength = null;
            this.txtHonninKubun3.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtHonninKubun3.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtHonninKubun3.Location = new System.Drawing.Point(114, 77);
            this.txtHonninKubun3.MaxLength = 1;
            this.txtHonninKubun3.Name = "txtHonninKubun3";
            this.txtHonninKubun3.Size = new System.Drawing.Size(18, 23);
            this.txtHonninKubun3.TabIndex = 3;
            this.txtHonninKubun3.Text = "0";
            this.txtHonninKubun3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtHonninKubun3.Validating += new System.ComponentModel.CancelEventHandler(this.txtHonninKubun3_Validating);
            // 
            // lblHonninKubun3
            // 
            this.lblHonninKubun3.BackColor = System.Drawing.Color.Silver;
            this.lblHonninKubun3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblHonninKubun3.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblHonninKubun3.Location = new System.Drawing.Point(32, 77);
            this.lblHonninKubun3.Name = "lblHonninKubun3";
            this.lblHonninKubun3.Size = new System.Drawing.Size(82, 23);
            this.lblHonninKubun3.TabIndex = 60;
            this.lblHonninKubun3.Text = "本人区分③";
            this.lblHonninKubun3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblHonninKubun2Nm
            // 
            this.lblHonninKubun2Nm.BackColor = System.Drawing.Color.Silver;
            this.lblHonninKubun2Nm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblHonninKubun2Nm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblHonninKubun2Nm.Location = new System.Drawing.Point(132, 54);
            this.lblHonninKubun2Nm.Name = "lblHonninKubun2Nm";
            this.lblHonninKubun2Nm.Size = new System.Drawing.Size(123, 23);
            this.lblHonninKubun2Nm.TabIndex = 59;
            this.lblHonninKubun2Nm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtHonninKubun2
            // 
            this.txtHonninKubun2.AutoSizeFromLength = true;
            this.txtHonninKubun2.DisplayLength = null;
            this.txtHonninKubun2.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtHonninKubun2.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtHonninKubun2.Location = new System.Drawing.Point(114, 54);
            this.txtHonninKubun2.MaxLength = 1;
            this.txtHonninKubun2.Name = "txtHonninKubun2";
            this.txtHonninKubun2.Size = new System.Drawing.Size(18, 23);
            this.txtHonninKubun2.TabIndex = 2;
            this.txtHonninKubun2.Text = "0";
            this.txtHonninKubun2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtHonninKubun2.Validating += new System.ComponentModel.CancelEventHandler(this.txtHonninKubun2_Validating);
            // 
            // lblHonninKubun2
            // 
            this.lblHonninKubun2.BackColor = System.Drawing.Color.Silver;
            this.lblHonninKubun2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblHonninKubun2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblHonninKubun2.Location = new System.Drawing.Point(32, 54);
            this.lblHonninKubun2.Name = "lblHonninKubun2";
            this.lblHonninKubun2.Size = new System.Drawing.Size(82, 23);
            this.lblHonninKubun2.TabIndex = 57;
            this.lblHonninKubun2.Text = "本人区分②";
            this.lblHonninKubun2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShogaishaIppan
            // 
            this.txtShogaishaIppan.AutoSizeFromLength = true;
            this.txtShogaishaIppan.DisplayLength = null;
            this.txtShogaishaIppan.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShogaishaIppan.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtShogaishaIppan.Location = new System.Drawing.Point(193, 402);
            this.txtShogaishaIppan.MaxLength = 2;
            this.txtShogaishaIppan.Name = "txtShogaishaIppan";
            this.txtShogaishaIppan.Size = new System.Drawing.Size(32, 23);
            this.txtShogaishaIppan.TabIndex = 15;
            this.txtShogaishaIppan.Text = "0";
            this.txtShogaishaIppan.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShogaishaIppan.Validating += new System.ComponentModel.CancelEventHandler(this.txtShogaishaIppan_Validating);
            // 
            // lblShogaishaIppan
            // 
            this.lblShogaishaIppan.BackColor = System.Drawing.Color.Silver;
            this.lblShogaishaIppan.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShogaishaIppan.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShogaishaIppan.Location = new System.Drawing.Point(32, 402);
            this.lblShogaishaIppan.Name = "lblShogaishaIppan";
            this.lblShogaishaIppan.Size = new System.Drawing.Size(161, 23);
            this.lblShogaishaIppan.TabIndex = 54;
            this.lblShogaishaIppan.Text = "一　　　　般";
            this.lblShogaishaIppan.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFuyoDokyoTokushoIppan
            // 
            this.txtFuyoDokyoTokushoIppan.AutoSizeFromLength = true;
            this.txtFuyoDokyoTokushoIppan.DisplayLength = null;
            this.txtFuyoDokyoTokushoIppan.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoDokyoTokushoIppan.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtFuyoDokyoTokushoIppan.Location = new System.Drawing.Point(193, 379);
            this.txtFuyoDokyoTokushoIppan.MaxLength = 2;
            this.txtFuyoDokyoTokushoIppan.Name = "txtFuyoDokyoTokushoIppan";
            this.txtFuyoDokyoTokushoIppan.Size = new System.Drawing.Size(32, 23);
            this.txtFuyoDokyoTokushoIppan.TabIndex = 14;
            this.txtFuyoDokyoTokushoIppan.Text = "0";
            this.txtFuyoDokyoTokushoIppan.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFuyoDokyoTokushoIppan.Validating += new System.ComponentModel.CancelEventHandler(this.txtFuyoDokyoTokushoIppan_Validating);
            // 
            // lblFuyoDokyoTokushoIppan
            // 
            this.lblFuyoDokyoTokushoIppan.BackColor = System.Drawing.Color.Silver;
            this.lblFuyoDokyoTokushoIppan.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFuyoDokyoTokushoIppan.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFuyoDokyoTokushoIppan.Location = new System.Drawing.Point(32, 379);
            this.lblFuyoDokyoTokushoIppan.Name = "lblFuyoDokyoTokushoIppan";
            this.lblFuyoDokyoTokushoIppan.Size = new System.Drawing.Size(161, 23);
            this.lblFuyoDokyoTokushoIppan.TabIndex = 51;
            this.lblFuyoDokyoTokushoIppan.Text = "同居特別障害者";
            this.lblFuyoDokyoTokushoIppan.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblFuyoRojin
            // 
            this.lblFuyoRojin.BackColor = System.Drawing.Color.Silver;
            this.lblFuyoRojin.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFuyoRojin.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFuyoRojin.Location = new System.Drawing.Point(32, 308);
            this.lblFuyoRojin.Name = "lblFuyoRojin";
            this.lblFuyoRojin.Size = new System.Drawing.Size(25, 46);
            this.lblFuyoRojin.TabIndex = 50;
            this.lblFuyoRojin.Text = "老人";
            this.lblFuyoRojin.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblZeigakuHyoNm
            // 
            this.lblZeigakuHyoNm.BackColor = System.Drawing.Color.Silver;
            this.lblZeigakuHyoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblZeigakuHyoNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblZeigakuHyoNm.Location = new System.Drawing.Point(132, 8);
            this.lblZeigakuHyoNm.Name = "lblZeigakuHyoNm";
            this.lblZeigakuHyoNm.Size = new System.Drawing.Size(123, 23);
            this.lblZeigakuHyoNm.TabIndex = 48;
            this.lblZeigakuHyoNm.Text = "0:なし 1:甲 2:乙";
            this.lblZeigakuHyoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFuyoRojinDokyoRoshinto
            // 
            this.txtFuyoRojinDokyoRoshinto.AutoSizeFromLength = true;
            this.txtFuyoRojinDokyoRoshinto.DisplayLength = null;
            this.txtFuyoRojinDokyoRoshinto.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoRojinDokyoRoshinto.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtFuyoRojinDokyoRoshinto.Location = new System.Drawing.Point(193, 331);
            this.txtFuyoRojinDokyoRoshinto.MaxLength = 2;
            this.txtFuyoRojinDokyoRoshinto.Name = "txtFuyoRojinDokyoRoshinto";
            this.txtFuyoRojinDokyoRoshinto.Size = new System.Drawing.Size(32, 23);
            this.txtFuyoRojinDokyoRoshinto.TabIndex = 12;
            this.txtFuyoRojinDokyoRoshinto.Text = "0";
            this.txtFuyoRojinDokyoRoshinto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFuyoRojinDokyoRoshinto.Validating += new System.ComponentModel.CancelEventHandler(this.txtFuyoRojinDokyoRoshinto_Validating);
            // 
            // lblFuyoRojinDokyoRoshinto
            // 
            this.lblFuyoRojinDokyoRoshinto.BackColor = System.Drawing.Color.Silver;
            this.lblFuyoRojinDokyoRoshinto.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFuyoRojinDokyoRoshinto.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFuyoRojinDokyoRoshinto.Location = new System.Drawing.Point(57, 331);
            this.lblFuyoRojinDokyoRoshinto.Name = "lblFuyoRojinDokyoRoshinto";
            this.lblFuyoRojinDokyoRoshinto.Size = new System.Drawing.Size(136, 23);
            this.lblFuyoRojinDokyoRoshinto.TabIndex = 36;
            this.lblFuyoRojinDokyoRoshinto.Text = "同居老親等";
            this.lblFuyoRojinDokyoRoshinto.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFuyoRojinDokyoRoshintoIgai
            // 
            this.txtFuyoRojinDokyoRoshintoIgai.AutoSizeFromLength = true;
            this.txtFuyoRojinDokyoRoshintoIgai.DisplayLength = null;
            this.txtFuyoRojinDokyoRoshintoIgai.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoRojinDokyoRoshintoIgai.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtFuyoRojinDokyoRoshintoIgai.Location = new System.Drawing.Point(193, 308);
            this.txtFuyoRojinDokyoRoshintoIgai.MaxLength = 2;
            this.txtFuyoRojinDokyoRoshintoIgai.Name = "txtFuyoRojinDokyoRoshintoIgai";
            this.txtFuyoRojinDokyoRoshintoIgai.Size = new System.Drawing.Size(32, 23);
            this.txtFuyoRojinDokyoRoshintoIgai.TabIndex = 11;
            this.txtFuyoRojinDokyoRoshintoIgai.Text = "0";
            this.txtFuyoRojinDokyoRoshintoIgai.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFuyoRojinDokyoRoshintoIgai.Validating += new System.ComponentModel.CancelEventHandler(this.txtFuyoRojinDokyoRoshintoIgai_Validating);
            // 
            // lblFuyoRojinDokyoRoshintoIgai
            // 
            this.lblFuyoRojinDokyoRoshintoIgai.BackColor = System.Drawing.Color.Silver;
            this.lblFuyoRojinDokyoRoshintoIgai.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFuyoRojinDokyoRoshintoIgai.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFuyoRojinDokyoRoshintoIgai.Location = new System.Drawing.Point(57, 308);
            this.lblFuyoRojinDokyoRoshintoIgai.Name = "lblFuyoRojinDokyoRoshintoIgai";
            this.lblFuyoRojinDokyoRoshintoIgai.Size = new System.Drawing.Size(136, 23);
            this.lblFuyoRojinDokyoRoshintoIgai.TabIndex = 33;
            this.lblFuyoRojinDokyoRoshintoIgai.Text = "同居老親等以外";
            this.lblFuyoRojinDokyoRoshintoIgai.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFuyoTokutei
            // 
            this.txtFuyoTokutei.AutoSizeFromLength = true;
            this.txtFuyoTokutei.DisplayLength = null;
            this.txtFuyoTokutei.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoTokutei.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtFuyoTokutei.Location = new System.Drawing.Point(193, 285);
            this.txtFuyoTokutei.MaxLength = 2;
            this.txtFuyoTokutei.Name = "txtFuyoTokutei";
            this.txtFuyoTokutei.Size = new System.Drawing.Size(32, 23);
            this.txtFuyoTokutei.TabIndex = 10;
            this.txtFuyoTokutei.Text = "0";
            this.txtFuyoTokutei.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFuyoTokutei.Validating += new System.ComponentModel.CancelEventHandler(this.txtFuyoTokutei_Validating);
            // 
            // lblFuyoTokutei
            // 
            this.lblFuyoTokutei.BackColor = System.Drawing.Color.Silver;
            this.lblFuyoTokutei.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFuyoTokutei.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFuyoTokutei.Location = new System.Drawing.Point(32, 285);
            this.lblFuyoTokutei.Name = "lblFuyoTokutei";
            this.lblFuyoTokutei.Size = new System.Drawing.Size(161, 23);
            this.lblFuyoTokutei.TabIndex = 30;
            this.lblFuyoTokutei.Text = "特　　　定";
            this.lblFuyoTokutei.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFuyoIppan
            // 
            this.txtFuyoIppan.AutoSizeFromLength = true;
            this.txtFuyoIppan.DisplayLength = null;
            this.txtFuyoIppan.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoIppan.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtFuyoIppan.Location = new System.Drawing.Point(193, 262);
            this.txtFuyoIppan.MaxLength = 2;
            this.txtFuyoIppan.Name = "txtFuyoIppan";
            this.txtFuyoIppan.Size = new System.Drawing.Size(32, 23);
            this.txtFuyoIppan.TabIndex = 9;
            this.txtFuyoIppan.Text = "0";
            this.txtFuyoIppan.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFuyoIppan.Validating += new System.ComponentModel.CancelEventHandler(this.txtFuyoIppan_Validating);
            // 
            // lblFuyoIppan
            // 
            this.lblFuyoIppan.BackColor = System.Drawing.Color.Silver;
            this.lblFuyoIppan.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFuyoIppan.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFuyoIppan.Location = new System.Drawing.Point(32, 262);
            this.lblFuyoIppan.Name = "lblFuyoIppan";
            this.lblFuyoIppan.Size = new System.Drawing.Size(161, 23);
            this.lblFuyoIppan.TabIndex = 27;
            this.lblFuyoIppan.Text = "控除対象扶養親族等";
            this.lblFuyoIppan.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblHonninKubun1Nm
            // 
            this.lblHonninKubun1Nm.BackColor = System.Drawing.Color.Silver;
            this.lblHonninKubun1Nm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblHonninKubun1Nm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblHonninKubun1Nm.Location = new System.Drawing.Point(132, 31);
            this.lblHonninKubun1Nm.Name = "lblHonninKubun1Nm";
            this.lblHonninKubun1Nm.Size = new System.Drawing.Size(123, 23);
            this.lblHonninKubun1Nm.TabIndex = 18;
            this.lblHonninKubun1Nm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtHonninKubun1
            // 
            this.txtHonninKubun1.AutoSizeFromLength = true;
            this.txtHonninKubun1.DisplayLength = null;
            this.txtHonninKubun1.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtHonninKubun1.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtHonninKubun1.Location = new System.Drawing.Point(114, 31);
            this.txtHonninKubun1.MaxLength = 1;
            this.txtHonninKubun1.Name = "txtHonninKubun1";
            this.txtHonninKubun1.Size = new System.Drawing.Size(18, 23);
            this.txtHonninKubun1.TabIndex = 1;
            this.txtHonninKubun1.Text = "0";
            this.txtHonninKubun1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtHonninKubun1.Validating += new System.ComponentModel.CancelEventHandler(this.txtHonninKubun1_Validating);
            // 
            // lblHonninKubun1
            // 
            this.lblHonninKubun1.BackColor = System.Drawing.Color.Silver;
            this.lblHonninKubun1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblHonninKubun1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblHonninKubun1.Location = new System.Drawing.Point(32, 31);
            this.lblHonninKubun1.Name = "lblHonninKubun1";
            this.lblHonninKubun1.Size = new System.Drawing.Size(82, 23);
            this.lblHonninKubun1.TabIndex = 8;
            this.lblHonninKubun1.Text = "本人区分①";
            this.lblHonninKubun1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtZeigakuHyo
            // 
            this.txtZeigakuHyo.AutoSizeFromLength = false;
            this.txtZeigakuHyo.DisplayLength = null;
            this.txtZeigakuHyo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtZeigakuHyo.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtZeigakuHyo.Location = new System.Drawing.Point(114, 8);
            this.txtZeigakuHyo.MaxLength = 1;
            this.txtZeigakuHyo.Name = "txtZeigakuHyo";
            this.txtZeigakuHyo.Size = new System.Drawing.Size(18, 23);
            this.txtZeigakuHyo.TabIndex = 0;
            this.txtZeigakuHyo.Text = "0";
            this.txtZeigakuHyo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtZeigakuHyo.Validating += new System.ComponentModel.CancelEventHandler(this.txtZeigakuHyo_Validating);
            // 
            // lblZeigakuHyo
            // 
            this.lblZeigakuHyo.BackColor = System.Drawing.Color.Silver;
            this.lblZeigakuHyo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblZeigakuHyo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblZeigakuHyo.Location = new System.Drawing.Point(32, 8);
            this.lblZeigakuHyo.Name = "lblZeigakuHyo";
            this.lblZeigakuHyo.Size = new System.Drawing.Size(82, 23);
            this.lblZeigakuHyo.TabIndex = 0;
            this.lblZeigakuHyo.Text = "税 額 表";
            this.lblZeigakuHyo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Gray;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(7, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(24, 184);
            this.label3.TabIndex = 99;
            this.label3.Text = "所得者本人";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Sai16MimanNm
            // 
            this.Sai16MimanNm.BackColor = System.Drawing.Color.Silver;
            this.Sai16MimanNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Sai16MimanNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Sai16MimanNm.Location = new System.Drawing.Point(225, 262);
            this.Sai16MimanNm.Name = "Sai16MimanNm";
            this.Sai16MimanNm.Size = new System.Drawing.Size(30, 23);
            this.Sai16MimanNm.TabIndex = 86;
            this.Sai16MimanNm.Text = "人";
            this.Sai16MimanNm.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(72, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 23);
            this.label2.TabIndex = 2;
            this.label2.Text = "年度";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblGengoNendo
            // 
            this.lblGengoNendo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGengoNendo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGengoNendo.Location = new System.Drawing.Point(0, 0);
            this.lblGengoNendo.Name = "lblGengoNendo";
            this.lblGengoNendo.Size = new System.Drawing.Size(47, 23);
            this.lblGengoNendo.TabIndex = 0;
            this.lblGengoNendo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.lblGengoNendo);
            this.panel1.Controls.Add(this.txtGengoYearNendo);
            this.panel1.Location = new System.Drawing.Point(15, 40);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(118, 25);
            this.panel1.TabIndex = 6;
            // 
            // txtGengoYearNendo
            // 
            this.txtGengoYearNendo.AutoSizeFromLength = false;
            this.txtGengoYearNendo.DisplayLength = null;
            this.txtGengoYearNendo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGengoYearNendo.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtGengoYearNendo.Location = new System.Drawing.Point(47, 0);
            this.txtGengoYearNendo.MaxLength = 2;
            this.txtGengoYearNendo.Name = "txtGengoYearNendo";
            this.txtGengoYearNendo.Size = new System.Drawing.Size(25, 23);
            this.txtGengoYearNendo.TabIndex = 0;
            this.txtGengoYearNendo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGengoYearNendo.Validating += new System.ComponentModel.CancelEventHandler(this.txtGengoYearKaikeiNendo_Validating);
            // 
            // pnl1
            // 
            this.pnl1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.pnl1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnl1.Controls.Add(this.txtJutakuKariireToTkbtKJSu);
            this.pnl1.Controls.Add(this.lblJutakuKariireToTkbtKJSu);
            this.pnl1.Controls.Add(this.txtJutakuKariireToTkbtKJKubun1);
            this.pnl1.Controls.Add(this.txtJutakuKariireToTkbtKJKubun2);
            this.pnl1.Controls.Add(this.txtJutakuKariireToNenmatsuZan2);
            this.pnl1.Controls.Add(this.lblKojoKubun2);
            this.pnl1.Controls.Add(this.lblNenZan2);
            this.pnl1.Controls.Add(this.txtJutakuKariireToNenmatsuZan1);
            this.pnl1.Controls.Add(this.lblNenZan1);
            this.pnl1.Controls.Add(this.pnlTaishokuNengappi2);
            this.pnl1.Controls.Add(this.panel3);
            this.pnl1.Controls.Add(this.txtJutakuShutokutoTkbtKojogaku);
            this.pnl1.Controls.Add(this.label31);
            this.pnl1.Controls.Add(this.label30);
            this.pnl1.Controls.Add(this.txtZenshokubunShotokuzeigaku);
            this.pnl1.Controls.Add(this.label13);
            this.pnl1.Controls.Add(this.txtZenshokubunShakaiHokenryo);
            this.pnl1.Controls.Add(this.label14);
            this.pnl1.Controls.Add(this.label15);
            this.pnl1.Controls.Add(this.txtZenshokubunKazeiKyuyogaku);
            this.pnl1.Controls.Add(this.label25);
            this.pnl1.Controls.Add(this.txtKokuminNenkinHokenryoShrigk);
            this.pnl1.Controls.Add(this.label29);
            this.pnl1.Controls.Add(this.txtChokiSongaiHokenryoShrigk);
            this.pnl1.Controls.Add(this.label28);
            this.pnl1.Controls.Add(this.txtKojinNenkinHokenryoShrigk);
            this.pnl1.Controls.Add(this.label23);
            this.pnl1.Controls.Add(this.txtShinKojinNenkinHokenryo);
            this.pnl1.Controls.Add(this.label22);
            this.pnl1.Controls.Add(this.txtKaigoIryoHokenryo);
            this.pnl1.Controls.Add(this.label21);
            this.pnl1.Controls.Add(this.txtKyuSeimeiHokenryo);
            this.pnl1.Controls.Add(this.label20);
            this.pnl1.Controls.Add(this.txtShinSeimeiHokenryo);
            this.pnl1.Controls.Add(this.label12);
            this.pnl1.Controls.Add(this.txtHaigushaTokubetsuKojogaku);
            this.pnl1.Controls.Add(this.label10);
            this.pnl1.Controls.Add(this.txtSongaihokenryoKojogaku);
            this.pnl1.Controls.Add(this.label8);
            this.pnl1.Controls.Add(this.txtSeimeihokenryoKojogaku);
            this.pnl1.Controls.Add(this.label6);
            this.pnl1.Controls.Add(this.txtSkbKigyoKyosaiKakekinKjgk);
            this.pnl1.Controls.Add(this.label1);
            this.pnl1.Controls.Add(this.label7);
            this.pnl1.Controls.Add(this.txtHaigushaGokeiShotoku);
            this.pnl1.Controls.Add(this.txtShakaiHokenryoKjgkSnkkbn);
            this.pnl1.Controls.Add(this.label27);
            this.pnl1.Controls.Add(this.pnlTaishokuNengappi1);
            this.pnl1.Controls.Add(this.lblTaishokuNengappi1);
            this.pnl1.Controls.Add(this.label19);
            this.pnl1.Controls.Add(this.label24);
            this.pnl1.Controls.Add(this.lblZenTaishokuNengappi);
            this.pnl1.Controls.Add(this.lblTaishokuNengappi2);
            this.pnl1.Location = new System.Drawing.Point(15, 68);
            this.pnl1.Name = "pnl1";
            this.pnl1.Size = new System.Drawing.Size(540, 331);
            this.pnl1.TabIndex = 2;
            // 
            // txtJutakuKariireToTkbtKJSu
            // 
            this.txtJutakuKariireToTkbtKJSu.AutoSizeFromLength = false;
            this.txtJutakuKariireToTkbtKJSu.DisplayLength = null;
            this.txtJutakuKariireToTkbtKJSu.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtJutakuKariireToTkbtKJSu.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtJutakuKariireToTkbtKJSu.Location = new System.Drawing.Point(478, 301);
            this.txtJutakuKariireToTkbtKJSu.MaxLength = 1;
            this.txtJutakuKariireToTkbtKJSu.Name = "txtJutakuKariireToTkbtKJSu";
            this.txtJutakuKariireToTkbtKJSu.Size = new System.Drawing.Size(40, 23);
            this.txtJutakuKariireToTkbtKJSu.TabIndex = 30;
            this.txtJutakuKariireToTkbtKJSu.Validating += new System.ComponentModel.CancelEventHandler(this.txtJutakuKariireToTkbtKJSu_Validating);
            // 
            // lblJutakuKariireToTkbtKJSu
            // 
            this.lblJutakuKariireToTkbtKJSu.BackColor = System.Drawing.Color.Silver;
            this.lblJutakuKariireToTkbtKJSu.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblJutakuKariireToTkbtKJSu.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblJutakuKariireToTkbtKJSu.Location = new System.Drawing.Point(288, 302);
            this.lblJutakuKariireToTkbtKJSu.Name = "lblJutakuKariireToTkbtKJSu";
            this.lblJutakuKariireToTkbtKJSu.Size = new System.Drawing.Size(236, 23);
            this.lblJutakuKariireToTkbtKJSu.TabIndex = 960;
            this.lblJutakuKariireToTkbtKJSu.Text = "住宅借入金等特別控除適用数";
            this.lblJutakuKariireToTkbtKJSu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtJutakuKariireToTkbtKJKubun1
            // 
            this.txtJutakuKariireToTkbtKJKubun1.AutoSizeFromLength = false;
            this.txtJutakuKariireToTkbtKJKubun1.DisplayLength = null;
            this.txtJutakuKariireToTkbtKJKubun1.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtJutakuKariireToTkbtKJKubun1.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtJutakuKariireToTkbtKJKubun1.Location = new System.Drawing.Point(144, 276);
            this.txtJutakuKariireToTkbtKJKubun1.MaxLength = 20;
            this.txtJutakuKariireToTkbtKJKubun1.Name = "txtJutakuKariireToTkbtKJKubun1";
            this.txtJutakuKariireToTkbtKJKubun1.Size = new System.Drawing.Size(116, 23);
            this.txtJutakuKariireToTkbtKJKubun1.TabIndex = 14;
            this.txtJutakuKariireToTkbtKJKubun1.Validating += new System.ComponentModel.CancelEventHandler(this.txtJutakuKariireToTkbtKJKubun1_Validating);
            // 
            // txtJutakuKariireToTkbtKJKubun2
            // 
            this.txtJutakuKariireToTkbtKJKubun2.AutoSizeFromLength = false;
            this.txtJutakuKariireToTkbtKJKubun2.DisplayLength = null;
            this.txtJutakuKariireToTkbtKJKubun2.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtJutakuKariireToTkbtKJKubun2.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtJutakuKariireToTkbtKJKubun2.Location = new System.Drawing.Point(144, 299);
            this.txtJutakuKariireToTkbtKJKubun2.MaxLength = 20;
            this.txtJutakuKariireToTkbtKJKubun2.Name = "txtJutakuKariireToTkbtKJKubun2";
            this.txtJutakuKariireToTkbtKJKubun2.Size = new System.Drawing.Size(116, 23);
            this.txtJutakuKariireToTkbtKJKubun2.TabIndex = 15;
            this.txtJutakuKariireToTkbtKJKubun2.Validating += new System.ComponentModel.CancelEventHandler(this.txtJutakuKariireToTkbtKJKubun2_Validating);
            // 
            // txtJutakuKariireToNenmatsuZan2
            // 
            this.txtJutakuKariireToNenmatsuZan2.AcceptsReturn = true;
            this.txtJutakuKariireToNenmatsuZan2.AutoSizeFromLength = false;
            this.txtJutakuKariireToNenmatsuZan2.DisplayLength = null;
            this.txtJutakuKariireToNenmatsuZan2.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtJutakuKariireToNenmatsuZan2.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtJutakuKariireToNenmatsuZan2.Location = new System.Drawing.Point(141, 252);
            this.txtJutakuKariireToNenmatsuZan2.MaxLength = 9;
            this.txtJutakuKariireToNenmatsuZan2.Name = "txtJutakuKariireToNenmatsuZan2";
            this.txtJutakuKariireToNenmatsuZan2.Size = new System.Drawing.Size(112, 23);
            this.txtJutakuKariireToNenmatsuZan2.TabIndex = 13;
            this.txtJutakuKariireToNenmatsuZan2.Text = "0";
            this.txtJutakuKariireToNenmatsuZan2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtJutakuKariireToNenmatsuZan2.Validating += new System.ComponentModel.CancelEventHandler(this.txtJutakuKariireToNenmatsuZan2_Validating);
            // 
            // lblKojoKubun2
            // 
            this.lblKojoKubun2.BackColor = System.Drawing.Color.Silver;
            this.lblKojoKubun2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKojoKubun2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKojoKubun2.Location = new System.Drawing.Point(30, 300);
            this.lblKojoKubun2.Name = "lblKojoKubun2";
            this.lblKojoKubun2.Size = new System.Drawing.Size(114, 23);
            this.lblKojoKubun2.TabIndex = 957;
            this.lblKojoKubun2.Text = "控除区分(2回目)";
            this.lblKojoKubun2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblNenZan2
            // 
            this.lblNenZan2.BackColor = System.Drawing.Color.Silver;
            this.lblNenZan2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblNenZan2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblNenZan2.Location = new System.Drawing.Point(29, 253);
            this.lblNenZan2.Name = "lblNenZan2";
            this.lblNenZan2.Size = new System.Drawing.Size(114, 23);
            this.lblNenZan2.TabIndex = 953;
            this.lblNenZan2.Text = "年末残高(2回目)";
            this.lblNenZan2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtJutakuKariireToNenmatsuZan1
            // 
            this.txtJutakuKariireToNenmatsuZan1.AcceptsReturn = true;
            this.txtJutakuKariireToNenmatsuZan1.AutoSizeFromLength = false;
            this.txtJutakuKariireToNenmatsuZan1.DisplayLength = null;
            this.txtJutakuKariireToNenmatsuZan1.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtJutakuKariireToNenmatsuZan1.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtJutakuKariireToNenmatsuZan1.Location = new System.Drawing.Point(141, 229);
            this.txtJutakuKariireToNenmatsuZan1.MaxLength = 9;
            this.txtJutakuKariireToNenmatsuZan1.Name = "txtJutakuKariireToNenmatsuZan1";
            this.txtJutakuKariireToNenmatsuZan1.Size = new System.Drawing.Size(112, 23);
            this.txtJutakuKariireToNenmatsuZan1.TabIndex = 12;
            this.txtJutakuKariireToNenmatsuZan1.Text = "0";
            this.txtJutakuKariireToNenmatsuZan1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtJutakuKariireToNenmatsuZan1.Validating += new System.ComponentModel.CancelEventHandler(this.txtJutakuKariireToNenmatsuZan1_Validating);
            // 
            // lblNenZan1
            // 
            this.lblNenZan1.BackColor = System.Drawing.Color.Silver;
            this.lblNenZan1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblNenZan1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblNenZan1.Location = new System.Drawing.Point(29, 229);
            this.lblNenZan1.Name = "lblNenZan1";
            this.lblNenZan1.Size = new System.Drawing.Size(114, 23);
            this.lblNenZan1.TabIndex = 951;
            this.lblNenZan1.Text = "年末残高(1回目)";
            this.lblNenZan1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnlTaishokuNengappi2
            // 
            this.pnlTaishokuNengappi2.BackColor = System.Drawing.Color.Silver;
            this.pnlTaishokuNengappi2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlTaishokuNengappi2.Controls.Add(this.lblDayTaishokuNengappi2);
            this.pnlTaishokuNengappi2.Controls.Add(this.txtDayIjuKaishiDate2);
            this.pnlTaishokuNengappi2.Controls.Add(this.lblMonthTaishokuNengappi2);
            this.pnlTaishokuNengappi2.Controls.Add(this.lblGengoYearTaishokuNengappi2);
            this.pnlTaishokuNengappi2.Controls.Add(this.txtMonthIjuKaishiDate2);
            this.pnlTaishokuNengappi2.Controls.Add(this.lblGengoIjuKaishiDate2);
            this.pnlTaishokuNengappi2.Controls.Add(this.txtGengoYearIjuKaishiDate2);
            this.pnlTaishokuNengappi2.Location = new System.Drawing.Point(28, 202);
            this.pnlTaishokuNengappi2.Name = "pnlTaishokuNengappi2";
            this.pnlTaishokuNengappi2.Size = new System.Drawing.Size(225, 25);
            this.pnlTaishokuNengappi2.TabIndex = 16;
            // 
            // lblDayTaishokuNengappi2
            // 
            this.lblDayTaishokuNengappi2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDayTaishokuNengappi2.Location = new System.Drawing.Point(164, 0);
            this.lblDayTaishokuNengappi2.Name = "lblDayTaishokuNengappi2";
            this.lblDayTaishokuNengappi2.Size = new System.Drawing.Size(21, 23);
            this.lblDayTaishokuNengappi2.TabIndex = 14;
            this.lblDayTaishokuNengappi2.Text = "日";
            this.lblDayTaishokuNengappi2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtDayIjuKaishiDate2
            // 
            this.txtDayIjuKaishiDate2.AutoSizeFromLength = false;
            this.txtDayIjuKaishiDate2.DisplayLength = null;
            this.txtDayIjuKaishiDate2.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDayIjuKaishiDate2.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtDayIjuKaishiDate2.Location = new System.Drawing.Point(139, 0);
            this.txtDayIjuKaishiDate2.MaxLength = 2;
            this.txtDayIjuKaishiDate2.Name = "txtDayIjuKaishiDate2";
            this.txtDayIjuKaishiDate2.Size = new System.Drawing.Size(25, 23);
            this.txtDayIjuKaishiDate2.TabIndex = 11;
            this.txtDayIjuKaishiDate2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDayIjuKaishiDate2.Validating += new System.ComponentModel.CancelEventHandler(this.txtDayIjuKaishiDate2_Validating);
            // 
            // lblMonthTaishokuNengappi2
            // 
            this.lblMonthTaishokuNengappi2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMonthTaishokuNengappi2.Location = new System.Drawing.Point(118, 0);
            this.lblMonthTaishokuNengappi2.Name = "lblMonthTaishokuNengappi2";
            this.lblMonthTaishokuNengappi2.Size = new System.Drawing.Size(21, 23);
            this.lblMonthTaishokuNengappi2.TabIndex = 12;
            this.lblMonthTaishokuNengappi2.Text = "月";
            this.lblMonthTaishokuNengappi2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblGengoYearTaishokuNengappi2
            // 
            this.lblGengoYearTaishokuNengappi2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGengoYearTaishokuNengappi2.Location = new System.Drawing.Point(72, 0);
            this.lblGengoYearTaishokuNengappi2.Name = "lblGengoYearTaishokuNengappi2";
            this.lblGengoYearTaishokuNengappi2.Size = new System.Drawing.Size(21, 23);
            this.lblGengoYearTaishokuNengappi2.TabIndex = 10;
            this.lblGengoYearTaishokuNengappi2.Text = "年";
            this.lblGengoYearTaishokuNengappi2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtMonthIjuKaishiDate2
            // 
            this.txtMonthIjuKaishiDate2.AutoSizeFromLength = false;
            this.txtMonthIjuKaishiDate2.DisplayLength = null;
            this.txtMonthIjuKaishiDate2.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMonthIjuKaishiDate2.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtMonthIjuKaishiDate2.Location = new System.Drawing.Point(93, 0);
            this.txtMonthIjuKaishiDate2.MaxLength = 2;
            this.txtMonthIjuKaishiDate2.Name = "txtMonthIjuKaishiDate2";
            this.txtMonthIjuKaishiDate2.Size = new System.Drawing.Size(25, 23);
            this.txtMonthIjuKaishiDate2.TabIndex = 10;
            this.txtMonthIjuKaishiDate2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonthIjuKaishiDate2.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthIjuKaishiDate2_Validating);
            // 
            // lblGengoIjuKaishiDate2
            // 
            this.lblGengoIjuKaishiDate2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGengoIjuKaishiDate2.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGengoIjuKaishiDate2.Location = new System.Drawing.Point(0, 0);
            this.lblGengoIjuKaishiDate2.Name = "lblGengoIjuKaishiDate2";
            this.lblGengoIjuKaishiDate2.Size = new System.Drawing.Size(47, 23);
            this.lblGengoIjuKaishiDate2.TabIndex = 5;
            this.lblGengoIjuKaishiDate2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtGengoYearIjuKaishiDate2
            // 
            this.txtGengoYearIjuKaishiDate2.AutoSizeFromLength = false;
            this.txtGengoYearIjuKaishiDate2.DisplayLength = null;
            this.txtGengoYearIjuKaishiDate2.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGengoYearIjuKaishiDate2.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtGengoYearIjuKaishiDate2.Location = new System.Drawing.Point(47, 0);
            this.txtGengoYearIjuKaishiDate2.MaxLength = 2;
            this.txtGengoYearIjuKaishiDate2.Name = "txtGengoYearIjuKaishiDate2";
            this.txtGengoYearIjuKaishiDate2.Size = new System.Drawing.Size(25, 23);
            this.txtGengoYearIjuKaishiDate2.TabIndex = 9;
            this.txtGengoYearIjuKaishiDate2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGengoYearIjuKaishiDate2.Validating += new System.ComponentModel.CancelEventHandler(this.txtGengoYearIjuKaishiDate2_Validating);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Silver;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Controls.Add(this.label32);
            this.panel3.Controls.Add(this.txtDayZenTaishokuDate);
            this.panel3.Controls.Add(this.label35);
            this.panel3.Controls.Add(this.label36);
            this.panel3.Controls.Add(this.txtMonthZenTaishokuDate);
            this.panel3.Controls.Add(this.lblGengoZenTaishokuDate);
            this.panel3.Controls.Add(this.txtGengoYearZenTaishokuDate);
            this.panel3.Location = new System.Drawing.Point(299, 276);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(225, 25);
            this.panel3.TabIndex = 949;
            // 
            // label32
            // 
            this.label32.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label32.Location = new System.Drawing.Point(164, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(21, 23);
            this.label32.TabIndex = 6;
            this.label32.Text = "日";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtDayZenTaishokuDate
            // 
            this.txtDayZenTaishokuDate.AutoSizeFromLength = false;
            this.txtDayZenTaishokuDate.DisplayLength = null;
            this.txtDayZenTaishokuDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDayZenTaishokuDate.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtDayZenTaishokuDate.Location = new System.Drawing.Point(139, 0);
            this.txtDayZenTaishokuDate.MaxLength = 2;
            this.txtDayZenTaishokuDate.Name = "txtDayZenTaishokuDate";
            this.txtDayZenTaishokuDate.Size = new System.Drawing.Size(25, 23);
            this.txtDayZenTaishokuDate.TabIndex = 29;
            this.txtDayZenTaishokuDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDayZenTaishokuDate.Validating += new System.ComponentModel.CancelEventHandler(this.txtDayZenTaishokuDate_Validating);
            // 
            // label35
            // 
            this.label35.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label35.Location = new System.Drawing.Point(118, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(21, 23);
            this.label35.TabIndex = 6;
            this.label35.Text = "月";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label36
            // 
            this.label36.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label36.Location = new System.Drawing.Point(72, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(21, 23);
            this.label36.TabIndex = 3;
            this.label36.Text = "年";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtMonthZenTaishokuDate
            // 
            this.txtMonthZenTaishokuDate.AutoSizeFromLength = false;
            this.txtMonthZenTaishokuDate.DisplayLength = null;
            this.txtMonthZenTaishokuDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMonthZenTaishokuDate.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtMonthZenTaishokuDate.Location = new System.Drawing.Point(93, 0);
            this.txtMonthZenTaishokuDate.MaxLength = 2;
            this.txtMonthZenTaishokuDate.Name = "txtMonthZenTaishokuDate";
            this.txtMonthZenTaishokuDate.Size = new System.Drawing.Size(25, 23);
            this.txtMonthZenTaishokuDate.TabIndex = 28;
            this.txtMonthZenTaishokuDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonthZenTaishokuDate.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthZenTaishokuDate_Validating);
            // 
            // lblGengoZenTaishokuDate
            // 
            this.lblGengoZenTaishokuDate.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGengoZenTaishokuDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGengoZenTaishokuDate.Location = new System.Drawing.Point(0, 0);
            this.lblGengoZenTaishokuDate.Name = "lblGengoZenTaishokuDate";
            this.lblGengoZenTaishokuDate.Size = new System.Drawing.Size(47, 23);
            this.lblGengoZenTaishokuDate.TabIndex = 1;
            this.lblGengoZenTaishokuDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtGengoYearZenTaishokuDate
            // 
            this.txtGengoYearZenTaishokuDate.AutoSizeFromLength = false;
            this.txtGengoYearZenTaishokuDate.DisplayLength = null;
            this.txtGengoYearZenTaishokuDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGengoYearZenTaishokuDate.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtGengoYearZenTaishokuDate.Location = new System.Drawing.Point(47, 0);
            this.txtGengoYearZenTaishokuDate.MaxLength = 2;
            this.txtGengoYearZenTaishokuDate.Name = "txtGengoYearZenTaishokuDate";
            this.txtGengoYearZenTaishokuDate.Size = new System.Drawing.Size(25, 23);
            this.txtGengoYearZenTaishokuDate.TabIndex = 27;
            this.txtGengoYearZenTaishokuDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGengoYearZenTaishokuDate.Validating += new System.ComponentModel.CancelEventHandler(this.txtGengoYearZenTaishokuDate_Validating);
            // 
            // txtJutakuShutokutoTkbtKojogaku
            // 
            this.txtJutakuShutokutoTkbtKojogaku.AcceptsReturn = true;
            this.txtJutakuShutokutoTkbtKojogaku.AutoSizeFromLength = false;
            this.txtJutakuShutokutoTkbtKojogaku.DisplayLength = null;
            this.txtJutakuShutokutoTkbtKojogaku.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtJutakuShutokutoTkbtKojogaku.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtJutakuShutokutoTkbtKojogaku.Location = new System.Drawing.Point(156, 120);
            this.txtJutakuShutokutoTkbtKojogaku.MaxLength = 9;
            this.txtJutakuShutokutoTkbtKojogaku.Name = "txtJutakuShutokutoTkbtKojogaku";
            this.txtJutakuShutokutoTkbtKojogaku.Size = new System.Drawing.Size(98, 23);
            this.txtJutakuShutokutoTkbtKojogaku.TabIndex = 5;
            this.txtJutakuShutokutoTkbtKojogaku.Text = "0";
            this.txtJutakuShutokutoTkbtKojogaku.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtJutakuShutokutoTkbtKojogaku.Enter += new System.EventHandler(this.txtKingaku_Enter);
            this.txtJutakuShutokutoTkbtKojogaku.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingaku_Validating);
            // 
            // label31
            // 
            this.label31.BackColor = System.Drawing.Color.Silver;
            this.label31.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label31.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label31.Location = new System.Drawing.Point(30, 120);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(126, 23);
            this.label31.TabIndex = 947;
            this.label31.Text = "住宅借入金等特別";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label30
            // 
            this.label30.BackColor = System.Drawing.Color.Gray;
            this.label30.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label30.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label30.ForeColor = System.Drawing.Color.White;
            this.label30.Location = new System.Drawing.Point(5, 120);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(24, 203);
            this.label30.TabIndex = 945;
            this.label30.Text = "税額控除額";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtZenshokubunShotokuzeigaku
            // 
            this.txtZenshokubunShotokuzeigaku.AcceptsReturn = true;
            this.txtZenshokubunShotokuzeigaku.AutoSizeFromLength = false;
            this.txtZenshokubunShotokuzeigaku.DisplayLength = null;
            this.txtZenshokubunShotokuzeigaku.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtZenshokubunShotokuzeigaku.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtZenshokubunShotokuzeigaku.Location = new System.Drawing.Point(428, 235);
            this.txtZenshokubunShotokuzeigaku.MaxLength = 9;
            this.txtZenshokubunShotokuzeigaku.Name = "txtZenshokubunShotokuzeigaku";
            this.txtZenshokubunShotokuzeigaku.Size = new System.Drawing.Size(98, 23);
            this.txtZenshokubunShotokuzeigaku.TabIndex = 26;
            this.txtZenshokubunShotokuzeigaku.Text = "0";
            this.txtZenshokubunShotokuzeigaku.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtZenshokubunShotokuzeigaku.Enter += new System.EventHandler(this.txtKingaku_Enter);
            this.txtZenshokubunShotokuzeigaku.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingaku_Validating);
            // 
            // label13
            // 
            this.label13.BackColor = System.Drawing.Color.Silver;
            this.label13.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label13.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label13.Location = new System.Drawing.Point(302, 235);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(126, 23);
            this.label13.TabIndex = 943;
            this.label13.Text = "所得税額";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtZenshokubunShakaiHokenryo
            // 
            this.txtZenshokubunShakaiHokenryo.AcceptsReturn = true;
            this.txtZenshokubunShakaiHokenryo.AutoSizeFromLength = false;
            this.txtZenshokubunShakaiHokenryo.DisplayLength = null;
            this.txtZenshokubunShakaiHokenryo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtZenshokubunShakaiHokenryo.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtZenshokubunShakaiHokenryo.Location = new System.Drawing.Point(428, 212);
            this.txtZenshokubunShakaiHokenryo.MaxLength = 9;
            this.txtZenshokubunShakaiHokenryo.Name = "txtZenshokubunShakaiHokenryo";
            this.txtZenshokubunShakaiHokenryo.Size = new System.Drawing.Size(98, 23);
            this.txtZenshokubunShakaiHokenryo.TabIndex = 25;
            this.txtZenshokubunShakaiHokenryo.Text = "0";
            this.txtZenshokubunShakaiHokenryo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtZenshokubunShakaiHokenryo.Enter += new System.EventHandler(this.txtKingaku_Enter);
            this.txtZenshokubunShakaiHokenryo.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingaku_Validating);
            // 
            // label14
            // 
            this.label14.BackColor = System.Drawing.Color.Silver;
            this.label14.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label14.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label14.Location = new System.Drawing.Point(302, 212);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(126, 23);
            this.label14.TabIndex = 941;
            this.label14.Text = "社会保険料";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label15
            // 
            this.label15.BackColor = System.Drawing.Color.Gray;
            this.label15.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label15.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label15.ForeColor = System.Drawing.Color.White;
            this.label15.Location = new System.Drawing.Point(278, 189);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(24, 69);
            this.label15.TabIndex = 940;
            this.label15.Text = "前職分給与";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtZenshokubunKazeiKyuyogaku
            // 
            this.txtZenshokubunKazeiKyuyogaku.AcceptsReturn = true;
            this.txtZenshokubunKazeiKyuyogaku.AutoSizeFromLength = false;
            this.txtZenshokubunKazeiKyuyogaku.DisplayLength = null;
            this.txtZenshokubunKazeiKyuyogaku.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtZenshokubunKazeiKyuyogaku.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtZenshokubunKazeiKyuyogaku.Location = new System.Drawing.Point(428, 189);
            this.txtZenshokubunKazeiKyuyogaku.MaxLength = 9;
            this.txtZenshokubunKazeiKyuyogaku.Name = "txtZenshokubunKazeiKyuyogaku";
            this.txtZenshokubunKazeiKyuyogaku.Size = new System.Drawing.Size(98, 23);
            this.txtZenshokubunKazeiKyuyogaku.TabIndex = 24;
            this.txtZenshokubunKazeiKyuyogaku.Text = "0";
            this.txtZenshokubunKazeiKyuyogaku.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtZenshokubunKazeiKyuyogaku.Enter += new System.EventHandler(this.txtKingaku_Enter);
            this.txtZenshokubunKazeiKyuyogaku.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingaku_Validating);
            // 
            // label25
            // 
            this.label25.BackColor = System.Drawing.Color.Silver;
            this.label25.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label25.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label25.Location = new System.Drawing.Point(302, 189);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(126, 23);
            this.label25.TabIndex = 938;
            this.label25.Text = "課税分給与額";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKokuminNenkinHokenryoShrigk
            // 
            this.txtKokuminNenkinHokenryoShrigk.AcceptsReturn = true;
            this.txtKokuminNenkinHokenryoShrigk.AutoSizeFromLength = false;
            this.txtKokuminNenkinHokenryoShrigk.DisplayLength = null;
            this.txtKokuminNenkinHokenryoShrigk.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKokuminNenkinHokenryoShrigk.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKokuminNenkinHokenryoShrigk.Location = new System.Drawing.Point(428, 164);
            this.txtKokuminNenkinHokenryoShrigk.MaxLength = 9;
            this.txtKokuminNenkinHokenryoShrigk.Name = "txtKokuminNenkinHokenryoShrigk";
            this.txtKokuminNenkinHokenryoShrigk.Size = new System.Drawing.Size(98, 23);
            this.txtKokuminNenkinHokenryoShrigk.TabIndex = 23;
            this.txtKokuminNenkinHokenryoShrigk.Text = "0";
            this.txtKokuminNenkinHokenryoShrigk.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKokuminNenkinHokenryoShrigk.Enter += new System.EventHandler(this.txtKingaku_Enter);
            this.txtKokuminNenkinHokenryoShrigk.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingaku_Validating);
            // 
            // label29
            // 
            this.label29.BackColor = System.Drawing.Color.Silver;
            this.label29.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label29.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label29.Location = new System.Drawing.Point(302, 164);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(126, 23);
            this.label29.TabIndex = 936;
            this.label29.Text = "国民年金保険料";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtChokiSongaiHokenryoShrigk
            // 
            this.txtChokiSongaiHokenryoShrigk.AcceptsReturn = true;
            this.txtChokiSongaiHokenryoShrigk.AutoSizeFromLength = false;
            this.txtChokiSongaiHokenryoShrigk.DisplayLength = null;
            this.txtChokiSongaiHokenryoShrigk.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtChokiSongaiHokenryoShrigk.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtChokiSongaiHokenryoShrigk.Location = new System.Drawing.Point(428, 141);
            this.txtChokiSongaiHokenryoShrigk.MaxLength = 9;
            this.txtChokiSongaiHokenryoShrigk.Name = "txtChokiSongaiHokenryoShrigk";
            this.txtChokiSongaiHokenryoShrigk.Size = new System.Drawing.Size(98, 23);
            this.txtChokiSongaiHokenryoShrigk.TabIndex = 22;
            this.txtChokiSongaiHokenryoShrigk.Text = "0";
            this.txtChokiSongaiHokenryoShrigk.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtChokiSongaiHokenryoShrigk.Enter += new System.EventHandler(this.txtKingaku_Enter);
            this.txtChokiSongaiHokenryoShrigk.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingaku_Validating);
            // 
            // label28
            // 
            this.label28.BackColor = System.Drawing.Color.Silver;
            this.label28.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label28.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label28.Location = new System.Drawing.Point(302, 141);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(126, 23);
            this.label28.TabIndex = 934;
            this.label28.Text = "旧長期損害保険料";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKojinNenkinHokenryoShrigk
            // 
            this.txtKojinNenkinHokenryoShrigk.AcceptsReturn = true;
            this.txtKojinNenkinHokenryoShrigk.AutoSizeFromLength = false;
            this.txtKojinNenkinHokenryoShrigk.DisplayLength = null;
            this.txtKojinNenkinHokenryoShrigk.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKojinNenkinHokenryoShrigk.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKojinNenkinHokenryoShrigk.Location = new System.Drawing.Point(428, 118);
            this.txtKojinNenkinHokenryoShrigk.MaxLength = 9;
            this.txtKojinNenkinHokenryoShrigk.Name = "txtKojinNenkinHokenryoShrigk";
            this.txtKojinNenkinHokenryoShrigk.Size = new System.Drawing.Size(98, 23);
            this.txtKojinNenkinHokenryoShrigk.TabIndex = 21;
            this.txtKojinNenkinHokenryoShrigk.Text = "0";
            this.txtKojinNenkinHokenryoShrigk.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKojinNenkinHokenryoShrigk.Enter += new System.EventHandler(this.txtKingaku_Enter);
            this.txtKojinNenkinHokenryoShrigk.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingaku_Validating);
            // 
            // label23
            // 
            this.label23.BackColor = System.Drawing.Color.Silver;
            this.label23.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label23.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label23.Location = new System.Drawing.Point(302, 118);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(126, 23);
            this.label23.TabIndex = 932;
            this.label23.Text = "旧個人年金保険料";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShinKojinNenkinHokenryo
            // 
            this.txtShinKojinNenkinHokenryo.AcceptsReturn = true;
            this.txtShinKojinNenkinHokenryo.AutoSizeFromLength = false;
            this.txtShinKojinNenkinHokenryo.DisplayLength = null;
            this.txtShinKojinNenkinHokenryo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShinKojinNenkinHokenryo.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtShinKojinNenkinHokenryo.Location = new System.Drawing.Point(428, 95);
            this.txtShinKojinNenkinHokenryo.MaxLength = 9;
            this.txtShinKojinNenkinHokenryo.Name = "txtShinKojinNenkinHokenryo";
            this.txtShinKojinNenkinHokenryo.Size = new System.Drawing.Size(98, 23);
            this.txtShinKojinNenkinHokenryo.TabIndex = 20;
            this.txtShinKojinNenkinHokenryo.Text = "0";
            this.txtShinKojinNenkinHokenryo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShinKojinNenkinHokenryo.Enter += new System.EventHandler(this.txtKingaku_Enter);
            this.txtShinKojinNenkinHokenryo.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingaku_Validating);
            // 
            // label22
            // 
            this.label22.BackColor = System.Drawing.Color.Silver;
            this.label22.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label22.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label22.Location = new System.Drawing.Point(302, 95);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(126, 23);
            this.label22.TabIndex = 930;
            this.label22.Text = "新個人年金保険料";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKaigoIryoHokenryo
            // 
            this.txtKaigoIryoHokenryo.AcceptsReturn = true;
            this.txtKaigoIryoHokenryo.AutoSizeFromLength = false;
            this.txtKaigoIryoHokenryo.DisplayLength = null;
            this.txtKaigoIryoHokenryo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKaigoIryoHokenryo.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKaigoIryoHokenryo.Location = new System.Drawing.Point(428, 72);
            this.txtKaigoIryoHokenryo.MaxLength = 9;
            this.txtKaigoIryoHokenryo.Name = "txtKaigoIryoHokenryo";
            this.txtKaigoIryoHokenryo.Size = new System.Drawing.Size(98, 23);
            this.txtKaigoIryoHokenryo.TabIndex = 19;
            this.txtKaigoIryoHokenryo.Text = "0";
            this.txtKaigoIryoHokenryo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKaigoIryoHokenryo.Enter += new System.EventHandler(this.txtKingaku_Enter);
            this.txtKaigoIryoHokenryo.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingaku_Validating);
            // 
            // label21
            // 
            this.label21.BackColor = System.Drawing.Color.Silver;
            this.label21.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label21.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label21.Location = new System.Drawing.Point(302, 72);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(126, 23);
            this.label21.TabIndex = 928;
            this.label21.Text = "介護医療保険料";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKyuSeimeiHokenryo
            // 
            this.txtKyuSeimeiHokenryo.AcceptsReturn = true;
            this.txtKyuSeimeiHokenryo.AutoSizeFromLength = false;
            this.txtKyuSeimeiHokenryo.DisplayLength = null;
            this.txtKyuSeimeiHokenryo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKyuSeimeiHokenryo.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKyuSeimeiHokenryo.Location = new System.Drawing.Point(428, 49);
            this.txtKyuSeimeiHokenryo.MaxLength = 9;
            this.txtKyuSeimeiHokenryo.Name = "txtKyuSeimeiHokenryo";
            this.txtKyuSeimeiHokenryo.Size = new System.Drawing.Size(98, 23);
            this.txtKyuSeimeiHokenryo.TabIndex = 18;
            this.txtKyuSeimeiHokenryo.Text = "0";
            this.txtKyuSeimeiHokenryo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKyuSeimeiHokenryo.Enter += new System.EventHandler(this.txtKingaku_Enter);
            this.txtKyuSeimeiHokenryo.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingaku_Validating);
            // 
            // label20
            // 
            this.label20.BackColor = System.Drawing.Color.Silver;
            this.label20.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label20.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label20.Location = new System.Drawing.Point(302, 49);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(126, 23);
            this.label20.TabIndex = 926;
            this.label20.Text = "旧生命保険料";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShinSeimeiHokenryo
            // 
            this.txtShinSeimeiHokenryo.AcceptsReturn = true;
            this.txtShinSeimeiHokenryo.AutoSizeFromLength = false;
            this.txtShinSeimeiHokenryo.DisplayLength = null;
            this.txtShinSeimeiHokenryo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShinSeimeiHokenryo.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtShinSeimeiHokenryo.Location = new System.Drawing.Point(428, 26);
            this.txtShinSeimeiHokenryo.MaxLength = 9;
            this.txtShinSeimeiHokenryo.Name = "txtShinSeimeiHokenryo";
            this.txtShinSeimeiHokenryo.Size = new System.Drawing.Size(98, 23);
            this.txtShinSeimeiHokenryo.TabIndex = 17;
            this.txtShinSeimeiHokenryo.Text = "0";
            this.txtShinSeimeiHokenryo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShinSeimeiHokenryo.Enter += new System.EventHandler(this.txtKingaku_Enter);
            this.txtShinSeimeiHokenryo.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingaku_Validating);
            // 
            // label12
            // 
            this.label12.BackColor = System.Drawing.Color.Silver;
            this.label12.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label12.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label12.Location = new System.Drawing.Point(302, 26);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(126, 23);
            this.label12.TabIndex = 924;
            this.label12.Text = "新生命保険料";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtHaigushaTokubetsuKojogaku
            // 
            this.txtHaigushaTokubetsuKojogaku.AcceptsReturn = true;
            this.txtHaigushaTokubetsuKojogaku.AutoSizeFromLength = false;
            this.txtHaigushaTokubetsuKojogaku.DisplayLength = null;
            this.txtHaigushaTokubetsuKojogaku.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtHaigushaTokubetsuKojogaku.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtHaigushaTokubetsuKojogaku.Location = new System.Drawing.Point(156, 96);
            this.txtHaigushaTokubetsuKojogaku.MaxLength = 9;
            this.txtHaigushaTokubetsuKojogaku.Name = "txtHaigushaTokubetsuKojogaku";
            this.txtHaigushaTokubetsuKojogaku.Size = new System.Drawing.Size(98, 23);
            this.txtHaigushaTokubetsuKojogaku.TabIndex = 4;
            this.txtHaigushaTokubetsuKojogaku.Text = "0";
            this.txtHaigushaTokubetsuKojogaku.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtHaigushaTokubetsuKojogaku.Enter += new System.EventHandler(this.txtKingaku_Enter);
            this.txtHaigushaTokubetsuKojogaku.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingaku_Validating);
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.Color.Silver;
            this.label10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label10.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label10.Location = new System.Drawing.Point(30, 96);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(126, 23);
            this.label10.TabIndex = 922;
            this.label10.Text = "配偶者特別";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtSongaihokenryoKojogaku
            // 
            this.txtSongaihokenryoKojogaku.AcceptsReturn = true;
            this.txtSongaihokenryoKojogaku.AutoSizeFromLength = false;
            this.txtSongaihokenryoKojogaku.DisplayLength = null;
            this.txtSongaihokenryoKojogaku.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtSongaihokenryoKojogaku.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtSongaihokenryoKojogaku.Location = new System.Drawing.Point(156, 73);
            this.txtSongaihokenryoKojogaku.MaxLength = 9;
            this.txtSongaihokenryoKojogaku.Name = "txtSongaihokenryoKojogaku";
            this.txtSongaihokenryoKojogaku.Size = new System.Drawing.Size(98, 23);
            this.txtSongaihokenryoKojogaku.TabIndex = 3;
            this.txtSongaihokenryoKojogaku.Text = "0";
            this.txtSongaihokenryoKojogaku.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSongaihokenryoKojogaku.Enter += new System.EventHandler(this.txtKingaku_Enter);
            this.txtSongaihokenryoKojogaku.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingaku_Validating);
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.Silver;
            this.label8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label8.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label8.Location = new System.Drawing.Point(30, 73);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(126, 23);
            this.label8.TabIndex = 920;
            this.label8.Text = "地震保険料";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtSeimeihokenryoKojogaku
            // 
            this.txtSeimeihokenryoKojogaku.AcceptsReturn = true;
            this.txtSeimeihokenryoKojogaku.AutoSizeFromLength = false;
            this.txtSeimeihokenryoKojogaku.DisplayLength = null;
            this.txtSeimeihokenryoKojogaku.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtSeimeihokenryoKojogaku.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtSeimeihokenryoKojogaku.Location = new System.Drawing.Point(156, 50);
            this.txtSeimeihokenryoKojogaku.MaxLength = 9;
            this.txtSeimeihokenryoKojogaku.Name = "txtSeimeihokenryoKojogaku";
            this.txtSeimeihokenryoKojogaku.Size = new System.Drawing.Size(98, 23);
            this.txtSeimeihokenryoKojogaku.TabIndex = 2;
            this.txtSeimeihokenryoKojogaku.Text = "0";
            this.txtSeimeihokenryoKojogaku.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSeimeihokenryoKojogaku.Enter += new System.EventHandler(this.txtKingaku_Enter);
            this.txtSeimeihokenryoKojogaku.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingaku_Validating);
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.Silver;
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label6.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label6.Location = new System.Drawing.Point(30, 50);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(126, 23);
            this.label6.TabIndex = 918;
            this.label6.Text = "生命保険料";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtSkbKigyoKyosaiKakekinKjgk
            // 
            this.txtSkbKigyoKyosaiKakekinKjgk.AcceptsReturn = true;
            this.txtSkbKigyoKyosaiKakekinKjgk.AutoSizeFromLength = false;
            this.txtSkbKigyoKyosaiKakekinKjgk.DisplayLength = null;
            this.txtSkbKigyoKyosaiKakekinKjgk.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtSkbKigyoKyosaiKakekinKjgk.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtSkbKigyoKyosaiKakekinKjgk.Location = new System.Drawing.Point(156, 27);
            this.txtSkbKigyoKyosaiKakekinKjgk.MaxLength = 9;
            this.txtSkbKigyoKyosaiKakekinKjgk.Name = "txtSkbKigyoKyosaiKakekinKjgk";
            this.txtSkbKigyoKyosaiKakekinKjgk.Size = new System.Drawing.Size(98, 23);
            this.txtSkbKigyoKyosaiKakekinKjgk.TabIndex = 1;
            this.txtSkbKigyoKyosaiKakekinKjgk.Text = "0";
            this.txtSkbKigyoKyosaiKakekinKjgk.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSkbKigyoKyosaiKakekinKjgk.Enter += new System.EventHandler(this.txtKingaku_Enter);
            this.txtSkbKigyoKyosaiKakekinKjgk.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingaku_Validating);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Gray;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(278, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(24, 184);
            this.label1.TabIndex = 916;
            this.label1.Text = "支払金額等";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.Gray;
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label7.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(6, 3);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(24, 116);
            this.label7.TabIndex = 915;
            this.label7.Text = "所得控除額";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtHaigushaGokeiShotoku
            // 
            this.txtHaigushaGokeiShotoku.AcceptsReturn = true;
            this.txtHaigushaGokeiShotoku.AutoSizeFromLength = false;
            this.txtHaigushaGokeiShotoku.DisplayLength = null;
            this.txtHaigushaGokeiShotoku.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtHaigushaGokeiShotoku.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtHaigushaGokeiShotoku.Location = new System.Drawing.Point(428, 3);
            this.txtHaigushaGokeiShotoku.MaxLength = 9;
            this.txtHaigushaGokeiShotoku.Name = "txtHaigushaGokeiShotoku";
            this.txtHaigushaGokeiShotoku.Size = new System.Drawing.Size(98, 23);
            this.txtHaigushaGokeiShotoku.TabIndex = 16;
            this.txtHaigushaGokeiShotoku.Text = "0";
            this.txtHaigushaGokeiShotoku.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtHaigushaGokeiShotoku.Enter += new System.EventHandler(this.txtKingaku_Enter);
            this.txtHaigushaGokeiShotoku.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingaku_Validating);
            // 
            // txtShakaiHokenryoKjgkSnkkbn
            // 
            this.txtShakaiHokenryoKjgkSnkkbn.AcceptsReturn = true;
            this.txtShakaiHokenryoKjgkSnkkbn.AutoSizeFromLength = false;
            this.txtShakaiHokenryoKjgkSnkkbn.DisplayLength = null;
            this.txtShakaiHokenryoKjgkSnkkbn.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShakaiHokenryoKjgkSnkkbn.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtShakaiHokenryoKjgkSnkkbn.Location = new System.Drawing.Point(156, 3);
            this.txtShakaiHokenryoKjgkSnkkbn.MaxLength = 9;
            this.txtShakaiHokenryoKjgkSnkkbn.Name = "txtShakaiHokenryoKjgkSnkkbn";
            this.txtShakaiHokenryoKjgkSnkkbn.Size = new System.Drawing.Size(98, 23);
            this.txtShakaiHokenryoKjgkSnkkbn.TabIndex = 0;
            this.txtShakaiHokenryoKjgkSnkkbn.Text = "0";
            this.txtShakaiHokenryoKjgkSnkkbn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShakaiHokenryoKjgkSnkkbn.Enter += new System.EventHandler(this.txtKingaku_Enter);
            this.txtShakaiHokenryoKjgkSnkkbn.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingaku_Validating);
            // 
            // label27
            // 
            this.label27.BackColor = System.Drawing.Color.Silver;
            this.label27.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label27.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label27.Location = new System.Drawing.Point(302, 3);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(126, 23);
            this.label27.TabIndex = 911;
            this.label27.Text = "配偶者合計所得";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnlTaishokuNengappi1
            // 
            this.pnlTaishokuNengappi1.BackColor = System.Drawing.Color.Silver;
            this.pnlTaishokuNengappi1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlTaishokuNengappi1.Controls.Add(this.lblDayTaishokuNengappi1);
            this.pnlTaishokuNengappi1.Controls.Add(this.txtDayIjuKaishiDate1);
            this.pnlTaishokuNengappi1.Controls.Add(this.lblMonthTaishokuNengappi1);
            this.pnlTaishokuNengappi1.Controls.Add(this.lblGengoYearTaishokuNengappi1);
            this.pnlTaishokuNengappi1.Controls.Add(this.txtMonthIjuKaishiDate1);
            this.pnlTaishokuNengappi1.Controls.Add(this.lblGengoIjuKaishiDate1);
            this.pnlTaishokuNengappi1.Controls.Add(this.txtGengoYearIjuKaishiDate1);
            this.pnlTaishokuNengappi1.Location = new System.Drawing.Point(28, 160);
            this.pnlTaishokuNengappi1.Name = "pnlTaishokuNengappi1";
            this.pnlTaishokuNengappi1.Size = new System.Drawing.Size(225, 25);
            this.pnlTaishokuNengappi1.TabIndex = 6;
            // 
            // lblDayTaishokuNengappi1
            // 
            this.lblDayTaishokuNengappi1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDayTaishokuNengappi1.Location = new System.Drawing.Point(164, 0);
            this.lblDayTaishokuNengappi1.Name = "lblDayTaishokuNengappi1";
            this.lblDayTaishokuNengappi1.Size = new System.Drawing.Size(21, 23);
            this.lblDayTaishokuNengappi1.TabIndex = 14;
            this.lblDayTaishokuNengappi1.Text = "日";
            this.lblDayTaishokuNengappi1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtDayIjuKaishiDate1
            // 
            this.txtDayIjuKaishiDate1.AutoSizeFromLength = false;
            this.txtDayIjuKaishiDate1.DisplayLength = null;
            this.txtDayIjuKaishiDate1.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDayIjuKaishiDate1.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtDayIjuKaishiDate1.Location = new System.Drawing.Point(139, 0);
            this.txtDayIjuKaishiDate1.MaxLength = 2;
            this.txtDayIjuKaishiDate1.Name = "txtDayIjuKaishiDate1";
            this.txtDayIjuKaishiDate1.Size = new System.Drawing.Size(25, 23);
            this.txtDayIjuKaishiDate1.TabIndex = 8;
            this.txtDayIjuKaishiDate1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDayIjuKaishiDate1.Validating += new System.ComponentModel.CancelEventHandler(this.txtDayIjuKaishiDate_Validating);
            // 
            // lblMonthTaishokuNengappi1
            // 
            this.lblMonthTaishokuNengappi1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMonthTaishokuNengappi1.Location = new System.Drawing.Point(118, 0);
            this.lblMonthTaishokuNengappi1.Name = "lblMonthTaishokuNengappi1";
            this.lblMonthTaishokuNengappi1.Size = new System.Drawing.Size(21, 23);
            this.lblMonthTaishokuNengappi1.TabIndex = 12;
            this.lblMonthTaishokuNengappi1.Text = "月";
            this.lblMonthTaishokuNengappi1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblGengoYearTaishokuNengappi1
            // 
            this.lblGengoYearTaishokuNengappi1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGengoYearTaishokuNengappi1.Location = new System.Drawing.Point(72, 0);
            this.lblGengoYearTaishokuNengappi1.Name = "lblGengoYearTaishokuNengappi1";
            this.lblGengoYearTaishokuNengappi1.Size = new System.Drawing.Size(21, 23);
            this.lblGengoYearTaishokuNengappi1.TabIndex = 10;
            this.lblGengoYearTaishokuNengappi1.Text = "年";
            this.lblGengoYearTaishokuNengappi1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtMonthIjuKaishiDate1
            // 
            this.txtMonthIjuKaishiDate1.AutoSizeFromLength = false;
            this.txtMonthIjuKaishiDate1.DisplayLength = null;
            this.txtMonthIjuKaishiDate1.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMonthIjuKaishiDate1.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtMonthIjuKaishiDate1.Location = new System.Drawing.Point(93, 0);
            this.txtMonthIjuKaishiDate1.MaxLength = 2;
            this.txtMonthIjuKaishiDate1.Name = "txtMonthIjuKaishiDate1";
            this.txtMonthIjuKaishiDate1.Size = new System.Drawing.Size(25, 23);
            this.txtMonthIjuKaishiDate1.TabIndex = 7;
            this.txtMonthIjuKaishiDate1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonthIjuKaishiDate1.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthIjuKaishiDate_Validating);
            // 
            // lblGengoIjuKaishiDate1
            // 
            this.lblGengoIjuKaishiDate1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGengoIjuKaishiDate1.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGengoIjuKaishiDate1.Location = new System.Drawing.Point(0, 0);
            this.lblGengoIjuKaishiDate1.Name = "lblGengoIjuKaishiDate1";
            this.lblGengoIjuKaishiDate1.Size = new System.Drawing.Size(47, 23);
            this.lblGengoIjuKaishiDate1.TabIndex = 5;
            this.lblGengoIjuKaishiDate1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtGengoYearIjuKaishiDate1
            // 
            this.txtGengoYearIjuKaishiDate1.AutoSizeFromLength = false;
            this.txtGengoYearIjuKaishiDate1.DisplayLength = null;
            this.txtGengoYearIjuKaishiDate1.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGengoYearIjuKaishiDate1.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtGengoYearIjuKaishiDate1.Location = new System.Drawing.Point(47, 0);
            this.txtGengoYearIjuKaishiDate1.MaxLength = 2;
            this.txtGengoYearIjuKaishiDate1.Name = "txtGengoYearIjuKaishiDate1";
            this.txtGengoYearIjuKaishiDate1.Size = new System.Drawing.Size(25, 23);
            this.txtGengoYearIjuKaishiDate1.TabIndex = 6;
            this.txtGengoYearIjuKaishiDate1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGengoYearIjuKaishiDate1.Validating += new System.ComponentModel.CancelEventHandler(this.txtGengoYearIjuKaishiDate_Validating);
            // 
            // lblTaishokuNengappi1
            // 
            this.lblTaishokuNengappi1.BackColor = System.Drawing.Color.Transparent;
            this.lblTaishokuNengappi1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTaishokuNengappi1.Location = new System.Drawing.Point(30, 139);
            this.lblTaishokuNengappi1.Name = "lblTaishokuNengappi1";
            this.lblTaishokuNengappi1.Size = new System.Drawing.Size(224, 25);
            this.lblTaishokuNengappi1.TabIndex = 2;
            this.lblTaishokuNengappi1.Text = "居住開始年月日(1回目)";
            this.lblTaishokuNengappi1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label19
            // 
            this.label19.BackColor = System.Drawing.Color.Silver;
            this.label19.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label19.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label19.Location = new System.Drawing.Point(30, 27);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(126, 23);
            this.label19.TabIndex = 16;
            this.label19.Text = "小規模企業共済掛金";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label24
            // 
            this.label24.BackColor = System.Drawing.Color.Silver;
            this.label24.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label24.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label24.Location = new System.Drawing.Point(30, 4);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(126, 23);
            this.label24.TabIndex = 0;
            this.label24.Text = "社会保険料申告分";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblZenTaishokuNengappi
            // 
            this.lblZenTaishokuNengappi.BackColor = System.Drawing.Color.Transparent;
            this.lblZenTaishokuNengappi.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblZenTaishokuNengappi.Location = new System.Drawing.Point(301, 254);
            this.lblZenTaishokuNengappi.Name = "lblZenTaishokuNengappi";
            this.lblZenTaishokuNengappi.Size = new System.Drawing.Size(224, 25);
            this.lblZenTaishokuNengappi.TabIndex = 0;
            this.lblZenTaishokuNengappi.Text = "前職退職年月日";
            this.lblZenTaishokuNengappi.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTaishokuNengappi2
            // 
            this.lblTaishokuNengappi2.BackColor = System.Drawing.Color.Transparent;
            this.lblTaishokuNengappi2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTaishokuNengappi2.Location = new System.Drawing.Point(30, 181);
            this.lblTaishokuNengappi2.Name = "lblTaishokuNengappi2";
            this.lblTaishokuNengappi2.Size = new System.Drawing.Size(224, 25);
            this.lblTaishokuNengappi2.TabIndex = 15;
            this.lblTaishokuNengappi2.Text = "居住開始年月日(2回目)";
            this.lblTaishokuNengappi2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblNyuryokuF
            // 
            this.lblNyuryokuF.BackColor = System.Drawing.Color.Silver;
            this.lblNyuryokuF.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblNyuryokuF.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblNyuryokuF.Location = new System.Drawing.Point(684, 42);
            this.lblNyuryokuF.Name = "lblNyuryokuF";
            this.lblNyuryokuF.Size = new System.Drawing.Size(41, 23);
            this.lblNyuryokuF.TabIndex = 911;
            this.lblNyuryokuF.Text = "済";
            this.lblNyuryokuF.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblNenmatsuChosei
            // 
            this.lblNenmatsuChosei.BackColor = System.Drawing.Color.Silver;
            this.lblNenmatsuChosei.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblNenmatsuChosei.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblNenmatsuChosei.Location = new System.Drawing.Point(642, 42);
            this.lblNenmatsuChosei.Name = "lblNenmatsuChosei";
            this.lblNenmatsuChosei.Size = new System.Drawing.Size(42, 23);
            this.lblNenmatsuChosei.TabIndex = 910;
            this.lblNenmatsuChosei.Text = "入力";
            this.lblNenmatsuChosei.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKeisanF
            // 
            this.lblKeisanF.BackColor = System.Drawing.Color.Silver;
            this.lblKeisanF.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKeisanF.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKeisanF.Location = new System.Drawing.Point(777, 42);
            this.lblKeisanF.Name = "lblKeisanF";
            this.lblKeisanF.Size = new System.Drawing.Size(41, 23);
            this.lblKeisanF.TabIndex = 913;
            this.lblKeisanF.Text = "済";
            this.lblKeisanF.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label33
            // 
            this.label33.BackColor = System.Drawing.Color.Silver;
            this.label33.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label33.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label33.Location = new System.Drawing.Point(735, 42);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(42, 23);
            this.label33.TabIndex = 912;
            this.label33.Text = "計算";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnl2
            // 
            this.pnl2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.pnl2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnl2.Controls.Add(this.txtChoseiShoyoShotokuzeigaku);
            this.pnl2.Controls.Add(this.label45);
            this.pnl2.Controls.Add(this.txtChoseiShoyoShakaiHokenryo);
            this.pnl2.Controls.Add(this.label46);
            this.pnl2.Controls.Add(this.txtChoseiKyuyoShotokuzeigaku);
            this.pnl2.Controls.Add(this.label49);
            this.pnl2.Controls.Add(this.txtChoseiKyuyoShakaiHokenryo);
            this.pnl2.Controls.Add(this.label50);
            this.pnl2.Controls.Add(this.label51);
            this.pnl2.Controls.Add(this.txtChoseiShoyoKazeiKyuyogaku);
            this.pnl2.Controls.Add(this.txtChoseiKyuyoKazeiKyuyogaku);
            this.pnl2.Controls.Add(this.label52);
            this.pnl2.Controls.Add(this.label58);
            this.pnl2.Controls.Add(this.label59);
            this.pnl2.Location = new System.Drawing.Point(15, 413);
            this.pnl2.Name = "pnl2";
            this.pnl2.Size = new System.Drawing.Size(540, 85);
            this.pnl2.TabIndex = 3;
            // 
            // txtChoseiShoyoShotokuzeigaku
            // 
            this.txtChoseiShoyoShotokuzeigaku.AcceptsReturn = true;
            this.txtChoseiShoyoShotokuzeigaku.AutoSizeFromLength = false;
            this.txtChoseiShoyoShotokuzeigaku.DisplayLength = null;
            this.txtChoseiShoyoShotokuzeigaku.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtChoseiShoyoShotokuzeigaku.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtChoseiShoyoShotokuzeigaku.Location = new System.Drawing.Point(428, 52);
            this.txtChoseiShoyoShotokuzeigaku.MaxLength = 9;
            this.txtChoseiShoyoShotokuzeigaku.Name = "txtChoseiShoyoShotokuzeigaku";
            this.txtChoseiShoyoShotokuzeigaku.Size = new System.Drawing.Size(98, 23);
            this.txtChoseiShoyoShotokuzeigaku.TabIndex = 5;
            this.txtChoseiShoyoShotokuzeigaku.Text = "0";
            this.txtChoseiShoyoShotokuzeigaku.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtChoseiShoyoShotokuzeigaku.Enter += new System.EventHandler(this.txtKingaku_Enter);
            this.txtChoseiShoyoShotokuzeigaku.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingaku_Validating);
            // 
            // label45
            // 
            this.label45.BackColor = System.Drawing.Color.Silver;
            this.label45.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label45.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label45.Location = new System.Drawing.Point(302, 52);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(126, 23);
            this.label45.TabIndex = 926;
            this.label45.Text = "所得税額";
            this.label45.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtChoseiShoyoShakaiHokenryo
            // 
            this.txtChoseiShoyoShakaiHokenryo.AcceptsReturn = true;
            this.txtChoseiShoyoShakaiHokenryo.AutoSizeFromLength = false;
            this.txtChoseiShoyoShakaiHokenryo.DisplayLength = null;
            this.txtChoseiShoyoShakaiHokenryo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtChoseiShoyoShakaiHokenryo.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtChoseiShoyoShakaiHokenryo.Location = new System.Drawing.Point(428, 29);
            this.txtChoseiShoyoShakaiHokenryo.MaxLength = 9;
            this.txtChoseiShoyoShakaiHokenryo.Name = "txtChoseiShoyoShakaiHokenryo";
            this.txtChoseiShoyoShakaiHokenryo.Size = new System.Drawing.Size(98, 23);
            this.txtChoseiShoyoShakaiHokenryo.TabIndex = 4;
            this.txtChoseiShoyoShakaiHokenryo.Text = "0";
            this.txtChoseiShoyoShakaiHokenryo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtChoseiShoyoShakaiHokenryo.Enter += new System.EventHandler(this.txtKingaku_Enter);
            this.txtChoseiShoyoShakaiHokenryo.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingaku_Validating);
            // 
            // label46
            // 
            this.label46.BackColor = System.Drawing.Color.Silver;
            this.label46.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label46.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label46.Location = new System.Drawing.Point(302, 29);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(126, 23);
            this.label46.TabIndex = 924;
            this.label46.Text = "社会保険料";
            this.label46.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtChoseiKyuyoShotokuzeigaku
            // 
            this.txtChoseiKyuyoShotokuzeigaku.AcceptsReturn = true;
            this.txtChoseiKyuyoShotokuzeigaku.AutoSizeFromLength = false;
            this.txtChoseiKyuyoShotokuzeigaku.DisplayLength = null;
            this.txtChoseiKyuyoShotokuzeigaku.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtChoseiKyuyoShotokuzeigaku.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtChoseiKyuyoShotokuzeigaku.Location = new System.Drawing.Point(158, 53);
            this.txtChoseiKyuyoShotokuzeigaku.MaxLength = 9;
            this.txtChoseiKyuyoShotokuzeigaku.Name = "txtChoseiKyuyoShotokuzeigaku";
            this.txtChoseiKyuyoShotokuzeigaku.Size = new System.Drawing.Size(98, 23);
            this.txtChoseiKyuyoShotokuzeigaku.TabIndex = 2;
            this.txtChoseiKyuyoShotokuzeigaku.Text = "0";
            this.txtChoseiKyuyoShotokuzeigaku.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtChoseiKyuyoShotokuzeigaku.Enter += new System.EventHandler(this.txtKingaku_Enter);
            this.txtChoseiKyuyoShotokuzeigaku.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingaku_Validating);
            // 
            // label49
            // 
            this.label49.BackColor = System.Drawing.Color.Silver;
            this.label49.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label49.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label49.Location = new System.Drawing.Point(32, 53);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(126, 23);
            this.label49.TabIndex = 918;
            this.label49.Text = "所得税額";
            this.label49.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtChoseiKyuyoShakaiHokenryo
            // 
            this.txtChoseiKyuyoShakaiHokenryo.AcceptsReturn = true;
            this.txtChoseiKyuyoShakaiHokenryo.AutoSizeFromLength = false;
            this.txtChoseiKyuyoShakaiHokenryo.DisplayLength = null;
            this.txtChoseiKyuyoShakaiHokenryo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtChoseiKyuyoShakaiHokenryo.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtChoseiKyuyoShakaiHokenryo.Location = new System.Drawing.Point(158, 30);
            this.txtChoseiKyuyoShakaiHokenryo.MaxLength = 9;
            this.txtChoseiKyuyoShakaiHokenryo.Name = "txtChoseiKyuyoShakaiHokenryo";
            this.txtChoseiKyuyoShakaiHokenryo.Size = new System.Drawing.Size(98, 23);
            this.txtChoseiKyuyoShakaiHokenryo.TabIndex = 1;
            this.txtChoseiKyuyoShakaiHokenryo.Text = "0";
            this.txtChoseiKyuyoShakaiHokenryo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtChoseiKyuyoShakaiHokenryo.Enter += new System.EventHandler(this.txtKingaku_Enter);
            this.txtChoseiKyuyoShakaiHokenryo.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingaku_Validating);
            // 
            // label50
            // 
            this.label50.BackColor = System.Drawing.Color.Gray;
            this.label50.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label50.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label50.ForeColor = System.Drawing.Color.White;
            this.label50.Location = new System.Drawing.Point(278, 6);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(24, 69);
            this.label50.TabIndex = 916;
            this.label50.Text = "賞与調整額";
            this.label50.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label51
            // 
            this.label51.BackColor = System.Drawing.Color.Gray;
            this.label51.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label51.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label51.ForeColor = System.Drawing.Color.White;
            this.label51.Location = new System.Drawing.Point(8, 6);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(24, 70);
            this.label51.TabIndex = 915;
            this.label51.Text = "給与調整額";
            this.label51.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtChoseiShoyoKazeiKyuyogaku
            // 
            this.txtChoseiShoyoKazeiKyuyogaku.AcceptsReturn = true;
            this.txtChoseiShoyoKazeiKyuyogaku.AutoSizeFromLength = false;
            this.txtChoseiShoyoKazeiKyuyogaku.DisplayLength = null;
            this.txtChoseiShoyoKazeiKyuyogaku.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtChoseiShoyoKazeiKyuyogaku.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtChoseiShoyoKazeiKyuyogaku.Location = new System.Drawing.Point(428, 6);
            this.txtChoseiShoyoKazeiKyuyogaku.MaxLength = 9;
            this.txtChoseiShoyoKazeiKyuyogaku.Name = "txtChoseiShoyoKazeiKyuyogaku";
            this.txtChoseiShoyoKazeiKyuyogaku.Size = new System.Drawing.Size(98, 23);
            this.txtChoseiShoyoKazeiKyuyogaku.TabIndex = 3;
            this.txtChoseiShoyoKazeiKyuyogaku.Text = "0";
            this.txtChoseiShoyoKazeiKyuyogaku.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtChoseiShoyoKazeiKyuyogaku.Enter += new System.EventHandler(this.txtKingaku_Enter);
            this.txtChoseiShoyoKazeiKyuyogaku.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingaku_Validating);
            // 
            // txtChoseiKyuyoKazeiKyuyogaku
            // 
            this.txtChoseiKyuyoKazeiKyuyogaku.AcceptsReturn = true;
            this.txtChoseiKyuyoKazeiKyuyogaku.AutoSizeFromLength = false;
            this.txtChoseiKyuyoKazeiKyuyogaku.DisplayLength = null;
            this.txtChoseiKyuyoKazeiKyuyogaku.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtChoseiKyuyoKazeiKyuyogaku.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtChoseiKyuyoKazeiKyuyogaku.Location = new System.Drawing.Point(158, 6);
            this.txtChoseiKyuyoKazeiKyuyogaku.MaxLength = 9;
            this.txtChoseiKyuyoKazeiKyuyogaku.Name = "txtChoseiKyuyoKazeiKyuyogaku";
            this.txtChoseiKyuyoKazeiKyuyogaku.Size = new System.Drawing.Size(98, 23);
            this.txtChoseiKyuyoKazeiKyuyogaku.TabIndex = 0;
            this.txtChoseiKyuyoKazeiKyuyogaku.Text = "0";
            this.txtChoseiKyuyoKazeiKyuyogaku.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtChoseiKyuyoKazeiKyuyogaku.Enter += new System.EventHandler(this.txtKingaku_Enter);
            this.txtChoseiKyuyoKazeiKyuyogaku.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingaku_Validating);
            // 
            // label52
            // 
            this.label52.BackColor = System.Drawing.Color.Silver;
            this.label52.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label52.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label52.Location = new System.Drawing.Point(302, 6);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(126, 23);
            this.label52.TabIndex = 911;
            this.label52.Text = "課税分給与額";
            this.label52.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label58
            // 
            this.label58.BackColor = System.Drawing.Color.Silver;
            this.label58.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label58.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label58.Location = new System.Drawing.Point(32, 30);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(126, 23);
            this.label58.TabIndex = 16;
            this.label58.Text = "社会保険料";
            this.label58.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label59
            // 
            this.label59.BackColor = System.Drawing.Color.Silver;
            this.label59.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label59.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label59.Location = new System.Drawing.Point(32, 7);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(126, 23);
            this.label59.TabIndex = 0;
            this.label59.Text = "課税分給与額";
            this.label59.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label34
            // 
            this.label34.BackColor = System.Drawing.Color.Transparent;
            this.label34.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label34.ForeColor = System.Drawing.Color.Red;
            this.label34.Location = new System.Drawing.Point(12, 396);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(531, 20);
            this.label34.TabIndex = 916;
            this.label34.Text = "※課税分給与額には社会保険料控除前の金額を入力します。";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.txtTekiyo2);
            this.panel2.Controls.Add(this.txtTekiyo1);
            this.panel2.Controls.Add(this.lblTekiyo2);
            this.panel2.Controls.Add(this.lblTekiyo1);
            this.panel2.Controls.Add(this.label38);
            this.panel2.Location = new System.Drawing.Point(15, 499);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(540, 61);
            this.panel2.TabIndex = 5;
            // 
            // txtTekiyo2
            // 
            this.txtTekiyo2.AutoSizeFromLength = false;
            this.txtTekiyo2.DisplayLength = null;
            this.txtTekiyo2.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTekiyo2.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtTekiyo2.Location = new System.Drawing.Point(116, 28);
            this.txtTekiyo2.MaxLength = 50;
            this.txtTekiyo2.Name = "txtTekiyo2";
            this.txtTekiyo2.Size = new System.Drawing.Size(408, 23);
            this.txtTekiyo2.TabIndex = 1;
            this.txtTekiyo2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LastControl_KeyDown);
            this.txtTekiyo2.Validating += new System.ComponentModel.CancelEventHandler(this.txtTekiyo2_Validating);
            // 
            // txtTekiyo1
            // 
            this.txtTekiyo1.AutoSizeFromLength = false;
            this.txtTekiyo1.DisplayLength = null;
            this.txtTekiyo1.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTekiyo1.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtTekiyo1.Location = new System.Drawing.Point(116, 5);
            this.txtTekiyo1.MaxLength = 50;
            this.txtTekiyo1.Name = "txtTekiyo1";
            this.txtTekiyo1.Size = new System.Drawing.Size(408, 23);
            this.txtTekiyo1.TabIndex = 0;
            this.txtTekiyo1.Validating += new System.ComponentModel.CancelEventHandler(this.txtTekiyo1_Validating);
            // 
            // lblTekiyo2
            // 
            this.lblTekiyo2.BackColor = System.Drawing.Color.Silver;
            this.lblTekiyo2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTekiyo2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTekiyo2.Location = new System.Drawing.Point(59, 28);
            this.lblTekiyo2.Name = "lblTekiyo2";
            this.lblTekiyo2.Size = new System.Drawing.Size(57, 23);
            this.lblTekiyo2.TabIndex = 919;
            this.lblTekiyo2.Text = "摘要②";
            this.lblTekiyo2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTekiyo1
            // 
            this.lblTekiyo1.BackColor = System.Drawing.Color.Silver;
            this.lblTekiyo1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTekiyo1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTekiyo1.Location = new System.Drawing.Point(59, 5);
            this.lblTekiyo1.Name = "lblTekiyo1";
            this.lblTekiyo1.Size = new System.Drawing.Size(57, 23);
            this.lblTekiyo1.TabIndex = 918;
            this.lblTekiyo1.Text = "摘要①";
            this.lblTekiyo1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label38
            // 
            this.label38.BackColor = System.Drawing.Color.Gray;
            this.label38.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label38.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label38.ForeColor = System.Drawing.Color.White;
            this.label38.Location = new System.Drawing.Point(6, 5);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(51, 46);
            this.label38.TabIndex = 915;
            this.label38.Text = "源泉　徴収票";
            this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblKojoKubun1
            // 
            this.lblKojoKubun1.BackColor = System.Drawing.Color.Silver;
            this.lblKojoKubun1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKojoKubun1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKojoKubun1.Location = new System.Drawing.Point(47, 348);
            this.lblKojoKubun1.Name = "lblKojoKubun1";
            this.lblKojoKubun1.Size = new System.Drawing.Size(114, 23);
            this.lblKojoKubun1.TabIndex = 955;
            this.lblKojoKubun1.Text = "控除区分(1回目)";
            this.lblKojoKubun1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // KYNE1011
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 638);
            this.Controls.Add(this.lblKojoKubun1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.pnl2);
            this.Controls.Add(this.lblKeisanF);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.lblNyuryokuF);
            this.Controls.Add(this.pnl3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lblNenmatsuChosei);
            this.Controls.Add(this.lblShainNm);
            this.Controls.Add(this.pnl1);
            this.Controls.Add(this.txtShainCd);
            this.Controls.Add(this.lblShainCd);
            this.Controls.Add(this.label34);
            this.Name = "KYNE1011";
            this.Text = "年末調整入力";
            this.Controls.SetChildIndex(this.label34, 0);
            this.Controls.SetChildIndex(this.lblShainCd, 0);
            this.Controls.SetChildIndex(this.txtShainCd, 0);
            this.Controls.SetChildIndex(this.pnl1, 0);
            this.Controls.SetChildIndex(this.lblShainNm, 0);
            this.Controls.SetChildIndex(this.lblNenmatsuChosei, 0);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.pnl3, 0);
            this.Controls.SetChildIndex(this.lblNyuryokuF, 0);
            this.Controls.SetChildIndex(this.label33, 0);
            this.Controls.SetChildIndex(this.lblKeisanF, 0);
            this.Controls.SetChildIndex(this.pnl2, 0);
            this.Controls.SetChildIndex(this.panel2, 0);
            this.Controls.SetChildIndex(this.lblKojoKubun1, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.pnlDebug.ResumeLayout(false);
            this.pnl3.ResumeLayout(false);
            this.pnl3.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.pnl1.ResumeLayout(false);
            this.pnl1.PerformLayout();
            this.pnlTaishokuNengappi2.ResumeLayout(false);
            this.pnlTaishokuNengappi2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.pnlTaishokuNengappi1.ResumeLayout(false);
            this.pnlTaishokuNengappi1.PerformLayout();
            this.pnl2.ResumeLayout(false);
            this.pnl2.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private jp.co.fsi.common.controls.FsiTextBox txtShainCd;
        private System.Windows.Forms.Label lblShainCd;
        private System.Windows.Forms.Label lblShainNm;
        private jp.co.fsi.common.FsiPanel pnl3;
        private common.controls.FsiTextBox txtShogaishaTokubetsu;
        private System.Windows.Forms.Label lblShogaishaTokubetsu;
        private System.Windows.Forms.Label lblShogaisha;
        private common.controls.FsiTextBox txtSai16Miman;
        private System.Windows.Forms.Label lblSai16Miman;
        private System.Windows.Forms.Label lblHaigushaKubunNm;
        private common.controls.FsiTextBox txtHaigushaKubun;
        private System.Windows.Forms.Label lblHaigushaKubun;
        private System.Windows.Forms.Label lblGaikokujinNm;
        private common.controls.FsiTextBox txtGaikokujin;
        private System.Windows.Forms.Label lblGaikokujin;
        private System.Windows.Forms.Label lblSaigaishaNm;
        private common.controls.FsiTextBox txtSaigaisha;
        private System.Windows.Forms.Label lblSaigaisha;
        private System.Windows.Forms.Label lblShiboTaishokuNm;
        private common.controls.FsiTextBox txtShiboTaishoku;
        private System.Windows.Forms.Label lblShiboTaishoku;
        private System.Windows.Forms.Label lblMiseinenshaNm;
        private common.controls.FsiTextBox txtMiseinensha;
        private System.Windows.Forms.Label lblMiseinensha;
        private System.Windows.Forms.Label lblHonninKubun3Nm;
        private common.controls.FsiTextBox txtHonninKubun3;
        private System.Windows.Forms.Label lblHonninKubun3;
        private System.Windows.Forms.Label lblHonninKubun2Nm;
        private common.controls.FsiTextBox txtHonninKubun2;
        private System.Windows.Forms.Label lblHonninKubun2;
        private common.controls.FsiTextBox txtShogaishaIppan;
        private System.Windows.Forms.Label lblShogaishaIppan;
        private common.controls.FsiTextBox txtFuyoDokyoTokushoIppan;
        private System.Windows.Forms.Label lblFuyoDokyoTokushoIppan;
        private System.Windows.Forms.Label lblFuyoRojin;
        private System.Windows.Forms.Label lblZeigakuHyoNm;
        private common.controls.FsiTextBox txtFuyoRojinDokyoRoshinto;
        private System.Windows.Forms.Label lblFuyoRojinDokyoRoshinto;
        private common.controls.FsiTextBox txtFuyoRojinDokyoRoshintoIgai;
        private System.Windows.Forms.Label lblFuyoRojinDokyoRoshintoIgai;
        private common.controls.FsiTextBox txtFuyoTokutei;
        private System.Windows.Forms.Label lblFuyoTokutei;
        private common.controls.FsiTextBox txtFuyoIppan;
        private System.Windows.Forms.Label lblFuyoIppan;
        private System.Windows.Forms.Label lblHonninKubun1Nm;
        private common.controls.FsiTextBox txtHonninKubun1;
        private System.Windows.Forms.Label lblHonninKubun1;
        private common.controls.FsiTextBox txtZeigakuHyo;
        private System.Windows.Forms.Label lblZeigakuHyo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label Sai16MimanNm;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblGengoNendo;
        private jp.co.fsi.common.FsiPanel panel1;
        private common.controls.FsiTextBox txtGengoYearNendo;
        private jp.co.fsi.common.FsiPanel pnl1;
        private jp.co.fsi.common.FsiPanel pnlTaishokuNengappi1;
        private System.Windows.Forms.Label lblDayTaishokuNengappi1;
        private common.controls.FsiTextBox txtDayIjuKaishiDate1;
        private System.Windows.Forms.Label lblMonthTaishokuNengappi1;
        private System.Windows.Forms.Label lblGengoYearTaishokuNengappi1;
        private common.controls.FsiTextBox txtMonthIjuKaishiDate1;
        private System.Windows.Forms.Label lblGengoIjuKaishiDate1;
        private common.controls.FsiTextBox txtGengoYearIjuKaishiDate1;
        private System.Windows.Forms.Label lblTaishokuNengappi1;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label lblNyuryokuF;
        private System.Windows.Forms.Label lblNenmatsuChosei;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label9;
        private common.controls.FsiTextBox txtJutakuShutokutoTkbtKojogaku;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private common.controls.FsiTextBox txtZenshokubunShotokuzeigaku;
        private System.Windows.Forms.Label label13;
        private common.controls.FsiTextBox txtZenshokubunShakaiHokenryo;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private common.controls.FsiTextBox txtZenshokubunKazeiKyuyogaku;
        private System.Windows.Forms.Label label25;
        private common.controls.FsiTextBox txtKokuminNenkinHokenryoShrigk;
        private System.Windows.Forms.Label label29;
        private common.controls.FsiTextBox txtChokiSongaiHokenryoShrigk;
        private System.Windows.Forms.Label label28;
        private common.controls.FsiTextBox txtKojinNenkinHokenryoShrigk;
        private System.Windows.Forms.Label label23;
        private common.controls.FsiTextBox txtShinKojinNenkinHokenryo;
        private System.Windows.Forms.Label label22;
        private common.controls.FsiTextBox txtKaigoIryoHokenryo;
        private System.Windows.Forms.Label label21;
        private common.controls.FsiTextBox txtKyuSeimeiHokenryo;
        private System.Windows.Forms.Label label20;
        private common.controls.FsiTextBox txtShinSeimeiHokenryo;
        private System.Windows.Forms.Label label12;
        private common.controls.FsiTextBox txtHaigushaTokubetsuKojogaku;
        private System.Windows.Forms.Label label10;
        private common.controls.FsiTextBox txtSongaihokenryoKojogaku;
        private System.Windows.Forms.Label label8;
        private common.controls.FsiTextBox txtSeimeihokenryoKojogaku;
        private System.Windows.Forms.Label label6;
        private common.controls.FsiTextBox txtSkbKigyoKyosaiKakekinKjgk;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label7;
        private common.controls.FsiTextBox txtHaigushaGokeiShotoku;
        private common.controls.FsiTextBox txtShakaiHokenryoKjgkSnkkbn;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label lblKeisanF;
        private System.Windows.Forms.Label label33;
        private jp.co.fsi.common.FsiPanel pnl2;
        private common.controls.FsiTextBox txtChoseiShoyoShotokuzeigaku;
        private System.Windows.Forms.Label label45;
        private common.controls.FsiTextBox txtChoseiShoyoShakaiHokenryo;
        private System.Windows.Forms.Label label46;
        private common.controls.FsiTextBox txtChoseiKyuyoShotokuzeigaku;
        private System.Windows.Forms.Label label49;
        private common.controls.FsiTextBox txtChoseiKyuyoShakaiHokenryo;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
        private common.controls.FsiTextBox txtChoseiShoyoKazeiKyuyogaku;
        private common.controls.FsiTextBox txtChoseiKyuyoKazeiKyuyogaku;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label34;
        private jp.co.fsi.common.FsiPanel panel2;
        private common.controls.FsiTextBox txtTekiyo2;
        private common.controls.FsiTextBox txtTekiyo1;
        private System.Windows.Forms.Label lblTekiyo2;
        private System.Windows.Forms.Label lblTekiyo1;
        private System.Windows.Forms.Label label38;
        private jp.co.fsi.common.FsiPanel panel3;
        private System.Windows.Forms.Label label32;
        private common.controls.FsiTextBox txtDayZenTaishokuDate;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private common.controls.FsiTextBox txtMonthZenTaishokuDate;
        private System.Windows.Forms.Label lblGengoZenTaishokuDate;
        private common.controls.FsiTextBox txtGengoYearZenTaishokuDate;
        private System.Windows.Forms.Label lblZenTaishokuNengappi;
        private common.controls.FsiTextBox txtJutakuKariireToNenmatsuZan2;
        private System.Windows.Forms.Label lblNenZan2;
        private common.controls.FsiTextBox txtJutakuKariireToNenmatsuZan1;
        private System.Windows.Forms.Label lblNenZan1;
        private jp.co.fsi.common.FsiPanel pnlTaishokuNengappi2;
        private System.Windows.Forms.Label lblDayTaishokuNengappi2;
        private common.controls.FsiTextBox txtDayIjuKaishiDate2;
        private System.Windows.Forms.Label lblMonthTaishokuNengappi2;
        private System.Windows.Forms.Label lblGengoYearTaishokuNengappi2;
        private common.controls.FsiTextBox txtMonthIjuKaishiDate2;
        private System.Windows.Forms.Label lblGengoIjuKaishiDate2;
        private common.controls.FsiTextBox txtGengoYearIjuKaishiDate2;
        private System.Windows.Forms.Label lblTaishokuNengappi2;
        private System.Windows.Forms.Label lblKojoKubun1;
        private System.Windows.Forms.Label lblKojoKubun2;
        private common.controls.FsiTextBox txtJutakuKariireToTkbtKJKubun1;
        private common.controls.FsiTextBox txtJutakuKariireToTkbtKJKubun2;
        private common.controls.FsiTextBox txtJutakuKariireToTkbtKJSu;
        private System.Windows.Forms.Label lblJutakuKariireToTkbtKJSu;
    }
}