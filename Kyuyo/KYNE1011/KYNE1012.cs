﻿using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.ky.kyne1011
{
    /// <summary>
    /// 社員の検索(KYNE1012)
    /// </summary>
    public partial class KYNE1012 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// モード(コード検索)
        /// </summary>
        private const string MODE_CD_SRC = "1";
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KYNE1012()
        {
            InitializeComponent();
        }
        #endregion

        #region protectedメソッド(継承)

        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // サイズを縮める
            this.Size = new Size(740, 500);
            // EscapeとF1のみ表示
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF1.Location = this.btnF2.Location;
            this.btnF2.Visible = false;
            this.btnF3.Visible = false;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF6.Visible = false;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;
            this.ShowFButton = true;

            // リスト表示
            SearchData();

            // 編集中の社員コードが引き渡されている場合
            string shainCd = Util.ToString(((string[])this.InData)[1]);
            if (Util.ToInt(shainCd) > 0)
            {
                // 元々選択していたデータを選択
                for (int i = 1; i < this.dgvList.Rows.Count; i++)
                {
                    if (shainCd.Equals(Util.ToString(this.dgvList.Rows[i].Cells["社員コード"].Value)))
                    {
                        if (this.dgvList.Rows.Count > i + 1)
                        {
                            this.dgvList.Rows[i + 1].Selected = true;       // 次の社員を選択
                        }
                        else
                        {
                            this.dgvList.Rows[i].Selected = true;
                        }
                        break;
                    }
                }
            }
            // Gridにフォーカスをセット
            this.ActiveControl = this.dgvList;
            this.dgvList.Focus();
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // ダイアログとしての処理結果を返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        public override void PressF1()
        {
            // カナ名にフォーカスを戻す
            this.txtKanaName.Focus();
            this.txtKanaName.SelectAll();
        }

        #endregion

        #region イベント

        /// <summary>
        /// 社員コード検証時の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShainCd_Validating(object sender, CancelEventArgs e)
        {
            SearchData();
        }

        /// <summary>
        /// カナ名検証時の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKanaName_Validating(object sender, CancelEventArgs e)
        {
            // 入力された情報を元に検索する
            SearchData();
        }

        /// <summary>
        /// グリッドでのキーダウン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                ReturnVal();
            }
        }

        /// <summary>
        /// グリッドのセルダブルクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            ReturnVal();
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// データを検索する
        /// </summary>
        private void SearchData()
        {
            DateTime nendo = Util.ToDate(((string[])this.InData)[0]);

            // 社員情報・年末調整データを取得して表示
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
            dpc.SetParam("@NENDO", SqlDbType.DateTime, nendo);
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT A.SHAIN_CD AS 社員コード");
            sql.Append(",A.SHAIN_NM AS 社員名");
            sql.Append(",A.SHAIN_KANA_NM AS 社員カナ名");
            sql.Append(", (case when B.NYURYOKU_F = 1 then '済み' else '未' end) as 入力");
            sql.Append(", (case when B.KEISAN_F = 1 then '済み' else '未' end) as 計算");
            sql.Append(" FROM TB_KY_SHAIN_JOHO AS A");
            sql.Append(" LEFT OUTER JOIN TB_KY_NENMATSU_CHOSEI AS B");
            sql.Append(" ON A.KAISHA_CD = B.KAISHA_CD");
            sql.Append(" AND  A.SHAIN_CD = B.SHAIN_CD");
            sql.Append(" AND  B.NENDO = @NENDO");
            sql.Append(" WHERE A.KAISHA_CD = @KAISHA_CD");
            // 社員コード条件指定
            if (Util.ToDecimal(Util.ToString(this.txtShainCd.Text)) > 0)
            {
                dpc.SetParam("@SHAIN_CD", SqlDbType.Decimal, 6, this.txtShainCd.Text);
                sql.Append(" AND A.SHAIN_CD = @SHAIN_CD");
            }
            // カナ名条件指定
            if (!ValChk.IsEmpty(this.txtKanaName.Text))
            {
                dpc.SetParam("@SHAIN_KANA_NM", SqlDbType.VarChar, 30 + 2, this.txtKanaName.Text );
                sql.Append(" AND A.SHAIN_KANA_NM LIKE @SHAIN_KANA_NM");
            }
            sql.Append(" ORDER BY");
            sql.Append(" A.KAISHA_CD ASC");
            sql.Append(",A.SHAIN_CD ASC");
            DataTable dt = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);

            // 該当データがなければエラーメッセージを表示
            if (dt.Rows.Count == 0)
            {
                Msg.Info("該当データがありません。");
                dt.Rows.Add(dt.NewRow());
            }

            this.dgvList.DataSource = dt;

            // ユーザーによるソートを禁止させる
            foreach (DataGridViewColumn c in this.dgvList.Columns)
                c.SortMode = DataGridViewColumnSortMode.NotSortable;

            // フォントを設定する
            this.dgvList.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F, FontStyle.Bold);
            this.dgvList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.dgvList.DefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F);

            // 列幅を設定する
            this.dgvList.Columns[0].Width = 110;
            this.dgvList.Columns[1].Width = 200;
            this.dgvList.Columns[2].Width = 200;
            this.dgvList.Columns[3].Width = 80;
            this.dgvList.Columns[4].Width = 80;
        }

        /// <summary>
        /// 呼び出し元に戻り値を返す
        /// </summary>
        private void ReturnVal()
        {
            this.OutData = new string[3] { 
                Util.ToString(this.dgvList.SelectedRows[0].Cells["社員コード"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["社員名"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["社員カナ名"].Value)
            };
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        #endregion

    }
}
