﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Reflection;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;
using jp.co.fsi.common.controls;

namespace jp.co.fsi.ky.kyne1011
{
    /// <summary>
    /// 年末調整入力(KYNE1011)
    /// </summary>
    public partial class KYNE1011 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// モード(新規)
        /// </summary>
        private const string MODE_NEW = "1";

        /// <summary>
        /// モード(編集)
        /// </summary>
        private const string MODE_EDIT = "2";
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KYNE1011()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="par1">引数1</param>
        public KYNE1011(string par1)
            : base(par1)
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 編集データクリア
            DataClear();

            // 初期フォーカス
            this.txtShainCd.Focus();
            this.txtShainCd.SelectAll();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 元号年とコード項目に
            // フォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtGengoYearNendo":
                case "txtShainCd":
                case "txtHonninKubun1":
                case "txtHonninKubun2":
                case "txtHonninKubun3":
                case "txtHaigushaKubun":
                    this.btnF1.Enabled = true;
                    break;
                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }
       
        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        public override void PressF1()
        {
            Assembly asm;
            Type t;

            // 元号年とコード項目に
            // フォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtShainCd":
                    #region 社員検索
                    // 社員検索フォームをダイアログ表示
                    KYNE1012 subFrm = new KYNE1012();
                    string[] args = new string[2];
                    args[0] = Util.ToString(
                        Util.ConvAdDate(this.lblGengoNendo.Text, this.txtGengoYearNendo.Text, "1", "1", this.Dba));
                    args[1] = null;
                    subFrm.InData = args;
                    subFrm.ShowDialog(this);
                    if (subFrm.DialogResult == DialogResult.OK)
                    {
                        string[] result = (string[])subFrm.OutData;
                        this.txtShainCd.Text = result[0];

                        // 編集データ読込
                        DataLoad(this.txtShainCd.Text);
                    }
                    break;
                    #endregion
                case "txtGengoYearNendo":
                case "txtGengoYearIjuKaishiDate":
                case "txtGengoYearIjuKaishiDate1":
                case "txtGengoYearIjuKaishiDate2":
                case "txtGengoYearZenTaishokuDate":
                    #region 元号検索
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom("CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            if (this.ActiveCtlNm == "txtGengoYearNendo")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.InData = this.lblGengoNendo.Text;
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.lblGengoNendo.Text = result[1];

                                    // 和暦年未入力時の初期値
                                    if (Util.ToInt(Util.ToString(this.txtGengoYearNendo.Text)) == 0)
                                    {
                                        string[] nendo = Util.ConvJpDate(new DateTime(this.UInfo.KaikeiNendo, 4, 1), this.Dba);
                                        this.lblGengoNendo.Text = nendo[0];
                                        this.txtGengoYearNendo.Text = nendo[2];
                                    }

                                    // 和暦年補正
                                    DateTime adDate =
                                        Util.ConvAdDate(this.lblGengoNendo.Text, this.txtGengoYearNendo.Text, "4", "1", this.Dba);
                                    string[] jpDate = Util.ConvJpDate(adDate, this.Dba);
                                    this.lblGengoNendo.Text = jpDate[0];
                                    this.txtGengoYearNendo.Text = jpDate[2];

                                    // 社員コード入力済みなら編集データ再読込
                                    if (Util.ToInt(this.txtShainCd.Text) > 0) DataLoad(this.txtShainCd.Text);
                                }
                            }
                            else if (this.ActiveCtlNm == "txtGengoYearIjuKaishiDate1")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.InData = this.lblGengoIjuKaishiDate1.Text;
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.lblGengoIjuKaishiDate1.Text = result[1];

                                    // 和暦年未入力時の初期値
                                    if (ValChk.IsEmpty(this.txtGengoYearIjuKaishiDate1.Text))
                                    {
                                        string[] date = Util.ConvJpDate(DateTime.Today, this.Dba);
                                        this.txtGengoYearIjuKaishiDate1.Text = date[2];
                                    }

                                    FixDateControls(
                                        this.lblGengoIjuKaishiDate1,
                                        this.txtGengoYearIjuKaishiDate1,
                                        this.txtMonthIjuKaishiDate1,
                                        this.txtDayIjuKaishiDate1);
                                }
                            }
                            else if (this.ActiveCtlNm == "txtGengoYearIjuKaishiDate2")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.InData = this.lblGengoIjuKaishiDate2.Text;
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.lblGengoIjuKaishiDate2.Text = result[1];

                                    // 和暦年未入力時の初期値
                                    if (ValChk.IsEmpty(this.txtGengoYearIjuKaishiDate2.Text))
                                    {
                                        string[] date = Util.ConvJpDate(DateTime.Today, this.Dba);
                                        this.txtGengoYearIjuKaishiDate2.Text = date[2];
                                    }

                                    FixDateControls(
                                        this.lblGengoIjuKaishiDate2,
                                        this.txtGengoYearIjuKaishiDate2,
                                        this.txtMonthIjuKaishiDate2,
                                        this.txtDayIjuKaishiDate2);
                                }
                            }
                            else if (this.ActiveCtlNm == "txtGengoYearZenTaishokuDate")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.InData = this.lblGengoZenTaishokuDate.Text;
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.lblGengoZenTaishokuDate.Text = result[1];

                                    // 和暦年未入力時の初期値
                                    if (ValChk.IsEmpty(this.txtGengoYearZenTaishokuDate.Text))
                                    {
                                        string[] date = Util.ConvJpDate(DateTime.Today, this.Dba);
                                        this.txtGengoYearZenTaishokuDate.Text = date[2];
                                    }

                                    FixDateControls(
                                        this.lblGengoZenTaishokuDate,
                                        this.txtGengoYearZenTaishokuDate,
                                        this.txtMonthZenTaishokuDate,
                                        this.txtDayZenTaishokuDate);
                                }
                            }
                        }
                    }
                    #endregion
                    break;
                case "txtHonninKubun1":
                    #region 本人区分1検索
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom("CMCM1041.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1041.CMCM1041");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "TB_KY_F_HONNIN_KUBUN1";       // 起動パラメータ
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.txtHonninKubun1.Text = result[0];
                                this.lblHonninKubun1Nm.Text = result[1];
                            }
                        }
                    }
                    #endregion
                    break;
                case "txtHonninKubun2":
                    #region 本人区分2検索
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom("CMCM1041.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1041.CMCM1041");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "TB_KY_F_HONNIN_KUBUN2";       // 起動パラメータ
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.txtHonninKubun2.Text = result[0];
                                this.lblHonninKubun2Nm.Text = result[1];
                            }
                        }
                    }
                    #endregion
                    break;
                case "txtHonninKubun3":
                    #region 本人区分3検索
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom("CMCM1041.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1041.CMCM1041");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "TB_KY_F_HONNIN_KUBUN3";       // 起動パラメータ
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.txtHonninKubun3.Text = result[0];
                                this.lblHonninKubun3Nm.Text = result[1];
                            }
                        }
                    }
                    #endregion
                    break;
                case "txtHaigushaKubun":
                    #region 配偶者検索
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom("CMCM1041.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1041.CMCM1041");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "TB_KY_F_HAIGUSHA_KUBUN";       // 起動パラメータ
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.txtHaigushaKubun.Text = result[0];
                                this.lblHaigushaKubunNm.Text = result[1];
                            }
                        }
                    }
                    #endregion
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// F3キー押下時処理
        /// </summary>
        public override void PressF3()
        {
            if (this.btnF3.Enabled)
            {
                // 確認メッセージを表示
                string msg = "削除しますか？";
                if (Msg.ConfYesNo(msg) == DialogResult.No)
                {
                    // 「いいえ」を押されたら処理終了
                    return;
                }

                // 全項目を再度入力値チェック
                if (!ValidateAll())
                {
                    // エラーありの場合ここで処理終了
                    return;
                }

                // 削除用パラメータ
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
                dpc.SetParam("@SHAIN_CD", SqlDbType.Decimal, 6, this.txtShainCd.Text);
                dpc.SetParam("@NENDO", SqlDbType.DateTime,
                    Util.ConvAdDate(this.lblGengoNendo.Text, this.txtGengoYearNendo.Text, "1", "1", this.Dba));

                try
                {
                    // トランザクション開始
                    this.Dba.BeginTransaction();

                    // 年末調整データ削除
                    this.Dba.Delete("TB_KY_NENMATSU_CHOSEI", "KAISHA_CD = @KAISHA_CD AND SHAIN_CD = @SHAIN_CD AND NENDO = @NENDO", dpc);

                    // トランザクションをコミット
                    this.Dba.Commit();
                }
                finally
                {
                    // ロールバック
                    this.Dba.Rollback();
                }

                // 画面再読込
                DataLoad(this.txtShainCd.Text);
                this.txtShainCd.Focus();
            }
        }
        
        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        public override void PressF6()
        {
            // 確認メッセージを表示
            string msg = (MODE_NEW.Equals(this.Par1) ? "登録" : "更新") + "しますか？";
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            // 入力値をバインドパラメータとしてセットする
            ArrayList alParamsKyShainJoho = SetKyShainJohoParams();
            ArrayList alParamsKyNenmatsuChosei = SetKyNenmatsuChoseiParams();

            try
            {
                // トランザクション開始
                this.Dba.BeginTransaction();

                // 社員情報データ更新
                this.Dba.Update("TB_KY_SHAIN_JOHO",
                    (DbParamCollection)alParamsKyShainJoho[1],
                    "KAISHA_CD = @KAISHA_CD AND SHAIN_CD = @SHAIN_CD",
                    (DbParamCollection)alParamsKyShainJoho[0]);

                // 年末調整データ削除
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
                dpc.SetParam("@SHAIN_CD", SqlDbType.Decimal, 6, this.txtShainCd.Text);
                dpc.SetParam("@NENDO", SqlDbType.DateTime,
                    Util.ConvAdDate(this.lblGengoNendo.Text, this.txtGengoYearNendo.Text, "1", "1", this.Dba));
                this.Dba.Delete("TB_KY_NENMATSU_CHOSEI", "KAISHA_CD = @KAISHA_CD AND SHAIN_CD = @SHAIN_CD AND NENDO = @NENDO", dpc);

                // 年末調整データ登録
                this.Dba.Insert("TB_KY_NENMATSU_CHOSEI", (DbParamCollection)alParamsKyNenmatsuChosei[0]);

                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // 画面再読込後、社員検索フォームを表示
            DataLoad(this.txtShainCd.Text);
            KYNE1012 subFrm = new KYNE1012();
            string[] args = new string[2];
            args[0] = Util.ToString(
                Util.ConvAdDate(this.lblGengoNendo.Text, this.txtGengoYearNendo.Text, "1", "1", this.Dba));
            args[1] = this.txtShainCd.Text;
            subFrm.InData = args;
            subFrm.ShowDialog(this);
            if (subFrm.DialogResult == DialogResult.OK)
            {
                string[] result = (string[])subFrm.OutData;
                this.txtShainCd.Text = result[0];

                // 編集データ読込
                DataLoad(this.txtShainCd.Text);
            }
            this.txtShainCd.Focus();
        }

        #endregion

        #region イベント
        /// <summary>
        /// 年調処理年の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGengoYearKaikeiNendo_Validating(object sender, CancelEventArgs e)
        {
            string[] nendo;

            // 年度の取得
            if (Util.ToInt(Util.ToString(this.txtGengoYearNendo.Text)) == 0)
            {
                // 無効年入力の時
                nendo = Util.ConvJpDate(new DateTime(this.UInfo.KaikeiNendo, 1, 1), this.Dba);
            }
            else
            {
                DateTime adNendo = 
                    Util.ConvAdDate(this.lblGengoNendo.Text, this.txtGengoYearNendo.Text, "4", "1", this.Dba);
                nendo = Util.ConvJpDate(adNendo, this.Dba);
            }
            this.lblGengoNendo.Text = nendo[0];
            this.txtGengoYearNendo.Text = nendo[2];

            // 社員コード入力済みなら編集データ再読込
            if (Util.ToInt(this.txtShainCd.Text) > 0) DataLoad(this.txtShainCd.Text);
        }

        /// <summary>
        /// 社員コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShainCd_Validating(object sender, CancelEventArgs e)
        {
            // 入力値補正
            this.txtShainCd.Text = Util.ToString(Util.ToDecimal(this.txtShainCd.Text));

            if (!IsValidShainCd())
            {
                e.Cancel = true;
                this.txtShainCd.SelectAll();
                return;
            }

            // 編集データ読込
            DataLoad(this.txtShainCd.Text);
        }

        /// <summary>
        /// 居住開始日付(年)1回目値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGengoYearIjuKaishiDate_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtGengoYearIjuKaishiDate1.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtGengoYearIjuKaishiDate1.SelectAll();
                e.Cancel = true;
                return;
            }

            // 日付コントロール補正
            FixDateControls(
                this.lblGengoIjuKaishiDate1,
                this.txtGengoYearIjuKaishiDate1,
                this.txtMonthIjuKaishiDate1,
                this.txtDayIjuKaishiDate1);
        }

        /// <summary>
        /// 居住開始日付(年)2回目値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGengoYearIjuKaishiDate2_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtGengoYearIjuKaishiDate2.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtGengoYearIjuKaishiDate2.SelectAll();
                e.Cancel = true;
                return;
            }

            // 日付コントロール補正
            FixDateControls(
                this.lblGengoIjuKaishiDate2,
                this.txtGengoYearIjuKaishiDate2,
                this.txtMonthIjuKaishiDate2,
                this.txtDayIjuKaishiDate2);
        }

        /// <summary>
        /// 居住開始日付(月)1回目値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMonthIjuKaishiDate_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtMonthIjuKaishiDate1.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtMonthIjuKaishiDate1.SelectAll();
                e.Cancel = true;
                return;
            }

            // 日付コントロール補正
            FixDateControls(
                this.lblGengoIjuKaishiDate1,
                this.txtGengoYearIjuKaishiDate1,
                this.txtMonthIjuKaishiDate1,
                this.txtDayIjuKaishiDate1);
        }

        /// <summary>
        /// 居住開始日付(月)2回目値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMonthIjuKaishiDate2_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtMonthIjuKaishiDate2.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtMonthIjuKaishiDate2.SelectAll();
                e.Cancel = true;
                return;
            }

            // 日付コントロール補正
            FixDateControls(
                this.lblGengoIjuKaishiDate2,
                this.txtGengoYearIjuKaishiDate2,
                this.txtMonthIjuKaishiDate2,
                this.txtDayIjuKaishiDate2);
        }

        /// <summary>
        /// 居住開始日付(日)1回目値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDayIjuKaishiDate_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtDayIjuKaishiDate1.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtDayIjuKaishiDate1.SelectAll();
                e.Cancel = true;
                return;
            }

            // 日付コントロール補正
            FixDateControls(
                this.lblGengoIjuKaishiDate1,
                this.txtGengoYearIjuKaishiDate1,
                this.txtMonthIjuKaishiDate1,
                this.txtDayIjuKaishiDate1);
        }

        /// <summary>
        /// 居住開始日付(日)2回目値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDayIjuKaishiDate2_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtDayIjuKaishiDate2.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtDayIjuKaishiDate2.SelectAll();
                e.Cancel = true;
                return;
            }

            // 日付コントロール補正
            FixDateControls(
                this.lblGengoIjuKaishiDate2,
                this.txtGengoYearIjuKaishiDate2,
                this.txtMonthIjuKaishiDate2,
                this.txtDayIjuKaishiDate2);
        }


        /// <summary>
        /// 前職退職日付(年)値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGengoYearZenTaishokuDate_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtGengoYearZenTaishokuDate.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtGengoYearZenTaishokuDate.SelectAll();
                e.Cancel = true;
                return;
            }

            // 日付コントロール補正
            FixDateControls(
                this.lblGengoZenTaishokuDate,
                this.txtGengoYearZenTaishokuDate,
                this.txtMonthZenTaishokuDate,
                this.txtDayZenTaishokuDate);
        }

        /// <summary>
        /// 前職退職日付(月)値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMonthZenTaishokuDate_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtMonthZenTaishokuDate.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtMonthZenTaishokuDate.SelectAll();
                e.Cancel = true;
                return;
            }

            // 日付コントロール補正
            FixDateControls(
                this.lblGengoZenTaishokuDate,
                this.txtGengoYearZenTaishokuDate,
                this.txtMonthZenTaishokuDate,
                this.txtDayZenTaishokuDate);
        }

        /// <summary>
        /// 前職退職日付(日)値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDayZenTaishokuDate_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtDayZenTaishokuDate.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtDayZenTaishokuDate.SelectAll();
                e.Cancel = true;
                return;
            }

            // 日付コントロール補正
            FixDateControls(
                this.lblGengoZenTaishokuDate,
                this.txtGengoYearZenTaishokuDate,
                this.txtMonthZenTaishokuDate,
                this.txtDayZenTaishokuDate);
        }
        /// <summary>
        /// 税額表の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtZeigakuHyo_Validating(object sender, CancelEventArgs e)
        {
            if (ValChk.IsEmpty(this.txtZeigakuHyo.Text) || !ValChk.IsNumber(this.txtZeigakuHyo.Text))
            {
                this.txtZeigakuHyo.Text = "0";
            }
            else
            {
                // 定義外の値を補正
                this.txtZeigakuHyo.Text = FixDefinedCode(this.txtZeigakuHyo.Text, 2);

            }
        }

        /// <summary>
        /// 本人区分①の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtHonninKubun1_Validating(object sender, CancelEventArgs e)
        {
            // 入力値補正
            this.txtHonninKubun1.Text = Util.ToString(Util.ToDecimal(this.txtHonninKubun1.Text));

            // 数字のみの入力を許可・未入力はNG
            if (!IsValidHonninKubun1())
            {
                e.Cancel = true;
                this.txtHonninKubun1.SelectAll();
            }

            if (this.txtHonninKubun1.Modified || this.txtHonninKubun1.Text == "0")
            {
                // 名称セット
                this.lblHonninKubun1Nm.Text = 
                    this.Dba.GetKyuyoKbnNm("2", Util.ToString(this.txtHonninKubun1.Text));

                this.txtHonninKubun1.Modified = false;
            }
        }

        /// <summary>
        /// 本人区分②の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtHonninKubun2_Validating(object sender, CancelEventArgs e)
        {
            // 入力値補正
            this.txtHonninKubun2.Text = Util.ToString(Util.ToDecimal(this.txtHonninKubun2.Text));

            // 数字のみの入力を許可・未入力はNG
            if (!IsValidHonninKubun2())
            {
                e.Cancel = true;
                this.txtHonninKubun2.SelectAll();
            }

            if (this.txtHonninKubun2.Modified || this.txtHonninKubun2.Text == "0")
            {
                // 名称セット
                this.lblHonninKubun2Nm.Text = 
                    this.Dba.GetKyuyoKbnNm("3", Util.ToString(this.txtHonninKubun2.Text));

                this.txtHonninKubun2.Modified = false;
            }
        }

        /// <summary>
        /// 本人区分③の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtHonninKubun3_Validating(object sender, CancelEventArgs e)
        {
            // 入力値補正
            this.txtHonninKubun3.Text = Util.ToString(Util.ToDecimal(this.txtHonninKubun3.Text));

            // 数字のみの入力を許可・未入力はNG
            if (!IsValidHonninKubun3())
            {
                e.Cancel = true;
                this.txtHonninKubun3.SelectAll();
            }

            if (this.txtHonninKubun3.Modified || this.txtHonninKubun3.Text == "0")
            {
                // 名称セット
                this.lblHonninKubun3Nm.Text =
                    this.Dba.GetKyuyoKbnNm("4", Util.ToString(this.txtHonninKubun3.Text));

                this.txtHonninKubun3.Modified = false;
            }
        }

        /// <summary>
        /// 未成年者の別の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMiseinensha_Validating(object sender, CancelEventArgs e)
        {
            // 入力値補正
            this.txtMiseinensha.Text = Util.ToString(Util.ToDecimal(this.txtMiseinensha.Text));
            // 定義外の値を補正
            this.txtMiseinensha.Text = FixDefinedCode(this.txtMiseinensha.Text, 1);
        }

        /// <summary>
        /// 死亡退職の別の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShiboTaishoku_Validating(object sender, CancelEventArgs e)
        {
            // 入力値補正
            this.txtShiboTaishoku.Text = Util.ToString(Util.ToDecimal(this.txtShiboTaishoku.Text));
            // 定義外の値を補正
            this.txtShiboTaishoku.Text = FixDefinedCode(this.txtShiboTaishoku.Text, 1);
        }

        /// <summary>
        /// 災害者の別の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSaigaisha_Validating(object sender, CancelEventArgs e)
        {
            // 入力値補正
            this.txtSaigaisha.Text = Util.ToString(Util.ToDecimal(this.txtSaigaisha.Text));
            // 定義外の値を補正
            this.txtSaigaisha.Text = FixDefinedCode(this.txtSaigaisha.Text, 1);
        }

        /// <summary>
        /// 外国人の別の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGaikokujin_Validating(object sender, CancelEventArgs e)
        {
            // 入力値補正
            this.txtGaikokujin.Text = Util.ToString(Util.ToDecimal(this.txtGaikokujin.Text));
            // 定義外の値を補正
            this.txtGaikokujin.Text = FixDefinedCode(this.txtGaikokujin.Text, 1);
        }

        /// <summary>
        /// 配偶者区分の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtHaigushaKubun_Validating(object sender, CancelEventArgs e)
        {
            // 入力値補正
            this.txtHaigushaKubun.Text = Util.ToString(Util.ToDecimal(this.txtHaigushaKubun.Text));

            // 数字のみの入力を許可・未入力はNG
            if (!IsValidHaigushaKubun())
            {
                e.Cancel = true;
                this.txtHaigushaKubun.SelectAll();
            }

            if (this.txtHaigushaKubun.Modified || this.txtHaigushaKubun.Text == "0")
            {
                // 名称セット
                this.lblHaigushaKubunNm.Text =
                    this.Dba.GetKyuyoKbnNm("5", Util.ToString(this.txtHaigushaKubun.Text));

                this.txtHaigushaKubun.Modified = false;
            }
        }

        /// <summary>
        /// 控除対象扶養の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFuyoIppan_Validating(object sender, CancelEventArgs e)
        {
            // 入力値補正
            this.txtFuyoIppan.Text = Util.ToString(Util.ToDecimal(this.txtFuyoIppan.Text));

            // 人数値入力の検証
            if (!IsValidNumber(this.txtFuyoIppan))
            {
                e.Cancel = true;
                this.txtFuyoIppan.SelectAll();
            }
        }

        /// <summary>
        /// 特定扶養親族の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFuyoTokutei_Validating(object sender, CancelEventArgs e)
        {
            // 入力値補正
            this.txtFuyoTokutei.Text = Util.ToString(Util.ToDecimal(this.txtFuyoTokutei.Text));

            // 人数値入力の検証
            if (!IsValidNumber(this.txtFuyoTokutei))
            {
                e.Cancel = true;
                this.txtFuyoTokutei.SelectAll();
            }
        }

        /// <summary>
        /// 同居老親等以外の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFuyoRojinDokyoRoshintoIgai_Validating(object sender, CancelEventArgs e)
        {
            // 入力値補正
            this.txtFuyoRojinDokyoRoshintoIgai.Text = Util.ToString(Util.ToDecimal(this.txtFuyoRojinDokyoRoshintoIgai.Text));

            // 人数値入力の検証
            if (!IsValidNumber(this.txtFuyoRojinDokyoRoshintoIgai))
            {
                e.Cancel = true;
                this.txtFuyoRojinDokyoRoshintoIgai.SelectAll();
            }
        }

        /// <summary>
        /// 同居老親等の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFuyoRojinDokyoRoshinto_Validating(object sender, CancelEventArgs e)
        {
            // 入力値補正
            this.txtFuyoRojinDokyoRoshinto.Text = Util.ToString(Util.ToDecimal(this.txtFuyoRojinDokyoRoshinto.Text));

            // 人数値入力の検証
            if (!IsValidNumber(this.txtFuyoRojinDokyoRoshinto))
            {
                e.Cancel = true;
                this.txtFuyoRojinDokyoRoshinto.SelectAll();
            }
        }

        /// <summary>
        /// １６歳未満の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSai16Miman_Validating(object sender, CancelEventArgs e)
        {
            // 入力値補正
            this.txtSai16Miman.Text = Util.ToString(Util.ToDecimal(this.txtSai16Miman.Text));

            // 人数値入力の検証
            if (!IsValidNumber(this.txtSai16Miman))
            {
                e.Cancel = true;
                this.txtSai16Miman.SelectAll();
            }
        }

        /// <summary>
        /// 同居特別障害者の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFuyoDokyoTokushoIppan_Validating(object sender, CancelEventArgs e)
        {
            // 入力値補正
            this.txtFuyoDokyoTokushoIppan.Text = Util.ToString(Util.ToDecimal(this.txtFuyoDokyoTokushoIppan.Text));

            // 人数値入力の検証
            if (!IsValidNumber(this.txtFuyoDokyoTokushoIppan))
            {
                e.Cancel = true;
                this.txtFuyoDokyoTokushoIppan.SelectAll();
            }
        }

        /// <summary>
        /// 一般障害者の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShogaishaIppan_Validating(object sender, CancelEventArgs e)
        {
            // 入力値補正
            this.txtShogaishaIppan.Text = Util.ToString(Util.ToDecimal(this.txtShogaishaIppan.Text));

            // 人数値入力の検証
            if (!IsValidNumber(this.txtShogaishaIppan))
            {
                e.Cancel = true;
                this.txtShogaishaIppan.SelectAll();
            }
        }

        /// <summary>
        /// 特別障害者の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShogaishaTokubetsu_Validating(object sender, CancelEventArgs e)
        {
            // 入力値補正
            this.txtShogaishaTokubetsu.Text = Util.ToString(Util.ToDecimal(this.txtShogaishaTokubetsu.Text));

            // 人数値入力の検証
            if (!IsValidNumber(this.txtShogaishaTokubetsu))
            {
                e.Cancel = true;
                this.txtShogaishaTokubetsu.SelectAll();
            }
        }

        /// <summary>
        /// 住宅借入金等特別控除区分(1回目)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtJutakuKariireToTkbtKJKubun1_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidJutakuKariireToTkbtKJKubun1())
            {
                e.Cancel = true;
                this.txtJutakuKariireToTkbtKJKubun1.SelectAll();
            }
        }

        /// <summary>
        /// 住宅借入金等特別控除区分(2回目)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtJutakuKariireToTkbtKJKubun2_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidJutakuKariireToTkbtKJKubun2())
            {
                e.Cancel = true;
                this.txtJutakuKariireToTkbtKJKubun2.SelectAll();
            }
        }

        /// <summary>
        /// 住宅借入金等特別控除適用数の入力チェックの値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtJutakuKariireToTkbtKJSu_Validating(object sender, CancelEventArgs e)
        {
            // 入力値補正
            this.txtJutakuKariireToTkbtKJSu.Text = Util.ToString(Util.ToDecimal(this.txtJutakuKariireToTkbtKJSu.Text));

            if (!IsValidJutakuKariireToTkbtKJSu())
            {
                e.Cancel = true;
                this.txtJutakuKariireToTkbtKJSu.SelectAll();
            }
        }

        /// <summary>
        /// 源泉徴収票・摘要①の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTekiyo1_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidTekiyo1())
            {
                e.Cancel = true;
                this.txtTekiyo1.SelectAll();
            }
        }

        /// <summary>
        /// 源泉徴収票・摘要②の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTekiyo2_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidTekiyo2())
            {
                e.Cancel = true;
                this.txtTekiyo2.SelectAll();
            }
        }

        /// <summary>
        /// 金額フィールドのチェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKingaku_Validating(object sender, CancelEventArgs e)
        {
            FsiTextBox txt = (FsiTextBox)sender;
            if (!IsValidCurrency(txt))
            {
                e.Cancel = true;
                txt.SelectAll();
            }
            else
            {
                txt.Text = Util.FormatNum(txt.Text);
            }

        }
        /// <summary>
        /// 住宅借入等年末残(1回目)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtJutakuKariireToNenmatsuZan1_Validating(object sender, CancelEventArgs e)
        {
            FsiTextBox txt = (FsiTextBox)sender;
            if (!IsValidCurrency(txt))
            {
                e.Cancel = true;
                txt.SelectAll();
            }
            else
            {
                txt.Text = Util.FormatNum(txt.Text);
            }
        }

        /// <summary>
        /// 住宅借入等年末残(2回目)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtJutakuKariireToNenmatsuZan2_Validating(object sender, CancelEventArgs e)
        {
            FsiTextBox txt = (FsiTextBox)sender;
            if (!IsValidCurrency(txt))
            {
                e.Cancel = true;
                txt.SelectAll();
            }
            else
            {
                txt.Text = Util.FormatNum(txt.Text);
            }
        }

        /// <summary>
        /// 金額フィールドのEnter時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKingaku_Enter(object sender, EventArgs e)
        {
            FsiTextBox txt = (FsiTextBox)sender;
            
            // 入力書式へ
            txt.Text = Util.ToString(Util.ToDecimal(txt.Text));
            txt.SelectAll();
        }

        /// <summary>
        /// 画面の最後のコントールでのキーダウン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LastControl_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.btnF6.Enabled)
            {
                this.PressF6();
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 編集データクリア
        /// </summary>
        private void DataClear()
        {
            // 年調年度
            if (Util.ToInt(Util.ToString(this.txtGengoYearNendo.Text)) == 0)
            {
                string[] nendo = Util.ConvJpDate(new DateTime(this.UInfo.KaikeiNendo, 4, 1), this.Dba);
                this.lblGengoNendo.Text = nendo[0];
                this.txtGengoYearNendo.Text = nendo[2];
            }

            // 社員情報・基本情報
            this.txtShainCd.Text = "0";
            this.lblShainNm.Text = "";
            this.lblNyuryokuF.Text = "未";
            this.lblKeisanF.Text = "未";
            this.btnF3.Enabled = false;

            // 年末調整情報
            this.txtShakaiHokenryoKjgkSnkkbn.Text = "0";
            this.txtSkbKigyoKyosaiKakekinKjgk.Text = "0";
            this.txtSeimeihokenryoKojogaku.Text = "0";
            this.txtSongaihokenryoKojogaku.Text = "0";
            this.txtHaigushaTokubetsuKojogaku.Text = "0";
            this.txtJutakuShutokutoTkbtKojogaku.Text = "0";
            this.txtJutakuKariireToNenmatsuZan1.Text = "0";
            this.txtJutakuKariireToNenmatsuZan2.Text = "0";
            this.lblGengoIjuKaishiDate1.Text = "";
            this.txtGengoYearIjuKaishiDate1.Text = "";
            this.txtMonthIjuKaishiDate1.Text = "";
            this.txtDayIjuKaishiDate1.Text = "";
            this.txtJutakuKariireToTkbtKJKubun1.Text = "";
            this.txtJutakuKariireToTkbtKJKubun2.Text = "";
            this.txtJutakuKariireToTkbtKJSu.Text = "";

            this.lblGengoZenTaishokuDate.Text = "";
            this.txtGengoYearZenTaishokuDate.Text = "";
            this.txtMonthZenTaishokuDate.Text = "";
            this.txtDayZenTaishokuDate.Text = "";
            this.txtHaigushaGokeiShotoku.Text = "0";
            this.txtShinSeimeiHokenryo.Text = "0";
            this.txtKyuSeimeiHokenryo.Text = "0";
            this.txtKaigoIryoHokenryo.Text = "0";
            this.txtShinKojinNenkinHokenryo.Text = "0";
            this.txtKojinNenkinHokenryoShrigk.Text = "0";
            this.txtChokiSongaiHokenryoShrigk.Text = "0";
            this.txtKokuminNenkinHokenryoShrigk.Text = "0";
            this.txtChoseiKyuyoKazeiKyuyogaku.Text = "0";
            this.txtChoseiKyuyoShakaiHokenryo.Text = "0";
            this.txtChoseiKyuyoShotokuzeigaku.Text = "0";
            this.txtChoseiShoyoKazeiKyuyogaku.Text = "0";
            this.txtChoseiShoyoShakaiHokenryo.Text = "0";
            this.txtChoseiShoyoShotokuzeigaku.Text = "0";

            // 社員情報・所得税情報
            this.txtZeigakuHyo.Text = "0";
            this.txtHonninKubun1.Text = "0";
            this.lblHonninKubun1Nm.Text = "";
            this.txtHonninKubun2.Text = "0";
            this.lblHonninKubun2Nm.Text = "";
            this.txtHonninKubun3.Text = "0";
            this.lblHonninKubun3Nm.Text = "";
            this.txtMiseinensha.Text = "0";
            this.txtShiboTaishoku.Text = "0";
            this.txtSaigaisha.Text = "0";
            this.txtGaikokujin.Text = "0";
            this.txtHaigushaKubun.Text = "0";
            this.lblHaigushaKubunNm.Text = "";
            this.txtFuyoIppan.Text = "0";
            this.txtFuyoTokutei.Text = "0";
            this.txtFuyoRojinDokyoRoshintoIgai.Text = "0";
            this.txtFuyoRojinDokyoRoshinto.Text = "0";
            this.txtSai16Miman.Text = "0";
            this.txtFuyoDokyoTokushoIppan.Text = "0";
            this.txtShogaishaIppan.Text = "0";
            this.txtShogaishaTokubetsu.Text = "0";
            this.txtTekiyo1.Text = "";
            this.txtTekiyo2.Text = "";
        }

        /// <summary>
        /// 編集データの読込
        /// </summary>
        /// <param name="shainCd">社員コード</param>
        private void DataLoad(string shainCd)
        {
            DbParamCollection dpc;
            DataTable dt;

            // VI社員情報データ参照
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHAIN_CD", SqlDbType.Decimal, 6, shainCd);
            dt = this.Dba.GetDataTableFromSqlWithParams(GetShainJohoSql(), dpc);
            if (dt.Rows.Count ==　1)
            {
                // 社員情報・基本情報
                DataRow dr = dt.Rows[0];
                this.txtShainCd.Text = Util.ToString(dr["SHAIN_CD"]);
                this.lblShainNm.Text = Util.ToString(dr["SHAIN_NM"]);
                // 社員情報・所得税情報
                this.txtZeigakuHyo.Text = Util.ToString(dr["ZEIGAKU_HYO"]);
                this.txtHonninKubun1.Text = Util.ToString(dr["HONNIN_KUBUN1"]);
                this.lblHonninKubun1Nm.Text = Util.ToString(dr["HONNIN_KUBUN1_NM"]);
                this.txtHonninKubun2.Text = Util.ToString(dr["HONNIN_KUBUN2"]);
                this.lblHonninKubun2Nm.Text = Util.ToString(dr["HONNIN_KUBUN2_NM"]);
                this.txtHonninKubun3.Text = Util.ToString(dr["HONNIN_KUBUN3"]);
                this.lblHonninKubun3Nm.Text = Util.ToString(dr["HONNIN_KUBUN3_NM"]);
                this.txtMiseinensha.Text = Util.ToString(dr["MISEINENSHA"]);
                this.txtShiboTaishoku.Text = Util.ToString(dr["SHIBO_TAISHOKU"]);
                this.txtSaigaisha.Text = Util.ToString(dr["SAIGAISHA"]);
                this.txtGaikokujin.Text = Util.ToString(dr["GAIKOKUJIN"]);
                this.txtHaigushaKubun.Text = Util.ToString(dr["HAIGUSHA_KUBUN"]);
                this.lblHaigushaKubunNm.Text = Util.ToString(dr["HAIGUSHA_KUBUN_NM"]);
                this.txtFuyoIppan.Text = Util.ToString(dr["FUYO_IPPAN"]);
                this.txtFuyoTokutei.Text = Util.ToString(dr["FUYO_TOKUTEI"]);
                this.txtFuyoRojinDokyoRoshintoIgai.Text = Util.ToString(dr["FUYO_ROJIN_DOKYO_ROSHINTO_IGAI"]);
                this.txtFuyoRojinDokyoRoshinto.Text = Util.ToString(dr["FUYO_ROJIN_DOKYO_ROSHINTO"]);
                this.txtSai16Miman.Text = Util.ToString(dr["SAI16_MIMAN"]);
                this.txtFuyoDokyoTokushoIppan.Text = Util.ToString(dr["FUYO_DOKYO_TOKUSHO_IPPAN"]);
                this.txtShogaishaIppan.Text = Util.ToString(dr["SHOGAISHA_IPPAN"]);
                this.txtShogaishaTokubetsu.Text = Util.ToString(dr["SHOGAISHA_TOKUBETSU"]);
                this.txtTekiyo1.Text = Util.ToString(dr["TEKIYO1"]);
                this.txtTekiyo2.Text = Util.ToString(dr["TEKIYO2"]);
            }
            // TB年末調整
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHAIN_CD", SqlDbType.Decimal, 6, shainCd);
            dpc.SetParam("@NENDO", SqlDbType.DateTime, 
                Util.ConvAdDate(this.lblGengoNendo.Text, this.txtGengoYearNendo.Text, "1", "1", this.Dba));
            dt = this.Dba.GetDataTableFromSqlWithParams(GetNenmatsuChoseiSql(), dpc);
            if (dt.Rows.Count == 0)
            {
                this.txtShakaiHokenryoKjgkSnkkbn.Text = "0";
                this.txtSkbKigyoKyosaiKakekinKjgk.Text = "0";
                this.txtSeimeihokenryoKojogaku.Text = "0";
                this.txtSongaihokenryoKojogaku.Text = "0";
                this.txtHaigushaTokubetsuKojogaku.Text = "0";
                this.txtJutakuShutokutoTkbtKojogaku.Text = "0";
                this.txtJutakuKariireToNenmatsuZan1.Text = "0";
                this.txtJutakuKariireToNenmatsuZan2.Text = "0";
                this.lblGengoIjuKaishiDate1.Text = "";
                this.txtGengoYearIjuKaishiDate1.Text = "";
                this.txtMonthIjuKaishiDate1.Text = "";
                this.txtDayIjuKaishiDate1.Text = "";
                this.lblGengoIjuKaishiDate2.Text = "";
                this.txtGengoYearIjuKaishiDate2.Text = "";
                this.txtMonthIjuKaishiDate2.Text = "";
                this.txtDayIjuKaishiDate2.Text = "";

                this.txtJutakuKariireToTkbtKJKubun1.Text = "";
                this.txtJutakuKariireToTkbtKJKubun2.Text = "";
                this.txtJutakuKariireToTkbtKJSu.Text = "";

                this.lblGengoZenTaishokuDate.Text = "";
                this.txtGengoYearZenTaishokuDate.Text = "";
                this.txtMonthZenTaishokuDate.Text = "";
                this.txtDayZenTaishokuDate.Text = "";
                this.txtHaigushaGokeiShotoku.Text = "0";
                this.txtShinSeimeiHokenryo.Text = "0";
                this.txtKyuSeimeiHokenryo.Text = "0";
                this.txtKaigoIryoHokenryo.Text = "0";
                this.txtShinKojinNenkinHokenryo.Text = "0";
                this.txtKojinNenkinHokenryoShrigk.Text = "0";
                this.txtChokiSongaiHokenryoShrigk.Text = "0";
                this.txtKokuminNenkinHokenryoShrigk.Text = "0";
                this.txtZenshokubunKazeiKyuyogaku.Text = "0";
                this.txtZenshokubunShakaiHokenryo.Text = "0";
                this.txtZenshokubunShotokuzeigaku.Text = "0";
                this.txtChoseiKyuyoKazeiKyuyogaku.Text = "0";
                this.txtChoseiKyuyoShakaiHokenryo.Text = "0";
                this.txtChoseiKyuyoShotokuzeigaku.Text = "0";
                this.txtChoseiShoyoKazeiKyuyogaku.Text = "0";
                this.txtChoseiShoyoShakaiHokenryo.Text = "0";
                this.txtChoseiShoyoShotokuzeigaku.Text = "0";

                this.lblNyuryokuF.Text = "未";
                this.lblKeisanF.Text = "未";

                this.btnF3.Enabled = false;
            }
            else
            {
                DataRow dr = dt.Rows[0];
                this.txtShakaiHokenryoKjgkSnkkbn.Text = Util.FormatNum(dr["SHAKAI_HOKENRYO_KJGK_SNKKBN"]);
                this.txtSkbKigyoKyosaiKakekinKjgk.Text = Util.FormatNum(dr["SKB_KIGYO_KYOSAI_KAKEKIN_KJGK"]);
                this.txtSeimeihokenryoKojogaku.Text = Util.FormatNum(dr["SEIMEIHOKENRYO_KOJOGAKU"]);
                this.txtSongaihokenryoKojogaku.Text = Util.FormatNum(dr["SONGAIHOKENRYO_KOJOGAKU"]);
                this.txtHaigushaTokubetsuKojogaku.Text = Util.FormatNum(dr["HAIGUSHA_TOKUBETSU_KOJOGAKU"]);
                this.txtJutakuShutokutoTkbtKojogaku.Text = Util.FormatNum(dr["JUTAKU_SHUTOKUTO_TKBT_KOJOGAKU"]);
                this.txtJutakuKariireToNenmatsuZan1.Text = Util.FormatNum(dr["JUTAKU_KARIIRETO_NENMATSUZAN1"]);
                this.txtJutakuKariireToNenmatsuZan2.Text = Util.FormatNum(dr["JUTAKU_KARIIRETO_NENMATSUZAN2"]);
                this.txtJutakuKariireToTkbtKJKubun1.Text = Util.ToString(dr["JUTAKU_KARIIRETO_TKBT_KOJO_KUBUN1"]);
                this.txtJutakuKariireToTkbtKJKubun2.Text = Util.ToString(dr["JUTAKU_KARIIRETO_TKBT_KOJO_KUBUN2"]);
                this.txtJutakuKariireToTkbtKJSu.Text = Util.FormatNum(dr["JUTAKU_KARIIRETO_TKBT_KOJOSU"]);
                if (!ValChk.IsEmpty(dr["IJU_KAISHI_DATE1"]))
                {
                    string[] jpDate = Util.ConvJpDate(Util.ToDate(dr["IJU_KAISHI_DATE1"]), this.Dba);
                    this.lblGengoIjuKaishiDate1.Text = jpDate[0];
                    this.txtGengoYearIjuKaishiDate1.Text = jpDate[2];
                    this.txtMonthIjuKaishiDate1.Text = jpDate[3];
                    this.txtDayIjuKaishiDate1.Text = jpDate[4];
                }
                if (!ValChk.IsEmpty(dr["IJU_KAISHI_DATE2"]))
                {
                    string[] jpDate = Util.ConvJpDate(Util.ToDate(dr["IJU_KAISHI_DATE2"]), this.Dba);
                    this.lblGengoIjuKaishiDate2.Text = jpDate[0];
                    this.txtGengoYearIjuKaishiDate2.Text = jpDate[2];
                    this.txtMonthIjuKaishiDate2.Text = jpDate[3];
                    this.txtDayIjuKaishiDate2.Text = jpDate[4];
                }
                if (!ValChk.IsEmpty(dr["ZENTAISHOKU_DATE"]))
                {
                    string[] jpDate = Util.ConvJpDate(Util.ToDate(dr["ZENTAISHOKU_DATE"]), this.Dba);
                    this.lblGengoZenTaishokuDate.Text = jpDate[0];
                    this.txtGengoYearZenTaishokuDate.Text = jpDate[2];
                    this.txtMonthZenTaishokuDate.Text = jpDate[3];
                    this.txtDayZenTaishokuDate.Text = jpDate[4];
                }
                this.txtHaigushaGokeiShotoku.Text = Util.FormatNum(dr["HAIGUSHA_GOKEI_SHOTOKU"]);
                this.txtShinSeimeiHokenryo.Text = Util.FormatNum(dr["SHIN_SEIMEI_HOKENRYO"]);
                this.txtKyuSeimeiHokenryo.Text = Util.FormatNum(dr["KYU_SEIMEI_HOKENRYO"]);
                this.txtKaigoIryoHokenryo.Text = Util.FormatNum(dr["KAIGO_IRYO_HOKENRYO"]);
                this.txtShinKojinNenkinHokenryo.Text = Util.FormatNum(dr["SHIN_KOJIN_NENKIN_HOKENRYO"]);
                this.txtKojinNenkinHokenryoShrigk.Text = Util.FormatNum(dr["KOJIN_NENKIN_HOKENRYO_SHRIGK"]);
                this.txtChokiSongaiHokenryoShrigk.Text = Util.FormatNum(dr["CHOKI_SONGAI_HOKENRYO_SHRIGK"]);
                this.txtKokuminNenkinHokenryoShrigk.Text = Util.FormatNum(dr["KOKUMIN_NENKIN_HOKENRYO_SHRIGK"]);
                this.txtZenshokubunKazeiKyuyogaku.Text = Util.FormatNum(dr["ZENSHOKUBUN_KAZEI_KYUYOGAKU"]);
                this.txtZenshokubunShakaiHokenryo.Text = Util.FormatNum(dr["ZENSHOKUBUN_SHAKAI_HOKENRYO"]);
                this.txtZenshokubunShotokuzeigaku.Text = Util.FormatNum(dr["ZENSHOKUBUN_SHOTOKUZEIGAKU"]);
                this.txtChoseiKyuyoKazeiKyuyogaku.Text = Util.FormatNum(dr["CHOSEI_KYUYO_KAZEI_KYUYOGAKU"]);
                this.txtChoseiKyuyoShakaiHokenryo.Text = Util.FormatNum(dr["CHOSEI_KYUYO_SHAKAI_HOKENRYO"]);
                this.txtChoseiKyuyoShotokuzeigaku.Text = Util.FormatNum(dr["CHOSEI_KYUYO_SHOTOKUZEIGAKU"]);
                this.txtChoseiShoyoKazeiKyuyogaku.Text = Util.FormatNum(dr["CHOSEI_SHOYO_KAZEI_KYUYOGAKU"]);
                this.txtChoseiShoyoShakaiHokenryo.Text = Util.FormatNum(dr["CHOSEI_SHOYO_SHAKAI_HOKENRYO"]);
                this.txtChoseiShoyoShotokuzeigaku.Text = Util.FormatNum(dr["CHOSEI_SHOYO_SHOTOKUZEIGAKU"]);

                this.lblNyuryokuF.Text = Util.ToString(dr["NYURYOKU_F"]) == "1" ? "済み" : "未";
                this.lblKeisanF.Text = Util.ToString(dr["KEISAN_F"]) == "1" ? "済み" : "未";

                this.btnF3.Enabled = true;
            }
        }

        /// <summary>
        /// 社員データの照会SQL文を返す
        /// </summary>
        private string GetShainJohoSql()
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT");
            sql.Append(" KAISHA_CD");
            sql.Append(",SHAIN_CD");
            sql.Append(",SHAIN_NM");
            sql.Append(",SHAIN_KANA_NM");
            sql.Append(",SEIBETSU");
            sql.Append(",SEINENGAPPI");
            sql.Append(",YUBIN_BANGO1");
            sql.Append(",YUBIN_BANGO2");
            sql.Append(",JUSHO1");
            sql.Append(",JUSHO2");
            sql.Append(",DENWA_BANGO");
            sql.Append(",BUMON_CD");
            sql.Append(",BUMON_NM");
            sql.Append(",BUKA_CD");
            sql.Append(",BUKA_NM");
            sql.Append(",YAKUSHOKU_CD");
            sql.Append(",YAKUSHOKU_NM");
            sql.Append(",KYUYO_KEITAI");
            sql.Append(",KYUYO_KEITAI_NM");
            sql.Append(",NYUSHA_NENGAPPI");
            sql.Append(",TAISHOKU_NENGAPPI");
            sql.Append(",KENKO_HOKENSHO_BANGO");
            sql.Append(",KENKO_HOKEN_TOKYU");
            sql.Append(",KENKO_HOKEN_HYOJUN_HOSHU_GTGK");
            sql.Append(",KENKO_HOKENRYO");
            sql.Append(",KENKO_HOKEN_SHOYO_KEISAN_KUBUN");
            sql.Append(",KOSEI_NENKIN_BANGO");
            sql.Append(",KOSEI_NENKIN_TOKYU");
            sql.Append(",KOSEI_NENKIN_HYOJUN_HOSHU_GTGK");
            sql.Append(",KOSEI_NENKIN_HOKENRYO");
            sql.Append(",KOSEI_NENKIN_SHOYO_KEISAN_KBN");
            sql.Append(",KENKO_HOKEN_KANYU_KUBUN");
            sql.Append(",KOSEI_NENKIN_KANYU_KUBUN");
            sql.Append(",KOSEI_NENKIN_SHUBETSU");
            sql.Append(",KOSEI_NENKIN_SHUBETSU_NM");
            sql.Append(",SHAHO_JUZEN_KAITEI_TSUKI");
            sql.Append(",SHAHO_JUZEN_KAITEI_GENIN");
            sql.Append(",KOYO_HOKEN_KEISAN_KUBUN");
            sql.Append(",JUMINZEI_SHICHOSON_CD");
            sql.Append(",JUMINZEI_SHICHOSON_NM");
            sql.Append(",JUMINZEI_6GATSUBUN");
            sql.Append(",JUMINZEI_7GATSUBUN_IKO");
            sql.Append(",ZEIGAKU_HYO");
            sql.Append(",NENMATSU_CHOSEI");
            sql.Append(",HONNIN_KUBUN1");
            sql.Append(",HONNIN_KUBUN1_NM");
            sql.Append(",HONNIN_KUBUN2");
            sql.Append(",HONNIN_KUBUN2_NM");
            sql.Append(",HONNIN_KUBUN3");
            sql.Append(",HONNIN_KUBUN3_NM");
            sql.Append(",HAIGUSHA_KUBUN");
            sql.Append(",HAIGUSHA_KUBUN_NM");
            sql.Append(",FUYO_IPPAN");
            sql.Append(",FUYO_NENSHO");
            sql.Append(",FUYO_TOKUTEI");
            sql.Append(",FUYO_ROJIN_DOKYO_ROSHINTO_IGAI");
            sql.Append(",FUYO_ROJIN_DOKYO_ROSHINTO");
            sql.Append(",FUYO_DOKYO_TOKUSHO_IPPAN");
            sql.Append(",FUYO_DOKYO_TOKUSHO_NENSHO");
            sql.Append(",FUYO_DOKYO_TOKUSHO_TOKUTEI");
            sql.Append(",FUYO_DOKYO_TKS_DOKYO_RSNT_IGAI");
            sql.Append(",FUYO_DOKYO_TKS_DOKYO_RSNT");
            sql.Append(",SHOGAISHA_IPPAN");
            sql.Append(",SHOGAISHA_TOKUBETSU");
            sql.Append(",OTTO_ARI");
            sql.Append(",MISEINENSHA");
            sql.Append(",SHIBO_TAISHOKU");
            sql.Append(",SAIGAISHA");
            sql.Append(",GAIKOKUJIN");
            sql.Append(",TEKIYO1");
            sql.Append(",TEKIYO2");
            sql.Append(",SAI16_MIMAN");
            sql.Append(",KYUYO_SHUBETSU");
            sql.Append(",TEATE_SHIKYU_KOMOKU1");
            sql.Append(",TEATE_SHIKYU_KOMOKU2");
            sql.Append(",TEATE_SHIKYU_KOMOKU3");
            sql.Append(",TEATE_SHIKYU_KOMOKU4");
            sql.Append(",TEATE_SHIKYU_KOMOKU5");
            sql.Append(",TEATE_SHIKYU_KOMOKU6");
            sql.Append(",TEATE_SHIKYU_KOMOKU7");
            sql.Append(",TEATE_SHIKYU_KOMOKU8");
            sql.Append(",TEATE_SHIKYU_KOMOKU9");
            sql.Append(",TEATE_SHIKYU_KOMOKU10");
            sql.Append(",TEATE_SHIKYU_KOMOKU11");
            sql.Append(",TEATE_SHIKYU_KOMOKU12");
            sql.Append(",TEATE_SHIKYU_KOMOKU13");
            sql.Append(",TEATE_SHIKYU_KOMOKU14");
            sql.Append(",TEATE_SHIKYU_KOMOKU15");
            sql.Append(",TEATE_SHIKYU_KOMOKU16");
            sql.Append(",TEATE_SHIKYU_KOMOKU17");
            sql.Append(",TEATE_SHIKYU_KOMOKU18");
            sql.Append(",TEATE_SHIKYU_KOMOKU19");
            sql.Append(",TEATE_SHIKYU_KOMOKU20");
            sql.Append(",TEATE_SHIKYU_KOMOKU21");
            sql.Append(",TEATE_SHIKYU_KOMOKU22");
            sql.Append(",TEATE_SHIKYU_KOMOKU23");
            sql.Append(",TEATE_SHIKYU_KOMOKU24");
            sql.Append(",TEATE_SHIKYU_KOMOKU25");
            sql.Append(",TEATE_SHIKYU_KOMOKU26");
            sql.Append(",TEATE_SHIKYU_KOMOKU27");
            sql.Append(",TEATE_SHIKYU_KOMOKU28");
            sql.Append(",TEATE_SHIKYU_KOMOKU29");
            sql.Append(",TEATE_SHIKYU_KOMOKU30");
            sql.Append(",TEATE_KOJO_KOMOKU1");
            sql.Append(",TEATE_KOJO_KOMOKU2");
            sql.Append(",TEATE_KOJO_KOMOKU3");
            sql.Append(",TEATE_KOJO_KOMOKU4");
            sql.Append(",TEATE_KOJO_KOMOKU5");
            sql.Append(",TEATE_KOJO_KOMOKU6");
            sql.Append(",TEATE_KOJO_KOMOKU7");
            sql.Append(",TEATE_KOJO_KOMOKU8");
            sql.Append(",TEATE_KOJO_KOMOKU9");
            sql.Append(",TEATE_KOJO_KOMOKU10");
            sql.Append(",TEATE_KOJO_KOMOKU11");
            sql.Append(",TEATE_KOJO_KOMOKU12");
            sql.Append(",TEATE_KOJO_KOMOKU13");
            sql.Append(",TEATE_KOJO_KOMOKU14");
            sql.Append(",TEATE_KOJO_KOMOKU15");
            sql.Append(",TEATE_KOJO_KOMOKU16");
            sql.Append(",TEATE_KOJO_KOMOKU17");
            sql.Append(",TEATE_KOJO_KOMOKU18");
            sql.Append(",TEATE_KOJO_KOMOKU19");
            sql.Append(",TEATE_KOJO_KOMOKU20");
            sql.Append(",KYUYO_SHIKYU_HOHO1");
            sql.Append(",KYUYO_SHIKYU_KINGAKU1");
            sql.Append(",KYUYO_SHIKYU_RITSU1");
            sql.Append(",KYUYO_GINKO_CD1");
            sql.Append(",KYUYO_GINKO_NM1");
            sql.Append(",KYUYO_SHITEN_CD1");
            sql.Append(",KYUYO_SHITEN_NM1");
            sql.Append(",KYUYO_YOKIN_SHUMOKU1");
            sql.Append(",KYUYO_KOZA_BANGO1");
            sql.Append(",KYUYO_KOZA_MEIGININ_KANJI1");
            sql.Append(",KYUYO_KOZA_MEIGININ_KANA1");
            sql.Append(",KYUYO_KEIYAKUSHA_BANGO1");
            sql.Append(",KYUYO_SHIKYU_HOHO2");
            sql.Append(",KYUYO_SHIKYU_KINGAKU2");
            sql.Append(",KYUYO_SHIKYU_RITSU2");
            sql.Append(",KYUYO_GINKO_CD2");
            sql.Append(",KYUYO_GINKO_NM2");
            sql.Append(",KYUYO_SHITEN_CD2");
            sql.Append(",KYUYO_SHITEN_NM2");
            sql.Append(",KYUYO_YOKIN_SHUMOKU2");
            sql.Append(",KYUYO_KOZA_BANGO2");
            sql.Append(",KYUYO_KOZA_MEIGININ_KANJI2");
            sql.Append(",KYUYO_KOZA_MEIGININ_KANA2");
            sql.Append(",KYUYO_KEIYAKUSHA_BANGO2");
            sql.Append(",KAIGO_HOKEN_KUBUN");
            sql.Append(" FROM");
            sql.Append(" VI_KY_SHAIN_JOHO");
            sql.Append(" WHERE");
            sql.Append(" KAISHA_CD = @KAISHA_CD");
            sql.Append(" AND SHAIN_CD = @SHAIN_CD");
            return sql.ToString();
        }

        /// <summary>
        /// 年調データの照会SQL文を返す
        /// </summary>
        private string GetNenmatsuChoseiSql()
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT");
            sql.Append(" KAISHA_CD");
            sql.Append(" ,NENDO");
            sql.Append(" ,SHAIN_CD");
            sql.Append(" ,ZENSHOKUBUN_KAZEI_KYUYOGAKU");
            sql.Append(" ,ZENSHOKUBUN_SHAKAI_HOKENRYO");
            sql.Append(" ,ZENSHOKUBUN_SHOTOKUZEIGAKU");
            sql.Append(" ,CHOSEI_KYUYO_KAZEI_KYUYOGAKU");
            sql.Append(" ,CHOSEI_KYUYO_HIKAZEIGAKU");
            sql.Append(" ,CHOSEI_KYUYO_SHAKAI_HOKENRYO");
            sql.Append(" ,CHOSEI_KYUYO_SHOTOKUZEIGAKU");
            sql.Append(" ,CHOSEI_SHOYO_KAZEI_KYUYOGAKU");
            sql.Append(" ,CHOSEI_SHOYO_HIKAZEIGAKU");
            sql.Append(" ,CHOSEI_SHOYO_SHAKAI_HOKENRYO");
            sql.Append(" ,CHOSEI_SHOYO_SHOTOKUZEIGAKU");
            sql.Append(" ,KYUYOTO_KAZEI_KINGAKU");
            sql.Append(" ,KYUYOTO_SHOTOKUZEIGAKU");
            sql.Append(" ,SHOYOTO_KAZEI_KINGAKU");
            sql.Append(" ,SHOYOTO_SHOTOKUZEIGAKU");
            sql.Append(" ,KYUYO_SHOTOKU_KOJOGO_NO_KNGK");
            sql.Append(" ,SHAKAI_HOKENRYO_KJGK_KYUYOTO");
            sql.Append(" ,SHAKAI_HOKENRYO_KJGK_SNKKBN");
            sql.Append(" ,SKB_KIGYO_KYOSAI_KAKEKIN_KJGK");
            sql.Append(" ,SEIMEIHOKENRYO_KOJOGAKU");
            sql.Append(" ,SONGAIHOKENRYO_KOJOGAKU");
            sql.Append(" ,HAIGUSHA_TOKUBETSU_KOJOGAKU");
            sql.Append(" ,HAIGUSHA_KOJOGAKU");
            sql.Append(" ,FUYO_KOJOGAKU");
            sql.Append(" ,KISO_KOJOGAKU");
            sql.Append(" ,SHOGAISHA_KOJOGAKU");
            sql.Append(" ,RONENSHA_KAFU_KOJOGAKU");
            sql.Append(" ,KINRO_GAKUSEI_KOJOGAKU");
            sql.Append(" ,KAZEI_KYUYO_SHOTOKU_KINGAKU");
            sql.Append(" ,SANSHUTSU_NENZEIGAKU");
            sql.Append(" ,JUTAKU_SHUTOKUTO_TKBT_KOJOGAKU");
            sql.Append(" ,JUTAKU_KARIIRETO_NENMATSUZAN1");
            sql.Append(" ,JUTAKU_KARIIRETO_NENMATSUZAN2");
            sql.Append(" ,JUTAKU_KARIIRETO_TKBT_KOJO_KUBUN1");
            sql.Append(" ,JUTAKU_KARIIRETO_TKBT_KOJO_KUBUN2");
            sql.Append(" ,JUTAKU_KARIIRETO_TKBT_KOJOSU");
            sql.Append(" ,NENCHO_NENZEIGAKU");
            sql.Append(" ,NENCHO_TOKUBETSU_GENZEIGAKU");
            sql.Append(" ,NENZEIGAKU");
            sql.Append(" ,HAIGUSHA_GOKEI_SHOTOKU");
            sql.Append(" ,KOJIN_NENKIN_HOKENRYO_SHRIGK");
            sql.Append(" ,CHOKI_SONGAI_HOKENRYO_SHRIGK");
            sql.Append(" ,ZEIGAKU_HYO");
            sql.Append(" ,NENMATSU_CHOSEI");
            sql.Append(" ,HONNIN_KUBUN1");
            sql.Append(" ,HONNIN_KUBUN2");
            sql.Append(" ,HONNIN_KUBUN3");
            sql.Append(" ,OTTO_ARI");
            sql.Append(" ,MISEINENSHA");
            sql.Append(" ,SHIBO_TAISHOKU");
            sql.Append(" ,SAIGAISHA");
            sql.Append(" ,GAIKOKUJIN");
            sql.Append(" ,HAIGUSHA_KUBUN");
            sql.Append(" ,FUYO_IPPAN");
            sql.Append(" ,FUYO_NENSHO");
            sql.Append(" ,FUYO_TOKUTEI");
            sql.Append(" ,FUYO_ROZIN_DOKYO_ROSHINTO_IGAI");
            sql.Append(" ,FUYO_ROZIN_DOKYO_ROSHINTO");
            sql.Append(" ,FUYO_DOKYO_TOKUSHO_IPPAN");
            sql.Append(" ,FUYO_DOKYO_TOKUSHO_NENSHO");
            sql.Append(" ,FUYO_DOKYO_TOKUSHO_TOKUTEI");
            sql.Append(" ,FUYO_DOKYO_TKS_DOKYO_RSNT_IGAI");
            sql.Append(" ,FUYO_DOKYO_TKS_DOKYO_RSNT");
            sql.Append(" ,SHOGAISHA_IPPAN");
            sql.Append(" ,SHOGAISHA_TOKUBETSU");
            sql.Append(" ,TEKIYO1");
            sql.Append(" ,TEKIYO2");
            sql.Append(" ,NYURYOKU_F");
            sql.Append(" ,KEISAN_F");
            sql.Append(" ,CHOSEI_F");
            sql.Append(" ,REGIST_DATE");
            sql.Append(" ,UPDATE_DATE");
            sql.Append(" ,KOKUMIN_NENKIN_HOKENRYO_SHRIGK");
            sql.Append(" ,ZENTAISHOKU_DATE");
            sql.Append(" ,IJU_KAISHI_DATE1");
            sql.Append(" ,IJU_KAISHI_DATE2");
            sql.Append(" ,KYUYO_KANPU_DATE");
            sql.Append(" ,SAI16_MIMAN");
            sql.Append(" ,SHIN_SEIMEI_HOKENRYO");
            sql.Append(" ,KYU_SEIMEI_HOKENRYO");
            sql.Append(" ,KAIGO_IRYO_HOKENRYO");
            sql.Append(" ,SHIN_KOJIN_NENKIN_HOKENRYO");
            sql.Append("  FROM TB_KY_NENMATSU_CHOSEI");
            sql.Append(" WHERE");
            sql.Append(" KAISHA_CD = @KAISHA_CD");
            sql.Append(" AND NENDO = @NENDO");
            sql.Append(" AND SHAIN_CD = @SHAIN_CD");
            return sql.ToString();
        }

        /// <summary>
        /// コード項目の補正
        /// </summary>
        /// <param name="value">入力値</param>
        /// <param name="maxCode">定義上限値</param>
        /// <returns>下限0から上限値までの値</returns>
        private string FixDefinedCode(object value, int maxCode)
        {
            string ret = "0";

            if (value != null)
            {
                if (Util.ToInt(value) > maxCode)
                {
                    ret = Util.ToString(maxCode);
                }
                else
                {
                    ret = Util.ToString(value);
                }
            }

            return ret;
        }

        /// <summary>
        /// 日付入力の補正
        /// </summary>
        /// <param name="gengo">元号ラベル</param>
        /// <param name="gengoYear">和暦年テキストボックス</param>
        /// <param name="month">月テキストボックス</param>
        /// <param name="day">日テキストボックス</param>
        private void FixDateControls(Label gengo, FsiTextBox gengoYear, FsiTextBox month, FsiTextBox day)
        {
            if (ValChk.IsEmpty(gengoYear.Text))
            {
                // 和暦年が未入力の場合、日付入力をクリア
                gengo.Text = "";
                gengoYear.Text = "";
                month.Text = "";
                day.Text = "";
            }
            else
            {
                // 未入力部分は 0年1月1日から補完
                if (ValChk.IsEmpty(gengo.Text)) gengo.Text = "平成";
                //if (ValChk.IsEmpty(gengoYear.Text)) gengoYear.Text = "0";
                if (ValChk.IsEmpty(month.Text) || month.Text == "0") month.Text = "1";
                if (ValChk.IsEmpty(day.Text) || day.Text == "0") day.Text = "1";

                // 月が12以上の場合は12をセット
                if (Util.ToInt(month.Text) > 12) month.Text = "12";

                // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
                DateTime tmpDate = Util.ConvAdDate(gengo.Text, gengoYear.Text, month.Text, "1", this.Dba);
                int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);
                if (Util.ToInt(day.Text) > lastDayInMonth)
                {
                    day.Text = Util.ToString(lastDayInMonth);
                }
            }
        }

        /// <summary>
        /// 配列に格納された移住開始年月日(1回目)和暦をセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpIjuKaishiDate1(string[] arrJpDate)
        {
            this.lblGengoIjuKaishiDate1.Text = arrJpDate[0];
            this.txtGengoYearIjuKaishiDate1.Text = arrJpDate[2];
            this.txtMonthIjuKaishiDate1.Text = arrJpDate[3];
            this.txtDayIjuKaishiDate1.Text = arrJpDate[4];
        }

        /// <summary>
        /// 配列に格納された移住開始年月日(2回目)和暦をセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpIjuKaishiDate2(string[] arrJpDate)
        {
            this.lblGengoIjuKaishiDate2.Text = arrJpDate[0];
            this.txtGengoYearIjuKaishiDate2.Text = arrJpDate[2];
            this.txtMonthIjuKaishiDate2.Text = arrJpDate[3];
            this.txtDayIjuKaishiDate2.Text = arrJpDate[4];
        }

        /// <summary>
        /// TB_KY_SHAIN_JOHOに更新するためのパラメータ設定をします。
        /// </summary>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 更新処理：DbParamCollection*2(Where句,Set句)
        /// </returns>
        private ArrayList SetKyShainJohoParams()
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            // WHERE句パラメータ
            DbParamCollection whereParam = new DbParamCollection();
            whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
            whereParam.SetParam("@SHAIN_CD", SqlDbType.Decimal, 6, Util.ToDecimal(this.txtShainCd.Text));
                
            alParams.Add(whereParam);

            // 所得税関連情報
            updParam.SetParam("@ZEIGAKU_HYO", SqlDbType.Decimal, 1, Util.ToDecimal(this.txtZeigakuHyo.Text));
            updParam.SetParam("@HONNIN_KUBUN1", SqlDbType.Decimal, 1, Util.ToDecimal(this.txtHonninKubun1.Text));
            updParam.SetParam("@HONNIN_KUBUN2", SqlDbType.Decimal, 1, Util.ToDecimal(this.txtHonninKubun2.Text));
            updParam.SetParam("@HONNIN_KUBUN3", SqlDbType.Decimal, 1, Util.ToDecimal(this.txtHonninKubun3.Text));
            updParam.SetParam("@HAIGUSHA_KUBUN", SqlDbType.Decimal, 1, Util.ToDecimal(this.txtHaigushaKubun.Text));
            updParam.SetParam("@FUYO_IPPAN", SqlDbType.Decimal, 2, Util.ToDecimal(this.txtFuyoIppan.Text));
            updParam.SetParam("@FUYO_TOKUTEI", SqlDbType.Decimal, 2, Util.ToDecimal(this.txtFuyoTokutei.Text));
            updParam.SetParam("@FUYO_ROJIN_DOKYO_ROSHINTO_IGAI", SqlDbType.Decimal, 2, Util.ToDecimal(this.txtFuyoRojinDokyoRoshintoIgai.Text));
            updParam.SetParam("@FUYO_ROJIN_DOKYO_ROSHINTO", SqlDbType.Decimal, 2, Util.ToDecimal(this.txtFuyoRojinDokyoRoshinto.Text));
            updParam.SetParam("@FUYO_DOKYO_TOKUSHO_IPPAN", SqlDbType.Decimal, 2, Util.ToDecimal(this.txtFuyoDokyoTokushoIppan.Text));
            updParam.SetParam("@SHOGAISHA_IPPAN", SqlDbType.Decimal, 2, Util.ToDecimal(this.txtShogaishaIppan.Text));
            updParam.SetParam("@SHOGAISHA_TOKUBETSU", SqlDbType.Decimal, 2, Util.ToDecimal(this.txtShogaishaTokubetsu.Text));
            updParam.SetParam("@MISEINENSHA", SqlDbType.Decimal, 2, Util.ToDecimal(this.txtMiseinensha.Text));
            updParam.SetParam("@SHIBO_TAISHOKU", SqlDbType.Decimal, 2, Util.ToDecimal(this.txtShiboTaishoku.Text));
            updParam.SetParam("@SAIGAISHA", SqlDbType.Decimal, 2, Util.ToDecimal(this.txtSaigaisha.Text));
            updParam.SetParam("@GAIKOKUJIN", SqlDbType.Decimal, 2, Util.ToDecimal(this.txtGaikokujin.Text));
            updParam.SetParam("@TEKIYO1", SqlDbType.VarChar, 50, this.txtTekiyo1.Text);
            updParam.SetParam("@TEKIYO2", SqlDbType.VarChar, 50, this.txtTekiyo2.Text);
            updParam.SetParam("@SAI16_MIMAN", SqlDbType.Decimal, 2, Util.ToDecimal(this.txtSai16Miman.Text));
            updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWDATE");

            alParams.Add(updParam);

            return alParams;
        }

        /// <summary>
        /// TB_KY_NENMATSU_CHOSEIに更新するためのパラメータ設定をします。
        /// </summary>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection
        /// </returns>
        private ArrayList SetKyNenmatsuChoseiParams()
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection insParam = new DbParamCollection();

            // 基本情報
            insParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
            insParam.SetParam("@SHAIN_CD", SqlDbType.Decimal, 6, Util.ToDecimal(this.txtShainCd.Text));
            insParam.SetParam("@NENDO", SqlDbType.DateTime,
                Util.ConvAdDate(this.lblGengoNendo.Text, this.txtGengoYearNendo.Text, "1", "1", this.Dba));
            // 所得税関連情報
            insParam.SetParam("@SHAKAI_HOKENRYO_KJGK_SNKKBN", SqlDbType.Decimal, 9, 
                Util.ToDecimal(this.txtShakaiHokenryoKjgkSnkkbn.Text));
            insParam.SetParam("@SKB_KIGYO_KYOSAI_KAKEKIN_KJGK", SqlDbType.Decimal, 9, 
                Util.ToDecimal(this.txtSkbKigyoKyosaiKakekinKjgk.Text));
            insParam.SetParam("@SEIMEIHOKENRYO_KOJOGAKU", SqlDbType.Decimal, 9, 
                Util.ToDecimal(this.txtSeimeihokenryoKojogaku.Text));
            insParam.SetParam("@SONGAIHOKENRYO_KOJOGAKU", SqlDbType.Decimal, 9, 
                Util.ToDecimal(this.txtSongaihokenryoKojogaku.Text));
            insParam.SetParam("@HAIGUSHA_TOKUBETSU_KOJOGAKU", SqlDbType.Decimal, 9,
                Util.ToDecimal(this.txtHaigushaTokubetsuKojogaku.Text));
            insParam.SetParam("@JUTAKU_SHUTOKUTO_TKBT_KOJOGAKU", SqlDbType.Decimal, 9,
                Util.ToDecimal(this.txtJutakuShutokutoTkbtKojogaku.Text));
            insParam.SetParam("@JUTAKU_KARIIRETO_NENMATSUZAN1", SqlDbType.Decimal, 9,
                Util.ToDecimal(this.txtJutakuKariireToNenmatsuZan1.Text));
            insParam.SetParam("@JUTAKU_KARIIRETO_NENMATSUZAN2", SqlDbType.Decimal, 9,
                Util.ToDecimal(this.txtJutakuKariireToNenmatsuZan2.Text));

            insParam.SetParam("@JUTAKU_KARIIRETO_TKBT_KOJO_KUBUN1", SqlDbType.VarChar, 20,
                this.txtJutakuKariireToTkbtKJKubun1.Text);
            insParam.SetParam("@JUTAKU_KARIIRETO_TKBT_KOJO_KUBUN2", SqlDbType.VarChar, 20,
                this.txtJutakuKariireToTkbtKJKubun2.Text);
            insParam.SetParam("@JUTAKU_KARIIRETO_TKBT_KOJOSU", SqlDbType.Decimal, 1,
                Util.ToDecimal(this.txtJutakuKariireToTkbtKJSu.Text));

            insParam.SetParam("@HAIGUSHA_GOKEI_SHOTOKU", SqlDbType.Decimal, 9, 
                Util.ToDecimal(this.txtHaigushaGokeiShotoku.Text));
            insParam.SetParam("@SHIN_SEIMEI_HOKENRYO", SqlDbType.Decimal, 9, 
                Util.ToDecimal(this.txtShinSeimeiHokenryo.Text));
            insParam.SetParam("@KYU_SEIMEI_HOKENRYO", SqlDbType.Decimal, 9, 
                Util.ToDecimal(this.txtKyuSeimeiHokenryo.Text));
            insParam.SetParam("@KAIGO_IRYO_HOKENRYO", SqlDbType.Decimal, 9, 
                Util.ToDecimal(this.txtKaigoIryoHokenryo.Text));
            insParam.SetParam("@SHIN_KOJIN_NENKIN_HOKENRYO", SqlDbType.Decimal, 9, 
                Util.ToDecimal(this.txtShinKojinNenkinHokenryo.Text));
            insParam.SetParam("@KOJIN_NENKIN_HOKENRYO_SHRIGK", SqlDbType.Decimal, 9, 
                Util.ToDecimal(this.txtKojinNenkinHokenryoShrigk.Text));
            insParam.SetParam("@CHOKI_SONGAI_HOKENRYO_SHRIGK", SqlDbType.Decimal, 9, 
                Util.ToDecimal(this.txtChokiSongaiHokenryoShrigk.Text));
            insParam.SetParam("@KOKUMIN_NENKIN_HOKENRYO_SHRIGK", SqlDbType.Decimal, 9, 
                Util.ToDecimal(this.txtKokuminNenkinHokenryoShrigk.Text));
            insParam.SetParam("@ZENSHOKUBUN_KAZEI_KYUYOGAKU", SqlDbType.Decimal, 9,
                Util.ToDecimal(this.txtZenshokubunKazeiKyuyogaku.Text));
            insParam.SetParam("@ZENSHOKUBUN_SHAKAI_HOKENRYO", SqlDbType.Decimal, 9,
                Util.ToDecimal(this.txtZenshokubunShakaiHokenryo.Text));
            insParam.SetParam("@ZENSHOKUBUN_SHOTOKUZEIGAKU", SqlDbType.Decimal, 9,
                Util.ToDecimal(this.txtZenshokubunShotokuzeigaku.Text));
            insParam.SetParam("@CHOSEI_KYUYO_KAZEI_KYUYOGAKU", SqlDbType.Decimal, 9, 
                Util.ToDecimal(this.txtChoseiKyuyoKazeiKyuyogaku.Text));
            insParam.SetParam("@CHOSEI_KYUYO_SHAKAI_HOKENRYO", SqlDbType.Decimal, 9, 
                Util.ToDecimal(this.txtChoseiKyuyoShakaiHokenryo.Text));
            insParam.SetParam("@CHOSEI_KYUYO_SHOTOKUZEIGAKU", SqlDbType.Decimal, 9, 
                Util.ToDecimal(this.txtChoseiKyuyoShotokuzeigaku.Text));
            insParam.SetParam("@CHOSEI_SHOYO_KAZEI_KYUYOGAKU", SqlDbType.Decimal, 9, 
                Util.ToDecimal(this.txtChoseiShoyoKazeiKyuyogaku.Text));
            insParam.SetParam("@CHOSEI_SHOYO_SHAKAI_HOKENRYO", SqlDbType.Decimal, 9, 
                Util.ToDecimal(this.txtChoseiShoyoShakaiHokenryo.Text));
            insParam.SetParam("@CHOSEI_SHOYO_SHOTOKUZEIGAKU", SqlDbType.Decimal, 9,
                Util.ToDecimal(this.txtChoseiShoyoShotokuzeigaku.Text));
            if (ValChk.IsEmpty(this.txtGengoYearIjuKaishiDate1.Text))
            {
                insParam.SetParam("@IJU_KAISHI_DATE1", SqlDbType.DateTime, DBNull.Value);
            }
            else
            {
                insParam.SetParam("@IJU_KAISHI_DATE1", SqlDbType.DateTime,
                    Util.ConvAdDate(
                        this.lblGengoIjuKaishiDate1.Text,
                        this.txtGengoYearIjuKaishiDate1.Text,
                        this.txtMonthIjuKaishiDate1.Text,
                        this.txtDayIjuKaishiDate1.Text, this.Dba));
            }
            if (ValChk.IsEmpty(this.txtGengoYearIjuKaishiDate2.Text))
            {
                insParam.SetParam("@IJU_KAISHI_DATE2", SqlDbType.DateTime, DBNull.Value);
            }
            else
            {
                insParam.SetParam("@IJU_KAISHI_DATE2", SqlDbType.DateTime,
                    Util.ConvAdDate(
                        this.lblGengoIjuKaishiDate2.Text,
                        this.txtGengoYearIjuKaishiDate2.Text,
                        this.txtMonthIjuKaishiDate2.Text,
                        this.txtDayIjuKaishiDate2.Text, this.Dba));
            }
            if (ValChk.IsEmpty(this.txtGengoYearZenTaishokuDate.Text))
            {
                insParam.SetParam("@ZENTAISHOKU_DATE", SqlDbType.DateTime, DBNull.Value);
            }
            else
            {
                insParam.SetParam("@ZENTAISHOKU_DATE", SqlDbType.DateTime,
                    Util.ConvAdDate(
                        this.lblGengoZenTaishokuDate.Text,
                        this.txtGengoYearZenTaishokuDate.Text,
                        this.txtMonthZenTaishokuDate.Text,
                        this.txtDayZenTaishokuDate.Text, this.Dba));
            }
            insParam.SetParam("@NYURYOKU_F", SqlDbType.Decimal, 1, 1);
            insParam.SetParam("@KEISAN_F", SqlDbType.Decimal, 1, 0);
            insParam.SetParam("@CHOSEI_F", SqlDbType.Decimal, 1, 0);
            insParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWDATE");
            insParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWDATE");

            alParams.Add(insParam);

            return alParams;
        }

        #endregion

        #region privateメソッド(入力チェック)

        /// <summary>
        /// 社員コードの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShainCd()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtShainCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 存在しないコードを入力した場合はエラーとする
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
            dpc.SetParam("@SHAIN_CD", SqlDbType.Decimal, 6, this.txtShainCd.Text);
            StringBuilder where = new StringBuilder();
            where.Append(" KAISHA_CD = @KAISHA_CD");
            where.Append(" AND SHAIN_CD = @SHAIN_CD");
            DataTable dt =
                this.Dba.GetDataTableByConditionWithParams("SHAIN_CD",
                    "TB_KY_SHAIN_JOHO", Util.ToString(where), dpc);
            if (dt.Rows.Count == 0)
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 本人区分①の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidHonninKubun1()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtHonninKubun1.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 存在しないコードを入力されたらエラー
            if (this.txtHonninKubun1.Text != "0")
            {
                string name = this.Dba.GetKyuyoKbnNm("2", Util.ToString(this.txtHonninKubun1.Text));
                if (ValChk.IsEmpty(name))
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// 本人区分②の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidHonninKubun2()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtHonninKubun2.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 存在しないコードを入力されたらエラー
            if (this.txtHonninKubun2.Text != "0")
            {
                string name = this.Dba.GetKyuyoKbnNm("3", Util.ToString(this.txtHonninKubun2.Text));
                if (ValChk.IsEmpty(name))
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// 本人区分③の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidHonninKubun3()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtHonninKubun3.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 存在しないコードを入力されたらエラー
            if (this.txtHonninKubun3.Text != "0")
            {
                string name = this.Dba.GetKyuyoKbnNm("4", Util.ToString(this.txtHonninKubun3.Text));
                if (ValChk.IsEmpty(name))
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// 配偶者区分の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidHaigushaKubun()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtHaigushaKubun.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 存在しないコードを入力されたらエラー
            if (this.txtHaigushaKubun.Text != "0")
            {
                string name = this.Dba.GetKyuyoKbnNm("5", Util.ToString(this.txtHaigushaKubun.Text));
                if (ValChk.IsEmpty(name))
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// 摘要1の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidTekiyo1()
        {
            return IsValidText(this.txtTekiyo1);
        }

        /// <summary>
        /// 摘要2の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidTekiyo2()
        {
            return IsValidText(this.txtTekiyo2);
        }

        /// <summary>
        /// 住宅借入金等特別控除適用数の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidJutakuKariireToTkbtKJSu()
        {
            return IsValidText(this.txtJutakuKariireToTkbtKJSu);
        }

        /// <summary>
        ///  住宅借入金等特別控除区分(1回目)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidJutakuKariireToTkbtKJKubun1()
        {
            return IsValidText(this.txtJutakuKariireToTkbtKJKubun1);
        }

        /// <summary>
        ///  住宅借入金等特別控除区分(2回目)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidJutakuKariireToTkbtKJKubun2()
        {
            return IsValidText(this.txtJutakuKariireToTkbtKJKubun2);
        }

        /// <summary>
        /// 文字フィールドの入力チェック
        /// </summary>
        /// <param name="textBox"></param>
        /// <returns></returns>
        private bool IsValidText(FsiTextBox textBox)
        {
            // 入力サイズ
            if (!ValChk.IsWithinLength(textBox.Text, textBox.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 金額フィールドの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidCurrency(FsiTextBox textBox)
        {
            // 金額値のみ入力可能
            if (!ValChk.IsNumber(Util.ToDecimal(textBox.Text)))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 入力サイズ(書式なし数値で判定)
            if (!ValChk.IsWithinLength(Util.ToDecimal(textBox.Text), textBox.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 数値フィールドの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidNumber(FsiTextBox textBox)
        {
            // 数値のみ入力可能
            if (!ValChk.IsNumber(textBox.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 入力サイズ値
            if (!ValChk.IsWithinLength(textBox.Text, textBox.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 必須項目のチェック
            if (this.txtShainCd.Text == "0")
            {
                Msg.Notice("ゼロは使用できません。");
                this.txtShainCd.Focus();
                return false;
            }

            if (MODE_NEW.Equals(this.Par1))
            {
                // 社員コードのチェック
                if (!IsValidShainCd())
                {
                    this.txtShainCd.Focus();
                    return false;
                }
            }

            #region 年調情報のチェック
            // 社会保険料申告分
            if (!IsValidCurrency(this.txtShakaiHokenryoKjgkSnkkbn))
            {
                this.txtShakaiHokenryoKjgkSnkkbn.Focus();
                return false;
            }
                        // 小規模企業共済掛金
            if (!IsValidCurrency(this.txtSkbKigyoKyosaiKakekinKjgk))
            {
                this.txtSkbKigyoKyosaiKakekinKjgk.Focus();
                return false;
            }
            // 生命保険料
            if (!IsValidCurrency(this.txtSeimeihokenryoKojogaku))
            {
                this.txtSeimeihokenryoKojogaku.Focus();
                return false;
            }
            // 地震保険料
            if (!IsValidCurrency(this.txtSongaihokenryoKojogaku))
            {
                this.txtSongaihokenryoKojogaku.Focus();
                return false;
            }
            // 配偶者特別
            if (!IsValidCurrency(this.txtHaigushaTokubetsuKojogaku))
            {
                this.txtHaigushaTokubetsuKojogaku.Focus();
                return false;
            }
            // 住宅借入金等特別
            if (!IsValidCurrency(this.txtJutakuShutokutoTkbtKojogaku))
            {
                this.txtJutakuShutokutoTkbtKojogaku.Focus();
                return false;
            }
            // 住宅借入金等年末残高(1回目)
            if (!IsValidCurrency(this.txtJutakuKariireToNenmatsuZan1))
            {
                this.txtJutakuKariireToNenmatsuZan1.Focus();
                return false;
            }
            // 住宅借入金等年末残高(2回目)
            if (!IsValidCurrency(this.txtJutakuKariireToNenmatsuZan2))
            {
                this.txtJutakuKariireToNenmatsuZan2.Focus();
                return false;
            }
            // 居住開始年月日 補正済みのため省略
            // 配偶者合計所得
            if (!IsValidCurrency(this.txtHaigushaGokeiShotoku))
            {
                this.txtHaigushaGokeiShotoku.Focus();
                return false;
            }
            // 新生命保険料
            if (!IsValidCurrency(this.txtShinSeimeiHokenryo))
            {
                this.txtShinSeimeiHokenryo.Focus();
                return false;
            }
            // 旧生命保険料
            if (!IsValidCurrency(this.txtKyuSeimeiHokenryo))
            {
                this.txtKyuSeimeiHokenryo.Focus();
                return false;
            }
            // 介護医療保険料
            if (!IsValidCurrency(this.txtKaigoIryoHokenryo))
            {
                this.txtKaigoIryoHokenryo.Focus();
                return false;
            }
            // 新個人年金保険料
            if (!IsValidCurrency(this.txtShinKojinNenkinHokenryo))
            {
                this.txtShinKojinNenkinHokenryo.Focus();
                return false;
            }
            // 旧個人年金保険料
            if (!IsValidCurrency(this.txtKojinNenkinHokenryoShrigk))
            {
                this.txtKojinNenkinHokenryoShrigk.Focus();
                return false;
            }
            // 旧長期損害保険料
            if (!IsValidCurrency(this.txtChokiSongaiHokenryoShrigk))
            {
                this.txtChokiSongaiHokenryoShrigk.Focus();
                return false;
            }
            // 国民年金保険料
            if (!IsValidCurrency(this.txtKokuminNenkinHokenryoShrigk))
            {
                this.txtKokuminNenkinHokenryoShrigk.Focus();
                return false;
            }
            // 前職課税分給与額
            if (!IsValidCurrency(this.txtZenshokubunKazeiKyuyogaku))
            {
                this.txtZenshokubunKazeiKyuyogaku.Focus();
                return false;
            }
            // 前職社会保険料
            if (!IsValidCurrency(this.txtZenshokubunShakaiHokenryo))
            {
                this.txtZenshokubunShakaiHokenryo.Focus();
                return false;
            }
            // 前職所得税額
            if (!IsValidCurrency(this.txtZenshokubunShotokuzeigaku))
            {
                this.txtZenshokubunShotokuzeigaku.Focus();
                return false;
            }
            // 給与調整課税分給与
            if (!IsValidCurrency(this.txtChoseiKyuyoKazeiKyuyogaku))
            {
                this.txtChoseiKyuyoKazeiKyuyogaku.Focus();
                return false;
            }
            // 給与調整社会保険料
            if (!IsValidCurrency(this.txtChoseiKyuyoShakaiHokenryo))
            {
                this.txtChoseiKyuyoShakaiHokenryo.Focus();
                return false;
            }
            // 給与調整所得税額
            if (!IsValidCurrency(this.txtChoseiKyuyoShotokuzeigaku))
            {
                this.txtChoseiKyuyoShotokuzeigaku.Focus();
                return false;
            }
            // 賞与調整課税分給与
            if (!IsValidCurrency(this.txtChoseiShoyoKazeiKyuyogaku))
            {
                this.txtChoseiShoyoKazeiKyuyogaku.Focus();
                return false;
            }
            // 賞与調整社会保険料
            if (!IsValidCurrency(this.txtChoseiShoyoShakaiHokenryo))
            {
                this.txtChoseiShoyoShakaiHokenryo.Focus();
                return false;
            }
            // 賞与調整所得税額
            if (!IsValidCurrency(this.txtChoseiShoyoShotokuzeigaku))
            {
                this.txtChoseiShoyoShotokuzeigaku.Focus();
                return false;
            }
            #endregion

            #region 所得税のチェック

            // 摘要①のチェック
            if (!IsValidTekiyo1())
            {
                this.txtTekiyo1.Focus();
                return false;
            }
            // 摘要②のチェック
            if (!IsValidTekiyo2())
            {
                this.txtTekiyo2.Focus();
                return false;
            }
            
            #endregion

            return true;
        }

        #endregion
    }
}
