﻿namespace jp.co.fsi.ky.kycm1071
{
    partial class KYCM1073
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblYakushokuCdFr = new System.Windows.Forms.Label();
            this.lblCodeBet1 = new System.Windows.Forms.Label();
            this.txtYakushokuCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtYakushokuCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblYakushokuCdTo = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pnlDebug.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Size = new System.Drawing.Size(705, 23);
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(8, 61);
            this.pnlDebug.Size = new System.Drawing.Size(550, 100);
            // 
            // lblYakushokuCdFr
            // 
            this.lblYakushokuCdFr.BackColor = System.Drawing.Color.Silver;
            this.lblYakushokuCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblYakushokuCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblYakushokuCdFr.Location = new System.Drawing.Point(70, 38);
            this.lblYakushokuCdFr.Name = "lblYakushokuCdFr";
            this.lblYakushokuCdFr.Size = new System.Drawing.Size(180, 19);
            this.lblYakushokuCdFr.TabIndex = 910;
            this.lblYakushokuCdFr.Text = "先  頭";
            this.lblYakushokuCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCodeBet1
            // 
            this.lblCodeBet1.AutoSize = true;
            this.lblCodeBet1.Location = new System.Drawing.Point(256, 42);
            this.lblCodeBet1.Name = "lblCodeBet1";
            this.lblCodeBet1.Size = new System.Drawing.Size(20, 13);
            this.lblCodeBet1.TabIndex = 911;
            this.lblCodeBet1.Text = "～";
            // 
            // txtYakushokuCdFr
            // 
            this.txtYakushokuCdFr.AutoSizeFromLength = false;
            this.txtYakushokuCdFr.DisplayLength = null;
            this.txtYakushokuCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtYakushokuCdFr.Location = new System.Drawing.Point(16, 38);
            this.txtYakushokuCdFr.MaxLength = 4;
            this.txtYakushokuCdFr.Name = "txtYakushokuCdFr";
            this.txtYakushokuCdFr.Size = new System.Drawing.Size(48, 20);
            this.txtYakushokuCdFr.TabIndex = 907;
            this.txtYakushokuCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtYakushokuCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtYakushokuCdFr_Validating);
            // 
            // txtYakushokuCdTo
            // 
            this.txtYakushokuCdTo.AutoSizeFromLength = false;
            this.txtYakushokuCdTo.DisplayLength = null;
            this.txtYakushokuCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtYakushokuCdTo.Location = new System.Drawing.Point(279, 38);
            this.txtYakushokuCdTo.MaxLength = 4;
            this.txtYakushokuCdTo.Name = "txtYakushokuCdTo";
            this.txtYakushokuCdTo.Size = new System.Drawing.Size(48, 20);
            this.txtYakushokuCdTo.TabIndex = 908;
            this.txtYakushokuCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtYakushokuCdTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LastControl_KeyDown);
            this.txtYakushokuCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtYakushokuCdTo_Validating);
            // 
            // lblYakushokuCdTo
            // 
            this.lblYakushokuCdTo.BackColor = System.Drawing.Color.Silver;
            this.lblYakushokuCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblYakushokuCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblYakushokuCdTo.Location = new System.Drawing.Point(333, 38);
            this.lblYakushokuCdTo.Name = "lblYakushokuCdTo";
            this.lblYakushokuCdTo.Size = new System.Drawing.Size(180, 19);
            this.lblYakushokuCdTo.TabIndex = 909;
            this.lblYakushokuCdTo.Text = "最　後";
            this.lblYakushokuCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblYakushokuCdFr);
            this.groupBox1.Controls.Add(this.lblYakushokuCdTo);
            this.groupBox1.Controls.Add(this.txtYakushokuCdTo);
            this.groupBox1.Controls.Add(this.txtYakushokuCdFr);
            this.groupBox1.Controls.Add(this.lblCodeBet1);
            this.groupBox1.Font = new System.Drawing.Font("MS UI Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(529, 90);
            this.groupBox1.TabIndex = 912;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "役職コード範囲";
            // 
            // KYCM1073
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(545, 167);
            this.Controls.Add(this.groupBox1);
            this.Name = "KYCM1073";
            this.Text = "役職の印刷";
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.pnlDebug.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblYakushokuCdFr;
        private System.Windows.Forms.Label lblCodeBet1;
        private common.controls.FsiTextBox txtYakushokuCdFr;
        private common.controls.FsiTextBox txtYakushokuCdTo;
        private System.Windows.Forms.Label lblYakushokuCdTo;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}