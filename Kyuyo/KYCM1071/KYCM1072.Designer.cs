﻿namespace jp.co.fsi.ky.kycm1071
{
    partial class KYCM1072
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtYakushokuCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblYakushokuCd = new System.Windows.Forms.Label();
            this.lblMODE = new System.Windows.Forms.Label();
            this.txtYakushokuKanaNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblYakushokuKanaNm = new System.Windows.Forms.Label();
            this.txtYakushokuNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblYakushokuNm = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Size = new System.Drawing.Size(382, 23);
            this.lblTitle.Text = "役職の登録";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(5, 53);
            this.pnlDebug.Size = new System.Drawing.Size(415, 100);
            // 
            // txtYakushokuCd
            // 
            this.txtYakushokuCd.AutoSizeFromLength = true;
            this.txtYakushokuCd.DisplayLength = null;
            this.txtYakushokuCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtYakushokuCd.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtYakushokuCd.Location = new System.Drawing.Point(127, 13);
            this.txtYakushokuCd.MaxLength = 4;
            this.txtYakushokuCd.Name = "txtYakushokuCd";
            this.txtYakushokuCd.Size = new System.Drawing.Size(64, 23);
            this.txtYakushokuCd.TabIndex = 0;
            this.txtYakushokuCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtYakushokuCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtYakushokuCd_Validating);
            // 
            // lblYakushokuCd
            // 
            this.lblYakushokuCd.BackColor = System.Drawing.Color.Silver;
            this.lblYakushokuCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblYakushokuCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblYakushokuCd.Location = new System.Drawing.Point(12, 13);
            this.lblYakushokuCd.Name = "lblYakushokuCd";
            this.lblYakushokuCd.Size = new System.Drawing.Size(115, 23);
            this.lblYakushokuCd.TabIndex = 1;
            this.lblYakushokuCd.Text = "役職コード";
            this.lblYakushokuCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMODE
            // 
            this.lblMODE.BackColor = System.Drawing.Color.White;
            this.lblMODE.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMODE.Location = new System.Drawing.Point(318, 13);
            this.lblMODE.Name = "lblMODE";
            this.lblMODE.Size = new System.Drawing.Size(78, 23);
            this.lblMODE.TabIndex = 902;
            this.lblMODE.Text = "【編集】";
            this.lblMODE.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtYakushokuKanaNm
            // 
            this.txtYakushokuKanaNm.AutoSizeFromLength = false;
            this.txtYakushokuKanaNm.DisplayLength = null;
            this.txtYakushokuKanaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtYakushokuKanaNm.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtYakushokuKanaNm.Location = new System.Drawing.Point(127, 70);
            this.txtYakushokuKanaNm.MaxLength = 30;
            this.txtYakushokuKanaNm.Name = "txtYakushokuKanaNm";
            this.txtYakushokuKanaNm.Size = new System.Drawing.Size(269, 23);
            this.txtYakushokuKanaNm.TabIndex = 905;
            this.txtYakushokuKanaNm.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LastControl_KeyDown);
            this.txtYakushokuKanaNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtYakushokuKanaNm_Validating);
            // 
            // lblYakushokuKanaNm
            // 
            this.lblYakushokuKanaNm.BackColor = System.Drawing.Color.Silver;
            this.lblYakushokuKanaNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblYakushokuKanaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblYakushokuKanaNm.Location = new System.Drawing.Point(12, 70);
            this.lblYakushokuKanaNm.Name = "lblYakushokuKanaNm";
            this.lblYakushokuKanaNm.Size = new System.Drawing.Size(115, 23);
            this.lblYakushokuKanaNm.TabIndex = 906;
            this.lblYakushokuKanaNm.Text = "カナ名";
            this.lblYakushokuKanaNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtYakushokuNm
            // 
            this.txtYakushokuNm.AutoSizeFromLength = false;
            this.txtYakushokuNm.DisplayLength = null;
            this.txtYakushokuNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtYakushokuNm.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtYakushokuNm.Location = new System.Drawing.Point(127, 47);
            this.txtYakushokuNm.MaxLength = 30;
            this.txtYakushokuNm.Name = "txtYakushokuNm";
            this.txtYakushokuNm.Size = new System.Drawing.Size(269, 23);
            this.txtYakushokuNm.TabIndex = 903;
            this.txtYakushokuNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtYakushokuNm_Validating);
            // 
            // lblYakushokuNm
            // 
            this.lblYakushokuNm.BackColor = System.Drawing.Color.Silver;
            this.lblYakushokuNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblYakushokuNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblYakushokuNm.Location = new System.Drawing.Point(12, 47);
            this.lblYakushokuNm.Name = "lblYakushokuNm";
            this.lblYakushokuNm.Size = new System.Drawing.Size(115, 23);
            this.lblYakushokuNm.TabIndex = 904;
            this.lblYakushokuNm.Text = "役職名";
            this.lblYakushokuNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // KYCM1072
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(407, 156);
            this.Controls.Add(this.txtYakushokuKanaNm);
            this.Controls.Add(this.lblYakushokuKanaNm);
            this.Controls.Add(this.txtYakushokuNm);
            this.Controls.Add(this.lblYakushokuNm);
            this.Controls.Add(this.lblMODE);
            this.Controls.Add(this.txtYakushokuCd);
            this.Controls.Add(this.lblYakushokuCd);
            this.Name = "KYCM1072";
            this.Text = "役職の登録";
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblYakushokuCd, 0);
            this.Controls.SetChildIndex(this.txtYakushokuCd, 0);
            this.Controls.SetChildIndex(this.lblMODE, 0);
            this.Controls.SetChildIndex(this.lblYakushokuNm, 0);
            this.Controls.SetChildIndex(this.txtYakushokuNm, 0);
            this.Controls.SetChildIndex(this.lblYakushokuKanaNm, 0);
            this.Controls.SetChildIndex(this.txtYakushokuKanaNm, 0);
            this.pnlDebug.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private jp.co.fsi.common.controls.FsiTextBox txtYakushokuCd;
        private System.Windows.Forms.Label lblYakushokuCd;
        private System.Windows.Forms.Label lblMODE;
        private common.controls.FsiTextBox txtYakushokuKanaNm;
        private System.Windows.Forms.Label lblYakushokuKanaNm;
        private common.controls.FsiTextBox txtYakushokuNm;
        private System.Windows.Forms.Label lblYakushokuNm;
    }
}