﻿using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;
using System.Reflection;
using System;

namespace jp.co.fsi.ky.kycm1071
{
    /// <summary>
    /// 役職の登録(KYCM1071)
    /// </summary>
    public partial class KYCM1071 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// モード(コード検索)
        /// </summary>
        private const string MODE_CD_SRC = "1";

        /// <summary>
        /// 検索画面用画面タイトル
        /// </summary>
        private const string SEARCH_TITLE = "役職の検索";
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KYCM1071()
        {
            InitializeComponent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                // Par1が"1"の場合、コード検索画面としての挙動をする
                // フォームのキャプションにラベルタイトルのtextを設定する
                this.Text = SEARCH_TITLE;
                // サイズを縮める
                this.Size = new Size(600, 500);
                // EscapeとF1のみ表示
                this.btnEsc.Location = this.btnF1.Location;
                this.btnF1.Location = this.btnF2.Location;
                this.btnF2.Visible = false;
                this.btnF3.Visible = false;
                this.btnF4.Visible = false;
                this.btnF5.Visible = false;
                this.btnF6.Visible = false;
                this.btnF7.Visible = false;
                this.btnF8.Visible = false;
                this.btnF9.Visible = false;
                this.btnF10.Visible = false;
                this.btnF11.Visible = false;
                this.btnF12.Visible = false;
                this.ShowFButton = true;
            }

            // データ表示
            SearchData();

            // カナ名にフォーカス
            this.txtKanaName.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtKanaName.Focus();
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                // Par1が"1"の場合、ダイアログとしての処理結果を返却する
                this.DialogResult = DialogResult.Cancel;
            }
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        public override void PressF1()
        {
            // カナ名にフォーカスを戻す
            this.txtKanaName.Focus();
            this.txtKanaName.SelectAll();
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // メンテ機能で立ち上げている場合のみ役職登録画面を立ち上げる
            if (ValChk.IsEmpty(this.Par1))
            {
                // 役職登録画面の起動
                EditYakushoku(string.Empty);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF5();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF5()
        {
            // 摘要の印刷画面を立ち上げる
            KYCM1073 frmKYCM1073 = new KYCM1073();
            frmKYCM1073.ShowDialog(this);
        }

        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            // 設定画面の起動
            // MEMO:原則としてここで渡す帳票IDの設定はReport.csvに保持していることが前提ですが、
            // 保持していない場合は、設定画面での保存(F6)時に新規に設定が保持されます。
            PrintSettingForm psForm = new PrintSettingForm(new string[1] { "KYCM1071R" });
            psForm.ShowDialog();
        }
        #endregion

        #region イベント

        /// <summary>
        /// カナ名検証時の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKanaName_Validating(object sender, CancelEventArgs e)
        {
            // 入力された情報を元に検索する
            SearchData();
        }

        /// <summary>
        /// グリッドでのキーダウン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (MODE_CD_SRC.Equals(this.Par1))
                {
                    ReturnVal();
                }
                else
                {
                    EditYakushoku(Util.ToString(this.dgvList.SelectedRows[0].Cells["役職コード"].Value));
                    e.Handled = true;
                }
            }
        }

        /// <summary>
        /// グリッドのセルダブルクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                ReturnVal();
            }
            else
            {
                EditYakushoku(Util.ToString(this.dgvList.SelectedRows[0].Cells["役職コード"].Value));
            }
        }

        #endregion

        #region privateメソッド

        /// <summary>
        /// データを検索する
        /// </summary>
        private void SearchData()
        {
            // 役職マスタからデータを取得して表示
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
            string where = "KAISHA_CD = @KAISHA_CD";
            // カナ名が入力されている場合
            if (!ValChk.IsEmpty(this.txtKanaName.Text))
            {
                where += " AND YAKUSHOKU_KANA_NM LIKE @YAKUSHOKU_KANA_NM";
                dpc.SetParam("@YAKUSHOKU_KANA_NM", SqlDbType.VarChar, 30 + 2, "%" + this.txtKanaName.Text + "%");
            }
            string cols = "YAKUSHOKU_CD AS 役職コード, YAKUSHOKU_NM AS 役職名, YAKUSHOKU_KANA_NM AS 役職カナ名";
            string from = "TB_KY_YAKUSHOKU";
            string order = "YAKUSHOKU_CD";

            DataTable dt = this.Dba.GetDataTableByConditionWithParams(cols, from, where, order, dpc);

            // 初期処理以外の場合、該当データがなければエラーメッセージを表示
            if (dt.Rows.Count == 0)
            {
                Msg.Info("該当データがありません。");

                dt.Rows.Add(dt.NewRow());
            }

            this.dgvList.DataSource = dt;

            // ユーザーによるソートを禁止させる
            foreach (DataGridViewColumn c in this.dgvList.Columns)
                c.SortMode = DataGridViewColumnSortMode.NotSortable;

            // フォントを設定する
            this.dgvList.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F, FontStyle.Regular);
            this.dgvList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.dgvList.DefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F);

            // 列幅を設定する
            this.dgvList.Columns[0].Width = 110;
            this.dgvList.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvList.Columns[1].Width = 200;
            this.dgvList.Columns[2].Width = 200;
        }

        /// <summary>
        /// 役職を追加編集する
        /// </summary>
        /// <param name="code">役職コード(空：新規登録、以外：編集)</param>
        private void EditYakushoku(string code)
        {
            KYCM1072 frm;

            if (ValChk.IsEmpty(code))
            {
                // 新規登録モードで登録画面を起動
                frm = new KYCM1072("1");
            }
            else
            {
                // 編集モードで登録画面を起動
                frm = new KYCM1072("2");
                frm.InData = code;
            }

            DialogResult result = frm.ShowDialog(this);

            if (result == DialogResult.OK)
            {
                // データを再検索する
                SearchData();
                // 元々選択していたデータを選択
                for (int i = 0; i < this.dgvList.Rows.Count; i++)
                {
                    if (code.Equals(Util.ToString(this.dgvList.Rows[i].Cells["役職コード"].Value)))
                    {
                        this.dgvList.Rows[i].Selected = true;
                        break;
                    }
                }
                // Gridに再度フォーカスをセット
                this.ActiveControl = this.dgvList;
                this.dgvList.Focus();
            }
        }

        /// <summary>
        /// 呼び出し元に戻り値を返す
        /// </summary>
        private void ReturnVal()
        {
            this.OutData = new string[3] { 
                Util.ToString(this.dgvList.SelectedRows[0].Cells["役職コード"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["役職名"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["役職カナ名"].Value)
            };
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        #endregion
    }
}
