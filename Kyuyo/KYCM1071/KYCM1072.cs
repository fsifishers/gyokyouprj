﻿using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.ky.kycm1071
{
    /// <summary>
    /// 役職の登録(KYCM1072)
    /// </summary>
    public partial class KYCM1072 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// モード(新規)
        /// </summary>
        private const string MODE_NEW = "1";

        /// <summary>
        /// モード(編集)
        /// </summary>
        private const string MODE_EDIT = "2";
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KYCM1072()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="par1">引数1</param>
        public KYCM1072(string par1)
            : base(par1)
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // タイトルは非表示
            this.lblTitle.Visible = false;

            // 引数：Par1／モード(1:新規、2:変更)、InData：役職コード
            if (MODE_NEW.Equals(this.Par1))
            {
                // 新規モードの初期表示
                InitDispOnNew();
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // 編集モードの初期表示
                InitDispOnEdit();
            }
            else
            {
                // 不正な起動として閉じる
                Msg.Error("不正な起動です。終了します。");
                this.Close();
            }
            this.ShowFButton = true;

            // ボタンを調整
            this.btnF1.Visible = true;
            this.btnF2.Visible = false;
            this.btnF3.Visible = true;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF6.Visible = true;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;

            this.btnEsc.Location = this.btnF1.Location;
            this.btnF1.Location = this.btnF2.Location;
            this.btnF6.Location = this.btnF4.Location;
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F3キー押下時処理
        /// </summary>
        public override void PressF3()
        {
            // 新規モードの場合は処理させない
            if (this.Par1.Equals(MODE_NEW)) return;

            // 確認メッセージを表示
            string msg = "削除しますか？";
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            // 削除用パラメータ
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
            dpc.SetParam("@YAKUSHOKU_CD", SqlDbType.Decimal, 4, Util.ToString(this.InData));

            try
            {
                // トランザクション開始
                this.Dba.BeginTransaction();

                if (MODE_EDIT.Equals(this.Par1))
                {
                    // データ削除
                    // 給与役職マスタ
                    this.Dba.Delete("TB_KY_YAKUSHOKU", "KAISHA_CD = @KAISHA_CD AND YAKUSHOKU_CD = @YAKUSHOKU_CD", dpc);
                }

                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        
        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        public override void PressF6()
        {
            // 確認メッセージを表示
            string msg = (MODE_NEW.Equals(this.Par1) ? "登録" : "更新") + "しますか？";
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            // 入力値をバインドパラメータとしてセットする
            ArrayList alParamsKyYakushoku = SetKyYakushokuParams();

            try
            {
                // トランザクション開始
                this.Dba.BeginTransaction();

                if (MODE_NEW.Equals(this.Par1))
                {
                    // データ登録
                    // 給与役職マスタ
                    this.Dba.Insert("TB_KY_YAKUSHOKU", (DbParamCollection)alParamsKyYakushoku[0]);
                }
                else if (MODE_EDIT.Equals(this.Par1))
                {
                    // データ更新
                    // 給与役職マスタ
                    this.Dba.Update("TB_KY_YAKUSHOKU",
                        (DbParamCollection)alParamsKyYakushoku[1],
                        "KAISHA_CD = @KAISHA_CD AND YAKUSHOKU_CD = @YAKUSHOKU_CD",
                        (DbParamCollection)alParamsKyYakushoku[0]);
                }

                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 役職コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtYakushokuCd_Validating(object sender, CancelEventArgs e)
        {
            // 入力値補正
            this.txtYakushokuCd.Text = Util.ToString(Util.ToDecimal(this.txtYakushokuCd.Text));

            if (!IsValidYakushokuCd())
            {
                e.Cancel = true;
                this.txtYakushokuCd.SelectAll();
            }
        }

        /// <summary>
        /// 役職名の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtYakushokuNm_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidYakushokuNm())
            {
                e.Cancel = true;
                this.txtYakushokuNm.SelectAll();
            }
        }

        /// <summary>
        /// カナ名の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtYakushokuKanaNm_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidYakushokuKanaNm())
            {
                e.Cancel = true;
                this.txtYakushokuKanaNm.SelectAll();
            }
        }

        /// <summary>
        /// 画面の最後のコントールでのキーダウン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LastControl_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.btnF6.Enabled)
            {
                this.PressF6();
            }
        }
        #endregion

        #region privateメソッド

        /// <summary>
        /// 新規モードの初期表示
        /// </summary>
        private void InitDispOnNew()
        {
            // 初期値
            this.txtYakushokuCd.Text="0";

            // 追加・変更状態表示
            this.lblMODE.Text = "【追加】";

            // 役職コードに初期フォーカス
            this.ActiveControl = this.txtYakushokuCd;
            this.txtYakushokuCd.Focus();
        }

        /// <summary>
        /// 編集モードの初期表示
        /// </summary>
        private void InitDispOnEdit()
        {
            // 現在DBに登録されている値、入力制御を実装
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
            dpc.SetParam("@YAKUSHOKU_CD", SqlDbType.Decimal, 4, Util.ToString(this.InData));
            string cols = "YAKUSHOKU_CD, YAKUSHOKU_NM, YAKUSHOKU_KANA_NM";
            string from = "TB_KY_YAKUSHOKU";
            string where = "KAISHA_CD = @KAISHA_CD AND YAKUSHOKU_CD = @YAKUSHOKU_CD";
            DataTable dt = this.Dba.GetDataTableByConditionWithParams(cols, from, where, dpc);
            if (dt.Rows.Count == 0)
            {
                Msg.Error("不正な起動です。終了します。");
                this.Close();
            }

            // 取得した内容を表示
            DataRow drDispData = dt.Rows[0];
            this.txtYakushokuCd.Text = Util.ToString(drDispData["YAKUSHOKU_CD"]);
            this.txtYakushokuNm.Text = Util.ToString(drDispData["YAKUSHOKU_NM"]);
            this.txtYakushokuKanaNm.Text = Util.ToString(drDispData["YAKUSHOKU_KANA_NM"]);

            // 追加・変更状態表示
            this.lblMODE.Text = "【変更】";

            // 役職コードは入力不可
            this.lblYakushokuCd.Enabled = false;
            this.txtYakushokuCd.Enabled = false;
        }

        /// <summary>
        /// 役職コードの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidYakushokuCd()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtYakushokuCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 既に存在するコードを入力した場合はエラーとする
            if (this.txtYakushokuCd.Text != "0")
            {
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
                dpc.SetParam("@YAKUSHOKU_CD", SqlDbType.Decimal, 4, this.txtYakushokuCd.Text);
                string cols = "YAKUSHOKU_CD";
                string from = "TB_KY_YAKUSHOKU";
                string where = "KAISHA_CD = @KAISHA_CD AND YAKUSHOKU_CD = @YAKUSHOKU_CD";
                DataTable dt = this.Dba.GetDataTableByConditionWithParams(cols, from, where, dpc);
                if (dt.Rows.Count > 0)
                {
                    Msg.Error("既に存在する役職コードと重複しています。");
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// 役職名の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidYakushokuNm()
        {
            // 指定バイト数を超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtYakushokuNm.Text, this.txtYakushokuNm.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// カナ名の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidYakushokuKanaNm()
        {
            // 指定バイト数を超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtYakushokuKanaNm.Text, this.txtYakushokuKanaNm.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 必須項目のチェック
            if (this.txtYakushokuCd.Text == "0")
            {
                Msg.Notice("ゼロは使用できません。");
                this.txtYakushokuCd.Focus();
                return false;
            }

            if (MODE_NEW.Equals(this.Par1))
            {
                // 役職コードのチェック
                if (!IsValidYakushokuCd())
                {
                    this.txtYakushokuCd.Focus();
                    return false;
                }
            }

            // 役職名のチェック
            if (!IsValidYakushokuNm())
            {
                this.txtYakushokuNm.Focus();
                return false;
            }

            // カナ名のチェック
            if (!IsValidYakushokuKanaNm())
            {
                this.txtYakushokuKanaNm.Focus();
                return false;
            }

            return true;
        }

        /// <summary>
        /// TB_KY_YAKUSHOKUに更新するためのパラメータ設定をします。
        /// </summary>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// 更新処理：DbParamCollection*2(Where句,Set句)
        /// </returns>
        private ArrayList SetKyYakushokuParams()
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            if (MODE_NEW.Equals(this.Par1))
            {
                // 会社コード
                updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
                // 役職コード
                updParam.SetParam("@YAKUSHOKU_CD", SqlDbType.Decimal, 4, this.txtYakushokuCd.Text);
                // 登録日
                updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWDATE");
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // WHERE句のパラメータに設定
                DbParamCollection whereParam = new DbParamCollection();
                whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
                whereParam.SetParam("@YAKUSHOKU_CD", SqlDbType.Decimal, 4, this.txtYakushokuCd.Text);
                alParams.Add(whereParam);
            }

            // 役職名
            updParam.SetParam("@YAKUSHOKU_NM", SqlDbType.VarChar, 30, this.txtYakushokuNm.Text);
            // カナ名
            updParam.SetParam("@YAKUSHOKU_KANA_NM", SqlDbType.VarChar, 30, this.txtYakushokuKanaNm.Text);
            // 更新日
            updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWDATE");

            alParams.Add(updParam);

            return alParams;
        }

        #endregion

    }
}
