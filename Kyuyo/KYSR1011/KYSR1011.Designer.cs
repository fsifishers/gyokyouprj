﻿namespace jp.co.fsi.ky.kysr1011
{
    partial class KYSR1011
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpNengetsu = new System.Windows.Forms.GroupBox();
            this.lblMonthNengetsu = new System.Windows.Forms.Label();
            this.lblYearNengetsu = new System.Windows.Forms.Label();
            this.txtMonthNengetsu = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblGengoNengetsu = new System.Windows.Forms.Label();
            this.txtGengoYearNengetsu = new jp.co.fsi.common.controls.FsiTextBox();
            this.grpTaisho = new System.Windows.Forms.GroupBox();
            this.lblMonthTaisho = new System.Windows.Forms.Label();
            this.lblYearTaisho = new System.Windows.Forms.Label();
            this.txtMonthTaisho = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblGengoTaisho = new System.Windows.Forms.Label();
            this.txtGengoYearTaisho = new jp.co.fsi.common.controls.FsiTextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.grpBukaCd = new System.Windows.Forms.GroupBox();
            this.lblBukaCdTo = new System.Windows.Forms.Label();
            this.txtBukaCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.lblBukaCdFr = new System.Windows.Forms.Label();
            this.txtBukaCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.grpBumonCd = new System.Windows.Forms.GroupBox();
            this.lblBumonCdTo = new System.Windows.Forms.Label();
            this.txtBumonCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lblBumonCdFr = new System.Windows.Forms.Label();
            this.txtBumonCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.grpShainCd = new System.Windows.Forms.GroupBox();
            this.lblShainCdTo = new System.Windows.Forms.Label();
            this.txtShainCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblCodeBet = new System.Windows.Forms.Label();
            this.lblShainCdFr = new System.Windows.Forms.Label();
            this.txtShainCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.grpOrder = new System.Windows.Forms.GroupBox();
            this.rdoOrder2 = new System.Windows.Forms.RadioButton();
            this.rdoOrder1 = new System.Windows.Forms.RadioButton();
            this.grpCopy = new System.Windows.Forms.GroupBox();
            this.rdoCopy2 = new System.Windows.Forms.RadioButton();
            this.rdoCopy1 = new System.Windows.Forms.RadioButton();
            this.pnlDebug.SuspendLayout();
            this.grpNengetsu.SuspendLayout();
            this.grpTaisho.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.grpBukaCd.SuspendLayout();
            this.grpBumonCd.SuspendLayout();
            this.grpShainCd.SuspendLayout();
            this.grpOrder.SuspendLayout();
            this.grpCopy.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Text = "賞与明細印刷";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Size = new System.Drawing.Size(847, 100);
            // 
            // grpNengetsu
            // 
            this.grpNengetsu.Controls.Add(this.lblMonthNengetsu);
            this.grpNengetsu.Controls.Add(this.lblYearNengetsu);
            this.grpNengetsu.Controls.Add(this.txtMonthNengetsu);
            this.grpNengetsu.Controls.Add(this.lblGengoNengetsu);
            this.grpNengetsu.Controls.Add(this.txtGengoYearNengetsu);
            this.grpNengetsu.Font = new System.Drawing.Font("ＭＳ ゴシック", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.grpNengetsu.Location = new System.Drawing.Point(12, 57);
            this.grpNengetsu.Name = "grpNengetsu";
            this.grpNengetsu.Size = new System.Drawing.Size(204, 53);
            this.grpNengetsu.TabIndex = 0;
            this.grpNengetsu.TabStop = false;
            this.grpNengetsu.Text = "支給年月";
            // 
            // lblMonthNengetsu
            // 
            this.lblMonthNengetsu.AutoSize = true;
            this.lblMonthNengetsu.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMonthNengetsu.Location = new System.Drawing.Point(156, 20);
            this.lblMonthNengetsu.Name = "lblMonthNengetsu";
            this.lblMonthNengetsu.Size = new System.Drawing.Size(24, 16);
            this.lblMonthNengetsu.TabIndex = 4;
            this.lblMonthNengetsu.Text = "月";
            // 
            // lblYearNengetsu
            // 
            this.lblYearNengetsu.AutoSize = true;
            this.lblYearNengetsu.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblYearNengetsu.Location = new System.Drawing.Point(91, 20);
            this.lblYearNengetsu.Name = "lblYearNengetsu";
            this.lblYearNengetsu.Size = new System.Drawing.Size(24, 16);
            this.lblYearNengetsu.TabIndex = 2;
            this.lblYearNengetsu.Text = "年";
            // 
            // txtMonthNengetsu
            // 
            this.txtMonthNengetsu.AutoSizeFromLength = false;
            this.txtMonthNengetsu.DisplayLength = null;
            this.txtMonthNengetsu.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMonthNengetsu.Location = new System.Drawing.Point(116, 18);
            this.txtMonthNengetsu.MaxLength = 2;
            this.txtMonthNengetsu.Name = "txtMonthNengetsu";
            this.txtMonthNengetsu.Size = new System.Drawing.Size(34, 23);
            this.txtMonthNengetsu.TabIndex = 3;
            this.txtMonthNengetsu.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonthNengetsu.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthNengetsu_Validating);
            // 
            // lblGengoNengetsu
            // 
            this.lblGengoNengetsu.AutoSize = true;
            this.lblGengoNengetsu.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGengoNengetsu.Location = new System.Drawing.Point(8, 20);
            this.lblGengoNengetsu.Name = "lblGengoNengetsu";
            this.lblGengoNengetsu.Size = new System.Drawing.Size(40, 16);
            this.lblGengoNengetsu.TabIndex = 0;
            this.lblGengoNengetsu.Text = "平成";
            // 
            // txtGengoYearNengetsu
            // 
            this.txtGengoYearNengetsu.AutoSizeFromLength = false;
            this.txtGengoYearNengetsu.DisplayLength = null;
            this.txtGengoYearNengetsu.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGengoYearNengetsu.Location = new System.Drawing.Point(51, 18);
            this.txtGengoYearNengetsu.MaxLength = 2;
            this.txtGengoYearNengetsu.Name = "txtGengoYearNengetsu";
            this.txtGengoYearNengetsu.Size = new System.Drawing.Size(34, 23);
            this.txtGengoYearNengetsu.TabIndex = 1;
            this.txtGengoYearNengetsu.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGengoYearNengetsu.Validating += new System.ComponentModel.CancelEventHandler(this.txtGengoYearNengetsu_Validating);
            // 
            // grpTaisho
            // 
            this.grpTaisho.Controls.Add(this.lblMonthTaisho);
            this.grpTaisho.Controls.Add(this.lblYearTaisho);
            this.grpTaisho.Controls.Add(this.txtMonthTaisho);
            this.grpTaisho.Controls.Add(this.lblGengoTaisho);
            this.grpTaisho.Controls.Add(this.txtGengoYearTaisho);
            this.grpTaisho.Font = new System.Drawing.Font("ＭＳ ゴシック", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.grpTaisho.Location = new System.Drawing.Point(12, 116);
            this.grpTaisho.Name = "grpTaisho";
            this.grpTaisho.Size = new System.Drawing.Size(204, 53);
            this.grpTaisho.TabIndex = 1;
            this.grpTaisho.TabStop = false;
            this.grpTaisho.Text = "対象年月";
            // 
            // lblMonthTaisho
            // 
            this.lblMonthTaisho.AutoSize = true;
            this.lblMonthTaisho.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMonthTaisho.Location = new System.Drawing.Point(156, 22);
            this.lblMonthTaisho.Name = "lblMonthTaisho";
            this.lblMonthTaisho.Size = new System.Drawing.Size(24, 16);
            this.lblMonthTaisho.TabIndex = 17;
            this.lblMonthTaisho.Text = "月";
            // 
            // lblYearTaisho
            // 
            this.lblYearTaisho.AutoSize = true;
            this.lblYearTaisho.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblYearTaisho.Location = new System.Drawing.Point(91, 22);
            this.lblYearTaisho.Name = "lblYearTaisho";
            this.lblYearTaisho.Size = new System.Drawing.Size(24, 16);
            this.lblYearTaisho.TabIndex = 15;
            this.lblYearTaisho.Text = "年";
            // 
            // txtMonthTaisho
            // 
            this.txtMonthTaisho.AutoSizeFromLength = false;
            this.txtMonthTaisho.DisplayLength = null;
            this.txtMonthTaisho.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMonthTaisho.Location = new System.Drawing.Point(116, 20);
            this.txtMonthTaisho.MaxLength = 2;
            this.txtMonthTaisho.Name = "txtMonthTaisho";
            this.txtMonthTaisho.Size = new System.Drawing.Size(34, 23);
            this.txtMonthTaisho.TabIndex = 16;
            this.txtMonthTaisho.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonthTaisho.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthTaisho_Validating);
            // 
            // lblGengoTaisho
            // 
            this.lblGengoTaisho.AutoSize = true;
            this.lblGengoTaisho.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGengoTaisho.Location = new System.Drawing.Point(8, 22);
            this.lblGengoTaisho.Name = "lblGengoTaisho";
            this.lblGengoTaisho.Size = new System.Drawing.Size(40, 16);
            this.lblGengoTaisho.TabIndex = 13;
            this.lblGengoTaisho.Text = "平成";
            // 
            // txtGengoYearTaisho
            // 
            this.txtGengoYearTaisho.AutoSizeFromLength = false;
            this.txtGengoYearTaisho.DisplayLength = null;
            this.txtGengoYearTaisho.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGengoYearTaisho.Location = new System.Drawing.Point(51, 20);
            this.txtGengoYearTaisho.MaxLength = 2;
            this.txtGengoYearTaisho.Name = "txtGengoYearTaisho";
            this.txtGengoYearTaisho.Size = new System.Drawing.Size(34, 23);
            this.txtGengoYearTaisho.TabIndex = 14;
            this.txtGengoYearTaisho.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGengoYearTaisho.Validating += new System.ComponentModel.CancelEventHandler(this.txtGengoYearTaisho_Validating);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.grpBukaCd);
            this.groupBox1.Controls.Add(this.grpBumonCd);
            this.groupBox1.Controls.Add(this.grpShainCd);
            this.groupBox1.Font = new System.Drawing.Font("ＭＳ ゴシック", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBox1.Location = new System.Drawing.Point(12, 185);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(590, 206);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "出力範囲";
            // 
            // grpBukaCd
            // 
            this.grpBukaCd.Controls.Add(this.lblBukaCdTo);
            this.grpBukaCd.Controls.Add(this.txtBukaCdTo);
            this.grpBukaCd.Controls.Add(this.label5);
            this.grpBukaCd.Controls.Add(this.lblBukaCdFr);
            this.grpBukaCd.Controls.Add(this.txtBukaCdFr);
            this.grpBukaCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.grpBukaCd.Location = new System.Drawing.Point(5, 138);
            this.grpBukaCd.Name = "grpBukaCd";
            this.grpBukaCd.Size = new System.Drawing.Size(575, 53);
            this.grpBukaCd.TabIndex = 5;
            this.grpBukaCd.TabStop = false;
            this.grpBukaCd.Text = "部課コード";
            // 
            // lblBukaCdTo
            // 
            this.lblBukaCdTo.BackColor = System.Drawing.Color.Silver;
            this.lblBukaCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBukaCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBukaCdTo.Location = new System.Drawing.Point(365, 20);
            this.lblBukaCdTo.Name = "lblBukaCdTo";
            this.lblBukaCdTo.Size = new System.Drawing.Size(194, 20);
            this.lblBukaCdTo.TabIndex = 4;
            this.lblBukaCdTo.Text = "最　後";
            this.lblBukaCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtBukaCdTo
            // 
            this.txtBukaCdTo.AutoSizeFromLength = false;
            this.txtBukaCdTo.DisplayLength = null;
            this.txtBukaCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtBukaCdTo.Location = new System.Drawing.Point(303, 19);
            this.txtBukaCdTo.MaxLength = 4;
            this.txtBukaCdTo.Name = "txtBukaCdTo";
            this.txtBukaCdTo.Size = new System.Drawing.Size(55, 23);
            this.txtBukaCdTo.TabIndex = 3;
            this.txtBukaCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBukaCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtBukaCdTo_Validating);
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label5.Location = new System.Drawing.Point(276, 20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(18, 20);
            this.label5.TabIndex = 2;
            this.label5.Text = "～";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblBukaCdFr
            // 
            this.lblBukaCdFr.BackColor = System.Drawing.Color.Silver;
            this.lblBukaCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBukaCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBukaCdFr.Location = new System.Drawing.Point(76, 20);
            this.lblBukaCdFr.Name = "lblBukaCdFr";
            this.lblBukaCdFr.Size = new System.Drawing.Size(194, 20);
            this.lblBukaCdFr.TabIndex = 1;
            this.lblBukaCdFr.Text = "先　頭";
            this.lblBukaCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtBukaCdFr
            // 
            this.txtBukaCdFr.AutoSizeFromLength = false;
            this.txtBukaCdFr.DisplayLength = null;
            this.txtBukaCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtBukaCdFr.Location = new System.Drawing.Point(15, 19);
            this.txtBukaCdFr.MaxLength = 4;
            this.txtBukaCdFr.Name = "txtBukaCdFr";
            this.txtBukaCdFr.Size = new System.Drawing.Size(55, 23);
            this.txtBukaCdFr.TabIndex = 0;
            this.txtBukaCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBukaCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtBukaCdFr_Validating);
            // 
            // grpBumonCd
            // 
            this.grpBumonCd.Controls.Add(this.lblBumonCdTo);
            this.grpBumonCd.Controls.Add(this.txtBumonCdTo);
            this.grpBumonCd.Controls.Add(this.label2);
            this.grpBumonCd.Controls.Add(this.lblBumonCdFr);
            this.grpBumonCd.Controls.Add(this.txtBumonCdFr);
            this.grpBumonCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.grpBumonCd.Location = new System.Drawing.Point(6, 79);
            this.grpBumonCd.Name = "grpBumonCd";
            this.grpBumonCd.Size = new System.Drawing.Size(575, 53);
            this.grpBumonCd.TabIndex = 4;
            this.grpBumonCd.TabStop = false;
            this.grpBumonCd.Text = "部門コード";
            // 
            // lblBumonCdTo
            // 
            this.lblBumonCdTo.BackColor = System.Drawing.Color.Silver;
            this.lblBumonCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBumonCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBumonCdTo.Location = new System.Drawing.Point(365, 20);
            this.lblBumonCdTo.Name = "lblBumonCdTo";
            this.lblBumonCdTo.Size = new System.Drawing.Size(194, 20);
            this.lblBumonCdTo.TabIndex = 4;
            this.lblBumonCdTo.Text = "最　後";
            this.lblBumonCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtBumonCdTo
            // 
            this.txtBumonCdTo.AutoSizeFromLength = false;
            this.txtBumonCdTo.DisplayLength = null;
            this.txtBumonCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtBumonCdTo.Location = new System.Drawing.Point(303, 19);
            this.txtBumonCdTo.MaxLength = 4;
            this.txtBumonCdTo.Name = "txtBumonCdTo";
            this.txtBumonCdTo.Size = new System.Drawing.Size(55, 23);
            this.txtBumonCdTo.TabIndex = 3;
            this.txtBumonCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBumonCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtBumonCdTo_Validating);
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(276, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(18, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "～";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblBumonCdFr
            // 
            this.lblBumonCdFr.BackColor = System.Drawing.Color.Silver;
            this.lblBumonCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBumonCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBumonCdFr.Location = new System.Drawing.Point(76, 20);
            this.lblBumonCdFr.Name = "lblBumonCdFr";
            this.lblBumonCdFr.Size = new System.Drawing.Size(194, 20);
            this.lblBumonCdFr.TabIndex = 1;
            this.lblBumonCdFr.Text = "先　頭";
            this.lblBumonCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtBumonCdFr
            // 
            this.txtBumonCdFr.AutoSizeFromLength = false;
            this.txtBumonCdFr.DisplayLength = null;
            this.txtBumonCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtBumonCdFr.Location = new System.Drawing.Point(15, 19);
            this.txtBumonCdFr.MaxLength = 4;
            this.txtBumonCdFr.Name = "txtBumonCdFr";
            this.txtBumonCdFr.Size = new System.Drawing.Size(55, 23);
            this.txtBumonCdFr.TabIndex = 0;
            this.txtBumonCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBumonCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtBumonCdFr_Validating);
            // 
            // grpShainCd
            // 
            this.grpShainCd.Controls.Add(this.lblShainCdTo);
            this.grpShainCd.Controls.Add(this.txtShainCdTo);
            this.grpShainCd.Controls.Add(this.lblCodeBet);
            this.grpShainCd.Controls.Add(this.lblShainCdFr);
            this.grpShainCd.Controls.Add(this.txtShainCdFr);
            this.grpShainCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.grpShainCd.Location = new System.Drawing.Point(6, 20);
            this.grpShainCd.Name = "grpShainCd";
            this.grpShainCd.Size = new System.Drawing.Size(575, 53);
            this.grpShainCd.TabIndex = 3;
            this.grpShainCd.TabStop = false;
            this.grpShainCd.Text = "社員コード";
            // 
            // lblShainCdTo
            // 
            this.lblShainCdTo.BackColor = System.Drawing.Color.Silver;
            this.lblShainCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShainCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShainCdTo.Location = new System.Drawing.Point(365, 20);
            this.lblShainCdTo.Name = "lblShainCdTo";
            this.lblShainCdTo.Size = new System.Drawing.Size(194, 20);
            this.lblShainCdTo.TabIndex = 4;
            this.lblShainCdTo.Text = "最　後";
            this.lblShainCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShainCdTo
            // 
            this.txtShainCdTo.AutoSizeFromLength = false;
            this.txtShainCdTo.DisplayLength = null;
            this.txtShainCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShainCdTo.Location = new System.Drawing.Point(303, 19);
            this.txtShainCdTo.MaxLength = 4;
            this.txtShainCdTo.Name = "txtShainCdTo";
            this.txtShainCdTo.Size = new System.Drawing.Size(55, 23);
            this.txtShainCdTo.TabIndex = 3;
            this.txtShainCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShainCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtShainCdTo_Validating);
            // 
            // lblCodeBet
            // 
            this.lblCodeBet.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblCodeBet.Location = new System.Drawing.Point(276, 20);
            this.lblCodeBet.Name = "lblCodeBet";
            this.lblCodeBet.Size = new System.Drawing.Size(18, 20);
            this.lblCodeBet.TabIndex = 2;
            this.lblCodeBet.Text = "～";
            this.lblCodeBet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShainCdFr
            // 
            this.lblShainCdFr.BackColor = System.Drawing.Color.Silver;
            this.lblShainCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShainCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShainCdFr.Location = new System.Drawing.Point(76, 20);
            this.lblShainCdFr.Name = "lblShainCdFr";
            this.lblShainCdFr.Size = new System.Drawing.Size(194, 20);
            this.lblShainCdFr.TabIndex = 1;
            this.lblShainCdFr.Text = "先　頭";
            this.lblShainCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShainCdFr
            // 
            this.txtShainCdFr.AutoSizeFromLength = false;
            this.txtShainCdFr.DisplayLength = null;
            this.txtShainCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShainCdFr.Location = new System.Drawing.Point(15, 19);
            this.txtShainCdFr.MaxLength = 4;
            this.txtShainCdFr.Name = "txtShainCdFr";
            this.txtShainCdFr.Size = new System.Drawing.Size(55, 23);
            this.txtShainCdFr.TabIndex = 0;
            this.txtShainCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShainCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtShainCdFr_Validating);
            // 
            // grpOrder
            // 
            this.grpOrder.Controls.Add(this.rdoOrder2);
            this.grpOrder.Controls.Add(this.rdoOrder1);
            this.grpOrder.Font = new System.Drawing.Font("ＭＳ ゴシック", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.grpOrder.Location = new System.Drawing.Point(236, 116);
            this.grpOrder.Name = "grpOrder";
            this.grpOrder.Size = new System.Drawing.Size(180, 53);
            this.grpOrder.TabIndex = 2;
            this.grpOrder.TabStop = false;
            this.grpOrder.Text = "出力順";
            // 
            // rdoOrder2
            // 
            this.rdoOrder2.AutoSize = true;
            this.rdoOrder2.Checked = true;
            this.rdoOrder2.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoOrder2.Location = new System.Drawing.Point(94, 20);
            this.rdoOrder2.Name = "rdoOrder2";
            this.rdoOrder2.Size = new System.Drawing.Size(74, 20);
            this.rdoOrder2.TabIndex = 1;
            this.rdoOrder2.TabStop = true;
            this.rdoOrder2.Text = "部門順";
            this.rdoOrder2.UseVisualStyleBackColor = true;
            // 
            // rdoOrder1
            // 
            this.rdoOrder1.AutoSize = true;
            this.rdoOrder1.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoOrder1.Location = new System.Drawing.Point(9, 20);
            this.rdoOrder1.Name = "rdoOrder1";
            this.rdoOrder1.Size = new System.Drawing.Size(74, 20);
            this.rdoOrder1.TabIndex = 0;
            this.rdoOrder1.TabStop = true;
            this.rdoOrder1.Text = "社員順";
            this.rdoOrder1.UseVisualStyleBackColor = true;
            // 
            // grpCopy
            // 
            this.grpCopy.Controls.Add(this.rdoCopy2);
            this.grpCopy.Controls.Add(this.rdoCopy1);
            this.grpCopy.Font = new System.Drawing.Font("ＭＳ ゴシック", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.grpCopy.Location = new System.Drawing.Point(422, 116);
            this.grpCopy.Name = "grpCopy";
            this.grpCopy.Size = new System.Drawing.Size(180, 53);
            this.grpCopy.TabIndex = 3;
            this.grpCopy.TabStop = false;
            this.grpCopy.Text = "控印刷";
            // 
            // rdoCopy2
            // 
            this.rdoCopy2.AutoSize = true;
            this.rdoCopy2.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoCopy2.Location = new System.Drawing.Point(100, 20);
            this.rdoCopy2.Name = "rdoCopy2";
            this.rdoCopy2.Size = new System.Drawing.Size(74, 20);
            this.rdoCopy2.TabIndex = 3;
            this.rdoCopy2.TabStop = true;
            this.rdoCopy2.Text = "しない";
            this.rdoCopy2.UseVisualStyleBackColor = true;
            // 
            // rdoCopy1
            // 
            this.rdoCopy1.AutoSize = true;
            this.rdoCopy1.Checked = true;
            this.rdoCopy1.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoCopy1.Location = new System.Drawing.Point(15, 20);
            this.rdoCopy1.Name = "rdoCopy1";
            this.rdoCopy1.Size = new System.Drawing.Size(58, 20);
            this.rdoCopy1.TabIndex = 2;
            this.rdoCopy1.TabStop = true;
            this.rdoCopy1.Text = "する";
            this.rdoCopy1.UseVisualStyleBackColor = true;
            // 
            // KYSR1011
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 638);
            this.Controls.Add(this.grpCopy);
            this.Controls.Add(this.grpOrder);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.grpTaisho);
            this.Controls.Add(this.grpNengetsu);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "KYSR1011";
            this.Text = "賞与明細印刷";
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.grpNengetsu, 0);
            this.Controls.SetChildIndex(this.grpTaisho, 0);
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.Controls.SetChildIndex(this.grpOrder, 0);
            this.Controls.SetChildIndex(this.grpCopy, 0);
            this.pnlDebug.ResumeLayout(false);
            this.grpNengetsu.ResumeLayout(false);
            this.grpNengetsu.PerformLayout();
            this.grpTaisho.ResumeLayout(false);
            this.grpTaisho.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.grpBukaCd.ResumeLayout(false);
            this.grpBukaCd.PerformLayout();
            this.grpBumonCd.ResumeLayout(false);
            this.grpBumonCd.PerformLayout();
            this.grpShainCd.ResumeLayout(false);
            this.grpShainCd.PerformLayout();
            this.grpOrder.ResumeLayout(false);
            this.grpOrder.PerformLayout();
            this.grpCopy.ResumeLayout(false);
            this.grpCopy.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpNengetsu;
        private System.Windows.Forms.Label lblMonthNengetsu;
        private System.Windows.Forms.Label lblYearNengetsu;
        private jp.co.fsi.common.controls.FsiTextBox txtMonthNengetsu;
        private System.Windows.Forms.Label lblGengoNengetsu;
        private jp.co.fsi.common.controls.FsiTextBox txtGengoYearNengetsu;
        private System.Windows.Forms.GroupBox grpTaisho;
        private System.Windows.Forms.Label lblMonthTaisho;
        private System.Windows.Forms.Label lblYearTaisho;
        private common.controls.FsiTextBox txtMonthTaisho;
        private System.Windows.Forms.Label lblGengoTaisho;
        private common.controls.FsiTextBox txtGengoYearTaisho;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox grpOrder;
        private System.Windows.Forms.RadioButton rdoOrder2;
        private System.Windows.Forms.RadioButton rdoOrder1;
        private System.Windows.Forms.GroupBox grpCopy;
        private System.Windows.Forms.RadioButton rdoCopy2;
        private System.Windows.Forms.RadioButton rdoCopy1;
        private System.Windows.Forms.GroupBox grpBukaCd;
        private System.Windows.Forms.Label lblBukaCdTo;
        private common.controls.FsiTextBox txtBukaCdTo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblBukaCdFr;
        private common.controls.FsiTextBox txtBukaCdFr;
        private System.Windows.Forms.GroupBox grpBumonCd;
        private System.Windows.Forms.Label lblBumonCdTo;
        private common.controls.FsiTextBox txtBumonCdTo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblBumonCdFr;
        private common.controls.FsiTextBox txtBumonCdFr;
        private System.Windows.Forms.GroupBox grpShainCd;
        private System.Windows.Forms.Label lblShainCdTo;
        private common.controls.FsiTextBox txtShainCdTo;
        private System.Windows.Forms.Label lblCodeBet;
        private System.Windows.Forms.Label lblShainCdFr;
        private common.controls.FsiTextBox txtShainCdFr;
    }
}