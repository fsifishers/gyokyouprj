﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Reflection;

using GrapeCity.ActiveReports;

using jp.co.fsi.common.access;
using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.ky.kysr1011
{
    /// <summary>
    /// 賞与明細印刷(KYSR1011)
    /// </summary>
    public partial class KYSR1011 : BasePgForm
    {
        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KYSR1011()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 支給年月の初期値取得
            string[] jpDate;
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            string sql = "SELECT MAX(NENGETSU) AS MAX_NENGETSU";
            sql += " FROM TB_KY_SHOYO_MEISAI_KINTAI WHERE KAISHA_CD = @KAISHA_CD";
            DataTable dtShikyuInfo =
                this.Dba.GetDataTableFromSqlWithParams(sql, dpc);
            if (dtShikyuInfo.Rows.Count == 0 || ValChk.IsEmpty(dtShikyuInfo.Rows[0]["MAX_NENGETSU"]))
            {
                // 該当支給データ存在しない場合はシステム日付をセット
                jpDate = Util.ConvJpDate(DateTime.Now, this.Dba);
            }
            else
            {
                jpDate = Util.ConvJpDate(Util.ToDate(dtShikyuInfo.Rows[0]["MAX_NENGETSU"]), this.Dba);
            }

            // 支給月
            lblGengoNengetsu.Text     = jpDate[0];
            txtGengoYearNengetsu.Text = jpDate[2];
            txtMonthNengetsu.Text     = jpDate[3];
            // 対象月
            lblGengoTaisho.Text = jpDate[0];
            txtGengoYearTaisho.Text = jpDate[2];
            txtMonthTaisho.Text = jpDate[3];

            // 初期フォーカス
            this.txtGengoYearNengetsu.Focus();
            this.txtGengoYearNengetsu.SelectAll();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 元号年とコード項目に
            // フォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtGengoYearNengetsu":
                case "txtGengoYearTaisho":
                case "txtShainCdFr":
                case "txtShainCdTo":
                case "txtBumonCdFr":
                case "txtBumonCdTo":
                case "txtBukaCdFr":
                case "txtBukaCdTo":
                    this.btnF1.Enabled = true;
                    break;
                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        public override void PressF1()
        {
            Assembly asm;
            Type t;

            switch (this.ActiveCtlNm)
            {
                case "txtShainCdFr":
                case "txtShainCdTo":
                    #region 社員検索
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom("KYCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.ky.kycm1021.KYCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            if (this.ActiveCtlNm == "txtShainCdFr")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.Par1 = "1";                     // ダイアログ起動パラメータ
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.txtShainCdFr.Text = result[0];
                                    this.lblShainCdFr.Text = result[1];
                                }
                            }
                            else if (this.ActiveCtlNm == "txtShainCdTo")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.Par1 = "1";                     // ダイアログ起動パラメータ
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.txtShainCdTo.Text = result[0];
                                    this.lblShainCdTo.Text = result[1];
                                }
                            }
                        }
                    }
                    #endregion
                    break;
                case "txtBumonCdFr":
                case "txtBumonCdTo":
                    #region 部門検索
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom("KYCM1031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.ky.kycm1031.KYCM1031");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            if (this.ActiveCtlNm == "txtBumonCdFr")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.Par1 = "1";
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.txtBumonCdFr.Text = result[0];
                                    this.lblBumonCdFr.Text = result[1];
                                    this.txtBukaCdFr.Text = "";
                                    this.lblBukaCdFr.Text = "先　頭";
                                }
                            }
                            else if (this.ActiveCtlNm == "txtBumonCdTo")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.Par1 = "1";
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.txtBumonCdTo.Text = result[0];
                                    this.lblBumonCdTo.Text = result[1];
                                    this.txtBukaCdTo.Text = "";
                                    this.lblBukaCdTo.Text = "最　後";
                                }
                            }
                        }
                    }
                    #endregion
                    break;                
                case "txtBukaCdFr":
                case "txtBukaCdTo":
                    #region 部課検索
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom("KYCM1041.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.ky.kycm1041.KYCM1041");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            if (this.ActiveCtlNm == "txtBukaCdFr" 
                                && !ValChk.IsEmpty(this.txtBumonCdFr.Text))
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.Par1 = "1";                         // 検索ダイアログ起動パラメータ
                                frm.InData = this.txtBumonCdFr.Text;    // (string)部門コード
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.txtBukaCdFr.Text = result[0];
                                    this.lblBukaCdFr.Text = result[1];
                                }
                            }
                            else if (this.ActiveCtlNm == "txtBukaCdTo"
                                && !ValChk.IsEmpty(this.txtBumonCdTo.Text))
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.Par1 = "1";                         // 検索ダイアログ起動パラメータ
                                frm.InData = this.txtBumonCdTo.Text;    // (string)部門コード
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.txtBukaCdTo.Text = result[0];
                                    this.lblBukaCdTo.Text = result[1];
                                }
                            }
                        }
                    }
                    #endregion
                    break;
                case "txtGengoYearNengetsu":
                case "txtGengoYearTaisho":
                    #region 元号検索
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom("CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.com.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            if (this.ActiveCtlNm == "txtGengoYearNengetsu")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.InData = this.lblGengoNengetsu.Text;
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.lblGengoNengetsu.Text = result[1];

                                    // 存在しない日付の場合、補正して存在する日付に戻す
                                    string[] arrJpDate =
                                        Util.FixJpDate(this.lblGengoNengetsu.Text,
                                            this.txtGengoYearNengetsu.Text,
                                            this.txtMonthNengetsu.Text,
                                            "1",
                                            this.Dba);
                                    this.lblGengoNengetsu.Text = arrJpDate[0];
                                    this.txtGengoYearNengetsu.Text = arrJpDate[2];
                                    this.txtMonthNengetsu.Text = arrJpDate[3];
                                }
                            }
                            else if (this.ActiveCtlNm == "txtGengoYearTaisho")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.InData = this.lblGengoTaisho.Text;
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.lblGengoTaisho.Text = result[1];

                                    // 存在しない日付の場合、補正して存在する日付に戻す
                                    string[] arrJpDate =
                                        Util.FixJpDate(this.lblGengoTaisho.Text,
                                            this.txtGengoYearTaisho.Text,
                                            this.txtMonthTaisho.Text,
                                            "1",
                                            this.Dba);
                                    this.lblGengoTaisho.Text = arrJpDate[0];
                                    this.txtGengoYearTaisho.Text = arrJpDate[2];
                                    this.txtMonthTaisho.Text = arrJpDate[3];
                                }
                            }
                        }
                        
                    }
                    #endregion
                    break;
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
            {
                // プレビュー処理
                DoPrint(true);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            if (Msg.ConfNmYesNo("印刷", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }

        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            // 設定画面の起動
            // MEMO:原則としてここで渡す帳票IDの設定はReport.csvに保持していることが前提ですが、
            // 保持していない場合は、設定画面での保存(F6)時に新規に設定が保持されます。
            PrintSettingForm psForm = new PrintSettingForm(new string[1] { "KYSR1011R" });
            psForm.ShowDialog();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 社員コード(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShainCdFr_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtShainCdFr.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtShainCdFr.SelectAll();
                e.Cancel = true;
                return;
            }

            // コードを元に名称を取得する
            // 取得された場合、名称をラベルに反映する
            if (this.txtShainCdFr.Text.Length > 0)
            {
                this.lblShainCdFr.Text = this.Dba.GetName(this.UInfo, "TB_KY_SHAIN_JOHO", " ", this.txtShainCdFr.Text);
            }
            else
            {
                this.lblShainCdFr.Text = "先　頭";
            }
        }

        /// <summary>
        /// 社員コード(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShainCdTo_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtShainCdTo.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtShainCdTo.SelectAll();
                e.Cancel = true;
                return;
            }

            // コードを元に名称を取得する
            // 取得された場合、名称をラベルに反映する
            if (this.txtShainCdTo.Text.Length > 0)
            {
                this.lblShainCdTo.Text = this.Dba.GetName(this.UInfo, "TB_KY_SHAIN_JOHO", " ", this.txtShainCdTo.Text);
            }
            else
            {
                this.lblShainCdTo.Text = "最　後";
            }
        }

        /// <summary>
        /// 部門コード(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtBumonCdFr_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtBumonCdFr.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtBumonCdFr.SelectAll();
                e.Cancel = true;
                return;
            }

            // コードを元に名称を取得する
            // 取得された場合、名称をラベルに反映する
            if (this.txtBumonCdFr.Text.Length > 0)
            {
                this.lblBumonCdFr.Text = this.Dba.GetName(this.UInfo, "TB_KY_BUMON", " ", this.txtBumonCdFr.Text);
            }
            else
            {
                this.lblBumonCdFr.Text = "先　頭";
            }
        }

        /// <summary>
        /// 部門コード(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtBumonCdTo_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtBumonCdTo.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtBumonCdTo.SelectAll();
                e.Cancel = true;
                return;
            }

            // コードを元に名称を取得する
            // 取得された場合、名称をラベルに反映する
            if (this.txtBumonCdTo.Text.Length > 0)
            {
                this.lblBumonCdTo.Text = this.Dba.GetName(this.UInfo, "TB_KY_BUMON", " ", this.txtBumonCdTo.Text);
            }
            else
            {
                this.lblBumonCdTo.Text = "最　後";
            }
        }

        /// <summary>
        /// 部課コード(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtBukaCdFr_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtBukaCdFr.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtBukaCdFr.SelectAll();
                e.Cancel = true;
                return;
            }

            // コードを元に名称を取得する
            // 取得された場合、名称をラベルに反映する
            if (this.txtBukaCdFr.Text.Length > 0)
            {
                this.lblBukaCdFr.Text = this.Dba.GetBukaNm(this.UInfo, this.txtBumonCdFr.Text, this.txtBukaCdFr.Text);
            }
            else
            {
                this.lblBukaCdFr.Text = "先　頭";
            }
        }

        /// <summary>
        /// 部課コード(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtBukaCdTo_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtBukaCdTo.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtBukaCdTo.SelectAll();
                e.Cancel = true;
                return;
            }

            // コードを元に名称を取得する
            // 取得された場合、名称をラベルに反映する
            if (this.txtBukaCdTo.Text.Length > 0)
            {
                this.lblBukaCdTo.Text = this.Dba.GetBukaNm(this.UInfo,this.txtBumonCdTo.Text, this.txtBukaCdTo.Text);
            }
            else
            {
                this.lblBukaCdTo.Text = "最　後";
            }
        }



        /// <summary>
        /// 支給月(年)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGengoYearNengetsu_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtGengoYearNengetsu.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtGengoYearNengetsu.SelectAll();
                e.Cancel = true;
                return;
            }

            // 空の場合、0年として処理
            if (ValChk.IsEmpty(this.txtGengoYearNengetsu.Text))
            {
                this.txtGengoYearNengetsu.Text = "0";
            }

            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            this.SetJpDateNengetsu(Util.FixJpDate(this.lblGengoNengetsu.Text, this.txtGengoYearNengetsu.Text,
                this.txtMonthNengetsu.Text, "1", this.Dba));
        }

        /// <summary>
        /// 支給月(月)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMonthNengetsu_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtMonthNengetsu.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtMonthNengetsu.SelectAll();
                e.Cancel = true;
                return;
            }

            if (ValChk.IsEmpty(this.txtMonthNengetsu.Text) || Util.ToInt(this.txtMonthNengetsu.Text) == 0)
            {
                // 空の場合、1月として処理
                this.txtMonthNengetsu.Text = "1";
            }
            else
            {
                // 12を超える月が入力された場合、12月として処理
                if (Util.ToInt(this.txtMonthNengetsu.Text) > 12)
                {
                    this.txtMonthNengetsu.Text = "12";
                }
            }

            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            this.SetJpDateNengetsu(Util.FixJpDate(this.lblGengoNengetsu.Text, this.txtGengoYearNengetsu.Text,
                this.txtMonthNengetsu.Text, "1", this.Dba));
        }

        /// <summary>
        /// 対象年月(年)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGengoYearTaisho_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtGengoYearTaisho.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtGengoYearTaisho.SelectAll();
                e.Cancel = true;
                return;
            }

            // 空の場合、0年として処理
            if (ValChk.IsEmpty(this.txtGengoYearTaisho.Text))
            {
                this.txtGengoYearTaisho.Text = "0";
            }

            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            this.SetJpDateTaisho(Util.FixJpDate(this.lblGengoTaisho.Text, this.txtGengoYearTaisho.Text,
                this.txtMonthTaisho.Text, "1", this.Dba));
        }

        /// <summary>
        /// 対象年月(月)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMonthTaisho_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtMonthTaisho.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtMonthTaisho.SelectAll();
                e.Cancel = true;
                return;
            }

            if (ValChk.IsEmpty(this.txtMonthTaisho.Text) || Util.ToInt(this.txtMonthTaisho.Text) == 0)
            {
                // 空の場合、1月として処理
                this.txtMonthTaisho.Text = "1";
            }
            else
            {
                // 12を超える月が入力された場合、12月として処理
                if (Util.ToInt(this.txtMonthTaisho.Text) > 12)
                {
                    this.txtMonthTaisho.Text = "12";
                }
            }

            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            this.SetJpDateTaisho(Util.FixJpDate(this.lblGengoTaisho.Text, this.txtGengoYearTaisho.Text,
                this.txtMonthTaisho.Text, "1", this.Dba));
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview)
        {
            try
            {
                bool dataFlag;

                this.Dba.BeginTransaction();

                // 帳票出力用にワークテーブルにデータを作成
                dataFlag = MakeWkData();

                // 帳票出力
                if (dataFlag)
                {
                    // 取得列の定義
                    StringBuilder cols = new StringBuilder();
                    cols.Append("  ITEM001");
                    cols.Append(" ,ITEM002");
                    cols.Append(" ,ITEM003");
                    cols.Append(" ,ITEM004");
                    cols.Append(" ,ITEM005");
                    cols.Append(" ,ITEM006");
                    cols.Append(" ,ITEM007");
                    cols.Append(" ,ITEM008");
                    cols.Append(" ,ITEM009");
                    cols.Append(" ,ITEM010");
                    cols.Append(" ,ITEM011");
                    cols.Append(" ,ITEM012");
                    cols.Append(" ,ITEM013");
                    cols.Append(" ,ITEM014");
                    cols.Append(" ,ITEM015");
                    cols.Append(" ,ITEM016");
                    cols.Append(" ,ITEM017");
                    cols.Append(" ,ITEM018");
                    cols.Append(" ,ITEM019");
                    cols.Append(" ,ITEM020");
                    cols.Append(" ,ITEM021");
                    cols.Append(" ,ITEM022");
                    cols.Append(" ,ITEM023");
                    cols.Append(" ,ITEM024");
                    cols.Append(" ,ITEM025");
                    cols.Append(" ,ITEM026");
                    cols.Append(" ,ITEM027");
                    cols.Append(" ,ITEM028");
                    cols.Append(" ,ITEM029");
                    cols.Append(" ,ITEM030");
                    cols.Append(" ,ITEM031");
                    cols.Append(" ,ITEM032");
                    cols.Append(" ,ITEM033");
                    cols.Append(" ,ITEM034");
                    cols.Append(" ,ITEM035");
                    cols.Append(" ,ITEM036");
                    cols.Append(" ,ITEM037");
                    cols.Append(" ,ITEM038");
                    cols.Append(" ,ITEM039");
                    cols.Append(" ,ITEM040");
                    cols.Append(" ,ITEM041");
                    cols.Append(" ,ITEM042");
                    cols.Append(" ,ITEM043");
                    cols.Append(" ,ITEM044");
                    cols.Append(" ,ITEM045");
                    cols.Append(" ,ITEM046");
                    cols.Append(" ,ITEM047");
                    cols.Append(" ,ITEM048");
                    cols.Append(" ,ITEM049");
                    cols.Append(" ,ITEM050");
                    cols.Append(" ,ITEM051");
                    cols.Append(" ,ITEM052");
                    cols.Append(" ,ITEM053");
                    cols.Append(" ,ITEM054");
                    cols.Append(" ,ITEM055");
                    cols.Append(" ,ITEM056");
                    cols.Append(" ,ITEM057");
                    cols.Append(" ,ITEM058");
                    cols.Append(" ,ITEM059");
                    cols.Append(" ,ITEM060");
                    cols.Append(" ,ITEM061");
                    cols.Append(" ,ITEM062");
                    cols.Append(" ,ITEM063");
                    cols.Append(" ,ITEM064");
                    cols.Append(" ,ITEM065");
                    cols.Append(" ,ITEM066");
                    cols.Append(" ,ITEM067");
                    cols.Append(" ,ITEM068");
                    cols.Append(" ,ITEM069");
                    cols.Append(" ,ITEM070");
                    cols.Append(" ,ITEM071");
                    cols.Append(" ,ITEM072");
                    cols.Append(" ,ITEM073");
                    cols.Append(" ,ITEM074");
                    cols.Append(" ,ITEM075");
                    cols.Append(" ,ITEM076");
                    cols.Append(" ,ITEM077");
                    cols.Append(" ,ITEM078");
                    cols.Append(" ,ITEM079");
                    cols.Append(" ,ITEM080");
                    cols.Append(" ,ITEM081");
                    cols.Append(" ,ITEM082");
                    cols.Append(" ,ITEM083");
                    cols.Append(" ,ITEM084");
                    cols.Append(" ,ITEM085");
                    cols.Append(" ,ITEM086");
                    cols.Append(" ,ITEM087");
                    cols.Append(" ,ITEM088");
                    cols.Append(" ,ITEM089");
                    cols.Append(" ,ITEM090");
                    cols.Append(" ,ITEM091");
                    cols.Append(" ,ITEM092");
                    cols.Append(" ,ITEM093");
                    cols.Append(" ,ITEM094");
                    cols.Append(" ,ITEM095");
                    cols.Append(" ,ITEM096");
                    cols.Append(" ,ITEM097");
                    cols.Append(" ,ITEM098");
                    cols.Append(" ,ITEM099");
                    cols.Append(" ,ITEM100");
                    cols.Append(" ,ITEM101");
                    cols.Append(" ,ITEM102");
                    cols.Append(" ,ITEM103");
                    cols.Append(" ,ITEM104");
                    cols.Append(" ,ITEM105");
                    cols.Append(" ,ITEM106");
                    cols.Append(" ,ITEM107");
                    cols.Append(" ,ITEM108");
                    cols.Append(" ,ITEM109");
                    cols.Append(" ,ITEM110");
                    cols.Append(" ,ITEM111");
                    cols.Append(" ,ITEM112");
                    cols.Append(" ,ITEM113");
                    cols.Append(" ,ITEM114");
                    cols.Append(" ,ITEM115");
                    cols.Append(" ,ITEM116");
                    cols.Append(" ,ITEM117");
                    cols.Append(" ,ITEM118");
                    cols.Append(" ,ITEM119");
                    cols.Append(" ,ITEM120");
                    cols.Append(" ,ITEM121");
                    cols.Append(" ,ITEM122");
                    cols.Append(" ,ITEM123");
                    cols.Append(" ,ITEM124");
                    cols.Append(" ,ITEM125");
                    cols.Append(" ,ITEM126");
                    cols.Append(" ,ITEM127");
                    cols.Append(" ,ITEM128");
                    cols.Append(" ,ITEM129");
                    cols.Append(" ,ITEM130");
                    cols.Append(" ,ITEM131");
                    cols.Append(" ,ITEM132");
                    cols.Append(" ,ITEM133");
                    cols.Append(" ,ITEM134");
                    cols.Append(" ,ITEM135");
                    cols.Append(" ,ITEM136");
                    cols.Append(" ,ITEM137");
                    cols.Append(" ,ITEM138");
                    cols.Append(" ,ITEM139");
                    cols.Append(" ,ITEM140");
                    cols.Append(" ,ITEM141");
                    cols.Append(" ,ITEM142");
                    cols.Append(" ,ITEM143");
                    cols.Append(" ,ITEM144");
                    cols.Append(" ,ITEM145");
                    cols.Append(" ,ITEM146");
                    cols.Append(" ,ITEM147");
                    cols.Append(" ,ITEM148");
                    cols.Append(" ,ITEM149");
                    cols.Append(" ,ITEM150");

                    // バインドパラメータの設定
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);

                    // データの取得
                    DataTable dtOutput = this.Dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_KY_TBL", "GUID = @GUID", "SORT ASC", dpc);

                    // 帳票オブジェクトをインスタンス化
                    KYSR1011R rpt = new KYSR1011R(dtOutput);

                    if (isPreview)
                    {
                        // プレビュー画面表示
                        PreviewForm pFrm = new PreviewForm(rpt, this.UnqId);
                        pFrm.WindowState = FormWindowState.Maximized;
                        pFrm.Show();
                    }
                    else
                    {
                        // 直接印刷
                        rpt.Run(false);
                        rpt.Document.Print(true, true, false);
                    }
                }
            }
            finally
            {
                this.Dba.Rollback();
            }
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData()
        {
            // 支給月・対象月を西暦にして保持
            DateTime nengetsu = Util.ConvAdDate(this.lblGengoNengetsu.Text, this.txtGengoYearNengetsu.Text,
                    this.txtMonthNengetsu.Text, "1", this.Dba);
            DateTime taisho = Util.ConvAdDate(this.lblGengoTaisho.Text, this.txtGengoYearTaisho.Text,
                    this.txtMonthTaisho.Text, "1", this.Dba);
            // 支給月・対象月を和暦で保持
            string[] jpNengetsu = Util.ConvJpDate(nengetsu, this.Dba);
            string[] jpTaisho = Util.ConvJpDate(taisho, this.Dba);
            // 社員コード設定
            string shainCdFr;
            string shainCdTo;
            if (Util.ToDecimal(txtShainCdFr.Text) > 0) 
            {
                shainCdFr = txtShainCdFr.Text;
            }
            else
            {
                shainCdFr = "0";
            }
            if (Util.ToDecimal(txtShainCdTo.Text) > 0) 
            {
                shainCdTo = txtShainCdTo.Text;
            }
            else
            {
                shainCdTo = "999999";
            }
            // 部門コード設定
            string bumonCdFr;
            string bumonCdTo;
            if (Util.ToDecimal(txtBumonCdFr.Text) > 0)
            {
                bumonCdFr = txtBumonCdFr.Text;
            }
            else
            {
                bumonCdFr = "0";
            }
            if (Util.ToDecimal(txtBumonCdTo.Text) > 0)
            {
                bumonCdTo = txtBumonCdTo.Text;
            }
            else
            {
                bumonCdTo = "9999";
            }
            // 部課コード設定
            string bukaCdFr;
            string bukaCdTo;
            if (Util.ToDecimal(txtBukaCdFr.Text) > 0)
            {
                bukaCdFr = txtBukaCdFr.Text;
            }
            else
            {
                bukaCdFr = "0";
            }
            if (Util.ToDecimal(txtBukaCdTo.Text) > 0)
            {
                bukaCdTo = txtBukaCdTo.Text;
            }
            else
            {
                bukaCdTo = "9999";
            }

            #region メインループデータ取得

            DbParamCollection dpc = new DbParamCollection();
            StringBuilder sql = new StringBuilder();

            // VI_賞与項目設定(VI_KY_SHOYO_KOMOKU_SETTEI)
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 2, this.UInfo.KaishaCd);
            DataTable dtSti = this.Dba.GetDataTableByConditionWithParams(
                "*",
                "VI_KY_SHOYO_KOMOKU_SETTEI",
                "KAISHA_CD = @KAISHA_CD AND INJI_KUBUN <> '0'",
                "KOMOKU_SHUBETSU, KOMOKU_BANGO",
                dpc);
            if (dtSti.Rows.Count == 0)
            {
                return false;
            }
            
            // 賞与明細データ
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 2, this.UInfo.KaishaCd);
            dpc.SetParam("@NENGETSU", SqlDbType.VarChar, 10, nengetsu.Date.ToString("yyyy/MM/dd"));
            dpc.SetParam("@SHAIN_CD_FR", SqlDbType.VarChar, 6, shainCdFr);
            dpc.SetParam("@SHAIN_CD_TO", SqlDbType.VarChar, 6, shainCdTo);
            dpc.SetParam("@BUMON_CD_FR", SqlDbType.VarChar, 4, bumonCdFr);
            dpc.SetParam("@BUMON_CD_TO", SqlDbType.VarChar, 4, bumonCdTo);
            dpc.SetParam("@BUKA_CD_FR", SqlDbType.VarChar, 4, bukaCdFr);
            dpc.SetParam("@BUKA_CD_TO", SqlDbType.VarChar, 4, bukaCdTo);
            // SElECT句
            #region 賞与明細データSELECT句
            sql = new StringBuilder();
            sql.Append("SELECT K1.KAISHA_CD AS KAISHA_CD");
            sql.Append(", M1.SHAIN_CD AS SHAIN_CD");
            sql.Append(", M1.BUMON_CD AS BUMON_CD");
            sql.Append(", M1.BUKA_CD AS BUKA_CD");
            sql.Append(", M1.SHAIN_NM AS SHAIN_NM");
            sql.Append(", K1.SHIKYUBI AS SHIKYUBI");
            sql.Append(", K1.KOMOKU1 AS KINTAI_KOMOKU1");
            sql.Append(", K1.KOMOKU2 AS KINTAI_KOMOKU2");
            sql.Append(", K1.KOMOKU3 AS KINTAI_KOMOKU3");
            sql.Append(", K1.KOMOKU4 AS KINTAI_KOMOKU4");
            sql.Append(", K1.KOMOKU5 AS KINTAI_KOMOKU5");
            sql.Append(", K1.KOMOKU6 AS KINTAI_KOMOKU6");
            sql.Append(", K1.KOMOKU7 AS KINTAI_KOMOKU7");
            sql.Append(", K1.KOMOKU8 AS KINTAI_KOMOKU8");
            sql.Append(", K1.KOMOKU9 AS KINTAI_KOMOKU9");
            sql.Append(", K1.KOMOKU10 AS KINTAI_KOMOKU10");
            sql.Append(", K1.KOMOKU11 AS KINTAI_KOMOKU11");
            sql.Append(", K1.KOMOKU12 AS KINTAI_KOMOKU12");
            sql.Append(", K1.KOMOKU13 AS KINTAI_KOMOKU13");
            sql.Append(", K1.KOMOKU14 AS KINTAI_KOMOKU14");
            sql.Append(", K1.KOMOKU15 AS KINTAI_KOMOKU15");
            sql.Append(", K1.KOMOKU16 AS KINTAI_KOMOKU16");
            sql.Append(", K1.KOMOKU17 AS KINTAI_KOMOKU17");
            sql.Append(", K1.KOMOKU18 AS KINTAI_KOMOKU18");
            sql.Append(", K1.KOMOKU19 AS KINTAI_KOMOKU19");
            sql.Append(", K1.KOMOKU20 AS KINTAI_KOMOKU20");
            sql.Append(", K2.KOMOKU1 AS SHIKYU_KOMOKU1");
            sql.Append(", K2.KOMOKU2 AS SHIKYU_KOMOKU2");
            sql.Append(", K2.KOMOKU3 AS SHIKYU_KOMOKU3");
            sql.Append(", K2.KOMOKU4 AS SHIKYU_KOMOKU4");
            sql.Append(", K2.KOMOKU5 AS SHIKYU_KOMOKU5");
            sql.Append(", K2.KOMOKU6 AS SHIKYU_KOMOKU6");
            sql.Append(", K2.KOMOKU7 AS SHIKYU_KOMOKU7");
            sql.Append(", K2.KOMOKU8 AS SHIKYU_KOMOKU8");
            sql.Append(", K2.KOMOKU9 AS SHIKYU_KOMOKU9");
            sql.Append(", K2.KOMOKU10 AS SHIKYU_KOMOKU10");
            sql.Append(", K2.KOMOKU11 AS SHIKYU_KOMOKU11");
            sql.Append(", K2.KOMOKU12 AS SHIKYU_KOMOKU12");
            sql.Append(", K2.KOMOKU13 AS SHIKYU_KOMOKU13");
            sql.Append(", K2.KOMOKU14 AS SHIKYU_KOMOKU14");
            sql.Append(", K2.KOMOKU15 AS SHIKYU_KOMOKU15");
            sql.Append(", K2.KOMOKU16 AS SHIKYU_KOMOKU16");
            sql.Append(", K2.KOMOKU17 AS SHIKYU_KOMOKU17");
            sql.Append(", K2.KOMOKU18 AS SHIKYU_KOMOKU18");
            sql.Append(", K2.KOMOKU19 AS SHIKYU_KOMOKU19");
            sql.Append(", K2.KOMOKU20 AS SHIKYU_KOMOKU20");
            sql.Append(", K3.KOMOKU1 AS KOJO_KOMOKU1");
            sql.Append(", K3.KOMOKU2 AS KOJO_KOMOKU2");
            sql.Append(", K3.KOMOKU3 AS KOJO_KOMOKU3");
            sql.Append(", K3.KOMOKU4 AS KOJO_KOMOKU4");
            sql.Append(", K3.KOMOKU5 AS KOJO_KOMOKU5");
            sql.Append(", K3.KOMOKU6 AS KOJO_KOMOKU6");
            sql.Append(", K3.KOMOKU7 AS KOJO_KOMOKU7");
            sql.Append(", K3.KOMOKU8 AS KOJO_KOMOKU8");
            sql.Append(", K3.KOMOKU9 AS KOJO_KOMOKU9");
            sql.Append(", K3.KOMOKU10 AS KOJO_KOMOKU10");
            sql.Append(", K3.KOMOKU11 AS KOJO_KOMOKU11");
            sql.Append(", K3.KOMOKU12 AS KOJO_KOMOKU12");
            sql.Append(", K3.KOMOKU13 AS KOJO_KOMOKU13");
            sql.Append(", K3.KOMOKU14 AS KOJO_KOMOKU14");
            sql.Append(", K3.KOMOKU15 AS KOJO_KOMOKU15");
            sql.Append(", K3.KOMOKU16 AS KOJO_KOMOKU16");
            sql.Append(", K3.KOMOKU17 AS KOJO_KOMOKU17");
            sql.Append(", K3.KOMOKU18 AS KOJO_KOMOKU18");
            sql.Append(", K3.KOMOKU19 AS KOJO_KOMOKU19");
            sql.Append(", K3.KOMOKU20 AS KOJO_KOMOKU20");
            sql.Append(", K4.KOMOKU1 AS GOKEI_KOMOKU1");
            sql.Append(", K4.KOMOKU2 AS GOKEI_KOMOKU2");
            sql.Append(", K4.KOMOKU3 AS GOKEI_KOMOKU3");
            sql.Append(", K4.KOMOKU4 AS GOKEI_KOMOKU4");
            sql.Append(", K4.KOMOKU5 AS GOKEI_KOMOKU5");
            sql.Append(", K4.KOMOKU6 AS GOKEI_KOMOKU6");
            sql.Append(", K4.KOMOKU7 AS GOKEI_KOMOKU7");
            sql.Append(", K4.KOMOKU8 AS GOKEI_KOMOKU8");
            sql.Append(", K4.KOMOKU9 AS GOKEI_KOMOKU9");
            sql.Append(", K4.KOMOKU10 AS GOKEI_KOMOKU10");
            sql.Append("  FROM TB_KY_SHOYO_MEISAI_KINTAI AS K1");
            sql.Append("  LEFT OUTER JOIN TB_KY_SHOYO_MEISAI_SHIKYU AS K2");
            sql.Append("  ON (K1.KAISHA_CD = K2.KAISHA_CD)");
            sql.Append("  AND (K1.SHAIN_CD = K2.SHAIN_CD)");
            sql.Append("  AND (K1.NENGETSU = K2.NENGETSU)");
            sql.Append("  LEFT OUTER JOIN TB_KY_SHOYO_MEISAI_KOJO AS K3");
            sql.Append("  ON (K1.KAISHA_CD = K3.KAISHA_CD)");
            sql.Append("  AND (K1.SHAIN_CD = K3.SHAIN_CD)");
            sql.Append("  AND (K1.NENGETSU = K3.NENGETSU)");
            sql.Append("  LEFT OUTER JOIN TB_KY_SHOYO_MEISAI_GOKEI AS K4");
            sql.Append("  ON (K1.KAISHA_CD = K4.KAISHA_CD)");
            sql.Append("  AND (K1.SHAIN_CD = K4.SHAIN_CD)");
            sql.Append("  AND (K1.NENGETSU = K4.NENGETSU)");
            sql.Append("  LEFT OUTER JOIN TB_KY_SHAIN_JOHO AS M1");
            sql.Append("  ON (K1.KAISHA_CD = M1.KAISHA_CD)");
            sql.Append("  AND (K1.SHAIN_CD = M1.SHAIN_CD)");
            sql.Append("  WHERE K1.KAISHA_CD = @KAISHA_CD");
            sql.Append("  AND K1.NENGETSU = @NENGETSU");
            sql.Append("  AND K1.SHAIN_CD BETWEEN @SHAIN_CD_FR AND @SHAIN_CD_TO");
            sql.Append("  AND M1.BUMON_CD BETWEEN @BUMON_CD_FR AND @BUMON_CD_TO");
            sql.Append("  AND M1.BUKA_CD BETWEEN @BUKA_CD_FR AND @BUKA_CD_TO");
            #endregion
            // ORDER句
            if (this.rdoOrder1.Checked) 
            {
                sql.Append("  ORDER BY KAISHA_CD ASC,SHAIN_CD ASC");                            // 社員順
            }
            else
            {
                sql.Append("  ORDER BY KAISHA_CD ASC,BUMON_CD ASC,BUKA_CD ASC,SHAIN_CD ASC");   // 部門順
            }

            #endregion

            DataTable dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);
            if (dtMainLoop.Rows.Count == 0)
            {
                return false;
            }
            else
            {
                ArrayList alParamsPrKyTbl;      // ワークテーブルINSERT用パラメータ
                int sort = 0;                   // ソートフィールドカウンタ

                foreach (DataRow dr in dtMainLoop.Rows)
                {
                    sort++;     // カウントアップ

                    // データ登録
                    alParamsPrKyTbl = SetPrKyTblParams(sort, "",dtSti, dr);
                    this.Dba.Insert("PR_KY_TBL", (DbParamCollection)alParamsPrKyTbl[0]);
                    
                    // 控え印刷あり
                    if (this.rdoCopy1.Checked)
                    {
                        sort++;     // カウントアップ
                        // データ登録
                        alParamsPrKyTbl = SetPrKyTblParams(sort, "(控)",dtSti, dr);
                        this.Dba.Insert("PR_KY_TBL", (DbParamCollection)alParamsPrKyTbl[0]);
                    }
                }
            }

            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
                "SORT",
                "PR_KY_TBL",
                "GUID = @GUID",
                dpc);
            bool dataFlag;
            if (tmpdtPR_HN_TBL.Rows.Count > 0)
            {
                dataFlag = true;
            }
            else
            {
                dataFlag = false;
            }

            return dataFlag;
        }

        /// <summary>
        /// PR_KY_TBLに更新するためのパラメータ設定をします。
        /// </summary>
        /// <param name="sort">ソートフィールド値</param>
        /// <param name="note1">注記１</param>
        /// <param name="dtSti">賞与項目設定データ</param>
        /// <param name="dr">賞与明細データ行</param>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// </returns>
        private ArrayList SetPrKyTblParams(int sort, string note1, DataTable dtSti, DataRow dr)
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            // 共通情報
            updParam.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            updParam.SetParam("@SORT", SqlDbType.Int, sort);
            if (Util.ToInt(dr["BUMON_CD"]) > 0)
                updParam.SetParam("@ITEM001", SqlDbType.VarChar, 200, Util.ToString(dr["BUMON_CD"]));   // 部門コード
            if (Util.ToInt(dr["BUKA_CD"]) > 0)
                updParam.SetParam("@ITEM002", SqlDbType.VarChar, 200, Util.ToString(dr["BUMON_CD"]));   // 部課コード
            updParam.SetParam("@ITEM003", SqlDbType.VarChar, 200, Util.ToString(dr["SHAIN_CD"]));       // 社員コード
            updParam.SetParam("@ITEM004", SqlDbType.VarChar, 200, Util.ToString(dr["SHAIN_NM"]));       // 社員名
            updParam.SetParam("@ITEM005", SqlDbType.VarChar, 200, 
                string.Format("{0} {1}年 {2}月分", 
                    this.lblGengoTaisho.Text, this.txtGengoYearTaisho.Text, this.txtMonthTaisho.Text)); // 対象月
            updParam.SetParam("@ITEM006", SqlDbType.VarChar, 200, note1);                               // 注記１
            updParam.SetParam("@ITEM007", SqlDbType.VarChar, 200,
                string.Format("{0} {2}年 {3}月 {4}日", Util.ConvJpDate(Util.ToDate(dr["SHIKYUBI"]), this.Dba)));   // 支給日
            // 各項目別情報
            foreach (DataRow drSti in dtSti.Rows)
            {
                string wkField1 = "";   // 印刷ワーク見出しフィールド名
                string wkField2 = "";   // 印刷ワークフィールド名
                string srcField = "";   // 保存値照会フィールド名

                // 設定データの項目種別、項目番号からフィールド名セット
                // (1)勤怠見出し(011-030) 支給見出し(051-070) 控除見出し(091-110) 合計見出し(131-140)
                wkField1 = "ITEM" + string.Format("{0:D3}", 
                    ((Util.ToInt(drSti["KOMOKU_SHUBETSU"]) - 1) * 40 + 10 + Util.ToInt(drSti["KOMOKU_BANGO"])));
                // (2)勤怠項目値(031-050) 支給項目値(071-090) 控除項目値(111-130) 合計項目値(141-150)
                if (Util.ToInt(drSti["KOMOKU_SHUBETSU"]) < 4)
                {
                    wkField2 = "ITEM" + string.Format("{0:D3}",
                        ((Util.ToInt(drSti["KOMOKU_SHUBETSU"]) - 1) * 40 + 30 + Util.ToInt(drSti["KOMOKU_BANGO"])));
                }
                else
                {
                    wkField2 = "ITEM" + string.Format("{0:D3}",
                        ((Util.ToInt(drSti["KOMOKU_SHUBETSU"]) - 1) * 40 + 20 + Util.ToInt(drSti["KOMOKU_BANGO"])));
                }
                // (3)各項目値を参照するフィールド名
                switch (Util.ToInt(drSti["KOMOKU_SHUBETSU"]))
                {
                    case 1:     // 勤怠項目
                        srcField = "KINTAI_KOMOKU" + Util.ToInt(drSti["KOMOKU_BANGO"]);
                        break;
                    case 2:     // 支給項目
                        srcField = "SHIKYU_KOMOKU" + Util.ToString(drSti["KOMOKU_BANGO"]);
                        break;
                    case 3:     // 控除項目
                        srcField = "KOJO_KOMOKU" + Util.ToString(drSti["KOMOKU_BANGO"]);
                        break;
                    case 4:     // 合計項目
                        srcField = "GOKEI_KOMOKU" + Util.ToString(drSti["KOMOKU_BANGO"]);
                        break;
                    default:
                        break;
                }
                // 少数利用フラグ(少数0あり・1なし) 
                bool UseShosu = Util.ToString(drSti["KOMOKU_KUBUN2"]).Equals("0") ? true : false;

                // 見出し：印字区分＝1印字する/3見出しのみの場合に出力する
                if (Util.ToInt(drSti["INJI_KUBUN"]) == 1 | Util.ToInt(drSti["INJI_KUBUN"]) == 3)
                {
                    updParam.SetParam("@" + wkField1, SqlDbType.VarChar, 200, Util.ToString(drSti["KOMOKU_NM"]));                                 
                }

                // 項目値：印字区分＝1印字する/2項目のみの場合に出力する
                if (Util.ToInt(drSti["INJI_KUBUN"]) == 1 | Util.ToInt(drSti["INJI_KUBUN"]) == 2)
                {
                    if (UseShosu)  // 少数あり⇒0.00　少数なし⇒#,##0 の書式
                    {
                        updParam.SetParam("@" + wkField2, SqlDbType.VarChar, 200, Util.FormatNum(dr[srcField], 2));                                 
                    }
                    else
                    {
                        updParam.SetParam("@" + wkField2, SqlDbType.VarChar, 200, Util.FormatNum(dr[srcField]));                                 
                    }
                }
            }

            alParams.Add(updParam);

            return alParams;
        }

        /// <summary>
        /// 配列に格納された和暦支給月を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpDateNengetsu(string[] arrJpDate)
        {
            this.lblGengoNengetsu.Text = arrJpDate[0];
            this.txtGengoYearNengetsu.Text = arrJpDate[2];
            this.txtMonthNengetsu.Text = arrJpDate[3];
        }

        /// <summary>
        /// 配列に格納された和暦対象月を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpDateTaisho(string[] arrJpDate)
        {
            this.lblGengoTaisho.Text = arrJpDate[0];
            this.txtGengoYearTaisho.Text = arrJpDate[2];
            this.txtMonthTaisho.Text = arrJpDate[3];
        }
        #endregion
    }
}